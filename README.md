1) pdate submodules:   
	git submodule sync  
    git submodule update --init --progress

2) Prepare pg_wrapper dll file:  
	2a) Open ..\dev_env\pg_wrapper\PostgreSQLWrapper.sln in Visual Studio.  
	2b) Switch configuration solution to Release.  
	2c) Press F6 and build solution.    
	2d) File ..\dev_env\pg_wrapper\bin\Release\PostgreSQLWrapper.dll was created.

3) Prepare application for configuration total estimate calculations  
	Work on this application is in progress now. It could not operate properly.

	5a) Open ..\NfiEstaApp.sln in Visual Studio
	5b) Open project NfiEstaUI
	5b) In folder References check if path to PostgreSQLWrapper.dll is set correctly.  
		If not set path to the file you have created in step 2 : ..\dev_env\pg_wrapper\bin\Release\PostgreSQLWrapper.dll
	5e) Switch configuration solution to Release.  
	5f) Press F6 and build solution.  
	5g) File ..\nfiesta_ui\bin\Release\NfiEstaUI.exe was created.  
	5h) Click on file ..\nfiesta_ui\bin\Release\NfiEstaUI.exe to run application.  	
