﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.PostgreSQL;

namespace NfiEstaApp
{

    /// <summary>
    /// <para lang="cs">Formulář pro změny v nastavení aplikace</para>
    /// <para lang="en">Form for changes in application settings</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    internal partial class FormSettings
        : Form, INfiEstaControl
    {

        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        private Control controlOwner;

        private string msgSelectedLanguageVersionError = String.Empty;
        private string msgSelectedLocalDensityThreadCountError = String.Empty;
        private string msgSelectedAuxVarTotalThreadCountError = String.Empty;
        private string msgSelectedAuxiliaryDataRefreshTimeError = String.Empty;
        private string msgSelectedConfigurationThreadCountError = String.Empty;
        private string msgSelectedEstimateThreadCountError = String.Empty;
        private string msgSelectedLocalDensityThresholdError = String.Empty;
        private string msgSelectedAdditivityLimitError = String.Empty;
        private string msgSelectedCellCoverageToleranceError = String.Empty;

        private readonly ToolTip tipLocalDensityThreadCount = new();
        private readonly ToolTip tipLocalDensityThreshold = new();
        private readonly ToolTip tipAuxVarTotalThreadCount = new();
        private readonly ToolTip tipAuxVarTotalRefreshTime = new();
        private readonly ToolTip tipConfigurationThreadCount = new();
        private readonly ToolTip tipEstimateThreadCount = new();
        private readonly ToolTip tipAdditivityLimit = new();
        private readonly ToolTip tipCellCoverageTolerance = new();

        #endregion Private Fields


        #region Controls

        /// <summary>
        /// <para lang="cs">ComboBox pro výběr jazyka pro mezinárodní verzi</para>
        /// <para lang="en">ComboBox for language selection for international version</para>
        /// </summary>
        private ComboBox cboInternational;

        /// <summary>
        /// <para lang="cs">ComboBox pro výběr jazyka pro národní verzi</para>
        /// <para lang="en">ComboBox for language selection for national version</para>
        /// </summary>
        private ComboBox cboNational;

        #endregion Controls


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor formuláře</para>
        /// <para lang="en">Form constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        public FormSettings(Control controlOwner)
        {
            InitializeComponent();
            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Properties

        #region General

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                if (controlOwner == null)
                {
                    throw new ArgumentNullException(
                        message: $"Argument {nameof(ControlOwner)} must not be null.",
                        paramName: nameof(ControlOwner));
                }

                if (controlOwner is not FormMain)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(FormMain)}.",
                        paramName: nameof(ControlOwner));
                }

                return controlOwner;
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException(
                        message: $"Argument {nameof(ControlOwner)} must not be null.",
                        paramName: nameof(ControlOwner));
                }

                if (value is not FormMain)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(FormMain)}.",
                        paramName: nameof(ControlOwner));
                }

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables(read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((FormMain)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyk popisků (read-only)</para>
        /// <para lang="en">Language of labels (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((FormMain)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((FormMain)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyk pro národní verzi</para>
        /// <para lang="en">Langauge for national environment</para>
        /// </summary>
        public static LanguageItem NationalEnvironment
        {
            get
            {
                return
                    LanguageList.Items
                        .Where(a => a.Language == FormMain.NationalEnvironment)
                        .FirstOrDefault<LanguageItem>();
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyk pro mezinárodní verzi</para>
        /// <para lang="en">Langauge for international environment</para>
        /// </summary>
        public static LanguageItem InternationalEnvironment
        {
            get
            {
                return
                    LanguageList.Items
                        .Where(a => a.Language == FormMain.InternationalEnvironment)
                        .FirstOrDefault<LanguageItem>();
            }
        }

        #endregion General


        #region Module General

        /// <summary>
        /// <para lang="cs">Vybraná jazyková verze</para>
        /// <para lang="en">Selected language version</para>
        /// </summary>
        public LanguageVersion SelectedLanguageVersion
        {
            get
            {
                if (rdoNational.Checked && (!rdoInternational.Checked))
                {
                    return LanguageVersion.National;
                }

                if ((!rdoNational.Checked) && rdoInternational.Checked)
                {
                    return LanguageVersion.International;
                }

                throw new ArgumentOutOfRangeException(
                    message: "Unknown language version.",
                    paramName: nameof(SelectedLanguageVersion));
            }
            set
            {
                switch (value)
                {
                    case LanguageVersion.National:
                        rdoInternational.Checked = false;
                        rdoNational.Checked = true;
                        break;

                    case LanguageVersion.International:
                        rdoInternational.Checked = true;
                        rdoNational.Checked = false;
                        break;

                    default:
                        throw new ArgumentOutOfRangeException(
                            message: "Unknown language version.",
                            paramName: nameof(SelectedLanguageVersion));
                }

                InitializeLabels();
            }
        }

        /// <summary>
        /// <para lang="cs">Vybraný jazyk pro mezinárodní verzi</para>
        /// <para lang="en">Selected language for international environment</para>
        /// </summary>
        public Language SelectedInternationalEnvironment
        {
            get
            {
                if (cboInternational == null)
                {
                    return Language.EN;
                }

                if (cboInternational.SelectedItem == null)
                {
                    return Language.EN;
                }
                return
                   ((LanguageItem)cboInternational.SelectedItem).Language;
            }
        }

        /// <summary>
        /// <para lang="cs">Vybraný jazyk pro národní verzi</para>
        /// <para lang="en">Selected language for national environment</para>
        /// </summary>
        public Language SelectedNationalEnvironment
        {
            get
            {
                if (cboNational == null)
                {
                    return Language.EN;
                }

                if (cboNational.SelectedItem == null)
                {
                    return Language.EN;
                }
                return
                   ((LanguageItem)cboNational.SelectedItem).Language;
            }
        }

        #endregion Module General


        #region Module Target Data

        /// <summary>
        /// <para lang="cs">Vybraný počet vláken pro výpočet lokálních hustot</para>
        /// <para lang="en">Selected number of threads to calculate local densities</para>
        /// </summary>
        public int SelectedLocalDensityThreadCount
        {
            get
            {
                return
                    Functions.ConvertToInt(
                        text: txtLocalDensityThreadCount.Text.Trim(),
                        defaultValue: -1);
            }
            set
            {
                txtLocalDensityThreadCount.Text =
                    value.ToString();
            }
        }

        /// <summary>
        /// <para lang="cs">Vybraná mezní hodnota pro výpočet lokálních hustot</para>
        /// <para lang="en">Selected local density threshold</para>
        /// </summary>
        public double SelectedLocalDensityThreshold
        {
            get
            {
                return
                     Functions.ConvertToDouble(
                         text: txtLocalDensityThreshold.Text.Trim(),
                         defaultValue: -1);
            }
            set
            {
                txtLocalDensityThreshold.Text =
                    value.ToString();
            }
        }

        #endregion Module Target Data


        #region Module Auxiliary Data

        /// <summary>
        /// <para lang="cs">Vybraný počet vláken pro výpočet úhrnů pomocných proměnných</para>
        /// <para lang="en">Selected number of threads to calculate auxiliary variable totals</para>
        /// </summary>
        public int SelectedAuxVarTotalThreadCount
        {
            get
            {
                return
                    Functions.ConvertToInt(
                        text: txtAuxVarTotalThreadCount.Text.Trim(),
                        defaultValue: -1);
            }
            set
            {
                txtAuxVarTotalThreadCount.Text =
                    value.ToString();
            }
        }

        /// <summary>
        /// <para lang="cs">Vybraná doba pro překreslení formuláře při výpočtu úhrnů pomocné proměnné v milisekundách</para>
        /// <para lang="en">Selected time to redraw the form when calculating auxiliary variable totals in milliseconds</para>
        /// </summary>
        public int SelectedAuxiliaryDataRefreshTime
        {
            get
            {
                return
                    Functions.ConvertToInt(
                        text: txtAuxVarTotalRefreshTime.Text.Trim(),
                        defaultValue: -1);
            }
            set
            {
                txtAuxVarTotalRefreshTime.Text =
                    value.ToString();
            }
        }

        #endregion Module Auxiliary Data


        #region Module Estimate

        /// <summary>
        /// <para lang="cs">Vybraný počet vláken pro konfiguraci odhadů</para>
        /// <para lang="en">Selected number of threads to configurate estimates</para>
        /// </summary>
        public int SelectedConfigurationThreadCount
        {
            get
            {
                return
                    Functions.ConvertToInt(
                        text: txtConfigurationThreadCount.Text.Trim(),
                        defaultValue: -1);
            }
            set
            {
                txtConfigurationThreadCount.Text =
                    value.ToString();
            }
        }

        /// <summary>
        /// <para lang="cs">Vybraný počet vláken pro výpočet odhadů</para>
        /// <para lang="en">Selected number of threads to calculate estimates</para>
        /// </summary>
        public int SelectedEstimateThreadCount
        {
            get
            {
                return
                    Functions.ConvertToInt(
                        text: txtEstimateThreadCount.Text.Trim(),
                        defaultValue: -1);
            }
            set
            {
                txtEstimateThreadCount.Text =
                    value.ToString();
            }
        }

        /// <summary>
        /// <para lang="cs">Mezní hodnota pro kontrolu atributové a geografické aditivity</para>
        /// <para lang="en">Threshold for checking attribute and geographic additivity</para>
        /// </summary>
        public double SelectedAdditivityLimit
        {
            get
            {
                return
                     Functions.ConvertToDouble(
                         text: txtAdditivityLimit.Text.Trim(),
                         defaultValue: -1);
            }
            set
            {
                txtAdditivityLimit.Text =
                    value.ToString();
            }
        }

        /// <summary>
        /// <para lang="cs">Tolerance pokrytí oblasti odhadu</para>
        /// <para lang="en">Cell coverage tolerance</para>
        /// </summary>
        public double SelectedCellCoverageTolerance
        {
            get
            {
                return
                     Functions.ConvertToDouble(
                         text: txtCellCoverageTolerance.Text.Trim(),
                         defaultValue: -1);
            }
            set
            {
                txtCellCoverageTolerance.Text =
                    value.ToString();
            }
        }

        #endregion Module Estimate

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(FormSettings),                                 "Nastavení aplikace" },
                        { nameof(btnCancel),                                    "Zrušit" },
                        { nameof(btnOK),                                        "OK" },
                        { nameof(tpModuleGeneral),                              "OBECNÉ" },
                        { nameof(tpModuleTargetData),                           "TERÉNNÍ DATA" },
                        { nameof(tpModuleAuxiliaryData),                        "POMOCNÁ DATA" },
                        { nameof(tpModuleEstimate),                             "ODHADY" },
                        { nameof(grpLanguage),                                  "Jazyk:" },
                        { nameof(grpLocalDensityThreadCount),                   "Počet vláken pro výpočet lokálních hustot:" },
                        { nameof(tipLocalDensityThreadCount),                   "Počet vláken pro výpočet lokálních hustot." },
                        { nameof(grpLocalDensityThreshold),                     "Mezní hodnota pro výpočet lokálních hustot:" },
                        { nameof(tipLocalDensityThreshold),                     "Mezní hodnota pro výpočet lokálních hustot." },
                        { nameof(grpAuxVarTotalThreadCount),                    "Počet vláken pro výpočet úhrnů pomocných proměnných:" },
                        { nameof(tipAuxVarTotalThreadCount),                    "Počet vláken pro výpočet úhrnů pomocných proměnných." },
                        { nameof(grpAuxVarTotalRefreshTime),                    "Doba pro překreslení formuláře při výpočtu úhrnů pomocné proměnné v milisekundách:" },
                        { nameof(tipAuxVarTotalRefreshTime),                    "Doba pro překreslení formuláře při výpočtu úhrnů pomocné proměnné v milisekundách." },
                        { nameof(grpConfigurationThreadCount),                  "Počet vláken pro konfiguraci odhadů:" },
                        { nameof(tipConfigurationThreadCount),                  "Počet vláken pro konfiguraci odhadů." },
                        { nameof(grpEstimateThreadCount),                       "Počet vláken pro výpočet odhadů:" },
                        { nameof(tipEstimateThreadCount),                       "Počet vláken pro výpočet odhadů." },
                        { nameof(grpAdditivityLimit),                           "Mezní hodnota pro kontrolu atributové a geografické aditivity:" },
                        { nameof(tipAdditivityLimit),                           "Mezní hodnota pro kontrolu atributové a geografické aditivity." },
                        { nameof(grpCellCoverageTolerance),                     "Tolerance pokrytí oblasti odhadu:" },
                        { nameof(tipCellCoverageTolerance),                     String.Concat(
                                                                                "Minimální podíl plochy odhadovací oblasti, který musí být pokryt výběrovými straty, ",
                                                                                "aby byla oblast považována za plně pokrytou.") },
                        { nameof(rdoInternational),                             "Mezinárodní prostředí:" },
                        { nameof(rdoNational),                                  "Národní prostředí:" },
                        { nameof(msgSelectedLanguageVersionError),              "Neznámá jazyková verze." },
                        { nameof(msgSelectedLocalDensityThreadCountError),      "Počet vláken pro výpočet lokálních hustot musí být celé číslo v rozsahu 1 až 100." },
                        { nameof(msgSelectedAuxVarTotalThreadCountError),       "Počet vláken pro pro výpočet úhrnů pomocných proměnných musí být celé číslo v rozsahu 1 až 100." },
                        { nameof(msgSelectedConfigurationThreadCountError),     "Počet vláken pro konfiguraci odhadů musí být celé číslo v rozsahu 1 až 100." },
                        { nameof(msgSelectedEstimateThreadCountError),          "Počet vláken pro výpočet odhadů musí být celé číslo v rozsahu 1 až 100." },
                        { nameof(msgSelectedAuxiliaryDataRefreshTimeError),     "Doba pro překreslení formuláře musí být celé číslo v rozsahu 0 až 100000 ms." },
                        { nameof(msgSelectedLocalDensityThresholdError),        "Mezní hodnota pro výpočet lokálních hustot musí být číslo v rozsahu 0 až 1000." },
                        { nameof(msgSelectedAdditivityLimitError),              "Mezní hodnota pro kontrolu atributové a geografické aditivity musí být číslo v rozsahu 0 až 1000." },
                        { nameof(msgSelectedCellCoverageToleranceError),        "Tolerance pokrytí oblasti odhadu musí být desetinné číslo v rozsahu 0 až 1 (defaultně 0.001)." }
                    }
                    : languageFile.NationalVersion.MainApplicationDictionary.TryGetValue(
                        key: nameof(FormSettings),
                        out Dictionary<string, string> dictNational)
                            ? dictNational
            : [],

                LanguageVersion.International => (languageFile == null)
                        ? new Dictionary<string, string>()
                        {
                        { nameof(FormSettings),                                 "Settings" },
                        { nameof(btnCancel),                                    "Cancel" },
                        { nameof(btnOK),                                        "OK" },
                        { nameof(tpModuleGeneral),                              "GENERAL" },
                        { nameof(tpModuleTargetData),                           "FIELD DATA" },
                        { nameof(tpModuleAuxiliaryData),                        "GIS DATA" },
                        { nameof(tpModuleEstimate),                             "ESTIMATES" },
                        { nameof(grpLanguage),                                  "Language:" },
                        { nameof(grpLocalDensityThreadCount),                   "Number of threads to calculate local densities:" },
                        { nameof(tipLocalDensityThreadCount),                   "Number of threads to calculate local densities." },
                        { nameof(grpLocalDensityThreshold),                     "Local density threshold:" },
                        { nameof(tipLocalDensityThreshold),                     "Local density threshold." },
                        { nameof(grpAuxVarTotalThreadCount),                    "Number of threads to calculate auxiliary variable totals:" },
                        { nameof(tipAuxVarTotalThreadCount),                    "Number of threads to calculate auxiliary variable totals." },
                        { nameof(grpAuxVarTotalRefreshTime),                    "Time to redraw the form when calculating auxiliary variable totals in milliseconds:" },
                        { nameof(tipAuxVarTotalRefreshTime),                    "Time to redraw the form when calculating auxiliary variable totals in milliseconds." },
                        { nameof(grpConfigurationThreadCount),                  "Number of threads to configure estimates:" },
                        { nameof(tipConfigurationThreadCount),                  "Number of threads to configure estimates." },
                        { nameof(grpEstimateThreadCount),                       "Number of threads to calculate estimates:" },
                        { nameof(tipEstimateThreadCount),                       "Number of threads to calculate estimates." },
                        { nameof(grpAdditivityLimit),                           "Threshold for checking attribute and geographic additivity:" },
                        { nameof(tipAdditivityLimit),                           "Threshold for checking attribute and geographic additivity." },
                        { nameof(grpCellCoverageTolerance),                     "Cell coverage tolerance:" },
                        { nameof(tipCellCoverageTolerance),                     String.Concat(
                                                                                "The minimum relative proportion of the cell area that must be covered by sampling strata ",
                                                                                "for the cell to be considered as fully covered") },
                        { nameof(rdoInternational),                             "International environment:" },
                        { nameof(rdoNational),                                  "National environment:" },
                        { nameof(msgSelectedLanguageVersionError),              "Unknown language version." },
                        { nameof(msgSelectedLocalDensityThreadCountError),      "The number of threads to calculate local densities must be an integer number in the range of 1 to 100." },
                        { nameof(msgSelectedAuxVarTotalThreadCountError),       "The number of threads to calculate auxiliary variable totals must be an integer number in the range of 1 to 100." },
                        { nameof(msgSelectedAuxiliaryDataRefreshTimeError),     "Time to redraw the form when calculating auxiliary variable totals must be an integer number in the range 0 až 100000 ms." },
                        { nameof(msgSelectedConfigurationThreadCountError),     "The number of threads to configure estimates must be an integer number in the range of 1 to 100." },
                        { nameof(msgSelectedEstimateThreadCountError),          "The number of threads to calculate estimates must be an integer number in the range of 1 to 100." },
                        { nameof(msgSelectedLocalDensityThresholdError),        "The local density threshold must be a number in the range 0 to 1000." },
                        { nameof(msgSelectedAdditivityLimitError),              "The threshold for checking attribute and geographic additivity must be a number in the range 0 to 1000." },
                        { nameof(msgSelectedCellCoverageToleranceError),        "Cell coverage tolerance must be an decimal number in the range of 0 to 1 (default 0.001)." }
                        }
                        : languageFile.InternationalVersion.MainApplicationDictionary.TryGetValue(
                            key: nameof(FormSettings),
                            out Dictionary<string, string> value)
                                ? value
                                : [],
                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace formuláře</para>
        /// <para lang="en">Initializing the form</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        private void Initialize(Control controlOwner)
        {
            ControlOwner = controlOwner;

            // Module General:

            pnlInternationalValue.Controls.Clear();
            cboInternational = new ComboBox()
            {
                DataSource = LanguageList.Items,
                DisplayMember = "Name",
                ValueMember = "Id",
                Enabled = false,
                Dock = DockStyle.Fill,
                DropDownStyle = ComboBoxStyle.DropDownList,
                FlatStyle = FlatStyle.Flat,
                Font = new Font(
                   familyName: "Segoe UI",
                   emSize: 9F,
                   style: FontStyle.Regular,
                   unit: GraphicsUnit.Point,
                   gdiCharSet: 0),
                Margin = new Padding(all: 0),
                Padding = new Padding(all: 0)
            };
            cboInternational.CreateControl();
            pnlInternationalValue.Controls.Add(value: cboInternational);

            pnlNationalValue.Controls.Clear();
            cboNational = new ComboBox()
            {
                DataSource = LanguageList.Items,
                DisplayMember = "Name",
                ValueMember = "Id",
                Enabled = false,
                Dock = DockStyle.Fill,
                DropDownStyle = ComboBoxStyle.DropDownList,
                FlatStyle = FlatStyle.Flat,
                Font = new Font(
                   familyName: "Segoe UI",
                   emSize: 9F,
                   style: FontStyle.Regular,
                   unit: GraphicsUnit.Point,
                   gdiCharSet: 0),
                Margin = new Padding(all: 0),
                Padding = new Padding(all: 0)
            };
            cboNational.CreateControl();
            pnlNationalValue.Controls.Add(value: cboNational);

            cboInternational.SelectedValue = InternationalEnvironment.Id;
            cboNational.SelectedValue = NationalEnvironment.Id;

            // Defaultní hodnoty:

            SelectedLanguageVersion =
                LanguageVersion.International;

            SelectedLocalDensityThreadCount =
                ZaJi.ModuleTargetData.Setting.DefaultLocalDensityRequiredThreadsNumber;

            SelectedLocalDensityThreshold =
                ZaJi.ModuleTargetData.Setting.DefaultLocalDensityThreshold;

            SelectedAuxVarTotalThreadCount =
                ZaJi.ModuleAuxiliaryData.Setting.DefaultAuxiliaryDataThreadCount;

            SelectedAuxiliaryDataRefreshTime =
                ZaJi.ModuleAuxiliaryData.Setting.DefaultRefreshTime;

            SelectedConfigurationThreadCount =
                ZaJi.ModuleEstimate.Setting.DefaultConfigurationRequiredThreadsNumber;

            SelectedEstimateThreadCount =
                ZaJi.ModuleEstimate.Setting.DefaultEstimateRequiredThreadsNumber;

            SelectedAdditivityLimit =
                ZaJi.ModuleEstimate.Setting.DefaultAdditivityLimit;

            SelectedCellCoverageTolerance =
                ZaJi.ModuleEstimate.Setting.DefaultCellCoverageTolerance;

            // Event handlers:

            btnCancel.Click += new EventHandler(
                (sender, e) =>
                {
                    DialogResult = DialogResult.Cancel;
                    Close();
                });

            btnOK.Click += new EventHandler(
                (sender, e) =>
                {
                    DialogResult = DialogResult.OK;
                    Close();
                });

            rdoInternational.CheckedChanged += new EventHandler(
                (sender, e) =>
                {
                    rdoNational.Checked = !rdoInternational.Checked;
                    InitializeLabels();
                });

            rdoNational.CheckedChanged += new EventHandler(
                (sender, e) =>
                {
                    rdoInternational.Checked = !rdoNational.Checked;
                    InitializeLabels();
                });

            FormClosing += new FormClosingEventHandler(
                (sender, e) =>
                {
                    if (DialogResult == DialogResult.OK)
                    {
                        e.Cancel = !TestSettings();
                    }
                    else
                    {
                        e.Cancel = false;
                    }
                });

        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků formuláře</para>
        /// <para lang="en">Initializing form labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
                languageVersion: SelectedLanguageVersion,
                languageFile: LanguageFile);

            Text =
                labels.TryGetValue(key: nameof(FormSettings),
                out string frmCaptionText)
                    ? frmCaptionText
                    : String.Empty;

            btnCancel.Text =
                labels.TryGetValue(key: nameof(btnCancel),
                out string btnCancelText)
                    ? btnCancelText
                    : String.Empty;

            btnOK.Text =
                labels.TryGetValue(key: nameof(btnOK),
                out string btnOKText)
                    ? btnOKText
                    : String.Empty;

            tpModuleGeneral.Text =
                labels.TryGetValue(key: nameof(tpModuleGeneral),
                out string tpModuleGeneralText)
                    ? tpModuleGeneralText
                    : String.Empty;

            tpModuleTargetData.Text =
                labels.TryGetValue(key: nameof(tpModuleTargetData),
                out string tpModuleTargetDataText)
                    ? tpModuleTargetDataText
                    : String.Empty;

            tpModuleAuxiliaryData.Text =
                labels.TryGetValue(key: nameof(tpModuleAuxiliaryData),
                out string tpModuleAuxiliaryDataText)
                    ? tpModuleAuxiliaryDataText
                    : String.Empty;

            tpModuleEstimate.Text =
                labels.TryGetValue(key: nameof(tpModuleEstimate),
                out string tpModuleEstimateText)
                    ? tpModuleEstimateText
                    : String.Empty;

            grpLanguage.Text =
                labels.TryGetValue(key: nameof(grpLanguage),
                out string grpLanguageText)
                    ? grpLanguageText
                    : String.Empty;

            grpLocalDensityThreadCount.Text =
                labels.TryGetValue(key: nameof(grpLocalDensityThreadCount),
                out string grpLocalDensityThreadCountText)
                    ? grpLocalDensityThreadCountText
                    : String.Empty;

            tipLocalDensityThreadCount.SetToolTip(
                control: txtLocalDensityThreadCount,
                caption:
                    labels.TryGetValue(key: nameof(tipLocalDensityThreadCount),
                    out string tipLocalDensityThreadCountText)
                        ? tipLocalDensityThreadCountText
                        : String.Empty);

            grpLocalDensityThreshold.Text =
                labels.TryGetValue(key: nameof(grpLocalDensityThreshold),
                out string grpLocalDensityThresholdText)
                    ? grpLocalDensityThresholdText
                    : String.Empty;

            tipLocalDensityThreshold.SetToolTip(
                control: txtLocalDensityThreshold,
                caption:
                    labels.TryGetValue(key: nameof(tipLocalDensityThreshold),
                    out string tipLocalDensityThresholdText)
                        ? tipLocalDensityThresholdText
                        : String.Empty);

            grpAuxVarTotalThreadCount.Text =
                labels.TryGetValue(key: nameof(grpAuxVarTotalThreadCount),
                out string grpAuxVarTotalThreadCountText)
                    ? grpAuxVarTotalThreadCountText
                    : String.Empty;

            tipAuxVarTotalThreadCount.SetToolTip(
                control: txtAuxVarTotalThreadCount,
                caption:
                    labels.TryGetValue(key: nameof(tipAuxVarTotalThreadCount),
                    out string tipAuxVarTotalThreadCountText)
                        ? tipAuxVarTotalThreadCountText
                        : String.Empty);

            grpAuxVarTotalRefreshTime.Text =
                labels.TryGetValue(key: nameof(grpAuxVarTotalRefreshTime),
                out string grpAuxVarTotalRefreshTimeText)
                    ? grpAuxVarTotalRefreshTimeText
                    : String.Empty;

            tipAuxVarTotalRefreshTime.SetToolTip(
                control: txtAuxVarTotalRefreshTime,
                caption:
                    labels.TryGetValue(key: nameof(tipAuxVarTotalRefreshTime),
                    out string tipAuxVarTotalRefreshTimeText)
                        ? tipAuxVarTotalRefreshTimeText
                        : String.Empty);

            grpConfigurationThreadCount.Text =
                labels.TryGetValue(key: nameof(grpConfigurationThreadCount),
                out string grpConfigurationThreadCountText)
                    ? grpConfigurationThreadCountText
                    : String.Empty;

            tipConfigurationThreadCount.SetToolTip(
                control: txtConfigurationThreadCount,
                caption:
                    labels.TryGetValue(key: nameof(tipConfigurationThreadCount),
                    out string tipConfigurationThreadCountText)
                        ? tipConfigurationThreadCountText
                        : String.Empty);

            grpEstimateThreadCount.Text =
                labels.TryGetValue(key: nameof(grpEstimateThreadCount),
                out string grpEstimateThreadCountText)
                    ? grpEstimateThreadCountText
                    : String.Empty;

            tipEstimateThreadCount.SetToolTip(
                control: txtEstimateThreadCount,
                caption:
                    labels.TryGetValue(key: nameof(tipEstimateThreadCount),
                    out string tipEstimateThreadCountText)
                        ? tipEstimateThreadCountText
                        : String.Empty);

            grpAdditivityLimit.Text =
                labels.TryGetValue(key: nameof(grpAdditivityLimit),
                out string grpAdditivityLimitText)
                    ? grpAdditivityLimitText
                    : String.Empty;

            tipAdditivityLimit.SetToolTip(
                 control: txtAdditivityLimit,
                 caption:
                     labels.TryGetValue(key: nameof(tipAdditivityLimit),
                     out string tipAdditivityLimitText)
                         ? tipAdditivityLimitText
                         : String.Empty);

            grpCellCoverageTolerance.Text =
                labels.TryGetValue(key: nameof(grpCellCoverageTolerance),
                out string grpCellCoverageToleranceText)
                    ? grpCellCoverageToleranceText
                    : String.Empty;

            tipCellCoverageTolerance.SetToolTip(
                control: txtCellCoverageTolerance,
                caption:
                    labels.TryGetValue(key: nameof(tipCellCoverageTolerance),
                    out string tipCellCoverageToleranceText)
                        ? tipCellCoverageToleranceText
                        : String.Empty);

            rdoInternational.Text =
                labels.TryGetValue(key: nameof(rdoInternational),
                out string rdoInternationalText)
                    ? rdoInternationalText
                    : String.Empty;

            rdoNational.Text =
                 labels.TryGetValue(key: nameof(rdoNational),
                 out string rdoNationalText)
                     ? rdoNationalText
                     : String.Empty;

            msgSelectedLanguageVersionError =
                labels.TryGetValue(key: nameof(msgSelectedLanguageVersionError),
                out msgSelectedLanguageVersionError)
                    ? msgSelectedLanguageVersionError
                    : String.Empty;

            msgSelectedLocalDensityThreadCountError =
                labels.TryGetValue(key: nameof(msgSelectedLocalDensityThreadCountError),
                out msgSelectedLocalDensityThreadCountError)
                    ? msgSelectedLocalDensityThreadCountError
                    : String.Empty;

            msgSelectedLocalDensityThresholdError =
                labels.TryGetValue(key: nameof(msgSelectedLocalDensityThresholdError),
                out msgSelectedLocalDensityThresholdError)
                    ? msgSelectedLocalDensityThresholdError
                    : String.Empty;

            msgSelectedAuxVarTotalThreadCountError =
                labels.TryGetValue(key: nameof(msgSelectedAuxVarTotalThreadCountError),
                out msgSelectedAuxVarTotalThreadCountError)
                    ? msgSelectedAuxVarTotalThreadCountError
                    : String.Empty;

            msgSelectedAuxiliaryDataRefreshTimeError =
                labels.TryGetValue(key: nameof(msgSelectedAuxiliaryDataRefreshTimeError),
                out msgSelectedAuxiliaryDataRefreshTimeError)
                    ? msgSelectedAuxiliaryDataRefreshTimeError
                    : String.Empty;

            msgSelectedConfigurationThreadCountError =
                labels.TryGetValue(key: nameof(msgSelectedConfigurationThreadCountError),
                out msgSelectedConfigurationThreadCountError)
                    ? msgSelectedConfigurationThreadCountError
                    : String.Empty;

            msgSelectedEstimateThreadCountError =
                labels.TryGetValue(key: nameof(msgSelectedEstimateThreadCountError),
                out msgSelectedEstimateThreadCountError)
                    ? msgSelectedEstimateThreadCountError
                    : String.Empty;

            msgSelectedAdditivityLimitError =
                labels.TryGetValue(key: nameof(msgSelectedAdditivityLimitError),
                out msgSelectedAdditivityLimitError)
                    ? msgSelectedAdditivityLimitError
                    : String.Empty;

            msgSelectedCellCoverageToleranceError =
                labels.TryGetValue(key: nameof(msgSelectedCellCoverageToleranceError),
                out msgSelectedCellCoverageToleranceError)
                    ? msgSelectedCellCoverageToleranceError
                    : String.Empty;
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent() { }

        /// <summary>
        /// <para lang="cs">Ověří, zda hodnoty nastavení jsou správně zadané</para>
        /// <para lang="en">Verifies that the setting values are correctly entered</para>
        /// </summary>
        /// <returns>
        /// <para lang="cs">Vrací true, pokud hodnoty nastavení jsou správně zadané, jinak false</para>
        /// <para lang="en">Returns true if the setting values are correctly entered, false otherwise</para>
        /// </returns>
        private bool TestSettings()
        {
            try
            {
                LanguageVersion lv = SelectedLanguageVersion;
            }
            catch
            {
                MessageBox.Show(
                    text: msgSelectedLanguageVersionError,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return false;
            }

            if (!(
                (SelectedLocalDensityThreadCount >= 1) &&
                (SelectedLocalDensityThreadCount <= 100)))
            {
                MessageBox.Show(
                    text: msgSelectedLocalDensityThreadCountError,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            if (!(
                (SelectedLocalDensityThreshold >= 0.0) &&
                (SelectedLocalDensityThreshold <= 1000.0)))
            {
                MessageBox.Show(
                    text: msgSelectedLocalDensityThresholdError,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            if (!(
                (SelectedAuxVarTotalThreadCount >= 1) &&
                (SelectedAuxVarTotalThreadCount <= 100)))
            {
                MessageBox.Show(
                    text: msgSelectedAuxVarTotalThreadCountError,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            if (!(
                (SelectedAuxiliaryDataRefreshTime >= 0) &&
                (SelectedAuxiliaryDataRefreshTime <= 100000)))
            {
                MessageBox.Show(
                    text: msgSelectedAuxiliaryDataRefreshTimeError,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            if (!(
                (SelectedConfigurationThreadCount >= 1) &&
                (SelectedConfigurationThreadCount <= 100)))
            {
                MessageBox.Show(
                    text: msgSelectedConfigurationThreadCountError,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            if (!(
                (SelectedEstimateThreadCount >= 1) &&
                (SelectedEstimateThreadCount <= 100)))
            {
                MessageBox.Show(
                    text: msgSelectedEstimateThreadCountError,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            if (!(
                (SelectedAdditivityLimit >= 0.0) &&
                (SelectedAdditivityLimit <= 1000.0)))
            {
                MessageBox.Show(
                    text: msgSelectedAdditivityLimitError,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            if (!(
                (SelectedCellCoverageTolerance > 0.0) &&
                (SelectedCellCoverageTolerance < 1.0)))
            {
                MessageBox.Show(
                    text: msgSelectedCellCoverageToleranceError,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return false;
            }

            return true;
        }

        #endregion

    }

}
