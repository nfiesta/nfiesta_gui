﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace NfiEstaApp
{
    partial class FormInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            pnlMain = new System.Windows.Forms.Panel();
            tlpMain = new System.Windows.Forms.TableLayoutPanel();
            tlpButtons = new System.Windows.Forms.TableLayoutPanel();
            pnlOK = new System.Windows.Forms.Panel();
            btnOK = new System.Windows.Forms.Button();
            pnlCancel = new System.Windows.Forms.Panel();
            btnCancel = new System.Windows.Forms.Button();
            pnlInfo = new System.Windows.Forms.Panel();
            grpModuleETL = new System.Windows.Forms.GroupBox();
            lblModuleETLSDesignExtension = new System.Windows.Forms.Label();
            lblModuleETLNfiestaExtension = new System.Windows.Forms.Label();
            lblModuleETLExportConnection = new System.Windows.Forms.Label();
            lblModuleETLTargetDataExtension = new System.Windows.Forms.Label();
            lblModuleETLConnection = new System.Windows.Forms.Label();
            grpModuleEstimate = new System.Windows.Forms.GroupBox();
            lblModuleEstimateSDesignExtension = new System.Windows.Forms.Label();
            lblModuleEstimateNfiestaExtension = new System.Windows.Forms.Label();
            lblModuleEstimateConnection = new System.Windows.Forms.Label();
            grpModuleAuxiliaryData = new System.Windows.Forms.GroupBox();
            lblModuleAuxiliaryDataConnection = new System.Windows.Forms.Label();
            grpModuleTargetData = new System.Windows.Forms.GroupBox();
            lblModuleTargetDataExtension = new System.Windows.Forms.Label();
            lblModuleTargetDataConnection = new System.Windows.Forms.Label();
            pnlMain.SuspendLayout();
            tlpMain.SuspendLayout();
            tlpButtons.SuspendLayout();
            pnlOK.SuspendLayout();
            pnlCancel.SuspendLayout();
            pnlInfo.SuspendLayout();
            grpModuleETL.SuspendLayout();
            grpModuleEstimate.SuspendLayout();
            grpModuleAuxiliaryData.SuspendLayout();
            grpModuleTargetData.SuspendLayout();
            SuspendLayout();
            // 
            // pnlMain
            // 
            pnlMain.BackColor = System.Drawing.Color.DarkSeaGreen;
            pnlMain.Controls.Add(tlpMain);
            pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlMain.Location = new System.Drawing.Point(0, 0);
            pnlMain.Margin = new System.Windows.Forms.Padding(0);
            pnlMain.Name = "pnlMain";
            pnlMain.Padding = new System.Windows.Forms.Padding(3);
            pnlMain.Size = new System.Drawing.Size(960, 540);
            pnlMain.TabIndex = 3;
            // 
            // tlpMain
            // 
            tlpMain.BackColor = System.Drawing.Color.WhiteSmoke;
            tlpMain.ColumnCount = 1;
            tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.Controls.Add(tlpButtons, 0, 1);
            tlpMain.Controls.Add(pnlInfo, 0, 0);
            tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpMain.Location = new System.Drawing.Point(3, 3);
            tlpMain.Margin = new System.Windows.Forms.Padding(0);
            tlpMain.Name = "tlpMain";
            tlpMain.RowCount = 2;
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            tlpMain.Size = new System.Drawing.Size(954, 534);
            tlpMain.TabIndex = 3;
            // 
            // tlpButtons
            // 
            tlpButtons.BackColor = System.Drawing.Color.Transparent;
            tlpButtons.ColumnCount = 3;
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            tlpButtons.Controls.Add(pnlOK, 1, 0);
            tlpButtons.Controls.Add(pnlCancel, 2, 0);
            tlpButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpButtons.Location = new System.Drawing.Point(0, 494);
            tlpButtons.Margin = new System.Windows.Forms.Padding(0);
            tlpButtons.Name = "tlpButtons";
            tlpButtons.RowCount = 1;
            tlpButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.Size = new System.Drawing.Size(954, 40);
            tlpButtons.TabIndex = 4;
            // 
            // pnlOK
            // 
            pnlOK.Controls.Add(btnOK);
            pnlOK.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlOK.Location = new System.Drawing.Point(634, 0);
            pnlOK.Margin = new System.Windows.Forms.Padding(0);
            pnlOK.Name = "pnlOK";
            pnlOK.Padding = new System.Windows.Forms.Padding(5);
            pnlOK.Size = new System.Drawing.Size(160, 40);
            pnlOK.TabIndex = 13;
            // 
            // btnOK
            // 
            btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            btnOK.Dock = System.Windows.Forms.DockStyle.Fill;
            btnOK.Font = new System.Drawing.Font("Segoe UI", 9F);
            btnOK.Location = new System.Drawing.Point(5, 5);
            btnOK.Margin = new System.Windows.Forms.Padding(0);
            btnOK.Name = "btnOK";
            btnOK.Size = new System.Drawing.Size(150, 30);
            btnOK.TabIndex = 12;
            btnOK.Text = "btnOK";
            btnOK.UseVisualStyleBackColor = true;
            btnOK.Visible = false;
            // 
            // pnlCancel
            // 
            pnlCancel.Controls.Add(btnCancel);
            pnlCancel.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlCancel.Location = new System.Drawing.Point(794, 0);
            pnlCancel.Margin = new System.Windows.Forms.Padding(0);
            pnlCancel.Name = "pnlCancel";
            pnlCancel.Padding = new System.Windows.Forms.Padding(5);
            pnlCancel.Size = new System.Drawing.Size(160, 40);
            pnlCancel.TabIndex = 12;
            // 
            // btnCancel
            // 
            btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            btnCancel.Dock = System.Windows.Forms.DockStyle.Fill;
            btnCancel.Font = new System.Drawing.Font("Segoe UI", 9F);
            btnCancel.Location = new System.Drawing.Point(5, 5);
            btnCancel.Margin = new System.Windows.Forms.Padding(0);
            btnCancel.Name = "btnCancel";
            btnCancel.Size = new System.Drawing.Size(150, 30);
            btnCancel.TabIndex = 13;
            btnCancel.Text = "btnCancel";
            btnCancel.UseVisualStyleBackColor = true;
            // 
            // pnlInfo
            // 
            pnlInfo.AutoScroll = true;
            pnlInfo.Controls.Add(grpModuleETL);
            pnlInfo.Controls.Add(grpModuleEstimate);
            pnlInfo.Controls.Add(grpModuleAuxiliaryData);
            pnlInfo.Controls.Add(grpModuleTargetData);
            pnlInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlInfo.Location = new System.Drawing.Point(0, 0);
            pnlInfo.Margin = new System.Windows.Forms.Padding(0);
            pnlInfo.Name = "pnlInfo";
            pnlInfo.Padding = new System.Windows.Forms.Padding(5);
            pnlInfo.Size = new System.Drawing.Size(954, 494);
            pnlInfo.TabIndex = 2;
            // 
            // grpModuleETL
            // 
            grpModuleETL.BackColor = System.Drawing.Color.Transparent;
            grpModuleETL.Controls.Add(lblModuleETLSDesignExtension);
            grpModuleETL.Controls.Add(lblModuleETLNfiestaExtension);
            grpModuleETL.Controls.Add(lblModuleETLExportConnection);
            grpModuleETL.Controls.Add(lblModuleETLTargetDataExtension);
            grpModuleETL.Controls.Add(lblModuleETLConnection);
            grpModuleETL.Dock = System.Windows.Forms.DockStyle.Top;
            grpModuleETL.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            grpModuleETL.ForeColor = System.Drawing.Color.MediumBlue;
            grpModuleETL.Location = new System.Drawing.Point(5, 305);
            grpModuleETL.Margin = new System.Windows.Forms.Padding(0);
            grpModuleETL.Name = "grpModuleETL";
            grpModuleETL.Padding = new System.Windows.Forms.Padding(5);
            grpModuleETL.Size = new System.Drawing.Size(944, 125);
            grpModuleETL.TabIndex = 23;
            grpModuleETL.TabStop = false;
            grpModuleETL.Text = "grpModuleETL";
            // 
            // lblModuleETLSDesignExtension
            // 
            lblModuleETLSDesignExtension.AutoEllipsis = true;
            lblModuleETLSDesignExtension.Dock = System.Windows.Forms.DockStyle.Top;
            lblModuleETLSDesignExtension.Font = new System.Drawing.Font("Segoe UI", 9F);
            lblModuleETLSDesignExtension.ForeColor = System.Drawing.Color.Black;
            lblModuleETLSDesignExtension.Location = new System.Drawing.Point(5, 105);
            lblModuleETLSDesignExtension.Margin = new System.Windows.Forms.Padding(0);
            lblModuleETLSDesignExtension.Name = "lblModuleETLSDesignExtension";
            lblModuleETLSDesignExtension.Size = new System.Drawing.Size(934, 20);
            lblModuleETLSDesignExtension.TabIndex = 5;
            lblModuleETLSDesignExtension.Text = "lblModuleETLSDesignExtension";
            lblModuleETLSDesignExtension.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblModuleETLNfiestaExtension
            // 
            lblModuleETLNfiestaExtension.AutoEllipsis = true;
            lblModuleETLNfiestaExtension.Dock = System.Windows.Forms.DockStyle.Top;
            lblModuleETLNfiestaExtension.Font = new System.Drawing.Font("Segoe UI", 9F);
            lblModuleETLNfiestaExtension.ForeColor = System.Drawing.Color.Black;
            lblModuleETLNfiestaExtension.Location = new System.Drawing.Point(5, 85);
            lblModuleETLNfiestaExtension.Margin = new System.Windows.Forms.Padding(0);
            lblModuleETLNfiestaExtension.Name = "lblModuleETLNfiestaExtension";
            lblModuleETLNfiestaExtension.Size = new System.Drawing.Size(934, 20);
            lblModuleETLNfiestaExtension.TabIndex = 4;
            lblModuleETLNfiestaExtension.Text = "lblModuleETLNfiestaExtension";
            lblModuleETLNfiestaExtension.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblModuleETLExportConnection
            // 
            lblModuleETLExportConnection.AutoEllipsis = true;
            lblModuleETLExportConnection.Dock = System.Windows.Forms.DockStyle.Top;
            lblModuleETLExportConnection.Font = new System.Drawing.Font("Segoe UI", 9F);
            lblModuleETLExportConnection.ForeColor = System.Drawing.Color.Black;
            lblModuleETLExportConnection.Location = new System.Drawing.Point(5, 65);
            lblModuleETLExportConnection.Margin = new System.Windows.Forms.Padding(0);
            lblModuleETLExportConnection.Name = "lblModuleETLExportConnection";
            lblModuleETLExportConnection.Size = new System.Drawing.Size(934, 20);
            lblModuleETLExportConnection.TabIndex = 3;
            lblModuleETLExportConnection.Text = "lblModuleETLExportConnection";
            lblModuleETLExportConnection.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblModuleETLTargetDataExtension
            // 
            lblModuleETLTargetDataExtension.AutoEllipsis = true;
            lblModuleETLTargetDataExtension.Dock = System.Windows.Forms.DockStyle.Top;
            lblModuleETLTargetDataExtension.Font = new System.Drawing.Font("Segoe UI", 9F);
            lblModuleETLTargetDataExtension.ForeColor = System.Drawing.Color.Black;
            lblModuleETLTargetDataExtension.Location = new System.Drawing.Point(5, 45);
            lblModuleETLTargetDataExtension.Margin = new System.Windows.Forms.Padding(0);
            lblModuleETLTargetDataExtension.Name = "lblModuleETLTargetDataExtension";
            lblModuleETLTargetDataExtension.Size = new System.Drawing.Size(934, 20);
            lblModuleETLTargetDataExtension.TabIndex = 2;
            lblModuleETLTargetDataExtension.Text = "lblModuleETLTargetDataExtension";
            lblModuleETLTargetDataExtension.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblModuleETLConnection
            // 
            lblModuleETLConnection.AutoEllipsis = true;
            lblModuleETLConnection.Dock = System.Windows.Forms.DockStyle.Top;
            lblModuleETLConnection.Font = new System.Drawing.Font("Segoe UI", 9F);
            lblModuleETLConnection.ForeColor = System.Drawing.Color.Black;
            lblModuleETLConnection.Location = new System.Drawing.Point(5, 25);
            lblModuleETLConnection.Margin = new System.Windows.Forms.Padding(0);
            lblModuleETLConnection.Name = "lblModuleETLConnection";
            lblModuleETLConnection.Size = new System.Drawing.Size(934, 20);
            lblModuleETLConnection.TabIndex = 1;
            lblModuleETLConnection.Text = "lblModuleETLConnection";
            lblModuleETLConnection.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // grpModuleEstimate
            // 
            grpModuleEstimate.BackColor = System.Drawing.Color.Transparent;
            grpModuleEstimate.Controls.Add(lblModuleEstimateSDesignExtension);
            grpModuleEstimate.Controls.Add(lblModuleEstimateNfiestaExtension);
            grpModuleEstimate.Controls.Add(lblModuleEstimateConnection);
            grpModuleEstimate.Dock = System.Windows.Forms.DockStyle.Top;
            grpModuleEstimate.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            grpModuleEstimate.ForeColor = System.Drawing.Color.MediumBlue;
            grpModuleEstimate.Location = new System.Drawing.Point(5, 205);
            grpModuleEstimate.Margin = new System.Windows.Forms.Padding(0);
            grpModuleEstimate.Name = "grpModuleEstimate";
            grpModuleEstimate.Padding = new System.Windows.Forms.Padding(5);
            grpModuleEstimate.Size = new System.Drawing.Size(944, 100);
            grpModuleEstimate.TabIndex = 22;
            grpModuleEstimate.TabStop = false;
            grpModuleEstimate.Text = "grpModuleEstimate";
            // 
            // lblModuleEstimateSDesignExtension
            // 
            lblModuleEstimateSDesignExtension.AutoEllipsis = true;
            lblModuleEstimateSDesignExtension.Dock = System.Windows.Forms.DockStyle.Top;
            lblModuleEstimateSDesignExtension.Font = new System.Drawing.Font("Segoe UI", 9F);
            lblModuleEstimateSDesignExtension.ForeColor = System.Drawing.Color.Black;
            lblModuleEstimateSDesignExtension.Location = new System.Drawing.Point(5, 65);
            lblModuleEstimateSDesignExtension.Margin = new System.Windows.Forms.Padding(0);
            lblModuleEstimateSDesignExtension.Name = "lblModuleEstimateSDesignExtension";
            lblModuleEstimateSDesignExtension.Size = new System.Drawing.Size(934, 20);
            lblModuleEstimateSDesignExtension.TabIndex = 3;
            lblModuleEstimateSDesignExtension.Text = "lblModuleEstimateSDesignExtension";
            lblModuleEstimateSDesignExtension.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblModuleEstimateNfiestaExtension
            // 
            lblModuleEstimateNfiestaExtension.AutoEllipsis = true;
            lblModuleEstimateNfiestaExtension.Dock = System.Windows.Forms.DockStyle.Top;
            lblModuleEstimateNfiestaExtension.Font = new System.Drawing.Font("Segoe UI", 9F);
            lblModuleEstimateNfiestaExtension.ForeColor = System.Drawing.Color.Black;
            lblModuleEstimateNfiestaExtension.Location = new System.Drawing.Point(5, 45);
            lblModuleEstimateNfiestaExtension.Margin = new System.Windows.Forms.Padding(0);
            lblModuleEstimateNfiestaExtension.Name = "lblModuleEstimateNfiestaExtension";
            lblModuleEstimateNfiestaExtension.Size = new System.Drawing.Size(934, 20);
            lblModuleEstimateNfiestaExtension.TabIndex = 2;
            lblModuleEstimateNfiestaExtension.Text = "lblModuleEstimateNfiestaExtension";
            lblModuleEstimateNfiestaExtension.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblModuleEstimateConnection
            // 
            lblModuleEstimateConnection.AutoEllipsis = true;
            lblModuleEstimateConnection.Dock = System.Windows.Forms.DockStyle.Top;
            lblModuleEstimateConnection.Font = new System.Drawing.Font("Segoe UI", 9F);
            lblModuleEstimateConnection.ForeColor = System.Drawing.Color.Black;
            lblModuleEstimateConnection.Location = new System.Drawing.Point(5, 25);
            lblModuleEstimateConnection.Margin = new System.Windows.Forms.Padding(0);
            lblModuleEstimateConnection.Name = "lblModuleEstimateConnection";
            lblModuleEstimateConnection.Size = new System.Drawing.Size(934, 20);
            lblModuleEstimateConnection.TabIndex = 1;
            lblModuleEstimateConnection.Text = "lblModuleEstimateConnection";
            lblModuleEstimateConnection.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // grpModuleAuxiliaryData
            // 
            grpModuleAuxiliaryData.BackColor = System.Drawing.Color.Transparent;
            grpModuleAuxiliaryData.Controls.Add(lblModuleAuxiliaryDataConnection);
            grpModuleAuxiliaryData.Dock = System.Windows.Forms.DockStyle.Top;
            grpModuleAuxiliaryData.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            grpModuleAuxiliaryData.ForeColor = System.Drawing.Color.MediumBlue;
            grpModuleAuxiliaryData.Location = new System.Drawing.Point(5, 105);
            grpModuleAuxiliaryData.Margin = new System.Windows.Forms.Padding(0);
            grpModuleAuxiliaryData.Name = "grpModuleAuxiliaryData";
            grpModuleAuxiliaryData.Padding = new System.Windows.Forms.Padding(5);
            grpModuleAuxiliaryData.Size = new System.Drawing.Size(944, 100);
            grpModuleAuxiliaryData.TabIndex = 21;
            grpModuleAuxiliaryData.TabStop = false;
            grpModuleAuxiliaryData.Text = "grpModuleAuxiliaryData";
            // 
            // lblModuleAuxiliaryDataConnection
            // 
            lblModuleAuxiliaryDataConnection.AutoEllipsis = true;
            lblModuleAuxiliaryDataConnection.Dock = System.Windows.Forms.DockStyle.Top;
            lblModuleAuxiliaryDataConnection.Font = new System.Drawing.Font("Segoe UI", 9F);
            lblModuleAuxiliaryDataConnection.ForeColor = System.Drawing.Color.Black;
            lblModuleAuxiliaryDataConnection.Location = new System.Drawing.Point(5, 25);
            lblModuleAuxiliaryDataConnection.Margin = new System.Windows.Forms.Padding(0);
            lblModuleAuxiliaryDataConnection.Name = "lblModuleAuxiliaryDataConnection";
            lblModuleAuxiliaryDataConnection.Size = new System.Drawing.Size(934, 20);
            lblModuleAuxiliaryDataConnection.TabIndex = 1;
            lblModuleAuxiliaryDataConnection.Text = "lblModuleAuxiliaryDataConnection";
            lblModuleAuxiliaryDataConnection.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // grpModuleTargetData
            // 
            grpModuleTargetData.BackColor = System.Drawing.Color.Transparent;
            grpModuleTargetData.Controls.Add(lblModuleTargetDataExtension);
            grpModuleTargetData.Controls.Add(lblModuleTargetDataConnection);
            grpModuleTargetData.Dock = System.Windows.Forms.DockStyle.Top;
            grpModuleTargetData.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            grpModuleTargetData.ForeColor = System.Drawing.Color.MediumBlue;
            grpModuleTargetData.Location = new System.Drawing.Point(5, 5);
            grpModuleTargetData.Margin = new System.Windows.Forms.Padding(0);
            grpModuleTargetData.Name = "grpModuleTargetData";
            grpModuleTargetData.Padding = new System.Windows.Forms.Padding(5);
            grpModuleTargetData.Size = new System.Drawing.Size(944, 100);
            grpModuleTargetData.TabIndex = 20;
            grpModuleTargetData.TabStop = false;
            grpModuleTargetData.Text = "grpModuleTargetData";
            // 
            // lblModuleTargetDataExtension
            // 
            lblModuleTargetDataExtension.AutoEllipsis = true;
            lblModuleTargetDataExtension.Dock = System.Windows.Forms.DockStyle.Top;
            lblModuleTargetDataExtension.Font = new System.Drawing.Font("Segoe UI", 9F);
            lblModuleTargetDataExtension.ForeColor = System.Drawing.Color.Black;
            lblModuleTargetDataExtension.Location = new System.Drawing.Point(5, 45);
            lblModuleTargetDataExtension.Margin = new System.Windows.Forms.Padding(0);
            lblModuleTargetDataExtension.Name = "lblModuleTargetDataExtension";
            lblModuleTargetDataExtension.Size = new System.Drawing.Size(934, 20);
            lblModuleTargetDataExtension.TabIndex = 1;
            lblModuleTargetDataExtension.Text = "lblModuleTargetDataExtension";
            lblModuleTargetDataExtension.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblModuleTargetDataConnection
            // 
            lblModuleTargetDataConnection.AutoEllipsis = true;
            lblModuleTargetDataConnection.Dock = System.Windows.Forms.DockStyle.Top;
            lblModuleTargetDataConnection.Font = new System.Drawing.Font("Segoe UI", 9F);
            lblModuleTargetDataConnection.ForeColor = System.Drawing.Color.Black;
            lblModuleTargetDataConnection.Location = new System.Drawing.Point(5, 25);
            lblModuleTargetDataConnection.Margin = new System.Windows.Forms.Padding(0);
            lblModuleTargetDataConnection.Name = "lblModuleTargetDataConnection";
            lblModuleTargetDataConnection.Size = new System.Drawing.Size(934, 20);
            lblModuleTargetDataConnection.TabIndex = 0;
            lblModuleTargetDataConnection.Text = "lblModuleTargetDataConnection";
            lblModuleTargetDataConnection.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FormInfo
            // 
            AcceptButton = btnOK;
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            BackColor = System.Drawing.Color.WhiteSmoke;
            CancelButton = btnCancel;
            ClientSize = new System.Drawing.Size(960, 540);
            Controls.Add(pnlMain);
            ForeColor = System.Drawing.Color.Black;
            FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "FormInfo";
            ShowIcon = false;
            ShowInTaskbar = false;
            StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            Text = "FormInfo";
            pnlMain.ResumeLayout(false);
            tlpMain.ResumeLayout(false);
            tlpButtons.ResumeLayout(false);
            pnlOK.ResumeLayout(false);
            pnlCancel.ResumeLayout(false);
            pnlInfo.ResumeLayout(false);
            grpModuleETL.ResumeLayout(false);
            grpModuleEstimate.ResumeLayout(false);
            grpModuleAuxiliaryData.ResumeLayout(false);
            grpModuleTargetData.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.TableLayoutPanel tlpButtons;
        private System.Windows.Forms.Panel pnlCancel;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Panel pnlOK;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Panel pnlInfo;
        private System.Windows.Forms.GroupBox grpModuleEstimate;
        private System.Windows.Forms.Label lblModuleEstimateSDesignExtension;
        private System.Windows.Forms.Label lblModuleEstimateNfiestaExtension;
        private System.Windows.Forms.Label lblModuleEstimateConnection;
        private System.Windows.Forms.GroupBox grpModuleAuxiliaryData;
        private System.Windows.Forms.Label lblModuleAuxiliaryDataConnection;
        private System.Windows.Forms.GroupBox grpModuleTargetData;
        private System.Windows.Forms.Label lblModuleTargetDataExtension;
        private System.Windows.Forms.Label lblModuleTargetDataConnection;
        private System.Windows.Forms.GroupBox grpModuleETL;
        private System.Windows.Forms.Label lblModuleETLExportConnection;
        private System.Windows.Forms.Label lblModuleETLTargetDataExtension;
        private System.Windows.Forms.Label lblModuleETLConnection;
        private System.Windows.Forms.Label lblModuleETLSDesignExtension;
        private System.Windows.Forms.Label lblModuleETLNfiestaExtension;
    }
}