﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Hanakova.ModuleEtl;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.ModuleAuxiliaryData;
using ZaJi.ModuleEstimate;
using ZaJi.ModuleTargetData;
using ZaJi.NfiEstaPg;
using ZaJi.PostgreSQL;

namespace NfiEstaApp
{

    /// <summary>
    /// <para lang="cs">Hlavní formulář aplikace NFIESTA</para>
    /// <para lang="en">Main form of the NFIESTA application</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    internal partial class FormMain
        : Form, INfiEstaControl
    {

        #region Constants

        /// <summary>
        /// <para lang="cs">Barva pro zvýraznění aktivního module</para>
        /// <para lang="en">Color to highlight the active module</para>
        /// </summary>
        private static readonly Color ActiveModuleColor = Color.White;

        #endregion Constants


        #region Private Fields

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků</para>
        /// <para lang="en">File with control labels</para>
        /// </summary>
        private LanguageFile languageFile;

        #endregion Private Fields


        #region Controls

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Modul pro pomocná data"</para>
        /// <para lang="en">Control "Module for auxiliary data"</para>
        /// </summary>
        private ControlAuxiliaryData ctrAuxiliaryData;

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Modul pro konfiguraci a výpočet odhadů"</para>
        /// <para lang="en">Control "Module for configuration and calculation estimates"</para>
        /// </summary>
        private ControlEstimate ctrEstimate;

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Modul pro cílová data"</para>
        /// <para lang="en">Control "Module for target data"</para>
        /// </summary>
        private ControlTargetData ctrTargetData;

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Modul pro ETL"</para>
        /// <para lang="en">Control "Module for ETL"</para>
        /// </summary>
        private ControlEtl ctrETL;

        #endregion Controls


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor formuláře</para>
        /// <para lang="en">Form constructor</para>
        /// </summary>
        public FormMain()
        {
            InitializeComponent();
            Initialize();
        }

        #endregion Constructor


        #region Properties

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                return null;
            }
            set { }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables(read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return null;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze</para>
        /// <para lang="en">Language version</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return
                    (LanguageVersion)Properties.Settings.Default.LanguageVersion;
            }
            set
            {
                SetLanguageVersion(version: value);
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyk pro národní verzi</para>
        /// <para lang="en">Language for national environment</para>
        /// </summary>
        public static Language NationalEnvironment
        {
            get
            {
                return
                    (Language)Properties.Settings.Default.NationalEnvironment;
            }
            set
            {
                Properties.Settings.Default.NationalEnvironment = (int)value;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyk pro mezinárodní verzi</para>
        /// <para lang="en">Language for international environment</para>
        /// </summary>
        public static Language InternationalEnvironment
        {
            get
            {
                return
                    (Language)Properties.Settings.Default.InternationalEnvironment;
            }
            set
            {
                Properties.Settings.Default.InternationalEnvironment = (int)value;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků</para>
        /// <para lang="en">File with control labels</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return languageFile ??
                    throw new ArgumentNullException(
                        message: $"Argument {nameof(LanguageFile)} must not be null.",
                        paramName: nameof(LanguageFile));
            }
            set
            {
                languageFile = value ??
                     throw new ArgumentNullException(
                        message: $"Argument {nameof(LanguageFile)} must not be null.",
                        paramName: nameof(LanguageFile));
            }
        }

        /// <summary>
        /// <para lang="cs">Aktivní modul</para>
        /// <para lang="en">Active module</para>
        /// </summary>
        public ModuleType ActiveModule
        {
            get
            {
                if (btnModuleAuxiliaryData.BackColor == ActiveModuleColor)
                {
                    return ModuleType.ModuleAuxiliaryData;
                }

                if (btnModuleEstimate.BackColor == ActiveModuleColor)
                {
                    return ModuleType.ModuleEstimate;
                }

                if (btnModuleTargetData.BackColor == ActiveModuleColor)
                {
                    return ModuleType.ModuleTargetData;
                }

                if (btnModuleETL.BackColor == ActiveModuleColor)
                {
                    return ModuleType.ModuleETL;
                }

                throw new ArgumentException(
                    message: $"Unknown {nameof(ActiveModule)}.",
                    paramName: nameof(ActiveModule));
            }
            set
            {
                switch (value)
                {
                    case ModuleType.ModuleAuxiliaryData:
                        btnModuleAuxiliaryData.BackColor = ActiveModuleColor;
                        btnModuleEstimate.BackColor = pnlHeading.BackColor;
                        btnModuleTargetData.BackColor = pnlHeading.BackColor;
                        btnModuleETL.BackColor = pnlHeading.BackColor;
                        ctrAuxiliaryData.Visible = true;
                        ctrEstimate.Visible = false;
                        ctrTargetData.Visible = false;
                        ctrETL.Visible = false;
                        break;

                    case ModuleType.ModuleEstimate:
                        btnModuleAuxiliaryData.BackColor = pnlHeading.BackColor;
                        btnModuleEstimate.BackColor = ActiveModuleColor;
                        btnModuleTargetData.BackColor = pnlHeading.BackColor;
                        btnModuleETL.BackColor = pnlHeading.BackColor;
                        ctrAuxiliaryData.Visible = false;
                        ctrEstimate.Visible = true;
                        ctrTargetData.Visible = false;
                        ctrETL.Visible = false;
                        break;

                    case ModuleType.ModuleTargetData:
                        btnModuleAuxiliaryData.BackColor = pnlHeading.BackColor;
                        btnModuleEstimate.BackColor = pnlHeading.BackColor;
                        btnModuleTargetData.BackColor = ActiveModuleColor;
                        btnModuleETL.BackColor = pnlHeading.BackColor;
                        ctrAuxiliaryData.Visible = false;
                        ctrEstimate.Visible = false;
                        ctrTargetData.Visible = true;
                        ctrETL.Visible = false;
                        break;

                    case ModuleType.ModuleETL:
                        btnModuleAuxiliaryData.BackColor = pnlHeading.BackColor;
                        btnModuleEstimate.BackColor = pnlHeading.BackColor;
                        btnModuleTargetData.BackColor = pnlHeading.BackColor;
                        btnModuleETL.BackColor = ActiveModuleColor;
                        ctrAuxiliaryData.Visible = false;
                        ctrEstimate.Visible = false;
                        ctrTargetData.Visible = false;
                        ctrETL.Visible = true;
                        break;


                    default:
                        throw new ArgumentException(
                           message: $"Unknown {nameof(ActiveModule)}.",
                           paramName: nameof(ActiveModule));
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Modul pro pomocná data" (read-only)</para>
        /// <para lang="en">Control "Module for auxiliary data" (read-only)</para>
        /// </summary>
        public ControlAuxiliaryData CtrAuxiliaryData
        {
            get
            {
                return ctrAuxiliaryData;
            }
        }

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Modul pro konfiguraci a výpočet odhadů" (read-only)</para>
        /// <para lang="en">Control "Module for configuration and calculation estimates" (read-only)</para>
        /// </summary>
        public ControlEstimate CtrEstimate
        {
            get
            {
                return ctrEstimate;
            }
        }

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Modul pro cílová data" (read-only)</para>
        /// <para lang="en">Control "Module for target data" (read-only)</para>
        /// </summary>
        public ControlTargetData CtrTargetData
        {
            get
            {
                return ctrTargetData;
            }
        }

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Modul pro ETL" (read-only)</para>
        /// <para lang="en">Control "Module for ETL" (read-only)</para>
        /// </summary>
        public ControlEtl CtrETL
        {
            get
            {
                return ctrETL;
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(FormMain),                     "nFIESTA GUI" },
                        { nameof(btnModuleAuxiliaryData),       "POMOCNÁ DATA" },
                        { nameof(btnModuleEstimate),            "ODHADY" },
                        { nameof(btnModuleTargetData),          "TERÉNNÍ DATA" },
                        { nameof(btnModuleETL),                 "MIGRACE DAT" },
                        { nameof(btnExit),                      String.Empty },
                        { nameof(btnInfo),                      String.Empty  },
                        { nameof(btnSettings),                  String.Empty  },
                        { nameof(tipExit),                      "Konec" },
                        { nameof(tipInfo),                      "Informace o připojení k databázi" },
                        { nameof(tipSettings),                  "Nastavení"  }
                    }
                    : languageFile.NationalVersion.MainApplicationDictionary.TryGetValue(
                        key: nameof(FormMain),
                        out Dictionary<string, string> dictNational)
                            ? dictNational
                            : [],

                LanguageVersion.International => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(FormMain),                     "nFIESTA GUI" },
                        { nameof(btnModuleAuxiliaryData),       "GIS DATA" },
                        { nameof(btnModuleEstimate),            "ESTIMATES" },
                        { nameof(btnModuleTargetData),          "FIELD DATA" },
                        { nameof(btnModuleETL),                 "DATA TRANSFER" },
                        { nameof(btnExit),                      String.Empty },
                        { nameof(btnInfo),                      String.Empty  },
                        { nameof(btnSettings),                  String.Empty  },
                        { nameof(tipExit),                      "Exit" },
                        { nameof(tipInfo),                      "Database connection information" },
                        { nameof(tipSettings),                  "Settings"  }
                    }
                    : languageFile.InternationalVersion.MainApplicationDictionary.TryGetValue(
                        key: nameof(FormMain),
                        out Dictionary<string, string> dictInternational)
                            ? dictInternational
                            : [],

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace formuláře</para>
        /// <para lang="en">Initializing the form</para>
        /// </summary>
        private void Initialize()
        {
            languageFile = new LanguageFile(
                nationalEnvironment: NationalEnvironment,
                internationalEnvironment: InternationalEnvironment);

            lblAssemblyVersion.Text =
                System.Reflection.Assembly.GetExecutingAssembly()
                    .GetName().Version.ToString();

            pnlModules.Controls.Clear();

            // Module Auxiliary Data
            ctrAuxiliaryData = new ControlAuxiliaryData()
            {
                ControlOwner = this,
                Dock = DockStyle.Fill,
                Visible = false
            };
            pnlModules.Controls.Add(value: ctrAuxiliaryData);

            // Module Estimate
            ctrEstimate = new ControlEstimate()
            {
                ControlOwner = this,
                Dock = DockStyle.Fill,
                Visible = false
            };
            pnlModules.Controls.Add(value: ctrEstimate);

            // Module Terrain Data
            ctrTargetData = new ControlTargetData()
            {
                ControlOwner = this,
                Dock = DockStyle.Fill,
                Visible = false
            };
            pnlModules.Controls.Add(value: ctrTargetData);


            // Module ETL Field Data
            ctrETL = new ControlEtl()
            {
                ControlOwner = this,
                Dock = DockStyle.Fill,
                Visible = false
            };
            pnlModules.Controls.Add(value: ctrETL);

            ActiveModule = ModuleType.ModuleEstimate;

            LoadSettings();

            LoadLanguageFile();

            // Nastaví stejnou language version také pro všechny moduly
            SetLanguageVersion(version: LanguageVersion);

            InitializeLabels();

            CtrAuxiliaryData.DisplayStatusStrip();
            CtrEstimate.DisplayStatusStrip();
            CtrTargetData.DisplayStatusStrip();
            CtrETL.DisplayStatusStrip();

            btnModuleAuxiliaryData.Click += new EventHandler(
                (sender, e) =>
                {
                    ActiveModule = ModuleType.ModuleAuxiliaryData;
                });

            btnModuleEstimate.Click += new EventHandler(
                (sender, e) =>
                {
                    ActiveModule = ModuleType.ModuleEstimate;
                });

            btnModuleETL.Click += new EventHandler(
                (sender, e) =>
                {
                    ActiveModule = ModuleType.ModuleETL;
                });

            btnModuleTargetData.Click += new EventHandler(
                (sender, e) =>
                {
                    ActiveModule = ModuleType.ModuleTargetData;
                });

            btnInfo.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayInfo();
                });

            btnSettings.Click += new EventHandler(
                (sender, e) =>
                {
                    ChangeSettings();
                });

            btnExit.Click += new EventHandler(
                (sender, e) =>
                {
                    Close();
                    Dispose();
                });

            btnLicense.Click += new EventHandler(
                (sender, e) =>
                {
                    // Zobrazení dialogu Licence Agreement, na kliknutí
                    DisplayLicenseDialog();
                });

            Shown += new EventHandler(
                (sender, e) =>
                {
                    // Zobrazení dialogu Licence Agreement,
                    // pouze při prvním spuštění,
                    // pokud je v Setting, že již byl dříve accepted,
                    // pak se nezobrazuje.
                    if (!Properties.Settings.Default.LicenseAccepted)
                    {
                        DisplayLicenseDialog();
                    }
                });

            FormClosing += new FormClosingEventHandler(
                (sender, e) =>
                {
                    SaveSettings();
                    CtrAuxiliaryData?.Disconnect();
                    CtrEstimate?.Disconnect();
                    CtrTargetData?.Disconnect();
                    CtrETL?.Disconnect();
                });

            KeyUp += new KeyEventHandler(
                (sender, e) =>
                {
                    PressKey(
                        keyCtrlPressed: e.Control,
                        keyCode: e.KeyCode);
                });
        }

        /// <summary>
        /// <para lang="cs">Načtení nastavení aplikace z konfiguračního souboru</para>
        /// <para lang="en">Loading application settings from a configuration file</para>
        /// </summary>
        private void LoadSettings()
        {
            Properties.Settings.Default.Reload();

            if (String.IsNullOrEmpty(value: Properties.Settings.Default.ModuleAuxiliaryDataSetting.Trim()))
            {
                CtrAuxiliaryData.Setting =
                    new ZaJi.ModuleAuxiliaryData.Setting();
            }
            else
            {
                CtrAuxiliaryData.Setting =
                    ZaJi.ModuleAuxiliaryData.Setting.Deserialize(
                        json: Properties.Settings.Default.ModuleAuxiliaryDataSetting.Trim());
            }

            if (String.IsNullOrEmpty(value: Properties.Settings.Default.ModuleEstimateSetting.Trim()))
            {
                CtrEstimate.Setting =
                    new ZaJi.ModuleEstimate.Setting();
            }
            else
            {
                CtrEstimate.Setting =
                    ZaJi.ModuleEstimate.Setting.Deserialize(
                        json: Properties.Settings.Default.ModuleEstimateSetting.Trim());
            }

            if (String.IsNullOrEmpty(value: Properties.Settings.Default.ModuleTargetDataSetting.Trim()))
            {
                CtrTargetData.Setting =
                    new ZaJi.ModuleTargetData.Setting();
            }
            else
            {
                CtrTargetData.Setting =
                    ZaJi.ModuleTargetData.Setting.Deserialize(
                        json: Properties.Settings.Default.ModuleTargetDataSetting.Trim());
            }

            if (String.IsNullOrEmpty(value: Properties.Settings.Default.ModuleETLSetting.Trim()))
            {
                CtrETL.Setting =
                    new Hanakova.ModuleEtl.Setting();
            }
            else
            {
                CtrETL.Setting =
                    Hanakova.ModuleEtl.Setting.Deserialize(
                        json: Properties.Settings.Default.ModuleETLSetting.Trim());
            }
        }

        /// <summary>
        /// <para lang="cs">Uložení nastavení aplikace do konfiguračního souboru</para>
        /// <para lang="en">Saving application settings to a configuration file</para>
        /// </summary>
        private void SaveSettings()
        {
            Properties.Settings.Default.ModuleAuxiliaryDataSetting =
               CtrAuxiliaryData.Setting.JSON;

            Properties.Settings.Default.ModuleEstimateSetting =
               CtrEstimate.Setting.JSON;

            Properties.Settings.Default.ModuleTargetDataSetting =
               ctrTargetData.Setting.JSON;

            Properties.Settings.Default.ModuleETLSetting =
                CtrETL.Setting.JSON;

            Properties.Settings.Default.Save();
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent() { }

        /// <summary>
        /// <para lang="cs">Nastaví jazykovou verzi aplikace</para>
        /// <para lang="en">Sets language version of the application</para>
        /// </summary>
        /// <param name="version">
        /// <para lang="cs">Jazyková verze</para>
        /// <para lang="en">Language version</para>
        /// </param>
        private void SetLanguageVersion(LanguageVersion version)
        {
            Properties.Settings.Default.LanguageVersion = (int)version;

            if (CtrAuxiliaryData != null)
            {
                CtrAuxiliaryData.LanguageFile = LanguageFile.AuxiliaryDataModuleLanguageFile;
                CtrAuxiliaryData.LanguageVersion = LanguageVersion;
                CtrAuxiliaryData.InitializeLabels();
            }

            if (CtrEstimate != null)
            {
                CtrEstimate.LanguageFile = LanguageFile.EstimateDataModuleLanguageFile;
                CtrEstimate.LanguageVersion = LanguageVersion;
                CtrEstimate.InitializeLabels();
            }

            if (CtrTargetData != null)
            {
                CtrTargetData.LanguageFile = LanguageFile.TargetDataModuleLanguageFile;
                CtrTargetData.LanguageVersion = LanguageVersion;
                CtrTargetData.InitializeLabels();
            }

            if (CtrETL != null)
            {
                CtrETL.LanguageFile = LanguageFile.ETLModuleLanguageFile;
                CtrETL.LanguageVersion = LanguageVersion;
                CtrETL.InitializeLabels();
            }

            InitializeLabels();
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků formuláře</para>
        /// <para lang="en">Initializing form labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            Text =
                labels.TryGetValue(key: nameof(FormMain),
                    out string formMainText)
                        ? formMainText
                        : String.Empty;

            btnModuleAuxiliaryData.Text =
                labels.TryGetValue(key: nameof(btnModuleAuxiliaryData),
                    out string btnModuleAuxiliaryDataText)
                        ? btnModuleAuxiliaryDataText
                        : String.Empty;

            btnModuleEstimate.Text =
                labels.TryGetValue(key: nameof(btnModuleEstimate),
                    out string btnModuleEstimateText)
                        ? btnModuleEstimateText
                        : String.Empty;

            btnModuleETL.Text =
                labels.TryGetValue(key: nameof(btnModuleETL),
                    out string btnModuleETLText)
                        ? btnModuleETLText
                        : String.Empty;

            btnModuleTargetData.Text =
                labels.TryGetValue(key: nameof(btnModuleTargetData),
                    out string btnModuleTargetDataText)
                        ? btnModuleTargetDataText
                        : String.Empty;

            btnExit.Text =
                labels.TryGetValue(key: nameof(btnExit),
                    out string btnExitText)
                        ? btnExitText
                        : String.Empty;

            tipExit.SetToolTip(
                control: btnExit,
                caption: labels.TryGetValue(key: nameof(tipExit),
                            out string tipExitText)
                                ? tipExitText
                                : String.Empty);

            btnInfo.Text =
                labels.TryGetValue(key: nameof(btnInfo),
                    out string btnInfoText)
                        ? btnInfoText
                        : String.Empty;

            tipInfo.SetToolTip(
                control: btnInfo,
                caption: labels.TryGetValue(key: nameof(tipInfo),
                    out string tipInfoText)
                        ? tipInfoText
                        : String.Empty);

            btnSettings.Text =
                labels.TryGetValue(key: nameof(btnSettings),
                    out string btnSettingsText)
                        ? btnSettingsText
                        : String.Empty;

            tipSettings.SetToolTip(
                control: btnSettings,
                caption: labels.TryGetValue(key: nameof(tipSettings),
                    out string tipSettingsText)
                        ? tipSettingsText
                        : String.Empty);
        }

        /// <summary>
        /// <para lang="cs">Načtení souboru s popisky ovládacích prvků</para>
        /// <para lang="en">Loading file with control labels</para>
        /// </summary>
        private void LoadLanguageFile()
        {
            string dirName = @"lang";

            string fileType = @"json";

            string pathNat = String.Concat(
                @"", dirName, @"/", LanguageList.ISO_639_1(language: NationalEnvironment), @".", fileType);

            string pathInt = String.Concat(
                @"", dirName, @"/", LanguageList.ISO_639_1(language: InternationalEnvironment), @".", fileType);

            if (!Directory.Exists(path: @"lang"))
            {
                Directory.CreateDirectory(path: @"lang");
            }

            if (File.Exists(path: pathNat))
            {
                LanguageFile.Version loadedVersionCs =
                    LanguageFile.Version.LoadFromFile(
                        languageVersion: LanguageVersion.National,
                        path: pathNat);

                LanguageFile.NationalVersion = loadedVersionCs;
            }
            else
            {
                LanguageFile.NationalVersion.SaveToFile(path: pathNat);
            }

            if (File.Exists(path: pathInt))
            {
                LanguageFile.Version loadedVersionEn =
                    LanguageFile.Version.LoadFromFile(
                        languageVersion: LanguageVersion.International,
                        path: pathInt);

                LanguageFile.InternationalVersion = loadedVersionEn;
            }
            else
            {
                LanguageFile.InternationalVersion.SaveToFile(path: pathInt);
            }


            if (CtrAuxiliaryData != null)
            {
                CtrAuxiliaryData.LanguageFile = LanguageFile.AuxiliaryDataModuleLanguageFile;
            }

            if (CtrEstimate != null)
            {
                CtrEstimate.LanguageFile = LanguageFile.EstimateDataModuleLanguageFile;
            }

            if (CtrTargetData != null)
            {
                CtrTargetData.LanguageFile = LanguageFile.TargetDataModuleLanguageFile;
            }

            if (CtrETL != null)
            {
                CtrETL.LanguageFile = LanguageFile.ETLModuleLanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Zobrazí formulář informace o připojení a databázových extenzích</para>
        /// <para lang="en">Displays a form with information about connections and database extensions</para>
        /// </summary>
        private void DisplayInfo()
        {
            FormInfo frmInfo =
                new(
                    controlOwner: this);

            if (frmInfo.ShowDialog() == DialogResult.OK) { }
        }

        /// <summary>
        /// <para lang="cs">Zobrazí formulář pro změnu nastavení a provede změny v nastavení aplikace</para>
        /// <para lang="en">Displays the settings change form and makes changes to the application settings</para>
        /// </summary>
        private void ChangeSettings()
        {
            FormSettings frmSettings =
                new(controlOwner: this)
                {
                    // Module General
                    SelectedLanguageVersion =
                        LanguageVersion,

                    // Module Auxiliary Data
                    SelectedAuxVarTotalThreadCount =
                        CtrAuxiliaryData.Setting.AuxiliaryDataThreadCount,
                    SelectedAuxiliaryDataRefreshTime =
                        CtrAuxiliaryData.Setting.RefreshTime,

                    // Module Estimate
                    SelectedConfigurationThreadCount =
                        CtrEstimate.Setting.ConfigurationRequiredThreadsNumber,
                    SelectedEstimateThreadCount =
                        CtrEstimate.Setting.EstimateRequiredThreadsNumber,
                    SelectedAdditivityLimit =
                        CtrEstimate.Setting.AdditivityLimit,
                    SelectedCellCoverageTolerance =
                        CtrEstimate.Setting.CellCoverageTolerance,

                    // Module Target Data
                    SelectedLocalDensityThreadCount =
                        CtrTargetData.Setting.LocalDensityRequiredThreadsNumber,
                    SelectedLocalDensityThreshold =
                        CtrTargetData.Setting.LocalDensityThreshold
                };

            if (frmSettings.ShowDialog() == DialogResult.OK)
            {
                // Module General
                if (frmSettings.SelectedLanguageVersion != LanguageVersion)
                {
                    LanguageVersion =
                        frmSettings.SelectedLanguageVersion;
                }

                // Module Auxiliary Data
                if (frmSettings.SelectedAuxVarTotalThreadCount != CtrAuxiliaryData.Setting.AuxiliaryDataThreadCount)
                {
                    CtrAuxiliaryData.Setting.AuxiliaryDataThreadCount =
                        frmSettings.SelectedAuxVarTotalThreadCount;
                }
                if (frmSettings.SelectedAuxiliaryDataRefreshTime != CtrAuxiliaryData.Setting.RefreshTime)
                {
                    CtrAuxiliaryData.Setting.RefreshTime =
                        frmSettings.SelectedAuxiliaryDataRefreshTime;
                }

                // Module Estimate
                if (frmSettings.SelectedConfigurationThreadCount != CtrEstimate.Setting.ConfigurationRequiredThreadsNumber)
                {
                    CtrEstimate.Setting.ConfigurationRequiredThreadsNumber =
                        frmSettings.SelectedConfigurationThreadCount;
                }
                if (frmSettings.SelectedEstimateThreadCount != CtrEstimate.Setting.EstimateRequiredThreadsNumber)
                {
                    CtrEstimate.Setting.EstimateRequiredThreadsNumber =
                        frmSettings.SelectedEstimateThreadCount;
                }
                if (frmSettings.SelectedAdditivityLimit != CtrEstimate.Setting.AdditivityLimit)
                {
                    CtrEstimate.Setting.AdditivityLimit =
                        frmSettings.SelectedAdditivityLimit;
                }
                if (frmSettings.SelectedCellCoverageTolerance != CtrEstimate.Setting.CellCoverageTolerance)
                {
                    CtrEstimate.Setting.CellCoverageTolerance =
                        frmSettings.SelectedCellCoverageTolerance;
                }

                // Module Target Data
                if (frmSettings.SelectedLocalDensityThreadCount != CtrTargetData.Setting.LocalDensityRequiredThreadsNumber)
                {
                    CtrTargetData.Setting.LocalDensityRequiredThreadsNumber =
                        frmSettings.SelectedLocalDensityThreadCount;
                }
                if (frmSettings.SelectedLocalDensityThreshold != CtrTargetData.Setting.LocalDensityThreshold)
                {
                    CtrTargetData.Setting.LocalDensityThreshold =
                    frmSettings.SelectedLocalDensityThreshold;
                }

                SaveSettings();
            }
        }

        /// <summary>
        /// <para lang="cs">Stisknutí klávesy</para>
        /// <para lang="en">Pressing a key</para>
        /// </summary>
        /// <param name="keyCtrlPressed">
        /// <para lang="cs">Bylo stisknuto Ctrl</para>
        /// <para lang="en">Key Ctrl was pressed</para>
        /// </param>
        /// <param name="keyCode">
        /// <para lang="cs">Kód klávesy</para>
        /// <para lang="en">Key code</para>
        /// </param>
        private void PressKey(bool keyCtrlPressed, Keys keyCode)
        {
            if (keyCtrlPressed)
            {
                switch (keyCode)
                {
                    // Ctrl + L - Změna jazyka
                    case Keys.L:
                        LanguageVersion =
                            (LanguageVersion == LanguageVersion.International) ?
                                LanguageVersion.National :
                                LanguageVersion.International;
                        break;

                    // Ctrl + R - Reset nastavení modulů
                    case Keys.R:
                        LanguageVersion = LanguageVersion.International;
                        switch (ActiveModule)
                        {
                            case ModuleType.ModuleAuxiliaryData:
                                CtrAuxiliaryData.Reset(displayMessage: true);
                                break;
                            case ModuleType.ModuleEstimate:
                                CtrEstimate.Reset(displayMessage: true);
                                break;
                            case ModuleType.ModuleTargetData:
                                CtrTargetData.Reset(displayMessage: true);
                                break;
                            case ModuleType.ModuleETL:
                                CtrETL.Reset(displayMessage: true);
                                break;
                            default:
                                break;
                        }
                        SaveSettings();
                        LoadSettings();
                        break;

                    // Zobrazení SQL příkazů
                    // Display SQL commands
                    case Keys.Q:
                        CtrAuxiliaryData.SwitchDisplaySQLCommands();
                        CtrEstimate.SwitchDisplaySQLCommands();
                        CtrTargetData.SwitchDisplaySQLCommands();
                        break;

                    // Ctrl + T - Zobrazit nebo skrýt stavový řádek
                    // Show or hide status strip
                    case Keys.T:
                        switch (ActiveModule)
                        {
                            case ModuleType.ModuleAuxiliaryData:
                                CtrAuxiliaryData.Setting.StatusStripVisible =
                                    !CtrAuxiliaryData.Setting.StatusStripVisible;
                                CtrAuxiliaryData.DisplayStatusStrip();
                                break;
                            case ModuleType.ModuleEstimate:
                                CtrEstimate.Setting.StatusStripVisible =
                                    !CtrEstimate.Setting.StatusStripVisible;
                                CtrEstimate.DisplayStatusStrip();
                                break;
                            case ModuleType.ModuleTargetData:
                                CtrTargetData.Setting.StatusStripVisible =
                                    !CtrTargetData.Setting.StatusStripVisible;
                                CtrTargetData.DisplayStatusStrip();
                                break;
                            case ModuleType.ModuleETL:
                                CtrETL.Setting.StatusStripVisible =
                                    !CtrTargetData.Setting.StatusStripVisible;
                                CtrETL.DisplayStatusStrip();
                                break;
                            default:
                                break;
                        }
                        break;

                    default:
                        break;
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Zobrazí formulář s EUPL licencí</para>
        /// <para lang="en">License Agreement Dialog</para>
        /// </summary>
        private void DisplayLicenseDialog()
        {
            FormLicense frmLicense =
                new(controlOwner: this)
                {
                    LicenseAccepted = Properties.Settings.Default.LicenseAccepted
                };

            if (frmLicense.ShowDialog() == DialogResult.OK)
            {
                Properties.Settings.Default.LicenseAccepted
                    = frmLicense.LicenseAccepted;
                Properties.Settings.Default.Save();
            }

            if (!Properties.Settings.Default.LicenseAccepted)
            {
                Close();
            }
        }

        #endregion Methods

    }

    /// <summary>
    /// <para lang="cs">Seznam modulů</para>
    /// <para lang="en">List of modules</para>
    /// </summary>
    internal enum ModuleType
    {
        /// <summary>
        /// <para lang="cs">Modul pro cílová data</para>
        /// <para lang="en">Target data module</para>
        /// </summary>
        ModuleTargetData = 1,

        /// <summary>
        /// <para lang="cs">Modul pro pomocná data</para>
        /// <para lang="en">Auxiliary data module</para>
        /// </summary>
        ModuleAuxiliaryData = 2,

        /// <summary>
        /// <para lang="cs">Modul pro konfiguraci a výpočet odhadů</para>
        /// <para lang="en">Module for configuration and calculation estimates</para>
        /// </summary>
        ModuleEstimate = 3,

        /// <summary>
        /// <para lang="cs">Modul pro import lokálních hustot</para>
        /// <para lang="en">Modul for import local densities</para>
        /// </summary>
        ModuleETL = 4
    }

}
