﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Hanakova.ModuleEtl;
using System;
using System.Collections.Generic;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.ModuleAuxiliaryData;
using ZaJi.ModuleEstimate;
using ZaJi.ModuleTargetData;
using ZaJi.NfiEstaPg;
using ZaJi.PostgreSQL;

namespace NfiEstaApp
{

    /// <summary>
    /// <para lang="cs">Formulář pro zobrazení stavu připojení k databázi a verzích extenzí</para>
    /// <para lang="en">Form for display database connection state and extensions version</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    public partial class FormInfo
        : Form, INfiEstaControl
    {

        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        private Control controlOwner;

        #endregion Private Fields


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor formuláře</para>
        /// <para lang="en">Form constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        public FormInfo(Control controlOwner)
        {
            InitializeComponent();
            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Properties

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                if (controlOwner == null)
                {
                    throw new ArgumentNullException(
                        message: $"Argument {nameof(ControlOwner)} must not be null.",
                        paramName: nameof(ControlOwner));
                }

                if (controlOwner is not FormMain)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(FormMain)}.",
                        paramName: nameof(ControlOwner));
                }

                return controlOwner;
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException(
                        message: $"Argument {nameof(ControlOwner)} must not be null.",
                        paramName: nameof(ControlOwner));
                }

                if (value is not FormMain)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(FormMain)}.",
                        paramName: nameof(ControlOwner));
                }

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables(read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((FormMain)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyk popisků (read-only)</para>
        /// <para lang="en">Language of labels (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((FormMain)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((FormMain)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Modul pro pomocná data" (read-only)</para>
        /// <para lang="en">Control "Module for auxiliary data" (read-only)</para>
        /// </summary>
        public ControlAuxiliaryData CtrAuxiliaryData
        {
            get
            {
                return ((FormMain)ControlOwner).CtrAuxiliaryData;
            }
        }

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Modul pro konfiguraci a výpočet odhadů" (read-only)</para>
        /// <para lang="en">Control "Module for configuration and calculation estimates" (read-only)</para>
        /// </summary>
        public ControlEstimate CtrEstimate
        {
            get
            {
                return ((FormMain)ControlOwner).CtrEstimate;
            }
        }

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Modul pro cílová data" (read-only)</para>
        /// <para lang="en">Control "Module for target data" (read-only)</para>
        /// </summary>
        public ControlTargetData CtrTargetData
        {
            get
            {
                return ((FormMain)ControlOwner).CtrTargetData;
            }
        }

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Modul pro ETL" (read-only)</para>
        /// <para lang="en">Control "Module for ETL" (read-only)</para>
        /// </summary>
        public ControlEtl CtrETL
        {
            get
            {
                return ((FormMain)ControlOwner).CtrETL;
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(FormInfo),                 "Stav připojení k databázi" },
                        { nameof(btnCancel),                "Zavřít" },
                        { nameof(btnOK),                    "OK" },
                        { nameof(grpModuleTargetData),      "TERÉNNÍ DATA" },
                        { nameof(grpModuleAuxiliaryData),   "POMOCNÁ DATA" },
                        { nameof(grpModuleEstimate),        "ODHADY" },
                        { nameof(grpModuleETL),             "MIGRACE DAT" }
                    }
                    : languageFile.NationalVersion.MainApplicationDictionary.TryGetValue(
                        key: nameof(FormInfo),
                        out Dictionary<string, string> dictNational)
                            ? dictNational
                            : [],

                LanguageVersion.International => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(FormInfo),                 "Database connection state" },
                        { nameof(btnCancel),                "Close" },
                        { nameof(btnOK),                    "OK" },
                        { nameof(grpModuleTargetData),      "FIELD DATA" },
                        { nameof(grpModuleAuxiliaryData),   "GIS DATA" },
                        { nameof(grpModuleEstimate),        "ESTIMATES" },
                        { nameof(grpModuleETL),             "DATA TRANSFER" }
                    }
                    : languageFile.InternationalVersion.MainApplicationDictionary.TryGetValue(
                        key: nameof(FormInfo),
                        out Dictionary<string, string> dictInternational)
                            ? dictInternational
                            : [],

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace formuláře</para>
        /// <para lang="en">Initializing the form</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        private void Initialize(Control controlOwner)
        {
            ControlOwner = controlOwner;
            InitializeLabels();

            btnOK.Click += new EventHandler(
                (sender, e) =>
                {
                    DialogResult = DialogResult.OK;
                    Close();
                });

            btnCancel.Click += new EventHandler(
                (sender, e) =>
                {
                    DialogResult = DialogResult.Cancel;
                    Close();
                });
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků formuláře</para>
        /// <para lang="en">Initializing form labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            Text =
                labels.TryGetValue(key: nameof(FormInfo),
                    out string formInfoText)
                        ? formInfoText
                        : String.Empty;

            btnCancel.Text =
                labels.TryGetValue(key: nameof(btnCancel),
                    out string btnCancelText)
                        ? btnCancelText
                        : String.Empty;

            btnOK.Text =
               labels.TryGetValue(key: nameof(btnOK),
                    out string btnOKText)
                        ? btnOKText
                        : String.Empty;

            grpModuleAuxiliaryData.Text =
               labels.TryGetValue(key: nameof(grpModuleAuxiliaryData),
                    out string grpModuleAuxiliaryDataText)
                        ? grpModuleAuxiliaryDataText
                        : String.Empty;

            grpModuleEstimate.Text =
               labels.TryGetValue(key: nameof(grpModuleEstimate),
                    out string grpModuleEstimateText)
                        ? grpModuleEstimateText
                        : String.Empty;

            grpModuleTargetData.Text =
                labels.TryGetValue(key: nameof(grpModuleTargetData),
                    out string grpModuleTargetDataText)
                        ? grpModuleTargetDataText
                        : String.Empty;

            grpModuleETL.Text =
              labels.TryGetValue(key: nameof(grpModuleETL),
                    out string grpModuleETLText)
                        ? grpModuleETLText
                        : String.Empty;

            // Module Auxiliary Data
            lblModuleAuxiliaryDataConnection.Text = CtrAuxiliaryData.lblStatus.Text;
            lblModuleAuxiliaryDataConnection.ForeColor = CtrAuxiliaryData.lblStatus.ForeColor;

            // Module Estimate
            lblModuleEstimateConnection.Text = CtrEstimate.lblStatus.Text;
            lblModuleEstimateConnection.ForeColor = CtrEstimate.lblStatus.ForeColor;

            lblModuleEstimateNfiestaExtension.Text = CtrEstimate.lblNfiestaExtension.Text;
            lblModuleEstimateNfiestaExtension.ForeColor = CtrEstimate.lblNfiestaExtension.ForeColor;

            lblModuleEstimateSDesignExtension.Text = CtrEstimate.lblSDesignExtension.Text;
            lblModuleEstimateSDesignExtension.ForeColor = CtrEstimate.lblSDesignExtension.ForeColor;

            // Module Target Data
            lblModuleTargetDataConnection.Text = CtrTargetData.lblStatus.Text;
            lblModuleTargetDataConnection.ForeColor = CtrTargetData.lblStatus.ForeColor;

            lblModuleTargetDataExtension.Text = CtrTargetData.lblTargetDataExtension.Text;
            lblModuleTargetDataExtension.ForeColor = CtrTargetData.lblTargetDataExtension.ForeColor;

            // Module ETL
            lblModuleETLConnection.Text = CtrETL.lblStatus.Text;
            lblModuleETLConnection.ForeColor = CtrETL.lblStatus.ForeColor;

            lblModuleETLTargetDataExtension.Text = CtrETL.lblTargetDataExtension.Text;
            lblModuleETLTargetDataExtension.ForeColor = CtrETL.lblTargetDataExtension.ForeColor;

            lblModuleETLExportConnection.Text = CtrETL.lblStatusExportConnection.Text;
            lblModuleETLExportConnection.ForeColor = CtrETL.lblStatusExportConnection.ForeColor;

            lblModuleETLNfiestaExtension.Text = CtrETL.lblNfiestaExtension.Text;
            lblModuleETLNfiestaExtension.Visible = CtrETL.lblNfiestaExtension.Visible;
            lblModuleETLNfiestaExtension.ForeColor = CtrETL.lblNfiestaExtension.ForeColor;

            lblModuleETLSDesignExtension.Text = CtrETL.lblSDesignExtension.Text;
            lblModuleETLSDesignExtension.Visible = CtrETL.lblSDesignExtension.Visible;
            lblModuleETLSDesignExtension.ForeColor = CtrETL.lblSDesignExtension.ForeColor;
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent() { }

        #endregion Methods

    }

}