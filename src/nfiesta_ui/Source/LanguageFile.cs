﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Versioning;
using System.Text;
using System.Text.Encodings.Web;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Text.Unicode;
using System.Windows.Forms;
using ZaJi.PostgreSQL;

namespace NfiEstaApp
{

    /// <summary>
    /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
    /// <para lang="en">Files with control labels for national and international version</para>
    /// </summary>
    /// <remarks>
    /// <para lang="cs">Konstruktor objektu</para>
    /// <para lang="en">Object constructor</para>
    /// </remarks>
    /// <param name="nationalEnvironment">
    /// <para lang="cs">Jazyk pro národní verzi</para>
    /// <para lang="en">Langauge for national environment</para>
    /// </param>
    /// <param name="internationalEnvironment">
    /// <para lang="cs">Jazyk pro mezinárodní verzi</para>
    /// <para lang="en">Langauge for international environment</para>
    /// </param>
    [SupportedOSPlatform("windows")]
    public class LanguageFile(
        Language nationalEnvironment,
        Language internationalEnvironment)
    {

        /// <summary>
        /// <para lang="cs">Data jazykové verze</para>
        /// <para lang="en">Language version data</para>
        /// </summary>
        public class Version
        {

            #region Private Fields

            /// <summary>
            /// <para lang="cs">Jazyková verze</para>
            /// <para lang="en">Language version</para>
            /// </summary>
            private LanguageVersion languageVersion;

            /// <summary>
            /// <para lang="cs">Jazyk </para>
            /// <para lang="en">Language</para>
            /// </summary>
            private Language language;


            /// <summary>
            /// <para lang="cs">Slovník s popisky ovládacích prvků pro aplikaci</para>
            /// <para lang="en">Dictionary with control labels for application</para>
            /// </summary>
            private Dictionary<string, Dictionary<string, string>> mainApplicationDictionary;

            /// <summary>
            /// <para lang="cs">Slovník s popisky ovládacích prvků pro "Modul pro pomocná data"</para>
            /// <para lang="en">Dictionary with control labels for "Module for auxiliary data"</para>
            /// </summary>
            private Dictionary<string, Dictionary<string, string>> auxiliaryDataModuleDictionary;

            /// <summary>
            /// <para lang="cs">Slovník s popisky ovládacích prvků pro "Modul pro konfiguraci a výpočet odhadů"</para>
            /// <para lang="en">Dictionary with control labels for "Module for configuration and calculation estimates"</para>
            /// </summary>
            private Dictionary<string, Dictionary<string, string>> estimateDataModuleDictionary;

            /// <summary>
            /// <para lang="cs">Slovník s popisky ovládacích prvků pro "Modul pro ETL"</para>
            /// <para lang="en">Dictionary with control labels for "Module for ETL"</para>
            /// </summary>
            private Dictionary<string, Dictionary<string, string>> etlModuleDictionary;

            /// <summary>
            /// <para lang="cs">Slovník s popisky ovládacích prvků pro "Modul pro cílová data"</para>
            /// <para lang="en">Dictionary with control labels "Module for target data"</para>
            /// </summary>
            private Dictionary<string, Dictionary<string, string>> targetDataModuleDictionary;


            /// <summary>
            /// <para lang="cs">Volby pro JSON serializer</para>
            /// <para lang="en">JSON serializer options</para>
            /// </summary>
            private JsonSerializerOptions serializerOptions;

            #endregion Private Fields


            #region Constructor

            /// <summary>
            /// <para lang="cs">Konstruktor objektu (default: angličtina)</para>
            /// <para lang="en">Object constructor (default: English)</para>
            /// </summary>
            public Version()
            {
                SerializerOptions = null;
                LanguageVersion = LanguageVersion.International;
                Language = Language.EN;
                MainApplicationDictionary = null;
                AuxiliaryDataModuleDictionary = null;
                EstimateDataModuleDictionary = null;
                ETLModuleDictionary = null;
                TargetDataModuleDictionary = null;
            }

            /// <summary>
            /// <para lang="cs">Konstruktor objektu</para>
            /// <para lang="en">Object constructor</para>
            /// </summary>
            /// <param name="languageVersion">
            /// <para lang="cs">Jazyková verze</para>
            /// <para lang="en">Language version</para>
            /// </param>
            /// <param name="language">
            /// <para lang="cs">Jazyk </para>
            /// <para lang="en">Language</para>
            /// </param>
            /// <param name="mainApplicationDictionary">
            /// <para lang="cs">Slovník s popisky ovládacích prvků pro aplikaci</para>
            /// <para lang="en">Dictionary with control labels for application</para>
            /// </param>
            /// <param name="auxiliaryDataModuleDictionary">
            /// <para lang="cs">Slovník s popisky ovládacích prvků pro "Modul pro pomocná data"</para>
            /// <para lang="en">Dictionary with control labels for "Module for auxiliary data"</para>
            /// </param>
            /// <param name="estimateDataModuleDictionary">
            /// <para lang="cs">Slovník s popisky ovládacích prvků pro "Modul pro konfiguraci a výpočet odhadů"</para>
            /// <para lang="en">Dictionary with control labels for "Module for configuration and calculation estimates"</para>
            /// </param>
            /// <param name="etlModuleDictionary">
            /// <para lang="cs">Slovník s popisky ovládacích prvků pro "Modul pro ETL"</para>
            /// <para lang="en">Dictionary with control labels for "Module for ETL"</para>
            /// </param>
            /// <param name="targetDataModuleDictionary">
            /// <para lang="cs">Slovník s popisky ovládacích prvků pro "Modul pro cílová data"</para>
            /// <para lang="en">Dictionary with control labels "Module for target data"</para>
            /// </param>
            public Version(
                LanguageVersion languageVersion,
                Language language,
                Dictionary<string, Dictionary<string, string>> mainApplicationDictionary,
                Dictionary<string, Dictionary<string, string>> auxiliaryDataModuleDictionary,
                Dictionary<string, Dictionary<string, string>> estimateDataModuleDictionary,
                Dictionary<string, Dictionary<string, string>> etlModuleDictionary,
                Dictionary<string, Dictionary<string, string>> targetDataModuleDictionary)
            {
                SerializerOptions = null;
                LanguageVersion = languageVersion;
                Language = language;
                MainApplicationDictionary = mainApplicationDictionary;
                AuxiliaryDataModuleDictionary = auxiliaryDataModuleDictionary;
                EstimateDataModuleDictionary = estimateDataModuleDictionary;
                ETLModuleDictionary = etlModuleDictionary;
                TargetDataModuleDictionary = targetDataModuleDictionary;
            }

            #endregion Constructor


            #region Properties

            /// <summary>
            /// <para lang="cs">Jazyková verze</para>
            /// <para lang="en">Language version</para>
            /// </summary>
            [JsonIgnore]
            public LanguageVersion LanguageVersion
            {
                get
                {
                    return languageVersion;
                }
                set
                {
                    languageVersion = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Jazyk </para>
            /// <para lang="en">Language</para>
            /// </summary>
            public Language Language
            {
                get
                {
                    return language;
                }
                set
                {
                    language = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Slovník s popisky ovládacích prvků pro aplikaci</para>
            /// <para lang="en">Dictionary with control labels for application</para>
            /// </summary>
            public Dictionary<string, Dictionary<string, string>> MainApplicationDictionary
            {
                get
                {
                    return mainApplicationDictionary ??
                        Dictionary(
                        languageVersion: LanguageVersion,
                        languageFile: null);
                }
                set
                {
                    mainApplicationDictionary = value ??
                        Dictionary(
                        languageVersion: LanguageVersion,
                        languageFile: null);
                }
            }

            /// <summary>
            /// <para lang="cs">Slovník s popisky ovládacích prvků pro "Modul pro pomocná data"</para>
            /// <para lang="en">Dictionary with control labels for "Module for auxiliary data"</para>
            /// </summary>
            public Dictionary<string, Dictionary<string, string>> AuxiliaryDataModuleDictionary
            {
                get
                {
                    return auxiliaryDataModuleDictionary ??
                        ZaJi.ModuleAuxiliaryData.LanguageFile.Dictionary(
                        languageVersion: LanguageVersion,
                        languageFile: null);
                }
                set
                {
                    auxiliaryDataModuleDictionary = value ??
                        ZaJi.ModuleAuxiliaryData.LanguageFile.Dictionary(
                        languageVersion: LanguageVersion,
                        languageFile: null);
                }
            }

            /// <summary>
            /// <para lang="cs">Slovník s popisky ovládacích prvků pro "Modul pro konfiguraci a výpočet odhadů"</para>
            /// <para lang="en">Dictionary with control labels for "Module for configuration and calculation estimates"</para>
            /// </summary>
            public Dictionary<string, Dictionary<string, string>> EstimateDataModuleDictionary
            {
                get
                {
                    return estimateDataModuleDictionary ??
                        ZaJi.ModuleEstimate.LanguageFile.Dictionary(
                        languageVersion: LanguageVersion,
                        languageFile: null);
                }
                set
                {
                    estimateDataModuleDictionary = value ??
                        ZaJi.ModuleEstimate.LanguageFile.Dictionary(
                        languageVersion: LanguageVersion,
                        languageFile: null);
                }
            }

            /// <summary>
            /// <para lang="cs">Slovník s popisky ovládacích prvků pro "Modul pro ETL"</para>
            /// <para lang="en">Dictionary with control labels for "Module for ETL"</para>
            /// </summary>
            public Dictionary<string, Dictionary<string, string>> ETLModuleDictionary
            {
                get
                {
                    return etlModuleDictionary ??
                        Hanakova.ModuleEtl.LanguageFile.Dictionary(
                        languageVersion: LanguageVersion,
                        languageFile: null);
                }
                set
                {
                    etlModuleDictionary = value ??
                        Hanakova.ModuleEtl.LanguageFile.Dictionary(
                        languageVersion: LanguageVersion,
                        languageFile: null);
                }
            }


            /// <summary>
            /// <para lang="cs">Slovník s popisky ovládacích prvků pro "Modul pro cílová data"</para>
            /// <para lang="en">Dictionary with control labels "Module for target data"</para>
            /// </summary>
            public Dictionary<string, Dictionary<string, string>> TargetDataModuleDictionary
            {
                get
                {
                    return targetDataModuleDictionary ??
                        ZaJi.ModuleTargetData.LanguageFile.Dictionary(
                        languageVersion: LanguageVersion,
                        languageFile: null);
                }
                set
                {
                    targetDataModuleDictionary = value ??
                        ZaJi.ModuleTargetData.LanguageFile.Dictionary(
                        languageVersion: LanguageVersion,
                        languageFile: null);
                }
            }

            /// <summary>
            /// <para lang="cs">Volby pro JSON serializer</para>
            /// <para lang="en">JSON serializer options</para>
            /// </summary>
            [JsonIgnore]
            private JsonSerializerOptions SerializerOptions
            {
                get
                {
                    return serializerOptions ??
                        new()
                        {
                            Encoder = JavaScriptEncoder.Create(allowedRanges: UnicodeRanges.All),
                            WriteIndented = true
                        };
                }
                set
                {
                    serializerOptions = value ??
                        new()
                        {
                            Encoder = JavaScriptEncoder.Create(allowedRanges: UnicodeRanges.All),
                            WriteIndented = true
                        };
                }
            }

            /// <summary>
            /// <para lang="cs">Serializace objektu do formátu JSON</para>
            /// <para lang="en">Object serialization to JSON format</para>
            /// </summary>
            [JsonIgnore]
            public string JSON
            {
                get
                {
                    return JsonSerializer.Serialize(
                        value: this,
                        options: SerializerOptions);
                }
            }

            #endregion Properties


            #region Static Methods

            /// <summary>
            /// <para lang="cs">Obnovení objektu z formátu JSON</para>
            /// <para lang="en">Restoring an object from JSON format</para>
            /// </summary>
            /// <param name="languageVersion">
            /// <para lang="cs">Jazyková verze</para>
            /// <para lang="en">Language version</para>
            /// </param>
            /// <param name="json">
            /// <para lang="cs">JSON text</para>
            /// <para lang="en">JSON text</para>
            /// </param>
            /// <returns>
            /// <para lang="cs">Data jazykové verze</para>
            /// <para lang="en">Language version data</para>
            /// </returns>
            public static Version Deserialize(
                LanguageVersion languageVersion,
                string json)
            {
                try
                {
                    Version version =
                        JsonSerializer.Deserialize<Version>(json: json);
                    version.LanguageVersion = languageVersion;
                    return version;
                }
                catch (Exception exception)
                {
                    MessageBox.Show(
                        text: exception.Message,
                        caption: String.Empty,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Exclamation);

                    return new Version();
                }
            }

            /// <summary>
            /// <para lang="cs">Načte objekt ze souboru json</para>
            /// <para lang="en">Loads object from json file</para>
            /// </summary>
            /// <param name="languageVersion">
            /// <para lang="cs">Jazyková verze</para>
            /// <para lang="en">Language version</para>
            /// </param>
            /// <param name="path">
            /// <para lang="cs">Cesta k json souboru</para>
            /// <para lang="en">Path to json file</para>
            /// </param>
            /// <returns>
            /// <para lang="cs">Data jazykové verze</para>
            /// <para lang="en">Language version data</para>
            /// </returns>
            public static Version LoadFromFile(
                LanguageVersion languageVersion,
                string path)
            {
                try
                {
                    Version version =
                        Deserialize(
                            languageVersion: languageVersion,
                            json: File.ReadAllText(
                                path: path,
                                encoding: Encoding.UTF8));
                    return version;
                }
                catch (Exception exception)
                {
                    MessageBox.Show(
                        text: exception.Message,
                        caption: String.Empty,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Information);

                    Version version = new();
                    return version;
                }
            }

            #endregion Static Methods


            #region Methods

            /// <summary>
            /// <para lang="cs">Uloží objekt do souboru json</para>
            /// <para lang="en">Save object into json file</para>
            /// </summary>
            /// <param name="path">
            /// <para lang="cs">Cesta k json souboru</para>
            /// <para lang="en">Path to json file</para>
            /// </param>
            public void SaveToFile(string path)
            {
                try
                {
                    File.WriteAllText(
                        path: path,
                        contents: JSON,
                        encoding: Encoding.UTF8);
                }
                catch (Exception exception)
                {
                    MessageBox.Show(
                        text: exception.Message,
                        caption: String.Empty,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Information);
                }
            }

            #endregion Methods

        }

        #region Private Fields

        /// <summary>
        /// <para lang="cs">Národní verze</para>
        /// <para lang="en">National version</para>
        /// </summary>
        private Version nationalVersion = new(
                languageVersion: LanguageVersion.National,
                language: nationalEnvironment,
                mainApplicationDictionary: Dictionary(
                        languageVersion: LanguageVersion.National,
                        languageFile: null),
                auxiliaryDataModuleDictionary: ZaJi.ModuleAuxiliaryData.LanguageFile
                    .Dictionary(
                        languageVersion: LanguageVersion.National,
                        languageFile: null),
                estimateDataModuleDictionary: ZaJi.ModuleEstimate.LanguageFile
                    .Dictionary(
                        languageVersion: LanguageVersion.National,
                        languageFile: null),
                etlModuleDictionary: Hanakova.ModuleEtl.LanguageFile
                    .Dictionary(
                        languageVersion: LanguageVersion.National,
                        languageFile: null),
                targetDataModuleDictionary: ZaJi.ModuleTargetData.LanguageFile
                    .Dictionary(
                        languageVersion: LanguageVersion.National,
                        languageFile: null));

        /// <summary>
        /// <para lang="cs">Mezinárodní verze</para>
        /// <para lang="en">International version</para>
        /// </summary>
        private Version internationalVersion = new(
                languageVersion: LanguageVersion.International,
                language: internationalEnvironment,
                mainApplicationDictionary: Dictionary(
                        languageVersion: LanguageVersion.International,
                        languageFile: null),
                auxiliaryDataModuleDictionary: ZaJi.ModuleAuxiliaryData.LanguageFile
                    .Dictionary(
                        languageVersion: LanguageVersion.International,
                        languageFile: null),
                estimateDataModuleDictionary: ZaJi.ModuleEstimate.LanguageFile
                    .Dictionary(
                        languageVersion: LanguageVersion.International,
                        languageFile: null),
                etlModuleDictionary: Hanakova.ModuleEtl.LanguageFile
                    .Dictionary(
                        languageVersion: LanguageVersion.International,
                        languageFile: null),
                targetDataModuleDictionary: ZaJi.ModuleTargetData.LanguageFile
                    .Dictionary(
                        languageVersion: LanguageVersion.International,
                        languageFile: null));

        #endregion Private Fields


        #region Properties

        /// <summary>
        /// <para lang="cs">Národní verze</para>
        /// <para lang="en">National version</para>
        /// </summary>
        public Version NationalVersion
        {
            get
            {
                return nationalVersion ??
                    new LanguageFile.Version(
                        languageVersion: LanguageVersion.National,
                        language: Language.CS,
                        mainApplicationDictionary: Dictionary(
                                languageVersion: LanguageVersion.National,
                                languageFile: null),
                        auxiliaryDataModuleDictionary: ZaJi.ModuleAuxiliaryData.LanguageFile
                            .Dictionary(
                                languageVersion: LanguageVersion.National,
                                languageFile: null),
                        estimateDataModuleDictionary: ZaJi.ModuleEstimate.LanguageFile
                            .Dictionary(
                                languageVersion: LanguageVersion.National,
                                languageFile: null),
                        etlModuleDictionary: Hanakova.ModuleEtl.LanguageFile
                            .Dictionary(
                                languageVersion: LanguageVersion.National,
                                languageFile: null),
                        targetDataModuleDictionary: ZaJi.ModuleTargetData.LanguageFile
                            .Dictionary(
                                languageVersion: LanguageVersion.National,
                                languageFile: null));
            }
            set
            {
                nationalVersion = value ??
                    new LanguageFile.Version(
                        languageVersion: LanguageVersion.National,
                        language: Language.CS,
                        mainApplicationDictionary: Dictionary(
                                languageVersion: LanguageVersion.National,
                                languageFile: null),
                        auxiliaryDataModuleDictionary: ZaJi.ModuleAuxiliaryData.LanguageFile
                            .Dictionary(
                                languageVersion: LanguageVersion.National,
                                languageFile: null),
                        estimateDataModuleDictionary: ZaJi.ModuleEstimate.LanguageFile
                            .Dictionary(
                                languageVersion: LanguageVersion.National,
                                languageFile: null),
                        etlModuleDictionary: Hanakova.ModuleEtl.LanguageFile
                            .Dictionary(
                                languageVersion: LanguageVersion.National,
                                languageFile: null),
                        targetDataModuleDictionary: ZaJi.ModuleTargetData.LanguageFile
                            .Dictionary(
                                languageVersion: LanguageVersion.National,
                                languageFile: null));
            }
        }

        /// <summary>
        /// <para lang="cs">Mezinárodní verze</para>
        /// <para lang="en">International version</para>
        /// </summary>
        public Version InternationalVersion
        {
            get
            {
                return internationalVersion ??
                    new LanguageFile.Version(
                        languageVersion: LanguageVersion.International,
                        language: Language.EN,
                        mainApplicationDictionary: Dictionary(
                                languageVersion: LanguageVersion.International,
                                languageFile: null),
                        auxiliaryDataModuleDictionary: ZaJi.ModuleAuxiliaryData.LanguageFile
                            .Dictionary(
                                languageVersion: LanguageVersion.International,
                                languageFile: null),
                        estimateDataModuleDictionary: ZaJi.ModuleEstimate.LanguageFile
                            .Dictionary(
                                languageVersion: LanguageVersion.International,
                                languageFile: null),
                        etlModuleDictionary: Hanakova.ModuleEtl.LanguageFile
                            .Dictionary(
                                languageVersion: LanguageVersion.International,
                                languageFile: null),
                        targetDataModuleDictionary: ZaJi.ModuleTargetData.LanguageFile
                            .Dictionary(
                                languageVersion: LanguageVersion.International,
                                languageFile: null));
            }
            set
            {
                internationalVersion = value ??
                    new LanguageFile.Version(
                        languageVersion: LanguageVersion.International,
                        language: Language.EN,
                        mainApplicationDictionary: Dictionary(
                                languageVersion: LanguageVersion.International,
                                languageFile: null),
                        auxiliaryDataModuleDictionary: ZaJi.ModuleAuxiliaryData.LanguageFile
                            .Dictionary(
                                languageVersion: LanguageVersion.International,
                                languageFile: null),
                        estimateDataModuleDictionary: ZaJi.ModuleEstimate.LanguageFile
                            .Dictionary(
                                languageVersion: LanguageVersion.International,
                                languageFile: null),
                        etlModuleDictionary: Hanakova.ModuleEtl.LanguageFile
                            .Dictionary(
                                languageVersion: LanguageVersion.International,
                                languageFile: null),
                        targetDataModuleDictionary: ZaJi.ModuleTargetData.LanguageFile
                            .Dictionary(
                                languageVersion: LanguageVersion.International,
                                languageFile: null));
            }
        }

        /// <summary>
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro "Modul pro pomocná data"</para>
        /// <para lang="en">Files with control labels for "Module for auxiliary data"</para>
        /// </summary>
        public ZaJi.ModuleAuxiliaryData.LanguageFile AuxiliaryDataModuleLanguageFile
        {
            get
            {
                return
                    new ZaJi.ModuleAuxiliaryData.LanguageFile()
                    {
                        NationalVersion = new ZaJi.ModuleAuxiliaryData.LanguageFile.Version(
                            languageVersion: NationalVersion.LanguageVersion,
                            language: NationalVersion.Language,
                            data: NationalVersion.AuxiliaryDataModuleDictionary),
                        InternationalVersion = new ZaJi.ModuleAuxiliaryData.LanguageFile.Version(
                            languageVersion: InternationalVersion.LanguageVersion,
                            language: InternationalVersion.Language,
                            data: InternationalVersion.AuxiliaryDataModuleDictionary),
                    };
            }
        }

        /// <summary>
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro "Modul pro cílová data"</para>
        /// <para lang="en">Files with control labels "Module for target data"</para>
        /// </summary>
        public ZaJi.ModuleEstimate.LanguageFile EstimateDataModuleLanguageFile
        {
            get
            {
                return
                    new ZaJi.ModuleEstimate.LanguageFile()
                    {
                        NationalVersion = new ZaJi.ModuleEstimate.LanguageFile.Version(
                            languageVersion: NationalVersion.LanguageVersion,
                            language: NationalVersion.Language,
                            data: NationalVersion.EstimateDataModuleDictionary),
                        InternationalVersion = new ZaJi.ModuleEstimate.LanguageFile.Version(
                            languageVersion: InternationalVersion.LanguageVersion,
                            language: InternationalVersion.Language,
                            data: InternationalVersion.EstimateDataModuleDictionary),
                    };
            }
        }

        /// <summary>
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro "Modul pro ETL"</para>
        /// <para lang="en">Files with control labels "Module for ETL"</para>
        /// </summary>
        public Hanakova.ModuleEtl.LanguageFile ETLModuleLanguageFile
        {
            get
            {
                return
                    new Hanakova.ModuleEtl.LanguageFile()
                    {
                        NationalVersion = new Hanakova.ModuleEtl.LanguageFile.Version(
                            languageVersion: NationalVersion.LanguageVersion,
                            language: NationalVersion.Language,
                            data: NationalVersion.ETLModuleDictionary),
                        InternationalVersion = new Hanakova.ModuleEtl.LanguageFile.Version(
                            languageVersion: InternationalVersion.LanguageVersion,
                            language: InternationalVersion.Language,
                            data: InternationalVersion.ETLModuleDictionary),
                    };
            }
        }

        /// <summary>
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro "Modul pro cílová data"</para>
        /// <para lang="en">Files with control labels "Module for target data"</para>
        /// </summary>
        public ZaJi.ModuleTargetData.LanguageFile TargetDataModuleLanguageFile
        {
            get
            {
                return
                    new ZaJi.ModuleTargetData.LanguageFile()
                    {
                        NationalVersion = new ZaJi.ModuleTargetData.LanguageFile.Version(
                            languageVersion: NationalVersion.LanguageVersion,
                            language: NationalVersion.Language,
                            data: NationalVersion.TargetDataModuleDictionary),
                        InternationalVersion = new ZaJi.ModuleTargetData.LanguageFile.Version(
                            languageVersion: InternationalVersion.LanguageVersion,
                            language: InternationalVersion.Language,
                            data: InternationalVersion.TargetDataModuleDictionary),
                    };
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, Dictionary<string, string>>
            Dictionary(
                LanguageVersion languageVersion,
                LanguageFile languageFile)
        {
            return
                new Dictionary<string, Dictionary<string, string>>()
                {
                    { nameof(FormMain),
                            FormMain.Dictionary(
                            languageVersion: languageVersion,
                            languageFile: languageFile) },
                    { nameof(FormLicense),
                            FormLicense.Dictionary(
                            languageVersion: languageVersion,
                            languageFile: languageFile) },
                    { nameof(FormInfo),
                            FormInfo.Dictionary(
                            languageVersion: languageVersion,
                            languageFile: languageFile) },
                    { nameof(FormSettings),
                            FormSettings.Dictionary(
                            languageVersion: languageVersion,
                            languageFile: languageFile) },
                };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Vrací text popisující objekt</para>
        /// <para lang="en">Returns a string that represents the current object</para>
        /// </summary>
        /// <returns>
        /// <para lang="cs">Vrací text popisující objekt</para>
        /// <para lang="en">Returns a string that represents the current object</para>
        /// </returns>
        public override string ToString()
        {
            return String.Concat(
                $"National version language : ",
                $"{LanguageList.Name(language: NationalVersion.Language)} {Environment.NewLine}",
                $"International version language : ",
                $"{LanguageList.Name(language: InternationalVersion.Language)} {Environment.NewLine}");
        }

        #endregion Methods

    }
}