﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace NfiEstaApp
{

    partial class FormLicense
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            pnlMain = new System.Windows.Forms.Panel();
            tlpMain = new System.Windows.Forms.TableLayoutPanel();
            tlpButtons = new System.Windows.Forms.TableLayoutPanel();
            pnlOK = new System.Windows.Forms.Panel();
            btnOK = new System.Windows.Forms.Button();
            pnlInfo = new System.Windows.Forms.Panel();
            lblLicense = new System.Windows.Forms.Label();
            pnlRadioButtons = new System.Windows.Forms.Panel();
            rdoDecline = new System.Windows.Forms.RadioButton();
            rdoAccept = new System.Windows.Forms.RadioButton();
            pnlCaption = new System.Windows.Forms.Panel();
            lblCaption = new System.Windows.Forms.Label();
            pnlLink = new System.Windows.Forms.Panel();
            linkEupl = new System.Windows.Forms.LinkLabel();
            pnlMain.SuspendLayout();
            tlpMain.SuspendLayout();
            tlpButtons.SuspendLayout();
            pnlOK.SuspendLayout();
            pnlInfo.SuspendLayout();
            pnlRadioButtons.SuspendLayout();
            pnlCaption.SuspendLayout();
            pnlLink.SuspendLayout();
            SuspendLayout();
            // 
            // pnlMain
            // 
            pnlMain.BackColor = System.Drawing.Color.DarkSeaGreen;
            pnlMain.Controls.Add(tlpMain);
            pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlMain.Location = new System.Drawing.Point(0, 0);
            pnlMain.Margin = new System.Windows.Forms.Padding(0);
            pnlMain.Name = "pnlMain";
            pnlMain.Padding = new System.Windows.Forms.Padding(3);
            pnlMain.Size = new System.Drawing.Size(960, 540);
            pnlMain.TabIndex = 4;
            // 
            // tlpMain
            // 
            tlpMain.BackColor = System.Drawing.Color.WhiteSmoke;
            tlpMain.ColumnCount = 1;
            tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.Controls.Add(tlpButtons, 0, 5);
            tlpMain.Controls.Add(pnlInfo, 0, 2);
            tlpMain.Controls.Add(pnlRadioButtons, 0, 4);
            tlpMain.Controls.Add(pnlCaption, 0, 0);
            tlpMain.Controls.Add(pnlLink, 0, 1);
            tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpMain.Location = new System.Drawing.Point(3, 3);
            tlpMain.Margin = new System.Windows.Forms.Padding(0);
            tlpMain.Name = "tlpMain";
            tlpMain.RowCount = 6;
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            tlpMain.Size = new System.Drawing.Size(954, 534);
            tlpMain.TabIndex = 3;
            // 
            // tlpButtons
            // 
            tlpButtons.BackColor = System.Drawing.Color.Transparent;
            tlpButtons.ColumnCount = 3;
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            tlpButtons.Controls.Add(pnlOK, 2, 0);
            tlpButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpButtons.Location = new System.Drawing.Point(0, 494);
            tlpButtons.Margin = new System.Windows.Forms.Padding(0);
            tlpButtons.Name = "tlpButtons";
            tlpButtons.RowCount = 1;
            tlpButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.Size = new System.Drawing.Size(954, 40);
            tlpButtons.TabIndex = 4;
            // 
            // pnlOK
            // 
            pnlOK.Controls.Add(btnOK);
            pnlOK.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlOK.Location = new System.Drawing.Point(794, 0);
            pnlOK.Margin = new System.Windows.Forms.Padding(0);
            pnlOK.Name = "pnlOK";
            pnlOK.Padding = new System.Windows.Forms.Padding(5);
            pnlOK.Size = new System.Drawing.Size(160, 40);
            pnlOK.TabIndex = 13;
            // 
            // btnOK
            // 
            btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            btnOK.Dock = System.Windows.Forms.DockStyle.Fill;
            btnOK.Font = new System.Drawing.Font("Segoe UI", 9F);
            btnOK.Location = new System.Drawing.Point(5, 5);
            btnOK.Margin = new System.Windows.Forms.Padding(0);
            btnOK.Name = "btnOK";
            btnOK.Size = new System.Drawing.Size(150, 30);
            btnOK.TabIndex = 12;
            btnOK.Text = "btnOK";
            btnOK.UseVisualStyleBackColor = true;
            // 
            // pnlInfo
            // 
            pnlInfo.AutoScroll = true;
            pnlInfo.BackColor = System.Drawing.Color.White;
            pnlInfo.Controls.Add(lblLicense);
            pnlInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlInfo.Font = new System.Drawing.Font("Segoe UI", 9F);
            pnlInfo.Location = new System.Drawing.Point(0, 40);
            pnlInfo.Margin = new System.Windows.Forms.Padding(0);
            pnlInfo.Name = "pnlInfo";
            pnlInfo.Padding = new System.Windows.Forms.Padding(5);
            pnlInfo.Size = new System.Drawing.Size(954, 389);
            pnlInfo.TabIndex = 2;
            // 
            // lblLicense
            // 
            lblLicense.AutoEllipsis = true;
            lblLicense.AutoSize = true;
            lblLicense.Dock = System.Windows.Forms.DockStyle.Top;
            lblLicense.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            lblLicense.Location = new System.Drawing.Point(5, 5);
            lblLicense.Name = "lblLicense";
            lblLicense.Size = new System.Drawing.Size(0, 15);
            lblLicense.TabIndex = 0;
            // 
            // pnlRadioButtons
            // 
            pnlRadioButtons.BackColor = System.Drawing.Color.Transparent;
            pnlRadioButtons.Controls.Add(rdoDecline);
            pnlRadioButtons.Controls.Add(rdoAccept);
            pnlRadioButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlRadioButtons.Location = new System.Drawing.Point(0, 434);
            pnlRadioButtons.Margin = new System.Windows.Forms.Padding(0);
            pnlRadioButtons.Name = "pnlRadioButtons";
            pnlRadioButtons.Size = new System.Drawing.Size(954, 60);
            pnlRadioButtons.TabIndex = 5;
            // 
            // rdoDecline
            // 
            rdoDecline.AutoEllipsis = true;
            rdoDecline.BackColor = System.Drawing.Color.Transparent;
            rdoDecline.Dock = System.Windows.Forms.DockStyle.Top;
            rdoDecline.Font = new System.Drawing.Font("Segoe UI", 9F);
            rdoDecline.Location = new System.Drawing.Point(0, 29);
            rdoDecline.Margin = new System.Windows.Forms.Padding(0);
            rdoDecline.Name = "rdoDecline";
            rdoDecline.Padding = new System.Windows.Forms.Padding(5);
            rdoDecline.Size = new System.Drawing.Size(954, 29);
            rdoDecline.TabIndex = 1;
            rdoDecline.Text = "I do not accept the terms in the licence agreement";
            rdoDecline.UseVisualStyleBackColor = false;
            // 
            // rdoAccept
            // 
            rdoAccept.AutoEllipsis = true;
            rdoAccept.BackColor = System.Drawing.Color.Transparent;
            rdoAccept.Checked = true;
            rdoAccept.Dock = System.Windows.Forms.DockStyle.Top;
            rdoAccept.Font = new System.Drawing.Font("Segoe UI", 9F);
            rdoAccept.Location = new System.Drawing.Point(0, 0);
            rdoAccept.Margin = new System.Windows.Forms.Padding(0);
            rdoAccept.Name = "rdoAccept";
            rdoAccept.Padding = new System.Windows.Forms.Padding(5);
            rdoAccept.Size = new System.Drawing.Size(954, 29);
            rdoAccept.TabIndex = 0;
            rdoAccept.TabStop = true;
            rdoAccept.Tag = "";
            rdoAccept.Text = "I accept the terms in the license agreement";
            rdoAccept.UseVisualStyleBackColor = false;
            // 
            // pnlCaption
            // 
            pnlCaption.BackColor = System.Drawing.Color.Transparent;
            pnlCaption.Controls.Add(lblCaption);
            pnlCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlCaption.Location = new System.Drawing.Point(0, 0);
            pnlCaption.Margin = new System.Windows.Forms.Padding(0);
            pnlCaption.Name = "pnlCaption";
            pnlCaption.Size = new System.Drawing.Size(954, 20);
            pnlCaption.TabIndex = 8;
            // 
            // lblCaption
            // 
            lblCaption.AutoEllipsis = true;
            lblCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblCaption.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, 0);
            lblCaption.ForeColor = System.Drawing.Color.Black;
            lblCaption.Location = new System.Drawing.Point(0, 0);
            lblCaption.Margin = new System.Windows.Forms.Padding(0);
            lblCaption.Name = "lblCaption";
            lblCaption.Size = new System.Drawing.Size(954, 20);
            lblCaption.TabIndex = 0;
            lblCaption.Text = "License Agreement";
            lblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnlLink
            // 
            pnlLink.BackColor = System.Drawing.Color.Transparent;
            pnlLink.Controls.Add(linkEupl);
            pnlLink.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlLink.Location = new System.Drawing.Point(0, 20);
            pnlLink.Margin = new System.Windows.Forms.Padding(0);
            pnlLink.Name = "pnlLink";
            pnlLink.Size = new System.Drawing.Size(954, 20);
            pnlLink.TabIndex = 7;
            // 
            // linkEupl
            // 
            linkEupl.AutoEllipsis = true;
            linkEupl.BackColor = System.Drawing.Color.Transparent;
            linkEupl.Dock = System.Windows.Forms.DockStyle.Fill;
            linkEupl.Location = new System.Drawing.Point(0, 0);
            linkEupl.Name = "linkEupl";
            linkEupl.Size = new System.Drawing.Size(954, 20);
            linkEupl.TabIndex = 7;
            linkEupl.TabStop = true;
            linkEupl.Text = "https://joinup.ec.europa.eu/software/page/eupl";
            linkEupl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FormLicense
            // 
            AcceptButton = btnOK;
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            BackColor = System.Drawing.Color.WhiteSmoke;
            ClientSize = new System.Drawing.Size(960, 540);
            Controls.Add(pnlMain);
            Font = new System.Drawing.Font("Segoe UI", 9F);
            ForeColor = System.Drawing.Color.Black;
            FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "FormLicense";
            ShowIcon = false;
            ShowInTaskbar = false;
            StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            Text = "License Agreement";
            pnlMain.ResumeLayout(false);
            tlpMain.ResumeLayout(false);
            tlpButtons.ResumeLayout(false);
            pnlOK.ResumeLayout(false);
            pnlInfo.ResumeLayout(false);
            pnlInfo.PerformLayout();
            pnlRadioButtons.ResumeLayout(false);
            pnlCaption.ResumeLayout(false);
            pnlLink.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.TableLayoutPanel tlpButtons;
        private System.Windows.Forms.Panel pnlOK;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Panel pnlInfo;
        private System.Windows.Forms.Label lblLicense;
        private System.Windows.Forms.Panel pnlRadioButtons;
        private System.Windows.Forms.RadioButton rdoDecline;
        private System.Windows.Forms.RadioButton rdoAccept;
        private System.Windows.Forms.Panel pnlLink;
        private System.Windows.Forms.LinkLabel linkEupl;
        private System.Windows.Forms.Panel pnlCaption;
        private System.Windows.Forms.Label lblCaption;
    }

}