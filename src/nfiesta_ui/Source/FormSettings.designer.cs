﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace NfiEstaApp
{

    partial class FormSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            pnlMain = new System.Windows.Forms.Panel();
            tlpMain = new System.Windows.Forms.TableLayoutPanel();
            tabMain = new System.Windows.Forms.TabControl();
            tpModuleGeneral = new System.Windows.Forms.TabPage();
            tlpModuleGeneral = new System.Windows.Forms.TableLayoutPanel();
            grpLanguage = new System.Windows.Forms.GroupBox();
            tlpLanguage = new System.Windows.Forms.TableLayoutPanel();
            pnlInternationalLabel = new System.Windows.Forms.Panel();
            rdoInternational = new System.Windows.Forms.RadioButton();
            pnlNationalLabel = new System.Windows.Forms.Panel();
            rdoNational = new System.Windows.Forms.RadioButton();
            pnlInternationalValue = new System.Windows.Forms.Panel();
            pnlNationalValue = new System.Windows.Forms.Panel();
            tpModuleTargetData = new System.Windows.Forms.TabPage();
            tlpModuleTargetData = new System.Windows.Forms.TableLayoutPanel();
            grpLocalDensityThreadCount = new System.Windows.Forms.GroupBox();
            pnlLocalDensityThreadCount = new System.Windows.Forms.Panel();
            txtLocalDensityThreadCount = new System.Windows.Forms.TextBox();
            grpLocalDensityThreshold = new System.Windows.Forms.GroupBox();
            pnlLocalDensityThreshold = new System.Windows.Forms.Panel();
            txtLocalDensityThreshold = new System.Windows.Forms.TextBox();
            tpModuleAuxiliaryData = new System.Windows.Forms.TabPage();
            tlpModuleAuxiliaryData = new System.Windows.Forms.TableLayoutPanel();
            grpAuxVarTotalThreadCount = new System.Windows.Forms.GroupBox();
            pnlAuxVarTotalThreadCount = new System.Windows.Forms.Panel();
            txtAuxVarTotalThreadCount = new System.Windows.Forms.TextBox();
            grpAuxVarTotalRefreshTime = new System.Windows.Forms.GroupBox();
            pnlAuxVarTotalRefreshTime = new System.Windows.Forms.Panel();
            txtAuxVarTotalRefreshTime = new System.Windows.Forms.TextBox();
            tpModuleEstimate = new System.Windows.Forms.TabPage();
            tlpModuleEstimate = new System.Windows.Forms.TableLayoutPanel();
            grpCellCoverageTolerance = new System.Windows.Forms.GroupBox();
            pnlCellCoverageTolerance = new System.Windows.Forms.Panel();
            txtCellCoverageTolerance = new System.Windows.Forms.TextBox();
            grpAdditivityLimit = new System.Windows.Forms.GroupBox();
            pnlAdditivityLimit = new System.Windows.Forms.Panel();
            txtAdditivityLimit = new System.Windows.Forms.TextBox();
            grpEstimateThreadCount = new System.Windows.Forms.GroupBox();
            pnlEstimateThreadCount = new System.Windows.Forms.Panel();
            txtEstimateThreadCount = new System.Windows.Forms.TextBox();
            grpConfigurationThreadCount = new System.Windows.Forms.GroupBox();
            pnlConfigurationThreadCount = new System.Windows.Forms.Panel();
            txtConfigurationThreadCount = new System.Windows.Forms.TextBox();
            pnlButtons = new System.Windows.Forms.Panel();
            tlpButtons = new System.Windows.Forms.TableLayoutPanel();
            pnlOK = new System.Windows.Forms.Panel();
            btnOK = new System.Windows.Forms.Button();
            pnlCancel = new System.Windows.Forms.Panel();
            btnCancel = new System.Windows.Forms.Button();
            pnlMain.SuspendLayout();
            tlpMain.SuspendLayout();
            tabMain.SuspendLayout();
            tpModuleGeneral.SuspendLayout();
            tlpModuleGeneral.SuspendLayout();
            grpLanguage.SuspendLayout();
            tlpLanguage.SuspendLayout();
            pnlInternationalLabel.SuspendLayout();
            pnlNationalLabel.SuspendLayout();
            tpModuleTargetData.SuspendLayout();
            tlpModuleTargetData.SuspendLayout();
            grpLocalDensityThreadCount.SuspendLayout();
            pnlLocalDensityThreadCount.SuspendLayout();
            grpLocalDensityThreshold.SuspendLayout();
            pnlLocalDensityThreshold.SuspendLayout();
            tpModuleAuxiliaryData.SuspendLayout();
            tlpModuleAuxiliaryData.SuspendLayout();
            grpAuxVarTotalThreadCount.SuspendLayout();
            pnlAuxVarTotalThreadCount.SuspendLayout();
            grpAuxVarTotalRefreshTime.SuspendLayout();
            pnlAuxVarTotalRefreshTime.SuspendLayout();
            tpModuleEstimate.SuspendLayout();
            tlpModuleEstimate.SuspendLayout();
            grpCellCoverageTolerance.SuspendLayout();
            pnlCellCoverageTolerance.SuspendLayout();
            grpAdditivityLimit.SuspendLayout();
            pnlAdditivityLimit.SuspendLayout();
            grpEstimateThreadCount.SuspendLayout();
            pnlEstimateThreadCount.SuspendLayout();
            grpConfigurationThreadCount.SuspendLayout();
            pnlConfigurationThreadCount.SuspendLayout();
            pnlButtons.SuspendLayout();
            tlpButtons.SuspendLayout();
            pnlOK.SuspendLayout();
            pnlCancel.SuspendLayout();
            SuspendLayout();
            // 
            // pnlMain
            // 
            pnlMain.BackColor = System.Drawing.Color.DarkSeaGreen;
            pnlMain.Controls.Add(tlpMain);
            pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlMain.Location = new System.Drawing.Point(0, 0);
            pnlMain.Margin = new System.Windows.Forms.Padding(0);
            pnlMain.Name = "pnlMain";
            pnlMain.Padding = new System.Windows.Forms.Padding(3);
            pnlMain.Size = new System.Drawing.Size(960, 540);
            pnlMain.TabIndex = 2;
            // 
            // tlpMain
            // 
            tlpMain.BackColor = System.Drawing.Color.Transparent;
            tlpMain.ColumnCount = 1;
            tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.Controls.Add(tabMain, 0, 0);
            tlpMain.Controls.Add(pnlButtons, 0, 1);
            tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpMain.Location = new System.Drawing.Point(3, 3);
            tlpMain.Margin = new System.Windows.Forms.Padding(0);
            tlpMain.Name = "tlpMain";
            tlpMain.RowCount = 2;
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            tlpMain.Size = new System.Drawing.Size(954, 534);
            tlpMain.TabIndex = 2;
            // 
            // tabMain
            // 
            tabMain.Controls.Add(tpModuleGeneral);
            tabMain.Controls.Add(tpModuleTargetData);
            tabMain.Controls.Add(tpModuleAuxiliaryData);
            tabMain.Controls.Add(tpModuleEstimate);
            tabMain.Dock = System.Windows.Forms.DockStyle.Fill;
            tabMain.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            tabMain.Location = new System.Drawing.Point(0, 0);
            tabMain.Margin = new System.Windows.Forms.Padding(0);
            tabMain.Name = "tabMain";
            tabMain.Padding = new System.Drawing.Point(0, 0);
            tabMain.SelectedIndex = 0;
            tabMain.Size = new System.Drawing.Size(954, 494);
            tabMain.TabIndex = 2;
            // 
            // tpModuleGeneral
            // 
            tpModuleGeneral.Controls.Add(tlpModuleGeneral);
            tpModuleGeneral.Location = new System.Drawing.Point(4, 24);
            tpModuleGeneral.Margin = new System.Windows.Forms.Padding(0);
            tpModuleGeneral.Name = "tpModuleGeneral";
            tpModuleGeneral.Size = new System.Drawing.Size(946, 466);
            tpModuleGeneral.TabIndex = 3;
            tpModuleGeneral.Text = "tpModuleGeneral";
            tpModuleGeneral.UseVisualStyleBackColor = true;
            // 
            // tlpModuleGeneral
            // 
            tlpModuleGeneral.BackColor = System.Drawing.Color.Transparent;
            tlpModuleGeneral.ColumnCount = 1;
            tlpModuleGeneral.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpModuleGeneral.Controls.Add(grpLanguage, 0, 0);
            tlpModuleGeneral.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpModuleGeneral.Location = new System.Drawing.Point(0, 0);
            tlpModuleGeneral.Margin = new System.Windows.Forms.Padding(0);
            tlpModuleGeneral.Name = "tlpModuleGeneral";
            tlpModuleGeneral.RowCount = 2;
            tlpModuleGeneral.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            tlpModuleGeneral.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpModuleGeneral.Size = new System.Drawing.Size(946, 466);
            tlpModuleGeneral.TabIndex = 5;
            // 
            // grpLanguage
            // 
            grpLanguage.BackColor = System.Drawing.Color.Transparent;
            grpLanguage.Controls.Add(tlpLanguage);
            grpLanguage.Dock = System.Windows.Forms.DockStyle.Fill;
            grpLanguage.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            grpLanguage.ForeColor = System.Drawing.Color.MediumBlue;
            grpLanguage.Location = new System.Drawing.Point(0, 0);
            grpLanguage.Margin = new System.Windows.Forms.Padding(0);
            grpLanguage.Name = "grpLanguage";
            grpLanguage.Padding = new System.Windows.Forms.Padding(5);
            grpLanguage.Size = new System.Drawing.Size(946, 100);
            grpLanguage.TabIndex = 19;
            grpLanguage.TabStop = false;
            grpLanguage.Text = "grpLanguage";
            // 
            // tlpLanguage
            // 
            tlpLanguage.ColumnCount = 2;
            tlpLanguage.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 233F));
            tlpLanguage.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpLanguage.Controls.Add(pnlInternationalLabel, 0, 0);
            tlpLanguage.Controls.Add(pnlNationalLabel, 0, 1);
            tlpLanguage.Controls.Add(pnlInternationalValue, 1, 0);
            tlpLanguage.Controls.Add(pnlNationalValue, 1, 1);
            tlpLanguage.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpLanguage.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            tlpLanguage.ForeColor = System.Drawing.Color.Black;
            tlpLanguage.Location = new System.Drawing.Point(5, 25);
            tlpLanguage.Margin = new System.Windows.Forms.Padding(0);
            tlpLanguage.Name = "tlpLanguage";
            tlpLanguage.RowCount = 3;
            tlpLanguage.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            tlpLanguage.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            tlpLanguage.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpLanguage.Size = new System.Drawing.Size(936, 70);
            tlpLanguage.TabIndex = 0;
            // 
            // pnlInternationalLabel
            // 
            pnlInternationalLabel.Controls.Add(rdoInternational);
            pnlInternationalLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlInternationalLabel.Location = new System.Drawing.Point(0, 0);
            pnlInternationalLabel.Margin = new System.Windows.Forms.Padding(0);
            pnlInternationalLabel.Name = "pnlInternationalLabel";
            pnlInternationalLabel.Size = new System.Drawing.Size(233, 35);
            pnlInternationalLabel.TabIndex = 6;
            // 
            // rdoInternational
            // 
            rdoInternational.AutoEllipsis = true;
            rdoInternational.Dock = System.Windows.Forms.DockStyle.Fill;
            rdoInternational.Font = new System.Drawing.Font("Segoe UI", 9F);
            rdoInternational.ForeColor = System.Drawing.Color.Black;
            rdoInternational.Location = new System.Drawing.Point(0, 0);
            rdoInternational.Margin = new System.Windows.Forms.Padding(0);
            rdoInternational.Name = "rdoInternational";
            rdoInternational.Size = new System.Drawing.Size(233, 35);
            rdoInternational.TabIndex = 3;
            rdoInternational.TabStop = true;
            rdoInternational.Text = "rdoInternational";
            rdoInternational.UseVisualStyleBackColor = true;
            // 
            // pnlNationalLabel
            // 
            pnlNationalLabel.Controls.Add(rdoNational);
            pnlNationalLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlNationalLabel.Location = new System.Drawing.Point(0, 35);
            pnlNationalLabel.Margin = new System.Windows.Forms.Padding(0);
            pnlNationalLabel.Name = "pnlNationalLabel";
            pnlNationalLabel.Size = new System.Drawing.Size(233, 35);
            pnlNationalLabel.TabIndex = 7;
            // 
            // rdoNational
            // 
            rdoNational.AutoEllipsis = true;
            rdoNational.Dock = System.Windows.Forms.DockStyle.Fill;
            rdoNational.Font = new System.Drawing.Font("Segoe UI", 9F);
            rdoNational.ForeColor = System.Drawing.Color.Black;
            rdoNational.Location = new System.Drawing.Point(0, 0);
            rdoNational.Margin = new System.Windows.Forms.Padding(0);
            rdoNational.Name = "rdoNational";
            rdoNational.Size = new System.Drawing.Size(233, 35);
            rdoNational.TabIndex = 4;
            rdoNational.TabStop = true;
            rdoNational.Text = "rdoNational";
            rdoNational.UseVisualStyleBackColor = true;
            // 
            // pnlInternationalValue
            // 
            pnlInternationalValue.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlInternationalValue.Location = new System.Drawing.Point(233, 0);
            pnlInternationalValue.Margin = new System.Windows.Forms.Padding(0);
            pnlInternationalValue.Name = "pnlInternationalValue";
            pnlInternationalValue.Size = new System.Drawing.Size(703, 35);
            pnlInternationalValue.TabIndex = 8;
            // 
            // pnlNationalValue
            // 
            pnlNationalValue.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlNationalValue.Location = new System.Drawing.Point(233, 35);
            pnlNationalValue.Margin = new System.Windows.Forms.Padding(0);
            pnlNationalValue.Name = "pnlNationalValue";
            pnlNationalValue.Size = new System.Drawing.Size(703, 35);
            pnlNationalValue.TabIndex = 9;
            // 
            // tpModuleTargetData
            // 
            tpModuleTargetData.Controls.Add(tlpModuleTargetData);
            tpModuleTargetData.Location = new System.Drawing.Point(4, 24);
            tpModuleTargetData.Margin = new System.Windows.Forms.Padding(0);
            tpModuleTargetData.Name = "tpModuleTargetData";
            tpModuleTargetData.Size = new System.Drawing.Size(946, 466);
            tpModuleTargetData.TabIndex = 0;
            tpModuleTargetData.Text = "tpModuleTargetData";
            tpModuleTargetData.UseVisualStyleBackColor = true;
            // 
            // tlpModuleTargetData
            // 
            tlpModuleTargetData.BackColor = System.Drawing.Color.Transparent;
            tlpModuleTargetData.ColumnCount = 1;
            tlpModuleTargetData.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpModuleTargetData.Controls.Add(grpLocalDensityThreadCount, 0, 0);
            tlpModuleTargetData.Controls.Add(grpLocalDensityThreshold, 0, 1);
            tlpModuleTargetData.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpModuleTargetData.Location = new System.Drawing.Point(0, 0);
            tlpModuleTargetData.Margin = new System.Windows.Forms.Padding(0);
            tlpModuleTargetData.Name = "tlpModuleTargetData";
            tlpModuleTargetData.RowCount = 3;
            tlpModuleTargetData.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            tlpModuleTargetData.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            tlpModuleTargetData.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpModuleTargetData.Size = new System.Drawing.Size(946, 466);
            tlpModuleTargetData.TabIndex = 5;
            // 
            // grpLocalDensityThreadCount
            // 
            grpLocalDensityThreadCount.BackColor = System.Drawing.Color.Transparent;
            grpLocalDensityThreadCount.Controls.Add(pnlLocalDensityThreadCount);
            grpLocalDensityThreadCount.Dock = System.Windows.Forms.DockStyle.Fill;
            grpLocalDensityThreadCount.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            grpLocalDensityThreadCount.ForeColor = System.Drawing.Color.MediumBlue;
            grpLocalDensityThreadCount.Location = new System.Drawing.Point(0, 0);
            grpLocalDensityThreadCount.Margin = new System.Windows.Forms.Padding(0);
            grpLocalDensityThreadCount.Name = "grpLocalDensityThreadCount";
            grpLocalDensityThreadCount.Padding = new System.Windows.Forms.Padding(5);
            grpLocalDensityThreadCount.Size = new System.Drawing.Size(946, 55);
            grpLocalDensityThreadCount.TabIndex = 25;
            grpLocalDensityThreadCount.TabStop = false;
            grpLocalDensityThreadCount.Text = "grpLocalDensityThreadCount";
            // 
            // pnlLocalDensityThreadCount
            // 
            pnlLocalDensityThreadCount.BackColor = System.Drawing.Color.Transparent;
            pnlLocalDensityThreadCount.Controls.Add(txtLocalDensityThreadCount);
            pnlLocalDensityThreadCount.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlLocalDensityThreadCount.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            pnlLocalDensityThreadCount.ForeColor = System.Drawing.Color.Black;
            pnlLocalDensityThreadCount.Location = new System.Drawing.Point(5, 25);
            pnlLocalDensityThreadCount.Margin = new System.Windows.Forms.Padding(0);
            pnlLocalDensityThreadCount.Name = "pnlLocalDensityThreadCount";
            pnlLocalDensityThreadCount.Padding = new System.Windows.Forms.Padding(5);
            pnlLocalDensityThreadCount.Size = new System.Drawing.Size(936, 25);
            pnlLocalDensityThreadCount.TabIndex = 0;
            // 
            // txtLocalDensityThreadCount
            // 
            txtLocalDensityThreadCount.BackColor = System.Drawing.Color.White;
            txtLocalDensityThreadCount.BorderStyle = System.Windows.Forms.BorderStyle.None;
            txtLocalDensityThreadCount.Dock = System.Windows.Forms.DockStyle.Fill;
            txtLocalDensityThreadCount.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold);
            txtLocalDensityThreadCount.ForeColor = System.Drawing.Color.Black;
            txtLocalDensityThreadCount.Location = new System.Drawing.Point(5, 5);
            txtLocalDensityThreadCount.Margin = new System.Windows.Forms.Padding(0);
            txtLocalDensityThreadCount.Name = "txtLocalDensityThreadCount";
            txtLocalDensityThreadCount.Size = new System.Drawing.Size(926, 16);
            txtLocalDensityThreadCount.TabIndex = 1;
            // 
            // grpLocalDensityThreshold
            // 
            grpLocalDensityThreshold.BackColor = System.Drawing.Color.Transparent;
            grpLocalDensityThreshold.Controls.Add(pnlLocalDensityThreshold);
            grpLocalDensityThreshold.Dock = System.Windows.Forms.DockStyle.Fill;
            grpLocalDensityThreshold.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            grpLocalDensityThreshold.ForeColor = System.Drawing.Color.MediumBlue;
            grpLocalDensityThreshold.Location = new System.Drawing.Point(0, 55);
            grpLocalDensityThreshold.Margin = new System.Windows.Forms.Padding(0);
            grpLocalDensityThreshold.Name = "grpLocalDensityThreshold";
            grpLocalDensityThreshold.Padding = new System.Windows.Forms.Padding(5);
            grpLocalDensityThreshold.Size = new System.Drawing.Size(946, 55);
            grpLocalDensityThreshold.TabIndex = 24;
            grpLocalDensityThreshold.TabStop = false;
            grpLocalDensityThreshold.Text = "grpLocalDensityThreshold";
            // 
            // pnlLocalDensityThreshold
            // 
            pnlLocalDensityThreshold.BackColor = System.Drawing.Color.Transparent;
            pnlLocalDensityThreshold.Controls.Add(txtLocalDensityThreshold);
            pnlLocalDensityThreshold.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlLocalDensityThreshold.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            pnlLocalDensityThreshold.ForeColor = System.Drawing.Color.Black;
            pnlLocalDensityThreshold.Location = new System.Drawing.Point(5, 25);
            pnlLocalDensityThreshold.Margin = new System.Windows.Forms.Padding(0);
            pnlLocalDensityThreshold.Name = "pnlLocalDensityThreshold";
            pnlLocalDensityThreshold.Padding = new System.Windows.Forms.Padding(5);
            pnlLocalDensityThreshold.Size = new System.Drawing.Size(936, 25);
            pnlLocalDensityThreshold.TabIndex = 1;
            // 
            // txtLocalDensityThreshold
            // 
            txtLocalDensityThreshold.BackColor = System.Drawing.Color.White;
            txtLocalDensityThreshold.BorderStyle = System.Windows.Forms.BorderStyle.None;
            txtLocalDensityThreshold.Dock = System.Windows.Forms.DockStyle.Fill;
            txtLocalDensityThreshold.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold);
            txtLocalDensityThreshold.ForeColor = System.Drawing.Color.Black;
            txtLocalDensityThreshold.Location = new System.Drawing.Point(5, 5);
            txtLocalDensityThreshold.Margin = new System.Windows.Forms.Padding(0);
            txtLocalDensityThreshold.Name = "txtLocalDensityThreshold";
            txtLocalDensityThreshold.Size = new System.Drawing.Size(926, 16);
            txtLocalDensityThreshold.TabIndex = 1;
            // 
            // tpModuleAuxiliaryData
            // 
            tpModuleAuxiliaryData.Controls.Add(tlpModuleAuxiliaryData);
            tpModuleAuxiliaryData.Location = new System.Drawing.Point(4, 24);
            tpModuleAuxiliaryData.Margin = new System.Windows.Forms.Padding(0);
            tpModuleAuxiliaryData.Name = "tpModuleAuxiliaryData";
            tpModuleAuxiliaryData.Size = new System.Drawing.Size(946, 466);
            tpModuleAuxiliaryData.TabIndex = 1;
            tpModuleAuxiliaryData.Text = "tpModuleAuxiliaryData";
            tpModuleAuxiliaryData.UseVisualStyleBackColor = true;
            // 
            // tlpModuleAuxiliaryData
            // 
            tlpModuleAuxiliaryData.BackColor = System.Drawing.Color.Transparent;
            tlpModuleAuxiliaryData.ColumnCount = 1;
            tlpModuleAuxiliaryData.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpModuleAuxiliaryData.Controls.Add(grpAuxVarTotalThreadCount, 0, 0);
            tlpModuleAuxiliaryData.Controls.Add(grpAuxVarTotalRefreshTime, 0, 1);
            tlpModuleAuxiliaryData.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpModuleAuxiliaryData.Location = new System.Drawing.Point(0, 0);
            tlpModuleAuxiliaryData.Margin = new System.Windows.Forms.Padding(0);
            tlpModuleAuxiliaryData.Name = "tlpModuleAuxiliaryData";
            tlpModuleAuxiliaryData.RowCount = 3;
            tlpModuleAuxiliaryData.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            tlpModuleAuxiliaryData.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            tlpModuleAuxiliaryData.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpModuleAuxiliaryData.Size = new System.Drawing.Size(946, 466);
            tlpModuleAuxiliaryData.TabIndex = 5;
            // 
            // grpAuxVarTotalThreadCount
            // 
            grpAuxVarTotalThreadCount.BackColor = System.Drawing.Color.Transparent;
            grpAuxVarTotalThreadCount.Controls.Add(pnlAuxVarTotalThreadCount);
            grpAuxVarTotalThreadCount.Dock = System.Windows.Forms.DockStyle.Fill;
            grpAuxVarTotalThreadCount.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            grpAuxVarTotalThreadCount.ForeColor = System.Drawing.Color.MediumBlue;
            grpAuxVarTotalThreadCount.Location = new System.Drawing.Point(0, 0);
            grpAuxVarTotalThreadCount.Margin = new System.Windows.Forms.Padding(0);
            grpAuxVarTotalThreadCount.Name = "grpAuxVarTotalThreadCount";
            grpAuxVarTotalThreadCount.Padding = new System.Windows.Forms.Padding(5);
            grpAuxVarTotalThreadCount.Size = new System.Drawing.Size(946, 55);
            grpAuxVarTotalThreadCount.TabIndex = 22;
            grpAuxVarTotalThreadCount.TabStop = false;
            grpAuxVarTotalThreadCount.Text = "grpAuxVarTotalThreadCount";
            // 
            // pnlAuxVarTotalThreadCount
            // 
            pnlAuxVarTotalThreadCount.BackColor = System.Drawing.Color.Transparent;
            pnlAuxVarTotalThreadCount.Controls.Add(txtAuxVarTotalThreadCount);
            pnlAuxVarTotalThreadCount.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlAuxVarTotalThreadCount.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            pnlAuxVarTotalThreadCount.ForeColor = System.Drawing.Color.Black;
            pnlAuxVarTotalThreadCount.Location = new System.Drawing.Point(5, 25);
            pnlAuxVarTotalThreadCount.Margin = new System.Windows.Forms.Padding(0);
            pnlAuxVarTotalThreadCount.Name = "pnlAuxVarTotalThreadCount";
            pnlAuxVarTotalThreadCount.Padding = new System.Windows.Forms.Padding(5);
            pnlAuxVarTotalThreadCount.Size = new System.Drawing.Size(936, 25);
            pnlAuxVarTotalThreadCount.TabIndex = 2;
            // 
            // txtAuxVarTotalThreadCount
            // 
            txtAuxVarTotalThreadCount.BackColor = System.Drawing.Color.White;
            txtAuxVarTotalThreadCount.BorderStyle = System.Windows.Forms.BorderStyle.None;
            txtAuxVarTotalThreadCount.Dock = System.Windows.Forms.DockStyle.Fill;
            txtAuxVarTotalThreadCount.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold);
            txtAuxVarTotalThreadCount.ForeColor = System.Drawing.Color.Black;
            txtAuxVarTotalThreadCount.Location = new System.Drawing.Point(5, 5);
            txtAuxVarTotalThreadCount.Margin = new System.Windows.Forms.Padding(0);
            txtAuxVarTotalThreadCount.Name = "txtAuxVarTotalThreadCount";
            txtAuxVarTotalThreadCount.Size = new System.Drawing.Size(926, 16);
            txtAuxVarTotalThreadCount.TabIndex = 1;
            // 
            // grpAuxVarTotalRefreshTime
            // 
            grpAuxVarTotalRefreshTime.BackColor = System.Drawing.Color.Transparent;
            grpAuxVarTotalRefreshTime.Controls.Add(pnlAuxVarTotalRefreshTime);
            grpAuxVarTotalRefreshTime.Dock = System.Windows.Forms.DockStyle.Fill;
            grpAuxVarTotalRefreshTime.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            grpAuxVarTotalRefreshTime.ForeColor = System.Drawing.Color.MediumBlue;
            grpAuxVarTotalRefreshTime.Location = new System.Drawing.Point(0, 55);
            grpAuxVarTotalRefreshTime.Margin = new System.Windows.Forms.Padding(0);
            grpAuxVarTotalRefreshTime.Name = "grpAuxVarTotalRefreshTime";
            grpAuxVarTotalRefreshTime.Padding = new System.Windows.Forms.Padding(5);
            grpAuxVarTotalRefreshTime.Size = new System.Drawing.Size(946, 55);
            grpAuxVarTotalRefreshTime.TabIndex = 23;
            grpAuxVarTotalRefreshTime.TabStop = false;
            grpAuxVarTotalRefreshTime.Text = "grpAuxVarTotalRefreshTime";
            // 
            // pnlAuxVarTotalRefreshTime
            // 
            pnlAuxVarTotalRefreshTime.BackColor = System.Drawing.Color.Transparent;
            pnlAuxVarTotalRefreshTime.Controls.Add(txtAuxVarTotalRefreshTime);
            pnlAuxVarTotalRefreshTime.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlAuxVarTotalRefreshTime.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            pnlAuxVarTotalRefreshTime.ForeColor = System.Drawing.Color.Black;
            pnlAuxVarTotalRefreshTime.Location = new System.Drawing.Point(5, 25);
            pnlAuxVarTotalRefreshTime.Margin = new System.Windows.Forms.Padding(0);
            pnlAuxVarTotalRefreshTime.Name = "pnlAuxVarTotalRefreshTime";
            pnlAuxVarTotalRefreshTime.Padding = new System.Windows.Forms.Padding(5);
            pnlAuxVarTotalRefreshTime.Size = new System.Drawing.Size(936, 25);
            pnlAuxVarTotalRefreshTime.TabIndex = 2;
            // 
            // txtAuxVarTotalRefreshTime
            // 
            txtAuxVarTotalRefreshTime.BackColor = System.Drawing.Color.White;
            txtAuxVarTotalRefreshTime.BorderStyle = System.Windows.Forms.BorderStyle.None;
            txtAuxVarTotalRefreshTime.Dock = System.Windows.Forms.DockStyle.Fill;
            txtAuxVarTotalRefreshTime.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold);
            txtAuxVarTotalRefreshTime.ForeColor = System.Drawing.Color.Black;
            txtAuxVarTotalRefreshTime.Location = new System.Drawing.Point(5, 5);
            txtAuxVarTotalRefreshTime.Margin = new System.Windows.Forms.Padding(0);
            txtAuxVarTotalRefreshTime.Name = "txtAuxVarTotalRefreshTime";
            txtAuxVarTotalRefreshTime.Size = new System.Drawing.Size(926, 16);
            txtAuxVarTotalRefreshTime.TabIndex = 1;
            // 
            // tpModuleEstimate
            // 
            tpModuleEstimate.Controls.Add(tlpModuleEstimate);
            tpModuleEstimate.Location = new System.Drawing.Point(4, 24);
            tpModuleEstimate.Margin = new System.Windows.Forms.Padding(0);
            tpModuleEstimate.Name = "tpModuleEstimate";
            tpModuleEstimate.Size = new System.Drawing.Size(946, 466);
            tpModuleEstimate.TabIndex = 2;
            tpModuleEstimate.Text = "tpModuleEstimate";
            tpModuleEstimate.UseVisualStyleBackColor = true;
            // 
            // tlpModuleEstimate
            // 
            tlpModuleEstimate.BackColor = System.Drawing.Color.Transparent;
            tlpModuleEstimate.ColumnCount = 1;
            tlpModuleEstimate.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpModuleEstimate.Controls.Add(grpCellCoverageTolerance, 0, 3);
            tlpModuleEstimate.Controls.Add(grpAdditivityLimit, 0, 2);
            tlpModuleEstimate.Controls.Add(grpEstimateThreadCount, 0, 1);
            tlpModuleEstimate.Controls.Add(grpConfigurationThreadCount, 0, 0);
            tlpModuleEstimate.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpModuleEstimate.Location = new System.Drawing.Point(0, 0);
            tlpModuleEstimate.Margin = new System.Windows.Forms.Padding(0);
            tlpModuleEstimate.Name = "tlpModuleEstimate";
            tlpModuleEstimate.RowCount = 5;
            tlpModuleEstimate.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            tlpModuleEstimate.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            tlpModuleEstimate.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            tlpModuleEstimate.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            tlpModuleEstimate.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpModuleEstimate.Size = new System.Drawing.Size(946, 466);
            tlpModuleEstimate.TabIndex = 5;
            // 
            // grpCellCoverageTolerance
            // 
            grpCellCoverageTolerance.BackColor = System.Drawing.Color.Transparent;
            grpCellCoverageTolerance.Controls.Add(pnlCellCoverageTolerance);
            grpCellCoverageTolerance.Dock = System.Windows.Forms.DockStyle.Fill;
            grpCellCoverageTolerance.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            grpCellCoverageTolerance.ForeColor = System.Drawing.Color.MediumBlue;
            grpCellCoverageTolerance.Location = new System.Drawing.Point(0, 165);
            grpCellCoverageTolerance.Margin = new System.Windows.Forms.Padding(0);
            grpCellCoverageTolerance.Name = "grpCellCoverageTolerance";
            grpCellCoverageTolerance.Padding = new System.Windows.Forms.Padding(5);
            grpCellCoverageTolerance.Size = new System.Drawing.Size(946, 55);
            grpCellCoverageTolerance.TabIndex = 27;
            grpCellCoverageTolerance.TabStop = false;
            grpCellCoverageTolerance.Text = "grpCellCoverageTolerance";
            // 
            // pnlCellCoverageTolerance
            // 
            pnlCellCoverageTolerance.BackColor = System.Drawing.Color.Transparent;
            pnlCellCoverageTolerance.Controls.Add(txtCellCoverageTolerance);
            pnlCellCoverageTolerance.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlCellCoverageTolerance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            pnlCellCoverageTolerance.ForeColor = System.Drawing.Color.Black;
            pnlCellCoverageTolerance.Location = new System.Drawing.Point(5, 25);
            pnlCellCoverageTolerance.Margin = new System.Windows.Forms.Padding(0);
            pnlCellCoverageTolerance.Name = "pnlCellCoverageTolerance";
            pnlCellCoverageTolerance.Padding = new System.Windows.Forms.Padding(5);
            pnlCellCoverageTolerance.Size = new System.Drawing.Size(936, 25);
            pnlCellCoverageTolerance.TabIndex = 3;
            // 
            // txtCellCoverageTolerance
            // 
            txtCellCoverageTolerance.BackColor = System.Drawing.Color.White;
            txtCellCoverageTolerance.BorderStyle = System.Windows.Forms.BorderStyle.None;
            txtCellCoverageTolerance.Dock = System.Windows.Forms.DockStyle.Fill;
            txtCellCoverageTolerance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold);
            txtCellCoverageTolerance.ForeColor = System.Drawing.Color.Black;
            txtCellCoverageTolerance.Location = new System.Drawing.Point(5, 5);
            txtCellCoverageTolerance.Margin = new System.Windows.Forms.Padding(0);
            txtCellCoverageTolerance.Name = "txtCellCoverageTolerance";
            txtCellCoverageTolerance.Size = new System.Drawing.Size(926, 16);
            txtCellCoverageTolerance.TabIndex = 1;
            // 
            // grpAdditivityLimit
            // 
            grpAdditivityLimit.BackColor = System.Drawing.Color.Transparent;
            grpAdditivityLimit.Controls.Add(pnlAdditivityLimit);
            grpAdditivityLimit.Dock = System.Windows.Forms.DockStyle.Fill;
            grpAdditivityLimit.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            grpAdditivityLimit.ForeColor = System.Drawing.Color.MediumBlue;
            grpAdditivityLimit.Location = new System.Drawing.Point(0, 110);
            grpAdditivityLimit.Margin = new System.Windows.Forms.Padding(0);
            grpAdditivityLimit.Name = "grpAdditivityLimit";
            grpAdditivityLimit.Padding = new System.Windows.Forms.Padding(5);
            grpAdditivityLimit.Size = new System.Drawing.Size(946, 55);
            grpAdditivityLimit.TabIndex = 23;
            grpAdditivityLimit.TabStop = false;
            grpAdditivityLimit.Text = "grpAdditivityLimit";
            // 
            // pnlAdditivityLimit
            // 
            pnlAdditivityLimit.BackColor = System.Drawing.Color.Transparent;
            pnlAdditivityLimit.Controls.Add(txtAdditivityLimit);
            pnlAdditivityLimit.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlAdditivityLimit.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            pnlAdditivityLimit.ForeColor = System.Drawing.Color.Black;
            pnlAdditivityLimit.Location = new System.Drawing.Point(5, 25);
            pnlAdditivityLimit.Margin = new System.Windows.Forms.Padding(0);
            pnlAdditivityLimit.Name = "pnlAdditivityLimit";
            pnlAdditivityLimit.Padding = new System.Windows.Forms.Padding(5);
            pnlAdditivityLimit.Size = new System.Drawing.Size(936, 25);
            pnlAdditivityLimit.TabIndex = 3;
            // 
            // txtAdditivityLimit
            // 
            txtAdditivityLimit.BackColor = System.Drawing.Color.White;
            txtAdditivityLimit.BorderStyle = System.Windows.Forms.BorderStyle.None;
            txtAdditivityLimit.Dock = System.Windows.Forms.DockStyle.Fill;
            txtAdditivityLimit.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold);
            txtAdditivityLimit.ForeColor = System.Drawing.Color.Black;
            txtAdditivityLimit.Location = new System.Drawing.Point(5, 5);
            txtAdditivityLimit.Margin = new System.Windows.Forms.Padding(0);
            txtAdditivityLimit.Name = "txtAdditivityLimit";
            txtAdditivityLimit.Size = new System.Drawing.Size(926, 16);
            txtAdditivityLimit.TabIndex = 1;
            // 
            // grpEstimateThreadCount
            // 
            grpEstimateThreadCount.BackColor = System.Drawing.Color.Transparent;
            grpEstimateThreadCount.Controls.Add(pnlEstimateThreadCount);
            grpEstimateThreadCount.Dock = System.Windows.Forms.DockStyle.Fill;
            grpEstimateThreadCount.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            grpEstimateThreadCount.ForeColor = System.Drawing.Color.MediumBlue;
            grpEstimateThreadCount.Location = new System.Drawing.Point(0, 55);
            grpEstimateThreadCount.Margin = new System.Windows.Forms.Padding(0);
            grpEstimateThreadCount.Name = "grpEstimateThreadCount";
            grpEstimateThreadCount.Padding = new System.Windows.Forms.Padding(5);
            grpEstimateThreadCount.Size = new System.Drawing.Size(946, 55);
            grpEstimateThreadCount.TabIndex = 26;
            grpEstimateThreadCount.TabStop = false;
            grpEstimateThreadCount.Text = "grpEstimateThreadCount";
            // 
            // pnlEstimateThreadCount
            // 
            pnlEstimateThreadCount.BackColor = System.Drawing.Color.Transparent;
            pnlEstimateThreadCount.Controls.Add(txtEstimateThreadCount);
            pnlEstimateThreadCount.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlEstimateThreadCount.Font = new System.Drawing.Font("Segoe UI", 9F);
            pnlEstimateThreadCount.ForeColor = System.Drawing.Color.Black;
            pnlEstimateThreadCount.Location = new System.Drawing.Point(5, 25);
            pnlEstimateThreadCount.Margin = new System.Windows.Forms.Padding(0);
            pnlEstimateThreadCount.Name = "pnlEstimateThreadCount";
            pnlEstimateThreadCount.Padding = new System.Windows.Forms.Padding(5);
            pnlEstimateThreadCount.Size = new System.Drawing.Size(936, 25);
            pnlEstimateThreadCount.TabIndex = 0;
            // 
            // txtEstimateThreadCount
            // 
            txtEstimateThreadCount.BackColor = System.Drawing.Color.White;
            txtEstimateThreadCount.BorderStyle = System.Windows.Forms.BorderStyle.None;
            txtEstimateThreadCount.Dock = System.Windows.Forms.DockStyle.Fill;
            txtEstimateThreadCount.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, 238);
            txtEstimateThreadCount.ForeColor = System.Drawing.Color.Black;
            txtEstimateThreadCount.Location = new System.Drawing.Point(5, 5);
            txtEstimateThreadCount.Margin = new System.Windows.Forms.Padding(0);
            txtEstimateThreadCount.Name = "txtEstimateThreadCount";
            txtEstimateThreadCount.Size = new System.Drawing.Size(926, 16);
            txtEstimateThreadCount.TabIndex = 1;
            // 
            // grpConfigurationThreadCount
            // 
            grpConfigurationThreadCount.BackColor = System.Drawing.Color.Transparent;
            grpConfigurationThreadCount.Controls.Add(pnlConfigurationThreadCount);
            grpConfigurationThreadCount.Dock = System.Windows.Forms.DockStyle.Fill;
            grpConfigurationThreadCount.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            grpConfigurationThreadCount.ForeColor = System.Drawing.Color.MediumBlue;
            grpConfigurationThreadCount.Location = new System.Drawing.Point(0, 0);
            grpConfigurationThreadCount.Margin = new System.Windows.Forms.Padding(0);
            grpConfigurationThreadCount.Name = "grpConfigurationThreadCount";
            grpConfigurationThreadCount.Padding = new System.Windows.Forms.Padding(5);
            grpConfigurationThreadCount.Size = new System.Drawing.Size(946, 55);
            grpConfigurationThreadCount.TabIndex = 28;
            grpConfigurationThreadCount.TabStop = false;
            grpConfigurationThreadCount.Text = "grpConfigurationThreadCount";
            // 
            // pnlConfigurationThreadCount
            // 
            pnlConfigurationThreadCount.BackColor = System.Drawing.Color.Transparent;
            pnlConfigurationThreadCount.Controls.Add(txtConfigurationThreadCount);
            pnlConfigurationThreadCount.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlConfigurationThreadCount.Font = new System.Drawing.Font("Segoe UI", 9F);
            pnlConfigurationThreadCount.ForeColor = System.Drawing.Color.Black;
            pnlConfigurationThreadCount.Location = new System.Drawing.Point(5, 25);
            pnlConfigurationThreadCount.Margin = new System.Windows.Forms.Padding(0);
            pnlConfigurationThreadCount.Name = "pnlConfigurationThreadCount";
            pnlConfigurationThreadCount.Padding = new System.Windows.Forms.Padding(5);
            pnlConfigurationThreadCount.Size = new System.Drawing.Size(936, 25);
            pnlConfigurationThreadCount.TabIndex = 0;
            // 
            // txtConfigurationThreadCount
            // 
            txtConfigurationThreadCount.BackColor = System.Drawing.Color.White;
            txtConfigurationThreadCount.BorderStyle = System.Windows.Forms.BorderStyle.None;
            txtConfigurationThreadCount.Dock = System.Windows.Forms.DockStyle.Fill;
            txtConfigurationThreadCount.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, 238);
            txtConfigurationThreadCount.ForeColor = System.Drawing.Color.Black;
            txtConfigurationThreadCount.Location = new System.Drawing.Point(5, 5);
            txtConfigurationThreadCount.Margin = new System.Windows.Forms.Padding(0);
            txtConfigurationThreadCount.Name = "txtConfigurationThreadCount";
            txtConfigurationThreadCount.Size = new System.Drawing.Size(926, 16);
            txtConfigurationThreadCount.TabIndex = 1;
            // 
            // pnlButtons
            // 
            pnlButtons.BackColor = System.Drawing.Color.Transparent;
            pnlButtons.Controls.Add(tlpButtons);
            pnlButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlButtons.Location = new System.Drawing.Point(0, 494);
            pnlButtons.Margin = new System.Windows.Forms.Padding(0);
            pnlButtons.Name = "pnlButtons";
            pnlButtons.Size = new System.Drawing.Size(954, 40);
            pnlButtons.TabIndex = 3;
            // 
            // tlpButtons
            // 
            tlpButtons.BackColor = System.Drawing.Color.WhiteSmoke;
            tlpButtons.ColumnCount = 3;
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            tlpButtons.Controls.Add(pnlOK, 1, 0);
            tlpButtons.Controls.Add(pnlCancel, 2, 0);
            tlpButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpButtons.Location = new System.Drawing.Point(0, 0);
            tlpButtons.Margin = new System.Windows.Forms.Padding(0);
            tlpButtons.Name = "tlpButtons";
            tlpButtons.RowCount = 1;
            tlpButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.Size = new System.Drawing.Size(954, 40);
            tlpButtons.TabIndex = 2;
            // 
            // pnlOK
            // 
            pnlOK.Controls.Add(btnOK);
            pnlOK.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlOK.Location = new System.Drawing.Point(634, 0);
            pnlOK.Margin = new System.Windows.Forms.Padding(0);
            pnlOK.Name = "pnlOK";
            pnlOK.Padding = new System.Windows.Forms.Padding(5);
            pnlOK.Size = new System.Drawing.Size(160, 40);
            pnlOK.TabIndex = 13;
            // 
            // btnOK
            // 
            btnOK.AutoEllipsis = true;
            btnOK.BackColor = System.Drawing.Color.Transparent;
            btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            btnOK.Dock = System.Windows.Forms.DockStyle.Fill;
            btnOK.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            btnOK.Location = new System.Drawing.Point(5, 5);
            btnOK.Margin = new System.Windows.Forms.Padding(0);
            btnOK.Name = "btnOK";
            btnOK.Size = new System.Drawing.Size(150, 30);
            btnOK.TabIndex = 12;
            btnOK.Text = "btnOK";
            btnOK.UseVisualStyleBackColor = false;
            // 
            // pnlCancel
            // 
            pnlCancel.Controls.Add(btnCancel);
            pnlCancel.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlCancel.Location = new System.Drawing.Point(794, 0);
            pnlCancel.Margin = new System.Windows.Forms.Padding(0);
            pnlCancel.Name = "pnlCancel";
            pnlCancel.Padding = new System.Windows.Forms.Padding(5);
            pnlCancel.Size = new System.Drawing.Size(160, 40);
            pnlCancel.TabIndex = 12;
            // 
            // btnCancel
            // 
            btnCancel.AutoEllipsis = true;
            btnCancel.BackColor = System.Drawing.Color.Transparent;
            btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            btnCancel.Dock = System.Windows.Forms.DockStyle.Fill;
            btnCancel.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            btnCancel.Location = new System.Drawing.Point(5, 5);
            btnCancel.Margin = new System.Windows.Forms.Padding(0);
            btnCancel.Name = "btnCancel";
            btnCancel.Size = new System.Drawing.Size(150, 30);
            btnCancel.TabIndex = 13;
            btnCancel.Text = "btnCancel";
            btnCancel.UseVisualStyleBackColor = false;
            // 
            // FormSettings
            // 
            AcceptButton = btnOK;
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            BackColor = System.Drawing.Color.WhiteSmoke;
            CancelButton = btnCancel;
            ClientSize = new System.Drawing.Size(960, 540);
            Controls.Add(pnlMain);
            ForeColor = System.Drawing.Color.Black;
            FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "FormSettings";
            ShowIcon = false;
            ShowInTaskbar = false;
            StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            Tag = "";
            Text = "FormSettings";
            pnlMain.ResumeLayout(false);
            tlpMain.ResumeLayout(false);
            tabMain.ResumeLayout(false);
            tpModuleGeneral.ResumeLayout(false);
            tlpModuleGeneral.ResumeLayout(false);
            grpLanguage.ResumeLayout(false);
            tlpLanguage.ResumeLayout(false);
            pnlInternationalLabel.ResumeLayout(false);
            pnlNationalLabel.ResumeLayout(false);
            tpModuleTargetData.ResumeLayout(false);
            tlpModuleTargetData.ResumeLayout(false);
            grpLocalDensityThreadCount.ResumeLayout(false);
            pnlLocalDensityThreadCount.ResumeLayout(false);
            pnlLocalDensityThreadCount.PerformLayout();
            grpLocalDensityThreshold.ResumeLayout(false);
            pnlLocalDensityThreshold.ResumeLayout(false);
            pnlLocalDensityThreshold.PerformLayout();
            tpModuleAuxiliaryData.ResumeLayout(false);
            tlpModuleAuxiliaryData.ResumeLayout(false);
            grpAuxVarTotalThreadCount.ResumeLayout(false);
            pnlAuxVarTotalThreadCount.ResumeLayout(false);
            pnlAuxVarTotalThreadCount.PerformLayout();
            grpAuxVarTotalRefreshTime.ResumeLayout(false);
            pnlAuxVarTotalRefreshTime.ResumeLayout(false);
            pnlAuxVarTotalRefreshTime.PerformLayout();
            tpModuleEstimate.ResumeLayout(false);
            tlpModuleEstimate.ResumeLayout(false);
            grpCellCoverageTolerance.ResumeLayout(false);
            pnlCellCoverageTolerance.ResumeLayout(false);
            pnlCellCoverageTolerance.PerformLayout();
            grpAdditivityLimit.ResumeLayout(false);
            pnlAdditivityLimit.ResumeLayout(false);
            pnlAdditivityLimit.PerformLayout();
            grpEstimateThreadCount.ResumeLayout(false);
            pnlEstimateThreadCount.ResumeLayout(false);
            pnlEstimateThreadCount.PerformLayout();
            grpConfigurationThreadCount.ResumeLayout(false);
            pnlConfigurationThreadCount.ResumeLayout(false);
            pnlConfigurationThreadCount.PerformLayout();
            pnlButtons.ResumeLayout(false);
            tlpButtons.ResumeLayout(false);
            pnlOK.ResumeLayout(false);
            pnlCancel.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.TabControl tabMain;
        private System.Windows.Forms.TabPage tpModuleGeneral;
        private System.Windows.Forms.TabPage tpModuleTargetData;
        private System.Windows.Forms.TabPage tpModuleAuxiliaryData;
        private System.Windows.Forms.TabPage tpModuleEstimate;
        private System.Windows.Forms.Panel pnlButtons;
        private System.Windows.Forms.TableLayoutPanel tlpButtons;
        private System.Windows.Forms.Panel pnlCancel;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Panel pnlOK;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.TableLayoutPanel tlpModuleAuxiliaryData;
        private System.Windows.Forms.GroupBox grpAuxVarTotalThreadCount;
        private System.Windows.Forms.Panel pnlAuxVarTotalThreadCount;
        private System.Windows.Forms.TextBox txtAuxVarTotalThreadCount;
        private System.Windows.Forms.GroupBox grpAuxVarTotalRefreshTime;
        private System.Windows.Forms.Panel pnlAuxVarTotalRefreshTime;
        private System.Windows.Forms.TextBox txtAuxVarTotalRefreshTime;
        private System.Windows.Forms.TableLayoutPanel tlpModuleEstimate;
        private System.Windows.Forms.GroupBox grpCellCoverageTolerance;
        private System.Windows.Forms.Panel pnlCellCoverageTolerance;
        private System.Windows.Forms.TextBox txtCellCoverageTolerance;
        private System.Windows.Forms.GroupBox grpAdditivityLimit;
        private System.Windows.Forms.Panel pnlAdditivityLimit;
        private System.Windows.Forms.TextBox txtAdditivityLimit;
        private System.Windows.Forms.GroupBox grpEstimateThreadCount;
        private System.Windows.Forms.Panel pnlEstimateThreadCount;
        private System.Windows.Forms.TextBox txtEstimateThreadCount;
        private System.Windows.Forms.TableLayoutPanel tlpModuleGeneral;
        private System.Windows.Forms.GroupBox grpLanguage;
        private System.Windows.Forms.TableLayoutPanel tlpLanguage;
        private System.Windows.Forms.Panel pnlInternationalLabel;
        private System.Windows.Forms.RadioButton rdoInternational;
        private System.Windows.Forms.Panel pnlNationalLabel;
        private System.Windows.Forms.RadioButton rdoNational;
        private System.Windows.Forms.Panel pnlInternationalValue;
        private System.Windows.Forms.Panel pnlNationalValue;
        private System.Windows.Forms.TableLayoutPanel tlpModuleTargetData;
        private System.Windows.Forms.GroupBox grpLocalDensityThreadCount;
        private System.Windows.Forms.Panel pnlLocalDensityThreadCount;
        private System.Windows.Forms.TextBox txtLocalDensityThreadCount;
        private System.Windows.Forms.GroupBox grpLocalDensityThreshold;
        private System.Windows.Forms.Panel pnlLocalDensityThreshold;
        private System.Windows.Forms.TextBox txtLocalDensityThreshold;
        private System.Windows.Forms.GroupBox grpConfigurationThreadCount;
        private System.Windows.Forms.Panel pnlConfigurationThreadCount;
        private System.Windows.Forms.TextBox txtConfigurationThreadCount;
    }

}