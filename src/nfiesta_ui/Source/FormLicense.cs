﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Runtime.Versioning;
using System.Text;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.PostgreSQL;

namespace NfiEstaApp
{

    /// <summary>
    /// <para lang="cs">Formulář pro zobrazení licence</para>
    /// <para lang="en">License Agreement Dialog</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    public partial class FormLicense
        : Form, INfiEstaControl
    {

        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        private Control controlOwner;

        #endregion Private Fields


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor formuláře</para>
        /// <para lang="en">Form constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        public FormLicense(Control controlOwner)
        {
            InitializeComponent();
            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                if (controlOwner == null)
                {
                    throw new ArgumentNullException(
                        message: $"Argument {nameof(ControlOwner)} must not be null.",
                        paramName: nameof(ControlOwner));
                }

                if (controlOwner is not FormMain)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(FormMain)}.",
                        paramName: nameof(ControlOwner));
                }

                return controlOwner;
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException(
                        message: $"Argument {nameof(ControlOwner)} must not be null.",
                        paramName: nameof(ControlOwner));
                }

                if (value is not FormMain)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(FormMain)}.",
                        paramName: nameof(ControlOwner));
                }

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables(read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((FormMain)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyk popisků (read-only)</para>
        /// <para lang="en">Language of labels (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((FormMain)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((FormMain)ControlOwner).LanguageFile;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">EUPL text in English</para>
        /// <para lang="en">EUPL text in English</para>
        /// </summary>
        public static string LicenseTextEn
        {
            get
            {
                StringBuilder sb = new();
                sb.AppendLine("                      EUROPEAN UNION PUBLIC LICENCE v. 1.2");
                sb.AppendLine("                      EUPL © the European Union 2007, 2016");
                sb.AppendLine("");
                sb.AppendLine("This European Union Public Licence (the ‘EUPL’) applies to the Work (as defined");
                sb.AppendLine("below) which is provided under the terms of this Licence. Any use of the Work,");
                sb.AppendLine("other than as authorised under this Licence is prohibited (to the extent such");
                sb.AppendLine("use is covered by a right of the copyright holder of the Work).");
                sb.AppendLine("");
                sb.AppendLine("The Work is provided under the terms of this Licence when the Licensor (as");
                sb.AppendLine("defined below) has placed the following notice immediately following the");
                sb.AppendLine("copyright notice for the Work:");
                sb.AppendLine("");
                sb.AppendLine("        Licensed under the EUPL");
                sb.AppendLine("");
                sb.AppendLine("or has expressed by any other means his willingness to license under the EUPL.");
                sb.AppendLine("");
                sb.AppendLine("1. Definitions");
                sb.AppendLine("");
                sb.AppendLine("In this Licence, the following terms have the following meaning:");
                sb.AppendLine("");
                sb.AppendLine("- ‘The Licence’: this Licence.");
                sb.AppendLine("");
                sb.AppendLine("- ‘The Original Work’: the work or software distributed or communicated by the");
                sb.AppendLine("  Licensor under this Licence, available as Source Code and also as Executable");
                sb.AppendLine("  Code as the case may be.");
                sb.AppendLine("");
                sb.AppendLine("- ‘Derivative Works’: the works or software that could be created by the");
                sb.AppendLine("  Licensee, based upon the Original Work or modifications thereof. This Licence");
                sb.AppendLine("  does not define the extent of modification or dependence on the Original Work");
                sb.AppendLine("  required in order to classify a work as a Derivative Work; this extent is");
                sb.AppendLine("  determined by copyright law applicable in the country mentioned in Article 15.");
                sb.AppendLine("");
                sb.AppendLine("- ‘The Work’: the Original Work or its Derivative Works.");
                sb.AppendLine("");
                sb.AppendLine("- ‘The Source Code’: the human-readable form of the Work which is the most");
                sb.AppendLine("  convenient for people to study and modify.");
                sb.AppendLine("");
                sb.AppendLine("- ‘The Executable Code’: any code which has generally been compiled and which is");
                sb.AppendLine("  meant to be interpreted by a computer as a program.");
                sb.AppendLine("");
                sb.AppendLine("- ‘The Licensor’: the natural or legal person that distributes or communicates");
                sb.AppendLine("  the Work under the Licence.");
                sb.AppendLine("");
                sb.AppendLine("- ‘Contributor(s)’: any natural or legal person who modifies the Work under the");
                sb.AppendLine("  Licence, or otherwise contributes to the creation of a Derivative Work.");
                sb.AppendLine("");
                sb.AppendLine("- ‘The Licensee’ or ‘You’: any natural or legal person who makes any usage of");
                sb.AppendLine("  the Work under the terms of the Licence.");
                sb.AppendLine("");
                sb.AppendLine("- ‘Distribution’ or ‘Communication’: any act of selling, giving, lending,");
                sb.AppendLine("  renting, distributing, communicating, transmitting, or otherwise making");
                sb.AppendLine("  available, online or offline, copies of the Work or providing access to its");
                sb.AppendLine("  essential functionalities at the disposal of any other natural or legal");
                sb.AppendLine("  person.");
                sb.AppendLine("");
                sb.AppendLine("2. Scope of the rights granted by the Licence");
                sb.AppendLine("");
                sb.AppendLine("The Licensor hereby grants You a worldwide, royalty-free, non-exclusive,");
                sb.AppendLine("sublicensable licence to do the following, for the duration of copyright vested");
                sb.AppendLine("in the Original Work:");
                sb.AppendLine("");
                sb.AppendLine("- use the Work in any circumstance and for all usage,");
                sb.AppendLine("- reproduce the Work,");
                sb.AppendLine("- modify the Work, and make Derivative Works based upon the Work,");
                sb.AppendLine("- communicate to the public, including the right to make available or display");
                sb.AppendLine("  the Work or copies thereof to the public and perform publicly, as the case may");
                sb.AppendLine("  be, the Work,");
                sb.AppendLine("- distribute the Work or copies thereof,");
                sb.AppendLine("- lend and rent the Work or copies thereof,");
                sb.AppendLine("- sublicense rights in the Work or copies thereof.");
                sb.AppendLine("");
                sb.AppendLine("Those rights can be exercised on any media, supports and formats, whether now");
                sb.AppendLine("known or later invented, as far as the applicable law permits so.");
                sb.AppendLine("");
                sb.AppendLine("In the countries where moral rights apply, the Licensor waives his right to");
                sb.AppendLine("exercise his moral right to the extent allowed by law in order to make effective");
                sb.AppendLine("the licence of the economic rights here above listed.");
                sb.AppendLine("");
                sb.AppendLine("The Licensor grants to the Licensee royalty-free, non-exclusive usage rights to");
                sb.AppendLine("any patents held by the Licensor, to the extent necessary to make use of the");
                sb.AppendLine("rights granted on the Work under this Licence.");
                sb.AppendLine("");
                sb.AppendLine("3. Communication of the Source Code");
                sb.AppendLine("");
                sb.AppendLine("The Licensor may provide the Work either in its Source Code form, or as");
                sb.AppendLine("Executable Code. If the Work is provided as Executable Code, the Licensor");
                sb.AppendLine("provides in addition a machine-readable copy of the Source Code of the Work");
                sb.AppendLine("along with each copy of the Work that the Licensor distributes or indicates, in");
                sb.AppendLine("a notice following the copyright notice attached to the Work, a repository where");
                sb.AppendLine("the Source Code is easily and freely accessible for as long as the Licensor");
                sb.AppendLine("continues to distribute or communicate the Work.");
                sb.AppendLine("");
                sb.AppendLine("4. Limitations on copyright");
                sb.AppendLine("");
                sb.AppendLine("Nothing in this Licence is intended to deprive the Licensee of the benefits from");
                sb.AppendLine("any exception or limitation to the exclusive rights of the rights owners in the");
                sb.AppendLine("Work, of the exhaustion of those rights or of other applicable limitations");
                sb.AppendLine("thereto.");
                sb.AppendLine("");
                sb.AppendLine("5. Obligations of the Licensee");
                sb.AppendLine("");
                sb.AppendLine("The grant of the rights mentioned above is subject to some restrictions and");
                sb.AppendLine("obligations imposed on the Licensee. Those obligations are the following:");
                sb.AppendLine("");
                sb.AppendLine("Attribution right: The Licensee shall keep intact all copyright, patent or");
                sb.AppendLine("trademarks notices and all notices that refer to the Licence and to the");
                sb.AppendLine("disclaimer of warranties. The Licensee must include a copy of such notices and a");
                sb.AppendLine("copy of the Licence with every copy of the Work he/she distributes or");
                sb.AppendLine("communicates. The Licensee must cause any Derivative Work to carry prominent");
                sb.AppendLine("notices stating that the Work has been modified and the date of modification.");
                sb.AppendLine("");
                sb.AppendLine("Copyleft clause: If the Licensee distributes or communicates copies of the");
                sb.AppendLine("Original Works or Derivative Works, this Distribution or Communication will be");
                sb.AppendLine("done under the terms of this Licence or of a later version of this Licence");
                sb.AppendLine("unless the Original Work is expressly distributed only under this version of the");
                sb.AppendLine("Licence — for example by communicating ‘EUPL v. 1.2 only’. The Licensee");
                sb.AppendLine("(becoming Licensor) cannot offer or impose any additional terms or conditions on");
                sb.AppendLine("the Work or Derivative Work that alter or restrict the terms of the Licence.");
                sb.AppendLine("");
                sb.AppendLine("Compatibility clause: If the Licensee Distributes or Communicates Derivative");
                sb.AppendLine("Works or copies thereof based upon both the Work and another work licensed under");
                sb.AppendLine("a Compatible Licence, this Distribution or Communication can be done under the");
                sb.AppendLine("terms of this Compatible Licence. For the sake of this clause, ‘Compatible");
                sb.AppendLine("Licence’ refers to the licences listed in the appendix attached to this Licence.");
                sb.AppendLine("Should the Licensee's obligations under the Compatible Licence conflict with");
                sb.AppendLine("his/her obligations under this Licence, the obligations of the Compatible");
                sb.AppendLine("Licence shall prevail.");
                sb.AppendLine("");
                sb.AppendLine("Provision of Source Code: When distributing or communicating copies of the Work,");
                sb.AppendLine("the Licensee will provide a machine-readable copy of the Source Code or indicate");
                sb.AppendLine("a repository where this Source will be easily and freely available for as long");
                sb.AppendLine("as the Licensee continues to distribute or communicate the Work.");
                sb.AppendLine("");
                sb.AppendLine("Legal Protection: This Licence does not grant permission to use the trade names,");
                sb.AppendLine("trademarks, service marks, or names of the Licensor, except as required for");
                sb.AppendLine("reasonable and customary use in describing the origin of the Work and");
                sb.AppendLine("reproducing the content of the copyright notice.");
                sb.AppendLine("");
                sb.AppendLine("6. Chain of Authorship");
                sb.AppendLine("");
                sb.AppendLine("The original Licensor warrants that the copyright in the Original Work granted");
                sb.AppendLine("hereunder is owned by him/her or licensed to him/her and that he/she has the");
                sb.AppendLine("power and authority to grant the Licence.");
                sb.AppendLine("");
                sb.AppendLine("Each Contributor warrants that the copyright in the modifications he/she brings");
                sb.AppendLine("to the Work are owned by him/her or licensed to him/her and that he/she has the");
                sb.AppendLine("power and authority to grant the Licence.");
                sb.AppendLine("");
                sb.AppendLine("Each time You accept the Licence, the original Licensor and subsequent");
                sb.AppendLine("Contributors grant You a licence to their contributions to the Work, under the");
                sb.AppendLine("terms of this Licence.");
                sb.AppendLine("");
                sb.AppendLine("7. Disclaimer of Warranty");
                sb.AppendLine("");
                sb.AppendLine("The Work is a work in progress, which is continuously improved by numerous");
                sb.AppendLine("Contributors. It is not a finished work and may therefore contain defects or");
                sb.AppendLine("‘bugs’ inherent to this type of development.");
                sb.AppendLine("");
                sb.AppendLine("For the above reason, the Work is provided under the Licence on an ‘as is’ basis");
                sb.AppendLine("and without warranties of any kind concerning the Work, including without");
                sb.AppendLine("limitation merchantability, fitness for a particular purpose, absence of defects");
                sb.AppendLine("or errors, accuracy, non-infringement of intellectual property rights other than");
                sb.AppendLine("copyright as stated in Article 6 of this Licence.");
                sb.AppendLine("");
                sb.AppendLine("This disclaimer of warranty is an essential part of the Licence and a condition");
                sb.AppendLine("for the grant of any rights to the Work.");
                sb.AppendLine("");
                sb.AppendLine("8. Disclaimer of Liability");
                sb.AppendLine("");
                sb.AppendLine("Except in the cases of wilful misconduct or damages directly caused to natural");
                sb.AppendLine("persons, the Licensor will in no event be liable for any direct or indirect,");
                sb.AppendLine("material or moral, damages of any kind, arising out of the Licence or of the use");
                sb.AppendLine("of the Work, including without limitation, damages for loss of goodwill, work");
                sb.AppendLine("stoppage, computer failure or malfunction, loss of data or any commercial");
                sb.AppendLine("damage, even if the Licensor has been advised of the possibility of such damage.");
                sb.AppendLine("However, the Licensor will be liable under statutory product liability laws as");
                sb.AppendLine("far such laws apply to the Work.");
                sb.AppendLine("");
                sb.AppendLine("9. Additional agreements");
                sb.AppendLine("");
                sb.AppendLine("While distributing the Work, You may choose to conclude an additional agreement,");
                sb.AppendLine("defining obligations or services consistent with this Licence. However, if");
                sb.AppendLine("accepting obligations, You may act only on your own behalf and on your sole");
                sb.AppendLine("responsibility, not on behalf of the original Licensor or any other Contributor,");
                sb.AppendLine("and only if You agree to indemnify, defend, and hold each Contributor harmless");
                sb.AppendLine("for any liability incurred by, or claims asserted against such Contributor by");
                sb.AppendLine("the fact You have accepted any warranty or additional liability.");
                sb.AppendLine("");
                sb.AppendLine("10. Acceptance of the Licence");
                sb.AppendLine("");
                sb.AppendLine("The provisions of this Licence can be accepted by clicking on an icon ‘I agree’");
                sb.AppendLine("placed under the bottom of a window displaying the text of this Licence or by");
                sb.AppendLine("affirming consent in any other similar way, in accordance with the rules of");
                sb.AppendLine("applicable law. Clicking on that icon indicates your clear and irrevocable");
                sb.AppendLine("acceptance of this Licence and all of its terms and conditions.");
                sb.AppendLine("");
                sb.AppendLine("Similarly, you irrevocably accept this Licence and all of its terms and");
                sb.AppendLine("conditions by exercising any rights granted to You by Article 2 of this Licence,");
                sb.AppendLine("such as the use of the Work, the creation by You of a Derivative Work or the");
                sb.AppendLine("Distribution or Communication by You of the Work or copies thereof.");
                sb.AppendLine("");
                sb.AppendLine("11. Information to the public");
                sb.AppendLine("");
                sb.AppendLine("In case of any Distribution or Communication of the Work by means of electronic");
                sb.AppendLine("communication by You (for example, by offering to download the Work from a");
                sb.AppendLine("remote location) the distribution channel or media (for example, a website) must");
                sb.AppendLine("at least provide to the public the information requested by the applicable law");
                sb.AppendLine("regarding the Licensor, the Licence and the way it may be accessible, concluded,");
                sb.AppendLine("stored and reproduced by the Licensee.");
                sb.AppendLine("");
                sb.AppendLine("12. Termination of the Licence");
                sb.AppendLine("");
                sb.AppendLine("The Licence and the rights granted hereunder will terminate automatically upon");
                sb.AppendLine("any breach by the Licensee of the terms of the Licence.");
                sb.AppendLine("");
                sb.AppendLine("Such a termination will not terminate the licences of any person who has");
                sb.AppendLine("received the Work from the Licensee under the Licence, provided such persons");
                sb.AppendLine("remain in full compliance with the Licence.");
                sb.AppendLine("");
                sb.AppendLine("13. Miscellaneous");
                sb.AppendLine("");
                sb.AppendLine("Without prejudice of Article 9 above, the Licence represents the complete");
                sb.AppendLine("agreement between the Parties as to the Work.");
                sb.AppendLine("");
                sb.AppendLine("If any provision of the Licence is invalid or unenforceable under applicable");
                sb.AppendLine("law, this will not affect the validity or enforceability of the Licence as a");
                sb.AppendLine("whole. Such provision will be construed or reformed so as necessary to make it");
                sb.AppendLine("valid and enforceable.");
                sb.AppendLine("");
                sb.AppendLine("The European Commission may publish other linguistic versions or new versions of");
                sb.AppendLine("this Licence or updated versions of the Appendix, so far this is required and");
                sb.AppendLine("reasonable, without reducing the scope of the rights granted by the Licence. New");
                sb.AppendLine("versions of the Licence will be published with a unique version number.");
                sb.AppendLine("");
                sb.AppendLine("All linguistic versions of this Licence, approved by the European Commission,");
                sb.AppendLine("have identical value. Parties can take advantage of the linguistic version of");
                sb.AppendLine("their choice.");
                sb.AppendLine("");
                sb.AppendLine("14. Jurisdiction");
                sb.AppendLine("");
                sb.AppendLine("Without prejudice to specific agreement between parties,");
                sb.AppendLine("");
                sb.AppendLine("- any litigation resulting from the interpretation of this License, arising");
                sb.AppendLine("  between the European Union institutions, bodies, offices or agencies, as a");
                sb.AppendLine("  Licensor, and any Licensee, will be subject to the jurisdiction of the Court");
                sb.AppendLine("  of Justice of the European Union, as laid down in article 272 of the Treaty on");
                sb.AppendLine("  the Functioning of the European Union,");
                sb.AppendLine("");
                sb.AppendLine("- any litigation arising between other parties and resulting from the");
                sb.AppendLine("  interpretation of this License, will be subject to the exclusive jurisdiction");
                sb.AppendLine("  of the competent court where the Licensor resides or conducts its primary");
                sb.AppendLine("  business.");
                sb.AppendLine("");
                sb.AppendLine("15. Applicable Law");
                sb.AppendLine("");
                sb.AppendLine("Without prejudice to specific agreement between parties,");
                sb.AppendLine("");
                sb.AppendLine("- this Licence shall be governed by the law of the European Union Member State");
                sb.AppendLine("  where the Licensor has his seat, resides or has his registered office,");
                sb.AppendLine("");
                sb.AppendLine("- this licence shall be governed by Belgian law if the Licensor has no seat,");
                sb.AppendLine("  residence or registered office inside a European Union Member State.");
                sb.AppendLine("");
                sb.AppendLine("Appendix");
                sb.AppendLine("");
                sb.AppendLine("‘Compatible Licences’ according to Article 5 EUPL are:");
                sb.AppendLine("");
                sb.AppendLine("- GNU General Public License (GPL) v. 2, v. 3");
                sb.AppendLine("- GNU Affero General Public License (AGPL) v. 3");
                sb.AppendLine("- Open Software License (OSL) v. 2.1, v. 3.0");
                sb.AppendLine("- Eclipse Public License (EPL) v. 1.0");
                sb.AppendLine("- CeCILL v. 2.0, v. 2.1");
                sb.AppendLine("- Mozilla Public Licence (MPL) v. 2");
                sb.AppendLine("- GNU Lesser General Public Licence (LGPL) v. 2.1, v. 3");
                sb.AppendLine("- Creative Commons Attribution-ShareAlike v. 3.0 Unported (CC BY-SA 3.0) for");
                sb.AppendLine("  works other than software");
                sb.AppendLine("- European Union Public Licence (EUPL) v. 1.1, v. 1.2");
                sb.AppendLine("- Québec Free and Open-Source Licence — Reciprocity (LiLiQ-R) or Strong");
                sb.AppendLine("  Reciprocity (LiLiQ-R+).");
                sb.AppendLine("");
                sb.AppendLine("The European Commission may update this Appendix to later versions of the above");
                sb.AppendLine("licences without producing a new version of the EUPL, as long as they provide");
                sb.AppendLine("the rights granted in Article 2 of this Licence and protect the covered Source");
                sb.AppendLine("Code from exclusive appropriation.");
                sb.AppendLine("");
                sb.AppendLine("All other changes or additions to this Appendix require the production of a new");
                sb.AppendLine("EUPL version.");
                return sb.ToString();
            }
        }

        /// <summary>
        /// <para lang="cs">EUPL text in Czech</para>
        /// <para lang="en">EUPL text in Czech</para>
        /// </summary>
        public static string LicenseTextCs
        {
            get
            {
                StringBuilder sb = new();
                sb.AppendLine("VEŘEJNÁ LICENCE EVROPSKÉ UNIE v. 1.2 ");
                sb.AppendLine("EUPL © Evropská unie 2007, 2016 ");
                sb.AppendLine("Tato veřejná licence Evropské unie („EUPL“) se vztahuje na dílo (ve smyslu níže uvedených definic), které se poskytuje podle podmínek této licence.");
                sb.AppendLine("Použití díla v rozporu s touto licencí je zakázáno (v míře, v jaké se na uvedené použití vztahuje právo držitele autorských práv k dílu). ");
                sb.AppendLine("Dílo se poskytuje podle podmínek této licence, pokud poskytovatel licence (ve smyslu níže uvedených definic) bezprostředně ");
                sb.AppendLine("za doložku o autorských právech k dílu umístil toto upozornění: ");
                sb.AppendLine("                   Na toto dílo se vztahuje licence EUPL ");
                sb.AppendLine("nebo jakýmkoli jiným způsobem vyjádřil úmysl, aby se dílo poskytovalo podle licence EUPL. ");
                sb.AppendLine("");
                sb.AppendLine("1.Definice ");
                sb.AppendLine("V rámci této licence mají následující výrazy tento význam: ");
                sb.AppendLine("—  „Licencí“ se rozumí tato licence. ");
                sb.AppendLine("—  „Původním dílem“ se rozumí dílo nebo software šířené nebo sdělené poskytovatelem licence podle této licence, ");
                sb.AppendLine("dostupné jako zdrojový kód anebo též jako spustitelný kód. ");
                sb.AppendLine("—  „Odvozenými díly“ se rozumí díla nebo software, které by nabyvatel licence mohl vytvořit na základě původního díla nebo jeho úprav. ");
                sb.AppendLine("V této licenci není definováno, jaká musí být míra úprav nebo závislosti na původním díle, aby bylo považováno za odvozené dílo; ");
                sb.AppendLine("tuto míru určuje autorské právo použitelné v zemi podle článku 15. ");
                sb.AppendLine("—  „Dílem“ se rozumí původní dílo nebo díla z něj odvozená. ");
                sb.AppendLine("—  „Zdrojovým kódem“ se rozumí podoba díla čitelná pro člověka, kterou mohou lidé nejpohodlněji analyzovat a upravovat. ");
                sb.AppendLine("—  „Spustitelným kódem“ se rozumí takový kód, který byl obecně sestaven a jenž má být interpretován počítačem jako program. ");
                sb.AppendLine("—  „Poskytovatelem licence“ se rozumí fyzická nebo právnická osoba, která dílo podle licence šíří nebo sděluje. ");
                sb.AppendLine("—  „Přispěvatelem“ se rozumí fyzická nebo právnická osoba, jež dílo podle licence upraví nebo jinak přispěje k vytvoření odvozeného díla. ");
                sb.AppendLine("—  „Nabyvatelem licence“ nebo „Vámi“ se rozumí fyzická nebo právnická osoba, která používá dílo podle podmínek licence. ");
                sb.AppendLine("—  „Šířením“ nebo „sdělováním“ se rozumí prodej, předání, zapůjčení, pronájem, šíření, sdělování, přenos nebo jiný způsob zpřístupnění kopií díla ");
                sb.AppendLine("nebo zpřístupnění jeho podstatných funkcí jakékoli fyzické nebo právnické osobě, a to online nebo offline. ");
                sb.AppendLine("");
                sb.AppendLine("2.Rozsah práv udělených licencí ");
                sb.AppendLine("Poskytovatel licence Vám tímto po dobu trvání autorských práv k původnímu dílu udílí celosvětovou, bezúplatnou, ");
                sb.AppendLine("nevýhradní licenci s možností poskytování podlicence, k těmto činnostem: ");
                sb.AppendLine("—  používání díla za jakýchkoli okolností a pro veškerá použití, ");
                sb.AppendLine("—  rozmnožování díla, ");
                sb.AppendLine("—  úpravě díla a vytvoření odvozených děl na základě díla, ");
                sb.AppendLine("—  sdělování veřejnosti, včetně práva zpřístupnit nebo zobrazit dílo nebo jeho kopie veřejnosti, popřípadě veřejně dílo provozovat, ");
                sb.AppendLine("—  šíření díla nebo jeho kopií, ");
                sb.AppendLine("—  zapůjčení a pronájmu díla nebo jeho kopií, ");
                sb.AppendLine("—  poskytování podlicencí k dílu nebo jeho kopiím. ");
                sb.AppendLine("Uvedená práva lze uplatnit na všechna média, nosiče a formáty, které jsou v současnosti známé nebo budou v budoucnu vynalezeny, ");
                sb.AppendLine("do té míry, do jaké to připouští rozhodné právo. ");
                sb.AppendLine("V zemích, kde se uplatňují osobnostní práva, se poskytovatel licence vzdává práva vykonávat své osobnostní právo v rozsahu, ");
                sb.AppendLine("v jakém to rozhodné právo umožňuje, aby tak byl umožněn účinný výkon majetkových práv výše uvedených. ");
                sb.AppendLine("Poskytovatel licence v míře nezbytné k využívání práv udělených k dílu podle této licence udílí nabyvateli licence bezúplatná, ");
                sb.AppendLine("nevýlučná práva na využití případných patentů náležejících poskytovateli licence. ");
                sb.AppendLine("");
                sb.AppendLine("3.Sdělování zdrojového kódu ");
                sb.AppendLine("Poskytovatel licence může poskytnout dílo buď v podobě zdrojového kódu, nebo jako spustitelný kód. ");
                sb.AppendLine("Pokud se dílo poskytuje v podobě spustitelného kódu, poskytovatel licence navíc společně s každou kopií díla, ");
                sb.AppendLine("kterou šíří nebo na ni odkazuje, poskytne strojově čitelné vyhotovení zdrojového kódu díla; ");
                sb.AppendLine("může též v závěru doložky o autorských plánech odkázat na umístění, kde se po dobu, kdy poskytovatel licence šíří nebo sděluje dílo, ");
                sb.AppendLine("nachází snadno a volně dostupný zdrojový kód. ");
                sb.AppendLine("");
                sb.AppendLine("4.Omezení autorských práv ");
                sb.AppendLine("Žádným ustanovením této licence nemůže být nabyvatel licence zbaven výhod z případných výjimek z výlučných práv majitelů práv k dílu ");
                sb.AppendLine("nebo případného omezení těchto výlučných práv, nebo z vyčerpání uvedených práv nebo z jiných relevantních omezení vztahujících se k takovým právům.");
                sb.AppendLine(" ");
                sb.AppendLine("5.Závazky nabyvatele licence ");
                sb.AppendLine("Udělení uvedených práv podléhá určitým omezením a závazkům vůči nabyvateli licence. Jedná se o tyto závazky:");
                sb.AppendLine(" ");
                sb.AppendLine("Právo na označení: Nabyvatel licence zachová beze změny veškerá oznámení týkající se autorských práv, ");
                sb.AppendLine("patentů či ochranných známek a veškerá oznámení odkazující na licenci a na vyloučení záruk. ");
                sb.AppendLine("Kopii takových oznámení a kopii licence musí nabyvatel licence připojit ke každé kopii díla, kterou šíří nebo sděluje. ");
                sb.AppendLine("Nabyvatel licence musí zajistit, aby odvozené dílo bylo opatřeno výrazným oznámením, že původní dílo bylo upraveno, a též datem úpravy. ");
                sb.AppendLine("");
                sb.AppendLine("Ustanovení o nezměnitelnosti podmínek (tzv. copyleft clause): Pokud nabyvatel licence šíří nebo sděluje kopie původních děl nebo odvozených děl, ");
                sb.AppendLine("uvedené šíření nebo sdělování bude probíhat podle podmínek této licence nebo podle pozdější verze této licence, ");
                sb.AppendLine("není-li původní dílo výslovně šířeno pouze pod touto verzí licence – např. označením „výlučně EUPL v. 1.2“. ");
                sb.AppendLine("Nabyvatel licence (který se stává poskytovatelem licence) nemůže v souvislosti s dílem nebo odvozeným dílem nabízet ");
                sb.AppendLine("nebo ukládat žádné dodatečné podmínky, které by měnily nebo omezovaly podmínky licence.");
                sb.AppendLine("");
                sb.AppendLine("Ustanovení o slučitelnosti: Pokud nabyvatel licence šíří nebo sděluje odvozená díla nebo jejich kopie na základě jak díla, tak jiného díla, ");
                sb.AppendLine("na které se vztahuje slučitelná licence, lze tak činit podle podmínek uvedené slučitelné licence. ");
                sb.AppendLine("Pro účely tohoto ustanovení se „slučitelnou licencí“ rozumí licence uvedené v dodatku této licence. ");
                sb.AppendLine("Jsou-li závazky nabyvatele licence podle slučitelné licence v rozporu s jeho závazky podle této licence, ");
                sb.AppendLine("mají přednost závazky dle slučitelné licence. ");
                sb.AppendLine("");
                sb.AppendLine("Poskytování zdrojového kódu: Při šíření nebo sdělování kopií díla nabyvatel licence poskytne strojově čitelné znění zdrojového kódu ");
                sb.AppendLine("nebo odkáže na umístění, kde po dobu, kdy nabyvatel licence pokračuje v šíření nebo sdělování díla, je uvedený zdrojový kód snadno a volně přístupný. "); ;
                sb.AppendLine("");
                sb.AppendLine("Právní ochrana: Touto licencí se nepovoluje použít obchodní názvy, ochranné známky pro výrobky a služby nebo názvy poskytovatele licence, ");
                sb.AppendLine("s výjimkou případů, kdy tento požadavek vzniká s ohledem na přiměřené a obvyklé použití při popisu původu díla ");
                sb.AppendLine("a při reprodukci obsahu doložky o autorských právech. ");
                sb.AppendLine("");
                sb.AppendLine("6.Řetězec autorství ");
                sb.AppendLine("Původní poskytovatel licence prohlašuje, že je majitelem autorských práv k původnímu dílu, jež jsou předmětem této licence, ");
                sb.AppendLine("nebo že mu k nim byla udělena licence a že má pravomoc a oprávnění licenci udělit. ");
                sb.AppendLine("Každý přispěvatel prohlašuje, že je majitelem autorských práv k úpravám, kterými dílo mění, ");
                sb.AppendLine("nebo že mu k nim byla udělena licence a že má pravomoc a oprávnění licenci udělit. ");
                sb.AppendLine("Kdykoli přijmete licenci, původní poskytovatel licence a následní přispěvatelé Vám udílejí licenci ");
                sb.AppendLine("ke svým příspěvkům k dílu podle podmínek této licence. ");
                sb.AppendLine("");
                sb.AppendLine("7.Vyloučení záruky ");
                sb.AppendLine("Dílo se chápe jako dílo, které se stále vyvíjí a které četní přispěvatelé průběžně zdokonalují. ");
                sb.AppendLine("Nejedná se o hotové dílo, a může proto obsahovat vady či chyby v programu, které se při takovémto způsobu vývoje běžně vyskytují. ");
                sb.AppendLine("Z uvedeného důvodu se dílo podle licence poskytuje v podobě tak, jak je, a bez jakýchkoli záruk k dílu, včetně, mimo jiné, obchodovatelnosti, ");
                sb.AppendLine("vhodnosti pro konkrétní účel, bezchybnosti a nechybovosti, přesnosti, neporušení práv duševního vlastnictví vyjma práva autorského, ");
                sb.AppendLine("jak je uvedeno v článku 6 této licence. Toto odmítnutí záruky je nedílnou součástí licence a podmínkou pro udělení jakýchkoli práv k dílu. ");
                sb.AppendLine("");
                sb.AppendLine("8.Vyloučení odpovědnosti ");
                sb.AppendLine("S výjimkou případů úmyslného pochybení nebo škod přímo způsobených fyzickým osobám nenese poskytovatel licence v žádném případě odpovědnost ");
                sb.AppendLine("za jakékoli přímé nebo nepřímé škody, hmotnou nebo mravní újmu či jakékoli jiné škody vyplývající z licence nebo použití díla, ");
                sb.AppendLine("včetně škod způsobených ztrátou dobré pověsti, přerušením práce, poruchou nebo nesprávným fungováním počítače, ");
                sb.AppendLine("ztrátou dat nebo jakýchkoli škod komerční povahy, a to i tehdy, pokud poskytovatel licence byl na možnost takové škody upozorněn. ");
                sb.AppendLine("Poskytovatel licence však ponese odpovědnost podle zákonů týkajících se odpovědnosti za škodu způsobenou vadou výrobku v míře, ");
                sb.AppendLine("v jaké se takové zákony na dané dílo vztahují. ");
                sb.AppendLine("");
                sb.AppendLine("9.Dodatečné dohody ");
                sb.AppendLine("Při rozšiřování díla se můžete rozhodnout uzavřít dodatečné dohody, v nichž se vymezí povinnosti nebo služby v souladu s touto licencí. ");
                sb.AppendLine("Pokud však na sebe vezmete závazky, můžete jednat pouze vlastním jménem a výhradně na vlastní odpovědnost, ");
                sb.AppendLine("nikoli jménem původního poskytovatele licence či jakéhokoli dalšího přispěvatele, a pouze pokud souhlasíte s tím, ");
                sb.AppendLine("že každého přispěvatele odškodníte, budete hájit a uchráníte před škodou nebo uplatňovanými nároky, ");
                sb.AppendLine("pokud jde o jakoukoli odpovědnost vzniklou uvedenému přispěvateli v důsledku skutečnosti, že jste přijali záruku nebo dodatečnou odpovědnost. ");
                sb.AppendLine("");
                sb.AppendLine("10.Přijetí licence ");
                sb.AppendLine("S ustanoveními této licence lze vyjádřit souhlas kliknutím na ikonu „Souhlasím“ („I agree“) umístěnou pod oknem, ");
                sb.AppendLine("kde je zobrazeno znění této licence, nebo vyjádřením souhlasu obdobným způsobem podle rozhodného práva. ");
                sb.AppendLine("Kliknutím na uvedenou ikonu vyjadřujete zřejmý a neodvolatelný souhlas s touto licencí a veškerými jejími podmínkami. ");
                sb.AppendLine("Obdobně tuto licenci a veškeré její podmínky neodvolatelně přijímáte, pokud vykonáváte práva, ");
                sb.AppendLine("která jsou Vám poskytnuta v souladu s článkem 2 této licence, např. pokud dílo užíváte, vytváříte odvozené dílo nebo dílo ");
                sb.AppendLine("nebo jeho kopie šíříte nebo sdělujete. ");
                sb.AppendLine("");
                sb.AppendLine("11.Informování veřejnosti ");
                sb.AppendLine("Pokud dílo šíříte nebo sdělujete prostřednictvím elektronické komunikace (např. nabízíte stažení díla ze vzdáleného umístění), ");
                sb.AppendLine("musí příslušný distribuční kanál nebo média (např. internetové stránky) uvádět alespoň takové informace určené veřejnosti, ");
                sb.AppendLine("které jsou požadovány rozhodným právem ohledně poskytovatele licence, licence samotné a způsobu jejího zpřístupnění, uzavření, ");
                sb.AppendLine("uchovávání a reprodukování nabyvatelem licence.");
                sb.AppendLine("");
                sb.AppendLine("12.Zánik licence ");
                sb.AppendLine("Platnost licence a práv touto licencí udělených bez dalšího zaniká, pokud nabyvatel licence poruší jakýmkoli způsobem podmínky licence. ");
                sb.AppendLine("Uvedeným zánikem není ukončena licence jakékoli osoby, která podle licence dílo obdržela od nabyvatele licence, za podmínky, ");
                sb.AppendLine("že takové osoby nadále plně jednají v souladu s podmínkami licence. ");
                sb.AppendLine("");
                sb.AppendLine("13.Různé ");
                sb.AppendLine("Aniž je dotčen výše uvedený článek 9, představuje licence úplnou dohodu mezi stranami, pokud jde o dílo. ");
                sb.AppendLine("Je-li podle rozhodného práva jakékoli ustanovení této licence neplatné nebo nevymahatelné, ");
                sb.AppendLine("neovlivní tato skutečnost platnost nebo vymahatelnost licence jako celku. ");
                sb.AppendLine("Takové ustanovení se vyloží nebo podle potřeby upraví tak, aby bylo platné a vymahatelné. ");
                sb.AppendLine("Evropská komise může v požadované a přiměřené míře zveřejnit další jazykové verze nebo nové verze této licence nebo aktualizované verze dodatku, ");
                sb.AppendLine("aniž by omezila rozsah práv udělených licencí. Novým verzím bude při zveřejnění přiděleno jedinečné číslo verze. ");
                sb.AppendLine("Všechny jazykové verze této licence schválené Evropskou komisí mají stejnou platnost. Strany mohou využít jazykové verze dle svého výběru. ");
                sb.AppendLine("");
                sb.AppendLine("14.Soudní příslušnost ");
                sb.AppendLine("Aniž jsou dotčeny zvláštní dohody mezi stranami, ");
                sb.AppendLine("—  soudní spor, který by popřípadě vyplynul z výkladu této licence mezi orgány, ");
                sb.AppendLine("úřady nebo agenturami Evropské unie jakožto poskytovateli licence a jakýmkoli nabyvatelem licence, ");
                sb.AppendLine("se bude řídit pravomocí Soudního dvora Evropské unie, jak stanoví článek 272 Smlouvy o fungování Evropské unie, ");
                sb.AppendLine("—  soudní spor, který by popřípadě vyplynul z výkladu této licence mezi jinými stranami, se bude řídit výlučnou pravomocí příslušného soudu, ");
                sb.AppendLine("kde má poskytovatel licence bydliště nebo vykonává hlavní obchodní činnost. ");
                sb.AppendLine("");
                sb.AppendLine("15.Rozhodné právo ");
                sb.AppendLine("Aniž jsou dotčeny zvláštní dohody mezi stranami, ");
                sb.AppendLine("—  tato licence se řídí právem toho členského státu Evropské unie, kde má poskytovatel licence své sídlo, bydliště nebo sídlo podnikání, ");
                sb.AppendLine("—  pokud poskytovatel licence nemá sídlo, bydliště nebo sídlo podnikání v některém členském státě Evropské unie, řídí se tato licence belgickým právem.");
                sb.AppendLine("    ");
                sb.AppendLine("Dodatek ");
                sb.AppendLine("„Slučitelnými licencemi“ podle článku 5 licence EUPL se rozumí: ");
                sb.AppendLine("—  GNU General Public License (GPL) v. 2, v. 3 ");
                sb.AppendLine("—  GNU Affero General Public License (AGPL) v. 3 ");
                sb.AppendLine("—  Open Software License (OSL) v. 2.1, v. 3.0 ");
                sb.AppendLine("—  Eclipse Public License (EPL) v. 1.0 ");
                sb.AppendLine("—  CeCILL v. 2.0, v. 2.1 ");
                sb.AppendLine("—  Mozilla Public Licence (MPL) v. 2 ");
                sb.AppendLine("—  GNU Lesser General Public Licence (LGPL) v. 2.1, v. 3 ");
                sb.AppendLine("—  Creative Commons Attribution-ShareAlike v. 3.0 Unported (CC BY-SA 3.0) pro díla jiná než software ");
                sb.AppendLine("—  veřejná licence Evropské unie (EUPL) v. 1.1, v. 1.2 ");
                sb.AppendLine("—  Québec Free and Open-Source Licence – Reciprocity (LiLiQ-R) or Strong Reciprocity (LiLiQ-R+)");
                sb.AppendLine("Evropská komise může tento dodatek aktualizovat, aby zahrnoval pozdější verze uvedených licencí, bez zavedení nové verze EUPL, ");
                sb.AppendLine("pokud uvedené licence zajišťují práva udělená článkem 2 této licence a chrání zdrojový kód, který je jejím předmětem, ");
                sb.AppendLine("před výhradním přivlastněním.");
                sb.AppendLine("Všechny ostatní změny nebo doplnění k tomuto dodatku vyžadují novou verzi EUPL.");

                return sb.ToString();
            }
        }

        /// <summary>
        /// <para lang="cs">License accepted</para>
        /// <para lang="en">License accepted</para>
        /// </summary>
        public bool LicenseAccepted
        {
            get
            {
                return rdoAccept.Checked;
            }
            set
            {
                rdoAccept.Checked = value;
                rdoDecline.Checked = !value;
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(FormLicense),      "Licenční smlouva" },
                        { nameof(btnOK),            "OK" },
                        { nameof(lblCaption),       "Licenční smlouva" },
                        { nameof(rdoAccept),        "Souhlasím s podmínkami licenční smlouvy" },
                        { nameof(rdoDecline),       "Nesouhlasím s podmínkami licenční smlouvy" }
                    }
                    : languageFile.NationalVersion.MainApplicationDictionary.TryGetValue(
                        key: nameof(FormLicense),
                        out Dictionary<string, string> dictNational)
                            ? dictNational
                            : [],

                LanguageVersion.International => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(FormLicense),      "License Agreement" },
                        { nameof(btnOK),            "OK" },
                        { nameof(lblCaption),       "License Agreement" },
                        { nameof(rdoAccept),        "I accept the terms in the license agreement" },
                        { nameof(rdoDecline),       "I do not accept the terms in the licence agreement" }
                    }
                    : languageFile.InternationalVersion.MainApplicationDictionary.TryGetValue(
                        key: nameof(FormLicense),
                        out Dictionary<string, string> dictInternational)
                            ? dictInternational
                            : [],

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace formuláře</para>
        /// <para lang="en">Initializing the form</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        private void Initialize(Control controlOwner)
        {
            ControlOwner = controlOwner;

            InitializeLabels();

            linkEupl.LinkClicked += new LinkLabelLinkClickedEventHandler(
                (sender, e) =>
                {
                    linkEupl.LinkVisited = true;
                    System.Diagnostics.Process.Start("https://joinup.ec.europa.eu/software/page/eupl");
                });

            btnOK.Click += new EventHandler(
                (sender, e) =>
                {
                    DialogResult = DialogResult.OK;
                    Close();
                });

            btnOK.Focus();
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků formuláře</para>
        /// <para lang="en">Initializing form labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            Text =
                labels.TryGetValue(key: nameof(FormLicense),
                    out string formLicenseText)
                        ? formLicenseText
                        : String.Empty;

            lblCaption.Text =
              labels.TryGetValue(key: nameof(lblCaption),
                    out string lblCaptionText)
                        ? lblCaptionText
                        : String.Empty;

            lblLicense.Text =
                (LanguageVersion == LanguageVersion.International)
                    ? LicenseTextEn
                    : (LanguageVersion == LanguageVersion.National)
                        ? LicenseTextCs
                        : LicenseTextEn;

            rdoAccept.Text =
               labels.TryGetValue(key: nameof(rdoAccept),
                    out string rdoAcceptText)
                        ? rdoAcceptText
                        : String.Empty;

            rdoDecline.Text =
               labels.TryGetValue(key: nameof(rdoDecline),
                    out string rdoDeclineText)
                        ? rdoDeclineText
                        : String.Empty;

            btnOK.Text =
               labels.TryGetValue(key: nameof(btnOK),
                    out string btnOKText)
                        ? btnOKText
                        : String.Empty;
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent() { }

        #endregion Methods

    }

}
