﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace NfiEstaApp
{
    partial class FormMain
    {
        /// <summary>
        /// Vyžaduje se proměnná návrháře.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Uvolněte všechny používané prostředky.
        /// </summary>
        /// <param name="disposing">hodnota true, když by se měl spravovaný prostředek odstranit; jinak false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kód generovaný Návrhářem Windows Form

        /// <summary>
        /// Metoda vyžadovaná pro podporu Návrháře - neupravovat
        /// obsah této metody v editoru kódu.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            tlpModules = new System.Windows.Forms.TableLayoutPanel();
            pnlHeading = new System.Windows.Forms.Panel();
            btnModuleETL = new System.Windows.Forms.Button();
            btnInfo = new System.Windows.Forms.Button();
            btnSettings = new System.Windows.Forms.Button();
            btnModuleEstimate = new System.Windows.Forms.Button();
            btnExit = new System.Windows.Forms.Button();
            btnModuleAuxiliaryData = new System.Windows.Forms.Button();
            btnModuleTargetData = new System.Windows.Forms.Button();
            pnlModules = new System.Windows.Forms.Panel();
            pnlFooter = new System.Windows.Forms.Panel();
            btnLicense = new System.Windows.Forms.Button();
            lblAssemblyVersion = new System.Windows.Forms.Label();
            tipInfo = new System.Windows.Forms.ToolTip(components);
            tipSettings = new System.Windows.Forms.ToolTip(components);
            tipExit = new System.Windows.Forms.ToolTip(components);
            tlpModules.SuspendLayout();
            pnlHeading.SuspendLayout();
            pnlFooter.SuspendLayout();
            SuspendLayout();
            // 
            // tlpModules
            // 
            tlpModules.BackColor = System.Drawing.Color.Transparent;
            tlpModules.ColumnCount = 1;
            tlpModules.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpModules.Controls.Add(pnlHeading, 0, 0);
            tlpModules.Controls.Add(pnlModules, 0, 1);
            tlpModules.Controls.Add(pnlFooter, 0, 2);
            tlpModules.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpModules.Location = new System.Drawing.Point(0, 0);
            tlpModules.Margin = new System.Windows.Forms.Padding(0);
            tlpModules.Name = "tlpModules";
            tlpModules.RowCount = 3;
            tlpModules.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            tlpModules.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpModules.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            tlpModules.Size = new System.Drawing.Size(1264, 761);
            tlpModules.TabIndex = 3;
            // 
            // pnlHeading
            // 
            pnlHeading.BackColor = System.Drawing.Color.DarkSeaGreen;
            pnlHeading.Controls.Add(btnModuleETL);
            pnlHeading.Controls.Add(btnInfo);
            pnlHeading.Controls.Add(btnSettings);
            pnlHeading.Controls.Add(btnModuleEstimate);
            pnlHeading.Controls.Add(btnExit);
            pnlHeading.Controls.Add(btnModuleAuxiliaryData);
            pnlHeading.Controls.Add(btnModuleTargetData);
            pnlHeading.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlHeading.Location = new System.Drawing.Point(0, 0);
            pnlHeading.Margin = new System.Windows.Forms.Padding(0);
            pnlHeading.Name = "pnlHeading";
            pnlHeading.Size = new System.Drawing.Size(1264, 45);
            pnlHeading.TabIndex = 0;
            // 
            // btnModuleETL
            // 
            btnModuleETL.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            btnModuleETL.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            btnModuleETL.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            btnModuleETL.Location = new System.Drawing.Point(185, 5);
            btnModuleETL.Margin = new System.Windows.Forms.Padding(0);
            btnModuleETL.Name = "btnModuleETL";
            btnModuleETL.Size = new System.Drawing.Size(175, 35);
            btnModuleETL.TabIndex = 6;
            btnModuleETL.Text = "btnModuleETL";
            btnModuleETL.UseVisualStyleBackColor = true;
            // 
            // btnInfo
            // 
            btnInfo.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right;
            btnInfo.BackColor = System.Drawing.Color.Transparent;
            btnInfo.BackgroundImage = (System.Drawing.Image)resources.GetObject("btnInfo.BackgroundImage");
            btnInfo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            btnInfo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            btnInfo.Location = new System.Drawing.Point(1140, 5);
            btnInfo.Margin = new System.Windows.Forms.Padding(0);
            btnInfo.Name = "btnInfo";
            btnInfo.Size = new System.Drawing.Size(35, 35);
            btnInfo.TabIndex = 5;
            btnInfo.UseVisualStyleBackColor = false;
            // 
            // btnSettings
            // 
            btnSettings.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right;
            btnSettings.BackColor = System.Drawing.Color.Transparent;
            btnSettings.BackgroundImage = (System.Drawing.Image)resources.GetObject("btnSettings.BackgroundImage");
            btnSettings.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            btnSettings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            btnSettings.Location = new System.Drawing.Point(1180, 5);
            btnSettings.Margin = new System.Windows.Forms.Padding(0);
            btnSettings.Name = "btnSettings";
            btnSettings.Size = new System.Drawing.Size(35, 35);
            btnSettings.TabIndex = 4;
            btnSettings.UseVisualStyleBackColor = false;
            // 
            // btnModuleEstimate
            // 
            btnModuleEstimate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            btnModuleEstimate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            btnModuleEstimate.Location = new System.Drawing.Point(545, 5);
            btnModuleEstimate.Margin = new System.Windows.Forms.Padding(0);
            btnModuleEstimate.Name = "btnModuleEstimate";
            btnModuleEstimate.Size = new System.Drawing.Size(175, 35);
            btnModuleEstimate.TabIndex = 4;
            btnModuleEstimate.Text = "btnModuleEstimate";
            btnModuleEstimate.UseVisualStyleBackColor = true;
            // 
            // btnExit
            // 
            btnExit.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right;
            btnExit.BackColor = System.Drawing.Color.Transparent;
            btnExit.BackgroundImage = (System.Drawing.Image)resources.GetObject("btnExit.BackgroundImage");
            btnExit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            btnExit.Location = new System.Drawing.Point(1220, 5);
            btnExit.Margin = new System.Windows.Forms.Padding(0);
            btnExit.Name = "btnExit";
            btnExit.Size = new System.Drawing.Size(35, 35);
            btnExit.TabIndex = 3;
            btnExit.UseVisualStyleBackColor = false;
            // 
            // btnModuleAuxiliaryData
            // 
            btnModuleAuxiliaryData.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            btnModuleAuxiliaryData.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            btnModuleAuxiliaryData.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            btnModuleAuxiliaryData.Location = new System.Drawing.Point(365, 5);
            btnModuleAuxiliaryData.Margin = new System.Windows.Forms.Padding(0);
            btnModuleAuxiliaryData.Name = "btnModuleAuxiliaryData";
            btnModuleAuxiliaryData.Size = new System.Drawing.Size(175, 35);
            btnModuleAuxiliaryData.TabIndex = 3;
            btnModuleAuxiliaryData.Text = "btnModuleAuxiliaryData";
            btnModuleAuxiliaryData.UseVisualStyleBackColor = true;
            // 
            // btnModuleTargetData
            // 
            btnModuleTargetData.BackColor = System.Drawing.Color.DarkSeaGreen;
            btnModuleTargetData.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            btnModuleTargetData.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            btnModuleTargetData.Location = new System.Drawing.Point(5, 5);
            btnModuleTargetData.Margin = new System.Windows.Forms.Padding(0);
            btnModuleTargetData.Name = "btnModuleTargetData";
            btnModuleTargetData.Size = new System.Drawing.Size(175, 35);
            btnModuleTargetData.TabIndex = 2;
            btnModuleTargetData.Text = "btnModuleTargetData";
            btnModuleTargetData.UseVisualStyleBackColor = false;
            // 
            // pnlModules
            // 
            pnlModules.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlModules.Location = new System.Drawing.Point(0, 45);
            pnlModules.Margin = new System.Windows.Forms.Padding(0);
            pnlModules.Name = "pnlModules";
            pnlModules.Padding = new System.Windows.Forms.Padding(5);
            pnlModules.Size = new System.Drawing.Size(1264, 686);
            pnlModules.TabIndex = 1;
            // 
            // pnlFooter
            // 
            pnlFooter.BackColor = System.Drawing.Color.DarkSeaGreen;
            pnlFooter.Controls.Add(btnLicense);
            pnlFooter.Controls.Add(lblAssemblyVersion);
            pnlFooter.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlFooter.Location = new System.Drawing.Point(0, 731);
            pnlFooter.Margin = new System.Windows.Forms.Padding(0);
            pnlFooter.Name = "pnlFooter";
            pnlFooter.Size = new System.Drawing.Size(1264, 30);
            pnlFooter.TabIndex = 2;
            // 
            // btnLicense
            // 
            btnLicense.Dock = System.Windows.Forms.DockStyle.Left;
            btnLicense.FlatAppearance.BorderSize = 0;
            btnLicense.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            btnLicense.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            btnLicense.ForeColor = System.Drawing.Color.Blue;
            btnLicense.Location = new System.Drawing.Point(0, 0);
            btnLicense.Margin = new System.Windows.Forms.Padding(0);
            btnLicense.Name = "btnLicense";
            btnLicense.Size = new System.Drawing.Size(88, 30);
            btnLicense.TabIndex = 1;
            btnLicense.Text = "EUPL v. 1.2";
            btnLicense.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            btnLicense.UseVisualStyleBackColor = true;
            // 
            // lblAssemblyVersion
            // 
            lblAssemblyVersion.AutoSize = true;
            lblAssemblyVersion.Dock = System.Windows.Forms.DockStyle.Right;
            lblAssemblyVersion.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, 0);
            lblAssemblyVersion.Location = new System.Drawing.Point(1264, 0);
            lblAssemblyVersion.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            lblAssemblyVersion.Name = "lblAssemblyVersion";
            lblAssemblyVersion.Size = new System.Drawing.Size(0, 14);
            lblAssemblyVersion.TabIndex = 0;
            // 
            // FormMain
            // 
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            BackColor = System.Drawing.Color.WhiteSmoke;
            ClientSize = new System.Drawing.Size(1264, 761);
            Controls.Add(tlpModules);
            ForeColor = System.Drawing.Color.Black;
            Icon = (System.Drawing.Icon)resources.GetObject("$this.Icon");
            KeyPreview = true;
            Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            Name = "FormMain";
            StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            tlpModules.ResumeLayout(false);
            pnlHeading.ResumeLayout(false);
            pnlFooter.ResumeLayout(false);
            pnlFooter.PerformLayout();
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlpModules;
        private System.Windows.Forms.Panel pnlHeading;
        private System.Windows.Forms.Panel pnlModules;
        private System.Windows.Forms.Button btnModuleEstimate;
        private System.Windows.Forms.Button btnModuleAuxiliaryData;
        private System.Windows.Forms.Button btnModuleTargetData;
        private System.Windows.Forms.Panel pnlFooter;
        private System.Windows.Forms.Button btnSettings;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Label lblAssemblyVersion;
        private System.Windows.Forms.Button btnInfo;
        private System.Windows.Forms.ToolTip tipInfo;
        private System.Windows.Forms.ToolTip tipSettings;
        private System.Windows.Forms.ToolTip tipExit;
        private System.Windows.Forms.Button btnModuleETL;
        private System.Windows.Forms.Button btnLicense;
    }
}

