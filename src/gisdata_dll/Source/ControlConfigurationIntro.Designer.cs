﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi.ModuleAuxiliaryData
{

    partial class ControlConfigurationIntro
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            tlpMain = new System.Windows.Forms.TableLayoutPanel();
            pnlButtons = new System.Windows.Forms.Panel();
            tlpButtons = new System.Windows.Forms.TableLayoutPanel();
            pnlNext = new System.Windows.Forms.Panel();
            btnNext = new System.Windows.Forms.Button();
            tlpParams = new System.Windows.Forms.TableLayoutPanel();
            grpConfigurationLabelCs = new System.Windows.Forms.GroupBox();
            txtConfigurationLabelCs = new System.Windows.Forms.TextBox();
            grpConfigurationDescriptionCs = new System.Windows.Forms.GroupBox();
            txtConfigurationDescriptionCs = new System.Windows.Forms.TextBox();
            pnlAuxiliaryVariableCategory = new System.Windows.Forms.Panel();
            grpConfigQuery = new System.Windows.Forms.GroupBox();
            rdoQueryVar500 = new System.Windows.Forms.RadioButton();
            rdoQueryVar400 = new System.Windows.Forms.RadioButton();
            rdoQueryVar300 = new System.Windows.Forms.RadioButton();
            rdoQueryVar200 = new System.Windows.Forms.RadioButton();
            rdoQueryVar100 = new System.Windows.Forms.RadioButton();
            pnlParams = new System.Windows.Forms.Panel();
            grpConfigurationLabelEn = new System.Windows.Forms.GroupBox();
            txtConfigurationLabelEn = new System.Windows.Forms.TextBox();
            grpConfigurationDescriptionEn = new System.Windows.Forms.GroupBox();
            txtConfigurationDescriptionEn = new System.Windows.Forms.TextBox();
            tlpMain.SuspendLayout();
            pnlButtons.SuspendLayout();
            tlpButtons.SuspendLayout();
            pnlNext.SuspendLayout();
            tlpParams.SuspendLayout();
            grpConfigurationLabelCs.SuspendLayout();
            grpConfigurationDescriptionCs.SuspendLayout();
            grpConfigQuery.SuspendLayout();
            pnlParams.SuspendLayout();
            grpConfigurationLabelEn.SuspendLayout();
            grpConfigurationDescriptionEn.SuspendLayout();
            SuspendLayout();
            // 
            // tlpMain
            // 
            tlpMain.ColumnCount = 1;
            tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.Controls.Add(pnlButtons, 0, 1);
            tlpMain.Controls.Add(pnlParams, 0, 0);
            tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpMain.Location = new System.Drawing.Point(0, 0);
            tlpMain.Margin = new System.Windows.Forms.Padding(0);
            tlpMain.Name = "tlpMain";
            tlpMain.RowCount = 2;
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpMain.Size = new System.Drawing.Size(960, 540);
            tlpMain.TabIndex = 4;
            // 
            // pnlButtons
            // 
            pnlButtons.Controls.Add(tlpButtons);
            pnlButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlButtons.Location = new System.Drawing.Point(0, 500);
            pnlButtons.Margin = new System.Windows.Forms.Padding(0);
            pnlButtons.Name = "pnlButtons";
            pnlButtons.Size = new System.Drawing.Size(960, 40);
            pnlButtons.TabIndex = 8;
            // 
            // tlpButtons
            // 
            tlpButtons.BackColor = System.Drawing.Color.Transparent;
            tlpButtons.ColumnCount = 3;
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            tlpButtons.Controls.Add(pnlNext, 2, 0);
            tlpButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpButtons.Location = new System.Drawing.Point(0, 0);
            tlpButtons.Margin = new System.Windows.Forms.Padding(0);
            tlpButtons.Name = "tlpButtons";
            tlpButtons.RowCount = 1;
            tlpButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.Size = new System.Drawing.Size(960, 40);
            tlpButtons.TabIndex = 14;
            // 
            // pnlNext
            // 
            pnlNext.Controls.Add(btnNext);
            pnlNext.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlNext.Location = new System.Drawing.Point(800, 0);
            pnlNext.Margin = new System.Windows.Forms.Padding(0);
            pnlNext.Name = "pnlNext";
            pnlNext.Padding = new System.Windows.Forms.Padding(5);
            pnlNext.Size = new System.Drawing.Size(160, 40);
            pnlNext.TabIndex = 13;
            // 
            // btnNext
            // 
            btnNext.Dock = System.Windows.Forms.DockStyle.Fill;
            btnNext.Enabled = false;
            btnNext.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            btnNext.Location = new System.Drawing.Point(5, 5);
            btnNext.Margin = new System.Windows.Forms.Padding(0);
            btnNext.Name = "btnNext";
            btnNext.Size = new System.Drawing.Size(150, 30);
            btnNext.TabIndex = 9;
            btnNext.Text = "btnNext";
            btnNext.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            btnNext.UseVisualStyleBackColor = true;
            // 
            // tlpParams
            // 
            tlpParams.ColumnCount = 1;
            tlpParams.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpParams.Controls.Add(grpConfigurationDescriptionEn, 0, 3);
            tlpParams.Controls.Add(grpConfigurationLabelEn, 0, 1);
            tlpParams.Controls.Add(grpConfigurationLabelCs, 0, 0);
            tlpParams.Controls.Add(pnlAuxiliaryVariableCategory, 0, 5);
            tlpParams.Controls.Add(grpConfigQuery, 0, 4);
            tlpParams.Controls.Add(grpConfigurationDescriptionCs, 0, 2);
            tlpParams.Dock = System.Windows.Forms.DockStyle.Top;
            tlpParams.Location = new System.Drawing.Point(0, 0);
            tlpParams.Margin = new System.Windows.Forms.Padding(0);
            tlpParams.Name = "tlpParams";
            tlpParams.RowCount = 7;
            tlpParams.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            tlpParams.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            tlpParams.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            tlpParams.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            tlpParams.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 180F));
            tlpParams.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            tlpParams.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpParams.Size = new System.Drawing.Size(943, 540);
            tlpParams.TabIndex = 9;
            // 
            // grpConfigurationLabelCs
            // 
            grpConfigurationLabelCs.Controls.Add(txtConfigurationLabelCs);
            grpConfigurationLabelCs.Dock = System.Windows.Forms.DockStyle.Fill;
            grpConfigurationLabelCs.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            grpConfigurationLabelCs.ForeColor = System.Drawing.Color.MediumBlue;
            grpConfigurationLabelCs.Location = new System.Drawing.Point(0, 0);
            grpConfigurationLabelCs.Margin = new System.Windows.Forms.Padding(0);
            grpConfigurationLabelCs.Name = "grpConfigurationLabelCs";
            grpConfigurationLabelCs.Padding = new System.Windows.Forms.Padding(5);
            grpConfigurationLabelCs.Size = new System.Drawing.Size(943, 60);
            grpConfigurationLabelCs.TabIndex = 11;
            grpConfigurationLabelCs.TabStop = false;
            grpConfigurationLabelCs.Text = "grpConfigurationLabelCs";
            // 
            // txtConfigurationLabelCs
            // 
            txtConfigurationLabelCs.Dock = System.Windows.Forms.DockStyle.Fill;
            txtConfigurationLabelCs.Font = new System.Drawing.Font("Segoe UI", 9F);
            txtConfigurationLabelCs.ForeColor = System.Drawing.Color.Black;
            txtConfigurationLabelCs.Location = new System.Drawing.Point(5, 25);
            txtConfigurationLabelCs.Margin = new System.Windows.Forms.Padding(0);
            txtConfigurationLabelCs.Name = "txtConfigurationLabelCs";
            txtConfigurationLabelCs.Size = new System.Drawing.Size(933, 23);
            txtConfigurationLabelCs.TabIndex = 1;
            // 
            // grpConfigurationDescriptionCs
            // 
            grpConfigurationDescriptionCs.Controls.Add(txtConfigurationDescriptionCs);
            grpConfigurationDescriptionCs.Dock = System.Windows.Forms.DockStyle.Fill;
            grpConfigurationDescriptionCs.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            grpConfigurationDescriptionCs.ForeColor = System.Drawing.Color.MediumBlue;
            grpConfigurationDescriptionCs.Location = new System.Drawing.Point(0, 120);
            grpConfigurationDescriptionCs.Margin = new System.Windows.Forms.Padding(0);
            grpConfigurationDescriptionCs.Name = "grpConfigurationDescriptionCs";
            grpConfigurationDescriptionCs.Padding = new System.Windows.Forms.Padding(5);
            grpConfigurationDescriptionCs.Size = new System.Drawing.Size(943, 60);
            grpConfigurationDescriptionCs.TabIndex = 12;
            grpConfigurationDescriptionCs.TabStop = false;
            grpConfigurationDescriptionCs.Text = "grpConfigurationDescriptionCs";
            // 
            // txtConfigurationDescriptionCs
            // 
            txtConfigurationDescriptionCs.Dock = System.Windows.Forms.DockStyle.Fill;
            txtConfigurationDescriptionCs.Font = new System.Drawing.Font("Segoe UI", 9F);
            txtConfigurationDescriptionCs.ForeColor = System.Drawing.Color.Black;
            txtConfigurationDescriptionCs.Location = new System.Drawing.Point(5, 25);
            txtConfigurationDescriptionCs.Margin = new System.Windows.Forms.Padding(0);
            txtConfigurationDescriptionCs.Name = "txtConfigurationDescriptionCs";
            txtConfigurationDescriptionCs.Size = new System.Drawing.Size(933, 23);
            txtConfigurationDescriptionCs.TabIndex = 1;
            // 
            // pnlAuxiliaryVariableCategory
            // 
            pnlAuxiliaryVariableCategory.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlAuxiliaryVariableCategory.Location = new System.Drawing.Point(0, 420);
            pnlAuxiliaryVariableCategory.Margin = new System.Windows.Forms.Padding(0);
            pnlAuxiliaryVariableCategory.Name = "pnlAuxiliaryVariableCategory";
            pnlAuxiliaryVariableCategory.Size = new System.Drawing.Size(943, 120);
            pnlAuxiliaryVariableCategory.TabIndex = 25;
            // 
            // grpConfigQuery
            // 
            grpConfigQuery.Controls.Add(rdoQueryVar500);
            grpConfigQuery.Controls.Add(rdoQueryVar400);
            grpConfigQuery.Controls.Add(rdoQueryVar300);
            grpConfigQuery.Controls.Add(rdoQueryVar200);
            grpConfigQuery.Controls.Add(rdoQueryVar100);
            grpConfigQuery.Dock = System.Windows.Forms.DockStyle.Fill;
            grpConfigQuery.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            grpConfigQuery.ForeColor = System.Drawing.Color.MediumBlue;
            grpConfigQuery.Location = new System.Drawing.Point(0, 240);
            grpConfigQuery.Margin = new System.Windows.Forms.Padding(0);
            grpConfigQuery.Name = "grpConfigQuery";
            grpConfigQuery.Padding = new System.Windows.Forms.Padding(5);
            grpConfigQuery.Size = new System.Drawing.Size(943, 180);
            grpConfigQuery.TabIndex = 26;
            grpConfigQuery.TabStop = false;
            grpConfigQuery.Text = "grpConfigQuery";
            // 
            // rdoQueryVar500
            // 
            rdoQueryVar500.AutoSize = true;
            rdoQueryVar500.Font = new System.Drawing.Font("Segoe UI", 9F);
            rdoQueryVar500.ForeColor = System.Drawing.Color.Black;
            rdoQueryVar500.Location = new System.Drawing.Point(17, 147);
            rdoQueryVar500.Margin = new System.Windows.Forms.Padding(0);
            rdoQueryVar500.Name = "rdoQueryVar500";
            rdoQueryVar500.Size = new System.Drawing.Size(109, 19);
            rdoQueryVar500.TabIndex = 4;
            rdoQueryVar500.Text = "rdoQueryVar500";
            rdoQueryVar500.UseVisualStyleBackColor = true;
            // 
            // rdoQueryVar400
            // 
            rdoQueryVar400.AutoSize = true;
            rdoQueryVar400.Font = new System.Drawing.Font("Segoe UI", 9F);
            rdoQueryVar400.ForeColor = System.Drawing.Color.Black;
            rdoQueryVar400.Location = new System.Drawing.Point(17, 117);
            rdoQueryVar400.Margin = new System.Windows.Forms.Padding(0);
            rdoQueryVar400.Name = "rdoQueryVar400";
            rdoQueryVar400.Size = new System.Drawing.Size(109, 19);
            rdoQueryVar400.TabIndex = 3;
            rdoQueryVar400.Text = "rdoQueryVar400";
            rdoQueryVar400.UseVisualStyleBackColor = true;
            // 
            // rdoQueryVar300
            // 
            rdoQueryVar300.AutoSize = true;
            rdoQueryVar300.Font = new System.Drawing.Font("Segoe UI", 9F);
            rdoQueryVar300.ForeColor = System.Drawing.Color.Black;
            rdoQueryVar300.Location = new System.Drawing.Point(17, 87);
            rdoQueryVar300.Margin = new System.Windows.Forms.Padding(0);
            rdoQueryVar300.Name = "rdoQueryVar300";
            rdoQueryVar300.Size = new System.Drawing.Size(109, 19);
            rdoQueryVar300.TabIndex = 2;
            rdoQueryVar300.Text = "rdoQueryVar300";
            rdoQueryVar300.UseVisualStyleBackColor = true;
            // 
            // rdoQueryVar200
            // 
            rdoQueryVar200.AutoSize = true;
            rdoQueryVar200.Font = new System.Drawing.Font("Segoe UI", 9F);
            rdoQueryVar200.ForeColor = System.Drawing.Color.Black;
            rdoQueryVar200.Location = new System.Drawing.Point(17, 57);
            rdoQueryVar200.Margin = new System.Windows.Forms.Padding(0);
            rdoQueryVar200.Name = "rdoQueryVar200";
            rdoQueryVar200.Size = new System.Drawing.Size(109, 19);
            rdoQueryVar200.TabIndex = 1;
            rdoQueryVar200.Text = "rdoQueryVar200";
            rdoQueryVar200.UseVisualStyleBackColor = true;
            // 
            // rdoQueryVar100
            // 
            rdoQueryVar100.AutoSize = true;
            rdoQueryVar100.Checked = true;
            rdoQueryVar100.Font = new System.Drawing.Font("Segoe UI", 9F);
            rdoQueryVar100.ForeColor = System.Drawing.Color.Black;
            rdoQueryVar100.Location = new System.Drawing.Point(17, 27);
            rdoQueryVar100.Margin = new System.Windows.Forms.Padding(0);
            rdoQueryVar100.Name = "rdoQueryVar100";
            rdoQueryVar100.Size = new System.Drawing.Size(109, 19);
            rdoQueryVar100.TabIndex = 0;
            rdoQueryVar100.TabStop = true;
            rdoQueryVar100.Text = "rdoQueryVar100";
            rdoQueryVar100.UseVisualStyleBackColor = true;
            // 
            // pnlParams
            // 
            pnlParams.AutoScroll = true;
            pnlParams.Controls.Add(tlpParams);
            pnlParams.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlParams.Location = new System.Drawing.Point(0, 0);
            pnlParams.Margin = new System.Windows.Forms.Padding(0);
            pnlParams.Name = "pnlParams";
            pnlParams.Size = new System.Drawing.Size(960, 500);
            pnlParams.TabIndex = 9;
            // 
            // grpConfigurationLabelEn
            // 
            grpConfigurationLabelEn.Controls.Add(txtConfigurationLabelEn);
            grpConfigurationLabelEn.Dock = System.Windows.Forms.DockStyle.Fill;
            grpConfigurationLabelEn.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            grpConfigurationLabelEn.ForeColor = System.Drawing.Color.MediumBlue;
            grpConfigurationLabelEn.Location = new System.Drawing.Point(0, 60);
            grpConfigurationLabelEn.Margin = new System.Windows.Forms.Padding(0);
            grpConfigurationLabelEn.Name = "grpConfigurationLabelEn";
            grpConfigurationLabelEn.Padding = new System.Windows.Forms.Padding(5);
            grpConfigurationLabelEn.Size = new System.Drawing.Size(943, 60);
            grpConfigurationLabelEn.TabIndex = 12;
            grpConfigurationLabelEn.TabStop = false;
            grpConfigurationLabelEn.Text = "grpConfigurationLabelEn";
            // 
            // txtConfigurationLabelEn
            // 
            txtConfigurationLabelEn.Dock = System.Windows.Forms.DockStyle.Fill;
            txtConfigurationLabelEn.Font = new System.Drawing.Font("Segoe UI", 9F);
            txtConfigurationLabelEn.ForeColor = System.Drawing.Color.Black;
            txtConfigurationLabelEn.Location = new System.Drawing.Point(5, 25);
            txtConfigurationLabelEn.Margin = new System.Windows.Forms.Padding(0);
            txtConfigurationLabelEn.Name = "txtConfigurationLabelEn";
            txtConfigurationLabelEn.Size = new System.Drawing.Size(933, 23);
            txtConfigurationLabelEn.TabIndex = 1;
            // 
            // grpConfigurationDescriptionEn
            // 
            grpConfigurationDescriptionEn.Controls.Add(txtConfigurationDescriptionEn);
            grpConfigurationDescriptionEn.Dock = System.Windows.Forms.DockStyle.Fill;
            grpConfigurationDescriptionEn.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            grpConfigurationDescriptionEn.ForeColor = System.Drawing.Color.MediumBlue;
            grpConfigurationDescriptionEn.Location = new System.Drawing.Point(0, 180);
            grpConfigurationDescriptionEn.Margin = new System.Windows.Forms.Padding(0);
            grpConfigurationDescriptionEn.Name = "grpConfigurationDescriptionEn";
            grpConfigurationDescriptionEn.Padding = new System.Windows.Forms.Padding(5);
            grpConfigurationDescriptionEn.Size = new System.Drawing.Size(943, 60);
            grpConfigurationDescriptionEn.TabIndex = 13;
            grpConfigurationDescriptionEn.TabStop = false;
            grpConfigurationDescriptionEn.Text = "grpConfigurationDescriptionEn";
            // 
            // txtConfigurationDescriptionEn
            // 
            txtConfigurationDescriptionEn.Dock = System.Windows.Forms.DockStyle.Fill;
            txtConfigurationDescriptionEn.Font = new System.Drawing.Font("Segoe UI", 9F);
            txtConfigurationDescriptionEn.ForeColor = System.Drawing.Color.Black;
            txtConfigurationDescriptionEn.Location = new System.Drawing.Point(5, 25);
            txtConfigurationDescriptionEn.Margin = new System.Windows.Forms.Padding(0);
            txtConfigurationDescriptionEn.Name = "txtConfigurationDescriptionEn";
            txtConfigurationDescriptionEn.Size = new System.Drawing.Size(933, 23);
            txtConfigurationDescriptionEn.TabIndex = 1;
            // 
            // ControlConfigurationIntro
            // 
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            BackColor = System.Drawing.Color.Transparent;
            BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            Controls.Add(tlpMain);
            ForeColor = System.Drawing.Color.Black;
            Margin = new System.Windows.Forms.Padding(0);
            Name = "ControlConfigurationIntro";
            Size = new System.Drawing.Size(960, 540);
            tlpMain.ResumeLayout(false);
            pnlButtons.ResumeLayout(false);
            tlpButtons.ResumeLayout(false);
            pnlNext.ResumeLayout(false);
            tlpParams.ResumeLayout(false);
            grpConfigurationLabelCs.ResumeLayout(false);
            grpConfigurationLabelCs.PerformLayout();
            grpConfigurationDescriptionCs.ResumeLayout(false);
            grpConfigurationDescriptionCs.PerformLayout();
            grpConfigQuery.ResumeLayout(false);
            grpConfigQuery.PerformLayout();
            pnlParams.ResumeLayout(false);
            grpConfigurationLabelEn.ResumeLayout(false);
            grpConfigurationLabelEn.PerformLayout();
            grpConfigurationDescriptionEn.ResumeLayout(false);
            grpConfigurationDescriptionEn.PerformLayout();
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.Panel pnlButtons;
        private System.Windows.Forms.TableLayoutPanel tlpButtons;
        private System.Windows.Forms.Panel pnlNext;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.TableLayoutPanel tlpParams;
        private System.Windows.Forms.GroupBox grpConfigurationLabelCs;
        private System.Windows.Forms.TextBox txtConfigurationLabelCs;
        private System.Windows.Forms.GroupBox grpConfigurationDescriptionCs;
        private System.Windows.Forms.TextBox txtConfigurationDescriptionCs;
        private System.Windows.Forms.Panel pnlAuxiliaryVariableCategory;
        private System.Windows.Forms.GroupBox grpConfigQuery;
        private System.Windows.Forms.RadioButton rdoQueryVar500;
        private System.Windows.Forms.RadioButton rdoQueryVar400;
        private System.Windows.Forms.RadioButton rdoQueryVar300;
        private System.Windows.Forms.RadioButton rdoQueryVar200;
        private System.Windows.Forms.RadioButton rdoQueryVar100;
        private System.Windows.Forms.Panel pnlParams;
        private System.Windows.Forms.GroupBox grpConfigurationLabelEn;
        private System.Windows.Forms.TextBox txtConfigurationLabelEn;
        private System.Windows.Forms.GroupBox grpConfigurationDescriptionEn;
        private System.Windows.Forms.TextBox txtConfigurationDescriptionEn;
    }

}
