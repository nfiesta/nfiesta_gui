﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.AuxiliaryData;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.Catalog;

namespace ZaJi.ModuleAuxiliaryData
{

    /// <summary>
    /// <para lang="cs">Ovládací prvek "Volba geometrie pro rastrovou vrstvu"</para>
    /// <para lang="en">Control "Geometry selection in raster layer"</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    internal partial class ControlRasterGeom
        : UserControl, INfiEstaControl, IAuxiliaryDataControl
    {

        #region Constants

        /// <summary>
        /// <para lang="cs">Koeficient pro převod na metry čtvereční</para>
        /// <para lang="en">Coefficient for conversion to square metres</para>
        /// </summary>
        private const double METERS = 1.0;

        /// <summary>
        /// <para lang="cs">Defaultní jméno sloupce primárního klíče pro rastrovou vrstvu</para>
        /// <para lang="en">Default column name of primary key in raster layer</para>
        /// </summary>
        private const string RID = "rid";

        /// <summary>
        /// <para lang="cs">Defaultní jméno sloupce s rastrem</para>
        /// <para lang="en">Default column name with raster</para>
        /// </summary>
        private const string RAST = "rast";

        private const string NAME = "Name";
        private const string INTEGER = "integer";
        private const string RASTER = "raster";

        #endregion Constants


        #region Controls

        private readonly Label lblColumnIdentError = new();

        private readonly Label lblColumnNameError = new();

        #endregion Controls


        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        private Control controlOwner;

        #endregion Private Fields


        #region Events

        /// <summary>
        /// <para lang="cs">Událost kliknutí na tlačítko "Předchozí"</para>
        /// <para lang="en">Click event on the "Previous" button</para>
        /// </summary>
        public event EventHandler PreviousClick;

        /// <summary>
        /// <para lang="cs">Událost kliknutí na tlačítko "Další"</para>
        /// <para lang="en">Click event on the "Next" button</para>
        /// </summary>
        public event EventHandler NextClick;

        #endregion Events


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor ovládacího prvku</para>
        /// <para lang="en">Control constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        public ControlRasterGeom(Control controlOwner)
        {
            InitializeComponent();
            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                IAuxiliaryDataControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                if (controlOwner is not FormConfigCollectionGuide)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(FormConfigCollectionGuide)}.",
                        paramName: nameof(ControlOwner));
                }

                return controlOwner;
            }
            set
            {
                IAuxiliaryDataControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                if (value is not FormConfigCollectionGuide)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(FormConfigCollectionGuide)}.",
                        paramName: nameof(ControlOwner));
                }

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables (read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((IAuxiliaryDataControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro pomocná data (read-only)</para>
        /// <para lang="en">Module for auxiliary data setting (read-only)</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((IAuxiliaryDataControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">Skupina konfigurací</para>
        /// <para lang="en">Configuration collection</para>
        /// </summary>
        public ConfigCollection ConfigCollection
        {
            get
            {
                return ((FormConfigCollectionGuide)ControlOwner).ConfigCollection;
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(btnPrevious),                          "Předchozí" },
                        { nameof(btnNext),                              "Další" },
                        { nameof(grpColumnIdent),                       "Sloupec s identifikátorem:" },
                        { nameof(grpColumnName),                        "Sloupec s rastrem:" },
                        { nameof(grpUnitCoefficient),                   "Koeficient pro převod jednotek:" },
                        { nameof(lblColumnIdentError),                  "Vybraná databázová tabulka nemá identifikátor záznamu." },
                        { nameof(lblColumnNameError),                   "Vybraná databázová tabulka nemá žádný atribut typu rastr." },
                        { nameof(lblUnitCoefficientDescription1),       "Koeficient bude použitý k převodu jednotky například v případech, kdy velikost pixelu je uváděna v metrech čtverečních" },
                        { nameof(lblUnitCoefficientDescription2),       "a je požadována výsledná hodnota úhrnu pomocné proměnné v hektarech, nebo hodnota pixelu je v procentech" },
                        { nameof(lblUnitCoefficientDescription3),       "a je potřeba převod na desetinné číslo. Konečný výsledek bude uvedeným koeficientem vynásoben." },
                        { nameof(lblUnitCoefficientDescription4),       "Pokud převod nepožadujete uveďte 1." }
                    }
                    : languageFile.NationalVersion.Data.TryGetValue(
                        key: nameof(ControlRasterGeom),
                        out Dictionary<string, string> dictNational)
                            ? dictNational
                            : [],

                LanguageVersion.International => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(btnPrevious),                      "Previous" },
                        { nameof(btnNext),                          "Next" },
                        { nameof(grpColumnIdent),                   "Identifier column:" },
                        { nameof(grpColumnName),                    "Column with raster:" },
                        { nameof(grpUnitCoefficient),               "Coefficient for unit conversion:" },
                        { nameof(lblColumnIdentError),              "Selected database table does not contain any identifier." },
                        { nameof(lblColumnNameError),               "Selected database table does not contain any attribute with type raster." },
                        { nameof(lblUnitCoefficientDescription1),   "This coefficient will be used to unit conversion, for example in case when pixel size is in square meters " },
                        { nameof(lblUnitCoefficientDescription2),   "and result auxiliary variable total have to be in hectares, or when pixel value is percentual and conversion " },
                        { nameof(lblUnitCoefficientDescription3),   "into decimal number is required. The final result will be multiplied by this coefficient." },
                        { nameof(lblUnitCoefficientDescription4),   "If you don't require any conversion, set coefficient value to 1." },
                    }
                    : languageFile.InternationalVersion.Data.TryGetValue(
                        key: nameof(ControlRasterGeom),
                        out Dictionary<string, string> dictInternational)
                            ? dictInternational
                            : [],

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku</para>
        /// <para lang="en">Initialization of the control</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        private void Initialize(Control controlOwner)
        {
            BorderStyle = BorderStyle.None;
            ControlOwner = controlOwner;

            InitializeLabels();

            btnPrevious.Click += new EventHandler(
                (sender, e) =>
                {
                    PreviousClick?.Invoke(sender: sender, e: e);
                });

            btnNext.Click += new EventHandler(
               (sender, e) =>
               {
                   NextClick?.Invoke(sender: sender, e: e);
               });

            txtUnitCoefficient.TextChanged += new EventHandler(
                (sender, e) => { SetUnit(); });
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků ovládacího prvku</para>
        /// <para lang="en">Initialization of control labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            btnPrevious.Text =
                labels.TryGetValue(key: nameof(btnPrevious),
                out string btnPreviousText)
                    ? btnPreviousText
                    : String.Empty;

            btnNext.Text =
                labels.TryGetValue(key: nameof(btnNext),
                out string btnNextText)
                    ? btnNextText
                    : String.Empty;

            grpColumnIdent.Text =
                labels.TryGetValue(key: nameof(grpColumnIdent),
                out string grpColumnIdentText)
                    ? grpColumnIdentText
                    : String.Empty;

            grpColumnName.Text =
                labels.TryGetValue(key: nameof(grpColumnName),
                out string grpColumnNameText)
                    ? grpColumnNameText
                    : String.Empty;

            grpUnitCoefficient.Text =
                labels.TryGetValue(key: nameof(grpUnitCoefficient),
                out string grpUnitCoefficientText)
                    ? grpUnitCoefficientText
                    : String.Empty;

            lblUnitCoefficientDescription1.Text =
                labels.TryGetValue(key: nameof(lblUnitCoefficientDescription1),
                out string lblUnitCoefficientDescription1Text)
                    ? lblUnitCoefficientDescription1Text
                    : String.Empty;

            lblUnitCoefficientDescription2.Text =
                labels.TryGetValue(key: nameof(lblUnitCoefficientDescription2),
                out string lblUnitCoefficientDescription2Text)
                    ? lblUnitCoefficientDescription2Text
                    : String.Empty;

            lblUnitCoefficientDescription3.Text =
                labels.TryGetValue(key: nameof(lblUnitCoefficientDescription3),
                out string lblUnitCoefficientDescription3Text)
                    ? lblUnitCoefficientDescription3Text
                    : String.Empty;

            lblUnitCoefficientDescription4.Text =
                labels.TryGetValue(key: nameof(lblUnitCoefficientDescription4),
                out string lblUnitCoefficientDescription4Text)
                    ? lblUnitCoefficientDescription4Text
                    : String.Empty;

            foreach (Label lblColumnIdentError in grpColumnIdent.Controls.OfType<Label>())
            {
                lblColumnIdentError.Text =
                    labels.TryGetValue(key: nameof(lblColumnIdentError),
                    out string lblColumnIdentErrorText)
                        ? lblColumnIdentErrorText
                        : String.Empty;
            }

            foreach (Label lblColumnNameError in grpColumnName.Controls.OfType<Label>())
            {
                lblColumnNameError.Text =
                    labels.TryGetValue(key: nameof(lblColumnNameError),
                    out string lblColumnNameErrorText)
                        ? lblColumnNameErrorText
                        : String.Empty;
            }
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent()
        {

            InitializeComboBoxColumnIdent();
            InitializeComboBoxColumnName();
            InitializeTextBoxUnit();

            EnableNext();
        }

        /// <summary>
        /// <para lang="cs">Povolí postup na další formulář</para>
        /// <para lang="en">Allows progression to the next form</para>
        /// </summary>
        private void EnableNext()
        {
            if (ConfigCollection == null)
            {
                btnNext.Enabled = false;
                return;
            }

            btnNext.Enabled =
                    !String.IsNullOrEmpty(ConfigCollection.LabelCs) &&
                    !String.IsNullOrEmpty(ConfigCollection.LabelEn) &&
                    !String.IsNullOrEmpty(ConfigCollection.DescriptionCs) &&
                    !String.IsNullOrEmpty(ConfigCollection.DescriptionEn) &&
                    (ConfigCollection.ConfigFunctionValue == ConfigFunctionEnum.RasterTotal) &&
                    !String.IsNullOrEmpty(value: ConfigCollection?.CatalogName) &&
                    !String.IsNullOrEmpty(value: ConfigCollection?.SchemaName) &&
                    !String.IsNullOrEmpty(value: ConfigCollection?.TableName) &&
                    !String.IsNullOrEmpty(value: ConfigCollection?.ColumnIdent) &&
                    !String.IsNullOrEmpty(value: ConfigCollection?.ColumnName) &&
                    (ConfigCollection?.Unit != null);
        }

        /// <summary>
        /// <para lang="cs">
        /// Inicializace ComboBox se seznamem sloupců,
        /// které jsou součástí jednoduchého primárního klíče nebo jednoduchého unique omezení
        /// </para>
        /// <para lang="en">
        /// Initializing ComboBox with list of columns
        /// that are part of a simple primary key or simple unique constraint
        /// </para>
        /// </summary>
        private void InitializeComboBoxColumnIdent()
        {
            grpColumnIdent.Controls.Clear();

            if (ConfigCollection == null)
            {
                return;
            }

            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            List<DBColumn> identColumns = GetIdentColumns(
                schemaName: ConfigCollection?.SchemaName,
                tableName: ConfigCollection?.TableName);

            DBColumn selectedItem = identColumns
                .Where(a => a.Name == (ConfigCollection.ColumnIdent ?? String.Empty))
                .FirstOrDefault<DBColumn>();

            if (identColumns.Count == 0)
            {
                Label lblColumnIdentError = new()
                {
                    AutoSize = false,
                    BackColor = Color.Transparent,
                    Dock = DockStyle.Fill,
                    Font = Setting.LabelErrorFont,
                    ForeColor = Setting.LabelErrorForeColor,
                    Margin = new Padding(all: 0),
                    Padding = new Padding(all: 0),
                    TextAlign = ContentAlignment.MiddleLeft,
                    Text =
                        labels.TryGetValue(key: nameof(lblColumnIdentError),
                        out string lblColumnIdentErrorText)
                            ? lblColumnIdentErrorText
                            : String.Empty
                };
                lblColumnIdentError.CreateControl();
                grpColumnIdent.Controls.Add(value: lblColumnIdentError);
                return;
            }

            ComboBox cboColumnIdent = new()
            {
                BackColor = Setting.ComboBoxBackColor,
                DataSource = identColumns,
                DisplayMember = NAME,
                Dock = DockStyle.Fill,
                DropDownStyle = ComboBoxStyle.DropDownList,
                FlatStyle = FlatStyle.Standard,
                Font = Setting.ComboBoxFont,
                ForeColor = Setting.ComboBoxForeColor,
                ValueMember = NAME
            };

            cboColumnIdent.SelectedIndexChanged += new EventHandler(
                (sender, e) =>
                {
                    if (cboColumnIdent.SelectedItem != null)
                    {
                        ConfigCollection.ColumnIdent = !String.IsNullOrEmpty(
                            value: ((DBColumn)cboColumnIdent.SelectedItem)?.Name)
                                ? ((DBColumn)cboColumnIdent.SelectedItem)?.Name
                                : null;
                        EnableNext();
                    }
                });

            cboColumnIdent.CreateControl();
            grpColumnIdent.Controls.Add(value: cboColumnIdent);

            // Volba selected item v ComboBox
            if (selectedItem != null)
            {
                cboColumnIdent.SelectedItem = null;
                cboColumnIdent.SelectedItem = selectedItem;
            }
            else
            {
                selectedItem = identColumns
                    .Where(a => a.Name == RID)
                    .FirstOrDefault<DBColumn>();

                if (selectedItem != null)
                {
                    cboColumnIdent.SelectedItem = null;
                    cboColumnIdent.SelectedItem = selectedItem;
                }
                else if (identColumns.Count > 0)
                {
                    cboColumnIdent.SelectedItem = null;
                    selectedItem = identColumns[0];
                    cboColumnIdent.SelectedItem = selectedItem;
                }
            }
        }

        /// <summary>
        /// <para lang="cs">
        /// Inicializace ComboBox se seznamem sloupců,
        /// které mají datový typ raster
        /// </para>
        /// <para lang="en">
        /// Initializing ComboBox with list of columns
        /// that have the raster data type
        /// </para>
        /// </summary>
        private void InitializeComboBoxColumnName()
        {
            grpColumnName.Controls.Clear();

            if (ConfigCollection == null)
            {
                return;
            }
            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            List<DBColumn> rasterColumns = GetRasterColumns(
                schemaName: ConfigCollection?.SchemaName,
                tableName: ConfigCollection?.TableName);

            DBColumn selectedItem = rasterColumns
                .Where(a => a.Name == (ConfigCollection.ColumnName ?? String.Empty))
                .FirstOrDefault<DBColumn>();

            if (rasterColumns.Count == 0)
            {
                Label lblColumnNameError = new()
                {
                    AutoSize = false,
                    BackColor = Color.Transparent,
                    Dock = DockStyle.Fill,
                    Font = Setting.LabelErrorFont,
                    ForeColor = Setting.LabelErrorForeColor,
                    Margin = new Padding(all: 0),
                    Padding = new Padding(all: 0),
                    TextAlign = ContentAlignment.MiddleLeft,
                    Text =
                        labels.TryGetValue(key: nameof(lblColumnNameError),
                        out string lblColumnNameErrorText)
                            ? lblColumnNameErrorText
                            : String.Empty
                };
                lblColumnNameError.CreateControl();
                grpColumnName.Controls.Add(value: lblColumnNameError);
                return;
            }

            ComboBox cboColumnName = new()
            {
                BackColor = Setting.ComboBoxBackColor,
                DataSource = rasterColumns,
                DisplayMember = NAME,
                Dock = DockStyle.Fill,
                DropDownStyle = ComboBoxStyle.DropDownList,
                FlatStyle = FlatStyle.Standard,
                Font = Setting.ComboBoxFont,
                ForeColor = Setting.ComboBoxForeColor,
                ValueMember = NAME
            };

            cboColumnName.SelectedIndexChanged += new EventHandler(
               (sender, e) =>
               {
                   if (cboColumnName.SelectedItem != null)
                   {
                       ConfigCollection.ColumnName = !String.IsNullOrEmpty(
                            value: ((DBColumn)cboColumnName.SelectedItem)?.Name)
                                ? ((DBColumn)cboColumnName.SelectedItem)?.Name
                                : null;
                       EnableNext();
                   }
               });

            cboColumnName.CreateControl();
            grpColumnName.Controls.Add(value: cboColumnName);

            // Volba selected item v ComboBox
            if (selectedItem != null)
            {
                cboColumnName.SelectedItem = null;
                cboColumnName.SelectedItem = selectedItem;
            }
            else
            {
                selectedItem = rasterColumns
                    .Where(a => a.Name == RAST)
                    .FirstOrDefault<DBColumn>();

                if (selectedItem != null)
                {
                    cboColumnName.SelectedItem = null;
                    cboColumnName.SelectedItem = selectedItem;
                }
                else if (rasterColumns.Count > 0)
                {
                    cboColumnName.SelectedItem = null;
                    selectedItem = rasterColumns[0];
                    cboColumnName.SelectedItem = selectedItem;
                }
            }
        }

        /// <summary>
        /// <para lang="cs">
        /// Inicializace TextBox pro volbu koeficientu pro převod jednotky
        /// </para>
        /// <para lang="en">
        /// Initializing TextBox for unit conversion coefficent selection
        /// </para>
        /// </summary>
        private void InitializeTextBoxUnit()
        {
            if (ConfigCollection == null)
            {
                return;
            }

            txtUnitCoefficient.Text =
                ConfigCollection.Unit != null
                    ? ConfigCollection.Unit.ToString()
                    : METERS.ToString();
        }

        /// <summary>
        /// <para lang="cs">Seznam sloupců, které jsou součástí jednoduchého primárního klíče nebo jednoduchého unique omezení</para>
        /// <para lang="en">List of columns that are part of a simple primary key or simple unique constraint</para>
        /// </summary>
        /// <param name="schemaName">
        /// <para lang="cs">Jméno schéma</para>
        /// <para lang="en">Schema name</para>
        /// </param>
        /// <param name="tableName">
        /// <para lang="cs">Jméno tabulky</para>
        /// <para lang="en">Table name</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Seznam sloupců, které jsou součástí jednoduchého primárního klíče nebo jednoduchého unique omezení</para>
        /// <para lang="en">List of columns that are part of a simple primary key or simple unique constraint</para>
        /// </returns>
        private List<DBColumn> GetIdentColumns(string schemaName, string tableName)
        {
            if (Database.Postgres.Catalog.Schemas[schemaName] == null)
            {
                // Schéma neexistuje, vrátí se prázdný seznam
                return [];
            }

            if (Database.Postgres.Catalog.Schemas[schemaName].Tables[tableName] != null)
            {
                // Existuje datová tabulka

                // Vybere sloupečky s jednoduchých primárních klíčů (obsahují pouze jeden sloupeček)
                IEnumerable<DBColumn> pkeyCols =
                    Database.Postgres.Catalog.Schemas[schemaName].Tables[tableName].PrimaryKeys.Items
                        .Where(a => a.ConstraintColumns.Count == 1)
                        .SelectMany(a => a.ConstraintColumns);

                // Vybere sloupečky s jednoduchých unique omezení (obsahují pouze jeden sloupeček)
                IEnumerable<DBColumn> ukeyCols =
                    Database.Postgres.Catalog.Schemas[schemaName].Tables[tableName].UniqueConstraints.Items
                        .Where(a => a.ConstraintColumns.Count == 1)
                        .SelectMany(a => a.ConstraintColumns);

                return [.. pkeyCols
                        .Concat(second: ukeyCols)
                        .Distinct<DBColumn>()
                        .OrderBy(a => a.Name)];
            }

            else if (Database.Postgres.Catalog.Schemas[schemaName].ForeignTables[tableName] != null)
            {
                // Existuje cizí datová tabulka

                // Pro cizí tabulky není dostupný seznam primárních klíčů a unique omezení
                // vrací se seznam všech sloupců datového typu integer
                return
                    [.. Database.Postgres.Catalog.Schemas[schemaName].ForeignTables[tableName].Columns.Items
                    .Where(a => a.TypeInfo.Contains(
                        value: INTEGER,
                        comparisonType: StringComparison.InvariantCultureIgnoreCase))
                    .Distinct<DBColumn>()
                    .OrderBy(a => a.Name)];
            }

            else
            {
                // Tabulka neexistuje, vrátí se prázdný seznam
                return [];
            }
        }

        /// <summary>
        /// <para lang="cs">Seznam sloupců, které mají datový typ rastr</para>
        /// <para lang="en">List of columns that have the raster data type</para>
        /// </summary>
        /// <param name="schemaName">
        /// <para lang="cs">Jméno schéma</para>
        /// <para lang="en">Schema name</para>
        /// </param>
        /// <param name="tableName">
        /// <para lang="cs">Jméno tabulky</para>
        /// <para lang="en">Table name</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Seznam sloupců, které mají datový typ rastr</para>
        /// <para lang="en">List of columns that have the raster data type</para>
        /// </returns>
        private List<DBColumn> GetRasterColumns(string schemaName, string tableName)
        {
            if (Database.Postgres.Catalog.Schemas[schemaName] == null)
            {
                // Schéma neexistuje, vrátí se prázdný seznam
                return [];
            }

            if (Database.Postgres.Catalog.Schemas[schemaName].Tables[tableName] != null)
            {
                // Existuje datová tabulka

                return
                    [.. Database.Postgres.Catalog.Schemas[schemaName].Tables[tableName].Columns.Items
                    .Where(a => a.TypeName.Contains(
                        value: RASTER,
                        comparisonType: StringComparison.InvariantCultureIgnoreCase))
                    .Distinct<DBColumn>()
                    .OrderBy(a => a.Name)];
            }

            else if (Database.Postgres.Catalog.Schemas[schemaName].ForeignTables[tableName] != null)
            {
                // Existuje cizí datová tabulka

                return
                    [.. Database.Postgres.Catalog.Schemas[schemaName].ForeignTables[tableName].Columns.Items
                    .Where(a => a.TypeName.Contains(
                        value: RASTER,
                        comparisonType: StringComparison.InvariantCultureIgnoreCase))
                    .Distinct<DBColumn>()
                    .OrderBy(a => a.Name)];
            }

            else
            {
                // Tabulka neexistuje, vrátí se prázdný seznam
                return [];
            }
        }

        /// <summary>
        /// <para lang="cs">Nastaví vlastnost ConfigCollection.Unit z textového políčka</para>
        /// <para lang="en">Sets property ConfigCollection.Unit from TextBox</para>
        /// </summary>
        private void SetUnit()
        {
            Nullable<double> unit =
                Functions.ConvertToNDouble(
                    text: txtUnitCoefficient.Text.Trim(),
                    defaultValue: null);
            if (unit != null)
            {
                // Převod textu na číslo se podařil:
                txtUnitCoefficient.ForeColor = Color.Black;
                ConfigCollection.Unit = unit;
                EnableNext();
                return;
            }

            // Pokusí se nahradit desetinnou čárku tečkou a znovu zkusí konverzi na desetinné číslo:
            unit =
                Functions.ConvertToNDouble(
                    text: txtUnitCoefficient.Text.Trim().Replace(oldChar: ',', newChar: '.'),
                    defaultValue: null);
            if (unit != null)
            {
                // Převod textu na číslo se podařil:
                txtUnitCoefficient.ForeColor = Color.Black;
                ConfigCollection.Unit = unit;
                EnableNext();
                return;
            }

            // Pokusí se nahradit desetinnou tečku čárkou a znovu zkusí konverzi na desetinné číslo:
            unit =
                Functions.ConvertToNDouble(
                    text: txtUnitCoefficient.Text.Trim().Replace(oldChar: '.', newChar: ','),
                    defaultValue: null);
            if (unit != null)
            {
                // Převod textu na číslo se podařil:
                txtUnitCoefficient.ForeColor = Color.Black;
                ConfigCollection.Unit = unit;
                EnableNext();
                return;
            }

            // Převod textu na desetinné číslo se nepodařil
            txtUnitCoefficient.ForeColor = Color.Red;
            ConfigCollection.Unit = unit;
            EnableNext();
            return;
        }

        #endregion Methods

    }

}