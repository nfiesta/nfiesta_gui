﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.AuxiliaryData;
using ZaJi.PostgreSQL;

namespace ZaJi.ModuleAuxiliaryData
{

    /// <summary>
    /// <para lang="cs">Ovládací prvek "Protínání bodové vrstvy s GIS vrstvou úhrnů"</para>
    /// <para lang="en">Control "Intersection of point layer with GIS layer of totals"</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    internal partial class ControlPointIntersection
        : UserControl, INfiEstaControl, IAuxiliaryDataControl
    {

        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        private Control controlOwner;

        #endregion Private Fields


        #region Events

        /// <summary>
        /// <para lang="cs">Událost kliknutí na tlačítko "Předchozí"</para>
        /// <para lang="en">Click event on the "Previous" button</para>
        /// </summary>
        public event EventHandler PreviousClick;

        /// <summary>
        /// <para lang="cs">Událost kliknutí na tlačítko "Další"</para>
        /// <para lang="en">Click event on the "Next" button</para>
        /// </summary>
        public event EventHandler NextClick;

        #endregion Events


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor ovládacího prvku</para>
        /// <para lang="en">Control constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        public ControlPointIntersection(Control controlOwner)
        {
            InitializeComponent();
            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                IAuxiliaryDataControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                if (controlOwner is not FormConfigCollectionGuide)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(FormConfigCollectionGuide)}.",
                        paramName: nameof(ControlOwner));
                }

                return controlOwner;
            }
            set
            {
                IAuxiliaryDataControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                if (value is not FormConfigCollectionGuide)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(FormConfigCollectionGuide)}.",
                        paramName: nameof(ControlOwner));
                }

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables (read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((IAuxiliaryDataControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro pomocná data (read-only)</para>
        /// <para lang="en">Module for auxiliary data setting (read-only)</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((IAuxiliaryDataControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">Skupina konfigurací</para>
        /// <para lang="en">Configuration collection</para>
        /// </summary>
        public ConfigCollection ConfigCollection
        {
            get
            {
                return ((FormConfigCollectionGuide)ControlOwner).ConfigCollection;
            }
        }

        /// <summary>
        /// <para lang="cs">Ovládací prvek pro výběr skupiny konfigurací bodové vrstvy</para>
        /// <para lang="en">Control "Configuration collection selector for point layer"</para>
        /// </summary>
        private ControlConfigCollectionSelector CtrPointLayer
        {
            get
            {
                return
                    pnlGISLayerA.Controls
                        .OfType<ControlConfigCollectionSelector>()
                        .FirstOrDefault<ControlConfigCollectionSelector>();
            }
        }

        /// <summary>
        /// <para lang="cs">Ovládací prvek pro výběr skupiny konfigurací GIS vrstvy úhrnů</para>
        /// <para lang="en">Control "Configuration collection selection for GIS layer of totals"</para>
        /// </summary>
        private ControlConfigCollectionSelector CtrTotalsLayer
        {
            get
            {
                return
                    pnlGISLayerB.Controls
                        .OfType<ControlConfigCollectionSelector>()
                        .FirstOrDefault<ControlConfigCollectionSelector>();
            }
        }

        /// <summary>
        /// <para lang="cs">Vybraná skupina konfigurací bodové vrstvy</para>
        /// <para lang="en">Selected configuration collection for point layer</para>
        /// </summary>
        public ConfigCollection PointLayer
        {
            get
            {
                return
                    CtrPointLayer?.ConfigCollection;
            }
        }

        /// <summary>
        /// <para lang="cs">Vybraná skupina konfigurací GIS vrstvy úhrnů</para>
        /// <para lang="en">Selected configuration for GIS layer of totals</para>
        /// </summary>
        public ConfigCollection TotalsLayer
        {
            get
            {
                return
                    CtrTotalsLayer?.ConfigCollection;
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(btnPrevious),                        "Předchozí" },
                        { nameof(btnNext),                            "Další" },
                    }
                    : languageFile.NationalVersion.Data.TryGetValue(
                        key: nameof(ControlPointIntersection),
                        out Dictionary<string, string> dictNational)
                            ? dictNational
                            : [],

                LanguageVersion.International => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(btnPrevious),                        "Previous" },
                        { nameof(btnNext),                            "Next" },
                    }
                    : languageFile.InternationalVersion.Data.TryGetValue(
                        key: nameof(ControlPointIntersection),
                        out Dictionary<string, string> dictInternational)
                            ? dictInternational
                            : [],

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku</para>
        /// <para lang="en">Initialization of the control</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        private void Initialize(Control controlOwner)
        {
            BorderStyle = BorderStyle.None;
            ControlOwner = controlOwner;

            pnlGISLayerA.Controls.Clear();
            ControlConfigCollectionSelector ctrConfigCollectionSelectorPoints =
                new(
                    controlOwner: this,
                    configFunctions:
                        Database.SAuxiliaryData.CConfigFunction.Items
                            .Where(a => a.Id == (int)ConfigFunctionEnum.PointLayer)
                            .ToList<ConfigFunction>())
                {
                    Dock = DockStyle.Fill,
                    ConfigCollection = ConfigCollection?.RefLayerPoints
                };
            ctrConfigCollectionSelectorPoints.ConfigCollectionChanged +=
                new EventHandler(
                    (sender, e) => { SetGisLayers(); });
            ctrConfigCollectionSelectorPoints.CreateControl();
            pnlGISLayerA.Controls.Add(value: ctrConfigCollectionSelectorPoints);

            pnlGISLayerB.Controls.Clear();
            ControlConfigCollectionSelector ctrConfigCollectionSelectorTotals =
                new(
                    controlOwner: this,
                    configFunctions:
                        Database.SAuxiliaryData.CConfigFunction.Items
                            .Where(a => new List<int>()
                                { (int)ConfigFunctionEnum.VectorTotal,
                                  (int)ConfigFunctionEnum.RasterTotal,
                                  (int)ConfigFunctionEnum.RasterTotalWithinVector,
                                  (int)ConfigFunctionEnum.RastersProductTotal}
                            .Contains(item: a.Id))
                            .ToList<ConfigFunction>())
                {
                    Dock = DockStyle.Fill,
                    ConfigCollection = ConfigCollection?.RefTotal
                };
            ctrConfigCollectionSelectorTotals.ConfigCollectionChanged += new EventHandler(
                (sender, e) => { SetGisLayers(); });
            ctrConfigCollectionSelectorTotals.CreateControl();
            pnlGISLayerB.Controls.Add(value: ctrConfigCollectionSelectorTotals);

            InitializeLabels();

            EnableNext();

            btnPrevious.Click += new EventHandler(
                (sender, e) =>
                {
                    PreviousClick?.Invoke(sender: sender, e: e);
                });

            btnNext.Click += new EventHandler(
               (sender, e) =>
               {
                   NextClick?.Invoke(sender: sender, e: e);
               });
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků ovládacího prvku</para>
        /// <para lang="en">Initialization of control labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            btnPrevious.Text =
                labels.TryGetValue(key: nameof(btnPrevious),
                out string btnPreviousText)
                    ? btnPreviousText
                    : String.Empty;

            btnNext.Text =
                labels.TryGetValue(key: nameof(btnNext),
                out string btnNextText)
                    ? btnNextText
                    : String.Empty;

            CtrPointLayer?.InitializeLabels();
            CtrTotalsLayer?.InitializeLabels();
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent()
        {
        }

        /// <summary>
        /// <para lang="cs">Nastaví zvolené vrstvy v objektu konfigurace</para>
        /// <para lang="en">Sets selected layers in configuration object</para>
        /// </summary>
        private void SetGisLayers()
        {
            if (ConfigCollection.ConfigFunction.Value == ConfigFunctionEnum.PointTotalCombination)
            {
                ConfigCollection.RefLayerPointsId = PointLayer?.Id;
                ConfigCollection.RefTotalId = TotalsLayer?.Id;
            }
            else
            {
                ConfigCollection.RefLayerPointsId = null;
                ConfigCollection.RefTotalId = null;
            }

            EnableNext();
        }

        /// <summary>
        /// <para lang="cs">Povolí postup na další formulář</para>
        /// <para lang="en">Allows progression to the next form</para>
        /// </summary>
        private void EnableNext()
        {
            if (ConfigCollection == null)
            {
                btnNext.Enabled = false;
                return;
            }

            btnNext.Enabled =
                    !String.IsNullOrEmpty(ConfigCollection.LabelCs) &&
                    !String.IsNullOrEmpty(ConfigCollection.LabelEn) &&
                    !String.IsNullOrEmpty(ConfigCollection.DescriptionCs) &&
                    !String.IsNullOrEmpty(ConfigCollection.DescriptionEn) &&
                    (ConfigCollection.ConfigFunctionValue == ConfigFunctionEnum.PointTotalCombination) &&
                    ConfigCollection.RefLayerPointsId != null &&
                    ConfigCollection.RefTotalId != null;
        }

        #endregion Methods

    }

}
