﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.AuxiliaryData;
using ZaJi.PostgreSQL;

namespace ZaJi.ModuleAuxiliaryData
{

    /// <summary>
    /// <para lang="cs">Zobrazení parametrů skupiny konfigurací</para>
    /// <para lang="en">Control "Display config collection parameters"</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    internal partial class ControlConfigCollectionView
        : UserControl, INfiEstaControl, IAuxiliaryDataControl
    {

        #region Constants

        /// <summary>
        /// <para lang="cs">Výška řádku v tabulce</para>
        /// <para lang="en">Table layout panel row height</para>
        /// </summary>
        private const int rowHeight = 20;

        /// <summary>
        /// <para lang="cs">Šířka prvního sloupce v tabulce</para>
        /// <para lang="en">Table layout panel first column width</para>
        /// </summary>
        private const int colRowNumberWidth = 25;

        #endregion Constants


        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Skupina konfigurací</para>
        /// <para lang="en">Configuration collection</para>
        /// </summary>
        private ConfigCollection configCollection;

        private string strClosedTrue = String.Empty;
        private string strClosedFalse = String.Empty;
        private string strAggregatedTrue = String.Empty;
        private string strAggregatedFalse = String.Empty;

        #endregion Private Fields


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor ovládacího prvku</para>
        /// <para lang="en">Control constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        public ControlConfigCollectionView(Control controlOwner)
        {
            InitializeComponent();
            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                IAuxiliaryDataControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                return controlOwner;
            }
            set
            {
                IAuxiliaryDataControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables (read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((IAuxiliaryDataControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro pomocná data (read-only)</para>
        /// <para lang="en">Module for auxiliary data setting (read-only)</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((IAuxiliaryDataControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">Skupina konfigurací</para>
        /// <para lang="en">Configuration collection</para>
        /// </summary>
        public ConfigCollection ConfigCollection
        {
            get
            {
                return configCollection;
            }
            set
            {
                configCollection = value;

                InitializeLabels();

                SetRowHeights();
            }
        }

        /// <summary>
        /// <para lang="cs">Zobrazovat čísla řádků</para>
        /// <para lang="en">Display row numbers</para>
        /// </summary>
        public bool RowNumbers
        {
            get
            {
                return
                    tlpConfigCollectionParameters.ColumnStyles[0].Width != 0;
            }
            set
            {
                tlpConfigCollectionParameters.ColumnStyles[0].Width =
                    value
                    ? colRowNumberWidth
                    : 0;
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(grpCaption),                       String.Empty  },
                        { nameof(grpConfigCollectionParameters),    "Parametry pomocných dat:"  },
                        { nameof(lblIdCaption),                     "ID:" },
                        { nameof(lblAuxiliaryVariableIdCaption),    "Pomocná proměnná:" },
                        { nameof(lblConfigFunctionIdCaption),       "Výpočetní funkce:" },
                        { nameof(lblLabelCaption),                  "Název:" },
                        { nameof(lblDescriptionCaption),            "Popis:" },
                        { nameof(lblClosedCaption),                 "Uzavřená skupina:" },
                        { nameof(lblAggregatedCaption),             "Agregovaná skupina:" },
                        { nameof(lblCatalogNameCaption),            "Katalog:" },
                        { nameof(lblSchemaNameCaption),             "Schéma:" },
                        { nameof(lblTableNameCaption),              "Tabulka:" },
                        { nameof(lblColumnIdentCaption),            "Identifikátor:" },
                        { nameof(lblColumnNameCaption),             "Atribut:" },
                        { nameof(lblUnitCaption),                   "Koeficient:" },
                        { nameof(lblConditionCaption),              "Podmínka:" },
                        { nameof(lblTotalPointsCaption),            "Počet bodů:" },
                        { nameof(lblTagCaption),                    "Doplňující informace:" },
                        { nameof(lblRefIDLayerPointsCaption),       "Bodová vrstva:" },
                        { nameof(lblRefIDTotalCaption),             "Úhrny pomocné proměnné:" },
                        { nameof(lblEditDateCaption),               "Datum editace:" },
                        { nameof(strClosedTrue),                    "ano" },
                        { nameof(strClosedFalse),                   "ne" },
                        { nameof(strAggregatedTrue),                "ano" },
                        { nameof(strAggregatedFalse),               "ne" }
                    }
                    : languageFile.NationalVersion.Data.TryGetValue(
                        key: nameof(ControlConfigCollectionView),
                        out Dictionary<string, string> dictNational)
                            ? dictNational
                            : [],

                LanguageVersion.International => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(grpCaption),                       String.Empty  },
                        { nameof(grpConfigCollectionParameters),    "Auxiliary data parameters:"  },

                        { nameof(lblIdCaption),                     "ID:" },
                        { nameof(lblAuxiliaryVariableIdCaption),    "Auxiliary variable:" },
                        { nameof(lblConfigFunctionIdCaption),       "Function for calculation:" },
                        { nameof(lblLabelCaption),                  "Name:" },
                        { nameof(lblDescriptionCaption),            "Description:" },
                        { nameof(lblClosedCaption),                 "Locked group:" },
                        { nameof(lblAggregatedCaption),             "Aggregated group:" },
                        { nameof(lblCatalogNameCaption),            "Catalog:" },
                        { nameof(lblSchemaNameCaption),             "Schema:" },
                        { nameof(lblTableNameCaption),              "Table:" },
                        { nameof(lblColumnIdentCaption),            "Identifier:" },
                        { nameof(lblColumnNameCaption),             "Attribute:" },
                        { nameof(lblUnitCaption),                   "Coefficient:" },
                        { nameof(lblConditionCaption),              "Condition:" },
                        { nameof(lblTotalPointsCaption),            "Number of points:" },
                        { nameof(lblTagCaption),                    "Additional information:" },
                        { nameof(lblRefIDLayerPointsCaption),       "Point layer:" },
                        { nameof(lblRefIDTotalCaption),             "Auxiliary variable totals:" },
                        { nameof(lblEditDateCaption),               "Edit date:" },
                        { nameof(strClosedTrue),                    "yes" },
                        { nameof(strClosedFalse),                   "no" },
                        { nameof(strAggregatedTrue),                "yes" },
                        { nameof(strAggregatedFalse),               "no" }
                    }
                    : languageFile.InternationalVersion.Data.TryGetValue(
                        key: nameof(ControlConfigCollectionView),
                        out Dictionary<string, string> dictInternational)
                            ? dictInternational
                            : [],

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku</para>
        /// <para lang="en">Initialization of the control</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        private void Initialize(Control controlOwner)
        {
            BorderStyle = BorderStyle.None;

            ControlOwner = controlOwner;

            ConfigCollection = null;

            LoadContent();
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků ovládacího prvku</para>
        /// <para lang="en">Initialization of control labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
               languageVersion: LanguageVersion,
               languageFile: LanguageFile);

            lblCaption.Text =
                        (ConfigCollection != null)
                        ? (LanguageVersion == LanguageVersion.International)
                            ? ConfigCollection.DescriptionEn
                            : (LanguageVersion == LanguageVersion.National)
                                ? ConfigCollection.DescriptionCs
                                : ConfigCollection.DescriptionEn
                        : String.Empty;

            #region Captions

            grpCaption.Text =
               labels.TryGetValue(key: nameof(grpCaption),
               out string grpCaptionText)
                   ? grpCaptionText
                   : String.Empty;

            grpConfigCollectionParameters.Text =
                labels.TryGetValue(key: nameof(grpConfigCollectionParameters),
                out string grpConfigCollectionParametersText)
                    ? grpConfigCollectionParametersText
                    : String.Empty;

            lblIdCaption.Text =
                labels.TryGetValue(key: nameof(lblIdCaption),
                out string lblIdCaptionText)
                    ? lblIdCaptionText
                    : String.Empty;

            lblAuxiliaryVariableIdCaption.Text =
                labels.TryGetValue(key: nameof(lblAuxiliaryVariableIdCaption),
                out string lblAuxiliaryVariableIdCaptionText)
                    ? lblAuxiliaryVariableIdCaptionText
                    : String.Empty;

            lblConfigFunctionIdCaption.Text =
                labels.TryGetValue(key: nameof(lblConfigFunctionIdCaption),
                out string lblConfigFunctionIdCaptionText)
                    ? lblConfigFunctionIdCaptionText
                    : String.Empty;

            lblLabelCaption.Text =
                labels.TryGetValue(key: nameof(lblLabelCaption),
                out string lblLabelCaptionText)
                    ? lblLabelCaptionText
                    : String.Empty;

            lblDescriptionCaption.Text =
                labels.TryGetValue(key: nameof(lblDescriptionCaption),
                out string lblDescriptionCaptionText)
                    ? lblDescriptionCaptionText
                    : String.Empty;

            lblClosedCaption.Text =
                labels.TryGetValue(key: nameof(lblClosedCaption),
                out string lblClosedCaptionText)
                    ? lblClosedCaptionText
                    : String.Empty;

            lblAggregatedCaption.Text =
                labels.TryGetValue(key: nameof(lblAggregatedCaption),
                out string lblAggregatedCaptionText)
                    ? lblAggregatedCaptionText
                    : String.Empty;

            lblCatalogNameCaption.Text =
               labels.TryGetValue(key: nameof(lblCatalogNameCaption),
               out string lblCatalogNameCaptionText)
                   ? lblCatalogNameCaptionText
                   : String.Empty;

            lblSchemaNameCaption.Text =
               labels.TryGetValue(key: nameof(lblSchemaNameCaption),
               out string lblSchemaNameCaptionText)
                   ? lblSchemaNameCaptionText
                   : String.Empty;

            lblTableNameCaption.Text =
               labels.TryGetValue(key: nameof(lblTableNameCaption),
               out string lblTableNameCaptionText)
                   ? lblTableNameCaptionText
                   : String.Empty;

            lblColumnIdentCaption.Text =
               labels.TryGetValue(key: nameof(lblColumnIdentCaption),
               out string lblColumnIdentCaptionText)
                   ? lblColumnIdentCaptionText
                   : String.Empty;

            lblColumnNameCaption.Text =
               labels.TryGetValue(key: nameof(lblColumnNameCaption),
               out string lblColumnNameCaptionText)
                   ? lblColumnNameCaptionText
                   : String.Empty;

            lblUnitCaption.Text =
               labels.TryGetValue(key: nameof(lblUnitCaption),
               out string lblUnitCaptionText)
                   ? lblUnitCaptionText
                   : String.Empty;

            lblConditionCaption.Text =
               labels.TryGetValue(key: nameof(lblConditionCaption),
               out string lblConditionCaptionText)
                   ? lblConditionCaptionText
                   : String.Empty;

            lblTotalPointsCaption.Text =
               labels.TryGetValue(key: nameof(lblTotalPointsCaption),
               out string lblTotalPointsCaptionText)
                   ? lblTotalPointsCaptionText
                   : String.Empty;

            lblTagCaption.Text =
               labels.TryGetValue(key: nameof(lblTagCaption),
               out string lblTagCaptionText)
                   ? lblTagCaptionText
                   : String.Empty;

            lblRefIDLayerPointsCaption.Text =
               labels.TryGetValue(key: nameof(lblRefIDLayerPointsCaption),
               out string lblRefIDLayerPointsCaptionText)
                   ? lblRefIDLayerPointsCaptionText
                   : String.Empty;

            lblRefIDTotalCaption.Text =
               labels.TryGetValue(key: nameof(lblRefIDTotalCaption),
               out string lblRefIDTotalCaptionText)
                   ? lblRefIDTotalCaptionText
                   : String.Empty;

            lblEditDateCaption.Text =
               labels.TryGetValue(key: nameof(lblEditDateCaption),
               out string lblEditDateCaptionText)
                   ? lblEditDateCaptionText
                   : String.Empty;

            strClosedTrue =
                labels.TryGetValue(key: nameof(strClosedTrue),
                out strClosedTrue)
                   ? strClosedTrue
                   : String.Empty;

            strClosedFalse =
                labels.TryGetValue(key: nameof(strClosedFalse),
                out strClosedFalse)
                   ? strClosedFalse
                   : String.Empty;

            strAggregatedTrue =
                labels.TryGetValue(key: nameof(strAggregatedTrue),
                out strAggregatedTrue)
                   ? strAggregatedTrue
                   : String.Empty;

            strAggregatedFalse =
                labels.TryGetValue(key: nameof(strAggregatedFalse),
                out strAggregatedFalse)
                   ? strAggregatedFalse
                   : String.Empty;

            #endregion Captions

            #region Values

            if (ConfigCollection != null)
            {
                lblIdValue.Text =
                    ConfigCollection.Id.ToString();

                lblAuxiliaryVariableIdValue.Text =
                    (ConfigCollection.AuxiliaryVariable != null)
                    ? (LanguageVersion == LanguageVersion.National)
                        ? ConfigCollection.AuxiliaryVariable.ExtendedLabelCs ?? String.Empty
                        : ConfigCollection.AuxiliaryVariable.ExtendedLabelEn ?? String.Empty
                    : (ConfigCollection.AuxiliaryVariableId != null)
                        ? ConfigCollection.AuxiliaryVariableId.ToString()
                        : String.Empty;

                lblConfigFunctionIdValue.Text =
                    (ConfigCollection.ConfigFunction != null)
                    ? (LanguageVersion == LanguageVersion.National)
                        ? ConfigCollection.ConfigFunction.ExtendedLabelCs ?? String.Empty
                        : ConfigCollection.ConfigFunction.ExtendedLabelEn ?? String.Empty
                    : ConfigCollection.ConfigFunctionId.ToString();

                lblLabelValue.Text =
                    (LanguageVersion == LanguageVersion.National)
                    ? ConfigCollection.LabelCs ?? String.Empty
                    : ConfigCollection.LabelEn ?? String.Empty;

                lblDescriptionValue.Text =
                    (LanguageVersion == LanguageVersion.National)
                    ? ConfigCollection.DescriptionCs ?? String.Empty
                    : ConfigCollection.DescriptionEn ?? String.Empty;

                lblClosedValue.Text = ConfigCollection.Closed
                    ? strClosedTrue
                    : strClosedFalse;

                lblAggregatedValue.Text = ConfigCollection.Aggregated
                    ? strAggregatedTrue
                    : strAggregatedFalse;

                lblCatalogNameValue.Text =
                    ConfigCollection.CatalogName ?? String.Empty;

                lblSchemaNameValue.Text =
                    ConfigCollection.SchemaName ?? String.Empty;

                lblTableNameValue.Text =
                    ConfigCollection.TableName ?? String.Empty;

                lblColumnIdentValue.Text =
                    ConfigCollection.ColumnIdent ?? String.Empty;

                lblColumnNameValue.Text =
                    ConfigCollection.ColumnName ?? String.Empty;

                lblUnitValue.Text =
                    (ConfigCollection.Unit != null)
                    ? ConfigCollection.Unit.ToString()
                    : String.Empty;

                lblConditionValue.Text =
                    ConfigCollection.Condition ?? String.Empty;

                lblTotalPointsValue.Text =
                    (ConfigCollection.TotalPoints != null)
                    ? ConfigCollection.TotalPoints.ToString()
                    : String.Empty;

                lblTagValue.Text =
                    ConfigCollection.Tag ?? String.Empty;

                lblRefIDLayerPointsValue.Text =
                    (ConfigCollection.RefLayerPoints != null)
                    ? (LanguageVersion == LanguageVersion.National)
                        ? ConfigCollection.RefLayerPoints.ExtendedLabelCs ?? String.Empty
                        : ConfigCollection.RefLayerPoints.ExtendedLabelEn ?? String.Empty
                    : (ConfigCollection.RefLayerPoints != null)
                        ? ConfigCollection.RefLayerPoints.ToString()
                        : String.Empty;

                lblRefIDTotalValue.Text =
                   (ConfigCollection.RefTotal != null)
                    ? (LanguageVersion == LanguageVersion.National)
                        ? ConfigCollection.RefTotal.ExtendedLabelCs ?? String.Empty
                        : ConfigCollection.RefTotal.ExtendedLabelEn ?? String.Empty
                    : (ConfigCollection.RefTotal != null)
                        ? ConfigCollection.RefTotal.ToString()
                        : String.Empty;

                lblEditDateValue.Text =
                    ConfigCollection.EditDateText ?? String.Empty;
            }
            else
            {
                lblIdValue.Text = String.Empty;
                lblAuxiliaryVariableIdValue.Text = String.Empty;
                lblConfigFunctionIdValue.Text = String.Empty;
                lblLabelValue.Text = String.Empty;
                lblDescriptionValue.Text = String.Empty;
                lblClosedValue.Text = String.Empty;
                lblAggregatedValue.Text = String.Empty;
                lblCatalogNameValue.Text = String.Empty;
                lblSchemaNameValue.Text = String.Empty;
                lblTableNameValue.Text = String.Empty;
                lblColumnIdentValue.Text = String.Empty;
                lblColumnNameValue.Text = String.Empty;
                lblUnitValue.Text = String.Empty;
                lblConditionValue.Text = String.Empty;
                lblTotalPointsValue.Text = String.Empty;
                lblTagValue.Text = String.Empty;
                lblRefIDLayerPointsValue.Text = String.Empty;
                lblRefIDTotalValue.Text = String.Empty;
                lblEditDateValue.Text = String.Empty;
            }

            #endregion Values
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent() { }

        /// <summary>
        /// <para lang="cs">Nastaví výšky řádku v TableLayoutPanel</para>
        /// <para lang="en">Sets TableLayoutPanel row heights</para>
        /// </summary>
        private void SetRowHeights()
        {
            if (ConfigCollection == null)
            {
                tlpConfigCollectionParameters.Visible = false;
                return;
            }

            tlpConfigCollectionParameters.Visible = false;

            List<int> visibleRowIndexes =
                ConfigCollection.Aggregated switch
                {
                    false =>
                         ConfigCollection.ConfigFunctionValue switch
                         {
                             ConfigFunctionEnum.VectorTotal => [0, 1, 2, 3, 5, 6, 7, 8, 9, 10, 11, 12, 18],
                             ConfigFunctionEnum.RasterTotal => [0, 1, 2, 3, 5, 6, 7, 8, 9, 10, 11, 12, 18],
                             ConfigFunctionEnum.RasterTotalWithinVector => [0, 1, 2, 3, 5, 6, 18],
                             ConfigFunctionEnum.RastersProductTotal => [0, 1, 2, 3, 5, 6, 18],
                             ConfigFunctionEnum.PointLayer => [0, 2, 3, 5, 6, 7, 8, 9, 11, 13, 14, 15, 18],
                             ConfigFunctionEnum.PointTotalCombination => [0, 2, 3, 5, 6, 16, 17, 18],
                             _ => [],
                         },
                    true =>
                          ConfigCollection.ConfigFunctionValue switch
                          {
                              ConfigFunctionEnum.VectorTotal => [0, 1, 2, 3, 5, 6, 18],
                              ConfigFunctionEnum.RasterTotal => [0, 1, 2, 3, 5, 6, 18],
                              ConfigFunctionEnum.RasterTotalWithinVector => [0, 1, 2, 3, 5, 6, 18],
                              ConfigFunctionEnum.RastersProductTotal => [0, 1, 2, 3, 5, 6, 18],
                              ConfigFunctionEnum.PointLayer => [0, 2, 3, 5, 6, 7, 8, 9, 11, 13, 14, 15, 18],
                              ConfigFunctionEnum.PointTotalCombination => [0, 2, 3, 5, 6, 16, 17, 18],
                              _ => [],
                          }
                };

            if (ConfigCollection.AuxiliaryVariableId == null)
            {
                visibleRowIndexes.Remove(item: 1);
            }

            for (int i = 0; i < tlpConfigCollectionParameters.RowStyles.Count; i++)
            {
                if (visibleRowIndexes.Contains(item: i))
                {
                    tlpConfigCollectionParameters.RowStyles[i].Height = rowHeight;
                }
                else
                {
                    tlpConfigCollectionParameters.RowStyles[i].Height = 0;
                }
            }
            tlpConfigCollectionParameters.Height =
                tlpConfigCollectionParameters.RowStyles
                .OfType<RowStyle>()
                .Select(a => (int)a.Height).Sum();

            tlpConfigCollectionParameters.Visible = true;
        }

        #endregion Methods

    }

}
