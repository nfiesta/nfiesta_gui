﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi.ModuleAuxiliaryData
{

    partial class FormAuxiliaryVariableCategorySelector
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormAuxiliaryVariableCategorySelector));
            tlpMain = new System.Windows.Forms.TableLayoutPanel();
            grpItems = new System.Windows.Forms.GroupBox();
            tvwItems = new System.Windows.Forms.TreeView();
            ilTreeView = new System.Windows.Forms.ImageList(components);
            grpSelectedItem = new System.Windows.Forms.GroupBox();
            lblSelectedItem = new System.Windows.Forms.Label();
            tlpButtons = new System.Windows.Forms.TableLayoutPanel();
            pnlNext = new System.Windows.Forms.Panel();
            btnCancel = new System.Windows.Forms.Button();
            pnlPrevious = new System.Windows.Forms.Panel();
            btnOK = new System.Windows.Forms.Button();
            tlpMain.SuspendLayout();
            grpItems.SuspendLayout();
            grpSelectedItem.SuspendLayout();
            tlpButtons.SuspendLayout();
            pnlNext.SuspendLayout();
            pnlPrevious.SuspendLayout();
            SuspendLayout();
            // 
            // tlpMain
            // 
            tlpMain.ColumnCount = 1;
            tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.Controls.Add(grpItems, 0, 1);
            tlpMain.Controls.Add(grpSelectedItem, 0, 0);
            tlpMain.Controls.Add(tlpButtons, 0, 2);
            tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpMain.Location = new System.Drawing.Point(0, 0);
            tlpMain.Margin = new System.Windows.Forms.Padding(0);
            tlpMain.Name = "tlpMain";
            tlpMain.RowCount = 3;
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpMain.Size = new System.Drawing.Size(944, 501);
            tlpMain.TabIndex = 5;
            // 
            // grpItems
            // 
            grpItems.Controls.Add(tvwItems);
            grpItems.Dock = System.Windows.Forms.DockStyle.Fill;
            grpItems.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            grpItems.ForeColor = System.Drawing.Color.MediumBlue;
            grpItems.Location = new System.Drawing.Point(0, 60);
            grpItems.Margin = new System.Windows.Forms.Padding(0);
            grpItems.Name = "grpItems";
            grpItems.Padding = new System.Windows.Forms.Padding(5);
            grpItems.Size = new System.Drawing.Size(944, 401);
            grpItems.TabIndex = 15;
            grpItems.TabStop = false;
            grpItems.Text = "grpItems";
            // 
            // tvwItems
            // 
            tvwItems.BackColor = System.Drawing.Color.White;
            tvwItems.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            tvwItems.CheckBoxes = true;
            tvwItems.Dock = System.Windows.Forms.DockStyle.Fill;
            tvwItems.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            tvwItems.ForeColor = System.Drawing.Color.Black;
            tvwItems.HideSelection = false;
            tvwItems.ImageIndex = 3;
            tvwItems.ImageList = ilTreeView;
            tvwItems.LineColor = System.Drawing.Color.MediumBlue;
            tvwItems.Location = new System.Drawing.Point(5, 25);
            tvwItems.Margin = new System.Windows.Forms.Padding(0);
            tvwItems.Name = "tvwItems";
            tvwItems.SelectedImageIndex = 3;
            tvwItems.ShowLines = false;
            tvwItems.ShowRootLines = false;
            tvwItems.Size = new System.Drawing.Size(934, 371);
            tvwItems.TabIndex = 5;
            // 
            // ilTreeView
            // 
            ilTreeView.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            ilTreeView.ImageStream = (System.Windows.Forms.ImageListStreamer)resources.GetObject("ilTreeView.ImageStream");
            ilTreeView.TransparentColor = System.Drawing.Color.Transparent;
            ilTreeView.Images.SetKeyName(0, "RedBall");
            ilTreeView.Images.SetKeyName(1, "BlueBall");
            ilTreeView.Images.SetKeyName(2, "YellowBall");
            ilTreeView.Images.SetKeyName(3, "GreenBall");
            ilTreeView.Images.SetKeyName(4, "GrayBall");
            ilTreeView.Images.SetKeyName(5, "Lock");
            ilTreeView.Images.SetKeyName(6, "Schema");
            ilTreeView.Images.SetKeyName(7, "Vector");
            ilTreeView.Images.SetKeyName(8, "Raster");
            // 
            // grpSelectedItem
            // 
            grpSelectedItem.Controls.Add(lblSelectedItem);
            grpSelectedItem.Dock = System.Windows.Forms.DockStyle.Fill;
            grpSelectedItem.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            grpSelectedItem.ForeColor = System.Drawing.Color.MediumBlue;
            grpSelectedItem.Location = new System.Drawing.Point(0, 0);
            grpSelectedItem.Margin = new System.Windows.Forms.Padding(0);
            grpSelectedItem.Name = "grpSelectedItem";
            grpSelectedItem.Padding = new System.Windows.Forms.Padding(5);
            grpSelectedItem.Size = new System.Drawing.Size(944, 60);
            grpSelectedItem.TabIndex = 17;
            grpSelectedItem.TabStop = false;
            grpSelectedItem.Text = "grpSelectedItem";
            // 
            // lblSelectedItem
            // 
            lblSelectedItem.Dock = System.Windows.Forms.DockStyle.Fill;
            lblSelectedItem.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, 238);
            lblSelectedItem.ForeColor = System.Drawing.Color.Black;
            lblSelectedItem.Location = new System.Drawing.Point(5, 25);
            lblSelectedItem.Margin = new System.Windows.Forms.Padding(0);
            lblSelectedItem.Name = "lblSelectedItem";
            lblSelectedItem.Size = new System.Drawing.Size(934, 30);
            lblSelectedItem.TabIndex = 0;
            lblSelectedItem.Text = "lblSelectedItem";
            lblSelectedItem.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tlpButtons
            // 
            tlpButtons.BackColor = System.Drawing.Color.Transparent;
            tlpButtons.ColumnCount = 3;
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            tlpButtons.Controls.Add(pnlNext, 2, 0);
            tlpButtons.Controls.Add(pnlPrevious, 1, 0);
            tlpButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpButtons.Location = new System.Drawing.Point(0, 461);
            tlpButtons.Margin = new System.Windows.Forms.Padding(0);
            tlpButtons.Name = "tlpButtons";
            tlpButtons.RowCount = 1;
            tlpButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.Size = new System.Drawing.Size(944, 40);
            tlpButtons.TabIndex = 16;
            // 
            // pnlNext
            // 
            pnlNext.Controls.Add(btnCancel);
            pnlNext.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlNext.Location = new System.Drawing.Point(784, 0);
            pnlNext.Margin = new System.Windows.Forms.Padding(0);
            pnlNext.Name = "pnlNext";
            pnlNext.Padding = new System.Windows.Forms.Padding(5);
            pnlNext.Size = new System.Drawing.Size(160, 40);
            pnlNext.TabIndex = 13;
            // 
            // btnCancel
            // 
            btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            btnCancel.Dock = System.Windows.Forms.DockStyle.Fill;
            btnCancel.ForeColor = System.Drawing.Color.Black;
            btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            btnCancel.Location = new System.Drawing.Point(5, 5);
            btnCancel.Margin = new System.Windows.Forms.Padding(0);
            btnCancel.Name = "btnCancel";
            btnCancel.Size = new System.Drawing.Size(150, 30);
            btnCancel.TabIndex = 9;
            btnCancel.Text = "btnCancel";
            btnCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            btnCancel.UseVisualStyleBackColor = true;
            // 
            // pnlPrevious
            // 
            pnlPrevious.Controls.Add(btnOK);
            pnlPrevious.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlPrevious.Location = new System.Drawing.Point(624, 0);
            pnlPrevious.Margin = new System.Windows.Forms.Padding(0);
            pnlPrevious.Name = "pnlPrevious";
            pnlPrevious.Padding = new System.Windows.Forms.Padding(5);
            pnlPrevious.Size = new System.Drawing.Size(160, 40);
            pnlPrevious.TabIndex = 14;
            // 
            // btnOK
            // 
            btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            btnOK.Dock = System.Windows.Forms.DockStyle.Fill;
            btnOK.ForeColor = System.Drawing.Color.Black;
            btnOK.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            btnOK.Location = new System.Drawing.Point(5, 5);
            btnOK.Margin = new System.Windows.Forms.Padding(0);
            btnOK.Name = "btnOK";
            btnOK.Size = new System.Drawing.Size(150, 30);
            btnOK.TabIndex = 9;
            btnOK.Text = "btnOK";
            btnOK.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            btnOK.UseVisualStyleBackColor = true;
            // 
            // FormAuxiliaryVariableCategorySelector
            // 
            AcceptButton = btnOK;
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            BackColor = System.Drawing.Color.WhiteSmoke;
            CancelButton = btnCancel;
            ClientSize = new System.Drawing.Size(944, 501);
            Controls.Add(tlpMain);
            ForeColor = System.Drawing.Color.Black;
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "FormAuxiliaryVariableCategorySelector";
            ShowIcon = false;
            ShowInTaskbar = false;
            StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            Text = "FormAuxiliaryVariableCategorySelector";
            tlpMain.ResumeLayout(false);
            grpItems.ResumeLayout(false);
            grpSelectedItem.ResumeLayout(false);
            tlpButtons.ResumeLayout(false);
            pnlNext.ResumeLayout(false);
            pnlPrevious.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.GroupBox grpItems;
        private System.Windows.Forms.TreeView tvwItems;
        private System.Windows.Forms.GroupBox grpSelectedItem;
        private System.Windows.Forms.Label lblSelectedItem;
        private System.Windows.Forms.TableLayoutPanel tlpButtons;
        private System.Windows.Forms.Panel pnlNext;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Panel pnlPrevious;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.ImageList ilTreeView;
    }

}