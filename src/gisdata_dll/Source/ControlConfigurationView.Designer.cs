﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi.ModuleAuxiliaryData
{

    partial class ControlConfigurationView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            tlpMain = new System.Windows.Forms.TableLayoutPanel();
            grpCaption = new System.Windows.Forms.GroupBox();
            lblCaption = new System.Windows.Forms.Label();
            grpConfigurationParameters = new System.Windows.Forms.GroupBox();
            pnlConfigurationParameters = new System.Windows.Forms.Panel();
            tlpConfigurationParameters = new System.Windows.Forms.TableLayoutPanel();
            lblEditDateValue = new System.Windows.Forms.Label();
            lblRowNumber17 = new System.Windows.Forms.Label();
            lblRowNumber16 = new System.Windows.Forms.Label();
            lblRowNumber15 = new System.Windows.Forms.Label();
            lblRowNumber14 = new System.Windows.Forms.Label();
            lblRowNumber13 = new System.Windows.Forms.Label();
            lblRowNumber12 = new System.Windows.Forms.Label();
            lblRowNumber11 = new System.Windows.Forms.Label();
            lblRowNumber10 = new System.Windows.Forms.Label();
            lblRowNumber09 = new System.Windows.Forms.Label();
            lblRowNumber08 = new System.Windows.Forms.Label();
            lblRowNumber07 = new System.Windows.Forms.Label();
            lblRowNumber06 = new System.Windows.Forms.Label();
            lblAuxDataCountValue = new System.Windows.Forms.Label();
            lblAuxDataCountCaption = new System.Windows.Forms.Label();
            lblAuxTotalCountValue = new System.Windows.Forms.Label();
            lblAuxTotalCountCaption = new System.Windows.Forms.Label();
            lblCompleteCaption = new System.Windows.Forms.Label();
            lblCategoriesCaption = new System.Windows.Forms.Label();
            lblRasterBValue = new System.Windows.Forms.Label();
            lblRasterBCaption = new System.Windows.Forms.Label();
            lblRasterAValue = new System.Windows.Forms.Label();
            lblRasterACaption = new System.Windows.Forms.Label();
            lblVectorValue = new System.Windows.Forms.Label();
            lblVectorCaption = new System.Windows.Forms.Label();
            lblReclassValue = new System.Windows.Forms.Label();
            lblReclassCaption = new System.Windows.Forms.Label();
            lblBandValue = new System.Windows.Forms.Label();
            lblBandCaption = new System.Windows.Forms.Label();
            lblConditionValue = new System.Windows.Forms.Label();
            lblConditionCaption = new System.Windows.Forms.Label();
            lblTagValue = new System.Windows.Forms.Label();
            lblTagCaption = new System.Windows.Forms.Label();
            lblDescriptionValue = new System.Windows.Forms.Label();
            lblDescriptionCaption = new System.Windows.Forms.Label();
            lblLabelValue = new System.Windows.Forms.Label();
            lblLabelCaption = new System.Windows.Forms.Label();
            lblConfigQueryIdValue = new System.Windows.Forms.Label();
            lblConfigQueryIdCaption = new System.Windows.Forms.Label();
            lblConfigCollectionIdValue = new System.Windows.Forms.Label();
            lblConfigCollectionIdCaption = new System.Windows.Forms.Label();
            lblAuxiliaryVariableCategoryIdValue = new System.Windows.Forms.Label();
            lblAuxiliaryVariableCategoryIdCaption = new System.Windows.Forms.Label();
            lblIdValue = new System.Windows.Forms.Label();
            lblIdCaption = new System.Windows.Forms.Label();
            lblRowNumber00 = new System.Windows.Forms.Label();
            lblRowNumber01 = new System.Windows.Forms.Label();
            lblRowNumber02 = new System.Windows.Forms.Label();
            lblRowNumber03 = new System.Windows.Forms.Label();
            lblRowNumber04 = new System.Windows.Forms.Label();
            lblRowNumber05 = new System.Windows.Forms.Label();
            lblEditDateCaption = new System.Windows.Forms.Label();
            pnlCategories = new System.Windows.Forms.Panel();
            pnlComplete = new System.Windows.Forms.Panel();
            tlpMain.SuspendLayout();
            grpCaption.SuspendLayout();
            grpConfigurationParameters.SuspendLayout();
            pnlConfigurationParameters.SuspendLayout();
            tlpConfigurationParameters.SuspendLayout();
            SuspendLayout();
            // 
            // tlpMain
            // 
            tlpMain.ColumnCount = 1;
            tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.Controls.Add(grpCaption, 0, 0);
            tlpMain.Controls.Add(grpConfigurationParameters, 0, 1);
            tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpMain.Location = new System.Drawing.Point(0, 0);
            tlpMain.Margin = new System.Windows.Forms.Padding(0);
            tlpMain.Name = "tlpMain";
            tlpMain.RowCount = 2;
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpMain.Size = new System.Drawing.Size(960, 540);
            tlpMain.TabIndex = 2;
            // 
            // grpCaption
            // 
            grpCaption.Controls.Add(lblCaption);
            grpCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            grpCaption.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            grpCaption.ForeColor = System.Drawing.Color.MediumBlue;
            grpCaption.Location = new System.Drawing.Point(0, 0);
            grpCaption.Margin = new System.Windows.Forms.Padding(0);
            grpCaption.Name = "grpCaption";
            grpCaption.Padding = new System.Windows.Forms.Padding(5);
            grpCaption.Size = new System.Drawing.Size(960, 60);
            grpCaption.TabIndex = 13;
            grpCaption.TabStop = false;
            grpCaption.Text = "grpCaption";
            // 
            // lblCaption
            // 
            lblCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblCaption.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, 0);
            lblCaption.Location = new System.Drawing.Point(5, 25);
            lblCaption.Margin = new System.Windows.Forms.Padding(0);
            lblCaption.Name = "lblCaption";
            lblCaption.Size = new System.Drawing.Size(950, 30);
            lblCaption.TabIndex = 0;
            lblCaption.Text = "lblCaption";
            lblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // grpConfigurationParameters
            // 
            grpConfigurationParameters.Controls.Add(pnlConfigurationParameters);
            grpConfigurationParameters.Dock = System.Windows.Forms.DockStyle.Fill;
            grpConfigurationParameters.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            grpConfigurationParameters.ForeColor = System.Drawing.Color.MediumBlue;
            grpConfigurationParameters.Location = new System.Drawing.Point(0, 60);
            grpConfigurationParameters.Margin = new System.Windows.Forms.Padding(0);
            grpConfigurationParameters.Name = "grpConfigurationParameters";
            grpConfigurationParameters.Padding = new System.Windows.Forms.Padding(5);
            grpConfigurationParameters.Size = new System.Drawing.Size(960, 480);
            grpConfigurationParameters.TabIndex = 14;
            grpConfigurationParameters.TabStop = false;
            grpConfigurationParameters.Text = "grpConfigurationParameters";
            // 
            // pnlConfigurationParameters
            // 
            pnlConfigurationParameters.AutoScroll = true;
            pnlConfigurationParameters.Controls.Add(tlpConfigurationParameters);
            pnlConfigurationParameters.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlConfigurationParameters.Font = new System.Drawing.Font("Segoe UI", 9F);
            pnlConfigurationParameters.ForeColor = System.Drawing.Color.Black;
            pnlConfigurationParameters.Location = new System.Drawing.Point(5, 25);
            pnlConfigurationParameters.Margin = new System.Windows.Forms.Padding(0);
            pnlConfigurationParameters.Name = "pnlConfigurationParameters";
            pnlConfigurationParameters.Size = new System.Drawing.Size(950, 450);
            pnlConfigurationParameters.TabIndex = 1;
            // 
            // tlpConfigurationParameters
            // 
            tlpConfigurationParameters.ColumnCount = 3;
            tlpConfigurationParameters.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            tlpConfigurationParameters.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 300F));
            tlpConfigurationParameters.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpConfigurationParameters.Controls.Add(lblEditDateValue, 2, 17);
            tlpConfigurationParameters.Controls.Add(lblRowNumber17, 0, 17);
            tlpConfigurationParameters.Controls.Add(lblRowNumber16, 0, 16);
            tlpConfigurationParameters.Controls.Add(lblRowNumber15, 0, 15);
            tlpConfigurationParameters.Controls.Add(lblRowNumber14, 0, 14);
            tlpConfigurationParameters.Controls.Add(lblRowNumber13, 0, 13);
            tlpConfigurationParameters.Controls.Add(lblRowNumber12, 0, 12);
            tlpConfigurationParameters.Controls.Add(lblRowNumber11, 0, 11);
            tlpConfigurationParameters.Controls.Add(lblRowNumber10, 0, 10);
            tlpConfigurationParameters.Controls.Add(lblRowNumber09, 0, 9);
            tlpConfigurationParameters.Controls.Add(lblRowNumber08, 0, 8);
            tlpConfigurationParameters.Controls.Add(lblRowNumber07, 0, 7);
            tlpConfigurationParameters.Controls.Add(lblRowNumber06, 0, 6);
            tlpConfigurationParameters.Controls.Add(lblAuxDataCountValue, 2, 16);
            tlpConfigurationParameters.Controls.Add(lblAuxDataCountCaption, 1, 16);
            tlpConfigurationParameters.Controls.Add(lblAuxTotalCountValue, 2, 15);
            tlpConfigurationParameters.Controls.Add(lblAuxTotalCountCaption, 1, 15);
            tlpConfigurationParameters.Controls.Add(lblCompleteCaption, 1, 14);
            tlpConfigurationParameters.Controls.Add(lblCategoriesCaption, 1, 13);
            tlpConfigurationParameters.Controls.Add(lblRasterBValue, 2, 12);
            tlpConfigurationParameters.Controls.Add(lblRasterBCaption, 1, 12);
            tlpConfigurationParameters.Controls.Add(lblRasterAValue, 2, 11);
            tlpConfigurationParameters.Controls.Add(lblRasterACaption, 1, 11);
            tlpConfigurationParameters.Controls.Add(lblVectorValue, 2, 10);
            tlpConfigurationParameters.Controls.Add(lblVectorCaption, 1, 10);
            tlpConfigurationParameters.Controls.Add(lblReclassValue, 2, 9);
            tlpConfigurationParameters.Controls.Add(lblReclassCaption, 1, 9);
            tlpConfigurationParameters.Controls.Add(lblBandValue, 2, 8);
            tlpConfigurationParameters.Controls.Add(lblBandCaption, 1, 8);
            tlpConfigurationParameters.Controls.Add(lblConditionValue, 2, 7);
            tlpConfigurationParameters.Controls.Add(lblConditionCaption, 1, 7);
            tlpConfigurationParameters.Controls.Add(lblTagValue, 2, 6);
            tlpConfigurationParameters.Controls.Add(lblTagCaption, 1, 6);
            tlpConfigurationParameters.Controls.Add(lblDescriptionValue, 2, 5);
            tlpConfigurationParameters.Controls.Add(lblDescriptionCaption, 1, 5);
            tlpConfigurationParameters.Controls.Add(lblLabelValue, 2, 4);
            tlpConfigurationParameters.Controls.Add(lblLabelCaption, 1, 4);
            tlpConfigurationParameters.Controls.Add(lblConfigQueryIdValue, 2, 3);
            tlpConfigurationParameters.Controls.Add(lblConfigQueryIdCaption, 1, 3);
            tlpConfigurationParameters.Controls.Add(lblConfigCollectionIdValue, 2, 2);
            tlpConfigurationParameters.Controls.Add(lblConfigCollectionIdCaption, 1, 2);
            tlpConfigurationParameters.Controls.Add(lblAuxiliaryVariableCategoryIdValue, 2, 1);
            tlpConfigurationParameters.Controls.Add(lblAuxiliaryVariableCategoryIdCaption, 1, 1);
            tlpConfigurationParameters.Controls.Add(lblIdValue, 2, 0);
            tlpConfigurationParameters.Controls.Add(lblIdCaption, 1, 0);
            tlpConfigurationParameters.Controls.Add(lblRowNumber00, 0, 0);
            tlpConfigurationParameters.Controls.Add(lblRowNumber01, 0, 1);
            tlpConfigurationParameters.Controls.Add(lblRowNumber02, 0, 2);
            tlpConfigurationParameters.Controls.Add(lblRowNumber03, 0, 3);
            tlpConfigurationParameters.Controls.Add(lblRowNumber04, 0, 4);
            tlpConfigurationParameters.Controls.Add(lblRowNumber05, 0, 5);
            tlpConfigurationParameters.Controls.Add(lblEditDateCaption, 1, 17);
            tlpConfigurationParameters.Controls.Add(pnlCategories, 2, 13);
            tlpConfigurationParameters.Controls.Add(pnlComplete, 2, 14);
            tlpConfigurationParameters.Dock = System.Windows.Forms.DockStyle.Top;
            tlpConfigurationParameters.Location = new System.Drawing.Point(0, 0);
            tlpConfigurationParameters.Margin = new System.Windows.Forms.Padding(0);
            tlpConfigurationParameters.Name = "tlpConfigurationParameters";
            tlpConfigurationParameters.RowCount = 18;
            tlpConfigurationParameters.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpConfigurationParameters.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpConfigurationParameters.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpConfigurationParameters.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpConfigurationParameters.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpConfigurationParameters.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpConfigurationParameters.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpConfigurationParameters.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpConfigurationParameters.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpConfigurationParameters.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpConfigurationParameters.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpConfigurationParameters.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpConfigurationParameters.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpConfigurationParameters.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpConfigurationParameters.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpConfigurationParameters.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpConfigurationParameters.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpConfigurationParameters.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpConfigurationParameters.Size = new System.Drawing.Size(950, 360);
            tlpConfigurationParameters.TabIndex = 3;
            // 
            // lblEditDateValue
            // 
            lblEditDateValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblEditDateValue.Location = new System.Drawing.Point(325, 340);
            lblEditDateValue.Margin = new System.Windows.Forms.Padding(0);
            lblEditDateValue.Name = "lblEditDateValue";
            lblEditDateValue.Size = new System.Drawing.Size(625, 20);
            lblEditDateValue.TabIndex = 34;
            lblEditDateValue.Text = "lblEditDateValue";
            // 
            // lblRowNumber17
            // 
            lblRowNumber17.Dock = System.Windows.Forms.DockStyle.Fill;
            lblRowNumber17.ForeColor = System.Drawing.Color.MediumBlue;
            lblRowNumber17.Location = new System.Drawing.Point(0, 340);
            lblRowNumber17.Margin = new System.Windows.Forms.Padding(0);
            lblRowNumber17.Name = "lblRowNumber17";
            lblRowNumber17.Size = new System.Drawing.Size(25, 20);
            lblRowNumber17.TabIndex = 55;
            lblRowNumber17.Text = "17.";
            lblRowNumber17.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblRowNumber16
            // 
            lblRowNumber16.Dock = System.Windows.Forms.DockStyle.Fill;
            lblRowNumber16.ForeColor = System.Drawing.Color.MediumBlue;
            lblRowNumber16.Location = new System.Drawing.Point(0, 320);
            lblRowNumber16.Margin = new System.Windows.Forms.Padding(0);
            lblRowNumber16.Name = "lblRowNumber16";
            lblRowNumber16.Size = new System.Drawing.Size(25, 20);
            lblRowNumber16.TabIndex = 54;
            lblRowNumber16.Text = "16.";
            lblRowNumber16.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblRowNumber15
            // 
            lblRowNumber15.Dock = System.Windows.Forms.DockStyle.Fill;
            lblRowNumber15.ForeColor = System.Drawing.Color.MediumBlue;
            lblRowNumber15.Location = new System.Drawing.Point(0, 300);
            lblRowNumber15.Margin = new System.Windows.Forms.Padding(0);
            lblRowNumber15.Name = "lblRowNumber15";
            lblRowNumber15.Size = new System.Drawing.Size(25, 20);
            lblRowNumber15.TabIndex = 53;
            lblRowNumber15.Text = "15.";
            lblRowNumber15.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblRowNumber14
            // 
            lblRowNumber14.Dock = System.Windows.Forms.DockStyle.Fill;
            lblRowNumber14.ForeColor = System.Drawing.Color.MediumBlue;
            lblRowNumber14.Location = new System.Drawing.Point(0, 280);
            lblRowNumber14.Margin = new System.Windows.Forms.Padding(0);
            lblRowNumber14.Name = "lblRowNumber14";
            lblRowNumber14.Size = new System.Drawing.Size(25, 20);
            lblRowNumber14.TabIndex = 52;
            lblRowNumber14.Text = "14.";
            lblRowNumber14.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblRowNumber13
            // 
            lblRowNumber13.Dock = System.Windows.Forms.DockStyle.Fill;
            lblRowNumber13.ForeColor = System.Drawing.Color.MediumBlue;
            lblRowNumber13.Location = new System.Drawing.Point(0, 260);
            lblRowNumber13.Margin = new System.Windows.Forms.Padding(0);
            lblRowNumber13.Name = "lblRowNumber13";
            lblRowNumber13.Size = new System.Drawing.Size(25, 20);
            lblRowNumber13.TabIndex = 51;
            lblRowNumber13.Text = "13.";
            lblRowNumber13.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblRowNumber12
            // 
            lblRowNumber12.Dock = System.Windows.Forms.DockStyle.Fill;
            lblRowNumber12.ForeColor = System.Drawing.Color.MediumBlue;
            lblRowNumber12.Location = new System.Drawing.Point(0, 240);
            lblRowNumber12.Margin = new System.Windows.Forms.Padding(0);
            lblRowNumber12.Name = "lblRowNumber12";
            lblRowNumber12.Size = new System.Drawing.Size(25, 20);
            lblRowNumber12.TabIndex = 50;
            lblRowNumber12.Text = "12.";
            lblRowNumber12.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblRowNumber11
            // 
            lblRowNumber11.Dock = System.Windows.Forms.DockStyle.Fill;
            lblRowNumber11.ForeColor = System.Drawing.Color.MediumBlue;
            lblRowNumber11.Location = new System.Drawing.Point(0, 220);
            lblRowNumber11.Margin = new System.Windows.Forms.Padding(0);
            lblRowNumber11.Name = "lblRowNumber11";
            lblRowNumber11.Size = new System.Drawing.Size(25, 20);
            lblRowNumber11.TabIndex = 49;
            lblRowNumber11.Text = "11.";
            lblRowNumber11.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblRowNumber10
            // 
            lblRowNumber10.Dock = System.Windows.Forms.DockStyle.Fill;
            lblRowNumber10.ForeColor = System.Drawing.Color.MediumBlue;
            lblRowNumber10.Location = new System.Drawing.Point(0, 200);
            lblRowNumber10.Margin = new System.Windows.Forms.Padding(0);
            lblRowNumber10.Name = "lblRowNumber10";
            lblRowNumber10.Size = new System.Drawing.Size(25, 20);
            lblRowNumber10.TabIndex = 48;
            lblRowNumber10.Text = "10.";
            lblRowNumber10.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblRowNumber09
            // 
            lblRowNumber09.Dock = System.Windows.Forms.DockStyle.Fill;
            lblRowNumber09.ForeColor = System.Drawing.Color.MediumBlue;
            lblRowNumber09.Location = new System.Drawing.Point(0, 180);
            lblRowNumber09.Margin = new System.Windows.Forms.Padding(0);
            lblRowNumber09.Name = "lblRowNumber09";
            lblRowNumber09.Size = new System.Drawing.Size(25, 20);
            lblRowNumber09.TabIndex = 47;
            lblRowNumber09.Text = "9.";
            lblRowNumber09.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblRowNumber08
            // 
            lblRowNumber08.Dock = System.Windows.Forms.DockStyle.Fill;
            lblRowNumber08.ForeColor = System.Drawing.Color.MediumBlue;
            lblRowNumber08.Location = new System.Drawing.Point(0, 160);
            lblRowNumber08.Margin = new System.Windows.Forms.Padding(0);
            lblRowNumber08.Name = "lblRowNumber08";
            lblRowNumber08.Size = new System.Drawing.Size(25, 20);
            lblRowNumber08.TabIndex = 46;
            lblRowNumber08.Text = "8.";
            lblRowNumber08.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblRowNumber07
            // 
            lblRowNumber07.Dock = System.Windows.Forms.DockStyle.Fill;
            lblRowNumber07.ForeColor = System.Drawing.Color.MediumBlue;
            lblRowNumber07.Location = new System.Drawing.Point(0, 140);
            lblRowNumber07.Margin = new System.Windows.Forms.Padding(0);
            lblRowNumber07.Name = "lblRowNumber07";
            lblRowNumber07.Size = new System.Drawing.Size(25, 20);
            lblRowNumber07.TabIndex = 45;
            lblRowNumber07.Text = "7.";
            lblRowNumber07.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblRowNumber06
            // 
            lblRowNumber06.Dock = System.Windows.Forms.DockStyle.Fill;
            lblRowNumber06.ForeColor = System.Drawing.Color.MediumBlue;
            lblRowNumber06.Location = new System.Drawing.Point(0, 120);
            lblRowNumber06.Margin = new System.Windows.Forms.Padding(0);
            lblRowNumber06.Name = "lblRowNumber06";
            lblRowNumber06.Size = new System.Drawing.Size(25, 20);
            lblRowNumber06.TabIndex = 44;
            lblRowNumber06.Text = "6.";
            lblRowNumber06.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblAuxDataCountValue
            // 
            lblAuxDataCountValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblAuxDataCountValue.Location = new System.Drawing.Point(325, 320);
            lblAuxDataCountValue.Margin = new System.Windows.Forms.Padding(0);
            lblAuxDataCountValue.Name = "lblAuxDataCountValue";
            lblAuxDataCountValue.Size = new System.Drawing.Size(625, 20);
            lblAuxDataCountValue.TabIndex = 33;
            lblAuxDataCountValue.Text = "lblAuxDataCountValue";
            // 
            // lblAuxDataCountCaption
            // 
            lblAuxDataCountCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblAuxDataCountCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblAuxDataCountCaption.Location = new System.Drawing.Point(25, 320);
            lblAuxDataCountCaption.Margin = new System.Windows.Forms.Padding(0);
            lblAuxDataCountCaption.Name = "lblAuxDataCountCaption";
            lblAuxDataCountCaption.Size = new System.Drawing.Size(300, 20);
            lblAuxDataCountCaption.TabIndex = 32;
            lblAuxDataCountCaption.Text = "lblAuxDataCountCaption";
            // 
            // lblAuxTotalCountValue
            // 
            lblAuxTotalCountValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblAuxTotalCountValue.Location = new System.Drawing.Point(325, 300);
            lblAuxTotalCountValue.Margin = new System.Windows.Forms.Padding(0);
            lblAuxTotalCountValue.Name = "lblAuxTotalCountValue";
            lblAuxTotalCountValue.Size = new System.Drawing.Size(625, 20);
            lblAuxTotalCountValue.TabIndex = 31;
            lblAuxTotalCountValue.Text = "lblAuxTotalCountValue";
            // 
            // lblAuxTotalCountCaption
            // 
            lblAuxTotalCountCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblAuxTotalCountCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblAuxTotalCountCaption.Location = new System.Drawing.Point(25, 300);
            lblAuxTotalCountCaption.Margin = new System.Windows.Forms.Padding(0);
            lblAuxTotalCountCaption.Name = "lblAuxTotalCountCaption";
            lblAuxTotalCountCaption.Size = new System.Drawing.Size(300, 20);
            lblAuxTotalCountCaption.TabIndex = 30;
            lblAuxTotalCountCaption.Text = "lblAuxTotalCountCaption";
            // 
            // lblCompleteCaption
            // 
            lblCompleteCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblCompleteCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblCompleteCaption.Location = new System.Drawing.Point(25, 280);
            lblCompleteCaption.Margin = new System.Windows.Forms.Padding(0);
            lblCompleteCaption.Name = "lblCompleteCaption";
            lblCompleteCaption.Size = new System.Drawing.Size(300, 20);
            lblCompleteCaption.TabIndex = 28;
            lblCompleteCaption.Text = "lblCompleteCaption";
            // 
            // lblCategoriesCaption
            // 
            lblCategoriesCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblCategoriesCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblCategoriesCaption.Location = new System.Drawing.Point(25, 260);
            lblCategoriesCaption.Margin = new System.Windows.Forms.Padding(0);
            lblCategoriesCaption.Name = "lblCategoriesCaption";
            lblCategoriesCaption.Size = new System.Drawing.Size(300, 20);
            lblCategoriesCaption.TabIndex = 26;
            lblCategoriesCaption.Text = "lblCategoriesCaption";
            // 
            // lblRasterBValue
            // 
            lblRasterBValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblRasterBValue.Location = new System.Drawing.Point(325, 240);
            lblRasterBValue.Margin = new System.Windows.Forms.Padding(0);
            lblRasterBValue.Name = "lblRasterBValue";
            lblRasterBValue.Size = new System.Drawing.Size(625, 20);
            lblRasterBValue.TabIndex = 25;
            lblRasterBValue.Text = "lblRasterBValue";
            // 
            // lblRasterBCaption
            // 
            lblRasterBCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblRasterBCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblRasterBCaption.Location = new System.Drawing.Point(25, 240);
            lblRasterBCaption.Margin = new System.Windows.Forms.Padding(0);
            lblRasterBCaption.Name = "lblRasterBCaption";
            lblRasterBCaption.Size = new System.Drawing.Size(300, 20);
            lblRasterBCaption.TabIndex = 24;
            lblRasterBCaption.Text = "lblRasterBCaption";
            // 
            // lblRasterAValue
            // 
            lblRasterAValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblRasterAValue.Location = new System.Drawing.Point(325, 220);
            lblRasterAValue.Margin = new System.Windows.Forms.Padding(0);
            lblRasterAValue.Name = "lblRasterAValue";
            lblRasterAValue.Size = new System.Drawing.Size(625, 20);
            lblRasterAValue.TabIndex = 23;
            lblRasterAValue.Text = "lblRasterAValue";
            // 
            // lblRasterACaption
            // 
            lblRasterACaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblRasterACaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblRasterACaption.Location = new System.Drawing.Point(25, 220);
            lblRasterACaption.Margin = new System.Windows.Forms.Padding(0);
            lblRasterACaption.Name = "lblRasterACaption";
            lblRasterACaption.Size = new System.Drawing.Size(300, 20);
            lblRasterACaption.TabIndex = 22;
            lblRasterACaption.Text = "lblRasterACaption";
            // 
            // lblVectorValue
            // 
            lblVectorValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblVectorValue.Location = new System.Drawing.Point(325, 200);
            lblVectorValue.Margin = new System.Windows.Forms.Padding(0);
            lblVectorValue.Name = "lblVectorValue";
            lblVectorValue.Size = new System.Drawing.Size(625, 20);
            lblVectorValue.TabIndex = 21;
            lblVectorValue.Text = "lblVectorValue";
            // 
            // lblVectorCaption
            // 
            lblVectorCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblVectorCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblVectorCaption.Location = new System.Drawing.Point(25, 200);
            lblVectorCaption.Margin = new System.Windows.Forms.Padding(0);
            lblVectorCaption.Name = "lblVectorCaption";
            lblVectorCaption.Size = new System.Drawing.Size(300, 20);
            lblVectorCaption.TabIndex = 20;
            lblVectorCaption.Text = "lblVectorCaption";
            // 
            // lblReclassValue
            // 
            lblReclassValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblReclassValue.Location = new System.Drawing.Point(325, 180);
            lblReclassValue.Margin = new System.Windows.Forms.Padding(0);
            lblReclassValue.Name = "lblReclassValue";
            lblReclassValue.Size = new System.Drawing.Size(625, 20);
            lblReclassValue.TabIndex = 19;
            lblReclassValue.Text = "lblReclassValue";
            // 
            // lblReclassCaption
            // 
            lblReclassCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblReclassCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblReclassCaption.Location = new System.Drawing.Point(25, 180);
            lblReclassCaption.Margin = new System.Windows.Forms.Padding(0);
            lblReclassCaption.Name = "lblReclassCaption";
            lblReclassCaption.Size = new System.Drawing.Size(300, 20);
            lblReclassCaption.TabIndex = 18;
            lblReclassCaption.Text = "lblReclassCaption";
            // 
            // lblBandValue
            // 
            lblBandValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblBandValue.Location = new System.Drawing.Point(325, 160);
            lblBandValue.Margin = new System.Windows.Forms.Padding(0);
            lblBandValue.Name = "lblBandValue";
            lblBandValue.Size = new System.Drawing.Size(625, 20);
            lblBandValue.TabIndex = 17;
            lblBandValue.Text = "lblBandValue";
            // 
            // lblBandCaption
            // 
            lblBandCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblBandCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblBandCaption.Location = new System.Drawing.Point(25, 160);
            lblBandCaption.Margin = new System.Windows.Forms.Padding(0);
            lblBandCaption.Name = "lblBandCaption";
            lblBandCaption.Size = new System.Drawing.Size(300, 20);
            lblBandCaption.TabIndex = 16;
            lblBandCaption.Text = "lblBandCaption";
            // 
            // lblConditionValue
            // 
            lblConditionValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblConditionValue.Location = new System.Drawing.Point(325, 140);
            lblConditionValue.Margin = new System.Windows.Forms.Padding(0);
            lblConditionValue.Name = "lblConditionValue";
            lblConditionValue.Size = new System.Drawing.Size(625, 20);
            lblConditionValue.TabIndex = 15;
            lblConditionValue.Text = "lblConditionValue";
            // 
            // lblConditionCaption
            // 
            lblConditionCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblConditionCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblConditionCaption.Location = new System.Drawing.Point(25, 140);
            lblConditionCaption.Margin = new System.Windows.Forms.Padding(0);
            lblConditionCaption.Name = "lblConditionCaption";
            lblConditionCaption.Size = new System.Drawing.Size(300, 20);
            lblConditionCaption.TabIndex = 14;
            lblConditionCaption.Text = "lblConditionCaption";
            // 
            // lblTagValue
            // 
            lblTagValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblTagValue.Location = new System.Drawing.Point(325, 120);
            lblTagValue.Margin = new System.Windows.Forms.Padding(0);
            lblTagValue.Name = "lblTagValue";
            lblTagValue.Size = new System.Drawing.Size(625, 20);
            lblTagValue.TabIndex = 13;
            lblTagValue.Text = "lblTagValue";
            // 
            // lblTagCaption
            // 
            lblTagCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblTagCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblTagCaption.Location = new System.Drawing.Point(25, 120);
            lblTagCaption.Margin = new System.Windows.Forms.Padding(0);
            lblTagCaption.Name = "lblTagCaption";
            lblTagCaption.Size = new System.Drawing.Size(300, 20);
            lblTagCaption.TabIndex = 12;
            lblTagCaption.Text = "lblTagCaption";
            // 
            // lblDescriptionValue
            // 
            lblDescriptionValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblDescriptionValue.Location = new System.Drawing.Point(325, 100);
            lblDescriptionValue.Margin = new System.Windows.Forms.Padding(0);
            lblDescriptionValue.Name = "lblDescriptionValue";
            lblDescriptionValue.Size = new System.Drawing.Size(625, 20);
            lblDescriptionValue.TabIndex = 11;
            lblDescriptionValue.Text = "lblDescriptionValue";
            // 
            // lblDescriptionCaption
            // 
            lblDescriptionCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblDescriptionCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblDescriptionCaption.Location = new System.Drawing.Point(25, 100);
            lblDescriptionCaption.Margin = new System.Windows.Forms.Padding(0);
            lblDescriptionCaption.Name = "lblDescriptionCaption";
            lblDescriptionCaption.Size = new System.Drawing.Size(300, 20);
            lblDescriptionCaption.TabIndex = 10;
            lblDescriptionCaption.Text = "lblDescriptionCaption";
            // 
            // lblLabelValue
            // 
            lblLabelValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblLabelValue.Location = new System.Drawing.Point(325, 80);
            lblLabelValue.Margin = new System.Windows.Forms.Padding(0);
            lblLabelValue.Name = "lblLabelValue";
            lblLabelValue.Size = new System.Drawing.Size(625, 20);
            lblLabelValue.TabIndex = 9;
            lblLabelValue.Text = "lblLabelValue";
            // 
            // lblLabelCaption
            // 
            lblLabelCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblLabelCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblLabelCaption.Location = new System.Drawing.Point(25, 80);
            lblLabelCaption.Margin = new System.Windows.Forms.Padding(0);
            lblLabelCaption.Name = "lblLabelCaption";
            lblLabelCaption.Size = new System.Drawing.Size(300, 20);
            lblLabelCaption.TabIndex = 8;
            lblLabelCaption.Text = "lblLabelCaption";
            // 
            // lblConfigQueryIdValue
            // 
            lblConfigQueryIdValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblConfigQueryIdValue.Location = new System.Drawing.Point(325, 60);
            lblConfigQueryIdValue.Margin = new System.Windows.Forms.Padding(0);
            lblConfigQueryIdValue.Name = "lblConfigQueryIdValue";
            lblConfigQueryIdValue.Size = new System.Drawing.Size(625, 20);
            lblConfigQueryIdValue.TabIndex = 7;
            lblConfigQueryIdValue.Text = "lblConfigQueryIdValue";
            // 
            // lblConfigQueryIdCaption
            // 
            lblConfigQueryIdCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblConfigQueryIdCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblConfigQueryIdCaption.Location = new System.Drawing.Point(25, 60);
            lblConfigQueryIdCaption.Margin = new System.Windows.Forms.Padding(0);
            lblConfigQueryIdCaption.Name = "lblConfigQueryIdCaption";
            lblConfigQueryIdCaption.Size = new System.Drawing.Size(300, 20);
            lblConfigQueryIdCaption.TabIndex = 6;
            lblConfigQueryIdCaption.Text = "lblConfigQueryIdCaption";
            // 
            // lblConfigCollectionIdValue
            // 
            lblConfigCollectionIdValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblConfigCollectionIdValue.Location = new System.Drawing.Point(325, 40);
            lblConfigCollectionIdValue.Margin = new System.Windows.Forms.Padding(0);
            lblConfigCollectionIdValue.Name = "lblConfigCollectionIdValue";
            lblConfigCollectionIdValue.Size = new System.Drawing.Size(625, 20);
            lblConfigCollectionIdValue.TabIndex = 5;
            lblConfigCollectionIdValue.Text = "lblConfigCollectionIdValue";
            // 
            // lblConfigCollectionIdCaption
            // 
            lblConfigCollectionIdCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblConfigCollectionIdCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblConfigCollectionIdCaption.Location = new System.Drawing.Point(25, 40);
            lblConfigCollectionIdCaption.Margin = new System.Windows.Forms.Padding(0);
            lblConfigCollectionIdCaption.Name = "lblConfigCollectionIdCaption";
            lblConfigCollectionIdCaption.Size = new System.Drawing.Size(300, 20);
            lblConfigCollectionIdCaption.TabIndex = 4;
            lblConfigCollectionIdCaption.Text = "lblConfigCollectionIdCaption";
            // 
            // lblAuxiliaryVariableCategoryIdValue
            // 
            lblAuxiliaryVariableCategoryIdValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblAuxiliaryVariableCategoryIdValue.Location = new System.Drawing.Point(325, 20);
            lblAuxiliaryVariableCategoryIdValue.Margin = new System.Windows.Forms.Padding(0);
            lblAuxiliaryVariableCategoryIdValue.Name = "lblAuxiliaryVariableCategoryIdValue";
            lblAuxiliaryVariableCategoryIdValue.Size = new System.Drawing.Size(625, 20);
            lblAuxiliaryVariableCategoryIdValue.TabIndex = 3;
            lblAuxiliaryVariableCategoryIdValue.Text = "lblAuxiliaryVariableCategoryIdValue";
            // 
            // lblAuxiliaryVariableCategoryIdCaption
            // 
            lblAuxiliaryVariableCategoryIdCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblAuxiliaryVariableCategoryIdCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblAuxiliaryVariableCategoryIdCaption.Location = new System.Drawing.Point(25, 20);
            lblAuxiliaryVariableCategoryIdCaption.Margin = new System.Windows.Forms.Padding(0);
            lblAuxiliaryVariableCategoryIdCaption.Name = "lblAuxiliaryVariableCategoryIdCaption";
            lblAuxiliaryVariableCategoryIdCaption.Size = new System.Drawing.Size(300, 20);
            lblAuxiliaryVariableCategoryIdCaption.TabIndex = 2;
            lblAuxiliaryVariableCategoryIdCaption.Text = "lblAuxiliaryVariableCategoryIdCaption";
            // 
            // lblIdValue
            // 
            lblIdValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblIdValue.Location = new System.Drawing.Point(325, 0);
            lblIdValue.Margin = new System.Windows.Forms.Padding(0);
            lblIdValue.Name = "lblIdValue";
            lblIdValue.Size = new System.Drawing.Size(625, 20);
            lblIdValue.TabIndex = 1;
            lblIdValue.Text = "lblIdValue";
            // 
            // lblIdCaption
            // 
            lblIdCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblIdCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblIdCaption.Location = new System.Drawing.Point(25, 0);
            lblIdCaption.Margin = new System.Windows.Forms.Padding(0);
            lblIdCaption.Name = "lblIdCaption";
            lblIdCaption.Size = new System.Drawing.Size(300, 20);
            lblIdCaption.TabIndex = 0;
            lblIdCaption.Text = "lblIdCaption";
            // 
            // lblRowNumber00
            // 
            lblRowNumber00.Dock = System.Windows.Forms.DockStyle.Fill;
            lblRowNumber00.ForeColor = System.Drawing.Color.MediumBlue;
            lblRowNumber00.Location = new System.Drawing.Point(0, 0);
            lblRowNumber00.Margin = new System.Windows.Forms.Padding(0);
            lblRowNumber00.Name = "lblRowNumber00";
            lblRowNumber00.Size = new System.Drawing.Size(25, 20);
            lblRowNumber00.TabIndex = 38;
            lblRowNumber00.Tag = "";
            lblRowNumber00.Text = "0.";
            lblRowNumber00.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblRowNumber01
            // 
            lblRowNumber01.Dock = System.Windows.Forms.DockStyle.Fill;
            lblRowNumber01.ForeColor = System.Drawing.Color.MediumBlue;
            lblRowNumber01.Location = new System.Drawing.Point(0, 20);
            lblRowNumber01.Margin = new System.Windows.Forms.Padding(0);
            lblRowNumber01.Name = "lblRowNumber01";
            lblRowNumber01.Size = new System.Drawing.Size(25, 20);
            lblRowNumber01.TabIndex = 39;
            lblRowNumber01.Text = "1.";
            lblRowNumber01.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblRowNumber02
            // 
            lblRowNumber02.Dock = System.Windows.Forms.DockStyle.Fill;
            lblRowNumber02.ForeColor = System.Drawing.Color.MediumBlue;
            lblRowNumber02.Location = new System.Drawing.Point(0, 40);
            lblRowNumber02.Margin = new System.Windows.Forms.Padding(0);
            lblRowNumber02.Name = "lblRowNumber02";
            lblRowNumber02.Size = new System.Drawing.Size(25, 20);
            lblRowNumber02.TabIndex = 40;
            lblRowNumber02.Text = "2.";
            lblRowNumber02.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblRowNumber03
            // 
            lblRowNumber03.Dock = System.Windows.Forms.DockStyle.Fill;
            lblRowNumber03.ForeColor = System.Drawing.Color.MediumBlue;
            lblRowNumber03.Location = new System.Drawing.Point(0, 60);
            lblRowNumber03.Margin = new System.Windows.Forms.Padding(0);
            lblRowNumber03.Name = "lblRowNumber03";
            lblRowNumber03.Size = new System.Drawing.Size(25, 20);
            lblRowNumber03.TabIndex = 41;
            lblRowNumber03.Text = "3.";
            lblRowNumber03.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblRowNumber04
            // 
            lblRowNumber04.Dock = System.Windows.Forms.DockStyle.Fill;
            lblRowNumber04.ForeColor = System.Drawing.Color.MediumBlue;
            lblRowNumber04.Location = new System.Drawing.Point(0, 80);
            lblRowNumber04.Margin = new System.Windows.Forms.Padding(0);
            lblRowNumber04.Name = "lblRowNumber04";
            lblRowNumber04.Size = new System.Drawing.Size(25, 20);
            lblRowNumber04.TabIndex = 42;
            lblRowNumber04.Text = "4.";
            lblRowNumber04.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblRowNumber05
            // 
            lblRowNumber05.Dock = System.Windows.Forms.DockStyle.Fill;
            lblRowNumber05.ForeColor = System.Drawing.Color.MediumBlue;
            lblRowNumber05.Location = new System.Drawing.Point(0, 100);
            lblRowNumber05.Margin = new System.Windows.Forms.Padding(0);
            lblRowNumber05.Name = "lblRowNumber05";
            lblRowNumber05.Size = new System.Drawing.Size(25, 20);
            lblRowNumber05.TabIndex = 43;
            lblRowNumber05.Text = "5.";
            lblRowNumber05.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblEditDateCaption
            // 
            lblEditDateCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblEditDateCaption.Location = new System.Drawing.Point(25, 340);
            lblEditDateCaption.Margin = new System.Windows.Forms.Padding(0);
            lblEditDateCaption.Name = "lblEditDateCaption";
            lblEditDateCaption.Size = new System.Drawing.Size(300, 20);
            lblEditDateCaption.TabIndex = 56;
            lblEditDateCaption.Text = "lblEditDateCaption";
            // 
            // pnlCategories
            // 
            pnlCategories.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlCategories.Location = new System.Drawing.Point(325, 260);
            pnlCategories.Margin = new System.Windows.Forms.Padding(0);
            pnlCategories.Name = "pnlCategories";
            pnlCategories.Size = new System.Drawing.Size(625, 20);
            pnlCategories.TabIndex = 57;
            // 
            // pnlComplete
            // 
            pnlComplete.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlComplete.Location = new System.Drawing.Point(325, 280);
            pnlComplete.Margin = new System.Windows.Forms.Padding(0);
            pnlComplete.Name = "pnlComplete";
            pnlComplete.Size = new System.Drawing.Size(625, 20);
            pnlComplete.TabIndex = 58;
            // 
            // ControlConfigurationView
            // 
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            BackColor = System.Drawing.Color.Transparent;
            BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            Controls.Add(tlpMain);
            ForeColor = System.Drawing.Color.Black;
            Margin = new System.Windows.Forms.Padding(0);
            Name = "ControlConfigurationView";
            Size = new System.Drawing.Size(960, 540);
            tlpMain.ResumeLayout(false);
            grpCaption.ResumeLayout(false);
            grpConfigurationParameters.ResumeLayout(false);
            pnlConfigurationParameters.ResumeLayout(false);
            tlpConfigurationParameters.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.GroupBox grpCaption;
        private System.Windows.Forms.Label lblCaption;
        private System.Windows.Forms.GroupBox grpConfigurationParameters;
        private System.Windows.Forms.Panel pnlConfigurationParameters;
        private System.Windows.Forms.TableLayoutPanel tlpConfigurationParameters;
        private System.Windows.Forms.Label lblRowNumber16;
        private System.Windows.Forms.Label lblRowNumber15;
        private System.Windows.Forms.Label lblRowNumber14;
        private System.Windows.Forms.Label lblRowNumber13;
        private System.Windows.Forms.Label lblRowNumber12;
        private System.Windows.Forms.Label lblRowNumber11;
        private System.Windows.Forms.Label lblRowNumber10;
        private System.Windows.Forms.Label lblRowNumber09;
        private System.Windows.Forms.Label lblRowNumber08;
        private System.Windows.Forms.Label lblRowNumber07;
        private System.Windows.Forms.Label lblRowNumber06;
        private System.Windows.Forms.Label lblAuxDataCountValue;
        private System.Windows.Forms.Label lblAuxDataCountCaption;
        private System.Windows.Forms.Label lblAuxTotalCountValue;
        private System.Windows.Forms.Label lblAuxTotalCountCaption;
        private System.Windows.Forms.Label lblCompleteCaption;
        private System.Windows.Forms.Label lblCategoriesCaption;
        private System.Windows.Forms.Label lblRasterBValue;
        private System.Windows.Forms.Label lblRasterBCaption;
        private System.Windows.Forms.Label lblRasterAValue;
        private System.Windows.Forms.Label lblRasterACaption;
        private System.Windows.Forms.Label lblVectorValue;
        private System.Windows.Forms.Label lblVectorCaption;
        private System.Windows.Forms.Label lblReclassValue;
        private System.Windows.Forms.Label lblReclassCaption;
        private System.Windows.Forms.Label lblBandValue;
        private System.Windows.Forms.Label lblBandCaption;
        private System.Windows.Forms.Label lblConditionValue;
        private System.Windows.Forms.Label lblConditionCaption;
        private System.Windows.Forms.Label lblTagValue;
        private System.Windows.Forms.Label lblTagCaption;
        private System.Windows.Forms.Label lblDescriptionValue;
        private System.Windows.Forms.Label lblDescriptionCaption;
        private System.Windows.Forms.Label lblLabelValue;
        private System.Windows.Forms.Label lblLabelCaption;
        private System.Windows.Forms.Label lblConfigQueryIdValue;
        private System.Windows.Forms.Label lblConfigQueryIdCaption;
        private System.Windows.Forms.Label lblConfigCollectionIdValue;
        private System.Windows.Forms.Label lblConfigCollectionIdCaption;
        private System.Windows.Forms.Label lblAuxiliaryVariableCategoryIdValue;
        private System.Windows.Forms.Label lblAuxiliaryVariableCategoryIdCaption;
        private System.Windows.Forms.Label lblIdValue;
        private System.Windows.Forms.Label lblIdCaption;
        private System.Windows.Forms.Label lblRowNumber00;
        private System.Windows.Forms.Label lblRowNumber01;
        private System.Windows.Forms.Label lblRowNumber02;
        private System.Windows.Forms.Label lblRowNumber03;
        private System.Windows.Forms.Label lblRowNumber04;
        private System.Windows.Forms.Label lblRowNumber05;
        private System.Windows.Forms.Label lblRowNumber17;
        private System.Windows.Forms.Label lblEditDateCaption;
        private System.Windows.Forms.Label lblEditDateValue;
        private System.Windows.Forms.Panel pnlCategories;
        private System.Windows.Forms.Panel pnlComplete;
    }

}
