﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.AuxiliaryData;
using ZaJi.PostgreSQL;

namespace ZaJi.ModuleAuxiliaryData
{

    /// <summary>
    /// <para lang="cs">Zobrazení parametrů konfigurace</para>
    /// <para lang="en">Control "Display configuration parameters"</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    internal partial class ControlConfigurationView
        : UserControl, INfiEstaControl, IAuxiliaryDataControl
    {

        #region Constants

        /// <summary>
        /// <para lang="cs">Výška řádku v tabulce</para>
        /// <para lang="en">Table layout panel row height</para>
        /// </summary>
        private const int rowHeight = 20;

        /// <summary>
        /// <para lang="cs">Šířka prvního sloupce v tabulce</para>
        /// <para lang="en">Table layout panel first column width</para>
        /// </summary>
        private const int colRowNumberWidth = 25;

        #endregion Constants


        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Konfigurace výpočtu úhrnu</para>
        /// <para lang="en">Configuration for total calculation</para>
        /// </summary>
        private Config configuration;

        #endregion Private Fields


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor ovládacího prvku</para>
        /// <para lang="en">Control constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        public ControlConfigurationView(Control controlOwner)
        {
            InitializeComponent();
            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                IAuxiliaryDataControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                return controlOwner;
            }
            set
            {
                IAuxiliaryDataControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables (read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((IAuxiliaryDataControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro pomocná data (read-only)</para>
        /// <para lang="en">Module for auxiliary data setting (read-only)</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((IAuxiliaryDataControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">Konfigurace výpočtu úhrnu</para>
        /// <para lang="en">Configuration for total calculation</para>
        /// </summary>
        public Config Configuration
        {
            get
            {
                return configuration;
            }
            set
            {
                configuration = value;

                InitializeLabels();

                SetRowHeights();
            }
        }

        /// <summary>
        /// <para lang="cs">Zobrazovat čísla řádků</para>
        /// <para lang="en">Display row numbers</para>
        /// </summary>
        public bool RowNumbers
        {
            get
            {
                return
                    tlpConfigurationParameters.ColumnStyles[0].Width != 0;
            }
            set
            {
                tlpConfigurationParameters.ColumnStyles[0].Width =
                    value
                    ? colRowNumberWidth
                    : 0;
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(grpCaption),                             String.Empty  },
                        { nameof(grpConfigurationParameters),             "Parametry veličiny:"  },
                        { nameof(lblIdCaption),                           "ID:" },
                        { nameof(lblAuxiliaryVariableCategoryIdCaption),  "Kategorie pomocné proměnné:" },
                        { nameof(lblConfigCollectionIdCaption),           "Skupina konfigurací:" },
                        { nameof(lblConfigQueryIdCaption),                "Typ výpočtu:" },
                        { nameof(lblLabelCaption),                        "Název:"},
                        { nameof(lblDescriptionCaption),                  "Popis:" },
                        { nameof(lblTagCaption),                          "Doplňující informace:"  },
                        { nameof(lblConditionCaption),                    "Výběrová podmínka:" },
                        { nameof(lblBandCaption),                         "Pásmo rastru:" },
                        { nameof(lblReclassCaption),                      "Reklasifikace rastru:" },
                        { nameof(lblVectorCaption),                       "Konfigurace vektorové vrstvy:" },
                        { nameof(lblRasterACaption),                      "Konfigurace rastrové vrstvy:" },
                        { nameof(lblRasterBCaption),                      "Konfigurace druhé rastrové vrstvy:" },
                        { nameof(lblCategoriesCaption),                   "Kategorie:" },
                        { nameof(lblCompleteCaption),                     "Celková kategorie:" },
                        { nameof(lblAuxTotalCountCaption),                "Výpočetní buňky s úhrny pomocné proměnné:" },
                        { nameof(lblAuxDataCountCaption),                 "Body s hodnotami pomocné proměnné:" },
                        { nameof(lblEditDateCaption),                     "Datum editace:"  }
                    }
                    : languageFile.NationalVersion.Data.TryGetValue(
                        key: nameof(ControlConfigurationView),
                        out Dictionary<string, string> dictNational)
                            ? dictNational
                            : [],

                LanguageVersion.International => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(grpCaption),                             String.Empty  },
                        { nameof(grpConfigurationParameters),             "Parameters of the variable:"   },
                        { nameof(lblIdCaption),                           "ID:" },
                        { nameof(lblAuxiliaryVariableCategoryIdCaption),  "Auxiliary variable category:" },
                        { nameof(lblConfigCollectionIdCaption),           "Configuration collection:" },
                        { nameof(lblConfigQueryIdCaption),                "Calculation type:" },
                        { nameof(lblLabelCaption),                        "Name:" },
                        { nameof(lblDescriptionCaption),                  "Description:" },
                        { nameof(lblTagCaption),                          "Additional information:" },
                        { nameof(lblConditionCaption),                    "Condition for select:" },
                        { nameof(lblBandCaption),                         "Raster band:" },
                        { nameof(lblReclassCaption),                      "Raster reclassification:" },
                        { nameof(lblVectorCaption),                       "Vector layer configuration:"  },
                        { nameof(lblRasterACaption),                      "Raster layer configuration:" },
                        { nameof(lblRasterBCaption),                      "Second raster layer configuration:" },
                        { nameof(lblCategoriesCaption),                   "Categories:" },
                        { nameof(lblCompleteCaption),                     "Total category:" },
                        { nameof(lblAuxTotalCountCaption),                "Estimation cells with auxiliary variable totals:"  },
                        { nameof(lblAuxDataCountCaption),                 "Points with auxiliary variable values:"  },
                        { nameof(lblEditDateCaption),                     "Edit date:" }
                    }
                    : languageFile.InternationalVersion.Data.TryGetValue(
                        key: nameof(ControlConfigurationView),
                        out Dictionary<string, string> dictInternational)
                            ? dictInternational
                            : [],

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku</para>
        /// <para lang="en">Initialization of the control</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        private void Initialize(Control controlOwner)
        {
            BorderStyle = BorderStyle.None;

            ControlOwner = controlOwner;

            Configuration = null;

            LoadContent();
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků ovládacího prvku</para>
        /// <para lang="en">Initialization of control labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
               languageVersion: LanguageVersion,
               languageFile: LanguageFile);

            lblCaption.Text =
                        (Configuration != null)
                        ? (LanguageVersion == LanguageVersion.International)
                            ? Configuration.DescriptionEn
                            : (LanguageVersion == LanguageVersion.National)
                                ? Configuration.DescriptionCs
                                : Configuration.DescriptionEn
                        : String.Empty;

            #region Captions

            grpCaption.Text =
                labels.TryGetValue(key: nameof(grpCaption),
                out string grpCaptionText)
                    ? grpCaptionText
                    : String.Empty;

            grpConfigurationParameters.Text =
                labels.TryGetValue(key: nameof(grpConfigurationParameters),
                out string grpConfigurationParametersText)
                    ? grpConfigurationParametersText
                    : String.Empty;

            lblIdCaption.Text =
                labels.TryGetValue(key: nameof(lblIdCaption),
                out string lblIdCaptionText)
                    ? lblIdCaptionText
                    : String.Empty;

            lblAuxiliaryVariableCategoryIdCaption.Text =
                labels.TryGetValue(key: nameof(lblAuxiliaryVariableCategoryIdCaption),
                out string lblAuxiliaryVariableCategoryIdCaptionText)
                    ? lblAuxiliaryVariableCategoryIdCaptionText
                    : String.Empty;

            lblConfigCollectionIdCaption.Text =
               labels.TryGetValue(key: nameof(lblConfigCollectionIdCaption),
               out string lblConfigCollectionIdCaptionText)
                   ? lblConfigCollectionIdCaptionText
                   : String.Empty;

            lblConfigQueryIdCaption.Text =
               labels.TryGetValue(key: nameof(lblConfigQueryIdCaption),
               out string lblConfigQueryIdCaptionText)
                   ? lblConfigQueryIdCaptionText
                   : String.Empty;

            lblLabelCaption.Text =
               labels.TryGetValue(key: nameof(lblLabelCaption),
               out string lblLabelCaptionText)
                   ? lblLabelCaptionText
                   : String.Empty;

            lblDescriptionCaption.Text =
               labels.TryGetValue(key: nameof(lblDescriptionCaption),
               out string lblDescriptionCaptionText)
                   ? lblDescriptionCaptionText
                   : String.Empty;

            lblTagCaption.Text =
               labels.TryGetValue(key: nameof(lblTagCaption),
               out string lblTagCaptionText)
                   ? lblTagCaptionText
                   : String.Empty;

            lblConditionCaption.Text =
               labels.TryGetValue(key: nameof(lblConditionCaption),
               out string lblConditionCaptionText)
                   ? lblConditionCaptionText
                   : String.Empty;

            lblBandCaption.Text =
               labels.TryGetValue(key: nameof(lblBandCaption),
               out string lblBandCaptionText)
                   ? lblBandCaptionText
                   : String.Empty;

            lblReclassCaption.Text =
               labels.TryGetValue(key: nameof(lblReclassCaption),
               out string lblReclassCaptionText)
                   ? lblReclassCaptionText
                   : String.Empty;

            lblVectorCaption.Text =
               labels.TryGetValue(key: nameof(lblVectorCaption),
               out string lblVectorCaptionText)
                   ? lblVectorCaptionText
                   : String.Empty;

            lblRasterACaption.Text =
               labels.TryGetValue(key: nameof(lblRasterACaption),
               out string lblRasterACaptionText)
                   ? lblRasterACaptionText
                   : String.Empty;

            lblRasterBCaption.Text =
               labels.TryGetValue(key: nameof(lblRasterBCaption),
               out string lblRasterBCaptionText)
                   ? lblRasterBCaptionText
                   : String.Empty;

            lblCategoriesCaption.Text =
               labels.TryGetValue(key: nameof(lblCategoriesCaption),
               out string lblCategoriesCaptionText)
                   ? lblCategoriesCaptionText
                   : String.Empty;

            lblCompleteCaption.Text =
               labels.TryGetValue(key: nameof(lblCompleteCaption),
               out string lblCompleteCaptionText)
                   ? lblCompleteCaptionText
                   : String.Empty;

            lblAuxTotalCountCaption.Text =
               labels.TryGetValue(key: nameof(lblAuxTotalCountCaption),
               out string lblAuxTotalCountCaptionText)
                   ? lblAuxTotalCountCaptionText
                   : String.Empty;

            lblAuxDataCountCaption.Text =
               labels.TryGetValue(key: nameof(lblAuxDataCountCaption),
               out string lblAuxDataCountCaptionText)
                   ? lblAuxDataCountCaptionText
                   : String.Empty;

            lblEditDateCaption.Text =
               labels.TryGetValue(key: nameof(lblEditDateCaption),
               out string lblEditDateCaptionText)
                   ? lblEditDateCaptionText
                   : String.Empty;

            #endregion Captions

            #region Values

            if (Configuration != null)
            {
                lblIdValue.Text =
                    Configuration.Id.ToString();

                lblAuxiliaryVariableCategoryIdValue.Text =
                    (Configuration.AuxiliaryVariableCategory != null)
                    ? (LanguageVersion == LanguageVersion.National)
                        ? Configuration.AuxiliaryVariableCategory.ExtendedLabelCs ?? String.Empty
                        : Configuration.AuxiliaryVariableCategory.ExtendedLabelEn ?? String.Empty
                    : (Configuration.AuxiliaryVariableCategoryId != null)
                        ? Configuration.AuxiliaryVariableCategoryId.ToString()
                        : String.Empty;

                lblConfigCollectionIdValue.Text =
                    (Configuration.ConfigCollection != null)
                    ? (LanguageVersion == LanguageVersion.National)
                        ? Configuration.ConfigCollection.ExtendedLabelCs ?? String.Empty
                        : Configuration.ConfigCollection.ExtendedLabelEn ?? String.Empty
                    : Configuration.ConfigCollectionId.ToString();

                lblConfigQueryIdValue.Text =
                    (Configuration.ConfigQuery != null)
                    ? (LanguageVersion == LanguageVersion.National)
                        ? Configuration.ConfigQuery.ExtendedLabelCs ?? String.Empty
                        : Configuration.ConfigQuery.ExtendedLabelEn ?? String.Empty
                    : Configuration.ConfigQueryId.ToString();

                lblLabelValue.Text =
                    (LanguageVersion == LanguageVersion.National)
                    ? Configuration.LabelCs ?? String.Empty
                    : Configuration.LabelEn ?? String.Empty;

                lblDescriptionValue.Text =
                    (LanguageVersion == LanguageVersion.National)
                    ? Configuration.DescriptionCs ?? String.Empty
                    : Configuration.DescriptionEn ?? String.Empty;

                lblTagValue.Text =
                    Configuration.Tag ?? String.Empty;

                lblConditionValue.Text =
                    Configuration.Condition ?? String.Empty;

                lblBandValue.Text =
                    (Configuration.Band != null)
                    ? Configuration.Band.ToString()
                    : String.Empty;

                lblReclassValue.Text =
                    (Configuration.Reclass != null)
                    ? Configuration.Reclass.ToString()
                    : String.Empty;

                lblVectorValue.Text =
                    (Configuration.Vector != null)
                    ? (LanguageVersion == LanguageVersion.National)
                        ? Configuration.Vector.ExtendedLabelCs ?? String.Empty
                        : Configuration.Vector.ExtendedLabelEn ?? String.Empty
                    : (Configuration.VectorId != null)
                        ? Configuration.VectorId.ToString()
                        : String.Empty;

                lblRasterAValue.Text =
                    (Configuration.RasterA != null)
                    ? (LanguageVersion == LanguageVersion.National)
                        ? Configuration.RasterA.ExtendedLabelCs ?? String.Empty
                        : Configuration.RasterA.ExtendedLabelEn ?? String.Empty
                    : (Configuration.RasterAId != null)
                        ? Configuration.RasterAId.ToString()
                        : String.Empty;

                lblRasterBValue.Text =
                   (Configuration.RasterB != null)
                    ? (LanguageVersion == LanguageVersion.National)
                        ? Configuration.RasterB.ExtendedLabelCs ?? String.Empty
                        : Configuration.RasterB.ExtendedLabelEn ?? String.Empty
                    : (Configuration.RasterBId != null)
                        ? Configuration.RasterBId.ToString()
                        : String.Empty;

                pnlCategories.Controls.Clear();
                foreach (Config category in Configuration.CategoriesConfigurations.OrderByDescending(a => a.Id))
                {
                    pnlCategories.Controls.Add(value:
                        new Label()
                        {
                            AutoSize = false,
                            Dock = DockStyle.Top,
                            Font = Setting.LabelValueFont,
                            ForeColor = Setting.LabelValueForeColor,
                            Height = 20,
                            Text = (LanguageVersion == LanguageVersion.International)
                                ? category.ExtendedLabelEn
                                : (LanguageVersion == LanguageVersion.National)
                                    ? category.ExtendedLabelCs
                                    : category.ExtendedLabelEn,
                            TextAlign = System.Drawing.ContentAlignment.TopLeft
                        });
                }

                pnlComplete.Controls.Clear();
                foreach (Config category in Configuration.CompleteConfigurations.OrderByDescending(a => a.Id))
                {
                    pnlComplete.Controls.Add(value:
                        new Label()
                        {
                            AutoSize = false,
                            Dock = DockStyle.Top,
                            Font = Setting.LabelValueFont,
                            ForeColor = Setting.LabelValueForeColor,
                            Height = 20,
                            Text = (LanguageVersion == LanguageVersion.International)
                                ? category.ExtendedLabelEn
                                : (LanguageVersion == LanguageVersion.National)
                                    ? category.ExtendedLabelCs
                                    : category.ExtendedLabelEn,
                            TextAlign = System.Drawing.ContentAlignment.TopLeft
                        });
                }

                lblAuxTotalCountValue.Text =
                   Configuration.AuxTotalCount.ToString();

                lblAuxDataCountValue.Text =
                    Configuration.AuxDataCount.ToString();

                lblEditDateValue.Text =
                    Configuration.EditDateText ?? String.Empty;
            }
            else
            {
                lblIdValue.Text = String.Empty;
                lblAuxiliaryVariableCategoryIdValue.Text = String.Empty;
                lblConfigCollectionIdValue.Text = String.Empty;
                lblConfigQueryIdValue.Text = String.Empty;
                lblLabelValue.Text = String.Empty;
                lblDescriptionValue.Text = String.Empty;
                lblTagValue.Text = String.Empty;
                lblConditionValue.Text = String.Empty;
                lblBandValue.Text = String.Empty;
                lblReclassValue.Text = String.Empty;
                lblVectorValue.Text = String.Empty;
                lblRasterAValue.Text = String.Empty;
                lblRasterBValue.Text = String.Empty;
                pnlCategories.Controls.Clear();
                pnlComplete.Controls.Clear();
                lblAuxTotalCountValue.Text = String.Empty;
                lblAuxDataCountValue.Text = String.Empty;
                lblEditDateValue.Text = String.Empty;
            }

            #endregion Values
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent() { }

        /// <summary>
        /// <para lang="cs">Nastaví výšky řádku v TableLayoutPanel</para>
        /// <para lang="en">Sets TableLayoutPanel row heights</para>
        /// </summary>
        private void SetRowHeights()
        {
            if ((Configuration == null) || (Configuration.ConfigCollection == null))
            {
                tlpConfigurationParameters.Visible = false;
                return;
            }

            tlpConfigurationParameters.Visible = false;

            List<int> visibleRowIndexes = Configuration.ConfigQueryValue switch
            {
                ConfigQueryEnum.GisLayer => Configuration.ConfigCollection.ConfigFunctionValue switch
                {
                    ConfigFunctionEnum.VectorTotal => [0, 1, 3, 4, 7, 15, 16, 17],
                    ConfigFunctionEnum.RasterTotal => [0, 1, 3, 4, 7, 8, 9, 15, 16, 17],
                    ConfigFunctionEnum.RasterTotalWithinVector => [0, 1, 3, 4, 10, 11, 15, 16, 17],
                    ConfigFunctionEnum.RastersProductTotal => [0, 1, 3, 4, 11, 12, 15, 16, 17],
                    ConfigFunctionEnum.PointLayer => [],
                    ConfigFunctionEnum.PointTotalCombination => [],
                    _ => [],
                },
                ConfigQueryEnum.SumOfVariables => [0, 1, 3, 4, 13, 15, 16, 17],
                ConfigQueryEnum.AdditionToCellArea => [0, 1, 3, 4, 13, 15, 16, 17],
                ConfigQueryEnum.AdditionToTotal => [0, 1, 3, 4, 13, 14, 15, 16, 17],
                ConfigQueryEnum.LinkToVariable => [0, 1, 3, 4, 13, 15, 16, 17],
                _ => [],
            };

            if (Configuration.AuxiliaryVariableCategoryId == null)
            {
                visibleRowIndexes.Remove(item: 1);
            }

            for (int i = 0; i < tlpConfigurationParameters.RowStyles.Count; i++)
            {
                if (visibleRowIndexes.Contains(item: i))
                {
                    tlpConfigurationParameters.RowStyles[i].Height = rowHeight;

                    if (i == 13)
                    {
                        tlpConfigurationParameters.RowStyles[i].Height = rowHeight * Configuration.CategoriesList.Count;
                    }

                    if (i == 14)
                    {
                        tlpConfigurationParameters.RowStyles[i].Height = rowHeight * Configuration.CompleteList.Count;
                    }
                }
                else
                {
                    tlpConfigurationParameters.RowStyles[i].Height = 0;
                }
            }
            tlpConfigurationParameters.Height =
                tlpConfigurationParameters.RowStyles
                .OfType<RowStyle>()
                .Select(a => (int)a.Height).Sum();

            tlpConfigurationParameters.Visible = true;
        }

        #endregion Methods

    }

}