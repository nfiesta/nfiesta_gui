﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi.ModuleAuxiliaryData
{

    public partial class ControlAuxiliaryDataMain
    {
        /// <summary> 
        /// Vyžaduje se proměnná návrháře.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Uvolněte všechny používané prostředky.
        /// </summary>
        /// <param name="disposing">hodnota true, když by se měl spravovaný prostředek odstranit; jinak false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kód vygenerovaný pomocí Návrháře komponent

        /// <summary> 
        /// Metoda vyžadovaná pro podporu Návrháře - neupravovat
        /// obsah této metody v editoru kódu.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ControlAuxiliaryDataMain));
            pnlMain = new System.Windows.Forms.Panel();
            tlpMain = new System.Windows.Forms.TableLayoutPanel();
            splMain = new System.Windows.Forms.SplitContainer();
            grpConfigurations = new System.Windows.Forms.GroupBox();
            tlpConfigurations = new System.Windows.Forms.TableLayoutPanel();
            tvwConfigurations = new System.Windows.Forms.TreeView();
            ilTreeView = new System.Windows.Forms.ImageList(components);
            pnlConfigurationsHeader = new System.Windows.Forms.Panel();
            tsrConfiguration = new System.Windows.Forms.ToolStrip();
            btnLoadConfigurationList = new System.Windows.Forms.ToolStripButton();
            btnNewConfigCollection = new System.Windows.Forms.ToolStripButton();
            btnNewConfiguration = new System.Windows.Forms.ToolStripButton();
            btnUpdateConfiguration = new System.Windows.Forms.ToolStripButton();
            btnDeleteConfiguration = new System.Windows.Forms.ToolStripButton();
            btnLockConfigCollection = new System.Windows.Forms.ToolStripButton();
            btnUnlockConfigCollection = new System.Windows.Forms.ToolStripButton();
            btnViewData = new System.Windows.Forms.ToolStripButton();
            pnlConfigurationsButtons = new System.Windows.Forms.Panel();
            grpConfigurationAttributes = new System.Windows.Forms.GroupBox();
            tabMain = new System.Windows.Forms.TabControl();
            tpParameters = new System.Windows.Forms.TabPage();
            pnlParameters = new System.Windows.Forms.Panel();
            tpSQL = new System.Windows.Forms.TabPage();
            grpSQL = new System.Windows.Forms.GroupBox();
            txtSQL = new System.Windows.Forms.RichTextBox();
            cmsConfigFunction = new System.Windows.Forms.ContextMenuStrip(components);
            tsmiNewConfigCollection = new System.Windows.Forms.ToolStripMenuItem();
            cmsConfiguration = new System.Windows.Forms.ContextMenuStrip(components);
            tsmiUpdateConfiguration = new System.Windows.Forms.ToolStripMenuItem();
            tsmiDeleteConfiguration = new System.Windows.Forms.ToolStripMenuItem();
            cmsLockedConfigCollection = new System.Windows.Forms.ContextMenuStrip(components);
            tsmiUnlockConfigCollection = new System.Windows.Forms.ToolStripMenuItem();
            tsmiViewData = new System.Windows.Forms.ToolStripMenuItem();
            cmsConfigCollection = new System.Windows.Forms.ContextMenuStrip(components);
            tsmiNewConfiguration = new System.Windows.Forms.ToolStripMenuItem();
            tsmiUpdateConfigCollection = new System.Windows.Forms.ToolStripMenuItem();
            tsmiDeleteConfigCollection = new System.Windows.Forms.ToolStripMenuItem();
            tsmiLockConfigCollection = new System.Windows.Forms.ToolStripMenuItem();
            pnlMain.SuspendLayout();
            tlpMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)splMain).BeginInit();
            splMain.Panel1.SuspendLayout();
            splMain.Panel2.SuspendLayout();
            splMain.SuspendLayout();
            grpConfigurations.SuspendLayout();
            tlpConfigurations.SuspendLayout();
            pnlConfigurationsHeader.SuspendLayout();
            tsrConfiguration.SuspendLayout();
            grpConfigurationAttributes.SuspendLayout();
            tabMain.SuspendLayout();
            tpParameters.SuspendLayout();
            tpSQL.SuspendLayout();
            grpSQL.SuspendLayout();
            cmsConfigFunction.SuspendLayout();
            cmsConfiguration.SuspendLayout();
            cmsLockedConfigCollection.SuspendLayout();
            cmsConfigCollection.SuspendLayout();
            SuspendLayout();
            // 
            // pnlMain
            // 
            pnlMain.Controls.Add(tlpMain);
            pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlMain.Location = new System.Drawing.Point(0, 0);
            pnlMain.Margin = new System.Windows.Forms.Padding(0);
            pnlMain.Name = "pnlMain";
            pnlMain.Size = new System.Drawing.Size(960, 540);
            pnlMain.TabIndex = 6;
            // 
            // tlpMain
            // 
            tlpMain.ColumnCount = 1;
            tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.Controls.Add(splMain, 0, 0);
            tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpMain.Location = new System.Drawing.Point(0, 0);
            tlpMain.Margin = new System.Windows.Forms.Padding(0);
            tlpMain.Name = "tlpMain";
            tlpMain.RowCount = 1;
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpMain.Size = new System.Drawing.Size(960, 540);
            tlpMain.TabIndex = 5;
            // 
            // splMain
            // 
            splMain.Dock = System.Windows.Forms.DockStyle.Fill;
            splMain.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            splMain.Location = new System.Drawing.Point(0, 0);
            splMain.Margin = new System.Windows.Forms.Padding(0);
            splMain.Name = "splMain";
            // 
            // splMain.Panel1
            // 
            splMain.Panel1.Controls.Add(grpConfigurations);
            splMain.Panel1MinSize = 200;
            // 
            // splMain.Panel2
            // 
            splMain.Panel2.Controls.Add(grpConfigurationAttributes);
            splMain.Size = new System.Drawing.Size(960, 540);
            splMain.SplitterDistance = 408;
            splMain.SplitterWidth = 5;
            splMain.TabIndex = 3;
            // 
            // grpConfigurations
            // 
            grpConfigurations.Controls.Add(tlpConfigurations);
            grpConfigurations.Dock = System.Windows.Forms.DockStyle.Fill;
            grpConfigurations.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            grpConfigurations.ForeColor = System.Drawing.Color.MediumBlue;
            grpConfigurations.Location = new System.Drawing.Point(0, 0);
            grpConfigurations.Margin = new System.Windows.Forms.Padding(0);
            grpConfigurations.Name = "grpConfigurations";
            grpConfigurations.Padding = new System.Windows.Forms.Padding(5);
            grpConfigurations.Size = new System.Drawing.Size(408, 540);
            grpConfigurations.TabIndex = 7;
            grpConfigurations.TabStop = false;
            grpConfigurations.Text = "grpConfigurations";
            // 
            // tlpConfigurations
            // 
            tlpConfigurations.ColumnCount = 1;
            tlpConfigurations.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpConfigurations.Controls.Add(tvwConfigurations, 0, 1);
            tlpConfigurations.Controls.Add(pnlConfigurationsHeader, 0, 0);
            tlpConfigurations.Controls.Add(pnlConfigurationsButtons, 0, 2);
            tlpConfigurations.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpConfigurations.Location = new System.Drawing.Point(5, 25);
            tlpConfigurations.Margin = new System.Windows.Forms.Padding(0);
            tlpConfigurations.Name = "tlpConfigurations";
            tlpConfigurations.RowCount = 3;
            tlpConfigurations.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 46F));
            tlpConfigurations.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpConfigurations.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 1F));
            tlpConfigurations.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            tlpConfigurations.Size = new System.Drawing.Size(398, 510);
            tlpConfigurations.TabIndex = 0;
            // 
            // tvwConfigurations
            // 
            tvwConfigurations.AllowDrop = true;
            tvwConfigurations.BackColor = System.Drawing.Color.White;
            tvwConfigurations.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            tvwConfigurations.Dock = System.Windows.Forms.DockStyle.Fill;
            tvwConfigurations.Font = new System.Drawing.Font("Segoe UI", 9F);
            tvwConfigurations.ForeColor = System.Drawing.Color.Black;
            tvwConfigurations.HideSelection = false;
            tvwConfigurations.ImageIndex = 0;
            tvwConfigurations.ImageList = ilTreeView;
            tvwConfigurations.LineColor = System.Drawing.Color.MediumBlue;
            tvwConfigurations.Location = new System.Drawing.Point(0, 46);
            tvwConfigurations.Margin = new System.Windows.Forms.Padding(0);
            tvwConfigurations.Name = "tvwConfigurations";
            tvwConfigurations.SelectedImageIndex = 0;
            tvwConfigurations.ShowLines = false;
            tvwConfigurations.ShowRootLines = false;
            tvwConfigurations.Size = new System.Drawing.Size(398, 463);
            tvwConfigurations.TabIndex = 10;
            // 
            // ilTreeView
            // 
            ilTreeView.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            ilTreeView.ImageStream = (System.Windows.Forms.ImageListStreamer)resources.GetObject("ilTreeView.ImageStream");
            ilTreeView.TransparentColor = System.Drawing.Color.Transparent;
            ilTreeView.Images.SetKeyName(0, "RedBall");
            ilTreeView.Images.SetKeyName(1, "BlueBall");
            ilTreeView.Images.SetKeyName(2, "YellowBall");
            ilTreeView.Images.SetKeyName(3, "GreenBall");
            ilTreeView.Images.SetKeyName(4, "GrayBall");
            ilTreeView.Images.SetKeyName(5, "Lock");
            ilTreeView.Images.SetKeyName(6, "Schema");
            ilTreeView.Images.SetKeyName(7, "Vector");
            ilTreeView.Images.SetKeyName(8, "Raster");
            // 
            // pnlConfigurationsHeader
            // 
            pnlConfigurationsHeader.Controls.Add(tsrConfiguration);
            pnlConfigurationsHeader.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlConfigurationsHeader.Location = new System.Drawing.Point(0, 0);
            pnlConfigurationsHeader.Margin = new System.Windows.Forms.Padding(0);
            pnlConfigurationsHeader.Name = "pnlConfigurationsHeader";
            pnlConfigurationsHeader.Size = new System.Drawing.Size(398, 46);
            pnlConfigurationsHeader.TabIndex = 0;
            // 
            // tsrConfiguration
            // 
            tsrConfiguration.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { btnLoadConfigurationList, btnNewConfigCollection, btnNewConfiguration, btnUpdateConfiguration, btnDeleteConfiguration, btnLockConfigCollection, btnUnlockConfigCollection, btnViewData });
            tsrConfiguration.Location = new System.Drawing.Point(0, 0);
            tsrConfiguration.Name = "tsrConfiguration";
            tsrConfiguration.Padding = new System.Windows.Forms.Padding(0);
            tsrConfiguration.Size = new System.Drawing.Size(398, 25);
            tsrConfiguration.TabIndex = 0;
            tsrConfiguration.Text = "tsrConfiguration";
            // 
            // btnLoadConfigurationList
            // 
            btnLoadConfigurationList.BackColor = System.Drawing.Color.Transparent;
            btnLoadConfigurationList.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            btnLoadConfigurationList.ForeColor = System.Drawing.Color.Black;
            btnLoadConfigurationList.Image = (System.Drawing.Image)resources.GetObject("btnLoadConfigurationList.Image");
            btnLoadConfigurationList.ImageTransparentColor = System.Drawing.Color.Magenta;
            btnLoadConfigurationList.Name = "btnLoadConfigurationList";
            btnLoadConfigurationList.Size = new System.Drawing.Size(23, 22);
            btnLoadConfigurationList.Text = "btnLoadConfigurationList";
            // 
            // btnNewConfigCollection
            // 
            btnNewConfigCollection.BackColor = System.Drawing.Color.Transparent;
            btnNewConfigCollection.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            btnNewConfigCollection.ForeColor = System.Drawing.Color.Black;
            btnNewConfigCollection.Image = (System.Drawing.Image)resources.GetObject("btnNewConfigCollection.Image");
            btnNewConfigCollection.ImageTransparentColor = System.Drawing.Color.Magenta;
            btnNewConfigCollection.Name = "btnNewConfigCollection";
            btnNewConfigCollection.Size = new System.Drawing.Size(23, 22);
            btnNewConfigCollection.Text = "btnNewConfigCollection";
            // 
            // btnNewConfiguration
            // 
            btnNewConfiguration.BackColor = System.Drawing.Color.Transparent;
            btnNewConfiguration.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            btnNewConfiguration.ForeColor = System.Drawing.Color.Black;
            btnNewConfiguration.Image = (System.Drawing.Image)resources.GetObject("btnNewConfiguration.Image");
            btnNewConfiguration.ImageTransparentColor = System.Drawing.Color.Magenta;
            btnNewConfiguration.Name = "btnNewConfiguration";
            btnNewConfiguration.Size = new System.Drawing.Size(23, 22);
            btnNewConfiguration.Text = "btnNewConfiguration";
            // 
            // btnUpdateConfiguration
            // 
            btnUpdateConfiguration.BackColor = System.Drawing.Color.Transparent;
            btnUpdateConfiguration.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            btnUpdateConfiguration.ForeColor = System.Drawing.Color.Black;
            btnUpdateConfiguration.Image = (System.Drawing.Image)resources.GetObject("btnUpdateConfiguration.Image");
            btnUpdateConfiguration.ImageTransparentColor = System.Drawing.Color.Magenta;
            btnUpdateConfiguration.Name = "btnUpdateConfiguration";
            btnUpdateConfiguration.Size = new System.Drawing.Size(23, 22);
            btnUpdateConfiguration.Text = "btnUpdateConfiguration";
            // 
            // btnDeleteConfiguration
            // 
            btnDeleteConfiguration.BackColor = System.Drawing.Color.Transparent;
            btnDeleteConfiguration.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            btnDeleteConfiguration.ForeColor = System.Drawing.Color.Black;
            btnDeleteConfiguration.Image = (System.Drawing.Image)resources.GetObject("btnDeleteConfiguration.Image");
            btnDeleteConfiguration.ImageTransparentColor = System.Drawing.Color.Magenta;
            btnDeleteConfiguration.Name = "btnDeleteConfiguration";
            btnDeleteConfiguration.Size = new System.Drawing.Size(23, 22);
            btnDeleteConfiguration.Text = "btnDeleteConfiguration";
            // 
            // btnLockConfigCollection
            // 
            btnLockConfigCollection.BackColor = System.Drawing.Color.Transparent;
            btnLockConfigCollection.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            btnLockConfigCollection.ForeColor = System.Drawing.Color.Black;
            btnLockConfigCollection.Image = (System.Drawing.Image)resources.GetObject("btnLockConfigCollection.Image");
            btnLockConfigCollection.ImageTransparentColor = System.Drawing.Color.Magenta;
            btnLockConfigCollection.Name = "btnLockConfigCollection";
            btnLockConfigCollection.Size = new System.Drawing.Size(23, 22);
            btnLockConfigCollection.Text = "btnLockConfigCollection";
            // 
            // btnUnlockConfigCollection
            // 
            btnUnlockConfigCollection.BackColor = System.Drawing.Color.Transparent;
            btnUnlockConfigCollection.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            btnUnlockConfigCollection.ForeColor = System.Drawing.Color.Black;
            btnUnlockConfigCollection.Image = (System.Drawing.Image)resources.GetObject("btnUnlockConfigCollection.Image");
            btnUnlockConfigCollection.ImageTransparentColor = System.Drawing.Color.Magenta;
            btnUnlockConfigCollection.Name = "btnUnlockConfigCollection";
            btnUnlockConfigCollection.Size = new System.Drawing.Size(23, 22);
            btnUnlockConfigCollection.Text = "btnUnlockConfigCollection";
            // 
            // btnViewData
            // 
            btnViewData.BackColor = System.Drawing.Color.Transparent;
            btnViewData.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            btnViewData.ForeColor = System.Drawing.Color.Black;
            btnViewData.Image = (System.Drawing.Image)resources.GetObject("btnViewData.Image");
            btnViewData.ImageTransparentColor = System.Drawing.Color.Magenta;
            btnViewData.Name = "btnViewData";
            btnViewData.Size = new System.Drawing.Size(23, 22);
            btnViewData.Text = "btnViewData";
            // 
            // pnlConfigurationsButtons
            // 
            pnlConfigurationsButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlConfigurationsButtons.Location = new System.Drawing.Point(0, 509);
            pnlConfigurationsButtons.Margin = new System.Windows.Forms.Padding(0);
            pnlConfigurationsButtons.Name = "pnlConfigurationsButtons";
            pnlConfigurationsButtons.Size = new System.Drawing.Size(398, 1);
            pnlConfigurationsButtons.TabIndex = 8;
            // 
            // grpConfigurationAttributes
            // 
            grpConfigurationAttributes.Controls.Add(tabMain);
            grpConfigurationAttributes.Dock = System.Windows.Forms.DockStyle.Fill;
            grpConfigurationAttributes.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            grpConfigurationAttributes.ForeColor = System.Drawing.Color.MediumBlue;
            grpConfigurationAttributes.Location = new System.Drawing.Point(0, 0);
            grpConfigurationAttributes.Margin = new System.Windows.Forms.Padding(0);
            grpConfigurationAttributes.Name = "grpConfigurationAttributes";
            grpConfigurationAttributes.Padding = new System.Windows.Forms.Padding(5);
            grpConfigurationAttributes.Size = new System.Drawing.Size(547, 540);
            grpConfigurationAttributes.TabIndex = 0;
            grpConfigurationAttributes.TabStop = false;
            grpConfigurationAttributes.Text = "grpConfigurationAttributes";
            // 
            // tabMain
            // 
            tabMain.Controls.Add(tpParameters);
            tabMain.Controls.Add(tpSQL);
            tabMain.Dock = System.Windows.Forms.DockStyle.Fill;
            tabMain.Font = new System.Drawing.Font("Segoe UI", 9F);
            tabMain.Location = new System.Drawing.Point(5, 25);
            tabMain.Margin = new System.Windows.Forms.Padding(0);
            tabMain.Name = "tabMain";
            tabMain.SelectedIndex = 0;
            tabMain.Size = new System.Drawing.Size(537, 510);
            tabMain.TabIndex = 11;
            // 
            // tpParameters
            // 
            tpParameters.Controls.Add(pnlParameters);
            tpParameters.Font = new System.Drawing.Font("Segoe UI", 9F);
            tpParameters.Location = new System.Drawing.Point(4, 24);
            tpParameters.Margin = new System.Windows.Forms.Padding(0);
            tpParameters.Name = "tpParameters";
            tpParameters.Size = new System.Drawing.Size(529, 482);
            tpParameters.TabIndex = 0;
            tpParameters.Text = "tpParameters";
            tpParameters.UseVisualStyleBackColor = true;
            // 
            // pnlParameters
            // 
            pnlParameters.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlParameters.Location = new System.Drawing.Point(0, 0);
            pnlParameters.Margin = new System.Windows.Forms.Padding(0);
            pnlParameters.Name = "pnlParameters";
            pnlParameters.Size = new System.Drawing.Size(529, 482);
            pnlParameters.TabIndex = 12;
            // 
            // tpSQL
            // 
            tpSQL.Controls.Add(grpSQL);
            tpSQL.Location = new System.Drawing.Point(4, 24);
            tpSQL.Margin = new System.Windows.Forms.Padding(0);
            tpSQL.Name = "tpSQL";
            tpSQL.Size = new System.Drawing.Size(529, 482);
            tpSQL.TabIndex = 1;
            tpSQL.Text = "tpSQL";
            tpSQL.UseVisualStyleBackColor = true;
            // 
            // grpSQL
            // 
            grpSQL.Controls.Add(txtSQL);
            grpSQL.Dock = System.Windows.Forms.DockStyle.Fill;
            grpSQL.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            grpSQL.ForeColor = System.Drawing.Color.MediumBlue;
            grpSQL.Location = new System.Drawing.Point(0, 0);
            grpSQL.Margin = new System.Windows.Forms.Padding(0);
            grpSQL.Name = "grpSQL";
            grpSQL.Padding = new System.Windows.Forms.Padding(5);
            grpSQL.Size = new System.Drawing.Size(529, 482);
            grpSQL.TabIndex = 8;
            grpSQL.TabStop = false;
            grpSQL.Text = "grpSQL";
            // 
            // txtSQL
            // 
            txtSQL.BackColor = System.Drawing.Color.White;
            txtSQL.BorderStyle = System.Windows.Forms.BorderStyle.None;
            txtSQL.Dock = System.Windows.Forms.DockStyle.Fill;
            txtSQL.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            txtSQL.Location = new System.Drawing.Point(5, 25);
            txtSQL.Margin = new System.Windows.Forms.Padding(0);
            txtSQL.Name = "txtSQL";
            txtSQL.ReadOnly = true;
            txtSQL.Size = new System.Drawing.Size(519, 452);
            txtSQL.TabIndex = 0;
            txtSQL.Text = "";
            // 
            // cmsConfigFunction
            // 
            cmsConfigFunction.BackColor = System.Drawing.Color.Transparent;
            cmsConfigFunction.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { tsmiNewConfigCollection });
            cmsConfigFunction.Name = "contextMenuCollection";
            cmsConfigFunction.Size = new System.Drawing.Size(212, 24);
            cmsConfigFunction.Text = "cmsConfigFunction";
            // 
            // tsmiNewConfigCollection
            // 
            tsmiNewConfigCollection.Image = (System.Drawing.Image)resources.GetObject("tsmiNewConfigCollection.Image");
            tsmiNewConfigCollection.Name = "tsmiNewConfigCollection";
            tsmiNewConfigCollection.Padding = new System.Windows.Forms.Padding(0);
            tsmiNewConfigCollection.Size = new System.Drawing.Size(211, 20);
            tsmiNewConfigCollection.Text = "tsmiNewConfigCollection";
            // 
            // cmsConfiguration
            // 
            cmsConfiguration.BackColor = System.Drawing.Color.Transparent;
            cmsConfiguration.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { tsmiUpdateConfiguration, tsmiDeleteConfiguration });
            cmsConfiguration.Name = "MenuConfiguration";
            cmsConfiguration.Size = new System.Drawing.Size(210, 44);
            cmsConfiguration.Text = "cmsConfiguration";
            // 
            // tsmiUpdateConfiguration
            // 
            tsmiUpdateConfiguration.Image = (System.Drawing.Image)resources.GetObject("tsmiUpdateConfiguration.Image");
            tsmiUpdateConfiguration.Name = "tsmiUpdateConfiguration";
            tsmiUpdateConfiguration.Padding = new System.Windows.Forms.Padding(0);
            tsmiUpdateConfiguration.Size = new System.Drawing.Size(209, 20);
            tsmiUpdateConfiguration.Text = "tsmiUpdateConfiguration";
            // 
            // tsmiDeleteConfiguration
            // 
            tsmiDeleteConfiguration.Image = (System.Drawing.Image)resources.GetObject("tsmiDeleteConfiguration.Image");
            tsmiDeleteConfiguration.Name = "tsmiDeleteConfiguration";
            tsmiDeleteConfiguration.Padding = new System.Windows.Forms.Padding(0);
            tsmiDeleteConfiguration.Size = new System.Drawing.Size(209, 20);
            tsmiDeleteConfiguration.Text = "tsmiDeleteConfiguration";
            // 
            // cmsLockedConfigCollection
            // 
            cmsLockedConfigCollection.BackColor = System.Drawing.Color.Transparent;
            cmsLockedConfigCollection.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { tsmiUnlockConfigCollection, tsmiViewData });
            cmsLockedConfigCollection.Name = "MenuLockedConfigCollection";
            cmsLockedConfigCollection.Size = new System.Drawing.Size(225, 44);
            cmsLockedConfigCollection.Text = "cmsLockedConfigCollection";
            // 
            // tsmiUnlockConfigCollection
            // 
            tsmiUnlockConfigCollection.Image = (System.Drawing.Image)resources.GetObject("tsmiUnlockConfigCollection.Image");
            tsmiUnlockConfigCollection.Name = "tsmiUnlockConfigCollection";
            tsmiUnlockConfigCollection.Padding = new System.Windows.Forms.Padding(0);
            tsmiUnlockConfigCollection.Size = new System.Drawing.Size(224, 20);
            tsmiUnlockConfigCollection.Text = "tsmiUnlockConfigCollection";
            // 
            // tsmiViewData
            // 
            tsmiViewData.Image = (System.Drawing.Image)resources.GetObject("tsmiViewData.Image");
            tsmiViewData.Name = "tsmiViewData";
            tsmiViewData.Padding = new System.Windows.Forms.Padding(0);
            tsmiViewData.Size = new System.Drawing.Size(224, 20);
            tsmiViewData.Text = "tsmiViewData";
            // 
            // cmsConfigCollection
            // 
            cmsConfigCollection.BackColor = System.Drawing.Color.Transparent;
            cmsConfigCollection.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { tsmiNewConfiguration, tsmiUpdateConfigCollection, tsmiDeleteConfigCollection, tsmiLockConfigCollection });
            cmsConfigCollection.Name = "MenuConfigCollection";
            cmsConfigCollection.Size = new System.Drawing.Size(226, 84);
            cmsConfigCollection.Text = "cmsConfigCollection";
            // 
            // tsmiNewConfiguration
            // 
            tsmiNewConfiguration.Image = (System.Drawing.Image)resources.GetObject("tsmiNewConfiguration.Image");
            tsmiNewConfiguration.Name = "tsmiNewConfiguration";
            tsmiNewConfiguration.Padding = new System.Windows.Forms.Padding(0);
            tsmiNewConfiguration.Size = new System.Drawing.Size(225, 20);
            tsmiNewConfiguration.Text = "tsmiNewConfiguration";
            // 
            // tsmiUpdateConfigCollection
            // 
            tsmiUpdateConfigCollection.Image = (System.Drawing.Image)resources.GetObject("tsmiUpdateConfigCollection.Image");
            tsmiUpdateConfigCollection.Name = "tsmiUpdateConfigCollection";
            tsmiUpdateConfigCollection.Padding = new System.Windows.Forms.Padding(0);
            tsmiUpdateConfigCollection.Size = new System.Drawing.Size(225, 20);
            tsmiUpdateConfigCollection.Text = "tsmiUpdateConfigCollection";
            // 
            // tsmiDeleteConfigCollection
            // 
            tsmiDeleteConfigCollection.Image = (System.Drawing.Image)resources.GetObject("tsmiDeleteConfigCollection.Image");
            tsmiDeleteConfigCollection.Name = "tsmiDeleteConfigCollection";
            tsmiDeleteConfigCollection.Padding = new System.Windows.Forms.Padding(0);
            tsmiDeleteConfigCollection.Size = new System.Drawing.Size(225, 20);
            tsmiDeleteConfigCollection.Text = "tsmiDeleteConfigCollection";
            // 
            // tsmiLockConfigCollection
            // 
            tsmiLockConfigCollection.Image = (System.Drawing.Image)resources.GetObject("tsmiLockConfigCollection.Image");
            tsmiLockConfigCollection.Name = "tsmiLockConfigCollection";
            tsmiLockConfigCollection.Padding = new System.Windows.Forms.Padding(0);
            tsmiLockConfigCollection.Size = new System.Drawing.Size(225, 20);
            tsmiLockConfigCollection.Text = "tsmiLockConfigCollection";
            // 
            // ControlAuxiliaryDataMain
            // 
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            BackColor = System.Drawing.Color.Transparent;
            BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            Controls.Add(pnlMain);
            ForeColor = System.Drawing.Color.Black;
            Margin = new System.Windows.Forms.Padding(0);
            Name = "ControlAuxiliaryDataMain";
            Size = new System.Drawing.Size(960, 540);
            pnlMain.ResumeLayout(false);
            tlpMain.ResumeLayout(false);
            splMain.Panel1.ResumeLayout(false);
            splMain.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)splMain).EndInit();
            splMain.ResumeLayout(false);
            grpConfigurations.ResumeLayout(false);
            tlpConfigurations.ResumeLayout(false);
            pnlConfigurationsHeader.ResumeLayout(false);
            pnlConfigurationsHeader.PerformLayout();
            tsrConfiguration.ResumeLayout(false);
            tsrConfiguration.PerformLayout();
            grpConfigurationAttributes.ResumeLayout(false);
            tabMain.ResumeLayout(false);
            tpParameters.ResumeLayout(false);
            tpSQL.ResumeLayout(false);
            grpSQL.ResumeLayout(false);
            cmsConfigFunction.ResumeLayout(false);
            cmsConfiguration.ResumeLayout(false);
            cmsLockedConfigCollection.ResumeLayout(false);
            cmsConfigCollection.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion
        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.SplitContainer splMain;
        private System.Windows.Forms.GroupBox grpConfigurations;
        private System.Windows.Forms.TableLayoutPanel tlpConfigurations;
        private System.Windows.Forms.TreeView tvwConfigurations;
        private System.Windows.Forms.Panel pnlConfigurationsHeader;
        private System.Windows.Forms.ToolStrip tsrConfiguration;
        private System.Windows.Forms.ToolStripButton btnLoadConfigurationList;
        private System.Windows.Forms.ToolStripButton btnNewConfigCollection;
        private System.Windows.Forms.ToolStripButton btnNewConfiguration;
        private System.Windows.Forms.ToolStripButton btnUpdateConfiguration;
        private System.Windows.Forms.ToolStripButton btnDeleteConfiguration;
        private System.Windows.Forms.ToolStripButton btnLockConfigCollection;
        private System.Windows.Forms.ToolStripButton btnUnlockConfigCollection;
        private System.Windows.Forms.ToolStripButton btnViewData;
        private System.Windows.Forms.Panel pnlConfigurationsButtons;
        private System.Windows.Forms.GroupBox grpConfigurationAttributes;
        private System.Windows.Forms.TabControl tabMain;
        private System.Windows.Forms.TabPage tpParameters;
        private System.Windows.Forms.TabPage tpSQL;
        private System.Windows.Forms.GroupBox grpSQL;
        private System.Windows.Forms.RichTextBox txtSQL;
        private System.Windows.Forms.ContextMenuStrip cmsConfigFunction;
        private System.Windows.Forms.ToolStripMenuItem tsmiNewConfigCollection;
        private System.Windows.Forms.ContextMenuStrip cmsConfiguration;
        private System.Windows.Forms.ToolStripMenuItem tsmiUpdateConfiguration;
        private System.Windows.Forms.ToolStripMenuItem tsmiDeleteConfiguration;
        private System.Windows.Forms.ContextMenuStrip cmsLockedConfigCollection;
        private System.Windows.Forms.ToolStripMenuItem tsmiUnlockConfigCollection;
        private System.Windows.Forms.ToolStripMenuItem tsmiViewData;
        private System.Windows.Forms.ContextMenuStrip cmsConfigCollection;
        private System.Windows.Forms.ToolStripMenuItem tsmiNewConfiguration;
        private System.Windows.Forms.ToolStripMenuItem tsmiUpdateConfigCollection;
        private System.Windows.Forms.ToolStripMenuItem tsmiDeleteConfigCollection;
        private System.Windows.Forms.ToolStripMenuItem tsmiLockConfigCollection;
        private System.Windows.Forms.ImageList ilTreeView;
        private System.Windows.Forms.Panel pnlParameters;
    }

}
