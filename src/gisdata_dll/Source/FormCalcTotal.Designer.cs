﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi.ModuleAuxiliaryData
{

    partial class FormCalcTotal
    {

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormCalcTotal));
            tlpMain = new System.Windows.Forms.TableLayoutPanel();
            prgCalcTotal = new System.Windows.Forms.ProgressBar();
            tlpButtons = new System.Windows.Forms.TableLayoutPanel();
            pnlClose = new System.Windows.Forms.Panel();
            btnClose = new System.Windows.Forms.Button();
            splMain = new System.Windows.Forms.SplitContainer();
            grpEstimationCells = new System.Windows.Forms.GroupBox();
            tlpEstimationCells = new System.Windows.Forms.TableLayoutPanel();
            tvwEstimationCells = new System.Windows.Forms.TreeView();
            ilTreeView = new System.Windows.Forms.ImageList(components);
            tsrButtons = new System.Windows.Forms.ToolStrip();
            btnReload = new System.Windows.Forms.ToolStripButton();
            btnCalculate = new System.Windows.Forms.ToolStripButton();
            btnStop = new System.Windows.Forms.ToolStripButton();
            splLog = new System.Windows.Forms.SplitContainer();
            tlpAuxiliaryVariableTotals = new System.Windows.Forms.TableLayoutPanel();
            grpAuxiliaryVariableTotals = new System.Windows.Forms.GroupBox();
            dgvData = new System.Windows.Forms.DataGridView();
            grpConfigCollection = new System.Windows.Forms.GroupBox();
            lblConfigCollection = new System.Windows.Forms.Label();
            tlpFilters = new System.Windows.Forms.TableLayoutPanel();
            cboGUIVersion = new System.Windows.Forms.ComboBox();
            cboExtensionVersion = new System.Windows.Forms.ComboBox();
            lblGUIVersionCaption = new System.Windows.Forms.Label();
            lblConfigurationCaption = new System.Windows.Forms.Label();
            lblExtensionVersionCaption = new System.Windows.Forms.Label();
            cboConfiguration = new System.Windows.Forms.ComboBox();
            grpLog = new System.Windows.Forms.GroupBox();
            txtLog = new System.Windows.Forms.TextBox();
            cmsEstimationCells = new System.Windows.Forms.ContextMenuStrip(components);
            tsmiCheckAll = new System.Windows.Forms.ToolStripMenuItem();
            tsmiUncheckAll = new System.Windows.Forms.ToolStripMenuItem();
            tsmiCheckLevel1 = new System.Windows.Forms.ToolStripMenuItem();
            tsmiCheckLevel2 = new System.Windows.Forms.ToolStripMenuItem();
            tsmiCheckLevel3 = new System.Windows.Forms.ToolStripMenuItem();
            tsmiCheckUncalculated = new System.Windows.Forms.ToolStripMenuItem();
            tsmiCheckIncomplete = new System.Windows.Forms.ToolStripMenuItem();
            tsmiCheckCalculated = new System.Windows.Forms.ToolStripMenuItem();
            tlpMain.SuspendLayout();
            tlpButtons.SuspendLayout();
            pnlClose.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)splMain).BeginInit();
            splMain.Panel1.SuspendLayout();
            splMain.Panel2.SuspendLayout();
            splMain.SuspendLayout();
            grpEstimationCells.SuspendLayout();
            tlpEstimationCells.SuspendLayout();
            tsrButtons.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)splLog).BeginInit();
            splLog.Panel1.SuspendLayout();
            splLog.Panel2.SuspendLayout();
            splLog.SuspendLayout();
            tlpAuxiliaryVariableTotals.SuspendLayout();
            grpAuxiliaryVariableTotals.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)dgvData).BeginInit();
            grpConfigCollection.SuspendLayout();
            tlpFilters.SuspendLayout();
            grpLog.SuspendLayout();
            cmsEstimationCells.SuspendLayout();
            SuspendLayout();
            // 
            // tlpMain
            // 
            tlpMain.ColumnCount = 1;
            tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.Controls.Add(prgCalcTotal, 0, 1);
            tlpMain.Controls.Add(tlpButtons, 0, 2);
            tlpMain.Controls.Add(splMain, 0, 0);
            tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpMain.Location = new System.Drawing.Point(0, 0);
            tlpMain.Margin = new System.Windows.Forms.Padding(0);
            tlpMain.Name = "tlpMain";
            tlpMain.RowCount = 3;
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpMain.Size = new System.Drawing.Size(944, 501);
            tlpMain.TabIndex = 3;
            // 
            // prgCalcTotal
            // 
            prgCalcTotal.Dock = System.Windows.Forms.DockStyle.Fill;
            prgCalcTotal.ForeColor = System.Drawing.Color.MediumBlue;
            prgCalcTotal.Location = new System.Drawing.Point(0, 441);
            prgCalcTotal.Margin = new System.Windows.Forms.Padding(0);
            prgCalcTotal.Name = "prgCalcTotal";
            prgCalcTotal.Size = new System.Drawing.Size(944, 20);
            prgCalcTotal.TabIndex = 18;
            // 
            // tlpButtons
            // 
            tlpButtons.BackColor = System.Drawing.Color.Transparent;
            tlpButtons.ColumnCount = 2;
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpButtons.Controls.Add(pnlClose, 1, 0);
            tlpButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpButtons.Location = new System.Drawing.Point(0, 461);
            tlpButtons.Margin = new System.Windows.Forms.Padding(0);
            tlpButtons.Name = "tlpButtons";
            tlpButtons.RowCount = 1;
            tlpButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.Size = new System.Drawing.Size(944, 40);
            tlpButtons.TabIndex = 16;
            // 
            // pnlClose
            // 
            pnlClose.Controls.Add(btnClose);
            pnlClose.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlClose.Location = new System.Drawing.Point(784, 0);
            pnlClose.Margin = new System.Windows.Forms.Padding(0);
            pnlClose.Name = "pnlClose";
            pnlClose.Padding = new System.Windows.Forms.Padding(5);
            pnlClose.Size = new System.Drawing.Size(160, 40);
            pnlClose.TabIndex = 13;
            // 
            // btnClose
            // 
            btnClose.DialogResult = System.Windows.Forms.DialogResult.OK;
            btnClose.Dock = System.Windows.Forms.DockStyle.Fill;
            btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            btnClose.Location = new System.Drawing.Point(5, 5);
            btnClose.Margin = new System.Windows.Forms.Padding(0);
            btnClose.Name = "btnClose";
            btnClose.Size = new System.Drawing.Size(150, 30);
            btnClose.TabIndex = 9;
            btnClose.Text = "btnClose";
            btnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            btnClose.UseVisualStyleBackColor = true;
            // 
            // splMain
            // 
            splMain.BackColor = System.Drawing.Color.Transparent;
            splMain.Dock = System.Windows.Forms.DockStyle.Fill;
            splMain.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            splMain.IsSplitterFixed = true;
            splMain.Location = new System.Drawing.Point(0, 0);
            splMain.Margin = new System.Windows.Forms.Padding(0);
            splMain.Name = "splMain";
            // 
            // splMain.Panel1
            // 
            splMain.Panel1.Controls.Add(grpEstimationCells);
            splMain.Panel1MinSize = 100;
            // 
            // splMain.Panel2
            // 
            splMain.Panel2.Controls.Add(splLog);
            splMain.Size = new System.Drawing.Size(944, 441);
            splMain.SplitterDistance = 300;
            splMain.SplitterWidth = 1;
            splMain.TabIndex = 0;
            // 
            // grpEstimationCells
            // 
            grpEstimationCells.Controls.Add(tlpEstimationCells);
            grpEstimationCells.Dock = System.Windows.Forms.DockStyle.Fill;
            grpEstimationCells.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            grpEstimationCells.ForeColor = System.Drawing.Color.MediumBlue;
            grpEstimationCells.Location = new System.Drawing.Point(0, 0);
            grpEstimationCells.Margin = new System.Windows.Forms.Padding(0);
            grpEstimationCells.Name = "grpEstimationCells";
            grpEstimationCells.Padding = new System.Windows.Forms.Padding(5);
            grpEstimationCells.Size = new System.Drawing.Size(300, 441);
            grpEstimationCells.TabIndex = 6;
            grpEstimationCells.TabStop = false;
            grpEstimationCells.Text = "grpEstimationCells";
            // 
            // tlpEstimationCells
            // 
            tlpEstimationCells.ColumnCount = 1;
            tlpEstimationCells.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpEstimationCells.Controls.Add(tvwEstimationCells, 0, 1);
            tlpEstimationCells.Controls.Add(tsrButtons, 0, 0);
            tlpEstimationCells.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpEstimationCells.Location = new System.Drawing.Point(5, 25);
            tlpEstimationCells.Margin = new System.Windows.Forms.Padding(0);
            tlpEstimationCells.Name = "tlpEstimationCells";
            tlpEstimationCells.RowCount = 2;
            tlpEstimationCells.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            tlpEstimationCells.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpEstimationCells.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpEstimationCells.Size = new System.Drawing.Size(290, 411);
            tlpEstimationCells.TabIndex = 1;
            // 
            // tvwEstimationCells
            // 
            tvwEstimationCells.BackColor = System.Drawing.Color.White;
            tvwEstimationCells.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            tvwEstimationCells.CheckBoxes = true;
            tvwEstimationCells.Dock = System.Windows.Forms.DockStyle.Fill;
            tvwEstimationCells.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            tvwEstimationCells.ForeColor = System.Drawing.Color.Black;
            tvwEstimationCells.HideSelection = false;
            tvwEstimationCells.ImageIndex = 4;
            tvwEstimationCells.ImageList = ilTreeView;
            tvwEstimationCells.LineColor = System.Drawing.Color.MediumBlue;
            tvwEstimationCells.Location = new System.Drawing.Point(0, 25);
            tvwEstimationCells.Margin = new System.Windows.Forms.Padding(0);
            tvwEstimationCells.Name = "tvwEstimationCells";
            tvwEstimationCells.SelectedImageIndex = 4;
            tvwEstimationCells.ShowLines = false;
            tvwEstimationCells.ShowRootLines = false;
            tvwEstimationCells.Size = new System.Drawing.Size(290, 386);
            tvwEstimationCells.TabIndex = 6;
            // 
            // ilTreeView
            // 
            ilTreeView.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            ilTreeView.ImageStream = (System.Windows.Forms.ImageListStreamer)resources.GetObject("ilTreeView.ImageStream");
            ilTreeView.TransparentColor = System.Drawing.Color.Transparent;
            ilTreeView.Images.SetKeyName(0, "RedBall");
            ilTreeView.Images.SetKeyName(1, "BlueBall");
            ilTreeView.Images.SetKeyName(2, "YellowBall");
            ilTreeView.Images.SetKeyName(3, "GreenBall");
            ilTreeView.Images.SetKeyName(4, "GrayBall");
            ilTreeView.Images.SetKeyName(5, "Lock");
            ilTreeView.Images.SetKeyName(6, "Schema");
            ilTreeView.Images.SetKeyName(7, "Vector");
            ilTreeView.Images.SetKeyName(8, "Raster");
            // 
            // tsrButtons
            // 
            tsrButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            tsrButtons.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { btnReload, btnCalculate, btnStop });
            tsrButtons.Location = new System.Drawing.Point(0, 0);
            tsrButtons.Name = "tsrButtons";
            tsrButtons.Size = new System.Drawing.Size(290, 25);
            tsrButtons.TabIndex = 5;
            tsrButtons.Text = "tsrButtons";
            // 
            // btnReload
            // 
            btnReload.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            btnReload.Image = (System.Drawing.Image)resources.GetObject("btnReload.Image");
            btnReload.ImageTransparentColor = System.Drawing.Color.Magenta;
            btnReload.Name = "btnReload";
            btnReload.Size = new System.Drawing.Size(23, 22);
            btnReload.Text = "btnReload";
            // 
            // btnCalculate
            // 
            btnCalculate.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            btnCalculate.Image = (System.Drawing.Image)resources.GetObject("btnCalculate.Image");
            btnCalculate.ImageTransparentColor = System.Drawing.Color.Magenta;
            btnCalculate.Name = "btnCalculate";
            btnCalculate.Size = new System.Drawing.Size(23, 22);
            btnCalculate.Text = "btnCalculate";
            // 
            // btnStop
            // 
            btnStop.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            btnStop.Image = (System.Drawing.Image)resources.GetObject("btnStop.Image");
            btnStop.ImageTransparentColor = System.Drawing.Color.Magenta;
            btnStop.Name = "btnStop";
            btnStop.Size = new System.Drawing.Size(23, 22);
            btnStop.Text = "btnStop";
            // 
            // splLog
            // 
            splLog.Dock = System.Windows.Forms.DockStyle.Fill;
            splLog.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            splLog.Location = new System.Drawing.Point(0, 0);
            splLog.Margin = new System.Windows.Forms.Padding(0);
            splLog.Name = "splLog";
            splLog.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splLog.Panel1
            // 
            splLog.Panel1.Controls.Add(tlpAuxiliaryVariableTotals);
            // 
            // splLog.Panel2
            // 
            splLog.Panel2.Controls.Add(grpLog);
            splLog.Size = new System.Drawing.Size(643, 441);
            splLog.SplitterDistance = 341;
            splLog.SplitterWidth = 2;
            splLog.TabIndex = 0;
            // 
            // tlpAuxiliaryVariableTotals
            // 
            tlpAuxiliaryVariableTotals.ColumnCount = 1;
            tlpAuxiliaryVariableTotals.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpAuxiliaryVariableTotals.Controls.Add(grpAuxiliaryVariableTotals, 0, 2);
            tlpAuxiliaryVariableTotals.Controls.Add(grpConfigCollection, 0, 0);
            tlpAuxiliaryVariableTotals.Controls.Add(tlpFilters, 0, 1);
            tlpAuxiliaryVariableTotals.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpAuxiliaryVariableTotals.Location = new System.Drawing.Point(0, 0);
            tlpAuxiliaryVariableTotals.Margin = new System.Windows.Forms.Padding(0);
            tlpAuxiliaryVariableTotals.Name = "tlpAuxiliaryVariableTotals";
            tlpAuxiliaryVariableTotals.RowCount = 3;
            tlpAuxiliaryVariableTotals.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            tlpAuxiliaryVariableTotals.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 75F));
            tlpAuxiliaryVariableTotals.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpAuxiliaryVariableTotals.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpAuxiliaryVariableTotals.Size = new System.Drawing.Size(643, 341);
            tlpAuxiliaryVariableTotals.TabIndex = 4;
            // 
            // grpAuxiliaryVariableTotals
            // 
            grpAuxiliaryVariableTotals.Controls.Add(dgvData);
            grpAuxiliaryVariableTotals.Dock = System.Windows.Forms.DockStyle.Fill;
            grpAuxiliaryVariableTotals.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            grpAuxiliaryVariableTotals.ForeColor = System.Drawing.Color.MediumBlue;
            grpAuxiliaryVariableTotals.Location = new System.Drawing.Point(0, 135);
            grpAuxiliaryVariableTotals.Margin = new System.Windows.Forms.Padding(0);
            grpAuxiliaryVariableTotals.Name = "grpAuxiliaryVariableTotals";
            grpAuxiliaryVariableTotals.Padding = new System.Windows.Forms.Padding(5);
            grpAuxiliaryVariableTotals.Size = new System.Drawing.Size(643, 206);
            grpAuxiliaryVariableTotals.TabIndex = 5;
            grpAuxiliaryVariableTotals.TabStop = false;
            grpAuxiliaryVariableTotals.Text = "grpAuxiliaryVariableTotals";
            // 
            // dgvData
            // 
            dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dgvData.Dock = System.Windows.Forms.DockStyle.Fill;
            dgvData.Location = new System.Drawing.Point(5, 25);
            dgvData.Margin = new System.Windows.Forms.Padding(0);
            dgvData.Name = "dgvData";
            dgvData.Size = new System.Drawing.Size(633, 176);
            dgvData.TabIndex = 2;
            // 
            // grpConfigCollection
            // 
            grpConfigCollection.Controls.Add(lblConfigCollection);
            grpConfigCollection.Dock = System.Windows.Forms.DockStyle.Fill;
            grpConfigCollection.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            grpConfigCollection.ForeColor = System.Drawing.Color.MediumBlue;
            grpConfigCollection.Location = new System.Drawing.Point(0, 0);
            grpConfigCollection.Margin = new System.Windows.Forms.Padding(0);
            grpConfigCollection.Name = "grpConfigCollection";
            grpConfigCollection.Padding = new System.Windows.Forms.Padding(5);
            grpConfigCollection.Size = new System.Drawing.Size(643, 60);
            grpConfigCollection.TabIndex = 0;
            grpConfigCollection.TabStop = false;
            grpConfigCollection.Text = "grpConfigCollection";
            // 
            // lblConfigCollection
            // 
            lblConfigCollection.Dock = System.Windows.Forms.DockStyle.Fill;
            lblConfigCollection.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, 238);
            lblConfigCollection.ForeColor = System.Drawing.Color.Black;
            lblConfigCollection.Location = new System.Drawing.Point(5, 25);
            lblConfigCollection.Margin = new System.Windows.Forms.Padding(0);
            lblConfigCollection.Name = "lblConfigCollection";
            lblConfigCollection.Size = new System.Drawing.Size(633, 30);
            lblConfigCollection.TabIndex = 0;
            lblConfigCollection.Text = "lblConfigCollection";
            lblConfigCollection.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tlpFilters
            // 
            tlpFilters.ColumnCount = 2;
            tlpFilters.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            tlpFilters.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpFilters.Controls.Add(cboGUIVersion, 1, 2);
            tlpFilters.Controls.Add(cboExtensionVersion, 1, 1);
            tlpFilters.Controls.Add(lblGUIVersionCaption, 0, 2);
            tlpFilters.Controls.Add(lblConfigurationCaption, 0, 0);
            tlpFilters.Controls.Add(lblExtensionVersionCaption, 0, 1);
            tlpFilters.Controls.Add(cboConfiguration, 1, 0);
            tlpFilters.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpFilters.Location = new System.Drawing.Point(0, 60);
            tlpFilters.Margin = new System.Windows.Forms.Padding(0);
            tlpFilters.Name = "tlpFilters";
            tlpFilters.RowCount = 3;
            tlpFilters.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            tlpFilters.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            tlpFilters.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            tlpFilters.Size = new System.Drawing.Size(643, 75);
            tlpFilters.TabIndex = 6;
            // 
            // cboGUIVersion
            // 
            cboGUIVersion.Dock = System.Windows.Forms.DockStyle.Fill;
            cboGUIVersion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            cboGUIVersion.FormattingEnabled = true;
            cboGUIVersion.Location = new System.Drawing.Point(200, 50);
            cboGUIVersion.Margin = new System.Windows.Forms.Padding(0);
            cboGUIVersion.Name = "cboGUIVersion";
            cboGUIVersion.Size = new System.Drawing.Size(443, 23);
            cboGUIVersion.TabIndex = 7;
            // 
            // cboExtensionVersion
            // 
            cboExtensionVersion.Dock = System.Windows.Forms.DockStyle.Fill;
            cboExtensionVersion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            cboExtensionVersion.FormattingEnabled = true;
            cboExtensionVersion.Location = new System.Drawing.Point(200, 25);
            cboExtensionVersion.Margin = new System.Windows.Forms.Padding(0);
            cboExtensionVersion.Name = "cboExtensionVersion";
            cboExtensionVersion.Size = new System.Drawing.Size(443, 23);
            cboExtensionVersion.TabIndex = 6;
            // 
            // lblGUIVersionCaption
            // 
            lblGUIVersionCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblGUIVersionCaption.Location = new System.Drawing.Point(0, 50);
            lblGUIVersionCaption.Margin = new System.Windows.Forms.Padding(0);
            lblGUIVersionCaption.Name = "lblGUIVersionCaption";
            lblGUIVersionCaption.Size = new System.Drawing.Size(200, 20);
            lblGUIVersionCaption.TabIndex = 4;
            lblGUIVersionCaption.Text = "lblGUIVersionCaption";
            lblGUIVersionCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblConfigurationCaption
            // 
            lblConfigurationCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblConfigurationCaption.Location = new System.Drawing.Point(0, 0);
            lblConfigurationCaption.Margin = new System.Windows.Forms.Padding(0);
            lblConfigurationCaption.Name = "lblConfigurationCaption";
            lblConfigurationCaption.Size = new System.Drawing.Size(200, 20);
            lblConfigurationCaption.TabIndex = 2;
            lblConfigurationCaption.Text = "lblConfigurationCaption";
            lblConfigurationCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblExtensionVersionCaption
            // 
            lblExtensionVersionCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblExtensionVersionCaption.Location = new System.Drawing.Point(0, 25);
            lblExtensionVersionCaption.Margin = new System.Windows.Forms.Padding(0);
            lblExtensionVersionCaption.Name = "lblExtensionVersionCaption";
            lblExtensionVersionCaption.Size = new System.Drawing.Size(200, 20);
            lblExtensionVersionCaption.TabIndex = 3;
            lblExtensionVersionCaption.Text = "lblExtensionVersionCaption";
            lblExtensionVersionCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cboConfiguration
            // 
            cboConfiguration.Dock = System.Windows.Forms.DockStyle.Fill;
            cboConfiguration.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            cboConfiguration.FormattingEnabled = true;
            cboConfiguration.Location = new System.Drawing.Point(200, 0);
            cboConfiguration.Margin = new System.Windows.Forms.Padding(0);
            cboConfiguration.Name = "cboConfiguration";
            cboConfiguration.Size = new System.Drawing.Size(443, 23);
            cboConfiguration.TabIndex = 5;
            // 
            // grpLog
            // 
            grpLog.Controls.Add(txtLog);
            grpLog.Dock = System.Windows.Forms.DockStyle.Fill;
            grpLog.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            grpLog.ForeColor = System.Drawing.Color.MediumBlue;
            grpLog.Location = new System.Drawing.Point(0, 0);
            grpLog.Margin = new System.Windows.Forms.Padding(0);
            grpLog.Name = "grpLog";
            grpLog.Padding = new System.Windows.Forms.Padding(5);
            grpLog.Size = new System.Drawing.Size(643, 98);
            grpLog.TabIndex = 9;
            grpLog.TabStop = false;
            grpLog.Text = "grpLog";
            // 
            // txtLog
            // 
            txtLog.BackColor = System.Drawing.Color.White;
            txtLog.BorderStyle = System.Windows.Forms.BorderStyle.None;
            txtLog.Dock = System.Windows.Forms.DockStyle.Fill;
            txtLog.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            txtLog.ForeColor = System.Drawing.Color.Black;
            txtLog.Location = new System.Drawing.Point(5, 25);
            txtLog.Margin = new System.Windows.Forms.Padding(0);
            txtLog.Multiline = true;
            txtLog.Name = "txtLog";
            txtLog.ReadOnly = true;
            txtLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            txtLog.Size = new System.Drawing.Size(633, 68);
            txtLog.TabIndex = 0;
            // 
            // cmsEstimationCells
            // 
            cmsEstimationCells.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { tsmiCheckAll, tsmiUncheckAll, tsmiCheckLevel1, tsmiCheckLevel2, tsmiCheckLevel3, tsmiCheckUncalculated, tsmiCheckIncomplete, tsmiCheckCalculated });
            cmsEstimationCells.Name = "contextMenuCollection";
            cmsEstimationCells.Size = new System.Drawing.Size(200, 180);
            // 
            // tsmiCheckAll
            // 
            tsmiCheckAll.Image = (System.Drawing.Image)resources.GetObject("tsmiCheckAll.Image");
            tsmiCheckAll.Name = "tsmiCheckAll";
            tsmiCheckAll.Size = new System.Drawing.Size(199, 22);
            tsmiCheckAll.Text = "tsmiCheckAll";
            // 
            // tsmiUncheckAll
            // 
            tsmiUncheckAll.Image = (System.Drawing.Image)resources.GetObject("tsmiUncheckAll.Image");
            tsmiUncheckAll.Name = "tsmiUncheckAll";
            tsmiUncheckAll.Size = new System.Drawing.Size(199, 22);
            tsmiUncheckAll.Text = "tsmiUncheckAll";
            // 
            // tsmiCheckLevel1
            // 
            tsmiCheckLevel1.Image = (System.Drawing.Image)resources.GetObject("tsmiCheckLevel1.Image");
            tsmiCheckLevel1.Name = "tsmiCheckLevel1";
            tsmiCheckLevel1.Size = new System.Drawing.Size(199, 22);
            tsmiCheckLevel1.Text = "tsmiCheckLevel1";
            // 
            // tsmiCheckLevel2
            // 
            tsmiCheckLevel2.Image = (System.Drawing.Image)resources.GetObject("tsmiCheckLevel2.Image");
            tsmiCheckLevel2.Name = "tsmiCheckLevel2";
            tsmiCheckLevel2.Size = new System.Drawing.Size(199, 22);
            tsmiCheckLevel2.Text = "tsmiCheckLevel2";
            // 
            // tsmiCheckLevel3
            // 
            tsmiCheckLevel3.Image = (System.Drawing.Image)resources.GetObject("tsmiCheckLevel3.Image");
            tsmiCheckLevel3.Name = "tsmiCheckLevel3";
            tsmiCheckLevel3.Size = new System.Drawing.Size(199, 22);
            tsmiCheckLevel3.Text = "tsmiCheckLevel3";
            // 
            // tsmiCheckUncalculated
            // 
            tsmiCheckUncalculated.Image = (System.Drawing.Image)resources.GetObject("tsmiCheckUncalculated.Image");
            tsmiCheckUncalculated.Name = "tsmiCheckUncalculated";
            tsmiCheckUncalculated.Size = new System.Drawing.Size(199, 22);
            tsmiCheckUncalculated.Text = "tsmiCheckUncalculated";
            // 
            // tsmiCheckIncomplete
            // 
            tsmiCheckIncomplete.Image = (System.Drawing.Image)resources.GetObject("tsmiCheckIncomplete.Image");
            tsmiCheckIncomplete.Name = "tsmiCheckIncomplete";
            tsmiCheckIncomplete.Size = new System.Drawing.Size(199, 22);
            tsmiCheckIncomplete.Text = "tsmiCheckIncomplete";
            tsmiCheckIncomplete.Visible = false;
            // 
            // tsmiCheckCalculated
            // 
            tsmiCheckCalculated.Image = (System.Drawing.Image)resources.GetObject("tsmiCheckCalculated.Image");
            tsmiCheckCalculated.Name = "tsmiCheckCalculated";
            tsmiCheckCalculated.Size = new System.Drawing.Size(199, 22);
            tsmiCheckCalculated.Text = "tsmiCheckCalculated";
            // 
            // FormCalcTotal
            // 
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            BackColor = System.Drawing.Color.WhiteSmoke;
            ClientSize = new System.Drawing.Size(944, 501);
            Controls.Add(tlpMain);
            ForeColor = System.Drawing.Color.Black;
            Name = "FormCalcTotal";
            ShowIcon = false;
            ShowInTaskbar = false;
            StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            Text = "FormCalcTotal";
            tlpMain.ResumeLayout(false);
            tlpButtons.ResumeLayout(false);
            pnlClose.ResumeLayout(false);
            splMain.Panel1.ResumeLayout(false);
            splMain.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)splMain).EndInit();
            splMain.ResumeLayout(false);
            grpEstimationCells.ResumeLayout(false);
            tlpEstimationCells.ResumeLayout(false);
            tlpEstimationCells.PerformLayout();
            tsrButtons.ResumeLayout(false);
            tsrButtons.PerformLayout();
            splLog.Panel1.ResumeLayout(false);
            splLog.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)splLog).EndInit();
            splLog.ResumeLayout(false);
            tlpAuxiliaryVariableTotals.ResumeLayout(false);
            grpAuxiliaryVariableTotals.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)dgvData).EndInit();
            grpConfigCollection.ResumeLayout(false);
            tlpFilters.ResumeLayout(false);
            grpLog.ResumeLayout(false);
            grpLog.PerformLayout();
            cmsEstimationCells.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.TableLayoutPanel tlpButtons;
        private System.Windows.Forms.Panel pnlClose;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.SplitContainer splMain;
        private System.Windows.Forms.GroupBox grpEstimationCells;
        private System.Windows.Forms.TableLayoutPanel tlpEstimationCells;
        private System.Windows.Forms.ToolStrip tsrButtons;
        private System.Windows.Forms.ToolStripButton btnReload;
        private System.Windows.Forms.ToolStripButton btnCalculate;
        private System.Windows.Forms.ToolStripButton btnStop;
        private System.Windows.Forms.ImageList ilTreeView;
        private System.Windows.Forms.TreeView tvwEstimationCells;
        private System.Windows.Forms.ContextMenuStrip cmsEstimationCells;
        private System.Windows.Forms.ToolStripMenuItem tsmiCheckAll;
        private System.Windows.Forms.ToolStripMenuItem tsmiUncheckAll;
        private System.Windows.Forms.ToolStripMenuItem tsmiCheckLevel1;
        private System.Windows.Forms.ToolStripMenuItem tsmiCheckLevel2;
        private System.Windows.Forms.ToolStripMenuItem tsmiCheckLevel3;
        private System.Windows.Forms.SplitContainer splLog;
        private System.Windows.Forms.TableLayoutPanel tlpAuxiliaryVariableTotals;
        private System.Windows.Forms.GroupBox grpAuxiliaryVariableTotals;
        private System.Windows.Forms.DataGridView dgvData;
        private System.Windows.Forms.GroupBox grpConfigCollection;
        private System.Windows.Forms.Label lblConfigCollection;
        private System.Windows.Forms.TableLayoutPanel tlpFilters;
        private System.Windows.Forms.ComboBox cboGUIVersion;
        private System.Windows.Forms.ComboBox cboExtensionVersion;
        private System.Windows.Forms.Label lblGUIVersionCaption;
        private System.Windows.Forms.Label lblConfigurationCaption;
        private System.Windows.Forms.Label lblExtensionVersionCaption;
        private System.Windows.Forms.ComboBox cboConfiguration;
        private System.Windows.Forms.GroupBox grpLog;
        private System.Windows.Forms.TextBox txtLog;
        private System.Windows.Forms.ProgressBar prgCalcTotal;
        private System.Windows.Forms.ToolStripMenuItem tsmiCheckUncalculated;
        private System.Windows.Forms.ToolStripMenuItem tsmiCheckIncomplete;
        private System.Windows.Forms.ToolStripMenuItem tsmiCheckCalculated;
    }

}