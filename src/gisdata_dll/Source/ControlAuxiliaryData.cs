﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.AuxiliaryData;
using ZaJi.NfiEstaPg.Core;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.Catalog;

namespace ZaJi.ModuleAuxiliaryData
{

    /// <summary>
    /// <para lang="cs">Ovládací prvek "Modul pro pomocná data"</para>
    /// <para lang="en">Control "Module for auxiliary data"</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    public partial class ControlAuxiliaryData
        : UserControl, INfiEstaControl, IAuxiliaryDataControl
    {

        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Databázové tabulky</para>
        /// <para lang="en">Database tables</para>
        /// </summary>
        private NfiEstaDB database;

        /// <summary>
        /// <para lang="cs">Jazyková verze</para>
        /// <para lang="en">Language version</para>
        /// </summary>
        private LanguageVersion languageVersion;

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků</para>
        /// <para lang="en">File with control labels</para>
        /// </summary>
        private LanguageFile languageFile;

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro pomocná data</para>
        /// <para lang="en">Module for auxiliary data setting</para>
        /// </summary>
        private Setting setting;

        private string msgReset = String.Empty;
        private string msgSQLCommandsDisplayEnabled = String.Empty;
        private string msgSQLCommandsDisplayDisabled = String.Empty;
        private string msgNoDatabaseConnection = String.Empty;
        private string msgNoAuxiliaryDataDbExtension = String.Empty;
        private string msgNoAuxiliaryDataDbSchema = String.Empty;
        private string msgInvalidAuxiliaryDataDbVersion = String.Empty;
        private string msgValidAuxiliaryDataDbVersion = String.Empty;
        private string msgNoNfiEstaDbExtension = String.Empty;
        private string msgNoNfiEstaDbSchema = String.Empty;
        private string msgInvalidNfiEstaDbVersion = String.Empty;
        private string msgValidNfiEstaDbVersion = String.Empty;

        #endregion Private Fields


        #region Controls

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Modul pro pomocná data"</para>
        /// <para lang="en">Control "Auxiliary data module"</para>
        /// </summary>
        private ControlAuxiliaryDataMain ctrAuxiliaryDataMain;

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Modul pro pomocná data" (read-only)</para>
        /// <para lang="en">Control "Auxiliary data module" (read-only)</para>
        /// </summary>
        internal ControlAuxiliaryDataMain CtrAuxiliaryDataMain
        {
            get
            {
                return ctrAuxiliaryDataMain ??
                    throw new ArgumentNullException(
                        message: $"{nameof(CtrAuxiliaryDataMain)} is null.",
                        paramName: nameof(CtrAuxiliaryDataMain));
            }
        }

        #endregion Controls


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor ovládacího prvku</para>
        /// <para lang="en">Control constructor</para>
        /// </summary>
        public ControlAuxiliaryData()
        {
            InitializeComponent();

            Initialize(
                controlOwner: null,
                languageVersion: LanguageVersion.International,
                languageFile: new LanguageFile(),
                setting: new Setting(),
                withEventHandlers: true);
        }

        /// <summary>
        /// <para lang="cs">Konstruktor ovládacího prvku</para>
        /// <para lang="en">Control constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        /// <param name="languageVersion">
        /// <para lang="cs">Jazyková verze</para>
        /// <para lang="en">Language version</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubor s popisky ovládacích prvků</para>
        /// <para lang="en">File with control labels</para>
        /// </param>
        /// <param name="setting">
        /// <para lang="cs">Nastavení modulu pro pomocná data</para>
        /// <para lang="en">Module for auxiliary data setting</para>
        /// </param>
        public ControlAuxiliaryData(
            Control controlOwner,
            LanguageVersion languageVersion,
            LanguageFile languageFile,
            Setting setting)
        {
            InitializeComponent();

            Initialize(
                controlOwner: controlOwner,
                languageVersion: languageVersion,
                languageFile: languageFile,
                setting: setting,
                withEventHandlers: true);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                return controlOwner;
            }
            set
            {
                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables(read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze</para>
        /// <para lang="en">Language version</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return languageVersion;
            }
            set
            {
                languageVersion = value;
                Setting.DBSetting.LanguageVersion = LanguageVersion;
                Database.Postgres.Setting = Setting.DBSetting;

                InitializeLabels();
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků</para>
        /// <para lang="en">File with control labels</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return languageFile ??
                    new LanguageFile();
            }
            set
            {
                languageFile = value ??
                    new LanguageFile();

                InitializeLabels();
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro pomocná data (read-only)</para>
        /// <para lang="en">Module for auxiliary data setting (read-only)</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return setting ??
                    new Setting();
            }
            set
            {
                setting = value ??
                    new Setting();
                Database.Postgres.Setting = Setting.DBSetting;
            }
        }

        #endregion Common Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(tsmiDatabase),                     "Databáze" },
                        { nameof(tsmiConnect),                      "Připojit" },
                        { nameof(tsmiAuxiliaryDataExtension),       "Data extenze $1" },
                        { nameof(tsmiNfiEstaExtension),             "Data extenze $1" },
                        { nameof(msgReset),                         "Nastavení modulu pro pomocná data bylo resetováno na výchozí hodnoty." },
                        { nameof(msgSQLCommandsDisplayEnabled),     "Povoleno zobrazování SQL příkazů." },
                        { nameof(msgSQLCommandsDisplayDisabled),    "Zakázáno zobrazování SQL příkazů." },
                        { nameof(msgNoDatabaseConnection),          "Aplikace není připojena k databázi." },
                        { nameof(msgNoAuxiliaryDataDbExtension),    "V databázi není nainstalovaná extenze $1." },
                        { nameof(msgNoAuxiliaryDataDbSchema),       "V databázi neexistuje schéma $1." },
                        { nameof(msgInvalidAuxiliaryDataDbVersion), "Nainstalovaná verze databázové extenze $1 $2 se neshoduje s očekávanou verzí $3." },
                        { nameof(msgValidAuxiliaryDataDbVersion),   "Databázová extenze $1 $2 [$3]." },
                        { nameof(msgNoNfiEstaDbExtension),          "V databázi není nainstalovaná extenze $1." },
                        { nameof(msgNoNfiEstaDbSchema),             "V databázi neexistuje schéma $1." },
                        { nameof(msgInvalidNfiEstaDbVersion),       "Nainstalovaná verze databázové extenze $1 $2 se neshoduje s očekávanou verzí $3." },
                        { nameof(msgValidNfiEstaDbVersion),         "Databázová extenze $1 $2 [$3]." }
                    }
                    : languageFile.NationalVersion.Data.TryGetValue(
                        key: nameof(ControlAuxiliaryData),
                        out Dictionary<string, string> dictNational)
                            ? dictNational
                            : [],

                LanguageVersion.International => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(tsmiDatabase),                     "Database" },
                        { nameof(tsmiConnect),                      "Connect" },
                        { nameof(tsmiAuxiliaryDataExtension),       "Data from extension $1" },
                        { nameof(tsmiNfiEstaExtension),             "Data from extension $1"},
                        { nameof(msgReset),                         "GIS data module settings have been reset to default values." },
                        { nameof(msgSQLCommandsDisplayEnabled),     "Display of SQL statements enabled." },
                        { nameof(msgSQLCommandsDisplayDisabled),    "Display of SQL statements disabled." },
                        { nameof(msgNoDatabaseConnection),          "Application is not connected to database." },
                        { nameof(msgNoAuxiliaryDataDbExtension),    "Database extension $1 is not installed." },
                        { nameof(msgNoAuxiliaryDataDbSchema),       "Schema $1 does not exist in the database." },
                        { nameof(msgInvalidAuxiliaryDataDbVersion), "Installed version of the database extension $1 $2 does not correspond with expected version $3." },
                        { nameof(msgValidAuxiliaryDataDbVersion),   "Database extension $1 $2 [$3]." },
                        { nameof(msgNoNfiEstaDbExtension),          "Database extension $1 is not installed."},
                        { nameof(msgNoNfiEstaDbSchema),             "Schema $1 does not exist in the database." },
                        { nameof(msgInvalidNfiEstaDbVersion),       "Installed version of the database extension $1 $2 does not correspond with expected version $3." },
                        { nameof(msgValidNfiEstaDbVersion),         "Database extension $1 $2 [$3]." }
                    }
                    : languageFile.InternationalVersion.Data.TryGetValue(
                        key: nameof(ControlAuxiliaryData),
                        out Dictionary<string, string> dictInternational)
                            ? dictInternational
                            : [],

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku</para>
        /// <para lang="en">Initializing the control</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        /// <param name="languageVersion">
        /// <para lang="cs">Jazyková verze</para>
        /// <para lang="en">Language version</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubor s popisky ovládacích prvků</para>
        /// <para lang="en">File with control labels</para>
        /// </param>
        /// <param name="setting">
        /// <para lang="cs">Nastavení modulu pro pomocná data</para>
        /// <para lang="en">Module for auxiliary data setting</para>
        /// </param>
        /// <param name="withEventHandlers">
        /// <para lang="cs">Inicializace včetně obsluhy událostí tlačítek</para>
        /// <para lang="en">Initialize with button event handlers</para>
        /// </param>
        private void Initialize(
            Control controlOwner,
            LanguageVersion languageVersion,
            LanguageFile languageFile,
            Setting setting,
            bool withEventHandlers)
        {
            BorderStyle = BorderStyle.None;

            // Controls
            // Pozn: Při inicializaci objektu je třeba Controls nastavit na null,
            // tj. před vytvořením objektu připojení k databázi,
            // jinak v nich dochází k přístupu k databázi, ke které ještě neexistuje připojení
            ctrAuxiliaryDataMain = null;
            pnlControls.Controls.Clear();

            // ControlOwner
            ControlOwner = controlOwner;

            // Database
            Disconnect();
            database = new NfiEstaDB();
            database.Postgres.StayConnected = true;

            Database.Postgres.ConnectionError +=
                new ZaJi.PostgreSQL.PostgreSQLWrapper.ConnectionErrorHandler(
                    (sender, e) =>
                    {
                        MessageBox.Show(
                            text: e.ToString(),
                            caption: e.MessageBoxCaption,
                            buttons: MessageBoxButtons.OK,
                            icon: MessageBoxIcon.Error);
                    });

            Database.Postgres.ExceptionReceived +=
                new ZaJi.PostgreSQL.PostgreSQLWrapper.ExceptionReceivedHandler(
                    (sender, e) =>
                    {
                        if (!Setting.DBSuppressError)
                        {
                            MessageBox.Show(
                                text: e.ToString(),
                                caption: e.MessageBoxCaption,
                                buttons: MessageBoxButtons.OK,
                                icon: MessageBoxIcon.Error);
                        }
                    });

            Database.Postgres.NoticeReceived +=
                new ZaJi.PostgreSQL.PostgreSQLWrapper.NoticeReceivedHandler(
                (sender, e) =>
                {
                    if (!Setting.DBSuppressWarning)
                    {
                        MessageBox.Show(
                            text: $"{e.Notice.MessageText}",
                            caption: $"{e.Notice.Severity}",
                            buttons: MessageBoxButtons.OK,
                            icon: MessageBoxIcon.Information);
                    }
                });

            // LanguageVersion
            LanguageVersion = languageVersion;

            // LanguageFile
            LanguageFile = languageFile;

            // Setting
            Setting = setting;

            // StatusStrip
            ssrMain.Visible = Setting.StatusStripVisible;

            if (withEventHandlers)
            {
                tsmiConnect.Click += new EventHandler(
                    (sender, e) => { Connect(); });

                tsmiAuxiliaryDataExtension.Click += new EventHandler(
                    (sender, e) => { AuxiliaryDataExtension(); });

                tsmiNfiEstaExtension.Click += new EventHandler(
                    (sender, e) => { NfiEstaExtension(); });
            }

#if DEBUG
            tsmiSeparator.Visible = true;
            tsmiAuxiliaryDataExtension.Visible = true;
            tsmiNfiEstaExtension.Visible = true;
#endif

#if RELEASE
            tsmiSeparator.Visible = false;
            tsmiAuxiliaryDataExtension.Visible = false;
            tsmiNfiEstaExtension.Visible = false;
#endif
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků ovládacího prvku</para>
        /// <para lang="en">Initializing module control labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            tsmiDatabase.Text =
                labels.TryGetValue(key: nameof(tsmiDatabase),
                out string tsmiDatabaseText)
                ? tsmiDatabaseText
                : String.Empty;

            tsmiConnect.Text =
                labels.TryGetValue(key: nameof(tsmiConnect),
                out string tsmiConnectText)
                ? tsmiConnectText
                : String.Empty;

            tsmiAuxiliaryDataExtension.Text =
                (labels.TryGetValue(key: nameof(tsmiAuxiliaryDataExtension),
                out string tsmiAuxiliaryDataExtensionText)
                ? tsmiAuxiliaryDataExtensionText
                : String.Empty)
                .Replace(oldValue: "$1", newValue: ADSchema.ExtensionName);

            tsmiNfiEstaExtension.Text =
                (labels.TryGetValue(key: nameof(tsmiNfiEstaExtension),
                out string tsmiNfiEstaExtensionText)
                ? tsmiNfiEstaExtensionText
                : String.Empty)
                .Replace(oldValue: "$1", newValue: NfiEstaSchema.ExtensionName);

            msgReset =
                labels.TryGetValue(key: nameof(msgReset),
                out msgReset)
                ? msgReset
                : String.Empty;

            msgSQLCommandsDisplayEnabled =
                labels.TryGetValue(key: nameof(msgSQLCommandsDisplayEnabled),
                out msgSQLCommandsDisplayEnabled)
                ? msgSQLCommandsDisplayEnabled
                : String.Empty;

            msgSQLCommandsDisplayDisabled =
                labels.TryGetValue(key: nameof(msgSQLCommandsDisplayDisabled),
                out msgSQLCommandsDisplayDisabled)
                ? msgSQLCommandsDisplayDisabled
                : String.Empty;

            msgNoDatabaseConnection =
                labels.TryGetValue(key: nameof(msgNoDatabaseConnection),
                out msgNoDatabaseConnection)
                ? msgNoDatabaseConnection
                : String.Empty;

            msgNoAuxiliaryDataDbExtension =
                labels.TryGetValue(key: nameof(msgNoAuxiliaryDataDbExtension),
                out msgNoAuxiliaryDataDbExtension)
                ? msgNoAuxiliaryDataDbExtension
                : String.Empty;

            msgNoAuxiliaryDataDbSchema =
                labels.TryGetValue(key: nameof(msgNoAuxiliaryDataDbSchema),
                out msgNoAuxiliaryDataDbSchema)
                ? msgNoAuxiliaryDataDbSchema
                : String.Empty;

            msgInvalidAuxiliaryDataDbVersion =
                labels.TryGetValue(key: nameof(msgInvalidAuxiliaryDataDbVersion),
                out msgInvalidAuxiliaryDataDbVersion)
                ? msgInvalidAuxiliaryDataDbVersion
                : String.Empty;

            msgValidAuxiliaryDataDbVersion =
                labels.TryGetValue(key: nameof(msgValidAuxiliaryDataDbVersion),
                out msgValidAuxiliaryDataDbVersion)
                ? msgValidAuxiliaryDataDbVersion
                : String.Empty;

            msgNoNfiEstaDbExtension =
                labels.TryGetValue(key: nameof(msgNoNfiEstaDbExtension),
                out msgNoNfiEstaDbExtension)
                ? msgNoNfiEstaDbExtension
                : String.Empty;

            msgNoNfiEstaDbSchema =
                labels.TryGetValue(key: nameof(msgNoNfiEstaDbSchema),
                out msgNoNfiEstaDbSchema)
                ? msgNoNfiEstaDbSchema
                : String.Empty;

            msgInvalidNfiEstaDbVersion =
                labels.TryGetValue(key: nameof(msgInvalidNfiEstaDbVersion),
                out msgInvalidNfiEstaDbVersion)
                ? msgInvalidNfiEstaDbVersion
                : String.Empty;

            msgValidNfiEstaDbVersion =
                labels.TryGetValue(key: nameof(msgValidNfiEstaDbVersion),
                out msgValidNfiEstaDbVersion)
                ? msgValidNfiEstaDbVersion
                : String.Empty;

            ctrAuxiliaryDataMain?.InitializeLabels();

            DisplayAuxiliaryDataExtensionStatus();

            DisplayNfiEstaExtensionStatus();
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent() { }

        /// <summary>
        /// <para lang="cs">Resetuje nastavení modulu pro pomocná data na výchozí hodnoty</para>
        /// <para lang="en">Resets auxiliary data module settings to default values</para>
        /// </summary>
        /// <param name="displayMessage">
        /// <para lang="cs">Zobrazení zprávy o provedeném resetu nastavení modulu</para>
        /// <para lang="en">Display a message about the reset of the module settings</para>
        /// </param>
        public void Reset(bool displayMessage)
        {
            Setting = new Setting();

            if (displayMessage)
            {
                MessageBox.Show(
                    text: msgReset,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
            }
        }

        /// <summary>
        /// <para lang="cs">Připojit k databázi</para>
        /// <para lang="en">Connect to a database</para>
        /// </summary>
        private void Connect()
        {
            Initialize(
                controlOwner: ControlOwner,
                languageVersion: LanguageVersion,
                languageFile: LanguageFile,
                setting: Setting,
                withEventHandlers: false);

            Database.Postgres.Setting = Setting.DBSetting;
            Database.Connect(
                applicationName: Setting.DBSetting.ApplicationName);
            Setting.DBSetting = Database.Postgres.Setting;

            pnlControls.Controls.Clear();

            // Pozn. je nutné spustit obě metody nelze použít zkrácené vyhodnocení logického výrazu
            // musí zde být operátor & (nelze &&)
            if (
                DisplayAuxiliaryDataExtensionStatus()
            // & DisplayNfiEstaExtensionStatus()
            )
            {
                ctrAuxiliaryDataMain =
                    new ControlAuxiliaryDataMain(controlOwner: this)
                    {
                        Dock = DockStyle.Fill
                    };

                LoadContent();
                CtrAuxiliaryDataMain.LoadContent();

                pnlControls.Controls.Add(value: CtrAuxiliaryDataMain);
            }
        }

        /// <summary>
        /// <para lang="cs">Odpojit od databáze</para>
        /// <para lang="en">Disconnect from database</para>
        /// </summary>
        public void Disconnect()
        {
            Database?.Postgres?.CloseConnection();
        }

        /// <summary>
        /// <para lang="cs">ThreadSave povolení přístupu k ovládacímu prvku</para>
        /// <para lang="en">ThreadSave enable access to the control</para>
        /// </summary>
        public void Enable()
        {
            if (InvokeRequired)
            {
                BeginInvoke(
                    method: (MethodInvoker)delegate ()
                    {
                        Enabled = true;
                    });
            }
            else
            {
                Enabled = true;
            }
        }

        /// <summary>
        /// <para lang="cs">ThreadSave zakázání přístupu k ovládacímu prvku</para>
        /// <para lang="en">ThreadSave disable access to the control</para>
        /// </summary>
        public void Disable()
        {
            if (InvokeRequired)
            {
                BeginInvoke(
                    method: (MethodInvoker)delegate ()
                    {
                        Enabled = false;
                    });
            }
            else
            {
                Enabled = false;
            }
        }

        /// <summary>
        /// <para lang="cs">Zobrazí stav instalace extenze nfiesta_gisdata</para>
        /// <para lang="en">Method displays installation status of the database extension nfiesta_gisdata</para>
        /// </summary>
        /// <returns>
        /// <para lang="cs">Vrací true, pokud v připojené databázi je nainstalovaná extenze nfiesta_gisdata
        /// v kompatibilní verzi s aplikací. Jinak false.</para>
        /// <para lang="en">It returns true, when there is installed database extension nfiesta_gisdata
        /// in compatible version with application.</para>
        /// </returns>
        private bool DisplayAuxiliaryDataExtensionStatus()
        {
            lblStatus.Text = Database.Postgres.ConnectionInfo();

            // Není připojeno k databázi
            if (!Database.Postgres.Initialized)
            {
                lblAuxiliaryDataExtension.Text = String.Empty;
                return false;
            }

            // Extenze nfiesta_gisdata neexistuje
            if (Database.Postgres.Catalog.Extensions[ADSchema.ExtensionName] == null)
            {
                lblAuxiliaryDataExtension.ForeColor = System.Drawing.Color.Red;
                lblAuxiliaryDataExtension.Text =
                    msgNoAuxiliaryDataDbExtension
                    .Replace(oldValue: "$1", newValue: ADSchema.ExtensionName);
                return false;
            }

            // Extenze existuje
            ZaJi.PostgreSQL.DataModel.ExtensionVersion dbExtensionVersion =
                new(version: Database.Postgres.Catalog.Extensions[ADSchema.ExtensionName].Version);

            // Schéma neexistuje
            if (Database.Postgres.Catalog.Extensions[ADSchema.ExtensionName].Schema == null)
            {
                lblAuxiliaryDataExtension.ForeColor = System.Drawing.Color.Red;
                lblAuxiliaryDataExtension.Text =
                    msgNoAuxiliaryDataDbSchema
                    .Replace(oldValue: "$1", newValue: ADSchema.Name);
                return false;
            }

            // Neshoduje se minor verze
            if (!ADSchema.ExtensionVersion.MinorEquals(obj: dbExtensionVersion))
            {
                lblAuxiliaryDataExtension.ForeColor = System.Drawing.Color.Red;
                lblAuxiliaryDataExtension.Text =
                    msgInvalidAuxiliaryDataDbVersion
                    .Replace(oldValue: "$1", newValue: ADSchema.ExtensionName)
                    .Replace(oldValue: "$2", newValue: Database.Postgres.Catalog.Extensions[ADSchema.ExtensionName].Version)
                    .Replace(oldValue: "$3", newValue: ADSchema.ExtensionVersion.ToString());
                return false;
            }

            // Neshoduje se patch verze
            if (!ADSchema.ExtensionVersion.Equals(obj: dbExtensionVersion))
            {
                lblAuxiliaryDataExtension.ForeColor = System.Drawing.Color.Red;
                lblAuxiliaryDataExtension.Text =
                    msgInvalidAuxiliaryDataDbVersion
                    .Replace(oldValue: "$1", newValue: ADSchema.ExtensionName)
                    .Replace(oldValue: "$2", newValue: Database.Postgres.Catalog.Extensions[ADSchema.ExtensionName].Version)
                    .Replace(oldValue: "$3", newValue: ADSchema.ExtensionVersion.ToString());
                return true;
            }

            // Extenze je OK
            lblAuxiliaryDataExtension.ForeColor = System.Drawing.SystemColors.ControlText;
            lblAuxiliaryDataExtension.Text =
                 msgValidAuxiliaryDataDbVersion
                .Replace(oldValue: "$1", newValue: ADSchema.ExtensionName)
                .Replace(oldValue: "$2", newValue: ADSchema.ExtensionVersion.ToString())
                .Replace(oldValue: "$3", newValue: ADSchema.Name);

            return true;
        }

        /// <summary>
        /// <para lang="cs">Zobrazí stav instalace extenze nfiesta_pg</para>
        /// <para lang="en">Method displays installation status of the database extension nfiesta_pg</para>
        /// </summary>
        /// <returns>
        /// <para lang="cs">Vrací true, pokud v připojené databázi je nainstalovaná extenze nfiesta_pg
        /// v kompatibilní verzi s aplikací. Jinak false.</para>
        /// <para lang="en">It returns true, when there is installed database extension nfiesta_pg
        /// in compatible version with application.</para>
        /// </returns>
        private bool DisplayNfiEstaExtensionStatus()
        {
            lblStatus.Text = Database.Postgres.ConnectionInfo();

            // Není připojeno k databázi
            // No database connection
            if (!Database.Postgres.Initialized)
            {
                lblNfiestaExtension.Text = String.Empty;
                return false;
            }

            // Extenze nfiesta neexistuje
            // The nfiesta extension does not exist
            if (Database.Postgres.Catalog.Extensions[NfiEstaSchema.ExtensionName] == null)
            {
                lblNfiestaExtension.ForeColor = System.Drawing.Color.Red;
                lblNfiestaExtension.Text =
                    msgNoNfiEstaDbExtension
                    .Replace(oldValue: "$1", newValue: NfiEstaSchema.ExtensionName);
                return false;
            }

            // Extenze existuje
            ZaJi.PostgreSQL.DataModel.ExtensionVersion dbExtensionVersion =
                new(version: Database.Postgres.Catalog.Extensions[NfiEstaSchema.ExtensionName].Version);

            // Schéma neexistuje
            if (Database.Postgres.Catalog.Extensions[NfiEstaSchema.ExtensionName].Schema == null)
            {
                lblNfiestaExtension.ForeColor = System.Drawing.Color.Red;
                lblNfiestaExtension.Text =
                    msgNoNfiEstaDbSchema
                    .Replace(oldValue: "$1", newValue: NfiEstaSchema.Name);
                return false;
            }

            // Neshoduje se minor verze
            if (!NfiEstaSchema.ExtensionVersion.MinorEquals(obj: dbExtensionVersion))
            {
                lblNfiestaExtension.ForeColor = System.Drawing.Color.Red;
                lblNfiestaExtension.Text =
                    msgInvalidNfiEstaDbVersion
                    .Replace(oldValue: "$1", newValue: NfiEstaSchema.ExtensionName)
                    .Replace(oldValue: "$2", newValue: Database.Postgres.Catalog.Extensions[NfiEstaSchema.ExtensionName].Version)
                    .Replace(oldValue: "$3", newValue: NfiEstaSchema.ExtensionVersion.ToString());
                return false;
            }

            // Neshoduje se patch verze
            if (!NfiEstaSchema.ExtensionVersion.Equals(obj: dbExtensionVersion))
            {
                lblNfiestaExtension.ForeColor = System.Drawing.Color.Red;
                lblNfiestaExtension.Text =
                    msgInvalidNfiEstaDbVersion
                     .Replace(oldValue: "$1", newValue: NfiEstaSchema.ExtensionName)
                     .Replace(oldValue: "$2", newValue: Database.Postgres.Catalog.Extensions[NfiEstaSchema.ExtensionName].Version)
                     .Replace(oldValue: "$3", newValue: NfiEstaSchema.ExtensionVersion.ToString());
                return true;
            }

            // Extenze je OK
            lblNfiestaExtension.ForeColor = System.Drawing.SystemColors.ControlText;
            lblNfiestaExtension.Text =
                msgValidNfiEstaDbVersion
                .Replace(oldValue: "$1", newValue: NfiEstaSchema.ExtensionName)
                .Replace(oldValue: "$2", newValue: NfiEstaSchema.ExtensionVersion.ToString())
                .Replace(oldValue: "$3", newValue: NfiEstaSchema.Name);

            return true;
        }

        /// <summary>
        /// <para lang="cs">Zobrazí nebo skryje stavový řádek</para>
        /// <para lang="en">Shows or hides status strip</para>
        /// </summary>
        public void DisplayStatusStrip()
        {
            if (Setting.StatusStripVisible)
            {
                ShowStatusStrip();
            }
            else
            {
                HideStatusStrip();
            }
        }

        /// <summary>
        /// <para lang="cs">Zobrazí stavový řádek</para>
        /// <para lang="en">Show status strip</para>
        /// </summary>
        public void ShowStatusStrip()
        {
            Setting.StatusStripVisible = true;
            ssrMain.Visible = Setting.StatusStripVisible;
        }

        /// <summary>
        /// <para lang="cs">Zobrazí stavový řádek</para>
        /// <para lang="en">Hide status strip</para>
        /// </summary>
        public void HideStatusStrip()
        {
            Setting.StatusStripVisible = false;
            ssrMain.Visible = Setting.StatusStripVisible;
        }

        /// <summary>
        /// <para lang="cs">Povolí nebo zakáže zobrazování SQL příkazů</para>
        /// <para lang="en">Enables or disables the display of SQL statements</para>
        /// </summary>
        public void SwitchDisplaySQLCommands()
        {
            Setting.Verbose =
                !Setting.Verbose;

            if (Setting.Verbose)
            {
                MessageBox.Show(
                    text: msgSQLCommandsDisplayEnabled,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show(
                    text: msgSQLCommandsDisplayDisabled,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
            }
        }

        /// <summary>
        /// <para lang="cs">Zobrazí data extenze nfiesta_gisdata</para>
        /// <para lang="en">Displays data from extension nfiesta_gisdata"</para>
        /// </summary>
        private void AuxiliaryDataExtension()
        {
            if (!Database.Postgres.Initialized)
            {
                MessageBox.Show(
                    text: msgNoDatabaseConnection,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return;
            }

            Database.SAuxiliaryData.OmittedTables =
            [
                "c_fty_10_eu_bands",
                "c_fty_10_eu_category",
                "c_gfc_20_eu_bands",
                "c_gfc_20_eu_category",
                "c_tcd_10_eu_bands",
                "r_fty_10_eu",
                "r_gfc_20_eu",
                "r_tcd_10_eu"
            ];

            List<DBStoredProcedure> storedProcedures =
            [..
                Database.Postgres.Catalog
                    .Schemas[ADSchema.Name]
                    .StoredProcedures.Items
                    .OrderBy(a => a.Name)
            ];

            FormAuxiliaryData frm = new(controlOwner: this)
            {
                Limit = 1000
            };

            if (frm.ShowDialog() == DialogResult.OK) { }
        }

        /// <summary>
        /// <para lang="cs">Zobrazí data extenze nfiesta_target_data</para>
        /// <para lang="en">Displays data from extension nfiesta_target_data"</para>
        /// </summary>
        private void NfiEstaExtension()
        {
            if (!Database.Postgres.Initialized)
            {
                MessageBox.Show(
                    text: msgNoDatabaseConnection,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
                return;
            }

            List<ZaJi.PostgreSQL.Catalog.DBStoredProcedure> storedProcedures =
            [..
                Database.Postgres.Catalog
                    .Schemas[NfiEstaSchema.Name]
                    .StoredProcedures.Items
                    .OrderBy(a => a.Name)
            ];

            FormNfiEsta frm = new(controlOwner: this)
            {
                Limit = 1000
            };

            if (frm.ShowDialog() == DialogResult.OK) { }
        }

        #endregion Methods

    }

}