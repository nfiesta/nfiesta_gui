﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi.ModuleAuxiliaryData
{

    partial class ControlConfigCollectionView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            tlpMain = new System.Windows.Forms.TableLayoutPanel();
            grpCaption = new System.Windows.Forms.GroupBox();
            lblCaption = new System.Windows.Forms.Label();
            grpConfigCollectionParameters = new System.Windows.Forms.GroupBox();
            pnlConfigCollectionParameters = new System.Windows.Forms.Panel();
            tlpConfigCollectionParameters = new System.Windows.Forms.TableLayoutPanel();
            lblRowNumber18 = new System.Windows.Forms.Label();
            lblRowNumber17 = new System.Windows.Forms.Label();
            lblRowNumber16 = new System.Windows.Forms.Label();
            lblRowNumber15 = new System.Windows.Forms.Label();
            lblRowNumber14 = new System.Windows.Forms.Label();
            lblRowNumber13 = new System.Windows.Forms.Label();
            lblRowNumber12 = new System.Windows.Forms.Label();
            lblRowNumber11 = new System.Windows.Forms.Label();
            lblRowNumber10 = new System.Windows.Forms.Label();
            lblRowNumber09 = new System.Windows.Forms.Label();
            lblRowNumber08 = new System.Windows.Forms.Label();
            lblRowNumber07 = new System.Windows.Forms.Label();
            lblRowNumber06 = new System.Windows.Forms.Label();
            lblEditDateValue = new System.Windows.Forms.Label();
            lblEditDateCaption = new System.Windows.Forms.Label();
            lblRefIDTotalValue = new System.Windows.Forms.Label();
            lblRefIDTotalCaption = new System.Windows.Forms.Label();
            lblRefIDLayerPointsValue = new System.Windows.Forms.Label();
            lblRefIDLayerPointsCaption = new System.Windows.Forms.Label();
            lblTagValue = new System.Windows.Forms.Label();
            lblTagCaption = new System.Windows.Forms.Label();
            lblTotalPointsValue = new System.Windows.Forms.Label();
            lblTotalPointsCaption = new System.Windows.Forms.Label();
            lblConditionValue = new System.Windows.Forms.Label();
            lblConditionCaption = new System.Windows.Forms.Label();
            lblUnitValue = new System.Windows.Forms.Label();
            lblUnitCaption = new System.Windows.Forms.Label();
            lblColumnNameValue = new System.Windows.Forms.Label();
            lblColumnNameCaption = new System.Windows.Forms.Label();
            lblColumnIdentValue = new System.Windows.Forms.Label();
            lblColumnIdentCaption = new System.Windows.Forms.Label();
            lblTableNameValue = new System.Windows.Forms.Label();
            lblTableNameCaption = new System.Windows.Forms.Label();
            lblSchemaNameValue = new System.Windows.Forms.Label();
            lblSchemaNameCaption = new System.Windows.Forms.Label();
            lblCatalogNameValue = new System.Windows.Forms.Label();
            lblCatalogNameCaption = new System.Windows.Forms.Label();
            lblAggregatedValue = new System.Windows.Forms.Label();
            lblAggregatedCaption = new System.Windows.Forms.Label();
            lblClosedValue = new System.Windows.Forms.Label();
            lblClosedCaption = new System.Windows.Forms.Label();
            lblDescriptionValue = new System.Windows.Forms.Label();
            lblDescriptionCaption = new System.Windows.Forms.Label();
            lblLabelValue = new System.Windows.Forms.Label();
            lblLabelCaption = new System.Windows.Forms.Label();
            lblConfigFunctionIdValue = new System.Windows.Forms.Label();
            lblConfigFunctionIdCaption = new System.Windows.Forms.Label();
            lblAuxiliaryVariableIdValue = new System.Windows.Forms.Label();
            lblAuxiliaryVariableIdCaption = new System.Windows.Forms.Label();
            lblIdValue = new System.Windows.Forms.Label();
            lblIdCaption = new System.Windows.Forms.Label();
            lblRowNumber00 = new System.Windows.Forms.Label();
            lblRowNumber01 = new System.Windows.Forms.Label();
            lblRowNumber02 = new System.Windows.Forms.Label();
            lblRowNumber03 = new System.Windows.Forms.Label();
            lblRowNumber04 = new System.Windows.Forms.Label();
            lblRowNumber05 = new System.Windows.Forms.Label();
            tlpMain.SuspendLayout();
            grpCaption.SuspendLayout();
            grpConfigCollectionParameters.SuspendLayout();
            pnlConfigCollectionParameters.SuspendLayout();
            tlpConfigCollectionParameters.SuspendLayout();
            SuspendLayout();
            // 
            // tlpMain
            // 
            tlpMain.ColumnCount = 1;
            tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.Controls.Add(grpCaption, 0, 0);
            tlpMain.Controls.Add(grpConfigCollectionParameters, 0, 1);
            tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpMain.Location = new System.Drawing.Point(0, 0);
            tlpMain.Name = "tlpMain";
            tlpMain.RowCount = 2;
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.Size = new System.Drawing.Size(960, 540);
            tlpMain.TabIndex = 2;
            // 
            // grpCaption
            // 
            grpCaption.Controls.Add(lblCaption);
            grpCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            grpCaption.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            grpCaption.ForeColor = System.Drawing.Color.MediumBlue;
            grpCaption.Location = new System.Drawing.Point(0, 0);
            grpCaption.Margin = new System.Windows.Forms.Padding(0);
            grpCaption.Name = "grpCaption";
            grpCaption.Padding = new System.Windows.Forms.Padding(5);
            grpCaption.Size = new System.Drawing.Size(960, 60);
            grpCaption.TabIndex = 14;
            grpCaption.TabStop = false;
            grpCaption.Text = "grpCaption";
            // 
            // lblCaption
            // 
            lblCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblCaption.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, 0);
            lblCaption.Location = new System.Drawing.Point(5, 25);
            lblCaption.Margin = new System.Windows.Forms.Padding(0);
            lblCaption.Name = "lblCaption";
            lblCaption.Size = new System.Drawing.Size(950, 30);
            lblCaption.TabIndex = 0;
            lblCaption.Text = "lblCaption";
            lblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // grpConfigCollectionParameters
            // 
            grpConfigCollectionParameters.Controls.Add(pnlConfigCollectionParameters);
            grpConfigCollectionParameters.Dock = System.Windows.Forms.DockStyle.Fill;
            grpConfigCollectionParameters.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            grpConfigCollectionParameters.ForeColor = System.Drawing.Color.MediumBlue;
            grpConfigCollectionParameters.Location = new System.Drawing.Point(0, 60);
            grpConfigCollectionParameters.Margin = new System.Windows.Forms.Padding(0);
            grpConfigCollectionParameters.Name = "grpConfigCollectionParameters";
            grpConfigCollectionParameters.Padding = new System.Windows.Forms.Padding(5);
            grpConfigCollectionParameters.Size = new System.Drawing.Size(960, 480);
            grpConfigCollectionParameters.TabIndex = 13;
            grpConfigCollectionParameters.TabStop = false;
            grpConfigCollectionParameters.Text = "grpConfigCollectionParameters";
            // 
            // pnlConfigCollectionParameters
            // 
            pnlConfigCollectionParameters.AutoScroll = true;
            pnlConfigCollectionParameters.Controls.Add(tlpConfigCollectionParameters);
            pnlConfigCollectionParameters.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlConfigCollectionParameters.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            pnlConfigCollectionParameters.ForeColor = System.Drawing.Color.Black;
            pnlConfigCollectionParameters.Location = new System.Drawing.Point(5, 25);
            pnlConfigCollectionParameters.Margin = new System.Windows.Forms.Padding(0);
            pnlConfigCollectionParameters.Name = "pnlConfigCollectionParameters";
            pnlConfigCollectionParameters.Size = new System.Drawing.Size(950, 450);
            pnlConfigCollectionParameters.TabIndex = 16;
            // 
            // tlpConfigCollectionParameters
            // 
            tlpConfigCollectionParameters.ColumnCount = 3;
            tlpConfigCollectionParameters.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            tlpConfigCollectionParameters.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 300F));
            tlpConfigCollectionParameters.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpConfigCollectionParameters.Controls.Add(lblRowNumber18, 0, 18);
            tlpConfigCollectionParameters.Controls.Add(lblRowNumber17, 0, 17);
            tlpConfigCollectionParameters.Controls.Add(lblRowNumber16, 0, 16);
            tlpConfigCollectionParameters.Controls.Add(lblRowNumber15, 0, 15);
            tlpConfigCollectionParameters.Controls.Add(lblRowNumber14, 0, 14);
            tlpConfigCollectionParameters.Controls.Add(lblRowNumber13, 0, 13);
            tlpConfigCollectionParameters.Controls.Add(lblRowNumber12, 0, 12);
            tlpConfigCollectionParameters.Controls.Add(lblRowNumber11, 0, 11);
            tlpConfigCollectionParameters.Controls.Add(lblRowNumber10, 0, 10);
            tlpConfigCollectionParameters.Controls.Add(lblRowNumber09, 0, 9);
            tlpConfigCollectionParameters.Controls.Add(lblRowNumber08, 0, 8);
            tlpConfigCollectionParameters.Controls.Add(lblRowNumber07, 0, 7);
            tlpConfigCollectionParameters.Controls.Add(lblRowNumber06, 0, 6);
            tlpConfigCollectionParameters.Controls.Add(lblEditDateValue, 2, 18);
            tlpConfigCollectionParameters.Controls.Add(lblEditDateCaption, 1, 18);
            tlpConfigCollectionParameters.Controls.Add(lblRefIDTotalValue, 2, 17);
            tlpConfigCollectionParameters.Controls.Add(lblRefIDTotalCaption, 1, 17);
            tlpConfigCollectionParameters.Controls.Add(lblRefIDLayerPointsValue, 2, 16);
            tlpConfigCollectionParameters.Controls.Add(lblRefIDLayerPointsCaption, 1, 16);
            tlpConfigCollectionParameters.Controls.Add(lblTagValue, 2, 15);
            tlpConfigCollectionParameters.Controls.Add(lblTagCaption, 1, 15);
            tlpConfigCollectionParameters.Controls.Add(lblTotalPointsValue, 2, 14);
            tlpConfigCollectionParameters.Controls.Add(lblTotalPointsCaption, 1, 14);
            tlpConfigCollectionParameters.Controls.Add(lblConditionValue, 2, 13);
            tlpConfigCollectionParameters.Controls.Add(lblConditionCaption, 1, 13);
            tlpConfigCollectionParameters.Controls.Add(lblUnitValue, 2, 12);
            tlpConfigCollectionParameters.Controls.Add(lblUnitCaption, 1, 12);
            tlpConfigCollectionParameters.Controls.Add(lblColumnNameValue, 2, 11);
            tlpConfigCollectionParameters.Controls.Add(lblColumnNameCaption, 1, 11);
            tlpConfigCollectionParameters.Controls.Add(lblColumnIdentValue, 2, 10);
            tlpConfigCollectionParameters.Controls.Add(lblColumnIdentCaption, 1, 10);
            tlpConfigCollectionParameters.Controls.Add(lblTableNameValue, 2, 9);
            tlpConfigCollectionParameters.Controls.Add(lblTableNameCaption, 1, 9);
            tlpConfigCollectionParameters.Controls.Add(lblSchemaNameValue, 2, 8);
            tlpConfigCollectionParameters.Controls.Add(lblSchemaNameCaption, 1, 8);
            tlpConfigCollectionParameters.Controls.Add(lblCatalogNameValue, 2, 7);
            tlpConfigCollectionParameters.Controls.Add(lblCatalogNameCaption, 1, 7);
            tlpConfigCollectionParameters.Controls.Add(lblAggregatedValue, 2, 6);
            tlpConfigCollectionParameters.Controls.Add(lblAggregatedCaption, 1, 6);
            tlpConfigCollectionParameters.Controls.Add(lblClosedValue, 2, 5);
            tlpConfigCollectionParameters.Controls.Add(lblClosedCaption, 1, 5);
            tlpConfigCollectionParameters.Controls.Add(lblDescriptionValue, 2, 4);
            tlpConfigCollectionParameters.Controls.Add(lblDescriptionCaption, 1, 4);
            tlpConfigCollectionParameters.Controls.Add(lblLabelValue, 2, 3);
            tlpConfigCollectionParameters.Controls.Add(lblLabelCaption, 1, 3);
            tlpConfigCollectionParameters.Controls.Add(lblConfigFunctionIdValue, 2, 2);
            tlpConfigCollectionParameters.Controls.Add(lblConfigFunctionIdCaption, 1, 2);
            tlpConfigCollectionParameters.Controls.Add(lblAuxiliaryVariableIdValue, 2, 1);
            tlpConfigCollectionParameters.Controls.Add(lblAuxiliaryVariableIdCaption, 1, 1);
            tlpConfigCollectionParameters.Controls.Add(lblIdValue, 2, 0);
            tlpConfigCollectionParameters.Controls.Add(lblIdCaption, 1, 0);
            tlpConfigCollectionParameters.Controls.Add(lblRowNumber00, 0, 0);
            tlpConfigCollectionParameters.Controls.Add(lblRowNumber01, 0, 1);
            tlpConfigCollectionParameters.Controls.Add(lblRowNumber02, 0, 2);
            tlpConfigCollectionParameters.Controls.Add(lblRowNumber03, 0, 3);
            tlpConfigCollectionParameters.Controls.Add(lblRowNumber04, 0, 4);
            tlpConfigCollectionParameters.Controls.Add(lblRowNumber05, 0, 5);
            tlpConfigCollectionParameters.Dock = System.Windows.Forms.DockStyle.Top;
            tlpConfigCollectionParameters.Location = new System.Drawing.Point(0, 0);
            tlpConfigCollectionParameters.Margin = new System.Windows.Forms.Padding(0);
            tlpConfigCollectionParameters.Name = "tlpConfigCollectionParameters";
            tlpConfigCollectionParameters.RowCount = 19;
            tlpConfigCollectionParameters.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpConfigCollectionParameters.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpConfigCollectionParameters.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpConfigCollectionParameters.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpConfigCollectionParameters.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpConfigCollectionParameters.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpConfigCollectionParameters.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpConfigCollectionParameters.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpConfigCollectionParameters.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpConfigCollectionParameters.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpConfigCollectionParameters.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpConfigCollectionParameters.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpConfigCollectionParameters.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpConfigCollectionParameters.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpConfigCollectionParameters.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpConfigCollectionParameters.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpConfigCollectionParameters.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpConfigCollectionParameters.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpConfigCollectionParameters.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpConfigCollectionParameters.Size = new System.Drawing.Size(950, 380);
            tlpConfigCollectionParameters.TabIndex = 1;
            // 
            // lblRowNumber18
            // 
            lblRowNumber18.Dock = System.Windows.Forms.DockStyle.Fill;
            lblRowNumber18.ForeColor = System.Drawing.Color.MediumBlue;
            lblRowNumber18.Location = new System.Drawing.Point(0, 360);
            lblRowNumber18.Margin = new System.Windows.Forms.Padding(0);
            lblRowNumber18.Name = "lblRowNumber18";
            lblRowNumber18.Size = new System.Drawing.Size(25, 20);
            lblRowNumber18.TabIndex = 56;
            lblRowNumber18.Text = "18.";
            lblRowNumber18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblRowNumber17
            // 
            lblRowNumber17.Dock = System.Windows.Forms.DockStyle.Fill;
            lblRowNumber17.ForeColor = System.Drawing.Color.MediumBlue;
            lblRowNumber17.Location = new System.Drawing.Point(0, 340);
            lblRowNumber17.Margin = new System.Windows.Forms.Padding(0);
            lblRowNumber17.Name = "lblRowNumber17";
            lblRowNumber17.Size = new System.Drawing.Size(25, 20);
            lblRowNumber17.TabIndex = 55;
            lblRowNumber17.Text = "17.";
            lblRowNumber17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblRowNumber16
            // 
            lblRowNumber16.Dock = System.Windows.Forms.DockStyle.Fill;
            lblRowNumber16.ForeColor = System.Drawing.Color.MediumBlue;
            lblRowNumber16.Location = new System.Drawing.Point(0, 320);
            lblRowNumber16.Margin = new System.Windows.Forms.Padding(0);
            lblRowNumber16.Name = "lblRowNumber16";
            lblRowNumber16.Size = new System.Drawing.Size(25, 20);
            lblRowNumber16.TabIndex = 54;
            lblRowNumber16.Text = "16.";
            lblRowNumber16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblRowNumber15
            // 
            lblRowNumber15.Dock = System.Windows.Forms.DockStyle.Fill;
            lblRowNumber15.ForeColor = System.Drawing.Color.MediumBlue;
            lblRowNumber15.Location = new System.Drawing.Point(0, 300);
            lblRowNumber15.Margin = new System.Windows.Forms.Padding(0);
            lblRowNumber15.Name = "lblRowNumber15";
            lblRowNumber15.Size = new System.Drawing.Size(25, 20);
            lblRowNumber15.TabIndex = 53;
            lblRowNumber15.Text = "15.";
            lblRowNumber15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblRowNumber14
            // 
            lblRowNumber14.Dock = System.Windows.Forms.DockStyle.Fill;
            lblRowNumber14.ForeColor = System.Drawing.Color.MediumBlue;
            lblRowNumber14.Location = new System.Drawing.Point(0, 280);
            lblRowNumber14.Margin = new System.Windows.Forms.Padding(0);
            lblRowNumber14.Name = "lblRowNumber14";
            lblRowNumber14.Size = new System.Drawing.Size(25, 20);
            lblRowNumber14.TabIndex = 52;
            lblRowNumber14.Text = "14.";
            lblRowNumber14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblRowNumber13
            // 
            lblRowNumber13.Dock = System.Windows.Forms.DockStyle.Fill;
            lblRowNumber13.ForeColor = System.Drawing.Color.MediumBlue;
            lblRowNumber13.Location = new System.Drawing.Point(0, 260);
            lblRowNumber13.Margin = new System.Windows.Forms.Padding(0);
            lblRowNumber13.Name = "lblRowNumber13";
            lblRowNumber13.Size = new System.Drawing.Size(25, 20);
            lblRowNumber13.TabIndex = 51;
            lblRowNumber13.Text = "13.";
            lblRowNumber13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblRowNumber12
            // 
            lblRowNumber12.Dock = System.Windows.Forms.DockStyle.Fill;
            lblRowNumber12.ForeColor = System.Drawing.Color.MediumBlue;
            lblRowNumber12.Location = new System.Drawing.Point(0, 240);
            lblRowNumber12.Margin = new System.Windows.Forms.Padding(0);
            lblRowNumber12.Name = "lblRowNumber12";
            lblRowNumber12.Size = new System.Drawing.Size(25, 20);
            lblRowNumber12.TabIndex = 50;
            lblRowNumber12.Text = "12.";
            lblRowNumber12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblRowNumber11
            // 
            lblRowNumber11.Dock = System.Windows.Forms.DockStyle.Fill;
            lblRowNumber11.ForeColor = System.Drawing.Color.MediumBlue;
            lblRowNumber11.Location = new System.Drawing.Point(0, 220);
            lblRowNumber11.Margin = new System.Windows.Forms.Padding(0);
            lblRowNumber11.Name = "lblRowNumber11";
            lblRowNumber11.Size = new System.Drawing.Size(25, 20);
            lblRowNumber11.TabIndex = 49;
            lblRowNumber11.Text = "11.";
            lblRowNumber11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblRowNumber10
            // 
            lblRowNumber10.Dock = System.Windows.Forms.DockStyle.Fill;
            lblRowNumber10.ForeColor = System.Drawing.Color.MediumBlue;
            lblRowNumber10.Location = new System.Drawing.Point(0, 200);
            lblRowNumber10.Margin = new System.Windows.Forms.Padding(0);
            lblRowNumber10.Name = "lblRowNumber10";
            lblRowNumber10.Size = new System.Drawing.Size(25, 20);
            lblRowNumber10.TabIndex = 48;
            lblRowNumber10.Text = "10.";
            lblRowNumber10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblRowNumber09
            // 
            lblRowNumber09.Dock = System.Windows.Forms.DockStyle.Fill;
            lblRowNumber09.ForeColor = System.Drawing.Color.MediumBlue;
            lblRowNumber09.Location = new System.Drawing.Point(0, 180);
            lblRowNumber09.Margin = new System.Windows.Forms.Padding(0);
            lblRowNumber09.Name = "lblRowNumber09";
            lblRowNumber09.Size = new System.Drawing.Size(25, 20);
            lblRowNumber09.TabIndex = 47;
            lblRowNumber09.Text = "9.";
            lblRowNumber09.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblRowNumber08
            // 
            lblRowNumber08.Dock = System.Windows.Forms.DockStyle.Fill;
            lblRowNumber08.ForeColor = System.Drawing.Color.MediumBlue;
            lblRowNumber08.Location = new System.Drawing.Point(0, 160);
            lblRowNumber08.Margin = new System.Windows.Forms.Padding(0);
            lblRowNumber08.Name = "lblRowNumber08";
            lblRowNumber08.Size = new System.Drawing.Size(25, 20);
            lblRowNumber08.TabIndex = 46;
            lblRowNumber08.Text = "8.";
            lblRowNumber08.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblRowNumber07
            // 
            lblRowNumber07.Dock = System.Windows.Forms.DockStyle.Fill;
            lblRowNumber07.ForeColor = System.Drawing.Color.MediumBlue;
            lblRowNumber07.Location = new System.Drawing.Point(0, 140);
            lblRowNumber07.Margin = new System.Windows.Forms.Padding(0);
            lblRowNumber07.Name = "lblRowNumber07";
            lblRowNumber07.Size = new System.Drawing.Size(25, 20);
            lblRowNumber07.TabIndex = 45;
            lblRowNumber07.Text = "7.";
            lblRowNumber07.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblRowNumber06
            // 
            lblRowNumber06.Dock = System.Windows.Forms.DockStyle.Fill;
            lblRowNumber06.ForeColor = System.Drawing.Color.MediumBlue;
            lblRowNumber06.Location = new System.Drawing.Point(0, 120);
            lblRowNumber06.Margin = new System.Windows.Forms.Padding(0);
            lblRowNumber06.Name = "lblRowNumber06";
            lblRowNumber06.Size = new System.Drawing.Size(25, 20);
            lblRowNumber06.TabIndex = 44;
            lblRowNumber06.Text = "6.";
            lblRowNumber06.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblEditDateValue
            // 
            lblEditDateValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblEditDateValue.Location = new System.Drawing.Point(325, 360);
            lblEditDateValue.Margin = new System.Windows.Forms.Padding(0);
            lblEditDateValue.Name = "lblEditDateValue";
            lblEditDateValue.Size = new System.Drawing.Size(625, 20);
            lblEditDateValue.TabIndex = 37;
            lblEditDateValue.Text = "lblEditDateValue";
            lblEditDateValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblEditDateCaption
            // 
            lblEditDateCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblEditDateCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblEditDateCaption.Location = new System.Drawing.Point(25, 360);
            lblEditDateCaption.Margin = new System.Windows.Forms.Padding(0);
            lblEditDateCaption.Name = "lblEditDateCaption";
            lblEditDateCaption.Size = new System.Drawing.Size(300, 20);
            lblEditDateCaption.TabIndex = 36;
            lblEditDateCaption.Text = "lblEditDateCaption";
            lblEditDateCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblRefIDTotalValue
            // 
            lblRefIDTotalValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblRefIDTotalValue.Location = new System.Drawing.Point(325, 340);
            lblRefIDTotalValue.Margin = new System.Windows.Forms.Padding(0);
            lblRefIDTotalValue.Name = "lblRefIDTotalValue";
            lblRefIDTotalValue.Size = new System.Drawing.Size(625, 20);
            lblRefIDTotalValue.TabIndex = 35;
            lblRefIDTotalValue.Text = "lblRefIDTotalValue";
            lblRefIDTotalValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblRefIDTotalCaption
            // 
            lblRefIDTotalCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblRefIDTotalCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblRefIDTotalCaption.Location = new System.Drawing.Point(25, 340);
            lblRefIDTotalCaption.Margin = new System.Windows.Forms.Padding(0);
            lblRefIDTotalCaption.Name = "lblRefIDTotalCaption";
            lblRefIDTotalCaption.Size = new System.Drawing.Size(300, 20);
            lblRefIDTotalCaption.TabIndex = 34;
            lblRefIDTotalCaption.Text = "lblRefIDTotalCaption";
            lblRefIDTotalCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblRefIDLayerPointsValue
            // 
            lblRefIDLayerPointsValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblRefIDLayerPointsValue.Location = new System.Drawing.Point(325, 320);
            lblRefIDLayerPointsValue.Margin = new System.Windows.Forms.Padding(0);
            lblRefIDLayerPointsValue.Name = "lblRefIDLayerPointsValue";
            lblRefIDLayerPointsValue.Size = new System.Drawing.Size(625, 20);
            lblRefIDLayerPointsValue.TabIndex = 33;
            lblRefIDLayerPointsValue.Text = "lblRefIDLayerPointsValue";
            lblRefIDLayerPointsValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblRefIDLayerPointsCaption
            // 
            lblRefIDLayerPointsCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblRefIDLayerPointsCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblRefIDLayerPointsCaption.Location = new System.Drawing.Point(25, 320);
            lblRefIDLayerPointsCaption.Margin = new System.Windows.Forms.Padding(0);
            lblRefIDLayerPointsCaption.Name = "lblRefIDLayerPointsCaption";
            lblRefIDLayerPointsCaption.Size = new System.Drawing.Size(300, 20);
            lblRefIDLayerPointsCaption.TabIndex = 32;
            lblRefIDLayerPointsCaption.Text = "lblRefIDLayerPointsCaption";
            lblRefIDLayerPointsCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTagValue
            // 
            lblTagValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblTagValue.Location = new System.Drawing.Point(325, 300);
            lblTagValue.Margin = new System.Windows.Forms.Padding(0);
            lblTagValue.Name = "lblTagValue";
            lblTagValue.Size = new System.Drawing.Size(625, 20);
            lblTagValue.TabIndex = 31;
            lblTagValue.Text = "lblTagValue";
            lblTagValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTagCaption
            // 
            lblTagCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblTagCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblTagCaption.Location = new System.Drawing.Point(25, 300);
            lblTagCaption.Margin = new System.Windows.Forms.Padding(0);
            lblTagCaption.Name = "lblTagCaption";
            lblTagCaption.Size = new System.Drawing.Size(300, 20);
            lblTagCaption.TabIndex = 30;
            lblTagCaption.Text = "lblTagCaption";
            lblTagCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTotalPointsValue
            // 
            lblTotalPointsValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblTotalPointsValue.Location = new System.Drawing.Point(325, 280);
            lblTotalPointsValue.Margin = new System.Windows.Forms.Padding(0);
            lblTotalPointsValue.Name = "lblTotalPointsValue";
            lblTotalPointsValue.Size = new System.Drawing.Size(625, 20);
            lblTotalPointsValue.TabIndex = 29;
            lblTotalPointsValue.Text = "lblTotalPointsValue";
            lblTotalPointsValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTotalPointsCaption
            // 
            lblTotalPointsCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblTotalPointsCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblTotalPointsCaption.Location = new System.Drawing.Point(25, 280);
            lblTotalPointsCaption.Margin = new System.Windows.Forms.Padding(0);
            lblTotalPointsCaption.Name = "lblTotalPointsCaption";
            lblTotalPointsCaption.Size = new System.Drawing.Size(300, 20);
            lblTotalPointsCaption.TabIndex = 28;
            lblTotalPointsCaption.Text = "lblTotalPointsCaption";
            lblTotalPointsCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblConditionValue
            // 
            lblConditionValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblConditionValue.Location = new System.Drawing.Point(325, 260);
            lblConditionValue.Margin = new System.Windows.Forms.Padding(0);
            lblConditionValue.Name = "lblConditionValue";
            lblConditionValue.Size = new System.Drawing.Size(625, 20);
            lblConditionValue.TabIndex = 27;
            lblConditionValue.Text = "lblConditionValue";
            lblConditionValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblConditionCaption
            // 
            lblConditionCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblConditionCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblConditionCaption.Location = new System.Drawing.Point(25, 260);
            lblConditionCaption.Margin = new System.Windows.Forms.Padding(0);
            lblConditionCaption.Name = "lblConditionCaption";
            lblConditionCaption.Size = new System.Drawing.Size(300, 20);
            lblConditionCaption.TabIndex = 26;
            lblConditionCaption.Text = "lblConditionCaption";
            lblConditionCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblUnitValue
            // 
            lblUnitValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblUnitValue.Location = new System.Drawing.Point(325, 240);
            lblUnitValue.Margin = new System.Windows.Forms.Padding(0);
            lblUnitValue.Name = "lblUnitValue";
            lblUnitValue.Size = new System.Drawing.Size(625, 20);
            lblUnitValue.TabIndex = 25;
            lblUnitValue.Text = "lblUnitValue";
            lblUnitValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblUnitCaption
            // 
            lblUnitCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblUnitCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblUnitCaption.Location = new System.Drawing.Point(25, 240);
            lblUnitCaption.Margin = new System.Windows.Forms.Padding(0);
            lblUnitCaption.Name = "lblUnitCaption";
            lblUnitCaption.Size = new System.Drawing.Size(300, 20);
            lblUnitCaption.TabIndex = 24;
            lblUnitCaption.Text = "lblUnitCaption";
            lblUnitCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblColumnNameValue
            // 
            lblColumnNameValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblColumnNameValue.Location = new System.Drawing.Point(325, 220);
            lblColumnNameValue.Margin = new System.Windows.Forms.Padding(0);
            lblColumnNameValue.Name = "lblColumnNameValue";
            lblColumnNameValue.Size = new System.Drawing.Size(625, 20);
            lblColumnNameValue.TabIndex = 23;
            lblColumnNameValue.Text = "lblColumnNameValue";
            lblColumnNameValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblColumnNameCaption
            // 
            lblColumnNameCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblColumnNameCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblColumnNameCaption.Location = new System.Drawing.Point(25, 220);
            lblColumnNameCaption.Margin = new System.Windows.Forms.Padding(0);
            lblColumnNameCaption.Name = "lblColumnNameCaption";
            lblColumnNameCaption.Size = new System.Drawing.Size(300, 20);
            lblColumnNameCaption.TabIndex = 22;
            lblColumnNameCaption.Text = "lblColumnNameCaption";
            lblColumnNameCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblColumnIdentValue
            // 
            lblColumnIdentValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblColumnIdentValue.Location = new System.Drawing.Point(325, 200);
            lblColumnIdentValue.Margin = new System.Windows.Forms.Padding(0);
            lblColumnIdentValue.Name = "lblColumnIdentValue";
            lblColumnIdentValue.Size = new System.Drawing.Size(625, 20);
            lblColumnIdentValue.TabIndex = 21;
            lblColumnIdentValue.Text = "lblColumnIdentValue";
            lblColumnIdentValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblColumnIdentCaption
            // 
            lblColumnIdentCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblColumnIdentCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblColumnIdentCaption.Location = new System.Drawing.Point(25, 200);
            lblColumnIdentCaption.Margin = new System.Windows.Forms.Padding(0);
            lblColumnIdentCaption.Name = "lblColumnIdentCaption";
            lblColumnIdentCaption.Size = new System.Drawing.Size(300, 20);
            lblColumnIdentCaption.TabIndex = 20;
            lblColumnIdentCaption.Text = "lblColumnIdentCaption";
            lblColumnIdentCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTableNameValue
            // 
            lblTableNameValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblTableNameValue.Location = new System.Drawing.Point(325, 180);
            lblTableNameValue.Margin = new System.Windows.Forms.Padding(0);
            lblTableNameValue.Name = "lblTableNameValue";
            lblTableNameValue.Size = new System.Drawing.Size(625, 20);
            lblTableNameValue.TabIndex = 19;
            lblTableNameValue.Text = "lblTableNameValue";
            lblTableNameValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTableNameCaption
            // 
            lblTableNameCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblTableNameCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblTableNameCaption.Location = new System.Drawing.Point(25, 180);
            lblTableNameCaption.Margin = new System.Windows.Forms.Padding(0);
            lblTableNameCaption.Name = "lblTableNameCaption";
            lblTableNameCaption.Size = new System.Drawing.Size(300, 20);
            lblTableNameCaption.TabIndex = 18;
            lblTableNameCaption.Text = "lblTableNameCaption";
            lblTableNameCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSchemaNameValue
            // 
            lblSchemaNameValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblSchemaNameValue.Location = new System.Drawing.Point(325, 160);
            lblSchemaNameValue.Margin = new System.Windows.Forms.Padding(0);
            lblSchemaNameValue.Name = "lblSchemaNameValue";
            lblSchemaNameValue.Size = new System.Drawing.Size(625, 20);
            lblSchemaNameValue.TabIndex = 17;
            lblSchemaNameValue.Text = "lblSchemaNameValue";
            lblSchemaNameValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSchemaNameCaption
            // 
            lblSchemaNameCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblSchemaNameCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblSchemaNameCaption.Location = new System.Drawing.Point(25, 160);
            lblSchemaNameCaption.Margin = new System.Windows.Forms.Padding(0);
            lblSchemaNameCaption.Name = "lblSchemaNameCaption";
            lblSchemaNameCaption.Size = new System.Drawing.Size(300, 20);
            lblSchemaNameCaption.TabIndex = 16;
            lblSchemaNameCaption.Text = "lblSchemaNameCaption";
            lblSchemaNameCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCatalogNameValue
            // 
            lblCatalogNameValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblCatalogNameValue.Location = new System.Drawing.Point(325, 140);
            lblCatalogNameValue.Margin = new System.Windows.Forms.Padding(0);
            lblCatalogNameValue.Name = "lblCatalogNameValue";
            lblCatalogNameValue.Size = new System.Drawing.Size(625, 20);
            lblCatalogNameValue.TabIndex = 15;
            lblCatalogNameValue.Text = "lblCatalogNameValue";
            lblCatalogNameValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCatalogNameCaption
            // 
            lblCatalogNameCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblCatalogNameCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblCatalogNameCaption.Location = new System.Drawing.Point(25, 140);
            lblCatalogNameCaption.Margin = new System.Windows.Forms.Padding(0);
            lblCatalogNameCaption.Name = "lblCatalogNameCaption";
            lblCatalogNameCaption.Size = new System.Drawing.Size(300, 20);
            lblCatalogNameCaption.TabIndex = 14;
            lblCatalogNameCaption.Text = "lblCatalogNameCaption";
            lblCatalogNameCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblAggregatedValue
            // 
            lblAggregatedValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblAggregatedValue.Location = new System.Drawing.Point(325, 120);
            lblAggregatedValue.Margin = new System.Windows.Forms.Padding(0);
            lblAggregatedValue.Name = "lblAggregatedValue";
            lblAggregatedValue.Size = new System.Drawing.Size(625, 20);
            lblAggregatedValue.TabIndex = 13;
            lblAggregatedValue.Text = "lblAggregatedValue";
            lblAggregatedValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblAggregatedCaption
            // 
            lblAggregatedCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblAggregatedCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblAggregatedCaption.Location = new System.Drawing.Point(25, 120);
            lblAggregatedCaption.Margin = new System.Windows.Forms.Padding(0);
            lblAggregatedCaption.Name = "lblAggregatedCaption";
            lblAggregatedCaption.Size = new System.Drawing.Size(300, 20);
            lblAggregatedCaption.TabIndex = 12;
            lblAggregatedCaption.Text = "lblAggregatedCaption";
            lblAggregatedCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblClosedValue
            // 
            lblClosedValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblClosedValue.Location = new System.Drawing.Point(325, 100);
            lblClosedValue.Margin = new System.Windows.Forms.Padding(0);
            lblClosedValue.Name = "lblClosedValue";
            lblClosedValue.Size = new System.Drawing.Size(625, 20);
            lblClosedValue.TabIndex = 11;
            lblClosedValue.Text = "lblClosedValue";
            lblClosedValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblClosedCaption
            // 
            lblClosedCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblClosedCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblClosedCaption.Location = new System.Drawing.Point(25, 100);
            lblClosedCaption.Margin = new System.Windows.Forms.Padding(0);
            lblClosedCaption.Name = "lblClosedCaption";
            lblClosedCaption.Size = new System.Drawing.Size(300, 20);
            lblClosedCaption.TabIndex = 10;
            lblClosedCaption.Text = "lblClosedCaption";
            lblClosedCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDescriptionValue
            // 
            lblDescriptionValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblDescriptionValue.Location = new System.Drawing.Point(325, 80);
            lblDescriptionValue.Margin = new System.Windows.Forms.Padding(0);
            lblDescriptionValue.Name = "lblDescriptionValue";
            lblDescriptionValue.Size = new System.Drawing.Size(625, 20);
            lblDescriptionValue.TabIndex = 9;
            lblDescriptionValue.Text = "lblDescriptionValue";
            lblDescriptionValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDescriptionCaption
            // 
            lblDescriptionCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblDescriptionCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblDescriptionCaption.Location = new System.Drawing.Point(25, 80);
            lblDescriptionCaption.Margin = new System.Windows.Forms.Padding(0);
            lblDescriptionCaption.Name = "lblDescriptionCaption";
            lblDescriptionCaption.Size = new System.Drawing.Size(300, 20);
            lblDescriptionCaption.TabIndex = 8;
            lblDescriptionCaption.Text = "lblDescriptionCaption";
            lblDescriptionCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblLabelValue
            // 
            lblLabelValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblLabelValue.Location = new System.Drawing.Point(325, 60);
            lblLabelValue.Margin = new System.Windows.Forms.Padding(0);
            lblLabelValue.Name = "lblLabelValue";
            lblLabelValue.Size = new System.Drawing.Size(625, 20);
            lblLabelValue.TabIndex = 7;
            lblLabelValue.Text = "lblLabelValue";
            lblLabelValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblLabelCaption
            // 
            lblLabelCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblLabelCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblLabelCaption.Location = new System.Drawing.Point(25, 60);
            lblLabelCaption.Margin = new System.Windows.Forms.Padding(0);
            lblLabelCaption.Name = "lblLabelCaption";
            lblLabelCaption.Size = new System.Drawing.Size(300, 20);
            lblLabelCaption.TabIndex = 6;
            lblLabelCaption.Text = "lblLabelCaption";
            lblLabelCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblConfigFunctionIdValue
            // 
            lblConfigFunctionIdValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblConfigFunctionIdValue.Location = new System.Drawing.Point(325, 40);
            lblConfigFunctionIdValue.Margin = new System.Windows.Forms.Padding(0);
            lblConfigFunctionIdValue.Name = "lblConfigFunctionIdValue";
            lblConfigFunctionIdValue.Size = new System.Drawing.Size(625, 20);
            lblConfigFunctionIdValue.TabIndex = 5;
            lblConfigFunctionIdValue.Text = "lblConfigFunctionIdValue";
            lblConfigFunctionIdValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblConfigFunctionIdCaption
            // 
            lblConfigFunctionIdCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblConfigFunctionIdCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblConfigFunctionIdCaption.Location = new System.Drawing.Point(25, 40);
            lblConfigFunctionIdCaption.Margin = new System.Windows.Forms.Padding(0);
            lblConfigFunctionIdCaption.Name = "lblConfigFunctionIdCaption";
            lblConfigFunctionIdCaption.Size = new System.Drawing.Size(300, 20);
            lblConfigFunctionIdCaption.TabIndex = 4;
            lblConfigFunctionIdCaption.Text = "lblConfigFunctionIdCaption";
            lblConfigFunctionIdCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblAuxiliaryVariableIdValue
            // 
            lblAuxiliaryVariableIdValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblAuxiliaryVariableIdValue.Location = new System.Drawing.Point(325, 20);
            lblAuxiliaryVariableIdValue.Margin = new System.Windows.Forms.Padding(0);
            lblAuxiliaryVariableIdValue.Name = "lblAuxiliaryVariableIdValue";
            lblAuxiliaryVariableIdValue.Size = new System.Drawing.Size(625, 20);
            lblAuxiliaryVariableIdValue.TabIndex = 3;
            lblAuxiliaryVariableIdValue.Text = "lblAuxiliaryVariableIdValue";
            lblAuxiliaryVariableIdValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblAuxiliaryVariableIdCaption
            // 
            lblAuxiliaryVariableIdCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblAuxiliaryVariableIdCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblAuxiliaryVariableIdCaption.Location = new System.Drawing.Point(25, 20);
            lblAuxiliaryVariableIdCaption.Margin = new System.Windows.Forms.Padding(0);
            lblAuxiliaryVariableIdCaption.Name = "lblAuxiliaryVariableIdCaption";
            lblAuxiliaryVariableIdCaption.Size = new System.Drawing.Size(300, 20);
            lblAuxiliaryVariableIdCaption.TabIndex = 2;
            lblAuxiliaryVariableIdCaption.Text = "lblAuxiliaryVariableIdCaption";
            lblAuxiliaryVariableIdCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblIdValue
            // 
            lblIdValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblIdValue.Location = new System.Drawing.Point(325, 0);
            lblIdValue.Margin = new System.Windows.Forms.Padding(0);
            lblIdValue.Name = "lblIdValue";
            lblIdValue.Size = new System.Drawing.Size(625, 20);
            lblIdValue.TabIndex = 1;
            lblIdValue.Text = "lblIdValue";
            lblIdValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblIdCaption
            // 
            lblIdCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblIdCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblIdCaption.Location = new System.Drawing.Point(25, 0);
            lblIdCaption.Margin = new System.Windows.Forms.Padding(0);
            lblIdCaption.Name = "lblIdCaption";
            lblIdCaption.Size = new System.Drawing.Size(300, 20);
            lblIdCaption.TabIndex = 0;
            lblIdCaption.Text = "lblIdCaption";
            lblIdCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblRowNumber00
            // 
            lblRowNumber00.Dock = System.Windows.Forms.DockStyle.Fill;
            lblRowNumber00.ForeColor = System.Drawing.Color.MediumBlue;
            lblRowNumber00.Location = new System.Drawing.Point(0, 0);
            lblRowNumber00.Margin = new System.Windows.Forms.Padding(0);
            lblRowNumber00.Name = "lblRowNumber00";
            lblRowNumber00.Size = new System.Drawing.Size(25, 20);
            lblRowNumber00.TabIndex = 38;
            lblRowNumber00.Tag = "";
            lblRowNumber00.Text = "0.";
            lblRowNumber00.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblRowNumber01
            // 
            lblRowNumber01.Dock = System.Windows.Forms.DockStyle.Fill;
            lblRowNumber01.ForeColor = System.Drawing.Color.MediumBlue;
            lblRowNumber01.Location = new System.Drawing.Point(0, 20);
            lblRowNumber01.Margin = new System.Windows.Forms.Padding(0);
            lblRowNumber01.Name = "lblRowNumber01";
            lblRowNumber01.Size = new System.Drawing.Size(25, 20);
            lblRowNumber01.TabIndex = 39;
            lblRowNumber01.Text = "1.";
            lblRowNumber01.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblRowNumber02
            // 
            lblRowNumber02.Dock = System.Windows.Forms.DockStyle.Fill;
            lblRowNumber02.ForeColor = System.Drawing.Color.MediumBlue;
            lblRowNumber02.Location = new System.Drawing.Point(0, 40);
            lblRowNumber02.Margin = new System.Windows.Forms.Padding(0);
            lblRowNumber02.Name = "lblRowNumber02";
            lblRowNumber02.Size = new System.Drawing.Size(25, 20);
            lblRowNumber02.TabIndex = 40;
            lblRowNumber02.Text = "2.";
            lblRowNumber02.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblRowNumber03
            // 
            lblRowNumber03.Dock = System.Windows.Forms.DockStyle.Fill;
            lblRowNumber03.ForeColor = System.Drawing.Color.MediumBlue;
            lblRowNumber03.Location = new System.Drawing.Point(0, 60);
            lblRowNumber03.Margin = new System.Windows.Forms.Padding(0);
            lblRowNumber03.Name = "lblRowNumber03";
            lblRowNumber03.Size = new System.Drawing.Size(25, 20);
            lblRowNumber03.TabIndex = 41;
            lblRowNumber03.Text = "3.";
            lblRowNumber03.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblRowNumber04
            // 
            lblRowNumber04.Dock = System.Windows.Forms.DockStyle.Fill;
            lblRowNumber04.ForeColor = System.Drawing.Color.MediumBlue;
            lblRowNumber04.Location = new System.Drawing.Point(0, 80);
            lblRowNumber04.Margin = new System.Windows.Forms.Padding(0);
            lblRowNumber04.Name = "lblRowNumber04";
            lblRowNumber04.Size = new System.Drawing.Size(25, 20);
            lblRowNumber04.TabIndex = 42;
            lblRowNumber04.Text = "4.";
            lblRowNumber04.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblRowNumber05
            // 
            lblRowNumber05.Dock = System.Windows.Forms.DockStyle.Fill;
            lblRowNumber05.ForeColor = System.Drawing.Color.MediumBlue;
            lblRowNumber05.Location = new System.Drawing.Point(0, 100);
            lblRowNumber05.Margin = new System.Windows.Forms.Padding(0);
            lblRowNumber05.Name = "lblRowNumber05";
            lblRowNumber05.Size = new System.Drawing.Size(25, 20);
            lblRowNumber05.TabIndex = 43;
            lblRowNumber05.Text = "5.";
            lblRowNumber05.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ControlConfigCollectionView
            // 
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            BackColor = System.Drawing.Color.Transparent;
            BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            Controls.Add(tlpMain);
            ForeColor = System.Drawing.Color.Black;
            Margin = new System.Windows.Forms.Padding(0);
            Name = "ControlConfigCollectionView";
            Size = new System.Drawing.Size(960, 540);
            tlpMain.ResumeLayout(false);
            grpCaption.ResumeLayout(false);
            grpConfigCollectionParameters.ResumeLayout(false);
            pnlConfigCollectionParameters.ResumeLayout(false);
            tlpConfigCollectionParameters.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion
        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.GroupBox grpCaption;
        private System.Windows.Forms.Label lblCaption;
        private System.Windows.Forms.GroupBox grpConfigCollectionParameters;
        private System.Windows.Forms.Panel pnlConfigCollectionParameters;
        private System.Windows.Forms.TableLayoutPanel tlpConfigCollectionParameters;
        private System.Windows.Forms.Label lblRowNumber18;
        private System.Windows.Forms.Label lblRowNumber17;
        private System.Windows.Forms.Label lblRowNumber16;
        private System.Windows.Forms.Label lblRowNumber15;
        private System.Windows.Forms.Label lblRowNumber14;
        private System.Windows.Forms.Label lblRowNumber13;
        private System.Windows.Forms.Label lblRowNumber12;
        private System.Windows.Forms.Label lblRowNumber11;
        private System.Windows.Forms.Label lblRowNumber10;
        private System.Windows.Forms.Label lblRowNumber09;
        private System.Windows.Forms.Label lblRowNumber08;
        private System.Windows.Forms.Label lblRowNumber07;
        private System.Windows.Forms.Label lblRowNumber06;
        private System.Windows.Forms.Label lblEditDateValue;
        private System.Windows.Forms.Label lblEditDateCaption;
        private System.Windows.Forms.Label lblRefIDTotalValue;
        private System.Windows.Forms.Label lblRefIDTotalCaption;
        private System.Windows.Forms.Label lblRefIDLayerPointsValue;
        private System.Windows.Forms.Label lblRefIDLayerPointsCaption;
        private System.Windows.Forms.Label lblTagValue;
        private System.Windows.Forms.Label lblTagCaption;
        private System.Windows.Forms.Label lblTotalPointsValue;
        private System.Windows.Forms.Label lblTotalPointsCaption;
        private System.Windows.Forms.Label lblConditionValue;
        private System.Windows.Forms.Label lblConditionCaption;
        private System.Windows.Forms.Label lblUnitValue;
        private System.Windows.Forms.Label lblUnitCaption;
        private System.Windows.Forms.Label lblColumnNameValue;
        private System.Windows.Forms.Label lblColumnNameCaption;
        private System.Windows.Forms.Label lblColumnIdentValue;
        private System.Windows.Forms.Label lblColumnIdentCaption;
        private System.Windows.Forms.Label lblTableNameValue;
        private System.Windows.Forms.Label lblTableNameCaption;
        private System.Windows.Forms.Label lblSchemaNameValue;
        private System.Windows.Forms.Label lblSchemaNameCaption;
        private System.Windows.Forms.Label lblCatalogNameValue;
        private System.Windows.Forms.Label lblCatalogNameCaption;
        private System.Windows.Forms.Label lblAggregatedValue;
        private System.Windows.Forms.Label lblAggregatedCaption;
        private System.Windows.Forms.Label lblClosedValue;
        private System.Windows.Forms.Label lblClosedCaption;
        private System.Windows.Forms.Label lblDescriptionValue;
        private System.Windows.Forms.Label lblDescriptionCaption;
        private System.Windows.Forms.Label lblLabelValue;
        private System.Windows.Forms.Label lblLabelCaption;
        private System.Windows.Forms.Label lblConfigFunctionIdValue;
        private System.Windows.Forms.Label lblConfigFunctionIdCaption;
        private System.Windows.Forms.Label lblAuxiliaryVariableIdValue;
        private System.Windows.Forms.Label lblAuxiliaryVariableIdCaption;
        private System.Windows.Forms.Label lblIdValue;
        private System.Windows.Forms.Label lblIdCaption;
        private System.Windows.Forms.Label lblRowNumber00;
        private System.Windows.Forms.Label lblRowNumber01;
        private System.Windows.Forms.Label lblRowNumber02;
        private System.Windows.Forms.Label lblRowNumber03;
        private System.Windows.Forms.Label lblRowNumber04;
        private System.Windows.Forms.Label lblRowNumber05;
    }

}
