﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.AuxiliaryData;
using ZaJi.PostgreSQL;

namespace ZaJi.ModuleAuxiliaryData
{

    /// <summary>
    /// <para lang="cs">Průvodce vytvořením nové nebo upravou existující konfigurace</para>
    /// <para lang="en">Guide to creating a new or editing an existing configuration</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    internal partial class FormConfigurationGuide
        : Form, INfiEstaControl, IAuxiliaryDataControl
    {

        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Konfigurace</para>
        /// <para lang="en">Configuration</para>
        /// </summary>
        private Config configuration;

        /// <summary>
        /// <para lang="cs">Seznam vybraných databázových sloupců pro sestavení výběrové podmínky</para>
        /// <para lang="en">List of selected database columns for construction of selection condition</para>
        /// </summary>
        public List<WideColumn> conditionColumns;

        /// <summary>
        /// <para lang="cs">
        /// Provede se zápis nové (newEntry = true)
        /// nebo uprava existující konfigurace (newEntry = false) do databáze
        /// </para>
        /// A new entry is inserted (newEntry = true)
        /// or updated an existing configuration (newEntry = false) into the database
        /// </summary>
        private bool newEntry;

        #endregion Private Fields


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor ovládacího prvku</para>
        /// <para lang="en">Control constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        public FormConfigurationGuide(Control controlOwner)
        {
            InitializeComponent();
            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                IAuxiliaryDataControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                return controlOwner;
            }
            set
            {
                IAuxiliaryDataControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables (read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((IAuxiliaryDataControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro pomocná data (read-only)</para>
        /// <para lang="en">Module for auxiliary data setting (read-only)</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((IAuxiliaryDataControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">Konfigurace</para>
        /// <para lang="en">Configuration</para>
        /// </summary>
        public Config Configuration
        {
            get
            {
                return configuration;
            }
            set
            {
                configuration = value;
                InitializeLabels();
            }
        }

        /// <summary>
        /// <para lang="cs">Seznam vybraných databázových sloupců pro sestavení výběrové podmínky</para>
        /// <para lang="en">List of selected database columns for construction of selection condition</para>
        /// </summary>
        public List<WideColumn> ConditionColumns
        {
            get
            {
                return conditionColumns ?? [];
            }
            set
            {
                conditionColumns = value ?? [];
            }
        }

        /// <summary>
        /// <para lang="cs">
        /// Provede se zápis nové (newEntry = true)
        /// nebo uprava existující konfigurace (newEntry = false) do databáze
        /// </para>
        /// A new entry is inserted (newEntry = true)
        /// or updated an existing configuration (newEntry = false) into the database
        /// </summary>
        public bool NewEntry
        {
            get
            {
                return newEntry;
            }
            set
            {
                newEntry = value;
                InitializeLabels();
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { $"{nameof(FormConfigurationGuide)}Insert",    "Nová konfigurace" },
                        { $"{nameof(FormConfigurationGuide)}Update",    "Editace konfigurace"  }
                    }
                    : languageFile.NationalVersion.Data.TryGetValue(
                        key: nameof(FormConfigurationGuide),
                        out Dictionary<string, string> dictNational)
                            ? dictNational
                            : [],

                LanguageVersion.International => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { $"{nameof(FormConfigurationGuide)}Insert",    "New configuration" },
                        { $"{nameof(FormConfigurationGuide)}Update",    "Update configuration"  }
                    }
                    : languageFile.InternationalVersion.Data.TryGetValue(
                        key: nameof(FormConfigurationGuide),
                        out Dictionary<string, string> dictInternational)
                            ? dictInternational
                            : [],

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku</para>
        /// <para lang="en">Initialization of the control</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        private void Initialize(Control controlOwner)
        {
            ControlOwner = controlOwner;
            Configuration = null;
            ConditionColumns = [];
            NewEntry = true;
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků ovládacího prvku</para>
        /// <para lang="en">Initialization of control labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
               languageVersion: LanguageVersion,
               languageFile: LanguageFile);

            Text = (newEntry)
                ? labels.TryGetValue(key: $"{nameof(FormConfigurationGuide)}Insert",
                    out string frmConfigurationGuideInsertText)
                        ? frmConfigurationGuideInsertText
                        : String.Empty
                : labels.TryGetValue(key: $"{nameof(FormConfigurationGuide)}Update",
                    out string frmConfigurationGuideUpdateText)
                        ? frmConfigurationGuideUpdateText
                        : String.Empty;

            foreach (INfiEstaControl control in Controls.OfType<INfiEstaControl>())
            {
                control?.InitializeLabels();
            }
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent()
        {
            DisplayConfigIntro();
        }


        /// <summary>
        /// <para lang="cs">Zobrazí ovládací prvek "Nová konfigurace"</para>
        /// <para lang="en">Shows control "New configuration"</para>
        /// </summary>
        private void DisplayConfigIntro()
        {
            Controls.Clear();
            if (Configuration == null)
            {
                return;
            }

            ControlConfigurationIntro ctrConfigurationIntro =
                new(controlOwner: this)
                {
                    Dock = DockStyle.Fill
                };
            ctrConfigurationIntro.LoadContent();

            ctrConfigurationIntro.NextClick +=
                new EventHandler((sender, e) =>
                {
                    switch (Configuration.ConfigQueryValue)
                    {
                        case ConfigQueryEnum.GisLayer:
                            switch (Configuration.ConfigCollection.ConfigFunctionValue)
                            {
                                case ConfigFunctionEnum.VectorTotal:
                                    DisplayVectorColumns();
                                    return;

                                case ConfigFunctionEnum.RasterTotal:
                                    DisplayRasterBands();
                                    return;

                                case ConfigFunctionEnum.RasterTotalWithinVector:
                                    DisplayVRInteraction();
                                    return;

                                case ConfigFunctionEnum.RastersProductTotal:
                                    DisplayRRInteraction();
                                    return;

                                case ConfigFunctionEnum.PointLayer:
                                    // Nemá konfigurace
                                    return;

                                case ConfigFunctionEnum.PointTotalCombination:
                                    // Nemá konfigurace
                                    return;

                                default:
                                    return;
                            }

                        case ConfigQueryEnum.SumOfVariables:
                            DisplayCategorySum();
                            return;

                        case ConfigQueryEnum.AdditionToCellArea:
                            DisplayEstimationCellComplement();
                            return;

                        case ConfigQueryEnum.AdditionToTotal:
                            DisplayCategoryComplete();
                            return;

                        case ConfigQueryEnum.LinkToVariable:
                            DisplayCategoryLink();
                            return;

                        default:
                            return;
                    }
                });

            ctrConfigurationIntro.CreateControl();
            Controls.Add(value: ctrConfigurationIntro);
        }


        /// <summary>
        /// <para lang="cs">Zobrazí ovládací prvek "Volba sloupců pro výběr z vektorové vrstvy"</para>
        /// <para lang="en">Shows control "Selecting columns for selection from vector layer"</para>
        /// </summary>
        private void DisplayVectorColumns()
        {
            Controls.Clear();
            if (Configuration == null)
            {
                return;
            }

            ControlVectorColumns ctrVectorColumns =
                new(controlOwner: this)
                {
                    Dock = DockStyle.Fill
                };
            ctrVectorColumns.LoadContent();

            ctrVectorColumns.PreviousClick +=
                new EventHandler((sender, e) =>
                {
                    DisplayConfigIntro();
                });

            ctrVectorColumns.NextClick +=
                new EventHandler((sender, e) =>
                {
                    ConditionColumns =
                        [.. ctrVectorColumns.SelectedColumns];

                    if (ConditionColumns.Count > 0)
                    {
                        DisplayVectorCondition();
                    }
                    else
                    {
                        DisplayConfigSummary();
                    }
                });

            ctrVectorColumns.CreateControl();
            Controls.Add(value: ctrVectorColumns);
        }

        /// <summary>
        /// <para lang="cs">Zobrazí ovládací prvek "Sestavení výběrové podmínky pro vektorovou vrstvu"</para>
        /// <para lang="en">Shows control "Constructing a selection condition for a vector layer"</para>
        /// </summary>
        private void DisplayVectorCondition()
        {
            Controls.Clear();
            if (Configuration == null)
            {
                return;
            }

            ControlVectorCondition ctrVectorCondition =
                new(controlOwner: this)
                {
                    Dock = DockStyle.Fill
                };
            ctrVectorCondition.LoadContent();

            ctrVectorCondition.PreviousClick +=
                new EventHandler((sender, e) =>
                {
                    DisplayVectorColumns();
                });

            ctrVectorCondition.NextClick +=
                new EventHandler((sender, e) =>
                {
                    DisplayConfigSummary();
                });

            ctrVectorCondition.CreateControl();
            Controls.Add(value: ctrVectorCondition);
        }


        /// <summary>
        /// <para lang="cs">Zobrazí ovládací prvek "Výběr pásma rastru"</para>
        /// <para lang="en">Shows control "Selecting raster band"</para>
        /// </summary>
        private void DisplayRasterBands()
        {
            Controls.Clear();
            if (Configuration == null)
            {
                return;
            }

            ControlRasterBand ctrRasterBand =
                new(controlOwner: this)
                {
                    Dock = DockStyle.Fill
                };
            ctrRasterBand.LoadContent();

            ctrRasterBand.PreviousClick +=
                new EventHandler((sender, e) =>
                {
                    DisplayConfigIntro();
                });

            ctrRasterBand.NextClick +=
                new EventHandler((sender, e) =>
                {
                    DisplayRasterReclass();
                });

            ctrRasterBand.CreateControl();
            Controls.Add(value: ctrRasterBand);
        }

        /// <summary>
        /// <para lang="cs">Zobrazí ovládací prvek "Reklasifikace hodnot pixelu rastru"</para>
        /// <para lang="en">Shows control "Reclassification of raster pixel values"</para>
        /// </summary>
        private void DisplayRasterReclass()
        {
            Controls.Clear();
            if (Configuration == null)
            {
                return;
            }

            ControlRasterReclass ctrRasterReclass =
                new(controlOwner: this)
                {
                    Dock = DockStyle.Fill
                };
            ctrRasterReclass.LoadContent();

            ctrRasterReclass.PreviousClick +=
                new EventHandler((sender, e) =>
                {
                    DisplayRasterBands();
                });

            ctrRasterReclass.NextClick +=
                new EventHandler((sender, e) =>
                {
                    DisplayRasterColumns();
                });

            ctrRasterReclass.CreateControl();
            Controls.Add(value: ctrRasterReclass);
        }

        /// <summary>
        /// <para lang="cs">Zobrazí ovládací prvek "Volba sloupců pro výběr z rastrové vrstvy"</para>
        /// <para lang="en">Shows control "Selecting columns for selection from raster layer"</para>
        /// </summary>
        private void DisplayRasterColumns()
        {
            Controls.Clear();
            if (Configuration == null)
            {
                return;
            }

            ControlRasterColumns ctrRasterColumns =
                new(controlOwner: this)
                {
                    Dock = DockStyle.Fill
                };
            ctrRasterColumns.LoadContent();

            ctrRasterColumns.PreviousClick +=
                new EventHandler((sender, e) =>
                {
                    DisplayRasterReclass();
                });

            ctrRasterColumns.NextClick +=
                new EventHandler((sender, e) =>
                {
                    ConditionColumns =
                        [.. ctrRasterColumns.SelectedColumns];

                    if (ConditionColumns.Count > 0)
                    {
                        DisplayRasterCondition();
                    }
                    else
                    {
                        DisplayConfigSummary();
                    }
                });

            ctrRasterColumns.CreateControl();
            Controls.Add(value: ctrRasterColumns);
        }

        /// <summary>
        /// <para lang="cs">Zobrazí ovládací prvek "Sestavení výběrové podmínky pro rastrovou vrstvu"</para>
        /// <para lang="en">Shows control "Constructing a selection condition for a raster layer"</para>
        /// </summary>
        private void DisplayRasterCondition()
        {
            Controls.Clear();
            if (Configuration == null)
            {
                return;
            }

            ControlRasterCondition ctrRasterCondition =
                new(controlOwner: this)
                {
                    Dock = DockStyle.Fill
                };
            ctrRasterCondition.LoadContent();

            ctrRasterCondition.PreviousClick +=
                new EventHandler((sender, e) =>
                {
                    DisplayRasterColumns();
                });

            ctrRasterCondition.NextClick +=
                new EventHandler((sender, e) =>
                {
                    DisplayConfigSummary();
                });

            ctrRasterCondition.CreateControl();
            Controls.Add(value: ctrRasterCondition);
        }


        /// <summary>
        /// <para lang="cs">Zobrazí ovládací prvek "Kombinace vrstev vektor x raster"</para>
        /// <para lang="en">Shows control "Combination layers vector x raster"</para>
        /// </summary>
        private void DisplayVRInteraction()
        {
            Controls.Clear();
            if (Configuration == null)
            {
                return;
            }

            ControlInteraction ctrInteraction =
                new(
                    controlOwner: this,
                    configFunctionA: Database.SAuxiliaryData.CConfigFunction.VectorTotal,
                    configFunctionB: Database.SAuxiliaryData.CConfigFunction.RasterTotal)
                {
                    Dock = DockStyle.Fill
                };
            ctrInteraction.LoadContent();

            ctrInteraction.PreviousClick +=
                new EventHandler((sender, e) =>
                {
                    DisplayConfigIntro();
                });

            ctrInteraction.NextClick +=
                new EventHandler((sender, e) =>
                {
                    DisplayConfigSummary();
                });

            ctrInteraction.CreateControl();
            Controls.Add(value: ctrInteraction);
        }

        /// <summary>
        /// <para lang="cs">Zobrazí ovládací prvek "Kombinace vrstev raster x raster"</para>
        /// <para lang="en">Shows control "Combination layers raster x raster"</para>
        /// </summary>
        private void DisplayRRInteraction()
        {
            Controls.Clear();
            if (Configuration == null)
            {
                return;
            }

            ControlInteraction ctrInteraction =
                new(
                    controlOwner: this,
                    configFunctionA: Database.SAuxiliaryData.CConfigFunction.RasterTotal,
                    configFunctionB: Database.SAuxiliaryData.CConfigFunction.RasterTotal)
                {
                    Dock = DockStyle.Fill
                };
            ctrInteraction.LoadContent();

            ctrInteraction.PreviousClick +=
                new EventHandler((sender, e) =>
                {
                    DisplayConfigIntro();
                });

            ctrInteraction.NextClick +=
                new EventHandler((sender, e) =>
                {
                    DisplayConfigSummary();
                });

            ctrInteraction.CreateControl();
            Controls.Add(value: ctrInteraction);
        }


        /// <summary>
        /// <para lang="cs">Zobrazí ovládací prvek "Výběr konfigurací pro získání úhrnu"</para>
        /// <para lang="en">Shows control "Selecting configurations to get a summary"</para>
        /// </summary>
        private void DisplayCategorySum()
        {
            Controls.Clear();
            if (Configuration == null)
            {
                return;
            }

            ControlCategory ctrCategory =
                new(controlOwner: this)
                {
                    Dock = DockStyle.Fill,
                    IsComplete = false
                };
            ctrCategory.LoadContent();

            ctrCategory.PreviousClick +=
                new EventHandler((sender, e) =>
                {
                    DisplayConfigIntro();
                });

            ctrCategory.NextClick +=
                new EventHandler((sender, e) =>
                {
                    DisplayConfigSummary();
                });

            ctrCategory.CreateControl();
            Controls.Add(value: ctrCategory);
        }

        /// <summary>
        /// <para lang="cs">Zobrazí ovládací prvek "Výběr konfigurací pro získání doplňku do rozlohy výpočetní buňky"</para>
        /// <para lang="en">Shows control "Selecting configurations to get a complement to estimation cell area"</para>
        /// </summary>
        private void DisplayEstimationCellComplement()
        {
            Controls.Clear();
            if (Configuration == null)
            {
                return;
            }

            ControlCategory ctrCategory =
                new(controlOwner: this)
                {
                    Dock = DockStyle.Fill,
                    IsComplete = false
                };
            ctrCategory.LoadContent();

            ctrCategory.PreviousClick +=
                new EventHandler((sender, e) =>
                {
                    DisplayConfigIntro();
                });

            ctrCategory.NextClick +=
                new EventHandler((sender, e) =>
                {
                    DisplayConfigSummary();
                });

            ctrCategory.CreateControl();
            Controls.Add(value: ctrCategory);
        }

        /// <summary>
        /// <para lang="cs">Zobrazí ovládací prvek "Výběr konfigurací pro získání celku"</para>
        /// <para lang="en">Shows control "Selecting configurations to get a complete"</para>
        /// </summary>
        private void DisplayCategoryComplete()
        {
            Controls.Clear();
            if (Configuration == null)
            {
                return;
            }

            ControlCategory ctrCategory =
                new(controlOwner: this)
                {
                    Dock = DockStyle.Fill,
                    IsComplete = true
                };
            ctrCategory.LoadContent();

            ctrCategory.PreviousClick +=
                new EventHandler((sender, e) =>
                {
                    DisplayConfigIntro();
                });

            ctrCategory.NextClick +=
                new EventHandler((sender, e) =>
                {
                    DisplayCategoryComplement();
                });

            ctrCategory.CreateControl();
            Controls.Add(value: ctrCategory);
        }

        /// <summary>
        /// <para lang="cs">Zobrazí ovládací prvek "Výběr konfigurací pro získání doplňku do rozlohy celku"</para>
        /// <para lang="en">Shows control "Selecting configurations to get a complement to complete area"</para>
        /// </summary>
        private void DisplayCategoryComplement()
        {
            Controls.Clear();
            if (Configuration == null)
            {
                return;
            }

            ControlCategory ctrCategory =
                new(controlOwner: this)
                {
                    Dock = DockStyle.Fill,
                    IsComplete = false
                };
            ctrCategory.LoadContent();

            ctrCategory.PreviousClick +=
                new EventHandler((sender, e) =>
                {
                    DisplayCategoryComplete();
                });

            ctrCategory.NextClick +=
                new EventHandler((sender, e) =>
                {
                    DisplayConfigSummary();
                });

            ctrCategory.CreateControl();
            Controls.Add(value: ctrCategory);
        }

        /// <summary>
        /// <para lang="cs">Zobrazí ovládací prvek "Výběr odkazu na existující konfiguraci"</para>
        /// <para lang="en">Shows control "Selecting a link to an existing configuration"</para>
        /// </summary>
        private void DisplayCategoryLink()
        {
            Controls.Clear();
            if (Configuration == null)
            {
                return;
            }

            ControlCategory ctrCategory =
                new(controlOwner: this)
                {
                    Dock = DockStyle.Fill,
                    IsComplete = false
                };
            ctrCategory.LoadContent();

            ctrCategory.PreviousClick +=
                new EventHandler((sender, e) =>
                {
                    DisplayConfigIntro();
                });

            ctrCategory.NextClick +=
                new EventHandler((sender, e) =>
                {
                    DisplayConfigSummary();
                });

            ctrCategory.CreateControl();
            Controls.Add(value: ctrCategory);
        }


        /// <summary>
        /// <para lang="cs">Zobrazí ovládací prvek "Zápis nové konfigurace do databáze"</para>
        /// <para lang="en">Shows control "Insert new configuration to database"</para>
        /// </summary>
        private void DisplayConfigSummary()
        {
            Controls.Clear();
            if (Configuration == null)
            {
                return;
            }

            ControlConfigurationSummary ctrConfigurationSummary =
                new(controlOwner: this)
                {
                    Dock = DockStyle.Fill
                };
            ctrConfigurationSummary.LoadContent();

            ctrConfigurationSummary.PreviousClick +=
                new EventHandler((sender, e) =>
                {
                    switch (Configuration.ConfigQueryValue)
                    {
                        case ConfigQueryEnum.GisLayer:
                            switch (Configuration.ConfigCollection.ConfigFunctionValue)
                            {
                                case ConfigFunctionEnum.VectorTotal:
                                    if (ConditionColumns.Count > 0)
                                    {
                                        DisplayVectorCondition();
                                    }
                                    else
                                    {
                                        DisplayVectorColumns();
                                    }
                                    return;

                                case ConfigFunctionEnum.RasterTotal:
                                    if (ConditionColumns.Count > 0)
                                    {
                                        DisplayRasterCondition();
                                    }
                                    else
                                    {
                                        DisplayRasterColumns();
                                    }
                                    return;

                                case ConfigFunctionEnum.RasterTotalWithinVector:
                                    DisplayVRInteraction();
                                    return;

                                case ConfigFunctionEnum.RastersProductTotal:
                                    DisplayRRInteraction();
                                    return;

                                case ConfigFunctionEnum.PointLayer:
                                    // Nemá konfigurace
                                    return;

                                case ConfigFunctionEnum.PointTotalCombination:
                                    // Nemá konfigurace
                                    return;

                                default:
                                    return;
                            }

                        case ConfigQueryEnum.SumOfVariables:
                            DisplayCategorySum();
                            return;

                        case ConfigQueryEnum.AdditionToCellArea:
                            DisplayEstimationCellComplement();
                            return;

                        case ConfigQueryEnum.AdditionToTotal:
                            DisplayCategoryComplement();
                            return;

                        case ConfigQueryEnum.LinkToVariable:
                            DisplayCategoryLink();
                            return;

                        default:
                            return;
                    }
                });

            ctrConfigurationSummary.NextClick +=
                new EventHandler((sender, e) =>
                {
                    DialogResult = DialogResult.OK;
                    Close();
                });

            ctrConfigurationSummary.CreateControl();
            Controls.Add(value: ctrConfigurationSummary);
        }

        #endregion Methods

    }

}