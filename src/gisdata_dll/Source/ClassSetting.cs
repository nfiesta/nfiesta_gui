﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Drawing;
using System.IO;
using System.Runtime.Versioning;
using System.Text;
using System.Text.Encodings.Web;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Text.Unicode;
using System.Windows.Forms;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace ModuleAuxiliaryData
    {

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro pomocná data</para>
        /// <para lang="en">Module for auxiliary data setting</para>
        /// </summary>
        [SupportedOSPlatform("windows")]
        public class Setting
        {

            #region Constants

#if DEBUG
            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Parametry připojení k databázovému serveru</para>
            /// <para lang="en">Default value - Database server connection parameters</para>
            /// </summary>
            private static readonly PostgreSQL.Setting DefaultDBSetting = new()
            {
                ApplicationName = "NfiEstaGUI: Module Auxiliary Data",
                LanguageVersion = LanguageVersion.National,
                Host = "localhost",
                Hosts = ["bran-nfiesta", "localhost"],
                Port = 5555,
                Ports = [5432, 5433, 5555],
                Database = "pathfinder_ds_1_5_jirka",
                Databases = [
                    "nfiesta_gisdata", "nfiesta_gisdata_dev", "nfiesta_test",
                    "pathfinder_ds", "pathfinder_ds_1_5", "pathfinder_ds_1_5_jirka"],
                UserName = "jirka",
                UserNames = ["jirka", "vagrant"],
                CommandTimeout = 0,         // SQL příkazy mají neomezenou dobu na vykonání
                KeepAlive = 180,            // keep alive je posíláno 1x za 3 minuty
                TcpKeepAlive = true,        // tcp keep alive zapnuto
                TcpKeepAliveTime = 60,      // tcp keep alive je posíláno 1x za 1 minutu
                TcpKeepAliveInterval = 5
            };
#else
            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Parametry připojení k databázovému serveru</para>
            /// <para lang="en">Default value - Database server connection parameters</para>
            /// </summary>
            private static readonly PostgreSQL.Setting DefaultDBSetting = new()
            {
                ApplicationName = "NfiEstaGUI: Module Auxiliary Data",
                LanguageVersion = LanguageVersion.International,
                Host = "localhost",
                Hosts = ["bran-nfiesta", "localhost"],
                Port = 5555,
                Ports = [5432, 5433, 5555],
                Database = "pathfinder_ds",
                Databases = [
                    "nfiesta_gisdata", "nfiesta_gisdata_dev",
                    "pathfinder_ds", "pathfinder_ds_1_5"],
                UserName = "vagrant",
                UserNames = ["vagrant"],
                CommandTimeout = 0,         // SQL příkazy mají neomezenou dobu na vykonání
                KeepAlive = 180,            // keep alive je posíláno 1x za 3 minuty
                TcpKeepAlive = true,        // tcp keep alive zapnuto
                TcpKeepAliveTime = 60,      // tcp keep alive je posíláno 1x za 1 minutu
                TcpKeepAliveInterval = 5
            };
#endif

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Počet vláken pro výpočet úhrnů pomocných proměnných</para>
            /// <para lang="en">Default value - Number of threads to calculate auxiliary variable totals</para>
            /// </summary>
            public const int DefaultAuxiliaryDataThreadCount = 10;

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Zobrazit stavový řádek</para>
            /// <para lang="en">Default value - Display status strip</para>
            /// </summary>
            private const bool DefaultStatusStripVisible = false;

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Zobrazení prováděných SQL příkazů při ladění modulu</para>
            /// <para lang="en">Default value - Display executed SQL commands when debugging module</para>
            /// </summary>
            private const bool DefaultVerbose = false;

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Potlačení zobrazení zprávy o chybě z SQL příkazu</para>
            /// <para lang="en">Default value - Suppressing the display of error message from SQL statement</para>
            /// </summary>
            private const bool DefaultDBSuppressError = false;

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Potlačení zobrazení zpráv z SQL příkazu</para>
            /// <para lang="en">Default value - Suppressing the display of messages from SQL statement</para>
            /// </summary>
            private const bool DefaultDBSuppressWarning = true;

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Aktuální verze modulu pro pomocná data</para>
            /// <para lang="en">Default value - Current module for auxiliary data version</para>
            /// </summary>
            private const string DefaultModuleVersion = "4.1.0";

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Délka logu</para>
            /// <para lang="en">Default value - Log length</para>
            /// </summary>
            private const int DefaultLogLength = 50;

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Doba pro překreslení formuláře při výpočtu úhrnů pomocné proměnné v milisekundách</para>
            /// <para lang="en">Default value - Time to redraw the form when calculating auxiliary variable totals in milliseconds</para>
            /// </summary>
            public const int DefaultRefreshTime = 1000;

            #endregion Constants


            #region Private Fields

            /// <summary>
            /// <para lang="cs">Parametry připojení k databázovému serveru</para>
            /// <para lang="en">Database server connection parameters</para>
            /// </summary>
            private PostgreSQL.Setting dbSetting;

            /// <summary>
            /// <para lang="cs">Počet vláken pro výpočet úhrnů pomocných proměnných</para>
            /// <para lang="en">Number of threads to calculate auxiliary variable totals</para>
            /// </summary>
            private Nullable<int> auxiliaryDataThreadCount;

            /// <summary>
            /// <para lang="cs">Zobrazit stavový řádek</para>
            /// <para lang="en">Display status strip</para>
            /// </summary>
            private Nullable<bool> statusStripVisible;

            /// <summary>
            /// <para lang="cs">Zobrazení prováděných SQL příkazů při ladění modulu</para>
            /// <para lang="en">Display executed SQL commands when debugging module</para>
            /// </summary>
            private Nullable<bool> verbose;

            /// <summary>
            /// <para lang="cs">Potlačení zobrazení zprávy o chybě z SQL příkazu</para>
            /// <para lang="en">Suppressing the display of error message from SQL statement</para>
            /// </summary>
            private Nullable<bool> dbSuppressError;

            /// <summary>
            /// <para lang="cs">Potlačení zobrazení zpráv z SQL příkazu</para>
            /// <para lang="en">Suppressing the display of messages from SQL statement</para>
            /// </summary>
            private Nullable<bool> dbSuppressWarning;

            /// <summary>
            /// <para lang="cs">Aktuální verze modulu pro pomocná data</para>
            /// <para lang="en">Current module for auxiliary data version</para>
            /// </summary>
            private string moduleVersion;

            /// <summary>
            /// <para lang="cs">Délka logu</para>
            /// <para lang="en">Log length</para>
            /// </summary>
            private Nullable<int> logLength;

            /// <summary>
            /// <para lang="cs">Doba pro překreslení formuláře při výpočtu úhrnů pomocné proměnné v milisekundách</para>
            /// <para lang="en">Time to redraw the form when calculating auxiliary variable totals in milliseconds</para>
            /// </summary>
            private Nullable<int> refreshTime;

            /// <summary>
            /// <para lang="cs">Volby pro JSON serializer</para>
            /// <para lang="en">JSON serializer options</para>
            /// </summary>
            private JsonSerializerOptions serializerOptions;

            #endregion Private Fields


            #region Constructor

            /// <summary>
            /// <para lang="cs">Konstruktor objektu</para>
            /// <para lang="en">Object constructor</para>
            /// </summary>
            public Setting()
            {
                SerializerOptions = null;
                DBSetting = DefaultDBSetting;
                AuxiliaryDataThreadCount = DefaultAuxiliaryDataThreadCount;
                StatusStripVisible = DefaultStatusStripVisible;
                Verbose = DefaultVerbose;
                DBSuppressError = DefaultDBSuppressError;
                DBSuppressWarning = DefaultDBSuppressWarning;
                ModuleVersion = DefaultModuleVersion;
                LogLength = DefaultLogLength;
                RefreshTime = DefaultRefreshTime;
            }

            #endregion Constructor


            #region Properties

            /// <summary>
            /// <para lang="cs">Parametry připojení k databázovému serveru</para>
            /// <para lang="en">Database server connection parameters</para>
            /// </summary>
            public PostgreSQL.Setting DBSetting
            {
                get
                {
                    return dbSetting ?? DefaultDBSetting;
                }
                set
                {
                    dbSetting = value ?? DefaultDBSetting;
                }
            }

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Počet vláken pro výpočet úhrnů pomocných proměnných</para>
            /// <para lang="en">Default value - Number of threads to calculate auxiliary variable totals</para>
            /// </summary>
            public int AuxiliaryDataThreadCount
            {
                get
                {
                    return auxiliaryDataThreadCount ?? DefaultAuxiliaryDataThreadCount;
                }
                set
                {
                    auxiliaryDataThreadCount = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Zobrazit stavový řádek</para>
            /// <para lang="en">Display status strip</para>
            /// </summary>
            public bool StatusStripVisible
            {
                get
                {
                    return statusStripVisible ?? DefaultStatusStripVisible;
                }
                set
                {
                    statusStripVisible = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Zobrazení prováděných SQL příkazů při ladění modulu</para>
            /// <para lang="en">Display executed SQL commands when debugging module</para>
            /// </summary>
            public bool Verbose
            {
                get
                {
                    return verbose ?? DefaultVerbose;
                }
                set
                {
                    verbose = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Potlačení zobrazení zprávy o chybě z SQL příkazu</para>
            /// <para lang="en">Suppressing the display of error message from SQL statement</para>
            /// </summary>
            public bool DBSuppressError
            {
                get
                {
                    return dbSuppressError ?? DefaultDBSuppressError;
                }
                set
                {
                    dbSuppressError = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Potlačení zobrazení zpráv z SQL příkazu</para>
            /// <para lang="en">Suppressing the display of messages from SQL statement</para>
            /// </summary>
            public bool DBSuppressWarning
            {
                get
                {
                    return dbSuppressWarning ?? DefaultDBSuppressWarning;
                }
                set
                {
                    dbSuppressWarning = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Aktuální verze modulu pro pomocná data</para>
            /// <para lang="en">Current module for auxiliary data version</para>
            /// </summary>
            public string ModuleVersion
            {
                get
                {
                    return String.IsNullOrEmpty(value: moduleVersion)
                        ? DefaultModuleVersion
                        : moduleVersion;
                }
                set
                {
                    moduleVersion = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Délka logu</para>
            /// <para lang="en">Log length</para>
            /// </summary>
            public int LogLength
            {
                get
                {
                    return logLength ?? DefaultLogLength;
                }
                set
                {
                    logLength = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Doba pro překreslení formuláře při výpočtu úhrnů pomocné proměnné v milisekundách</para>
            /// <para lang="en">Time to redraw the form when calculating auxiliary variable totals in milliseconds</para>
            /// </summary>
            public int RefreshTime
            {
                get
                {
                    return refreshTime ?? DefaultRefreshTime;
                }
                set
                {
                    refreshTime = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Volby pro JSON serializer</para>
            /// <para lang="en">JSON serializer options</para>
            /// </summary>
            [JsonIgnore]
            private JsonSerializerOptions SerializerOptions
            {
                get
                {
                    return serializerOptions ??
                        new()
                        {
                            Encoder = JavaScriptEncoder.Create(allowedRanges: UnicodeRanges.All),
                            WriteIndented = true
                        };
                }
                set
                {
                    serializerOptions = value ??
                        new()
                        {
                            Encoder = JavaScriptEncoder.Create(allowedRanges: UnicodeRanges.All),
                            WriteIndented = true
                        };
                }
            }

            /// <summary>
            /// <para lang="cs">Serializace objektu do formátu JSON</para>
            /// <para lang="en">Object serialization to JSON format</para>
            /// </summary>
            [JsonIgnore]
            public string JSON
            {
                get
                {
                    return JsonSerializer.Serialize(
                        value: this,
                        options: SerializerOptions);
                }
            }

            #endregion Properties


            #region Static Methods

            /// <summary>
            /// <para lang="cs">Obnovení objektu z formátu JSON</para>
            /// <para lang="en">Restoring an object from JSON format</para>
            /// </summary>
            /// <param name="json">
            /// <para lang="cs">JSON text</para>
            /// <para lang="en">JSON text</para>
            /// </param>
            /// <returns>
            /// <para lang="cs">Nastavení modulu</para>
            /// <para lang="en">Module setting</para>
            /// </returns>
            public static Setting Deserialize(string json)
            {
                try
                {
                    return
                        JsonSerializer.Deserialize<Setting>(json: json);
                }
                catch (Exception exception)
                {
                    MessageBox.Show(
                        text: exception.Message,
                        caption: String.Empty,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Exclamation);

                    return new Setting();
                }
            }

            /// <summary>
            /// <para lang="cs">Načte objekt nastavení ze souboru json</para>
            /// <para lang="en">Loads setting object from json file</para>
            /// </summary>
            /// <param name="path">
            /// <para lang="cs">Cesta k json souboru</para>
            /// <para lang="en">Path to json file</para>
            /// </param>
            /// <returns>
            /// <para lang="cs">Vrací načtený objekt nastavení</para>
            /// <para lang="en">Return loaded setting object</para>
            /// </returns>
            public static Setting LoadFromFile(string path)
            {
                try
                {
                    return Deserialize(
                        json: File.ReadAllText(
                            path: path,
                            encoding: Encoding.UTF8));
                }
                catch (Exception exception)
                {
                    MessageBox.Show(
                        text: exception.Message,
                        caption: String.Empty,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Information);

                    return new Setting();
                }
            }

            #endregion Static Methods


            #region Methods

            /// <summary>
            /// <para lang="cs">Uloží objekt nastavení do souboru json</para>
            /// <para lang="en">Save setting object into json file</para>
            /// </summary>
            /// <param name="path">
            /// <para lang="cs">Cesta k json souboru</para>
            /// <para lang="en">Path to json file</para>
            /// </param>
            public void SaveToFile(string path)
            {
                try
                {
                    File.WriteAllText(
                        path: path,
                        contents: JSON,
                        encoding: Encoding.UTF8);
                }
                catch (Exception exception)
                {
                    MessageBox.Show(
                        text: exception.Message,
                        caption: String.Empty,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Information);
                }
            }

            #endregion Methods


            #region Style

            #region Button

            /// <summary>
            /// <para lang="cs">Styl písma pro tlačítko</para>
            /// <para lang="en">Font style for button</para>
            /// </summary>
            public static readonly Font ButtonFont = new(
                   familyName: "Segoe UI",
                   emSize: 9F,
                   style: FontStyle.Regular,
                   unit: GraphicsUnit.Point,
                   gdiCharSet: 0);

            /// <summary>
            /// <para lang="cs">Barva písma pro tlačítko</para>
            /// <para lang="en">Font color for button</para>
            /// </summary>
            public static readonly Color ButtonForeColor =
                Color.Black;

            #endregion Button


            #region ComboBox

            /// <summary>
            /// <para lang="cs">Styl písma pro rozbalovací seznam</para>
            /// <para lang="en">Font style for combo box</para>
            /// </summary>
            public static readonly Font ComboBoxFont = new(
                   familyName: "Segoe UI",
                   emSize: 9F,
                   style: FontStyle.Regular,
                   unit: GraphicsUnit.Point,
                   gdiCharSet: 0);

            /// <summary>
            /// <para lang="cs">Barva písma pro rozbalovací seznam</para>
            /// <para lang="en">Font color for combo box</para>
            /// </summary>
            public static readonly Color ComboBoxForeColor =
                Color.Black;

            /// <summary>
            /// <para lang="cs">Barva pozadí pro rozbalovací seznam</para>
            /// <para lang="en">Back color for combo box</para>
            /// </summary>
            public static readonly Color ComboBoxBackColor =
                Color.White;

            #endregion ComboBox


            #region CheckBox

            /// <summary>
            /// <para lang="cs">Styl písma pro zaškrtávací políčko</para>
            /// <para lang="en">Font style for check box</para>
            /// </summary>
            public static readonly Font CheckBoxFont = new(
                   familyName: "Segoe UI",
                   emSize: 9F,
                   style: FontStyle.Regular,
                   unit: GraphicsUnit.Point,
                   gdiCharSet: 0);

            /// <summary>
            /// <para lang="cs">Barva písma pro zaškrtávací políčko</para>
            /// <para lang="en">Font color for check box</para>
            /// </summary>
            public static readonly Color CheckBoxForeColor =
                Color.Black;

            #endregion CheckBox


            #region CheckedListBox

            /// <summary>
            /// <para lang="cs">Styl písma pro zaškrtávací seznam</para>
            /// <para lang="en">Font style for checked list box</para>
            /// </summary>
            public static readonly Font CheckedListBoxFont = new(
                   familyName: "Segoe UI",
                   emSize: 9F,
                   style: FontStyle.Regular,
                   unit: GraphicsUnit.Point,
                   gdiCharSet: 0);

            /// <summary>
            /// <para lang="cs">Barva písma pro zaškrtávací seznam</para>
            /// <para lang="en">Font color for checked list box</para>
            /// </summary>
            public static readonly Color CheckedListBoxForeColor =
                Color.Black;

            #endregion CheckedListBox


            #region Form

            /// <summary>
            /// <para lang="cs">Styl písma pro formulář</para>
            /// <para lang="en">Font style for form</para>
            /// </summary>
            public static readonly Font FormFont = new(
                    familyName: "Segoe UI",
                    emSize: 9F,
                    style: FontStyle.Regular,
                    unit: GraphicsUnit.Point,
                    gdiCharSet: 0);

            /// <summary>
            /// <para lang="cs">Barva písma pro formulář</para>
            /// <para lang="en">Font color for form</para>
            /// </summary>
            public static readonly Color FormForeColor =
                Color.Black;

            #endregion Form


            #region GroupBox

            /// <summary>
            /// <para lang="cs">Styl písma pro skupinu</para>
            /// <para lang="en">Font style for group box</para>
            /// </summary>
            public static readonly Font GroupBoxFont = new(
                    familyName: "Segoe UI",
                    emSize: 11.25F,
                    style: FontStyle.Regular,
                    unit: GraphicsUnit.Point,
                    gdiCharSet: 0);

            /// <summary>
            /// <para lang="cs">Barva písma pro skupinu</para>
            /// <para lang="en">Font color for group box</para>
            /// </summary>
            public static readonly Color GroupBoxForeColor =
                Color.MediumBlue;

            #endregion GroupBox


            #region Label

            /// <summary>
            /// <para lang="cs">Styl písma pro popisek</para>
            /// <para lang="en">Font style for label</para>
            /// </summary>
            public static readonly Font LabelFont = new(
                   familyName: "Segoe UI",
                   emSize: 9F,
                   style: FontStyle.Regular,
                   unit: GraphicsUnit.Point,
                   gdiCharSet: 0);

            /// <summary>
            /// <para lang="cs">Barva písma pro popisek</para>
            /// <para lang="en">Font color for label</para>
            /// </summary>
            public static readonly Color LabelForeColor =
                Color.MediumBlue;


            /// <summary>
            /// <para lang="cs">Styl písma pro popisek s hodnotou</para>
            /// <para lang="en">Font style for label with value</para>
            /// </summary>
            public static readonly Font LabelValueFont = new(
                   familyName: "Segoe UI",
                   emSize: 9F,
                   style: FontStyle.Regular,
                   unit: GraphicsUnit.Point,
                   gdiCharSet: 0);

            /// <summary>
            /// <para lang="cs">Barva písma pro popisek s hodnotou</para>
            /// <para lang="en">Font color for label with value</para>
            /// </summary>
            public static readonly Color LabelValueForeColor =
                Color.Black;


            /// <summary>
            /// <para lang="cs">Styl písma pro popisek s chybovou hláškou</para>
            /// <para lang="en">Font style for label with error message</para>
            /// </summary>
            public static readonly Font LabelErrorFont = new(
                   familyName: "Segoe UI",
                   emSize: 9F,
                   style: FontStyle.Regular,
                   unit: GraphicsUnit.Point,
                   gdiCharSet: 0);

            /// <summary>
            /// <para lang="cs">Barva písma pro popisek s chybovou hláškou</para>
            /// <para lang="en">Font color for label with error message</para>
            /// </summary>
            public static readonly Color LabelErrorForeColor =
                Color.Red;


            /// <summary>
            /// <para lang="cs">Styl písma pro popisek s nadpisem</para>
            /// <para lang="en">Font style for label with main caption</para>
            /// </summary>
            public static readonly Font LabelMainFont = new(
                   familyName: "Segoe UI",
                   emSize: 11.25F,
                   style: FontStyle.Bold,
                   unit: GraphicsUnit.Point,
                   gdiCharSet: 0);

            /// <summary>
            /// <para lang="cs">Barva písma pro popisek s nadpisem</para>
            /// <para lang="en">Font color for label with main caption</para>
            /// </summary>
            public static readonly Color LabelMainForeColor =
                Color.Black;

            #endregion Label


            #region ListBox

            /// <summary>
            /// <para lang="cs">Styl písma pro seznam</para>
            /// <para lang="en">Font style for list box</para>
            /// </summary>
            public static readonly Font ListBoxFont = new(
                   familyName: "Segoe UI",
                   emSize: 9F,
                   style: FontStyle.Regular,
                   unit: GraphicsUnit.Point,
                   gdiCharSet: 0);

            /// <summary>
            /// <para lang="cs">Barva písma pro seznam</para>
            /// <para lang="en">Font color for list box</para>
            /// </summary>
            public static readonly Color ListBoxForeColor =
                Color.Black;

            #endregion ListBox


            #region RadioButton

            /// <summary>
            /// <para lang="cs">Styl písma pro přepínací tlačítko</para>
            /// <para lang="en">Font style for radio button</para>
            /// </summary>
            public static readonly Font RadioButtonFont = new(
                   familyName: "Segoe UI",
                   emSize: 9F,
                   style: FontStyle.Regular,
                   unit: GraphicsUnit.Point,
                   gdiCharSet: 0);

            /// <summary>
            /// <para lang="cs">Barva písma pro přepínací tlačítko</para>
            /// <para lang="en">Font color for radio button</para>
            /// </summary>
            public static readonly Color RadioButtonForeColor =
                Color.Black;

            #endregion RadioButton


            #region Table

            /// <summary>
            /// <para lang="cs">Styl písma v hlavičce tabulky</para>
            /// <para lang="en">Font style in table header</para>
            /// </summary>
            public static readonly Font TableHeaderFont = new(
                   familyName: "Segoe UI",
                   emSize: 9F,
                   style: FontStyle.Regular,
                   unit: GraphicsUnit.Point,
                   gdiCharSet: 0);

            /// <summary>
            /// <para lang="cs">Barva písma v hlavičce tabulky</para>
            /// <para lang="en">Font color in table header</para>
            /// </summary>
            public static readonly Color TableHeaderForeColor =
                Color.White;

            /// <summary>
            /// <para lang="cs">Barva pozadí hlavičky tabulky</para>
            /// <para lang="en">Table header back color</para>
            /// </summary>
            public static readonly Color TableHeaderBackColor =
                Color.DarkBlue;


            /// <summary>
            /// <para lang="cs">Styl písma pro řádek tabulky</para>
            /// <para lang="en">Font style for table row</para>
            /// </summary>
            public static readonly Font TableRowFont = new(
                   familyName: "Segoe UI",
                   emSize: 9F,
                   style: FontStyle.Regular,
                   unit: GraphicsUnit.Point,
                   gdiCharSet: 0);

            /// <summary>
            /// <para lang="cs">Barva písma pro řádek tabulky</para>
            /// <para lang="en">Font color for table row</para>
            /// </summary>
            public static readonly Color TableRowForeColor =
                Color.Black;

            /// <summary>
            /// <para lang="cs">Barva pozadí pro řádek tabulky</para>
            /// <para lang="en">Table row back color</para>
            /// </summary>
            public static readonly Color TableRowBackColor =
                Color.White;


            /// <summary>
            /// <para lang="cs">Styl písma pro vybraný řádek tabulky</para>
            /// <para lang="en">Font style for selected table row</para>
            /// </summary>
            public static readonly Font TableSelectedRowFont = new(
                   familyName: "Segoe UI",
                   emSize: 9F,
                   style: FontStyle.Regular,
                   unit: GraphicsUnit.Point,
                   gdiCharSet: 0);

            /// <summary>
            /// <para lang="cs">Barva písma pro vzbrařádek tabulky</para>
            /// <para lang="en">Font color for table row</para>
            /// </summary>
            public static readonly Color TableSelectedRowForeColor =
                Color.Black;

            /// <summary>
            /// <para lang="cs">Barva pozadí pro řádek tabulky</para>
            /// <para lang="en">Table row back color</para>
            /// </summary>
            public static readonly Color TableSelectedRowBackColor =
                Color.Bisque;

            #endregion Table


            #region TextBox

            /// <summary>
            /// <para lang="cs">Styl písma pro textové políčko</para>
            /// <para lang="en">Font style for text box</para>
            /// </summary>
            public static readonly Font TextBoxFont = new(
                   familyName: "Segoe UI",
                   emSize: 9F,
                   style: FontStyle.Regular,
                   unit: GraphicsUnit.Point,
                   gdiCharSet: 0);

            /// <summary>
            /// <para lang="cs">Barva písma pro textové políčko</para>
            /// <para lang="en">Font color for text box</para>
            /// </summary>
            public static readonly Color TextBoxForeColor =
                Color.Black;


            /// <summary>
            /// <para lang="cs">Styl písma pro textové políčko s kódem</para>
            /// <para lang="en">Font style for text box</para>
            /// </summary>
            public static readonly Font TextBoxCodeFont = new(
                   familyName: "Courier New",
                   emSize: 9F,
                   style: FontStyle.Regular,
                   unit: GraphicsUnit.Point,
                   gdiCharSet: 0);

            /// <summary>
            /// <para lang="cs">Barva písma pro textové políčko s kódem</para>
            /// <para lang="en">Font color for text box with code</para>
            /// </summary>
            public static readonly Color TextBoxCodeForeColor =
                Color.Black;

            #endregion TextBox


            #region ToolStripButton

            /// <summary>
            /// <para lang="cs">Styl písma pro tlačítko</para>
            /// <para lang="en">Font style for tool strip button</para>
            /// </summary>
            public static readonly Font ToolStripButtonFont = new(
                   familyName: "Segoe UI",
                   emSize: 9F,
                   style: FontStyle.Regular,
                   unit: GraphicsUnit.Point,
                   gdiCharSet: 0);

            /// <summary>
            /// <para lang="cs">Barva písma pro tlačítko</para>
            /// <para lang="en">Font color for button</para>
            /// </summary>
            public static readonly Color ToolStripButtonForeColor =
                Color.Black;

            #endregion ToolStripButton

            #endregion Style

        }

    }
}
