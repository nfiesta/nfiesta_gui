﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.AuxiliaryData;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi.ModuleAuxiliaryData
{

    /// <summary>
    /// <para lang="cs">Formulář pro zobrazení a výpočet úhrnů pomocné proměnné</para>
    /// <para lang="en">Form for displaying and calculating auxiliary variable totals</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    internal partial class FormCalcTotal
        : Form, INfiEstaControl, IAuxiliaryDataControl
    {

        #region Constants

        private const int ImageRedBall = 0;
        private const int ImageBlueBall = 1;
        private const int ImageYellowBall = 2;
        private const int ImageGreenBall = 3;
        private const int ImageGrayBall = 4;
        private const int ImageLock = 5;
        private const int ImageSchema = 6;
        private const int ImageVector = 7;
        private const int ImageRaster = 8;

        private const string ID = "Id";

        #endregion Constants


        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Uzly v TreeView</para>
        /// <para lang="en">TreeView nodes</para>
        /// </summary>
        private Dictionary<int, TreeNode> nodes;

        /// <summary>
        /// <para lang="cs">Aktuální uzel v TreeView, na který se kliklo pravým tlačítkem myši</para>
        /// <para lang="en">Current right-clicked node</para>
        /// </summary>
        private TreeNode currentNode;

        /// <summary>
        /// <para lang="cs">Indikátor editace ovládacího prvku</para>
        /// <para lang="en">Control editing indicator</para>
        /// </summary>
        private bool onEdit;

        /// <summary>
        /// <para lang="cs">Seznam úhrnů pomocných proměnných pro jednu skupinu konfigurací</para>
        /// <para lang="en">List of calculated auxiliary variable totals for one configuration collection</para>
        /// </summary>
        private TFnGetAuxTotalList auxVarTotals;

        /// <summary>
        /// <para lang="cs">Řídící vlákno výpočtu</para>
        /// <para lang="en">Control thread of the calculation</para>
        /// </summary>
        private TotalsControlThread controlThread;

        /// <summary>
        /// <para lang="cs">Doba výpočtu</para>
        /// <para lang="en">Calculation time</para>
        /// </summary>
        private Stopwatch stopWatch;

        private string msgNoEstimationCell = String.Empty;
        private string msgCalculationRunning = String.Empty;
        private string msgCalculationStopped = String.Empty;
        private string msgCalculationCompleted = String.Empty;
        private string msgCalculationTime = String.Empty;

        #endregion Private Fields


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor ovládacího prvku</para>
        /// <para lang="en">Control constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        public FormCalcTotal(Control controlOwner)
        {
            InitializeComponent();
            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                IAuxiliaryDataControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                if (controlOwner is not ControlAuxiliaryDataMain)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(ControlAuxiliaryDataMain)}.",
                        paramName: nameof(ControlOwner));
                }

                return controlOwner;
            }
            set
            {
                IAuxiliaryDataControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                if (value is not ControlAuxiliaryDataMain)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(ControlAuxiliaryDataMain)}.",
                        paramName: nameof(ControlOwner));
                }

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables (read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((IAuxiliaryDataControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro pomocná data (read-only)</para>
        /// <para lang="en">Module for auxiliary data setting (read-only)</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((IAuxiliaryDataControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">Skupina konfigurací (read-only)</para>
        /// <para lang="en">Configuration collection (read-only)</para>
        /// </summary>
        public ConfigCollection ConfigCollection
        {
            get
            {
                return ((ControlAuxiliaryDataMain)ControlOwner).AvailableConfigCollection;
            }
        }

        /// <summary>
        /// <para lang="cs">Konfigurace (read-only)</para>
        /// <para lang="en">Configuration (read-only)</para>
        /// </summary>
        private Config Configuration
        {
            get
            {
                return ((ControlAuxiliaryDataMain)ControlOwner).SelectedConfiguration;
            }
        }

        /// <summary>
        /// <para lang="cs">Aktuální verze databázové extenze (read-only)</para>
        /// <para lang="en">Current database extension version (read-only)</para>
        /// </summary>
        public ZaJi.NfiEstaPg.AuxiliaryData.ExtensionVersion ExtensionVersion
        {
            get
            {
                return ((ControlAuxiliaryDataMain)ControlOwner).ExtensionVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Aktuální verze modulu pro pomocná data (read-only)</para>
        /// <para lang="en">Current module for auxiliary data version (read-only)</para>
        /// </summary>
        public ZaJi.NfiEstaPg.AuxiliaryData.GUIVersion GUIVersion
        {
            get
            {
                return ((ControlAuxiliaryDataMain)ControlOwner).GUIVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Konfigurace vybraná v ComboBoxu formuláře (read-only)</para>
        /// <para lang="en">Configuration selected in ComboBox on the form (read-only)</para>
        /// </summary>
        private Config SelectedConfiguration
        {
            get
            {
                if (cboConfiguration.SelectedItem == null)
                {
                    return null;
                }
                if (cboConfiguration.SelectedItem is not Config)
                {
                    return null;
                }
                if (((Config)cboConfiguration.SelectedItem).Id == 0)
                {
                    return null;
                }

                return (Config)cboConfiguration.SelectedItem;
            }
        }

        /// <summary>
        /// <para lang="cs">Verze databázové extenze vybraná v ComboBoxu formuláře (read-only)</para>
        /// <para lang="en">Database extension version selected in ComboBox on the form (read-only)</para>
        /// </summary>
        private ZaJi.NfiEstaPg.AuxiliaryData.ExtensionVersion SelectedExtensionVersion
        {
            get
            {
                if (cboExtensionVersion.SelectedItem == null)
                {
                    return null;
                }
                if (cboExtensionVersion.SelectedItem is not ZaJi.NfiEstaPg.AuxiliaryData.ExtensionVersion)
                {
                    return null;
                }
                if (((ZaJi.NfiEstaPg.AuxiliaryData.ExtensionVersion)cboExtensionVersion.SelectedItem).Id == 0)
                {
                    return null;
                }

                return (ZaJi.NfiEstaPg.AuxiliaryData.ExtensionVersion)cboExtensionVersion.SelectedItem;
            }
        }

        /// <summary>
        /// <para lang="cs">Verze databázové extenze vybraná v ComboBoxu formuláře (read-only)</para>
        /// <para lang="en">Database extension version selected in ComboBox on the form (read-only)</para>
        /// </summary>
        private ZaJi.NfiEstaPg.AuxiliaryData.GUIVersion SelectedGUIVersion
        {
            get
            {
                if (cboGUIVersion.SelectedItem == null)
                {
                    return null;
                }
                if (cboGUIVersion.SelectedItem is not ZaJi.NfiEstaPg.AuxiliaryData.GUIVersion)
                {
                    return null;
                }
                if (((ZaJi.NfiEstaPg.AuxiliaryData.GUIVersion)cboGUIVersion.SelectedItem).Id == 0)
                {
                    return null;
                }

                return (ZaJi.NfiEstaPg.AuxiliaryData.GUIVersion)cboGUIVersion.SelectedItem;
            }
        }

        /// <summary>
        /// <para lang="cs">Seznam úhrnů pomocných proměnných pro jednu skupinu konfigurací</para>
        /// <para lang="en">List of calculated auxiliary variable totals for one configuration collection</para>
        /// </summary>
        private TFnGetAuxTotalList AuxVarTotals
        {
            get
            {
                return auxVarTotals ??
                    new TFnGetAuxTotalList(database: Database);
            }
            set
            {
                auxVarTotals = value ??
                    new TFnGetAuxTotalList(database: Database);
            }
        }

        /// <summary>
        /// <para lang="cs">Indikátor editace ovládacího prvku</para>
        /// <para lang="en">Control editing indicator</para>
        /// </summary>
        private bool OnEdit
        {
            get
            {
                return onEdit;
            }
            set
            {
                onEdit = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Uzly v TreeView</para>
        /// <para lang="en">TreeView nodes</para>
        /// </summary>
        private Dictionary<int, TreeNode> Nodes
        {
            get
            {
                return nodes ?? [];
            }
            set
            {
                nodes = value ?? [];
            }
        }

        /// <summary>
        /// <para lang="cs">Aktuální uzel v TreeView, na který se kliklo pravým tlačítkem myši</para>
        /// <para lang="en">Current right-clicked node</para>
        /// </summary>
        private TreeNode CurrentNode
        {
            get
            {
                return currentNode;
            }
            set
            {
                currentNode = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Zaškrtnuté uzly v TreeView (read-only)</para>
        /// <para lang="en">Checked TreeView nodes (read-only)</para>
        /// </summary>
        private IEnumerable<TreeNode> CheckedNodes
        {
            get
            {
                return
                    Nodes.Values
                    .Where(a => a.Checked);
            }
        }

        /// <summary>
        /// <para lang="cs">Zaškrtnuté výpočetní buňky v TreeView (read-only)</para>
        /// <para lang="en">Checked estimation cells in TreeView (read-only)</para>
        /// </summary>
        public IEnumerable<TFnApiGetEstimationCellHierarchy> CheckedEstimationCells
        {
            get
            {
                return CheckedNodes
                    .Where(a => a.Tag != null)
                    .Where(a => a.Tag is TFnApiGetEstimationCellHierarchy)
                    .Select(a => (TFnApiGetEstimationCellHierarchy)a.Tag);
            }
        }

        /// <summary>
        /// <para lang="cs">Identifikační čísla zaškrtnutých výpočetních buňek v TreeView (read-only)</para>
        /// <para lang="en">Identifiers of checked estimation cells in TreeView (read-only)</para>
        /// </summary>
        private List<Nullable<int>> CheckedEstimationCellsIds
        {
            get
            {
                return [..
                    CheckedEstimationCells
                        .Select(a => (Nullable<int>)a.EstimationCellId)
                        .Distinct<Nullable<int>>()
                        .OrderBy(a => a)
                    ];
            }
        }

        /// <summary>
        /// <para lang="cs">Řídící vlákno výpočtu</para>
        /// <para lang="en">Control thread of the calculation</para>
        /// </summary>
        private TotalsControlThread ControlThread
        {
            get
            {
                return controlThread;
            }
            set
            {
                controlThread = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Doba výpočtu</para>
        /// <para lang="en">Calculation time</para>
        /// </summary>
        private Stopwatch StopWatch
        {
            get
            {
                return stopWatch;
            }
            set
            {
                stopWatch = value;
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(FormCalcTotal),                "Úhrny pomocné proměnné" },

                        { nameof(btnCalculate),                 "Zahájit výpočet úhrnů pomocných proměnných pro vybrané výpočetní buňky" },
                        { nameof(btnClose),                     "Zavřít" },
                        { nameof(btnReload),                    "Znovu načíst seznam výpočetních buněk" },
                        { nameof(btnStop),                      "Zastavit výpočet úhrnů pomocných proměnných" },

                        { nameof(cboConfiguration),             "LabelCs" },
                        { nameof(cboExtensionVersion),          "LabelCs" },
                        { nameof(cboGUIVersion),                "LabelCs" },

                        { nameof(grpAuxiliaryVariableTotals),   "Úhrny pomocné proměnné:" },
                        { nameof(grpConfigCollection),          String.Empty },
                        { nameof(grpEstimationCells),           "Seznam výpočetních buněk:" },
                        { nameof(grpLog),                       "Události vláken:" },

                        { nameof(lblConfigurationCaption),      "Kategorie:" },
                        { nameof(lblExtensionVersionCaption),   "Verze extenze:" },
                        { nameof(lblGUIVersionCaption),         "Verze modulu:" },

                        { nameof(msgNoEstimationCell),          "Nejsou vybrané žádné výpočetní buňky pro výpočet úhrnů pomocné proměnné." },
                        { nameof(msgCalculationRunning),        "Probíhá výpočet úhrnů pomocné proměnné." },
                        { nameof(msgCalculationStopped),        "Výpočet úhrnů pomocných proměnných byl zastaven." },
                        { nameof(msgCalculationCompleted),      "Výpočet úhrnů pomocných proměnných byl dokončen." },
                        { nameof(msgCalculationTime),           "Doba výpočtu: $1 sekund." },

                        { nameof(tsmiCheckAll),                 "Vybrat všechny geograficky nižší výpočetní buňky"},
                        { nameof(tsmiUncheckAll),               "Odebrat z výběru všechny geograficky nižší výpočetní buňky" },
                        { nameof(tsmiCheckLevel1),              "Vybrat výpočetní buňky geograficky nižší o jednu úroveň" },
                        { nameof(tsmiCheckLevel2),              "Vybrat výpočetní buňky geograficky nižší o dvě úrovně" },
                        { nameof(tsmiCheckLevel3),              "Vybrat výpočetní buňky geograficky nižší o tři úrovně" },
                        { nameof(tsmiCheckUncalculated),        "Vybrat všechny nevypočtené geograficky nižší výpočetní buňky"},
                        { nameof(tsmiCheckIncomplete),          "Vybrat všechny nedokončené geograficky nižší výpočetní buňky"},
                        { nameof(tsmiCheckCalculated),          "Vybrat všechny vypočtené geograficky nižší výpočetní buňky"},

                        // DataGridView
                        // 0
                        { $"col-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColId.Name}",
                            "Identifikátor" },
                        { $"tip-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColId.Name}",
                            "Identifikátor" },

                        // 1
                        { $"col-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColConfigCollectionId.Name}",
                            "Pomocná proměnná" },
                        { $"tip-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColConfigCollectionId.Name}",
                            "Pomocná proměnná - identifikátor" },

                        // 2
                        { $"col-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColConfigId.Name}",
                            "Kategorie" },
                        { $"tip-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColConfigId.Name}",
                            "Kategorie - identifikátor" },
                        // 3
                        { $"col-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColConfigLabelCs.Name}",
                            "Kategorie" },
                        { $"tip-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColConfigLabelCs.Name}",
                            "Kategorie [cs]" },
                        // 4
                        { $"col-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColConfigDescriptionCs.Name}",
                            "Kategorie" },
                        { $"tip-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColConfigDescriptionCs.Name}",
                            "Kategorie - popis [cs]" },
                        // 5
                        { $"col-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColConfigLabelEn.Name}",
                            "Kategorie" },
                        { $"tip-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColConfigLabelEn.Name}",
                            "Kategorie [en]" },
                        // 6
                        { $"col-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColConfigDescriptionEn.Name}",
                            "Kategorie" },
                        { $"tip-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColConfigDescriptionEn.Name}",
                            "Kategorie - popis [en]" },

                        // 7
                        { $"col-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColEstimationCellId.Name}",
                            "Výpočetní buňka" },
                        { $"tip-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColEstimationCellId.Name}",
                            "Výpočetní buňka - identifikátor" },
                        // 8
                        { $"col-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColEstimationCellLabelCs.Name}",
                            "Výpočetní buňka" },
                        { $"tip-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColEstimationCellLabelCs.Name}",
                            "Výpočetní buňka [cs]" },
                        // 9
                        { $"col-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColEstimationCellDescriptionCs.Name}",
                            "Výpočetní buňka" },
                        { $"tip-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColEstimationCellDescriptionCs.Name}",
                            "Výpočetní buňka - popis [cs]" },
                        // 10
                        { $"col-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColEstimationCellLabelEn.Name}",
                            "Výpočetní buňka" },
                        { $"tip-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColEstimationCellLabelEn.Name}",
                            "Výpočetní buňka [en]" },
                        // 11
                        { $"col-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColEstimationCellDescriptionEn.Name}",
                            "Výpočetní buňka" },
                        { $"tip-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColEstimationCellDescriptionEn.Name}",
                            "Výpočetní buňka - popis [en]" },

                        // 12
                        { $"col-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColCellId.Name}",
                            "Segment výpočetní buňky" },
                        { $"tip-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColCellId.Name}",
                            "Segment výpočetní buňky - identifikátor" },

                        // 13
                        { $"col-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColAuxTotal.Name}",
                            "Úhrn" },
                        { $"tip-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColAuxTotal.Name}",
                            "Úhrn" },

                        // 14
                        { $"col-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColExtVersionId.Name}",
                            "Verze extenze" },
                        { $"tip-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColExtVersionId.Name}",
                            "Verze extenze - identifikátor" },
                        // 15
                        { $"col-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColExtVersionLabelCs.Name}",
                            "Verze extenze" },
                        { $"tip-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColExtVersionLabelCs.Name}",
                            "Verze extenze [cs]" },
                        // 16
                        { $"col-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColExtVersionDescriptionCs.Name}",
                            "Verze extenze" },
                        { $"tip-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColExtVersionDescriptionCs.Name}",
                            "Verze extenze - popis [cs]" },
                        // 17
                        { $"col-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColExtVersionLabelEn.Name}",
                            "Verze extenze" },
                        { $"tip-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColExtVersionLabelEn.Name}",
                            "Verze extenze [en]" },
                        // 18
                        { $"col-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColExtVersionDescriptionEn.Name}",
                            "Verze extenze" },
                        { $"tip-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColExtVersionDescriptionEn.Name}",
                            "Verze extenze - popis [en]" },

                        // 19
                        { $"col-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColGuiVersionId.Name}",
                            "Verze modulu" },
                        { $"tip-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColGuiVersionId.Name}",
                            "Verze modulu - identifikátor" },
                        // 20
                        { $"col-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColGuiVersionLabelCs.Name}",
                            "Verze modulu" },
                        { $"tip-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColGuiVersionLabelCs.Name}",
                            "Verze modulu [cs]" },
                        // 21
                        { $"col-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColGuiVersionDescriptionCs.Name}",
                            "Verze modulu" },
                        { $"tip-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColGuiVersionDescriptionCs.Name}",
                            "Verze modulu - popis [cs]" },
                        // 22
                        { $"col-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColGuiVersionLabelEn.Name}",
                            "Verze modulu" },
                        { $"tip-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColGuiVersionLabelEn.Name}",
                            "Verze modulu [en]" },
                        // 23
                        { $"col-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColGuiVersionDescriptionEn.Name}",
                            "Verze modulu" },
                        { $"tip-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColGuiVersionDescriptionEn.Name}",
                            "Verze modulu - popis [en]" },

                        // 24
                        { $"col-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColEstDate.Name}",
                            "Datum výpočtu" },
                        { $"tip-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColEstDate.Name}",
                            "Datum výpočtu" }
                    }
                    : languageFile.NationalVersion.Data.TryGetValue(
                        key: nameof(FormCalcTotal),
                        out Dictionary<string, string> dictNational)
                            ? dictNational
                            : [],

                LanguageVersion.International => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(FormCalcTotal),                "Auxiliary variable totals" },

                        { nameof(btnCalculate),                 "Begin auxiliary variable totals calculation for selected estimation cells" },
                        { nameof(btnClose),                     "Close" },
                        { nameof(btnReload),                    "Reload list of estimation cells" },
                        { nameof(btnStop),                      "Stop auxiliary variable totals calculation" },

                        { nameof(cboConfiguration),             "LabelEn" },
                        { nameof(cboExtensionVersion),          "LabelEn" },
                        { nameof(cboGUIVersion),                "LabelEn" },

                        { nameof(grpAuxiliaryVariableTotals),   "Auxiliary variable totals:" },
                        { nameof(grpConfigCollection),          String.Empty },
                        { nameof(grpEstimationCells),           "List of estimation cells:" },
                        { nameof(grpLog),                       "Threads events:" },

                        { nameof(lblConfigurationCaption),      "Category:" },
                        { nameof(lblExtensionVersionCaption),   "Extension version:" },
                        { nameof(lblGUIVersionCaption),         "Module version:" },

                        { nameof(msgNoEstimationCell),          "No selected estimation cells for calculation auxiliary variable totals." },
                        { nameof(msgCalculationRunning),        "The calculation of auxiliary variable totals is running." },
                        { nameof(msgCalculationStopped),        "The calculation of auxiliary variable totals has been stopped." },
                        { nameof(msgCalculationCompleted),      "The calculation of auxiliary variable totals has been completed." },
                        { nameof(msgCalculationTime),           "Calculation time: $1 seconds." },

                        { nameof(tsmiCheckAll),                 "Select all geographically inferior estimation cells" },
                        { nameof(tsmiUncheckAll),               "Remove from selection all geographically inferior estimation cells" },
                        { nameof(tsmiCheckLevel1),              "Select estimation cells geographically inferior by one level"},
                        { nameof(tsmiCheckLevel2),              "Select estimation cells geographically inferior by two levels" },
                        { nameof(tsmiCheckLevel3),              "Select estimation cells geographically lower by three levels" },
                        { nameof(tsmiCheckUncalculated),        "Select all uncalculated geographically inferior estimation cells"},
                        { nameof(tsmiCheckIncomplete),          "Select all incomplete geographically inferior estimation cells"},
                        { nameof(tsmiCheckCalculated),          "Select all calculated geographically inferior estimation cells"},

                        // DataGridView
                        // 0
                        { $"col-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColId.Name}",
                            "Identifier" },
                        { $"tip-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColId.Name}",
                            "Identifier" },

                        // 1
                        { $"col-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColConfigCollectionId.Name}",
                            "Auxiliary variable" },
                        { $"tip-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColConfigCollectionId.Name}",
                            "Auxiliary variable - identifier" },

                        // 2
                        { $"col-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColConfigId.Name}",
                            "Category" },
                        { $"tip-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColConfigId.Name}",
                            "Category - identifier" },
                        // 3
                        { $"col-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColConfigLabelCs.Name}",
                            "Category" },
                        { $"tip-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColConfigLabelCs.Name}",
                            "Category [cs]" },
                        // 4
                        { $"col-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColConfigDescriptionCs.Name}",
                            "Category" },
                        { $"tip-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColConfigDescriptionCs.Name}",
                            "Category - description [cs]" },
                        // 5
                        { $"col-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColConfigLabelEn.Name}",
                            "Category" },
                        { $"tip-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColConfigLabelEn.Name}",
                            "Category [en]" },
                        // 6
                        { $"col-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColConfigDescriptionEn.Name}",
                            "Category" },
                        { $"tip-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColConfigDescriptionEn.Name}",
                            "Category - description [en]" },

                        // 7
                        { $"col-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColEstimationCellId.Name}",
                            "Estimation cell" },
                        { $"tip-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColEstimationCellId.Name}",
                            "Estimation cell - identifier" },
                        // 8
                        { $"col-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColEstimationCellLabelCs.Name}",
                            "Estimation cell" },
                        { $"tip-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColEstimationCellLabelCs.Name}",
                            "Estimation cell [cs]" },
                        // 9
                        { $"col-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColEstimationCellDescriptionCs.Name}",
                            "Estimation cell" },
                        { $"tip-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColEstimationCellDescriptionCs.Name}",
                            "Estimation cell - description [cs]" },
                        // 10
                        { $"col-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColEstimationCellLabelEn.Name}",
                            "Estimation cell" },
                        { $"tip-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColEstimationCellLabelEn.Name}",
                            "Estimation cell [en]" },
                        // 11
                        { $"col-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColEstimationCellDescriptionEn.Name}",
                            "Estimation cell" },
                        { $"tip-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColEstimationCellDescriptionEn.Name}",
                            "Estimation cell - description [en]" },

                        // 12
                        { $"col-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColCellId.Name}",
                            "Estimation cell segment" },
                        { $"tip-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColCellId.Name}",
                            "Estimation cell segment - identifier" },

                        // 13
                        { $"col-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColAuxTotal.Name}",
                            "Total" },
                        { $"tip-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColAuxTotal.Name}",
                            "Total" },

                        // 14
                        { $"col-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColExtVersionId.Name}",
                            "Extension version" },
                        { $"tip-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColExtVersionId.Name}",
                            "Extension version - identifier" },
                        // 15
                        { $"col-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColExtVersionLabelCs.Name}",
                            "Extension version" },
                        { $"tip-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColExtVersionLabelCs.Name}",
                            "Extension version [cs]" },
                        // 16
                        { $"col-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColExtVersionDescriptionCs.Name}",
                            "Extension version" },
                        { $"tip-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColExtVersionDescriptionCs.Name}",
                            "Extension version - description [cs]" },
                        // 17
                        { $"col-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColExtVersionLabelEn.Name}",
                            "Extension version" },
                        { $"tip-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColExtVersionLabelEn.Name}",
                            "Extension version [en]" },
                        // 18
                        { $"col-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColExtVersionDescriptionEn.Name}",
                            "Extension version" },
                        { $"tip-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColExtVersionDescriptionEn.Name}",
                            "Extension version - description [en]" },

                        // 19
                        { $"col-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColGuiVersionId.Name}",
                            "Module version" },
                        { $"tip-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColGuiVersionId.Name}",
                            "Module version - identifier" },
                        // 20
                        { $"col-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColGuiVersionLabelCs.Name}",
                            "Module version" },
                        { $"tip-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColGuiVersionLabelCs.Name}",
                            "Module version [cs]" },
                        // 21
                        { $"col-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColGuiVersionDescriptionCs.Name}",
                            "Module version" },
                        { $"tip-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColGuiVersionDescriptionCs.Name}",
                            "Module version - description [cs]" },
                        // 22
                        { $"col-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColGuiVersionLabelEn.Name}",
                            "Module version" },
                        { $"tip-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColGuiVersionLabelEn.Name}",
                            "Module version [en]" },
                        // 23
                        { $"col-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColGuiVersionDescriptionEn.Name}",
                            "Module version" },
                        { $"tip-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColGuiVersionDescriptionEn.Name}",
                            "Module version - description [en]" },

                        // 24
                        { $"col-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColEstDate.Name}",
                            "Estimation date" },
                        { $"tip-{TFnGetAuxTotalList.Name}.{TFnGetAuxTotalList.ColEstDate.Name}",
                            "Estimation date" }
                    }
                    : languageFile.InternationalVersion.Data.TryGetValue(
                        key: nameof(FormCalcTotal),
                        out Dictionary<string, string> dictInternational)
                            ? dictInternational
                            : [],

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku</para>
        /// <para lang="en">Initialization of the control</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        private void Initialize(Control controlOwner)
        {
            ControlOwner = controlOwner;
            Nodes = [];
            CurrentNode = null;
            OnEdit = false;
            AuxVarTotals = new TFnGetAuxTotalList(database: Database);
            ControlThread = null;
            StopWatch = null;

            InitializeFilters();

            InitializeLabels();

            InitializeTreeView();

            ResetTreeViewNodeImageIndexes();

            GetAuxTotal();

            FormClosing += new FormClosingEventHandler(
                (sender, e) =>
                {
                    Stop();
                });

            btnClose.Click += new EventHandler(
                (sender, e) =>
                {
                    DialogResult = DialogResult.OK;
                    Close();
                });

            btnReload.Click += new EventHandler(
                (sender, e) =>
                {
                    if (IsWorking(verbose: true)) { return; }
                    GetAuxTotal();
                    ResetTreeViewNodeImageIndexes();
                });

            btnCalculate.Click += new EventHandler(
                (sender, e) =>
                {
                    Calculate(recalc: false);
                });

            btnStop.Click += new EventHandler(
                (sender, e) =>
                {
                    Stop();
                });

            cboConfiguration.SelectedIndexChanged += new EventHandler(
                (sender, e) =>
                {
                    if (onEdit) { return; }
                    GetAuxTotal();
                });

            cboExtensionVersion.SelectedIndexChanged += new EventHandler(
                (sender, e) =>
                {
                    if (onEdit) { return; }
                    GetAuxTotal();
                });

            cboGUIVersion.SelectedIndexChanged += new EventHandler(
                (sender, e) =>
                {
                    if (onEdit) { return; }
                    GetAuxTotal();
                });

            dgvData.SelectionChanged += new EventHandler(
                (sender, e) =>
                {
                    dgvData.ClearSelection();
                });

            tvwEstimationCells.AfterCheck += new TreeViewEventHandler(
                (sender, e) =>
                {
                    if (OnEdit) { return; }
                    GetAuxTotal();
                });

            tvwEstimationCells.AfterSelect += new TreeViewEventHandler(
               (sender, e) =>
               {
                   tvwEstimationCells.SelectedNode = null;
               });

            tvwEstimationCells.NodeMouseClick += new TreeNodeMouseClickEventHandler(
                (sender, e) =>
                {
                    if (e.Button == MouseButtons.Right)
                    {
                        CurrentNode = e.Node;
                    }
                });

            tsmiCheckAll.Click += new EventHandler(
                (sender, e) =>
                {
                    if (CurrentNode == null) { return; }
                    OnEdit = true;
                    foreach (TreeNode node in GetInferiorNodes(parentNode: CurrentNode))
                    {
                        node.Checked = true;
                    }
                    OnEdit = false;
                    GetAuxTotal();
                });

            tsmiUncheckAll.Click += new EventHandler(
               (sender, e) =>
               {
                   if (CurrentNode == null) { return; }
                   OnEdit = true;
                   foreach (TreeNode node in GetInferiorNodes(parentNode: CurrentNode))
                   {
                       node.Checked = false;
                   }
                   OnEdit = false;
                   GetAuxTotal();
               });

            tsmiCheckLevel1.Click += new EventHandler(
                (sender, e) =>
                {
                    if (CurrentNode == null) { return; }
                    OnEdit = true;
                    foreach (TreeNode node in GetInferiorNodes(parentNode: CurrentNode))
                    {
                        node.Checked = (node.Level == CurrentNode.Level + 1);
                    }
                    OnEdit = false;
                    GetAuxTotal();
                });

            tsmiCheckLevel2.Click += new EventHandler(
                (sender, e) =>
                {
                    if (CurrentNode == null) { return; }
                    OnEdit = true;
                    foreach (TreeNode node in GetInferiorNodes(parentNode: CurrentNode))
                    {
                        node.Checked = (node.Level == CurrentNode.Level + 2);
                    }
                    OnEdit = false;
                    GetAuxTotal();
                });

            tsmiCheckLevel3.Click += new EventHandler(
                (sender, e) =>
                {
                    if (CurrentNode == null) { return; }
                    OnEdit = true;
                    foreach (TreeNode node in GetInferiorNodes(parentNode: CurrentNode))
                    {
                        node.Checked = (node.Level == CurrentNode.Level + 3);
                    }
                    OnEdit = false;
                    GetAuxTotal();
                });

            tsmiCheckUncalculated.Click += new EventHandler(
                (sender, e) =>
                {
                    if (CurrentNode == null) { return; }
                    OnEdit = true;
                    foreach (TreeNode node in GetInferiorNodes(parentNode: CurrentNode))
                    {
                        node.Checked = (node.ImageIndex == ImageGrayBall);
                    }
                    OnEdit = false;
                    GetAuxTotal();
                });

            tsmiCheckIncomplete.Click += new EventHandler(
                (sender, e) =>
                {
                    if (CurrentNode == null) { return; }
                    OnEdit = true;
                    foreach (TreeNode node in GetInferiorNodes(parentNode: CurrentNode))
                    {
                        node.Checked = (node.ImageIndex == ImageYellowBall);
                    }
                    OnEdit = false;
                    GetAuxTotal();
                });

            tsmiCheckCalculated.Click += new EventHandler(
               (sender, e) =>
               {
                   if (CurrentNode == null) { return; }
                   OnEdit = true;
                   foreach (TreeNode node in GetInferiorNodes(parentNode: CurrentNode))
                   {
                       node.Checked = (node.ImageIndex == ImageGreenBall);
                   }
                   OnEdit = false;
                   GetAuxTotal();
               });
        }

        /// <summary>
        /// <para lang="cs">Inicializuje rozbovací seznamy fitrů</para>
        /// <para lang="en">Initializes comboboxes of fitters</para>
        /// </summary>
        private void InitializeFilters()
        {
            OnEdit = true;

            Dictionary<string, string> labels = Dictionary(
               languageVersion: LanguageVersion,
               languageFile: LanguageFile);

            ConfigList tConfig = new(database: Database);
            if (ConfigCollection != null)
            {
                tConfig.ReLoad(
                    condition: $"({ConfigList.ColConfigCollectionId.DbName} IN ({ConfigCollection.Id}))",
                extended: true);
            }

            // cboConfiguration
            cboConfiguration.DataSource = null;
            cboConfiguration.DataSource = tConfig.Items;
            cboConfiguration.ValueMember = ID;
            cboConfiguration.DisplayMember =
                labels.TryGetValue(key: nameof(cboConfiguration),
                out string cboConfigurationDisplayMember)
                    ? cboConfigurationDisplayMember
                    : ID;
            cboConfiguration.SelectedItem =
                tConfig.Items
                .Where(a => a.Id == (Configuration?.Id ?? 0))
                .FirstOrDefault<Config>();

            // cboExtensionVersion
            cboExtensionVersion.DataSource = null;
            cboExtensionVersion.DataSource = Database.SAuxiliaryData.CExtensionVersion.Items;
            cboExtensionVersion.ValueMember = ID;
            cboExtensionVersion.DisplayMember =
                 labels.TryGetValue(key: nameof(cboExtensionVersion), out string cboExtensionVersionDisplayMember)
                    ? cboExtensionVersionDisplayMember
                    : ID;
            cboExtensionVersion.SelectedItem =
                Database.SAuxiliaryData.CExtensionVersion.Items
                .Where(a => a.Id == (ExtensionVersion?.Id ?? 0))
                .FirstOrDefault<ZaJi.NfiEstaPg.AuxiliaryData.ExtensionVersion>();

            // cboGUIVersion
            cboGUIVersion.DataSource = null;
            cboGUIVersion.DataSource = Database.SAuxiliaryData.CGUIVersion.Items;
            cboGUIVersion.ValueMember = ID;
            cboGUIVersion.DisplayMember =
                labels.TryGetValue(key: nameof(cboGUIVersion),
                out string cboGUIVersionDisplayMember)
                    ? cboGUIVersionDisplayMember
                    : ID;
            cboGUIVersion.SelectedItem =
                Database.SAuxiliaryData.CGUIVersion.Items
                .Where(a => a.Id == (GUIVersion?.Id ?? 0))
                .FirstOrDefault<ZaJi.NfiEstaPg.AuxiliaryData.GUIVersion>();

            OnEdit = false;
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků ovládacího prvku</para>
        /// <para lang="en">Initialization of control labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            Dictionary<string, string> labelsCs = Dictionary(
                languageVersion: LanguageVersion.National,
                languageFile: LanguageFile);

            Dictionary<string, string> labelsEn = Dictionary(
                languageVersion: LanguageVersion.International,
                languageFile: LanguageFile);

            #region Controls

            Text =
                labels.TryGetValue(key: nameof(FormCalcTotal),
                out string frmCalcTotalText)
                    ? frmCalcTotalText
                    : String.Empty;

            btnCalculate.Text =
               labels.TryGetValue(key: nameof(btnCalculate),
               out string btnCalculateText)
                   ? btnCalculateText
                   : String.Empty;

            btnClose.Text =
                labels.TryGetValue(key: nameof(btnClose),
                out string btnCloseText)
                    ? btnCloseText
                    : String.Empty;

            btnReload.Text =
                labels.TryGetValue(key: nameof(btnReload),
                out string btnReloadText)
                    ? btnReloadText
                    : String.Empty;

            btnStop.Text =
                labels.TryGetValue(key: nameof(btnStop),
                out string btnStopText)
                    ? btnStopText
                    : String.Empty;

            OnEdit = true;
            cboConfiguration.DisplayMember =
                labels.TryGetValue(key: nameof(cboConfiguration),
                out string cboConfigurationDisplayMember)
                    ? cboConfigurationDisplayMember
                    : ID;

            cboExtensionVersion.DisplayMember =
                labels.TryGetValue(key: nameof(cboExtensionVersion),
                out string cboExtensionVersionDisplayMember)
                    ? cboExtensionVersionDisplayMember
                    : ID;

            cboGUIVersion.DisplayMember =
                labels.TryGetValue(key: nameof(cboGUIVersion),
                out string cboGUIVersionDisplayMember)
                    ? cboGUIVersionDisplayMember
                    : ID;
            onEdit = false;

            grpAuxiliaryVariableTotals.Text =
                labels.TryGetValue(key: nameof(grpAuxiliaryVariableTotals),
                out string grpAuxiliaryVariableTotalsText)
                    ? grpAuxiliaryVariableTotalsText
                    : String.Empty;

            grpConfigCollection.Text =
                labels.TryGetValue(key: nameof(grpConfigCollection),
                out string grpConfigCollectionText)
                    ? grpConfigCollectionText
                    : String.Empty;

            grpEstimationCells.Text =
                labels.TryGetValue(key: nameof(grpEstimationCells),
                out string grpEstimationCellsText)
                    ? grpEstimationCellsText
                    : String.Empty;

            grpLog.Text =
                labels.TryGetValue(key: nameof(grpLog),
                out string grpLogText)
                    ? grpLogText
                    : String.Empty;

            lblConfigurationCaption.Text =
                labels.TryGetValue(key: nameof(lblConfigurationCaption),
                out string lblConfigurationCaptionText)
                    ? lblConfigurationCaptionText
                    : String.Empty;

            lblExtensionVersionCaption.Text =
                labels.TryGetValue(key: nameof(lblExtensionVersionCaption),
                out string lblExtensionVersionCaptionText)
                    ? lblExtensionVersionCaptionText
                    : String.Empty;

            lblGUIVersionCaption.Text =
                labels.TryGetValue(key: nameof(lblGUIVersionCaption),
                out string lblGUIVersionCaptionText)
                    ? lblGUIVersionCaptionText
                    : String.Empty;

            lblConfigCollection.Text =
                (LanguageVersion == LanguageVersion.International)
                    ? ConfigCollection?.ExtendedLabelEn ?? String.Empty
                    : (LanguageVersion == LanguageVersion.National)
                        ? ConfigCollection?.ExtendedLabelCs ?? String.Empty
                        : ConfigCollection?.ExtendedLabelEn ?? String.Empty;

            msgNoEstimationCell =
                labels.TryGetValue(key: nameof(msgNoEstimationCell),
                out msgNoEstimationCell)
                    ? msgNoEstimationCell
                    : String.Empty;

            msgCalculationRunning =
                labels.TryGetValue(key: nameof(msgCalculationRunning),
                out msgCalculationRunning)
                    ? msgCalculationRunning
                    : String.Empty;

            msgCalculationStopped =
                labels.TryGetValue(key: nameof(msgCalculationStopped),
                out msgCalculationStopped)
                    ? msgCalculationStopped
                    : String.Empty;

            msgCalculationCompleted =
                labels.TryGetValue(key: nameof(msgCalculationCompleted),
                out msgCalculationCompleted)
                    ? msgCalculationCompleted
                    : String.Empty;

            msgCalculationTime =
                labels.TryGetValue(key: nameof(msgCalculationTime),
                out msgCalculationTime)
                    ? msgCalculationTime
                    : String.Empty;

            tsmiCheckAll.Text =
                labels.TryGetValue(key: nameof(tsmiCheckAll),
                out string tsmiCheckAllText)
                    ? tsmiCheckAllText
                    : String.Empty;

            tsmiUncheckAll.Text =
                labels.TryGetValue(key: nameof(tsmiUncheckAll),
                out string tsmiUncheckAllText)
                    ? tsmiUncheckAllText
                    : String.Empty;

            tsmiCheckLevel1.Text =
                labels.TryGetValue(key: nameof(tsmiCheckLevel1),
                out string tsmiCheckLevel1Text)
                    ? tsmiCheckLevel1Text
                    : String.Empty;

            tsmiCheckLevel2.Text =
                labels.TryGetValue(key: nameof(tsmiCheckLevel2),
                out string tsmiCheckLevel2Text)
                    ? tsmiCheckLevel2Text
                    : String.Empty;

            tsmiCheckLevel3.Text =
                labels.TryGetValue(key: nameof(tsmiCheckLevel3),
                out string tsmiCheckLevel3Text)
                    ? tsmiCheckLevel3Text
                    : String.Empty;

            tsmiCheckUncalculated.Text =
                labels.TryGetValue(key: nameof(tsmiCheckUncalculated),
                out string tsmiCheckUncalculatedText)
                    ? tsmiCheckUncalculatedText
                    : String.Empty;

            tsmiCheckIncomplete.Text =
                labels.TryGetValue(key: nameof(tsmiCheckIncomplete),
                out string tsmiCheckIncompleteText)
                    ? tsmiCheckIncompleteText
                    : String.Empty;

            tsmiCheckCalculated.Text =
                labels.TryGetValue(key: nameof(tsmiCheckCalculated),
                out string tsmiCheckCalculatedText)
                    ? tsmiCheckCalculatedText
                    : String.Empty;

            foreach (TreeNode node in Nodes.Values)
            {
                if ((node.Tag != null) &&
                    (node.Tag is TFnApiGetEstimationCellHierarchy estimationCell))
                {
                    node.Text =
                        (LanguageVersion == LanguageVersion.International)
                            ? estimationCell.EstimationCellDescriptionEn
                            : (LanguageVersion == LanguageVersion.National)
                                ? estimationCell.EstimationCellDescriptionCs
                                : estimationCell.EstimationCellDescriptionEn;
                }
            }

            #endregion Controls

            #region Columns

            switch (LanguageVersion)
            {
                case LanguageVersion.National:
                    TFnGetAuxTotalList.SetColumnsVisibility(
                        TFnGetAuxTotalList.ColConfigLabelCs.Name,
                        TFnGetAuxTotalList.ColEstimationCellDescriptionCs.Name,
                        TFnGetAuxTotalList.ColCellId.Name,
                        TFnGetAuxTotalList.ColAuxTotal.Name,
                        TFnGetAuxTotalList.ColExtVersionLabelCs.Name,
                        TFnGetAuxTotalList.ColGuiVersionLabelCs.Name,
                        TFnGetAuxTotalList.ColEstDate.Name
                        );
                    break;
                case LanguageVersion.International:
                    TFnGetAuxTotalList.SetColumnsVisibility(
                        TFnGetAuxTotalList.ColConfigLabelEn.Name,
                        TFnGetAuxTotalList.ColEstimationCellDescriptionEn.Name,
                        TFnGetAuxTotalList.ColCellId.Name,
                        TFnGetAuxTotalList.ColAuxTotal.Name,
                        TFnGetAuxTotalList.ColExtVersionLabelCs.Name,
                        TFnGetAuxTotalList.ColGuiVersionLabelCs.Name,
                        TFnGetAuxTotalList.ColEstDate.Name
                        );
                    break;
                default:
                    throw new ArgumentException(
                        message: $"Argument {nameof(LanguageVersion)} unknown value.",
                        paramName: nameof(LanguageVersion));
            }

            foreach (ColumnMetadata column in TFnGetAuxTotalList.Cols.Values)
            {
                TFnGetAuxTotalList.SetColumnHeader(
                    columnName:
                        column.Name,
                    headerTextCs:
                        labelsCs.ContainsKey(key: $"col-{TFnGetAuxTotalList.Name}.{column.Name}")
                            ? labelsCs[$"col-{TFnGetAuxTotalList.Name}.{column.Name}"]
                            : String.Empty,
                    headerTextEn:
                        labelsEn.ContainsKey(key: $"col-{TFnGetAuxTotalList.Name}.{column.Name}")
                            ? labelsEn[$"col-{TFnGetAuxTotalList.Name}.{column.Name}"]
                            : String.Empty,
                    toolTipTextCs:
                        labelsCs.ContainsKey(key: $"tip-{TFnGetAuxTotalList.Name}.{column.Name}")
                            ? labelsCs[$"tip-{TFnGetAuxTotalList.Name}.{column.Name}"]
                            : String.Empty,
                    toolTipTextEn:
                        labelsEn.ContainsKey(key: $"tip-{TFnGetAuxTotalList.Name}.{column.Name}")
                            ? labelsEn[$"tip-{TFnGetAuxTotalList.Name}.{column.Name}"]
                            : String.Empty);
            }

            #endregion Columns
        }

        /// <summary>
        /// <para lang="cs">Zobrazení výpočetních buněk v uzlech TreeView</para>
        /// <para lang="en">Displaying estimation cells in TreeView nodes</para>
        /// </summary>
        private void InitializeTreeView()
        {
            OnEdit = true;
            tvwEstimationCells.BeginUpdate();

            // Clear Dictionary and TreeView
            tvwEstimationCells.Nodes.Clear();
            Nodes.Clear();

            // Load Dictionary
            foreach (TFnApiGetEstimationCellHierarchy estimationCell in
                Database.SAuxiliaryData.VEstimationCell.Items)
            {
                TreeNode node = new()
                {
                    Checked = false,
                    ContextMenuStrip = cmsEstimationCells,
                    ImageIndex = ImageGrayBall,
                    Name = estimationCell.Id.ToString(),
                    SelectedImageIndex = ImageGrayBall,
                    Tag = estimationCell,
                    Text =
                        (LanguageVersion == LanguageVersion.International)
                            ? estimationCell.EstimationCellDescriptionEn
                            : (LanguageVersion == LanguageVersion.National)
                                ? estimationCell.EstimationCellDescriptionCs
                                : estimationCell.EstimationCellDescriptionEn
                };
                Nodes.Add(
                    key: estimationCell.Id,
                    value: node);
            }

            // Load TreeView
            foreach (int id in Nodes.Keys)
            {
                TreeNode node = Nodes[id];
                TFnApiGetEstimationCellHierarchy estimationCell =
                    (TFnApiGetEstimationCellHierarchy)node.Tag;
                int parentId = estimationCell.SuperiorId ?? 0;

                if (parentId == 0)
                {
                    // Root nodes
                    tvwEstimationCells.Nodes.Add(node: node);
                }
                else
                {
                    // Descendant nodes
                    TreeNode superiorNode = Nodes[estimationCell.SuperiorId ?? 0];
                    superiorNode.Nodes.Add(node: node);
                }
            }

            tvwEstimationCells.EndUpdate();
            OnEdit = false;
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent() { }

        /// <summary>
        /// <para lang="cs">Načte a zobrazí data úhrnů pomocných proměnných pro vybrané výpočetní buňky</para>
        /// <para lang="en">Retrieves and displays auxiliary variable totals for selected estimation cells</para>
        /// </summary>
        private void GetAuxTotal()
        {
            List<int> configurationIds = null;
            List<int> extensionVersionIds = null;
            List<int> estimationCellIds = null;
            List<int> guiVersionIds = null;

            if (ConfigCollection != null)
            {
                configurationIds =
                ((SelectedConfiguration == null) || (SelectedConfiguration.Id == 0))
                ? Database.SAuxiliaryData.TConfig.Items
                        .Where(a => a.ConfigCollectionId == ConfigCollection.Id)
                        .Where(a => a.Id != 0)
                        .Select(a => a.Id)
                        .Distinct()
                        .ToList<int>()
                : [SelectedConfiguration.Id];

                extensionVersionIds =
                    ((SelectedExtensionVersion == null) || (SelectedExtensionVersion.Id == 0))
                    ? Database.SAuxiliaryData.CExtensionVersion.Items
                        .Where(a => a.Id != 0)
                        .Select(a => a.Id)
                        .Distinct()
                        .ToList<int>()
                    : [SelectedExtensionVersion.Id];

                estimationCellIds =
                    (CheckedEstimationCellsIds != null)
                        ? CheckedEstimationCellsIds
                            .Where(a => a != null)
                            .Select(a => (int)a)
                            .ToList<int>()
                        : [];

                guiVersionIds =
                    ((SelectedGUIVersion == null) || (SelectedGUIVersion.Id == 0))
                    ? Database.SAuxiliaryData.CGUIVersion.Items
                        .Where(a => a.Id != 0)
                        .Select(a => a.Id)
                        .Distinct()
                        .ToList<int>()
                    : [SelectedGUIVersion.Id];

                AuxVarTotals = ADFunctions.FnGetAuxTotal.Execute(
                    database: Database,
                    configurationIds: configurationIds,
                    estimationCellIds: estimationCellIds,
                    extensionVersionIds: extensionVersionIds,
                    guiVersionIds: guiVersionIds);
            }
            else
            {
                configurationIds = null;
                extensionVersionIds = null;
                estimationCellIds = null;
                guiVersionIds = null;
                AuxVarTotals = new TFnGetAuxTotalList(database: Database);
            }

            AuxVarTotals.SetDataGridViewDataSource(
               dataGridView: dgvData);

            AuxVarTotals.FormatDataGridView(
                dataGridView: dgvData);

            dgvData.AutoSizeColumnsMode =
                DataGridViewAutoSizeColumnsMode.Fill;
        }

        /// <summary>
        /// <para lang="cs">
        /// Rekurzivně prochází strom podřízených uzlů zadanému uzlu
        /// a vrací seznam všech pořízených uzlů</para>
        /// <para lang="en">
        /// Recursively traverses the tree of child nodes for parent node
        /// and returns list of all child nodes</para>
        /// </summary>
        /// <param name="parentNode">
        /// <para lang="cs">Vybraný uzel</para>
        /// <para lang="en">Parent node</para>
        /// </param>
        private static List<TreeNode> GetInferiorNodes(TreeNode parentNode)
        {
            List<TreeNode> nodes = [];

            if (parentNode == null)
            {
                // parent null, vrátí prázdný seznam
                return nodes;
            }

            nodes.Add(item: parentNode);

            foreach (TreeNode node in parentNode.Nodes)
            {
                nodes.AddRange(collection: GetInferiorNodes(parentNode: node));
            }

            return nodes;
        }

        /// <summary>
        /// <para lang="cs">Znovu nastaví označení výpočetních buněk - šedá: bez vypočetných úhrnů, zelená: s vypočtenými úhrny</para>
        /// <para lang="en">Resets the icons of the estimation cells - grey: without calculated totals, green: with calculated totals</para>
        /// </summary>
        private void ResetTreeViewNodeImageIndexes()
        {
            List<int> calculatedEstimatimationCellIds = [];
            List<int> notCompleteEstimatimationCellIds = [];

            if (ConfigCollection != null)
            {
                TFnApiGetEstimationCellHierarchyStageList estimationCellStates =
                    ADFunctions.FnApiGetEstimationCellHierarchyStage.Execute(
                        database: Database,
                        configCollectionId: ConfigCollection?.Id);

                calculatedEstimatimationCellIds =
                    estimationCellStates.Items
                    .Where(a => a.Stage != null)
                    .Where(a => a.EstimationCellId != null)
                    .Where(a => (bool)a.Stage)
                    .Select(a => (int)a.EstimationCellId)
                    .Distinct()
                    .ToList<int>();

                notCompleteEstimatimationCellIds =
                    estimationCellStates.Items
                    .Where(a => a.Stage != null)
                    .Where(a => a.EstimationCellId != null)
                    .Where(a => !(bool)a.Stage)
                    .Select(a => (int)a.EstimationCellId)
                    .Distinct()
                    .ToList<int>();
            }

            foreach (TreeNode node in Nodes.Values)
            {
                if (node.Tag == null)
                {
                    // Chybný uzel
                    node.ImageIndex = ImageRedBall;
                    continue;
                }

                if (node.Tag is not TFnApiGetEstimationCellHierarchy)
                {
                    // Chybný node
                    node.ImageIndex = ImageRedBall;
                    continue;
                }

                if (((TFnApiGetEstimationCellHierarchy)node.Tag)?.EstimationCellId == null)
                {
                    // Chybný uzel
                    node.ImageIndex = ImageRedBall;
                    continue;
                }

                if (calculatedEstimatimationCellIds.Contains(item: (int)((TFnApiGetEstimationCellHierarchy)node.Tag).EstimationCellId))
                {
                    // Vypočtený uzel
                    node.ImageIndex = ImageGreenBall;
                    continue;
                }

                if (notCompleteEstimatimationCellIds.Contains(item: (int)((TFnApiGetEstimationCellHierarchy)node.Tag).EstimationCellId))
                {
                    // Nedokončený uzel
                    node.ImageIndex = ImageYellowBall;
                    continue;
                }

                node.ImageIndex = ImageGrayBall;
            }
        }

        /// <summary>
        /// <para lang="cs">Provádí řídící vlákno právě výpočet?</para>
        /// <para lang="en">Is the control thread calculating auxiliary variable totals?</para>
        /// </summary>
        /// <param name="verbose">
        /// <para lang="cs">Zobrazit zprávu</para>
        /// <para lang="en">Display MessageBox</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">ano|ne</para>
        /// <para lang="en">yes|no</para>
        /// </returns>
        public bool IsWorking(bool verbose = false)
        {
            bool isWorking =
                (ControlThread != null) &&
                ControlThread.IsWorking;

            if (verbose && isWorking)
            {
                MessageBox.Show(
                    text: msgCalculationRunning,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);
            }

            return isWorking;
        }

        /// <summary>
        /// <para lang="cs">Zastavení výpočtu úhrnů pomocné proměnné ve výpočetních buňkách</para>
        /// <para lang="en">Stops calculation auxiliary variable totals in estimation cells</para>
        /// </summary>
        private void Stop()
        {
            if (IsWorking(verbose: false))
            {
                ControlThread.Stop();
            }
        }

        /// <summary>
        /// <para lang="cs">Výpočet úhrnů pomocné proměnné ve výpočetních buňkách</para>
        /// <para lang="en">Calculate auxiliary variable totals in estimation cells</para>
        /// </summary>
        /// <param name="recalc">
        /// <para lang="cs">Přepočítat existující úhrny pomocné proměnné ve výpočetních buňkách</para>
        /// <para lang="en">Recalculate existing auxiliary variable totals in estimation cells</para>
        /// </param>
        private void Calculate(bool recalc = false)
        {
            if (!CheckedEstimationCells.Any())
            {
                MessageBox.Show(
                    text: msgNoEstimationCell,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return;
            }

            if (ControlThread == null)
            {
                // Pokud řídící vlákno neexistuje, vytvoří se nové a spustí se výpočet
                NewControlThread(recalc: recalc);
                controlThread.Start(obj: null);
                return;
            }

            if (IsWorking(verbose: true))
            {
                // Pokud už probíhá výpočet, nelze spustit další
                return;
            }

            NewControlThread(recalc: recalc);
            controlThread.Start(obj: null);
        }

        /// <summary>
        /// <para lang="cs">Vytvoří nové řídící vlákno pro výpočet úhrnů pomocné proměnné</para>
        /// <para lang="en">Creates new control thread for calculation auxiliary variable totals</para>
        /// </summary>
        /// <param name="recalc">
        /// <para lang="cs">Přepočítat existující úhrny pomocné proměnné ve výpočetních buňkách</para>
        /// <para lang="en">Recalculate existing auxiliary variable totals in estimation cells</para>
        /// </param>
        private void NewControlThread(bool recalc)
        {
            controlThread = new TotalsControlThread(
                controlOwner: this)
            {
                SelectedEstimationCellsIds = CheckedEstimationCellsIds,
                Recalc = recalc,
                ConfigCollection = Database.SAuxiliaryData.TConfigCollection[ConfigCollection.Id]
            };

            if (Setting.RefreshTime > 0)
            {
                controlThread.TimeElapsed += new TotalsControlThreadEventHandler(
                    (sender, e) =>
                    {
                        BeginInvoke(new Action(() =>
                        {
                            txtLog.Text = ControlThread.Log.Text;
                            prgCalcTotal.Value = ControlThread.Log.CellFinishedPerc;
                        }));
                    });
            }
            else
            {
                controlThread.CellFinished += new TotalsControlThreadEventHandler(
                    (sender, e) =>
                    {
                        BeginInvoke(new Action(() =>
                        {
                            txtLog.Text = ControlThread.Log.Text;
                            prgCalcTotal.Value = ControlThread.Log.CellFinishedPerc;
                        }));
                    });
            }

            controlThread.CellFailed += new TotalsControlThreadEventHandler(
                (sender, e) =>
                {
                    BeginInvoke(new Action(() =>
                    {
                        txtLog.Text = ControlThread.Log.Text;
                        prgCalcTotal.Value = ControlThread.Log.CellFinishedPerc;
                    }));
                });

            controlThread.ControlThreadStarted += new TotalsControlThreadEventHandler(
                (sender, e) =>
                {
                    BeginInvoke(new Action(() =>
                    {
                        StopWatch = new();
                        StopWatch.Start();

                        tvwEstimationCells.Enabled = false;
                        tlpFilters.Enabled = false;
                        grpAuxiliaryVariableTotals.Enabled = false;

                        txtLog.Text = ControlThread.Log.Text;
                    }));
                });

            controlThread.ControlThreadStopped += new TotalsControlThreadEventHandler(
                (sender, e) =>
                {
                    BeginInvoke(new Action(() =>
                    {
                        StopWatch?.Stop();

                        tvwEstimationCells.Enabled = true;
                        tlpFilters.Enabled = true;
                        grpAuxiliaryVariableTotals.Enabled = true;

                        txtLog.Text = ControlThread.Log.Text;
                        prgCalcTotal.Value = 100;

                        MessageBox.Show(
                                text: String.Concat(
                                        msgCalculationStopped,
                                        Environment.NewLine,
                                        msgCalculationTime
                                            .Replace(
                                                oldValue: "$1",
                                                newValue: $"{0.001 * StopWatch.ElapsedMilliseconds}")),
                                caption: String.Empty,
                                buttons: MessageBoxButtons.OK,
                                icon: MessageBoxIcon.Information);

                        GetAuxTotal();
                        ResetTreeViewNodeImageIndexes();

                        ControlThread.Log.Length = -1;
                        txtLog.Text = ControlThread.Log.Text;
                        prgCalcTotal.Value = 0;
                    }));
                });

            controlThread.ControlThreadFinished += new TotalsControlThreadEventHandler(
                (sender, e) =>
                {
                    BeginInvoke(new Action(() =>
                    {
                        StopWatch?.Stop();

                        tvwEstimationCells.Enabled = true;
                        tlpFilters.Enabled = true;
                        grpAuxiliaryVariableTotals.Enabled = true;

                        txtLog.Text = ControlThread.Log.Text;
                        prgCalcTotal.Value = 100;

                        MessageBox.Show(
                            text: String.Concat(
                                    msgCalculationCompleted,
                                    Environment.NewLine,
                                    msgCalculationTime
                                        .Replace(
                                            oldValue: "$1",
                                            newValue: $"{0.001 * StopWatch.ElapsedMilliseconds}")),
                            caption: String.Empty,
                            buttons: MessageBoxButtons.OK,
                            icon: MessageBoxIcon.Information);

                        GetAuxTotal();
                        ResetTreeViewNodeImageIndexes();

                        ControlThread.Log.Length = -1;
                        txtLog.Text = ControlThread.Log.Text;
                        prgCalcTotal.Value = 0;
                    }));
                });
        }

        #endregion Methods

    }

}