﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.AuxiliaryData;
using ZaJi.PostgreSQL;

namespace ZaJi.ModuleAuxiliaryData
{

    /// <summary>
    /// <para lang="cs">Ovládací prvek "Výběr konfigurace"</para>
    /// <para lang="en">Control "Configuration selection"</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    internal partial class ControlConfigurationSelector
        : UserControl, INfiEstaControl, IAuxiliaryDataControl
    {

        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Konfigurační funkce</para>
        /// <para lang="en">Config function</para>
        /// </summary>
        private ConfigFunction configFunction;

        /// <summary>
        /// <para lang="cs">Konfigurace</para>
        /// <para lang="en">Configuration</para>
        /// </summary>
        private Config configuration;

        private string strConfigurationVector = String.Empty;
        private string strConfigurationRaster = String.Empty;

        #endregion Private Fields


        #region Events

        /// <summary>
        /// <para lang="cs">Událost "Změna vybrané konfigurace"</para>
        /// <para lang="en">Event "Configuration changed"</para>
        /// </summary>
        public event EventHandler ConfigurationChanged;

        #endregion Events


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor ovládacího prvku</para>
        /// <para lang="en">Control constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        /// <param name="configFunction">
        /// <para lang="cs">Konfigurační funkce</para>
        /// <para lang="en">Config function</para>
        /// </param>
        public ControlConfigurationSelector(
            Control controlOwner,
            ConfigFunction configFunction)
        {
            InitializeComponent();
            Initialize(
                controlOwner: controlOwner,
                configFunction: configFunction);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                IAuxiliaryDataControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                return controlOwner;
            }
            set
            {
                IAuxiliaryDataControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables (read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((IAuxiliaryDataControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro pomocná data (read-only)</para>
        /// <para lang="en">Module for auxiliary data setting (read-only)</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((IAuxiliaryDataControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">Konfigurační funkce</para>
        /// <para lang="en">Config function</para>
        /// </summary>
        public ConfigFunction ConfigFunction
        {
            get
            {
                if (configFunction == null)
                {
                    throw new ArgumentNullException(
                        message: $"Argument {nameof(ConfigFunction)} must not be null.",
                        paramName: nameof(ConfigFunction));
                }

                return configFunction.Value switch
                {
                    ConfigFunctionEnum.VectorTotal => configFunction,
                    ConfigFunctionEnum.RasterTotal => configFunction,
                    _ =>
                        throw new ArgumentException(
                            message: $"Argument {nameof(ConfigFunction)} must not be vector or raster.",
                            paramName: nameof(ConfigFunction)),
                };
            }
            private set
            {
                if (value == null)
                {
                    throw new ArgumentNullException(
                        message: $"Argument {nameof(ConfigFunction)} must not be null.",
                        paramName: nameof(ConfigFunction));
                }

                switch (value.Value)
                {
                    case ConfigFunctionEnum.VectorTotal:
                        configFunction = value;
                        return;

                    case ConfigFunctionEnum.RasterTotal:
                        configFunction = value;
                        return;

                    default:
                        throw new ArgumentException(
                            message: $"Argument {nameof(ConfigFunction)} must not be vector or raster.",
                            paramName: nameof(ConfigFunction));
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Konfigurace</para>
        /// <para lang="en">Configuration</para>
        /// </summary>
        public Config Configuration
        {
            get
            {
                return configuration;
            }
            set
            {
                configuration = value;

                InitializeLabels();
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(btnCancel),                    "Zrušit výběr" },
                        { nameof(btnSelect),                    "Vybrat konfiguraci" },
                        { nameof(lblDescriptionCaption),        "Popis:" },
                        { nameof(lblIdCaption),                 "ID:" },
                        { nameof(lblLabelCaption),              "Název:" },
                        { nameof(strConfigurationVector),       "Konfigurace pro vektorovou vrstvu:" },
                        { nameof(strConfigurationRaster),       "Konfigurace pro rastrovou vrstvu:" }
                    }
                    : languageFile.NationalVersion.Data.TryGetValue(
                        key: nameof(ControlConfigurationSelector),
                        out Dictionary<string, string> dictNational)
                            ? dictNational
                            : [],

                LanguageVersion.International => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(btnCancel),                    "Cancel selection" },
                        { nameof(btnSelect),                    "Select configuration" },
                        { nameof(lblDescriptionCaption),        "Description:" },
                        { nameof(lblIdCaption),                 "ID:" },
                        { nameof(lblLabelCaption),              "Name:" },
                        { nameof(strConfigurationVector),       "Configuration for vector layer:" },
                        { nameof(strConfigurationRaster),       "Configuration for raster layer:" }
                    }
                    : languageFile.InternationalVersion.Data.TryGetValue(
                        key: nameof(ControlConfigurationSelector),
                        out Dictionary<string, string> dictInternational)
                            ? dictInternational
                            : [],

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku</para>
        /// <para lang="en">Initialization of the control</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        /// <param name="configFunction">
        /// <para lang="cs">Konfigurační funkce</para>
        /// <para lang="en">Config function</para>
        /// </param>
        private void Initialize(
            Control controlOwner,
            ConfigFunction configFunction)
        {
            BorderStyle = BorderStyle.None;
            ControlOwner = controlOwner;
            ConfigFunction = configFunction;

            btnCancel.Click += new EventHandler(
                (sender, e) =>
                {
                    Configuration = null;
                    ConfigurationChanged?.Invoke(sender: this, e: new EventArgs());
                });

            btnSelect.Click += new EventHandler(
               (sender, e) =>
               {
                   FormConfigurationSelector frmConfigurationSelector =
                        new(controlOwner: this, configFunction: ConfigFunction)
                        {
                            Configuration = Configuration
                        };

                   if (frmConfigurationSelector.ShowDialog() == DialogResult.OK)
                   {
                       Configuration = frmConfigurationSelector.Configuration;
                       ConfigurationChanged?.Invoke(sender: this, e: new EventArgs());
                   }
               });
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků ovládacího prvku</para>
        /// <para lang="en">Initialization of control labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            btnCancel.Text =
                labels.TryGetValue(key: nameof(btnCancel),
                out string btnCancelText)
                    ? btnCancelText
                    : String.Empty;

            btnSelect.Text =
                labels.TryGetValue(key: nameof(btnSelect),
                out string btnSelectText)
                    ? btnSelectText
                    : String.Empty;

            strConfigurationVector =
                labels.TryGetValue(key: nameof(strConfigurationVector),
                out strConfigurationVector)
                    ? strConfigurationVector
                    : String.Empty;

            strConfigurationRaster =
                labels.TryGetValue(key: nameof(strConfigurationRaster),
                out strConfigurationRaster)
                    ? strConfigurationRaster
                    : String.Empty;

            grpMain.Text =
                ConfigFunction.Value switch
                {
                    ConfigFunctionEnum.VectorTotal => strConfigurationVector,
                    ConfigFunctionEnum.RasterTotal => strConfigurationRaster,
                    _ => String.Empty,
                };

            lblIdCaption.Text =
                labels.TryGetValue(key: nameof(lblIdCaption),
                out string lblIdCaptionText)
                    ? lblIdCaptionText
                    : String.Empty;

            lblIdValue.Text = (Configuration?.Id != null)
                ? Configuration.Id.ToString()
                : String.Empty;

            lblLabelCaption.Text =
                labels.TryGetValue(key: nameof(lblLabelCaption),
                out string lblLabelCaptionText)
                    ? lblLabelCaptionText
                    : String.Empty;

            lblLabelValue.Text =
                (LanguageVersion == LanguageVersion.International)
                    ? Configuration?.LabelEn ?? String.Empty
                    : (LanguageVersion == LanguageVersion.National)
                        ? Configuration?.LabelCs ?? String.Empty
                        : Configuration?.LabelEn ?? String.Empty;

            lblDescriptionCaption.Text =
               labels.TryGetValue(key: nameof(lblDescriptionCaption),
               out string lblDescriptionCaptionText)
                   ? lblDescriptionCaptionText
                   : String.Empty;

            lblDescriptionValue.Text =
                (LanguageVersion == LanguageVersion.International)
                    ? Configuration?.DescriptionEn ?? String.Empty
                    : (LanguageVersion == LanguageVersion.National)
                        ? Configuration?.DescriptionCs ?? String.Empty
                        : Configuration?.DescriptionEn ?? String.Empty;
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent() { }

        #endregion Methods

    }
}