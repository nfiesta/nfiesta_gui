﻿////
//// Copyright 2020, 2024 ÚHÚL
////
//// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
//// You may not use this work except in compliance with the Licence.
//// You may obtain a copy of the Licence at:
////
//// https://joinup.ec.europa.eu/software/page/eupl
////
//// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
//// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//// See the Licence for the specific language governing permissions and limitations under the Licence.
////

//using System;
//using ZaJi.PostgreSQL;

//namespace ZaJi
//{
//    namespace ModuleAuxiliaryData
//    {

//        /// <summary>
//        /// Parametry udalosti ve vlakne
//        /// </summary>
//        internal class IntersectionsThreadEventArgs : EventArgs
//        {

//            #region Private Fields

//            /// <summary>
//            /// Pracovni vlakno, kde udalost vznikla
//            /// </summary>
//            private IntersectionsWorkerThread origin;

//            /// <summary>
//            /// Objekt chyby
//            /// </summary>
//            private Exception exception;

//            /// <summary>
//            /// Cas vzniku udalosti ve vlakne
//            /// </summary>
//            private readonly DateTime eventTime;

//            /// <summary>
//            /// Textova zprava o udalosti ve vlakne
//            /// </summary>
//            private string message;

//            #endregion Private Fields


//            #region Constructor

//            /// <summary>
//            /// Konstruktor objektu udalosti ve vlakne
//            /// </summary>
//            public IntersectionsThreadEventArgs()
//            {
//                this.origin = null;
//                this.exception = null;
//                this.eventTime = DateTime.Now;
//                this.message = String.Empty;
//            }

//            #endregion Constructor


//            #region Properties

//            /// <summary>
//            /// Pracovni vlakno, kde udalost vznikla
//            /// </summary>
//            public IntersectionsWorkerThread Origin
//            {
//                get
//                {
//                    return origin;
//                }
//                set
//                {
//                    origin = value;
//                }
//            }

//            /// <summary>
//            /// Objekt chyby
//            /// </summary>
//            public Exception Exception
//            {
//                get
//                {
//                    return exception;
//                }
//                set
//                {
//                    exception = value;
//                }
//            }

//            /// <summary>
//            /// Cas vzniku udalosti ve vlakne (read-only)
//            /// </summary>
//            public DateTime EventTime
//            {
//                get
//                {
//                    return eventTime;
//                }
//            }

//            /// <summary>
//            /// Textova zprava o udalosti ve vlakne
//            /// </summary>
//            public string Message
//            {
//                get
//                {
//                    return message;
//                }
//                set
//                {
//                    message = value;
//                }
//            }

//            #endregion Properties


//            #region Methods

//            /// <summary>
//            /// Textovy popis objektu udalosti ve vlakne
//            /// </summary>
//            /// <returns>Textovy popis objektu udalosti ve vlakne</returns>
//            public override string ToString()
//            {
//                switch (ControlAuxiliaryData.MainSetting.DBSetting.LanguageVersion)
//                {
//                    case LanguageVersion.National:
//                        return String.Concat($"Parametry události ve vlákně.");
//                    case LanguageVersion.International:
//                    default:
//                        return String.Concat($"Parameters of thread event.");
//                }
//            }

//            #endregion Methods

//        }

//    }
//}
