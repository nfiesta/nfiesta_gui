﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Runtime.Versioning;
using System.Threading;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.AuxiliaryData;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace ModuleAuxiliaryData
    {

        /// <summary>
        /// <para lang="cs">
        /// Delegát funkce obsluhující událost
        /// v řídícím vlákně
        /// </para>
        /// <para lang="en">
        /// Delegate of the function handling the event
        /// in the control thread
        /// </para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (TotalsWorkerThread)</para>
        /// <para lang="en">Object sending the event (TotalsWorkerThread)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události vlákna</para>
        /// <para lang="en">Thread event parameters</para>
        /// </param>
        internal delegate void TotalsControlThreadEventHandler(TotalsControlThread sender, TotalsThreadEventArgs e);

        /// <summary>
        /// <para lang="cs">Řídící vlákno pro výpočet úhrnů pomocné proměnné ve výpočetních buňkách</para>
        /// <para lang="en">Control thread for calculation of auxiliary variable totals in estimation cells</para>
        /// </summary>
        [SupportedOSPlatform("windows")]
        internal class TotalsControlThread
        {

            #region Private Fields

            /// <summary>
            /// <para lang="cs">Vlastník ovládacího prvku</para>
            /// <para lang="en">Control owner</para>
            /// </summary>
            private Control controlOwner;

            /// <summary>
            /// <para lang="cs">Seznam identifikátorů vybraných výpočetních buněk pro výpočet úhrnů pomocných proměnných</para>
            /// <para lang="en">List of selected estimation cells identifiers for calculation of auxiliary variable totals</para>
            /// </summary>
            public List<Nullable<int>> selectedEstimationCellsIds;

            /// <summary>
            /// <para lang="cs">Požadavek na přepočet úhrnů pomocných proměnných</para>
            /// <para lang="en">Request to recalculate auxiliary variable totats</para>
            /// </summary>
            private bool recalc;


            /// <summary>
            /// <para lang="cs">Seznam vybraných segmentů výpočetních buněk pro výpočet úhrnů pomocných proměnných</para>
            /// <para lang="en">List of selected estimation cell segments for calculation of auxiliary variable totals</para>
            /// </summary>
            private TFnGetGidsForAuxTotalAppList gidsForAuxTotal;

            /// <summary>
            /// <para lang="cs">Datová tabulka s výsledkem</para>
            /// <para lang="en">Data table with result</para>
            /// </summary>
            private DataTable result;

            /// <summary>
            /// <para lang="cs">Log s událostmi vláken</para>
            /// <para lang="en">Log with threads events</para>
            /// </summary>
            private AuxLog log;

            /// <summary>
            /// <para lang="cs">Provádí toto vlákno právě výpočet?</para>
            /// <para lang="en">Is this thread calculating auxiliary variable totals?</para>
            /// </summary>
            private bool isWorking;

            /// <summary>
            /// <para lang="cs">Požadavek na zastavení vlákna</para>
            /// <para lang="en">Request to stop thread</para>
            /// </summary>
            private bool stopRequest;


            /// <summary>
            /// <para lang="cs">Seznam pracovních vláken</para>
            /// <para lang="en">List of worker threads</para>
            /// </summary>
            private SortedList<int, TotalsWorkerThread> workerThreads;

            /// <summary>
            /// <para lang="cs">Metoda spuštěná vláknem</para>
            /// <para lang="en">Method that executes on thread</para>
            /// </summary>
            private ParameterizedThreadStart refThreadFunc;

            /// <summary>
            /// <para lang="cs">Objekt řídícího vlákna</para>
            /// <para lang="en">Control thread object</para>
            /// </summary>
            private Thread controlThread;

            /// <summary>
            /// <para lang="cs">Skupina konfigurací</para>
            /// <para lang="en">Configuration collection</para>
            /// </summary>
            private ConfigCollection configCollection;

            /// <summary>
            /// <para lang="cs">Časovač</para>
            /// <para lang="en">Timer</para>
            /// </summary>
            private System.Timers.Timer timer;

            #endregion Private Fields


            #region Constructor

            /// <summary>
            /// <para lang="cs">Konstruktor ovládacího prvku</para>
            /// <para lang="en">Control constructor</para>
            /// </summary>
            /// <param name="controlOwner">
            /// <para lang="cs">Vlastník ovládacího prvku</para>
            /// <para lang="en">Control owner</para>
            /// </param>
            public TotalsControlThread(Control controlOwner)
            {
                Initialize(controlOwner: controlOwner);
            }

            #endregion Constructor


            #region Common Properties

            /// <summary>
            /// <para lang="cs">Vlastník ovládacího prvku</para>
            /// <para lang="en">Control owner</para>
            /// </summary>
            public Control ControlOwner
            {
                get
                {
                    IAuxiliaryDataControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                    if (controlOwner is not FormCalcTotal)
                    {
                        throw new ArgumentException(
                            message: $"Argument {nameof(ControlOwner)} must be type of {nameof(FormCalcTotal)}.",
                            paramName: nameof(ControlOwner));
                    }

                    return controlOwner;
                }
                set
                {
                    IAuxiliaryDataControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                    if (value is not FormCalcTotal)
                    {
                        throw new ArgumentException(
                            message: $"Argument {nameof(ControlOwner)} must be type of {nameof(FormCalcTotal)}.",
                            paramName: nameof(ControlOwner));
                    }

                    controlOwner = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Databázové tabulky (read-only)</para>
            /// <para lang="en">Database tables (read-only)</para>
            /// </summary>
            public NfiEstaDB Database
            {
                get
                {
                    return ((INfiEstaControl)ControlOwner).Database;
                }
            }

            /// <summary>
            /// <para lang="cs">Jazyková verze (read-only)</para>
            /// <para lang="en">Language version (read-only)</para>
            /// </summary>
            public LanguageVersion LanguageVersion
            {
                get
                {
                    return ((INfiEstaControl)ControlOwner).LanguageVersion;
                }
            }

            /// <summary>
            /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
            /// <para lang="en">File with control labels (read-only)</para>
            /// </summary>
            public LanguageFile LanguageFile
            {
                get
                {
                    return ((IAuxiliaryDataControl)ControlOwner).LanguageFile;
                }
            }

            /// <summary>
            /// <para lang="cs">Nastavení modulu pro pomocná data (read-only)</para>
            /// <para lang="en">Module for auxiliary data setting (read-only)</para>
            /// </summary>
            public Setting Setting
            {
                get
                {
                    return ((IAuxiliaryDataControl)ControlOwner).Setting;
                }
            }

            #endregion Common Properties


            #region Properties

            /// <summary>
            /// <para lang="cs">Skupina konfigurací</para>
            /// <para lang="en">Configuration collection</para>
            /// </summary>
            public ConfigCollection ConfigCollection
            {
                get
                {
                    return configCollection;
                }
                set
                {
                    configCollection = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Aktuální verze databázové extenze (read-only)</para>
            /// <para lang="en">Current database extension version (read-only)</para>
            /// </summary>
            public ZaJi.NfiEstaPg.AuxiliaryData.ExtensionVersion ExtensionVersion
            {
                get
                {
                    return ((FormCalcTotal)ControlOwner).ExtensionVersion;
                }
            }

            /// <summary>
            /// <para lang="cs">Aktuální verze modulu pro pomocná data (read-only)</para>
            /// <para lang="en">Current module for auxiliary data version (read-only)</para>
            /// </summary>
            public ZaJi.NfiEstaPg.AuxiliaryData.GUIVersion GUIVersion
            {
                get
                {
                    return ((FormCalcTotal)ControlOwner).GUIVersion;
                }
            }

            /// <summary>
            /// <para lang="cs">Seznam identifikátorů vybraných výpočetních buněk pro výpočet úhrnů pomocných proměnných</para>
            /// <para lang="en">List of selected estimation cells identifiers for calculation of auxiliary variable totals</para>
            /// </summary>
            public List<Nullable<int>> SelectedEstimationCellsIds
            {
                get
                {
                    return selectedEstimationCellsIds ?? [];
                }
                set
                {
                    selectedEstimationCellsIds = value ?? [];
                }
            }

            /// <summary>
            /// <para lang="cs">Požadavek na přepočet úhrnů pomocných proměnných</para>
            /// <para lang="en">Request to recalculate auxiliary variable totats</para>
            /// </summary>
            public bool Recalc
            {
                get
                {
                    return recalc;
                }
                set
                {
                    recalc = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Seznam vybraných segmentů výpočetních buněk pro výpočet úhrnů pomocných proměnných</para>
            /// <para lang="en">List of selected estimation cell segments for calculation of auxiliary variable totals</para>
            /// </summary>
            public TFnGetGidsForAuxTotalAppList GidsForAuxTotal
            {
                get
                {
                    return gidsForAuxTotal;
                }
                private set
                {
                    gidsForAuxTotal = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Datová tabulka s výsledkem</para>
            /// <para lang="en">Data table with result</para>
            /// </summary>
            public DataTable Result
            {
                get
                {
                    return result;
                }
                private set
                {
                    result = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Log s událostmi vláken</para>
            /// <para lang="en">Log with threads events</para>
            /// </summary>
            public AuxLog Log
            {
                get
                {
                    return log;
                }
                private set
                {
                    log = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Provádí toto vlákno právě výpočet?</para>
            /// <para lang="en">Is this thread calculating auxiliary variable totals?</para>
            /// </summary>
            public bool IsWorking
            {
                get
                {
                    return isWorking;
                }
                private set
                {
                    isWorking = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Požadavek na zastavení vlákna</para>
            /// <para lang="en">Request to stop thread</para>
            /// </summary>
            public bool StopRequest
            {
                get
                {
                    return stopRequest;
                }
                private set
                {
                    stopRequest = value;
                }

            }

            #endregion Properties


            #region Methods

            /// <summary>
            /// <para lang="cs">Inicializace ovládacího prvku</para>
            /// <para lang="en">Initialization of the control</para>
            /// </summary>
            /// <param name="controlOwner">
            /// <para lang="cs">Vlastník ovládacího prvku</para>
            /// <para lang="en">Control owner</para>
            /// </param>
            private void Initialize(Control controlOwner)
            {
                ControlOwner = controlOwner;

                SelectedEstimationCellsIds = [];
                Recalc = false;
                ConfigCollection = null;

                GidsForAuxTotal = new TFnGetGidsForAuxTotalAppList(database: Database);

                Result = AuxTotalList.EmptyDataTable();

                Log = new AuxLog() { Length = Setting.LogLength };

                IsWorking = false;
                StopRequest = false;

                workerThreads = [];

                refThreadFunc = new ParameterizedThreadStart(BackgroundTask);

                controlThread = new Thread(start: refThreadFunc)
                {
                    IsBackground = true
                };
            }

            /// <summary>
            /// <para lang="cs">Spuštění výpočtu</para>
            /// <para lang="en">Start calculation</para>
            /// </summary>
            /// <param name="obj">
            /// <para lang="cs">Datový objekt pro použití metodou, kterou spouští vlákno</para>
            /// <para lang="en">An object that contains data to be used by the method the thread executes</para>
            /// </param>
            public void Start(object obj)
            {
                controlThread.Start(parameter: obj);
            }

            /// <summary>
            /// <para lang="cs">Zastavení výpočtu</para>
            /// <para lang="en">Stop calculation</para>
            /// </summary>
            public void Stop()
            {
                StopRequest = true;
                foreach (TotalsWorkerThread worker in workerThreads.Values)
                {
                    worker.Stop();
                }
            }

            /// <summary>
            /// <para lang="cs">Čeká na dokončení činnosti všech vláken</para>
            /// <para lang="en">Waits until all threads finish its work</para>
            /// </summary>
            public void Join()
            {
                foreach (TotalsWorkerThread worker in workerThreads.Values)
                {
                    worker.Join();
                }
            }

            /// <summary>
            /// <para lang="cs">Operace vykonávané řídícím vláknem</para>
            /// <para lang="en">Operations performed by the control thread</para>
            /// </summary>
            /// <param name="obj">
            /// <para lang="cs">Datový objekt pro použití metodou, kterou spouští vlákno</para>
            /// <para lang="en">An object that contains data to be used by the method the thread executes</para>
            /// </param>
            private void BackgroundTask(object obj)
            {
                // Vyvolá událost zahájení řídícího vlákna
                RaiseControlThreadStarted();

                if (Setting.RefreshTime > 0)
                {
                    timer = new(interval: Setting.RefreshTime);
                    timer.Elapsed += new System.Timers.ElapsedEventHandler(
                        (sender, e) => { TimeElapsed?.Invoke(sender: this, e: null); });
                    timer.Start();
                }

                // Inicializace objektů při opětovném spuštění
                GidsForAuxTotal = new TFnGetGidsForAuxTotalAppList(database: Database);
                Result = AuxTotalList.EmptyDataTable();

                // Výpočet pro každou konfiguraci ve skupině konfigurací
                foreach (Config configuration in ConfigCollection.Configurations)
                {
                    RaiseConfigurationStarted(configuration: configuration);

                    // Nahraje seznam gidů segmentů vybraných výpočetních buněk z databázové funkce
                    GidsForAuxTotal = ADFunctions.FnGetGidsForAuxTotalApp.Execute(
                            database: Database,
                            configId: configuration?.Id,
                            estimationCellIds: SelectedEstimationCellsIds,
                            extensionVersionId: ExtensionVersion?.Id,
                            guiVersionId: GUIVersion?.Id,
                            recalc: Recalc);

                    log.CellsTotal =
                        ConfigCollection.Configurations.Count *
                        gidsForAuxTotal.Items.Count;

                    foreach (int step in gidsForAuxTotal.Steps)
                    {
                        RaiseStepStarted(step: step);

                        // Vytvoří se nová skupina pracovních vláken pro daný krok
                        TFnGetGidsForAuxTotalAppList gidsInStep =
                            gidsForAuxTotal.GidsInStep(step: step);
                        NewWorkerThreads(gidsInStep: gidsInStep);

                        // Spustí pracovní vlákna ve skupině
                        foreach (TotalsWorkerThread worker in workerThreads.Values)
                        {
                            worker.Start();
                        }

                        // Řídící vlákno počká než všechna pracovní vlákna dokončí své úkoly v tomto kroce
                        // pak je spuštěn cyklus pro další krok
                        Join();

                        RaiseStepFinished(step: step);

                        if (StopRequest) break;
                    }

                    RaiseConfigurationFinished(configuration: configuration);

                    if (StopRequest) break;
                }

                if (Setting.RefreshTime > 0)
                {
                    timer.Stop();
                }

                if (StopRequest)
                {
                    RaiseControlThreadStopped();
                }
                else
                {
                    RaiseControlThreadFinished();
                }
            }

            /// <summary>
            /// <para lang="cs">Vytvoří novou skupinu pracovních vláken</para>
            /// <para lang="en">Creates new group of working threads</para>
            /// </summary>
            /// <param name="gidsInStep">
            /// <para lang="cs">Seznam segmentů výpočetních buněk pro výpočet úhrnů pomocných proměnných v jednom kroku</para>
            /// <para lang="en">List of estimation cell segments for calculation of auxiliary variable totals in one step</para>
            /// </param>
            private void NewWorkerThreads(
                TFnGetGidsForAuxTotalAppList gidsInStep)
            {
                workerThreads = [];

                for (int i = 1; i <= Setting.AuxiliaryDataThreadCount; i++)
                {
                    TFnGetGidsForAuxTotalAppList gidsInStepForThread =
                        gidsInStep.GidsForThread(
                            id: i,
                            countOfThreads: Setting.AuxiliaryDataThreadCount);

                    if (gidsInStepForThread.Data.Rows.Count > 0)
                    {
                        TotalsWorkerThread worker = new()
                        {
                            Id = i,
                            Connection = Database.Postgres,
                            GidsForAuxTotal = gidsInStepForThread,
                            LanguageVersion = LanguageVersion
                        };

                        worker.OnWorkerStart += new EventHandler(
                            (sender, e) => { RaiseWorkerThreadStarted(sender: sender, e: e); });

                        worker.OnWorkerStopped += new EventHandler(
                            (sender, e) => { RaiseWorkerThreadStopped(sender: sender, e: e); });

                        worker.OnConnectionException += new EventHandler(
                            (sender, e) => { });

                        worker.OnTaskStart += new EventHandler(
                           (sender, e) => { RaiseCellStarted(sender: sender, e: e); });

                        worker.OnTaskException += new EventHandler(
                            (sender, e) => { RaiseCellFailed(sender: sender, e: e); });

                        worker.OnTaskComplete += new EventHandler(
                            (sender, e) => { RaiseCellFinished(sender: sender, e: e); });

                        worker.OnWorkerComplete += new EventHandler(
                            (sender, e) => { RaiseWorkerThreadFinished(sender: sender, e: e); });

                        if (worker.Id != null)
                        {
                            workerThreads.Add(
                               key: (int)worker.Id,
                               value: worker);
                        }
                    }
                }
            }

            #endregion Methods


            #region Events and Event Handlers

            #region WorkerThreadStarted Event

            /// <summary>
            /// <para lang="cs">
            /// Událost
            /// ZAHÁJENÍ výpočtu úhrnu pomocné proměnné pro VŠECHNY segmenty výpočetních buněk
            /// v pracovním vlákně
            /// </para>
            /// <para lang="en">
            /// Event
            /// BEGIN calculation of the auxiliary variable totals for ALL segments of the estimation cells
            /// in the worker thread
            /// </para>
            /// </summary>
            public event TotalsControlThreadEventHandler WorkerThreadStarted;

            /// <summary>
            /// <para lang="cs">
            /// Vyvolání události
            /// ZAHÁJENÍ výpočtu úhrnu pomocné proměnné pro VŠECHNY segmenty výpočetních buněk
            /// v pracovním vlákně
            /// </para>
            /// <para lang="en">
            /// Invoking an event
            /// BEGIN calculation of the auxiliary variable totals for ALL segments of the estimation cells
            /// in the worker thread
            /// </para>
            /// </summary>
            /// <param name="sender">
            /// <para lang="cs">Object odesílající událost</para>
            /// <para lang="en">Sender</para>
            /// </param>
            /// <param name="e">
            /// <para lang="cs">Parametry události vlákna</para>
            /// <para lang="en">Thread event parameters</para>
            /// </param>
            private void RaiseWorkerThreadStarted(object sender, EventArgs e)
            {
                WorkerThreadStarted?.Invoke(
                    sender: this,
                    e: new TotalsThreadEventArgs(
                        sender: sender,
                        e: (ThreadEventArgs)e));
            }

            #endregion WorkerThreadStarted Event


            #region WorkerThreadStopped Event

            /// <summary>
            /// <para lang="cs">
            /// Událost
            /// ZASTAVENÍ výpočtu úhrnu pomocné proměnné pro VŠECHNY segmenty výpočetních buněk
            /// v pracovním vlákně
            /// </para>
            /// <para lang="en">
            /// Event
            /// STOP calculation of the auxiliary variable totals for ALL segments of the estimation cells
            /// in the worker thread
            /// </para>
            /// </summary>
            public event TotalsControlThreadEventHandler WorkerThreadStopped;

            /// <summary>
            /// <para lang="cs">
            /// Vyvolání události
            /// ZASTAVENÍ výpočtu úhrnu pomocné proměnné pro VŠECHNY segmenty výpočetních buněk
            /// v pracovním vlákně
            /// </para>
            /// <para lang="en">
            /// Invoking an event
            /// STOP calculation of the auxiliary variable totals for ALL segments of the estimation cells
            /// in the worker thread
            /// </para>
            /// </summary>
            /// <param name="sender">
            /// <para lang="cs">Object odesílající událost</para>
            /// <para lang="en">Sender</para>
            /// </param>
            /// <param name="e">
            /// <para lang="cs">Parametry události vlákna</para>
            /// <para lang="en">Thread event parameters</para>
            /// </param>
            private void RaiseWorkerThreadStopped(object sender, EventArgs e)
            {
                WorkerThreadStopped?.Invoke(
                    sender: this,
                    e: new TotalsThreadEventArgs(
                        sender: sender,
                        e: (ThreadEventArgs)e)); ;
            }

            #endregion WorkerThreadStopped Event


            #region WorkerThreadFinished Event

            /// <summary>
            /// <para lang="cs">
            /// Událost
            /// DOKONČENÍ výpočtu úhrnu pomocné proměnné pro VŠECHNY segmenty výpočetních buněk
            /// v pracovním vlákně
            /// </para>
            /// <para lang="en">
            /// Event
            /// COMPLETION of the calculation of the auxiliary variable totals for ALL segments of the estimation cells
            /// in the worker thread
            /// </para>
            /// </summary>
            public event TotalsControlThreadEventHandler WorkerThreadFinished;

            /// <summary>
            /// <para lang="cs">
            /// Vyvolání události
            /// DOKONČENÍ výpočtu úhrnu pomocné proměnné pro VŠECHNY segmenty výpočetních buněk
            /// v pracovním vlákně
            /// </para>
            /// <para lang="en">
            /// Invoking an event
            /// COMPLETION of the calculation of the auxiliary variable totals for ALL segments of the estimation cells
            /// in the worker thread
            /// </para>
            /// <param name="sender">
            /// <para lang="cs">Object odesílající událost</para>
            /// <para lang="en">Sender</para>
            /// </param>
            /// </summary>
            /// <param name="e">
            /// <para lang="cs">Parametry události vlákna</para>
            /// <para lang="en">Thread event parameters</para>
            /// </param>
            private void RaiseWorkerThreadFinished(object sender, EventArgs e)
            {
                WorkerThreadFinished?.Invoke(
                    sender: this,
                    e: new TotalsThreadEventArgs(
                        sender: sender,
                        e: (ThreadEventArgs)e));
            }

            #endregion WorkerThreadFinished Event


            #region CellStarted Event

            /// <summary>
            /// <para lang="cs">
            /// Událost
            /// ZAHÁJENÍ výpočtu úhrnu pomocné proměnné pro JEDEN segment výpočetní buňky
            /// v pracovním vlákně
            /// </para>
            /// <para lang="en">
            /// Event
            /// BEGIN calculation of the auxiliary variable totals for ONE segment of the estimation cell
            /// in the worker thread
            /// </para>
            /// </summary>
            public event TotalsControlThreadEventHandler CellStarted;

            /// <summary>
            /// <para lang="cs">
            /// Vyvolání události
            /// ZAHÁJENÍ výpočtu úhrnu pomocné proměnné pro JEDEN segment výpočetní buňky
            /// v pracovním vlákně
            /// </para>
            /// <para lang="en">
            /// Invoking an event
            /// BEGIN calculation of the auxiliary variable totals for ONE segment of the estimation cell
            /// in the worker thread
            /// </para>
            /// </summary>
            /// <param name="sender">
            /// <para lang="cs">Object odesílající událost</para>
            /// <para lang="en">Sender</para>
            /// </param>
            /// <param name="e">
            /// <para lang="cs">Parametry události vlákna</para>
            /// <para lang="en">Thread event parameters</para>
            /// </param>
            private void RaiseCellStarted(object sender, EventArgs e)
            {
                CellStarted?.Invoke(
                    sender: this,
                    e: new TotalsThreadEventArgs(
                        sender: sender,
                        e: (ThreadEventArgs)e));
            }

            #endregion CellStarted Event


            #region CellFinished Event

            /// <summary>
            /// <para lang="cs">
            /// Událost
            /// DOKONČENÍ výpočtu úhrnu pomocné proměnné pro JEDEN segment výpočetní buňky
            /// v pracovním vlákně
            /// </para>
            /// <para lang="en">
            /// Event
            /// COMPLETION calculation of the auxiliary variable totals for ONE segment of the estimation cell
            /// in the worker thread
            /// </para>
            /// </summary>
            public event TotalsControlThreadEventHandler CellFinished;

            /// <summary>
            /// <para lang="cs">
            /// Vyvolání události
            /// DOKONČENÍ výpočtu úhrnu pomocné proměnné pro JEDEN segment výpočetní buňky
            /// v pracovním vlákně
            /// </para>
            /// <para lang="en">
            /// Invoking an event
            /// COMPLETION calculation of the auxiliary variable totals for ONE segment of the estimation cell
            /// in the worker thread
            /// </para>
            /// </summary>
            /// <param name="sender">
            /// <para lang="cs">Object odesílající událost</para>
            /// <para lang="en">Sender</para>
            /// </param>
            /// <param name="e">
            /// <para lang="cs">Parametry události vlákna</para>
            /// <para lang="en">Thread event parameters</para>
            /// </param>
            private void RaiseCellFinished(object sender, EventArgs e)
            {
                TotalsThreadEventArgs eventArgs = new(
                    sender: sender,
                    e: (ThreadEventArgs)e);

                log.AddTotalsThreadEvent(
                    eType: AuxLogEventType.TotalsWorkerThreadCellFinished,
                    e: eventArgs);

                CellFinished?.Invoke(
                    sender: this,
                    e: eventArgs);
            }

            #endregion CellFinished Event


            #region CellFailed Event

            /// <summary>
            /// <para lang="cs">
            /// Událost
            /// CHYBA při výpočtu úhrnu pomocné proměnné pro JEDEN segment výpočetní buňky
            /// v pracovním vlákně
            /// </para>
            /// <para lang="en">
            /// Event
            /// ERROR in calculation of the auxiliary variable totals for ONE segment of the estimation cell
            /// in the worker thread
            /// </para>
            /// </summary>
            public event TotalsControlThreadEventHandler CellFailed;

            /// <summary>
            /// <para lang="cs">
            /// Vyvolání události
            /// CHYBA při výpočtu úhrnu pomocné proměnné pro JEDEN segment výpočetní buňky
            /// v pracovním vlákně
            /// </para>
            /// <para lang="en">
            /// Invoking an event
            /// ERROR in calculation of the auxiliary variable totals for ONE segment of the estimation cell
            /// in the worker thread
            /// </para>
            /// </summary>
            /// <param name="sender">
            /// <para lang="cs">Object odesílající událost</para>
            /// <para lang="en">Sender</para>
            /// </param>
            /// <param name="e">
            /// <para lang="cs">Parametry události vlákna</para>
            /// <para lang="en">Thread event parameters</para>
            /// </param>
            private void RaiseCellFailed(object sender, EventArgs e)
            {
                TotalsThreadEventArgs eventArgs = new(
                    sender: sender,
                    e: (ThreadEventArgs)e);

                log.AddTotalsThreadEvent(
                    eType: AuxLogEventType.TotalsWorkerThreadCellFailed,
                    e: eventArgs);

                CellFailed?.Invoke(
                    sender: this,
                    e: eventArgs);
            }

            #endregion CellFailed Event


            #region ControlThreadStarted Event

            /// <summary>
            /// <para lang="cs">
            /// Událost
            /// ZAHÁJENÍ činnosti řídícího vlákna pro výpočet úhrnů pomocných proměnných
            /// </para>
            /// <para lang="en">
            /// Event
            /// STARTING the control thread for calculating auxiliary variable totals
            /// </para>
            /// </summary>
            public event TotalsControlThreadEventHandler ControlThreadStarted;

            /// <summary>
            /// <para lang="cs">
            /// Vyvolání události
            /// ZAHÁJENÍ činnosti řídícího vlákna pro výpočet úhrnů pomocných proměnných
            /// </para>
            /// <para lang="en">
            /// Invoking an event
            /// STARTING the control thread for calculating auxiliary variable totals
            /// </para>
            /// </summary>
            private void RaiseControlThreadStarted()
            {
                IsWorking = true;
                StopRequest = false;

                TotalsThreadEventArgs e = new()
                {
                    Message = LanguageVersion switch
                    {
                        LanguageVersion.National =>
                            $"Řídící vlákno zahájilo činnost.",
                        LanguageVersion.International =>
                            $"Control thread has just started.",
                        _ =>
                            $"Control thread has just started."
                    }
                };

                ControlThreadStarted?.Invoke(sender: this, e: e);
            }

            #endregion ControlThreadStarted Event


            #region ControlThreadStopped Event

            /// <summary>
            /// <para lang="cs">
            /// Událost
            /// ZASTAVENÍ činnosti řídícího vlákna pro výpočet úhrnů pomocných proměnných
            /// </para>
            /// <para lang="en">
            /// Event
            /// STOP the control thread for calculating auxiliary variable totals
            /// </para>
            /// </summary>
            public event TotalsControlThreadEventHandler ControlThreadStopped;

            /// <summary>
            /// <para lang="cs">
            /// Vyvolání události
            /// ZASTAVENÍ činnosti řídícího vlákna pro výpočet úhrnů pomocných proměnných
            /// </para>
            /// <para lang="en">
            /// Invoking an event
            /// STOP the control thread for calculating auxiliary variable totals
            /// </para>
            /// </summary>
            private void RaiseControlThreadStopped()
            {
                TotalsThreadEventArgs e = new()
                {
                    Message = LanguageVersion switch
                    {
                        LanguageVersion.National =>
                            $"Řídící vlákno bylo zastaveno.",
                        LanguageVersion.International =>
                            $"Control thread has been stopped.",
                        _ =>
                            $"Control thread has been stopped.",
                    }
                };

                ControlThreadStopped?.Invoke(sender: this, e: e);

                IsWorking = false;
            }

            #endregion ControlThreadStopped Event


            #region ControlThreadFinished Event

            /// <summary>
            /// <para lang="cs">
            /// Událost
            /// DOKONČENÍ činnosti řídícího vlákna pro výpočet úhrnů pomocných proměnných
            /// </para>
            /// <para lang="en">
            /// Event
            /// COMPLETION the control thread for calculating auxiliary variable totals
            /// </para>
            /// </summary>
            public event TotalsControlThreadEventHandler ControlThreadFinished;

            /// <summary>
            /// <para lang="cs">
            /// Vyvolání události
            /// DOKONČENÍ činnosti řídícího vlákna pro výpočet úhrnů pomocných proměnných
            /// </para>
            /// <para lang="en">
            /// Invoking an event
            /// COMPLETION the control thread for calculating auxiliary variable totals
            /// </para>
            /// </summary>
            private void RaiseControlThreadFinished()
            {
                TotalsThreadEventArgs e = new()
                {
                    Message = LanguageVersion switch
                    {
                        LanguageVersion.National =>
                            $"VÝPOČET ÚHRNU POMOCNÉ PROMĚNNÉ BYL DOKONČEN.",
                        LanguageVersion.International =>
                            $"CALCULATION OF AUXILIARY VARIABLE TOTALS HAS BEEN DONE.",
                        _ =>
                            $"CALCULATION OF AUXILIARY VARIABLE TOTALS HAS BEEN DONE.",
                    }
                };

                ControlThreadFinished?.Invoke(sender: this, e: e);

                IsWorking = false;
            }

            #endregion ControlThreadFinished Event


            #region ConfigurationStarted Event

            /// <summary>
            /// <para lang="cs">
            /// Událost
            /// ZAHÁJENÍ výpočtu úhrnu pomocné proměnné pro JEDNU KONFIGURACI
            /// v řídícím vlákně
            /// </para>
            /// <para lang="en">
            /// Event
            /// BEGIN calculation of the auxiliary variable totals for ONE CONFIGURATION
            /// in the control thread
            /// </para>
            /// </summary>
            public event TotalsControlThreadEventHandler ConfigurationStarted;

            /// <summary>
            /// <para lang="cs">
            /// Vyvolání události
            /// ZAHÁJENÍ výpočtu úhrnu pomocné proměnné pro JEDNU KONFIGURACI
            /// v řídícím vlákně
            /// </para>
            /// <para lang="en">
            /// Invoking an event
            /// BEGIN calculation of the auxiliary variable totals for ONE CONFIGURATION
            /// in the control thread
            /// </para>
            /// </summary>
            /// <param name="configuration">
            /// <para lang="cs">Konfigurace</para>
            /// <para lang="en">Configuration</para>
            /// </param>
            private void RaiseConfigurationStarted(Config configuration)
            {
                TotalsThreadEventArgs e = new()
                {
                    Message = LanguageVersion switch
                    {
                        LanguageVersion.National => String.Concat(
                            $"ZAHÁJEN VÝPOČET ÚHRNů POMOCNÝCH PROMĚNNÝCH PRO VELIČINU ",
                            $"{configuration.Id} - {configuration.LabelCs}."),
                        LanguageVersion.International => String.Concat(
                            $"CALCULATION OF AUXILIARY VARIABLE TOTALS FOR THE VARIABLE ",
                            $"{configuration.Id} - {configuration.LabelEn} HAS STARTED."),
                        _ => String.Concat(
                            $"CALCULATION OF AUXILIARY VARIABLE TOTALS FOR THE VARIABLE ",
                            $"{configuration.Id} - {configuration.LabelEn} HAS STARTED."),
                    }
                };

                ConfigurationStarted?.Invoke(sender: this, e: e);
            }

            #endregion ConfigurationStarted Event


            #region ConfigurationFinished Event

            /// <summary>
            /// <para lang="cs">
            /// Událost
            /// DOKONČENÍ výpočtu úhrnu pomocné proměnné pro JEDNU KONFIGURACI
            /// v řídícím vlákně
            /// </para>
            /// <para lang="en">
            /// Event
            /// COMPLETION calculation of the auxiliary variable totals for ONE CONFIGURATION
            /// in the control thread
            /// </para>
            /// </summary>
            public event TotalsControlThreadEventHandler ConfigurationFinished;

            /// <summary>
            /// <para lang="cs">
            /// Vyvolání události
            /// DOKONČENÍ výpočtu úhrnu pomocné proměnné pro JEDNU KONFIGURACI
            /// v řídícím vlákně
            /// </para>
            /// <para lang="en">
            /// Invoking an event
            /// COMPLETION calculation of the auxiliary variable totals for ONE CONFIGURATION
            /// in the control thread
            /// </para>
            /// </summary>
            /// <param name="configuration">
            /// <para lang="cs">Konfigurace</para>
            /// <para lang="en">Configuration</para>
            /// </param>
            private void RaiseConfigurationFinished(Config configuration)
            {
                TotalsThreadEventArgs e = new()
                {
                    Message = LanguageVersion switch
                    {
                        LanguageVersion.National => String.Concat(
                            $"DOKONČEN VÝPOČET ÚHRNů POMOCNÝCH PROMĚNNÝCH PRO VELIČINU ",
                            $"{configuration.Id} - {configuration.LabelCs}."),
                        LanguageVersion.International => String.Concat(
                            $"CALCULATION OF AUXILIARY VARIABLE TOTALS FOR THE VARIABLE ",
                            $"{configuration.Id} - {configuration.LabelEn} HAS FINISHED."),
                        _ => String.Concat(
                            $"CALCULATION OF AUXILIARY VARIABLE TOTALS FOR THE VARIABLE ",
                            $"{configuration.Id} - {configuration.LabelEn} HAS FINISHED."),
                    }
                };

                ConfigurationFinished?.Invoke(sender: this, e: e);
            }

            #endregion ConfigurationFinished Event


            #region StepStarted Event

            /// <summary>
            /// <para lang="cs">
            /// Událost
            /// ZAHÁJENÍ výpočtu úhrnu pomocné proměnné pro JEDEN KROK
            /// v řídícím vlákně
            /// </para>
            /// <para lang="en">
            /// Event
            /// BEGIN calculation of the auxiliary variable totals for ONE STEP
            /// in the control thread
            /// </para>
            /// </summary>
            public event TotalsControlThreadEventHandler StepStarted;

            /// <summary>
            /// <para lang="cs">
            /// Vyvolání události
            /// ZAHÁJENÍ výpočtu úhrnu pomocné proměnné pro JEDEN KROK
            /// v řídícím vlákně
            /// </para>
            /// <para lang="en">
            /// Invoking an event
            /// BEGIN calculation of the auxiliary variable totals for ONE STEP
            /// in the control thread
            /// </para>
            /// </summary>
            /// <param name="step">
            /// <para lang="cs">Číslo kroku</para>
            /// <para lang="en">Step number</para>
            /// </param>
            private void RaiseStepStarted(int step)
            {
                TotalsThreadEventArgs e = new()
                {
                    Message = LanguageVersion switch
                    {
                        LanguageVersion.National =>
                            $"ZAHÁJEN VÝPOČET ÚHRNů POMOCNÝCH PROMĚNNÝCH V KROCE {step}.",
                        LanguageVersion.International =>
                            $"CALCULATION OF AUXILIARY VARIABLE TOTALS FOR STEP {step} HAS STARTED.",
                        _ =>
                            $"CALCULATION OF AUXILIARY VARIABLE TOTALS FOR STEP {step} HAS STARTED.",
                    }
                };

                StepStarted?.Invoke(sender: this, e: e);
            }

            #endregion StepStarted Event


            #region StepFinished Event

            /// <summary>
            /// <para lang="cs">
            /// Událost
            /// DOKONČENÍ výpočtu úhrnu pomocné proměnné pro JEDEN KROK
            /// v řídícím vlákně
            /// </para>
            /// <para lang="en">
            /// Event
            /// COMPLETION calculation of the auxiliary variable totals for ONE STEP
            /// in the control thread
            /// </para>
            /// </summary>
            public event TotalsControlThreadEventHandler StepFinished;

            /// <summary>
            /// <para lang="cs">
            /// Vyvolání události
            /// DOKONČENÍ výpočtu úhrnu pomocné proměnné pro JEDEN KROK
            /// v řídícím vlákně
            /// </para>
            /// <para lang="en">
            /// Invoking an event
            /// COMPLETION calculation of the auxiliary variable totals for ONE STEP
            /// in the control thread
            /// </para>
            /// </summary>
            /// <param name="step">
            /// <para lang="cs">Číslo kroku</para>
            /// <para lang="en">Step number</para>
            /// </param>
            private void RaiseStepFinished(int step)
            {
                TotalsThreadEventArgs e = new()
                {
                    Message = LanguageVersion switch
                    {
                        LanguageVersion.National =>
                            $"DOKONČEN VÝPOČET ÚHRNů POMOCNÝCH PROMĚNNÝCH V KROCE {step}.",
                        LanguageVersion.International =>
                            $"CALCULATION OF AUXILIARY VARIABLE TOTALS FOR STEP {step} HAS FINISHED.",
                        _ =>
                            $"CALCULATION OF AUXILIARY VARIABLE TOTALS FOR STEP {step} HAS FINISHED.",
                    }
                };

                StepFinished?.Invoke(sender: this, e: e);
            }

            #endregion StepFinished Event


            #region TimeElapsed Event

            /// <summary>
            /// <para lang="cs">
            /// Událost
            /// UPLYNUTÍ DOBY činnosti řídícího vlákna pro výpočet úhrnů pomocných proměnných
            /// </para>
            /// <para lang="en">
            /// Event
            /// ELAPSED TIME control thread activity for calculating auxiliary variable totals
            /// </para>
            /// </summary>
            public event TotalsControlThreadEventHandler TimeElapsed;

            #endregion TimeElapsed Event

            #endregion Events

        }

    }
}
