﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Linq;
using ZaJi.NfiEstaPg.AuxiliaryData;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace ModuleAuxiliaryData
    {

        /// <summary>
        /// <para lang="cs">Reklasifikační řetězec rastru</para>
        /// <para lang="en">Raster reclassification string</para>
        /// </summary>
        internal class RasterReclass
        {

            #region Private Fields

            /// <summary>
            /// <para lang="cs">Reklasifikační řetězec rastru</para>
            /// <para lang="en">Raster reclassification string</para>
            /// </summary>
            private string text;

            #endregion Private Fields


            #region Constructors

            /// <summary>
            /// <para lang="cs">Konstruktor</para>
            /// <para lang="en">Constructor</para>
            /// </summary>
            public RasterReclass()
            {
                Text = String.Empty;
            }

            /// <summary>
            /// <para lang="cs">Konstruktor</para>
            /// <para lang="en">Constructor</para>
            /// </summary>
            /// <param name="text">
            /// <para lang="cs">Reklasifikační řetězec rastru</para>
            /// <para lang="en">Raster reclassification string</para>
            /// </param>
            public RasterReclass(string text)
            {
                Text =
                    (String.IsNullOrEmpty(value: text)
                        ? String.Empty
                        : text)
                    .Trim();
            }

            /// <summary>
            /// <para lang="cs">Konstruktor</para>
            /// <para lang="en">Constructor</para>
            /// </summary>
            /// <param name="rasterCategories">
            /// <para lang="cs">Seznam kategorií rastru</para>
            /// <para lang="en">List of raster categories</para>
            /// </param>
            /// <param name="rasterValue">
            /// <para lang="cs">Zachovávaná kategorie po reklasifikaci rastru</para>
            /// <para lang="en">Maintained category after raster reclassification</para>
            /// </param>
            public RasterReclass(
                RasterCategoryList rasterCategories,
                Nullable<int> rasterValue)
            {
                if (rasterValue == null)
                {
                    Text = String.Empty;
                    return;
                }

                if (rasterCategories == null)
                {
                    Text = String.Empty;
                    return;
                }

                if (rasterCategories.Items.Count == 0)
                {
                    Text = String.Empty;
                    return;
                }

                Text =
                    rasterCategories.Items
                    .Select(a =>
                        (a.Id == (int)rasterValue)
                            ? $"{a.Id}:{1}"
                            : $"{a.Id}:{0}")
                    .Aggregate((a, b) => $"{a},{b}");
            }

            /// <summary>
            /// <para lang="cs">Konstruktor</para>
            /// <para lang="en">Constructor</para>
            /// </summary>
            /// <param name="rasterValue">
            /// <para lang="cs">Zachovávaná kategorie po reklasifikaci rastru</para>
            /// <para lang="en">Maintained category after raster reclassification</para>
            /// </param>
            public RasterReclass(
                Nullable<int> rasterValue)
            {
                if (rasterValue == null)
                {
                    Text = String.Empty;
                    return;
                }

                Text =
                    Enumerable.Range(start: 0, count: 16)
                    .Select(a =>
                        (a == (int)rasterValue)
                            ? $"{a}:{1}"
                            : $"{a}:{0}")
                    .Aggregate((a, b) => $"{a},{b}");
            }

            #endregion Constructors


            #region Properties

            /// <summary>
            /// <para lang="cs">Reklasifikační řetězec rastru (read-only)</para>
            /// <para lang="en">Raster reclassification string (read-only)</para>
            /// </summary>
            public string Text
            {
                get
                {
                    return text ?? String.Empty;
                }
                private set
                {
                    text = value ?? String.Empty;
                }
            }

            /// <summary>
            /// <para lang="cs">Zachovávaná kategorie po reklasifikaci rastru (read-only)</para>
            /// <para lang="en">Maintained category after raster reclassification (read-only)</para>
            /// </summary>
            public Nullable<int> RasterValue
            {
                get
                {
                    if (String.IsNullOrEmpty(value: Text))
                    {
                        return null;
                    }

                    char[] outerSeparator = [','];
                    char[] innerSeparator = [':'];

                    IEnumerable<string> categories =
                        Text.Split(
                            separator: outerSeparator,
                            options: StringSplitOptions.RemoveEmptyEntries)
                        .Where(a => a.Contains(value: innerSeparator[0]))
                        .Select(a => a.Trim());

                    return categories
                        .Select(a => new
                        {
                            LeftValue = Functions.ConvertToNInt(
                                text: a.Split(
                                        separator: innerSeparator,
                                        options: StringSplitOptions.None)[0].Trim(),
                                defaultValue: null),

                            RightValue = Functions.ConvertToNInt(
                                text: a.Split(
                                        separator: innerSeparator,
                                        options: StringSplitOptions.None)[1].Trim(),
                                defaultValue: null),
                        })
                        .Where(a =>
                            (a.LeftValue != null) &&
                            (a.RightValue != null) &&
                            (a.RightValue == 1))
                        .FirstOrDefault()?.LeftValue;
                }
            }

            /// <summary>
            /// <para lang="cs">Seznam identifikačních čísel kategorií rastru získaný z reklasifikačního řetězce (read-only)</para>
            /// <para lang="en">List of raster category identifiers obtained from the reclassification string (read-only)</para>
            /// </summary>
            public List<int> RasterCategories
            {
                get
                {
                    if (String.IsNullOrEmpty(value: Text))
                    {
                        return [];
                    }

                    char[] outerSeparator = [','];
                    char[] innerSeparator = [':'];

                    IEnumerable<string> categories =
                        Text.Split(
                            separator: outerSeparator,
                            options: StringSplitOptions.RemoveEmptyEntries)
                        .Where(a => a.Contains(value: innerSeparator[0]))
                        .Select(a => a.Trim());

                    return categories
                        .Select(a => new
                        {
                            LeftValue = Functions.ConvertToNInt(
                                text: a.Split(
                                        separator: innerSeparator,
                                        options: StringSplitOptions.None)[0].Trim(),
                                defaultValue: null),

                            RightValue = Functions.ConvertToNInt(
                                text: a.Split(
                                        separator: innerSeparator,
                                        options: StringSplitOptions.None)[1].Trim(),
                                defaultValue: null),
                        })
                        .Where(a =>
                            (a.LeftValue != null) &&
                            (a.RightValue != null))
                        .Select(a => (int)a.LeftValue)
                        .ToList<int>();
                }
            }

            #endregion Properties


            #region Methods

            /// <summary>
            /// <para lang="cs">Je reklasifikační řetězec rastru prázdný?</para>
            /// <para lang="en">Is raster reclassification string empty?</para>
            /// </summary>
            /// <returns>
            /// <para lang="cs">ano|ne</para>
            /// <para lang="en">yes|no</para>
            /// </returns>
            public bool IsEmpty()
            {
                return String.IsNullOrEmpty(value: Text);
            }

            /// <summary>
            /// <para lang="cs">Sestaví celý příkaz pro reklasifikaci rastru</para>
            /// <para lang="en">Builds the entire command to reclassify the raster</para>
            /// </summary>
            /// <param name="rast">
            /// <para lang="cs">Jméno sloupce s rastrem</para>
            /// <para lang="en">Column name with raster</para>
            /// </param>
            /// <param name="band">
            /// <para lang="cs">Číslo pásma rastru</para>
            /// <para lang="en">Raster band number</para>
            /// </param>
            /// <param name="pixelType">
            /// <para lang="cs">pixelType</para>
            /// <para lang="en">pixelType</para>
            /// </param>
            /// <param name="noDataVal">
            /// <para lang="cs">noDataVal</para>
            /// <para lang="en">noDataVal</para>
            /// </param>
            /// <returns>
            /// <para lang="cs">Příkaz pro reklasifikaci rastru</para>
            /// <para lang="en">Command to reclassify the raster</para>
            /// </returns>
            public string Command(
                string rast,
                Nullable<int> band,
                string pixelType = "4BUI",
                Nullable<double> noDataVal = 255)
            {
                if (String.IsNullOrEmpty(value: rast))
                {
                    return String.Empty;
                }

                if (band == null)
                {
                    return String.Empty;
                }

                if (RasterValue == null)
                {
                    return String.Empty;
                }

                if (String.IsNullOrEmpty(value: Text))
                {
                    return String.Empty;
                }

                if (String.IsNullOrEmpty(value: pixelType))
                {
                    return String.Empty;
                }

                string strNoDataVal =
                    (noDataVal != null)
                        ? noDataVal.ToString()
                        : Functions.StrNull;
                return
                    $"ST_Reclass({rast}, {band}, '{Text}', '{pixelType}', {strNoDataVal})";
            }

            /// <summary>
            /// <para lang="cs">Vrací řetězec, který představuje aktuální objekt</para>
            /// <para lang="en">Returns the string that represents current object</para>
            /// </summary>
            /// <returns>
            /// <para lang="cs">Vrací řetězec, který představuje aktuální objekt</para>
            /// <para lang="en">Returns the string that represents current object</para>
            /// </returns>
            public override string ToString()
            {
                return Text;
            }

            #endregion Methods

        }

    }
}
