﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.AuxiliaryData;
using ZaJi.PostgreSQL;

namespace ZaJi.ModuleAuxiliaryData
{

    /// <summary>
    /// <para lang="cs">Ovládací prvek "Výběr pásma rastru"</para>
    /// <para lang="en">Control "Selecting raster band"</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    internal partial class ControlRasterBand
        : UserControl, INfiEstaControl, IAuxiliaryDataControl
    {

        #region Constants

        private const int ImageRedBall = 0;
        private const int ImageBlueBall = 1;
        private const int ImageYellowBall = 2;
        private const int ImageGreenBall = 3;
        private const int ImageGrayBall = 4;
        private const int ImageLock = 5;
        private const int ImageSchema = 6;
        private const int ImageVector = 7;
        private const int ImageRaster = 8;

        #endregion Constants


        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        private Control controlOwner;

        #endregion Private Fields


        #region Events

        /// <summary>
        /// <para lang="cs">Událost kliknutí na tlačítko "Předchozí"</para>
        /// <para lang="en">Click event on the "Previous" button</para>
        /// </summary>
        public event EventHandler PreviousClick;

        /// <summary>
        /// <para lang="cs">Událost kliknutí na tlačítko "Další"</para>
        /// <para lang="en">Click event on the "Next" button</para>
        /// </summary>
        public event EventHandler NextClick;

        #endregion Events


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor ovládacího prvku</para>
        /// <para lang="en">Control constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        public ControlRasterBand(Control controlOwner)
        {
            InitializeComponent();
            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                IAuxiliaryDataControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                if (controlOwner is not FormConfigurationGuide)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(FormConfigurationGuide)}.",
                        paramName: nameof(ControlOwner));
                }

                return controlOwner;
            }
            set
            {
                IAuxiliaryDataControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                if (value is not FormConfigurationGuide)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(FormConfigurationGuide)}.",
                        paramName: nameof(ControlOwner));
                }

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables (read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((IAuxiliaryDataControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro pomocná data (read-only)</para>
        /// <para lang="en">Module for auxiliary data setting (read-only)</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((IAuxiliaryDataControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">Konfigurace (read-only)</para>
        /// <para lang="en">Configuration (read-only)</para>
        /// </summary>
        public Config Configuration
        {
            get
            {
                return ((FormConfigurationGuide)ControlOwner).Configuration;
            }
        }

        /// <summary>
        /// <para lang="cs">Vybrané pásmo rastru (read-only)</para>
        /// <para lang="en">Selected raster band (read-only)</para>
        /// </summary>
        private RasterBand SelectedRasterBand
        {
            get
            {
                if ((tvwRasterBands.SelectedNode == null) ||
                    (tvwRasterBands.SelectedNode.Tag == null) ||
                    (tvwRasterBands.SelectedNode.Tag is not RasterBand))
                {
                    return null;
                }

                return (RasterBand)tvwRasterBands.SelectedNode.Tag;
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(btnPrevious),                  "Předchozí" },
                        { nameof(btnNext),                      "Další" },
                        { nameof(grpSelectedRasterBand),        "Vybrané pásmo rastru:" },
                        { nameof(grpRasterBands),               "Pásma rastru:" },
                        { nameof(lblEmptyRasterBands),          String.Concat(
                                                                "Pro zadanou rastrovou vrstvu nebyl nalezen číselník $1.$2 ",
                                                                "s popisem pásem rastru nebo je prázdný.") }
                    }
                    : languageFile.NationalVersion.Data.TryGetValue(
                        key: nameof(ControlRasterBand),
                        out Dictionary<string, string> dictNational)
                            ? dictNational
                            : [],

                LanguageVersion.International => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(btnPrevious),                  "Previous" },
                        { nameof(btnNext),                      "Next" },
                        { nameof(grpSelectedRasterBand),        "Selected raster band:" },
                        { nameof(grpRasterBands),               "Raster bands:" },
                        { nameof(lblEmptyRasterBands),          String.Concat(
                                                                "Lookup table $1.$2 with raster band description has not been found ",
                                                                "for selected raster layer or is empty.") }
                    }
                    : languageFile.InternationalVersion.Data.TryGetValue(
                        key: nameof(ControlRasterBand),
                        out Dictionary<string, string> dictInternational)
                            ? dictInternational
                            : [],

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku</para>
        /// <para lang="en">Initialization of the control</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        private void Initialize(Control controlOwner)
        {
            BorderStyle = BorderStyle.None;
            ControlOwner = controlOwner;

            InitializeLabels();
            EnableNext();

            btnPrevious.Click += new EventHandler(
                (sender, e) =>
                {
                    PreviousClick?.Invoke(sender: sender, e: e);
                });

            btnNext.Click += new EventHandler(
               (sender, e) =>
               {
                   NextClick?.Invoke(sender: sender, e: e);
               });

            tvwRasterBands.AfterSelect += new TreeViewEventHandler(
                (sender, e) =>
                {
                    Configuration.Band = SelectedRasterBand?.Id;

                    lblSelectedRasterBand.Text =
                        (LanguageVersion == LanguageVersion.International)
                            ? SelectedRasterBand?.ExtendedLabelEn ?? String.Empty
                            : (LanguageVersion == LanguageVersion.National)
                                ? SelectedRasterBand?.ExtendedLabelCs ?? String.Empty
                                : SelectedRasterBand?.ExtendedLabelEn ?? String.Empty;

                    EnableNext();
                });
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků ovládacího prvku</para>
        /// <para lang="en">Initialization of control labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            btnPrevious.Text =
                labels.TryGetValue(key: nameof(btnPrevious),
                out string btnPreviousText)
                    ? btnPreviousText
                    : String.Empty;

            btnNext.Text =
                labels.TryGetValue(key: nameof(btnNext),
                out string btnNextText)
                    ? btnNextText
                    : String.Empty;

            grpRasterBands.Text =
                labels.TryGetValue(key: nameof(grpRasterBands),
                out string grpRasterBandsText)
                    ? grpRasterBandsText
                    : String.Empty;

            grpSelectedRasterBand.Text =
                labels.TryGetValue(key: nameof(grpSelectedRasterBand),
                out string grpSelectedRasterBandText)
                    ? grpSelectedRasterBandText
                    : String.Empty;

            lblSelectedRasterBand.Text =
                (LanguageVersion == LanguageVersion.International)
                    ? SelectedRasterBand?.ExtendedLabelEn ?? String.Empty
                    : (LanguageVersion == LanguageVersion.National)
                        ? SelectedRasterBand?.ExtendedLabelCs ?? String.Empty
                        : SelectedRasterBand?.ExtendedLabelEn ?? String.Empty;

            foreach (TreeNode nodeRasterBand in tvwRasterBands.Nodes.OfType<TreeNode>()
                .Where(a => a.Tag != null)
                .Where(a => a.Tag is RasterBand))
            {
                nodeRasterBand.Text = (LanguageVersion == LanguageVersion.International)
                        ? ((RasterBand)nodeRasterBand.Tag).ExtendedLabelEn
                        : (LanguageVersion == LanguageVersion.National)
                            ? ((RasterBand)nodeRasterBand.Tag).ExtendedLabelCs
                            : ((RasterBand)nodeRasterBand.Tag).ExtendedLabelEn;
            }
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent()
        {
            tvwRasterBands.Nodes.Clear();

            RasterBandList.SetName(
                schemaName: Configuration?.ConfigCollection?.SchemaName ?? String.Empty,
                tableName: Configuration?.ConfigCollection?.TableName ?? String.Empty);

            RasterBandList rasterBandList = new(database: Database);

            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            if (Database.Postgres.Catalog.Schemas[RasterBandList.Schema] == null)
            {
                // Schéma číselníku pásem rastru neexistuje
                lblEmptyRasterBands.Text =
                    labels.TryGetValue(key: nameof(lblEmptyRasterBands),
                    out string lblEmptyRasterBandsText)
                        ? lblEmptyRasterBandsText
                            .Replace(oldValue: "$1", newValue: RasterBandList.Schema ?? String.Empty)
                            .Replace(oldValue: "$2", newValue: RasterBandList.Name ?? String.Empty)
                        : String.Empty;

                tvwRasterBands.Visible = false;
                lblEmptyRasterBands.Visible = true;

                EnableNext();
                return;
            }

            if ((Database.Postgres.Catalog.Schemas[RasterBandList.Schema].Tables[RasterBandList.Name] == null) &&
                (Database.Postgres.Catalog.Schemas[RasterBandList.Schema].ForeignTables[RasterBandList.Name] == null))
            {
                // Tabulka číselníku pásem rastru neexistuje
                lblEmptyRasterBands.Text =
                    labels.TryGetValue(key: nameof(lblEmptyRasterBands),
                    out string lblEmptyRasterBandsText)
                        ? lblEmptyRasterBandsText
                            .Replace(oldValue: "$1", newValue: RasterBandList.Schema ?? String.Empty)
                            .Replace(oldValue: "$2", newValue: RasterBandList.Name ?? String.Empty)
                        : String.Empty;

                tvwRasterBands.Visible = false;
                lblEmptyRasterBands.Visible = true;

                EnableNext();
                return;
            }

            rasterBandList.ReLoad();

            if (rasterBandList.Data.Rows.Count == 0)
            {
                // Číselník pásem rastru je prázdný
                lblEmptyRasterBands.Text =
                    labels.TryGetValue(key: nameof(lblEmptyRasterBands),
                    out string lblEmptyRasterBandsText)
                        ? lblEmptyRasterBandsText
                            .Replace(oldValue: "$1", newValue: RasterBandList.Schema ?? String.Empty)
                            .Replace(oldValue: "$2", newValue: RasterBandList.Name ?? String.Empty)
                        : String.Empty;

                tvwRasterBands.Visible = false;
                lblEmptyRasterBands.Visible = true;

                EnableNext();
                return;
            }

            IEnumerable<RasterBand> rasterBands =
                (LanguageVersion == LanguageVersion.International)
                    ? rasterBandList.Items.OrderBy(a => a.ExtendedLabelEn)
                    : (LanguageVersion == LanguageVersion.National)
                        ? rasterBandList.Items.OrderBy(a => a.ExtendedLabelCs)
                        : rasterBandList.Items.OrderBy(a => a.ExtendedLabelEn);

            foreach (RasterBand rasterBand in rasterBands)
            {
                TreeNode nodeRasterBand = new()
                {
                    ImageIndex = ImageGreenBall,
                    Name = rasterBand.Id.ToString(),
                    SelectedImageIndex = ImageRedBall,
                    Tag = rasterBand,
                    Text = (LanguageVersion == LanguageVersion.International)
                        ? rasterBand.ExtendedLabelEn
                        : (LanguageVersion == LanguageVersion.National)
                            ? rasterBand.ExtendedLabelCs
                            : rasterBand.ExtendedLabelEn
                };
                tvwRasterBands.Nodes.Add(node: nodeRasterBand);
            }

            if (Configuration.Band != null)
            {
                SelectNode(id: (int)Configuration.Band);
            }
            else
            {
                SelectFirstNode();
            }

            tvwRasterBands.Visible = true;
            lblEmptyRasterBands.Visible = false;

            EnableNext();
        }

        /// <summary>
        /// <para lang="cs">Vybere první uzel v TreeView</para>
        /// <para lang="en">Selects first node in TreeView</para>
        /// </summary>
        private void SelectFirstNode()
        {
            if (tvwRasterBands.Nodes.Count == 0)
            {
                return;
            }
            tvwRasterBands.SelectedNode = tvwRasterBands.Nodes[0];
        }

        /// <summary>
        /// <para lang="cs">
        /// Vybere uzel v TreeView odpovídající identifikačnímu číslu pásma rastru
        /// </para>
        /// <para lang="en">
        /// Selects the TreeView node corresponding to raster band identifier
        /// </para>
        /// </summary>
        /// <param name="id">
        /// <para lang="cs">Identifikační číslo pásma rastru</para>
        /// <para lang="en">Raster band identifier</para>
        /// </param>
        private void SelectNode(int id)
        {
            TreeNode node = FindNode(id: id);
            if (node == null)
            {
                return;
            }
            tvwRasterBands.SelectedNode = node;
        }

        /// <summary>
        /// <para lang="cs">
        /// Vrátí uzel z TreeView odpovídající identifikačnímu číslu pásma rastru
        /// </para>
        /// <para lang="en">
        /// Returns the TreeView node corresponding to the raster band identifier
        /// </para>
        /// </summary>
        /// <param name="id">
        /// <para lang="cs">Identifikační číslo pásma rastru</para>
        /// <para lang="en">Raster band identifier</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">
        /// Vrátí uzel z TreeView odpovídající identifikačnímu číslu pásma rastru
        /// </para>
        /// <para lang="en">
        /// Returns the TreeView node corresponding to the raster band identifier
        /// </para>
        /// </returns>
        private TreeNode FindNode(int id)
        {
            return
                tvwRasterBands.Nodes.OfType<TreeNode>()
                .Where(a => a.Tag != null)
                .Where(a => a.Tag is RasterBand)
                .Where(a => ((RasterBand)a.Tag).Id == id)
                .FirstOrDefault<TreeNode>();
        }

        /// <summary>
        /// <para lang="cs">Povolí postup na další formulář</para>
        /// <para lang="en">Allows progression to the next form</para>
        /// </summary>
        private void EnableNext()
        {
            if (Configuration == null)
            {
                btnNext.Enabled = false;
                return;
            }

            btnNext.Enabled =
                !String.IsNullOrEmpty(Configuration.LabelCs) &&
                !String.IsNullOrEmpty(Configuration.LabelEn) &&
                !String.IsNullOrEmpty(Configuration.DescriptionCs) &&
                !String.IsNullOrEmpty(Configuration.DescriptionEn) &&
                (Configuration.ConfigQueryValue != ConfigQueryEnum.Unknown) &&
                Configuration.Band != null;
        }

        #endregion Methods

    }

}