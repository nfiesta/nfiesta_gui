﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.AuxiliaryData;
using ZaJi.PostgreSQL;

namespace ZaJi.ModuleAuxiliaryData
{

    /// <summary>
    /// <para lang="cs">Ovládací prvek "Výběr seznamu konfigurací pro kategorie pomocné proměnné"</para>
    /// <para lang="en">Control "Selecting list of configurations for auxiliary variable categories"</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    internal partial class ControlCategory
        : UserControl, INfiEstaControl, IAuxiliaryDataControl
    {

        #region Constants

        private const int LevelConfigFunction = 0;
        private const int LevelConfigCollection = 1;
        private const int LevelConfiguration = 2;

        private const int ImageRedBall = 0;
        private const int ImageBlueBall = 1;
        private const int ImageYellowBall = 2;
        private const int ImageGreenBall = 3;
        private const int ImageGrayBall = 4;
        private const int ImageLock = 5;
        private const int ImageSchema = 6;
        private const int ImageVector = 7;
        private const int ImageRaster = 8;

        #endregion Constants


        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Uzly v TreeView</para>
        /// <para lang="en">TreeView nodes</para>
        /// </summary>
        private List<TreeNode> nodes;

        /// <summary>
        /// <para lang="cs">Indikátor editace ovládacího prvku</para>
        /// <para lang="en">Control editing indicator</para>
        /// </summary>
        private bool onEdit;

        /// <summary>
        /// <para lang="cs">Určují vybrané konfigurace rozlohu kategorií celku?</para>
        /// <para lang="en">Do selected configurations determine the complete area of categories:"</para>
        /// </summary>
        private bool isComplete;

        private string strConfsForSum = String.Empty;
        private string strConfForComplementToEstCellArea = String.Empty;
        private string strConfForComplete = String.Empty;
        private string strConfForComplementToComplete = String.Empty;
        private string strConfForLink = String.Empty;

        #endregion Private Fields


        #region Events

        /// <summary>
        /// <para lang="cs">Událost kliknutí na tlačítko "Předchozí"</para>
        /// <para lang="en">Click event on the "Previous" button</para>
        /// </summary>
        public event EventHandler PreviousClick;

        /// <summary>
        /// <para lang="cs">Událost kliknutí na tlačítko "Další"</para>
        /// <para lang="en">Click event on the "Next" button</para>
        /// </summary>
        public event EventHandler NextClick;

        #endregion Events


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor ovládacího prvku</para>
        /// <para lang="en">Control constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        public ControlCategory(Control controlOwner)
        {
            InitializeComponent();
            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                IAuxiliaryDataControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                if (controlOwner is not FormConfigurationGuide)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(FormConfigurationGuide)}.",
                        paramName: nameof(ControlOwner));
                }

                return controlOwner;
            }
            set
            {
                IAuxiliaryDataControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                if (value is not FormConfigurationGuide)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(FormConfigurationGuide)}.",
                        paramName: nameof(ControlOwner));
                }

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables (read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((IAuxiliaryDataControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro pomocná data (read-only)</para>
        /// <para lang="en">Module for auxiliary data setting (read-only)</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((IAuxiliaryDataControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">Konfigurace (read-only)</para>
        /// <para lang="en">Configuration (read-only)</para>
        /// </summary>
        private Config Configuration
        {
            get
            {
                return ((FormConfigurationGuide)ControlOwner).Configuration;
            }
        }

        /// <summary>
        /// <para lang="cs">Určují vybrané konfigurace rozlohu kategorií celku?</para>
        /// <para lang="en">Do selected configurations determine the complete area of categories:"</para>
        /// </summary>
        public bool IsComplete
        {
            get
            {
                return isComplete;
            }
            set
            {
                isComplete = value;
                InitializeLabels();
            }
        }

        /// <summary>
        /// <para lang="cs">Indikátor editace ovládacího prvku</para>
        /// <para lang="en">Control editing indicator</para>
        /// </summary>
        private bool OnEdit
        {
            get
            {
                return onEdit;
            }
            set
            {
                onEdit = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Uzly v TreeView</para>
        /// <para lang="en">TreeView nodes</para>
        /// </summary>
        private List<TreeNode> Nodes
        {
            get
            {
                return nodes ?? [];
            }
            set
            {
                nodes = value ?? [];
            }
        }

        /// <summary>
        /// <para lang="cs">Uzly v TreeView úroveň výpočetních funkcí (read-only)</para>
        /// <para lang="en">TreeView nodes - ConfigFunctions level (read-only)</para>
        /// </summary>
        private IEnumerable<TreeNode> ConfigFunctionNodes
        {
            get
            {
                return
                    Nodes
                        .Where(a => a.Level == LevelConfigFunction)
                        .Where(a => a.Tag != null)
                        .Where(a => a.Tag is ConfigFunction);
            }
        }

        /// <summary>
        /// <para lang="cs">Uzly v TreeView úroveň skupin konfigurací (read-only)</para>
        /// <para lang="en">TreeView nodes - ConfigCollections level (read-only)</para>
        /// </summary>
        private IEnumerable<TreeNode> ConfigCollectionNodes
        {
            get
            {
                return
                    Nodes
                        .Where(a => a.Level == LevelConfigCollection)
                        .Where(a => a.Tag != null)
                        .Where(a => a.Tag is ConfigCollection);
            }
        }

        /// <summary>
        /// <para lang="cs">Uzly v TreeView úroveň konfigurací (read-only)</para>
        /// <para lang="en">TreeView nodes - Configurations level (read-only)</para>
        /// </summary>
        private IEnumerable<TreeNode> ConfigurationNodes
        {
            get
            {
                return
                    Nodes
                        .Where(a => a.Level == LevelConfiguration)
                        .Where(a => a.Tag != null)
                        .Where(a => a.Tag is Config);
            }
        }

        /// <summary>
        /// <para lang="cs">Zaškrtnuté uzly konfigurací (read-only)</para>
        /// <para lang="en">Checked configuration nodes (read-only)</para>
        /// </summary>
        private IEnumerable<TreeNode> CheckedConfigurationNodes
        {
            get
            {
                return
                    ConfigurationNodes
                        .Where(a => a.Checked);
            }
        }

        /// <summary>
        /// <para lang="cs">Zaškrtnuté konfigurace (read-only)</para>
        /// <para lang="en">Checked configurations (read-only)</para>
        /// </summary>
        private IEnumerable<Config> CheckedConfigurations
        {
            get
            {
                return CheckedConfigurationNodes
                    .Select(a => (Config)a.Tag);
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(btnPrevious),                                "Předchozí" },
                        { nameof(btnNext),                                    "Další" },
                        { nameof(grpConfigurations),                          "Seznam konfigurací:" },
                        { nameof(grpSelectedConfigurations),                  "Vybrané konfigurace:" },
                        { nameof(strConfsForSum),                             "Vybrané konfigurace určující součet rozlohy kategorií pomocných proměnných:" },
                        { nameof(strConfForComplementToEstCellArea),          "Vybrané konfigurace určující doplněk do rozlohy výpočetní buňky:" },
                        { nameof(strConfForComplete),                         "Vybrané konfigurace určující rozlohu kategorií celku:" },
                        { nameof(strConfForComplementToComplete),             "Vybrané konfigurace určující doplněk do rozlohy kategorií celku:" },
                        { nameof(strConfForLink),                             "Odkaz na stávající konfiguraci:" }
                    }
                    : languageFile.NationalVersion.Data.TryGetValue(
                        key: nameof(ControlCategory),
                        out Dictionary<string, string> dictNational)
                            ? dictNational
                            : [],

                LanguageVersion.International => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(btnPrevious),                                "Previous" },
                        { nameof(btnNext),                                    "Next" },
                        { nameof(grpConfigurations),                          "List of configurations:" },
                        { nameof(grpSelectedConfigurations),                  "Selected configurations:" },
                        { nameof(strConfsForSum),                             "Selected configurations determining the sum of auxiliary variable categories area:"},
                        { nameof(strConfForComplementToEstCellArea),          "Selected configurations determining the complement to the area of estimation cell:" },
                        { nameof(strConfForComplete),                         "Selected configurations determining the complete area of categories:" },
                        { nameof(strConfForComplementToComplete),             "Selected configurations determining the complement to the complete area of categories:" },
                        { nameof(strConfForLink),                             "Link to existing configuration:" }
                    }
                    : languageFile.InternationalVersion.Data.TryGetValue(
                        key: nameof(ControlCategory),
                        out Dictionary<string, string> dictInternational)
                            ? dictInternational
                            : [],

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku</para>
        /// <para lang="en">Initialization of the control</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        private void Initialize(Control controlOwner)
        {
            BorderStyle = BorderStyle.None;
            ControlOwner = controlOwner;
            IsComplete = false;
            Nodes = [];
            OnEdit = false;

            btnPrevious.Click += new EventHandler(
                (sender, e) =>
                {
                    PreviousClick?.Invoke(sender: sender, e: e);
                });

            btnNext.Click += new EventHandler(
               (sender, e) =>
               {
                   NextClick?.Invoke(sender: sender, e: e);
               });

            tvwConfigurations.AfterCheck += new TreeViewEventHandler(
                (sender, e) =>
                {
                    if (OnEdit)
                    {
                        return;
                    }

                    if (Configuration == null)
                    {
                        return;
                    }

                    if (Configuration.ConfigQueryValue == ConfigQueryEnum.LinkToVariable)
                    {
                        // Pro odkaz na existující konfiguraci, musí být zaškrtnutý pouze jeden uzel
                        if ((e?.Node?.Tag != null) && (e.Node.Tag is Config lastCheckedConfiguration))
                        {
                            CheckOnlyOneNode(
                                level: 2,
                                id: lastCheckedConfiguration.Id);
                        }
                    }

                    if (IsComplete)
                    {
                        Configuration.CompleteList =
                            CheckedConfigurations
                            .Select(a => a.Id)
                            .ToList<int>();
                    }
                    else
                    {
                        Configuration.CategoriesList =
                            CheckedConfigurations
                            .Select(a => a.Id)
                            .ToList<int>();
                    }

                    SetCaption();

                    EnableNext();
                });

            tvwConfigurations.AfterSelect += new TreeViewEventHandler(
                (sender, e) => tvwConfigurations.SelectedNode = null);
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků ovládacího prvku</para>
        /// <para lang="en">Initialization of control labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            btnPrevious.Text =
                labels.TryGetValue(key: nameof(btnPrevious),
                out string btnPreviousText)
                    ? btnPreviousText
                    : String.Empty;

            btnNext.Text =
                labels.TryGetValue(key: nameof(btnNext),
                out string btnNextText)
                    ? btnNextText
                    : String.Empty;

            grpConfigurations.Text =
                labels.TryGetValue(key: nameof(grpConfigurations),
                out string grpConfigurationsText)
                    ? grpConfigurationsText
                    : String.Empty;

            strConfsForSum =
                labels.TryGetValue(key: nameof(strConfsForSum),
                out strConfsForSum)
                    ? strConfsForSum
                    : String.Empty;

            strConfForComplementToEstCellArea =
                labels.TryGetValue(key: nameof(strConfForComplementToEstCellArea),
                out strConfForComplementToEstCellArea)
                    ? strConfForComplementToEstCellArea
                    : String.Empty;

            strConfForComplete =
                labels.TryGetValue(key: nameof(strConfForComplete),
                out strConfForComplete)
                    ? strConfForComplete
                    : String.Empty;

            strConfForComplementToComplete =
                labels.TryGetValue(key: nameof(strConfForComplementToComplete),
                out strConfForComplementToComplete)
                    ? strConfForComplementToComplete
                    : String.Empty;

            strConfForLink =
                labels.TryGetValue(key: nameof(strConfForLink),
                out strConfForLink)
                    ? strConfForLink
                    : String.Empty;

            if (Configuration != null)
            {
                grpSelectedConfigurations.Text =
                    Configuration.ConfigQueryValue switch
                    {
                        ConfigQueryEnum.SumOfVariables => strConfsForSum,
                        ConfigQueryEnum.AdditionToCellArea => strConfForComplementToEstCellArea,
                        ConfigQueryEnum.AdditionToTotal =>
                            IsComplete
                                ? strConfForComplete
                                : strConfForComplementToComplete,
                        ConfigQueryEnum.LinkToVariable => strConfForLink,
                        _ =>
                            labels.TryGetValue(key: nameof(grpSelectedConfigurations),
                            out string grpSelectedConfigurationsText)
                                ? grpSelectedConfigurationsText
                                : String.Empty,
                    };
            }
            else
            {
                grpSelectedConfigurations.Text =
                    labels.TryGetValue(key: nameof(grpSelectedConfigurations),
                    out string grpSelectedConfigurationsText)
                        ? grpSelectedConfigurationsText
                        : String.Empty;
            }

            SetCaption();

            foreach (TreeNode node in ConfigFunctionNodes)
            {
                node.Text =
                    (LanguageVersion == LanguageVersion.International)
                    ? ((ConfigFunction)node.Tag).LabelEn
                    : (LanguageVersion == LanguageVersion.National)
                        ? ((ConfigFunction)node.Tag).LabelCs
                        : ((ConfigFunction)node.Tag).LabelEn;
            }

            foreach (TreeNode node in ConfigCollectionNodes)
            {
                node.Text =
                    (LanguageVersion == LanguageVersion.International)
                    ? ((ConfigCollection)node.Tag).LabelEn
                    : (LanguageVersion == LanguageVersion.National)
                        ? ((ConfigCollection)node.Tag).LabelCs
                        : ((ConfigCollection)node.Tag).LabelEn;
            }

            foreach (TreeNode node in ConfigurationNodes)
            {
                node.Text =
                    (LanguageVersion == LanguageVersion.International)
                    ? ((Config)node.Tag).LabelEn
                    : (LanguageVersion == LanguageVersion.National)
                        ? ((Config)node.Tag).LabelCs
                        : ((Config)node.Tag).LabelEn;
            }
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent()
        {
            InitializeTreeView();

            SetCaption();

            EnableNext();
        }

        /// <summary>
        /// <para lang="cs">Zobrazení konfigurací v uzlech TreeView</para>
        /// <para lang="en">Displaying configurations in TreeView nodes</para>
        /// </summary>
        private void InitializeTreeView()
        {
            // Zabrání prokreslování TreeView během editace
            tvwConfigurations.BeginUpdate();

            // Smazání všech uzlů z TreeView
            Nodes.Clear();
            tvwConfigurations.Nodes.Clear();

            // (Seznam funkcí pro výpočet úhrnů pomocných proměnných)
            foreach (ConfigFunction configFunction
                in Database.SAuxiliaryData.CConfigFunction.Items)
            {
                TreeNode node = new()
                {
                    ImageIndex = ImageBlueBall,
                    Name = configFunction.Id.ToString(),
                    SelectedImageIndex = ImageBlueBall,
                    Tag = configFunction,
                    Text =
                        (LanguageVersion == LanguageVersion.International)
                            ? configFunction.LabelEn
                            : (LanguageVersion == LanguageVersion.National)
                                ? configFunction.LabelCs
                                : configFunction.LabelEn
                };
                Nodes.Add(item: node);
                tvwConfigurations.Nodes.Add(node: node);

                Functions.HideCheckBox(
                    treeView: tvwConfigurations,
                    node: node);
            }

            // (Seznam skupin konfigurací)
            foreach (ConfigCollection configCollection
                in Database.SAuxiliaryData.TConfigCollection.Items)
            {
                TreeNode parent = ConfigFunctionNodes
                    .Where(a => ((ConfigFunction)a.Tag).Id == configCollection.ConfigFunctionId)
                    .FirstOrDefault<TreeNode>();

                if (parent != null)
                {
                    TreeNode node = new()
                    {
                        ImageIndex = configCollection.Closed ? ImageLock : ImageYellowBall,
                        Name = configCollection.Id.ToString(),
                        SelectedImageIndex = configCollection.Closed ? ImageLock : ImageYellowBall,
                        Tag = configCollection,
                        Text =
                            (LanguageVersion == LanguageVersion.International)
                                ? configCollection.LabelEn
                                : (LanguageVersion == LanguageVersion.National)
                                    ? configCollection.LabelCs
                                    : configCollection.LabelEn
                    };
                    Nodes.Add(item: node);
                    parent.Nodes.Add(node: node);

                    Functions.HideCheckBox(
                        treeView: tvwConfigurations,
                        node: node);
                }
            }

            // Přidání uzlů třetí úrovně
            // (Seznam konfigurací)
            foreach (Config config
                in Database.SAuxiliaryData.TConfig.Items)
            {
                TreeNode parent = ConfigCollectionNodes
                    .Where(a => ((ConfigCollection)a.Tag).Id == config.ConfigCollectionId)
                    .FirstOrDefault<TreeNode>();

                if (parent != null)
                {
                    TreeNode node = new()
                    {
                        Checked = (IsComplete)
                            ? Configuration.CompleteList.Contains(value: config.Id)
                            : Configuration.CategoriesList.Contains(value: config.Id),
                        ImageIndex =
                            (config.ConfigQuery.Value != ConfigQueryEnum.LinkToVariable) ? ImageGreenBall : ImageGrayBall,
                        Name = config.Id.ToString(),
                        SelectedImageIndex =
                            (config.ConfigQuery.Value != ConfigQueryEnum.LinkToVariable) ? ImageGreenBall : ImageGrayBall,
                        Tag = config,
                        Text =
                            (LanguageVersion == LanguageVersion.International)
                                    ? config.LabelEn
                                    : (LanguageVersion == LanguageVersion.National)
                                        ? config.LabelCs
                                        : config.LabelEn
                    };
                    Nodes.Add(item: node);
                    parent.Nodes.Add(node: node);
                }
            }

            tvwConfigurations.EndUpdate();

            ExpandCheckedNodes();

            HideCheckBoxesForNodes();
        }

        /// <summary>
        /// <para lang="cs">Rozbalí všechny uzly, které obsahují alespoň jeden zaškrtnutý uzel</para>
        /// <para lang="en">Expand all nodes that contain at least one checked node</para>
        /// </summary>
        private void ExpandCheckedNodes()
        {
            // Zabrání prokreslování TreeView během editace
            tvwConfigurations.BeginUpdate();

            // Rozbalí všechny uzly, které obsahují alespoň jeden zaškrtnutý uzel
            foreach (TreeNode nodeFunction in tvwConfigurations.Nodes)
            {
                foreach (TreeNode nodeCollection in nodeFunction.Nodes)
                {
                    bool check = false;
                    foreach (TreeNode nodeConfiguration in nodeCollection.Nodes)
                    {
                        check = check || nodeConfiguration.Checked;
                    }
                    if (check)
                    {
                        nodeFunction.Expand();
                        nodeCollection.Expand();
                    }
                    else
                    {
                        nodeCollection.Collapse();
                    }
                }
            }

            tvwConfigurations.EndUpdate();
        }

        /// <summary>
        /// <para lang="cs">Skryje zaškrtávací políčka u uzlů pro výpočetní funkce a skupiny konfigurací</para>
        /// <para lang="en">Hides checkboxes for configuration function a configuration collection nodes</para>
        /// </summary>
        private void HideCheckBoxesForNodes()
        {
            // Zabrání prokreslování TreeView během editace
            tvwConfigurations.BeginUpdate();

            foreach (TreeNode node in ConfigFunctionNodes)
            {
                Functions.HideCheckBox(
                    treeView: tvwConfigurations,
                    node: node);
            }

            foreach (TreeNode node in ConfigCollectionNodes)
            {
                Functions.HideCheckBox(
                    treeView: tvwConfigurations,
                    node: node);
            }

            tvwConfigurations.EndUpdate();
        }

        /// <summary>
        /// <para lang="cs">
        /// Vrátí uzel z TreeView odpovídající identifikačnímu číslu
        /// funkce nebo skupiny konfigurací nebo konfiguraci
        /// </para>
        /// <para lang="en">
        /// Returns the TreeView node corresponding to
        /// the function, configuration collection or configuration identifier
        /// </para>
        /// </summary>
        /// <param name="level">
        /// <para lang="cs">Úroveň uzlu v TreeView</para>
        /// <para lang="en">TreeView node level</para>
        /// </param>
        /// <param name="id">
        /// <para lang="cs">Identifikační číslo funkce, skupiny konfigurací, konfigurace</para>
        /// <para lang="en">Function, configuration collection or configuration identifier</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">
        /// Vrátí uzel z TreeView odpovídající identifikačnímu číslu
        /// funkce nebo skupiny konfigurací nebo konfiguraci
        /// </para>
        /// <para lang="en">
        /// Returns the TreeView node corresponding to
        /// the function, configuration collection or configuration identifier
        /// </para>
        /// </returns>
        private TreeNode FindNode(int level, int id)
        {
            return level switch
            {
                LevelConfigFunction => ConfigFunctionNodes
                        .Where(a => ((ConfigFunction)a.Tag).Id == id)
                        .FirstOrDefault<TreeNode>(),
                LevelConfigCollection => ConfigCollectionNodes
                        .Where(a => ((ConfigCollection)a.Tag).Id == id)
                        .FirstOrDefault<TreeNode>(),
                LevelConfiguration => ConfigurationNodes
                        .Where(a => ((Config)a.Tag).Id == id)
                        .FirstOrDefault<TreeNode>(),
                _ => null,
            };
        }

        /// <summary>
        /// <para lang="cs">Zaškrtne pouze jeden uzel v TreeView</para>
        /// <para lang="en">Check only one node in TreeView</para>
        /// </summary>
        /// <param name="level">
        /// <para lang="cs">Úroveň uzlu v TreeView</para>
        /// <para lang="en">TreeView node level</para>
        /// </param>
        /// <param name="id">
        /// <para lang="cs">Identifikační číslo funkce, skupiny konfigurací, konfigurace</para>
        /// <para lang="en">Function, configuration collection or configuration identifier</para>
        /// </param>
        private void CheckOnlyOneNode(int level, int id)
        {
            OnEdit = true;

            foreach (TreeNode node in ConfigurationNodes)
            {
                node.Checked = false;
            }

            TreeNode checkedNode = FindNode(level: level, id: id);
            if (checkedNode != null)
            {
                checkedNode.Checked = true;
            }

            OnEdit = false;
        }

        /// <summary>
        /// <para lang="cs">Nastaví nadpis se seznamem vybraných konfigurací</para>
        /// <para lang="en">Sets the title with a list of selected configurations</para>
        /// </summary>
        private void SetCaption()
        {
            string lblSelectedConfigurationsEn =
                CheckedConfigurations.Any()
                    ? CheckedConfigurations
                        .Select(a => a.ExtendedLabelEn)
                        .Aggregate((a, b) => $"{a}, {b}")
                    : String.Empty;

            string lblSelectedConfigurationsCs =
                CheckedConfigurations.Any()
                   ? CheckedConfigurations
                    .Select(a => a.ExtendedLabelCs)
                    .Aggregate((a, b) => $"{a}, {b}")
                    : String.Empty;

            lblSelectedConfigurations.Text =
                (LanguageVersion == LanguageVersion.International)
                    ? lblSelectedConfigurationsEn
                    : (LanguageVersion == LanguageVersion.National)
                        ? lblSelectedConfigurationsCs
                        : lblSelectedConfigurationsEn;
        }

        /// <summary>
        /// <para lang="cs">Povolí postup na další formulář</para>
        /// <para lang="en">Allows progression to the next form</para>
        /// </summary>
        private void EnableNext()
        {
            if (Configuration == null)
            {
                btnNext.Enabled = false;
                return;
            }

            if (IsComplete)
            {
                btnNext.Enabled =
                    !String.IsNullOrEmpty(Configuration.LabelCs) &&
                    !String.IsNullOrEmpty(Configuration.LabelEn) &&
                    !String.IsNullOrEmpty(Configuration.DescriptionCs) &&
                    !String.IsNullOrEmpty(Configuration.DescriptionEn) &&
                    (Configuration.ConfigQueryValue != ConfigQueryEnum.Unknown) &&
                    !String.IsNullOrEmpty(Configuration.Complete);
            }
            else
            {
                btnNext.Enabled =
                    !String.IsNullOrEmpty(Configuration.LabelCs) &&
                    !String.IsNullOrEmpty(Configuration.LabelEn) &&
                    !String.IsNullOrEmpty(Configuration.DescriptionCs) &&
                    !String.IsNullOrEmpty(Configuration.DescriptionEn) &&
                    (Configuration.ConfigQueryValue != ConfigQueryEnum.Unknown) &&
                    !String.IsNullOrEmpty(Configuration.Categories);
            }
        }

        #endregion Methods

    }
}
