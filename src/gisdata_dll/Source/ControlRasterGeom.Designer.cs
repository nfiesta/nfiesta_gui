﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi.ModuleAuxiliaryData
{

    partial class ControlRasterGeom
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            tlpMain = new System.Windows.Forms.TableLayoutPanel();
            tlpButtons = new System.Windows.Forms.TableLayoutPanel();
            pnlNext = new System.Windows.Forms.Panel();
            btnNext = new System.Windows.Forms.Button();
            pnlPrevious = new System.Windows.Forms.Panel();
            btnPrevious = new System.Windows.Forms.Button();
            grpColumnName = new System.Windows.Forms.GroupBox();
            grpColumnIdent = new System.Windows.Forms.GroupBox();
            grpUnitCoefficient = new System.Windows.Forms.GroupBox();
            txtUnitCoefficient = new System.Windows.Forms.TextBox();
            lblUnitCoefficientDescription1 = new System.Windows.Forms.Label();
            lblUnitCoefficientDescription2 = new System.Windows.Forms.Label();
            lblUnitCoefficientDescription3 = new System.Windows.Forms.Label();
            lblUnitCoefficientDescription4 = new System.Windows.Forms.Label();
            tlpMain.SuspendLayout();
            tlpButtons.SuspendLayout();
            pnlNext.SuspendLayout();
            pnlPrevious.SuspendLayout();
            grpUnitCoefficient.SuspendLayout();
            SuspendLayout();
            // 
            // tlpMain
            // 
            tlpMain.ColumnCount = 1;
            tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.Controls.Add(tlpButtons, 0, 4);
            tlpMain.Controls.Add(grpColumnName, 0, 1);
            tlpMain.Controls.Add(grpColumnIdent, 0, 0);
            tlpMain.Controls.Add(grpUnitCoefficient, 0, 2);
            tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpMain.Location = new System.Drawing.Point(0, 0);
            tlpMain.Margin = new System.Windows.Forms.Padding(0);
            tlpMain.Name = "tlpMain";
            tlpMain.RowCount = 5;
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 140F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            tlpMain.Size = new System.Drawing.Size(960, 540);
            tlpMain.TabIndex = 2;
            // 
            // tlpButtons
            // 
            tlpButtons.BackColor = System.Drawing.Color.Transparent;
            tlpButtons.ColumnCount = 3;
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            tlpButtons.Controls.Add(pnlNext, 2, 0);
            tlpButtons.Controls.Add(pnlPrevious, 1, 0);
            tlpButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpButtons.Location = new System.Drawing.Point(0, 500);
            tlpButtons.Margin = new System.Windows.Forms.Padding(0);
            tlpButtons.Name = "tlpButtons";
            tlpButtons.RowCount = 1;
            tlpButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.Size = new System.Drawing.Size(960, 40);
            tlpButtons.TabIndex = 17;
            // 
            // pnlNext
            // 
            pnlNext.Controls.Add(btnNext);
            pnlNext.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlNext.Location = new System.Drawing.Point(800, 0);
            pnlNext.Margin = new System.Windows.Forms.Padding(0);
            pnlNext.Name = "pnlNext";
            pnlNext.Padding = new System.Windows.Forms.Padding(5);
            pnlNext.Size = new System.Drawing.Size(160, 40);
            pnlNext.TabIndex = 13;
            // 
            // btnNext
            // 
            btnNext.Dock = System.Windows.Forms.DockStyle.Fill;
            btnNext.Enabled = false;
            btnNext.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            btnNext.Location = new System.Drawing.Point(5, 5);
            btnNext.Margin = new System.Windows.Forms.Padding(0);
            btnNext.Name = "btnNext";
            btnNext.Size = new System.Drawing.Size(150, 30);
            btnNext.TabIndex = 9;
            btnNext.Text = "btnNext";
            btnNext.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            btnNext.UseVisualStyleBackColor = true;
            // 
            // pnlPrevious
            // 
            pnlPrevious.Controls.Add(btnPrevious);
            pnlPrevious.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlPrevious.Location = new System.Drawing.Point(640, 0);
            pnlPrevious.Margin = new System.Windows.Forms.Padding(0);
            pnlPrevious.Name = "pnlPrevious";
            pnlPrevious.Padding = new System.Windows.Forms.Padding(5);
            pnlPrevious.Size = new System.Drawing.Size(160, 40);
            pnlPrevious.TabIndex = 14;
            // 
            // btnPrevious
            // 
            btnPrevious.Dock = System.Windows.Forms.DockStyle.Fill;
            btnPrevious.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            btnPrevious.Location = new System.Drawing.Point(5, 5);
            btnPrevious.Margin = new System.Windows.Forms.Padding(0);
            btnPrevious.Name = "btnPrevious";
            btnPrevious.Size = new System.Drawing.Size(150, 30);
            btnPrevious.TabIndex = 9;
            btnPrevious.Text = "btnPrevious";
            btnPrevious.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            btnPrevious.UseVisualStyleBackColor = true;
            // 
            // grpColumnName
            // 
            grpColumnName.Dock = System.Windows.Forms.DockStyle.Fill;
            grpColumnName.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            grpColumnName.ForeColor = System.Drawing.Color.MediumBlue;
            grpColumnName.Location = new System.Drawing.Point(0, 60);
            grpColumnName.Margin = new System.Windows.Forms.Padding(0);
            grpColumnName.Name = "grpColumnName";
            grpColumnName.Padding = new System.Windows.Forms.Padding(5);
            grpColumnName.Size = new System.Drawing.Size(960, 60);
            grpColumnName.TabIndex = 9;
            grpColumnName.TabStop = false;
            grpColumnName.Text = "grpColumnName";
            // 
            // grpColumnIdent
            // 
            grpColumnIdent.Dock = System.Windows.Forms.DockStyle.Fill;
            grpColumnIdent.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            grpColumnIdent.ForeColor = System.Drawing.Color.MediumBlue;
            grpColumnIdent.Location = new System.Drawing.Point(0, 0);
            grpColumnIdent.Margin = new System.Windows.Forms.Padding(0);
            grpColumnIdent.Name = "grpColumnIdent";
            grpColumnIdent.Padding = new System.Windows.Forms.Padding(5);
            grpColumnIdent.Size = new System.Drawing.Size(960, 60);
            grpColumnIdent.TabIndex = 8;
            grpColumnIdent.TabStop = false;
            grpColumnIdent.Text = "grpColumnIdent";
            // 
            // grpUnitCoefficient
            // 
            grpUnitCoefficient.Controls.Add(lblUnitCoefficientDescription4);
            grpUnitCoefficient.Controls.Add(lblUnitCoefficientDescription3);
            grpUnitCoefficient.Controls.Add(lblUnitCoefficientDescription2);
            grpUnitCoefficient.Controls.Add(lblUnitCoefficientDescription1);
            grpUnitCoefficient.Controls.Add(txtUnitCoefficient);
            grpUnitCoefficient.Dock = System.Windows.Forms.DockStyle.Fill;
            grpUnitCoefficient.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            grpUnitCoefficient.ForeColor = System.Drawing.Color.MediumBlue;
            grpUnitCoefficient.Location = new System.Drawing.Point(0, 120);
            grpUnitCoefficient.Margin = new System.Windows.Forms.Padding(0);
            grpUnitCoefficient.Name = "grpUnitCoefficient";
            grpUnitCoefficient.Padding = new System.Windows.Forms.Padding(5);
            grpUnitCoefficient.Size = new System.Drawing.Size(960, 140);
            grpUnitCoefficient.TabIndex = 3;
            grpUnitCoefficient.TabStop = false;
            grpUnitCoefficient.Text = "boxUnitCoefficient";
            // 
            // txtUnitCoefficient
            // 
            txtUnitCoefficient.BackColor = System.Drawing.Color.White;
            txtUnitCoefficient.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            txtUnitCoefficient.Dock = System.Windows.Forms.DockStyle.Top;
            txtUnitCoefficient.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            txtUnitCoefficient.ForeColor = System.Drawing.Color.Black;
            txtUnitCoefficient.Location = new System.Drawing.Point(5, 25);
            txtUnitCoefficient.Margin = new System.Windows.Forms.Padding(0);
            txtUnitCoefficient.Name = "txtUnitCoefficient";
            txtUnitCoefficient.Size = new System.Drawing.Size(950, 23);
            txtUnitCoefficient.TabIndex = 1;
            // 
            // lblUnitCoefficientDescription1
            // 
            lblUnitCoefficientDescription1.Dock = System.Windows.Forms.DockStyle.Top;
            lblUnitCoefficientDescription1.Font = new System.Drawing.Font("Segoe UI", 9F);
            lblUnitCoefficientDescription1.ForeColor = System.Drawing.Color.Black;
            lblUnitCoefficientDescription1.Location = new System.Drawing.Point(5, 48);
            lblUnitCoefficientDescription1.Margin = new System.Windows.Forms.Padding(0);
            lblUnitCoefficientDescription1.Name = "lblUnitCoefficientDescription1";
            lblUnitCoefficientDescription1.Size = new System.Drawing.Size(950, 20);
            lblUnitCoefficientDescription1.TabIndex = 2;
            lblUnitCoefficientDescription1.Text = "lblUnitCoefficientDescription1";
            lblUnitCoefficientDescription1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblUnitCoefficientDescription2
            // 
            lblUnitCoefficientDescription2.Dock = System.Windows.Forms.DockStyle.Top;
            lblUnitCoefficientDescription2.Font = new System.Drawing.Font("Segoe UI", 9F);
            lblUnitCoefficientDescription2.ForeColor = System.Drawing.Color.Black;
            lblUnitCoefficientDescription2.Location = new System.Drawing.Point(5, 68);
            lblUnitCoefficientDescription2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            lblUnitCoefficientDescription2.Name = "lblUnitCoefficientDescription2";
            lblUnitCoefficientDescription2.Size = new System.Drawing.Size(950, 20);
            lblUnitCoefficientDescription2.TabIndex = 3;
            lblUnitCoefficientDescription2.Text = "lblUnitCoefficientDescription2";
            lblUnitCoefficientDescription2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblUnitCoefficientDescription3
            // 
            lblUnitCoefficientDescription3.Dock = System.Windows.Forms.DockStyle.Top;
            lblUnitCoefficientDescription3.Font = new System.Drawing.Font("Segoe UI", 9F);
            lblUnitCoefficientDescription3.ForeColor = System.Drawing.Color.Black;
            lblUnitCoefficientDescription3.Location = new System.Drawing.Point(5, 88);
            lblUnitCoefficientDescription3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            lblUnitCoefficientDescription3.Name = "lblUnitCoefficientDescription3";
            lblUnitCoefficientDescription3.Size = new System.Drawing.Size(950, 20);
            lblUnitCoefficientDescription3.TabIndex = 4;
            lblUnitCoefficientDescription3.Text = "lblUnitCoefficientDescription3";
            lblUnitCoefficientDescription3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblUnitCoefficientDescription4
            // 
            lblUnitCoefficientDescription4.Dock = System.Windows.Forms.DockStyle.Top;
            lblUnitCoefficientDescription4.Font = new System.Drawing.Font("Segoe UI", 9F);
            lblUnitCoefficientDescription4.ForeColor = System.Drawing.Color.Black;
            lblUnitCoefficientDescription4.Location = new System.Drawing.Point(5, 108);
            lblUnitCoefficientDescription4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            lblUnitCoefficientDescription4.Name = "lblUnitCoefficientDescription4";
            lblUnitCoefficientDescription4.Size = new System.Drawing.Size(950, 20);
            lblUnitCoefficientDescription4.TabIndex = 5;
            lblUnitCoefficientDescription4.Text = "lblUnitCoefficientDescription4";
            lblUnitCoefficientDescription4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ControlRasterGeom
            // 
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            BackColor = System.Drawing.Color.Transparent;
            BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            Controls.Add(tlpMain);
            ForeColor = System.Drawing.Color.Black;
            Margin = new System.Windows.Forms.Padding(0);
            Name = "ControlRasterGeom";
            Size = new System.Drawing.Size(960, 540);
            tlpMain.ResumeLayout(false);
            tlpButtons.ResumeLayout(false);
            pnlNext.ResumeLayout(false);
            pnlPrevious.ResumeLayout(false);
            grpUnitCoefficient.ResumeLayout(false);
            grpUnitCoefficient.PerformLayout();
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.TableLayoutPanel tlpButtons;
        private System.Windows.Forms.Panel pnlNext;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Panel pnlPrevious;
        private System.Windows.Forms.Button btnPrevious;
        private System.Windows.Forms.GroupBox grpColumnName;
        private System.Windows.Forms.GroupBox grpColumnIdent;
        private System.Windows.Forms.GroupBox grpUnitCoefficient;
        private System.Windows.Forms.TextBox txtUnitCoefficient;
        private System.Windows.Forms.Label lblUnitCoefficientDescription1;
        private System.Windows.Forms.Label lblUnitCoefficientDescription2;
        private System.Windows.Forms.Label lblUnitCoefficientDescription3;
        private System.Windows.Forms.Label lblUnitCoefficientDescription4;
    }

}
