﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.AuxiliaryData;
using ZaJi.PostgreSQL;

namespace ZaJi.ModuleAuxiliaryData
{

    /// <summary>
    /// <para lang="cs">Formulář "Výběr konfigurace pro vektorovou nebo rastrovou vrstvu"</para>
    /// <para lang="en">Form "Select configuration for vector or raster layer"</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    internal partial class FormConfigurationSelector
        : Form, INfiEstaControl, IAuxiliaryDataControl
    {

        #region Constants

        private const int LevelConfigCollection = 0;
        private const int LevelConfiguration = 1;
        private const int ImageRedBall = 0;
        private const int ImageBlueBall = 1;
        private const int ImageYellowBall = 2;
        private const int ImageGreenBall = 3;
        private const int ImageGrayBall = 4;
        private const int ImageLock = 5;
        private const int ImageSchema = 6;
        private const int ImageVector = 7;
        private const int ImageRaster = 8;

        #endregion Constants


        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Uzly v TreeView</para>
        /// <para lang="en">TreeView nodes</para>
        /// </summary>
        private List<TreeNode> nodes;

        /// <summary>
        /// <para lang="cs">Indikátor editace ovládacího prvku</para>
        /// <para lang="en">Control editing indicator</para>
        /// </summary>
        private bool onEdit;

        /// <summary>
        /// <para lang="cs">Konfigurační funkce</para>
        /// <para lang="en">Config function</para>
        /// </summary>
        private ConfigFunction configFunction;

        #endregion Private Fields


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor ovládacího prvku</para>
        /// <para lang="en">Control constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        /// <param name="configFunction">
        /// <para lang="cs">Konfigurační funkce</para>
        /// <para lang="en">Config function</para>
        /// </param>
        public FormConfigurationSelector(
            Control controlOwner,
            ConfigFunction configFunction)
        {
            InitializeComponent();
            Initialize(
                controlOwner: controlOwner,
                configFunction: configFunction);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                IAuxiliaryDataControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                return controlOwner;
            }
            set
            {
                IAuxiliaryDataControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables (read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((IAuxiliaryDataControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro pomocná data (read-only)</para>
        /// <para lang="en">Module for auxiliary data setting (read-only)</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((IAuxiliaryDataControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">Vybraná konfigurace</para>
        /// <para lang="en">Selected configuration</para>
        /// </summary>
        public Config Configuration
        {
            get
            {
                return
                    CheckedConfigurations.FirstOrDefault<Config>();
            }
            set
            {
                CheckOnlyOneNode(
                    level: LevelConfiguration,
                    id: value?.Id ?? 0);
            }
        }

        /// <summary>
        /// <para lang="cs">Konfigurační funkce</para>
        /// <para lang="en">Config function</para>
        /// </summary>
        private ConfigFunction ConfigFunction
        {
            get
            {
                return configFunction;
            }
            set
            {
                configFunction = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Indikátor editace ovládacího prvku</para>
        /// <para lang="en">Control editing indicator</para>
        /// </summary>
        private bool OnEdit
        {
            get
            {
                return onEdit;
            }
            set
            {
                onEdit = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Uzly v TreeView</para>
        /// <para lang="en">TreeView nodes</para>
        /// </summary>
        private List<TreeNode> Nodes
        {
            get
            {
                return nodes ?? [];
            }
            set
            {
                nodes = value ?? [];
            }
        }

        /// <summary>
        /// <para lang="cs">Uzly v TreeView úroveň skupin konfigurací (read-only)</para>
        /// <para lang="en">TreeView nodes - ConfigCollections level (read-only)</para>
        /// </summary>
        private IEnumerable<TreeNode> ConfigCollectionNodes
        {
            get
            {
                return
                    Nodes
                        .Where(a => a.Level == LevelConfigCollection)
                        .Where(a => a.Tag != null)
                        .Where(a => a.Tag is ConfigCollection);
            }
        }

        /// <summary>
        /// <para lang="cs">Uzly v TreeView úroveň konfigurací (read-only)</para>
        /// <para lang="en">TreeView nodes - Configurations level (read-only)</para>
        /// </summary>
        private IEnumerable<TreeNode> ConfigurationNodes
        {
            get
            {
                return
                    Nodes
                        .Where(a => a.Level == LevelConfiguration)
                        .Where(a => a.Tag != null)
                        .Where(a => a.Tag is Config);
            }
        }

        /// <summary>
        /// <para lang="cs">Zaškrtnuté uzly konfigurací (read-only)</para>
        /// <para lang="en">Checked configuration nodes (read-only)</para>
        /// </summary>
        private IEnumerable<TreeNode> CheckedConfigurationNodes
        {
            get
            {
                return
                    ConfigurationNodes
                        .Where(a => a.Checked);
            }
        }

        /// <summary>
        /// <para lang="cs">Zaškrtnuté konfigurace (read-only)</para>
        /// <para lang="en">Checked configurations (read-only)</para>
        /// </summary>
        private IEnumerable<Config> CheckedConfigurations
        {
            get
            {
                return CheckedConfigurationNodes
                    .Select(a => (Config)a.Tag);
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { $"{nameof(FormConfigurationSelector)}Vector",     "Konfigurace pro vektorovou vrstvu" },
                        { $"{nameof(FormConfigurationSelector)}Raster",     "Konfigurace pro rastrovou vrstvu"  },
                        { nameof(btnOK),                                    "OK" },
                        { nameof(btnCancel),                                "Zrušit" },
                        { nameof(grpItems),                                 "Seznam konfigurací:" },
                        { nameof(grpSelectedItem),                          "Vybraná konfigurace:" }
                    }
                    : languageFile.NationalVersion.Data.TryGetValue(
                        key: nameof(FormConfigurationSelector),
                        out Dictionary<string, string> dictNational)
                            ? dictNational
                            : [],

                LanguageVersion.International => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { $"{nameof(FormConfigurationSelector)}Vector",     "Configuration for vector layer" },
                        { $"{nameof(FormConfigurationSelector)}Raster",     "Configuration for raster layer" },
                        { nameof(btnOK),                                    "OK" },
                        { nameof(btnCancel),                                "Cancel" },
                        { nameof(grpItems),                                 "List of configurations:" },
                        { nameof(grpSelectedItem),                          "Selected configuration:"}
                    }
                    : languageFile.InternationalVersion.Data.TryGetValue(
                        key: nameof(FormConfigurationSelector),
                        out Dictionary<string, string> dictInternational)
                            ? dictInternational
                            : [],

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku</para>
        /// <para lang="en">Initialization of the control</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        /// <param name="configFunction">
        /// <para lang="cs">Konfigurační funkce</para>
        /// <para lang="en">Config function</para>
        /// </param>
        private void Initialize(
            Control controlOwner,
            ConfigFunction configFunction)
        {
            ControlOwner = controlOwner;
            ConfigFunction = configFunction;
            Nodes = [];
            OnEdit = false;

            LoadContent();

            btnOK.Click += new EventHandler(
                (sender, e) =>
                {
                    DialogResult = DialogResult.OK;
                    Close();
                });

            btnCancel.Click += new EventHandler(
               (sender, e) =>
               {
                   DialogResult = DialogResult.Cancel;
                   Close();
               });

            tvwItems.AfterCheck += new TreeViewEventHandler(
                (sender, e) =>
                {
                    if (OnEdit)
                    {
                        return;
                    }

                    if ((e?.Node?.Tag != null) && (e.Node.Tag is Config lastCheckedConfiguration))
                    {
                        CheckOnlyOneNode(
                            level: LevelConfiguration,
                            id: lastCheckedConfiguration.Id);
                    }

                    SetCaption();
                });

            tvwItems.AfterSelect += new TreeViewEventHandler(
                (sender, e) => tvwItems.SelectedNode = null);
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků ovládacího prvku</para>
        /// <para lang="en">Initialization of control labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            Text =
                ConfigFunction.Value switch
                {
                    ConfigFunctionEnum.VectorTotal =>
                        labels.TryGetValue(key: $"{nameof(FormConfigurationSelector)}Vector",
                            out string frmConfigurationForVectorSelectorText)
                                ? frmConfigurationForVectorSelectorText
                                : String.Empty,
                    ConfigFunctionEnum.RasterTotal =>
                        labels.TryGetValue(key: $"{nameof(FormConfigurationSelector)}Raster",
                            out string frmConfigurationForRasterSelectorText)
                                ? frmConfigurationForRasterSelectorText
                                : String.Empty,
                    _ => String.Empty,
                };

            grpSelectedItem.Text =
                labels.TryGetValue(key: nameof(grpSelectedItem),
                out string grpSelectedItemText)
                    ? grpSelectedItemText
                    : String.Empty;

            grpItems.Text =
                labels.TryGetValue(key: nameof(grpItems),
                out string grpItemsText)
                    ? grpItemsText
                    : String.Empty;

            btnOK.Text =
                labels.TryGetValue(key: nameof(btnOK),
                out string btnOKText)
                    ? btnOKText
                    : String.Empty;

            btnCancel.Text =
                labels.TryGetValue(key: nameof(btnCancel),
                out string btnCancelText)
                    ? btnCancelText
                    : String.Empty;

            SetCaption();

            foreach (TreeNode node in ConfigCollectionNodes)
            {
                node.Text =
                    (LanguageVersion == LanguageVersion.International)
                    ? ((ConfigCollection)node.Tag).LabelEn
                    : (LanguageVersion == LanguageVersion.National)
                        ? ((ConfigCollection)node.Tag).LabelCs
                        : ((ConfigCollection)node.Tag).LabelEn;
            }

            foreach (TreeNode node in ConfigurationNodes)
            {
                node.Text =
                    (LanguageVersion == LanguageVersion.International)
                    ? ((Config)node.Tag).LabelEn
                    : (LanguageVersion == LanguageVersion.National)
                        ? ((Config)node.Tag).LabelCs
                        : ((Config)node.Tag).LabelEn;
            }
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent()
        {
            InitializeTreeView();

            InitializeLabels();

            SetCaption();
        }

        /// <summary>
        /// <para lang="cs">Zobrazení konfigurací v uzlech TreeView</para>
        /// <para lang="en">Displaying configurations in TreeView nodes</para>
        /// </summary>
        private void InitializeTreeView()
        {
            // Zabrání prokreslování TreeView během editace
            tvwItems.BeginUpdate();

            // Smazání všech uzlů z TreeView
            Nodes.Clear();
            tvwItems.Nodes.Clear();

            // (Seznam skupin konfigurací)
            foreach (ConfigCollection configCollection
                in Database.SAuxiliaryData.TConfigCollection.Items
                .Where(a => a.ConfigFunction.Id == ConfigFunction.Id))
            {
                TreeNode node = new()
                {
                    ImageIndex = configCollection.Closed ? ImageLock : ImageYellowBall,
                    Name = configCollection.Id.ToString(),
                    SelectedImageIndex = configCollection.Closed ? ImageLock : ImageYellowBall,
                    Tag = configCollection,
                    Text =
                        (LanguageVersion == LanguageVersion.International)
                            ? configCollection.LabelEn
                            : (LanguageVersion == LanguageVersion.National)
                                ? configCollection.LabelCs
                                : configCollection.LabelEn
                };
                Nodes.Add(item: node);
                tvwItems.Nodes.Add(node: node);

                Functions.HideCheckBox(
                    treeView: tvwItems,
                    node: node);
            }

            // (Seznam konfigurací)
            foreach (Config config
                in Database.SAuxiliaryData.TConfig.Items
                .Where(a => a.ConfigCollection.ConfigFunction.Id == ConfigFunction.Id))
            {
                TreeNode parent = ConfigCollectionNodes
                    .Where(a => ((ConfigCollection)a.Tag).Id == config.ConfigCollectionId)
                    .FirstOrDefault<TreeNode>();

                if (parent != null)
                {
                    TreeNode node = new()
                    {
                        Checked = false,
                        ImageIndex =
                            (config.ConfigQuery.Value != ConfigQueryEnum.LinkToVariable)
                                ? ImageGreenBall
                                : ImageGrayBall,
                        Name = config.Id.ToString(),
                        SelectedImageIndex =
                            (config.ConfigQuery.Value != ConfigQueryEnum.LinkToVariable)
                                ? ImageGreenBall
                                : ImageGrayBall,
                        Tag = config,
                        Text =
                            (LanguageVersion == LanguageVersion.International)
                                    ? config.LabelEn
                                    : (LanguageVersion == LanguageVersion.National)
                                        ? config.LabelCs
                                        : config.LabelEn
                    };
                    Nodes.Add(item: node);
                    parent.Nodes.Add(node: node);
                }
            }

            tvwItems.EndUpdate();

            ExpandCheckedNodes();

            HideCheckBoxesForNodes();
        }

        /// <summary>
        /// <para lang="cs">Rozbalí všechny uzly, které obsahují alespoň jeden zaškrtnutý uzel</para>
        /// <para lang="en">Expand all nodes that contain at least one checked node</para>
        /// </summary>
        private void ExpandCheckedNodes()
        {
            // Zabrání prokreslování TreeView během editace
            tvwItems.BeginUpdate();

            // Rozbalí všechny uzly, které obsahují alespoň jeden zaškrtnutý uzel
            foreach (TreeNode nodeCollection in tvwItems.Nodes)
            {
                bool check = false;
                foreach (TreeNode nodeConfiguration in nodeCollection.Nodes)
                {
                    check = check || nodeConfiguration.Checked;
                }
                if (check)
                {
                    nodeCollection.Expand();
                }
                else
                {
                    nodeCollection.Collapse();
                }
            }

            tvwItems.EndUpdate();
        }

        /// <summary>
        /// <para lang="cs">Skryje zaškrtávací políčka u uzlů pro výpočetní funkce a skupiny konfigurací</para>
        /// <para lang="en">Hides checkboxes for configuration function a configuration collection nodes</para>
        /// </summary>
        private void HideCheckBoxesForNodes()
        {
            // Zabrání prokreslování TreeView během editace
            tvwItems.BeginUpdate();

            foreach (TreeNode node in ConfigCollectionNodes)
            {
                Functions.HideCheckBox(
                    treeView: tvwItems,
                    node: node);
            }

            tvwItems.EndUpdate();
        }

        /// <summary>
        /// <para lang="cs">
        /// Vrátí uzel z TreeView odpovídající identifikačnímu číslu
        /// funkce nebo skupiny konfigurací nebo konfiguraci
        /// </para>
        /// <para lang="en">
        /// Returns the TreeView node corresponding to
        /// the function, configuration collection or configuration identifier
        /// </para>
        /// </summary>
        /// <param name="level">
        /// <para lang="cs">Úroveň uzlu v TreeView</para>
        /// <para lang="en">TreeView node level</para>
        /// </param>
        /// <param name="id">
        /// <para lang="cs">Identifikační číslo funkce, skupiny konfigurací, konfigurace</para>
        /// <para lang="en">Function, configuration collection or configuration identifier</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">
        /// Vrátí uzel z TreeView odpovídající identifikačnímu číslu
        /// funkce nebo skupiny konfigurací nebo konfiguraci
        /// </para>
        /// <para lang="en">
        /// Returns the TreeView node corresponding to
        /// the function, configuration collection or configuration identifier
        /// </para>
        /// </returns>
        private TreeNode FindNode(int level, int id)
        {
            return level switch
            {
                LevelConfigCollection => ConfigCollectionNodes
                        .Where(a => ((ConfigCollection)a.Tag).Id == id)
                        .FirstOrDefault<TreeNode>(),
                LevelConfiguration => ConfigurationNodes
                        .Where(a => ((Config)a.Tag).Id == id)
                        .FirstOrDefault<TreeNode>(),
                _ => null,
            };
        }

        /// <summary>
        /// <para lang="cs">Zaškrtne pouze jeden uzel v TreeView</para>
        /// <para lang="en">Check only one node in TreeView</para>
        /// </summary>
        /// <param name="level">
        /// <para lang="cs">Úroveň uzlu v TreeView</para>
        /// <para lang="en">TreeView node level</para>
        /// </param>
        /// <param name="id">
        /// <para lang="cs">Identifikační číslo funkce, skupiny konfigurací, konfigurace</para>
        /// <para lang="en">Function, configuration collection or configuration identifier</para>
        /// </param>
        private void CheckOnlyOneNode(int level, int id)
        {
            OnEdit = true;

            foreach (TreeNode node in ConfigurationNodes)
            {
                node.Checked = false;
            }

            TreeNode checkedNode = FindNode(level: level, id: id);
            if (checkedNode != null)
            {
                checkedNode.Checked = true;
            }

            OnEdit = false;

            SetCaption();
        }

        /// <summary>
        /// <para lang="cs">Nastaví nadpis se seznamem vybraných konfigurací</para>
        /// <para lang="en">Sets the title with a list of selected configurations</para>
        /// </summary>
        private void SetCaption()
        {
            string strSelectedItemsEn =
                CheckedConfigurations.Any()
                    ? CheckedConfigurations
                        .Select(a => a.ExtendedLabelEn)
                        .Aggregate((a, b) => $"{a}, {b}")
                    : String.Empty;

            string strSelectedItemsCs =
                CheckedConfigurations.Any()
                   ? CheckedConfigurations
                    .Select(a => a.ExtendedLabelCs)
                    .Aggregate((a, b) => $"{a}, {b}")
                    : String.Empty;

            lblSelectedItem.Text =
                (LanguageVersion == LanguageVersion.International)
                    ? strSelectedItemsEn
                    : (LanguageVersion == LanguageVersion.National)
                        ? strSelectedItemsCs
                        : strSelectedItemsEn;
        }

        #endregion Methods

    }

}