﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.AuxiliaryData;
using ZaJi.NfiEstaPg.Core;
using ZaJi.PostgreSQL;

namespace ZaJi.ModuleAuxiliaryData
{

    /// <summary>
    /// <para lang="cs">Ovládací prvek "Výběr pomocné proměnné"</para>
    /// <para lang="en">Control "Auxiliary variable selection"</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    internal partial class ControlAuxiliaryVariableSelector
        : UserControl, INfiEstaControl, IAuxiliaryDataControl
    {

        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Pomocná proměnná</para>
        /// <para lang="en">Auxiliary variable</para>
        /// </summary>
        private AuxiliaryVariable auxiliaryVariable;

        #endregion Private Fields


        #region Events

        /// <summary>
        /// <para lang="cs">Událost "Změna vybrané pomocné proměnné"</para>
        /// <para lang="en">Event "Auxiliary variable changed"</para>
        /// </summary>
        public event EventHandler AuxiliaryVariableChanged;

        #endregion Events


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor ovládacího prvku</para>
        /// <para lang="en">Control constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        public ControlAuxiliaryVariableSelector(Control controlOwner)
        {
            InitializeComponent();
            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                IAuxiliaryDataControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                if (controlOwner is not ControlConfigCollectionIntro)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(ControlConfigCollectionIntro)}.",
                        paramName: nameof(ControlOwner));
                }

                return controlOwner;
            }
            set
            {
                IAuxiliaryDataControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                if (value is not ControlConfigCollectionIntro)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(ControlConfigCollectionIntro)}.",
                        paramName: nameof(ControlOwner));
                }

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables (read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((IAuxiliaryDataControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro pomocná data (read-only)</para>
        /// <para lang="en">Module for auxiliary data setting (read-only)</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((IAuxiliaryDataControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">Pomocná proměnná</para>
        /// <para lang="en">Auxiliary variable</para>
        /// </summary>
        public AuxiliaryVariable AuxiliaryVariable
        {
            get
            {
                return auxiliaryVariable;
            }
            set
            {
                auxiliaryVariable = value;

                InitializeLabels();
            }
        }

        /// <summary>
        /// <para lang="cs">Skupina konfigurací</para>
        /// <para lang="en">Configuration collection</para>
        /// </summary>
        public ConfigCollection ConfigCollection
        {
            get
            {
                return ((ControlConfigCollectionIntro)ControlOwner).ConfigCollection;
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(btnCancel),                  "Zrušit výběr" },
                        { nameof(btnSelect),                  "Vybrat" },
                        { nameof(lblDescriptionCaption),      "Popis:" },
                        { nameof(lblIdCaption),               "ID:" },
                        { nameof(lblLabelCaption),            "Název:" },
                        { nameof(grpMain),                    "Pomocná proměnná:" }
                    }
                    : languageFile.NationalVersion.Data.TryGetValue(
                        key: nameof(ControlAuxiliaryVariableSelector),
                        out Dictionary<string, string> dictNational)
                            ? dictNational
                            : [],

                LanguageVersion.International => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(btnCancel),                  "Cancel selection" },
                        { nameof(btnSelect),                  "Select" },
                        { nameof(lblDescriptionCaption),      "Description:" },
                        { nameof(lblIdCaption),               "ID:" },
                        { nameof(lblLabelCaption),            "Name:" },
                        { nameof(grpMain),                    "Auxiliary variable:" }
                    }
                    : languageFile.InternationalVersion.Data.TryGetValue(
                        key: nameof(ControlAuxiliaryVariableSelector),
                        out Dictionary<string, string> dictInternational)
                            ? dictInternational
                            : [],

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku</para>
        /// <para lang="en">Initialization of the control</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        private void Initialize(Control controlOwner)
        {
            BorderStyle = BorderStyle.None;
            ControlOwner = controlOwner;

            btnCancel.Click += new EventHandler(
                (sender, e) =>
                {
                    AuxiliaryVariable = null;
                    AuxiliaryVariableChanged?.Invoke(sender: this, e: new EventArgs());
                });

            btnSelect.Click += new EventHandler(
               (sender, e) =>
               {
                   FormAuxiliaryVariableSelector frmAuxiliaryVariableSelector =
                        new(controlOwner: this)
                        {
                            AuxiliaryVariable = AuxiliaryVariable
                        };

                   if (frmAuxiliaryVariableSelector.ShowDialog() == DialogResult.OK)
                   {
                       AuxiliaryVariable = frmAuxiliaryVariableSelector.AuxiliaryVariable;
                       AuxiliaryVariableChanged?.Invoke(sender: this, e: new EventArgs());
                   }
               });
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků ovládacího prvku</para>
        /// <para lang="en">Initialization of control labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            btnCancel.Text =
                labels.TryGetValue(key: nameof(btnCancel),
                out string btnCancelText)
                    ? btnCancelText
                    : String.Empty;

            btnSelect.Text =
                labels.TryGetValue(key: nameof(btnSelect),
                out string btnSelectText)
                    ? btnSelectText
                    : String.Empty;


            grpMain.Text =
                labels.TryGetValue(key: nameof(grpMain),
                out string grpMainText)
                    ? grpMainText
                    : String.Empty;

            lblIdCaption.Text =
                labels.TryGetValue(key: nameof(lblIdCaption),
                out string lblIdCaptionText)
                    ? lblIdCaptionText
                    : String.Empty;

            lblIdValue.Text = (AuxiliaryVariable?.Id != null)
                ? AuxiliaryVariable.Id.ToString()
                : String.Empty;


            lblLabelCaption.Text =
                labels.TryGetValue(key: nameof(lblLabelCaption),
                out string lblLabelCaptionText)
                    ? lblLabelCaptionText
                    : String.Empty;

            lblLabelValue.Text =
                (LanguageVersion == LanguageVersion.International)
                    ? AuxiliaryVariable?.LabelEn ?? String.Empty
                    : (LanguageVersion == LanguageVersion.National)
                        ? AuxiliaryVariable?.LabelCs ?? String.Empty
                        : AuxiliaryVariable?.LabelEn ?? String.Empty;


            lblDescriptionCaption.Text =
               labels.TryGetValue(key: nameof(lblDescriptionCaption),
               out string lblDescriptionCaptionText)
                   ? lblDescriptionCaptionText
                   : String.Empty;

            lblDescriptionValue.Text =
                (LanguageVersion == LanguageVersion.International)
                    ? AuxiliaryVariable?.DescriptionEn ?? String.Empty
                    : (LanguageVersion == LanguageVersion.National)
                        ? AuxiliaryVariable?.DescriptionCs ?? String.Empty
                        : AuxiliaryVariable?.DescriptionEn ?? String.Empty;
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent() { }

        #endregion Methods

    }

}
