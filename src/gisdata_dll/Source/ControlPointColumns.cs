﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.AuxiliaryData;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.Catalog;

namespace ZaJi.ModuleAuxiliaryData
{

    /// <summary>
    /// <para lang="cs">Ovládací prvek "Volba sloupců pro výběr z bodové vrstvy"</para>
    /// <para lang="en">Control "Selecting columns for selection from point layer"</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    internal partial class ControlPointColumns
        : UserControl, INfiEstaControl, IAuxiliaryDataControl
    {

        #region Constants

        /// <summary>
        /// <para lang="cs">Výška zaškrtávacího políčka</para>
        /// <para lang="en">CheckBox height</para>
        /// </summary>
        private const int checkBoxHeight = 25;

        #endregion Constants


        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        private Control controlOwner;

        #endregion Private Fields


        #region Events

        /// <summary>
        /// <para lang="cs">Událost kliknutí na tlačítko "Předchozí"</para>
        /// <para lang="en">Click event on the "Previous" button</para>
        /// </summary>
        public event EventHandler PreviousClick;

        /// <summary>
        /// <para lang="cs">Událost kliknutí na tlačítko "Další"</para>
        /// <para lang="en">Click event on the "Next" button</para>
        /// </summary>
        public event EventHandler NextClick;

        #endregion Events


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor ovládacího prvku</para>
        /// <para lang="en">Control constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        public ControlPointColumns(Control controlOwner)
        {
            InitializeComponent();
            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                IAuxiliaryDataControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                if (controlOwner is not FormConfigCollectionGuide)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(FormConfigCollectionGuide)}.",
                        paramName: nameof(ControlOwner));
                }

                return controlOwner;
            }
            set
            {
                IAuxiliaryDataControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                if (value is not FormConfigCollectionGuide)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(FormConfigCollectionGuide)}.",
                        paramName: nameof(ControlOwner));
                }

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables (read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((IAuxiliaryDataControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro pomocná data (read-only)</para>
        /// <para lang="en">Module for auxiliary data setting (read-only)</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((IAuxiliaryDataControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">Skupina konfigurací</para>
        /// <para lang="en">Configuration collection</para>
        /// </summary>
        public ConfigCollection ConfigCollection
        {
            get
            {
                return ((FormConfigCollectionGuide)ControlOwner).ConfigCollection;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové schéma (read-only)</para>
        /// <para lang="en">Database schema (read-only)</para>
        /// </summary>
        private DBSchema Schema
        {
            get
            {
                return
                    Database.Postgres.Catalog.Schemas[ConfigCollection?.SchemaName ?? String.Empty];
            }
        }

        /// <summary>
        /// <para lang="cs">Databázová tabulka (read-only)</para>
        /// <para lang="en">Database table (read-only)</para>
        /// </summary>
        private DBTable Table
        {
            get
            {
                return
                    Schema?.Tables[ConfigCollection?.TableName ?? String.Empty];
            }
        }

        /// <summary>
        /// <para lang="cs">Cizí databázová tabulka (read-only)</para>
        /// <para lang="en">Foreign database table (read-only)</para>
        /// </summary>
        private DBForeignTable ForeignTable
        {
            get
            {
                return Schema?.ForeignTables[ConfigCollection?.TableName ?? String.Empty];
            }
        }

        /// <summary>
        /// <para lang="cs">Seznam zobrazených databázových sloupců (read-only)</para>
        /// <para lang="en">List of displayed database columns (read-only)</para>
        /// </summary>
        private List<WideColumn> DisplayedColumns
        {
            get
            {
                return
                    pnlColumns.Controls.OfType<CheckBox>()
                    .Where(a => a.Tag != null)
                    .Where(a => a.Tag is WideColumn)
                    .Select(a => (WideColumn)a.Tag)
                    .ToList<WideColumn>();
            }
        }

        /// <summary>
        /// <para lang="cs">Seznam vybraných databázových sloupců (read-only)</para>
        /// <para lang="en">List of selected database columns (read-only)</para>
        /// </summary>
        public List<WideColumn> SelectedColumns
        {
            get
            {
                return
                    pnlColumns.Controls.OfType<CheckBox>()
                        .Where(a => a.Enabled)
                        .Where(a => a.Checked)
                        .Where(a => a.Tag != null)
                        .Where(a => a.Tag is WideColumn)
                        .Select(a => (WideColumn)a.Tag)
                        .ToList<WideColumn>();
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(btnAddColumn),                       "Přidat sloupec bez číselníku" },
                        { nameof(btnPrevious),                        "Předchozí" },
                        { nameof(btnNext),                            "Další" },
                        { nameof(chkOption),                          "Žádná výběrová podmínka" },
                        { nameof(grpColumns),                         "Výběr sloupců použitých k sestavení výběrové podmínky:" },
                        { nameof(grpOption),                          String.Empty }
                    }
                    : languageFile.NationalVersion.Data.TryGetValue(
                        key: nameof(ControlPointColumns),
                        out Dictionary<string, string> dictNational)
                            ? dictNational
                            : [],

                LanguageVersion.International => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(btnAddColumn),                       "Add column without lookup table" },
                        { nameof(btnPrevious),                        "Previous" },
                        { nameof(btnNext),                            "Next" },
                        { nameof(chkOption),                          "No select condition" },
                        { nameof(grpColumns),                         "Selection of columns used to build the selection condition:" },
                        { nameof(grpOption),                          String.Empty }
                    }
                    : languageFile.InternationalVersion.Data.TryGetValue(
                        key: nameof(ControlPointColumns),
                        out Dictionary<string, string> dictInternational)
                            ? dictInternational
                            : [],

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku</para>
        /// <para lang="en">Initialization of the control</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        private void Initialize(Control controlOwner)
        {
            BorderStyle = BorderStyle.None;
            ControlOwner = controlOwner;

            InitializeLabels();

            btnPrevious.Click += new EventHandler(
                (sender, e) =>
                {
                    PreviousClick?.Invoke(sender: sender, e: e);
                });

            btnNext.Click += new EventHandler(
               (sender, e) =>
               {
                   NextClick?.Invoke(sender: sender, e: e);
               });

            btnAddColumn.Click += new EventHandler(
                (sender, e) => { AddColumn(); });

            chkOption.CheckedChanged += new EventHandler(
                (sender, e) =>
                {
                    grpColumns.Enabled = !chkOption.Checked;
                    btnAddColumn.Enabled = !chkOption.Checked;
                    EnableNext();
                });
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků ovládacího prvku</para>
        /// <para lang="en">Initialization of control labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            btnAddColumn.Text =
                labels.TryGetValue(key: nameof(btnAddColumn),
                out string btnAddColumnText)
                    ? btnAddColumnText
                    : String.Empty;

            btnPrevious.Text =
                labels.TryGetValue(key: nameof(btnPrevious),
                out string btnPreviousText)
                    ? btnPreviousText
                    : String.Empty;

            btnNext.Text =
                labels.TryGetValue(key: nameof(btnNext),
                out string btnNextText)
                    ? btnNextText
                    : String.Empty;

            chkOption.Text =
                labels.TryGetValue(key: nameof(chkOption),
                out string chkOptionText)
                    ? chkOptionText
                    : String.Empty;

            grpColumns.Text =
                labels.TryGetValue(key: nameof(grpColumns),
                out string grpColumnsText)
                    ? grpColumnsText
                    : String.Empty;

            grpOption.Text =
               labels.TryGetValue(key: nameof(grpOption),
               out string grpOptionText)
                   ? grpOptionText
                   : String.Empty;
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent()
        {
            Condition condition = new(text: ConfigCollection?.Condition);

            // Zaškrtávací políčko
            // -> je zaškrtnuté, pokud není žádná výběrová podmínka
            chkOption.Checked = condition.IsEmpty;

            // Seznam databázových sloupců pro sestavení výběrové podmínky z datové tabulky
            pnlColumns.Controls.Clear();

            if (Table != null)
            {
                // Pro každý jednoduchý cizí klíč, přidá zaškrtávací políčko
                // políčko zaškrtne, pokud sloupec je obsažen ve výběrové podmínce
                foreach (DBForeignKey fkey in Table.ForeignKeys.Items
                        .Where(a => a.ConstraintColumns.Count == 1))
                {
                    AddCheckBox(wideColumn:
                        new WideColumn(
                            owner: this,
                            column: fkey.ConstraintColumns[0],
                            foreignKey: fkey),
                        value: condition.ColumnNames.Contains(value: fkey.ConstraintColumns[0].Name));
                }

                // Doplní sloupce bez cizího klíče, které nejsou zobrazené,
                // pokud jsou obsažené ve výběrové podmínce
                foreach (DBColumn column in
                    Table.Columns.Items
                        .Where(a => !DisplayedColumns.Select(a => a.Column.Name).Contains(value: a.Name))
                        .Where(a => condition.ColumnNames.Contains(value: a.Name)))
                {
                    AddCheckBox(wideColumn:
                        new WideColumn(
                            owner: this,
                            column: column,
                            foreignKey: null),
                        value: true);
                }
            }

            // Seznam databázových sloupců pro sestavení výběrové podmínky z cizí tabulky
            else
            {
                if (ForeignTable != null)
                {
                    // Doplní sloupce, které nejsou zobrazené,
                    // pokud jsou obsažené ve výběrové podmínce
                    foreach (DBColumn column in
                        ForeignTable.Columns.Items
                            .Where(a => !DisplayedColumns.Select(a => a.Column.Name).Contains(value: a.Name))
                            .Where(a => condition.ColumnNames.Contains(value: a.Name)))
                    {
                        AddCheckBox(wideColumn:
                            new WideColumn(
                                owner: this,
                                column: column,
                                foreignKey: null),
                            value: true);
                    }
                }
            }

            OrderCheckBoxes();

            EnableNext();
        }

        /// <summary>
        /// <para lang="cs">Přidává sloupec bez cizího klíče do výběrové podmínky</para>
        /// <para lang="en">Adds a column without a foreign key to the selection condition</para>
        /// </summary>
        private void AddColumn()
        {
            FormTableColumns frmTableColumns = new(controlOwner: this);

            if (Table != null)
            {
                frmTableColumns.Table = Table;

                // Vyřadí již zobrazené sloupce, geometrie a rastry
                frmTableColumns.ExcludedColumns =
                    DisplayedColumns.Select(a => a.Column)
                    .Concat(
                        second: Table.Columns.Items
                                .Where(a => a.TypeName.Contains(
                                    value: "geometry",
                                    comparisonType: StringComparison.InvariantCultureIgnoreCase)))
                    .Concat(
                        second: Table.Columns.Items
                                .Where(a => a.TypeName.Contains(
                                    value: "raster",
                                    comparisonType: StringComparison.InvariantCultureIgnoreCase)))
                    .ToList<DBColumn>();
            }
            else
            {
                if (ForeignTable != null)
                {
                    frmTableColumns.ForeignTable = ForeignTable;

                    // Vyřadí již zobrazené sloupce, geometrie a rastry
                    frmTableColumns.ExcludedColumns =
                        DisplayedColumns.Select(a => a.Column)
                        .Concat(
                            second: ForeignTable.Columns.Items
                                    .Where(a => a.TypeName.Contains(
                                        value: "geometry",
                                        comparisonType: StringComparison.InvariantCultureIgnoreCase)))
                        .Concat(
                            second: ForeignTable.Columns.Items
                                    .Where(a => a.TypeName.Contains(
                                        value: "raster",
                                        comparisonType: StringComparison.InvariantCultureIgnoreCase)))
                        .ToList<DBColumn>();
                }
                else
                {
                    return;
                }
            }

            if (frmTableColumns.ShowDialog() == DialogResult.OK)
            {
                foreach (DBColumn column in frmTableColumns.SelectedColumns)
                {
                    AddCheckBox(wideColumn:
                        new WideColumn(
                            owner: this,
                            column: column,
                            foreignKey: null),
                        value: true);
                }

                OrderCheckBoxes();

                EnableNext();
            }
        }

        /// <summary>
        /// <para lang="cs">Přidá zaškrtávací políčko pro databázový sloupec do seznamu</para>
        /// <para lang="en">Adds check box for database column in the list</para>
        /// </summary>
        /// <param name="wideColumn">
        /// <para lang="cs">Databázový sloupec s cizím klíčem</para>
        /// <para lang="en">Database column with foreign key</para>
        /// </param>
        /// <param name="value">
        /// <para lang="cs">Hodnota zaškrtávacího políčka</para>
        /// <para lang="en">Check box valu</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Zaškrtávací políčko pro databázový sloupec do seznamu</para>
        /// <para lang="en">Check box for database column in the list</para>
        /// </returns>
        private CheckBox AddCheckBox(
            WideColumn wideColumn,
            bool value)
        {
            if (
                pnlColumns.Controls.OfType<CheckBox>()
                .Where(a => a.Tag != null)
                .Where(a => a.Tag is WideColumn)
                .Where(a => ((WideColumn)a.Tag).Column.Name == wideColumn.Column.Name)
                .Any())
            {
                // Databázový sloupec se v seznamu již nachází
                return
                    pnlColumns.Controls.OfType<CheckBox>()
                        .Where(a => a.Tag != null)
                        .Where(a => a.Tag is WideColumn)
                        .Where(a => ((WideColumn)a.Tag).Column.Name == wideColumn.Column.Name)
                        .FirstOrDefault<CheckBox>();
            }

            CheckBox checkBox = new()
            {
                Checked = value,
                Dock = DockStyle.Top,
                Font = Setting.CheckBoxFont,
                ForeColor = Setting.CheckBoxForeColor,
                Height = checkBoxHeight,
                Padding = new Padding(left: 10, top: 0, right: 0, bottom: 0),
                Tag = wideColumn,
                Text = $"{wideColumn.Column.Name ?? String.Empty}"
            };

            checkBox.CheckedChanged += new EventHandler(
                (sender, e) =>
                {
                    if (ConfigCollection != null)
                    {
                        // Pokud změna v seznamu zaškrtnutých sloupců pro podmínku
                        // pak reset podmínky
                        ConfigCollection.Condition = null;
                    }

                    EnableNext();
                });

            checkBox.CreateControl();

            pnlColumns.Controls.Add(value: checkBox);

            return checkBox;
        }

        /// <summary>
        /// <para lang="cs">Seřazení zaškrtávacích políček pro databázové sloupce</para>
        /// <para lang="en">Sorting checkboxes for database columns</para>
        /// </summary>
        private void OrderCheckBoxes()
        {
            foreach (var pair in
                pnlColumns.Controls.OfType<CheckBox>()
                .Where(a => a.Tag != null)
                .Where(a => a.Tag is WideColumn)
                .OrderByDescending(a => ((WideColumn)a.Tag).Column.Name) // řadit abecedně podle jména sloupce
                .Select((a, i) => new { CheckBox = a, Index = i }))
            {
                pnlColumns.Controls.SetChildIndex(
                    child: pair.CheckBox,
                    newIndex: pair.Index);
            }
        }

        /// <summary>
        /// <para lang="cs">Povolí postup na další formulář</para>
        /// <para lang="en">Allows progression to the next form</para>
        /// </summary>
        private void EnableNext()
        {
            if (ConfigCollection == null)
            {
                btnNext.Enabled = false;
                return;
            }

            if (chkOption.Checked)
            {
                ConfigCollection.Condition = null;
                btnNext.Enabled =
                   !String.IsNullOrEmpty(ConfigCollection.LabelCs) &&
                   !String.IsNullOrEmpty(ConfigCollection.LabelEn) &&
                   !String.IsNullOrEmpty(ConfigCollection.DescriptionCs) &&
                   !String.IsNullOrEmpty(ConfigCollection.DescriptionEn) &&
                   (ConfigCollection.ConfigFunctionValue == ConfigFunctionEnum.PointLayer) &&
                   !String.IsNullOrEmpty(value: ConfigCollection?.CatalogName) &&
                   !String.IsNullOrEmpty(value: ConfigCollection?.SchemaName) &&
                   !String.IsNullOrEmpty(value: ConfigCollection?.TableName) &&
                   !String.IsNullOrEmpty(value: ConfigCollection?.ColumnName) &&
                   !String.IsNullOrEmpty(value: ConfigCollection?.Tag);
            }
            else
            {
                btnNext.Enabled =
                  !String.IsNullOrEmpty(ConfigCollection.LabelCs) &&
                   !String.IsNullOrEmpty(ConfigCollection.LabelEn) &&
                   !String.IsNullOrEmpty(ConfigCollection.DescriptionCs) &&
                   !String.IsNullOrEmpty(ConfigCollection.DescriptionEn) &&
                   (ConfigCollection.ConfigFunctionValue == ConfigFunctionEnum.PointLayer) &&
                   !String.IsNullOrEmpty(value: ConfigCollection?.CatalogName) &&
                   !String.IsNullOrEmpty(value: ConfigCollection?.SchemaName) &&
                   !String.IsNullOrEmpty(value: ConfigCollection?.TableName) &&
                   !String.IsNullOrEmpty(value: ConfigCollection?.ColumnName) &&
                   !String.IsNullOrEmpty(value: ConfigCollection?.Tag) &&
                    SelectedColumns.Count > 0;
            }
        }

        #endregion Methods

    }

}
