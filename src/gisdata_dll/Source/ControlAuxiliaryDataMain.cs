﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.AuxiliaryData;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.Catalog;

namespace ZaJi.ModuleAuxiliaryData
{

    /// <summary>
    /// <para lang="cs">Ovládací prvek "Modul pro pomocná data"</para>
    /// <para lang="en">Control "Module for auxiliary data"</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    public partial class ControlAuxiliaryDataMain
        : UserControl, INfiEstaControl, IAuxiliaryDataControl
    {

        #region Constants

        private const int LevelConfigFunction = 0;
        private const int LevelConfigCollection = 1;
        private const int LevelConfiguration = 2;

        private const int ImageRedBall = 0;
        private const int ImageBlueBall = 1;
        private const int ImageYellowBall = 2;
        private const int ImageGreenBall = 3;
        private const int ImageGrayBall = 4;
        private const int ImageLock = 5;
        private const int ImageSchema = 6;
        private const int ImageVector = 7;
        private const int ImageRaster = 8;

        #endregion Constants


        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Uzly v TreeView</para>
        /// <para lang="en">TreeView nodes</para>
        /// </summary>
        private List<TreeNode> nodes;

        private string msgConnectionNotInitialized = String.Empty;
        private string msgDelConfCollContainsDependent = String.Empty;
        private string msgDelConfCollContainsDependentConclusion = String.Empty;
        private string msgDelConfCollContainsCalculatedAuxTotals = String.Empty;
        private string msgDelConfCollContainsCalculatedAuxData = String.Empty;
        private string msgDelConfCollIsNotEmpty = String.Empty;
        private string msgDelConfCollIsClosed = String.Empty;
        private string msgCloseConfCollIsEmpty = String.Empty;
        private string msgCloseConfCollIsClosed = String.Empty;
        private string msgOpenConfCollContainsDependent = String.Empty;
        private string msgOpenConfCollContainsCalculatedAuxTotals = String.Empty;
        private string msgOpenConfCollContainsCalculatedAuxData = String.Empty;
        private string msgOpenConfCollIsOpened = String.Empty;
        private string msgDelConfigContainsDependent = String.Empty;
        private string msgDelConfigContainsDependentConclusion = String.Empty;
        private string msgDelConfigContainsDependentList = String.Empty;
        private string msgDelConfigContainsCalculatedAuxTotals = String.Empty;
        private string msgDelConfigContainsCalculatedAuxData = String.Empty;

        #endregion Private Fields


        #region Controls

        /// <summary>
        /// <para lang="cs">Zobrazení parametrů skupiny konfigurací</para>
        /// <para lang="en">Control "Display config collection parameters"</para>
        /// </summary>
        private ControlConfigCollectionView ctrConfigCollectionView;

        /// <summary>
        /// <para lang="cs">Zobrazení parametrů konfigurace</para>
        /// <para lang="en">Control "Display configuration parameters"</para>
        /// </summary>
        private ControlConfigurationView ctrConfigurationView;


        /// <summary>
        /// <para lang="cs">Zobrazení parametrů skupiny konfigurací</para>
        /// <para lang="en">Control "Display config collection parameters"</para>
        /// </summary>
        private ControlConfigCollectionView CtrConfigCollectionView
        {
            get
            {
                return ctrConfigCollectionView;
            }
            set
            {
                ctrConfigCollectionView = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Zobrazení parametrů konfigurace</para>
        /// <para lang="en">Control "Display configuration parameters"</para>
        /// </summary>
        private ControlConfigurationView CtrConfigurationView
        {
            get
            {
                return ctrConfigurationView;
            }
            set
            {
                ctrConfigurationView = value;
            }
        }

        #endregion Controls


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor ovládacího prvku</para>
        /// <para lang="en">Control constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        public ControlAuxiliaryDataMain(Control controlOwner)
        {
            InitializeComponent();
            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                IAuxiliaryDataControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                return controlOwner;
            }
            set
            {
                IAuxiliaryDataControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables (read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((IAuxiliaryDataControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro pomocná data (read-only)</para>
        /// <para lang="en">Module for auxiliary data setting (read-only)</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((IAuxiliaryDataControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">Uzly v TreeView</para>
        /// <para lang="en">TreeView nodes</para>
        /// </summary>
        private List<TreeNode> Nodes
        {
            get
            {
                return nodes ?? [];
            }
            set
            {
                nodes = value ?? [];
            }
        }

        /// <summary>
        /// <para lang="cs">Uzly v TreeView úroveň výpočetních funkcí</para>
        /// <para lang="en">TreeView nodes - ConfigFunctions level</para>
        /// </summary>
        private IEnumerable<TreeNode> ConfigFunctionNodes
        {
            get
            {
                return
                    Nodes
                        .Where(a => a.Level == LevelConfigFunction)
                        .Where(a => a.Tag != null)
                        .Where(a => a.Tag is ConfigFunction);
            }
        }

        /// <summary>
        /// <para lang="cs">Uzly v TreeView úroveň skupin konfigurací</para>
        /// <para lang="en">TreeView nodes - ConfigCollections level</para>
        /// </summary>
        private IEnumerable<TreeNode> ConfigCollectionNodes
        {
            get
            {
                return
                    Nodes
                        .Where(a => a.Level == LevelConfigCollection)
                        .Where(a => a.Tag != null)
                        .Where(a => a.Tag is ConfigCollection);
            }
        }

        /// <summary>
        /// <para lang="cs">Uzly v TreeView úroveň konfigurací</para>
        /// <para lang="en">TreeView nodes - Configurations level</para>
        /// </summary>
        private IEnumerable<TreeNode> ConfigurationNodes
        {
            get
            {
                return
                    Nodes
                        .Where(a => a.Level == LevelConfiguration)
                        .Where(a => a.Tag != null)
                        .Where(a => a.Tag is Config);
            }
        }

        /// <summary>
        /// <para lang="cs">Vybraná výpočetní funkce</para>
        /// <para lang="en">Selected config function</para>
        /// </summary>
        private ConfigFunction SelectedConfigFunction
        {
            get
            {
                if (tvwConfigurations.SelectedNode == null)
                {
                    return null;
                }

                if (tvwConfigurations.SelectedNode.Tag == null)
                {
                    return null;
                }

                if (tvwConfigurations.SelectedNode.Tag is not ConfigFunction)
                {
                    return null;
                }

                return tvwConfigurations.SelectedNode.Level switch
                {
                    LevelConfigFunction => (ConfigFunction)tvwConfigurations.SelectedNode.Tag,
                    LevelConfigCollection => null,
                    LevelConfiguration => null,
                    _ => null,
                };
            }
        }

        /// <summary>
        /// <para lang="cs">Vybraná skupina konfigurací</para>
        /// <para lang="en">Selected config collection</para>
        /// </summary>
        private ConfigCollection SelectedConfigCollection
        {
            get
            {
                if (tvwConfigurations.SelectedNode == null)
                {
                    return null;
                }

                if (tvwConfigurations.SelectedNode.Tag == null)
                {
                    return null;
                }

                if (tvwConfigurations.SelectedNode.Tag is not ConfigCollection)
                {
                    return null;
                }

                return tvwConfigurations.SelectedNode.Level switch
                {
                    LevelConfigFunction => null,
                    LevelConfigCollection => (ConfigCollection)tvwConfigurations.SelectedNode.Tag,
                    LevelConfiguration => null,
                    _ => null,
                };
            }
        }

        /// <summary>
        /// <para lang="cs">Vybraná konfigurace</para>
        /// <para lang="en">Selected configuration</para>
        /// </summary>
        public Config SelectedConfiguration
        {
            get
            {
                if (tvwConfigurations.SelectedNode == null)
                {
                    return null;
                }

                if (tvwConfigurations.SelectedNode.Tag == null)
                {
                    return null;
                }

                if (tvwConfigurations.SelectedNode.Tag is not Config)
                {
                    return null;
                }

                return tvwConfigurations.SelectedNode.Level switch
                {
                    LevelConfigFunction => null,
                    LevelConfigCollection => null,
                    LevelConfiguration => (Config)tvwConfigurations.SelectedNode.Tag,
                    _ => null,
                };
            }
        }

        /// <summary>
        /// <para lang="cs">Dostupná skupina konfigurací</para>
        /// <para lang="en">Available config collection</para>
        /// </summary>
        public ConfigCollection AvailableConfigCollection
        {
            get
            {
                if (SelectedConfigCollection != null) { return SelectedConfigCollection; }

                if (SelectedConfiguration != null) { return SelectedConfiguration.ConfigCollection; }

                return null;
            }
        }

        /// <summary>
        /// <para lang="cs">Aktuální verze databázové extenze</para>
        /// <para lang="en">Current database extension version</para>
        /// </summary>
        public ZaJi.NfiEstaPg.AuxiliaryData.ExtensionVersion ExtensionVersion
        {
            get
            {
                DBExtension dbExtension =
                    Database.Postgres.Catalog.Extensions.Items
                        .Where(a => a.Name == ADSchema.ExtensionName)
                        .FirstOrDefault();

                ZaJi.PostgreSQL.DataModel.ExtensionVersion dbExtensionVersion =
                    (dbExtension != null)
                        ? new(version: dbExtension?.Version)
                        : null;

                ZaJi.NfiEstaPg.AuxiliaryData.ExtensionVersion extensionVersion =
                    (dbExtensionVersion != null)
                        ? Database.SAuxiliaryData.CExtensionVersion.Items
                            .Where(a => dbExtensionVersion.ToString() ==
                                    (String.IsNullOrEmpty(value: a.LabelEn.Trim())
                                        ? a.LabelCs.Trim()
                                        : a.LabelEn.Trim()))
                            .FirstOrDefault<ZaJi.NfiEstaPg.AuxiliaryData.ExtensionVersion>()
                        : null;

                return
                    extensionVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Aktuální verze modulu pro pomocná data</para>
        /// <para lang="en">Current module for auxiliary data version</para>
        /// </summary>
        public ZaJi.NfiEstaPg.AuxiliaryData.GUIVersion GUIVersion
        {
            get
            {
                ZaJi.NfiEstaPg.AuxiliaryData.GUIVersion version =
                    Database.SAuxiliaryData.CGUIVersion.Items
                            .Where(a => Setting.ModuleVersion ==
                                    (String.IsNullOrEmpty(value: a.LabelEn.Trim())
                                        ? a.LabelCs.Trim()
                                        : a.LabelEn.Trim()))
                            .FirstOrDefault<ZaJi.NfiEstaPg.AuxiliaryData.GUIVersion>();

                return version;
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(btnDeleteConfiguration),                       "Smazat vybraná pomocná data nebo veličinu" },
                        { nameof(btnLoadConfigurationList),                     "Načíst pomocná data z databáze"  },
                        { nameof(btnLockConfigCollection),                      "Uzavřít pomocná data"  },
                        { nameof(btnNewConfigCollection),                       "Přidat nová pomocná data"  },
                        { nameof(btnNewConfiguration),                          "Definovat veličinu ve vybraných pomocných datech"  },
                        { nameof(btnUnlockConfigCollection),                    "Otevřít pomocná data"  },
                        { nameof(btnUpdateConfiguration),                       "Editovat vybraná pomocná data nebo veličinu"  },
                        { nameof(btnViewData),                                  "Zobrazit vypočtené úhrny pomocné proměnné"  },
                        { nameof(cmsConfigCollection),                          String.Empty  },
                        { nameof(cmsConfigFunction),                            String.Empty  },
                        { nameof(cmsConfiguration),                             String.Empty  },
                        { nameof(cmsLockedConfigCollection),                    String.Empty  },
                        { nameof(grpConfigurationAttributes),                   String.Empty  },
                        { nameof(grpConfigurations),                            String.Empty  },
                        { nameof(grpSQL),                                       "SQL příkaz pro výpočet úhrnů pomocné proměnné:"  },
                        { nameof(tpParameters),                                 "Parametry" },
                        { nameof(tpSQL),                                        "SQL" },
                        { nameof(tsmiDeleteConfigCollection),                   "Smazat vybraná pomocná data" },
                        { nameof(tsmiDeleteConfiguration),                      "Smazat vybranou veličinu" },
                        { nameof(tsmiLockConfigCollection),                     "Uzavřít pomocná data"  },
                        { nameof(tsmiNewConfigCollection),                      "Přidat nová pomocná data"   },
                        { nameof(tsmiNewConfiguration),                         "Definovat veličinu"  },
                        { nameof(tsmiUnlockConfigCollection),                   "Otevřít pomocná data"   },
                        { nameof(tsmiUpdateConfigCollection),                   "Editovat vybraná pomocná data"  },
                        { nameof(tsmiUpdateConfiguration),                      "Editovat vybranou veličinu" },
                        { nameof(tsmiViewData),                                 "Zobrazit vypočtené úhrny pomocné proměnné"  },
                        { nameof(tsrConfiguration),                             String.Empty },
                        { nameof(msgConnectionNotInitialized),                  "Připojení k databázi není inicializováno." },
                        { nameof(msgDelConfCollContainsDependent),              "Skupinu konfigurací nelze smazat, protože se na ni odkazují další skupiny konfigurací." },
                        { nameof(msgDelConfCollContainsDependentConclusion),    "Před odstraněním skupiny konfigurací odstraňte nejdříve všechny závislé skupiny konfigurace:" },
                        { nameof(msgDelConfCollContainsCalculatedAuxTotals),    "Skupinu konfigurací nelze smazat, protože obsahuje konfigurace s vypočtenými úhrny pomocné proměnné." },
                        { nameof(msgDelConfCollContainsCalculatedAuxData),      "Skupinu konfigurací nelze smazat, protože obsahuje konfigurace s provedeným protínáním bodové vrstvy s vrstvou úhrnů pomocné proměnné." },
                        { nameof(msgDelConfCollIsNotEmpty),                     "Skupinu konfigurací nelze smazat, protože není prázdná." },
                        { nameof(msgDelConfCollIsClosed),                       "Skupinu konfigurací nelze smazat, protože je uzavřená." },
                        { nameof(msgCloseConfCollIsEmpty),                      "Skupinu konfigurací nelze uzavřít, protože je prázdná." },
                        { nameof(msgCloseConfCollIsClosed),                     "Skupinu konfigurací nelze uzavřít, protože už je uzavřená." },
                        { nameof(msgOpenConfCollContainsDependent),             "Skupinu konfigurací nelze otevřít, protože se na ni odkazují další skupiny konfigurací." },
                        { nameof(msgOpenConfCollContainsCalculatedAuxTotals),   "Skupinu konfigurací nelze otevřít, protože obsahuje konfigurace s vypočtenými úhrny pomocné proměnné." },
                        { nameof(msgOpenConfCollContainsCalculatedAuxData),     "Skupinu konfigurací nelze otevřít, protože obsahuje konfigurace s provedeným protínáním bodové vrstvy s vrstvou úhrnů pomocné proměnné." },
                        { nameof(msgOpenConfCollIsOpened),                      "Skupinu konfigurací nelze otevřít, protože už je otevřená." },
                        { nameof(msgDelConfigContainsDependent),                "Konfiguraci nelze smazat, protože se na ni odkazují další konfigurace." },
                        { nameof(msgDelConfigContainsDependentConclusion),      "Před odstraněním konfigurace smažte nejdříve všechny závislé konfigurace:" },
                        { nameof(msgDelConfigContainsDependentList),            "$1 ve skupině ($2)" },
                        { nameof(msgDelConfigContainsCalculatedAuxTotals),      "Konfiguraci nelze smazat, protože obsahuje vypočtené úhrny pomocné proměnné." },
                        { nameof(msgDelConfigContainsCalculatedAuxData),        "Konfiguraci nelze smazat, protože obsahuje vypočtené protínání bodové vrstvy s vrstvou úhrnů pomocné proměnné." }
                    }
                    : languageFile.NationalVersion.Data.TryGetValue(
                        key: nameof(ControlAuxiliaryDataMain),
                        out Dictionary<string, string> dictNational)
                            ? dictNational
                            : [],

                LanguageVersion.International => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(btnDeleteConfiguration),                       "Delete selected auxiliary data or variable" },
                        { nameof(btnLoadConfigurationList),                     "Reload auxiliary data from database"  },
                        { nameof(btnLockConfigCollection),                      "Lock auxiliary data"  },
                        { nameof(btnNewConfigCollection),                       "Add new auxiliary data"  },
                        { nameof(btnNewConfiguration),                          "Define variable in selected auxiliary data"  },
                        { nameof(btnUnlockConfigCollection),                    "Open auxiliary data"  },
                        { nameof(btnUpdateConfiguration),                       "Edit selected auxiliary data or variable"  },
                        { nameof(btnViewData),                                  "Display calculated auxiliary variable totals"  },
                        { nameof(cmsConfigCollection),                          String.Empty  },
                        { nameof(cmsConfigFunction),                            String.Empty  },
                        { nameof(cmsConfiguration),                             String.Empty  },
                        { nameof(cmsLockedConfigCollection),                    String.Empty  },
                        { nameof(grpConfigurationAttributes),                   String.Empty  },
                        { nameof(grpConfigurations),                            String.Empty  },
                        { nameof(grpSQL),                                       "SQL command for calculation auxiliary variable totals:"  },
                        { nameof(tpParameters),                                 "Parameters" },
                        { nameof(tpSQL),                                        "SQL" },
                        { nameof(tsmiDeleteConfigCollection),                   "Delete selected auxiliary data"  },
                        { nameof(tsmiDeleteConfiguration),                      "Delete selected variable" },
                        { nameof(tsmiLockConfigCollection),                     "Lock auxiliary data" },
                        { nameof(tsmiNewConfigCollection),                      "Add new auxiliary data"  },
                        { nameof(tsmiNewConfiguration),                         "Define variable"  },
                        { nameof(tsmiUnlockConfigCollection),                   "Open auxiliary data"   },
                        { nameof(tsmiUpdateConfigCollection),                   "Edit selected auxiliary data"  },
                        { nameof(tsmiUpdateConfiguration),                      "Edit selected variable" },
                        { nameof(tsmiViewData),                                 "Display calculated auxiliary variable totals"  },
                        { nameof(tsrConfiguration),                             String.Empty },
                        { nameof(msgConnectionNotInitialized),                  "Connection to database is not initialized."},
                        { nameof(msgDelConfCollContainsDependent),              "Configuration collection cannot be deleted because it is referenced by other configuration collections." },
                        { nameof(msgDelConfCollContainsDependentConclusion),    "Before deleting a configuration collection, first delete all dependent configuration collections:" },
                        { nameof(msgDelConfCollContainsCalculatedAuxTotals),    "Configuration collection cannot be deleted because it contains configurations with calculated auxiliary variable totals." },
                        { nameof(msgDelConfCollContainsCalculatedAuxData),      "Configuration collection cannot be deleted because it contains configurations with calculated intersections between point layer and auxiliary variable totals layer." },
                        { nameof(msgDelConfCollIsNotEmpty),                     "Configuration collection cannot be deleted because it is not empty." },
                        { nameof(msgDelConfCollIsClosed),                       "Configuration collection cannot be deleted because it is closed." },
                        { nameof(msgCloseConfCollIsEmpty),                      "Configuration collection cannot be closed because it is empty." },
                        { nameof(msgCloseConfCollIsClosed),                     "Configuration collection cannot be closed because it is already closed." },
                        { nameof(msgOpenConfCollContainsDependent),             "Configuration collection cannot be opened because it is referenced by other configuration collections." },
                        { nameof(msgOpenConfCollContainsCalculatedAuxTotals),   "Configuration collection cannot be opened because it contains configurations with calculated auxiliary variable totals." },
                        { nameof(msgOpenConfCollContainsCalculatedAuxData),     "Configuration collection cannot be opened because it contains configurations with calculated intersections between point layer and auxiliary variable totals layer." },
                        { nameof(msgOpenConfCollIsOpened),                      "Configuration collection cannot be opened because it is already opened." },
                        { nameof(msgDelConfigContainsDependent),                "Configuration cannot be deleted because it is referenced by other configurations." },
                        { nameof(msgDelConfigContainsDependentConclusion),      "Before deleting a configuration, first delete all dependent configurations:"},
                        { nameof(msgDelConfigContainsDependentList),            "$1 in group ($2)" },
                        { nameof(msgDelConfigContainsCalculatedAuxTotals),      "Configuration cannot be deleted because it contains calculated auxiliary variable totals." },
                        { nameof(msgDelConfigContainsCalculatedAuxData),        "Configuration cannot be deleted because it contains calculated intersections between point layer and auxiliary variable totals layer." }
                    }
                    : languageFile.InternationalVersion.Data.TryGetValue(
                        key: nameof(ControlAuxiliaryDataMain),
                        out Dictionary<string, string> dictInternational)
                            ? dictInternational
                            : [],

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku</para>
        /// <para lang="en">Initialization of the control</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        private void Initialize(Control controlOwner)
        {
            BorderStyle = BorderStyle.None;

            ControlOwner = controlOwner;
            Nodes = [];

            CtrConfigCollectionView =
                new(controlOwner: this)
                {
                    ConfigCollection = SelectedConfigCollection,
                    Dock = DockStyle.Fill,
                    RowNumbers = false
                };
            CtrConfigCollectionView.CreateControl();
            pnlParameters.Controls.Add(value: CtrConfigCollectionView);

            CtrConfigurationView =
                new(controlOwner: this)
                {
                    Configuration = SelectedConfiguration,
                    Dock = DockStyle.Fill,
                    RowNumbers = false
                };
            CtrConfigurationView.CreateControl();
            pnlParameters.Controls.Add(value: CtrConfigurationView);

            SetButtonsVisibility();

            SetButtonsAvailability();

            SetGroupBoxesVisiblity();

            SetParams();

            SetSQL();

            InitializeLabels();

            btnDeleteConfiguration.Click += new EventHandler(
                (sender, e) => { DeleteItem(); });

            btnLoadConfigurationList.Click += new EventHandler(
                (sender, e) => { LoadContent(catalog: false, nfiesta: false, auxDataStatic: false, auxDataConfig: true, auxDataStats: false); });

            btnLockConfigCollection.Click += new EventHandler(
                (sender, e) => { CloseConfigCollection(); });

            btnNewConfigCollection.Click += new EventHandler(
               (sender, e) => { NewConfigCollection(); });

            btnNewConfiguration.Click += new EventHandler(
                (sender, e) => { NewConfiguration(); });

            btnUnlockConfigCollection.Click += new EventHandler(
                (sender, e) => { OpenConfigCollection(); });

            btnUpdateConfiguration.Click += new EventHandler(
                (sender, e) => { UpdateItem(); });

            btnViewData.Click += new EventHandler(
                (sender, e) => { DisplayData(); });

            Load += new EventHandler(
                (sender, e) => { InitializeLabels(); });

            tsmiDeleteConfigCollection.Click += new EventHandler(
                (sender, e) => { DeleteItem(); });

            tsmiDeleteConfiguration.Click += new EventHandler(
                (sender, e) => { DeleteItem(); });

            tsmiLockConfigCollection.Click += new EventHandler(
                (sender, e) => { CloseConfigCollection(); });

            tsmiNewConfigCollection.Click += new EventHandler(
                (sender, e) => { NewConfigCollection(); });

            tsmiNewConfiguration.Click += new EventHandler(
                (sender, e) => { NewConfiguration(); });

            tsmiUnlockConfigCollection.Click += new EventHandler(
                 (sender, e) => { OpenConfigCollection(); });

            tsmiUpdateConfigCollection.Click += new EventHandler(
                (sender, e) => { UpdateItem(); });

            tsmiUpdateConfiguration.Click += new EventHandler(
                (sender, e) => { UpdateItem(); });

            tsmiViewData.Click += new EventHandler(
                (sender, e) => { DisplayData(); });

            tvwConfigurations.AfterSelect += new TreeViewEventHandler(
                (sender, e) =>
                {
                    SetButtonsAvailability();

                    SetGroupBoxesVisiblity();

                    SetParams();

                    SetSQL();
                });
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků ovládacího prvku</para>
        /// <para lang="en">Initialization of control labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
               languageVersion: LanguageVersion,
               languageFile: LanguageFile);

            btnDeleteConfiguration.Text =
                labels.TryGetValue(key: nameof(btnDeleteConfiguration),
                out string btnDeleteConfigurationText)
                    ? btnDeleteConfigurationText
                    : String.Empty;

            btnLoadConfigurationList.Text =
                labels.TryGetValue(key: nameof(btnLoadConfigurationList),
                out string btnLoadConfigurationListText)
                    ? btnLoadConfigurationListText
                    : String.Empty;

            btnLockConfigCollection.Text =
                labels.TryGetValue(key: nameof(btnLockConfigCollection),
                out string btnLockConfigCollectionText)
                    ? btnLockConfigCollectionText
                    : String.Empty;

            btnNewConfigCollection.Text =
                labels.TryGetValue(key: nameof(btnNewConfigCollection),
                out string btnNewConfigCollectionText)
                    ? btnNewConfigCollectionText
                    : String.Empty;

            btnNewConfiguration.Text =
                labels.TryGetValue(key: nameof(btnNewConfiguration),
                out string btnNewConfigurationText)
                    ? btnNewConfigurationText
                    : String.Empty;

            btnUnlockConfigCollection.Text =
                labels.TryGetValue(key: nameof(btnUnlockConfigCollection),
                out string btnUnlockConfigCollectionText)
                    ? btnUnlockConfigCollectionText
                    : String.Empty;

            btnUpdateConfiguration.Text =
                labels.TryGetValue(key: nameof(btnUpdateConfiguration),
                out string btnUpdateConfigurationText)
                    ? btnUpdateConfigurationText
                    : String.Empty;

            btnViewData.Text =
                labels.TryGetValue(key: nameof(btnViewData),
                out string btnViewDataText)
                    ? btnViewDataText
                    : String.Empty;

            cmsConfigCollection.Text =
                labels.TryGetValue(key: nameof(cmsConfigCollection),
                out string cmsConfigCollectionText)
                    ? cmsConfigCollectionText
                    : String.Empty;

            cmsConfigFunction.Text =
                labels.TryGetValue(key: nameof(cmsConfigFunction),
                out string cmsConfigFunctionText)
                    ? cmsConfigFunctionText
                    : String.Empty;

            cmsConfiguration.Text =
                labels.TryGetValue(key: nameof(cmsConfiguration),
                out string cmsConfigurationText)
                    ? cmsConfigurationText
                    : String.Empty;

            cmsLockedConfigCollection.Text =
                labels.TryGetValue(key: nameof(cmsLockedConfigCollection),
                out string cmsLockedConfigCollectionText)
                    ? cmsLockedConfigCollectionText
                    : String.Empty;

            grpConfigurationAttributes.Text =
                labels.TryGetValue(key: nameof(grpConfigurationAttributes),
                out string grpConfigurationAttributesText)
                    ? grpConfigurationAttributesText
                    : String.Empty;

            grpConfigurations.Text =
                labels.TryGetValue(key: nameof(grpConfigurations),
                out string grpConfigurationsText)
                    ? grpConfigurationsText
                    : String.Empty;

            grpSQL.Text =
                labels.TryGetValue(key: nameof(grpSQL),
                out string grpSQLText)
                    ? grpSQLText
                    : String.Empty;

            tpParameters.Text =
                labels.TryGetValue(key: nameof(tpParameters),
                out string tpParametersText)
                    ? tpParametersText
                    : String.Empty;

            tpSQL.Text =
                labels.TryGetValue(key: nameof(tpSQL),
                out string tpSQLText)
                    ? tpSQLText
                    : String.Empty;

            tsmiDeleteConfigCollection.Text =
                labels.TryGetValue(key: nameof(tsmiDeleteConfigCollection),
                out string tsmiDeleteConfigCollectionText)
                    ? tsmiDeleteConfigCollectionText
                    : String.Empty;

            tsmiDeleteConfiguration.Text =
                labels.TryGetValue(key: nameof(tsmiDeleteConfiguration),
                out string tsmiDeleteConfigurationText)
                    ? tsmiDeleteConfigurationText
                    : String.Empty;

            tsmiLockConfigCollection.Text =
                labels.TryGetValue(key: nameof(tsmiLockConfigCollection),
                out string tsmiLockConfigCollectionText)
                    ? tsmiLockConfigCollectionText
                    : String.Empty;

            tsmiNewConfigCollection.Text =
                labels.TryGetValue(key: nameof(tsmiNewConfigCollection),
                out string tsmiNewConfigCollectionText)
                    ? tsmiNewConfigCollectionText
                    : String.Empty;

            tsmiNewConfiguration.Text =
                labels.TryGetValue(key: nameof(tsmiNewConfiguration),
                out string tsmiNewConfigurationText)
                    ? tsmiNewConfigurationText
                    : String.Empty;

            tsmiUnlockConfigCollection.Text =
                labels.TryGetValue(key: nameof(tsmiUnlockConfigCollection),
                out string tsmiUnlockConfigCollectionText)
                    ? tsmiUnlockConfigCollectionText
                    : String.Empty;

            tsmiUpdateConfigCollection.Text =
                labels.TryGetValue(key: nameof(tsmiUpdateConfigCollection),
                out string tsmiUpdateConfigCollectionText)
                    ? tsmiUpdateConfigCollectionText
                    : String.Empty;

            tsmiUpdateConfiguration.Text =
                labels.TryGetValue(key: nameof(tsmiUpdateConfiguration),
                out string tsmiUpdateConfigurationText)
                    ? tsmiUpdateConfigurationText
                    : String.Empty;

            tsmiViewData.Text =
                labels.TryGetValue(key: nameof(tsmiViewData),
                out string tsmiViewDataText)
                    ? tsmiViewDataText
                    : String.Empty;

            tsrConfiguration.Text =
               labels.TryGetValue(key: nameof(tsrConfiguration),
               out string tsrConfigurationText)
                    ? tsrConfigurationText
                    : String.Empty;

            msgConnectionNotInitialized =
                labels.TryGetValue(key: nameof(msgConnectionNotInitialized),
                out msgConnectionNotInitialized)
                ? msgConnectionNotInitialized
                : String.Empty;

            msgDelConfCollContainsDependent =
                labels.TryGetValue(key: nameof(msgDelConfCollContainsDependent),
                out msgDelConfCollContainsDependent)
                ? msgDelConfCollContainsDependent
                : String.Empty;

            msgDelConfCollContainsDependentConclusion =
                labels.TryGetValue(key: nameof(msgDelConfCollContainsDependentConclusion),
                out msgDelConfCollContainsDependentConclusion)
                ? msgDelConfCollContainsDependentConclusion
                : String.Empty;

            msgDelConfCollContainsCalculatedAuxTotals =
                labels.TryGetValue(key: nameof(msgDelConfCollContainsCalculatedAuxTotals),
                out msgDelConfCollContainsCalculatedAuxTotals)
                ? msgDelConfCollContainsCalculatedAuxTotals
                : String.Empty;

            msgDelConfCollContainsCalculatedAuxData =
                labels.TryGetValue(key: nameof(msgDelConfCollContainsCalculatedAuxData),
                out msgDelConfCollContainsCalculatedAuxData)
                ? msgDelConfCollContainsCalculatedAuxData
                : String.Empty;

            msgDelConfCollIsNotEmpty =
                labels.TryGetValue(key: nameof(msgDelConfCollIsNotEmpty),
                out msgDelConfCollIsNotEmpty)
                ? msgDelConfCollIsNotEmpty
                : String.Empty;

            msgDelConfCollIsClosed =
                labels.TryGetValue(key: nameof(msgDelConfCollIsClosed),
                out msgDelConfCollIsClosed)
                ? msgDelConfCollIsClosed
                : String.Empty;

            msgCloseConfCollIsEmpty =
                labels.TryGetValue(key: nameof(msgCloseConfCollIsEmpty),
                out msgCloseConfCollIsEmpty)
                ? msgCloseConfCollIsEmpty
                : String.Empty;

            msgCloseConfCollIsClosed =
                labels.TryGetValue(key: nameof(msgCloseConfCollIsClosed),
                out msgCloseConfCollIsClosed)
                ? msgCloseConfCollIsClosed
                : String.Empty;

            msgOpenConfCollContainsDependent =
                labels.TryGetValue(key: nameof(msgOpenConfCollContainsDependent),
                out msgOpenConfCollContainsDependent)
                ? msgOpenConfCollContainsDependent
                : String.Empty;

            msgOpenConfCollContainsCalculatedAuxTotals =
                labels.TryGetValue(key: nameof(msgOpenConfCollContainsCalculatedAuxTotals),
                out msgOpenConfCollContainsCalculatedAuxTotals)
                ? msgOpenConfCollContainsCalculatedAuxTotals
                : String.Empty;

            msgOpenConfCollContainsCalculatedAuxData =
                labels.TryGetValue(key: nameof(msgOpenConfCollContainsCalculatedAuxData),
                out msgOpenConfCollContainsCalculatedAuxData)
                ? msgOpenConfCollContainsCalculatedAuxData
                : String.Empty;

            msgOpenConfCollIsOpened =
               labels.TryGetValue(key: nameof(msgOpenConfCollIsOpened),
               out msgOpenConfCollIsOpened)
               ? msgOpenConfCollIsOpened
               : String.Empty;

            msgDelConfigContainsDependent =
               labels.TryGetValue(key: nameof(msgDelConfigContainsDependent),
               out msgDelConfigContainsDependent)
               ? msgDelConfigContainsDependent
               : String.Empty;

            msgDelConfigContainsDependentConclusion =
               labels.TryGetValue(key: nameof(msgDelConfigContainsDependentConclusion),
               out msgDelConfigContainsDependentConclusion)
               ? msgDelConfigContainsDependentConclusion
               : String.Empty;

            msgDelConfigContainsDependentList =
               labels.TryGetValue(key: nameof(msgDelConfigContainsDependentList),
               out msgDelConfigContainsDependentList)
               ? msgDelConfigContainsDependentList
               : String.Empty;

            msgDelConfigContainsCalculatedAuxTotals =
               labels.TryGetValue(key: nameof(msgDelConfigContainsCalculatedAuxTotals),
               out msgDelConfigContainsCalculatedAuxTotals)
               ? msgDelConfigContainsCalculatedAuxTotals
               : String.Empty;

            msgDelConfigContainsCalculatedAuxData =
               labels.TryGetValue(key: nameof(msgDelConfigContainsCalculatedAuxData),
               out msgDelConfigContainsCalculatedAuxData)
               ? msgDelConfigContainsCalculatedAuxData
               : String.Empty;

            foreach (TreeNode node in ConfigFunctionNodes)
            {
                node.Text =
                    (LanguageVersion == LanguageVersion.International)
                    ? ((ConfigFunction)node.Tag).LabelEn
                    : (LanguageVersion == LanguageVersion.National)
                        ? ((ConfigFunction)node.Tag).LabelCs
                        : ((ConfigFunction)node.Tag).LabelEn;
            }

            foreach (TreeNode node in ConfigCollectionNodes)
            {
                node.Text =
                    (LanguageVersion == LanguageVersion.International)
                    ? ((ConfigCollection)node.Tag).LabelEn
                    : (LanguageVersion == LanguageVersion.National)
                        ? ((ConfigCollection)node.Tag).LabelCs
                        : ((ConfigCollection)node.Tag).LabelEn;
            }

            foreach (TreeNode node in ConfigurationNodes)
            {
                node.Text =
                    (LanguageVersion == LanguageVersion.International)
                    ? ((Config)node.Tag).LabelEn
                    : (LanguageVersion == LanguageVersion.National)
                        ? ((Config)node.Tag).LabelCs
                        : ((Config)node.Tag).LabelEn;
            }

            CtrConfigCollectionView.InitializeLabels();

            CtrConfigurationView.InitializeLabels();
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent()
        {
            LoadContent(catalog: true, nfiesta: true, auxDataStatic: true, auxDataConfig: true, auxDataStats: true);
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        /// <param name="catalog">
        /// <para lang="cs">Nahrání dat katalogu</para>
        /// <para lang="en">Loading catalog data</para>
        /// </param>
        /// <param name="nfiesta">
        /// <para lang="cs">Nahrání dat schématu nfiesta</para>
        /// <para lang="en">Loading nfiesta schema data</para>
        /// </param>
        /// <param name="auxDataStatic">
        /// <para lang="cs">Nahrání dat schématu gisdata - číselníky a mapovací tabulky</para>
        /// <para lang="en">Loading gisdata schema data - lookup tables and mapping tables</para>
        /// </param>
        /// <param name="auxDataConfig">
        /// <para lang="cs">Nahrání dat schématu gisdata - tabulky konfigurací</para>
        /// <para lang="en">Loading gisdata schema data - configuration tables</para>
        /// </param>
        /// <param name="auxDataStats">
        /// <para lang="cs">Nahrání dat schématu gisdata - počty vypočtených úhrnů a hodnot pomocných proměnných</para>
        /// <para lang="en">Loading gisdata schema data - numbers of calculated auxiliary varible totals and values</para>
        /// </param>
        private void LoadContent(
            bool catalog,
            bool nfiesta,
            bool auxDataStatic,
            bool auxDataConfig,
            bool auxDataStats)
        {
            if (!Database.Postgres.Initialized)
            {
                MessageBox.Show(
                    text: msgConnectionNotInitialized,
                    caption: String.Empty,
                    buttons: MessageBoxButtons.OK,
                    icon: MessageBoxIcon.Information);

                return;
            }

            if (catalog)
            {
                Database.Postgres.Catalog.ReLoad();
            }

            if (nfiesta)
            {
                Database.SNfiEsta.CAuxiliaryVariable.ReLoad();
                Database.SNfiEsta.CAuxiliaryVariableCategory.ReLoad();
            }

            if (auxDataStatic)
            {
                Database.SAuxiliaryData.CConfigFunction.ReLoad();
                Database.SAuxiliaryData.CConfigQuery.ReLoad();
                Database.SAuxiliaryData.CEstimationCellCollection.ReLoad();
                Database.SAuxiliaryData.CExtensionVersion.ReLoad(extended: true);
                Database.SAuxiliaryData.CGUIVersion.ReLoad(extended: true);
                Database.SAuxiliaryData.CmEstimationCellCollection.ReLoad();
                Database.SAuxiliaryData.CmExtConfigFunction.ReLoad();
                Database.SAuxiliaryData.CmExtGUIVersion.ReLoad();

                Database.SAuxiliaryData.VEstimationCell =
                   ADFunctions.FnApiGetEstimationCellHierarchy.Execute(database: Database);
            }

            if (auxDataConfig)
            {
                Database.SAuxiliaryData.TConfigCollection.ReLoad();
                Database.SAuxiliaryData.TConfig.ReLoad();
                Database.SAuxiliaryData.TConfig.ReloadColumnCommand();
            }

            if (auxDataStats)
            {
                Database.SAuxiliaryData.VAuxTotalCount =
                    ADFunctions.FnGetAuxTotalCount.Execute(database: Database);

                Database.SAuxiliaryData.VAuxDataCount =
                    ADFunctions.FnGetAuxDataCount.Execute(database: Database);
            }

            InitializeTreeView();

            SetButtonsAvailability();
        }

        /// <summary>
        /// <para lang="cs">Nastavení viditelnosti tlačítek v ToolStrip a ContextMenu</para>
        /// <para lang="en">Setting buttons visibility in ToolStrip and ContextMenu</para>
        /// </summary>
        private void SetButtonsVisibility()
        {
#if DEBUG
            // ToolStrip
            // V režimu návrhu jsou dostupná všechna tlačítka
            btnLoadConfigurationList.Visible = true;
            btnNewConfigCollection.Visible = true;
            btnNewConfiguration.Visible = true;
            btnUpdateConfiguration.Visible = true;
            btnDeleteConfiguration.Visible = true;
            btnLockConfigCollection.Visible = true;
            btnUnlockConfigCollection.Visible = true;
            btnViewData.Visible = true;

            // ContextMenu
            // V režimu návrhu jsou dostupná všechna tlačítka
            tsmiNewConfigCollection.Visible = true;
            tsmiNewConfiguration.Visible = true;
            tsmiUpdateConfigCollection.Visible = true;
            tsmiUpdateConfiguration.Visible = true;
            tsmiDeleteConfigCollection.Visible = true;
            tsmiDeleteConfiguration.Visible = true;
            tsmiLockConfigCollection.Visible = true;
            tsmiUnlockConfigCollection.Visible = true;
            tsmiViewData.Visible = true;
#else
            // ToolStrip
            // V pracovním režimu nelze otevřít uzamčené skupiny konfigurací
            // (btnUnlockConfigCollection.Visible = false)
            btnLoadConfigurationList.Visible = true;
            btnNewConfigCollection.Visible = true;
            btnNewConfiguration.Visible = true;
            btnUpdateConfiguration.Visible = true;
            btnDeleteConfiguration.Visible = true;
            btnLockConfigCollection.Visible = true;
            btnUnlockConfigCollection.Visible = true;
            btnViewData.Visible = true;

            // ContextMenu
            // V pracovním režimu nelze otevřít uzamčené skupiny konfigurací
            // (btnUnlockConfigCollection.Visible = false)
            tsmiNewConfigCollection.Visible = true;
            tsmiNewConfiguration.Visible = true;
            tsmiUpdateConfigCollection.Visible = true;
            tsmiUpdateConfiguration.Visible = true;
            tsmiDeleteConfigCollection.Visible = true;
            tsmiDeleteConfiguration.Visible = true;
            tsmiLockConfigCollection.Visible = true;
            tsmiUnlockConfigCollection.Visible = true;
            tsmiViewData.Visible = true;
#endif
        }

        /// <summary>
        /// <para lang="cs">Nastavení dostupnosti tlačítek v ToolStrip</para>
        /// <para lang="en">Setting buttons availability in ToolStrip</para>
        /// </summary>
        private void SetButtonsAvailability()
        {
            if (Database.Postgres.Initialized)
            {
                if ((tvwConfigurations.SelectedNode != null) &&
                    (tvwConfigurations.SelectedNode.Tag != null))
                {
                    switch (tvwConfigurations.SelectedNode.Level)
                    {
                        case LevelConfigFunction:
                            // Stav připojeno k databázi, vybraný uzel - úroveň "Výpočetní funkce"
                            btnLoadConfigurationList.Enabled = true;    // Musí být připojeno k databázi
                            btnNewConfigCollection.Enabled = true;      // Musí být připojeno k databázi
                            btnNewConfiguration.Enabled = false;        // Musí být určena skupina, do které se přidává (uzel skupiny nebo konfigurace)
                            btnUpdateConfiguration.Enabled = false;     // Musí být určena skupina nebo konfigurace, která se mění
                            btnDeleteConfiguration.Enabled = false;     // Musí být určena skupina nebo konfigurace, která se maže
                            btnLockConfigCollection.Enabled = false;    // Musí být určena skupina, která se zavírá (uzel skupiny nebo konfigurace)
                            btnUnlockConfigCollection.Enabled = false;  // Musí být určena skupina, která se otevírá (uzel skupiny nebo konfigurace)
                            btnViewData.Enabled = false;                // Musí být určena skupina, která se počítá (uzel skupiny nebo konfigurace)
                            break;

                        case LevelConfigCollection:
                            // Stav připojeno k databázi, vybraný uzel - úroveň "Skupina konfigurací"
                            if (SelectedConfigCollection == null)
                            {
                                btnLoadConfigurationList.Enabled = true;    // Musí být připojeno k databázi
                                btnNewConfigCollection.Enabled = true;      // Musí být připojeno k databázi
                                btnNewConfiguration.Enabled = false;        // Musí být určena skupina, do které se přidává (uzel skupiny nebo konfigurace)
                                btnUpdateConfiguration.Enabled = false;     // Musí být určena skupina nebo konfigurace, která se mění
                                btnDeleteConfiguration.Enabled = false;     // Musí být určena skupina nebo konfigurace, která se maže
                                btnLockConfigCollection.Enabled = false;    // Musí být určena skupina, která se zavírá (uzel skupiny nebo konfigurace)
                                btnUnlockConfigCollection.Enabled = false;  // Musí být určena skupina, která se otevírá (uzel skupiny nebo konfigurace)
                                btnViewData.Enabled = false;                // Musí být určena skupina, která se počítá (uzel skupiny nebo konfigurace)
                            }
                            else if (SelectedConfigCollection.Closed)
                            {
                                // Uzavřená skupina konfigurací nedovoluje úpravy
                                // tj. nelze změnit, smazat, nelze přidat
                                // lze spouštět výpočty (pokud to není bodová vrstva)
                                btnLoadConfigurationList.Enabled = true;   // Musí být připojeno k databázi
                                btnNewConfigCollection.Enabled = true;     // Musí být připojeno k databázi
                                btnNewConfiguration.Enabled = false;       // Musí být určena skupina, do které se přidává (uzel skupiny nebo konfigurace)
                                btnUpdateConfiguration.Enabled = false;    // Musí být určena skupina nebo konfigurace, která se mění
                                btnDeleteConfiguration.Enabled = false;    // Musí být určena skupina nebo konfigurace, která se maže
                                btnLockConfigCollection.Enabled = false;   // Musí být určena skupina, která se zavírá (uzel skupiny nebo konfigurace)
                                btnUnlockConfigCollection.Enabled = true;  // Musí být určena skupina, která se otevírá (uzel skupiny nebo konfigurace)
                                btnViewData.Enabled = !SelectedConfigCollection.IsPointLayer; // Musí být určena skupina, která se počítá (uzel skupiny nebo konfigurace)
                            }
                            else
                            {
                                if (SelectedConfigCollection.IsForTotals)
                                {
                                    // Otevřená skupina konfigurací dovoluje úpravy a nepovoluje výpočet
                                    // tj. lze změnit, smazat, lze přidat
                                    // nelze spouštět výpočty
                                    btnLoadConfigurationList.Enabled = true;   // Musí být připojeno k databázi
                                    btnNewConfigCollection.Enabled = true;     // Musí být připojeno k databázi
                                    btnNewConfiguration.Enabled = true;        // Musí být určena skupina, do které se přidává (uzel skupiny nebo konfigurace)
                                    btnUpdateConfiguration.Enabled = true;     // Musí být určena skupina nebo konfigurace, která se mění
                                    btnDeleteConfiguration.Enabled = true;     // Musí být určena skupina nebo konfigurace, která se maže
                                    btnLockConfigCollection.Enabled = true;    // Musí být určena skupina, která se zavírá (uzel skupiny nebo konfigurace)
                                    btnUnlockConfigCollection.Enabled = false; // Musí být určena skupina, která se otevírá (uzel skupiny nebo konfigurace)
                                    btnViewData.Enabled = false;               // Musí být určena skupina, která se počítá (uzel skupiny nebo konfigurace)
                                }
                                else
                                {
                                    // Do skupin, které nejsou pro výpočet úhrnů nelze přidávat konfigurace
                                    btnLoadConfigurationList.Enabled = true;   // Musí být připojeno k databázi
                                    btnNewConfigCollection.Enabled = true;     // Musí být připojeno k databázi
                                    btnNewConfiguration.Enabled = false;       // Nesmí se umožnit přidávání
                                    btnUpdateConfiguration.Enabled = true;     // Musí být určena skupina nebo konfigurace, která se mění
                                    btnDeleteConfiguration.Enabled = true;     // Musí být určena skupina nebo konfigurace, která se maže
                                    btnLockConfigCollection.Enabled = true;    // Musí být určena skupina, která se zavírá (uzel skupiny nebo konfigurace)
                                    btnUnlockConfigCollection.Enabled = false; // Musí být určena skupina, která se otevírá (uzel skupiny nebo konfigurace)
                                    btnViewData.Enabled = false;               // Musí být určena skupina, která se počítá (uzel skupiny nebo konfigurace)
                                }
                            }
                            break;

                        case LevelConfiguration:
                            // Stav připojeno k databázi, vybraný uzel - úroveň "Konfigurace"
                            if (SelectedConfiguration?.ConfigCollection == null)
                            {
                                btnLoadConfigurationList.Enabled = true;    // Musí být připojeno k databázi
                                btnNewConfigCollection.Enabled = true;      // Musí být připojeno k databázi
                                btnNewConfiguration.Enabled = false;        // Musí být určena skupina, do které se přidává (uzel skupiny nebo konfigurace)
                                btnUpdateConfiguration.Enabled = false;     // Musí být určena skupina nebo konfigurace, která se mění
                                btnDeleteConfiguration.Enabled = false;     // Musí být určena skupina nebo konfigurace, která se maže
                                btnLockConfigCollection.Enabled = false;    // Musí být určena skupina, která se zavírá (uzel skupiny nebo konfigurace)
                                btnUnlockConfigCollection.Enabled = false;  // Musí být určena skupina, která se otevírá (uzel skupiny nebo konfigurace)
                                btnViewData.Enabled = false;                // Musí být určena skupina, která se počítá (uzel skupiny nebo konfigurace)
                            }
                            if (SelectedConfiguration.ConfigCollection.Closed)
                            {
                                // Uzavřená skupina konfigurací nedovoluje úpravy
                                // tj. nelze změnit, smazat, nelze přidat
                                // lze spouštět výpočty (pokud to není bodová vrstva)
                                btnLoadConfigurationList.Enabled = true;   // Musí být připojeno k databázi
                                btnNewConfigCollection.Enabled = true;     // Musí být připojeno k databázi
                                btnNewConfiguration.Enabled = false;       // Musí být určena skupina, do které se přidává (uzel skupiny nebo konfigurace)
                                btnUpdateConfiguration.Enabled = false;    // Musí být určena skupina nebo konfigurace, která se mění
                                btnDeleteConfiguration.Enabled = false;    // Musí být určena skupina nebo konfigurace, která se maže
                                btnLockConfigCollection.Enabled = false;   // Musí být určena skupina, která se zavírá (uzel skupiny nebo konfigurace)
                                btnUnlockConfigCollection.Enabled = true;  // Musí být určena skupina, která se otevírá (uzel skupiny nebo konfigurace)
                                btnViewData.Enabled = !SelectedConfiguration.ConfigCollection.IsPointLayer;  // Musí být určena skupina, která se počítá (uzel skupiny nebo konfigurace)
                            }
                            else
                            {
                                if (SelectedConfiguration.ConfigCollection.IsForTotals)
                                {
                                    // Otevřená skupina konfigurací dovoluje úpravy a nepovoluje výpočet
                                    // tj. lze změnit, smazat, lze přidat
                                    // nelze spouštět výpočty
                                    btnLoadConfigurationList.Enabled = true;   // Musí být připojeno k databázi
                                    btnNewConfigCollection.Enabled = true;     // Musí být připojeno k databázi
                                    btnNewConfiguration.Enabled = true;        // Musí být určena skupina, do které se přidává (uzel skupiny nebo konfigurace)
                                    btnUpdateConfiguration.Enabled = true;     // Musí být určena skupina nebo konfigurace, která se mění
                                    btnDeleteConfiguration.Enabled = true;     // Musí být určena skupina nebo konfigurace, která se maže
                                    btnLockConfigCollection.Enabled = true;    // Musí být určena skupina, která se zavírá (uzel skupiny nebo konfigurace)
                                    btnUnlockConfigCollection.Enabled = false; // Musí být určena skupina, která se otevírá (uzel skupiny nebo konfigurace)
                                    btnViewData.Enabled = false;               // Musí být určena skupina, která se počítá (uzel skupiny nebo konfigurace)
                                }
                                else
                                {
                                    // Do skupin, které nejsou pro výpočet úhrnů nelze přidávat konfigurace
                                    btnLoadConfigurationList.Enabled = true;   // Musí být připojeno k databázi
                                    btnNewConfigCollection.Enabled = true;     // Musí být připojeno k databázi
                                    btnNewConfiguration.Enabled = false;       // Nesmí se umožnit přidávání
                                    btnUpdateConfiguration.Enabled = true;     // Musí být určena skupina nebo konfigurace, která se mění
                                    btnDeleteConfiguration.Enabled = true;     // Musí být určena skupina nebo konfigurace, která se maže
                                    btnLockConfigCollection.Enabled = true;    // Musí být určena skupina, která se zavírá (uzel skupiny nebo konfigurace)
                                    btnUnlockConfigCollection.Enabled = false; // Musí být určena skupina, která se otevírá (uzel skupiny nebo konfigurace)
                                    btnViewData.Enabled = false;               // Musí být určena skupina, která se počítá (uzel skupiny nebo konfigurace)
                                }
                            }
                            break;

                        default:
                            // Chybový stav
                            btnLoadConfigurationList.Enabled = false;   // Musí být připojeno k databázi
                            btnNewConfigCollection.Enabled = false;     // Musí být připojeno k databázi
                            btnNewConfiguration.Enabled = false;        // Musí být určena skupina, do které se přidává (uzel skupiny nebo konfigurace)
                            btnUpdateConfiguration.Enabled = false;     // Musí být určena skupina nebo konfigurace, která se mění
                            btnDeleteConfiguration.Enabled = false;     // Musí být určena skupina nebo konfigurace, která se maže
                            btnLockConfigCollection.Enabled = false;    // Musí být určena skupina, která se zavírá (uzel skupiny nebo konfigurace)
                            btnUnlockConfigCollection.Enabled = false;  // Musí být určena skupina, která se otevírá (uzel skupiny nebo konfigurace)
                            btnViewData.Enabled = false;                // Musí být určena skupina, která se počítá (uzel skupiny nebo konfigurace)
                            break;
                    }
                }
                else
                {
                    // Stav připojeno k databázi, není vybraný žádný uzel
                    btnLoadConfigurationList.Enabled = true;    // Musí být připojeno k databázi
                    btnNewConfigCollection.Enabled = true;      // Musí být připojeno k databázi
                    btnNewConfiguration.Enabled = false;        // Musí být určena skupina, do které se přidává (uzel skupiny nebo konfigurace)
                    btnUpdateConfiguration.Enabled = false;     // Musí být určena skupina nebo konfigurace, která se mění
                    btnDeleteConfiguration.Enabled = false;     // Musí být určena skupina nebo konfigurace, která se maže
                    btnLockConfigCollection.Enabled = false;    // Musí být určena skupina, která se zavírá (uzel skupiny nebo konfigurace)
                    btnUnlockConfigCollection.Enabled = false;  // Musí být určena skupina, která se otevírá (uzel skupiny nebo konfigurace)
                    btnViewData.Enabled = false;                // Musí být určena skupina, která se počítá (uzel skupiny nebo konfigurace)
                }
            }
            else
            {
                btnLoadConfigurationList.Enabled = false;    // Musí být připojeno k databázi
                btnNewConfigCollection.Enabled = false;      // Musí být připojeno k databázi
                btnNewConfiguration.Enabled = false;         // Musí být určena skupina, do které se přidává (uzel skupiny nebo konfigurace)
                btnUpdateConfiguration.Enabled = false;      // Musí být určena skupina nebo konfigurace, která se mění
                btnDeleteConfiguration.Enabled = false;      // Musí být určena skupina nebo konfigurace, která se maže
                btnLockConfigCollection.Enabled = false;     // Musí být určena skupina, která se zavírá (uzel skupiny nebo konfigurace)
                btnUnlockConfigCollection.Enabled = false;   // Musí být určena skupina, která se otevírá (uzel skupiny nebo konfigurace)
                btnViewData.Enabled = false;                 // Musí být určena skupina, která se počítá (uzel skupiny nebo konfigurace)
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení viditelnosti skupin</para>
        /// <para lang="en">Setting GroupBoxes visibility</para>
        /// </summary>
        private void SetGroupBoxesVisiblity()
        {
            if ((tvwConfigurations.SelectedNode == null) ||
                (tvwConfigurations.SelectedNode.Tag == null))
            {
                CtrConfigCollectionView.Visible = false;
                CtrConfigurationView.Visible = false;
                grpSQL.Visible = false;
                return;
            }

            switch (tvwConfigurations.SelectedNode.Level)
            {
                case LevelConfigFunction:
                    // Na úrovni výpočetních funkcí se nic nezobrazuje
                    CtrConfigCollectionView.Visible = false;
                    CtrConfigurationView.Visible = false;
                    grpSQL.Visible = false;
                    return;

                case LevelConfigCollection:
                    // Na úrovni skupin konfigurací
                    // se zobrazuje popis skupiny
                    // a parametry skupiny
                    CtrConfigCollectionView.Visible = true;
                    CtrConfigurationView.Visible = false;
                    grpSQL.Visible = false;
                    return;

                case LevelConfiguration:
                    // Na úrovni konfigurací
                    // se zobrazuje popis konfigurace
                    // parametry konfigurace
                    // a SQL příkaz
                    CtrConfigCollectionView.Visible = false;
                    CtrConfigurationView.Visible = true;
                    grpSQL.Visible = true;
                    return;

                default:
                    // Nemělo by nastat
                    CtrConfigCollectionView.Visible = false;
                    CtrConfigurationView.Visible = false;
                    grpSQL.Visible = false;
                    return;
            }
        }

        /// <summary>
        /// <para lang="cs">Zobrazení parametrů vybrané skupiny konfigurací nebo konfigurace</para>
        /// <para lang="en">Display parameters of selected configuration collection or configuration</para>
        /// </summary>
        private void SetParams()
        {
            if ((tvwConfigurations.SelectedNode == null) ||
                (tvwConfigurations.SelectedNode.Tag == null))
            {
                return;
            }

            switch (tvwConfigurations.SelectedNode.Level)
            {
                case LevelConfigFunction:
                    return;

                case LevelConfigCollection:
                    CtrConfigCollectionView.ConfigCollection = SelectedConfigCollection;
                    return;

                case LevelConfiguration:
                    CtrConfigurationView.Configuration = SelectedConfiguration;
                    return;

                default:
                    return;
            }
        }

        /// <summary>
        /// <para lang="cs">Zobrazení SQL příkazu</para>
        /// <para lang="en">Display SQL command</para>
        /// </summary>
        private void SetSQL()
        {
            if ((tvwConfigurations.SelectedNode == null) ||
                (tvwConfigurations.SelectedNode.Tag == null))
            {
                txtSQL.Text = String.Empty;
                return;
            }

            switch (tvwConfigurations.SelectedNode.Level)
            {
                case LevelConfigFunction:
                    txtSQL.Text = String.Empty;
                    return;

                case LevelConfigCollection:
                    txtSQL.Text = String.Empty;
                    return;

                case LevelConfiguration:
                    txtSQL.Text = SelectedConfiguration.Command ?? String.Empty;
                    Functions.HighlightSQLSyntax(box: txtSQL);
                    return;

                default:
                    txtSQL.Text = String.Empty;
                    return;
            }
        }

        /// <summary>
        /// <para lang="cs">Zobrazení konfigurací v uzlech TreeView</para>
        /// <para lang="en">Displaying configurations in TreeView nodes</para>
        /// </summary>
        private void InitializeTreeView()
        {
            // Zabrání prokreslování TreeView během editace
            tvwConfigurations.BeginUpdate();

            // Uložení levelu a identifikátoru vybraného uzlu v TreeView,
            // umožní jeho znovu vybrání po překreslení TreeView
            TreeNode selectedNode = null;
            Nullable<int> level = null;
            Nullable<int> id = null;
            if (tvwConfigurations.SelectedNode != null)
            {
                if ((level == null) && (id == null))
                {
                    level = tvwConfigurations.SelectedNode.Level;
                    id = level switch
                    {
                        LevelConfigFunction => SelectedConfigFunction?.Id,
                        LevelConfigCollection => SelectedConfigCollection?.Id,
                        LevelConfiguration => SelectedConfiguration?.Id,
                        _ => null,
                    };
                }
            }

            // Zachování stavu TreeView - identifikátory rozbalených uzlů TreeView
            List<int> expandedConfigFunctionNodeIds =
                ConfigFunctionNodes
                    .Where(a => a.IsExpanded)
                    .Select(a => ((ConfigFunction)a.Tag).Id)
                    .ToList<int>();

            List<int> expandedConfigCollectionNodeIds =
                ConfigCollectionNodes
                    .Where(a => a.IsExpanded)
                    .Select(a => ((ConfigCollection)a.Tag).Id)
                    .ToList<int>();

            // Smazání všech uzlů z TreeView
            Nodes.Clear();
            tvwConfigurations.Nodes.Clear();

            // Přidání uzlů první úrovně
            // (Seznam funkcí pro výpočet úhrnů pomocných proměnných)
            foreach (ConfigFunction configFunction
                in Database.SAuxiliaryData.CConfigFunction.Items)
            {
                TreeNode node = new()
                {
                    ContextMenuStrip = cmsConfigFunction,
                    ImageIndex = ImageBlueBall,
                    Name = configFunction.Id.ToString(),
                    SelectedImageIndex = ImageBlueBall,
                    Tag = configFunction,
                    Text =
                        (LanguageVersion == LanguageVersion.International)
                            ? configFunction.LabelEn
                            : (LanguageVersion == LanguageVersion.National)
                                ? configFunction.LabelCs
                                : configFunction.LabelEn
                };
                Nodes.Add(item: node);
                tvwConfigurations.Nodes.Add(node: node);

                if ((level == LevelConfigFunction) && (id == configFunction.Id))
                {
                    selectedNode = node;
                }
            }

            // Přidání uzlů druhé úrovně
            // (Seznam skupin konfigurací)
            foreach (ConfigCollection configCollection
                in Database.SAuxiliaryData.TConfigCollection.Items)
            {
                TreeNode parent = ConfigFunctionNodes
                    .Where(a => ((ConfigFunction)a.Tag).Id == configCollection.ConfigFunctionId)
                    .FirstOrDefault<TreeNode>();

                if (parent != null)
                {
                    TreeNode node = new()
                    {
                        ContextMenuStrip = configCollection.Closed ? cmsLockedConfigCollection : cmsConfigCollection,
                        ImageIndex =
                            configCollection.Closed
                                ? ImageLock
                                : ImageYellowBall,
                        Name = configCollection.Id.ToString(),
                        SelectedImageIndex =
                            configCollection.Closed
                                    ? ImageLock
                                    : ImageYellowBall,
                        Tag = configCollection,
                        Text =
                            (LanguageVersion == LanguageVersion.International)
                                ? configCollection.LabelEn
                                : (LanguageVersion == LanguageVersion.National)
                                    ? configCollection.LabelCs
                                    : configCollection.LabelEn
                    };
                    Nodes.Add(item: node);
                    parent.Nodes.Add(node: node);

                    if ((level == LevelConfigCollection) && (id == configCollection.Id))
                    {
                        selectedNode = node;
                    }
                }
            }

            // Přidání uzlů třetí úrovně
            // (Seznam konfigurací)
            foreach (Config config
                in Database.SAuxiliaryData.TConfig.Items)
            {
                TreeNode parent = ConfigCollectionNodes
                    .Where(a => ((ConfigCollection)a.Tag).Id == config.ConfigCollectionId)
                    .FirstOrDefault<TreeNode>();

                if (parent != null)
                {
                    TreeNode node = new()
                    {
                        ContextMenuStrip = config.ConfigCollection.Closed ? null : cmsConfiguration,
                        ImageIndex =
                            (config.ConfigQuery.Value != ConfigQueryEnum.LinkToVariable)
                            ? ImageGreenBall
                            : ImageGrayBall,
                        Name = config.Id.ToString(),
                        SelectedImageIndex =
                            (config.ConfigQuery.Value != ConfigQueryEnum.LinkToVariable)
                            ? ImageGreenBall
                            : ImageGrayBall,
                        Tag = config,
                        Text =
                            (LanguageVersion == LanguageVersion.International)
                                    ? config.LabelEn
                                    : (LanguageVersion == LanguageVersion.National)
                                        ? config.LabelCs
                                        : config.LabelEn
                    };
                    Nodes.Add(item: node);
                    parent.Nodes.Add(node: node);

                    if ((level == LevelConfiguration) && (id == config.Id))
                    {
                        selectedNode = node;
                    }
                }
            }

            // Obnovení stavu TreeView
            // (Rozbalené uzly pro funkce)
            foreach (TreeNode node in ConfigFunctionNodes)
            {
                if (expandedConfigFunctionNodeIds.Contains(
                        item: ((ConfigFunction)node.Tag).Id))
                {
                    node.Expand();
                }
                else
                {
                    node.Collapse();
                }
            }

            // Obnovení stavu TreeView
            // (Rozbalené uzly pro skupiny konfigurací)
            foreach (TreeNode node in ConfigCollectionNodes)
            {
                if (expandedConfigCollectionNodeIds.Contains(
                        item: ((ConfigCollection)node.Tag).Id))
                {
                    node.Expand();
                }
                else
                {
                    node.Collapse();
                }
            }

            // Obnovení vybraného uzlu v TreeView
            if (selectedNode != null)
            {
                tvwConfigurations.SelectedNode = selectedNode;
            }

            tvwConfigurations.EndUpdate();
        }

        /// <summary>
        /// <para lang="cs">Vybere první uzel v TreeView</para>
        /// <para lang="en">Selects first node in TreeView</para>
        /// </summary>
        private void SelectFirstNode()
        {
            if (tvwConfigurations.Nodes.Count == 0)
            {
                return;
            }
            tvwConfigurations.SelectedNode = tvwConfigurations.Nodes[0];
        }

        /// <summary>
        /// <para lang="cs">
        /// Vybere uzel v TreeView odpovídající identifikačnímu číslu
        /// funkce nebo skupiny konfigurací nebo konfiguraci
        /// </para>
        /// <para lang="en">
        /// Selects the TreeView node corresponding to
        /// the function, configuration collection or configuration identifier
        /// </para>
        /// </summary>
        /// <param name="level">
        /// <para lang="cs">Úroveň uzlu v TreeView</para>
        /// <para lang="en">TreeView node level</para>
        /// </param>
        /// <param name="id">
        /// <para lang="cs">Identifikační číslo funkce, skupiny konfigurací, konfigurace</para>
        /// <para lang="en">Function, configuration collection or configuration identifier</para>
        /// </param>
        private void SelectNode(int level, int id)
        {
            TreeNode node = FindNode(level, id);
            if (node == null)
            {
                return;
            }
            tvwConfigurations.SelectedNode = node;
        }

        /// <summary>
        /// <para lang="cs">
        /// Vrátí uzel z TreeView odpovídající identifikačnímu číslu
        /// funkce nebo skupiny konfigurací nebo konfiguraci
        /// </para>
        /// <para lang="en">
        /// Returns the TreeView node corresponding to
        /// the function, configuration collection or configuration identifier
        /// </para>
        /// </summary>
        /// <param name="level">
        /// <para lang="cs">Úroveň uzlu v TreeView</para>
        /// <para lang="en">TreeView node level</para>
        /// </param>
        /// <param name="id">
        /// <para lang="cs">Identifikační číslo funkce, skupiny konfigurací, konfigurace</para>
        /// <para lang="en">Function, configuration collection or configuration identifier</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">
        /// Vrátí uzel z TreeView odpovídající identifikačnímu číslu
        /// funkce nebo skupiny konfigurací nebo konfiguraci
        /// </para>
        /// <para lang="en">
        /// Returns the TreeView node corresponding to
        /// the function, configuration collection or configuration identifier
        /// </para>
        /// </returns>
        private TreeNode FindNode(int level, int id)
        {
            return level switch
            {
                LevelConfigFunction => ConfigFunctionNodes
                        .Where(a => ((ConfigFunction)a.Tag).Id == id)
                        .FirstOrDefault<TreeNode>(),
                LevelConfigCollection => ConfigCollectionNodes
                        .Where(a => ((ConfigCollection)a.Tag).Id == id)
                        .FirstOrDefault<TreeNode>(),
                LevelConfiguration => ConfigurationNodes
                        .Where(a => ((Config)a.Tag).Id == id)
                        .FirstOrDefault<TreeNode>(),
                _ => null,
            };
        }

        /// <summary>
        /// <para lang="cs">Vytvoří nový objekt skupiny konfigurací</para>
        /// <para lang="en">Creates new configuration collection object</para>
        /// </summary>
        private void NewConfigCollection()
        {
            FormConfigCollectionGuide form = new(controlOwner: this)
            {
                ConfigCollection = Database.SAuxiliaryData.TConfigCollection.CreateNewItem(),
                NewEntry = true
            };
            form.LoadContent();

            // 1) Spustí průvodce pro nastavení parametrů skupiny konfigurací
            if (form.ShowDialog() == DialogResult.OK)
            {
                // 2) Zapíše nový objekt skupiny konfigurací do databáze
                form.ConfigCollection.Insert();

                // 3) Aktualizuje data v aplikaci z databáze a zobrazí aktuální stav v TreeView
                LoadContent(catalog: false, nfiesta: false, auxDataStatic: false, auxDataConfig: true, auxDataStats: false);

                // 4) Najde uzel nového objektu skupiny konfigurací v TreeView a nastaví ho jako vybraný
                SelectNode(level: LevelConfigCollection, id: form.ConfigCollection.Id);
            }
        }

        /// <summary>
        /// <para lang="cs">Vytvoří nový objekt konfigurace a přidá ho do skupiny konfigurací</para>
        /// <para lang="en">Creates new configuration object and adds it into configuration collection</para>
        /// </summary>
        private void NewConfiguration()
        {
            if (AvailableConfigCollection == null)
            {
                return;
            }

            FormConfigurationGuide form = new(controlOwner: this)
            {
                Configuration = Database.SAuxiliaryData.TConfig.CreateNewItem(
                    configCollection: AvailableConfigCollection),
                NewEntry = true
            };
            form.LoadContent();

            // 1) Spustí průvodce pro nastavení parametrů konfigurace
            if (form.ShowDialog() == DialogResult.OK)
            {
                // 2) Zapíše nový objekt konfigurace do databáze
                form.Configuration.Insert();

                // 3) Aktualizuje data v aplikaci z databáze a zobrazí aktuální stav v TreeView
                LoadContent(catalog: false, nfiesta: false, auxDataStatic: false, auxDataConfig: true, auxDataStats: false);

                // 4) Najde uzel nového objektu konfigurace v TreeView a nastaví ho jako vybraný
                SelectNode(level: LevelConfiguration, id: form.Configuration.Id);
            }
        }


        /// <summary>
        /// <para lang="cs">Editace vybrané skupiny konfigurací</para>
        /// <para lang="en">Edit selected configuration collection</para>
        /// </summary>
        private void UpdateConfigCollection()
        {
            FormConfigCollectionGuide form = new(controlOwner: this)
            {
                ConfigCollection = SelectedConfigCollection,
                NewEntry = false
            };
            form.LoadContent();

            // 1) Spustí průvodce pro nastavení parametrů skupiny konfigurací
            if (form.ShowDialog() == DialogResult.OK)
            {
                // 2) Zapíše aktualizaci objektu skupiny konfigurací do databáze
                form.ConfigCollection.Update();

                // 3) Aktualizuje data v aplikaci z databáze a zobrazí aktuální stav v TreeView
                LoadContent(catalog: false, nfiesta: false, auxDataStatic: false, auxDataConfig: true, auxDataStats: false);

                // 4) Najde uzel nového objektu skupiny konfigurací v TreeView a nastaví ho jako vybraný
                SelectNode(level: LevelConfigCollection, id: form.ConfigCollection.Id);
            }
            return;
        }

        /// <summary>
        /// <para lang="cs">Editace vybrané konfigurace</para>
        /// <para lang="en">Edit selected configuration</para>
        /// </summary>
        private void UpdateConfiguration()
        {
            FormConfigurationGuide form = new(controlOwner: this)
            {
                Configuration = SelectedConfiguration,
                NewEntry = false
            };
            form.LoadContent();

            // 1) Spustí průvodce pro nastavení parametrů konfigurace
            if (form.ShowDialog() == DialogResult.OK)
            {
                // 2) Zapíše aktualizaci objektu konfigurace do databáze
                form.Configuration.Update();

                // 3) Aktualizuje data v aplikaci z databáze a zobrazí aktuální stav v TreeView
                LoadContent(catalog: false, nfiesta: false, auxDataStatic: false, auxDataConfig: true, auxDataStats: false);

                // 4) Najde uzel nového objektu konfigurace v TreeView a nastaví ho jako vybraný
                SelectNode(level: LevelConfiguration, id: form.Configuration.Id);
            }
            return;
        }

        /// <summary>
        /// <para lang="cs">Editace vybrané skupiny konfigurací nebo vybrané konfigurace</para>
        /// <para lang="en">Edit selected configuration collection or selected configuration</para>
        /// </summary>
        private void UpdateItem()
        {
            if (SelectedConfigCollection != null)
            {
                UpdateConfigCollection();
                return;
            }

            if (SelectedConfiguration != null)
            {
                UpdateConfiguration();
                return;
            }

            return;
        }


        /// <summary>
        /// <para lang="cs">Smazání vybrané skupiny konfigurací</para>
        /// <para lang="en">Delete selected configuration collection</para>
        /// </summary>
        private void DeleteConfigCollection()
        {
            if (SelectedConfigCollection.TryDelete(
                    status: out ConfigCollectionStatus status))
            {
                SelectedConfigCollection.Delete();
                LoadContent(catalog: false, nfiesta: false, auxDataStatic: false, auxDataConfig: true, auxDataStats: false);
                SelectFirstNode();
                return;
            }
            else
            {
                switch (status)
                {
                    case ConfigCollectionStatus.ContainsDependentConfigCollections:
                        string message =
                            String.Concat(
                                msgDelConfCollContainsDependent,
                                Environment.NewLine,
                                msgDelConfCollContainsDependentConclusion,
                                Environment.NewLine,
                                (LanguageVersion == LanguageVersion.National)
                                    ? SelectedConfigCollection.DependentConfigCollections
                                        .Select(a => a.ExtendedLabelCs)
                                        .Aggregate((a, b) => $"{a},{Environment.NewLine}{b}")
                                    : SelectedConfigCollection.DependentConfigCollections
                                        .Select(a => a.ExtendedLabelEn)
                                        .Aggregate((a, b) => $"{a},{Environment.NewLine}{b}"),
                                Environment.NewLine);
                        MessageBox.Show(
                           text: message,
                           caption: String.Empty,
                           buttons: MessageBoxButtons.OK,
                           icon: MessageBoxIcon.Information);
                        return;

                    case ConfigCollectionStatus.ContainsCalculatedAuxTotals:
                        MessageBox.Show(
                          text: msgDelConfCollContainsCalculatedAuxTotals,
                          caption: String.Empty,
                          buttons: MessageBoxButtons.OK,
                          icon: MessageBoxIcon.Information);
                        return;

                    case ConfigCollectionStatus.ContainsCalculatedAuxData:
                        MessageBox.Show(
                           text: msgDelConfCollContainsCalculatedAuxData,
                           caption: String.Empty,
                           buttons: MessageBoxButtons.OK,
                           icon: MessageBoxIcon.Information);
                        return;

                    case ConfigCollectionStatus.IsNotEmpty:
                        MessageBox.Show(
                          text: msgDelConfCollIsNotEmpty,
                          caption: String.Empty,
                          buttons: MessageBoxButtons.OK,
                          icon: MessageBoxIcon.Information);
                        return;

                    case ConfigCollectionStatus.IsClosed:
                        MessageBox.Show(
                          text: msgDelConfCollIsClosed,
                          caption: String.Empty,
                          buttons: MessageBoxButtons.OK,
                          icon: MessageBoxIcon.Information);
                        return;

                    default:
                        return;
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Smazání vybrané konfigurace</para>
        /// <para lang="en">Delete selected configuration</para>
        /// </summary>
        private void DeleteConfiguration()
        {
            if (SelectedConfiguration.TryDelete(
                    status: out ConfigurationStatus status))
            {
                SelectedConfiguration.Delete();
                LoadContent(catalog: false, nfiesta: false, auxDataStatic: false, auxDataConfig: true, auxDataStats: false);
                SelectFirstNode();
                return;
            }
            else
            {
                switch (status)
                {
                    case ConfigurationStatus.ContainsDependentConfigurations:
                        string message =
                            String.Concat(
                                msgDelConfigContainsDependent,
                                Environment.NewLine,
                                msgDelConfigContainsDependentConclusion,
                                Environment.NewLine,
                                (LanguageVersion == LanguageVersion.National)
                                    ? SelectedConfiguration.DependentConfigurations
                                        .Select(a => a.ExtendedLabelCs)
                                        .Aggregate((a, b) => $"{a},{Environment.NewLine}{b}")
                                    : SelectedConfiguration.DependentConfigurations
                                        .Select(a => a.ExtendedLabelEn)
                                        .Aggregate((a, b) => $"{a},{Environment.NewLine}{b}"),
                                Environment.NewLine);
                        MessageBox.Show(
                           text: message,
                           caption: String.Empty,
                           buttons: MessageBoxButtons.OK,
                           icon: MessageBoxIcon.Information);
                        return;

                    case ConfigurationStatus.ContainsCalculatedAuxTotals:
                        MessageBox.Show(
                          text: msgDelConfigContainsCalculatedAuxTotals,
                          caption: String.Empty,
                          buttons: MessageBoxButtons.OK,
                          icon: MessageBoxIcon.Information);
                        return;

                    case ConfigurationStatus.ContainsCalculatedAuxData:
                        MessageBox.Show(
                           text: msgDelConfigContainsCalculatedAuxData,
                           caption: String.Empty,
                           buttons: MessageBoxButtons.OK,
                           icon: MessageBoxIcon.Information);
                        return;

                    default:
                        return;
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Smazání vybrané skupiny konfigurací nebo vybrané konfigurace</para>
        /// <para lang="en">Delete selected configuration collection or selected configuration</para>
        /// </summary>
        private void DeleteItem()
        {
            if (SelectedConfigCollection != null)
            {
                DeleteConfigCollection();
                return;
            }

            if (SelectedConfiguration != null)
            {
                DeleteConfiguration();
                return;
            }

            return;
        }


        /// <summary>
        /// <para lang="cs">Uzavření vybrané skupiny konfigurací</para>
        /// <para lang="en">Close selected configuration collection</para>
        /// </summary>
        private void CloseConfigCollection()
        {
            if (AvailableConfigCollection == null)
            {
                return;
            }

            if (AvailableConfigCollection.TryClose(
                status: out ConfigCollectionStatus status))
            {
                int id = AvailableConfigCollection.Id;
                AvailableConfigCollection.Close();
                LoadContent(catalog: false, nfiesta: false, auxDataStatic: false, auxDataConfig: true, auxDataStats: false);
                SelectNode(level: LevelConfigCollection, id: id);
                return;
            }
            else
            {
                switch (status)
                {
                    case ConfigCollectionStatus.IsEmpty:
                        MessageBox.Show(
                           text: msgCloseConfCollIsEmpty,
                           caption: String.Empty,
                           buttons: MessageBoxButtons.OK,
                           icon: MessageBoxIcon.Information);
                        return;

                    case ConfigCollectionStatus.IsClosed:
                        MessageBox.Show(
                           text: msgCloseConfCollIsClosed,
                           caption: String.Empty,
                           buttons: MessageBoxButtons.OK,
                           icon: MessageBoxIcon.Information);
                        return;


                    default:
                        return;
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Otevření vybrané skupiny konfigurací</para>
        /// <para lang="en">Open selected configuration collection</para>
        /// </summary>
        private void OpenConfigCollection()
        {
            if (AvailableConfigCollection == null)
            {
                return;
            }

            if (AvailableConfigCollection.TryOpen(
                status: out ConfigCollectionStatus status))
            {
                int id = AvailableConfigCollection.Id;
                AvailableConfigCollection.Open();
                LoadContent(catalog: false, nfiesta: false, auxDataStatic: false, auxDataConfig: true, auxDataStats: false);
                SelectNode(level: LevelConfigCollection, id: id);
                return;
            }
            else
            {
                switch (status)
                {
                    case ConfigCollectionStatus.ContainsDependentConfigCollections:
                        MessageBox.Show(
                           text: msgOpenConfCollContainsDependent,
                           caption: String.Empty,
                           buttons: MessageBoxButtons.OK,
                           icon: MessageBoxIcon.Information);
                        return;

                    case ConfigCollectionStatus.ContainsCalculatedAuxTotals:
                        MessageBox.Show(
                           text: msgOpenConfCollContainsCalculatedAuxTotals,
                           caption: String.Empty,
                           buttons: MessageBoxButtons.OK,
                           icon: MessageBoxIcon.Information);
                        return;

                    case ConfigCollectionStatus.ContainsCalculatedAuxData:
                        MessageBox.Show(
                           text: msgOpenConfCollContainsCalculatedAuxData,
                           caption: String.Empty,
                           buttons: MessageBoxButtons.OK,
                           icon: MessageBoxIcon.Information);
                        return;

                    case ConfigCollectionStatus.IsOpened:
                        MessageBox.Show(
                           text: msgOpenConfCollIsOpened,
                           caption: String.Empty,
                           buttons: MessageBoxButtons.OK,
                           icon: MessageBoxIcon.Information);
                        return;

                    default:
                        return;
                }
            }
        }


        /// <summary>
        /// <para lang="cs">
        /// Zobrazení a výpočet úhrnů pomocné proměnné
        /// a hodnot pomocné proměnné v inventarizačních bodech
        /// </para>
        /// <para lang="en">
        /// Displaying and calculating auxiliary variable totals
        /// and auxiliary variable values at inventory points</para>
        /// </summary>
        private void DisplayData()
        {
            if (AvailableConfigCollection == null)
            {
                return;
            }

            if (AvailableConfigCollection.IsForTotals)
            {
                FormCalcTotal frmCalcTotal = new(controlOwner: this);
                if (frmCalcTotal.ShowDialog() == DialogResult.OK) { }
                LoadContent(catalog: false, nfiesta: false, auxDataStatic: false, auxDataConfig: true, auxDataStats: true);
                SelectNode(level: LevelConfigCollection, id: AvailableConfigCollection.Id);
                return;
            }

            if (AvailableConfigCollection.IsForIntersection)
            {
                // TODO
                return;

                //FormCalcData frmCalcData = new(controlOwner: this);
                //if (frmCalcData.ShowDialog() == DialogResult.OK) { }
                //LoadConfigurationList();
                //SelectNode(level: LevelConfigCollection, id: AvailableConfigCollection.Id);
                //return;
            }
        }

        #endregion Methods


        #region Event Handlers - Tree View Node Drag and Drop

        ///// <summary>
        ///// Zahajeni pretahovani nodu konfigurace do jine skupiny konfiguraci
        ///// </summary>
        ///// <param name="sender">Objekt odesilajici udalost (TreeView)</param>
        ///// <param name="e">Parametry udalosti</param>
        //private void TreeConfigurations_ItemDrag(object sender, ItemDragEventArgs e)
        //{
        //    // pretahovany node
        //    TreeNode node = (TreeNode)e.Item;

        //    // pretahovani je mozne levym tlacitkem mysi
        //    if (e.Button == MouseButtons.Left)
        //    {
        //        // pretahuji se pouze nody obsahujici konfigurace
        //        if (node.Level == LevelConfiguration)
        //        {
        //            DoDragDrop(node, DragDropEffects.Copy);
        //        }
        //    }
        //}

        ///// <summary>
        ///// Pri pretazeni nodu mysi do klientske oblasti
        ///// </summary>
        ///// <param name="sender">Objekt odesilajici udalost (TreeView)</param>
        ///// <param name="e">Parametry udalosti</param>
        //private void TreeConfigurations_DragEnter(object sender, DragEventArgs e)
        //{
        //    e.Effect = e.AllowedEffect;
        //}

        ///// <summary>
        ///// Pri polozeni nodu konfigurace do jine skupiny konfiguraci
        ///// </summary>
        ///// <param name="sender">Objekt odesilajici udalost (TreeView)</param>
        ///// <param name="e">Parametry udalosti</param>
        //private void TreeConfigurations_DragDrop(object sender, DragEventArgs e)
        //{
        //    // souradnice uvnitr TreeView,
        //    // na kterych bylo provedeno Drop:
        //    Point targetPoint = tvwConfigurations.PointToClient(new Point(e.X, e.Y));

        //    // TreeNode,
        //    // na kterem bylo provedeno Drop (cilovy TreeNode):
        //    TreeNode targetNode = tvwConfigurations.GetNodeAt(targetPoint);

        //    // TreeNode,
        //    // ktery byl pretahovan
        //    TreeNode draggedNode = (TreeNode)e.Data.GetData(typeof(TreeNode));

        //    // Konfigurace,
        //    // ktera byla pretahovana
        //    Configuration draggedConfiguration = (Configuration)draggedNode.Tag;

        //    // Jestlize existuje vybrany node, urci skupinu konfiguraci prirazenou k vybranemu node
        //    ConfigCollection targetCollection = null;
        //    if (targetNode != null)
        //    {
        //        switch (targetNode.Level)
        //        {
        //            case 0:
        //                // do urovne funkci nelze node s konfiguraci vlozit
        //                break;
        //            case 1:
        //                // vlozeni do konfigurace do jine skupiny konfiguraci
        //                targetCollection = (ConfigCollection)targetNode.Tag;
        //                break;
        //            case 2:
        //                // vlozeni konfigurace do jine skupiny konfiguraci
        //                targetCollection = ((Configuration)targetNode.Tag).ConfigCollection;
        //                break;
        //            default:
        //                break;
        //        }
        //    }

        //    // Pro vlozeni konfigurace musi existovat cilova skupina konfiguraci
        //    if (targetCollection != null)
        //    {
        //        // Pro vlozeni konfigurace cilova skupina nesmi byt uzamcena
        //        if (!targetCollection.Closed)
        //        {
        //            // Pro vlozeni konfigurace cilova skupina nesmi byt stejna jako zdrojova skupina
        //            if (targetCollection.Id != draggedConfiguration.ConfigCollectionId)
        //            {
        //                // Provede se vytvoreni kopie pretahovane konfigurace a jeji ulozeni do cilove skupiny
        //                Configuration config = moduleDatabase.GisData.Configurations.NewConfiguration(targetCollection);

        //                config.ConfigQueryId = 500;
        //                config.Name = draggedConfiguration.Name;
        //                config.Description = draggedConfiguration.Description;
        //                config.Complete = String.Empty;
        //                config.Categories = draggedConfiguration.Id.ToString();
        //                config.VectorId = null;
        //                config.RasterId = null;
        //                config.Raster1Id = null;
        //                config.Band = null;
        //                config.Reclass = null;
        //                config.Condition = String.Empty;
        //                config.Tag = draggedConfiguration.Tag;

        //                config.Insert();
        //                config.Configurations.ModuleDatabase.GisData.Load();

        //                RefreshTreeView();

        //                SelectNode(2, config.Id);
        //            }
        //        }
        //    }
        //}

        #endregion Event Handlers - Tree View Node Drag and Drop

    }

}