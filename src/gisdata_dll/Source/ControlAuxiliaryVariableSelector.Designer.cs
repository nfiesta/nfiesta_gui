﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi.ModuleAuxiliaryData
{

    partial class ControlAuxiliaryVariableSelector
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            grpMain = new System.Windows.Forms.GroupBox();
            tlpMain = new System.Windows.Forms.TableLayoutPanel();
            tlpButtons = new System.Windows.Forms.TableLayoutPanel();
            pnlPrevious = new System.Windows.Forms.Panel();
            btnCancel = new System.Windows.Forms.Button();
            pnlNext = new System.Windows.Forms.Panel();
            btnSelect = new System.Windows.Forms.Button();
            tlpDescription = new System.Windows.Forms.TableLayoutPanel();
            lblIdCaption = new System.Windows.Forms.Label();
            lblLabelCaption = new System.Windows.Forms.Label();
            lblDescriptionCaption = new System.Windows.Forms.Label();
            lblIdValue = new System.Windows.Forms.Label();
            lblLabelValue = new System.Windows.Forms.Label();
            lblDescriptionValue = new System.Windows.Forms.Label();
            grpMain.SuspendLayout();
            tlpMain.SuspendLayout();
            tlpButtons.SuspendLayout();
            pnlPrevious.SuspendLayout();
            pnlNext.SuspendLayout();
            tlpDescription.SuspendLayout();
            SuspendLayout();
            // 
            // grpMain
            // 
            grpMain.Controls.Add(tlpMain);
            grpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            grpMain.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            grpMain.ForeColor = System.Drawing.Color.MediumBlue;
            grpMain.Location = new System.Drawing.Point(0, 0);
            grpMain.Margin = new System.Windows.Forms.Padding(0);
            grpMain.Name = "grpMain";
            grpMain.Padding = new System.Windows.Forms.Padding(5);
            grpMain.Size = new System.Drawing.Size(960, 540);
            grpMain.TabIndex = 1;
            grpMain.TabStop = false;
            grpMain.Text = "grpMain";
            // 
            // tlpMain
            // 
            tlpMain.ColumnCount = 2;
            tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            tlpMain.Controls.Add(tlpButtons, 0, 0);
            tlpMain.Controls.Add(tlpDescription, 0, 0);
            tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpMain.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            tlpMain.ForeColor = System.Drawing.Color.Black;
            tlpMain.Location = new System.Drawing.Point(5, 25);
            tlpMain.Margin = new System.Windows.Forms.Padding(0);
            tlpMain.Name = "tlpMain";
            tlpMain.RowCount = 1;
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.Size = new System.Drawing.Size(950, 510);
            tlpMain.TabIndex = 0;
            // 
            // tlpButtons
            // 
            tlpButtons.ColumnCount = 1;
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.Controls.Add(pnlPrevious, 0, 0);
            tlpButtons.Controls.Add(pnlNext, 0, 1);
            tlpButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpButtons.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            tlpButtons.ForeColor = System.Drawing.Color.Black;
            tlpButtons.Location = new System.Drawing.Point(790, 0);
            tlpButtons.Margin = new System.Windows.Forms.Padding(0);
            tlpButtons.Name = "tlpButtons";
            tlpButtons.RowCount = 3;
            tlpButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            tlpButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            tlpButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.Size = new System.Drawing.Size(160, 510);
            tlpButtons.TabIndex = 2;
            // 
            // pnlPrevious
            // 
            pnlPrevious.Controls.Add(btnCancel);
            pnlPrevious.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlPrevious.Location = new System.Drawing.Point(0, 0);
            pnlPrevious.Margin = new System.Windows.Forms.Padding(0);
            pnlPrevious.Name = "pnlPrevious";
            pnlPrevious.Padding = new System.Windows.Forms.Padding(5);
            pnlPrevious.Size = new System.Drawing.Size(160, 40);
            pnlPrevious.TabIndex = 15;
            // 
            // btnCancel
            // 
            btnCancel.Dock = System.Windows.Forms.DockStyle.Fill;
            btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            btnCancel.Location = new System.Drawing.Point(5, 5);
            btnCancel.Margin = new System.Windows.Forms.Padding(0);
            btnCancel.Name = "btnCancel";
            btnCancel.Size = new System.Drawing.Size(150, 30);
            btnCancel.TabIndex = 9;
            btnCancel.Text = "btnCancel";
            btnCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            btnCancel.UseVisualStyleBackColor = true;
            // 
            // pnlNext
            // 
            pnlNext.Controls.Add(btnSelect);
            pnlNext.Location = new System.Drawing.Point(0, 40);
            pnlNext.Margin = new System.Windows.Forms.Padding(0);
            pnlNext.Name = "pnlNext";
            pnlNext.Padding = new System.Windows.Forms.Padding(5);
            pnlNext.Size = new System.Drawing.Size(160, 40);
            pnlNext.TabIndex = 16;
            // 
            // btnSelect
            // 
            btnSelect.Dock = System.Windows.Forms.DockStyle.Fill;
            btnSelect.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            btnSelect.Location = new System.Drawing.Point(5, 5);
            btnSelect.Margin = new System.Windows.Forms.Padding(0);
            btnSelect.Name = "btnSelect";
            btnSelect.Size = new System.Drawing.Size(150, 30);
            btnSelect.TabIndex = 9;
            btnSelect.Text = "btnSelect";
            btnSelect.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            btnSelect.UseVisualStyleBackColor = true;
            // 
            // tlpDescription
            // 
            tlpDescription.ColumnCount = 2;
            tlpDescription.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            tlpDescription.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpDescription.Controls.Add(lblIdCaption, 0, 0);
            tlpDescription.Controls.Add(lblLabelCaption, 0, 1);
            tlpDescription.Controls.Add(lblDescriptionCaption, 0, 2);
            tlpDescription.Controls.Add(lblIdValue, 1, 0);
            tlpDescription.Controls.Add(lblLabelValue, 1, 1);
            tlpDescription.Controls.Add(lblDescriptionValue, 1, 2);
            tlpDescription.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpDescription.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            tlpDescription.ForeColor = System.Drawing.Color.Black;
            tlpDescription.Location = new System.Drawing.Point(0, 0);
            tlpDescription.Margin = new System.Windows.Forms.Padding(0);
            tlpDescription.Name = "tlpDescription";
            tlpDescription.RowCount = 4;
            tlpDescription.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            tlpDescription.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            tlpDescription.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            tlpDescription.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpDescription.Size = new System.Drawing.Size(790, 510);
            tlpDescription.TabIndex = 1;
            // 
            // lblIdCaption
            // 
            lblIdCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblIdCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblIdCaption.Location = new System.Drawing.Point(0, 0);
            lblIdCaption.Margin = new System.Windows.Forms.Padding(0);
            lblIdCaption.Name = "lblIdCaption";
            lblIdCaption.Size = new System.Drawing.Size(150, 25);
            lblIdCaption.TabIndex = 1;
            lblIdCaption.Text = "lblIdCaption";
            lblIdCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblLabelCaption
            // 
            lblLabelCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblLabelCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblLabelCaption.Location = new System.Drawing.Point(0, 25);
            lblLabelCaption.Margin = new System.Windows.Forms.Padding(0);
            lblLabelCaption.Name = "lblLabelCaption";
            lblLabelCaption.Size = new System.Drawing.Size(150, 25);
            lblLabelCaption.TabIndex = 9;
            lblLabelCaption.Text = "lblLabelCaption";
            lblLabelCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDescriptionCaption
            // 
            lblDescriptionCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            lblDescriptionCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblDescriptionCaption.Location = new System.Drawing.Point(0, 50);
            lblDescriptionCaption.Margin = new System.Windows.Forms.Padding(0);
            lblDescriptionCaption.Name = "lblDescriptionCaption";
            lblDescriptionCaption.Size = new System.Drawing.Size(150, 25);
            lblDescriptionCaption.TabIndex = 11;
            lblDescriptionCaption.Text = "lblDescriptionCaption";
            lblDescriptionCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblIdValue
            // 
            lblIdValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblIdValue.Location = new System.Drawing.Point(150, 0);
            lblIdValue.Margin = new System.Windows.Forms.Padding(0);
            lblIdValue.Name = "lblIdValue";
            lblIdValue.Size = new System.Drawing.Size(640, 25);
            lblIdValue.TabIndex = 12;
            lblIdValue.Text = "lblIdValue";
            lblIdValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblLabelValue
            // 
            lblLabelValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblLabelValue.Location = new System.Drawing.Point(150, 25);
            lblLabelValue.Margin = new System.Windows.Forms.Padding(0);
            lblLabelValue.Name = "lblLabelValue";
            lblLabelValue.Size = new System.Drawing.Size(640, 25);
            lblLabelValue.TabIndex = 13;
            lblLabelValue.Text = "lblLabelValue";
            lblLabelValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDescriptionValue
            // 
            lblDescriptionValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblDescriptionValue.Location = new System.Drawing.Point(150, 50);
            lblDescriptionValue.Margin = new System.Windows.Forms.Padding(0);
            lblDescriptionValue.Name = "lblDescriptionValue";
            lblDescriptionValue.Size = new System.Drawing.Size(640, 25);
            lblDescriptionValue.TabIndex = 14;
            lblDescriptionValue.Text = "lblDescriptionValue";
            lblDescriptionValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ControlAuxiliaryVariableSelector
            // 
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            BackColor = System.Drawing.Color.Transparent;
            BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            Controls.Add(grpMain);
            ForeColor = System.Drawing.Color.Black;
            Margin = new System.Windows.Forms.Padding(0);
            Name = "ControlAuxiliaryVariableSelector";
            Size = new System.Drawing.Size(960, 540);
            grpMain.ResumeLayout(false);
            tlpMain.ResumeLayout(false);
            tlpButtons.ResumeLayout(false);
            pnlPrevious.ResumeLayout(false);
            pnlNext.ResumeLayout(false);
            tlpDescription.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.GroupBox grpMain;
        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.TableLayoutPanel tlpButtons;
        private System.Windows.Forms.Panel pnlPrevious;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Panel pnlNext;
        private System.Windows.Forms.Button btnSelect;
        private System.Windows.Forms.TableLayoutPanel tlpDescription;
        private System.Windows.Forms.Label lblIdCaption;
        private System.Windows.Forms.Label lblLabelCaption;
        private System.Windows.Forms.Label lblDescriptionCaption;
        private System.Windows.Forms.Label lblIdValue;
        private System.Windows.Forms.Label lblLabelValue;
        private System.Windows.Forms.Label lblDescriptionValue;
    }

}
