﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi.ModuleAuxiliaryData
{

    partial class ControlVectorGeom
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            tlpMain = new System.Windows.Forms.TableLayoutPanel();
            tlpButtons = new System.Windows.Forms.TableLayoutPanel();
            pnlNext = new System.Windows.Forms.Panel();
            btnNext = new System.Windows.Forms.Button();
            pnlPrevious = new System.Windows.Forms.Panel();
            btnPrevious = new System.Windows.Forms.Button();
            grpColumnName = new System.Windows.Forms.GroupBox();
            grpColumnIdent = new System.Windows.Forms.GroupBox();
            grpUnitCoefficient = new System.Windows.Forms.GroupBox();
            chkUnitSelection = new System.Windows.Forms.CheckBox();
            lblUnitCoefficient = new System.Windows.Forms.Label();
            tlpMain.SuspendLayout();
            tlpButtons.SuspendLayout();
            pnlNext.SuspendLayout();
            pnlPrevious.SuspendLayout();
            grpUnitCoefficient.SuspendLayout();
            SuspendLayout();
            // 
            // tlpMain
            // 
            tlpMain.ColumnCount = 1;
            tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.Controls.Add(tlpButtons, 0, 4);
            tlpMain.Controls.Add(grpColumnName, 0, 1);
            tlpMain.Controls.Add(grpColumnIdent, 0, 0);
            tlpMain.Controls.Add(grpUnitCoefficient, 0, 2);
            tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpMain.Location = new System.Drawing.Point(0, 0);
            tlpMain.Margin = new System.Windows.Forms.Padding(0);
            tlpMain.Name = "tlpMain";
            tlpMain.RowCount = 5;
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            tlpMain.Size = new System.Drawing.Size(960, 540);
            tlpMain.TabIndex = 1;
            // 
            // tlpButtons
            // 
            tlpButtons.BackColor = System.Drawing.Color.Transparent;
            tlpButtons.ColumnCount = 3;
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            tlpButtons.Controls.Add(pnlNext, 2, 0);
            tlpButtons.Controls.Add(pnlPrevious, 1, 0);
            tlpButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpButtons.Location = new System.Drawing.Point(0, 500);
            tlpButtons.Margin = new System.Windows.Forms.Padding(0);
            tlpButtons.Name = "tlpButtons";
            tlpButtons.RowCount = 1;
            tlpButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.Size = new System.Drawing.Size(960, 40);
            tlpButtons.TabIndex = 17;
            // 
            // pnlNext
            // 
            pnlNext.Controls.Add(btnNext);
            pnlNext.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlNext.Location = new System.Drawing.Point(800, 0);
            pnlNext.Margin = new System.Windows.Forms.Padding(0);
            pnlNext.Name = "pnlNext";
            pnlNext.Padding = new System.Windows.Forms.Padding(5);
            pnlNext.Size = new System.Drawing.Size(160, 40);
            pnlNext.TabIndex = 13;
            // 
            // btnNext
            // 
            btnNext.Dock = System.Windows.Forms.DockStyle.Fill;
            btnNext.Enabled = false;
            btnNext.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            btnNext.Location = new System.Drawing.Point(5, 5);
            btnNext.Margin = new System.Windows.Forms.Padding(0);
            btnNext.Name = "btnNext";
            btnNext.Size = new System.Drawing.Size(150, 30);
            btnNext.TabIndex = 9;
            btnNext.Text = "btnNext";
            btnNext.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            btnNext.UseVisualStyleBackColor = true;
            // 
            // pnlPrevious
            // 
            pnlPrevious.Controls.Add(btnPrevious);
            pnlPrevious.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlPrevious.Location = new System.Drawing.Point(640, 0);
            pnlPrevious.Margin = new System.Windows.Forms.Padding(0);
            pnlPrevious.Name = "pnlPrevious";
            pnlPrevious.Padding = new System.Windows.Forms.Padding(5);
            pnlPrevious.Size = new System.Drawing.Size(160, 40);
            pnlPrevious.TabIndex = 14;
            // 
            // btnPrevious
            // 
            btnPrevious.Dock = System.Windows.Forms.DockStyle.Fill;
            btnPrevious.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            btnPrevious.Location = new System.Drawing.Point(5, 5);
            btnPrevious.Margin = new System.Windows.Forms.Padding(0);
            btnPrevious.Name = "btnPrevious";
            btnPrevious.Size = new System.Drawing.Size(150, 30);
            btnPrevious.TabIndex = 9;
            btnPrevious.Text = "btnPrevious";
            btnPrevious.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            btnPrevious.UseVisualStyleBackColor = true;
            // 
            // grpColumnName
            // 
            grpColumnName.Dock = System.Windows.Forms.DockStyle.Fill;
            grpColumnName.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            grpColumnName.ForeColor = System.Drawing.Color.MediumBlue;
            grpColumnName.Location = new System.Drawing.Point(0, 60);
            grpColumnName.Margin = new System.Windows.Forms.Padding(0);
            grpColumnName.Name = "grpColumnName";
            grpColumnName.Padding = new System.Windows.Forms.Padding(5);
            grpColumnName.Size = new System.Drawing.Size(960, 60);
            grpColumnName.TabIndex = 9;
            grpColumnName.TabStop = false;
            grpColumnName.Text = "grpColumnName";
            // 
            // grpColumnIdent
            // 
            grpColumnIdent.Dock = System.Windows.Forms.DockStyle.Fill;
            grpColumnIdent.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            grpColumnIdent.ForeColor = System.Drawing.Color.MediumBlue;
            grpColumnIdent.Location = new System.Drawing.Point(0, 0);
            grpColumnIdent.Margin = new System.Windows.Forms.Padding(0);
            grpColumnIdent.Name = "grpColumnIdent";
            grpColumnIdent.Padding = new System.Windows.Forms.Padding(5);
            grpColumnIdent.Size = new System.Drawing.Size(960, 60);
            grpColumnIdent.TabIndex = 8;
            grpColumnIdent.TabStop = false;
            grpColumnIdent.Text = "grpColumnIdent";
            // 
            // grpUnitCoefficient
            // 
            grpUnitCoefficient.Controls.Add(chkUnitSelection);
            grpUnitCoefficient.Controls.Add(lblUnitCoefficient);
            grpUnitCoefficient.Dock = System.Windows.Forms.DockStyle.Fill;
            grpUnitCoefficient.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            grpUnitCoefficient.ForeColor = System.Drawing.Color.MediumBlue;
            grpUnitCoefficient.Location = new System.Drawing.Point(0, 120);
            grpUnitCoefficient.Margin = new System.Windows.Forms.Padding(0);
            grpUnitCoefficient.Name = "grpUnitCoefficient";
            grpUnitCoefficient.Padding = new System.Windows.Forms.Padding(5);
            grpUnitCoefficient.Size = new System.Drawing.Size(960, 80);
            grpUnitCoefficient.TabIndex = 3;
            grpUnitCoefficient.TabStop = false;
            grpUnitCoefficient.Text = "boxUnitCoefficient";
            // 
            // chkUnitSelection
            // 
            chkUnitSelection.AutoSize = true;
            chkUnitSelection.Dock = System.Windows.Forms.DockStyle.Top;
            chkUnitSelection.Font = new System.Drawing.Font("Segoe UI", 9F);
            chkUnitSelection.ForeColor = System.Drawing.Color.Black;
            chkUnitSelection.Location = new System.Drawing.Point(5, 50);
            chkUnitSelection.Margin = new System.Windows.Forms.Padding(0);
            chkUnitSelection.Name = "chkUnitSelection";
            chkUnitSelection.Size = new System.Drawing.Size(950, 19);
            chkUnitSelection.TabIndex = 1;
            chkUnitSelection.Text = "chkUnitSelection";
            chkUnitSelection.UseVisualStyleBackColor = true;
            // 
            // lblUnitCoefficient
            // 
            lblUnitCoefficient.Dock = System.Windows.Forms.DockStyle.Top;
            lblUnitCoefficient.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            lblUnitCoefficient.ForeColor = System.Drawing.Color.Black;
            lblUnitCoefficient.Location = new System.Drawing.Point(5, 25);
            lblUnitCoefficient.Margin = new System.Windows.Forms.Padding(0);
            lblUnitCoefficient.Name = "lblUnitCoefficient";
            lblUnitCoefficient.Size = new System.Drawing.Size(950, 25);
            lblUnitCoefficient.TabIndex = 0;
            lblUnitCoefficient.Text = "lblUnitCoefficient";
            lblUnitCoefficient.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ControlVectorGeom
            // 
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            BackColor = System.Drawing.Color.Transparent;
            BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            Controls.Add(tlpMain);
            ForeColor = System.Drawing.Color.Black;
            Margin = new System.Windows.Forms.Padding(0);
            Name = "ControlVectorGeom";
            Size = new System.Drawing.Size(960, 540);
            tlpMain.ResumeLayout(false);
            tlpButtons.ResumeLayout(false);
            pnlNext.ResumeLayout(false);
            pnlPrevious.ResumeLayout(false);
            grpUnitCoefficient.ResumeLayout(false);
            grpUnitCoefficient.PerformLayout();
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.GroupBox grpColumnName;
        private System.Windows.Forms.GroupBox grpColumnIdent;
        private System.Windows.Forms.GroupBox grpUnitCoefficient;
        
        private System.Windows.Forms.Label lblUnitCoefficient;
        private System.Windows.Forms.CheckBox chkUnitSelection;
        private System.Windows.Forms.TableLayoutPanel tlpButtons;
        private System.Windows.Forms.Panel pnlNext;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Panel pnlPrevious;
        private System.Windows.Forms.Button btnPrevious;
    }

}
