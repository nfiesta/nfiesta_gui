﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.AuxiliaryData;
using ZaJi.PostgreSQL;

namespace ZaJi.ModuleAuxiliaryData
{

    /// <summary>
    /// <para lang="cs">Ovládací prvek "Výběr skupiny konfigurací"</para>
    /// <para lang="en">Control "Configuration collection selection"</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    internal partial class ControlConfigCollectionSelector
        : UserControl, INfiEstaControl, IAuxiliaryDataControl
    {

        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Seznam konfiguračních funkcí</para>
        /// <para lang="en">List of configuration functions</para>
        /// </summary>
        private List<ConfigFunction> configFunctions;

        /// <summary>
        /// <para lang="cs">Skupina konfigurací</para>
        /// <para lang="en">Configuration collection</para>
        /// </summary>
        private ConfigCollection configCollection;

        private string strConfigCollectionPoint = String.Empty;
        private string strConfigCollectionGis = String.Empty;

        #endregion Private Fields


        #region Events

        /// <summary>
        /// <para lang="cs">Událost "Změna vybrané skupiny konfigurací"</para>
        /// <para lang="en">Event "Configuration collection changed"</para>
        /// </summary>
        public event EventHandler ConfigCollectionChanged;

        #endregion Events


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor ovládacího prvku</para>
        /// <para lang="en">Control constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        /// <param name="configFunctions">
        /// <para lang="cs">Seznam konfiguračních funkcí</para>
        /// <para lang="en">List of configuration functions</para>
        /// </param>
        public ControlConfigCollectionSelector(
            Control controlOwner,
            List<ConfigFunction> configFunctions)
        {
            InitializeComponent();
            Initialize(
                controlOwner: controlOwner,
                configFunctions: configFunctions);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                IAuxiliaryDataControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                return controlOwner;
            }
            set
            {
                IAuxiliaryDataControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables (read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((IAuxiliaryDataControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro pomocná data (read-only)</para>
        /// <para lang="en">Module for auxiliary data setting (read-only)</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((IAuxiliaryDataControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">Seznam konfiguračních funkcí</para>
        /// <para lang="en">List of configuration functions</para>
        /// </summary>
        public List<ConfigFunction> ConfigFunctions
        {
            get
            {
                return configFunctions ?? [];
            }
            private set
            {
                configFunctions = value ?? [];

                InitializeLabels();
            }
        }

        /// <summary>
        /// <para lang="cs">Skupina konfigurací</para>
        /// <para lang="en">Configuration collection</para>
        /// </summary>
        public ConfigCollection ConfigCollection
        {
            get
            {
                return configCollection;
            }
            set
            {
                configCollection = value;

                InitializeLabels();
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(btnCancel),                    "Zrušit výběr" },
                        { nameof(btnSelect),                    "Vybrat skupinu" },
                        { nameof(lblDescriptionCaption),        "Popis:" },
                        { nameof(lblIdCaption),                 "ID:" },
                        { nameof(lblLabelCaption),              "Název:" },
                        { nameof(strConfigCollectionPoint),     "Skupina konfigurací pro bodovou vrstvu:" },
                        { nameof(strConfigCollectionGis),       "Skupina konfigurací pro GIS vrstvu úhrnů:" }
                    }
                    : languageFile.NationalVersion.Data.TryGetValue(
                        key: nameof(ControlConfigCollectionSelector),
                        out Dictionary<string, string> dictNational)
                            ? dictNational
                            : [],

                LanguageVersion.International => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(btnCancel),                    "Cancel selection" },
                        { nameof(btnSelect),                    "Select collection" },
                        { nameof(lblDescriptionCaption),        "Description:" },
                        { nameof(lblIdCaption),                 "ID:" },
                        { nameof(lblLabelCaption),              "Name:" },
                        { nameof(strConfigCollectionPoint),     "Configuration collection for point layer:" },
                        { nameof(strConfigCollectionGis),       "Configuration collection for GIS layer of totals:" }
                    }
                    : languageFile.InternationalVersion.Data.TryGetValue(
                        key: nameof(ControlConfigCollectionSelector),
                        out Dictionary<string, string> dictInternational)
                            ? dictInternational
                            : [],

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku</para>
        /// <para lang="en">Initialization of the control</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        /// <param name="configFunctions">
        /// <para lang="cs">Seznam konfiguračních funkcí</para>
        /// <para lang="en">List of configuration functions</para>
        /// </param>
        private void Initialize(
            Control controlOwner,
            List<ConfigFunction> configFunctions)
        {
            BorderStyle = BorderStyle.None;
            ControlOwner = controlOwner;
            ConfigFunctions = configFunctions;

            btnCancel.Click += new EventHandler(
                (sender, e) =>
                {
                    ConfigCollection = null;
                    ConfigCollectionChanged?.Invoke(sender: this, e: new EventArgs());
                });

            btnSelect.Click += new EventHandler(
               (sender, e) =>
               {
                   FormConfigCollectionSelector frmConfigCollectionSelector =
                        new(controlOwner: this, configFunctions: ConfigFunctions)
                        {
                            ConfigCollection = ConfigCollection
                        };

                   if (frmConfigCollectionSelector.ShowDialog() == DialogResult.OK)
                   {
                       ConfigCollection = frmConfigCollectionSelector.ConfigCollection;
                       ConfigCollectionChanged?.Invoke(sender: this, e: new EventArgs());
                   }
               });
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků ovládacího prvku</para>
        /// <para lang="en">Initialization of control labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            btnCancel.Text =
                labels.TryGetValue(key: nameof(btnCancel),
                out string btnCancelText)
                    ? btnCancelText
                    : String.Empty;

            btnSelect.Text =
                labels.TryGetValue(key: nameof(btnSelect),
                out string btnSelectText)
                    ? btnSelectText
                    : String.Empty;

            strConfigCollectionPoint =
                labels.TryGetValue(key: nameof(strConfigCollectionPoint),
                out strConfigCollectionPoint)
                    ? strConfigCollectionPoint
                    : String.Empty;

            strConfigCollectionGis =
                labels.TryGetValue(key: nameof(strConfigCollectionGis),
                out strConfigCollectionGis)
                    ? strConfigCollectionGis
                    : String.Empty;

            grpMain.Text =
                ConfigFunctions.Select(a => a.Value)
                .Contains(value: ConfigFunctionEnum.PointLayer)
                    ? strConfigCollectionPoint
                    : strConfigCollectionGis;

            lblIdCaption.Text =
                labels.TryGetValue(key: nameof(lblIdCaption),
                out string lblIdCaptionText)
                    ? lblIdCaptionText
                    : String.Empty;

            lblIdValue.Text = (ConfigCollection?.Id != null)
                ? ConfigCollection.Id.ToString()
                : String.Empty;

            lblLabelCaption.Text =
                labels.TryGetValue(key: nameof(lblLabelCaption),
                out string lblLabelCaptionText)
                    ? lblLabelCaptionText
                    : String.Empty;

            lblLabelValue.Text =
                (LanguageVersion == LanguageVersion.International)
                    ? ConfigCollection?.LabelEn ?? String.Empty
                    : (LanguageVersion == LanguageVersion.National)
                        ? ConfigCollection?.LabelCs ?? String.Empty
                        : ConfigCollection?.LabelEn ?? String.Empty;

            lblDescriptionCaption.Text =
               labels.TryGetValue(key: nameof(lblDescriptionCaption),
               out string lblDescriptionCaptionText)
                   ? lblDescriptionCaptionText
                   : String.Empty;

            lblDescriptionValue.Text =
                (LanguageVersion == LanguageVersion.International)
                    ? ConfigCollection?.DescriptionEn ?? String.Empty
                    : (LanguageVersion == LanguageVersion.National)
                        ? ConfigCollection?.DescriptionCs ?? String.Empty
                        : ConfigCollection?.DescriptionEn ?? String.Empty;
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent() { }

        #endregion Methods

    }
}