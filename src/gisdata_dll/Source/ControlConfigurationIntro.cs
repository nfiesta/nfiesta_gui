﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.AuxiliaryData;
using ZaJi.NfiEstaPg.Core;
using ZaJi.PostgreSQL;

namespace ZaJi.ModuleAuxiliaryData
{

    /// <summary>
    /// <para lang="cs">Ovládací prvek "Nová konfigurace"</para>
    /// <para lang="en">Control "New configuration"</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    internal partial class ControlConfigurationIntro
        : UserControl, INfiEstaControl, IAuxiliaryDataControl
    {

        #region Constants

        /// <summary>
        /// <para lang="cs">Index skupiny "Název konfigurace"</para>
        /// <para lang="en">Group "Configuration name" index</para>
        /// </summary>
        private const int grpConfigurationLabelCsIndex = 0;

        /// <summary>
        /// <para lang="cs">Výška skupiny "Název konfigurace"</para>
        /// <para lang="en">Group "Configuration name" height</para>
        /// </summary>
        private const int grpConfigurationLabelCsHeight = 60;


        /// <summary>
        /// <para lang="cs">Index skupiny "Název konfigurace v angličtině"</para>
        /// <para lang="en">Group "Configuration name in English" index</para>
        /// </summary>
        private const int grpConfigurationLabelEnIndex = 1;

        /// <summary>
        /// <para lang="cs">Výška skupiny "Název konfigurace v angličtině"</para>
        /// <para lang="en">Group "Configuration name in English" height</para>
        /// </summary>
        private const int grpConfigurationLabelEnHeight = 60;


        /// <summary>
        /// <para lang="cs">Index skupiny "Popis konfigurace"</para>
        /// <para lang="en">Group "Configuration description" index</para>
        /// </summary>
        private const int grpConfigurationDescriptionCsIndex = 2;

        /// <summary>
        /// <para lang="cs">Výška skupiny "Popis konfigurace"</para>
        /// <para lang="en">Group "Configuration description" height</para>
        /// </summary>
        private const int grpConfigurationDescriptionCsHeight = 60;


        /// <summary>
        /// <para lang="cs">Index skupiny "Popis konfigurace v angličtině"</para>
        /// <para lang="en">Group "Configuration description in English" index</para>
        /// </summary>
        private const int grpConfigurationDescriptionEnIndex = 3;

        /// <summary>
        /// <para lang="cs">Výška skupiny "Popis konfigurace v angličtině"</para>
        /// <para lang="en">Group "Configuration description in English" height</para>
        /// </summary>
        private const int grpConfigurationDescriptionEnHeight = 60;


        /// <summary>
        /// <para lang="cs">Index skupiny "Varianta zpracování výsledku"</para>
        /// <para lang="en">Group "Option for processing result" index</para>
        /// </summary>
        private const int grpConfigQueryIndex = 4;

        /// <summary>
        /// <para lang="cs">Výška skupiny "Varianta zpracování výsledku"</para>
        /// <para lang="en">Group "Option for processing result" height</para>
        /// </summary>
        private const int grpConfigQueryHeight = 180;


        /// <summary>
        /// <para lang="cs">Index skupiny "Kategorie pomocné proměnné"</para>
        /// <para lang="en">Group "Auxiliary variable category" index</para>
        /// </summary>
        private const int pnlAuxiliaryVariableCategoryIndex = 5;

        /// <summary>
        /// <para lang="cs">Výška skupiny "Kategorie pomocné proměnné"</para>
        /// <para lang="en">Group "Auxiliary variable category" height</para>
        /// </summary>
        private const int pnlAuxiliaryVariableCategoryHeight = 120;

        #endregion Constants


        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        private Control controlOwner;

        #endregion Private Fields


        #region Events

        /// <summary>
        /// <para lang="cs">Událost kliknutí na tlačítko "Další"</para>
        /// <para lang="en">Click event on the "Next" button</para>
        /// </summary>
        public event EventHandler NextClick;

        #endregion Events


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor ovládacího prvku</para>
        /// <para lang="en">Control constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        public ControlConfigurationIntro(Control controlOwner)
        {
            InitializeComponent();
            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                IAuxiliaryDataControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                if (controlOwner is not FormConfigurationGuide)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(FormConfigurationGuide)}.",
                        paramName: nameof(ControlOwner));
                }

                return controlOwner;
            }
            set
            {
                IAuxiliaryDataControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                if (value is not FormConfigurationGuide)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(FormConfigurationGuide)}.",
                        paramName: nameof(ControlOwner));
                }

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables (read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((IAuxiliaryDataControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro pomocná data (read-only)</para>
        /// <para lang="en">Module for auxiliary data setting (read-only)</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((IAuxiliaryDataControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">Konfigurace</para>
        /// <para lang="en">Configuration</para>
        /// </summary>
        public Config Configuration
        {
            get
            {
                return ((FormConfigurationGuide)ControlOwner).Configuration;
            }
        }

        /// <summary>
        /// <para lang="cs">
        /// Provede se zápis nové (newEntry = true)
        /// nebo uprava existující konfigurace (newEntry = false) do databáze (read-only)
        /// </para>
        /// A new entry is inserted (newEntry = true)
        /// or updated an existing configuration (newEntry = false) into the database (read-only)
        /// </summary>
        private bool NewEntry
        {
            get
            {
                return ((FormConfigurationGuide)ControlOwner).NewEntry;
            }
        }

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Výběr kategorie pomocné proměnné" (read-only)</para>
        /// <para lang="en">Control "Auxiliary variable category selection" (read-only)</para>
        /// </summary>
        private ControlAuxiliaryVariableCategorySelector CtrAuxiliaryVariableCategorySelector
        {
            get
            {
                return
                    pnlAuxiliaryVariableCategory.Controls.OfType<ControlAuxiliaryVariableCategorySelector>()
                        .FirstOrDefault<ControlAuxiliaryVariableCategorySelector>();
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(btnNext),                            "Další" },
                        { nameof(grpConfigQuery),                     "Volba varianty zpracování výsledku:" },
                        { nameof(grpConfigurationLabelCs),            "Název:" },
                        { nameof(grpConfigurationDescriptionCs),      "Popis:" },
                        { nameof(grpConfigurationLabelEn),            "Název (anglický):" },
                        { nameof(grpConfigurationDescriptionEn),      "Popis (anglický):" },
                        { nameof(rdoQueryVar100),                     "Výpočet z GIS vrstvy" },
                        { nameof(rdoQueryVar200),                     "Součet existujících veličin" },
                        { nameof(rdoQueryVar300),                     "Doplněk do rozlohy výpočetní buňky" },
                        { nameof(rdoQueryVar400),                     "Doplněk do úhrnu existující veličiny" },
                        { nameof(rdoQueryVar500),                     "Odkaz na existující veličinu" }
                    }
                    : languageFile.NationalVersion.Data.TryGetValue(
                        key: nameof(ControlConfigurationIntro),
                        out Dictionary<string, string> dictNational)
                            ? dictNational
                            : [],

                LanguageVersion.International => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(btnNext),                            "Next" },
                        { nameof(grpConfigQuery),                     "Option for processing result:" },
                        { nameof(grpConfigurationLabelCs),            "Name:" },
                        { nameof(grpConfigurationDescriptionCs),      "Description:" },
                        { nameof(grpConfigurationLabelEn),            "Name (English):" },
                        { nameof(grpConfigurationDescriptionEn),      "Description (English):" },
                        { nameof(rdoQueryVar100),                     "Calculation based on a GIS layer" },
                        { nameof(rdoQueryVar200),                     "Sum of existing variables" },
                        { nameof(rdoQueryVar300),                     "Addition to the area of the computational cell" },
                        { nameof(rdoQueryVar400),                     "Addition to the total of the existing variable" },
                        { nameof(rdoQueryVar500),                     "A link to the existing variable" }
                    }
                    : languageFile.InternationalVersion.Data.TryGetValue(
                        key: nameof(ControlConfigurationIntro),
                        out Dictionary<string, string> dictInternational)
                            ? dictInternational
                            : [],

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku</para>
        /// <para lang="en">Initialization of the control</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        private void Initialize(Control controlOwner)
        {
            BorderStyle = BorderStyle.None;
            ControlOwner = controlOwner;

            pnlAuxiliaryVariableCategory.Controls.Clear();
            ControlAuxiliaryVariableCategorySelector ctrAuxiliaryVariableCategorySelector =
                new(controlOwner: this)
                {
                    Dock = DockStyle.Fill,
                };
            ctrAuxiliaryVariableCategorySelector.CreateControl();
            pnlAuxiliaryVariableCategory.Controls.Add(value: ctrAuxiliaryVariableCategorySelector);
            SetAuxiliaryVariableCategoryAvailability();
            ResizeParamsTableLayoutPanel();

            // Nastavení Tag na objekt ConfigFunction
            rdoQueryVar100.Tag = Database.SAuxiliaryData.CConfigQuery[(int)ConfigQueryEnum.GisLayer];
            rdoQueryVar200.Tag = Database.SAuxiliaryData.CConfigQuery[(int)ConfigQueryEnum.SumOfVariables];
            rdoQueryVar300.Tag = Database.SAuxiliaryData.CConfigQuery[(int)ConfigQueryEnum.AdditionToCellArea];
            rdoQueryVar400.Tag = Database.SAuxiliaryData.CConfigQuery[(int)ConfigQueryEnum.AdditionToTotal];
            rdoQueryVar500.Tag = Database.SAuxiliaryData.CConfigQuery[(int)ConfigQueryEnum.LinkToVariable];
            rdoQueryVar100.Visible = rdoQueryVar100.Tag != null;
            rdoQueryVar200.Visible = rdoQueryVar200.Tag != null;
            rdoQueryVar300.Visible = rdoQueryVar300.Tag != null;
            rdoQueryVar400.Visible = rdoQueryVar400.Tag != null;
            rdoQueryVar500.Visible = rdoQueryVar500.Tag != null;

            InitializeLabels();

            txtConfigurationLabelCs.TextChanged += new EventHandler(
                (sender, e) => { ConfigurationLabelCsChanged(); });

            txtConfigurationLabelEn.TextChanged += new EventHandler(
                (sender, e) => { ConfigurationLabelEnChanged(); });

            txtConfigurationDescriptionCs.TextChanged += new EventHandler(
                (sender, e) => { ConfigurationDescriptionCsChanged(); });

            txtConfigurationDescriptionEn.TextChanged += new EventHandler(
                (sender, e) => { ConfigurationDescriptionEnChanged(); });

            rdoQueryVar100.CheckedChanged += new EventHandler(
                (sender, e) => { ConfigQueryChanged(); });

            rdoQueryVar200.CheckedChanged += new EventHandler(
                (sender, e) => { ConfigQueryChanged(); });

            rdoQueryVar300.CheckedChanged += new EventHandler(
                (sender, e) => { ConfigQueryChanged(); });

            rdoQueryVar400.CheckedChanged += new EventHandler(
                (sender, e) => { ConfigQueryChanged(); });

            rdoQueryVar500.CheckedChanged += new EventHandler(
                (sender, e) => { ConfigQueryChanged(); });

            ctrAuxiliaryVariableCategorySelector.AuxiliaryVariableCategoryChanged += new EventHandler(
                (sender, e) => { AuxiliaryVariableCategoryChanged(); });

            btnNext.Click += new EventHandler(
                (sender, e) => { NextClick?.Invoke(sender: sender, e: e); });
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků ovládacího prvku</para>
        /// <para lang="en">Initialization of control labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
               languageVersion: LanguageVersion,
               languageFile: LanguageFile);

            btnNext.Text =
                labels.TryGetValue(key: nameof(btnNext),
                out string btnNextText)
                    ? btnNextText
                    : String.Empty;

            grpConfigurationLabelCs.Text =
                labels.TryGetValue(key: nameof(grpConfigurationLabelCs),
                out string grpConfigurationLabelCsText)
                    ? grpConfigurationLabelCsText
                    : String.Empty;

            grpConfigurationLabelEn.Text =
                labels.TryGetValue(key: nameof(grpConfigurationLabelEn),
                out string grpConfigurationLabelEnText)
                    ? grpConfigurationLabelEnText
                    : String.Empty;

            grpConfigurationDescriptionCs.Text =
                labels.TryGetValue(key: nameof(grpConfigurationDescriptionCs),
                out string grpConfigurationDescriptionCsText)
                    ? grpConfigurationDescriptionCsText
                    : String.Empty;

            grpConfigurationDescriptionEn.Text =
                labels.TryGetValue(key: nameof(grpConfigurationDescriptionEn),
                out string grpConfigurationDescriptionEnText)
                    ? grpConfigurationDescriptionEnText
                    : String.Empty;

            grpConfigQuery.Text =
                labels.TryGetValue(key: nameof(grpConfigQuery),
                out string grpConfigQueryText)
                    ? grpConfigQueryText
                    : String.Empty;

            rdoQueryVar100.Text =
                (rdoQueryVar100.Tag != null)
                    ? (LanguageVersion == LanguageVersion.National)
                        ? ((ConfigQuery)rdoQueryVar100.Tag).LabelCs
                        : ((ConfigQuery)rdoQueryVar100.Tag).LabelEn
                    : labels.TryGetValue(key: nameof(rdoQueryVar100),
                        out string rdoQueryVar100Text)
                            ? rdoQueryVar100Text
                            : String.Empty;

            rdoQueryVar200.Text =
               (rdoQueryVar200.Tag != null)
                   ? (LanguageVersion == LanguageVersion.National)
                       ? ((ConfigQuery)rdoQueryVar200.Tag).LabelCs
                       : ((ConfigQuery)rdoQueryVar200.Tag).LabelEn
                   : labels.TryGetValue(key: nameof(rdoQueryVar200),
                        out string rdoQueryVar200Text)
                           ? rdoQueryVar200Text
                           : String.Empty;

            rdoQueryVar300.Text =
               (rdoQueryVar300.Tag != null)
                   ? (LanguageVersion == LanguageVersion.National)
                       ? ((ConfigQuery)rdoQueryVar300.Tag).LabelCs
                       : ((ConfigQuery)rdoQueryVar300.Tag).LabelEn
                   : labels.TryGetValue(key: nameof(rdoQueryVar300),
                        out string rdoQueryVar300Text)
                           ? rdoQueryVar300Text
                           : String.Empty;

            rdoQueryVar400.Text =
               (rdoQueryVar400.Tag != null)
                   ? (LanguageVersion == LanguageVersion.National)
                       ? ((ConfigQuery)rdoQueryVar400.Tag).LabelCs
                       : ((ConfigQuery)rdoQueryVar400.Tag).LabelEn
                   : labels.TryGetValue(key: nameof(rdoQueryVar400),
                        out string rdoQueryVar400Text)
                           ? rdoQueryVar400Text
                           : String.Empty;

            rdoQueryVar500.Text =
               (rdoQueryVar500.Tag != null)
                   ? (LanguageVersion == LanguageVersion.National)
                       ? ((ConfigQuery)rdoQueryVar500.Tag).LabelCs
                       : ((ConfigQuery)rdoQueryVar500.Tag).LabelEn
                   : labels.TryGetValue(key: nameof(rdoQueryVar500),
                        out string rdoQueryVar500Text)
                           ? rdoQueryVar500Text
                           : String.Empty;

            CtrAuxiliaryVariableCategorySelector?.InitializeLabels();
        }

        /// <summary>
        /// <para lang="cs">Změna velikosti tabulky parametrů</para>
        /// <para lang="en">Resize params table layout panel</para>
        /// </summary>
        private void ResizeParamsTableLayoutPanel()
        {
            tlpParams.RowStyles[grpConfigurationLabelCsIndex].Height =
                grpConfigurationLabelCs.Visible
                    ? grpConfigurationLabelCsHeight
                    : 0;

            tlpParams.RowStyles[grpConfigurationLabelEnIndex].Height =
                grpConfigurationLabelEn.Visible
                    ? grpConfigurationLabelEnHeight
                    : 0;

            tlpParams.RowStyles[grpConfigurationDescriptionCsIndex].Height =
                grpConfigurationDescriptionCs.Visible
                    ? grpConfigurationDescriptionCsHeight
                    : 0;

            tlpParams.RowStyles[grpConfigurationDescriptionEnIndex].Height =
                grpConfigurationDescriptionEn.Visible
                    ? grpConfigurationDescriptionEnHeight
                    : 0;

            tlpParams.RowStyles[grpConfigQueryIndex].Height =
                grpConfigQuery.Visible
                    ? grpConfigQueryHeight
                    : 0;

            tlpParams.RowStyles[pnlAuxiliaryVariableCategoryIndex].Height =
                pnlAuxiliaryVariableCategory.Visible
                    ? pnlAuxiliaryVariableCategoryHeight
                    : 0;

            tlpParams.Height =
                    (int)tlpParams.RowStyles[grpConfigurationLabelCsIndex].Height +
                    (int)tlpParams.RowStyles[grpConfigurationLabelEnIndex].Height +
                    (int)tlpParams.RowStyles[grpConfigurationDescriptionCsIndex].Height +
                    (int)tlpParams.RowStyles[grpConfigurationDescriptionEnIndex].Height +
                    (int)tlpParams.RowStyles[grpConfigQueryIndex].Height +
                    (int)tlpParams.RowStyles[pnlAuxiliaryVariableCategoryIndex].Height;
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent()
        {
            if (Configuration == null)
            {
                grpConfigurationLabelCs.Visible = false;
                grpConfigurationLabelEn.Visible = false;
                grpConfigurationDescriptionCs.Visible = false;
                grpConfigurationDescriptionEn.Visible = false;
                grpConfigQuery.Visible = false;
                pnlAuxiliaryVariableCategory.Visible = false;

                ResizeParamsTableLayoutPanel();

                EnableNext();

                return;
            }

            grpConfigurationLabelCs.Visible = true;
            txtConfigurationLabelCs.Text = Configuration.LabelCs ?? String.Empty;

            grpConfigurationLabelEn.Visible = true;
            txtConfigurationLabelEn.Text = Configuration.LabelEn ?? String.Empty;

            grpConfigurationDescriptionCs.Visible = true;
            txtConfigurationDescriptionCs.Text = Configuration.DescriptionCs ?? String.Empty;

            grpConfigurationDescriptionEn.Visible = true;
            txtConfigurationDescriptionEn.Text = Configuration.DescriptionEn ?? String.Empty;

            // Typ dotazu je možné nastavovat pouze u nových záznamů konfigurací
            // a později již nelze změnit
            grpConfigQuery.Visible = true;
            grpConfigQuery.Enabled = NewEntry;
            rdoQueryVar100.Checked = (rdoQueryVar100.Tag != null) &&
                Configuration.ConfigQueryId == ((ConfigQuery)rdoQueryVar100.Tag).Id;
            rdoQueryVar200.Checked = (rdoQueryVar200.Tag != null) &&
                Configuration.ConfigQueryId == ((ConfigQuery)rdoQueryVar200.Tag).Id;
            rdoQueryVar300.Checked = (rdoQueryVar300.Tag != null) &&
                Configuration.ConfigQueryId == ((ConfigQuery)rdoQueryVar300.Tag).Id;
            rdoQueryVar400.Checked = (rdoQueryVar400.Tag != null) &&
                Configuration.ConfigQueryId == ((ConfigQuery)rdoQueryVar400.Tag).Id;
            rdoQueryVar500.Checked = (rdoQueryVar500.Tag != null) &&
                Configuration.ConfigQueryId == ((ConfigQuery)rdoQueryVar500.Tag).Id;

            SetAuxiliaryVariableCategoryAvailability();
            if (pnlAuxiliaryVariableCategory.Visible)
            {
                CtrAuxiliaryVariableCategorySelector.AuxiliaryVariableCategory =
                    (Configuration?.AuxiliaryVariableCategoryId != null)
                        ? Database.SNfiEsta.CAuxiliaryVariableCategory.Items
                            .Where(a => a.Id == Configuration?.AuxiliaryVariableCategoryId)
                            .FirstOrDefault<AuxiliaryVariableCategory>()
                        : null;
            }

            ResizeParamsTableLayoutPanel();

            EnableNext();
        }

        /// <summary>
        /// <para lang="cs">Změna českého názvu konfigurace</para>
        /// <para lang="en">Configuration Czech name change</para>
        /// </summary>
        private void ConfigurationLabelCsChanged()
        {
            if (Configuration == null)
            {
                return;
            }

            Configuration.LabelCs =
                !String.IsNullOrEmpty(value: txtConfigurationLabelCs.Text.Trim())
                ? txtConfigurationLabelCs.Text.Trim()
                : null;

            EnableNext();
        }

        /// <summary>
        /// <para lang="cs">Změna anglického názvu konfigurace</para>
        /// <para lang="en">Configuration English name change</para>
        /// </summary>
        private void ConfigurationLabelEnChanged()
        {
            if (Configuration == null)
            {
                return;
            }

            Configuration.LabelEn =
                !String.IsNullOrEmpty(value: txtConfigurationLabelEn.Text.Trim())
                ? txtConfigurationLabelEn.Text.Trim()
                : null;

            EnableNext();
        }

        /// <summary>
        /// <para lang="cs">Změna českého popisu konfigurace</para>
        /// <para lang="en">Configuration Czech description change</para>
        /// </summary>
        private void ConfigurationDescriptionCsChanged()
        {
            if (Configuration == null)
            {
                return;
            }

            Configuration.DescriptionCs =
                !String.IsNullOrEmpty(value: txtConfigurationDescriptionCs.Text.Trim())
                ? txtConfigurationDescriptionCs.Text.Trim()
                : null;

            EnableNext();
        }

        /// <summary>
        /// <para lang="cs">Změna anglického popisu konfigurace</para>
        /// <para lang="en">Configuration English description change</para>
        /// </summary>
        private void ConfigurationDescriptionEnChanged()
        {
            if (Configuration == null)
            {
                return;
            }

            Configuration.DescriptionEn =
                !String.IsNullOrEmpty(value: txtConfigurationDescriptionEn.Text.Trim())
                ? txtConfigurationDescriptionEn.Text.Trim()
                : null;

            EnableNext();
        }

        /// <summary>
        /// <para lang="cs">Změna výběru varianty zpracování výsledku</para>
        /// <para lang="en">Option for processing result change</para>
        /// </summary>
        private void ConfigQueryChanged()
        {
            if (Configuration == null)
            {
                return;
            }

            ConfigQuery selectedConfigQuery =
                grpConfigQuery.Controls.OfType<RadioButton>()
                    .Where(a => a.Tag != null)
                    .Where(a => a.Tag is ConfigQuery)
                    .Where(a => a.Checked)
                    .Select(a => (ConfigQuery)a.Tag)
                    .FirstOrDefault<ConfigQuery>();

            if (selectedConfigQuery == null)
            {
                return;
            }

            if (Configuration.ConfigQueryId != selectedConfigQuery.Id)
            {
                Configuration.ConfigQueryId = selectedConfigQuery.Id;
                Configuration.Reset();
            }

            EnableNext();
        }

        /// <summary>
        /// <para lang="cs">Změna výběru kategorie pomocné proměnné</para>
        /// <para lang="en">Auxiliary variable category selection change</para>
        /// </summary>
        private void AuxiliaryVariableCategoryChanged()
        {
            if (pnlAuxiliaryVariableCategory.Visible)
            {
                if (Configuration == null)
                {
                    return;
                }

                Configuration.AuxiliaryVariableCategoryId =
                    CtrAuxiliaryVariableCategorySelector?.AuxiliaryVariableCategory?.Id;

                EnableNext();
            }
        }

        /// <summary>
        /// <para lang="cs">Nastaví dostupnost kategorie pomocné proměnné</para>
        /// <para lang="en">Sets auxiliary variable category availability</para>
        /// </summary>
        private void SetAuxiliaryVariableCategoryAvailability()
        {
            if (CtrAuxiliaryVariableCategorySelector == null)
            {
                pnlAuxiliaryVariableCategory.Visible = false;
                return;
            }

            if (Configuration == null)
            {
                pnlAuxiliaryVariableCategory.Visible = false;
                return;
            }

            if (Database.Postgres.Catalog.Schemas[NfiEstaSchema.Name] == null)
            {
                // Neexistuje schéma nfiesta
                pnlAuxiliaryVariableCategory.Visible = false;
                return;
            }

            if (
                (Database.Postgres.Catalog.Schemas[NfiEstaSchema.Name].Tables[AuxiliaryVariableCategoryList.Name] == null) &&
                (Database.Postgres.Catalog.Schemas[NfiEstaSchema.Name].ForeignTables[AuxiliaryVariableCategoryList.Name] == null))
            {
                // Neexistuje tabulka ani cizí tabulka c_auxiliary_variable_category s kategoriemi pomocné proměnné
                pnlAuxiliaryVariableCategory.Visible = false;
                return;
            }

            if (Database.SNfiEsta.CAuxiliaryVariable.Data.Rows.Count == 0)
            {
                // Tabulka c_auxiliary_variable je prázdná
                pnlAuxiliaryVariableCategory.Visible = false;
                return;
            }

            AuxiliaryVariable auxiliaryVariable =
                Database.SNfiEsta.CAuxiliaryVariable.Items
                .Where(a => a.Id == (Configuration?.ConfigCollection?.AuxiliaryVariableId ?? 0))
                .FirstOrDefault<AuxiliaryVariable>();

            if (auxiliaryVariable == null)
            {
                // Neexistuje pomocná proměná, pro kterou se má zvolit kategorie
                pnlAuxiliaryVariableCategory.Visible = false;
                return;
            }

            pnlAuxiliaryVariableCategory.Visible = true;

            return;
        }

        /// <summary>
        /// <para lang="cs">Povolí postup na další formulář</para>
        /// <para lang="en">Allows progression to the next form</para>
        /// </summary>
        private void EnableNext()
        {
            if (Configuration == null)
            {
                btnNext.Enabled = false;
                return;
            }

            btnNext.Enabled =
                !String.IsNullOrEmpty(Configuration.LabelCs) &&
                !String.IsNullOrEmpty(Configuration.LabelEn) &&
                !String.IsNullOrEmpty(Configuration.DescriptionCs) &&
                !String.IsNullOrEmpty(Configuration.DescriptionEn) &&
                (Configuration.ConfigQueryValue != ConfigQueryEnum.Unknown);
        }

        #endregion Methods

    }

}