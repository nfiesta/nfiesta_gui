﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.AuxiliaryData;
using ZaJi.PostgreSQL;

namespace ZaJi.ModuleAuxiliaryData
{

    /// <summary>
    /// <para lang="cs">Ovládací prvek "Sestavení výběrové podmínky pro bodovou vrstvu"</para>
    /// <para lang="en">Control "Constructing a selection condition for a point layer"</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    internal partial class ControlPointCondition
         : UserControl, INfiEstaControl, IAuxiliaryDataControl
    {

        #region Constants

        /// <summary>
        /// <para lang="cs">Výška řádku</para>
        /// <para lang="en">Row height</para>
        /// </summary>
        private const int rowHeight = 30;

        #endregion Constants


        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        private Control controlOwner;

        #endregion Private Fields


        #region Events

        /// <summary>
        /// <para lang="cs">Událost kliknutí na tlačítko "Předchozí"</para>
        /// <para lang="en">Click event on the "Previous" button</para>
        /// </summary>
        public event EventHandler PreviousClick;

        /// <summary>
        /// <para lang="cs">Událost kliknutí na tlačítko "Další"</para>
        /// <para lang="en">Click event on the "Next" button</para>
        /// </summary>
        public event EventHandler NextClick;

        #endregion Events


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor ovládacího prvku</para>
        /// <para lang="en">Control constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        public ControlPointCondition(Control controlOwner)
        {
            InitializeComponent();
            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                IAuxiliaryDataControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                if (controlOwner is not FormConfigCollectionGuide)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(FormConfigCollectionGuide)}.",
                        paramName: nameof(ControlOwner));
                }

                return controlOwner;
            }
            set
            {
                IAuxiliaryDataControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                if (value is not FormConfigCollectionGuide)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(FormConfigCollectionGuide)}.",
                        paramName: nameof(ControlOwner));
                }

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables (read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((IAuxiliaryDataControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro pomocná data (read-only)</para>
        /// <para lang="en">Module for auxiliary data setting (read-only)</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((IAuxiliaryDataControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">Skupina konfigurací</para>
        /// <para lang="en">Configuration collection</para>
        /// </summary>
        public ConfigCollection ConfigCollection
        {
            get
            {
                return ((FormConfigCollectionGuide)ControlOwner).ConfigCollection;
            }
        }

        /// <summary>
        /// <para lang="cs">Seznam vybraných databázových sloupců pro sestavení výběrové podmínky (read-only)</para>
        /// <para lang="en">List of selected database columns for construction of selection condition (read-only)</para>
        /// </summary>
        public List<WideColumn> ConditionColumns
        {
            get
            {
                return ((FormConfigCollectionGuide)ControlOwner).ConditionColumns;
            }
        }

        /// <summary>
        /// <para lang="cs">Seznam zobrazených rozbalovacích seznamů (read-only)</para>
        /// <para lang="en">List of displayed ComboBoxes (read-only)</para>
        /// </summary>
        private List<ComboBox> ComboBoxes
        {
            get
            {
                return
                    pnlComboBoxes.Controls.OfType<Panel>()
                        .SelectMany(a => a.Controls.OfType<ComboBox>())
                        .Where(a => a.Tag != null)
                        .Where(a => a.Tag is WideColumn)
                        .ToList<ComboBox>();
            }
        }

        /// <summary>
        /// <para lang="cs">Seznam zobrazených zaškrtavacích polí (read-only)</para>
        /// <para lang="en">List of displayed Checkboxes (read-only)</para>
        /// </summary>
        private List<CheckBox> CheckBoxes
        {
            get
            {
                return
                    pnlCheckBoxes.Controls.OfType<Panel>()
                        .SelectMany(a => a.Controls.OfType<CheckBox>())
                        .Where(a => a.Tag != null)
                        .Where(a => a.Tag is WideColumn)
                        .ToList<CheckBox>();
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(btnPrevious),            "Předchozí" },
                        { nameof(btnNext),                "Další" },
                        { nameof(btnDeleteCondition),     "Smazat podmínku" },
                        { nameof(btnAddCondition),        "Přidat do podmínky" },
                        { nameof(grpColumns),             "Kategorie" },
                        { nameof(grpCondition),           "Výběrová podmínka" }
                    }
                    : languageFile.NationalVersion.Data.TryGetValue(
                        key: nameof(ControlPointCondition),
                        out Dictionary<string, string> dictNational)
                            ? dictNational
                            : [],

                LanguageVersion.International => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(btnPrevious),            "Previous" },
                        { nameof(btnNext),                "Next" },
                        { nameof(btnDeleteCondition),     "Delete condition" },
                        { nameof(btnAddCondition),        "Add into condition" },
                        { nameof(grpColumns),             "Category" },
                        { nameof(grpCondition),           "Select condition" }
                    }
                    : languageFile.InternationalVersion.Data.TryGetValue(
                        key: nameof(ControlPointCondition),
                        out Dictionary<string, string> dictInternational)
                            ? dictInternational
                            : [],

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku</para>
        /// <para lang="en">Initialization of the control</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        private void Initialize(Control controlOwner)
        {
            BorderStyle = BorderStyle.None;
            ControlOwner = controlOwner;

            InitializeLabels();
            EnableNext();

            btnPrevious.Click += new EventHandler(
                (sender, e) =>
                {
                    PreviousClick?.Invoke(sender: sender, e: e);
                });

            btnNext.Click += new EventHandler(
               (sender, e) =>
               {
                   NextClick?.Invoke(sender: sender, e: e);
               });

            btnAddCondition.Click += new EventHandler(
                (sender, e) => { AddCondition(); });

            btnDeleteCondition.Click += new EventHandler(
                (sender, e) => { DeleteCondition(); });
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků ovládacího prvku</para>
        /// <para lang="en">Initialization of control labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            btnPrevious.Text =
                labels.TryGetValue(key: nameof(btnPrevious),
                out string btnPreviousText)
                    ? btnPreviousText
                    : String.Empty;

            btnNext.Text =
                labels.TryGetValue(key: nameof(btnNext),
                out string btnNextText)
                    ? btnNextText
                    : String.Empty;

            btnAddCondition.Text =
                labels.TryGetValue(key: nameof(btnAddCondition),
                out string btnAddConditionText)
                    ? btnAddConditionText
                    : String.Empty;

            btnDeleteCondition.Text =
                labels.TryGetValue(key: nameof(btnDeleteCondition),
                out string btnDeleteConditionText)
                    ? btnDeleteConditionText
                    : String.Empty;

            grpColumns.Text =
                labels.TryGetValue(key: nameof(grpColumns),
                out string grpColumnsText)
                    ? grpColumnsText
                    : String.Empty;

            grpCondition.Text =
                labels.TryGetValue(key: nameof(grpCondition),
                out string grpConditionText)
                    ? grpConditionText
                    : String.Empty;
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent()
        {
            pnlLabels.Controls.Clear();
            pnlComboBoxes.Controls.Clear();
            pnlCheckBoxes.Controls.Clear();

            foreach (WideColumn wideColumn in ConditionColumns)
            {
                // Nahraje data číselníku pro vybraný sloupec
                wideColumn.LoadLookupTable();

                AddControls(wideColumn: wideColumn);
            }

            txtCondition.Text = ConfigCollection?.Condition ?? String.Empty;
            EnableNext();
        }

        /// <summary>
        /// <para lang="cs">Přidá ovládací prvky pro databázový sloupec do formuláře</para>
        /// <para lang="en">Adds controls for the database column to the form</para>
        /// </summary>
        /// <param name="wideColumn">
        /// <para lang="cs">Databázový sloupec s přiřazeným cizím klíčem na číselník</para>
        /// <para lang="en">Database column with assigned foreign key to lookup table</para>
        /// </param>
        private void AddControls(WideColumn wideColumn)
        {
            Panel pnlLabel = new()
            {
                Dock = DockStyle.Top,
                Height = rowHeight,
                Margin = new(all: 0),
            };
            pnlLabel.CreateControl();
            pnlLabels.Controls.Add(value: pnlLabel);

            Label label = new()
            {
                AutoSize = false,
                Dock = DockStyle.Fill,
                Font = Setting.LabelFont,
                ForeColor = Setting.LabelForeColor,
                Height = rowHeight,
                Tag = wideColumn,
                Text = wideColumn.Column.Name,
                TextAlign = ContentAlignment.MiddleLeft
            };
            label.CreateControl();
            pnlLabel.Padding = new Padding(
                left: 10,
                top: (((pnlLabel.Height - label.Height) / 2) > 0)
                        ? ((pnlLabel.Height - label.Height) / 2)
                        : 0,
                right: 0,
                bottom: 0);
            pnlLabel.Controls.Add(value: label);

            Panel pnlComboBox = new()
            {
                Dock = DockStyle.Top,
                Height = rowHeight,
                Margin = new(all: 0)
            };
            pnlComboBox.CreateControl();
            pnlComboBoxes.Controls.Add(value: pnlComboBox);

            ComboBox comboBox = new()
            {
                DataSource = wideColumn.LookupTable,
                DisplayMember = WideColumn.ColLabelName,
                Dock = DockStyle.Fill,
                DropDownStyle = ComboBoxStyle.DropDownList,
                Height = rowHeight,
                Font = Setting.ComboBoxFont,
                ForeColor = Setting.ComboBoxForeColor,
                ValueMember = WideColumn.ColIdName,
                Tag = wideColumn
            };
            comboBox.CreateControl();
            pnlComboBox.Padding = new Padding(
               left: 0,
               top: (((pnlComboBox.Height - comboBox.Height) / 2) > 0)
                       ? ((pnlComboBox.Height - comboBox.Height) / 2)
                       : 0,
               right: 0,
               bottom: 0);
            pnlComboBox.Controls.Add(value: comboBox);

            Panel pnlCheckBox = new()
            {
                Dock = DockStyle.Top,
                Height = rowHeight,
                Margin = new(all: 0)
            };
            pnlCheckBox.CreateControl();
            pnlCheckBoxes.Controls.Add(value: pnlCheckBox);

            CheckBox checkBox = new()
            {
                AutoSize = false,
                Checked = false,
                CheckAlign = ContentAlignment.MiddleCenter,
                Dock = DockStyle.Fill,
                Font = Setting.CheckBoxFont,
                ForeColor = Setting.CheckBoxForeColor,
                Height = rowHeight,
                Tag = wideColumn,
                Text = String.Empty
            };
            checkBox.CreateControl();
            pnlCheckBox.Padding = new Padding(
               left: 0,
               top: (((pnlCheckBox.Height - checkBox.Height) / 2) > 0)
                       ? ((pnlCheckBox.Height - checkBox.Height) / 2)
                       : 0,
               right: 0,
               bottom: 0);
            pnlCheckBox.Controls.Add(value: checkBox);
        }

        /// <summary>
        /// <para lang="cs">Smaže výběrovou podmínku</para>
        /// <para lang="en">Delete condition</para>
        /// </summary>
        private void DeleteCondition()
        {
            ConfigCollection.Condition = null;
            txtCondition.Text = ConfigCollection.Condition ?? String.Empty;
            EnableNext();
        }

        /// <summary>
        /// <para lang="cs">Přidá výběrovou podmínku</para>
        /// <para lang="en">Delete condition</para>
        /// </summary>
        private void AddCondition()
        {
            Condition condition = new();

            foreach (ComboBox comboBox in ComboBoxes)
            {
                if (comboBox.SelectedValue == null)
                {
                    continue;
                }

                CheckBox checkBox = CheckBoxes
                    .Where(a => ((WideColumn)a.Tag).Column.Name == ((WideColumn)comboBox.Tag).Column.Name)
                    .FirstOrDefault<CheckBox>();

                condition.AddPart(
                    part: new InnerCondition(
                        columnName: ((WideColumn)comboBox.Tag).Column.Name,
                        columnValue: comboBox.SelectedValue.ToString(),
                        isEqual: (checkBox == null) || !checkBox.Checked));
            }

            ConfigCollection.Condition =
                !String.IsNullOrEmpty(condition?.Text)
                ? condition.Text
                : null;
            txtCondition.Text = ConfigCollection.Condition ?? String.Empty;
            EnableNext();
        }

        /// <summary>
        /// <para lang="cs">Povolí postup na další formulář</para>
        /// <para lang="en">Allows progression to the next form</para>
        /// </summary>
        private void EnableNext()
        {
            if (ConfigCollection == null)
            {
                btnNext.Enabled = false;
                return;
            }

            btnNext.Enabled =
                   !String.IsNullOrEmpty(ConfigCollection.LabelCs) &&
                   !String.IsNullOrEmpty(ConfigCollection.LabelEn) &&
                   !String.IsNullOrEmpty(ConfigCollection.DescriptionCs) &&
                   !String.IsNullOrEmpty(ConfigCollection.DescriptionEn) &&
                   (ConfigCollection.ConfigFunctionValue == ConfigFunctionEnum.PointLayer) &&
                   !String.IsNullOrEmpty(value: ConfigCollection?.CatalogName) &&
                   !String.IsNullOrEmpty(value: ConfigCollection?.SchemaName) &&
                   !String.IsNullOrEmpty(value: ConfigCollection?.TableName) &&
                   !String.IsNullOrEmpty(value: ConfigCollection?.ColumnName) &&
                   !String.IsNullOrEmpty(value: ConfigCollection?.Tag) &&
                   !String.IsNullOrEmpty(value: ConfigCollection?.Condition);
        }

        #endregion Methods

    }
}
