﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.AuxiliaryData;
using ZaJi.NfiEstaPg.Core;
using ZaJi.PostgreSQL;

namespace ZaJi.ModuleAuxiliaryData
{

    /// <summary>
    /// <para lang="cs">Ovládací prvek "Nová skupina konfigurací"</para>
    /// <para lang="en">Control "New configuration collection"</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    internal partial class ControlConfigCollectionIntro
        : UserControl, INfiEstaControl, IAuxiliaryDataControl
    {

        #region Constants

        /// <summary>
        /// <para lang="cs">Index skupiny "Název skupiny konfigurací"</para>
        /// <para lang="en">Group "Configuration collection name" index</para>
        /// </summary>
        private const int grpConfigCollectionLabelCsIndex = 0;

        /// <summary>
        /// <para lang="cs">Výška skupiny "Název skupiny konfigurací"</para>
        /// <para lang="en">Group "Configuration collection name" height</para>
        /// </summary>
        private const int grpConfigCollectionLabelCsHeight = 60;


        /// <summary>
        /// <para lang="cs">Index skupiny "Název skupiny konfigurací v angličtině"</para>
        /// <para lang="en">Group "Configuration collection name in English" index</para>
        /// </summary>
        private const int grpConfigCollectionLabelEnIndex = 1;

        /// <summary>
        /// <para lang="cs">Výška skupiny "Název skupiny konfigurací v angličtině"</para>
        /// <para lang="en">Group "Configuration collection name in English" height</para>
        /// </summary>
        private const int grpConfigCollectionLabelEnHeight = 60;


        /// <summary>
        /// <para lang="cs">Index skupiny "Popis skupiny konfigurací"</para>
        /// <para lang="en">Group "Configuraion collection description" index</para>
        /// </summary>
        private const int grpConfigCollectionDescriptionCsIndex = 2;

        /// <summary>
        /// <para lang="cs">Výška skupiny "Popis skupiny konfigurací"</para>
        /// <para lang="en">Group "Configuraion collection description" height</para>
        /// </summary>
        private const int grpConfigCollectionDescriptionCsHeight = 60;


        /// <summary>
        /// <para lang="cs">Index skupiny "Popis skupiny konfigurací v angličtině"</para>
        /// <para lang="en">Group "Configuraion collection description in English" index</para>
        /// </summary>
        private const int grpConfigCollectionDescriptionEnIndex = 3;

        /// <summary>
        /// <para lang="cs">Výška skupiny "Popis skupiny konfigurací v angličtině"</para>
        /// <para lang="en">Group "Configuraion collection description in English" height</para>
        /// </summary>
        private const int grpConfigCollectionDescriptionEnHeight = 60;


        /// <summary>
        /// <para lang="cs">Index skupiny "Konfigurační funkce"</para>
        /// <para lang="en">Group "Configuration function" index</para>
        /// </summary>
        private const int grpConfigFunctionIndex = 4;

        /// <summary>
        /// <para lang="cs">Výška skupiny "Konfigurační funkce"</para>
        /// <para lang="en">Group "Configuration function" height</para>
        /// </summary>
        private const int grpConfigFunctionHeight = 150;


        /// <summary>
        /// <para lang="cs">Index skupiny "Agregovaná skupina"</para>
        /// <para lang="en">Group "Aggregated group" index</para>
        /// </summary>
        private const int grpAggregatedIndex = 5;

        /// <summary>
        /// <para lang="cs">Výška skupiny "Agregovaná skupina"</para>
        /// <para lang="en">Group "Aggregated group" height</para>
        /// </summary>
        private const int grpAggregatedHeight = 60;


        /// <summary>
        /// <para lang="cs">Index skupiny "Pomocná proměnná"</para>
        /// <para lang="en">Group "Auxiliary variable" index</para>
        /// </summary>
        private const int pnlAuxiliaryVariableIndex = 6;

        /// <summary>
        /// <para lang="cs">Výška skupiny "Pomocná proměnná"</para>
        /// <para lang="en">Group "Auxiliary variable" height</para>
        /// </summary>
        private const int pnlAuxiliaryVariableHeight = 120;

        #endregion Constants


        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        private Control controlOwner;

        #endregion Private Fields


        #region Events

        /// <summary>
        /// <para lang="cs">Událost kliknutí na tlačítko "Další"</para>
        /// <para lang="en">Click event on the "Next" button</para>
        /// </summary>
        public event EventHandler NextClick;

        #endregion Events


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor ovládacího prvku</para>
        /// <para lang="en">Control constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        public ControlConfigCollectionIntro(Control controlOwner)
        {
            InitializeComponent();
            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                IAuxiliaryDataControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                if (controlOwner is not FormConfigCollectionGuide)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(FormConfigCollectionGuide)}.",
                        paramName: nameof(ControlOwner));
                }

                return controlOwner;
            }
            set
            {
                IAuxiliaryDataControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                if (value is not FormConfigCollectionGuide)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(FormConfigCollectionGuide)}.",
                        paramName: nameof(ControlOwner));
                }

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables (read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((IAuxiliaryDataControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro pomocná data (read-only)</para>
        /// <para lang="en">Module for auxiliary data setting (read-only)</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((IAuxiliaryDataControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">Skupina konfigurací</para>
        /// <para lang="en">Configuration collection</para>
        /// </summary>
        public ConfigCollection ConfigCollection
        {
            get
            {
                return ((FormConfigCollectionGuide)ControlOwner).ConfigCollection;
            }
        }

        /// <summary>
        /// <para lang="cs">
        /// Provede se zápis nové (newEntry = true)
        /// nebo uprava existující skupiny konfigurací (newEntry = false) do databáze (read-only)
        /// </para>
        /// A new entry is inserted (newEntry = true)
        /// or updated an existing configuration collection (newEntry = false) into the database (read-only)
        /// </summary>
        private bool NewEntry
        {
            get
            {
                return ((FormConfigCollectionGuide)ControlOwner).NewEntry;
            }
        }

        /// <summary>
        /// <para lang="cs">Ovládací prvek "Výběr pomocné proměnné" (read-only)</para>
        /// <para lang="en">Control "Auxiliary variable selection" (read-only)</para>
        /// </summary>
        private ControlAuxiliaryVariableSelector CtrAuxiliaryVariableSelector
        {
            get
            {
                return
                    pnlAuxiliaryVariable.Controls.OfType<ControlAuxiliaryVariableSelector>()
                        .FirstOrDefault<ControlAuxiliaryVariableSelector>();
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(btnNext),                            "Další" },
                        { nameof(grpAggregated),                      String.Empty },
                        { nameof(grpConfigCollectionLabelCs),         "Název:" },
                        { nameof(grpConfigCollectionDescriptionCs),   "Popis:" },
                        { nameof(grpConfigCollectionLabelEn),         "Název (anglický):" },
                        { nameof(grpConfigCollectionDescriptionEn),   "Popis (anglický):" },
                        { nameof(grpConfigFunction),                  "Volba varianty zpracování výsledku:" },
                        { nameof(chkAggregated),                      "Agregovaná skupina" },
                        { nameof(rdoFunctionVar100),                  "Úhrn kategorie pomocné proměnné z vektorové vrstvy." },
                        { nameof(rdoFunctionVar200),                  "Úhrn kategorie pomocné proměnné z rastrové vrstvy." },
                        { nameof(rdoFunctionVar300),                  "Úhrn kategorie pomocné proměnné z interakce vektorové a rastrové vrstvy." },
                        { nameof(rdoFunctionVar400),                  "Úhrn kategorie pomocné proměnné z interakce rasterové a rastrové vrstvy." },
                        { nameof(rdoFunctionVar500),                  "Konfigurace bodové vrstvy." },
                        { nameof(rdoFunctionVar600),                  "Konfigurace kombinace bodové vrstvy a úhrnu"}
                    }
                    : languageFile.NationalVersion.Data.TryGetValue(
                        key: nameof(ControlConfigCollectionIntro),
                        out Dictionary<string, string> dictNational)
                            ? dictNational
                            : [],

                LanguageVersion.International => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(btnNext),                            "Next" },
                        { nameof(grpAggregated),                      String.Empty},
                        { nameof(grpConfigCollectionLabelCs),         "Name:" },
                        { nameof(grpConfigCollectionDescriptionCs),   "Description:" },
                        { nameof(grpConfigCollectionLabelEn),         "Name (English):" },
                        { nameof(grpConfigCollectionDescriptionEn),   "Description (English):" },
                        { nameof(grpConfigFunction),                  "Option for processing result:" },
                        { nameof(chkAggregated),                      "Aggregate group" },
                        { nameof(rdoFunctionVar100),                  "Category total based on a vector layer." },
                        { nameof(rdoFunctionVar200),                  "Category total based on a raster layer." },
                        { nameof(rdoFunctionVar300),                  "Raster total within the vector layer category." },
                        { nameof(rdoFunctionVar400),                  "Total of the product of two rasters." },
                        { nameof(rdoFunctionVar500),                  "Point layer configuration." },
                        { nameof(rdoFunctionVar600),                  "Configuration of the combination of the point layer and the total." }
                    }
                    : languageFile.InternationalVersion.Data.TryGetValue(
                        key: nameof(ControlConfigCollectionIntro),
                        out Dictionary<string, string> dictInternational)
                            ? dictInternational
                            : [],

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku</para>
        /// <para lang="en">Initialization of the control</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        private void Initialize(Control controlOwner)
        {
            BorderStyle = BorderStyle.None;
            ControlOwner = controlOwner;

            pnlAuxiliaryVariable.Controls.Clear();
            ControlAuxiliaryVariableSelector ctrAuxiliaryVariableSelector =
                new(controlOwner: this)
                {
                    Dock = DockStyle.Fill,
                };
            ctrAuxiliaryVariableSelector.CreateControl();
            pnlAuxiliaryVariable.Controls.Add(value: ctrAuxiliaryVariableSelector);
            SetAuxiliaryVariableAvailability();
            ResizeParamsTableLayoutPanel();

            // Nastavení Tag na objekt ConfigFunction
            rdoFunctionVar100.Tag = Database.SAuxiliaryData.CConfigFunction[(int)ConfigFunctionEnum.VectorTotal];
            rdoFunctionVar200.Tag = Database.SAuxiliaryData.CConfigFunction[(int)ConfigFunctionEnum.RasterTotal];
            rdoFunctionVar300.Tag = Database.SAuxiliaryData.CConfigFunction[(int)ConfigFunctionEnum.RasterTotalWithinVector];
            rdoFunctionVar400.Tag = Database.SAuxiliaryData.CConfigFunction[(int)ConfigFunctionEnum.RastersProductTotal];
            rdoFunctionVar500.Tag = Database.SAuxiliaryData.CConfigFunction[(int)ConfigFunctionEnum.PointLayer];
            rdoFunctionVar600.Tag = Database.SAuxiliaryData.CConfigFunction[(int)ConfigFunctionEnum.PointTotalCombination];
            rdoFunctionVar100.Visible = rdoFunctionVar100.Tag != null;
            rdoFunctionVar200.Visible = rdoFunctionVar200.Tag != null;
            rdoFunctionVar300.Visible = rdoFunctionVar300.Tag != null;
            rdoFunctionVar400.Visible = rdoFunctionVar400.Tag != null;
            rdoFunctionVar500.Visible = rdoFunctionVar500.Tag != null;
            rdoFunctionVar600.Visible = rdoFunctionVar600.Tag != null;

            InitializeLabels();

            txtConfigCollectionLabelCs.TextChanged += new EventHandler(
                (sender, e) => { ConfigCollectionLabelCsChanged(); });

            txtConfigCollectionLabelEn.TextChanged += new EventHandler(
                (sender, e) => { ConfigCollectionLabelEnChanged(); });

            txtConfigCollectionDescriptionCs.TextChanged += new EventHandler(
                (sender, e) => { ConfigCollectionDescriptionCsChanged(); });

            txtConfigCollectionDescriptionEn.TextChanged += new EventHandler(
                (sender, e) => { ConfigCollectionDescriptionEnChanged(); });

            rdoFunctionVar100.CheckedChanged += new EventHandler(
                (sender, e) => { ConfigFunctionChanged(); });

            rdoFunctionVar200.CheckedChanged += new EventHandler(
                (sender, e) => { ConfigFunctionChanged(); });

            rdoFunctionVar300.CheckedChanged += new EventHandler(
                (sender, e) => { ConfigFunctionChanged(); });

            rdoFunctionVar400.CheckedChanged += new EventHandler(
                (sender, e) => { ConfigFunctionChanged(); });

            rdoFunctionVar500.CheckedChanged += new EventHandler(
                (sender, e) => { ConfigFunctionChanged(); });

            rdoFunctionVar600.CheckedChanged += new EventHandler(
                (sender, e) => { ConfigFunctionChanged(); });

            chkAggregated.CheckedChanged += new EventHandler(
                (sender, e) => { AggregatedChanged(); });

            ctrAuxiliaryVariableSelector.AuxiliaryVariableChanged += new EventHandler(
                (sender, e) => { AuxiliaryVariableChanged(); });

            btnNext.Click += new EventHandler(
                (sender, e) => { NextClick?.Invoke(sender: sender, e: e); });
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků ovládacího prvku</para>
        /// <para lang="en">Initialization of control labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
               languageVersion: LanguageVersion,
               languageFile: LanguageFile);

            btnNext.Text =
                labels.TryGetValue(key: nameof(btnNext),
                out string btnNextText)
                    ? btnNextText
                    : String.Empty;

            grpConfigCollectionLabelCs.Text =
              labels.TryGetValue(key: nameof(grpConfigCollectionLabelCs),
              out string grpConfigCollectionLabelCsText)
                  ? grpConfigCollectionLabelCsText
                  : String.Empty;

            grpConfigCollectionDescriptionCs.Text =
                labels.TryGetValue(key: nameof(grpConfigCollectionDescriptionCs),
                out string grpConfigCollectionDescriptionCsText)
                    ? grpConfigCollectionDescriptionCsText
                    : String.Empty;

            grpConfigCollectionLabelEn.Text =
                labels.TryGetValue(key: nameof(grpConfigCollectionLabelEn),
                out string grpConfigCollectionLabelEnText)
                    ? grpConfigCollectionLabelEnText
                    : String.Empty;

            grpConfigCollectionDescriptionEn.Text =
                labels.TryGetValue(key: nameof(grpConfigCollectionDescriptionEn),
                out string grpConfigCollectionDescriptionEnText)
                    ? grpConfigCollectionDescriptionEnText
                    : String.Empty;

            grpConfigFunction.Text =
                labels.TryGetValue(key: nameof(grpConfigFunction),
                out string grpConfigFunctionText)
                    ? grpConfigFunctionText
                    : String.Empty;

            grpAggregated.Text =
                labels.TryGetValue(key: nameof(grpAggregated),
                out string grpAggregatedText)
                    ? grpAggregatedText
                    : String.Empty;

            chkAggregated.Text =
                labels.TryGetValue(key: nameof(chkAggregated),
                out string chkAggregatedText)
                    ? chkAggregatedText
                    : String.Empty;

            rdoFunctionVar100.Text =
                (rdoFunctionVar100.Tag != null)
                    ? (LanguageVersion == LanguageVersion.National)
                        ? ((ConfigFunction)rdoFunctionVar100.Tag).LabelCs
                        : ((ConfigFunction)rdoFunctionVar100.Tag).LabelEn
                    : labels.TryGetValue(key: nameof(rdoFunctionVar100),
                    out string rdoFunctionVar100Text)
                        ? rdoFunctionVar100Text
                        : String.Empty;

            rdoFunctionVar200.Text =
               (rdoFunctionVar200.Tag != null)
                   ? (LanguageVersion == LanguageVersion.National)
                       ? ((ConfigFunction)rdoFunctionVar200.Tag).LabelCs
                       : ((ConfigFunction)rdoFunctionVar200.Tag).LabelEn
                   : labels.TryGetValue(key: nameof(rdoFunctionVar200),
                   out string rdoFunctionVar200Text)
                       ? rdoFunctionVar200Text
                       : String.Empty;

            rdoFunctionVar300.Text =
               (rdoFunctionVar300.Tag != null)
                   ? (LanguageVersion == LanguageVersion.National)
                       ? ((ConfigFunction)rdoFunctionVar300.Tag).LabelCs
                       : ((ConfigFunction)rdoFunctionVar300.Tag).LabelEn
                   : labels.TryGetValue(key: nameof(rdoFunctionVar300),
                   out string rdoFunctionVar300Text)
                       ? rdoFunctionVar300Text
                       : String.Empty;

            rdoFunctionVar400.Text =
               (rdoFunctionVar400.Tag != null)
                   ? (LanguageVersion == LanguageVersion.National)
                       ? ((ConfigFunction)rdoFunctionVar400.Tag).LabelCs
                       : ((ConfigFunction)rdoFunctionVar400.Tag).LabelEn
                   : labels.TryGetValue(key: nameof(rdoFunctionVar400),
                   out string rdoFunctionVar400Text)
                       ? rdoFunctionVar400Text
                       : String.Empty;

            rdoFunctionVar500.Text =
               (rdoFunctionVar500.Tag != null)
                   ? (LanguageVersion == LanguageVersion.National)
                       ? ((ConfigFunction)rdoFunctionVar500.Tag).LabelCs
                       : ((ConfigFunction)rdoFunctionVar500.Tag).LabelEn
                   : labels.TryGetValue(key: nameof(rdoFunctionVar500),
                   out string rdoFunctionVar500Text)
                       ? rdoFunctionVar500Text
                       : String.Empty;

            rdoFunctionVar600.Text =
               (rdoFunctionVar600.Tag != null)
                   ? (LanguageVersion == LanguageVersion.National)
                       ? ((ConfigFunction)rdoFunctionVar600.Tag).LabelCs
                       : ((ConfigFunction)rdoFunctionVar600.Tag).LabelEn
                   : labels.TryGetValue(key: nameof(rdoFunctionVar600),
                   out string rdoFunctionVar600Text)
                       ? rdoFunctionVar600Text
                       : String.Empty;

            CtrAuxiliaryVariableSelector?.InitializeLabels();
        }

        /// <summary>
        /// <para lang="cs">Změna velikosti tabulky parametrů</para>
        /// <para lang="en">Resize params table layout panel</para>
        /// </summary>
        private void ResizeParamsTableLayoutPanel()
        {
            tlpParams.RowStyles[grpConfigCollectionLabelCsIndex].Height =
                grpConfigCollectionLabelCs.Visible
                    ? grpConfigCollectionLabelCsHeight
                    : 0;

            tlpParams.RowStyles[grpConfigCollectionLabelEnIndex].Height =
                grpConfigCollectionLabelEn.Visible
                    ? grpConfigCollectionLabelEnHeight
                    : 0;

            tlpParams.RowStyles[grpConfigCollectionDescriptionCsIndex].Height =
                    grpConfigCollectionDescriptionCs.Visible
                        ? grpConfigCollectionDescriptionCsHeight
                        : 0;

            tlpParams.RowStyles[grpConfigCollectionDescriptionEnIndex].Height =
                    grpConfigCollectionDescriptionEn.Visible
                    ? grpConfigCollectionDescriptionEnHeight
                    : 0;

            tlpParams.RowStyles[grpConfigFunctionIndex].Height =
                    grpConfigFunction.Visible
                        ? grpConfigFunctionHeight
                        : 0;

            tlpParams.RowStyles[grpAggregatedIndex].Height =
                    grpAggregated.Visible
                        ? grpAggregatedHeight
                        : 0;

            tlpParams.RowStyles[pnlAuxiliaryVariableIndex].Height =
                    pnlAuxiliaryVariable.Visible
                        ? pnlAuxiliaryVariableHeight
                        : 0;

            tlpParams.Height =
                    (int)tlpParams.RowStyles[grpConfigCollectionLabelCsIndex].Height +
                    (int)tlpParams.RowStyles[grpConfigCollectionLabelEnIndex].Height +
                    (int)tlpParams.RowStyles[grpConfigCollectionDescriptionCsIndex].Height +
                    (int)tlpParams.RowStyles[grpConfigCollectionDescriptionEnIndex].Height +
                    (int)tlpParams.RowStyles[grpConfigFunctionIndex].Height +
                    (int)tlpParams.RowStyles[grpAggregatedIndex].Height +
                    (int)tlpParams.RowStyles[pnlAuxiliaryVariableIndex].Height;
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent()
        {
            if (ConfigCollection == null)
            {
                grpConfigCollectionLabelCs.Visible = false;
                grpConfigCollectionLabelEn.Visible = false;
                grpConfigCollectionDescriptionCs.Visible = false;
                grpConfigCollectionDescriptionEn.Visible = false;
                grpConfigFunction.Visible = false;
                grpAggregated.Visible = false;
                pnlAuxiliaryVariable.Visible = false;

                ResizeParamsTableLayoutPanel();

                EnableNext();

                return;
            }

            grpConfigCollectionLabelCs.Visible = true;
            txtConfigCollectionLabelCs.Text = ConfigCollection.LabelCs ?? String.Empty;

            grpConfigCollectionLabelEn.Visible = true;
            txtConfigCollectionLabelEn.Text = ConfigCollection.LabelEn ?? String.Empty;

            grpConfigCollectionDescriptionCs.Visible = true;
            txtConfigCollectionDescriptionCs.Text = ConfigCollection.DescriptionCs ?? String.Empty;

            grpConfigCollectionDescriptionEn.Visible = true;
            txtConfigCollectionDescriptionEn.Text = ConfigCollection.DescriptionEn ?? String.Empty;

            // Typ funkce je možné nastavovat pouze u nových záznamů kolekcí
            // a později již nelze změnit
            grpConfigFunction.Visible = true;
            grpConfigFunction.Enabled = NewEntry;
            rdoFunctionVar100.Checked = (rdoFunctionVar100.Tag != null) &&
                ConfigCollection.ConfigFunctionId == ((ConfigFunction)rdoFunctionVar100.Tag).Id;
            rdoFunctionVar200.Checked = (rdoFunctionVar200.Tag != null) &&
                ConfigCollection.ConfigFunctionId == ((ConfigFunction)rdoFunctionVar200.Tag).Id;
            rdoFunctionVar300.Checked = (rdoFunctionVar300.Tag != null) &&
                ConfigCollection.ConfigFunctionId == ((ConfigFunction)rdoFunctionVar300.Tag).Id;
            rdoFunctionVar400.Checked = (rdoFunctionVar400.Tag != null) &&
                ConfigCollection.ConfigFunctionId == ((ConfigFunction)rdoFunctionVar400.Tag).Id;
            rdoFunctionVar500.Checked = (rdoFunctionVar500.Tag != null) &&
                ConfigCollection.ConfigFunctionId == ((ConfigFunction)rdoFunctionVar500.Tag).Id;
            rdoFunctionVar600.Checked = (rdoFunctionVar600.Tag != null) &&
                ConfigCollection.ConfigFunctionId == ((ConfigFunction)rdoFunctionVar600.Tag).Id;

            // Typ skupiny agregovaná|základní je možné nastavovat
            // jen u nových záznamů nebo prázdných skupin a později nelze změnit
            // Typ skupiny agregovaná|základní je možné nastavovat
            // jen úhrnů
            grpAggregated.Visible = (NewEntry || ConfigCollection.IsEmpty) &&
                ((ConfigCollection.ConfigFunctionValue == ConfigFunctionEnum.VectorTotal) ||
                (ConfigCollection.ConfigFunctionValue == ConfigFunctionEnum.RasterTotal) ||
                (ConfigCollection.ConfigFunctionValue == ConfigFunctionEnum.RasterTotalWithinVector) ||
                (ConfigCollection.ConfigFunctionValue == ConfigFunctionEnum.RastersProductTotal));
            grpAggregated.Enabled = NewEntry;
            chkAggregated.Checked = ConfigCollection.Aggregated;

            SetAuxiliaryVariableAvailability();
            if (pnlAuxiliaryVariable.Visible)
            {
                CtrAuxiliaryVariableSelector.AuxiliaryVariable =
                    (ConfigCollection?.AuxiliaryVariableId != null)
                        ? Database.SNfiEsta.CAuxiliaryVariable.Items
                            .Where(a => a.Id == ConfigCollection?.AuxiliaryVariableId)
                            .FirstOrDefault<AuxiliaryVariable>()
                        : null;
            }

            ResizeParamsTableLayoutPanel();

            EnableNext();
        }

        /// <summary>
        /// <para lang="cs">Změna českého názvu skupiny konfigurací</para>
        /// <para lang="en">Configuration collection Czech name change</para>
        /// </summary>
        private void ConfigCollectionLabelCsChanged()
        {
            if (ConfigCollection == null)
            {
                return;
            }

            ConfigCollection.LabelCs =
                !String.IsNullOrEmpty(value: txtConfigCollectionLabelCs.Text.Trim())
                ? txtConfigCollectionLabelCs.Text.Trim()
                : null;

            EnableNext();
        }

        /// <summary>
        /// <para lang="cs">Změna anglického názvu skupiny konfigurací</para>
        /// <para lang="en">Configuration collection English name change</para>
        /// </summary>
        private void ConfigCollectionLabelEnChanged()
        {
            if (ConfigCollection == null)
            {
                return;
            }

            ConfigCollection.LabelEn =
                !String.IsNullOrEmpty(value: txtConfigCollectionLabelEn.Text.Trim())
                ? txtConfigCollectionLabelEn.Text.Trim()
                : null;

            EnableNext();
        }

        /// <summary>
        /// <para lang="cs">Změna českého popisu skupiny konfigurací</para>
        /// <para lang="en">Configuration collection Czech description change</para>
        /// </summary>
        private void ConfigCollectionDescriptionCsChanged()
        {
            if (ConfigCollection == null)
            {
                return;
            }

            ConfigCollection.DescriptionCs =
                !String.IsNullOrEmpty(value: txtConfigCollectionDescriptionCs.Text.Trim())
                ? txtConfigCollectionDescriptionCs.Text.Trim()
                : null;

            EnableNext();
        }

        /// <summary>
        /// <para lang="cs">Změna anglického popisu skupiny konfigurací</para>
        /// <para lang="en">Configuration collection English description change</para>
        /// </summary>
        private void ConfigCollectionDescriptionEnChanged()
        {
            if (ConfigCollection == null)
            {
                return;
            }

            ConfigCollection.DescriptionEn =
                !String.IsNullOrEmpty(value: txtConfigCollectionDescriptionEn.Text.Trim())
                ? txtConfigCollectionDescriptionEn.Text.Trim()
                : null;

            EnableNext();
        }

        /// <summary>
        /// <para lang="cs">Změna výběru konfigurační funkce</para>
        /// <para lang="en">Configuration function change</para>
        /// </summary>
        private void ConfigFunctionChanged()
        {
            if (ConfigCollection == null)
            {
                return;
            }

            ConfigFunction selectedConfigFunction =
                grpConfigFunction.Controls.OfType<RadioButton>()
                    .Where(a => a.Tag != null)
                    .Where(a => a.Tag is ConfigFunction)
                    .Where(a => a.Checked)
                    .Select(a => (ConfigFunction)a.Tag)
                    .FirstOrDefault<ConfigFunction>();

            if (selectedConfigFunction == null)
            {
                return;
            }

            if (ConfigCollection.ConfigFunctionId != selectedConfigFunction.Id)
            {
                ConfigCollection.ConfigFunctionId = selectedConfigFunction.Id;
                ConfigCollection.Reset();
            }

            // Zobrazení panelu aggregated je možné pouze pro konfigurace úhrnů
            grpAggregated.Visible =
                (NewEntry || ConfigCollection.IsEmpty) &&
                   ((selectedConfigFunction.Value == ConfigFunctionEnum.VectorTotal) ||
                   (selectedConfigFunction.Value == ConfigFunctionEnum.RasterTotal) ||
                   (selectedConfigFunction.Value == ConfigFunctionEnum.RasterTotalWithinVector) ||
                   (selectedConfigFunction.Value == ConfigFunctionEnum.RastersProductTotal));

            SetAuxiliaryVariableAvailability();

            ResizeParamsTableLayoutPanel();

            EnableNext();
        }

        /// <summary>
        /// <para lang="cs">Změna typu skupiny konfigurací (agregovaná / není agregrovaná)</para>
        /// <para lang="en">Configuration collection type (aggregated / not aggregated) change</para>
        /// </summary>
        private void AggregatedChanged()
        {
            if (ConfigCollection == null)
            {
                return;
            }

            bool aggregated =
                ((ConfigCollection.ConfigFunctionValue == ConfigFunctionEnum.VectorTotal) ||
                (ConfigCollection.ConfigFunctionValue == ConfigFunctionEnum.RasterTotal) ||
                (ConfigCollection.ConfigFunctionValue == ConfigFunctionEnum.RasterTotalWithinVector) ||
                (ConfigCollection.ConfigFunctionValue == ConfigFunctionEnum.RastersProductTotal))
                && chkAggregated.Checked;

            if (aggregated != ConfigCollection.Aggregated)
            {
                ConfigCollection.Aggregated = aggregated;
                ConfigCollection.Reset();
            }

            EnableNext();
        }

        /// <summary>
        /// <para lang="cs">Změna výběru pomocné proměnné</para>
        /// <para lang="en">Auxiliary variable selection change</para>
        /// </summary>
        private void AuxiliaryVariableChanged()
        {
            if (pnlAuxiliaryVariable.Visible)
            {
                if (ConfigCollection == null)
                {
                    return;
                }

                ConfigCollection.AuxiliaryVariableId =
                    CtrAuxiliaryVariableSelector?.AuxiliaryVariable?.Id;

                EnableNext();
            }
        }

        /// <summary>
        /// <para lang="cs">Nastaví dostupnost pomocné proměnné</para>
        /// <para lang="en">Sets auxiliary variable availability</para>
        /// </summary>
        private void SetAuxiliaryVariableAvailability()
        {
            if (CtrAuxiliaryVariableSelector == null)
            {
                pnlAuxiliaryVariable.Visible = false;
                return;
            }

            if (ConfigCollection == null)
            {
                pnlAuxiliaryVariable.Visible = false;
                return;
            }

            if (Database.Postgres.Catalog.Schemas[NfiEstaSchema.Name] == null)
            {
                // Neexistuje schéma nfiesta
                pnlAuxiliaryVariable.Visible = false;
                return;
            }

            if (
                (Database.Postgres.Catalog.Schemas[NfiEstaSchema.Name].Tables[AuxiliaryVariableList.Name] == null) &&
                (Database.Postgres.Catalog.Schemas[NfiEstaSchema.Name].ForeignTables[AuxiliaryVariableList.Name] == null))
            {
                // Neexistuje tabulka ani cizí tabulka c_auxiliary_variable s pomocnými proměnnými
                pnlAuxiliaryVariable.Visible = false;
                return;
            }

            if (Database.SNfiEsta.CAuxiliaryVariable.Data.Rows.Count == 0)
            {
                // Tabulka c_auxiliary_variable je prázdná
                pnlAuxiliaryVariable.Visible = false;
                return;
            }

            // Pomocná proměnná se volí pro vektorovou nebo rastrvou vrstvu nebo jejich kombinace
            pnlAuxiliaryVariable.Visible =
                (ConfigCollection.ConfigFunctionValue == ConfigFunctionEnum.VectorTotal) ||
                (ConfigCollection.ConfigFunctionValue == ConfigFunctionEnum.RasterTotal) ||
                (ConfigCollection.ConfigFunctionValue == ConfigFunctionEnum.RasterTotalWithinVector) ||
                (ConfigCollection.ConfigFunctionValue == ConfigFunctionEnum.RastersProductTotal);

            return;
        }

        /// <summary>
        /// <para lang="cs">Povolí postup na další formulář</para>
        /// <para lang="en">Allows progression to the next form</para>
        /// </summary>
        private void EnableNext()
        {
            if (ConfigCollection == null)
            {
                btnNext.Enabled = false;
                return;
            }

            btnNext.Enabled =
                !String.IsNullOrEmpty(ConfigCollection.LabelCs) &&
                !String.IsNullOrEmpty(ConfigCollection.LabelEn) &&
                !String.IsNullOrEmpty(ConfigCollection.DescriptionCs) &&
                !String.IsNullOrEmpty(ConfigCollection.DescriptionEn) &&
                (ConfigCollection.ConfigFunctionValue != ConfigFunctionEnum.Unknown);
        }

        #endregion Methods

    }

}