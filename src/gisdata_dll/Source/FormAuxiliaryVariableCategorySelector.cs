﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.Core;
using ZaJi.PostgreSQL;

namespace ZaJi.ModuleAuxiliaryData
{

    /// <summary>
    /// <para lang="cs">Ovládací prvek "Výběr kategorie pomocné proměnné"</para>
    /// <para lang="en">Control "Auxiliary variable category selection"</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    internal partial class FormAuxiliaryVariableCategorySelector
        : Form, INfiEstaControl, IAuxiliaryDataControl
    {

        #region Constants

        private const int ImageRedBall = 0;
        private const int ImageBlueBall = 1;
        private const int ImageYellowBall = 2;
        private const int ImageGreenBall = 3;
        private const int ImageGrayBall = 4;
        private const int ImageLock = 5;
        private const int ImageSchema = 6;
        private const int ImageVector = 7;
        private const int ImageRaster = 8;

        #endregion Constants


        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Uzly v TreeView</para>
        /// <para lang="en">TreeView nodes</para>
        /// </summary>
        private List<TreeNode> nodes;

        /// <summary>
        /// <para lang="cs">Indikátor editace ovládacího prvku</para>
        /// <para lang="en">Control editing indicator</para>
        /// </summary>
        private bool onEdit;

        /// <summary>
        /// <para lang="cs">Pomocná proměnná</para>
        /// <para lang="en">Auxiliary variable</para>
        /// </summary>
        private AuxiliaryVariable auxiliaryVariable;

        #endregion Private Fields


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor ovládacího prvku</para>
        /// <para lang="en">Control constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        /// <param name="auxiliaryVariable">
        /// <para lang="cs">Pomocná proměnná</para>
        /// <para lang="en">Auxiliary variable</para>
        /// </param>
        public FormAuxiliaryVariableCategorySelector(
            Control controlOwner,
            AuxiliaryVariable auxiliaryVariable)
        {
            InitializeComponent();
            Initialize(
                controlOwner: controlOwner,
                auxiliaryVariable: auxiliaryVariable);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                IAuxiliaryDataControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                return controlOwner;
            }
            set
            {
                IAuxiliaryDataControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables (read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((IAuxiliaryDataControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro pomocná data (read-only)</para>
        /// <para lang="en">Module for auxiliary data setting (read-only)</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((IAuxiliaryDataControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">Vybraná kategorie pomocné proměnné</para>
        /// <para lang="en">Selected auxiliary variable category</para>
        /// </summary>
        public AuxiliaryVariableCategory AuxiliaryVariableCategory
        {
            get
            {
                return
                    CheckedAuxiliaryVariableCategories.FirstOrDefault<AuxiliaryVariableCategory>();
            }
            set
            {
                CheckOnlyOneNode(
                    id: value?.Id ?? 0);
            }
        }

        /// <summary>
        /// <para lang="cs">Pomocná proměnná</para>
        /// <para lang="en">Auxiliary variable</para>
        /// </summary>
        private AuxiliaryVariable AuxiliaryVariable
        {
            get
            {
                return auxiliaryVariable;
            }
            set
            {
                auxiliaryVariable = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Indikátor editace ovládacího prvku</para>
        /// <para lang="en">Control editing indicator</para>
        /// </summary>
        private bool OnEdit
        {
            get
            {
                return onEdit;
            }
            set
            {
                onEdit = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Uzly v TreeView</para>
        /// <para lang="en">TreeView nodes</para>
        /// </summary>
        private List<TreeNode> Nodes
        {
            get
            {
                return nodes ?? [];
            }
            set
            {
                nodes = value ?? [];
            }
        }

        /// <summary>
        /// <para lang="cs">Uzly v TreeView úroveň kategorií pomocné proměnné (read-only)</para>
        /// <para lang="en">TreeView nodes - Auxiliary variable category level (read-only)</para>
        /// </summary>
        private IEnumerable<TreeNode> AuxiliaryVariableCategoryNodes
        {
            get
            {
                return
                    Nodes
                        .Where(a => a.Tag != null)
                        .Where(a => a.Tag is AuxiliaryVariableCategory);
            }
        }

        /// <summary>
        /// <para lang="cs">Zaškrtnuté uzly kategorií pomocné proměnné (read-only)</para>
        /// <para lang="en">Checked auxiliary variable category nodes (read-only)</para>
        /// </summary>
        private IEnumerable<TreeNode> CheckedAuxiliaryVariableCategoryNodes
        {
            get
            {
                return
                    AuxiliaryVariableCategoryNodes
                        .Where(a => a.Checked);
            }
        }

        /// <summary>
        /// <para lang="cs">Zaškrtnuté kategorie pomocné proměnné (read-only)</para>
        /// <para lang="en">Checked auxiliary variable categories (read-only)</para>
        /// </summary>
        private IEnumerable<AuxiliaryVariableCategory> CheckedAuxiliaryVariableCategories
        {
            get
            {
                return CheckedAuxiliaryVariableCategoryNodes
                    .Select(a => (AuxiliaryVariableCategory)a.Tag);
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(FormAuxiliaryVariableCategorySelector),    "Výběr kategorie pomocné proměnné"},
                        { nameof(btnOK),                                    "OK" },
                        { nameof(btnCancel),                                "Zrušit" },
                        { nameof(grpItems),                                 "Seznam kategorií pomocné proměnné:" },
                        { nameof(grpSelectedItem),                          "Vybraná kategorie pomocné proměnné:" }
                    }
                    : languageFile.NationalVersion.Data.TryGetValue(
                        key: nameof(FormAuxiliaryVariableCategorySelector),
                        out Dictionary<string, string> dictNational)
                            ? dictNational
                            : [],

                LanguageVersion.International => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(FormAuxiliaryVariableCategorySelector),    "Auxiliary variable category selection" },
                        { nameof(btnOK),                                    "OK" },
                        { nameof(btnCancel),                                "Cancel" },
                        { nameof(grpItems),                                 "List of auxiliary variable categories:" },
                        { nameof(grpSelectedItem),                          "Selected auxiliary variable category:"}
                    }
                    : languageFile.InternationalVersion.Data.TryGetValue(
                        key: nameof(FormAuxiliaryVariableCategorySelector),
                        out Dictionary<string, string> dictInternational)
                            ? dictInternational
                            : [],

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku</para>
        /// <para lang="en">Initialization of the control</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        /// <param name="auxiliaryVariable">
        /// <para lang="cs">Pomocná proměnná</para>
        /// <para lang="en">Auxiliary variable</para>
        /// </param>
        private void Initialize(
            Control controlOwner,
            AuxiliaryVariable auxiliaryVariable)
        {
            ControlOwner = controlOwner;
            AuxiliaryVariable = auxiliaryVariable;
            Nodes = [];
            OnEdit = false;

            LoadContent();

            btnOK.Click += new EventHandler(
                (sender, e) =>
                {
                    DialogResult = DialogResult.OK;
                    Close();
                });

            btnCancel.Click += new EventHandler(
               (sender, e) =>
               {
                   DialogResult = DialogResult.Cancel;
                   Close();
               });

            tvwItems.AfterCheck += new TreeViewEventHandler(
                (sender, e) =>
                {
                    if (OnEdit)
                    {
                        return;
                    }

                    if ((e?.Node?.Tag != null) && (e.Node.Tag is AuxiliaryVariableCategory lastAuxiliaryVariableCategory))
                    {
                        CheckOnlyOneNode(
                            id: lastAuxiliaryVariableCategory.Id);
                    }

                    SetCaption();
                });

            tvwItems.AfterSelect += new TreeViewEventHandler(
                (sender, e) => tvwItems.SelectedNode = null);
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků ovládacího prvku</para>
        /// <para lang="en">Initialization of control labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            Text =
               labels.TryGetValue(key: nameof(FormAuxiliaryVariableCategorySelector),
               out string frmAuxiliaryVariableCategorySelectorText)
                   ? frmAuxiliaryVariableCategorySelectorText
                   : String.Empty;

            grpSelectedItem.Text =
                labels.TryGetValue(key: nameof(grpSelectedItem),
                out string grpSelectedItemText)
                    ? grpSelectedItemText
                    : String.Empty;

            grpItems.Text =
                labels.TryGetValue(key: nameof(grpItems),
                out string grpItemsText)
                    ? grpItemsText
                    : String.Empty;

            btnOK.Text =
                labels.TryGetValue(key: nameof(btnOK),
                out string btnOKText)
                    ? btnOKText
                    : String.Empty;

            btnCancel.Text =
                labels.TryGetValue(key: nameof(btnCancel),
                out string btnCancelText)
                    ? btnCancelText
                    : String.Empty;

            SetCaption();

            foreach (TreeNode node in AuxiliaryVariableCategoryNodes)
            {
                node.Text =
                    (LanguageVersion == LanguageVersion.International)
                    ? ((AuxiliaryVariableCategory)node.Tag).LabelEn
                    : (LanguageVersion == LanguageVersion.National)
                        ? ((AuxiliaryVariableCategory)node.Tag).LabelCs
                        : ((AuxiliaryVariableCategory)node.Tag).LabelEn;
            }
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent()
        {
            InitializeTreeView();

            InitializeLabels();

            SetCaption();
        }

        /// <summary>
        /// <para lang="cs">Zobrazení kategorií pomocné proměnné v uzlech TreeView</para>
        /// <para lang="en">Displaying auxiliary variable categories in TreeView nodes</para>
        /// </summary>
        private void InitializeTreeView()
        {
            // Zabrání prokreslování TreeView během editace
            tvwItems.BeginUpdate();

            // Smazání všech uzlů z TreeView
            Nodes.Clear();
            tvwItems.Nodes.Clear();

            // (Seznam kategorií pomocné proměnné)
            foreach (AuxiliaryVariableCategory auxiliaryVariableCategory
                in Database.SNfiEsta.CAuxiliaryVariableCategory.Items
                .Where(a => a.AuxiliaryVariable.Id == AuxiliaryVariable.Id))
            {
                TreeNode node = new()
                {
                    ImageIndex = ImageGreenBall,
                    Name = auxiliaryVariableCategory.Id.ToString(),
                    SelectedImageIndex = ImageGreenBall,
                    Tag = auxiliaryVariableCategory,
                    Text =
                        (LanguageVersion == LanguageVersion.International)
                            ? auxiliaryVariableCategory.LabelEn
                            : (LanguageVersion == LanguageVersion.National)
                                ? auxiliaryVariableCategory.LabelCs
                                : auxiliaryVariableCategory.LabelEn
                };
                Nodes.Add(item: node);
                tvwItems.Nodes.Add(node: node);
            }

            tvwItems.EndUpdate();
        }

        /// <summary>
        /// <para lang="cs">
        /// Vrátí uzel z TreeView odpovídající identifikačnímu číslu kategorie pomocné proměnné
        /// </para>
        /// <para lang="en">
        /// Returns the TreeView node corresponding to the auxiliary variable category identifier
        /// </para>
        /// </summary>
        /// <param name="id">
        /// <para lang="cs">Identifikační číslo kategorie pomocné proměnné</para>
        /// <para lang="en">Auxiliary variable category identifier</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">
        /// Vrátí uzel z TreeView odpovídající identifikačnímu číslu kategorie pomocné proměnné
        /// </para>
        /// <para lang="en">
        /// Returns the TreeView node corresponding to the auxiliary variable category identifier
        /// </para>
        /// </returns>
        private TreeNode FindNode(int id)
        {
            return AuxiliaryVariableCategoryNodes
                .Where(a => ((AuxiliaryVariableCategory)a.Tag).Id == id)
                .FirstOrDefault<TreeNode>();
        }

        /// <summary>
        /// <para lang="cs">Zaškrtne pouze jeden uzel v TreeView</para>
        /// <para lang="en">Check only one node in TreeView</para>
        /// </summary>
        /// <param name="id">
        /// <para lang="cs">Identifikační číslo kategorie pomocné proměnné</para>
        /// <para lang="en">Auxiliary variable category identifier</para>
        /// </param>
        private void CheckOnlyOneNode(int id)
        {
            OnEdit = true;

            foreach (TreeNode node in AuxiliaryVariableCategoryNodes)
            {
                node.Checked = false;
            }

            TreeNode checkedNode = FindNode(id: id);
            if (checkedNode != null)
            {
                checkedNode.Checked = true;
            }

            OnEdit = false;

            SetCaption();
        }

        /// <summary>
        /// <para lang="cs">Nastaví nadpis se seznamem vybraných pomocných proměnných</para>
        /// <para lang="en">Sets the title with a list of selected auxiliary variables</para>
        /// </summary>
        private void SetCaption()
        {
            string strSelectedItemsEn =
                CheckedAuxiliaryVariableCategories.Any()
                    ? CheckedAuxiliaryVariableCategories
                        .Select(a => a.ExtendedLabelEn)
                        .Aggregate((a, b) => $"{a}, {b}")
                    : String.Empty;

            string strSelectedItemsCs =
                CheckedAuxiliaryVariableCategories.Any()
                   ? CheckedAuxiliaryVariableCategories
                    .Select(a => a.ExtendedLabelCs)
                    .Aggregate((a, b) => $"{a}, {b}")
                    : String.Empty;

            lblSelectedItem.Text =
                (LanguageVersion == LanguageVersion.International)
                    ? strSelectedItemsEn
                    : (LanguageVersion == LanguageVersion.National)
                        ? strSelectedItemsCs
                        : strSelectedItemsEn;
        }

        #endregion Methods

    }

}