﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.AuxiliaryData;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.Catalog;

namespace ZaJi.ModuleAuxiliaryData
{

    /// <summary>
    /// <para lang="cs">Ovládací prvek "Výběr sloupců s doplňujícími informacemi pro bodovou vrstvu"</para>
    /// <para lang="en">Control "Select columns with additional information for the point layer"</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    internal partial class ControlPointGeom
        : UserControl, INfiEstaControl, IAuxiliaryDataControl
    {

        #region Constants

        /// <summary>
        /// <para lang="cs">Defaultní jméno sloupce s geometrií bodu</para>
        /// <para lang="en">Default column name with point geometry</para>
        /// </summary>
        private const string GEOM = "geom";

        /// <summary>
        /// <para lang="cs">Není vybraný žádný sloupec tabulky</para>
        /// <para lang="en">No table column is selected</para>
        /// </summary>
        private const string NONE = "none";

        private const string NAME = "Name";
        private const string INTEGER = "integer";
        private const string GEOMETRY = "geometry";
        private const string POINT = "point";

        #endregion Constants


        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Doplňující informace k bodové vrstvě</para>
        /// <para lang="en">Additional information on the point layer</para>
        /// </summary>
        private PointAdditionalColumns info;

        #endregion Private Fields


        #region Controls

        private Label lblColumnNameError;

        #endregion Controls


        #region Events

        /// <summary>
        /// <para lang="cs">Událost kliknutí na tlačítko "Předchozí"</para>
        /// <para lang="en">Click event on the "Previous" button</para>
        /// </summary>
        public event EventHandler PreviousClick;

        /// <summary>
        /// <para lang="cs">Událost kliknutí na tlačítko "Další"</para>
        /// <para lang="en">Click event on the "Next" button</para>
        /// </summary>
        public event EventHandler NextClick;

        #endregion Events


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor ovládacího prvku</para>
        /// <para lang="en">Control constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        public ControlPointGeom(Control controlOwner)
        {
            InitializeComponent();
            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                IAuxiliaryDataControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                if (controlOwner is not FormConfigCollectionGuide)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(FormConfigCollectionGuide)}.",
                        paramName: nameof(ControlOwner));
                }

                return controlOwner;
            }
            set
            {
                IAuxiliaryDataControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                if (value is not FormConfigCollectionGuide)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(FormConfigCollectionGuide)}.",
                        paramName: nameof(ControlOwner));
                }

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables (read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((IAuxiliaryDataControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro pomocná data (read-only)</para>
        /// <para lang="en">Module for auxiliary data setting (read-only)</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((IAuxiliaryDataControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">Skupina konfigurací</para>
        /// <para lang="en">Configuration collection</para>
        /// </summary>
        public ConfigCollection ConfigCollection
        {
            get
            {
                return ((FormConfigCollectionGuide)ControlOwner).ConfigCollection;
            }
        }

        /// <summary>
        /// <para lang="cs">Doplňující informace k bodové vrstvě</para>
        /// <para lang="en">Additional information on the point layer</para>
        /// </summary>
        public PointAdditionalColumns Info
        {
            get
            {
                return info;
            }
            set
            {
                info = value;
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(btnPrevious),              "Předchozí" },
                        { nameof(btnNext),                  "Další" },
                        { nameof(grpCluster),               "Sloupec s identifikátorem klastru:" },
                        { nameof(grpCountry),               "Sloupec s identifikátorem země:" },
                        { nameof(grpGeom),                  "Sloupec s polohou (geometrií) inventarizačního bodu:" },
                        { nameof(grpPanel),                 "Sloupec s identifikátorem panelu:" },
                        { nameof(grpPlot),                  "Sloupec s identifikátorem inventarizačního bodu:" },
                        { nameof(grpStrataSet),             "Sloupec s identifikátorem skupiny strat:" },
                        { nameof(grpStratum),               "Sloupec s identifikátorem stata:" },
                        { nameof(lblColumnNameError),       "Vybraná databázová tabulka nemá žádný atribut typu geometrie bod." }
                    }
                    : languageFile.NationalVersion.Data.TryGetValue(
                        key: nameof(ControlPointGeom),
                        out Dictionary<string, string> dictNational)
                            ? dictNational
                            : [],

                LanguageVersion.International => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(btnPrevious),              "Previous" },
                        { nameof(btnNext),                  "Next" },
                        { nameof(grpCluster),               "Cluster identifier column:" },
                        { nameof(grpCountry),               "Country identifier column:" },
                        { nameof(grpGeom),                  "Inventory point geometry column:" },
                        { nameof(grpPanel),                 "Panel identifier column:" },
                        { nameof(grpPlot),                  "Inventory point identifier column:" },
                        { nameof(grpStrataSet),             "Strata set identifier column:" },
                        { nameof(grpStratum),               "Stratum identifier column:" },
                        { nameof(lblColumnNameError),       "Selected database table does not contain any attribute with type geometry point." }
                    }
                    : languageFile.InternationalVersion.Data.TryGetValue(
                        key: nameof(ControlPointGeom),
                        out Dictionary<string, string> dictInternational)
                            ? dictInternational
                            : [],

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku</para>
        /// <para lang="en">Initialization of the control</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        private void Initialize(Control controlOwner)
        {
            BorderStyle = BorderStyle.None;
            ControlOwner = controlOwner;
            Info = new PointAdditionalColumns();

            InitializeLabels();

            btnPrevious.Click += new EventHandler(
                (sender, e) =>
                {
                    if (ConfigCollection != null)
                    {
                        ConfigCollection.ColumnIdent = null;
                        ConfigCollection.Tag = Info.JSON;
                    }

                    PreviousClick?.Invoke(sender: sender, e: e);
                });

            btnNext.Click += new EventHandler(
               (sender, e) =>
               {
                   if (ConfigCollection != null)
                   {
                       ConfigCollection.ColumnIdent = null;
                       ConfigCollection.Tag = Info.JSON;
                   }

                   NextClick?.Invoke(sender: sender, e: e);
               });
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků ovládacího prvku</para>
        /// <para lang="en">Initialization of control labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            btnPrevious.Text =
                labels.TryGetValue(key: nameof(btnPrevious),
                out string btnPreviousText)
                    ? btnPreviousText
                    : String.Empty;

            btnNext.Text =
                labels.TryGetValue(key: nameof(btnNext),
                out string btnNextText)
                    ? btnNextText
                    : String.Empty;

            grpCluster.Text =
                labels.TryGetValue(key: nameof(grpCluster),
                out string grpClusterText)
                    ? grpClusterText
                    : String.Empty;

            grpCountry.Text =
                labels.TryGetValue(key: nameof(grpCountry),
                out string grpCountryText)
                    ? grpCountryText
                    : String.Empty;

            grpGeom.Text =
                labels.TryGetValue(key: nameof(grpGeom),
                out string grpGeomText)
                    ? grpGeomText
                    : String.Empty;

            grpPanel.Text =
                labels.TryGetValue(key: nameof(grpPanel),
                out string grpPanelText)
                    ? grpPanelText
                    : String.Empty;

            grpPlot.Text =
                labels.TryGetValue(key: nameof(grpPlot),
                out string grpPlotText)
                    ? grpPlotText
                    : String.Empty;

            grpStrataSet.Text =
               labels.TryGetValue(key: nameof(grpStrataSet),
               out string grpStrataSetText)
                   ? grpStrataSetText
                   : String.Empty;

            grpStratum.Text =
               labels.TryGetValue(key: nameof(grpStratum),
               out string grpStratumText)
                   ? grpStratumText
                   : String.Empty;
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent()
        {
            if (ConfigCollection != null)
            {
                Info = PointAdditionalColumns.Deserialize(json: ConfigCollection.Tag);
            }

            InitializeComboBoxColumnName();

            InitializeComboBoxCluster();

            InitializeComboBoxCountry();

            InitializeComboBoxPanel();

            InitializeComboBoxPlot();

            InitializeComboBoxStrataSet();

            InitializeComboBoxStratum();

            EnableNext();
        }

        /// <summary>
        /// <para lang="cs">Povolí postup na další formulář</para>
        /// <para lang="en">Allows progression to the next form</para>
        /// </summary>
        private void EnableNext()
        {
            if (ConfigCollection == null)
            {
                btnNext.Enabled = false;
                return;
            }

            btnNext.Enabled =
                   !String.IsNullOrEmpty(ConfigCollection.LabelCs) &&
                   !String.IsNullOrEmpty(ConfigCollection.LabelEn) &&
                   !String.IsNullOrEmpty(ConfigCollection.DescriptionCs) &&
                   !String.IsNullOrEmpty(ConfigCollection.DescriptionEn) &&
                   (ConfigCollection.ConfigFunctionValue == ConfigFunctionEnum.PointLayer) &&
                   !String.IsNullOrEmpty(value: ConfigCollection?.CatalogName) &&
                   !String.IsNullOrEmpty(value: ConfigCollection?.SchemaName) &&
                   !String.IsNullOrEmpty(value: ConfigCollection?.TableName) &&
                   !String.IsNullOrEmpty(value: ConfigCollection?.ColumnName) &&
                   !String.IsNullOrEmpty(value: Info?.JSON);
        }

        /// <summary>
        /// <para lang="cs">Seznam všech sloupců tabulky nebo cízí tabulky</para>
        /// <para lang="en">List of all columns in table or foreign table</para>
        /// </summary>
        /// <param name="schemaName">
        /// <para lang="cs">Jméno schéma</para>
        /// <para lang="en">Schema name</para>
        /// </param>
        /// <param name="tableName">
        /// <para lang="cs">Jméno tabulky</para>
        /// <para lang="en">Table name</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Seznam všech sloupců tabulky nebo cízí tabulky</para>
        /// <para lang="en">List of all columns in table or foreign table</para>
        /// </returns>
        private List<DBColumn> GetColumns(string schemaName, string tableName)
        {
            if (Database.Postgres.Catalog.Schemas[schemaName] == null)
            {
                // Schéma neexistuje, vrátí se prázdný seznam
                return [];
            }

            if (Database.Postgres.Catalog.Schemas[schemaName].Tables[tableName] != null)
            {
                // Existuje datová tabulka
                return
                    [.. Database.Postgres.Catalog.Schemas[schemaName].Tables[tableName].Columns.Items];
            }

            else if (Database.Postgres.Catalog.Schemas[schemaName].ForeignTables[tableName] != null)
            {
                // Existuje cizí datová tabulka
                return
                    [.. Database.Postgres.Catalog.Schemas[schemaName].ForeignTables[tableName].Columns.Items];
            }

            else
            {
                // Tabulka neexistuje, vrátí se prázdný seznam
                return [];
            }
        }

        /// <summary>
        /// <para lang="cs">Seznam sloupců, které jsou součástí jednoduchého primárního klíče nebo jednoduchého unique omezení</para>
        /// <para lang="en">List of columns that are part of a simple primary key or simple unique constraint</para>
        /// </summary>
        /// <param name="schemaName">
        /// <para lang="cs">Jméno schéma</para>
        /// <para lang="en">Schema name</para>
        /// </param>
        /// <param name="tableName">
        /// <para lang="cs">Jméno tabulky</para>
        /// <para lang="en">Table name</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Seznam sloupců, které jsou součástí jednoduchého primárního klíče nebo jednoduchého unique omezení</para>
        /// <para lang="en">List of columns that are part of a simple primary key or simple unique constraint</para>
        /// </returns>
        private List<DBColumn> GetIdentColumns(string schemaName, string tableName)
        {
            if (Database.Postgres.Catalog.Schemas[schemaName] == null)
            {
                // Schéma neexistuje, vrátí se prázdný seznam
                return [];
            }

            if (Database.Postgres.Catalog.Schemas[schemaName].Tables[tableName] != null)
            {
                // Existuje datová tabulka

                // Vybere sloupečky z jednoduchých primárních klíčů (obsahují pouze jeden sloupeček)
                IEnumerable<DBColumn> pkeyCols =
                    Database.Postgres.Catalog.Schemas[schemaName].Tables[tableName].PrimaryKeys.Items
                        .Where(a => a.ConstraintColumns.Count == 1)
                        .SelectMany(a => a.ConstraintColumns);

                // Vybere sloupečky z jednoduchých unique omezení (obsahují pouze jeden sloupeček)
                IEnumerable<DBColumn> ukeyCols =
                    Database.Postgres.Catalog.Schemas[schemaName].Tables[tableName].UniqueConstraints.Items
                        .Where(a => a.ConstraintColumns.Count == 1)
                        .SelectMany(a => a.ConstraintColumns);

                return [.. pkeyCols
                        .Concat(second: ukeyCols)
                        .Distinct<DBColumn>()
                        .OrderBy(a => a.Name)];
            }

            else if (Database.Postgres.Catalog.Schemas[schemaName].ForeignTables[tableName] != null)
            {
                // Existuje cizí datová tabulka

                // Pro cizí tabulky není dostupný seznam primárních klíčů a unique omezení
                // vrací se seznam všech sloupců datového typu integer
                return
                    [.. Database.Postgres.Catalog.Schemas[schemaName].ForeignTables[tableName].Columns.Items
                    .Where(a => a.TypeInfo.Contains(
                        value: INTEGER,
                        comparisonType: StringComparison.InvariantCultureIgnoreCase))
                    .Distinct<DBColumn>()
                    .OrderBy(a => a.Name)];
            }

            else
            {
                // Tabulka neexistuje, vrátí se prázdný seznam
                return [];
            }
        }

        /// <summary>
        /// <para lang="cs">Seznam sloupců, které mají datový typ geometrie (bod)</para>
        /// <para lang="en">List of columns that have the geometry data type (point)</para>
        /// </summary>
        /// <param name="schemaName">
        /// <para lang="cs">Jméno schéma</para>
        /// <para lang="en">Schema name</para>
        /// </param>
        /// <param name="tableName">
        /// <para lang="cs">Jméno tabulky</para>
        /// <para lang="en">Table name</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Seznam sloupců, které mají datový typ geometrie (bod)</para>
        /// <para lang="en">List of columns that have the geometry data type (point)</para>
        /// </returns>
        private List<DBColumn> GetPointColumns(string schemaName, string tableName)
        {
            if (Database.Postgres.Catalog.Schemas[schemaName] == null)
            {
                // Schéma neexistuje, vrátí se prázdný seznam
                return [];
            }

            if (Database.Postgres.Catalog.Schemas[schemaName].Tables[tableName] != null)
            {
                // Existuje datová tabulka
                return
                    [..
                    Database.Postgres.Catalog.Schemas[schemaName].Tables[tableName].Columns.Items
                    .Where(a => a.TypeName.Contains(
                        value: GEOMETRY,
                        comparisonType: StringComparison.InvariantCultureIgnoreCase))
                    .Where(a => a.TypeInfo.Contains(
                        value: POINT,
                        comparisonType: StringComparison.InvariantCultureIgnoreCase))
                    .Distinct<DBColumn>()
                    .OrderBy(a => a.Name)
                    ];
            }

            else if (Database.Postgres.Catalog.Schemas[schemaName].ForeignTables[tableName] != null)
            {
                // Existuje cizí datová tabulka
                return
                    [..
                    Database.Postgres.Catalog.Schemas[schemaName].ForeignTables[tableName].Columns.Items
                    .Where(a => a.TypeName.Contains(
                        value: GEOMETRY,
                        comparisonType: StringComparison.InvariantCultureIgnoreCase))
                    .Where(a => a.TypeInfo.Contains(
                        value: POINT,
                        comparisonType: StringComparison.InvariantCultureIgnoreCase))
                    .Distinct<DBColumn>()
                    .OrderBy(a => a.Name)
                    ];
            }

            else
            {
                // Tabulka neexistuje, vrátí se prázdný seznam
                return [];
            }
        }

        /// <summary>
        /// <para lang="cs">
        /// Inicializace ComboBox se seznamem sloupců,
        /// které mají datový typ geometrie (bod)
        /// </para>
        /// <para lang="en">
        /// Initializing ComboBox with list of columns
        /// that have the geometry data type (point)
        /// </para>
        /// </summary>
        private void InitializeComboBoxColumnName()
        {
            grpGeom.Controls.Clear();

            if (ConfigCollection == null)
            {
                return;
            }
            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            List<DBColumn> geometryColumns = GetPointColumns(
                schemaName: ConfigCollection?.SchemaName,
                tableName: ConfigCollection?.TableName);

            DBColumn selectedItem = geometryColumns
                .Where(a => a.Name == (ConfigCollection.ColumnName ?? String.Empty))
                .FirstOrDefault<DBColumn>();

            if (geometryColumns.Count == 0)
            {
                lblColumnNameError = new()
                {
                    AutoSize = false,
                    BackColor = Color.Transparent,
                    Dock = DockStyle.Fill,
                    Font = Setting.LabelErrorFont,
                    ForeColor = Setting.LabelErrorForeColor,
                    Margin = new Padding(all: 0),
                    Padding = new Padding(all: 0),
                    TextAlign = ContentAlignment.MiddleLeft,
                    Text =
                        labels.TryGetValue(key: nameof(lblColumnNameError),
                        out string lblColumnNameErrorText)
                            ? lblColumnNameErrorText
                            : String.Empty
                };
                lblColumnNameError.CreateControl();
                grpGeom.Controls.Add(value: lblColumnNameError);
                return;
            }

            ComboBox cboColumnName = new()
            {
                BackColor = Setting.ComboBoxBackColor,
                DataSource = geometryColumns,
                DisplayMember = NAME,
                Dock = DockStyle.Fill,
                DropDownStyle = ComboBoxStyle.DropDownList,
                FlatStyle = FlatStyle.Standard,
                Font = Setting.ComboBoxFont,
                ForeColor = Setting.ComboBoxForeColor,
                ValueMember = NAME
            };

            cboColumnName.SelectedIndexChanged += new EventHandler(
               (sender, e) =>
               {
                   if (cboColumnName.SelectedItem != null)
                   {
                       ConfigCollection.ColumnName = !String.IsNullOrEmpty(
                            value: ((DBColumn)cboColumnName.SelectedItem)?.Name)
                                ? ((DBColumn)cboColumnName.SelectedItem)?.Name
                                : null;
                       EnableNext();
                   }
               });

            cboColumnName.CreateControl();
            grpGeom.Controls.Add(value: cboColumnName);

            // Volba selected item v ComboBox
            if (selectedItem != null)
            {
                cboColumnName.SelectedItem = null;
                cboColumnName.SelectedItem = selectedItem;
            }
            else
            {
                selectedItem = geometryColumns
                    .Where(a => a.Name == GEOM)
                    .FirstOrDefault<DBColumn>();

                if (selectedItem != null)
                {
                    cboColumnName.SelectedItem = null;
                    cboColumnName.SelectedItem = selectedItem;
                }
                else if (geometryColumns.Count > 0)
                {
                    cboColumnName.SelectedItem = null;
                    selectedItem = geometryColumns[0];
                    cboColumnName.SelectedItem = selectedItem;
                }
            }
        }

        /// <summary>
        /// <para lang="cs">
        /// Inicializace ComboBox pro volbu identifikátoru klastru
        /// </para>
        /// <para lang="en">
        /// Initializing ComboBox for cluster identifier selection
        /// </para>
        /// </summary>
        private void InitializeComboBoxCluster()
        {
            grpCluster.Controls.Clear();

            if (ConfigCollection == null)
            {
                return;
            }

            List<DBColumn> columns = GetColumns(
                schemaName: ConfigCollection?.SchemaName,
                tableName: ConfigCollection?.TableName);
            columns.Insert(index: 0, item: new DBColumn() { Name = NONE });

            DBColumn selectedItem = columns
                .Where(a => a.Name == (Info.Cluster ?? PointAdditionalColumns.Default().Cluster))
                .FirstOrDefault<DBColumn>();

            ComboBox cboCluster = new()
            {
                BackColor = Setting.ComboBoxBackColor,
                DataSource = columns,
                DisplayMember = NAME,
                Dock = DockStyle.Fill,
                DropDownStyle = ComboBoxStyle.DropDownList,
                FlatStyle = FlatStyle.Standard,
                Font = Setting.ComboBoxFont,
                ForeColor = Setting.ComboBoxForeColor,
                ValueMember = NAME
            };

            cboCluster.SelectedIndexChanged += new EventHandler(
               (sender, e) =>
               {
                   if (cboCluster.SelectedItem != null)
                   {
                       string name =
                            !String.IsNullOrEmpty(value: ((DBColumn)cboCluster.SelectedItem)?.Name)
                                ? ((DBColumn)cboCluster.SelectedItem)?.Name
                                : null;

                       Info.Cluster =
                            (name != NONE)
                            ? name
                            : null;

                       EnableNext();
                   }
               });

            cboCluster.CreateControl();
            grpCluster.Controls.Add(value: cboCluster);

            // Volba selected item v ComboBox
            if (selectedItem != null)
            {
                cboCluster.SelectedItem = null;
                cboCluster.SelectedItem = selectedItem;
            }
            else
            {
                selectedItem = columns
                    .Where(a => a.Name == NONE)
                    .FirstOrDefault<DBColumn>();

                if (selectedItem != null)
                {
                    cboCluster.SelectedItem = null;
                    cboCluster.SelectedItem = selectedItem;
                }
                else if (columns.Count > 0)
                {
                    cboCluster.SelectedItem = null;
                    selectedItem = columns[0];
                    cboCluster.SelectedItem = selectedItem;
                }
            }
        }

        /// <summary>
        /// <para lang="cs">
        /// Inicializace ComboBox pro volbu identifikátoru země
        /// </para>
        /// <para lang="en">
        /// Initializing ComboBox for country identifier selection
        /// </para>
        /// </summary>
        private void InitializeComboBoxCountry()
        {
            grpCountry.Controls.Clear();

            if (ConfigCollection == null)
            {
                return;
            }

            List<DBColumn> columns = GetColumns(
                schemaName: ConfigCollection?.SchemaName,
                tableName: ConfigCollection?.TableName);
            columns.Insert(index: 0, item: new DBColumn() { Name = NONE });

            DBColumn selectedItem = columns
                .Where(a => a.Name == (Info.Country ?? PointAdditionalColumns.Default().Country))
                .FirstOrDefault<DBColumn>();

            ComboBox cboCountry = new()
            {
                BackColor = Setting.ComboBoxBackColor,
                DataSource = columns,
                DisplayMember = NAME,
                Dock = DockStyle.Fill,
                DropDownStyle = ComboBoxStyle.DropDownList,
                FlatStyle = FlatStyle.Standard,
                Font = Setting.ComboBoxFont,
                ForeColor = Setting.ComboBoxForeColor,
                ValueMember = NAME
            };

            cboCountry.SelectedIndexChanged += new EventHandler(
               (sender, e) =>
               {
                   string name =
                             !String.IsNullOrEmpty(value: ((DBColumn)cboCountry.SelectedItem)?.Name)
                                 ? ((DBColumn)cboCountry.SelectedItem)?.Name
                                 : null;

                   Info.Country =
                        (name != NONE)
                        ? name
                        : null;

                   EnableNext();
               });

            cboCountry.CreateControl();
            grpCountry.Controls.Add(value: cboCountry);

            // Volba selected item v ComboBox
            if (selectedItem != null)
            {
                cboCountry.SelectedItem = null;
                cboCountry.SelectedItem = selectedItem;
            }
            else
            {
                selectedItem = columns
                    .Where(a => a.Name == NONE)
                    .FirstOrDefault<DBColumn>();

                if (selectedItem != null)
                {
                    cboCountry.SelectedItem = null;
                    cboCountry.SelectedItem = selectedItem;
                }
                else if (columns.Count > 0)
                {
                    cboCountry.SelectedItem = null;
                    selectedItem = columns[0];
                    cboCountry.SelectedItem = selectedItem;
                }
            }
        }

        /// <summary>
        /// <para lang="cs">
        /// Inicializace ComboBox pro volbu identifikátoru panelu
        /// </para>
        /// <para lang="en">
        /// Initializing ComboBox for panel identifier selection
        /// </para>
        /// </summary>
        private void InitializeComboBoxPanel()
        {
            grpPanel.Controls.Clear();

            if (ConfigCollection == null)
            {
                return;
            }

            List<DBColumn> columns = GetColumns(
                schemaName: ConfigCollection?.SchemaName,
                tableName: ConfigCollection?.TableName);
            columns.Insert(index: 0, item: new DBColumn() { Name = NONE });

            DBColumn selectedItem = columns
                .Where(a => a.Name == (Info.Panel ?? PointAdditionalColumns.Default().Panel))
                .FirstOrDefault<DBColumn>();

            ComboBox cboPanel = new()
            {
                BackColor = Setting.ComboBoxBackColor,
                DataSource = columns,
                DisplayMember = NAME,
                Dock = DockStyle.Fill,
                DropDownStyle = ComboBoxStyle.DropDownList,
                FlatStyle = FlatStyle.Standard,
                Font = Setting.ComboBoxFont,
                ForeColor = Setting.ComboBoxForeColor,
                ValueMember = NAME
            };

            cboPanel.SelectedIndexChanged += new EventHandler(
               (sender, e) =>
               {
                   if (cboPanel.SelectedItem != null)
                   {
                       string name =
                            !String.IsNullOrEmpty(value: ((DBColumn)cboPanel.SelectedItem)?.Name)
                                ? ((DBColumn)cboPanel.SelectedItem)?.Name
                                : null;

                       Info.Panel =
                            (name != NONE)
                            ? name
                            : null;

                       EnableNext();
                   }
               });

            cboPanel.CreateControl();
            grpPanel.Controls.Add(value: cboPanel);

            // Volba selected item v ComboBox
            if (selectedItem != null)
            {
                cboPanel.SelectedItem = null;
                cboPanel.SelectedItem = selectedItem;
            }
            else
            {
                selectedItem = columns
                    .Where(a => a.Name == NONE)
                    .FirstOrDefault<DBColumn>();

                if (selectedItem != null)
                {
                    cboPanel.SelectedItem = null;
                    cboPanel.SelectedItem = selectedItem;
                }
                else if (columns.Count > 0)
                {
                    cboPanel.SelectedItem = null;
                    selectedItem = columns[0];
                    cboPanel.SelectedItem = selectedItem;
                }
            }
        }

        /// <summary>
        /// <para lang="cs">
        /// Inicializace ComboBox pro volbu identifikátoru inventarizační plochy
        /// </para>
        /// <para lang="en">
        /// Initializing ComboBox for inventory plot identifier selection
        /// </para>
        /// </summary>
        private void InitializeComboBoxPlot()
        {
            grpPlot.Controls.Clear();

            if (ConfigCollection == null)
            {
                return;
            }

            List<DBColumn> columns = GetColumns(
                schemaName: ConfigCollection?.SchemaName,
                tableName: ConfigCollection?.TableName);
            columns.Insert(index: 0, item: new DBColumn() { Name = NONE });

            DBColumn selectedItem = columns
                .Where(a => a.Name == (Info.Plot ?? PointAdditionalColumns.Default().Plot))
                .FirstOrDefault<DBColumn>();

            ComboBox cboPlot = new()
            {
                BackColor = Setting.ComboBoxBackColor,
                DataSource = columns,
                DisplayMember = NAME,
                Dock = DockStyle.Fill,
                DropDownStyle = ComboBoxStyle.DropDownList,
                FlatStyle = FlatStyle.Standard,
                Font = Setting.ComboBoxFont,
                ForeColor = Setting.ComboBoxForeColor,
                ValueMember = NAME
            };

            cboPlot.SelectedIndexChanged += new EventHandler(
               (sender, e) =>
               {
                   if (cboPlot.SelectedItem != null)
                   {
                       string name =
                            !String.IsNullOrEmpty(value: ((DBColumn)cboPlot.SelectedItem)?.Name)
                                ? ((DBColumn)cboPlot.SelectedItem)?.Name
                                : null;

                       Info.Plot =
                            (name != NONE)
                            ? name
                            : null;

                       EnableNext();
                   }
               });

            cboPlot.CreateControl();
            grpPlot.Controls.Add(value: cboPlot);

            // Volba selected item v ComboBox
            if (selectedItem != null)
            {
                cboPlot.SelectedItem = null;
                cboPlot.SelectedItem = selectedItem;
            }
            else
            {
                selectedItem = columns
                    .Where(a => a.Name == NONE)
                    .FirstOrDefault<DBColumn>();

                if (selectedItem != null)
                {
                    cboPlot.SelectedItem = null;
                    cboPlot.SelectedItem = selectedItem;
                }
                else if (columns.Count > 0)
                {
                    cboPlot.SelectedItem = null;
                    selectedItem = columns[0];
                    cboPlot.SelectedItem = selectedItem;
                }
            }
        }

        /// <summary>
        /// <para lang="cs">
        /// Inicializace ComboBox pro volbu identifikátoru skupiny strat
        /// </para>
        /// <para lang="en">
        /// Initializing ComboBox for strata set identifier selection
        /// </para>
        /// </summary>
        private void InitializeComboBoxStrataSet()
        {
            grpStrataSet.Controls.Clear();

            if (ConfigCollection == null)
            {
                return;
            }

            List<DBColumn> columns = GetColumns(
                schemaName: ConfigCollection?.SchemaName,
                tableName: ConfigCollection?.TableName);
            columns.Insert(index: 0, item: new DBColumn() { Name = NONE });

            DBColumn selectedItem = columns
                .Where(a => a.Name == (Info.StrataSet ?? PointAdditionalColumns.Default().StrataSet))
                .FirstOrDefault<DBColumn>();

            ComboBox cboStrataSet = new()
            {
                BackColor = Setting.ComboBoxBackColor,
                DataSource = columns,
                DisplayMember = NAME,
                Dock = DockStyle.Fill,
                DropDownStyle = ComboBoxStyle.DropDownList,
                FlatStyle = FlatStyle.Standard,
                Font = Setting.ComboBoxFont,
                ForeColor = Setting.ComboBoxForeColor,
                ValueMember = NAME
            };

            cboStrataSet.SelectedIndexChanged += new EventHandler(
               (sender, e) =>
               {
                   if (cboStrataSet.SelectedItem != null)
                   {
                       string name =
                            !String.IsNullOrEmpty(value: ((DBColumn)cboStrataSet.SelectedItem)?.Name)
                                ? ((DBColumn)cboStrataSet.SelectedItem)?.Name
                                : null;

                       Info.StrataSet =
                            (name != NONE)
                            ? name
                            : null;

                       EnableNext();
                   }
               });

            cboStrataSet.CreateControl();
            grpStrataSet.Controls.Add(value: cboStrataSet);

            // Volba selected item v ComboBox
            if (selectedItem != null)
            {
                cboStrataSet.SelectedItem = null;
                cboStrataSet.SelectedItem = selectedItem;
            }
            else
            {
                selectedItem = columns
                    .Where(a => a.Name == NONE)
                    .FirstOrDefault<DBColumn>();

                if (selectedItem != null)
                {
                    cboStrataSet.SelectedItem = null;
                    cboStrataSet.SelectedItem = selectedItem;
                }
                else if (columns.Count > 0)
                {
                    cboStrataSet.SelectedItem = null;
                    selectedItem = columns[0];
                    cboStrataSet.SelectedItem = selectedItem;
                }
            }
        }

        /// <summary>
        /// <para lang="cs">
        /// Inicializace ComboBox pro volbu identifikátoru strata
        /// </para>
        /// <para lang="en">
        /// Initializing ComboBox for stratum identifier selection
        /// </para>
        /// </summary>
        private void InitializeComboBoxStratum()
        {
            grpStratum.Controls.Clear();

            if (ConfigCollection == null)
            {
                return;
            }

            List<DBColumn> columns = GetColumns(
                schemaName: ConfigCollection?.SchemaName,
                tableName: ConfigCollection?.TableName);
            columns.Insert(index: 0, item: new DBColumn() { Name = NONE });

            DBColumn selectedItem = columns
                .Where(a => a.Name == (Info.Stratum ?? PointAdditionalColumns.Default().Stratum))
                .FirstOrDefault<DBColumn>();

            ComboBox cboStratum = new()
            {
                BackColor = Setting.ComboBoxBackColor,
                DataSource = columns,
                DisplayMember = NAME,
                Dock = DockStyle.Fill,
                DropDownStyle = ComboBoxStyle.DropDownList,
                FlatStyle = FlatStyle.Standard,
                Font = Setting.ComboBoxFont,
                ForeColor = Setting.ComboBoxForeColor,
                ValueMember = NAME
            };

            cboStratum.SelectedIndexChanged += new EventHandler(
               (sender, e) =>
               {
                   if (cboStratum.SelectedItem != null)
                   {
                       string name =
                            !String.IsNullOrEmpty(value: ((DBColumn)cboStratum.SelectedItem)?.Name)
                                ? ((DBColumn)cboStratum.SelectedItem)?.Name
                                : null;

                       Info.Stratum =
                            (name != NONE)
                            ? name
                            : null;

                       EnableNext();
                   }
               });

            cboStratum.CreateControl();
            grpStratum.Controls.Add(value: cboStratum);

            // Volba selected item v ComboBox
            if (selectedItem != null)
            {
                cboStratum.SelectedItem = null;
                cboStratum.SelectedItem = selectedItem;
            }
            else
            {
                selectedItem = columns
                    .Where(a => a.Name == NONE)
                    .FirstOrDefault<DBColumn>();

                if (selectedItem != null)
                {
                    cboStratum.SelectedItem = null;
                    cboStratum.SelectedItem = selectedItem;
                }
                else if (columns.Count > 0)
                {
                    cboStratum.SelectedItem = null;
                    selectedItem = columns[0];
                    cboStratum.SelectedItem = selectedItem;
                }
            }
        }

        #endregion Methods

    }

}