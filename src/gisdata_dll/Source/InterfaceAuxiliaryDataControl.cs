﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi
{
    namespace ModuleAuxiliaryData
    {

        /// <summary>
        /// <para lang="cs">Rozhraní ovládacích prvků modulu pro pomocná data</para>
        /// <para lang="en">Interface of the controls in auxiliary data module</para>
        /// </summary>
        internal interface IAuxiliaryDataControl
        {

            #region Properties

            /// <summary>
            /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
            /// <para lang="en">File with control labels (read-only)</para>
            /// </summary>
            LanguageFile LanguageFile { get; }

            /// <summary>
            /// <para lang="cs">Nastavení modulu pro pomocná data (read-only)</para>
            /// <para lang="en">Module for auxiliary data setting (read-only)</para>
            /// </summary>
            Setting Setting { get; }

            #endregion Properties


            #region Static Methods

            /// <summary>
            /// <para lang="cs">Vyvolá vyjímku když vlastník je null nebo neimplementuje rozhraní INfiEstaControl a IAuxiliaryDataControl</para>
            /// <para lang="en">Throws exception when owner is null or does not implement interface INfiEstaControl and IAuxiliaryDataControl</para>
            /// </summary>
            /// <param name="owner">
            /// <para lang="cs">Vlastník</para>
            /// <para lang="en">Owner</para>
            /// </param>
            /// <param name="name">
            /// <para lang="cs">Jméno vlastníka</para>
            /// <para lang="en">Owner name</para>
            /// </param>
            /// <exception cref="System.ArgumentNullException">
            /// <para lang="cs">Výjímka</para>
            /// <para lang="en">Exception</para>
            /// </exception>
            static void CheckOwner(System.Windows.Forms.Control owner, string name)
            {
                if (owner == null)
                {
                    throw new System.ArgumentNullException(
                        message: $"Argument {name} must not be null.",
                        paramName: name);
                }

                if (owner is not ZaJi.NfiEstaPg.INfiEstaControl)
                {
                    throw new System.ArgumentNullException(
                        message: $"Argument {name} must be type of {nameof(ZaJi.NfiEstaPg.INfiEstaControl)}.",
                        paramName: name);
                }

                if (owner is not IAuxiliaryDataControl)
                {
                    throw new System.ArgumentNullException(
                        message: $"Argument {name} must be type of {nameof(IAuxiliaryDataControl)}.",
                        paramName: name);
                }
            }

            #endregion Static Methods

        }

    }
}
