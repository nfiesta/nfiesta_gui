﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace ZaJi
{
    namespace ModuleAuxiliaryData
    {

        /// <summary>
        /// <para lang="cs">Logovaná událost vlákna</para>
        /// <para lang="en">Logged thread event</para>
        /// </summary>
        internal class AuxLogEvent
        {

            #region Private Fields

            /// <summary>
            /// <para lang="cs">Typ logované události vlákna</para>
            /// <para lang="en">Logged thread event type</para>
            /// </summary>
            private AuxLogEventType auxLogEventType;

            /// <summary>
            /// <para lang="cs">Textová zpráva o události vlákna</para>
            /// <para lang="en">Text message about thread event</para>
            /// </summary>
            private string message;

            #endregion Private Fields


            #region Constructor

            /// <summary>
            /// <para lang="cs">Konstruktor</para>
            /// <para lang="en">Constructor</para>
            /// </summary>
            /// <param name="auxLogEventType">
            /// <para lang="cs">Typ logované události vlákna</para>
            /// <para lang="en">Logged thread event type</para>
            /// </param>
            /// <param name="message">
            /// <para lang="cs">Textová zpráva o události vlákna</para>
            /// <para lang="en">Text message about thread event</para>
            /// </param>
            public AuxLogEvent(AuxLogEventType auxLogEventType, string message)
            {
                AuxLogEventType = auxLogEventType;
                Message = message;
            }

            #endregion Constructor


            #region Properties

            /// <summary>
            /// <para lang="cs">Typ logované události vlákna</para>
            /// <para lang="en">Logged thread event type</para>
            /// </summary>
            public AuxLogEventType AuxLogEventType
            {
                get
                {
                    return auxLogEventType;
                }
                private set
                {
                    auxLogEventType = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Textová zpráva o události vlákna</para>
            /// <para lang="en">Text message about thread event</para>
            /// </summary>
            public string Message
            {
                get
                {
                    return message;
                }
                private set
                {
                    message = value;
                }
            }

            #endregion Properties


            #region Methods

            /// <summary>
            /// <para lang="cs">Vrací řetězec, který představuje aktuální objekt</para>
            /// <para lang="en">Returns a string that represents the current object</para>
            /// </summary>
            /// <returns>
            /// <para lang="cs">Řetězec, který představuje aktuální objekt</para>
            /// <para lang="en">String that represents the current object</para>
            /// </returns>
            public override string ToString()
            {
                return Message;
            }

            #endregion Methods

        }

        /// <summary>
        /// <para lang="cs">Log událostí vláken</para>
        /// <para lang="en">Log of thread events</para>
        /// </summary>
        internal class AuxLog
        {

            #region Private Fields

            /// <summary>
            /// <para lang="cs">Zámek</para>
            /// <para lang="en">Lock</para>
            /// </summary>
            private readonly object lockA;

            /// <summary>
            /// <para lang="cs">Celkový seznam všech událostí vláken</para>
            /// <para lang="en">Total list of all thread events</para>
            /// </summary>
            private List<AuxLogEvent> items;

            /// <summary>
            /// <para lang="cs">Délka textového výpisu z logu událostí vláken (počet vypisovaných řádků)</para>
            /// <para lang="en">Length of text output from the thread event log (number of lines output)</para>
            /// </summary>
            private int length;

            /// <summary>
            /// <para lang="cs">Počet dokončených segmentů výpočetních buněk</para>
            /// <para lang="en">Number of completed estimation cell segments</para>
            /// </summary>
            private int cellsFinishedCount;

            /// <summary>
            /// <para lang="cs">Počet dokončených segmentů bodové vrstvy</para>
            /// <para lang="en">Number of completed point layer segments</para>
            /// </summary>
            private int segmentsFinishedCount;

            /// <summary>
            /// <para lang="cs">Celkový počet segmentů výpočetních buněk</para>
            /// <para lang="en">Total number of estimation cell segments</para>
            /// </summary>
            private int cellsTotal;

            /// <summary>
            /// <para lang="cs">Celkový počet segmentů bodové vrstvy</para>
            /// <para lang="en">Total number of point layer segments</para>
            /// </summary>
            private int segmentsTotal;

            #endregion Private Fields


            #region Constructor

            /// <summary>
            /// <para lang="cs">Konstruktor</para>
            /// <para lang="en">Constructor</para>
            /// </summary>
            public AuxLog()
            {
                lockA = new object();

                Items = [];
                Length = -1;
                CellsFinishedCount = 0;
                SegmentsFinishedCount = 0;
                CellsTotal = 0;
                SegmentsTotal = 0;
            }

            #endregion Constructor


            #region Properties

            /// <summary>
            /// <para lang="cs">Celkový seznam všech událostí vláken</para>
            /// <para lang="en">Total list of all thread events</para>
            /// </summary>
            public List<AuxLogEvent> Items
            {
                get
                {
                    return items ?? [];
                }
                private set
                {
                    Monitor.Enter(obj: lockA);
                    try
                    {
                        items = value ?? [];
                    }
                    finally
                    {
                        Monitor.Exit(obj: lockA);
                    }
                }
            }

            /// <summary>
            /// <para lang="cs">Délka textového výpisu z logu událostí vláken (počet vypisovaných řádků)</para>
            /// <para lang="en">Length of text output from the thread event log (number of lines output)</para>
            /// </summary>
            public int Length
            {
                get
                {
                    return length;
                }
                set
                {

                    Monitor.Enter(obj: lockA);
                    try
                    {
                        length = value;
                    }
                    finally
                    {
                        Monitor.Exit(obj: lockA);
                    }
                }
            }

            /// <summary>
            /// <para lang="cs">Počet dokončených segmentů výpočetních buněk</para>
            /// <para lang="en">Number of completed estimation cell segments</para>
            /// </summary>
            public int CellsFinishedCount
            {
                get
                {
                    return cellsFinishedCount;
                }
                private set
                {
                    Monitor.Enter(obj: lockA);
                    try
                    {
                        cellsFinishedCount = value;
                    }
                    finally
                    {
                        Monitor.Exit(obj: lockA);
                    }
                }
            }

            /// <summary>
            /// <para lang="cs">Počet dokončených segmentů bodové vrstvy</para>
            /// <para lang="en">Number of completed point layer segments</para>
            /// </summary>
            public int SegmentsFinishedCount
            {
                get
                {
                    return segmentsFinishedCount;
                }
                private set
                {
                    Monitor.Enter(obj: lockA);
                    try
                    {
                        segmentsFinishedCount = value;
                    }
                    finally
                    {
                        Monitor.Exit(obj: lockA);
                    }
                }
            }

            /// <summary>
            /// <para lang="cs">Celkový počet segmentů výpočetních buněk</para>
            /// <para lang="en">Total number of estimation cell segments</para>
            /// </summary>
            public int CellsTotal
            {
                get
                {
                    return cellsTotal;
                }
                set
                {
                    Monitor.Enter(obj: lockA);
                    try
                    {
                        cellsTotal = value;
                    }
                    finally
                    {
                        Monitor.Exit(obj: lockA);
                    }
                }
            }

            /// <summary>
            /// <para lang="cs">Celkový počet segmentů bodové vrstvy</para>
            /// <para lang="en">Total number of point layer segments</para>
            /// </summary>
            public int SegmentsTotal
            {
                get
                {
                    return segmentsTotal;
                }
                set
                {
                    Monitor.Enter(lockA);
                    try
                    {
                        segmentsTotal = value;
                    }
                    finally
                    {
                        Monitor.Exit(lockA);
                    }
                }
            }

            /// <summary>
            /// <para lang="cs">Počet dokončených segmentů výpočetních buněk - procento (read-only)</para>
            /// <para lang="en">Number of completed segments of estimation cell cells - percentage (read-only)</para>
            /// </summary>
            public int CellFinishedPerc
            {
                get
                {
                    if (CellsTotal > 0)
                    {
                        int val = (int)(100.0 * ((double)CellsFinishedCount / (double)CellsTotal));
                        val = (val < 0) ? 0 : val;
                        val = (val > 100) ? 100 : val;
                        return val;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }

            /// <summary>
            /// <para lang="cs">Počet dokončených segmentů bodové vrstvy - procento (read-only)</para>
            /// <para lang="en">Number of completed segments of point layer - percentage (read-only)</para>
            /// </summary>
            public int SegmentsFinishedPerc
            {
                get
                {
                    if (SegmentsTotal > 0)
                    {
                        int val = (int)(100.0 * ((double)SegmentsFinishedCount / (double)SegmentsTotal));
                        val = (val < 0) ? 0 : val;
                        val = (val > 100) ? 100 : val;
                        return val;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }

            /// <summary>
            /// <para lang="cs">Textový výpis logu (read-only)</para>
            /// <para lang="en">Text extract from the log (read-only)</para>
            /// </summary>
            public string Text
            {
                get
                {
                    string result;

                    Monitor.Enter(obj: lockA);
                    try
                    {
                        StringBuilder builder = new(value: String.Empty);

                        if (length == 0)
                        {
                            // nulová délka -> prázdný výpis
                            result = String.Empty;
                        }
                        else
                        {
                            // záporná délka -> celý výpis
                            if (length < 0)
                            {
                                foreach (AuxLogEvent item in Items)
                                {
                                    builder.AppendLine(value: item.Message);
                                }
                                result = builder.ToString();
                            }

                            // kladná délka -> pouze poslední počet řádků
                            else
                            {
                                if (length >= Items.Count)
                                {
                                    foreach (AuxLogEvent item in Items)
                                    {
                                        builder.AppendLine(value: item.Message);
                                    }
                                    result = builder.ToString();
                                }
                                else
                                {
                                    for (int i = Items.Count - length; i < Items.Count; i++)
                                    {
                                        builder.AppendLine(value: Items[i].Message);
                                    }
                                    result = builder.ToString();
                                }
                            }
                        }
                    }
                    finally
                    {
                        Monitor.Exit(obj: lockA);
                    }

                    return result;
                }
            }

            #endregion Properties


            #region Methods

            /// <summary>
            /// <para lang="cs">Zapíše událost z vlákna pro výpočet úhrnů do logu událostí vláken</para>
            /// <para lang="en">Writes an event from the thread for calculating totals to the thread event log</para>
            /// </summary>
            /// <param name="eType">
            /// <para lang="cs">Typ logované události vlákna</para>
            /// <para lang="en">Logged thread event type</para>
            /// </param>
            /// <param name="e">
            /// <para lang="cs">Parametry události vlákna</para>
            /// <para lang="en">Thread event parameters</para>
            /// </param>
            public void AddTotalsThreadEvent(
                AuxLogEventType eType,
                TotalsThreadEventArgs e)
            {
                // Logovat se bude pouze dokončení segmentu výpočetní buňky
                if (eType == AuxLogEventType.TotalsWorkerThreadCellFinished)
                {
                    Monitor.Enter(obj: lockA);
                    try
                    {
                        items.Add(item:
                            new AuxLogEvent(
                                auxLogEventType: eType,
                                message: $"{e.EventTime} - {e.Message}"));

                        cellsFinishedCount++;
                    }
                    finally
                    {
                        Monitor.Exit(obj: lockA);
                    }

                    return;
                }

                // Logovat se musí také všechny chyby
                if (eType == AuxLogEventType.TotalsWorkerThreadCellFailed)
                {
                    Monitor.Enter(obj: lockA);
                    try
                    {
                        items.Add(item:
                            new AuxLogEvent(
                                auxLogEventType: eType,
                                message: $"{e.EventTime} - {e.Message}"));
                        items.Add(item:
                            new AuxLogEvent(
                                auxLogEventType: eType,
                                message: e.Exception.Message));
                    }
                    finally
                    {
                        Monitor.Exit(obj: lockA);
                    }

                    return;
                }
            }


            ///// <summary>
            ///// <para lang="cs">Zapíše událost z vlákna pro protínání bodové vrstvy s vrstvou úhrnů do logu událostí vláken</para>
            ///// <para lang="en">Writes an event from the thread for intersection point layer with totals layer to the thread event log</para>
            ///// </summary>
            ///// <param name="eType">
            ///// <para lang="cs">Typ logované události vlákna</para>
            ///// <para lang="en">Logged thread event type</para>
            ///// </param>
            ///// <param name="e">
            ///// <para lang="cs">Parametry události vlákna</para>
            ///// <para lang="en">Thread event parameters</para>
            ///// </param>
            //public void AddIntersectionsThreadEvent(
            //    AuxLogEventType eType,
            //    IntersectionsThreadEventArgs e)
            //{
            //    Monitor.Enter(obj: lockA);
            //    try
            //    {
            //        items.Add(item:
            //            new AuxLogEvent(
            //                auxLogEventType: eType,
            //                message: $"{e.EventTime} - {e.Message}"));

            //        if (eType == AuxLogEventType.IntersectionsWorkerThreadSegmentFinished)
            //        {
            //            segmentsFinishedCount++;
            //        }
            //    }
            //    finally
            //    {
            //        Monitor.Exit(obj: lockA);
            //    }
            //}


            ///// <summary>
            ///// <para lang="cs">Zapíše událost výjimky z vlákna pro protínání bodové vrstvy s vrstvou úhrnů do logu událostí vláken</para>
            ///// <para lang="en">Writes an event of exception from the thread for intersection point layer with totals layer to the thread event log</para>
            ///// </summary>
            ///// <param name="eType">
            ///// <para lang="cs">Typ logované události vlákna</para>
            ///// <para lang="en">Logged thread event type</para>
            ///// </param>
            ///// <param name="e">
            ///// <para lang="cs">Parametry události vlákna</para>
            ///// <para lang="en">Thread event parameters</para>
            ///// </param>
            //public void AddIntersectionsThreadException(
            //    AuxLogEventType eType,
            //    IntersectionsThreadEventArgs e)
            //{
            //    Monitor.Enter(obj: lockA);
            //    try
            //    {
            //        items.Add(item:
            //            new AuxLogEvent(
            //                auxLogEventType: eType,
            //                message: $"{e.EventTime} - {e.Message}"));
            //        items.Add(item:
            //            new AuxLogEvent(
            //                auxLogEventType: eType,
            //                message: e.Exception.Message));
            //    }
            //    finally
            //    {
            //        Monitor.Exit(obj: lockA);
            //    }
            //}

            #endregion Methods

        }

        /// <summary>
        /// <para lang="cs">Typ logované události vlákna</para>
        /// <para lang="en">Logged thread event type</para>
        /// </summary>
        internal enum AuxLogEventType
        {

            #region TotalsWorkerThread

            /// <summary>
            /// <para lang="cs">
            /// Událost
            /// ZAHÁJENÍ výpočtu úhrnu pomocné proměnné pro VŠECHNY segmenty výpočetních buněk
            /// v pracovním vlákně
            /// </para>
            /// <para lang="en">
            /// Event
            /// BEGIN calculation of the auxiliary variable totals for ALL segments of the estimation cells
            /// in the worker thread
            /// </para>
            /// </summary>
            TotalsWorkerThreadStarted = 101,

            /// <summary>
            /// <para lang="cs">
            /// Událost
            /// ZASTAVENÍ výpočtu úhrnu pomocné proměnné pro VŠECHNY segmenty výpočetních buněk
            /// v pracovním vlákně
            /// </para>
            /// <para lang="en">
            /// Event
            /// STOP calculation of the auxiliary variable totals for ALL segments of the estimation cells
            /// in the worker thread
            /// </para>
            /// </summary>
            TotalsWorkerThreadStopped = 102,

            /// <summary>
            /// <para lang="cs">
            /// Událost
            /// DOKONČENÍ výpočtu úhrnu pomocné proměnné pro VŠECHNY segmenty výpočetních buněk
            /// v pracovním vlákně
            /// </para>
            /// <para lang="en">
            /// Event
            /// COMPLETION of the calculation of the auxiliary variable totals for ALL segments of the estimation cells
            /// in the worker thread
            /// </para>
            /// </summary>
            TotalsWorkerThreadFinished = 103,

            /// <summary>
            /// <para lang="cs">
            /// Událost
            /// ZAHÁJENÍ výpočtu úhrnu pomocné proměnné pro JEDEN segment výpočetní buňky
            /// v pracovním vlákně
            /// </para>
            /// <para lang="en">
            /// Event
            /// BEGIN calculation of the auxiliary variable totals for ONE segment of the estimation cell
            /// in the worker thread
            /// </para>
            /// </summary>
            TotalsWorkerThreadCellStarted = 104,

            /// <summary>
            /// <para lang="cs">
            /// Událost
            /// DOKONČENÍ výpočtu úhrnu pomocné proměnné pro JEDEN segment výpočetní buňky
            /// v pracovním vlákně
            /// </para>
            /// <para lang="en">
            /// Event
            /// COMPLETION calculation of the auxiliary variable totals for ONE segment of the estimation cell
            /// in the worker thread
            /// </para>
            /// </summary>
            TotalsWorkerThreadCellFinished = 105,

            /// <summary>
            /// <para lang="cs">
            /// Událost
            /// CHYBA při výpočtu úhrnu pomocné proměnné pro JEDEN segment výpočetní buňky
            /// v pracovním vlákně
            /// </para>
            /// <para lang="en">
            /// Event
            /// ERROR in calculation of the auxiliary variable totals for ONE segment of the estimation cell
            /// in the worker thread
            /// </para>
            /// </summary>
            TotalsWorkerThreadCellFailed = 106,

            #endregion TotalsWorkerThread


            #region TotalsControlThread

            /// <summary>
            /// <para lang="cs">
            /// Událost
            /// ZAHÁJENÍ činnosti řídícího vlákna pro výpočet úhrnů pomocných proměnných
            /// </para>
            /// <para lang="en">
            /// Event
            /// STARTING the control thread for calculating auxiliary variable totals
            /// </para>
            /// </summary>
            TotalsControlThreadStarted = 201,

            /// <summary>
            /// <para lang="cs">
            /// Událost
            /// ZASTAVENÍ činnosti řídícího vlákna pro výpočet úhrnů pomocných proměnných
            /// </para>
            /// <para lang="en">
            /// Event
            /// STOP the control thread for calculating auxiliary variable totals
            /// </para>
            /// </summary>
            TotalsControlThreadStopped = 202,

            /// <summary>
            /// <para lang="cs">
            /// Událost
            /// DOKONČENÍ činnosti řídícího vlákna pro výpočet úhrnů pomocných proměnných
            /// </para>
            /// <para lang="en">
            /// Event
            /// COMPLETION the control thread for calculating auxiliary variable totals
            /// </para>
            /// </summary>
            TotalsControlThreadFinished = 203,

            /// <summary>
            /// <para lang="cs">
            /// Událost
            /// ZAHÁJENÍ výpočtu úhrnu pomocné proměnné pro JEDNU KONFIGURACI
            /// v řídícím vlákně
            /// </para>
            /// <para lang="en">
            /// Event
            /// BEGIN calculation of the auxiliary variable totals for ONE CONFIGURATION
            /// in the control thread
            /// </para>
            /// </summary>
            TotalsControlThreadConfigurationStarted = 204,

            /// <summary>
            /// <para lang="cs">
            /// Událost
            /// DOKONČENÍ výpočtu úhrnu pomocné proměnné pro JEDNU KONFIGURACI
            /// v řídícím vlákně
            /// </para>
            /// <para lang="en">
            /// Event
            /// COMPLETION calculation of the auxiliary variable totals for ONE CONFIGURATION
            /// in the control thread
            /// </para>
            /// </summary>
            TotalsControlThreadConfigurationFinished = 205,

            /// <summary>
            /// <para lang="cs">
            /// Událost
            /// ZAHÁJENÍ výpočtu úhrnu pomocné proměnné pro JEDEN KROK
            /// v řídícím vlákně
            /// </para>
            /// <para lang="en">
            /// Event
            /// BEGIN calculation of the auxiliary variable totals for ONE STEP
            /// in the control thread
            /// </para>
            /// </summary>
            TotalsControlThreadStepStarted = 206,

            /// <summary>
            /// <para lang="cs">
            /// Událost
            /// DOKONČENÍ výpočtu úhrnu pomocné proměnné pro JEDEN KROK
            /// v řídícím vlákně
            /// </para>
            /// <para lang="en">
            /// Event
            /// COMPLETION calculation of the auxiliary variable totals for ONE STEP
            /// in the control thread
            /// </para>
            /// </summary>
            TotalsControlThreadStepFinished = 207,

            #endregion TotalsControlThread


            // Pracovni vlakno - protinani bodove vrstvy s vrstvou uhrnu pomocne promenne

            /// <summary>
            /// ZAHAJENI protinani bodove vrstvy s vrstvou uhrnu pomocne promenne
            /// v pracovnim vlakne
            /// </summary>
            IntersectionsWorkerThreadStarted = 301,

            /// <summary>
            /// ZASTAVENI protinani bodove vrstvy s vrstvou uhrnu pomocne promenne
            /// v pracovnim vlakne
            /// </summary>
            IntersectionsWorkerThreadStopped = 302,

            /// <summary>
            /// DOKONCENI protinani bodove vrstvy s vrstvou uhrnu pomocne promenne
            /// v pracovnim vlakne
            /// </summary>
            IntersectionsWorkerThreadFinished = 303,

            /// <summary>
            /// ZAHAJENI protinani bodove vrstvy s vrstvou uhrnu pomocne promenne
            /// pro jednu SKUPINU BODU
            /// v pracovnim vlakne
            /// </summary>
            IntersectionsWorkerThreadSegmentStarted = 304,

            /// <summary>
            /// DOKONCENI protinani bodove vrstvy s vrstvou uhrnu pomocne promenne
            /// pro jednu SKUPINU BODU
            /// </summary>
            IntersectionsWorkerThreadSegmentFinished = 305,

            /// <summary>
            /// VZNIK CHYBY pri protinani bodove vrstvy s vrstvou uhrnu pomocne promenne
            /// pro jednu SKUPINU BODU
            /// </summary>
            IntersectionsWorkerThreadSegmentFailed = 306,

            // Ridici vlakno - protinani bodove vrstvy s vrstvou uhrnu pomocne promenne

            /// <summary>
            /// ZAHAJENI prace ridiciho vlakna
            /// pro protinani bodove vrstvy s vrstvou uhrnu pomocne promenne
            /// </summary>
            IntersectionsControlThreadStarted = 401,

            /// <summary>
            /// ZASTAVENI prace ridiciho vlakna
            /// pro protinani bodove vrstvy s vrstvou uhrnu pomocne promenne
            /// </summary>
            IntersectionsControlThreadStopped = 402,

            /// <summary>
            /// DOKONCENI prace ridiciho vlakna
            /// pro protinani bodove vrstvy s vrstvou uhrnu pomocne promenne
            /// </summary>
            IntersectionsControlThreadFinished = 403,

            /// <summary>
            /// ZAHAJENI protinani bodove vrstvy s vrstvou uhrnu pomocne promenne
            /// pro jeden KROK VYPOCTU
            /// </summary>
            IntersectionsControlThreadStepStarted = 404,

            /// <summary>
            /// DOKONCENI protinani bodove vrstvy s vrstvou uhrnu pomocne promenne
            /// pro jeden KROK VYPOCTU
            /// </summary>
            IntersectionsControlThreadStepFinished = 405,

        }

    }
}