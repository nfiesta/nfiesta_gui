﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi.ModuleAuxiliaryData
{

    partial class ControlAuxiliaryData
    {
        /// <summary> 
        /// Vyžaduje se proměnná návrháře.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Uvolněte všechny používané prostředky.
        /// </summary>
        /// <param name="disposing">hodnota true, když by se měl spravovaný prostředek odstranit; jinak false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kód vygenerovaný pomocí Návrháře komponent

        /// <summary> 
        /// Metoda vyžadovaná pro podporu Návrháře - neupravovat
        /// obsah této metody v editoru kódu.
        /// </summary>
        private void InitializeComponent()
        {
            pnlMain = new System.Windows.Forms.Panel();
            pnlControls = new System.Windows.Forms.Panel();
            ssrMain = new System.Windows.Forms.StatusStrip();
            lblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            lblAuxiliaryDataExtension = new System.Windows.Forms.ToolStripStatusLabel();
            lblNfiestaExtension = new System.Windows.Forms.ToolStripStatusLabel();
            mnsMain = new System.Windows.Forms.MenuStrip();
            tsmiDatabase = new System.Windows.Forms.ToolStripMenuItem();
            tsmiConnect = new System.Windows.Forms.ToolStripMenuItem();
            tsmiSeparator = new System.Windows.Forms.ToolStripSeparator();
            tsmiAuxiliaryDataExtension = new System.Windows.Forms.ToolStripMenuItem();
            tsmiNfiEstaExtension = new System.Windows.Forms.ToolStripMenuItem();
            pnlMain.SuspendLayout();
            ssrMain.SuspendLayout();
            mnsMain.SuspendLayout();
            SuspendLayout();
            // 
            // pnlMain
            // 
            pnlMain.BackColor = System.Drawing.Color.WhiteSmoke;
            pnlMain.Controls.Add(pnlControls);
            pnlMain.Controls.Add(ssrMain);
            pnlMain.Controls.Add(mnsMain);
            pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlMain.Location = new System.Drawing.Point(2, 2);
            pnlMain.Margin = new System.Windows.Forms.Padding(0);
            pnlMain.Name = "pnlMain";
            pnlMain.Size = new System.Drawing.Size(956, 536);
            pnlMain.TabIndex = 4;
            // 
            // pnlControls
            // 
            pnlControls.BackColor = System.Drawing.Color.Transparent;
            pnlControls.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlControls.Location = new System.Drawing.Point(0, 24);
            pnlControls.Margin = new System.Windows.Forms.Padding(0);
            pnlControls.Name = "pnlControls";
            pnlControls.Size = new System.Drawing.Size(956, 437);
            pnlControls.TabIndex = 4;
            // 
            // ssrMain
            // 
            ssrMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { lblStatus, lblAuxiliaryDataExtension, lblNfiestaExtension });
            ssrMain.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.VerticalStackWithOverflow;
            ssrMain.Location = new System.Drawing.Point(0, 461);
            ssrMain.Name = "ssrMain";
            ssrMain.Padding = new System.Windows.Forms.Padding(1, 3, 1, 25);
            ssrMain.Size = new System.Drawing.Size(956, 75);
            ssrMain.TabIndex = 2;
            ssrMain.Text = "StatusMain";
            // 
            // lblStatus
            // 
            lblStatus.BackColor = System.Drawing.Color.Transparent;
            lblStatus.Margin = new System.Windows.Forms.Padding(0);
            lblStatus.Name = "lblStatus";
            lblStatus.Size = new System.Drawing.Size(953, 15);
            lblStatus.Text = "lblStatus";
            // 
            // lblAuxiliaryDataExtension
            // 
            lblAuxiliaryDataExtension.BackColor = System.Drawing.Color.Transparent;
            lblAuxiliaryDataExtension.Margin = new System.Windows.Forms.Padding(0);
            lblAuxiliaryDataExtension.Name = "lblAuxiliaryDataExtension";
            lblAuxiliaryDataExtension.Size = new System.Drawing.Size(953, 15);
            lblAuxiliaryDataExtension.Text = "lblAuxiliaryDataExtension";
            // 
            // lblNfiestaExtension
            // 
            lblNfiestaExtension.BackColor = System.Drawing.Color.Transparent;
            lblNfiestaExtension.Margin = new System.Windows.Forms.Padding(0);
            lblNfiestaExtension.Name = "lblNfiestaExtension";
            lblNfiestaExtension.Size = new System.Drawing.Size(953, 15);
            lblNfiestaExtension.Text = "lblNfiestaExtension";
            // 
            // mnsMain
            // 
            mnsMain.BackColor = System.Drawing.Color.Transparent;
            mnsMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { tsmiDatabase });
            mnsMain.Location = new System.Drawing.Point(0, 0);
            mnsMain.Name = "mnsMain";
            mnsMain.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            mnsMain.Size = new System.Drawing.Size(956, 24);
            mnsMain.TabIndex = 1;
            mnsMain.Text = "MenuMain";
            // 
            // tsmiDatabase
            // 
            tsmiDatabase.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] { tsmiConnect, tsmiSeparator, tsmiAuxiliaryDataExtension, tsmiNfiEstaExtension });
            tsmiDatabase.Name = "tsmiDatabase";
            tsmiDatabase.Padding = new System.Windows.Forms.Padding(0);
            tsmiDatabase.Size = new System.Drawing.Size(82, 24);
            tsmiDatabase.Text = "tsmiDatabase";
            // 
            // tsmiConnect
            // 
            tsmiConnect.BackColor = System.Drawing.Color.Transparent;
            tsmiConnect.ForeColor = System.Drawing.Color.Black;
            tsmiConnect.Name = "tsmiConnect";
            tsmiConnect.Padding = new System.Windows.Forms.Padding(0);
            tsmiConnect.Size = new System.Drawing.Size(218, 20);
            tsmiConnect.Text = "tsmiConnect";
            // 
            // tsmiSeparator
            // 
            tsmiSeparator.BackColor = System.Drawing.Color.Transparent;
            tsmiSeparator.ForeColor = System.Drawing.Color.Gray;
            tsmiSeparator.Name = "tsmiSeparator";
            tsmiSeparator.Size = new System.Drawing.Size(215, 6);
            tsmiSeparator.Visible = false;
            // 
            // tsmiAuxiliaryDataExtension
            // 
            tsmiAuxiliaryDataExtension.BackColor = System.Drawing.Color.Transparent;
            tsmiAuxiliaryDataExtension.ForeColor = System.Drawing.Color.Black;
            tsmiAuxiliaryDataExtension.Name = "tsmiAuxiliaryDataExtension";
            tsmiAuxiliaryDataExtension.Padding = new System.Windows.Forms.Padding(0);
            tsmiAuxiliaryDataExtension.Size = new System.Drawing.Size(218, 20);
            tsmiAuxiliaryDataExtension.Text = "tsmiAuxiliaryDataExtension";
            tsmiAuxiliaryDataExtension.Visible = false;
            // 
            // tsmiNfiEstaExtension
            // 
            tsmiNfiEstaExtension.BackColor = System.Drawing.Color.Transparent;
            tsmiNfiEstaExtension.ForeColor = System.Drawing.Color.Black;
            tsmiNfiEstaExtension.Name = "tsmiNfiEstaExtension";
            tsmiNfiEstaExtension.Padding = new System.Windows.Forms.Padding(0);
            tsmiNfiEstaExtension.Size = new System.Drawing.Size(218, 20);
            tsmiNfiEstaExtension.Text = "tsmiNfiEstaExtension";
            // 
            // ControlAuxiliaryData
            // 
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            BackColor = System.Drawing.Color.DarkSeaGreen;
            Controls.Add(pnlMain);
            ForeColor = System.Drawing.Color.Black;
            Margin = new System.Windows.Forms.Padding(0);
            Name = "ControlAuxiliaryData";
            Padding = new System.Windows.Forms.Padding(2);
            Size = new System.Drawing.Size(960, 540);
            pnlMain.ResumeLayout(false);
            pnlMain.PerformLayout();
            ssrMain.ResumeLayout(false);
            ssrMain.PerformLayout();
            mnsMain.ResumeLayout(false);
            mnsMain.PerformLayout();
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.StatusStrip ssrMain;
        private System.Windows.Forms.MenuStrip mnsMain;
        private System.Windows.Forms.ToolStripMenuItem tsmiDatabase;
        private System.Windows.Forms.ToolStripMenuItem tsmiConnect;
        private System.Windows.Forms.Panel pnlControls;
        private System.Windows.Forms.ToolStripMenuItem tsmiAuxiliaryDataExtension;
        private System.Windows.Forms.ToolStripSeparator tsmiSeparator;

        /// <summary>
        /// Database connection status
        /// </summary>
        public System.Windows.Forms.ToolStripStatusLabel lblStatus;

        /// <summary>
        /// Version of the nfiesta_target_data database extension
        /// </summary>
        public System.Windows.Forms.ToolStripStatusLabel lblAuxiliaryDataExtension;

        /// <summary>
        /// Version of the nfiesta_pg database extension
        /// </summary>
        public System.Windows.Forms.ToolStripStatusLabel lblNfiestaExtension;
        private System.Windows.Forms.ToolStripMenuItem tsmiNfiEstaExtension;
    }

}
