﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.Catalog;

namespace ZaJi.ModuleAuxiliaryData
{

    /// <summary>
    /// <para lang="cs">Formulář pro výběr ze všech sloupců tabulky nebo sloupců cizí tabulky</para>
    /// <para lang="en">Form for selection from all table columns or foreign table columns</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    internal partial class FormTableColumns
        : Form, INfiEstaControl, IAuxiliaryDataControl
    {

        #region Constants

        /// <summary>
        /// <para lang="cs">Výška zaškrtávacího políčka</para>
        /// <para lang="en">CheckBox height</para>
        /// </summary>
        private const int checkBoxHeight = 25;

        #endregion Constants


        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Databázová tabulka</para>
        /// <para lang="en">Database table</para>
        /// </summary>
        private DBTable table;

        /// <summary>
        /// <para lang="cs">Cizí databázová tabulka</para>
        /// <para lang="en">Foreign database table</para>
        /// </summary>
        private DBForeignTable foreignTable;

        #endregion Private Fields


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor ovládacího prvku</para>
        /// <para lang="en">Control constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        public FormTableColumns(Control controlOwner)
        {
            InitializeComponent();
            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                IAuxiliaryDataControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                return controlOwner;
            }
            set
            {
                IAuxiliaryDataControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables (read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((IAuxiliaryDataControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro pomocná data (read-only)</para>
        /// <para lang="en">Module for auxiliary data setting (read-only)</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((IAuxiliaryDataControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">Databázová tabulka</para>
        /// <para lang="en">Database table</para>
        /// </summary>
        public DBTable Table
        {
            get
            {
                return table;
            }
            set
            {
                table = value;
                foreignTable = null;

                InitializeLabels();
                LoadContent();
            }
        }

        /// <summary>
        /// <para lang="cs">Cizí databázová tabulka</para>
        /// <para lang="en">Foreign database table</para>
        /// </summary>
        public DBForeignTable ForeignTable
        {
            get
            {
                return foreignTable;
            }
            set
            {
                foreignTable = value;
                table = null;

                InitializeLabels();
                LoadContent();
            }
        }

        /// <summary>
        /// <para lang="cs">Seznam skrytých sloupců databázové tabulky</para>
        /// <para lang="en">List of hidden database columns</para>
        /// </summary>
        public List<DBColumn> ExcludedColumns
        {
            get
            {
                return
                    pnlColumns.Controls.OfType<CheckBox>()
                    .Where(a => !a.Visible)
                    .Where(a => a.Tag != null)
                    .Where(a => a.Tag is DBColumn)
                    .Select(a => (DBColumn)a.Tag)
                    .ToList<DBColumn>();
            }
            set
            {
                foreach (CheckBox checkBox in
                     pnlColumns.Controls.OfType<CheckBox>()
                        .Where(a => a.Tag != null)
                        .Where(a => a.Tag is DBColumn))
                {
                    checkBox.Visible = !value
                        .Select(a => a.Name)
                        .Contains(value: ((DBColumn)checkBox.Tag).Name);
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Seznam vybraných databázových sloupců (read-only)</para>
        /// <para lang="en">List of selected database columns (read-only)</para>
        /// </summary>
        public List<DBColumn> SelectedColumns
        {
            get
            {
                return
                    pnlColumns.Controls.OfType<CheckBox>()
                    .Where(a => a.Checked)
                    .Where(a => a.Tag != null)
                    .Where(a => a.Tag is DBColumn)
                    .Select(a => (DBColumn)a.Tag)
                    .ToList<DBColumn>();
            }
        }

        #endregion  Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(FormTableColumns),                 "Výběr ze všech sloupců databázové tabulky" },
                        { $"{nameof(FormTableColumns)}Foreign",     "Výběr ze všech sloupců cizí databázové tabulky" },
                        { nameof(btnOK),                            "OK" },
                        { nameof(btnCancel),                        "Zrušit" },
                        { nameof(grpColumns),                       "Sloupce tabulky:" },
                        { $"{nameof(grpColumns)}Foreign",           "Sloupce cizí tabulky:" }
                    }
                    : languageFile.NationalVersion.Data.TryGetValue(
                        key: nameof(FormTableColumns),
                        out Dictionary<string, string> dictNational)
                            ? dictNational
                            : [],

                LanguageVersion.International => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(FormTableColumns),                 "Selection from all columns of database table" },
                        { $"{nameof(FormTableColumns)}Foreign",     "Selection from all columns of foreign database table" },
                        { nameof(btnOK),                            "OK" },
                        { nameof(btnCancel),                        "Cancel" },
                        { nameof(grpColumns),                       "Table columns:" },
                        { $"{nameof(grpColumns)}Foreign",           "Foreign table columns:" }
                    }
                    : languageFile.InternationalVersion.Data.TryGetValue(
                        key: nameof(FormTableColumns),
                        out Dictionary<string, string> dictInternational)
                            ? dictInternational
                            : [],

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku</para>
        /// <para lang="en">Initialization of the control</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        private void Initialize(Control controlOwner)
        {
            ControlOwner = controlOwner;
            Table = null;
            ForeignTable = null;

            btnOK.Click += new EventHandler(
                (sender, e) =>
                {
                    DialogResult = DialogResult.OK;
                    Close();
                });

            btnCancel.Click += new EventHandler(
               (sender, e) =>
               {
                   DialogResult = DialogResult.Cancel;
                   Close();
               });
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků ovládacího prvku</para>
        /// <para lang="en">Initialization of control labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            Text =
                (ForeignTable == null)
                    ? labels.TryGetValue(key: nameof(FormTableColumns),
                        out string frmTableColumnsText)
                            ? frmTableColumnsText
                            : String.Empty
                    : labels.TryGetValue(key: $"{nameof(FormTableColumns)}Foreign",
                        out string frmForeignTableColumnsText)
                            ? frmForeignTableColumnsText
                            : String.Empty;

            grpColumns.Text =
                (ForeignTable == null)
                    ? labels.TryGetValue(key: nameof(grpColumns),
                        out string grpColumnsText)
                            ? grpColumnsText
                            : String.Empty
                    : labels.TryGetValue(key: $"{nameof(grpColumns)}Foreign",
                        out string grpForeignColumnsText)
                            ? grpForeignColumnsText
                            : String.Empty;

            btnOK.Text =
                labels.TryGetValue(key: nameof(btnOK),
                out string btnOKText)
                    ? btnOKText
                    : String.Empty;

            btnCancel.Text =
                labels.TryGetValue(key: nameof(btnCancel),
                out string btnCancelText)
                    ? btnCancelText
                    : String.Empty;
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent()
        {
            pnlColumns.Controls.Clear();

            // Seznam databázových sloupců tabulky
            if (Table != null)
            {
                foreach (DBColumn column in Table.Columns.Items)
                {
                    AddCheckBox(column: column);
                }
            }

            // Seznam databázových sloupců cizí tabulky
            else
            {
                if (ForeignTable != null)
                {
                    foreach (DBColumn column in ForeignTable.Columns.Items)
                    {
                        AddCheckBox(column: column);
                    }
                }
            }

            OrderCheckBoxes();
        }

        /// <summary>
        /// <para lang="cs">Přidá zaškrtávací políčko pro databázový sloupec do seznamu</para>
        /// <para lang="en">Adds check box for database column in the list</para>
        /// </summary>
        /// <param name="column">
        /// <para lang="cs">Databázový sloupec</para>
        /// <para lang="en">Database column</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Zaškrtávací políčko pro databázový sloupec do seznamu</para>
        /// <para lang="en">Check box for database column in the list</para>
        /// </returns>
        private CheckBox AddCheckBox(DBColumn column)
        {
            if (
                pnlColumns.Controls.OfType<CheckBox>()
                .Where(a => a.Tag != null)
                .Where(a => a.Tag is DBColumn)
                .Where(a => ((DBColumn)a.Tag).Name == column.Name)
                .Any())
            {
                // Databázový sloupec se v seznamu již nachází
                return
                    pnlColumns.Controls.OfType<CheckBox>()
                        .Where(a => a.Tag != null)
                        .Where(a => a.Tag is DBColumn)
                        .Where(a => ((DBColumn)a.Tag).Name == column.Name)
                        .FirstOrDefault<CheckBox>();
            }

            CheckBox checkBox = new()
            {
                Checked = false,
                Dock = DockStyle.Top,
                Font = Setting.CheckBoxFont,
                ForeColor = Setting.CheckBoxForeColor,
                Height = checkBoxHeight,
                Padding = new Padding(left: 10, top: 0, right: 0, bottom: 0),
                Tag = column,
                Text = $"{column.Name ?? String.Empty}",
            };

            checkBox.CreateControl();

            pnlColumns.Controls.Add(value: checkBox);

            return checkBox;
        }

        /// <summary>
        /// <para lang="cs">Seřazení zaškrtávacích políček pro databázové sloupce</para>
        /// <para lang="en">Sorting checkboxes for database columns</para>
        /// </summary>
        private void OrderCheckBoxes()
        {
            foreach (var pair in
                pnlColumns.Controls.OfType<CheckBox>()
                .Where(a => a.Tag != null)
                .Where(a => a.Tag is DBColumn)
                .OrderByDescending(a => ((DBColumn)a.Tag).Name) // řadit abecedně podle jména sloupce
                .Select((a, i) => new { CheckBox = a, Index = i }))
            {
                pnlColumns.Controls.SetChildIndex(
                    child: pair.CheckBox,
                    newIndex: pair.Index);
            }
        }

        #endregion Methods

    }

}
