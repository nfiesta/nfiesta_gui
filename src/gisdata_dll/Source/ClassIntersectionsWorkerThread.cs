﻿////
//// Copyright 2020, 2024 ÚHÚL
////
//// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
//// You may not use this work except in compliance with the Licence.
//// You may obtain a copy of the Licence at:
////
//// https://joinup.ec.europa.eu/software/page/eupl
////
//// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
//// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//// See the Licence for the specific language governing permissions and limitations under the Licence.
////

//using Npgsql;
//using System;
//using System.Data;
//using System.Threading;
//using ZaJi.PostgreSQL;

//namespace ZaJi
//{
//    namespace ModuleAuxiliaryData
//    {

//        /// <summary>
//        /// Pracovni vlakno
//        /// pro protinani bodove vrstvy s vrstvou uhrnu pomocne promenne
//        /// </summary>
//        internal class IntersectionsWorkerThread
//        {

//            #region Private Fields

//            /// <summary>
//            /// Identifikacni cislo pracovniho vlakna
//            /// pro protinani bodove vrstvy s vrstvou uhrnu pomocne promenne
//            /// </summary>
//            private readonly int id;

//            /// <summary>
//            /// Ridici vlakno
//            /// pro protinani bodove vrstvy s vrstvou uhrnu pomocne promenne
//            /// </summary>
//            private readonly IntersectionsControlThread controlThread;

//            /// <summary>
//            /// Skupiny bodu
//            /// pro protinani s vrstvou uhrnu pomocne promenne
//            /// resenych jednim vlaknem v jednom kroku
//            /// </summary>
//            private readonly FnConfigForAuxDataList segments;

//            /// <summary>
//            /// Pracovni vlakno je v cinnosti
//            /// </summary>
//            private bool isWorking;

//            /// <summary>
//            /// Zastavit vlakno pred dokoncenim ukolu
//            /// </summary>
//            private bool stop;

//            /// <summary>
//            /// Ukazatel na funkci spoustenou pracovnim vlaknem
//            /// </summary>
//            private readonly ParameterizedThreadStart refThreadFunc;

//            /// <summary>
//            /// Objekt pracovniho vlakna
//            /// </summary>
//            private readonly Thread workerThread;

//            #endregion Private Fields


//            #region Constructor

//            /// <summary>
//            /// Konstruktor objektu pracovniho vlakna pro protinani bodove vrstvy s vrstvou uhrnu pomocne promenne
//            /// </summary>
//            /// <param name="id">Identifikacni cislo vlakna pro protinani bodove vrstvy s vrstvou uhrnu pomocne promenne</param>
//            /// <param name="controlThread">Ridici vlakno pro protinani bodove vrstvy s vrstvou uhrnu pomocne promenne</param>
//            /// <param name="segments">Skupiny bodu pro protinani s vrstvou uhrnu</param>
//            public IntersectionsWorkerThread(
//                int id,
//                IntersectionsControlThread controlThread,
//                FnConfigForAuxDataList segments)
//            {
//                this.id = id;
//                this.controlThread = controlThread;
//                this.segments = segments;

//                this.isWorking = false;
//                this.stop = false;

//                this.refThreadFunc = new ParameterizedThreadStart(BackgroundTask);
//                this.workerThread = new Thread(refThreadFunc) { IsBackground = true };
//            }

//            #endregion Constructor


//            #region Properties

//            /// <summary>
//            /// Identifikacni cislo vlakna
//            /// pro protinani bodove vrstvy s vrstvou uhrnu pomocne promenne (read-only)
//            /// </summary>
//            public int Id
//            {
//                get
//                {
//                    return id;
//                }
//            }

//            /// <summary>
//            /// Ridici vlakno
//            /// pro protinani bodove vrstvy s vrstvou uhrnu pomocne promenne (read-only)
//            /// </summary>
//            public IntersectionsControlThread ControlThread
//            {
//                get
//                {
//                    return controlThread;
//                }
//            }

//            /// <summary>
//            /// Skupiny bodu
//            /// pro protinani s vrstvou uhrnu
//            /// resenych jednim vlaknem v jednom kroku (read-only)
//            /// </summary>
//            public FnConfigForAuxDataList Segments
//            {
//                get
//                {
//                    return segments;
//                }
//            }

//            /// <summary>
//            /// Pracovni vlakno je v cinnosti
//            /// </summary>
//            public bool IsWorking
//            {
//                get
//                {
//                    return isWorking;
//                }
//            }

//            #endregion Properties


//            #region Methods

//            /// <summary>
//            /// Spusteni vlakna
//            /// </summary>
//            /// <param name="obj">Objekt s parametry, ktere se predavaji dovnitr vlakna</param>
//            public void Start(object obj)
//            {
//                workerThread.Start(obj);
//            }

//            /// <summary>
//            /// Zastaveni vlakna
//            /// </summary>
//            public void Stop()
//            {
//                stop = true;
//            }

//            /// <summary>
//            /// Ceka na dokonceni vlakna
//            /// </summary>
//            public void Join()
//            {
//                workerThread.Join();
//            }

//            /// <summary>
//            /// Ukonceni vlakna
//            /// </summary>
//            public void Abort()
//            {
//                // TODO Abort
//                // workerThread.Abort();
//            }

//            /// <summary>
//            /// Ukol pracovniho vlakna
//            /// </summary>
//            /// <param name="obj">Objekt s parametry, ktere se predavaji dovnitr vlakna</param>
//            private void BackgroundTask(object obj)
//            {
//                // Vyvola udalost zahajeni pracovniho vlakna
//                RaiseWorkerThreadStarted();

//                NpgsqlConnection connection = new NpgsqlConnection(controlThread.ModuleDatabase.Postgres.ConnectionString);
//                NpgsqlTransaction transaction = null;
//                NpgsqlCommand command;
//                string sql;

//                // Protinani probiha pro kazdou skupinu bodu,
//                // ktera byla pracovnimu vlaknu pridelena
//                foreach (FnConfigForAuxData segment in segments.Items.Values)
//                {
//                    // Vyvola udalost zahajeni protinani pro jednu skupinu bodu
//                    RaiseSegmentStarted(segment);

//                    try
//                    {
//                        // Pri prvnim spusteni connection neni otevrene a tak se otevre
//                        // a zustava otevrene pro vsechny dalsi pruchody cyklem
//                        if (connection.State != ConnectionState.Open) { connection.Open(); }

//                        transaction = connection.BeginTransaction();

//                        // Sestavi prikaz pro vypocet protinani a zapis vysledku do tabulky t_auxiliary_data
//                        sql = String.Concat(
//                            $"INSERT INTO {ModuleSettings.GisDataSchemaName}.{AuxDataList.TAuxData}{Environment.NewLine}",
//                            $"({AuxDataList.ColConfigCollection},{Environment.NewLine}",
//                            $"{AuxDataList.ColConfig},{Environment.NewLine}",
//                            $"{AuxDataList.ColIdentPoint},{Environment.NewLine}",
//                            $"{AuxDataList.ColAuxValue},{Environment.NewLine}",
//                            $"{AuxDataList.ColExtVersion},{Environment.NewLine}",
//                            $"{AuxDataList.ColGuiVersion},{Environment.NewLine}",
//                            $"{AuxDataList.ColGid},{Environment.NewLine}",
//                            $"{AuxDataList.ColEstDate}){Environment.NewLine}",
//                            $"SELECT{Environment.NewLine}",
//                            $"    {AuxDataList.ColConfigCollection} AS {AuxDataList.ColConfigCollection},{Environment.NewLine}",
//                            $"    {AuxDataList.ColConfig} AS {AuxDataList.ColConfig},{Environment.NewLine}",
//                            $"    NULL::integer AS {AuxDataList.ColIdentPoint},{Environment.NewLine}",
//                            $"    {AuxDataList.ColAuxValue} AS {AuxDataList.ColAuxValue},{Environment.NewLine}",
//                            $"    {AuxDataList.ColExtVersion} AS {AuxDataList.ColExtVersion},{Environment.NewLine}",
//                            $"    {controlThread.ModuleDatabase.GisData.GuiVersions.CurrentVersion.Id} AS {AuxDataList.ColGuiVersion},{Environment.NewLine}",
//                            $"    {AuxDataList.ColGid} AS {AuxDataList.ColGid},{Environment.NewLine}",
//                            $"    now() AS {AuxDataList.ColEstDate}{Environment.NewLine}",
//                            $"FROM{Environment.NewLine}",
//                            $"    {ModuleSettings.GisDataSchemaName}.{FnGetAuxDataApp.FGetAuxTotalApp}({Environment.NewLine}",
//                            $"        {Functions.PrepIntArg(segment.ConfigCollectionId)},{Environment.NewLine}",
//                            $"        {Functions.PrepIntArg(segment.ConfigId)},{Environment.NewLine}",
//                            $"        {Functions.PrepBoolArg(segment.Intersects)},{Environment.NewLine}",
//                            $"        {Functions.PrepIntArg(segment.GidStart)},{Environment.NewLine}",
//                            $"        {Functions.PrepIntArg(segment.GidEnd)});{Environment.NewLine}");

//                        command = new NpgsqlCommand()
//                        {
//                            Connection = connection,
//                            Transaction = transaction,
//                            CommandText = sql
//                        };

//                        command.ExecuteNonQuery();

//                        // Zapis transakce do databaze
//                        transaction.Commit();

//                        // Vyvola udalost dokonceni protinani pro jednu skupinu bodu
//                        RaiseSegmentFinished(segment);
//                    }
//                    catch (Exception exception)
//                    {
//                        // Zruseni nedokoncene transakce
//                        transaction?.Rollback();

//                        // Pokud neco selhalo vyvola udalost
//                        // a preda objekt vyjimky:
//                        RaiseSegmentFailed(segment, exception);
//                    }

//                    // Ukonci cyklus v pripade, ze je pozadovano predcasne zastaveni protinani
//                    if (stop) { break; };
//                }

//                // Uklid - zavreni pripojeni a uvolneni pameti po pripojeni
//                connection.Close();
//                connection.Dispose();

//                if (stop)
//                {
//                    // Vyvola udalost o preruseni vlakna
//                    RaiseWorkerThreadStopped();
//                }
//                else
//                {
//                    // Vyvola udalost o dokonceni protinani bodove vrstvy s vrstvou uhrnu
//                    RaiseWorkerThreadFinished();
//                }
//            }

//            /// <summary>
//            /// Textovy popis objektu pracovniho vlakna
//            /// </summary>
//            /// <returns>Textovy popis objektu pracovniho vlakna</returns>
//            public override string ToString()
//            {
//                switch (ControlAuxiliaryData.MainSetting.DBSetting.LanguageVersion)
//                {
//                    case LanguageVersion.National:
//                        return String.Concat(
//                            $"Pracovní vlákno ID = {Id} pro protínání bodové vrstvy s vrstvou uhrnů pomocné proměnné.");
//                    case LanguageVersion.International:
//                    default:
//                        return String.Concat(
//                             $"Worker thread ID = {Id} for intersection between point layer and layer of auxiliary variable totals.");
//                }
//            }

//            #endregion Methods


//            #region Events


//            #region WorkerThreadStarted Event

//            /// <summary>
//            /// Delegat funkce obsluhujici udalost
//            /// ZAHAJENI protinani bodove vrstvy s vrstvou uhrnu pomocne promenne
//            /// v pracovnim vlakne
//            /// </summary>
//            /// <param name="sender">Objekt odesilajici udalost (IntersectionsWorkerThread)</param>
//            /// <param name="e">Parametry udalosti</param>
//            public delegate void WorkerThreadStartedHandler(object sender, IntersectionsThreadEventArgs e);

//            /// <summary>
//            /// Udalost
//            /// ZAHAJENI protinani bodove vrstvy s vrstvou uhrnu pomocne promenne
//            /// v pracovnim vlakne
//            /// </summary>
//            public event WorkerThreadStartedHandler WorkerThreadStarted;

//            /// <summary>
//            /// Vyvolani udalosti
//            /// ZAHAJENI protinani bodove vrstvy s vrstvou uhrnu pomocne promenne
//            /// v pracovnim vlakne
//            /// </summary>
//            private void RaiseWorkerThreadStarted()
//            {
//                isWorking = true;
//                stop = false;

//                IntersectionsThreadEventArgs e = new IntersectionsThreadEventArgs()
//                {
//                    Origin = this
//                };

//                switch (ControlAuxiliaryData.MainSetting.DBSetting.LanguageVersion)
//                {
//                    case LanguageVersion.National:
//                        e.Message = String.Concat(
//                            $"Vlákno {Id} právě zahájilo protínání bodové vrstvy s vrstvou úrhnů pomocné proměné.");
//                        break;
//                    case LanguageVersion.International:
//                    default:
//                        e.Message = String.Concat(
//                            $"Thread {Id} has just started intersection point layer with auxiliary totals layer.");
//                        break;
//                }

//                // Spousti zaregistrovane odberatele udalosti pokud existuji:
//                WorkerThreadStarted?.Invoke(this, e);
//            }

//            #endregion WorkerThreadStarted Event


//            #region WorkerThreadStopped Event

//            /// <summary>
//            /// Delegat funkce obsluhujici udalost
//            /// ZASTAVENI protinani bodove vrstvy s vrstvou uhrnu pomocne promenne
//            /// v pracovnim vlakne
//            /// </summary>
//            /// <param name="sender">Objekt odesilajici udalost (IntersectionsWorkerThread)</param>
//            /// <param name="e">Parametry udalosti</param>
//            public delegate void WorkerThreadStoppedHandler(object sender, IntersectionsThreadEventArgs e);

//            /// <summary>
//            /// Udalost
//            /// ZASTAVENI protinani bodove vrstvy s vrstvou uhrnu pomocne promenne
//            /// v pracovnim vlakne
//            /// </summary>
//            public event WorkerThreadStoppedHandler WorkerThreadStopped;

//            /// <summary>
//            /// Vyvolani udalosti
//            /// ZASTAVENI protinani bodove vrstvy s vrstvou uhrnu pomocne promenne
//            /// v pracovnim vlakne
//            /// </summary>
//            private void RaiseWorkerThreadStopped()
//            {
//                IntersectionsThreadEventArgs e = new IntersectionsThreadEventArgs()
//                {
//                    Origin = this
//                };

//                switch (ControlAuxiliaryData.MainSetting.DBSetting.LanguageVersion)
//                {
//                    case LanguageVersion.National:
//                        e.Message = $"Vlákno {Id} bylo zastaveno.";
//                        break;
//                    case LanguageVersion.International:
//                    default:
//                        e.Message = $"Thread {Id} has been stopped.";
//                        break;
//                }

//                // Spousti zaregistrovane odberatele udalosti pokud existuji:
//                WorkerThreadStopped?.Invoke(this, e);

//                isWorking = false;
//            }

//            #endregion WorkerThreadStopped Event


//            #region WorkerThreadFinished Event

//            /// <summary>
//            /// Delegat funkce obsluhujici udalost
//            /// DOKONCENI protinani bodove vrstvy s vrstvou uhrnu pomocne promenne
//            /// v pracovnim vlakne
//            /// </summary>
//            /// <param name="sender">Objekt odesilajici udalost (IntresectionsWorkerThread)</param>
//            /// <param name="e">Parametry udalosti</param>
//            public delegate void WorkerThreadFinishedHandler(object sender, IntersectionsThreadEventArgs e);

//            /// <summary>
//            /// Udalost
//            /// DOKONCENI protinani bodove vrstvy s vrstvou uhrnu pomocne promenne
//            /// v pracovnim vlakne
//            /// </summary>
//            public event WorkerThreadFinishedHandler WorkerThreadFinished;

//            /// <summary>
//            /// Vyvolani udalosti
//            /// DOKONCENI protinani bodove vrstvy s vrstvou uhrnu pomocne promenne
//            /// v pracovnim vlakne
//            /// </summary>
//            private void RaiseWorkerThreadFinished()
//            {
//                IntersectionsThreadEventArgs e = new IntersectionsThreadEventArgs()
//                {
//                    Origin = this
//                };

//                switch (ControlAuxiliaryData.MainSetting.DBSetting.LanguageVersion)
//                {
//                    case LanguageVersion.National:
//                        e.Message = String.Concat(
//                            $"Vlákno {Id} právě dokončilo protínání bodové vrstvy s vrstvou úrhnů pomocné proměné.");
//                        break;
//                    case LanguageVersion.International:
//                    default:
//                        e.Message = String.Concat(
//                            $"Thread {Id} has just intersection point layer with auxiliary totals layer.");
//                        break;
//                }

//                // Spousti zaregistrovane odberatele udalosti pokud existuji:
//                WorkerThreadFinished?.Invoke(this, e);

//                isWorking = false;
//            }

//            #endregion WorkerThreadFinished Event


//            #region SegmentStarted Event

//            /// <summary>
//            /// Delegat funkce obsluhujici udalost
//            /// ZAHAJENI protinani bodove vrstvy s vrstvou uhrnu pomocne promenne
//            /// pro jednu skupinu bodu
//            /// v pracovnim vlakne
//            /// </summary>
//            /// <param name="sender">Objekt odesilajici udalost (IntersectionsWorkerThread)</param>
//            /// <param name="e">Parametry udalosti</param>
//            public delegate void SegmentStartedHandler(object sender, IntersectionsThreadEventArgs e);

//            /// <summary>
//            /// Udalost
//            /// ZAHAJENI protinani bodove vrstvy s vrstvou uhrnu pomocne promenne
//            /// pro jednu skupinu bodu
//            /// v pracovnim vlakne
//            /// </summary>
//            public event SegmentStartedHandler SegmentStarted;

//            /// <summary>
//            /// Vyvolani udalosti
//            /// ZAHAJENI protinani bodove vrstvy s vrstvou uhrnu pomocne promenne
//            /// pro jednu skupinu bodu
//            /// v pracovnim vlakne
//            /// </summary>
//            /// <param name="segment">Skupina bodu</param>
//            private void RaiseSegmentStarted(FnConfigForAuxData segment)
//            {
//                IntersectionsThreadEventArgs e = new IntersectionsThreadEventArgs()
//                {
//                    Origin = this
//                };

//                switch (ControlAuxiliaryData.MainSetting.DBSetting.LanguageVersion)
//                {
//                    case LanguageVersion.National:
//                        e.Message = String.Concat(
//                             $"Vlákno {Id} právě zahájilo protínání bodové vrstvy s vrstvou úrhnů pomocné proměné ",
//                             $"pro skupinu bodů gid: {Functions.PrepNIntArg(segment.GidStart)}",
//                             $" - {Functions.PrepNIntArg(segment.GidEnd)}.");
//                        break;
//                    case LanguageVersion.International:
//                    default:
//                        e.Message = String.Concat(
//                            $"Thread {Id} has just started intersection point layer with auxiliary totals layer.",
//                            $"for group of points gid: {Functions.PrepNIntArg(segment.GidStart)}",
//                            $" - {Functions.PrepNIntArg(segment.GidEnd)}.");
//                        break;
//                }

//                // Spousti zaregistrovane odberatele udalosti pokud existuji:
//                SegmentStarted?.Invoke(this, e);
//            }

//            #endregion SegmentStarted Event


//            #region SegmentFinished Event

//            /// <summary>
//            /// Delegat funkce obsluhujici udalost
//            /// DOKONCENI protinani bodove vrstvy s vrstvou uhrnu pomocne promenne
//            /// pro jednu skupinu bodu
//            /// v pracovnim vlakne
//            /// </summary>
//            /// <param name="sender">Objekt odesilajici udalost (IntersectionsWorkerThread)</param>
//            /// <param name="e">Parametry udalosti</param>
//            public delegate void SegmentFinishedHandler(object sender, IntersectionsThreadEventArgs e);

//            /// <summary>
//            /// Udalost
//            /// DOKONCENI protinani bodove vrstvy s vrstvou uhrnu pomocne promenne
//            /// pro jednu skupinu bodu
//            /// v pracovnim vlakne
//            /// </summary>
//            public event SegmentFinishedHandler SegmentFinished;

//            /// <summary>
//            /// Vyvolani udalosti
//            /// DOKONCENI protinani bodove vrstvy s vrstvou uhrnu pomocne promenne
//            /// pro jednu skupinu bodu
//            /// v pracovnim vlakne
//            /// </summary>
//            /// <param name="segment">Skupina bodu</param>
//            private void RaiseSegmentFinished(FnConfigForAuxData segment)
//            {
//                IntersectionsThreadEventArgs e = new IntersectionsThreadEventArgs()
//                {
//                    Origin = this
//                };

//                switch (ControlAuxiliaryData.MainSetting.DBSetting.LanguageVersion)
//                {
//                    case LanguageVersion.National:
//                        e.Message = String.Concat(
//                            $"Vlákno {Id} právě dokončilo protínání bodové vrstvy s vrstvou úrhnů pomocné proměné ",
//                            $"pro skupinu bodů gid: {Functions.PrepNIntArg(segment.GidStart)}",
//                            $" - {Functions.PrepNIntArg(segment.GidEnd)}.");
//                        break;
//                    case LanguageVersion.International:
//                    default:
//                        e.Message = String.Concat(
//                            $"Thread {Id} has just finished intersection point layer with auxiliary totals layer ",
//                            $"for group of points gid: {Functions.PrepNIntArg(segment.GidStart)}",
//                            $" - {Functions.PrepNIntArg(segment.GidEnd)}.");
//                        break;
//                }

//                // Spousti zaregistrovane odberatele udalosti pokud existuji:
//                SegmentFinished?.Invoke(this, e);
//            }

//            #endregion SegmentFinished Event


//            #region SegmentFailed Event

//            /// <summary>
//            /// Delegat funkce obsluhujici udalost
//            /// VZNIKU CHYBY pri protinani bodove vrstvy s vrstvou uhrnu pomocne promenne
//            /// pro jednu skupinu bodu
//            /// v pracovnim vlakne
//            /// </summary>
//            /// <param name="sender">Objekt odesilajici udalost (IntersectionsWorkerThread)</param>
//            /// <param name="e">Parametry udalosti</param>
//            public delegate void SegmentFailedHandler(object sender, IntersectionsThreadEventArgs e);

//            /// <summary>
//            /// Udalost
//            /// VZNIKU CHYBY pri protinani bodove vrstvy s vrstvou uhrnu pomocne promenne
//            /// pro jednu skupinu bodu
//            /// v pracovnim vlakne
//            /// </summary>
//            public event SegmentFailedHandler SegmentFailed;

//            /// <summary>
//            /// Vyvolani udalosti
//            /// VZNIKU CHYBY pri protinani bodove vrstvy s vrstvou uhrnu pomocne promenne
//            /// pro jednu skupinu bodu
//            /// v pracovnim vlakne
//            /// </summary>
//            /// <param name="pointsInOneStep">Skupina bodu</param>
//            /// <param name="exception">Objekt chyby, ktera vznikla pri protinani bodove vrstvy s vrstvou uhrnu pomocne promenne</param>
//            private void RaiseSegmentFailed(FnConfigForAuxData pointsInOneStep, Exception exception)
//            {
//                IntersectionsThreadEventArgs e = new IntersectionsThreadEventArgs()
//                {
//                    Origin = this,
//                    Exception = exception
//                };

//                switch (ControlAuxiliaryData.MainSetting.DBSetting.LanguageVersion)
//                {
//                    case LanguageVersion.National:
//                        e.Message = String.Concat(
//                            $"Ve vlákně {Id} vznikla chyba při protínání bodové vrstvy s vrstvou úhrnů pomocné proměnné ",
//                            $"pro skupinu bodů gid: {Functions.PrepNIntArg(pointsInOneStep.GidStart)}",
//                            $" - {Functions.PrepNIntArg(pointsInOneStep.GidEnd)}.");
//                        break;
//                    case LanguageVersion.International:
//                    default:
//                        e.Message = String.Concat(
//                            $"Exception has been raised in thread {Id} when calculating intersection point layer with auxiliary totals layer ",
//                            $"for group of points gid: {Functions.PrepNIntArg(pointsInOneStep.GidStart)}",
//                            $" - {Functions.PrepNIntArg(pointsInOneStep.GidEnd)}.");
//                        break;
//                }

//                // Spousti zaregistrovane odberatele udalosti pokud existuji:
//                SegmentFailed?.Invoke(this, e);
//            }

//            #endregion SegmentFailed Event


//            #endregion Events

//        }

//    }
//}