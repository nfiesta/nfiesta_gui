﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Threading;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.AuxiliaryData;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace ModuleAuxiliaryData
    {

        /// <summary>
        /// <para lang="cs">Pracovní vlákno pro výpočet úhrnů pomocné proměnné ve výpočetních buňkách</para>
        /// <para lang="en">Worker thread for calculation of auxiliary variable totals in estimation cells</para>
        /// </summary>
        internal class TotalsWorkerThread
        {

            #region Constants

            /// <summary>
            /// <para lang="cs">Hodnota určuje, zda vlákno je nebo není na pozadí</para>
            /// <para lang="en">Value indicating whether or not a thread is a background thread</para>
            /// </summary>
            private const bool isBackground = true;

            /// <summary>
            /// <para lang="cs">Udržovat připojení k databázi</para>
            /// <para lang="en">Keep database connection alive</para>
            /// </summary>
            private const bool stayConnected = true;

            #endregion Constants


            #region Events

            /// <summary>
            /// <para lang="cs">Událost "Zahájení pracovního vlákna"</para>
            /// <para lang="en">"Starting a worker thread" event</para>
            /// </summary>
            public event EventHandler OnWorkerStart;

            /// <summary>
            /// <para lang="cs">Událost "Zastavení pracovního vlákna"</para>
            /// <para lang="en">"Stop a worker thread" event</para>
            /// </summary>
            public event EventHandler OnWorkerStopped;

            /// <summary>
            /// <para lang="cs">Událost "Chyba při připojení k databázi"</para>
            /// <para lang="en">"Database connection error" event</para>
            /// </summary>
            public event EventHandler OnConnectionException;

            /// <summary>
            /// <para lang="cs">Událost "Zahájení jednoho úkolu pracovního vlákna"</para>
            /// <para lang="en">"Starting one task of a worker thread" event</para>
            /// </summary>
            public event EventHandler OnTaskStart;

            /// <summary>
            /// <para lang="cs">Událost "Chyba při zpracování úkolu"</para>
            /// <para lang="en">"Task processing error" event</para>
            /// </summary>
            public event EventHandler OnTaskException;

            /// <summary>
            /// <para lang="cs">Událost "Ukončení jednoho úkolu pracovního vlákna"</para>
            /// <para lang="en">"Terminating one task of a worker thread" event</para>
            /// </summary>
            public event EventHandler OnTaskComplete;

            /// <summary>
            /// <para lang="cs">Událost "Ukončení pracovního vlákna"</para>
            /// <para lang="en">"Terminating a worker thread" event</para>
            /// </summary>
            public event EventHandler OnWorkerComplete;

            #endregion Events


            #region Private Fields

            /// <summary>
            /// <para lang="cs">Metoda spouštěná vláknem</para>
            /// <para lang="en">Method triggered by a thread</para>
            /// </summary>
            private ParameterizedThreadStart method = null;

            /// <summary>
            /// <para lang="cs">Objekt vlákna</para>
            /// <para lang="en">Thread object</para>
            /// </summary>
            private Thread thread = null;

            /// <summary>
            /// <para lang="cs">Požadavek na zastavení vlákna</para>
            /// <para lang="en">Request to stop the thread</para>
            /// </summary>
            private bool stopRequest = false;

            /// <summary>
            /// <para lang="cs">Provádí toto vlákno právě výpočet?</para>
            /// <para lang="en">Is this thread doing calculation?</para>
            /// </summary>
            private bool isWorking = false;

            /// <summary>
            /// <para lang="cs">Zpracovávaný úkol</para>
            /// <para lang="en">Task for processing</para>
            /// </summary>
            private object task = null;

            /// <summary>
            /// <para lang="cs">Zámek</para>
            /// <para lang="en">Locking object</para>
            /// </summary>
            private readonly object locker = new();

            /// <summary>
            /// <para lang="cs">Identifikační číslo pracovního vlákna</para>
            /// <para lang="en">Worker thread identifier</para>
            /// </summary>
            private Nullable<int> id = null;

            /// <summary>
            /// <para lang="cs">Připojení k databázi pro pracovní vlákna</para>
            /// <para lang="en">Database connection for worker thread</para>
            /// </summary>
            private ZaJi.PostgreSQL.PostgreSQLWrapper threadConnection = null;

            /// <summary>
            /// <para lang="cs">Připojení k databázi</para>
            /// <para lang="en">Database connection</para>
            /// </summary>
            private ZaJi.PostgreSQL.PostgreSQLWrapper connection = null;

            /// <summary>
            /// <para lang="cs">Argumenty pro uloženou proceduru</para>
            /// <para lang="en">Arguments for the stored procedure</para>
            /// </summary>
            private ThreadSafeList<object> storedProcedureArgs = null;


            /// <summary>
            /// <para lang="cs">
            /// Seznam vybraných segmentů výpočetních buněk
            /// pro výpočet úhrnů pomocných proměnných
            /// </para>
            /// <para lang="en">
            /// List of selected estimation cell segments
            /// for calculation of auxiliary variable totals
            /// </para>
            /// </summary>
            private TFnGetGidsForAuxTotalAppList gidsForAuxTotal = null;

            /// <summary>
            /// <para lang="cs">Slovník zpráv z pracovního vlákna pro národní verzi</para>
            /// <para lang="en">Dictionary of messages from worker thread for national version</para>
            /// </summary>
            private readonly Dictionary<string, string> dictNational = new()
            {
                { "msgOnWorkerStart",
                    String.Concat(
                        "Vlákno $1 právě zahájilo výpočet úhrnů pomocné proměnné ",
                        "pro všechny segmenty výpočetních buněk.") },

                { "msgOnWorkerStopped",
                    String.Concat(
                        "Vlákno $1 bylo zastaveno.") },

                { "msgOnWorkerComplete",
                    String.Concat(
                        "Vlákno $1 právě dokončilo výpočet úhrnů pomocné proměnné ",
                        "pro všechny segmenty výpočetních buněk.") },

                { "msgOnTaskStart",
                    String.Concat(
                        "Vlákno $1 právě zahájilo výpočet úhrnů pomocné proměnné ",
                        "pro výpočetní buňku: $2 segment: $3.") },

                { "msgOnTaskComplete",
                    String.Concat(
                        $"Vlákno $1 právě dokončilo výpočet úhrnů pomocné proměnné ",
                        "pro výpočetní buňku: $2 segment: $3.") },

                { "msgOnConnectionException",
                     String.Concat(
                        $"Ve vlákně $1 vznikla chyba při připojení k databázi. ") },

                { "msgOnTaskException",
                    String.Concat(
                        $"Ve vlákně $1 vznikla chyba při výpočtu úhrnů pomocné proměnné ",
                        "pro výpočetní buňku: $2 segment: $3.") }
            };

            /// <summary>
            /// <para lang="cs">Slovník zpráv z pracovního vlákna pro anglickou verzi</para>
            /// <para lang="en">Dictionary of messages from worker thread for English version</para>
            /// </summary>
            private readonly Dictionary<string, string> dictInternational = new()
            {
                { "msgOnWorkerStart",
                    String.Concat(
                        "Thread $1 has just started calculation of auxiliary variable totals ",
                        "for all segments of estimation cells.") },

                { "msgOnWorkerStopped",
                    String.Concat(
                        "Thread $1 has been stopped.") },

                { "msgOnWorkerComplete",
                    String.Concat(
                        "Thread {Id} has just finished calculation of auxiliary variable totals ",
                        "for all segments of estimation cells.") },

                { "msgOnTaskStart",
                    String.Concat(
                        "Thread $1 has just started calculation of auxiliary variable totals  ",
                        "for estimation cell: $2 segment: $3.") },

                { "msgOnTaskComplete",
                    String.Concat(
                        $"Thread $1 has just finished calculation of auxiliary variable totals ",
                        "for estimation cell: $2 segment: $3.") },

                { "msgOnConnectionException",
                     String.Concat(
                        $"Exception has been raised in thread $1 when connecting to the database. ") },

                { "msgOnTaskException",
                    String.Concat(
                        $"Exception has been raised in thread $1 when calculating auxiliary variable totals ",
                        "for estimation cell: $2 segment: $3.") }
            };

            /// <summary>
            /// <para lang="cs">Slovník zpráv z pracovního vlákna</para>
            /// <para lang="en">Dictionary of messages from worker thread</para>
            /// </summary>
            private Dictionary<string, string> dictionary = [];

            #endregion Private Fields


            #region Properties

            /// <summary>
            /// <para lang="cs">Identifikační číslo pracovního vlákna</para>
            /// <para lang="en">Worker thread identifier</para>
            /// </summary>
            public Nullable<int> Id
            {
                get
                {
                    lock (locker)
                    {
                        return id;
                    }
                }
                set
                {
                    lock (locker)
                    {
                        id = value;
                    }
                }
            }

            /// <summary>
            /// <para lang="cs">Zpracovávaný úkol</para>
            /// <para lang="en">Task for processing</para>
            /// </summary>
            public object Task
            {
                get
                {
                    lock (locker)
                    {
                        return task;
                    }
                }
                set
                {
                    lock (locker)
                    {
                        task = value;
                    }
                }
            }

            /// <summary>
            /// <para lang="cs">Provádí toto vlákno právě výpočet?</para>
            /// <para lang="en">Is this thread doing calculation?</para>
            /// </summary>
            public bool IsWorking
            {
                get
                {
                    lock (locker)
                    {
                        return isWorking;
                    }
                }
                private set
                {
                    lock (locker)
                    {
                        isWorking = value;
                    }
                }
            }

            /// <summary>
            /// <para lang="cs">Připojení k databázi</para>
            /// <para lang="en">Database connection</para>
            /// </summary>
            public ZaJi.PostgreSQL.PostgreSQLWrapper Connection
            {
                get
                {
                    lock (locker)
                    {
                        return connection;
                    }
                }
                set
                {
                    lock (locker)
                    {
                        connection = value;
                    }
                }
            }

            /// <summary>
            /// <para lang="cs">Připojení k databázi pro pracovní vlákna</para>
            /// <para lang="en">Database connection for worker thread</para>
            /// </summary>
            protected ZaJi.PostgreSQL.PostgreSQLWrapper ThreadConnection
            {
                get
                {
                    lock (locker)
                    {
                        return threadConnection;
                    }
                }
            }

            /// <summary>
            /// <para lang="cs">Argumenty pro uloženou proceduru</para>
            /// <para lang="en">Arguments for the stored procedure</para>
            /// </summary>
            public ThreadSafeList<object> StoredProcedureArgs
            {
                get
                {
                    return storedProcedureArgs ?? new ThreadSafeList<object>();
                }
                set
                {
                    storedProcedureArgs = value ?? new ThreadSafeList<object>();
                }
            }

            /// <summary>
            /// <para lang="cs">
            /// Seznam vybraných segmentů výpočetních buněk
            /// pro výpočet úhrnů pomocných proměnných
            /// </para>
            /// <para lang="en">
            /// List of selected estimation cell segments
            /// for calculation of auxiliary variable totals
            /// </para>
            /// </summary>
            public TFnGetGidsForAuxTotalAppList GidsForAuxTotal
            {
                get
                {
                    lock (locker)
                    {
                        return gidsForAuxTotal;
                    }
                }
                set
                {
                    lock (locker)
                    {
                        gidsForAuxTotal = value;
                    }
                }
            }

            /// <summary>
            /// <para lang="cs">Jazyková verze (write-only)</para>
            /// <para lang="en">Language version (write-only)</para>
            /// </summary>
            public LanguageVersion LanguageVersion
            {
                set
                {
                    switch (value)
                    {
                        case LanguageVersion.International:
                            dictionary = dictInternational;
                            return;

                        case LanguageVersion.National:
                            dictionary = dictNational;
                            return;

                        default:
                            dictionary = dictInternational;
                            return;
                    }
                }
            }

            #endregion Properties


            #region Methods

            /// <summary>
            /// <para lang="cs">Spuštění pracovního vlákna</para>
            /// <para lang="en">Starting a worker thread</para>
            /// </summary>
            public void Start()
            {
                method = new ParameterizedThreadStart(BackgroundTask);

                thread =
                    new Thread(start: method)
                    { IsBackground = isBackground };

                stopRequest = false;

                task = null;

                thread.Start(parameter: null);
            }

            /// <summary>
            /// <para lang="cs">Zastavení výpočtu</para>
            /// <para lang="en">Stop calculation</para>
            /// </summary>
            public void Stop()
            {
                stopRequest = true;
            }

            /// <summary>
            /// <para lang="cs">Čeká na dokončení činnosti vlákna</para>
            /// <para lang="en">Waits until all thread finishes its work</para>
            /// </summary>
            public void Join()
            {
                thread?.Join();
            }

            /// <summary>
            /// <para lang="cs">Připojení k databázi</para>
            /// <para lang="en">Database connection</para>
            /// </summary>
            private bool Connect()
            {
                if (Connection == null)
                {
                    return false;
                }

                threadConnection = new ZaJi.PostgreSQL.PostgreSQLWrapper()
                {
                    StayConnected = stayConnected
                };

                #region Connection Exception
                threadConnection.ConnectionError +=
                    new ZaJi.PostgreSQL.PostgreSQLWrapper.ConnectionErrorHandler(
                    (sender, e) =>
                    {
                        string message =
                         (dictionary.TryGetValue(key: "msgOnConnectionException",
                                    out string msgOnConnectionException)
                                        ? msgOnConnectionException
                                        : String.Empty)
                                .Replace(oldValue: "$1",
                                    newValue: (Id == null) ? Functions.StrNull : Id.ToString());
                        ThreadEventArgs eventArgs = new()
                        {
                            Id = Id,
                            Task = null,
                            TasksTotal = null,
                            TasksCompleted = null,
                            ElapsedTime = null,
                            ExceptionEventArgs = e,
                            Notice = null,
                            Message = message,
                            Tag = null
                        };
                        OnConnectionException?.Invoke(sender: this, e: eventArgs);
                    });
                #endregion Connection Exception

                #region Task Exception
                threadConnection.ExceptionReceived +=
                    new ZaJi.PostgreSQL.PostgreSQLWrapper.ExceptionReceivedHandler(
                    (sender, e) =>
                    {
                        TFnGetGidsForAuxTotalApp gidForAuxTotal = (TFnGetGidsForAuxTotalApp)Task;

                        string sql = ADFunctions.FnMakeAuxTotal.GetCommandText(
                            configurationId: gidForAuxTotal?.ConfigId,
                            estimationCellId: gidForAuxTotal?.EstimationCellId,
                            cellId: gidForAuxTotal?.CellId,
                            extensionVersionId: gidForAuxTotal?.ExtensionVersionId,
                            guiVersionId: gidForAuxTotal?.GuiVersionId,
                            recalc: gidForAuxTotal?.Recalc);

                        string message =
                            (dictionary.TryGetValue(key: "msgOnTaskException", out string msgOnTaskException)
                                ? msgOnTaskException
                                : String.Empty)
                            .Replace(oldValue: "$1",
                                newValue: (Id != null) ? Id.ToString() : Functions.StrNull)
                            .Replace(oldValue: "$2",
                                newValue: gidForAuxTotal?.ExtendedEstimationCellDescription ?? String.Empty)
                            .Replace(oldValue: "$3",
                                newValue: (gidForAuxTotal?.CellId != null)
                                    ? gidForAuxTotal?.CellId.ToString()
                                    : Functions.StrNull);

                        ThreadEventArgs eventArgs = new()
                        {
                            Id = Id,
                            Task = Task,
                            TasksTotal = null,
                            TasksCompleted = null,
                            ElapsedTime = null,
                            ExceptionEventArgs = e,
                            Notice = null,
                            Message = message,
                            Tag = null
                        };
                        OnTaskException?.Invoke(sender: this, e: eventArgs);
                    });
                #endregion Task Exception

                threadConnection.SetConnection(
                    applicationName: $"{Connection.ApplicationName} WorkerThread: {Id}",
                    host: Connection.Host,
                    port: Connection.Port,
                    database: Connection.Database,
                    userName: Connection.UserName,
                    password: Connection.Password,
                    commandTimeout: Connection.CommandTimeout,
                    keepAlive: Connection.KeepAlive,
                    tcpKeepAlive: Connection.TcpKeepAlive,
                    tcpKeepAliveTime: Connection.TcpKeepAliveTime);

                return threadConnection.Initialized;
            }

            /// <summary>
            /// <para lang="cs">Ukončení činnosti pracovního vlákna</para>
            /// <para lang="en">Worker thread termination</para>
            /// </summary>
            private void Terminate()
            {
                if (threadConnection != null)
                {
                    threadConnection.CloseConnection();
                    threadConnection.Connection.Dispose();
                }
            }

            /// <summary>
            /// <para lang="cs">Operace vykonávané pracovním vláknem</para>
            /// <para lang="en">Operations performed by the worker thread</para>
            /// </summary>
            /// <param name="obj">
            /// <para lang="cs">Objekt s daty, která se předávají dovnitř vlákna</para>
            /// <para lang="en">Object with data that is passed inside the thread</para>
            /// </param>
            private void BackgroundTask(object obj)
            {
                IsWorking = true;

                string sql;
                string message;
                ThreadEventArgs eventArgs;

                #region Worker Start
                sql = null;
                message =
                    (dictionary.TryGetValue(key: "msgOnWorkerStart",
                        out string msgOnWorkerStart)
                            ? msgOnWorkerStart
                            : String.Empty)
                        .Replace(oldValue: "$1",
                            newValue: (Id == null) ? Functions.StrNull : Id.ToString());
                eventArgs = new()
                {
                    Id = Id,
                    Task = null,
                    TasksTotal = null,
                    TasksCompleted = null,
                    ElapsedTime = null,
                    ExceptionEventArgs = null,
                    Notice = null,
                    Message = message,
                    Tag = null
                };
                OnWorkerStart?.Invoke(sender: this, e: eventArgs);
                #endregion Worker Start

                if (Connect())
                {
                    // Výpočet probíhá pro každý segment výpočetní buňky, který byl pracovnímu vláknu přidělen
                    foreach (TFnGetGidsForAuxTotalApp gidForAuxTotal in gidsForAuxTotal.Items)
                    {
                        Task = gidForAuxTotal;

                        #region Task Start
                        sql = ADFunctions.FnMakeAuxTotal.GetCommandText(
                            configurationId: gidForAuxTotal.ConfigId,
                            estimationCellId: gidForAuxTotal.EstimationCellId,
                            cellId: gidForAuxTotal.CellId,
                            extensionVersionId: gidForAuxTotal.ExtensionVersionId,
                            guiVersionId: gidForAuxTotal.GuiVersionId,
                            recalc: gidForAuxTotal.Recalc);
                        message =
                            (dictionary.TryGetValue(key: "msgOnTaskStart", out string msgOnTaskStart)
                                ? msgOnTaskStart
                                : String.Empty)
                            .Replace(oldValue: "$1",
                                newValue: (Id != null) ? Id.ToString() : Functions.StrNull)
                            .Replace(oldValue: "$2",
                                newValue: ((TFnGetGidsForAuxTotalApp)Task)?.ExtendedEstimationCellDescription ?? String.Empty)
                            .Replace(oldValue: "$3",
                                newValue: (((TFnGetGidsForAuxTotalApp)Task)?.CellId != null)
                                    ? ((TFnGetGidsForAuxTotalApp)Task)?.CellId.ToString()
                                    : Functions.StrNull);
                        eventArgs = new ThreadEventArgs()
                        {
                            Id = Id,
                            Task = Task,
                            TasksTotal = null,
                            TasksCompleted = null,
                            ElapsedTime = null,
                            ExceptionEventArgs = null,
                            Notice = null,
                            Message = message,
                            Tag = null
                        };
                        OnTaskStart?.Invoke(sender: this, e: eventArgs);
                        #endregion Task Start

                        // Task
                        //ADFunctions.FnMakeAuxTotal.Execute(
                        //    connection: ThreadConnection,
                        //    configurationId: gidForAuxTotal.ConfigId,
                        //    estimationCellId: gidForAuxTotal.EstimationCellId,
                        //    cellId: gidForAuxTotal.CellId,
                        //    extensionVersionId: gidForAuxTotal.ExtensionVersionId,
                        //    guiVersionId: gidForAuxTotal.GuiVersionId,
                        //    recalc: gidForAuxTotal.Recalc,
                        //    transaction: null);

                        ADFunctions.FnApiMakeAuxTotal.Execute(
                            connection: ThreadConnection,
                            configurationId: gidForAuxTotal.ConfigId,
                            estimationCellId: gidForAuxTotal.EstimationCellId,
                            cellId: gidForAuxTotal.CellId,
                            guiVersionId: gidForAuxTotal.GuiVersionId,
                            recalc: gidForAuxTotal.Recalc,
                            transaction: null);

                        #region Task Complete
                        sql = ADFunctions.FnMakeAuxTotal.GetCommandText(
                            configurationId: gidForAuxTotal.ConfigId,
                            estimationCellId: gidForAuxTotal.EstimationCellId,
                            cellId: gidForAuxTotal.CellId,
                            extensionVersionId: gidForAuxTotal.ExtensionVersionId,
                            guiVersionId: gidForAuxTotal.GuiVersionId,
                            recalc: gidForAuxTotal.Recalc);
                        message =
                            (dictionary.TryGetValue(key: "msgOnTaskComplete", out string msgOnTaskComplete)
                                ? msgOnTaskComplete
                                : String.Empty)
                            .Replace(oldValue: "$1",
                                newValue: (Id != null) ? Id.ToString() : Functions.StrNull)
                            .Replace(oldValue: "$2",
                                newValue: ((TFnGetGidsForAuxTotalApp)Task)?.ExtendedEstimationCellDescription ?? String.Empty)
                            .Replace(oldValue: "$3",
                                newValue: (((TFnGetGidsForAuxTotalApp)Task)?.CellId != null)
                                    ? ((TFnGetGidsForAuxTotalApp)Task)?.CellId.ToString()
                                    : Functions.StrNull);
                        eventArgs = new ThreadEventArgs()
                        {
                            Id = Id,
                            Task = Task,
                            TasksTotal = null,
                            TasksCompleted = null,
                            ElapsedTime = null,
                            ExceptionEventArgs = null,
                            Notice = ThreadConnection.Notice,
                            Message = message,
                            Tag = null
                        };
                        OnTaskComplete?.Invoke(sender: this, e: eventArgs);
                        #endregion Task Complete

                        if (stopRequest)
                        {
                            #region Worker Stop
                            sql = null;
                            message =
                                (dictionary.TryGetValue(key: "msgOnWorkerStopped",
                                    out string msgOnWorkerStopped)
                                        ? msgOnWorkerStopped
                                        : String.Empty)
                                .Replace(oldValue: "$1",
                                    newValue: (Id == null) ? Functions.StrNull : Id.ToString());
                            eventArgs = new ThreadEventArgs()
                            {
                                Id = Id,
                                Task = null,
                                TasksTotal = null,
                                TasksCompleted = null,
                                ElapsedTime = null,
                                ExceptionEventArgs = null,
                                Notice = null,
                                Message = message,
                                Tag = null
                            };
                            OnWorkerStopped?.Invoke(sender: this, e: eventArgs);
                            #endregion Worker Stop

                            break;
                        };
                    }
                }

                Terminate();

                #region Worker Complete
                sql = null;
                message =
                    (dictionary.TryGetValue(key: "msgOnWorkerComplete",
                        out string msgOnWorkerComplete)
                            ? msgOnWorkerComplete
                            : String.Empty)
                    .Replace(oldValue: "$1",
                        newValue: (Id == null) ? Functions.StrNull : Id.ToString());
                eventArgs = new ThreadEventArgs()
                {
                    Id = Id,
                    Task = null,
                    TasksTotal = null,
                    TasksCompleted = null,
                    ElapsedTime = null,
                    ExceptionEventArgs = null,
                    Notice = null,
                    Message = message,
                    Tag = null
                };
                OnWorkerComplete?.Invoke(sender: this, e: eventArgs);
                #endregion Worker Complete

                IsWorking = false;
            }

            #endregion Methods

        }

    }
}
