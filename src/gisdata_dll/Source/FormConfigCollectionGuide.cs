﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.AuxiliaryData;
using ZaJi.PostgreSQL;

namespace ZaJi.ModuleAuxiliaryData
{

    /// <summary>
    /// <para lang="cs">Průvodce vytvořením nové nebo upravou existující skupiny konfigurací</para>
    /// <para lang="en">Guide to creating a new or editing an existing configuration collection</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    internal partial class FormConfigCollectionGuide
        : Form, INfiEstaControl, IAuxiliaryDataControl
    {

        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Skupina konfigurací</para>
        /// <para lang="en">Configuration collection</para>
        /// </summary>
        private ConfigCollection configCollection;

        /// <summary>
        /// <para lang="cs">Seznam vybraných databázových sloupců pro sestavení výběrové podmínky</para>
        /// <para lang="en">List of selected database columns for construction of selection condition</para>
        /// </summary>
        public List<WideColumn> conditionColumns;

        /// <summary>
        /// <para lang="cs">
        /// Provede se zápis nové (newEntry = true)
        /// nebo uprava existující skupiny konfigurací (newEntry = false) do databáze
        /// </para>
        /// A new entry is inserted (newEntry = true)
        /// or updated an existing configuration collection (newEntry = false) into the database
        /// </summary>
        private bool newEntry;

        #endregion Private Fields


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor ovládacího prvku</para>
        /// <para lang="en">Control constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        public FormConfigCollectionGuide(Control controlOwner)
        {
            InitializeComponent();
            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                IAuxiliaryDataControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                return controlOwner;
            }
            set
            {
                IAuxiliaryDataControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables (read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((IAuxiliaryDataControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro pomocná data (read-only)</para>
        /// <para lang="en">Module for auxiliary data setting (read-only)</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((IAuxiliaryDataControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">Skupina konfigurací</para>
        /// <para lang="en">Configuration collection</para>
        /// </summary>
        public ConfigCollection ConfigCollection
        {
            get
            {
                return configCollection;
            }
            set
            {
                configCollection = value;
                InitializeLabels();
            }
        }

        /// <summary>
        /// <para lang="cs">Seznam vybraných databázových sloupců pro sestavení výběrové podmínky</para>
        /// <para lang="en">List of selected database columns for construction of selection condition</para>
        /// </summary>
        public List<WideColumn> ConditionColumns
        {
            get
            {
                return conditionColumns ?? [];
            }
            set
            {
                conditionColumns = value ?? [];
            }
        }

        /// <summary>
        /// <para lang="cs">
        /// Provede se zápis nové (newEntry = true)
        /// nebo uprava existující skupiny konfigurací (newEntry = false) do databáze
        /// </para>
        /// A new entry is inserted (newEntry = true)
        /// or updated an existing configuration collection (newEntry = false) into the database
        /// </summary>
        public bool NewEntry
        {
            get
            {
                return newEntry;
            }
            set
            {
                newEntry = value;
                InitializeLabels();
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { $"{nameof(FormConfigCollectionGuide)}Insert",     "Nová skupina konfigurací" },
                        { $"{nameof(FormConfigCollectionGuide)}Update",     "Editace skupiny konfigurace"  }
                    }
                    : languageFile.NationalVersion.Data.TryGetValue(
                        key: nameof(FormConfigCollectionGuide),
                        out Dictionary<string, string> dictNational)
                            ? dictNational
                            : [],

                LanguageVersion.International => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { $"{nameof(FormConfigCollectionGuide)}Insert",     "New configuration collection" },
                        { $"{nameof(FormConfigCollectionGuide)}Update",     "Update configuration collection"  }
                    }
                    : languageFile.InternationalVersion.Data.TryGetValue(
                        key: nameof(FormConfigCollectionGuide),
                        out Dictionary<string, string> dictInternational)
                            ? dictInternational
                            : [],

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku</para>
        /// <para lang="en">Initialization of the control</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        private void Initialize(Control controlOwner)
        {
            ControlOwner = controlOwner;
            ConfigCollection = null;
            ConditionColumns = [];
            NewEntry = true;
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků ovládacího prvku</para>
        /// <para lang="en">Initialization of control labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
               languageVersion: LanguageVersion,
               languageFile: LanguageFile);

            Text = newEntry
                ? labels.TryGetValue(key: $"{nameof(FormConfigCollectionGuide)}Insert",
                    out string frmConfigCollectionGuideInsertText)
                        ? frmConfigCollectionGuideInsertText
                        : String.Empty
                : labels.TryGetValue(key: $"{nameof(FormConfigCollectionGuide)}Update",
                    out string frmConfigCollectionGuideUpdateText)
                        ? frmConfigCollectionGuideUpdateText
                        : String.Empty;

            foreach (INfiEstaControl control in Controls.OfType<INfiEstaControl>())
            {
                control?.InitializeLabels();
            }
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent()
        {
            DisplayConfigCollectionIntro();
        }

        /// <summary>
        /// <para lang="cs">Zobrazí ovládací prvek "Nová skupina konfigurací"</para>
        /// <para lang="en">Shows control "New configuration collection"</para>
        /// </summary>
        private void DisplayConfigCollectionIntro()
        {
            Controls.Clear();
            if (ConfigCollection == null)
            {
                return;
            }

            ControlConfigCollectionIntro ctrConfigCollectionIntro =
                new(controlOwner: this)
                {
                    Dock = DockStyle.Fill
                };
            ctrConfigCollectionIntro.LoadContent();

            ctrConfigCollectionIntro.NextClick +=
                    new EventHandler((sender, e) =>
                    {
                        if (!NewEntry)
                        {
                            // Část pro UPDATE nového záznamu kolekce konfigurací.
                            // Výběr GIS vrstvy je možný pouze v přídadě nových skupin nebo ještě prázdných skupin.
                            // Po provedení výběru GIS vrstvy mohou ve skupině již existovat konfigurace závislé na tomto výběru,
                            // a proto výběr u skupin, které již konfigurace obsahují, není možně měnit.
                            // (Kromě label, description a přiřazení pomocné proměnné)
                            DisplayConfigCollectionSummary();
                            return;
                        }

                        if (ConfigCollection.Aggregated)
                        {
                            // Pro agregované skupiny se nevolí GIS vrstva
                            DisplayConfigCollectionSummary();
                            return;
                        }

                        switch (ConfigCollection.ConfigFunctionValue)
                        {
                            case ConfigFunctionEnum.VectorTotal:
                                DisplayVectorLayer();
                                return;

                            case ConfigFunctionEnum.RasterTotal:
                                DisplayRasterLayer();
                                return;

                            case ConfigFunctionEnum.RasterTotalWithinVector:
                                DisplayConfigCollectionSummary();
                                return;

                            case ConfigFunctionEnum.RastersProductTotal:
                                DisplayConfigCollectionSummary();
                                return;

                            case ConfigFunctionEnum.PointLayer:
                                DisplayPointLayer();
                                return;

                            case ConfigFunctionEnum.PointTotalCombination:
                                DisplayPointIntersection();
                                return;

                            default:
                                return;
                        }
                    });

            ctrConfigCollectionIntro.CreateControl();
            Controls.Add(value: ctrConfigCollectionIntro);
        }

        /// <summary>
        /// <para lang="cs">Zobrazí ovládací prvek "Volba vektorové vrstvy"</para>
        /// <para lang="en">Shows control "Vector layer selection"</para>
        /// </summary>
        private void DisplayVectorLayer()
        {
            Controls.Clear();
            if (ConfigCollection == null)
            {
                return;
            }

            ControlVectorLayer ctrVectorLayer =
                new(controlOwner: this)
                {
                    Dock = DockStyle.Fill
                };
            ctrVectorLayer.LoadContent();

            ctrVectorLayer.PreviousClick +=
                    new EventHandler((sender, e) =>
                    {
                        DisplayConfigCollectionIntro();
                    });

            ctrVectorLayer.NextClick +=
                    new EventHandler((sender, e) =>
                    {
                        DisplayVectorGeom();
                    });

            ctrVectorLayer.CreateControl();
            Controls.Add(value: ctrVectorLayer);
        }

        /// <summary>
        /// <para lang="cs">Zobrazí ovládací prvek "Volba geometrie pro vektorovou vrstvu"</para>
        /// <para lang="en">Shows control "Vector geometry selection in vector layer"</para>
        /// </summary>
        private void DisplayVectorGeom()
        {
            Controls.Clear();
            if (ConfigCollection == null)
            {
                return;
            }

            ControlVectorGeom ctrVectorGeom =
                new(controlOwner: this)
                {
                    Dock = DockStyle.Fill
                };
            ctrVectorGeom.LoadContent();

            ctrVectorGeom.PreviousClick +=
                    new EventHandler((sender, e) =>
                    {
                        DisplayVectorLayer();
                    });

            ctrVectorGeom.NextClick +=
                    new EventHandler((sender, e) =>
                    {
                        DisplayConfigCollectionSummary();
                    });

            ctrVectorGeom.CreateControl();
            Controls.Add(value: ctrVectorGeom);
        }

        /// <summary>
        /// <para lang="cs">Zobrazí ovládací prvek "Volba rastrové vrstvy"</para>
        /// <para lang="en">Shows control "Raster layer selection"</para>
        /// </summary>
        private void DisplayRasterLayer()
        {
            Controls.Clear();
            if (ConfigCollection == null)
            {
                return;
            }

            ControlRasterLayer ctrRasterLayer =
                new(controlOwner: this)
                {
                    Dock = DockStyle.Fill
                };
            ctrRasterLayer.LoadContent();

            ctrRasterLayer.PreviousClick +=
                    new EventHandler((sender, e) =>
                    {
                        DisplayConfigCollectionIntro();
                    });

            ctrRasterLayer.NextClick +=
                    new EventHandler((sender, e) =>
                    {
                        DisplayRasterGeom();
                    });

            ctrRasterLayer.CreateControl();
            Controls.Add(value: ctrRasterLayer);
        }

        /// <summary>
        /// <para lang="cs">Zobrazí ovládací prvek "Volba geometrie pro rastrovou vrstvu"</para>
        /// <para lang="en">Shows control "Geometry selection in raster layer"</para>
        /// </summary>
        private void DisplayRasterGeom()
        {
            Controls.Clear();
            if (ConfigCollection == null)
            {
                return;
            }

            ControlRasterGeom ctrRasterGeom =
                new(controlOwner: this)
                {
                    Dock = DockStyle.Fill
                };
            ctrRasterGeom.LoadContent();

            ctrRasterGeom.PreviousClick +=
                    new EventHandler((sender, e) =>
                    {
                        DisplayRasterLayer();
                    });

            ctrRasterGeom.NextClick +=
                    new EventHandler((sender, e) =>
                    {
                        DisplayConfigCollectionSummary();
                    });

            ctrRasterGeom.CreateControl();
            Controls.Add(value: ctrRasterGeom);
        }

        /// <summary>
        /// <para lang="cs">Zobrazí ovládací prvek "Volba bodové vrstvy"</para>
        /// <para lang="en">Shows control "Point layer selection"</para>
        /// </summary>
        private void DisplayPointLayer()
        {
            Controls.Clear();
            if (ConfigCollection == null)
            {
                return;
            }

            ControlPointLayer ctrPointLayer =
                new(controlOwner: this)
                {
                    Dock = DockStyle.Fill
                };
            ctrPointLayer.LoadContent();

            ctrPointLayer.PreviousClick +=
                    new EventHandler((sender, e) =>
                    {
                        DisplayConfigCollectionIntro();
                    });

            ctrPointLayer.NextClick +=
                    new EventHandler((sender, e) =>
                    {
                        DisplayPointGeom();
                    });

            ctrPointLayer.CreateControl();
            Controls.Add(value: ctrPointLayer);
        }

        /// <summary>
        /// <para lang="cs">Zobrazí ovládací prvek "Výběr sloupců s doplňujícími informacemi pro bodovou vrstvu"</para>
        /// <para lang="en">Shows control "Select columns with additional information for the point layer"</para>
        /// </summary>
        private void DisplayPointGeom()
        {
            Controls.Clear();
            if (ConfigCollection == null)
            {
                return;
            }

            ControlPointGeom ctrPointGeom =
                new(controlOwner: this)
                {
                    Dock = DockStyle.Fill
                };
            ctrPointGeom.LoadContent();

            ctrPointGeom.PreviousClick +=
                    new EventHandler((sender, e) =>
                    {
                        DisplayPointLayer();
                    });

            ctrPointGeom.NextClick +=
                    new EventHandler((sender, e) =>
                    {
                        DisplayPointColumns();
                    });

            ctrPointGeom.CreateControl();
            Controls.Add(value: ctrPointGeom);
        }

        /// <summary>
        /// <para lang="cs">Zobrazí ovládací prvek "Volba sloupců pro výběr z bodové vrstvy"</para>
        /// <para lang="en">Shows control "Selecting columns for selection from point layer"</para>
        /// </summary>
        private void DisplayPointColumns()
        {
            Controls.Clear();
            if (ConfigCollection == null)
            {
                return;
            }

            ControlPointColumns ctrPointColumns =
                new(controlOwner: this)
                {
                    Dock = DockStyle.Fill
                };
            ctrPointColumns.LoadContent();

            ctrPointColumns.PreviousClick +=
                    new EventHandler((sender, e) =>
                    {
                        DisplayPointGeom();
                    });

            ctrPointColumns.NextClick +=
                    new EventHandler((sender, e) =>
                    {
                        ConditionColumns =
                            [.. ctrPointColumns.SelectedColumns];

                        if (ConditionColumns.Count > 0)
                        {
                            DisplayPointCondition();
                        }
                        else
                        {
                            DisplayConfigCollectionSummary();
                        }
                    });

            ctrPointColumns.CreateControl();
            Controls.Add(value: ctrPointColumns);
        }

        /// <summary>
        /// <para lang="cs">Zobrazí ovládací prvek "Sestavení výběrové podmínky pro bodovou vrstvu"</para>
        /// <para lang="en">Shows control "Constructing a selection condition for a point layer"</para>
        /// </summary>
        private void DisplayPointCondition()
        {
            Controls.Clear();
            if (ConfigCollection == null)
            {
                return;
            }

            ControlPointCondition ctrPointCondition =
                new(controlOwner: this)
                {
                    Dock = DockStyle.Fill
                };
            ctrPointCondition.LoadContent();

            ctrPointCondition.PreviousClick +=
                    new EventHandler((sender, e) =>
                    {
                        DisplayPointColumns();
                    });

            ctrPointCondition.NextClick +=
                    new EventHandler((sender, e) =>
                    {
                        DisplayConfigCollectionSummary();
                    });

            ctrPointCondition.CreateControl();
            Controls.Add(value: ctrPointCondition);
        }

        /// <summary>
        /// <para lang="cs">Zobrazí ovládací prvek "Protínání bodové vrstvy s GIS vrstvou úhrnů"</para>
        /// <para lang="en">Shows control "Intersection of point layer with GIS layer of totals"</para>
        /// </summary>
        private void DisplayPointIntersection()
        {
            Controls.Clear();
            if (ConfigCollection == null)
            {
                return;
            }

            ControlPointIntersection ctrPointIntersection =
                new(controlOwner: this)
                {
                    Dock = DockStyle.Fill
                };
            ctrPointIntersection.LoadContent();

            ctrPointIntersection.PreviousClick +=
                    new EventHandler((sender, e) =>
                    {
                        DisplayConfigCollectionIntro();
                    });

            ctrPointIntersection.NextClick +=
                    new EventHandler((sender, e) =>
                    {
                        DisplayConfigCollectionSummary();
                    });

            ctrPointIntersection.CreateControl();
            Controls.Add(value: ctrPointIntersection);
        }

        /// <summary>
        /// <para lang="cs">Zobrazí ovládací prvek "Zápis nové skupiny konfigurací do databáze"</para>
        /// <para lang="en">Shows control "Insert new configuration collection to database"</para>
        /// </summary>
        private void DisplayConfigCollectionSummary()
        {
            Controls.Clear();
            if (ConfigCollection == null)
            {
                return;
            }

            ControlConfigCollectionSummary ctrConfigCollectionSummary =
                new(controlOwner: this)
                {
                    Dock = DockStyle.Fill
                };
            ctrConfigCollectionSummary.LoadContent();

            ctrConfigCollectionSummary.PreviousClick +=
                new EventHandler((sender, e) =>
                {
                    if (!NewEntry)
                    {
                        // Část pro UPDATE nového záznamu kolekce konfigurací.
                        // Výběr GIS vrstvy je možný pouze v přídadě nových skupin nebo ještě prázdných skupin.
                        // Po provedení výběru GIS vrstvy mohou ve skupině již existovat konfigurace závislé na tomto výběru,
                        // a proto výběr u skupin, které již konfigurace obsahují, není možně měnit.
                        // (Kromě label, description a přiřazení pomocné proměnné)
                        DisplayConfigCollectionIntro();
                        return;
                    }

                    if (ConfigCollection.Aggregated)
                    {
                        // Pro agregované skupiny se nevolí GIS vrstva
                        DisplayConfigCollectionIntro();
                        return;
                    }

                    switch (ConfigCollection.ConfigFunctionValue)
                    {
                        case ConfigFunctionEnum.VectorTotal:
                            DisplayVectorGeom();
                            return;

                        case ConfigFunctionEnum.RasterTotal:
                            DisplayRasterGeom();
                            return;

                        case ConfigFunctionEnum.RasterTotalWithinVector:
                            DisplayConfigCollectionIntro();
                            return;

                        case ConfigFunctionEnum.RastersProductTotal:
                            DisplayConfigCollectionIntro();
                            return;

                        case ConfigFunctionEnum.PointLayer:
                            if (ConditionColumns.Count > 0)
                            {
                                DisplayPointCondition();
                            }
                            else
                            {
                                DisplayPointColumns();
                            }
                            return;

                        case ConfigFunctionEnum.PointTotalCombination:
                            DisplayPointIntersection();
                            return;

                        default:
                            DisplayConfigCollectionIntro();
                            return;
                    }
                });

            ctrConfigCollectionSummary.NextClick +=
                new EventHandler((sender, e) =>
                {
                    DialogResult = DialogResult.OK;
                    Close();
                });

            ctrConfigCollectionSummary.CreateControl();
            Controls.Add(value: ctrConfigCollectionSummary);
        }

        #endregion Methods

    }
}