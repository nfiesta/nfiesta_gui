﻿////
//// Copyright 2020, 2024 ÚHÚL
////
//// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
//// You may not use this work except in compliance with the Licence.
//// You may obtain a copy of the Licence at:
////
//// https://joinup.ec.europa.eu/software/page/eupl
////
//// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
//// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//// See the Licence for the specific language governing permissions and limitations under the Licence.
////

//using System;
//using System.Collections.Generic;
//using System.Threading;
//using ZaJi.PostgreSQL;

//namespace ZaJi
//{
//    namespace ModuleAuxiliaryData
//    {

//        /// <summary>
//        /// Ridici vlakno
//        /// pro protinani bodove vrstvy s vrstvou uhrnu pomocnych promennych
//        /// </summary>
//        internal class IntersectionsControlThread
//        {

//            #region Private Fields

//            /// <summary>
//            /// Databazove tabulky
//            /// </summary>
//            private readonly ModuleDatabase moduleDatabase;

//            /// <summary>
//            /// Skupina konfiguraci,
//            /// pro kterou je pocitano protinani bodove vrstvy s vrstvou uhrnu
//            /// </summary>
//            private readonly ConfigCollection configCollection;

//            /// <summary>
//            /// Seznam konfiguraci a skupin bodu
//            /// pro vypocet protinani bodove vrstvy s vrstvou uhrnu pomocnych promennych
//            /// </summary>
//            private FnConfigForAuxDataList configForAuxDataList;

//            /// <summary>
//            /// Log udalosti vlaken
//            /// </summary>
//            private readonly Log log;

//            /// <summary>
//            /// Je vlakno v cinnosti?
//            /// </summary>
//            private bool isWorking;

//            /// <summary>
//            /// Pozadavek na prepocet
//            /// </summary>
//            private readonly bool recount;

//            /// <summary>
//            /// Zastavit vlakno pred dokoncenim ukolu
//            /// </summary>
//            private bool stop;

//            /// <summary>
//            /// Seznam pracovnich vlaken
//            /// </summary>
//            private SortedList<int, IntersectionsWorkerThread> workerThreads;

//            /// <summary>
//            /// Ukazatel na funkci spoustenou ridicim vlaknem
//            /// </summary>
//            private readonly ParameterizedThreadStart refThreadFunc;

//            /// <summary>
//            /// Objekt ridiciho vlakna
//            /// </summary>
//            private readonly Thread controlThread;

//            #endregion Private Fields


//            #region Constructor

//            /// <summary>
//            /// Konstruktor ridiciho vlakna
//            /// pro protinani bodove vrstvy s vrstvou uhrnu pomocnych promennych
//            /// </summary>
//            /// <param name="moduleDatabase">Databazove tabulky</param>
//            /// <param name="configCollection">Skupina konfiguraci, pro kterou je pocitano protinani bodove vrstvy s vrstvou uhrnu</param>
//            /// <param name="recount">Pozadavek na prepocet</param>
//            public IntersectionsControlThread(
//                ModuleDatabase moduleDatabase,
//                ConfigCollection configCollection,
//                bool recount)
//            {
//                this.moduleDatabase = moduleDatabase;
//                this.configCollection = configCollection;
//                this.configForAuxDataList = new FnConfigForAuxDataList(ModuleDatabase);
//                this.log = new Log() { Length = ModuleSettings.LogDefaultLength };
//                this.isWorking = false;
//                this.recount = recount;
//                this.stop = false;
//                this.workerThreads = new SortedList<int, IntersectionsWorkerThread>();
//                this.refThreadFunc = new ParameterizedThreadStart(BackgroundTask);
//                this.controlThread = new Thread(refThreadFunc) { IsBackground = true };
//            }

//            #endregion Constructor


//            #region Properties

//            /// <summary>
//            /// Databazove tabulky (read-only)
//            /// </summary>
//            public ModuleDatabase ModuleDatabase
//            {
//                get
//                {
//                    return moduleDatabase;
//                }
//            }

//            /// <summary>
//            /// Skupina konfiguraci, pro kterou je pocitano protinani bodove vrstvy s vrstvou uhrnu (read-only)
//            /// </summary>
//            public ConfigCollection ConfigCollection
//            {
//                get
//                {
//                    return configCollection;
//                }
//            }

//            /// <summary>
//            /// Seznam konfiguraci a skupin bodu
//            /// pro protinani bodove vrstvy s vrstvou uhrnu (read-only)
//            /// </summary>
//            public FnConfigForAuxDataList ConfigForAuxDataList
//            {
//                get
//                {
//                    return configForAuxDataList;
//                }
//            }

//            /// <summary>
//            /// Log udalosti vlaken (read-only)
//            /// </summary>
//            public Log Log
//            {
//                get
//                {
//                    return log;
//                }
//            }

//            /// <summary>
//            /// Je vlakno v cinnosti? (read-only)
//            /// </summary>
//            public bool IsWorking
//            {
//                get
//                {
//                    return isWorking;
//                }
//            }

//            /// <summary>
//            /// Pozadavek na prepocet (read-only)
//            /// </summary>
//            public bool Recount
//            {
//                get
//                {
//                    return recount;
//                }
//            }

//            #endregion Properties


//            #region Methods

//            /// <summary>
//            /// Spusteni ridiciho vlakna
//            /// </summary>
//            /// <param name="obj">Objekt s parametry, ktere se predavaji dovnitr vlakna</param>
//            public void Start(object obj)
//            {
//                controlThread.Start(obj);
//            }

//            /// <summary>
//            /// Zastavi vypocet ve vsech vlaknech
//            /// </summary>
//            public void Stop()
//            {
//                stop = true;
//                foreach (IntersectionsWorkerThread worker in workerThreads.Values)
//                {
//                    worker.Stop();
//                }
//            }

//            /// <summary>
//            /// Ceka na dokonceni vsech vlaken
//            /// </summary>
//            public void Join()
//            {
//                foreach (IntersectionsWorkerThread worker in workerThreads.Values)
//                {
//                    worker.Join();
//                }
//            }

//            /// <summary>
//            /// Ukonceni vsech bezicich vlaken
//            /// </summary>
//            public void Abort()
//            {
//                foreach (IntersectionsWorkerThread worker in workerThreads.Values)
//                {
//                    worker.Abort();
//                }
//            }

//            /// <summary>
//            /// Ukol zpracovavany vlaknem
//            /// </summary>
//            /// <param name="obj">Objekt s parametry, ktere se predavaji dovnitr vlakna</param>
//            private void BackgroundTask(object obj)
//            {
//                // Vyvola udalost zahajeni ridiciho vlakna
//                RaiseControlThreadStarted();

//                // Inicializace objektu pri opetovnem spusteni
//                this.configForAuxDataList = new FnConfigForAuxDataList(moduleDatabase);

//                // Nacteni seznamu konfiguraci pro vypocet AuxData z databaze
//                configForAuxDataList.Load(configCollection, recount);
//                log.SegmentsTotal = configForAuxDataList.Items.Count;

//                // Pro kazdy krok
//                foreach (int step in configForAuxDataList.Steps())
//                {
//                    RaiseStepStarted(step);

//                    FnConfigForAuxDataList segmentsForOneStep = configForAuxDataList.ItemsInStep(step);
//                    CreateNewWorkerThreads(segmentsForOneStep);

//                    // Pracovni vlakna ve skupine se spusti
//                    foreach (IntersectionsWorkerThread worker in workerThreads.Values)
//                    {
//                        worker.Start(null);
//                    }

//                    // Ridici vlakno pocka nez vsechna pracovni vlakna dokonci sve ukoly v tomto kroce
//                    // a pak je spusten cyklus pro dalsi krok
//                    Join();

//                    RaiseStepFinished(step);

//                    // Pokud bylo pozadovano zastaveni vlaken ukonci se cyklus
//                    // Vlakno se zastavuje vzdy az po dokonceni celeho kroku
//                    if (stop) { break; }
//                }

//                if (stop)
//                {
//                    RaiseControlThreadStopped();
//                }
//                else
//                {
//                    RaiseControlThreadFinished();
//                }
//            }

//            /// <summary>
//            /// Vytvori novy seznam pracovnich vlaken a zaregistruje odberatele jejich udalosti
//            /// </summary>
//            /// <param name="segmentsForOneStep">Skupiny bodu pro protinani s vrstvou uhrnu, resene v jednom kroce</param>
//            private void CreateNewWorkerThreads(FnConfigForAuxDataList segmentsForOneStep)
//            {
//                workerThreads = new SortedList<int, IntersectionsWorkerThread>();
//                for (int i = 1; i <= ControlAuxiliaryData.MainSetting.AuxiliaryDataThreadCount; i++)
//                {
//                    FnConfigForAuxDataList segments = segmentsForOneStep.ItemsForOneThread(i);
//                    if (segments.Items.Count > 0)
//                    {
//                        IntersectionsWorkerThread worker = new IntersectionsWorkerThread(i, this, segments);
//                        worker.WorkerThreadStarted += WorkerThread_WorkerThreadStarted;
//                        worker.WorkerThreadStopped += WorkerThread_WorkerThreadStopped;
//                        worker.WorkerThreadFinished += WorkerThread_WorkerThreadFinished;
//                        worker.SegmentStarted += WorkerThread_SegmentStarted;
//                        worker.SegmentFinished += WorkerThread_SegmentFinished;
//                        worker.SegmentFailed += WorkerThread_SegmentFailed;
//                        workerThreads.Add(worker.Id, worker);
//                    }
//                }
//            }

//            /// <summary>
//            /// Textovy popis objektu ridiciho vlakna
//            /// </summary>
//            /// <returns>Textovy popis objektu ridiciho vlakna</returns>
//            public override string ToString()
//            {
//                switch (ControlAuxiliaryData.MainSetting.DBSetting.LanguageVersion)
//                {
//                    case LanguageVersion.National:
//                        return String.Concat(
//                            $"Řídící vlákno pro protínání bodové vrstvy s vrstvou uhrnů pomocné proměnné");
//                    case LanguageVersion.International:
//                    default:
//                        return String.Concat(
//                             $"Control thread for intersection between point layer and layer of auxiliary variable totals");
//                }
//            }

//            #endregion Methods


//            #region Events and Event Handlers


//            #region WorkerThreadStarted Event

//            /// <summary>
//            /// Delegat funkce obsluhujici udalost
//            /// ZAHAJENI protinani bodove vrstvy s vrstvou uhrnu pomocne promenne
//            /// v pracovnim vlakne
//            /// </summary>
//            /// <param name="sender">Objekt odesilajici udalost (IntersectionsControlThread)</param>
//            /// <param name="e">Parametry udalosti</param>
//            public delegate void WorkerThreadStartedHandler(object sender, IntersectionsThreadEventArgs e);

//            /// <summary>
//            /// Udalost
//            /// ZAHAJENI protinani bodove vrstvy s vrstvou uhrnu pomocne promenne
//            /// v pracovnim vlakne
//            /// </summary>
//            public event WorkerThreadStartedHandler WorkerThreadStarted;

//            /// <summary>
//            /// Vyvolani udalosti
//            /// ZAHAJENI protinani bodove vrstvy s vrstvou uhrnu pomocne promenne
//            /// v pracovnim vlakne
//            /// </summary>
//            /// <param name="e">Parametry udalosti</param>
//            private void RaiseWorkerThreadStarted(IntersectionsThreadEventArgs e)
//            {
//                WorkerThreadStarted?.Invoke(this, e);
//            }

//            /// <summary>
//            /// Osetreni udalosti
//            /// ZAHAJENI protinani bodove vrstvy s vrstvou uhrnu pomocne promenne
//            /// v pracovnim vlakne
//            /// </summary>
//            /// <param name="sender">Objekt odesilajici udalost (IntersectionsWorkerThread)</param>
//            /// <param name="e">Parametry udalosti</param>
//            private void WorkerThread_WorkerThreadStarted(object sender, IntersectionsThreadEventArgs e)
//            {
//                log.AddIntersectionsThreadEvent(LogEventType.IntersectionsWorkerThreadStarted, e);
//                RaiseWorkerThreadStarted(e);
//            }

//            #endregion WorkerThreadStarted Event


//            #region WorkerThreadStopped Event

//            /// <summary>
//            /// Delegat funkce obsluhujici udalost
//            /// ZASTAVENI protinani bodove vrstvy s vrstvou uhrnu pomocne promenne
//            /// v pracovnim vlakne
//            /// </summary>
//            /// <param name="sender">Objekt odesilajici udalost (IntersectionsControlThread)</param>
//            /// <param name="e">Parametry udalosti</param>
//            public delegate void WorkerThreadStoppedHandler(object sender, IntersectionsThreadEventArgs e);

//            /// <summary>
//            /// Udalost
//            /// ZASTAVENI protinani bodove vrstvy s vrstvou uhrnu pomocne promenne
//            /// v pracovnim vlakne
//            /// </summary>
//            public event WorkerThreadStoppedHandler WorkerThreadStopped;

//            /// <summary>
//            /// Vyvolani udalosti
//            /// ZASTAVENI protinani bodove vrstvy s vrstvou uhrnu pomocne promenne
//            /// v pracovnim vlakne
//            /// </summary>
//            /// <param name="e">Parametry udalosti</param>
//            private void RaiseWorkerThreadStopped(IntersectionsThreadEventArgs e)
//            {
//                WorkerThreadStopped?.Invoke(this, e);
//            }

//            /// <summary>
//            /// Osetreni udalosti
//            /// ZASTAVENI protinani bodove vrstvy s vrstvou uhrnu pomocne promenne
//            /// v pracovnim vlakne
//            /// </summary>
//            /// <param name="sender">Objekt odesilajici udalost (IntersectionsWorkerThread)</param>
//            /// <param name="e">Parametry udalosti</param>
//            private void WorkerThread_WorkerThreadStopped(object sender, IntersectionsThreadEventArgs e)
//            {
//                log.AddIntersectionsThreadEvent(LogEventType.IntersectionsWorkerThreadStopped, e);
//                RaiseWorkerThreadStopped(e);
//            }

//            #endregion WorkerThreadStopped Event


//            #region WorkerThreadFinished Event

//            /// <summary>
//            /// Delegat funkce obsluhujici udalost
//            /// DOKONCENI protinani bodove vrstvy s vrstvou uhrnu pomocne promenne
//            /// v pracovnim vlakne
//            /// </summary>
//            /// <param name="sender">Objekt odesilajici udalost (IntersectionsControlThread)</param>
//            /// <param name="e">Parametry udalosti</param>
//            public delegate void WorkerThreadFinishedHandler(object sender, IntersectionsThreadEventArgs e);

//            /// <summary>
//            /// Udalost
//            /// DOKONCENI protinani bodove vrstvy s vrstvou uhrnu pomocne promenne
//            /// v pracovnim vlakne
//            /// </summary>
//            public event WorkerThreadFinishedHandler WorkerThreadFinished;

//            /// <summary>
//            /// Vyvolani udalosti
//            /// DOKONCENI protinani bodove vrstvy s vrstvou uhrnu pomocne promenne
//            /// v pracovnim vlakne
//            /// </summary>
//            /// <param name="e">Parametry udalosti</param>
//            private void RaiseWorkerThreadFinished(IntersectionsThreadEventArgs e)
//            {
//                WorkerThreadFinished?.Invoke(this, e);
//            }

//            /// <summary>
//            /// Osetreni udalosti
//            /// DOKONCENI protinani bodove vrstvy s vrstvou uhrnu pomocne promenne
//            /// v pracovnim vlakne
//            /// </summary>
//            /// <param name="sender">Objekt odesilajici udalost (IntersectionsWorkerThread)</param>
//            /// <param name="e">Parametry udalosti</param>
//            private void WorkerThread_WorkerThreadFinished(object sender, IntersectionsThreadEventArgs e)
//            {
//                log.AddIntersectionsThreadEvent(LogEventType.IntersectionsWorkerThreadFinished, e);
//                RaiseWorkerThreadFinished(e);
//            }

//            #endregion WorkerThreadFinished Event


//            #region SegmentStarted Event

//            /// <summary>
//            /// Delegat funkce obsluhujici udalost
//            /// ZAHAJENI protinani bodove vrstvy s vrstvou uhrnu pomocne promenne
//            /// pro jednu SKUPINU BODU
//            /// v pracovnim vlakne
//            /// </summary>
//            /// <param name="sender">Objekt odesilajici udalost (IntersectionsControlThread)</param>
//            /// <param name="e">Parametry udalosti</param>
//            public delegate void SegmentStartedHandler(object sender, IntersectionsThreadEventArgs e);

//            /// <summary>
//            /// Udalost
//            /// ZAHAJENI protinani bodove vrstvy s vrstvou uhrnu pomocne promenne
//            /// pro jednu SKUPINU BODU
//            /// v pracovnim vlakne
//            /// </summary>
//            public event SegmentStartedHandler SegmentStarted;

//            /// <summary>
//            /// Vyvolani udalosti
//            /// ZAHAJENI protinani bodove vrstvy s vrstvou uhrnu pomocne promenne
//            /// pro jednu SKUPINU BODU
//            /// v pracovnim vlakne
//            /// </summary>
//            /// <param name="e">Parametry udalosti</param>
//            private void RaiseSegmentStarted(IntersectionsThreadEventArgs e)
//            {
//                SegmentStarted?.Invoke(this, e);
//            }

//            /// <summary>
//            /// Osetreni udalosti
//            /// ZAHAJENI protinani bodove vrstvy s vrstvou uhrnu pomocne promenne
//            /// pro jednu SKUPINU BODU
//            /// v pracovnim vlakne
//            /// </summary>
//            /// <param name="sender">Objekt odesilajici udalost (IntersectionsWorkerThread)</param>
//            /// <param name="e">Parametry udalosti</param>
//            private void WorkerThread_SegmentStarted(object sender, IntersectionsThreadEventArgs e)
//            {
//                log.AddIntersectionsThreadEvent(LogEventType.IntersectionsWorkerThreadSegmentStarted, e);
//                RaiseSegmentStarted(e);
//            }

//            #endregion SegmentStarted Event


//            #region SegmentFinished Event

//            /// <summary>
//            /// Delegat funkce obsluhujici udalost
//            /// DOKONCENI protinani bodove vrstvy s vrstvou uhrnu pomocne promenne
//            /// pro jednu SKUPINU BODU
//            /// v pracovnim vlakne
//            /// </summary>
//            /// <param name="sender">Objekt odesilajici udalost (IntersectionsControlThread)</param>
//            /// <param name="e">Parametry udalosti</param>
//            public delegate void SegmentFinishedHandler(object sender, IntersectionsThreadEventArgs e);

//            /// <summary>
//            /// Udalost
//            /// DOKONCENI protinani bodove vrstvy s vrstvou uhrnu pomocne promenne
//            /// pro jednu SKUPINU BODU
//            /// v pracovnim vlakne
//            /// </summary>
//            public event SegmentFinishedHandler SegmentFinished;

//            /// <summary>
//            /// Vyvolani udalosti
//            /// DOKONCENI protinani bodove vrstvy s vrstvou uhrnu pomocne promenne
//            /// pro jednu SKUPINU BODU
//            /// v pracovnim vlakne
//            /// </summary>
//            /// <param name="e">Parametry udalosti</param>
//            private void RaiseSegmentFinished(IntersectionsThreadEventArgs e)
//            {
//                SegmentFinished?.Invoke(this, e);
//            }

//            /// <summary>
//            /// Osetreni udalosti
//            /// DOKONCENI protinani bodove vrstvy s vrstvou uhrnu pomocne promenne
//            /// pro jednu SKUPINU BODU
//            /// v pracovnim vlakne
//            /// </summary>
//            /// <param name="sender">Objekt odesilajici udalost (IntersectionsWorkerThread)</param>
//            /// <param name="e">Parametry udalosti</param>
//            private void WorkerThread_SegmentFinished(object sender, IntersectionsThreadEventArgs e)
//            {
//                log.AddIntersectionsThreadEvent(LogEventType.IntersectionsWorkerThreadSegmentFinished, e);
//                RaiseSegmentFinished(e);
//            }

//            #endregion SegmentFinished Event


//            #region SegmentFailed Event

//            /// <summary>
//            /// Delegat funkce obsluhujici udalost
//            /// VZNIKU CHYBY pri protinani bodove vrstvy s vrstvou uhrnu pomocne promenne
//            /// pro jednu SKUPINU BODU
//            /// v pracovnim vlakne
//            /// </summary>
//            /// <param name="sender">Objekt odesilajici udalost (IntersectionsControlThread)</param>
//            /// <param name="e">Parametry udalosti</param>
//            public delegate void SegmentFailedHandler(object sender, IntersectionsThreadEventArgs e);

//            /// <summary>
//            /// Udalost
//            /// VZNIKU CHYBY pri protinani bodove vrstvy s vrstvou uhrnu pomocne promenne
//            /// pro jednu SKUPINU BODU
//            /// v pracovnim vlakne
//            /// </summary>
//            public event SegmentFailedHandler SegmentFailed;

//            /// <summary>
//            /// Vyvolani udalosti
//            /// VZNIKU CHYBY pri protinani bodove vrstvy s vrstvou uhrnu pomocne promenne
//            /// pro jednu SKUPINU BODU
//            /// v pracovnim vlakne
//            /// </summary>
//            /// <param name="e">Parametry udalosti</param>
//            private void RaiseSegmentFailed(IntersectionsThreadEventArgs e)
//            {
//                SegmentFailed?.Invoke(this, e);
//            }

//            /// <summary>
//            /// Osetreni udalosti
//            /// VZNIKU CHYBY pri protinani bodove vrstvy s vrstvou uhrnu pomocne promenne
//            /// pro jednu SKUPINU BODU
//            /// </summary>
//            /// <param name="sender">Objekt odesilajici udalost (IntersectionsWorkerThread)</param>
//            /// <param name="e">Parametry udalosti</param>
//            private void WorkerThread_SegmentFailed(object sender, IntersectionsThreadEventArgs e)
//            {
//                log.AddIntersectionsThreadException(LogEventType.IntersectionsWorkerThreadSegmentFailed, e);
//                RaiseSegmentFailed(e);
//            }

//            #endregion SegmentFailed Event


//            #region ControlThreadStarted Event

//            /// <summary>
//            /// Delegat funkce obsluhujici udalost
//            /// ZAHAJENI prace ridiciho vlakna
//            /// pro protinani bodove vrstvy s vrstvou uhrnu pomocne promenne
//            /// </summary>
//            /// <param name="sender">Objekt odesilajici udalost (IntersectionsControlThread)</param>
//            /// <param name="e">Parametry udalosti</param>
//            public delegate void ControlThreadStartedHandler(object sender, IntersectionsThreadEventArgs e);

//            /// <summary>
//            /// Udalost
//            /// ZAHAJENI prace ridiciho vlakna
//            /// pro protinani bodove vrstvy s vrstvou uhrnu pomocne promenne
//            /// </summary>
//            public event ControlThreadStartedHandler ControlThreadStarted;

//            /// <summary>
//            /// Vyvolani udalosti
//            /// ZAHAJENI prace ridiciho vlakna
//            /// </summary>
//            private void RaiseControlThreadStarted()
//            {
//                this.isWorking = true;
//                this.stop = false;

//                IntersectionsThreadEventArgs e = new IntersectionsThreadEventArgs();

//                switch (ControlAuxiliaryData.MainSetting.DBSetting.LanguageVersion)
//                {
//                    case LanguageVersion.National:
//                        e.Message = String.Concat(
//                            $"Řídící vlákno zahájilo činnost.");
//                        break;
//                    case LanguageVersion.International:
//                    default:
//                        e.Message = String.Concat(
//                            $"Control thread has just started.");
//                        break;
//                }

//                log.AddIntersectionsThreadEvent(LogEventType.IntersectionsControlThreadStarted, e);
//                ControlThreadStarted?.Invoke(this, e);
//            }

//            #endregion ControlThreadStarted Event


//            #region ControlThreadStopped Event

//            /// <summary>
//            /// Delegat funkce obsluhujici udalost
//            /// ZASTAVENI prace ridiciho vlakna
//            /// pro protinani bodove vrstvy s vrstvou uhrnu pomocne promenne
//            /// </summary>
//            /// <param name="sender">Objekt odesilajici udalost (IntersectionsControlThread)</param>
//            /// <param name="e">Parametry udalosti</param>
//            public delegate void ControlThreadStoppedHandler(object sender, IntersectionsThreadEventArgs e);

//            /// <summary>
//            /// Udalost
//            /// ZASTAVENI prace ridiciho vlakna
//            /// pro protinani bodove vrstvy s vrstvou uhrnu pomocne promenne
//            /// </summary>
//            public event ControlThreadStoppedHandler ControlThreadStopped;

//            /// <summary>
//            /// Vyvolani udalosti
//            /// ZASTAVENI prace ridiciho vlakna
//            /// pro protinani bodove vrstvy s vrstvou uhrnu pomocne promenne
//            /// </summary>
//            private void RaiseControlThreadStopped()
//            {
//                IntersectionsThreadEventArgs e = new IntersectionsThreadEventArgs();

//                switch (ControlAuxiliaryData.MainSetting.DBSetting.LanguageVersion)
//                {
//                    case LanguageVersion.National:
//                        e.Message = $"Řídící vlákno bylo zastaveno.";
//                        break;
//                    case LanguageVersion.International:
//                    default:
//                        e.Message = $"Control thread has been stopped.";
//                        break;
//                }

//                log.AddIntersectionsThreadEvent(LogEventType.IntersectionsControlThreadStopped, e);
//                ControlThreadStopped?.Invoke(this, e);

//                isWorking = false;
//            }

//            #endregion ControlThreadStopped Event


//            #region ControlThreadFinished Event

//            /// <summary>
//            /// Delegat funkce obsluhujici udalost
//            /// DOKONCENI prace ridiciho vlakna
//            /// pro protinani bodove vrstvy s vrstvou uhrnu pomocne promenne
//            /// </summary>
//            /// <param name="sender">Objekt odesilajici udalost (IntersectionsControlThread)</param>
//            /// <param name="e">Parametry udalosti</param>
//            public delegate void ControlThreadFinishedHandler(object sender, IntersectionsThreadEventArgs e);

//            /// <summary>
//            /// Udalost
//            /// DOKONCENI prace ridiciho vlakna
//            /// pro protinani bodove vrstvy s vrstvou uhrnu pomocne promenne
//            /// </summary>
//            public event ControlThreadFinishedHandler ControlThreadFinished;

//            /// <summary>
//            /// Vyvolani udalosti
//            /// DOKONCENI prace ridiciho vlakna
//            /// pro protinani bodove vrstvy s vrstvou uhrnu pomocne promenne
//            /// </summary>
//            private void RaiseControlThreadFinished()
//            {
//                IntersectionsThreadEventArgs e = new IntersectionsThreadEventArgs();

//                switch (ControlAuxiliaryData.MainSetting.DBSetting.LanguageVersion)
//                {
//                    case LanguageVersion.National:
//                        e.Message = $"PROTÍNÁNÍ BODOVÉ VRSTVY S VRSTVOU ÚHRNÚ POMOCNÉ PROMĚNNÉ BYLO DOKONČENO.";
//                        break;
//                    case LanguageVersion.International:
//                    default:
//                        e.Message = $"INTERSECTION BETWEEN POINT LAYER AND AUXILIARY VARIABLE TOTALS LAYER HAS BEEN DONE.";
//                        break;
//                }

//                log.AddIntersectionsThreadEvent(LogEventType.IntersectionsControlThreadFinished, e);
//                ControlThreadFinished?.Invoke(this, e);

//                isWorking = false;
//            }

//            #endregion ControlThreadFinished Event


//            #region StepStarted Event

//            /// <summary>
//            /// Delegat funkce obsluhujici udalost
//            /// ZAHAJENI protinani bodove vrstvy s vrstvou uhrnu pomocne promenne
//            /// pro jeden KROK VYPOCTU
//            /// v ridicim vlakne
//            /// </summary>
//            /// <param name="sender">Objekt odesilajici udalost (IntersectionsControlThread)</param>
//            /// <param name="e">Parametry udalosti</param>
//            public delegate void StepStartedHandler(object sender, IntersectionsThreadEventArgs e);

//            /// <summary>
//            /// Udalost
//            /// ZAHAJENI protinani bodove vrstvy s vrstvou uhrnu pomocne promenne
//            /// pro jeden KROK VYPOCTU
//            /// v ridicim vlakne
//            /// </summary>
//            public event StepStartedHandler StepStarted;

//            /// <summary>
//            /// Vyvolani udalosti
//            /// ZAHAJENI protinani bodove vrstvy s vrstvou uhrnu pomocne promenne
//            /// pro jeden KROK VYPOCTU
//            /// v ridicim vlakne
//            /// </summary>
//            /// <param name="step">Cislo kroku</param>
//            private void RaiseStepStarted(int step)
//            {
//                IntersectionsThreadEventArgs e = new IntersectionsThreadEventArgs();

//                switch (ControlAuxiliaryData.MainSetting.DBSetting.LanguageVersion)
//                {
//                    case LanguageVersion.National:
//                        e.Message = $"ZAHÁJENO PROTÍNÁNÍ BODOVÉ VRSTVY S VRSTVOU ÚHRNů POMOCNÉ PROMĚNNÉ V KROCE {step}.";
//                        break;
//                    case LanguageVersion.International:
//                    default:
//                        e.Message = $"INTERSECTION BETWEEN POINT LAYER AND AUXILIARY VARIABLE TOTALS LAYER FOR STEP {step} HAS STARTED.";
//                        break;
//                }

//                log.AddIntersectionsThreadEvent(LogEventType.IntersectionsControlThreadStepStarted, e);
//                StepStarted?.Invoke(this, e);
//            }

//            #endregion StepStarted Event


//            #region StepFinished Event

//            /// <summary>
//            /// Delegat funkce obsluhujici udalost
//            /// DOKONCENI protinani bodove vrstvy s vrstvou uhrnu pomocne promenne
//            /// pro jeden KROK VYPOCTU
//            /// v ridicim vlakne
//            /// </summary>
//            /// <param name="sender">Objekt odesilajici udalost (IntersectionsControlThread)</param>
//            /// <param name="e">Parametry udalosti</param>
//            public delegate void StepFinishedHandler(object sender, IntersectionsThreadEventArgs e);

//            /// <summary>
//            /// Udalost
//            /// DOKONCENI protinani bodove vrstvy s vrstvou uhrnu pomocne promenne
//            /// pro jeden KROK VYPOCTU
//            /// v ridicim vlakne
//            /// </summary>
//            public event StepFinishedHandler StepFinished;

//            /// <summary>
//            /// Vyvolani udalosti
//            /// DOKONCENI protinani bodove vrstvy s vrstvou uhrnu pomocne promenne
//            /// pro jeden KROK VYPOCTU
//            /// v ridicim vlakne
//            /// </summary>
//            /// <param name="step">Cislo kroku</param>
//            private void RaiseStepFinished(int step)
//            {
//                IntersectionsThreadEventArgs e = new IntersectionsThreadEventArgs();

//                switch (ControlAuxiliaryData.MainSetting.DBSetting.LanguageVersion)
//                {
//                    case LanguageVersion.National:
//                        e.Message = $"DOKONČENO PROTÍNÁNÍ BODOVÉ VRSTVY S VRSTVOU ÚHRNů POMOCNÉ PROMĚNNÉ V KROCE {step}.";
//                        break;
//                    case LanguageVersion.International:
//                    default:
//                        e.Message = $"INTERSECTION BETWEEN POINT LAYER AND AUXILIARY VARIABLE TOTALS LAYAER FOR STEP {step} HAS FINISHED.";
//                        break;
//                }

//                log.AddIntersectionsThreadEvent(LogEventType.IntersectionsControlThreadStepFinished, e);
//                StepFinished?.Invoke(this, e);
//            }

//            #endregion StepFinished Event


//            #endregion Events

//        }

//    }
//}
