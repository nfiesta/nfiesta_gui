﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.AuxiliaryData;
using ZaJi.PostgreSQL;

namespace ZaJi.ModuleAuxiliaryData
{

    /// <summary>
    /// <para lang="cs">Ovládací prvek "Reklasifikace hodnot pixelu rastru"</para>
    /// <para lang="en">Control "Reclassification of raster pixel values"</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    internal partial class ControlRasterReclass
        : UserControl, INfiEstaControl, IAuxiliaryDataControl
    {

        #region Constants

        private const int ImageRedBall = 0;
        private const int ImageBlueBall = 1;
        private const int ImageYellowBall = 2;
        private const int ImageGreenBall = 3;
        private const int ImageGrayBall = 4;
        private const int ImageLock = 5;

        #endregion Constants


        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        private Control controlOwner;

        #endregion Private Fields


        #region Events

        /// <summary>
        /// <para lang="cs">Událost kliknutí na tlačítko "Předchozí"</para>
        /// <para lang="en">Click event on the "Previous" button</para>
        /// </summary>
        public event EventHandler PreviousClick;

        /// <summary>
        /// <para lang="cs">Událost kliknutí na tlačítko "Další"</para>
        /// <para lang="en">Click event on the "Next" button</para>
        /// </summary>
        public event EventHandler NextClick;

        #endregion Events


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor ovládacího prvku</para>
        /// <para lang="en">Control constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        public ControlRasterReclass(Control controlOwner)
        {
            InitializeComponent();
            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                IAuxiliaryDataControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                if (controlOwner is not FormConfigurationGuide)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(FormConfigurationGuide)}.",
                        paramName: nameof(ControlOwner));
                }

                return controlOwner;
            }
            set
            {
                IAuxiliaryDataControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                if (value is not FormConfigurationGuide)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(FormConfigurationGuide)}.",
                        paramName: nameof(ControlOwner));
                }

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables (read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((IAuxiliaryDataControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro pomocná data (read-only)</para>
        /// <para lang="en">Module for auxiliary data setting (read-only)</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((IAuxiliaryDataControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">Konfigurace (read-only)</para>
        /// <para lang="en">Configuration (read-only)</para>
        /// </summary>
        public Config Configuration
        {
            get
            {
                return ((FormConfigurationGuide)ControlOwner).Configuration;
            }
        }

        /// <summary>
        /// <para lang="cs">Vybraná kategorie rastru (read-only)</para>
        /// <para lang="en">Selected raster category (read-only)</para>
        /// </summary>
        private RasterCategory SelectedRasterCategory
        {
            get
            {
                if ((tvwRasterCategories.SelectedNode == null) ||
                    (tvwRasterCategories.SelectedNode.Tag == null) ||
                    (tvwRasterCategories.SelectedNode.Tag is not RasterCategory))
                {
                    return null;
                }

                return (RasterCategory)tvwRasterCategories.SelectedNode.Tag;
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(btnPrevious),                      "Předchozí" },
                        { nameof(btnNext),                          "Další" },
                        { nameof(grpRaster),                        "Sloupec s rastrem" },
                        { nameof(grpSelectedRasterCategory),        "Vybraná kategorie:" },
                        { nameof(grpRasterCategories),              "Kategorie:" },
                        { nameof(grpReclass),                       "Reklasifikace rastru:" },
                        { nameof(chkReclass),                       "Ponechat původní hodnotu pixelu" },
                        { nameof(lblEmptyRasterCategories),         String.Concat(
                                                                    "Pro zadanou rastrovou vrstvu nebyl nalezen číselník $1.$2 ",
                                                                    "s popisem kategorií rastru nebo je prázdný.") }
                    }
                    : languageFile.NationalVersion.Data.TryGetValue(
                        key: nameof(ControlRasterReclass),
                        out Dictionary<string, string> dictNational)
                            ? dictNational
                            : [],

                LanguageVersion.International => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(btnPrevious),                      "Previous" },
                        { nameof(btnNext),                          "Next" },
                        { nameof(grpRaster),                        "Raster column" },
                        { nameof(grpSelectedRasterCategory),        "Selected category:" },
                        { nameof(grpRasterCategories),              "Categories:" },
                        { nameof(grpReclass),                       "Raster reclassification:" },
                        { nameof(chkReclass),                       "Retain original value of pixel" },
                        { nameof(lblEmptyRasterCategories),         String.Concat(
                                                                    "Lookup table $1.$2 with raster categories description has not been found ",
                                                                    "for selected raster layer or is empty.") }
                    }
                    : languageFile.InternationalVersion.Data.TryGetValue(
                        key: nameof(ControlRasterReclass),
                        out Dictionary<string, string> dictInternational)
                            ? dictInternational
                            : [],

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku</para>
        /// <para lang="en">Initialization of the control</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        private void Initialize(Control controlOwner)
        {
            BorderStyle = BorderStyle.None;
            ControlOwner = controlOwner;

            InitializeLabels();
            EnableNext();

            btnPrevious.Click += new EventHandler(
                (sender, e) =>
                {
                    PreviousClick?.Invoke(sender: sender, e: e);
                });

            btnNext.Click += new EventHandler(
               (sender, e) =>
               {
                   NextClick?.Invoke(sender: sender, e: e);
               });

            chkReclass.CheckedChanged += new EventHandler(
                (sender, e) => { SelectCategory(); });

            tvwRasterCategories.AfterSelect += new TreeViewEventHandler(
                (sender, e) => { SelectCategory(); });
        }

        /// <summary>
        /// <para lang="cs">Nastavení hodnoty Configuration.Reclass</para>
        /// <para lang="en">Set the value of Configuration.Reclass</para>
        /// </summary>
        private void SelectCategory()
        {
            if (chkReclass.Checked)
            {
                grpSelectedRasterCategory.Visible = false;
                grpRasterCategories.Visible = false;
                grpReclass.Visible = false;

                lblSelectedRasterCategory.Text = String.Empty;
                lblReclass.Text = String.Empty;

                Configuration.Reclass = null;

                EnableNext();
                return;
            }

            if (SelectedRasterCategory == null)
            {
                grpSelectedRasterCategory.Visible = true;
                grpRasterCategories.Visible = true;
                grpReclass.Visible = true;

                lblSelectedRasterCategory.Text = String.Empty;
                lblReclass.Text = String.Empty;

                Configuration.Reclass = null;

                EnableNext();
                return;
            }

            grpSelectedRasterCategory.Visible = true;
            grpRasterCategories.Visible = true;
            grpReclass.Visible = true;

            RasterReclass reclass = new(
                rasterCategories: SelectedRasterCategory.Composite,
                rasterValue: SelectedRasterCategory.Id);

            lblSelectedRasterCategory.Text = (LanguageVersion == LanguageVersion.International)
                ? SelectedRasterCategory.ExtendedLabelEn
                : (LanguageVersion == LanguageVersion.National)
                    ? SelectedRasterCategory.ExtendedLabelCs
                    : SelectedRasterCategory.ExtendedLabelEn;

            lblReclass.Text =
                reclass.Command(
                    rast: Configuration?.ConfigCollection.ColumnName,
                    band: Configuration?.Band);

            Configuration.Reclass = SelectedRasterCategory?.Id;

            EnableNext();
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků ovládacího prvku</para>
        /// <para lang="en">Initialization of control labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            grpRaster.Text =
                labels.TryGetValue(key: nameof(grpRaster),
                out string grpRasterText)
                    ? grpRasterText
                    : String.Empty;

            lblRaster.Text = Configuration?.ConfigCollection?.ColumnName ?? String.Empty;

            chkReclass.Text =
               labels.TryGetValue(key: nameof(chkReclass),
               out string chkReclassText)
                   ? chkReclassText
                   : String.Empty;

            grpSelectedRasterCategory.Text =
                labels.TryGetValue(key: nameof(grpSelectedRasterCategory),
                out string grpSelectedRasterCategoryText)
                    ? grpSelectedRasterCategoryText
                    : String.Empty;

            grpRasterCategories.Text =
                labels.TryGetValue(key: nameof(grpRasterCategories),
                out string grpRasterCategoriesText)
                    ? grpRasterCategoriesText
                    : String.Empty;

            foreach (TreeNode nodeRasterCategory in tvwRasterCategories.Nodes.OfType<TreeNode>()
                .Where(a => a.Tag != null)
                .Where(a => a.Tag is RasterCategory))
            {
                nodeRasterCategory.Text = (LanguageVersion == LanguageVersion.International)
                        ? ((RasterCategory)nodeRasterCategory.Tag).ExtendedLabelEn
                        : (LanguageVersion == LanguageVersion.National)
                            ? ((RasterCategory)nodeRasterCategory.Tag).ExtendedLabelCs
                            : ((RasterCategory)nodeRasterCategory.Tag).ExtendedLabelEn;
            }

            grpReclass.Text =
                labels.TryGetValue(key: nameof(grpReclass),
                out string grpReclassText)
                    ? grpReclassText
                    : String.Empty;

            btnPrevious.Text =
                labels.TryGetValue(key: nameof(btnPrevious),
                out string btnPreviousText)
                    ? btnPreviousText
                    : String.Empty;

            btnNext.Text =
                labels.TryGetValue(key: nameof(btnNext),
                out string btnNextText)
                    ? btnNextText
                    : String.Empty;
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent()
        {
            tvwRasterCategories.Nodes.Clear();

            RasterCategoryList.SetName(
                schemaName: Configuration?.ConfigCollection?.SchemaName ?? String.Empty,
                tableName: Configuration?.ConfigCollection?.TableName ?? String.Empty);

            RasterCategoryList rasterCategoryList = new(database: Database);

            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            if (Database.Postgres.Catalog.Schemas[RasterCategoryList.Schema] == null)
            {
                // Schéma číselníku kategorií rastru neexistuje
                lblEmptyRasterCategories.Text =
                    labels.TryGetValue(key: nameof(lblEmptyRasterCategories),
                    out string lblEmptyRasterCategoriesText)
                    ? lblEmptyRasterCategoriesText
                        .Replace(oldValue: "$1", newValue: RasterCategoryList.Schema ?? String.Empty)
                        .Replace(oldValue: "$2", newValue: RasterCategoryList.Name ?? String.Empty)
                    : String.Empty;

                tvwRasterCategories.Visible = false;
                lblEmptyRasterCategories.Visible = true;

                EnableNext();
                return;
            }

            if ((Database.Postgres.Catalog.Schemas[RasterCategoryList.Schema].Tables[RasterCategoryList.Name] == null) &&
                (Database.Postgres.Catalog.Schemas[RasterCategoryList.Schema].ForeignTables[RasterCategoryList.Name] == null))
            {
                // Tabulka číselníku kategorií rastru neexistuje
                lblEmptyRasterCategories.Text =
                    labels.TryGetValue(key: nameof(lblEmptyRasterCategories),
                    out string lblEmptyRasterCategoriesText)
                    ? lblEmptyRasterCategoriesText
                        .Replace(oldValue: "$1", newValue: RasterCategoryList.Schema ?? String.Empty)
                        .Replace(oldValue: "$2", newValue: RasterCategoryList.Name ?? String.Empty)
                    : String.Empty;

                tvwRasterCategories.Visible = false;
                lblEmptyRasterCategories.Visible = true;

                EnableNext();
                return;
            }

            rasterCategoryList.ReLoad();

            if (rasterCategoryList.Data.Rows.Count == 0)
            {
                // Číselník kategorií rastru je prázdný
                lblEmptyRasterCategories.Text =
                    labels.TryGetValue(key: nameof(lblEmptyRasterCategories),
                    out string lblEmptyRasterCategoriesText)
                    ? lblEmptyRasterCategoriesText
                        .Replace(oldValue: "$1", newValue: RasterCategoryList.Schema ?? String.Empty)
                        .Replace(oldValue: "$2", newValue: RasterCategoryList.Name ?? String.Empty)
                    : String.Empty;

                tvwRasterCategories.Visible = false;
                lblEmptyRasterCategories.Visible = true;

                EnableNext();
                return;
            }

            IEnumerable<RasterCategory> rasterCategories =
                (LanguageVersion == LanguageVersion.International)
                    ? rasterCategoryList.Items.OrderBy(a => a.ExtendedLabelEn)
                    : (LanguageVersion == LanguageVersion.National)
                        ? rasterCategoryList.Items.OrderBy(a => a.ExtendedLabelCs)
                        : rasterCategoryList.Items.OrderBy(a => a.ExtendedLabelEn);

            foreach (RasterCategory rasterCategory in rasterCategories)
            {
                TreeNode nodeRasterCategory = new()
                {
                    ImageIndex = ImageGreenBall,
                    Name = rasterCategory.Id.ToString(),
                    SelectedImageIndex = ImageRedBall,
                    Tag = rasterCategory,
                    Text = (LanguageVersion == LanguageVersion.International)
                        ? rasterCategory.ExtendedLabelEn
                        : (LanguageVersion == LanguageVersion.National)
                            ? rasterCategory.ExtendedLabelCs
                            : rasterCategory.ExtendedLabelEn
                };
                tvwRasterCategories.Nodes.Add(node: nodeRasterCategory);
            }

            if (Configuration.Reclass != null)
            {
                chkReclass.Checked = false;
                SelectNode(id: (int)Configuration.Reclass);
            }
            else
            {
                chkReclass.Checked = true;
                SelectFirstNode();
            }

            tvwRasterCategories.Visible = true;
            lblEmptyRasterCategories.Visible = false;


            EnableNext();
        }

        /// <summary>
        /// <para lang="cs">Vybere první uzel v TreeView</para>
        /// <para lang="en">Selects first node in TreeView</para>
        /// </summary>
        private void SelectFirstNode()
        {
            if (tvwRasterCategories.Nodes.Count == 0)
            {
                return;
            }
            tvwRasterCategories.SelectedNode = tvwRasterCategories.Nodes[0];
        }

        /// <summary>
        /// <para lang="cs">
        /// Vybere uzel v TreeView odpovídající identifikačnímu číslu kategorie rastru
        /// </para>
        /// <para lang="en">
        /// Selects the TreeView node corresponding to raster category identifier
        /// </para>
        /// </summary>
        /// <param name="id">
        /// <para lang="cs">Identifikační číslo kategorie rastru</para>
        /// <para lang="en">Raster category identifier</para>
        /// </param>
        private void SelectNode(int id)
        {
            TreeNode node = FindNode(id: id);
            if (node == null)
            {
                return;
            }
            tvwRasterCategories.SelectedNode = node;
        }

        /// <summary>
        /// <para lang="cs">
        /// Vrátí uzel z TreeView odpovídající identifikačnímu číslu kategorie rastru
        /// </para>
        /// <para lang="en">
        /// Returns the TreeView node corresponding to the raster category identifier
        /// </para>
        /// </summary>
        /// <param name="id">
        /// <para lang="cs">Identifikační číslo kategorie rastru</para>
        /// <para lang="en">Raster category identifier</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">
        /// Vrátí uzel z TreeView odpovídající identifikačnímu číslu kategorie rastru
        /// </para>
        /// <para lang="en">
        /// Returns the TreeView node corresponding to the raster category identifier
        /// </para>
        /// </returns>
        private TreeNode FindNode(int id)
        {
            return
                tvwRasterCategories.Nodes.OfType<TreeNode>()
                .Where(a => a.Tag != null)
                .Where(a => a.Tag is RasterCategory)
                .Where(a => ((RasterCategory)a.Tag).Id == id)
                .FirstOrDefault<TreeNode>();
        }

        /// <summary>
        /// <para lang="cs">Povolí postup na další formulář</para>
        /// <para lang="en">Allows progression to the next form</para>
        /// </summary>
        private void EnableNext()
        {
            if (Configuration == null)
            {
                btnNext.Enabled = false;
                return;
            }

            if (chkReclass.Checked)
            {
                Configuration.Reclass = null;

                btnNext.Enabled =
                    !String.IsNullOrEmpty(Configuration.LabelCs) &&
                    !String.IsNullOrEmpty(Configuration.LabelEn) &&
                    !String.IsNullOrEmpty(Configuration.DescriptionCs) &&
                    !String.IsNullOrEmpty(Configuration.DescriptionEn) &&
                    (Configuration.ConfigQueryValue != ConfigQueryEnum.Unknown) &&
                    Configuration.Band != null &&
                    Configuration.Reclass == null;
            }

            else
            {
                btnNext.Enabled =
                    !String.IsNullOrEmpty(Configuration.LabelCs) &&
                    !String.IsNullOrEmpty(Configuration.LabelEn) &&
                    !String.IsNullOrEmpty(Configuration.DescriptionCs) &&
                    !String.IsNullOrEmpty(Configuration.DescriptionEn) &&
                    (Configuration.ConfigQueryValue != ConfigQueryEnum.Unknown) &&
                    Configuration.Band != null &&
                    Configuration.Reclass != null;
            }
        }

        #endregion Methods

    }
}
