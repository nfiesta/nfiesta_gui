﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi.ModuleAuxiliaryData
{
    partial class ControlRasterColumns
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ControlRasterColumns));
            tlpMain = new System.Windows.Forms.TableLayoutPanel();
            grpOption = new System.Windows.Forms.GroupBox();
            chkOption = new System.Windows.Forms.CheckBox();
            grpColumns = new System.Windows.Forms.GroupBox();
            pnlColumns = new System.Windows.Forms.Panel();
            tsrMain = new System.Windows.Forms.ToolStrip();
            btnAddColumn = new System.Windows.Forms.ToolStripButton();
            tlpButtons = new System.Windows.Forms.TableLayoutPanel();
            pnlNext = new System.Windows.Forms.Panel();
            btnNext = new System.Windows.Forms.Button();
            pnlPrevious = new System.Windows.Forms.Panel();
            btnPrevious = new System.Windows.Forms.Button();
            tlpMain.SuspendLayout();
            grpOption.SuspendLayout();
            grpColumns.SuspendLayout();
            tsrMain.SuspendLayout();
            tlpButtons.SuspendLayout();
            pnlNext.SuspendLayout();
            pnlPrevious.SuspendLayout();
            SuspendLayout();
            // 
            // tlpMain
            // 
            tlpMain.ColumnCount = 1;
            tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.Controls.Add(grpOption, 0, 2);
            tlpMain.Controls.Add(grpColumns, 0, 1);
            tlpMain.Controls.Add(tsrMain, 0, 0);
            tlpMain.Controls.Add(tlpButtons, 0, 3);
            tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpMain.Location = new System.Drawing.Point(0, 0);
            tlpMain.Margin = new System.Windows.Forms.Padding(0);
            tlpMain.Name = "tlpMain";
            tlpMain.RowCount = 4;
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpMain.Size = new System.Drawing.Size(960, 540);
            tlpMain.TabIndex = 3;
            // 
            // grpOption
            // 
            grpOption.Controls.Add(chkOption);
            grpOption.Dock = System.Windows.Forms.DockStyle.Fill;
            grpOption.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            grpOption.ForeColor = System.Drawing.Color.MediumBlue;
            grpOption.Location = new System.Drawing.Point(0, 440);
            grpOption.Margin = new System.Windows.Forms.Padding(0);
            grpOption.Name = "grpOption";
            grpOption.Padding = new System.Windows.Forms.Padding(5);
            grpOption.Size = new System.Drawing.Size(960, 60);
            grpOption.TabIndex = 5;
            grpOption.TabStop = false;
            grpOption.Text = "grpOption";
            // 
            // chkOption
            // 
            chkOption.AutoSize = true;
            chkOption.Dock = System.Windows.Forms.DockStyle.Fill;
            chkOption.Font = new System.Drawing.Font("Segoe UI", 9F);
            chkOption.ForeColor = System.Drawing.Color.Black;
            chkOption.Location = new System.Drawing.Point(5, 25);
            chkOption.Margin = new System.Windows.Forms.Padding(0);
            chkOption.Name = "chkOption";
            chkOption.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            chkOption.Size = new System.Drawing.Size(950, 30);
            chkOption.TabIndex = 3;
            chkOption.Text = "chkOption";
            chkOption.UseVisualStyleBackColor = true;
            // 
            // grpColumns
            // 
            grpColumns.Controls.Add(pnlColumns);
            grpColumns.Dock = System.Windows.Forms.DockStyle.Fill;
            grpColumns.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            grpColumns.ForeColor = System.Drawing.Color.MediumBlue;
            grpColumns.Location = new System.Drawing.Point(0, 25);
            grpColumns.Margin = new System.Windows.Forms.Padding(0);
            grpColumns.Name = "grpColumns";
            grpColumns.Padding = new System.Windows.Forms.Padding(5);
            grpColumns.Size = new System.Drawing.Size(960, 415);
            grpColumns.TabIndex = 0;
            grpColumns.TabStop = false;
            grpColumns.Text = "grpColumns";
            // 
            // pnlColumns
            // 
            pnlColumns.AutoScroll = true;
            pnlColumns.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlColumns.Location = new System.Drawing.Point(5, 25);
            pnlColumns.Margin = new System.Windows.Forms.Padding(0);
            pnlColumns.Name = "pnlColumns";
            pnlColumns.Size = new System.Drawing.Size(950, 385);
            pnlColumns.TabIndex = 0;
            // 
            // tsrMain
            // 
            tsrMain.Dock = System.Windows.Forms.DockStyle.Fill;
            tsrMain.GripMargin = new System.Windows.Forms.Padding(0);
            tsrMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { btnAddColumn });
            tsrMain.Location = new System.Drawing.Point(0, 0);
            tsrMain.Name = "tsrMain";
            tsrMain.Padding = new System.Windows.Forms.Padding(0);
            tsrMain.Size = new System.Drawing.Size(960, 25);
            tsrMain.TabIndex = 16;
            tsrMain.Text = "tsrMain";
            // 
            // btnAddColumn
            // 
            btnAddColumn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            btnAddColumn.Image = (System.Drawing.Image)resources.GetObject("btnAddColumn.Image");
            btnAddColumn.ImageTransparentColor = System.Drawing.Color.Magenta;
            btnAddColumn.Margin = new System.Windows.Forms.Padding(0);
            btnAddColumn.Name = "btnAddColumn";
            btnAddColumn.Size = new System.Drawing.Size(23, 25);
            btnAddColumn.Text = "btnAddColumn";
            // 
            // tlpButtons
            // 
            tlpButtons.BackColor = System.Drawing.Color.Transparent;
            tlpButtons.ColumnCount = 3;
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            tlpButtons.Controls.Add(pnlNext, 2, 0);
            tlpButtons.Controls.Add(pnlPrevious, 1, 0);
            tlpButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpButtons.Location = new System.Drawing.Point(0, 500);
            tlpButtons.Margin = new System.Windows.Forms.Padding(0);
            tlpButtons.Name = "tlpButtons";
            tlpButtons.RowCount = 1;
            tlpButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.Size = new System.Drawing.Size(960, 40);
            tlpButtons.TabIndex = 15;
            // 
            // pnlNext
            // 
            pnlNext.Controls.Add(btnNext);
            pnlNext.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlNext.Location = new System.Drawing.Point(800, 0);
            pnlNext.Margin = new System.Windows.Forms.Padding(0);
            pnlNext.Name = "pnlNext";
            pnlNext.Padding = new System.Windows.Forms.Padding(5);
            pnlNext.Size = new System.Drawing.Size(160, 40);
            pnlNext.TabIndex = 13;
            // 
            // btnNext
            // 
            btnNext.Dock = System.Windows.Forms.DockStyle.Fill;
            btnNext.Enabled = false;
            btnNext.ForeColor = System.Drawing.Color.Black;
            btnNext.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            btnNext.Location = new System.Drawing.Point(5, 5);
            btnNext.Margin = new System.Windows.Forms.Padding(0);
            btnNext.Name = "btnNext";
            btnNext.Size = new System.Drawing.Size(150, 30);
            btnNext.TabIndex = 9;
            btnNext.Text = "btnNext";
            btnNext.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            btnNext.UseVisualStyleBackColor = true;
            // 
            // pnlPrevious
            // 
            pnlPrevious.Controls.Add(btnPrevious);
            pnlPrevious.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlPrevious.Location = new System.Drawing.Point(640, 0);
            pnlPrevious.Margin = new System.Windows.Forms.Padding(0);
            pnlPrevious.Name = "pnlPrevious";
            pnlPrevious.Padding = new System.Windows.Forms.Padding(5);
            pnlPrevious.Size = new System.Drawing.Size(160, 40);
            pnlPrevious.TabIndex = 14;
            // 
            // btnPrevious
            // 
            btnPrevious.Dock = System.Windows.Forms.DockStyle.Fill;
            btnPrevious.ForeColor = System.Drawing.Color.Black;
            btnPrevious.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            btnPrevious.Location = new System.Drawing.Point(5, 5);
            btnPrevious.Margin = new System.Windows.Forms.Padding(0);
            btnPrevious.Name = "btnPrevious";
            btnPrevious.Size = new System.Drawing.Size(150, 30);
            btnPrevious.TabIndex = 9;
            btnPrevious.Text = "btnPrevious";
            btnPrevious.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            btnPrevious.UseVisualStyleBackColor = true;
            // 
            // ControlRasterColumns
            // 
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            BackColor = System.Drawing.Color.Transparent;
            BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            Controls.Add(tlpMain);
            ForeColor = System.Drawing.Color.Black;
            Margin = new System.Windows.Forms.Padding(0);
            Name = "ControlRasterColumns";
            Size = new System.Drawing.Size(960, 540);
            tlpMain.ResumeLayout(false);
            tlpMain.PerformLayout();
            grpOption.ResumeLayout(false);
            grpOption.PerformLayout();
            grpColumns.ResumeLayout(false);
            tsrMain.ResumeLayout(false);
            tsrMain.PerformLayout();
            tlpButtons.ResumeLayout(false);
            pnlNext.ResumeLayout(false);
            pnlPrevious.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.GroupBox grpOption;
        private System.Windows.Forms.CheckBox chkOption;
        private System.Windows.Forms.GroupBox grpColumns;
        private System.Windows.Forms.Panel pnlColumns;
        private System.Windows.Forms.ToolStrip tsrMain;
        private System.Windows.Forms.ToolStripButton btnAddColumn;
        private System.Windows.Forms.TableLayoutPanel tlpButtons;
        private System.Windows.Forms.Panel pnlNext;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Panel pnlPrevious;
        private System.Windows.Forms.Button btnPrevious;
    }
}
