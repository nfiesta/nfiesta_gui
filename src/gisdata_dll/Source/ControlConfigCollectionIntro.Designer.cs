﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi.ModuleAuxiliaryData
{

    partial class ControlConfigCollectionIntro
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            tlpMain = new System.Windows.Forms.TableLayoutPanel();
            pnlButtons = new System.Windows.Forms.Panel();
            tlpButtons = new System.Windows.Forms.TableLayoutPanel();
            pnlNext = new System.Windows.Forms.Panel();
            btnNext = new System.Windows.Forms.Button();
            pnlParams = new System.Windows.Forms.Panel();
            tlpParams = new System.Windows.Forms.TableLayoutPanel();
            grpConfigCollectionLabelCs = new System.Windows.Forms.GroupBox();
            txtConfigCollectionLabelCs = new System.Windows.Forms.TextBox();
            grpConfigCollectionDescriptionCs = new System.Windows.Forms.GroupBox();
            txtConfigCollectionDescriptionCs = new System.Windows.Forms.TextBox();
            pnlAuxiliaryVariable = new System.Windows.Forms.Panel();
            grpConfigFunction = new System.Windows.Forms.GroupBox();
            rdoFunctionVar600 = new System.Windows.Forms.RadioButton();
            rdoFunctionVar500 = new System.Windows.Forms.RadioButton();
            rdoFunctionVar400 = new System.Windows.Forms.RadioButton();
            rdoFunctionVar300 = new System.Windows.Forms.RadioButton();
            rdoFunctionVar200 = new System.Windows.Forms.RadioButton();
            rdoFunctionVar100 = new System.Windows.Forms.RadioButton();
            grpAggregated = new System.Windows.Forms.GroupBox();
            chkAggregated = new System.Windows.Forms.CheckBox();
            grpConfigCollectionLabelEn = new System.Windows.Forms.GroupBox();
            txtConfigCollectionLabelEn = new System.Windows.Forms.TextBox();
            grpConfigCollectionDescriptionEn = new System.Windows.Forms.GroupBox();
            txtConfigCollectionDescriptionEn = new System.Windows.Forms.TextBox();
            tlpMain.SuspendLayout();
            pnlButtons.SuspendLayout();
            tlpButtons.SuspendLayout();
            pnlNext.SuspendLayout();
            pnlParams.SuspendLayout();
            tlpParams.SuspendLayout();
            grpConfigCollectionLabelCs.SuspendLayout();
            grpConfigCollectionDescriptionCs.SuspendLayout();
            grpConfigFunction.SuspendLayout();
            grpAggregated.SuspendLayout();
            grpConfigCollectionLabelEn.SuspendLayout();
            grpConfigCollectionDescriptionEn.SuspendLayout();
            SuspendLayout();
            // 
            // tlpMain
            // 
            tlpMain.ColumnCount = 1;
            tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.Controls.Add(pnlButtons, 0, 1);
            tlpMain.Controls.Add(pnlParams, 0, 0);
            tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpMain.Location = new System.Drawing.Point(0, 0);
            tlpMain.Margin = new System.Windows.Forms.Padding(0);
            tlpMain.Name = "tlpMain";
            tlpMain.RowCount = 2;
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpMain.Size = new System.Drawing.Size(960, 540);
            tlpMain.TabIndex = 3;
            // 
            // pnlButtons
            // 
            pnlButtons.Controls.Add(tlpButtons);
            pnlButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlButtons.Location = new System.Drawing.Point(0, 500);
            pnlButtons.Margin = new System.Windows.Forms.Padding(0);
            pnlButtons.Name = "pnlButtons";
            pnlButtons.Size = new System.Drawing.Size(960, 40);
            pnlButtons.TabIndex = 8;
            // 
            // tlpButtons
            // 
            tlpButtons.BackColor = System.Drawing.Color.Transparent;
            tlpButtons.ColumnCount = 3;
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            tlpButtons.Controls.Add(pnlNext, 2, 0);
            tlpButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpButtons.Location = new System.Drawing.Point(0, 0);
            tlpButtons.Margin = new System.Windows.Forms.Padding(0);
            tlpButtons.Name = "tlpButtons";
            tlpButtons.RowCount = 1;
            tlpButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.Size = new System.Drawing.Size(960, 40);
            tlpButtons.TabIndex = 14;
            // 
            // pnlNext
            // 
            pnlNext.Controls.Add(btnNext);
            pnlNext.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlNext.Location = new System.Drawing.Point(800, 0);
            pnlNext.Margin = new System.Windows.Forms.Padding(0);
            pnlNext.Name = "pnlNext";
            pnlNext.Padding = new System.Windows.Forms.Padding(5);
            pnlNext.Size = new System.Drawing.Size(160, 40);
            pnlNext.TabIndex = 13;
            // 
            // btnNext
            // 
            btnNext.Dock = System.Windows.Forms.DockStyle.Fill;
            btnNext.Enabled = false;
            btnNext.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            btnNext.Location = new System.Drawing.Point(5, 5);
            btnNext.Margin = new System.Windows.Forms.Padding(0);
            btnNext.Name = "btnNext";
            btnNext.Size = new System.Drawing.Size(150, 30);
            btnNext.TabIndex = 9;
            btnNext.Text = "btnNext";
            btnNext.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            btnNext.UseVisualStyleBackColor = true;
            // 
            // pnlParams
            // 
            pnlParams.AutoScroll = true;
            pnlParams.Controls.Add(tlpParams);
            pnlParams.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlParams.Location = new System.Drawing.Point(0, 0);
            pnlParams.Margin = new System.Windows.Forms.Padding(0);
            pnlParams.Name = "pnlParams";
            pnlParams.Size = new System.Drawing.Size(960, 500);
            pnlParams.TabIndex = 9;
            // 
            // tlpParams
            // 
            tlpParams.ColumnCount = 1;
            tlpParams.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpParams.Controls.Add(grpConfigCollectionDescriptionCs, 0, 2);
            tlpParams.Controls.Add(grpConfigCollectionLabelCs, 0, 0);
            tlpParams.Controls.Add(pnlAuxiliaryVariable, 0, 6);
            tlpParams.Controls.Add(grpConfigFunction, 0, 4);
            tlpParams.Controls.Add(grpAggregated, 0, 5);
            tlpParams.Controls.Add(grpConfigCollectionDescriptionEn, 0, 3);
            tlpParams.Controls.Add(grpConfigCollectionLabelEn, 0, 1);
            tlpParams.Dock = System.Windows.Forms.DockStyle.Top;
            tlpParams.Location = new System.Drawing.Point(0, 0);
            tlpParams.Margin = new System.Windows.Forms.Padding(0);
            tlpParams.Name = "tlpParams";
            tlpParams.RowCount = 8;
            tlpParams.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            tlpParams.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            tlpParams.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            tlpParams.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            tlpParams.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            tlpParams.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            tlpParams.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            tlpParams.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpParams.Size = new System.Drawing.Size(943, 620);
            tlpParams.TabIndex = 1;
            // 
            // grpConfigCollectionLabelCs
            // 
            grpConfigCollectionLabelCs.Controls.Add(txtConfigCollectionLabelCs);
            grpConfigCollectionLabelCs.Dock = System.Windows.Forms.DockStyle.Fill;
            grpConfigCollectionLabelCs.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            grpConfigCollectionLabelCs.ForeColor = System.Drawing.Color.MediumBlue;
            grpConfigCollectionLabelCs.Location = new System.Drawing.Point(0, 0);
            grpConfigCollectionLabelCs.Margin = new System.Windows.Forms.Padding(0);
            grpConfigCollectionLabelCs.Name = "grpConfigCollectionLabelCs";
            grpConfigCollectionLabelCs.Padding = new System.Windows.Forms.Padding(5);
            grpConfigCollectionLabelCs.Size = new System.Drawing.Size(943, 60);
            grpConfigCollectionLabelCs.TabIndex = 11;
            grpConfigCollectionLabelCs.TabStop = false;
            grpConfigCollectionLabelCs.Text = "grpConfigCollectionLabelCs";
            // 
            // txtConfigCollectionLabelCs
            // 
            txtConfigCollectionLabelCs.Dock = System.Windows.Forms.DockStyle.Fill;
            txtConfigCollectionLabelCs.Font = new System.Drawing.Font("Segoe UI", 9F);
            txtConfigCollectionLabelCs.ForeColor = System.Drawing.Color.Black;
            txtConfigCollectionLabelCs.Location = new System.Drawing.Point(5, 25);
            txtConfigCollectionLabelCs.Margin = new System.Windows.Forms.Padding(0);
            txtConfigCollectionLabelCs.Name = "txtConfigCollectionLabelCs";
            txtConfigCollectionLabelCs.Size = new System.Drawing.Size(933, 23);
            txtConfigCollectionLabelCs.TabIndex = 1;
            // 
            // grpConfigCollectionDescriptionCs
            // 
            grpConfigCollectionDescriptionCs.Controls.Add(txtConfigCollectionDescriptionCs);
            grpConfigCollectionDescriptionCs.Dock = System.Windows.Forms.DockStyle.Fill;
            grpConfigCollectionDescriptionCs.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            grpConfigCollectionDescriptionCs.ForeColor = System.Drawing.Color.MediumBlue;
            grpConfigCollectionDescriptionCs.Location = new System.Drawing.Point(0, 120);
            grpConfigCollectionDescriptionCs.Margin = new System.Windows.Forms.Padding(0);
            grpConfigCollectionDescriptionCs.Name = "grpConfigCollectionDescriptionCs";
            grpConfigCollectionDescriptionCs.Padding = new System.Windows.Forms.Padding(5);
            grpConfigCollectionDescriptionCs.Size = new System.Drawing.Size(943, 60);
            grpConfigCollectionDescriptionCs.TabIndex = 12;
            grpConfigCollectionDescriptionCs.TabStop = false;
            grpConfigCollectionDescriptionCs.Text = "grpConfigCollectionDescriptionCs";
            // 
            // txtConfigCollectionDescriptionCs
            // 
            txtConfigCollectionDescriptionCs.Dock = System.Windows.Forms.DockStyle.Fill;
            txtConfigCollectionDescriptionCs.Font = new System.Drawing.Font("Segoe UI", 9F);
            txtConfigCollectionDescriptionCs.ForeColor = System.Drawing.Color.Black;
            txtConfigCollectionDescriptionCs.Location = new System.Drawing.Point(5, 25);
            txtConfigCollectionDescriptionCs.Margin = new System.Windows.Forms.Padding(0);
            txtConfigCollectionDescriptionCs.Name = "txtConfigCollectionDescriptionCs";
            txtConfigCollectionDescriptionCs.Size = new System.Drawing.Size(933, 23);
            txtConfigCollectionDescriptionCs.TabIndex = 1;
            // 
            // pnlAuxiliaryVariable
            // 
            pnlAuxiliaryVariable.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlAuxiliaryVariable.Location = new System.Drawing.Point(0, 450);
            pnlAuxiliaryVariable.Margin = new System.Windows.Forms.Padding(0);
            pnlAuxiliaryVariable.Name = "pnlAuxiliaryVariable";
            pnlAuxiliaryVariable.Size = new System.Drawing.Size(943, 120);
            pnlAuxiliaryVariable.TabIndex = 25;
            // 
            // grpConfigFunction
            // 
            grpConfigFunction.Controls.Add(rdoFunctionVar600);
            grpConfigFunction.Controls.Add(rdoFunctionVar500);
            grpConfigFunction.Controls.Add(rdoFunctionVar400);
            grpConfigFunction.Controls.Add(rdoFunctionVar300);
            grpConfigFunction.Controls.Add(rdoFunctionVar200);
            grpConfigFunction.Controls.Add(rdoFunctionVar100);
            grpConfigFunction.Dock = System.Windows.Forms.DockStyle.Fill;
            grpConfigFunction.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            grpConfigFunction.ForeColor = System.Drawing.Color.MediumBlue;
            grpConfigFunction.Location = new System.Drawing.Point(0, 240);
            grpConfigFunction.Margin = new System.Windows.Forms.Padding(0);
            grpConfigFunction.Name = "grpConfigFunction";
            grpConfigFunction.Padding = new System.Windows.Forms.Padding(5);
            grpConfigFunction.Size = new System.Drawing.Size(943, 150);
            grpConfigFunction.TabIndex = 26;
            grpConfigFunction.TabStop = false;
            grpConfigFunction.Text = "grpConfigFunction";
            // 
            // rdoFunctionVar600
            // 
            rdoFunctionVar600.AutoSize = true;
            rdoFunctionVar600.Font = new System.Drawing.Font("Segoe UI", 9F);
            rdoFunctionVar600.ForeColor = System.Drawing.Color.Black;
            rdoFunctionVar600.Location = new System.Drawing.Point(422, 57);
            rdoFunctionVar600.Margin = new System.Windows.Forms.Padding(0);
            rdoFunctionVar600.Name = "rdoFunctionVar600";
            rdoFunctionVar600.Size = new System.Drawing.Size(124, 19);
            rdoFunctionVar600.TabIndex = 5;
            rdoFunctionVar600.Text = "rdoFunctionVar600";
            rdoFunctionVar600.UseVisualStyleBackColor = true;
            // 
            // rdoFunctionVar500
            // 
            rdoFunctionVar500.AutoSize = true;
            rdoFunctionVar500.Font = new System.Drawing.Font("Segoe UI", 9F);
            rdoFunctionVar500.ForeColor = System.Drawing.Color.Black;
            rdoFunctionVar500.Location = new System.Drawing.Point(422, 27);
            rdoFunctionVar500.Margin = new System.Windows.Forms.Padding(0);
            rdoFunctionVar500.Name = "rdoFunctionVar500";
            rdoFunctionVar500.Size = new System.Drawing.Size(124, 19);
            rdoFunctionVar500.TabIndex = 4;
            rdoFunctionVar500.Text = "rdoFunctionVar500";
            rdoFunctionVar500.UseVisualStyleBackColor = true;
            // 
            // rdoFunctionVar400
            // 
            rdoFunctionVar400.AutoSize = true;
            rdoFunctionVar400.Font = new System.Drawing.Font("Segoe UI", 9F);
            rdoFunctionVar400.ForeColor = System.Drawing.Color.Black;
            rdoFunctionVar400.Location = new System.Drawing.Point(17, 117);
            rdoFunctionVar400.Margin = new System.Windows.Forms.Padding(0);
            rdoFunctionVar400.Name = "rdoFunctionVar400";
            rdoFunctionVar400.Size = new System.Drawing.Size(124, 19);
            rdoFunctionVar400.TabIndex = 3;
            rdoFunctionVar400.Text = "rdoFunctionVar400";
            rdoFunctionVar400.UseVisualStyleBackColor = true;
            // 
            // rdoFunctionVar300
            // 
            rdoFunctionVar300.AutoSize = true;
            rdoFunctionVar300.Font = new System.Drawing.Font("Segoe UI", 9F);
            rdoFunctionVar300.ForeColor = System.Drawing.Color.Black;
            rdoFunctionVar300.Location = new System.Drawing.Point(17, 87);
            rdoFunctionVar300.Margin = new System.Windows.Forms.Padding(0);
            rdoFunctionVar300.Name = "rdoFunctionVar300";
            rdoFunctionVar300.Size = new System.Drawing.Size(124, 19);
            rdoFunctionVar300.TabIndex = 2;
            rdoFunctionVar300.Text = "rdoFunctionVar300";
            rdoFunctionVar300.UseVisualStyleBackColor = true;
            // 
            // rdoFunctionVar200
            // 
            rdoFunctionVar200.AutoSize = true;
            rdoFunctionVar200.Font = new System.Drawing.Font("Segoe UI", 9F);
            rdoFunctionVar200.ForeColor = System.Drawing.Color.Black;
            rdoFunctionVar200.Location = new System.Drawing.Point(17, 57);
            rdoFunctionVar200.Margin = new System.Windows.Forms.Padding(0);
            rdoFunctionVar200.Name = "rdoFunctionVar200";
            rdoFunctionVar200.Size = new System.Drawing.Size(124, 19);
            rdoFunctionVar200.TabIndex = 1;
            rdoFunctionVar200.Text = "rdoFunctionVar200";
            rdoFunctionVar200.UseVisualStyleBackColor = true;
            // 
            // rdoFunctionVar100
            // 
            rdoFunctionVar100.AutoSize = true;
            rdoFunctionVar100.Checked = true;
            rdoFunctionVar100.Font = new System.Drawing.Font("Segoe UI", 9F);
            rdoFunctionVar100.ForeColor = System.Drawing.Color.Black;
            rdoFunctionVar100.Location = new System.Drawing.Point(17, 27);
            rdoFunctionVar100.Margin = new System.Windows.Forms.Padding(0);
            rdoFunctionVar100.Name = "rdoFunctionVar100";
            rdoFunctionVar100.Size = new System.Drawing.Size(124, 19);
            rdoFunctionVar100.TabIndex = 0;
            rdoFunctionVar100.TabStop = true;
            rdoFunctionVar100.Text = "rdoFunctionVar100";
            rdoFunctionVar100.UseVisualStyleBackColor = true;
            // 
            // grpAggregated
            // 
            grpAggregated.Controls.Add(chkAggregated);
            grpAggregated.Dock = System.Windows.Forms.DockStyle.Fill;
            grpAggregated.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            grpAggregated.ForeColor = System.Drawing.Color.MediumBlue;
            grpAggregated.Location = new System.Drawing.Point(0, 390);
            grpAggregated.Margin = new System.Windows.Forms.Padding(0);
            grpAggregated.Name = "grpAggregated";
            grpAggregated.Padding = new System.Windows.Forms.Padding(5);
            grpAggregated.Size = new System.Drawing.Size(943, 60);
            grpAggregated.TabIndex = 27;
            grpAggregated.TabStop = false;
            grpAggregated.Text = "grpAggregated";
            // 
            // chkAggregated
            // 
            chkAggregated.AutoSize = true;
            chkAggregated.Font = new System.Drawing.Font("Segoe UI", 9F);
            chkAggregated.ForeColor = System.Drawing.Color.Black;
            chkAggregated.Location = new System.Drawing.Point(17, 27);
            chkAggregated.Margin = new System.Windows.Forms.Padding(0);
            chkAggregated.Name = "chkAggregated";
            chkAggregated.Size = new System.Drawing.Size(107, 19);
            chkAggregated.TabIndex = 0;
            chkAggregated.Text = "chkAggregated";
            chkAggregated.UseVisualStyleBackColor = true;
            // 
            // grpConfigCollectionLabelEn
            // 
            grpConfigCollectionLabelEn.Controls.Add(txtConfigCollectionLabelEn);
            grpConfigCollectionLabelEn.Dock = System.Windows.Forms.DockStyle.Fill;
            grpConfigCollectionLabelEn.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            grpConfigCollectionLabelEn.ForeColor = System.Drawing.Color.MediumBlue;
            grpConfigCollectionLabelEn.Location = new System.Drawing.Point(0, 60);
            grpConfigCollectionLabelEn.Margin = new System.Windows.Forms.Padding(0);
            grpConfigCollectionLabelEn.Name = "grpConfigCollectionLabelEn";
            grpConfigCollectionLabelEn.Padding = new System.Windows.Forms.Padding(5);
            grpConfigCollectionLabelEn.Size = new System.Drawing.Size(943, 60);
            grpConfigCollectionLabelEn.TabIndex = 28;
            grpConfigCollectionLabelEn.TabStop = false;
            grpConfigCollectionLabelEn.Text = "grpConfigCollectionLabelEn";
            // 
            // txtConfigCollectionLabelEn
            // 
            txtConfigCollectionLabelEn.Dock = System.Windows.Forms.DockStyle.Fill;
            txtConfigCollectionLabelEn.Font = new System.Drawing.Font("Segoe UI", 9F);
            txtConfigCollectionLabelEn.ForeColor = System.Drawing.Color.Black;
            txtConfigCollectionLabelEn.Location = new System.Drawing.Point(5, 25);
            txtConfigCollectionLabelEn.Margin = new System.Windows.Forms.Padding(0);
            txtConfigCollectionLabelEn.Name = "txtConfigCollectionLabelEn";
            txtConfigCollectionLabelEn.Size = new System.Drawing.Size(933, 23);
            txtConfigCollectionLabelEn.TabIndex = 1;
            // 
            // grpConfigCollectionDescriptionEn
            // 
            grpConfigCollectionDescriptionEn.Controls.Add(txtConfigCollectionDescriptionEn);
            grpConfigCollectionDescriptionEn.Dock = System.Windows.Forms.DockStyle.Fill;
            grpConfigCollectionDescriptionEn.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            grpConfigCollectionDescriptionEn.ForeColor = System.Drawing.Color.MediumBlue;
            grpConfigCollectionDescriptionEn.Location = new System.Drawing.Point(0, 180);
            grpConfigCollectionDescriptionEn.Margin = new System.Windows.Forms.Padding(0);
            grpConfigCollectionDescriptionEn.Name = "grpConfigCollectionDescriptionEn";
            grpConfigCollectionDescriptionEn.Padding = new System.Windows.Forms.Padding(5);
            grpConfigCollectionDescriptionEn.Size = new System.Drawing.Size(943, 60);
            grpConfigCollectionDescriptionEn.TabIndex = 29;
            grpConfigCollectionDescriptionEn.TabStop = false;
            grpConfigCollectionDescriptionEn.Text = "grpConfigCollectionDescriptionEn";
            // 
            // txtConfigCollectionDescriptionEn
            // 
            txtConfigCollectionDescriptionEn.Dock = System.Windows.Forms.DockStyle.Fill;
            txtConfigCollectionDescriptionEn.Font = new System.Drawing.Font("Segoe UI", 9F);
            txtConfigCollectionDescriptionEn.ForeColor = System.Drawing.Color.Black;
            txtConfigCollectionDescriptionEn.Location = new System.Drawing.Point(5, 25);
            txtConfigCollectionDescriptionEn.Margin = new System.Windows.Forms.Padding(0);
            txtConfigCollectionDescriptionEn.Name = "txtConfigCollectionDescriptionEn";
            txtConfigCollectionDescriptionEn.Size = new System.Drawing.Size(933, 23);
            txtConfigCollectionDescriptionEn.TabIndex = 1;
            // 
            // ControlConfigCollectionIntro
            // 
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            BackColor = System.Drawing.Color.Transparent;
            BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            Controls.Add(tlpMain);
            ForeColor = System.Drawing.Color.Black;
            Margin = new System.Windows.Forms.Padding(0);
            Name = "ControlConfigCollectionIntro";
            Size = new System.Drawing.Size(960, 540);
            tlpMain.ResumeLayout(false);
            pnlButtons.ResumeLayout(false);
            tlpButtons.ResumeLayout(false);
            pnlNext.ResumeLayout(false);
            pnlParams.ResumeLayout(false);
            tlpParams.ResumeLayout(false);
            grpConfigCollectionLabelCs.ResumeLayout(false);
            grpConfigCollectionLabelCs.PerformLayout();
            grpConfigCollectionDescriptionCs.ResumeLayout(false);
            grpConfigCollectionDescriptionCs.PerformLayout();
            grpConfigFunction.ResumeLayout(false);
            grpConfigFunction.PerformLayout();
            grpAggregated.ResumeLayout(false);
            grpAggregated.PerformLayout();
            grpConfigCollectionLabelEn.ResumeLayout(false);
            grpConfigCollectionLabelEn.PerformLayout();
            grpConfigCollectionDescriptionEn.ResumeLayout(false);
            grpConfigCollectionDescriptionEn.PerformLayout();
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.Panel pnlButtons;
        private System.Windows.Forms.TableLayoutPanel tlpButtons;
        private System.Windows.Forms.Panel pnlNext;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Panel pnlParams;
        private System.Windows.Forms.TableLayoutPanel tlpParams;
        private System.Windows.Forms.GroupBox grpConfigCollectionLabelCs;
        private System.Windows.Forms.TextBox txtConfigCollectionLabelCs;
        private System.Windows.Forms.GroupBox grpConfigCollectionDescriptionCs;
        private System.Windows.Forms.TextBox txtConfigCollectionDescriptionCs;
        private System.Windows.Forms.Panel pnlAuxiliaryVariable;
        private System.Windows.Forms.GroupBox grpConfigFunction;
        private System.Windows.Forms.RadioButton rdoFunctionVar600;
        private System.Windows.Forms.RadioButton rdoFunctionVar500;
        private System.Windows.Forms.RadioButton rdoFunctionVar400;
        private System.Windows.Forms.RadioButton rdoFunctionVar300;
        private System.Windows.Forms.RadioButton rdoFunctionVar200;
        private System.Windows.Forms.RadioButton rdoFunctionVar100;
        private System.Windows.Forms.GroupBox grpAggregated;
        private System.Windows.Forms.CheckBox chkAggregated;
        private System.Windows.Forms.GroupBox grpConfigCollectionDescriptionEn;
        private System.Windows.Forms.TextBox txtConfigCollectionDescriptionEn;
        private System.Windows.Forms.GroupBox grpConfigCollectionLabelEn;
        private System.Windows.Forms.TextBox txtConfigCollectionLabelEn;
    }

}
