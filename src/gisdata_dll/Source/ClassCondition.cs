﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Linq;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace ModuleAuxiliaryData
    {

        /// <summary>
        /// <para lang="cs">
        /// Výběrová podmínka ve tvaru:
        /// ((TablePrefix1.ColumnName1 = ColumnValue1) AND (TablePrefix2.ColumnName2 = ColumnValue2))
        /// </para>
        /// <para lang="en">
        /// Selection condition in the form:
        /// ((TablePrefix1.ColumnName1 = ColumnValue1) AND (TablePrefix2.ColumnName2 = ColumnValue2))
        /// </para>
        /// </summary>
        internal class Condition
        {

            #region Private Fields

            /// <summary>
            /// <para lang="cs">Seznam vnitřních částí podmínky</para>
            /// <para lang="en">List of inner parts of the condition</para>
            /// </summary>
            private SortedList<string, InnerCondition> parts;

            #endregion Private Fields


            #region Constructors

            /// <summary>
            /// <para lang="cs">Konstruktor</para>
            /// <para lang="en">Constructor</para>
            /// </summary>
            public Condition()
            {
                Parts = [];
            }

            /// <summary>
            /// <para lang="cs">Konstruktor</para>
            /// <para lang="en">Constructor</para>
            /// </summary>
            /// <param name="parts">
            /// <para lang="cs">Seznam vnitřních částí podmínky</para>
            /// <para lang="en">List of inner parts of the condition</para>
            /// </param>
            public Condition(SortedList<string, InnerCondition> parts)
            {
                Parts = parts;
            }

            /// <summary>
            /// <para lang="cs">Konstruktor</para>
            /// <para lang="en">Constructor</para>
            /// </summary>
            /// <param name="text">
            /// <para lang="cs">Text podmínky</para>
            /// <para lang="en">Condition text</para>
            /// </param>
            public Condition(string text)
            {
                Parts = [];

                if (!String.IsNullOrEmpty(value: text))
                {
                    string[] separators = ["AND"];
                    string[] strParts =
                        text.Split(
                            separator: separators,
                            options: StringSplitOptions.RemoveEmptyEntries);

                    foreach (string strPart in strParts)
                    {
                        AddPart(part: new InnerCondition(text: strPart));
                    }
                }
            }

            #endregion Constructors


            #region Properties

            /// <summary>
            /// <para lang="cs">Seznam vnitřních částí podmínky (read-only)</para>
            /// <para lang="en">List of inner parts of the condition (read-only)</para>
            /// </summary>
            public SortedList<string, InnerCondition> Parts
            {
                get
                {
                    return parts ?? [];
                }
                private set
                {
                    parts = value ?? [];
                }
            }

            /// <summary>
            /// <para lang="cs">Seznam jmen databázových sloupců v podmínce (read-only)</para>
            /// <para lang="en">List of database column names in condition (read-only)</para>
            /// </summary>
            public List<string> ColumnNames
            {
                get
                {
                    return [.. Parts.Keys];
                }
            }

            /// <summary>
            /// <para lang="cs">Prázdná podmínka (read-only)</para>
            /// <para lang="en">Empty condition (read-only)</para>
            /// </summary>
            public bool IsEmpty
            {
                get
                {
                    return
                        !Parts.Values
                        .Where(a => !String.IsNullOrEmpty(value: a.Text))
                        .Any();
                }
            }

            /// <summary>
            /// <para lang="cs">Text podmínky (read-only)</para>
            /// <para lang="en">Condition text (read-only)</para>
            /// </summary>
            public string Text
            {
                get
                {
                    return
                        Parts.Values
                        .Where(a => !String.IsNullOrEmpty(value: a.Text))
                        .Count() switch
                        {
                            0 => null,
                            1 => Parts.Values
                                    .Where(a => !String.IsNullOrEmpty(value: a.Text))
                                    .Select(a => a.Text)
                                    .Aggregate((a, b) => $"{a} AND {b}"),
                            _ => String.Concat("(",
                                Parts.Values
                                    .Where(a => !String.IsNullOrEmpty(value: a.Text))
                                    .Select(a => a.Text)
                                    .Aggregate((a, b) => $"{a} AND {b}"),
                                ")"),
                        };
                }
            }

            #endregion Properties


            #region Methods

            /// <summary>
            /// <para lang="cs">Přidá vnitřní část do podmínky</para>
            /// <para lang="en">Adds the inner part to the condition</para>
            /// </summary>
            /// <param name="part">
            /// <para lang="cs">Vnitřní část podmínky</para>
            /// <para lang="en">Inner part of the condition</para>
            /// </param>
            public void AddPart(InnerCondition part)
            {
                if (!Parts.ContainsKey(part.ColumnName))
                {
                    Parts.Add(
                        key: part.ColumnName,
                        value: part);
                }
            }

            /// <summary>
            /// <para lang="cs">Vrací řetězec, který představuje aktuální objekt</para>
            /// <para lang="en">Returns the string that represents current object</para>
            /// </summary>
            /// <returns>
            /// <para lang="cs">Vrací řetězec, který představuje aktuální objekt</para>
            /// <para lang="en">Returns the string that represents current object</para>
            /// </returns>
            public override string ToString()
            {
                return Text;
            }

            #endregion Methods

        }

        /// <summary>
        /// <para lang="cs">
        /// Vnitřní část podmínky ve tvaru:
        /// (TablePrefix.ColumnName = ColumnValue)
        /// </para>
        /// <para lang="en">
        /// Inner part of the condition in the form:
        /// (TablePrefix.ColumnName = ColumnValue)
        /// </para>
        /// </summary>
        internal class InnerCondition
        {

            #region Private Fields

            /// <summary>
            /// <para lang="cs">Text vnitřní části podmínky</para>
            /// <para lang="en">Text of the inner part of the condition</para>
            /// </summary>
            private string text;

            #endregion Private Fields


            #region Constructors

            /// <summary>
            /// <para lang="cs">Konstruktor</para>
            /// <para lang="en">Constructor</para>
            /// </summary>
            /// <param name="columnName">
            /// <para lang="cs">Jméno databázového sloupce</para>
            /// <para lang="en">Database column name</para>
            /// </param>
            /// <param name="columnValue">
            /// <para lang="cs">Hodnota v databázovém sloupci</para>
            /// <para lang="en">Value in database column</para>
            /// </param>
            /// <param name="isEqual">
            /// <para lang="cs">Podmínka je rovnost</para>
            /// <para lang="en">Condition is equality</para>
            /// </param>
            public InnerCondition(string columnName, string columnValue, bool isEqual)
            {
                Text = PrepareConditionText(
                    tablePrefix: String.Empty,
                    columnName: columnName,
                    columnValue: columnValue,
                    isEqual: isEqual);
            }

            /// <summary>
            /// <para lang="cs">Konstruktor</para>
            /// <para lang="en">Constructor</para>
            /// </summary>
            /// <param name="tablePrefix">
            /// <para lang="cs">Předpona pro tabulku</para>
            /// <para lang="en">Table prefix</para>
            /// </param>
            /// <param name="columnName">
            /// <para lang="cs">Jméno databázového sloupce</para>
            /// <para lang="en">Database column name</para>
            /// </param>
            /// <param name="columnValue">
            /// <para lang="cs">Hodnota v databázovém sloupci</para>
            /// <para lang="en">Value in database column</para>
            /// </param>
            /// <param name="isEqual">
            /// <para lang="cs">Podmínka je rovnost</para>
            /// <para lang="en">Condition is equality</para>
            /// </param>
            public InnerCondition(string tablePrefix, string columnName, string columnValue, bool isEqual)
            {
                Text = PrepareConditionText(
                     tablePrefix: tablePrefix,
                     columnName: columnName,
                     columnValue: columnValue,
                     isEqual: isEqual);
            }

            /// <summary>
            /// <para lang="cs">Konstruktor</para>
            /// <para lang="en">Constructor</para>
            /// </summary>
            /// <param name="text">
            /// <para lang="cs">Text vnitřní části podmínky</para>
            /// <para lang="en">Text of the inner part of the condition</para>
            /// </param>
            public InnerCondition(string text)
            {
                string tablePrefix = GetTablePrefix(val: text);
                string columnName = GetColumnName(val: text);
                string columnValue = GetColumnValue(val: text);
                bool isEqual = GetIsEqual(val: text);

                Text = PrepareConditionText(
                    tablePrefix: tablePrefix,
                    columnName: columnName,
                    columnValue: columnValue,
                    isEqual: isEqual);
            }

            #endregion Constructors


            #region Properties

            /// <summary>
            /// <para lang="cs">Text vnitřní části podmínky (read-only)</para>
            /// <para lang="en">Text of the inner part of the condition (read-only)</para>
            /// </summary>
            public string Text
            {
                get
                {
                    return text;
                }
                private set
                {
                    text = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Předpona pro tabulku (read-only)</para>
            /// <para lang="en">Table prefix (read-only)</para>
            /// </summary>
            public string TablePrefix
            {
                get
                {
                    return GetTablePrefix(val: Text);
                }
            }

            /// <summary>
            /// <para lang="cs">Jméno databázového sloupce (read-only)</para>
            /// <para lang="en">Database column name (read-only)</para>
            /// </summary>
            public string ColumnName
            {
                get
                {
                    return GetColumnName(val: Text);
                }
            }

            /// <summary>
            /// <para lang="cs">Hodnota v databázovém sloupci (read-only)</para>
            /// <para lang="en">Value in database column (read-only)</para>
            /// </summary>
            public string ColumnValue
            {
                get
                {
                    return GetColumnValue(val: Text);
                }
            }

            /// <summary>
            /// <para lang="cs">Podmínka je rovnost (read-only)</para>
            /// <para lang="en">Condition is equality (read-only)</para>
            /// </summary>
            public bool IsEqual
            {
                get
                {
                    return GetIsEqual(val: text);
                }
            }

            /// <summary>
            /// <para lang="cs">Hodnota v databázovém sloupci je číslo (read-only)</para>
            /// <para lang="en">Value in database column is numeric (read-only)</para>
            /// </summary>
            public bool IsNumeric
            {
                get
                {
                    return Functions.IsDouble(text: ColumnValue);
                }
            }

            #endregion Properties


            #region Methods

            /// <summary>
            /// <para lang="cs">Předpona pro tabulku z textu vnitřní části podmínky</para>
            /// <para lang="en">Table prefix from text of the inner part of the condition</para>
            /// </summary>
            /// <param name="val">
            /// <para lang="cs">Text vnitřní části podmínky</para>
            /// <para lang="en">Text of the inner part of the condition</para>
            /// </param>
            /// <returns>
            /// <para lang="cs">Vrací předponu pro tabulku z textu vnitřní části podmínky</para>
            /// <para lang="en">Returns table prefix from text of the inner part of the condition</para>
            /// </returns>
            private static string GetTablePrefix(string val)
            {
                char[] tableSeparator = ['.'];
                char[] valueSeparator = ['='];

                val = val
                    .Trim()
                    .Replace(oldValue: "(", newValue: String.Empty)
                    .Replace(oldValue: ")", newValue: String.Empty)
                    .Replace(oldValue: "!", newValue: String.Empty)
                    .Replace(oldValue: "'", newValue: String.Empty)
                    .Replace(oldValue: " ", newValue: String.Empty);

                if (!val.Contains(value: valueSeparator[0]))
                {
                    return String.Empty;
                }

                val = val.Split(separator: valueSeparator)[0].Trim();

                if (!val.Contains(value: tableSeparator[0]))
                {
                    return String.Empty;
                }

                return
                    val.Split(separator: tableSeparator)[0].Trim();
            }

            /// <summary>
            /// <para lang="cs">Jméno databázového sloupce z textu vnitřní části podmínky</para>
            /// <para lang="en">Database column name from text of the inner part of the condition</para>
            /// </summary>
            /// <param name="val">
            /// <para lang="cs">Text vnitřní části podmínky</para>
            /// <para lang="en">Text of the inner part of the condition</para>
            /// </param>
            /// <returns>
            /// <para lang="cs">Vrací jméno databázového sloupce z textu vnitřní části podmínky</para>
            /// <para lang="en">Returns database column name from text of the inner part of the condition</para>
            /// </returns>
            private static string GetColumnName(string val)
            {
                char[] tableSeparator = ['.'];
                char[] valueSeparator = ['='];

                val = val
                    .Trim()
                    .Replace(oldValue: "(", newValue: String.Empty)
                    .Replace(oldValue: ")", newValue: String.Empty)
                    .Replace(oldValue: "!", newValue: String.Empty)
                    .Replace(oldValue: "'", newValue: String.Empty)
                    .Replace(oldValue: " ", newValue: String.Empty);

                if (!val.Contains(value: valueSeparator[0]))
                {
                    return String.Empty;
                }

                val = val.Split(separator: valueSeparator)[0].Trim();

                if (!val.Contains(value: tableSeparator[0]))
                {
                    return val.Trim();
                }

                return
                    val.Split(separator: tableSeparator)[1].Trim();
            }

            /// <summary>
            /// <para lang="cs">Hodnota v databázovém sloupci z textu vnitřní části podmínky</para>
            /// <para lang="en">Value in database column from text of the inner part of the condition</para>
            /// </summary>
            /// <param name="val">
            /// <para lang="cs">Text vnitřní části podmínky</para>
            /// <para lang="en">Text of the inner part of the condition</para>
            /// </param>
            /// <returns>
            /// <para lang="cs">Vrací hodnotu v databázovém sloupci z textu vnitřní části podmínky</para>
            /// <para lang="en">Returns value in database column from text of the inner part of the condition</para>
            /// </returns>
            private static string GetColumnValue(string val)
            {
                char[] valueSeparator = ['='];

                val = val
                   .Trim()
                   .Replace(oldValue: "(", newValue: String.Empty)
                   .Replace(oldValue: ")", newValue: String.Empty)
                   .Replace(oldValue: "!", newValue: String.Empty)
                   .Replace(oldValue: "'", newValue: String.Empty)
                   .Replace(oldValue: " ", newValue: String.Empty);

                if (!val.Contains(value: valueSeparator[0]))
                {
                    return String.Empty;
                }

                return
                    val.Split(separator: valueSeparator)[1].Trim();
            }

            /// <summary>
            /// <para lang="cs">Podmínka je rovnost</para>
            /// <para lang="en">Condition is equality</para>
            /// </summary>
            /// <param name="val">
            /// <para lang="cs">Text vnitřní části podmínky</para>
            /// <para lang="en">Text of the inner part of the condition</para>
            /// </param>
            /// <returns>
            /// <para lang="cs">ano|ne</para>
            /// <para lang="en">yes|no</para>
            /// </returns>
            private static bool GetIsEqual(string val)
            {
                return !val.Contains(value: '!');
            }

            /// <summary>
            /// <para lang="cs">Připraví text vnitřní části podmínky ve tvaru: (TablePrefix.ColumnName = ColumnValue)</para>
            /// <para lang="en">Prepares text of the inner part of the condition in in the form: (TablePrefix.ColumnName = ColumnValue)</para>
            /// </summary>
            /// <param name="tablePrefix">
            /// <para lang="cs">Předpona pro tabulku</para>
            /// <para lang="en">Table prefix</para>
            /// </param>
            /// <param name="columnName">
            /// <para lang="cs">Jméno databázového sloupce</para>
            /// <para lang="en">Database column name</para>
            /// </param>
            /// <param name="columnValue">
            /// <para lang="cs">Hodnota v databázovém sloupci</para>
            /// <para lang="en">Value in database column</para>
            /// </param>
            /// <param name="isEqual">
            /// <para lang="cs">Podmínka je rovnost</para>
            /// <para lang="en">Condition is equality</para>
            /// </param>
            /// <returns>
            /// <para lang="cs">Vrací text vnitřní části podmínky ve tvaru: (TablePrefix.ColumnName = ColumnValue)</para>
            /// <para lang="en">Returns text of the inner part of the condition in in the form: (TablePrefix.ColumnName = ColumnValue)</para>
            /// </returns>
            private static string PrepareConditionText(
                string tablePrefix,
                string columnName,
                string columnValue,
                bool isEqual)
            {
                columnValue = columnValue.Replace(
                    oldValue: "'",
                    newValue: String.Empty);

                if (String.IsNullOrEmpty(value: tablePrefix))
                {
                    if (Functions.IsDouble(text: columnValue))
                    {
                        // Číslo je bez apostrofů
                        return
                            isEqual
                            ? $"({columnName} = {columnValue})"
                            : $"({columnName} != {columnValue})";
                    }

                    if (Functions.IsBool(text: columnValue))
                    {
                        // Boolean je bez apostrofů
                        return
                            isEqual
                            ? $"({columnName} = {columnValue.ToLower()})"
                            : $"({columnName} != {columnValue.ToLower()})";
                    }

                    // Text musí být v apostrofech
                    return
                        isEqual
                        ? $"({columnName} = '{columnValue}')"
                        : $"({columnName} != '{columnValue}')";
                }

                else
                {
                    if (Functions.IsDouble(text: columnValue))
                    {
                        // Číslo je bez apostrofů
                        return
                            isEqual
                            ? $"({tablePrefix}.{columnName} = {columnValue})"
                            : $"({tablePrefix}.{columnName} != {columnValue})";
                    }

                    if (Functions.IsBool(text: columnValue))
                    {
                        // Boolean je bez apostrofů
                        return
                            isEqual
                            ? $"({tablePrefix}.{columnName} = {columnValue.ToLower()})"
                            : $"({tablePrefix}.{columnName} != {columnValue.ToLower()})";
                    }

                    // Text musí být v apostrofech
                    return
                        isEqual
                        ? $"({tablePrefix}.{columnName} = '{columnValue}')"
                        : $"({tablePrefix}.{columnName} != '{columnValue}')";
                }
            }

            /// <summary>
            /// <para lang="cs">Vrací řetězec, který představuje aktuální objekt</para>
            /// <para lang="en">Returns the string that represents current object</para>
            /// </summary>
            /// <returns>
            /// <para lang="cs">Vrací řetězec, který představuje aktuální objekt</para>
            /// <para lang="en">Returns the string that represents current object</para>
            /// </returns>
            public override string ToString()
            {
                return Text;
            }

            #endregion Methods

        }

    }
}