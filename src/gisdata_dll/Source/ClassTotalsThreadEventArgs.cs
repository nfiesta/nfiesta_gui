﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Data;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.AuxiliaryData;

namespace ZaJi
{
    namespace ModuleAuxiliaryData
    {

        /// <summary>
        /// <para lang="cs">Parametry události vlákna</para>
        /// <para lang="en">Thread event parameters</para>
        /// </summary>
        internal class TotalsThreadEventArgs
            : EventArgs
        {

            #region Private Fields

            /// <summary>
            /// <para lang="cs">Pracovní vlákno, kde událost vznikla</para>
            /// <para lang="en">The worker thread where the event originated</para>
            /// </summary>
            private TotalsWorkerThread origin = null;

            /// <summary>
            /// <para lang="cs">Zpracovávaný segment výpočetní buňky</para>
            /// <para lang="en">Processed estimation cell segment</para>
            /// </summary>
            private TFnGetGidsForAuxTotalApp gidForAuxTotal = null;

            /// <summary>
            /// <para lang="cs">Vypočtené úhrny pomocné proměné pro zpracovávaný segment výpočetní buňky</para>
            /// <para lang="en">Calculated auxiliary variable totals for the processed segment of the estimation cell</para>
            /// </summary>
            private DataTable result = null;

            /// <summary>
            /// <para lang="cs">Objekt chyby při výpočtu úhrnu pomocných proměnných pro zpracovávaný segment výpočetní buňky</para>
            /// <para lang="en">Error object when calculating the auxiliary variable totals for the processed segment of the estimation cell</para>
            /// </summary>
            private Exception exception = null;

            /// <summary>
            /// <para lang="cs">Čas vzniku události ve vlákně</para>
            /// <para lang="en">Time of the event in the thread</para>
            /// </summary>
            private DateTime eventTime = DateTime.Now;

            /// <summary>
            /// <para lang="cs">Textová zpráva o události ve vlákně</para>
            /// <para lang="en">Text message about the event in the thread</para>
            /// </summary>
            private string message = null;

            #endregion Private Fields


            #region Constructor

            /// <summary>
            /// <para lang="cs">Konstruktor</para>
            /// <para lang="en">Constructor</para>
            /// </summary>
            public TotalsThreadEventArgs()
            {
                Origin = null;
                GidForAuxTotal = null;
                Result = null;
                Exception = null;
                EventTime = DateTime.Now;
                Message = String.Empty;
            }

            /// <summary>
            /// <para lang="cs">Konstruktor</para>
            /// <para lang="en">Constructor</para>
            /// </summary>
            public TotalsThreadEventArgs(object sender, ThreadEventArgs e)
            {
                Origin = (TotalsWorkerThread)sender;
                GidForAuxTotal = (TFnGetGidsForAuxTotalApp)e.Task;
                Result = null;
                Exception = e.ExceptionEventArgs?.ExceptionObject;
                EventTime = e.TimeStamp;
                Message = e.Message;
            }

            #endregion Constructor


            #region Properties

            /// <summary>
            /// <para lang="cs">Pracovní vlákno, kde událost vznikla</para>
            /// <para lang="en">The worker thread where the event originated</para>
            /// </summary>
            public TotalsWorkerThread Origin
            {
                get
                {
                    return origin;
                }
                set
                {
                    origin = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Zpracovávaný segment výpočetní buňky</para>
            /// <para lang="en">Processed estimation cell segment</para>
            /// </summary>
            public TFnGetGidsForAuxTotalApp GidForAuxTotal
            {
                get
                {
                    return gidForAuxTotal;
                }
                set
                {
                    gidForAuxTotal = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Vypočtené úhrny pomocné proměné pro zpracovávaný segment výpočetní buňky</para>
            /// <para lang="en">Calculated auxiliary variable totals for the processed segment of the estimation cell</para>
            /// </summary>
            public DataTable Result
            {
                get
                {
                    return result;
                }
                set
                {
                    result = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Objekt chyby při výpočtu úhrnu pomocných proměnných pro zpracovávaný segment výpočetní buňky</para>
            /// <para lang="en">Error object when calculating the auxiliary variable totals for the processed segment of the estimation cell</para>
            /// </summary>
            public Exception Exception
            {
                get
                {
                    return exception;
                }
                set
                {
                    exception = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Čas vzniku události ve vlákně</para>
            /// <para lang="en">Time of the event in the thread</para>
            /// </summary>
            public DateTime EventTime
            {
                get
                {
                    return eventTime;
                }
                private set
                {
                    eventTime = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Textová zpráva o události ve vlákně</para>
            /// <para lang="en">Text message about the event in the thread</para>
            /// </summary>
            public string Message
            {
                get
                {
                    return message;
                }
                set
                {
                    message = value;
                }
            }

            #endregion Properties

        }

    }
}
