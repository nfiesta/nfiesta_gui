﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi.ModuleAuxiliaryData
{

    partial class ControlPointGeom
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            tlpMain = new System.Windows.Forms.TableLayoutPanel();
            tlpColumns = new System.Windows.Forms.TableLayoutPanel();
            grpPanel = new System.Windows.Forms.GroupBox();
            grpGeom = new System.Windows.Forms.GroupBox();
            grpPlot = new System.Windows.Forms.GroupBox();
            grpCluster = new System.Windows.Forms.GroupBox();
            grpStratum = new System.Windows.Forms.GroupBox();
            grpStrataSet = new System.Windows.Forms.GroupBox();
            grpCountry = new System.Windows.Forms.GroupBox();
            tlpButtons = new System.Windows.Forms.TableLayoutPanel();
            pnlNext = new System.Windows.Forms.Panel();
            btnNext = new System.Windows.Forms.Button();
            pnlPrevious = new System.Windows.Forms.Panel();
            btnPrevious = new System.Windows.Forms.Button();
            tlpMain.SuspendLayout();
            tlpColumns.SuspendLayout();
            tlpButtons.SuspendLayout();
            pnlNext.SuspendLayout();
            pnlPrevious.SuspendLayout();
            SuspendLayout();
            // 
            // tlpMain
            // 
            tlpMain.ColumnCount = 1;
            tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.Controls.Add(tlpColumns, 0, 0);
            tlpMain.Controls.Add(tlpButtons, 0, 1);
            tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpMain.Location = new System.Drawing.Point(0, 0);
            tlpMain.Margin = new System.Windows.Forms.Padding(0);
            tlpMain.Name = "tlpMain";
            tlpMain.RowCount = 2;
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpMain.Size = new System.Drawing.Size(960, 540);
            tlpMain.TabIndex = 3;
            // 
            // tlpColumns
            // 
            tlpColumns.ColumnCount = 1;
            tlpColumns.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpColumns.Controls.Add(grpPanel, 0, 3);
            tlpColumns.Controls.Add(grpGeom, 0, 0);
            tlpColumns.Controls.Add(grpPlot, 0, 1);
            tlpColumns.Controls.Add(grpCluster, 0, 2);
            tlpColumns.Controls.Add(grpStratum, 0, 4);
            tlpColumns.Controls.Add(grpStrataSet, 0, 5);
            tlpColumns.Controls.Add(grpCountry, 0, 6);
            tlpColumns.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpColumns.Location = new System.Drawing.Point(0, 0);
            tlpColumns.Margin = new System.Windows.Forms.Padding(0);
            tlpColumns.Name = "tlpColumns";
            tlpColumns.RowCount = 8;
            tlpColumns.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            tlpColumns.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            tlpColumns.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            tlpColumns.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            tlpColumns.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            tlpColumns.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            tlpColumns.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            tlpColumns.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpColumns.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpColumns.Size = new System.Drawing.Size(960, 500);
            tlpColumns.TabIndex = 17;
            // 
            // grpPanel
            // 
            grpPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            grpPanel.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            grpPanel.ForeColor = System.Drawing.Color.MediumBlue;
            grpPanel.Location = new System.Drawing.Point(0, 165);
            grpPanel.Margin = new System.Windows.Forms.Padding(0);
            grpPanel.Name = "grpPanel";
            grpPanel.Padding = new System.Windows.Forms.Padding(5);
            grpPanel.Size = new System.Drawing.Size(960, 55);
            grpPanel.TabIndex = 8;
            grpPanel.TabStop = false;
            grpPanel.Text = "grpPanel";
            // 
            // grpGeom
            // 
            grpGeom.Dock = System.Windows.Forms.DockStyle.Fill;
            grpGeom.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            grpGeom.ForeColor = System.Drawing.Color.MediumBlue;
            grpGeom.Location = new System.Drawing.Point(0, 0);
            grpGeom.Margin = new System.Windows.Forms.Padding(0);
            grpGeom.Name = "grpGeom";
            grpGeom.Padding = new System.Windows.Forms.Padding(5);
            grpGeom.Size = new System.Drawing.Size(960, 55);
            grpGeom.TabIndex = 12;
            grpGeom.TabStop = false;
            grpGeom.Text = "grpGeom";
            // 
            // grpPlot
            // 
            grpPlot.Dock = System.Windows.Forms.DockStyle.Fill;
            grpPlot.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            grpPlot.ForeColor = System.Drawing.Color.MediumBlue;
            grpPlot.Location = new System.Drawing.Point(0, 55);
            grpPlot.Margin = new System.Windows.Forms.Padding(0);
            grpPlot.Name = "grpPlot";
            grpPlot.Padding = new System.Windows.Forms.Padding(5);
            grpPlot.Size = new System.Drawing.Size(960, 55);
            grpPlot.TabIndex = 11;
            grpPlot.TabStop = false;
            grpPlot.Text = "grpPlot";
            // 
            // grpCluster
            // 
            grpCluster.Dock = System.Windows.Forms.DockStyle.Fill;
            grpCluster.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            grpCluster.ForeColor = System.Drawing.Color.MediumBlue;
            grpCluster.Location = new System.Drawing.Point(0, 110);
            grpCluster.Margin = new System.Windows.Forms.Padding(0);
            grpCluster.Name = "grpCluster";
            grpCluster.Padding = new System.Windows.Forms.Padding(5);
            grpCluster.Size = new System.Drawing.Size(960, 55);
            grpCluster.TabIndex = 6;
            grpCluster.TabStop = false;
            grpCluster.Text = "grpCluster";
            // 
            // grpStratum
            // 
            grpStratum.Dock = System.Windows.Forms.DockStyle.Fill;
            grpStratum.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            grpStratum.ForeColor = System.Drawing.Color.MediumBlue;
            grpStratum.Location = new System.Drawing.Point(0, 220);
            grpStratum.Margin = new System.Windows.Forms.Padding(0);
            grpStratum.Name = "grpStratum";
            grpStratum.Padding = new System.Windows.Forms.Padding(5);
            grpStratum.Size = new System.Drawing.Size(960, 55);
            grpStratum.TabIndex = 7;
            grpStratum.TabStop = false;
            grpStratum.Text = "grpStratum";
            // 
            // grpStrataSet
            // 
            grpStrataSet.Dock = System.Windows.Forms.DockStyle.Fill;
            grpStrataSet.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            grpStrataSet.ForeColor = System.Drawing.Color.MediumBlue;
            grpStrataSet.Location = new System.Drawing.Point(0, 275);
            grpStrataSet.Margin = new System.Windows.Forms.Padding(0);
            grpStrataSet.Name = "grpStrataSet";
            grpStrataSet.Padding = new System.Windows.Forms.Padding(5);
            grpStrataSet.Size = new System.Drawing.Size(960, 55);
            grpStrataSet.TabIndex = 6;
            grpStrataSet.TabStop = false;
            grpStrataSet.Text = "grpStrataSet";
            // 
            // grpCountry
            // 
            grpCountry.Dock = System.Windows.Forms.DockStyle.Fill;
            grpCountry.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            grpCountry.ForeColor = System.Drawing.Color.MediumBlue;
            grpCountry.Location = new System.Drawing.Point(0, 330);
            grpCountry.Margin = new System.Windows.Forms.Padding(0);
            grpCountry.Name = "grpCountry";
            grpCountry.Padding = new System.Windows.Forms.Padding(5);
            grpCountry.Size = new System.Drawing.Size(960, 55);
            grpCountry.TabIndex = 5;
            grpCountry.TabStop = false;
            grpCountry.Text = "grpCountry";
            // 
            // tlpButtons
            // 
            tlpButtons.BackColor = System.Drawing.Color.Transparent;
            tlpButtons.ColumnCount = 3;
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            tlpButtons.Controls.Add(pnlNext, 2, 0);
            tlpButtons.Controls.Add(pnlPrevious, 1, 0);
            tlpButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpButtons.Location = new System.Drawing.Point(0, 500);
            tlpButtons.Margin = new System.Windows.Forms.Padding(0);
            tlpButtons.Name = "tlpButtons";
            tlpButtons.RowCount = 1;
            tlpButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.Size = new System.Drawing.Size(960, 40);
            tlpButtons.TabIndex = 16;
            // 
            // pnlNext
            // 
            pnlNext.Controls.Add(btnNext);
            pnlNext.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlNext.Location = new System.Drawing.Point(800, 0);
            pnlNext.Margin = new System.Windows.Forms.Padding(0);
            pnlNext.Name = "pnlNext";
            pnlNext.Padding = new System.Windows.Forms.Padding(5);
            pnlNext.Size = new System.Drawing.Size(160, 40);
            pnlNext.TabIndex = 13;
            // 
            // btnNext
            // 
            btnNext.Dock = System.Windows.Forms.DockStyle.Fill;
            btnNext.Enabled = false;
            btnNext.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            btnNext.Location = new System.Drawing.Point(5, 5);
            btnNext.Margin = new System.Windows.Forms.Padding(0);
            btnNext.Name = "btnNext";
            btnNext.Size = new System.Drawing.Size(150, 30);
            btnNext.TabIndex = 9;
            btnNext.Text = "btnNext";
            btnNext.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            btnNext.UseVisualStyleBackColor = true;
            // 
            // pnlPrevious
            // 
            pnlPrevious.Controls.Add(btnPrevious);
            pnlPrevious.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlPrevious.Location = new System.Drawing.Point(640, 0);
            pnlPrevious.Margin = new System.Windows.Forms.Padding(0);
            pnlPrevious.Name = "pnlPrevious";
            pnlPrevious.Padding = new System.Windows.Forms.Padding(5);
            pnlPrevious.Size = new System.Drawing.Size(160, 40);
            pnlPrevious.TabIndex = 14;
            // 
            // btnPrevious
            // 
            btnPrevious.Dock = System.Windows.Forms.DockStyle.Fill;
            btnPrevious.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            btnPrevious.Location = new System.Drawing.Point(5, 5);
            btnPrevious.Margin = new System.Windows.Forms.Padding(0);
            btnPrevious.Name = "btnPrevious";
            btnPrevious.Size = new System.Drawing.Size(150, 30);
            btnPrevious.TabIndex = 9;
            btnPrevious.Text = "btnPrevious";
            btnPrevious.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            btnPrevious.UseVisualStyleBackColor = true;
            // 
            // ControlPointGeom
            // 
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            BackColor = System.Drawing.Color.Transparent;
            BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            Controls.Add(tlpMain);
            ForeColor = System.Drawing.Color.Black;
            Margin = new System.Windows.Forms.Padding(0);
            Name = "ControlPointGeom";
            Size = new System.Drawing.Size(960, 540);
            tlpMain.ResumeLayout(false);
            tlpColumns.ResumeLayout(false);
            tlpButtons.ResumeLayout(false);
            pnlNext.ResumeLayout(false);
            pnlPrevious.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.TableLayoutPanel tlpButtons;

        private System.Windows.Forms.Panel pnlNext;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Panel pnlPrevious;
        private System.Windows.Forms.Button btnPrevious;
        private System.Windows.Forms.TableLayoutPanel tlpColumns;
        private System.Windows.Forms.GroupBox grpPanel;
        private System.Windows.Forms.GroupBox grpGeom;
        private System.Windows.Forms.GroupBox grpPlot;
        private System.Windows.Forms.GroupBox grpCluster;
        private System.Windows.Forms.GroupBox grpStratum;
        private System.Windows.Forms.GroupBox grpStrataSet;
        private System.Windows.Forms.GroupBox grpCountry;
    }

}
