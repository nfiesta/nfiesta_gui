﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi.ModuleAuxiliaryData
{
    partial class FormCalcData
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormCalcData));
            tlpMain = new System.Windows.Forms.TableLayoutPanel();
            tlpAuxiliaryVariableValues = new System.Windows.Forms.TableLayoutPanel();
            grpAuxiliaryVariableValues = new System.Windows.Forms.GroupBox();
            dgvData = new System.Windows.Forms.DataGridView();
            tsrButtons = new System.Windows.Forms.ToolStrip();
            btnReload = new System.Windows.Forms.ToolStripButton();
            btnCalculate = new System.Windows.Forms.ToolStripButton();
            btnStop = new System.Windows.Forms.ToolStripButton();
            btnDelete = new System.Windows.Forms.ToolStripButton();
            grpConfigCollection = new System.Windows.Forms.GroupBox();
            lblConfigCollection = new System.Windows.Forms.Label();
            tlpFilters = new System.Windows.Forms.TableLayoutPanel();
            cboGUIVersion = new System.Windows.Forms.ComboBox();
            cboExtensionVersion = new System.Windows.Forms.ComboBox();
            lblGUIVersionCaption = new System.Windows.Forms.Label();
            lblConfigurationCaption = new System.Windows.Forms.Label();
            lblExtensionVersionCaption = new System.Windows.Forms.Label();
            cboConfiguration = new System.Windows.Forms.ComboBox();
            tlpButtons = new System.Windows.Forms.TableLayoutPanel();
            pnlClose = new System.Windows.Forms.Panel();
            btnClose = new System.Windows.Forms.Button();
            tlpMain.SuspendLayout();
            tlpAuxiliaryVariableValues.SuspendLayout();
            grpAuxiliaryVariableValues.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)dgvData).BeginInit();
            tsrButtons.SuspendLayout();
            grpConfigCollection.SuspendLayout();
            tlpFilters.SuspendLayout();
            tlpButtons.SuspendLayout();
            pnlClose.SuspendLayout();
            SuspendLayout();
            // 
            // tlpMain
            // 
            tlpMain.ColumnCount = 1;
            tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.Controls.Add(tlpAuxiliaryVariableValues, 0, 0);
            tlpMain.Controls.Add(tlpButtons, 0, 1);
            tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpMain.Location = new System.Drawing.Point(0, 0);
            tlpMain.Margin = new System.Windows.Forms.Padding(0);
            tlpMain.Name = "tlpMain";
            tlpMain.RowCount = 2;
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpMain.Size = new System.Drawing.Size(944, 501);
            tlpMain.TabIndex = 4;
            // 
            // tlpAuxiliaryVariableValues
            // 
            tlpAuxiliaryVariableValues.ColumnCount = 1;
            tlpAuxiliaryVariableValues.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpAuxiliaryVariableValues.Controls.Add(grpAuxiliaryVariableValues, 0, 3);
            tlpAuxiliaryVariableValues.Controls.Add(tsrButtons, 0, 0);
            tlpAuxiliaryVariableValues.Controls.Add(grpConfigCollection, 0, 1);
            tlpAuxiliaryVariableValues.Controls.Add(tlpFilters, 0, 2);
            tlpAuxiliaryVariableValues.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpAuxiliaryVariableValues.Location = new System.Drawing.Point(0, 0);
            tlpAuxiliaryVariableValues.Margin = new System.Windows.Forms.Padding(0);
            tlpAuxiliaryVariableValues.Name = "tlpAuxiliaryVariableValues";
            tlpAuxiliaryVariableValues.RowCount = 4;
            tlpAuxiliaryVariableValues.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            tlpAuxiliaryVariableValues.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            tlpAuxiliaryVariableValues.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 75F));
            tlpAuxiliaryVariableValues.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpAuxiliaryVariableValues.Size = new System.Drawing.Size(944, 461);
            tlpAuxiliaryVariableValues.TabIndex = 3;
            // 
            // grpAuxiliaryVariableValues
            // 
            grpAuxiliaryVariableValues.Controls.Add(dgvData);
            grpAuxiliaryVariableValues.Dock = System.Windows.Forms.DockStyle.Fill;
            grpAuxiliaryVariableValues.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            grpAuxiliaryVariableValues.ForeColor = System.Drawing.Color.MediumBlue;
            grpAuxiliaryVariableValues.Location = new System.Drawing.Point(0, 160);
            grpAuxiliaryVariableValues.Margin = new System.Windows.Forms.Padding(0);
            grpAuxiliaryVariableValues.Name = "grpAuxiliaryVariableValues";
            grpAuxiliaryVariableValues.Padding = new System.Windows.Forms.Padding(5);
            grpAuxiliaryVariableValues.Size = new System.Drawing.Size(944, 301);
            grpAuxiliaryVariableValues.TabIndex = 5;
            grpAuxiliaryVariableValues.TabStop = false;
            grpAuxiliaryVariableValues.Text = "grpAuxiliaryVariableValues";
            // 
            // dgvData
            // 
            dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dgvData.Dock = System.Windows.Forms.DockStyle.Fill;
            dgvData.Location = new System.Drawing.Point(5, 25);
            dgvData.Margin = new System.Windows.Forms.Padding(0);
            dgvData.Name = "dgvData";
            dgvData.Size = new System.Drawing.Size(934, 271);
            dgvData.TabIndex = 2;
            // 
            // tsrButtons
            // 
            tsrButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            tsrButtons.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { btnReload, btnCalculate, btnStop, btnDelete });
            tsrButtons.Location = new System.Drawing.Point(0, 0);
            tsrButtons.Name = "tsrButtons";
            tsrButtons.Size = new System.Drawing.Size(944, 25);
            tsrButtons.TabIndex = 5;
            tsrButtons.Text = "toolStrip1";
            // 
            // btnReload
            // 
            btnReload.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            btnReload.Image = (System.Drawing.Image)resources.GetObject("btnReload.Image");
            btnReload.ImageTransparentColor = System.Drawing.Color.Magenta;
            btnReload.Name = "btnReload";
            btnReload.Size = new System.Drawing.Size(23, 22);
            btnReload.Text = "btnReload";
            // 
            // btnCalculate
            // 
            btnCalculate.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            btnCalculate.Image = (System.Drawing.Image)resources.GetObject("btnCalculate.Image");
            btnCalculate.ImageTransparentColor = System.Drawing.Color.Magenta;
            btnCalculate.Name = "btnCalculate";
            btnCalculate.Size = new System.Drawing.Size(23, 22);
            btnCalculate.Text = "btnCalculate";
            // 
            // btnStop
            // 
            btnStop.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            btnStop.Image = (System.Drawing.Image)resources.GetObject("btnStop.Image");
            btnStop.ImageTransparentColor = System.Drawing.Color.Magenta;
            btnStop.Name = "btnStop";
            btnStop.Size = new System.Drawing.Size(23, 22);
            btnStop.Text = "btnStop";
            // 
            // btnDelete
            // 
            btnDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            btnDelete.Image = (System.Drawing.Image)resources.GetObject("btnDelete.Image");
            btnDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            btnDelete.Name = "btnDelete";
            btnDelete.Size = new System.Drawing.Size(23, 22);
            btnDelete.Text = "btnDelete";
            // 
            // grpConfigCollection
            // 
            grpConfigCollection.Controls.Add(lblConfigCollection);
            grpConfigCollection.Dock = System.Windows.Forms.DockStyle.Fill;
            grpConfigCollection.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            grpConfigCollection.ForeColor = System.Drawing.Color.MediumBlue;
            grpConfigCollection.Location = new System.Drawing.Point(0, 25);
            grpConfigCollection.Margin = new System.Windows.Forms.Padding(0);
            grpConfigCollection.Name = "grpConfigCollection";
            grpConfigCollection.Padding = new System.Windows.Forms.Padding(5);
            grpConfigCollection.Size = new System.Drawing.Size(944, 60);
            grpConfigCollection.TabIndex = 0;
            grpConfigCollection.TabStop = false;
            grpConfigCollection.Text = "grpConfigCollection";
            // 
            // lblConfigCollection
            // 
            lblConfigCollection.Dock = System.Windows.Forms.DockStyle.Fill;
            lblConfigCollection.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, 238);
            lblConfigCollection.ForeColor = System.Drawing.Color.Black;
            lblConfigCollection.Location = new System.Drawing.Point(5, 25);
            lblConfigCollection.Margin = new System.Windows.Forms.Padding(0);
            lblConfigCollection.Name = "lblConfigCollection";
            lblConfigCollection.Size = new System.Drawing.Size(934, 30);
            lblConfigCollection.TabIndex = 0;
            lblConfigCollection.Text = "lblConfigCollection";
            lblConfigCollection.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tlpFilters
            // 
            tlpFilters.ColumnCount = 2;
            tlpFilters.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            tlpFilters.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpFilters.Controls.Add(cboGUIVersion, 1, 2);
            tlpFilters.Controls.Add(cboExtensionVersion, 1, 1);
            tlpFilters.Controls.Add(lblGUIVersionCaption, 0, 2);
            tlpFilters.Controls.Add(lblConfigurationCaption, 0, 0);
            tlpFilters.Controls.Add(lblExtensionVersionCaption, 0, 1);
            tlpFilters.Controls.Add(cboConfiguration, 1, 0);
            tlpFilters.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpFilters.Location = new System.Drawing.Point(0, 85);
            tlpFilters.Margin = new System.Windows.Forms.Padding(0);
            tlpFilters.Name = "tlpFilters";
            tlpFilters.RowCount = 3;
            tlpFilters.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            tlpFilters.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            tlpFilters.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            tlpFilters.Size = new System.Drawing.Size(944, 75);
            tlpFilters.TabIndex = 6;
            // 
            // cboGUIVersion
            // 
            cboGUIVersion.Dock = System.Windows.Forms.DockStyle.Fill;
            cboGUIVersion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            cboGUIVersion.FormattingEnabled = true;
            cboGUIVersion.Location = new System.Drawing.Point(200, 50);
            cboGUIVersion.Margin = new System.Windows.Forms.Padding(0);
            cboGUIVersion.Name = "cboGUIVersion";
            cboGUIVersion.Size = new System.Drawing.Size(744, 23);
            cboGUIVersion.TabIndex = 7;
            // 
            // cboExtensionVersion
            // 
            cboExtensionVersion.Dock = System.Windows.Forms.DockStyle.Fill;
            cboExtensionVersion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            cboExtensionVersion.FormattingEnabled = true;
            cboExtensionVersion.Location = new System.Drawing.Point(200, 25);
            cboExtensionVersion.Margin = new System.Windows.Forms.Padding(0);
            cboExtensionVersion.Name = "cboExtensionVersion";
            cboExtensionVersion.Size = new System.Drawing.Size(744, 23);
            cboExtensionVersion.TabIndex = 6;
            // 
            // lblGUIVersionCaption
            // 
            lblGUIVersionCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblGUIVersionCaption.Location = new System.Drawing.Point(0, 50);
            lblGUIVersionCaption.Margin = new System.Windows.Forms.Padding(0);
            lblGUIVersionCaption.Name = "lblGUIVersionCaption";
            lblGUIVersionCaption.Size = new System.Drawing.Size(200, 20);
            lblGUIVersionCaption.TabIndex = 4;
            lblGUIVersionCaption.Text = "lblGUIVersionCaption";
            lblGUIVersionCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblConfigurationCaption
            // 
            lblConfigurationCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblConfigurationCaption.Location = new System.Drawing.Point(0, 0);
            lblConfigurationCaption.Margin = new System.Windows.Forms.Padding(0);
            lblConfigurationCaption.Name = "lblConfigurationCaption";
            lblConfigurationCaption.Size = new System.Drawing.Size(200, 20);
            lblConfigurationCaption.TabIndex = 2;
            lblConfigurationCaption.Text = "lblConfigurationCaption";
            lblConfigurationCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblExtensionVersionCaption
            // 
            lblExtensionVersionCaption.ForeColor = System.Drawing.Color.MediumBlue;
            lblExtensionVersionCaption.Location = new System.Drawing.Point(0, 25);
            lblExtensionVersionCaption.Margin = new System.Windows.Forms.Padding(0);
            lblExtensionVersionCaption.Name = "lblExtensionVersionCaption";
            lblExtensionVersionCaption.Size = new System.Drawing.Size(200, 20);
            lblExtensionVersionCaption.TabIndex = 3;
            lblExtensionVersionCaption.Text = "lblExtensionVersionCaption";
            lblExtensionVersionCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cboConfiguration
            // 
            cboConfiguration.Dock = System.Windows.Forms.DockStyle.Fill;
            cboConfiguration.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            cboConfiguration.FormattingEnabled = true;
            cboConfiguration.Location = new System.Drawing.Point(200, 0);
            cboConfiguration.Margin = new System.Windows.Forms.Padding(0);
            cboConfiguration.Name = "cboConfiguration";
            cboConfiguration.Size = new System.Drawing.Size(744, 23);
            cboConfiguration.TabIndex = 5;
            // 
            // tlpButtons
            // 
            tlpButtons.BackColor = System.Drawing.Color.Transparent;
            tlpButtons.ColumnCount = 2;
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpButtons.Controls.Add(pnlClose, 1, 0);
            tlpButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpButtons.Location = new System.Drawing.Point(0, 461);
            tlpButtons.Margin = new System.Windows.Forms.Padding(0);
            tlpButtons.Name = "tlpButtons";
            tlpButtons.RowCount = 1;
            tlpButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.Size = new System.Drawing.Size(944, 40);
            tlpButtons.TabIndex = 16;
            // 
            // pnlClose
            // 
            pnlClose.Controls.Add(btnClose);
            pnlClose.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlClose.Location = new System.Drawing.Point(784, 0);
            pnlClose.Margin = new System.Windows.Forms.Padding(0);
            pnlClose.Name = "pnlClose";
            pnlClose.Padding = new System.Windows.Forms.Padding(5);
            pnlClose.Size = new System.Drawing.Size(160, 40);
            pnlClose.TabIndex = 13;
            // 
            // btnClose
            // 
            btnClose.DialogResult = System.Windows.Forms.DialogResult.OK;
            btnClose.Dock = System.Windows.Forms.DockStyle.Fill;
            btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            btnClose.Location = new System.Drawing.Point(5, 5);
            btnClose.Margin = new System.Windows.Forms.Padding(0);
            btnClose.Name = "btnClose";
            btnClose.Size = new System.Drawing.Size(150, 30);
            btnClose.TabIndex = 9;
            btnClose.Text = "btnClose";
            btnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            btnClose.UseVisualStyleBackColor = true;
            // 
            // FormCalcData
            // 
            AcceptButton = btnClose;
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            BackColor = System.Drawing.Color.WhiteSmoke;
            ClientSize = new System.Drawing.Size(944, 501);
            Controls.Add(tlpMain);
            ForeColor = System.Drawing.Color.Black;
            Name = "FormCalcData";
            ShowIcon = false;
            ShowInTaskbar = false;
            StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            Text = "FormCalcData";
            tlpMain.ResumeLayout(false);
            tlpAuxiliaryVariableValues.ResumeLayout(false);
            tlpAuxiliaryVariableValues.PerformLayout();
            grpAuxiliaryVariableValues.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)dgvData).EndInit();
            tsrButtons.ResumeLayout(false);
            tsrButtons.PerformLayout();
            grpConfigCollection.ResumeLayout(false);
            tlpFilters.ResumeLayout(false);
            tlpButtons.ResumeLayout(false);
            pnlClose.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.TableLayoutPanel tlpButtons;
        private System.Windows.Forms.Panel pnlClose;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.TableLayoutPanel tlpAuxiliaryVariableValues;
        private System.Windows.Forms.GroupBox grpAuxiliaryVariableValues;
        private System.Windows.Forms.DataGridView dgvData;
        private System.Windows.Forms.GroupBox grpConfigCollection;
        private System.Windows.Forms.Label lblConfigCollection;
        private System.Windows.Forms.TableLayoutPanel tlpFilters;
        private System.Windows.Forms.ComboBox cboGUIVersion;
        private System.Windows.Forms.ComboBox cboExtensionVersion;
        private System.Windows.Forms.Label lblGUIVersionCaption;
        private System.Windows.Forms.Label lblConfigurationCaption;
        private System.Windows.Forms.Label lblExtensionVersionCaption;
        private System.Windows.Forms.ComboBox cboConfiguration;
        private System.Windows.Forms.ToolStrip tsrButtons;
        private System.Windows.Forms.ToolStripButton btnReload;
        private System.Windows.Forms.ToolStripButton btnCalculate;
        private System.Windows.Forms.ToolStripButton btnStop;
        private System.Windows.Forms.ToolStripButton btnDelete;
    }
}