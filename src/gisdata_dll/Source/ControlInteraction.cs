﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.AuxiliaryData;
using ZaJi.PostgreSQL;

namespace ZaJi.ModuleAuxiliaryData
{

    /// <summary>
    /// <para lang="cs">Ovládací prvek "Kombinace GIS vrstev"</para>
    /// <para lang="en">Control "GIS layer combination"</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    internal partial class ControlInteraction
        : UserControl, INfiEstaControl, IAuxiliaryDataControl
    {

        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        private Control controlOwner;

        #endregion Private Fields


        #region Events

        /// <summary>
        /// <para lang="cs">Událost kliknutí na tlačítko "Předchozí"</para>
        /// <para lang="en">Click event on the "Previous" button</para>
        /// </summary>
        public event EventHandler PreviousClick;

        /// <summary>
        /// <para lang="cs">Událost kliknutí na tlačítko "Další"</para>
        /// <para lang="en">Click event on the "Next" button</para>
        /// </summary>
        public event EventHandler NextClick;

        #endregion Events


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor ovládacího prvku</para>
        /// <para lang="en">Control constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        /// <param name="configFunctionA">
        /// <para lang="cs">Konfigurační funkce první GIS vrstvy</para>
        /// <para lang="en">Config function for first GIS layer</para>
        /// </param>
        /// <param name="configFunctionB">
        /// <para lang="cs">Konfigurační funkce druhé GIS vrstvy</para>
        /// <para lang="en">Config function for second GIS layer</para>
        /// </param>
        public ControlInteraction(
                Control controlOwner,
                ConfigFunction configFunctionA,
                ConfigFunction configFunctionB)
        {
            InitializeComponent();
            Initialize(
                controlOwner: controlOwner,
                configFunctionA: configFunctionA,
                configFunctionB: configFunctionB);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                IAuxiliaryDataControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                if (controlOwner is not FormConfigurationGuide)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(FormConfigurationGuide)}.",
                        paramName: nameof(ControlOwner));
                }

                return controlOwner;
            }
            set
            {
                IAuxiliaryDataControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                if (value is not FormConfigurationGuide)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(FormConfigurationGuide)}.",
                        paramName: nameof(ControlOwner));
                }

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables (read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((IAuxiliaryDataControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro pomocná data (read-only)</para>
        /// <para lang="en">Module for auxiliary data setting (read-only)</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((IAuxiliaryDataControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">Konfigurace (read-only)</para>
        /// <para lang="en">Configuration (read-only)</para>
        /// </summary>
        private Config Configuration
        {
            get
            {
                return ((FormConfigurationGuide)ControlOwner).Configuration;
            }
        }

        /// <summary>
        /// <para lang="cs">Ovládací prvek pro výběr konfigurace první GIS vrstvy</para>
        /// <para lang="en">Control "Configuration selection for first GIS layer"</para>
        /// </summary>
        private ControlConfigurationSelector CtrGISLayerA
        {
            get
            {
                return
                    pnlGISLayerA.Controls
                        .OfType<ControlConfigurationSelector>()
                        .FirstOrDefault<ControlConfigurationSelector>();
            }
        }

        /// <summary>
        /// <para lang="cs">Ovládací prvek pro výběr konfigurace druhé GIS vrstvy</para>
        /// <para lang="en">Control "Configuration selection for second GIS layer"</para>
        /// </summary>
        private ControlConfigurationSelector CtrGISLayerB
        {
            get
            {
                return
                    pnlGISLayerB.Controls
                        .OfType<ControlConfigurationSelector>()
                        .FirstOrDefault<ControlConfigurationSelector>();
            }
        }

        /// <summary>
        /// <para lang="cs">Konfigurační funkce první GIS vrstvy</para>
        /// <para lang="en">Config function for first GIS layer</para>
        /// </summary>
        public ConfigFunction ConfigFunctionA
        {
            get
            {
                return
                    CtrGISLayerA?.ConfigFunction;
            }
        }

        /// <summary>
        /// <para lang="cs">Konfigurační funkce druhé GIS vrstvy</para>
        /// <para lang="en">Config function for second GIS layer</para>
        /// </summary>
        public ConfigFunction ConfigFunctionB
        {
            get
            {
                return
                    CtrGISLayerB?.ConfigFunction;
            }
        }

        /// <summary>
        /// <para lang="cs">Vybraná konfigurace první GIS vrstvy</para>
        /// <para lang="en">Selected configuration for first GIS layer</para>
        /// </summary>
        public Config GISLayerA
        {
            get
            {
                return
                    CtrGISLayerA?.Configuration;
            }
        }

        /// <summary>
        /// <para lang="cs">Vybraná konfigurace druhé GIS vrstvy</para>
        /// <para lang="en">Selected configuration for second GIS layer</para>
        /// </summary>
        public Config GISLayerB
        {
            get
            {
                return
                    CtrGISLayerB?.Configuration;
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(btnPrevious),                        "Předchozí" },
                        { nameof(btnNext),                            "Další" },
                    }
                    : languageFile.NationalVersion.Data.TryGetValue(
                        key: nameof(ControlInteraction),
                        out Dictionary<string, string> dictNational)
                            ? dictNational
                            : [],

                LanguageVersion.International => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(btnPrevious),                        "Previous" },
                        { nameof(btnNext),                            "Next" },
                    }
                    : languageFile.InternationalVersion.Data.TryGetValue(
                        key: nameof(ControlInteraction),
                        out Dictionary<string, string> dictInternational)
                            ? dictInternational
                            : [],

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku</para>
        /// <para lang="en">Initialization of the control</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        /// <param name="configFunctionA">
        /// <para lang="cs">Konfigurační funkce první GIS vrstvy</para>
        /// <para lang="en">Config function for first GIS layer</para>
        /// </param>
        /// <param name="configFunctionB">
        /// <para lang="cs">Konfigurační funkce druhé GIS vrstvy</para>
        /// <para lang="en">Config function for second GIS layer</para>
        /// </param>
        private void Initialize(
            Control controlOwner,
            ConfigFunction configFunctionA,
            ConfigFunction configFunctionB)
        {
            BorderStyle = BorderStyle.None;
            ControlOwner = controlOwner;

            pnlGISLayerA.Controls.Clear();
            Config layerAConfiguration = (Configuration?.ConfigCollection != null)
                ? Configuration.ConfigCollection.ConfigFunctionValue switch
                {
                    ConfigFunctionEnum.RasterTotalWithinVector =>
                        Database.SAuxiliaryData.TConfig[Configuration?.VectorId ?? 0],
                    ConfigFunctionEnum.RastersProductTotal =>
                        Database.SAuxiliaryData.TConfig[Configuration?.RasterAId ?? 0],
                    _ => null,
                } : null;
            ControlConfigurationSelector ctrConfigurationSelectorA =
                new(
                    controlOwner: this,
                    configFunction: configFunctionA)
                {
                    Dock = DockStyle.Fill,
                    Configuration = layerAConfiguration

                };
            ctrConfigurationSelectorA.ConfigurationChanged += new EventHandler(
                (sender, e) => { SetGisLayers(); });
            ctrConfigurationSelectorA.CreateControl();
            pnlGISLayerA.Controls.Add(value: ctrConfigurationSelectorA);


            pnlGISLayerB.Controls.Clear();
            Config layerBConfiguration = (Configuration?.ConfigCollection != null)
                ? Configuration.ConfigCollection.ConfigFunctionValue switch
                {
                    ConfigFunctionEnum.RasterTotalWithinVector =>
                        Database.SAuxiliaryData.TConfig[Configuration?.RasterAId ?? 0],
                    ConfigFunctionEnum.RastersProductTotal =>
                        Database.SAuxiliaryData.TConfig[Configuration?.RasterBId ?? 0],
                    _ => null,
                } : null;
            ControlConfigurationSelector ctrConfigurationSelectorB =
                new(
                    controlOwner: this,
                    configFunction: configFunctionB)
                {
                    Dock = DockStyle.Fill,
                    Configuration = layerBConfiguration
                };
            ctrConfigurationSelectorB.ConfigurationChanged += new EventHandler(
                (sender, e) => { SetGisLayers(); });
            ctrConfigurationSelectorB.CreateControl();
            pnlGISLayerB.Controls.Add(value: ctrConfigurationSelectorB);

            InitializeLabels();

            EnableNext();

            btnPrevious.Click += new EventHandler(
                (sender, e) =>
                {
                    PreviousClick?.Invoke(sender: sender, e: e);
                });

            btnNext.Click += new EventHandler(
               (sender, e) =>
               {
                   NextClick?.Invoke(sender: sender, e: e);
               });
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků ovládacího prvku</para>
        /// <para lang="en">Initialization of control labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            btnPrevious.Text =
                labels.TryGetValue(key: nameof(btnPrevious),
                out string btnPreviousText)
                    ? btnPreviousText
                    : String.Empty;

            btnNext.Text =
                labels.TryGetValue(key: nameof(btnNext),
                out string btnNextText)
                    ? btnNextText
                    : String.Empty;

            CtrGISLayerA?.InitializeLabels();
            CtrGISLayerB?.InitializeLabels();
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent()
        {
        }

        /// <summary>
        /// <para lang="cs">Nastaví zvolené vrstvy v objektu konfigurace</para>
        /// <para lang="en">Sets selected layers in configuration object</para>
        /// </summary>
        private void SetGisLayers()
        {
            switch (Configuration.ConfigCollection.ConfigFunctionValue)
            {
                case ConfigFunctionEnum.RasterTotalWithinVector:
                    Configuration.VectorId = GISLayerA?.Id;
                    Configuration.RasterAId = GISLayerB?.Id;
                    Configuration.RasterBId = null;
                    break;

                case ConfigFunctionEnum.RastersProductTotal:
                    Configuration.VectorId = null;
                    Configuration.RasterAId = GISLayerA?.Id;
                    Configuration.RasterBId = GISLayerB?.Id;
                    break;

                default:
                    Configuration.VectorId = null;
                    Configuration.RasterAId = null;
                    Configuration.RasterBId = null;
                    break;
            }
            EnableNext();
        }

        /// <summary>
        /// <para lang="cs">Povolí postup na další formulář</para>
        /// <para lang="en">Allows progression to the next form</para>
        /// </summary>
        private void EnableNext()
        {
            if (Configuration == null)
            {
                btnNext.Enabled = false;
                return;
            }

            btnNext.Enabled =
                Configuration.ConfigCollection.ConfigFunctionValue switch
                {
                    ConfigFunctionEnum.RasterTotalWithinVector =>
                        !String.IsNullOrEmpty(Configuration.LabelCs) &&
                        !String.IsNullOrEmpty(Configuration.LabelEn) &&
                        !String.IsNullOrEmpty(Configuration.DescriptionCs) &&
                        !String.IsNullOrEmpty(Configuration.DescriptionEn) &&
                        (Configuration.ConfigQueryValue != ConfigQueryEnum.Unknown) &&
                        Configuration.VectorId != null &&
                        Configuration.RasterAId != null &&
                        Configuration.RasterBId == null,
                    ConfigFunctionEnum.RastersProductTotal =>
                        !String.IsNullOrEmpty(Configuration.LabelCs) &&
                        !String.IsNullOrEmpty(Configuration.LabelEn) &&
                        !String.IsNullOrEmpty(Configuration.DescriptionCs) &&
                        !String.IsNullOrEmpty(Configuration.DescriptionEn) &&
                        (Configuration.ConfigQueryValue != ConfigQueryEnum.Unknown) &&
                        Configuration.VectorId == null &&
                        Configuration.RasterAId != null &&
                        Configuration.RasterBId != null,
                    _ => false,
                };
        }

        #endregion Methods

    }

}
