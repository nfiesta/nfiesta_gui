﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi.ModuleAuxiliaryData
{

    partial class ControlRasterBand
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ControlRasterBand));
            tlpMain = new System.Windows.Forms.TableLayoutPanel();
            tlpButtons = new System.Windows.Forms.TableLayoutPanel();
            pnlNext = new System.Windows.Forms.Panel();
            btnNext = new System.Windows.Forms.Button();
            pnlPrevious = new System.Windows.Forms.Panel();
            btnPrevious = new System.Windows.Forms.Button();
            grpRasterBands = new System.Windows.Forms.GroupBox();
            lblEmptyRasterBands = new System.Windows.Forms.Label();
            tvwRasterBands = new System.Windows.Forms.TreeView();
            grpSelectedRasterBand = new System.Windows.Forms.GroupBox();
            lblSelectedRasterBand = new System.Windows.Forms.Label();
            ilTreeView = new System.Windows.Forms.ImageList(components);
            tlpMain.SuspendLayout();
            tlpButtons.SuspendLayout();
            pnlNext.SuspendLayout();
            pnlPrevious.SuspendLayout();
            grpRasterBands.SuspendLayout();
            grpSelectedRasterBand.SuspendLayout();
            SuspendLayout();
            // 
            // tlpMain
            // 
            tlpMain.ColumnCount = 1;
            tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.Controls.Add(tlpButtons, 0, 2);
            tlpMain.Controls.Add(grpRasterBands, 0, 1);
            tlpMain.Controls.Add(grpSelectedRasterBand, 0, 0);
            tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpMain.Location = new System.Drawing.Point(0, 0);
            tlpMain.Margin = new System.Windows.Forms.Padding(0);
            tlpMain.Name = "tlpMain";
            tlpMain.RowCount = 3;
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            tlpMain.Size = new System.Drawing.Size(960, 540);
            tlpMain.TabIndex = 4;
            // 
            // tlpButtons
            // 
            tlpButtons.BackColor = System.Drawing.Color.Transparent;
            tlpButtons.ColumnCount = 3;
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            tlpButtons.Controls.Add(pnlNext, 2, 0);
            tlpButtons.Controls.Add(pnlPrevious, 1, 0);
            tlpButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpButtons.Location = new System.Drawing.Point(0, 500);
            tlpButtons.Margin = new System.Windows.Forms.Padding(0);
            tlpButtons.Name = "tlpButtons";
            tlpButtons.RowCount = 1;
            tlpButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.Size = new System.Drawing.Size(960, 40);
            tlpButtons.TabIndex = 15;
            // 
            // pnlNext
            // 
            pnlNext.Controls.Add(btnNext);
            pnlNext.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlNext.Location = new System.Drawing.Point(800, 0);
            pnlNext.Margin = new System.Windows.Forms.Padding(0);
            pnlNext.Name = "pnlNext";
            pnlNext.Padding = new System.Windows.Forms.Padding(5);
            pnlNext.Size = new System.Drawing.Size(160, 40);
            pnlNext.TabIndex = 13;
            // 
            // btnNext
            // 
            btnNext.Dock = System.Windows.Forms.DockStyle.Fill;
            btnNext.Enabled = false;
            btnNext.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            btnNext.Location = new System.Drawing.Point(5, 5);
            btnNext.Margin = new System.Windows.Forms.Padding(0);
            btnNext.Name = "btnNext";
            btnNext.Size = new System.Drawing.Size(150, 30);
            btnNext.TabIndex = 9;
            btnNext.Text = "btnNext";
            btnNext.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            btnNext.UseVisualStyleBackColor = true;
            // 
            // pnlPrevious
            // 
            pnlPrevious.Controls.Add(btnPrevious);
            pnlPrevious.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlPrevious.Location = new System.Drawing.Point(640, 0);
            pnlPrevious.Margin = new System.Windows.Forms.Padding(0);
            pnlPrevious.Name = "pnlPrevious";
            pnlPrevious.Padding = new System.Windows.Forms.Padding(5);
            pnlPrevious.Size = new System.Drawing.Size(160, 40);
            pnlPrevious.TabIndex = 14;
            // 
            // btnPrevious
            // 
            btnPrevious.Dock = System.Windows.Forms.DockStyle.Fill;
            btnPrevious.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            btnPrevious.Location = new System.Drawing.Point(5, 5);
            btnPrevious.Margin = new System.Windows.Forms.Padding(0);
            btnPrevious.Name = "btnPrevious";
            btnPrevious.Size = new System.Drawing.Size(150, 30);
            btnPrevious.TabIndex = 9;
            btnPrevious.Text = "btnPrevious";
            btnPrevious.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            btnPrevious.UseVisualStyleBackColor = true;
            // 
            // grpRasterBands
            // 
            grpRasterBands.Controls.Add(lblEmptyRasterBands);
            grpRasterBands.Controls.Add(tvwRasterBands);
            grpRasterBands.Dock = System.Windows.Forms.DockStyle.Fill;
            grpRasterBands.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            grpRasterBands.ForeColor = System.Drawing.Color.MediumBlue;
            grpRasterBands.Location = new System.Drawing.Point(0, 60);
            grpRasterBands.Margin = new System.Windows.Forms.Padding(0);
            grpRasterBands.Name = "grpRasterBands";
            grpRasterBands.Padding = new System.Windows.Forms.Padding(5);
            grpRasterBands.Size = new System.Drawing.Size(960, 440);
            grpRasterBands.TabIndex = 5;
            grpRasterBands.TabStop = false;
            grpRasterBands.Text = "grpRasterBands";
            // 
            // lblEmptyRasterBands
            // 
            lblEmptyRasterBands.Dock = System.Windows.Forms.DockStyle.Top;
            lblEmptyRasterBands.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            lblEmptyRasterBands.ForeColor = System.Drawing.Color.Red;
            lblEmptyRasterBands.Location = new System.Drawing.Point(5, 25);
            lblEmptyRasterBands.Margin = new System.Windows.Forms.Padding(0);
            lblEmptyRasterBands.Name = "lblEmptyRasterBands";
            lblEmptyRasterBands.Size = new System.Drawing.Size(950, 30);
            lblEmptyRasterBands.TabIndex = 6;
            lblEmptyRasterBands.Text = "lblEmptyRasterBands";
            lblEmptyRasterBands.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            lblEmptyRasterBands.Visible = false;
            // 
            // tvwRasterBands
            // 
            tvwRasterBands.BackColor = System.Drawing.Color.White;
            tvwRasterBands.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            tvwRasterBands.Dock = System.Windows.Forms.DockStyle.Fill;
            tvwRasterBands.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            tvwRasterBands.ForeColor = System.Drawing.Color.Black;
            tvwRasterBands.HideSelection = false;
            tvwRasterBands.ImageIndex = 0;
            tvwRasterBands.ImageList = ilTreeView;
            tvwRasterBands.LineColor = System.Drawing.Color.MediumBlue;
            tvwRasterBands.Location = new System.Drawing.Point(5, 25);
            tvwRasterBands.Margin = new System.Windows.Forms.Padding(0);
            tvwRasterBands.Name = "tvwRasterBands";
            tvwRasterBands.SelectedImageIndex = 0;
            tvwRasterBands.ShowLines = false;
            tvwRasterBands.ShowRootLines = false;
            tvwRasterBands.Size = new System.Drawing.Size(950, 410);
            tvwRasterBands.TabIndex = 5;
            tvwRasterBands.Visible = false;
            // 
            // grpSelectedRasterBand
            // 
            grpSelectedRasterBand.Controls.Add(lblSelectedRasterBand);
            grpSelectedRasterBand.Dock = System.Windows.Forms.DockStyle.Fill;
            grpSelectedRasterBand.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            grpSelectedRasterBand.ForeColor = System.Drawing.Color.MediumBlue;
            grpSelectedRasterBand.Location = new System.Drawing.Point(0, 0);
            grpSelectedRasterBand.Margin = new System.Windows.Forms.Padding(0);
            grpSelectedRasterBand.Name = "grpSelectedRasterBand";
            grpSelectedRasterBand.Padding = new System.Windows.Forms.Padding(5);
            grpSelectedRasterBand.Size = new System.Drawing.Size(960, 60);
            grpSelectedRasterBand.TabIndex = 0;
            grpSelectedRasterBand.TabStop = false;
            grpSelectedRasterBand.Text = "grpSelectedRasterBand";
            // 
            // lblSelectedRasterBand
            // 
            lblSelectedRasterBand.Dock = System.Windows.Forms.DockStyle.Fill;
            lblSelectedRasterBand.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, 238);
            lblSelectedRasterBand.ForeColor = System.Drawing.Color.Black;
            lblSelectedRasterBand.Location = new System.Drawing.Point(5, 25);
            lblSelectedRasterBand.Margin = new System.Windows.Forms.Padding(0);
            lblSelectedRasterBand.Name = "lblSelectedRasterBand";
            lblSelectedRasterBand.Size = new System.Drawing.Size(950, 30);
            lblSelectedRasterBand.TabIndex = 0;
            lblSelectedRasterBand.Text = "lblSelectedRasterBand";
            lblSelectedRasterBand.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ilTreeView
            // 
            ilTreeView.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            ilTreeView.ImageStream = (System.Windows.Forms.ImageListStreamer)resources.GetObject("ilTreeView.ImageStream");
            ilTreeView.TransparentColor = System.Drawing.Color.Transparent;
            ilTreeView.Images.SetKeyName(0, "RedBall");
            ilTreeView.Images.SetKeyName(1, "BlueBall");
            ilTreeView.Images.SetKeyName(2, "YellowBall");
            ilTreeView.Images.SetKeyName(3, "GreenBall");
            ilTreeView.Images.SetKeyName(4, "GrayBall");
            ilTreeView.Images.SetKeyName(5, "Lock");
            ilTreeView.Images.SetKeyName(6, "Schema");
            ilTreeView.Images.SetKeyName(7, "Vector");
            ilTreeView.Images.SetKeyName(8, "Raster");
            // 
            // ControlRasterBand
            // 
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            BackColor = System.Drawing.Color.Transparent;
            BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            Controls.Add(tlpMain);
            ForeColor = System.Drawing.Color.Black;
            Margin = new System.Windows.Forms.Padding(0);
            Name = "ControlRasterBand";
            Size = new System.Drawing.Size(960, 540);
            tlpMain.ResumeLayout(false);
            tlpButtons.ResumeLayout(false);
            pnlNext.ResumeLayout(false);
            pnlPrevious.ResumeLayout(false);
            grpRasterBands.ResumeLayout(false);
            grpSelectedRasterBand.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.TableLayoutPanel tlpButtons;
        private System.Windows.Forms.Panel pnlNext;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Panel pnlPrevious;
        private System.Windows.Forms.Button btnPrevious;
        private System.Windows.Forms.GroupBox grpRasterBands;
        private System.Windows.Forms.TreeView tvwRasterBands;
        private System.Windows.Forms.GroupBox grpSelectedRasterBand;
        private System.Windows.Forms.Label lblSelectedRasterBand;
        private System.Windows.Forms.Label lblEmptyRasterBands;
        private System.Windows.Forms.ImageList ilTreeView;
    }

}