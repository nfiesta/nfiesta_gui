﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.AuxiliaryData;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.Catalog;

namespace ZaJi.ModuleAuxiliaryData
{

    /// <summary>
    /// <para lang="cs">Ovládací prvek "Volba bodové vrstvy"</para>
    /// <para lang="en">Control "Point layer selection"</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    internal partial class ControlPointLayer
        : UserControl, INfiEstaControl, IAuxiliaryDataControl
    {

        #region Constants

        private const int ImageRedBall = 0;
        private const int ImageBlueBall = 1;
        private const int ImageYellowBall = 2;
        private const int ImageGreenBall = 3;
        private const int ImageGrayBall = 4;
        private const int ImageLock = 5;
        private const int ImageSchema = 6;
        private const int ImageVector = 7;
        private const int ImageRaster = 8;

        private const string GEOMETRY = "geometry";
        private const string POINT = "point";

        #endregion Constants


        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        private Control controlOwner;

        #endregion Private Fields


        #region Events

        /// <summary>
        /// <para lang="cs">Událost kliknutí na tlačítko "Předchozí"</para>
        /// <para lang="en">Click event on the "Previous" button</para>
        /// </summary>
        public event EventHandler PreviousClick;

        /// <summary>
        /// <para lang="cs">Událost kliknutí na tlačítko "Další"</para>
        /// <para lang="en">Click event on the "Next" button</para>
        /// </summary>
        public event EventHandler NextClick;

        #endregion Events


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor ovládacího prvku</para>
        /// <para lang="en">Control constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        public ControlPointLayer(Control controlOwner)
        {
            InitializeComponent();
            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                IAuxiliaryDataControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                if (controlOwner is not FormConfigCollectionGuide)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(FormConfigCollectionGuide)}.",
                        paramName: nameof(ControlOwner));
                }

                return controlOwner;
            }
            set
            {
                IAuxiliaryDataControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                if (value is not FormConfigCollectionGuide)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(FormConfigCollectionGuide)}.",
                        paramName: nameof(ControlOwner));
                }

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables (read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((IAuxiliaryDataControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro pomocná data (read-only)</para>
        /// <para lang="en">Module for auxiliary data setting (read-only)</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((IAuxiliaryDataControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">Skupina konfigurací</para>
        /// <para lang="en">Configuration collection</para>
        /// </summary>
        public ConfigCollection ConfigCollection
        {
            get
            {
                return ((FormConfigCollectionGuide)ControlOwner).ConfigCollection;
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(btnPrevious),                        "Předchozí" },
                        { nameof(btnNext),                            "Další" },
                        { nameof(grpSelectedPointLayer),              "Vybraná bodová vrstva:" },
                        { nameof(grpPointLayers),                     "Bodové vrstvy:" }
                    }
                    : languageFile.NationalVersion.Data.TryGetValue(
                        key: nameof(ControlPointLayer),
                        out Dictionary<string, string> dictNational)
                            ? dictNational
                            : [],

                LanguageVersion.International => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(btnPrevious),                        "Previous" },
                        { nameof(btnNext),                            "Next" },
                        { nameof(grpSelectedPointLayer),              "Selected point layer:" },
                        { nameof(grpPointLayers),                     "Point layers:" }
                    }
                    : languageFile.InternationalVersion.Data.TryGetValue(
                        key: nameof(ControlPointLayer),
                        out Dictionary<string, string> dictInternational)
                            ? dictInternational
                            : [],

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku</para>
        /// <para lang="en">Initialization of the control</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        private void Initialize(Control controlOwner)
        {
            BorderStyle = BorderStyle.None;
            ControlOwner = controlOwner;

            InitializeLabels();

            InitializeTreeView();

            tvwPointLayers.AfterSelect += new TreeViewEventHandler(
                (sender, e) =>
                {
                    SelectPointLayer();
                    SetCaption();
                    EnableNext();
                });

            btnPrevious.Click += new EventHandler(
                (sender, e) =>
                {
                    PreviousClick?.Invoke(sender: sender, e: e);
                });

            btnNext.Click += new EventHandler(
               (sender, e) =>
               {
                   NextClick?.Invoke(sender: sender, e: e);
               });
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků ovládacího prvku</para>
        /// <para lang="en">Initialization of control labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            btnPrevious.Text =
                labels.TryGetValue(key: nameof(btnPrevious),
                out string btnPreviousText)
                    ? btnPreviousText
                    : String.Empty;

            btnNext.Text =
                labels.TryGetValue(key: nameof(btnNext),
                out string btnNextText)
                    ? btnNextText
                    : String.Empty;

            grpSelectedPointLayer.Text =
                labels.TryGetValue(key: nameof(grpSelectedPointLayer),
                out string grpSelectedPointLayerText)
                    ? grpSelectedPointLayerText
                    : String.Empty;

            grpPointLayers.Text =
                labels.TryGetValue(key: nameof(grpPointLayers),
                out string grpPointLayersText)
                    ? grpPointLayersText
                    : String.Empty;
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent()
        {
            SetCaption();

            EnableNext();
        }

        /// <summary>
        /// <para lang="cs">Inicializace seznamu bodových vrstev</para>
        /// <para lang="en">Initializing list of point layers</para>
        /// </summary>
        private void InitializeTreeView()
        {
            foreach (DBSchema schema in
                Database.Postgres.Catalog.Schemas.Items
                .OrderBy(a => a.Name))
            {
                TreeNode nodeSchema = new()
                {
                    ImageIndex = ImageSchema,
                    Name = schema.Name,
                    SelectedImageIndex = ImageSchema,
                    Tag = schema,
                    Text = schema.Name
                };
                tvwPointLayers.Nodes.Add(node: nodeSchema);

                // Přidá tabulky, které obsahují Point do seznamu
                foreach (
                    DBTable table in schema.Tables.Items
                    .Where(a => a.Columns.Items
                        .Where(c => c.TypeName.Contains(
                            value: GEOMETRY,
                            comparisonType: StringComparison.CurrentCultureIgnoreCase))
                        .Where(c => c.TypeInfo.Contains(
                            value: POINT,
                            comparisonType: StringComparison.CurrentCultureIgnoreCase))
                        .Any())
                    .OrderBy(a => a.Name))
                {
                    TreeNode nodeTable = new()
                    {
                        ImageIndex = ImageVector,
                        Name = $"{schema.Name}.{table.Name}",
                        SelectedImageIndex = ImageRedBall,
                        Tag = table,
                        Text = table.Name
                    };
                    nodeSchema.Nodes.Add(node: nodeTable);
                }

                // Přidá cizí tabulky, které obsahují MultiPolygon nebo Polygon do seznamu
                foreach (
                    DBForeignTable foreignTable in schema.ForeignTables.Items
                    .Where(a => a.Columns.Items
                        .Where(c => c.TypeName.Contains(
                            value: GEOMETRY,
                            comparisonType: StringComparison.CurrentCultureIgnoreCase))
                        .Where(c => c.TypeInfo.Contains(
                            value: POINT,
                            comparisonType: StringComparison.CurrentCultureIgnoreCase))
                        .Any())
                    .OrderBy(a => a.Name))
                {
                    TreeNode nodeForeignTable = new()
                    {
                        ImageIndex = ImageVector,
                        Name = $"{schema.Name}.{foreignTable.Name}",
                        SelectedImageIndex = ImageRedBall,
                        Tag = foreignTable,
                        Text = foreignTable.Name
                    };
                    nodeSchema.Nodes.Add(node: nodeForeignTable);
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Výběr bodové vrstvy</para>
        /// <para lang="en">Point layer selection</para>
        /// </summary>
        private void SelectPointLayer()
        {
            if (ConfigCollection == null)
            {
                return;
            }

            if (tvwPointLayers.SelectedNode == null)
            {
                ConfigCollection.CatalogName = null;
                ConfigCollection.SchemaName = null;
                ConfigCollection.TableName = null;
                return;
            }

            switch (tvwPointLayers.SelectedNode.Level)
            {
                case 0:
                    if (tvwPointLayers.SelectedNode.Tag == null)
                    {
                        ConfigCollection.CatalogName = null;
                        ConfigCollection.SchemaName = null;
                        ConfigCollection.TableName = null;
                        return;
                    }
                    else if (tvwPointLayers.SelectedNode.Tag is DBSchema schema)
                    {
                        ConfigCollection.CatalogName =
                            !String.IsNullOrEmpty(value: schema?.Catalog?.Name)
                                ? schema?.Catalog?.Name
                                : null;
                        ConfigCollection.SchemaName =
                            !String.IsNullOrEmpty(value: schema?.Name)
                                ? schema?.Catalog?.Name
                                : null;
                        ConfigCollection.TableName = null;
                        return;
                    }
                    else
                    {
                        ConfigCollection.CatalogName = null;
                        ConfigCollection.SchemaName = null;
                        ConfigCollection.TableName = null;
                        return;
                    }

                case 1:
                    if (tvwPointLayers.SelectedNode.Tag == null)
                    {
                        ConfigCollection.CatalogName = null;
                        ConfigCollection.SchemaName = null;
                        ConfigCollection.TableName = null;
                        return;
                    }
                    else if (tvwPointLayers.SelectedNode.Tag is DBTable table)
                    {
                        ConfigCollection.CatalogName =
                            !String.IsNullOrEmpty(value: table?.Schema?.Catalog?.Name)
                                ? table?.Schema?.Catalog?.Name
                                : null;
                        ConfigCollection.SchemaName =
                            !String.IsNullOrEmpty(value: table?.Schema?.Name)
                                ? table?.Schema?.Name
                                : null;
                        ConfigCollection.TableName =
                            !String.IsNullOrEmpty(value: table?.Name)
                                ? table?.Name
                                : null;
                        return;
                    }
                    else if (tvwPointLayers.SelectedNode.Tag is DBForeignTable foreignTable)
                    {
                        ConfigCollection.CatalogName =
                            !String.IsNullOrEmpty(value: foreignTable?.Schema?.Catalog?.Name)
                                ? foreignTable?.Schema?.Catalog?.Name
                                : null;
                        ConfigCollection.SchemaName =
                            !String.IsNullOrEmpty(value: foreignTable?.Schema?.Name)
                                ? foreignTable?.Schema?.Name
                                : null;
                        ConfigCollection.TableName =
                            !String.IsNullOrEmpty(value: foreignTable?.Name)
                                ? foreignTable?.Name
                                : null;
                        return;
                    }
                    else
                    {
                        ConfigCollection.CatalogName = null;
                        ConfigCollection.SchemaName = null;
                        ConfigCollection.TableName = null;
                        return;
                    }

                default:
                    ConfigCollection.CatalogName = null;
                    ConfigCollection.SchemaName = null;
                    ConfigCollection.TableName = null;
                    return;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastaví nadpis - informace o vybrané bodové vrstvě</para>
        /// <para lang="en">Set caption - information about selected point layer</para>
        /// </summary>
        private void SetCaption()
        {
            if (ConfigCollection == null)
            {
                return;
            }

            lblSelectedPointLayer.Text =
                    !String.IsNullOrEmpty(ConfigCollection.SchemaName)
                        ? !String.IsNullOrEmpty(ConfigCollection.TableName)
                            ? $"{ConfigCollection.SchemaName}.{ConfigCollection.TableName}"
                            : String.Empty
                        : !String.IsNullOrEmpty(ConfigCollection.TableName)
                            ? $"{ConfigCollection.TableName}"
                            : String.Empty;
        }

        /// <summary>
        /// <para lang="cs">Povolí postup na další formulář</para>
        /// <para lang="en">Allows progression to the next form</para>
        /// </summary>
        private void EnableNext()
        {
            if (ConfigCollection == null)
            {
                btnNext.Enabled = false;
                return;
            }

            btnNext.Enabled =
                    !String.IsNullOrEmpty(ConfigCollection.LabelCs) &&
                    !String.IsNullOrEmpty(ConfigCollection.LabelEn) &&
                    !String.IsNullOrEmpty(ConfigCollection.DescriptionCs) &&
                    !String.IsNullOrEmpty(ConfigCollection.DescriptionEn) &&
                    (ConfigCollection.ConfigFunctionValue == ConfigFunctionEnum.PointLayer) &&
                    !String.IsNullOrEmpty(value: ConfigCollection?.CatalogName) &&
                    !String.IsNullOrEmpty(value: ConfigCollection?.SchemaName) &&
                    !String.IsNullOrEmpty(value: ConfigCollection?.TableName);
        }

        #endregion Methods

    }
}
