﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi.ModuleAuxiliaryData
{
    partial class ControlRasterCondition
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            tlpMain = new System.Windows.Forms.TableLayoutPanel();
            tlpButtons = new System.Windows.Forms.TableLayoutPanel();
            pnlNext = new System.Windows.Forms.Panel();
            btnNext = new System.Windows.Forms.Button();
            pnlPrevious = new System.Windows.Forms.Panel();
            btnPrevious = new System.Windows.Forms.Button();
            grpCondition = new System.Windows.Forms.GroupBox();
            txtCondition = new System.Windows.Forms.TextBox();
            grpColumns = new System.Windows.Forms.GroupBox();
            tlpColumns = new System.Windows.Forms.TableLayoutPanel();
            tlpCategories = new System.Windows.Forms.TableLayoutPanel();
            pnlComboBoxes = new System.Windows.Forms.Panel();
            pnlLabels = new System.Windows.Forms.Panel();
            pnlCheckBoxes = new System.Windows.Forms.Panel();
            tlpColumnsButtons = new System.Windows.Forms.TableLayoutPanel();
            pnlAddCondition = new System.Windows.Forms.Panel();
            btnAddCondition = new System.Windows.Forms.Button();
            pnlDeleteCondition = new System.Windows.Forms.Panel();
            btnDeleteCondition = new System.Windows.Forms.Button();
            tlpMain.SuspendLayout();
            tlpButtons.SuspendLayout();
            pnlNext.SuspendLayout();
            pnlPrevious.SuspendLayout();
            grpCondition.SuspendLayout();
            grpColumns.SuspendLayout();
            tlpColumns.SuspendLayout();
            tlpCategories.SuspendLayout();
            tlpColumnsButtons.SuspendLayout();
            pnlAddCondition.SuspendLayout();
            pnlDeleteCondition.SuspendLayout();
            SuspendLayout();
            // 
            // tlpMain
            // 
            tlpMain.ColumnCount = 1;
            tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.Controls.Add(tlpButtons, 0, 2);
            tlpMain.Controls.Add(grpCondition, 0, 1);
            tlpMain.Controls.Add(grpColumns, 0, 0);
            tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpMain.Location = new System.Drawing.Point(0, 0);
            tlpMain.Margin = new System.Windows.Forms.Padding(0);
            tlpMain.Name = "tlpMain";
            tlpMain.RowCount = 3;
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 70F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            tlpMain.Size = new System.Drawing.Size(960, 540);
            tlpMain.TabIndex = 3;
            // 
            // tlpButtons
            // 
            tlpButtons.BackColor = System.Drawing.Color.Transparent;
            tlpButtons.ColumnCount = 3;
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            tlpButtons.Controls.Add(pnlNext, 2, 0);
            tlpButtons.Controls.Add(pnlPrevious, 1, 0);
            tlpButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpButtons.Location = new System.Drawing.Point(0, 500);
            tlpButtons.Margin = new System.Windows.Forms.Padding(0);
            tlpButtons.Name = "tlpButtons";
            tlpButtons.RowCount = 1;
            tlpButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.Size = new System.Drawing.Size(960, 40);
            tlpButtons.TabIndex = 15;
            // 
            // pnlNext
            // 
            pnlNext.Controls.Add(btnNext);
            pnlNext.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlNext.Location = new System.Drawing.Point(800, 0);
            pnlNext.Margin = new System.Windows.Forms.Padding(0);
            pnlNext.Name = "pnlNext";
            pnlNext.Padding = new System.Windows.Forms.Padding(5);
            pnlNext.Size = new System.Drawing.Size(160, 40);
            pnlNext.TabIndex = 13;
            // 
            // btnNext
            // 
            btnNext.Dock = System.Windows.Forms.DockStyle.Fill;
            btnNext.Enabled = false;
            btnNext.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            btnNext.Location = new System.Drawing.Point(5, 5);
            btnNext.Margin = new System.Windows.Forms.Padding(0);
            btnNext.Name = "btnNext";
            btnNext.Size = new System.Drawing.Size(150, 30);
            btnNext.TabIndex = 9;
            btnNext.Text = "btnNext";
            btnNext.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            btnNext.UseVisualStyleBackColor = true;
            // 
            // pnlPrevious
            // 
            pnlPrevious.Controls.Add(btnPrevious);
            pnlPrevious.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlPrevious.Location = new System.Drawing.Point(640, 0);
            pnlPrevious.Margin = new System.Windows.Forms.Padding(0);
            pnlPrevious.Name = "pnlPrevious";
            pnlPrevious.Padding = new System.Windows.Forms.Padding(5);
            pnlPrevious.Size = new System.Drawing.Size(160, 40);
            pnlPrevious.TabIndex = 14;
            // 
            // btnPrevious
            // 
            btnPrevious.Dock = System.Windows.Forms.DockStyle.Fill;
            btnPrevious.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            btnPrevious.Location = new System.Drawing.Point(5, 5);
            btnPrevious.Margin = new System.Windows.Forms.Padding(0);
            btnPrevious.Name = "btnPrevious";
            btnPrevious.Size = new System.Drawing.Size(150, 30);
            btnPrevious.TabIndex = 9;
            btnPrevious.Text = "btnPrevious";
            btnPrevious.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            btnPrevious.UseVisualStyleBackColor = true;
            // 
            // grpCondition
            // 
            grpCondition.Controls.Add(txtCondition);
            grpCondition.Dock = System.Windows.Forms.DockStyle.Fill;
            grpCondition.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            grpCondition.ForeColor = System.Drawing.Color.MediumBlue;
            grpCondition.Location = new System.Drawing.Point(0, 350);
            grpCondition.Margin = new System.Windows.Forms.Padding(0);
            grpCondition.Name = "grpCondition";
            grpCondition.Padding = new System.Windows.Forms.Padding(5);
            grpCondition.Size = new System.Drawing.Size(960, 150);
            grpCondition.TabIndex = 5;
            grpCondition.TabStop = false;
            grpCondition.Text = "grpCondition";
            // 
            // txtCondition
            // 
            txtCondition.Dock = System.Windows.Forms.DockStyle.Fill;
            txtCondition.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            txtCondition.Location = new System.Drawing.Point(5, 25);
            txtCondition.Margin = new System.Windows.Forms.Padding(0);
            txtCondition.Multiline = true;
            txtCondition.Name = "txtCondition";
            txtCondition.ReadOnly = true;
            txtCondition.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            txtCondition.Size = new System.Drawing.Size(950, 120);
            txtCondition.TabIndex = 1;
            // 
            // grpColumns
            // 
            grpColumns.Controls.Add(tlpColumns);
            grpColumns.Dock = System.Windows.Forms.DockStyle.Fill;
            grpColumns.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            grpColumns.ForeColor = System.Drawing.Color.MediumBlue;
            grpColumns.Location = new System.Drawing.Point(0, 0);
            grpColumns.Margin = new System.Windows.Forms.Padding(0);
            grpColumns.Name = "grpColumns";
            grpColumns.Padding = new System.Windows.Forms.Padding(5);
            grpColumns.Size = new System.Drawing.Size(960, 350);
            grpColumns.TabIndex = 0;
            grpColumns.TabStop = false;
            grpColumns.Text = "grpColumns";
            // 
            // tlpColumns
            // 
            tlpColumns.ColumnCount = 1;
            tlpColumns.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpColumns.Controls.Add(tlpCategories, 0, 0);
            tlpColumns.Controls.Add(tlpColumnsButtons, 0, 1);
            tlpColumns.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpColumns.Location = new System.Drawing.Point(5, 25);
            tlpColumns.Margin = new System.Windows.Forms.Padding(0);
            tlpColumns.Name = "tlpColumns";
            tlpColumns.RowCount = 2;
            tlpColumns.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpColumns.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            tlpColumns.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            tlpColumns.Size = new System.Drawing.Size(950, 320);
            tlpColumns.TabIndex = 2;
            // 
            // tlpCategories
            // 
            tlpCategories.ColumnCount = 3;
            tlpCategories.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            tlpCategories.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpCategories.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            tlpCategories.Controls.Add(pnlComboBoxes, 1, 0);
            tlpCategories.Controls.Add(pnlLabels, 0, 0);
            tlpCategories.Controls.Add(pnlCheckBoxes, 2, 0);
            tlpCategories.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpCategories.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            tlpCategories.ForeColor = System.Drawing.Color.Black;
            tlpCategories.Location = new System.Drawing.Point(0, 0);
            tlpCategories.Margin = new System.Windows.Forms.Padding(0);
            tlpCategories.Name = "tlpCategories";
            tlpCategories.RowCount = 1;
            tlpCategories.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpCategories.Size = new System.Drawing.Size(950, 280);
            tlpCategories.TabIndex = 16;
            // 
            // pnlComboBoxes
            // 
            pnlComboBoxes.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlComboBoxes.Location = new System.Drawing.Point(150, 0);
            pnlComboBoxes.Margin = new System.Windows.Forms.Padding(0);
            pnlComboBoxes.Name = "pnlComboBoxes";
            pnlComboBoxes.Size = new System.Drawing.Size(770, 280);
            pnlComboBoxes.TabIndex = 1;
            // 
            // pnlLabels
            // 
            pnlLabels.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlLabels.Location = new System.Drawing.Point(0, 0);
            pnlLabels.Margin = new System.Windows.Forms.Padding(0);
            pnlLabels.Name = "pnlLabels";
            pnlLabels.Size = new System.Drawing.Size(150, 280);
            pnlLabels.TabIndex = 0;
            // 
            // pnlCheckBoxes
            // 
            pnlCheckBoxes.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlCheckBoxes.Location = new System.Drawing.Point(920, 0);
            pnlCheckBoxes.Margin = new System.Windows.Forms.Padding(0);
            pnlCheckBoxes.Name = "pnlCheckBoxes";
            pnlCheckBoxes.Size = new System.Drawing.Size(30, 280);
            pnlCheckBoxes.TabIndex = 2;
            // 
            // tlpColumnsButtons
            // 
            tlpColumnsButtons.BackColor = System.Drawing.Color.Transparent;
            tlpColumnsButtons.ColumnCount = 3;
            tlpColumnsButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpColumnsButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            tlpColumnsButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            tlpColumnsButtons.Controls.Add(pnlAddCondition, 2, 0);
            tlpColumnsButtons.Controls.Add(pnlDeleteCondition, 1, 0);
            tlpColumnsButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpColumnsButtons.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            tlpColumnsButtons.ForeColor = System.Drawing.Color.Black;
            tlpColumnsButtons.Location = new System.Drawing.Point(0, 280);
            tlpColumnsButtons.Margin = new System.Windows.Forms.Padding(0);
            tlpColumnsButtons.Name = "tlpColumnsButtons";
            tlpColumnsButtons.RowCount = 1;
            tlpColumnsButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpColumnsButtons.Size = new System.Drawing.Size(950, 40);
            tlpColumnsButtons.TabIndex = 15;
            // 
            // pnlAddCondition
            // 
            pnlAddCondition.Controls.Add(btnAddCondition);
            pnlAddCondition.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlAddCondition.Location = new System.Drawing.Point(790, 0);
            pnlAddCondition.Margin = new System.Windows.Forms.Padding(0);
            pnlAddCondition.Name = "pnlAddCondition";
            pnlAddCondition.Padding = new System.Windows.Forms.Padding(5);
            pnlAddCondition.Size = new System.Drawing.Size(160, 40);
            pnlAddCondition.TabIndex = 13;
            // 
            // btnAddCondition
            // 
            btnAddCondition.Dock = System.Windows.Forms.DockStyle.Fill;
            btnAddCondition.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            btnAddCondition.Location = new System.Drawing.Point(5, 5);
            btnAddCondition.Margin = new System.Windows.Forms.Padding(0);
            btnAddCondition.Name = "btnAddCondition";
            btnAddCondition.Size = new System.Drawing.Size(150, 30);
            btnAddCondition.TabIndex = 9;
            btnAddCondition.Text = "btnAddCondition";
            btnAddCondition.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            btnAddCondition.UseVisualStyleBackColor = true;
            // 
            // pnlDeleteCondition
            // 
            pnlDeleteCondition.Controls.Add(btnDeleteCondition);
            pnlDeleteCondition.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlDeleteCondition.Location = new System.Drawing.Point(630, 0);
            pnlDeleteCondition.Margin = new System.Windows.Forms.Padding(0);
            pnlDeleteCondition.Name = "pnlDeleteCondition";
            pnlDeleteCondition.Padding = new System.Windows.Forms.Padding(5);
            pnlDeleteCondition.Size = new System.Drawing.Size(160, 40);
            pnlDeleteCondition.TabIndex = 14;
            // 
            // btnDeleteCondition
            // 
            btnDeleteCondition.Dock = System.Windows.Forms.DockStyle.Fill;
            btnDeleteCondition.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            btnDeleteCondition.Location = new System.Drawing.Point(5, 5);
            btnDeleteCondition.Margin = new System.Windows.Forms.Padding(0);
            btnDeleteCondition.Name = "btnDeleteCondition";
            btnDeleteCondition.Size = new System.Drawing.Size(150, 30);
            btnDeleteCondition.TabIndex = 9;
            btnDeleteCondition.Text = "btnDeleteCondition";
            btnDeleteCondition.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            btnDeleteCondition.UseVisualStyleBackColor = true;
            // 
            // ControlRasterCondition
            // 
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            BackColor = System.Drawing.Color.Transparent;
            BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            Controls.Add(tlpMain);
            ForeColor = System.Drawing.Color.Black;
            Margin = new System.Windows.Forms.Padding(0);
            Name = "ControlRasterCondition";
            Size = new System.Drawing.Size(960, 540);
            tlpMain.ResumeLayout(false);
            tlpButtons.ResumeLayout(false);
            pnlNext.ResumeLayout(false);
            pnlPrevious.ResumeLayout(false);
            grpCondition.ResumeLayout(false);
            grpCondition.PerformLayout();
            grpColumns.ResumeLayout(false);
            tlpColumns.ResumeLayout(false);
            tlpCategories.ResumeLayout(false);
            tlpColumnsButtons.ResumeLayout(false);
            pnlAddCondition.ResumeLayout(false);
            pnlDeleteCondition.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.TableLayoutPanel tlpButtons;
        private System.Windows.Forms.Panel pnlNext;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Panel pnlPrevious;
        private System.Windows.Forms.Button btnPrevious;
        private System.Windows.Forms.GroupBox grpCondition;
        private System.Windows.Forms.TextBox txtCondition;
        private System.Windows.Forms.GroupBox grpColumns;
        private System.Windows.Forms.TableLayoutPanel tlpColumns;
        private System.Windows.Forms.TableLayoutPanel tlpCategories;
        private System.Windows.Forms.Panel pnlComboBoxes;
        private System.Windows.Forms.Panel pnlLabels;
        private System.Windows.Forms.Panel pnlCheckBoxes;
        private System.Windows.Forms.TableLayoutPanel tlpColumnsButtons;
        private System.Windows.Forms.Panel pnlAddCondition;
        private System.Windows.Forms.Button btnAddCondition;
        private System.Windows.Forms.Panel pnlDeleteCondition;
        private System.Windows.Forms.Button btnDeleteCondition;
    }
}
