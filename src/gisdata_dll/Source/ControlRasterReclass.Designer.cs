﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi.ModuleAuxiliaryData
{

    partial class ControlRasterReclass
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ControlRasterReclass));
            tlpMain = new System.Windows.Forms.TableLayoutPanel();
            chkReclass = new System.Windows.Forms.CheckBox();
            grpReclass = new System.Windows.Forms.GroupBox();
            lblReclass = new System.Windows.Forms.Label();
            tlpButtons = new System.Windows.Forms.TableLayoutPanel();
            pnlNext = new System.Windows.Forms.Panel();
            btnNext = new System.Windows.Forms.Button();
            pnlPrevious = new System.Windows.Forms.Panel();
            btnPrevious = new System.Windows.Forms.Button();
            grpRasterCategories = new System.Windows.Forms.GroupBox();
            lblEmptyRasterCategories = new System.Windows.Forms.Label();
            tvwRasterCategories = new System.Windows.Forms.TreeView();
            ilTreeView = new System.Windows.Forms.ImageList(components);
            grpRaster = new System.Windows.Forms.GroupBox();
            lblRaster = new System.Windows.Forms.Label();
            grpSelectedRasterCategory = new System.Windows.Forms.GroupBox();
            lblSelectedRasterCategory = new System.Windows.Forms.Label();
            tlpMain.SuspendLayout();
            grpReclass.SuspendLayout();
            tlpButtons.SuspendLayout();
            pnlNext.SuspendLayout();
            pnlPrevious.SuspendLayout();
            grpRasterCategories.SuspendLayout();
            grpRaster.SuspendLayout();
            grpSelectedRasterCategory.SuspendLayout();
            SuspendLayout();
            // 
            // tlpMain
            // 
            tlpMain.ColumnCount = 1;
            tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.Controls.Add(grpSelectedRasterCategory, 0, 2);
            tlpMain.Controls.Add(chkReclass, 0, 1);
            tlpMain.Controls.Add(grpReclass, 0, 4);
            tlpMain.Controls.Add(tlpButtons, 0, 5);
            tlpMain.Controls.Add(grpRasterCategories, 0, 3);
            tlpMain.Controls.Add(grpRaster, 0, 0);
            tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpMain.Location = new System.Drawing.Point(0, 0);
            tlpMain.Margin = new System.Windows.Forms.Padding(0);
            tlpMain.Name = "tlpMain";
            tlpMain.RowCount = 6;
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            tlpMain.Size = new System.Drawing.Size(960, 540);
            tlpMain.TabIndex = 5;
            // 
            // chkReclass
            // 
            chkReclass.Dock = System.Windows.Forms.DockStyle.Fill;
            chkReclass.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            chkReclass.ForeColor = System.Drawing.Color.Black;
            chkReclass.Location = new System.Drawing.Point(0, 50);
            chkReclass.Margin = new System.Windows.Forms.Padding(0);
            chkReclass.Name = "chkReclass";
            chkReclass.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            chkReclass.Size = new System.Drawing.Size(960, 50);
            chkReclass.TabIndex = 18;
            chkReclass.Text = "chkReclass";
            chkReclass.UseVisualStyleBackColor = true;
            // 
            // grpReclass
            // 
            grpReclass.Controls.Add(lblReclass);
            grpReclass.Dock = System.Windows.Forms.DockStyle.Fill;
            grpReclass.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            grpReclass.ForeColor = System.Drawing.Color.MediumBlue;
            grpReclass.Location = new System.Drawing.Point(0, 440);
            grpReclass.Margin = new System.Windows.Forms.Padding(0);
            grpReclass.Name = "grpReclass";
            grpReclass.Padding = new System.Windows.Forms.Padding(5);
            grpReclass.Size = new System.Drawing.Size(960, 60);
            grpReclass.TabIndex = 17;
            grpReclass.TabStop = false;
            grpReclass.Text = "grpReclass";
            // 
            // lblReclass
            // 
            lblReclass.Dock = System.Windows.Forms.DockStyle.Fill;
            lblReclass.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            lblReclass.ForeColor = System.Drawing.Color.Black;
            lblReclass.Location = new System.Drawing.Point(5, 25);
            lblReclass.Margin = new System.Windows.Forms.Padding(0);
            lblReclass.Name = "lblReclass";
            lblReclass.Size = new System.Drawing.Size(950, 30);
            lblReclass.TabIndex = 0;
            lblReclass.Text = "lblReclass";
            lblReclass.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tlpButtons
            // 
            tlpButtons.BackColor = System.Drawing.Color.Transparent;
            tlpButtons.ColumnCount = 3;
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            tlpButtons.Controls.Add(pnlNext, 2, 0);
            tlpButtons.Controls.Add(pnlPrevious, 1, 0);
            tlpButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpButtons.Location = new System.Drawing.Point(0, 500);
            tlpButtons.Margin = new System.Windows.Forms.Padding(0);
            tlpButtons.Name = "tlpButtons";
            tlpButtons.RowCount = 1;
            tlpButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.Size = new System.Drawing.Size(960, 40);
            tlpButtons.TabIndex = 15;
            // 
            // pnlNext
            // 
            pnlNext.Controls.Add(btnNext);
            pnlNext.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlNext.Location = new System.Drawing.Point(800, 0);
            pnlNext.Margin = new System.Windows.Forms.Padding(0);
            pnlNext.Name = "pnlNext";
            pnlNext.Padding = new System.Windows.Forms.Padding(5);
            pnlNext.Size = new System.Drawing.Size(160, 40);
            pnlNext.TabIndex = 13;
            // 
            // btnNext
            // 
            btnNext.Dock = System.Windows.Forms.DockStyle.Fill;
            btnNext.Enabled = false;
            btnNext.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            btnNext.Location = new System.Drawing.Point(5, 5);
            btnNext.Margin = new System.Windows.Forms.Padding(0);
            btnNext.Name = "btnNext";
            btnNext.Size = new System.Drawing.Size(150, 30);
            btnNext.TabIndex = 9;
            btnNext.Text = "btnNext";
            btnNext.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            btnNext.UseVisualStyleBackColor = true;
            // 
            // pnlPrevious
            // 
            pnlPrevious.Controls.Add(btnPrevious);
            pnlPrevious.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlPrevious.Location = new System.Drawing.Point(640, 0);
            pnlPrevious.Margin = new System.Windows.Forms.Padding(0);
            pnlPrevious.Name = "pnlPrevious";
            pnlPrevious.Padding = new System.Windows.Forms.Padding(5);
            pnlPrevious.Size = new System.Drawing.Size(160, 40);
            pnlPrevious.TabIndex = 14;
            // 
            // btnPrevious
            // 
            btnPrevious.Dock = System.Windows.Forms.DockStyle.Fill;
            btnPrevious.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            btnPrevious.Location = new System.Drawing.Point(5, 5);
            btnPrevious.Margin = new System.Windows.Forms.Padding(0);
            btnPrevious.Name = "btnPrevious";
            btnPrevious.Size = new System.Drawing.Size(150, 30);
            btnPrevious.TabIndex = 9;
            btnPrevious.Text = "btnPrevious";
            btnPrevious.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            btnPrevious.UseVisualStyleBackColor = true;
            // 
            // grpRasterCategories
            // 
            grpRasterCategories.Controls.Add(lblEmptyRasterCategories);
            grpRasterCategories.Controls.Add(tvwRasterCategories);
            grpRasterCategories.Dock = System.Windows.Forms.DockStyle.Fill;
            grpRasterCategories.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            grpRasterCategories.ForeColor = System.Drawing.Color.MediumBlue;
            grpRasterCategories.Location = new System.Drawing.Point(0, 160);
            grpRasterCategories.Margin = new System.Windows.Forms.Padding(0);
            grpRasterCategories.Name = "grpRasterCategories";
            grpRasterCategories.Padding = new System.Windows.Forms.Padding(5);
            grpRasterCategories.Size = new System.Drawing.Size(960, 280);
            grpRasterCategories.TabIndex = 5;
            grpRasterCategories.TabStop = false;
            grpRasterCategories.Text = "grpRasterCategories";
            // 
            // lblEmptyRasterCategories
            // 
            lblEmptyRasterCategories.Dock = System.Windows.Forms.DockStyle.Top;
            lblEmptyRasterCategories.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            lblEmptyRasterCategories.ForeColor = System.Drawing.Color.Red;
            lblEmptyRasterCategories.Location = new System.Drawing.Point(5, 25);
            lblEmptyRasterCategories.Margin = new System.Windows.Forms.Padding(0);
            lblEmptyRasterCategories.Name = "lblEmptyRasterCategories";
            lblEmptyRasterCategories.Size = new System.Drawing.Size(950, 30);
            lblEmptyRasterCategories.TabIndex = 6;
            lblEmptyRasterCategories.Text = "lblEmptyRasterCategories";
            lblEmptyRasterCategories.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            lblEmptyRasterCategories.Visible = false;
            // 
            // tvwRasterCategories
            // 
            tvwRasterCategories.BackColor = System.Drawing.Color.White;
            tvwRasterCategories.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            tvwRasterCategories.Dock = System.Windows.Forms.DockStyle.Fill;
            tvwRasterCategories.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            tvwRasterCategories.ForeColor = System.Drawing.Color.Black;
            tvwRasterCategories.HideSelection = false;
            tvwRasterCategories.ImageIndex = 0;
            tvwRasterCategories.ImageList = ilTreeView;
            tvwRasterCategories.LineColor = System.Drawing.Color.MediumBlue;
            tvwRasterCategories.Location = new System.Drawing.Point(5, 25);
            tvwRasterCategories.Margin = new System.Windows.Forms.Padding(0);
            tvwRasterCategories.Name = "tvwRasterCategories";
            tvwRasterCategories.SelectedImageIndex = 0;
            tvwRasterCategories.ShowLines = false;
            tvwRasterCategories.ShowRootLines = false;
            tvwRasterCategories.Size = new System.Drawing.Size(950, 250);
            tvwRasterCategories.TabIndex = 5;
            tvwRasterCategories.Visible = false;
            // 
            // ilTreeView
            // 
            ilTreeView.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            ilTreeView.ImageStream = (System.Windows.Forms.ImageListStreamer)resources.GetObject("ilTreeView.ImageStream");
            ilTreeView.TransparentColor = System.Drawing.Color.Transparent;
            ilTreeView.Images.SetKeyName(0, "RedBall");
            ilTreeView.Images.SetKeyName(1, "BlueBall");
            ilTreeView.Images.SetKeyName(2, "YellowBall");
            ilTreeView.Images.SetKeyName(3, "GreenBall");
            ilTreeView.Images.SetKeyName(4, "GrayBall");
            ilTreeView.Images.SetKeyName(5, "Lock");
            ilTreeView.Images.SetKeyName(6, "Schema");
            ilTreeView.Images.SetKeyName(7, "Vector");
            ilTreeView.Images.SetKeyName(8, "Raster");
            // 
            // grpRaster
            // 
            grpRaster.Controls.Add(lblRaster);
            grpRaster.Dock = System.Windows.Forms.DockStyle.Fill;
            grpRaster.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            grpRaster.ForeColor = System.Drawing.Color.MediumBlue;
            grpRaster.Location = new System.Drawing.Point(0, 0);
            grpRaster.Margin = new System.Windows.Forms.Padding(0);
            grpRaster.Name = "grpRaster";
            grpRaster.Padding = new System.Windows.Forms.Padding(5);
            grpRaster.Size = new System.Drawing.Size(960, 50);
            grpRaster.TabIndex = 0;
            grpRaster.TabStop = false;
            grpRaster.Text = "grpRaster";
            // 
            // lblRaster
            // 
            lblRaster.Dock = System.Windows.Forms.DockStyle.Fill;
            lblRaster.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            lblRaster.ForeColor = System.Drawing.Color.Black;
            lblRaster.Location = new System.Drawing.Point(5, 25);
            lblRaster.Margin = new System.Windows.Forms.Padding(0);
            lblRaster.Name = "lblRaster";
            lblRaster.Size = new System.Drawing.Size(950, 20);
            lblRaster.TabIndex = 0;
            lblRaster.Text = "lblRaster";
            lblRaster.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // grpSelectedRasterCategory
            // 
            grpSelectedRasterCategory.Controls.Add(lblSelectedRasterCategory);
            grpSelectedRasterCategory.Dock = System.Windows.Forms.DockStyle.Fill;
            grpSelectedRasterCategory.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            grpSelectedRasterCategory.ForeColor = System.Drawing.Color.MediumBlue;
            grpSelectedRasterCategory.Location = new System.Drawing.Point(0, 100);
            grpSelectedRasterCategory.Margin = new System.Windows.Forms.Padding(0);
            grpSelectedRasterCategory.Name = "grpSelectedRasterCategory";
            grpSelectedRasterCategory.Padding = new System.Windows.Forms.Padding(5);
            grpSelectedRasterCategory.Size = new System.Drawing.Size(960, 60);
            grpSelectedRasterCategory.TabIndex = 19;
            grpSelectedRasterCategory.TabStop = false;
            grpSelectedRasterCategory.Text = "grpSelectedRasterCategory";
            // 
            // lblSelectedRasterCategory
            // 
            lblSelectedRasterCategory.Dock = System.Windows.Forms.DockStyle.Fill;
            lblSelectedRasterCategory.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, 238);
            lblSelectedRasterCategory.ForeColor = System.Drawing.Color.Black;
            lblSelectedRasterCategory.Location = new System.Drawing.Point(5, 25);
            lblSelectedRasterCategory.Margin = new System.Windows.Forms.Padding(0);
            lblSelectedRasterCategory.Name = "lblSelectedRasterCategory";
            lblSelectedRasterCategory.Size = new System.Drawing.Size(950, 30);
            lblSelectedRasterCategory.TabIndex = 0;
            lblSelectedRasterCategory.Text = "lblSelectedRasterCategory";
            lblSelectedRasterCategory.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ControlRasterReclass
            // 
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            BackColor = System.Drawing.Color.Transparent;
            BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            Controls.Add(tlpMain);
            ForeColor = System.Drawing.Color.Black;
            Margin = new System.Windows.Forms.Padding(0);
            Name = "ControlRasterReclass";
            Size = new System.Drawing.Size(960, 540);
            tlpMain.ResumeLayout(false);
            grpReclass.ResumeLayout(false);
            tlpButtons.ResumeLayout(false);
            pnlNext.ResumeLayout(false);
            pnlPrevious.ResumeLayout(false);
            grpRasterCategories.ResumeLayout(false);
            grpRaster.ResumeLayout(false);
            grpSelectedRasterCategory.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.TableLayoutPanel tlpButtons;
        private System.Windows.Forms.Panel pnlNext;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Panel pnlPrevious;
        private System.Windows.Forms.Button btnPrevious;
        private System.Windows.Forms.GroupBox grpRasterCategories;
        private System.Windows.Forms.Label lblEmptyRasterCategories;
        private System.Windows.Forms.TreeView tvwRasterCategories;
        private System.Windows.Forms.GroupBox grpRaster;
        private System.Windows.Forms.Label lblRaster;
        private System.Windows.Forms.GroupBox grpReclass;
        private System.Windows.Forms.Label lblReclass;
        private System.Windows.Forms.CheckBox chkReclass;
        private System.Windows.Forms.ImageList ilTreeView;
        private System.Windows.Forms.GroupBox grpSelectedRasterCategory;
        private System.Windows.Forms.Label lblSelectedRasterCategory;
    }

}
