﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.AuxiliaryData;
using ZaJi.PostgreSQL;

namespace ZaJi.ModuleAuxiliaryData
{

    /// <summary>
    /// <para lang="cs">Formulář "Výběr skupiny konfigurací"</para>
    /// <para lang="en">Form "Select configuration collection"</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    internal partial class FormConfigCollectionSelector
        : Form, INfiEstaControl, IAuxiliaryDataControl
    {

        #region Constants

        private const int LevelConfigFunction = 0;
        private const int LevelConfigCollection = 1;
        private const int ImageRedBall = 0;
        private const int ImageBlueBall = 1;
        private const int ImageYellowBall = 2;
        private const int ImageGreenBall = 3;
        private const int ImageGrayBall = 4;
        private const int ImageLock = 5;
        private const int ImageSchema = 6;
        private const int ImageVector = 7;
        private const int ImageRaster = 8;

        #endregion Constants


        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Uzly v TreeView</para>
        /// <para lang="en">TreeView nodes</para>
        /// </summary>
        private List<TreeNode> nodes;

        /// <summary>
        /// <para lang="cs">Indikátor editace ovládacího prvku</para>
        /// <para lang="en">Control editing indicator</para>
        /// </summary>
        private bool onEdit;

        /// <summary>
        /// <para lang="cs">Seznam konfiguračních funkcí</para>
        /// <para lang="en">List of configuration functions</para>
        /// </summary>
        private List<ConfigFunction> configFunctions;

        #endregion Private Fields


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor ovládacího prvku</para>
        /// <para lang="en">Control constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        /// <param name="configFunctions">
        /// <para lang="cs">Seznam konfiguračních funkcí</para>
        /// <para lang="en">List of configuration functions</para>
        /// </param>
        public FormConfigCollectionSelector(
            Control controlOwner,
            List<ConfigFunction> configFunctions)
        {
            InitializeComponent();
            Initialize(
                controlOwner: controlOwner,
                configFunctions: configFunctions);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                IAuxiliaryDataControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                return controlOwner;
            }
            set
            {
                IAuxiliaryDataControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables (read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((IAuxiliaryDataControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro pomocná data (read-only)</para>
        /// <para lang="en">Module for auxiliary data setting (read-only)</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((IAuxiliaryDataControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">Vybraná skupina konfigurací</para>
        /// <para lang="en">Selected configuration collection</para>
        /// </summary>
        public ConfigCollection ConfigCollection
        {
            get
            {
                return
                    CheckedConfigCollections.FirstOrDefault<ConfigCollection>();
            }
            set
            {
                CheckOnlyOneNode(
                    level: LevelConfigCollection,
                    id: value?.Id ?? 0);
            }
        }

        /// <summary>
        /// <para lang="cs">Seznam konfiguračních funkcí</para>
        /// <para lang="en">List of configuration functions</para>
        /// </summary>
        private List<ConfigFunction> ConfigFunctions
        {
            get
            {
                return configFunctions ?? [];
            }
            set
            {
                configFunctions = value ?? [];
            }
        }

        /// <summary>
        /// <para lang="cs">Indikátor editace ovládacího prvku</para>
        /// <para lang="en">Control editing indicator</para>
        /// </summary>
        private bool OnEdit
        {
            get
            {
                return onEdit;
            }
            set
            {
                onEdit = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Uzly v TreeView</para>
        /// <para lang="en">TreeView nodes</para>
        /// </summary>
        private List<TreeNode> Nodes
        {
            get
            {
                return nodes ?? [];
            }
            set
            {
                nodes = value ?? [];
            }
        }

        /// <summary>
        /// <para lang="cs">Uzly v TreeView úroveň konfiguračních funkcí (read-only)</para>
        /// <para lang="en">TreeView nodes - configuration functions level (read-only)</para>
        /// </summary>
        private IEnumerable<TreeNode> ConfigFunctionNodes
        {
            get
            {
                return
                    Nodes
                        .Where(a => a.Level == LevelConfigFunction)
                        .Where(a => a.Tag != null)
                        .Where(a => a.Tag is ConfigFunction);
            }
        }

        /// <summary>
        /// <para lang="cs">Uzly v TreeView úroveň skupin konfigurací (read-only)</para>
        /// <para lang="en">TreeView nodes - configuration collections level (read-only)</para>
        /// </summary>
        private IEnumerable<TreeNode> ConfigCollectionNodes
        {
            get
            {
                return
                    Nodes
                        .Where(a => a.Level == LevelConfigCollection)
                        .Where(a => a.Tag != null)
                        .Where(a => a.Tag is ConfigCollection);
            }
        }

        /// <summary>
        /// <para lang="cs">Zaškrtnuté uzly skupin konfigurací (read-only)</para>
        /// <para lang="en">Checked configuration collection nodes (read-only)</para>
        /// </summary>
        private IEnumerable<TreeNode> CheckedConfigCollectionNodes
        {
            get
            {
                return
                    ConfigCollectionNodes
                        .Where(a => a.Checked);
            }
        }

        /// <summary>
        /// <para lang="cs">Zaškrtnuté skupiny konfigurací (read-only)</para>
        /// <para lang="en">Checked configuration collections (read-only)</para>
        /// </summary>
        private IEnumerable<ConfigCollection> CheckedConfigCollections
        {
            get
            {
                return CheckedConfigCollectionNodes
                    .Select(a => (ConfigCollection)a.Tag);
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(FormConfigCollectionSelector),           "Výběr skupiny konfigurací" },
                        { nameof(btnOK),                                  "OK" },
                        { nameof(btnCancel),                              "Zrušit" },
                        { nameof(grpItems),                               "Seznam skupin konfigurací:" },
                        { nameof(grpSelectedItem),                        "Vybraná skupina konfigurací:" }
                    }
                    : languageFile.NationalVersion.Data.TryGetValue(
                        key: nameof(FormConfigCollectionSelector),
                        out Dictionary<string, string> dictNational)
                            ? dictNational
                            : [],

                LanguageVersion.International => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(FormConfigCollectionSelector),           "Configuration collection selection" },
                        { nameof(btnOK),                                  "OK" },
                        { nameof(btnCancel),                              "Cancel" },
                        { nameof(grpItems),                               "List of configuration collections:" },
                        { nameof(grpSelectedItem),                        "Selected configuration collection:"}
                    }
                    : languageFile.InternationalVersion.Data.TryGetValue(
                        key: nameof(FormConfigCollectionSelector),
                        out Dictionary<string, string> dictInternational)
                            ? dictInternational
                            : [],

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku</para>
        /// <para lang="en">Initialization of the control</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        /// <param name="configFunctions">
        /// <para lang="cs">Seznam konfiguračních funkcí</para>
        /// <para lang="en">List of configuration functions</para>
        /// </param>
        private void Initialize(
            Control controlOwner,
            List<ConfigFunction> configFunctions)
        {
            ControlOwner = controlOwner;
            ConfigFunctions = configFunctions;
            Nodes = [];
            OnEdit = false;

            LoadContent();

            btnOK.Click += new EventHandler(
                (sender, e) =>
                {
                    DialogResult = DialogResult.OK;
                    Close();
                });

            btnCancel.Click += new EventHandler(
               (sender, e) =>
               {
                   DialogResult = DialogResult.Cancel;
                   Close();
               });

            tvwItems.AfterCheck += new TreeViewEventHandler(
                (sender, e) =>
                {
                    if (OnEdit)
                    {
                        return;
                    }

                    if ((e?.Node?.Tag != null) && (e.Node.Tag is ConfigCollection lastCheckedConfigCollection))
                    {
                        CheckOnlyOneNode(
                            level: LevelConfigCollection,
                            id: lastCheckedConfigCollection.Id);
                    }

                    SetCaption();
                });

            tvwItems.AfterSelect += new TreeViewEventHandler(
                (sender, e) =>
                {
                    tvwItems.SelectedNode = null;
                });
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků ovládacího prvku</para>
        /// <para lang="en">Initialization of control labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            Text =
                labels.TryGetValue(key: nameof(FormConfigCollectionSelector),
                out string frmConfigCollectionSelectorText)
                    ? frmConfigCollectionSelectorText
                    : String.Empty;

            grpSelectedItem.Text =
                labels.TryGetValue(key: nameof(grpSelectedItem),
                out string grpSelectedItemText)
                    ? grpSelectedItemText
                    : String.Empty;

            grpItems.Text =
                labels.TryGetValue(key: nameof(grpItems),
                out string grpItemsText)
                    ? grpItemsText
                    : String.Empty;

            btnOK.Text =
                labels.TryGetValue(key: nameof(btnOK),
                out string btnOKText)
                    ? btnOKText
                    : String.Empty;

            btnCancel.Text =
                labels.TryGetValue(key: nameof(btnCancel),
                out string btnCancelText)
                    ? btnCancelText
                    : String.Empty;

            SetCaption();

            foreach (TreeNode node in ConfigFunctionNodes)
            {
                node.Text =
                    (LanguageVersion == LanguageVersion.International)
                    ? ((ConfigFunction)node.Tag).LabelEn
                    : (LanguageVersion == LanguageVersion.National)
                        ? ((ConfigFunction)node.Tag).LabelCs
                        : ((ConfigFunction)node.Tag).LabelEn;
            }

            foreach (TreeNode node in ConfigCollectionNodes)
            {
                node.Text =
                    (LanguageVersion == LanguageVersion.International)
                    ? ((ConfigCollection)node.Tag).LabelEn
                    : (LanguageVersion == LanguageVersion.National)
                        ? ((ConfigCollection)node.Tag).LabelCs
                        : ((ConfigCollection)node.Tag).LabelEn;
            }
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent()
        {
            InitializeTreeView();

            InitializeLabels();

            SetCaption();
        }

        /// <summary>
        /// <para lang="cs">Zobrazení skupin konfigurací v uzlech TreeView</para>
        /// <para lang="en">Displaying configuration collections in TreeView nodes</para>
        /// </summary>
        private void InitializeTreeView()
        {
            // Zabrání prokreslování TreeView během editace
            tvwItems.BeginUpdate();

            // Smazání všech uzlů z TreeView
            Nodes.Clear();
            tvwItems.Nodes.Clear();

            // (Seznam konfiguračních funkcí)
            foreach (ConfigFunction configFunction
                in Database.SAuxiliaryData.CConfigFunction.Items
                .Where(a => ConfigFunctions.Select(a => a.Id).Contains(value: a.Id)))
            {
                TreeNode node = new()
                {
                    ImageIndex = ImageBlueBall,
                    Name = configFunction.Id.ToString(),
                    SelectedImageIndex = ImageBlueBall,
                    Tag = configFunction,
                    Text =
                        (LanguageVersion == LanguageVersion.International)
                            ? configFunction.LabelEn
                            : (LanguageVersion == LanguageVersion.National)
                                ? configFunction.LabelCs
                                : configFunction.LabelEn
                };
                Nodes.Add(item: node);
                tvwItems.Nodes.Add(node: node);

                Functions.HideCheckBox(
                    treeView: tvwItems,
                    node: node);
            }

            // (Seznam skupin konfigurací)
            foreach (ConfigCollection configCollection
                in Database.SAuxiliaryData.TConfigCollection.Items
                .Where(a => ConfigFunctions.Select(b => b.Id).Contains(value: a.ConfigFunction.Id))
                .Where(a => a.Closed))
            {
                TreeNode parent = ConfigFunctionNodes
                    .Where(a => ((ConfigFunction)a.Tag).Id == configCollection.ConfigFunctionId)
                    .FirstOrDefault<TreeNode>();

                if (parent != null)
                {
                    TreeNode node = new()
                    {
                        Checked = false,
                        ImageIndex =
                            configCollection.Closed ? ImageLock : ImageYellowBall,
                        Name = configCollection.Id.ToString(),
                        SelectedImageIndex =
                            configCollection.Closed ? ImageLock : ImageYellowBall,
                        Tag = configCollection,
                        Text =
                            (LanguageVersion == LanguageVersion.International)
                                    ? configCollection.LabelEn
                                    : (LanguageVersion == LanguageVersion.National)
                                        ? configCollection.LabelCs
                                        : configCollection.LabelEn
                    };
                    Nodes.Add(item: node);
                    parent.Nodes.Add(node: node);
                }
            }

            tvwItems.EndUpdate();

            ExpandCheckedNodes();

            HideCheckBoxesForNodes();
        }

        /// <summary>
        /// <para lang="cs">Rozbalí všechny uzly, které obsahují alespoň jeden zaškrtnutý uzel</para>
        /// <para lang="en">Expand all nodes that contain at least one checked node</para>
        /// </summary>
        private void ExpandCheckedNodes()
        {
            // Zabrání prokreslování TreeView během editace
            tvwItems.BeginUpdate();

            // Rozbalí všechny uzly, které obsahují alespoň jeden zaškrtnutý uzel
            foreach (TreeNode nodeConfigFunction in tvwItems.Nodes)
            {
                bool check = false;
                foreach (TreeNode nodeConfigCollection in nodeConfigFunction.Nodes)
                {
                    check = check || nodeConfigCollection.Checked;
                }
                if (check)
                {
                    nodeConfigFunction.Expand();
                }
                else
                {
                    nodeConfigFunction.Collapse();
                }
            }

            tvwItems.EndUpdate();
        }

        /// <summary>
        /// <para lang="cs">Skryje zaškrtávací políčka u uzlů pro výpočetní funkce</para>
        /// <para lang="en">Hides checkboxes for configuration function nodes</para>
        /// </summary>
        private void HideCheckBoxesForNodes()
        {
            // Zabrání prokreslování TreeView během editace
            tvwItems.BeginUpdate();

            foreach (TreeNode node in ConfigFunctionNodes)
            {
                Functions.HideCheckBox(
                    treeView: tvwItems,
                    node: node);
            }

            tvwItems.EndUpdate();
        }

        /// <summary>
        /// <para lang="cs">
        /// Vrátí uzel z TreeView odpovídající identifikačnímu číslu
        /// funkce nebo skupiny konfigurací nebo konfiguraci
        /// </para>
        /// <para lang="en">
        /// Returns the TreeView node corresponding to
        /// the function, configuration collection or configuration identifier
        /// </para>
        /// </summary>
        /// <param name="level">
        /// <para lang="cs">Úroveň uzlu v TreeView</para>
        /// <para lang="en">TreeView node level</para>
        /// </param>
        /// <param name="id">
        /// <para lang="cs">Identifikační číslo funkce, skupiny konfigurací, konfigurace</para>
        /// <para lang="en">Function, configuration collection or configuration identifier</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">
        /// Vrátí uzel z TreeView odpovídající identifikačnímu číslu
        /// funkce nebo skupiny konfigurací nebo konfiguraci
        /// </para>
        /// <para lang="en">
        /// Returns the TreeView node corresponding to
        /// the function, configuration collection or configuration identifier
        /// </para>
        /// </returns>
        private TreeNode FindNode(int level, int id)
        {
            return level switch
            {
                LevelConfigFunction => ConfigFunctionNodes
                        .Where(a => ((ConfigFunction)a.Tag).Id == id)
                        .FirstOrDefault<TreeNode>(),
                LevelConfigCollection => ConfigCollectionNodes
                        .Where(a => ((ConfigCollection)a.Tag).Id == id)
                        .FirstOrDefault<TreeNode>(),
                _ => null,
            };
        }

        /// <summary>
        /// <para lang="cs">Zaškrtne pouze jeden uzel v TreeView</para>
        /// <para lang="en">Check only one node in TreeView</para>
        /// </summary>
        /// <param name="level">
        /// <para lang="cs">Úroveň uzlu v TreeView</para>
        /// <para lang="en">TreeView node level</para>
        /// </param>
        /// <param name="id">
        /// <para lang="cs">Identifikační číslo funkce, skupiny konfigurací, konfigurace</para>
        /// <para lang="en">Function, configuration collection or configuration identifier</para>
        /// </param>
        private void CheckOnlyOneNode(int level, int id)
        {
            OnEdit = true;

            foreach (TreeNode node in ConfigCollectionNodes)
            {
                node.Checked = false;
            }

            TreeNode checkedNode = FindNode(level: level, id: id);
            if (checkedNode != null)
            {
                checkedNode.Checked = true;
            }

            OnEdit = false;

            SetCaption();
        }

        /// <summary>
        /// <para lang="cs">Nastaví nadpis se seznamem vybraných skupin konfigurací</para>
        /// <para lang="en">Sets the title with a list of selected configuration collections</para>
        /// </summary>
        private void SetCaption()
        {
            string strSelectedItemsEn =
                CheckedConfigCollections.Any()
                    ? CheckedConfigCollections
                        .Select(a => a.ExtendedLabelEn)
                        .Aggregate((a, b) => $"{a}, {b}")
                    : String.Empty;

            string strSelectedItemsCs =
                CheckedConfigCollections.Any()
                   ? CheckedConfigCollections
                    .Select(a => a.ExtendedLabelCs)
                    .Aggregate((a, b) => $"{a}, {b}")
                    : String.Empty;

            lblSelectedItem.Text =
                (LanguageVersion == LanguageVersion.International)
                    ? strSelectedItemsEn
                    : (LanguageVersion == LanguageVersion.National)
                        ? strSelectedItemsCs
                        : strSelectedItemsEn;
        }

        #endregion Methods

    }
}
