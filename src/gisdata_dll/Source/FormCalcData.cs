﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.AuxiliaryData;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi.ModuleAuxiliaryData
{

    /// <summary>
    /// <para lang="cs">Formulář pro zobrazení a výpočet hodnot pomocné proměnné v inventarizačních bodech</para>
    /// <para lang="en">Form for displaying and calculating auxiliary variable values at inventory points</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    internal partial class FormCalcData
        : Form, INfiEstaControl, IAuxiliaryDataControl
    {

        #region Constants

        private const string ID = "Id";

        #endregion Constants


        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Pomocná data na inventarizačních plochách</para>
        /// <para lang="en">Auxiliary inventory plot data - data view</para>
        /// </summary>
        private VwAuxDataList viewAuxData;

        #endregion Private Fields


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor ovládacího prvku</para>
        /// <para lang="en">Control constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        public FormCalcData(Control controlOwner)
        {
            InitializeComponent();
            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                IAuxiliaryDataControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                if (controlOwner is not ControlAuxiliaryDataMain)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(ControlAuxiliaryDataMain)}.",
                        paramName: nameof(ControlOwner));
                }

                return controlOwner;
            }
            set
            {
                IAuxiliaryDataControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                if (value is not ControlAuxiliaryDataMain)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(ControlAuxiliaryDataMain)}.",
                        paramName: nameof(ControlOwner));
                }

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables (read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((IAuxiliaryDataControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro pomocná data (read-only)</para>
        /// <para lang="en">Module for auxiliary data setting (read-only)</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((IAuxiliaryDataControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">Pomocná data na inventarizačních plochách</para>
        /// <para lang="en">Auxiliary inventory plot data - data view</para>
        /// </summary>
        private VwAuxDataList ViewAuxData
        {
            get
            {
                return viewAuxData ??
                    new VwAuxDataList(database: Database);
            }
            set
            {
                viewAuxData = value ??
                    new VwAuxDataList(database: Database);
            }
        }

        /// <summary>
        /// <para lang="cs">Skupina konfigurací</para>
        /// <para lang="en">Configuration collection</para>
        /// </summary>
        public ConfigCollection ConfigCollection
        {
            get
            {
                return ((ControlAuxiliaryDataMain)ControlOwner).AvailableConfigCollection;
            }
        }

        /// <summary>
        /// <para lang="cs">Aktuální verze databázové extenze (read-only)</para>
        /// <para lang="en">Current database extension version (read-only)</para>
        /// </summary>
        public ZaJi.NfiEstaPg.AuxiliaryData.ExtensionVersion ExtensionVersion
        {
            get
            {
                return ((ControlAuxiliaryDataMain)ControlOwner).ExtensionVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Aktuální verze modulu pro pomocná data (read-only)</para>
        /// <para lang="en">Current module for auxiliary data version (read-only)</para>
        /// </summary>
        public ZaJi.NfiEstaPg.AuxiliaryData.GUIVersion GUIVersion
        {
            get
            {
                return ((ControlAuxiliaryDataMain)ControlOwner).GUIVersion;
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(FormCalcData),                 "Hodnoty pomocné proměnné v inventarizačních bodech" },

                        { nameof(btnCalculate),                 "Zahájit výpočet průniků bodové vrstvy s vrstvou úhrnů pomocné proměnné" },
                        { nameof(btnClose),                     "Zavřít" },
                        { nameof(btnDelete),                    "Smazat vypočtené průniky" },
                        { nameof(btnReload),                    "Znovu načíst hodnoty pomocné proměnné v inventarizačních bodech" },
                        { nameof(btnStop),                      "Zastavit výpočet průniků bodové vrstvy s vrstvou úhrnů pomocné proměnné" },

                        { nameof(cboConfiguration),             "LabelCs" },
                        { nameof(cboExtensionVersion),          "LabelCs" },
                        { nameof(cboGUIVersion),                "LabelCs" },

                        { nameof(grpAuxiliaryVariableValues),   "Hodnoty pomocné proměnné v inventarizačních bodech:" },
                        { nameof(grpConfigCollection),          String.Empty },

                        { nameof(lblConfigurationCaption),      "Kategorie:" },
                        { nameof(lblExtensionVersionCaption),   "Verze extenze:" },
                        { nameof(lblGUIVersionCaption),         "Verze modulu:" },

                        // DataGridView
                        // 0
                        { $"col-{VwAuxDataList.Name}.{VwAuxDataList.ColId.Name}",
                            "Identifikátor" },
                        { $"tip-{VwAuxDataList.Name}.{VwAuxDataList.ColId.Name}",
                            "Identifikátor" },
                        // 1
                        { $"col-{VwAuxDataList.Name}.{VwAuxDataList.ColConfigCollectionId.Name}",
                            "Skupina - identifikační číslo" },
                        { $"tip-{VwAuxDataList.Name}.{VwAuxDataList.ColConfigCollectionId.Name}",
                            "Skupina - identifikační číslo" },
                        // 2
                        { $"col-{VwAuxDataList.Name}.{VwAuxDataList.ColConfigId.Name}",
                            "Kategorie" },
                        { $"tip-{VwAuxDataList.Name}.{VwAuxDataList.ColConfigId.Name}",
                            "Kategorie - identifikační číslo" },
                        // 3
                        { $"col-{VwAuxDataList.Name}.{VwAuxDataList.ColConfigLabelCs.Name}",
                            "Kategorie" },
                        { $"tip-{VwAuxDataList.Name}.{VwAuxDataList.ColConfigLabelCs.Name}",
                            "Kategorie [cs]" },
                        // 4
                        { $"col-{VwAuxDataList.Name}.{VwAuxDataList.ColConfigDescriptionCs.Name}",
                            "Kategorie" },
                        { $"tip-{VwAuxDataList.Name}.{VwAuxDataList.ColConfigDescriptionCs.Name}",
                            "Kategorie - popis [cs]" },
                        // 5
                        { $"col-{VwAuxDataList.Name}.{VwAuxDataList.ColConfigLabelEn.Name}",
                            "Kategorie" },
                        { $"tip-{VwAuxDataList.Name}.{VwAuxDataList.ColConfigLabelEn.Name}",
                            "Kategorie [en]" },
                        // 6
                        { $"col-{VwAuxDataList.Name}.{VwAuxDataList.ColConfigDescriptionEn.Name}",
                            "Kategorie" },
                        { $"tip-{VwAuxDataList.Name}.{VwAuxDataList.ColConfigDescriptionEn.Name}",
                            "Kategorie - popis [en]" },
                        // 7
                        { $"col-{VwAuxDataList.Name}.{VwAuxDataList.ColExtensionVersionId.Name}",
                            "Verze extenze" },
                        { $"tip-{VwAuxDataList.Name}.{VwAuxDataList.ColExtensionVersionId.Name}",
                            "Verze extenze - identifikační číslo" },
                        // 8
                        { $"col-{VwAuxDataList.Name}.{VwAuxDataList.ColExtensionVersionLabelCs.Name}",
                            "Verze extenze" },
                        { $"tip-{VwAuxDataList.Name}.{VwAuxDataList.ColExtensionVersionLabelCs.Name}",
                            "Verze extenze [cs]" },
                        // 9
                        { $"col-{VwAuxDataList.Name}.{VwAuxDataList.ColExtensionVersionDescriptionCs.Name}",
                            "Verze extenze" },
                        { $"tip-{VwAuxDataList.Name}.{VwAuxDataList.ColExtensionVersionDescriptionCs.Name}",
                            "Verze extenze - popis [cs]" },
                        // 10
                        { $"col-{VwAuxDataList.Name}.{VwAuxDataList.ColExtensionVersionLabelEn.Name}",
                            "Verze extenze" },
                        { $"tip-{VwAuxDataList.Name}.{VwAuxDataList.ColExtensionVersionLabelEn.Name}",
                            "Verze extenze [en]" },
                        // 11
                        { $"col-{VwAuxDataList.Name}.{VwAuxDataList.ColExtensionVersionDescriptionEn.Name}",
                            "Verze extenze" },
                        { $"tip-{VwAuxDataList.Name}.{VwAuxDataList.ColExtensionVersionDescriptionEn.Name}",
                            "Verze extenze - popis [en]" },
                        // 12
                        { $"col-{VwAuxDataList.Name}.{VwAuxDataList.ColGUIVersionId.Name}",
                            "Verze modulu" },
                        { $"tip-{VwAuxDataList.Name}.{VwAuxDataList.ColGUIVersionId.Name}",
                            "Verze modulu - identifikační číslo" },
                        // 13
                        { $"col-{VwAuxDataList.Name}.{VwAuxDataList.ColGUIVersionLabelCs.Name}",
                            "Verze modulu" },
                        { $"tip-{VwAuxDataList.Name}.{VwAuxDataList.ColGUIVersionLabelCs.Name}",
                            "Verze modulu [cs]" },
                        // 14
                        { $"col-{VwAuxDataList.Name}.{VwAuxDataList.ColGUIVersionDescriptionCs.Name}",
                            "Verze modulu" },
                        { $"tip-{VwAuxDataList.Name}.{VwAuxDataList.ColGUIVersionDescriptionCs.Name}",
                            "Verze modulu - popis [cs]" },
                        // 15
                        { $"col-{VwAuxDataList.Name}.{VwAuxDataList.ColGUIVersionLabelEn.Name}",
                            "Verze modulu" },
                        { $"tip-{VwAuxDataList.Name}.{VwAuxDataList.ColGUIVersionLabelEn.Name}",
                            "Verze modulu [en]" },
                        // 16
                        { $"col-{VwAuxDataList.Name}.{VwAuxDataList.ColGUIVersionDescriptionEn.Name}",
                            "Verze modulu" },
                        { $"tip-{VwAuxDataList.Name}.{VwAuxDataList.ColGUIVersionDescriptionEn.Name}",
                            "Verze modulu - popis [en]" },
                        // 17
                        { $"col-{VwAuxDataList.Name}.{VwAuxDataList.ColIdentPoint.Name}",
                            "Inventarizační plocha" },
                        { $"tip-{VwAuxDataList.Name}.{VwAuxDataList.ColIdentPoint.Name}",
                            "Inventariazační plocha" },
                        // 18
                        { $"col-{VwAuxDataList.Name}.{VwAuxDataList.ColGid.Name}",
                            "GID" },
                        { $"tip-{VwAuxDataList.Name}.{VwAuxDataList.ColGid.Name}",
                            "GID" },
                        // 19
                        { $"col-{VwAuxDataList.Name}.{VwAuxDataList.ColAuxValue.Name}",
                            "Hodnota pomocné proměnné" },
                        { $"tip-{VwAuxDataList.Name}.{VwAuxDataList.ColAuxValue.Name}",
                            "Hodnota pomocné proměnné" },
                        // 20
                        { $"col-{VwAuxDataList.Name}.{VwAuxDataList.ColEstDate.Name}",
                            "Datum výpočtu" },
                        { $"tip-{VwAuxDataList.Name}.{VwAuxDataList.ColEstDate.Name}",
                            "Datum výpočtu" }
                    }
                    : languageFile.NationalVersion.Data.TryGetValue(
                        key: nameof(FormCalcData),
                        out Dictionary<string, string> dictNational)
                            ? dictNational
                            : [],

                LanguageVersion.International => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(FormCalcData),                 "Auxiliary variable values at inventory points" },

                        { nameof(btnCalculate),                 "Begin calculation of intersection point layer with auxiliary variable totals layer" },
                        { nameof(btnClose),                     "Close" },
                        { nameof(btnDelete),                    "Delete calculated intersections" },
                        { nameof(btnReload),                    "Reload auxiliary variable values at inventory points" },
                        { nameof(btnStop),                      "Stop calculation of intersection point layer with auxiliary variable totals layer" },

                        { nameof(cboConfiguration),             "LabelEn" },
                        { nameof(cboExtensionVersion),          "LabelEn" },
                        { nameof(cboGUIVersion),                "LabelEn" },

                        { nameof(grpAuxiliaryVariableValues),   "Auxiliary variable values at inventory points:" },
                        { nameof(grpConfigCollection),          String.Empty },

                        { nameof(lblConfigurationCaption),      "Category:" },
                        { nameof(lblExtensionVersionCaption),   "Extension version:" },
                        { nameof(lblGUIVersionCaption),         "Module version:" },

                        // DataGridView
                        // 0
                        { $"col-{VwAuxDataList.Name}.{VwAuxDataList.ColId.Name}",
                            "Identifier" },
                        { $"tip-{VwAuxDataList.Name}.{VwAuxDataList.ColId.Name}",
                            "Identifier" },
                        // 1
                        { $"col-{VwAuxDataList.Name}.{VwAuxDataList.ColConfigCollectionId.Name}",
                            "Category" },
                        { $"tip-{VwAuxDataList.Name}.{VwAuxDataList.ColConfigCollectionId.Name}",
                            "Category - identifier" },
                        // 2
                        { $"col-{VwAuxDataList.Name}.{VwAuxDataList.ColConfigId.Name}",
                            "Category" },
                        { $"tip-{VwAuxDataList.Name}.{VwAuxDataList.ColConfigId.Name}",
                            "Category - identifier" },
                        // 3
                        { $"col-{VwAuxDataList.Name}.{VwAuxDataList.ColConfigLabelCs.Name}",
                            "Category" },
                        { $"tip-{VwAuxDataList.Name}.{VwAuxDataList.ColConfigLabelCs.Name}",
                            "Category [cs]" },
                        // 4
                        { $"col-{VwAuxDataList.Name}.{VwAuxDataList.ColConfigDescriptionCs.Name}",
                            "Category" },
                        { $"tip-{VwAuxDataList.Name}.{VwAuxDataList.ColConfigDescriptionCs.Name}",
                            "Category - description [cs]" },
                        // 5
                        { $"col-{VwAuxDataList.Name}.{VwAuxDataList.ColConfigLabelEn.Name}",
                            "Category" },
                        { $"tip-{VwAuxDataList.Name}.{VwAuxDataList.ColConfigLabelEn.Name}",
                            "Category [en]" },
                        // 6
                        { $"col-{VwAuxDataList.Name}.{VwAuxDataList.ColConfigDescriptionEn.Name}",
                            "Category" },
                        { $"tip-{VwAuxDataList.Name}.{VwAuxDataList.ColConfigDescriptionEn.Name}",
                            "Category - description [en]" },
                        // 7
                        { $"col-{VwAuxDataList.Name}.{VwAuxDataList.ColExtensionVersionId.Name}",
                            "Extension version" },
                        { $"tip-{VwAuxDataList.Name}.{VwAuxDataList.ColExtensionVersionId.Name}",
                            "Extension version - identifier" },
                        // 8
                        { $"col-{VwAuxDataList.Name}.{VwAuxDataList.ColExtensionVersionLabelCs.Name}",
                            "Extension version" },
                        { $"tip-{VwAuxDataList.Name}.{VwAuxDataList.ColExtensionVersionLabelCs.Name}",
                            "Extension version [cs]" },
                        // 9
                        { $"col-{VwAuxDataList.Name}.{VwAuxDataList.ColExtensionVersionDescriptionCs.Name}",
                            "Extension version" },
                        { $"tip-{VwAuxDataList.Name}.{VwAuxDataList.ColExtensionVersionDescriptionCs.Name}",
                            "Extension version - description [cs]" },
                        // 10
                        { $"col-{VwAuxDataList.Name}.{VwAuxDataList.ColExtensionVersionLabelEn.Name}",
                            "Extension version" },
                        { $"tip-{VwAuxDataList.Name}.{VwAuxDataList.ColExtensionVersionLabelEn.Name}",
                            "Extension version [en]" },
                        // 11
                        { $"col-{VwAuxDataList.Name}.{VwAuxDataList.ColExtensionVersionDescriptionEn.Name}",
                            "Extension version" },
                        { $"tip-{VwAuxDataList.Name}.{VwAuxDataList.ColExtensionVersionDescriptionEn.Name}",
                            "Extension version - description [en]" },
                        // 12
                        { $"col-{VwAuxDataList.Name}.{VwAuxDataList.ColGUIVersionId.Name}",
                            "Module version" },
                        { $"tip-{VwAuxDataList.Name}.{VwAuxDataList.ColGUIVersionId.Name}",
                            "Module version - identifier" },
                        // 13
                        { $"col-{VwAuxDataList.Name}.{VwAuxDataList.ColGUIVersionLabelCs.Name}",
                            "Module version" },
                        { $"tip-{VwAuxDataList.Name}.{VwAuxDataList.ColGUIVersionLabelCs.Name}",
                            "Module version [cs]" },
                        // 14
                        { $"col-{VwAuxDataList.Name}.{VwAuxDataList.ColGUIVersionDescriptionCs.Name}",
                            "Module version" },
                        { $"tip-{VwAuxDataList.Name}.{VwAuxDataList.ColGUIVersionDescriptionCs.Name}",
                            "Module version - description [cs]" },
                        // 15
                        { $"col-{VwAuxDataList.Name}.{VwAuxDataList.ColGUIVersionLabelEn.Name}",
                            "Module version" },
                        { $"tip-{VwAuxDataList.Name}.{VwAuxDataList.ColGUIVersionLabelEn.Name}",
                            "Module version [en]" },
                        // 16
                        { $"col-{VwAuxDataList.Name}.{VwAuxDataList.ColGUIVersionDescriptionEn.Name}",
                            "Module version" },
                        { $"tip-{VwAuxDataList.Name}.{VwAuxDataList.ColGUIVersionDescriptionEn.Name}",
                            "Module version - description [en]" },
                        // 17
                        { $"col-{VwAuxDataList.Name}.{VwAuxDataList.ColIdentPoint.Name}",
                            "Inventory plot" },
                        { $"tip-{VwAuxDataList.Name}.{VwAuxDataList.ColIdentPoint.Name}",
                            "Inventory plot" },
                        // 18
                        { $"col-{VwAuxDataList.Name}.{VwAuxDataList.ColGid.Name}",
                            "GID" },
                        { $"tip-{VwAuxDataList.Name}.{VwAuxDataList.ColGid.Name}",
                            "GID" },
                        // 19
                        { $"col-{VwAuxDataList.Name}.{VwAuxDataList.ColAuxValue.Name}",
                            "Auxiliary variable value" },
                        { $"tip-{VwAuxDataList.Name}.{VwAuxDataList.ColAuxValue.Name}",
                            "Auxiliary variable value" },
                        // 20
                        { $"col-{VwAuxDataList.Name}.{VwAuxDataList.ColEstDate.Name}",
                            "Estimation date" },
                        { $"tip-{VwAuxDataList.Name}.{VwAuxDataList.ColEstDate.Name}",
                            "Estimation date" }
                    }
                    : languageFile.InternationalVersion.Data.TryGetValue(
                        key: nameof(FormCalcData),
                        out Dictionary<string, string> dictInternational)
                            ? dictInternational
                            : [],

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku</para>
        /// <para lang="en">Initialization of the control</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        private void Initialize(Control controlOwner)
        {
            ControlOwner = controlOwner;
            ViewAuxData = new VwAuxDataList(database: Database);

            SetButtonsVisibility();

            InitializeFilters();

            InitializeLabels();

            LoadContent();

            btnClose.Click += new EventHandler(
                (sender, e) =>
                {
                    DialogResult = DialogResult.OK;
                    Close();
                });

            btnReload.Click += new EventHandler(
                (sender, e) => { });

            btnCalculate.Click += new EventHandler(
                (sender, e) => { });

            btnStop.Click += new EventHandler(
                (sender, e) => { });

            btnDelete.Click += new EventHandler(
                (sender, e) => { });

            cboConfiguration.SelectedIndexChanged += new EventHandler(
                (sender, e) => { ViewAuxData.RowFilter = GetRowFilter(); });

            cboExtensionVersion.SelectedIndexChanged += new EventHandler(
                (sender, e) => { ViewAuxData.RowFilter = GetRowFilter(); });

            cboGUIVersion.SelectedIndexChanged += new EventHandler(
                (sender, e) => { ViewAuxData.RowFilter = GetRowFilter(); });

            dgvData.SelectionChanged += new EventHandler(
                (sender, e) => { dgvData.ClearSelection(); });
        }

        /// <summary>
        /// <para lang="cs">Nastavení viditelnosti tlačítek v ToolStrip</para>
        /// <para lang="en">Setting buttons visibility in ToolStrip</para>
        /// </summary>
        private void SetButtonsVisibility()
        {
#if DEBUG
            // ToolStrip
            // V režimu návrhu jsou dostupná všechna tlačítka
            btnReload.Visible = false;
            btnCalculate.Visible = true;
            btnStop.Visible = true;
            btnDelete.Visible = false;
#else
            // ToolStrip
            // V pracovním režimu nelze mazat vypočtené úhrny pomocných proměnných
            // (btnDelete.Visible = false)
            btnReload.Visible = false;
            btnCalculate.Visible = true;
            btnStop.Visible = true;
            btnDelete.Visible = false;
#endif
        }

        /// <summary>
        /// <para lang="cs">Inicializuje rozbovací seznamy fitrů</para>
        /// <para lang="en">Initializes comboboxes of fitters</para>
        /// </summary>
        private void InitializeFilters()
        {
            Dictionary<string, string> labels = Dictionary(
               languageVersion: LanguageVersion,
               languageFile: LanguageFile);

            ConfigList tConfig = new(database: Database);
            if (ConfigCollection != null)
            {
                tConfig.ReLoad(
                    condition: $"({ConfigList.ColConfigCollectionId.DbName} IN ({ConfigCollection.RefTotalId ?? 0}))",
                extended: true);
            }

            // cboConfiguration
            cboConfiguration.DataSource = null;
            cboConfiguration.DataSource = tConfig.Items;
            cboConfiguration.ValueMember = ID;
            cboConfiguration.DisplayMember =
                labels.TryGetValue(key: nameof(cboConfiguration),
                out string cboConfigurationDisplayMember)
                    ? cboConfigurationDisplayMember
                    : ID;
            cboConfiguration.SelectedItem =
                tConfig.Items
                .Where(a => a.Id == 0)
                .FirstOrDefault<Config>();

            // cboExtensionVersion
            cboExtensionVersion.DataSource = null;
            cboExtensionVersion.DataSource = Database.SAuxiliaryData.CExtensionVersion.Items;
            cboExtensionVersion.ValueMember = ID;
            cboExtensionVersion.DisplayMember =
                 labels.TryGetValue(key: nameof(cboExtensionVersion),
                 out string cboExtensionVersionDisplayMember)
                    ? cboExtensionVersionDisplayMember
                    : ID;
            cboExtensionVersion.SelectedItem =
                Database.SAuxiliaryData.CExtensionVersion.Items
                .Where(a => a.Id == (ExtensionVersion?.Id ?? 0))
                .FirstOrDefault<ZaJi.NfiEstaPg.AuxiliaryData.ExtensionVersion>();

            // cboGUIVersion
            cboGUIVersion.DataSource = null;
            cboGUIVersion.DataSource = Database.SAuxiliaryData.CGUIVersion.Items;
            cboGUIVersion.ValueMember = ID;
            cboGUIVersion.DisplayMember =
                labels.TryGetValue(key: nameof(cboGUIVersion),
                out string cboGUIVersionDisplayMember)
                    ? cboGUIVersionDisplayMember
                    : ID;
            cboGUIVersion.SelectedItem =
                Database.SAuxiliaryData.CGUIVersion.Items
                .Where(a => a.Id == (GUIVersion?.Id ?? 0))
                .FirstOrDefault<ZaJi.NfiEstaPg.AuxiliaryData.GUIVersion>();
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků ovládacího prvku</para>
        /// <para lang="en">Initialization of control labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
                 languageVersion: LanguageVersion,
                 languageFile: LanguageFile);

            Dictionary<string, string> labelsCs = Dictionary(
                languageVersion: LanguageVersion.National,
                languageFile: LanguageFile);

            Dictionary<string, string> labelsEn = Dictionary(
                languageVersion: LanguageVersion.International,
                languageFile: LanguageFile);

            #region Controls

            Text =
                labels.TryGetValue(key: nameof(FormCalcData),
                out string frmCalcDataText)
                    ? frmCalcDataText
                    : String.Empty;

            btnCalculate.Text =
              labels.TryGetValue(key: nameof(btnCalculate),
              out string btnCalculateText)
                  ? btnCalculateText
                  : String.Empty;

            btnClose.Text =
                labels.TryGetValue(key: nameof(btnClose),
                out string btnCloseText)
                    ? btnCloseText
                    : String.Empty;

            btnDelete.Text =
                labels.TryGetValue(key: nameof(btnDelete),
                out string btnDeleteText)
                    ? btnDeleteText
                    : String.Empty;

            btnReload.Text =
                labels.TryGetValue(key: nameof(btnReload),
                out string btnReloadText)
                    ? btnReloadText
                    : String.Empty;

            btnStop.Text =
                labels.TryGetValue(key: nameof(btnStop),
                out string btnStopText)
                    ? btnStopText
                    : String.Empty;

            cboConfiguration.DisplayMember =
                labels.TryGetValue(key: nameof(cboConfiguration),
                out string cboConfigurationDisplayMember)
                    ? cboConfigurationDisplayMember
                    : ID;

            cboExtensionVersion.DisplayMember =
                labels.TryGetValue(key: nameof(cboExtensionVersion),
                out string cboExtensionVersionDisplayMember)
                    ? cboExtensionVersionDisplayMember
                    : ID;

            cboGUIVersion.DisplayMember =
                labels.TryGetValue(key: nameof(cboGUIVersion),
                out string cboGUIVersionDisplayMember)
                    ? cboGUIVersionDisplayMember
                    : ID;

            grpAuxiliaryVariableValues.Text =
                labels.TryGetValue(key: nameof(grpAuxiliaryVariableValues),
                out string grpAuxiliaryVariableValuesText)
                    ? grpAuxiliaryVariableValuesText
                    : String.Empty;

            grpConfigCollection.Text =
                labels.TryGetValue(key: nameof(grpConfigCollection),
                out string grpConfigCollectionText)
                    ? grpConfigCollectionText
                    : String.Empty;

            lblConfigurationCaption.Text =
                labels.TryGetValue(key: nameof(lblConfigurationCaption),
                out string lblConfigurationCaptionText)
                    ? lblConfigurationCaptionText
                    : String.Empty;

            lblExtensionVersionCaption.Text =
                labels.TryGetValue(key: nameof(lblExtensionVersionCaption),
                out string lblExtensionVersionCaptionText)
                    ? lblExtensionVersionCaptionText
                    : String.Empty;

            lblGUIVersionCaption.Text =
                labels.TryGetValue(key: nameof(lblGUIVersionCaption),
                out string lblGUIVersionCaptionText)
                    ? lblGUIVersionCaptionText
                    : String.Empty;

            lblConfigCollection.Text =
                (LanguageVersion == LanguageVersion.International)
                    ? ConfigCollection?.ExtendedLabelEn ?? String.Empty
                    : (LanguageVersion == LanguageVersion.National)
                        ? ConfigCollection?.ExtendedLabelCs ?? String.Empty
                        : ConfigCollection?.ExtendedLabelEn ?? String.Empty;

            #endregion Controls

            #region Columns

            switch (LanguageVersion)
            {
                case LanguageVersion.National:
                    VwAuxDataList.SetColumnsVisibility(
                        VwAuxDataList.ColConfigLabelCs.Name,
                        VwAuxDataList.ColIdentPoint.Name,
                        VwAuxDataList.ColGid.Name,
                        VwAuxDataList.ColAuxValue.Name,
                        VwAuxDataList.ColExtensionVersionLabelCs.Name,
                        VwAuxDataList.ColGUIVersionLabelCs.Name,
                        VwAuxDataList.ColEstDate.Name
                        );
                    break;
                case LanguageVersion.International:
                    VwAuxDataList.SetColumnsVisibility(
                        VwAuxDataList.ColConfigLabelEn.Name,
                        VwAuxDataList.ColIdentPoint.Name,
                        VwAuxDataList.ColGid.Name,
                        VwAuxDataList.ColAuxValue.Name,
                        VwAuxDataList.ColExtensionVersionLabelEn.Name,
                        VwAuxDataList.ColGUIVersionLabelEn.Name,
                        VwAuxDataList.ColEstDate.Name
                        );
                    break;
                default:
                    throw new ArgumentException(
                        message: $"Argument {nameof(LanguageVersion)} unknown value.",
                        paramName: nameof(LanguageVersion));
            }

            foreach (ColumnMetadata column in VwAuxDataList.Cols.Values)
            {
                VwAuxDataList.SetColumnHeader(
                    columnName:
                        column.Name,
                    headerTextCs:
                        labelsCs.ContainsKey(key: $"col-{VwAuxDataList.Name}.{column.Name}")
                            ? labelsCs[$"col-{VwAuxDataList.Name}.{column.Name}"]
                            : String.Empty,
                    headerTextEn:
                        labelsEn.ContainsKey(key: $"col-{VwAuxDataList.Name}.{column.Name}")
                            ? labelsEn[$"col-{VwAuxDataList.Name}.{column.Name}"]
                            : String.Empty,
                    toolTipTextCs:
                        labelsCs.ContainsKey(key: $"tip-{VwAuxDataList.Name}.{column.Name}")
                            ? labelsCs[$"tip-{VwAuxDataList.Name}.{column.Name}"]
                            : String.Empty,
                    toolTipTextEn:
                        labelsEn.ContainsKey(key: $"tip-{VwAuxDataList.Name}.{column.Name}")
                            ? labelsEn[$"tip-{VwAuxDataList.Name}.{column.Name}"]
                            : String.Empty);
            }

            #endregion Columns
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent()
        {
            AuxDataList tAuxData = new(database: Database);
            ViewAuxData = new VwAuxDataList(database: Database);

            if (ConfigCollection != null)
            {
                tAuxData.ReLoad(
                    condition: String.Concat(
                        $"({AuxDataList.ColConfigCollectionId.DbName} = {ConfigCollection.Id})"));
            }

            ViewAuxData.ReLoad(
                   tAuxData: tAuxData,
                   tConfig: Database.SAuxiliaryData.TConfig,
                   cExtVersion: Database.SAuxiliaryData.CExtensionVersion,
                   cGuiVersion: Database.SAuxiliaryData.CGUIVersion);

            // dgvData
            ViewAuxData.SetDataGridViewDataSource(dataGridView: dgvData);
            ViewAuxData.FormatDataGridView(dataGridView: dgvData);
            dgvData.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

            ViewAuxData.RowFilter = GetRowFilter();
        }

        /// <summary>
        /// <para lang="cs">Text výběrové podmínky zobrazených řádků z tabulky hodnot pomocných proměnných</para>
        /// <para lang="en">Text of the selection condition of the displayed rows from the table of auxiliary variable values</para>
        /// </summary>
        /// <returns>
        /// <para lang="cs">Text výběrové podmínky zobrazených řádků z tabulky hodnot pomocných proměnných</para>
        /// <para lang="en">Text of the selection condition of the displayed rows from the table of auxiliary variable values</para>
        /// </returns>
        private string GetRowFilter()
        {
            string text = null;

            if (cboConfiguration.SelectedItem != null)
            {
                Config selectedConfigurationFilter =
                    (Config)cboConfiguration.SelectedItem;

                if (selectedConfigurationFilter.Id != 0)
                {
                    text = String.IsNullOrEmpty(value: text)
                        ? $"({VwAuxDataList.ColConfigId.Name} IN ({selectedConfigurationFilter.Id}))"
                        : $"{text} AND ({VwAuxDataList.ColConfigId.Name} IN ({selectedConfigurationFilter.Id}))";
                }
            }

            if (cboExtensionVersion.SelectedItem != null)
            {
                ZaJi.NfiEstaPg.AuxiliaryData.ExtensionVersion selectedExtensionVersionFilter =
                    (ZaJi.NfiEstaPg.AuxiliaryData.ExtensionVersion)cboExtensionVersion.SelectedItem;

                if (selectedExtensionVersionFilter.Id != 0)
                {
                    text = String.IsNullOrEmpty(value: text)
                        ? $"({VwAuxDataList.ColExtensionVersionId.Name} IN ({selectedExtensionVersionFilter.Id}))"
                        : $"{text} AND ({VwAuxDataList.ColExtensionVersionId.Name} IN ({selectedExtensionVersionFilter.Id}))";
                }
            }

            if (cboGUIVersion.SelectedItem != null)
            {
                ZaJi.NfiEstaPg.AuxiliaryData.GUIVersion selectedGUIVersionFilter =
                    (ZaJi.NfiEstaPg.AuxiliaryData.GUIVersion)cboGUIVersion.SelectedItem;

                if (selectedGUIVersionFilter.Id != 0)
                {
                    text = String.IsNullOrEmpty(value: text)
                        ? $"({VwAuxDataList.ColGUIVersionId.Name} IN ({selectedGUIVersionFilter.Id}))"
                        : $"{text} AND ({VwAuxDataList.ColGUIVersionId.Name} IN ({selectedGUIVersionFilter.Id}))";
                }
            }

            return
                !String.IsNullOrEmpty(value: text)
                    ? text
                    : null;
        }

        #endregion Methods

    }
}