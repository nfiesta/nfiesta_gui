﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Data;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.Catalog;

namespace ZaJi
{
    namespace ModuleAuxiliaryData
    {

        /// <summary>
        /// <para lang="cs">Databázový sloupec s přiřazeným cizím klíčem na číselník</para>
        /// <para lang="en">Database column with assigned foreign key to lookup table</para>
        /// </summary>
        internal class WideColumn
        {

            #region Constants

            /// <summary>
            /// <para lang="cs">Název sloupce s identifikátorem</para>
            /// <para lang="en">Identifier column name</para>
            /// </summary>
            public const string ColIdName = "id";

            /// <summary>
            /// <para lang="cs">Název datového typu sloupce s identifikátorem</para>
            /// <para lang="en">Identifier column data type name</para>
            /// </summary>
            public const string ColIdTypeName = "System.Int32";

            /// <summary>
            /// <para lang="cs">Název sloupce s popiskem</para>
            /// <para lang="en">Label column name</para>
            /// </summary>
            public const string ColLabelName = "label";

            /// <summary>
            /// <para lang="cs">Název datového typu sloupce s popiskem</para>
            /// <para lang="en">Label column data type name</para>
            /// </summary>
            public const string ColLabelTypeName = "System.String";

            #endregion Constants


            #region Private Fields

            /// <summary>
            /// <para lang="cs">Vlastník objektu</para>
            /// <para lang="en">Object owner</para>
            /// </summary>
            private Control owner;

            /// <summary>
            /// <para lang="cs">Databázový sloupec s přiřazeným cizím klíčem</para>
            /// <para lang="en">Database column with assigned foreign key</para>
            /// </summary>
            private DBColumn column;

            /// <summary>
            /// <para lang="cs">Cizí klíč přiřazený databázovému sloupci</para>
            /// <para lang="en">Foreign key assigned to a database column</para>
            /// </summary>
            private DBForeignKey foreignKey;

            /// <summary>
            /// <para lang="cs">Číselník definující doménu databázového sloupce</para>
            /// <para lang="en">Lookup table defining the database column domain</para>
            /// </summary>
            private DataTable lookupTable;

            #endregion Private Fields


            #region Constructor

            /// <summary>
            /// <para lang="cs">Konstruktor</para>
            /// <para lang="en">Constructor</para>
            /// </summary>
            /// <param name="owner">
            /// <para lang="cs">Vlastník objektu</para>
            /// <para lang="en">Object owner</para>
            /// </param>
            /// <param name="column">
            /// <para lang="cs">Databázový sloupec s přiřazeným cizím klíčem</para>
            /// <para lang="en">Database column with assigned foreign key</para>
            /// </param>
            /// <param name="foreignKey">
            /// <para lang="cs">Cizí klíč přiřazený databázovému sloupci</para>
            /// <para lang="en">Foreign key assigned to a database column</para>
            /// </param>
            public WideColumn(Control owner, DBColumn column, DBForeignKey foreignKey = null)
            {
                Owner = owner;
                Column = column;
                ForeignKey = foreignKey;
                LookupTable = null;
            }

            #endregion Constructors


            #region Common Properties

            /// <summary>
            /// <para lang="cs">Vlastník objektu</para>
            /// <para lang="en">Object owner</para>
            /// </summary>
            public Control Owner
            {
                get
                {
                    IAuxiliaryDataControl.CheckOwner(owner: owner, name: nameof(Owner));

                    return owner;
                }
                set
                {
                    IAuxiliaryDataControl.CheckOwner(owner: value, name: nameof(Owner));

                    owner = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Databázové tabulky (read-only)</para>
            /// <para lang="en">Database tables (read-only)</para>
            /// </summary>
            public NfiEstaDB Database
            {
                get
                {
                    return ((INfiEstaControl)Owner).Database;
                }
            }

            /// <summary>
            /// <para lang="cs">Jazyková verze (read-only)</para>
            /// <para lang="en">Language version (read-only)</para>
            /// </summary>
            public LanguageVersion LanguageVersion
            {
                get
                {
                    return ((INfiEstaControl)Owner).LanguageVersion;
                }
            }

            /// <summary>
            /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
            /// <para lang="en">File with control labels (read-only)</para>
            /// </summary>
            public LanguageFile LanguageFile
            {
                get
                {
                    return ((IAuxiliaryDataControl)Owner).LanguageFile;
                }
            }

            /// <summary>
            /// <para lang="cs">Nastavení modulu pro konfiguraci a výpočet odhadů</para>
            /// <para lang="en">Module for configuration and calculation estimates setting</para>
            /// </summary>
            public Setting Setting
            {
                get
                {
                    return ((IAuxiliaryDataControl)Owner).Setting;
                }
            }

            #endregion Common Properties


            #region Properties

            /// <summary>
            /// <para lang="cs">Databázový sloupec s přiřazeným cizím klíčem</para>
            /// <para lang="en">Database column with assigned foreign key</para>
            /// </summary>
            public DBColumn Column
            {
                get
                {
                    return column ??
                        throw new ArgumentNullException(
                            message: $"Argument {nameof(Column)} must not be null.",
                            paramName: nameof(Column));
                }
                private set
                {
                    column = value ??
                        throw new ArgumentNullException(
                            message: $"Argument {nameof(Column)} must not be null.",
                            paramName: nameof(Column));
                }
            }

            /// <summary>
            /// <para lang="cs">Cizí klíč přiřazený databázovému sloupci</para>
            /// <para lang="en">Foreign key assigned to a database column</para>
            /// </summary>
            public DBForeignKey ForeignKey
            {
                get
                {
                    return foreignKey;
                }
                private set
                {
                    foreignKey = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Číselník definující doménu databázového sloupce</para>
            /// <para lang="en">Lookup table defining the database column domain</para>
            /// </summary>
            public DataTable LookupTable
            {
                get
                {
                    return lookupTable ??
                        InitializeEmptyLookupTable();
                }
                private set
                {
                    lookupTable = value ??
                        InitializeEmptyLookupTable();
                }
            }

            #endregion Properties


            #region Static Methods

            /// <summary>
            /// <para lang="cs">Vytvoří prázdný číselník</para>
            /// <para lang="en">Creates empty lookup table</para>
            /// </summary>
            /// <returns>
            /// <para lang="cs">Prázdný číselník</para>
            /// <para lang="en">Empty lookup table</para>
            /// </returns>
            private static DataTable InitializeEmptyLookupTable()
            {
                DataTable result = new();

                result.Columns.Add(
                    column: new DataColumn()
                    {
                        ColumnName = ColIdName,
                        DataType = Type.GetType(typeName: ColIdTypeName)
                    });

                result.Columns.Add(
                    column: new DataColumn()
                    {
                        ColumnName = ColLabelName,
                        DataType = Type.GetType(typeName: ColLabelTypeName)
                    });

                return result;
            }

            #endregion Static Methods


            #region Methods

            /// <summary>
            /// <para lang="cs">Nahraje data číselníku z databáze</para>
            /// <para lang="en">Loads lookup table data from database</para>
            /// </summary>
            public void LoadLookupTable()
            {
                string command;

                if (ForeignKey != null)
                {
                    // Jestliže tabulka obsahuje cizí klíč na číselník
                    // pak se kategorie nahrají z číselníku

                    command =
                        String.Concat(
                            $"SELECT {ColIdName}, {ColLabelName} ",
                            $"FROM {ForeignKey.ForeignTable.Schema.Name}.{ForeignKey.ForeignTable.Name} ",
                            $"ORDER BY {ColIdName}; ");
                }

                else
                {
                    // Jestliže tabulka neobsahuje cizí klíč na číselník
                    // pak se kategorie vyberou přímo z hodnot ve sloupci dané tabulky
                    // https://wiki.postgresql.org/wiki/Loose_indexscan

                    command =
                        String.Concat(
                            $"WITH RECURSIVE t AS{Environment.NewLine}",
                            $"({Environment.NewLine}",
                            $"(SELECT {Column.Name} FROM {Column.Table.Schema.Name}.{Column.Table.Name} ORDER BY {Column.Name} LIMIT 1){Environment.NewLine}",
                            $"UNION ALL SELECT{Environment.NewLine}",
                            $"(SELECT {Column.Name} FROM {Column.Table.Schema.Name}.{Column.Table.Name} WHERE({Column.Name} > t.{Column.Name}) ORDER BY {Column.Name} LIMIT 1){Environment.NewLine}",
                            $"FROM t WHERE(t.{Column.Name} IS NOT NULL){Environment.NewLine}",
                            $"){Environment.NewLine}",
                            $"SELECT {Column.Name} AS {ColIdName}, {Column.Name} AS {ColLabelName}{Environment.NewLine}",
                            $"FROM t{Environment.NewLine}",
                            $"WHERE(t.{Column.Name} IS NOT NULL);{Environment.NewLine}"
                            );
                }

                LookupTable = Database.Postgres.ExecuteQuery(
                        sqlCommand: command);
            }

            /// <summary>
            /// <para lang="cs">Vrací řetězec, který představuje aktuální objekt</para>
            /// <para lang="en">Returns the string that represents current object</para>
            /// </summary>
            /// <returns>
            /// <para lang="cs">Vrací řetězec, který představuje aktuální objekt</para>
            /// <para lang="en">Returns the string that represents current object</para>
            /// </returns>
            public override string ToString()
            {
                if (ForeignKey != null)
                {
                    return LanguageVersion switch
                    {
                        LanguageVersion.National =>
                            String.Concat(
                                $"Databázový sloupec = {Functions.PrepStringArg(arg: Column.Name, replaceEmptyStringByNull: true)} ",
                                $"s cizím klíčem {Functions.PrepStringArg(arg: ForeignKey.Name, replaceEmptyStringByNull: true)} ",
                                $"na číselník {Functions.PrepStringArg(arg: ForeignKey.ForeignTable.Schema.Name, replaceEmptyStringByNull: true)}.",
                                $"{Functions.PrepStringArg(arg: ForeignKey.ForeignTable.Name, replaceEmptyStringByNull: true)} "),

                        LanguageVersion.International =>
                           String.Concat(
                                $"Database column = {Functions.PrepStringArg(arg: Column.Name, replaceEmptyStringByNull: true)} ",
                                $"with foreign key {Functions.PrepStringArg(arg: ForeignKey.Name, replaceEmptyStringByNull: true)} ",
                                $"to lookup table {Functions.PrepStringArg(arg: ForeignKey.ForeignTable.Schema.Name, replaceEmptyStringByNull: true)}.",
                                $"{Functions.PrepStringArg(arg: ForeignKey.ForeignTable.Name, replaceEmptyStringByNull: true)} "),

                        _ =>
                            String.Concat(
                                $"Database column = {Functions.PrepStringArg(arg: Column.Name, replaceEmptyStringByNull: true)} ",
                                $"with foreign key {Functions.PrepStringArg(arg: ForeignKey.Name, replaceEmptyStringByNull: true)} ",
                                $"to lookup table {Functions.PrepStringArg(arg: ForeignKey.ForeignTable.Schema.Name, replaceEmptyStringByNull: true)}.",
                                $"{Functions.PrepStringArg(arg: ForeignKey.ForeignTable.Name, replaceEmptyStringByNull: true)} "),
                    };
                }
                else
                {
                    return LanguageVersion switch
                    {
                        LanguageVersion.National =>
                            String.Concat(
                                $"Databázový sloupec = {Functions.PrepStringArg(arg: Column.Name, replaceEmptyStringByNull: true)} ",
                                $"bez cizího klíče "),

                        LanguageVersion.International =>
                            String.Concat(
                                $"Database column = {Functions.PrepStringArg(arg: Column.Name, replaceEmptyStringByNull: true)} ",
                                $"without foreign key. "),

                        _ =>
                            String.Concat(
                                $"Database column = {Functions.PrepStringArg(arg: Column.Name, replaceEmptyStringByNull: true)} ",
                                $"without foreign key. "),
                    };
                }
            }

            #endregion Methods

        }

    }
}
