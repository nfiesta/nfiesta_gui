﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg;
using ZaJi.NfiEstaPg.Core;
using ZaJi.PostgreSQL;

namespace ZaJi.ModuleAuxiliaryData
{

    /// <summary>
    /// <para lang="cs">Formulář "Výběr pomocné proměnné"</para>
    /// <para lang="en">Form "Auxiliary variable selection"</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    internal partial class FormAuxiliaryVariableSelector
        : Form, INfiEstaControl, IAuxiliaryDataControl
    {

        #region Constants

        private const int ImageRedBall = 0;
        private const int ImageBlueBall = 1;
        private const int ImageYellowBall = 2;
        private const int ImageGreenBall = 3;
        private const int ImageGrayBall = 4;
        private const int ImageLock = 5;
        private const int ImageSchema = 6;
        private const int ImageVector = 7;
        private const int ImageRaster = 8;

        #endregion Constants


        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Uzly v TreeView</para>
        /// <para lang="en">TreeView nodes</para>
        /// </summary>
        private List<TreeNode> nodes;

        /// <summary>
        /// <para lang="cs">Indikátor editace ovládacího prvku</para>
        /// <para lang="en">Control editing indicator</para>
        /// </summary>
        private bool onEdit;

        #endregion Private Fields


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor ovládacího prvku</para>
        /// <para lang="en">Control constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        public FormAuxiliaryVariableSelector(Control controlOwner)
        {
            InitializeComponent();
            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                IAuxiliaryDataControl.CheckOwner(owner: controlOwner, name: nameof(ControlOwner));

                return controlOwner;
            }
            set
            {
                IAuxiliaryDataControl.CheckOwner(owner: value, name: nameof(ControlOwner));

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables (read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Soubor s popisky ovládacích prvků (read-only)</para>
        /// <para lang="en">File with control labels (read-only)</para>
        /// </summary>
        public LanguageFile LanguageFile
        {
            get
            {
                return ((IAuxiliaryDataControl)ControlOwner).LanguageFile;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastavení modulu pro pomocná data (read-only)</para>
        /// <para lang="en">Module for auxiliary data setting (read-only)</para>
        /// </summary>
        public Setting Setting
        {
            get
            {
                return ((IAuxiliaryDataControl)ControlOwner).Setting;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">Vybraná pomocná proměnná</para>
        /// <para lang="en">Selected auxiliary variable</para>
        /// </summary>
        public AuxiliaryVariable AuxiliaryVariable
        {
            get
            {
                return
                    CheckedAuxiliaryVariables.FirstOrDefault<AuxiliaryVariable>();
            }
            set
            {
                CheckOnlyOneNode(
                    id: value?.Id ?? 0);
            }
        }

        /// <summary>
        /// <para lang="cs">Indikátor editace ovládacího prvku</para>
        /// <para lang="en">Control editing indicator</para>
        /// </summary>
        private bool OnEdit
        {
            get
            {
                return onEdit;
            }
            set
            {
                onEdit = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Uzly v TreeView</para>
        /// <para lang="en">TreeView nodes</para>
        /// </summary>
        private List<TreeNode> Nodes
        {
            get
            {
                return nodes ?? [];
            }
            set
            {
                nodes = value ?? [];
            }
        }

        /// <summary>
        /// <para lang="cs">Uzly v TreeView úroveň pomocných proměnných (read-only)</para>
        /// <para lang="en">TreeView nodes - Auxiliary variable level (read-only)</para>
        /// </summary>
        private IEnumerable<TreeNode> AuxiliaryVariableNodes
        {
            get
            {
                return
                    Nodes
                        .Where(a => a.Tag != null)
                        .Where(a => a.Tag is AuxiliaryVariable);
            }
        }

        /// <summary>
        /// <para lang="cs">Zaškrtnuté uzly pomocných proměnných (read-only)</para>
        /// <para lang="en">Checked auxiliary varible nodes (read-only)</para>
        /// </summary>
        private IEnumerable<TreeNode> CheckedAuxiliaryVariableNodes
        {
            get
            {
                return
                    AuxiliaryVariableNodes
                        .Where(a => a.Checked);
            }
        }

        /// <summary>
        /// <para lang="cs">Zaškrtnuté pomocné proměnné (read-only)</para>
        /// <para lang="en">Checked auxiliary variables (read-only)</para>
        /// </summary>
        private IEnumerable<AuxiliaryVariable> CheckedAuxiliaryVariables
        {
            get
            {
                return CheckedAuxiliaryVariableNodes
                    .Select(a => (AuxiliaryVariable)a.Tag);
            }
        }

        #endregion Properties


        #region Static Methods

        /// <summary>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts</para>
        /// </summary>
        /// <param name="languageVersion">
        /// <para lang="cs">Zvolený jazyk</para>
        /// <para lang="en">Selected language</para>
        /// </param>
        /// <param name="languageFile">
        /// <para lang="cs">Soubory s popisky ovládacích prvků pro národní a mezinárodní verzi</para>
        /// <para lang="en">File with control labels for national and international version</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">Slovník s texty hlášení</para>
        /// <para lang="en">Dictionary with message texts in selected language</para>
        /// </returns>
        public static Dictionary<string, string> Dictionary(
            LanguageVersion languageVersion,
            LanguageFile languageFile)
        {
            return languageVersion switch
            {
                LanguageVersion.National => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(FormAuxiliaryVariableSelector),    "Výběr pomocné proměnné" },
                        { nameof(btnOK),                            "OK" },
                        { nameof(btnCancel),                        "Zrušit" },
                        { nameof(grpItems),                         "Seznam pomocných proměnných:" },
                        { nameof(grpSelectedItem),                  "Vybraná pomocná proměnná:" }
                    }
                    : languageFile.NationalVersion.Data.TryGetValue(
                        key: nameof(FormAuxiliaryVariableSelector),
                        out Dictionary<string, string> dictNational)
                            ? dictNational
                            : [],

                LanguageVersion.International => (languageFile == null)
                    ? new Dictionary<string, string>()
                    {
                        { nameof(FormAuxiliaryVariableSelector),    "Auxiliary variable selection" },
                        { nameof(btnOK),                            "OK" },
                        { nameof(btnCancel),                        "Cancel" },
                        { nameof(grpItems),                         "List of auxiliary variables:" },
                        { nameof(grpSelectedItem),                  "Selected auxiliary variable:"}
                    }
                    : languageFile.InternationalVersion.Data.TryGetValue(
                        key: nameof(FormAuxiliaryVariableSelector),
                        out Dictionary<string, string> dictInternational)
                            ? dictInternational
                            : [],

                _ => [],
            };
        }

        #endregion Static Methods


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku</para>
        /// <para lang="en">Initialization of the control</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        private void Initialize(Control controlOwner)
        {
            ControlOwner = controlOwner;
            Nodes = [];
            OnEdit = false;

            LoadContent();

            btnOK.Click += new EventHandler(
                (sender, e) =>
                {
                    DialogResult = DialogResult.OK;
                    Close();
                });

            btnCancel.Click += new EventHandler(
               (sender, e) =>
               {
                   DialogResult = DialogResult.Cancel;
                   Close();
               });

            tvwItems.AfterCheck += new TreeViewEventHandler(
                (sender, e) =>
                {
                    if (OnEdit)
                    {
                        return;
                    }

                    if ((e?.Node?.Tag != null) && (e.Node.Tag is AuxiliaryVariable lastCheckedAuxiliaryVariable))
                    {
                        CheckOnlyOneNode(
                            id: lastCheckedAuxiliaryVariable.Id);
                    }

                    SetCaption();
                });

            tvwItems.AfterSelect += new TreeViewEventHandler(
                (sender, e) => tvwItems.SelectedNode = null);
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků ovládacího prvku</para>
        /// <para lang="en">Initialization of control labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            Dictionary<string, string> labels = Dictionary(
                languageVersion: LanguageVersion,
                languageFile: LanguageFile);

            Text =
                labels.TryGetValue(key: nameof(FormAuxiliaryVariableSelector),
                out string frmAuxiliaryVariableSelectorText)
                    ? frmAuxiliaryVariableSelectorText
                    : String.Empty;

            grpSelectedItem.Text =
                labels.TryGetValue(key: nameof(grpSelectedItem),
                out string grpSelectedItemText)
                    ? grpSelectedItemText
                    : String.Empty;

            grpItems.Text =
                labels.TryGetValue(key: nameof(grpItems),
                out string grpItemsText)
                    ? grpItemsText
                    : String.Empty;

            btnOK.Text =
                labels.TryGetValue(key: nameof(btnOK),
                out string btnOKText)
                    ? btnOKText
                    : String.Empty;

            btnCancel.Text =
                labels.TryGetValue(key: nameof(btnCancel),
                out string btnCancelText)
                    ? btnCancelText
                    : String.Empty;

            SetCaption();

            foreach (TreeNode node in AuxiliaryVariableNodes)
            {
                node.Text =
                    (LanguageVersion == LanguageVersion.International)
                    ? ((AuxiliaryVariable)node.Tag).LabelEn
                    : (LanguageVersion == LanguageVersion.National)
                        ? ((AuxiliaryVariable)node.Tag).LabelCs
                        : ((AuxiliaryVariable)node.Tag).LabelEn;
            }
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent()
        {
            InitializeTreeView();

            InitializeLabels();

            SetCaption();
        }

        /// <summary>
        /// <para lang="cs">Zobrazení pomocných proměnných v uzlech TreeView</para>
        /// <para lang="en">Displaying auxiliary variables in TreeView nodes</para>
        /// </summary>
        private void InitializeTreeView()
        {
            // Zabrání prokreslování TreeView během editace
            tvwItems.BeginUpdate();

            // Smazání všech uzlů z TreeView
            Nodes.Clear();
            tvwItems.Nodes.Clear();

            // (Seznam pomocných proměnných)
            foreach (AuxiliaryVariable auxiliaryVariable
                in Database.SNfiEsta.CAuxiliaryVariable.Items)
            {
                TreeNode node = new()
                {
                    ImageIndex = ImageGreenBall,
                    Name = auxiliaryVariable.Id.ToString(),
                    SelectedImageIndex = ImageGreenBall,
                    Tag = auxiliaryVariable,
                    Text =
                        (LanguageVersion == LanguageVersion.International)
                            ? auxiliaryVariable.LabelEn
                            : (LanguageVersion == LanguageVersion.National)
                                ? auxiliaryVariable.LabelCs
                                : auxiliaryVariable.LabelEn
                };
                Nodes.Add(item: node);
                tvwItems.Nodes.Add(node: node);
            }

            tvwItems.EndUpdate();
        }

        /// <summary>
        /// <para lang="cs">
        /// Vrátí uzel z TreeView odpovídající identifikačnímu číslu pomocné proměnné
        /// </para>
        /// <para lang="en">
        /// Returns the TreeView node corresponding to the auxiliary variable identifier
        /// </para>
        /// </summary>
        /// <param name="id">
        /// <para lang="cs">Identifikační číslo pomocné proměnné</para>
        /// <para lang="en">Auxiliary variable identifier</para>
        /// </param>
        /// <returns>
        /// <para lang="cs">
        /// Vrátí uzel z TreeView odpovídající identifikačnímu číslu pomocné proměnné
        /// </para>
        /// <para lang="en">
        /// Returns the TreeView node corresponding to the auxiliary variable identifier
        /// </para>
        /// </returns>
        private TreeNode FindNode(int id)
        {
            return AuxiliaryVariableNodes
                .Where(a => ((AuxiliaryVariable)a.Tag).Id == id)
                .FirstOrDefault<TreeNode>();
        }

        /// <summary>
        /// <para lang="cs">Zaškrtne pouze jeden uzel v TreeView</para>
        /// <para lang="en">Check only one node in TreeView</para>
        /// </summary>
        /// <param name="id">
        /// <para lang="cs">Identifikační číslo pomocné proměnné</para>
        /// <para lang="en">Auxiliary variable identifier</para>
        /// </param>
        private void CheckOnlyOneNode(int id)
        {
            OnEdit = true;

            foreach (TreeNode node in AuxiliaryVariableNodes)
            {
                node.Checked = false;
            }

            TreeNode checkedNode = FindNode(id: id);
            if (checkedNode != null)
            {
                checkedNode.Checked = true;
            }

            OnEdit = false;

            SetCaption();
        }

        /// <summary>
        /// <para lang="cs">Nastaví nadpis se seznamem vybraných pomocných proměnných</para>
        /// <para lang="en">Sets the title with a list of selected auxiliary variables</para>
        /// </summary>
        private void SetCaption()
        {
            string strSelectedItemsEn =
                CheckedAuxiliaryVariables.Any()
                    ? CheckedAuxiliaryVariables
                        .Select(a => a.ExtendedLabelEn)
                        .Aggregate((a, b) => $"{a}, {b}")
                    : String.Empty;

            string strSelectedItemsCs =
                CheckedAuxiliaryVariables.Any()
                   ? CheckedAuxiliaryVariables
                    .Select(a => a.ExtendedLabelCs)
                    .Aggregate((a, b) => $"{a}, {b}")
                    : String.Empty;

            lblSelectedItem.Text =
                (LanguageVersion == LanguageVersion.International)
                    ? strSelectedItemsEn
                    : (LanguageVersion == LanguageVersion.National)
                        ? strSelectedItemsCs
                        : strSelectedItemsEn;
        }

        #endregion Methods

    }

}