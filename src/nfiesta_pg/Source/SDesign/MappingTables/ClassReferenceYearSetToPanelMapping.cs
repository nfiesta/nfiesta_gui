﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace SDesign
        {
            // cm_refyearset2panel_mapping

            /// <summary>
            /// Mapping between panel and reference year set
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class ReferenceYearSetToPanelMapping(
                ReferenceYearSetToPanelMappingList composite = null,
                DataRow data = null)
                    : AMappingTableEntry<ReferenceYearSetToPanelMappingList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Mapping between panel and reference year set identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: ReferenceYearSetToPanelMappingList.ColId.Name,
                            defaultValue: Int32.Parse(s: ReferenceYearSetToPanelMappingList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: ReferenceYearSetToPanelMappingList.ColId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Sampling panel identifier
                /// </summary>
                public int PanelId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: ReferenceYearSetToPanelMappingList.ColPanelId.Name,
                            defaultValue: Int32.Parse(s: ReferenceYearSetToPanelMappingList.ColPanelId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: ReferenceYearSetToPanelMappingList.ColPanelId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Sampling panel object (read-only)
                /// </summary>
                public Panel Panel
                {
                    get
                    {
                        return
                             ((NfiEstaDB)Composite.Database).SDesign.TPanel[PanelId];
                    }
                }


                /// <summary>
                /// Reference year or season set identifier
                /// </summary>
                public int ReferenceYearSetId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: ReferenceYearSetToPanelMappingList.ColReferenceYearSetId.Name,
                            defaultValue: Int32.Parse(s: ReferenceYearSetToPanelMappingList.ColReferenceYearSetId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: ReferenceYearSetToPanelMappingList.ColReferenceYearSetId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Reference year or season set identifier (read-only)
                /// </summary>
                public ReferenceYearSet ReferenceYearSet
                {
                    get
                    {
                        return
                             ((NfiEstaDB)Composite.Database).SDesign.TReferenceYearSet[ReferenceYearSetId];
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true|false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not ReferenceYearSetToPanelMapping Type, return False
                    if (obj is not ReferenceYearSetToPanelMapping)
                    {
                        return false;
                    }

                    return ((ReferenceYearSetToPanelMapping)obj).Id == Id;
                }

                /// <summary>
                /// Returns a hash code for the current object
                /// </summary>
                /// <returns>Hash code for the current object</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                /// <summary>
                /// Returns a string that represents the current object
                /// </summary>
                /// <returns>String that represents the current object</returns>
                public override string ToString()
                {
                    return String.Concat(
                        $"{nameof(ReferenceYearSetToPanelMapping)}: {{",
                        $"{nameof(Id)}: {Functions.PrepIntArg(arg: Id)}; ",
                        $"{nameof(PanelId)}: {Functions.PrepIntArg(arg: PanelId)}; ",
                        $"{nameof(ReferenceYearSetId)}: {Functions.PrepIntArg(arg: ReferenceYearSetId)}}}");
                }

                #endregion Methods

            }


            /// <summary>
            /// List of mappings between panel and reference year set
            /// </summary>
            public class ReferenceYearSetToPanelMappingList
                : AMappingTable
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema = SDesignSchema.Name;

                /// <summary>
                /// Table name
                /// </summary>
                public const string Name = "cm_refyearset2panel_mapping";

                /// <summary>
                /// Table alias
                /// </summary>
                public const string Alias = "cm_refyearset2panel_mapping";

                /// <summary>
                /// Description in national language
                /// </summary>
                public const string CaptionCs = "Seznam mapování roků a sezón měření do panelů";

                /// <summary>
                /// Description in English
                /// </summary>
                public const string CaptionEn = "List of mappings between panel and reference year set";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
            {
                { "id", new ColumnMetadata()
                {
                    Name = "id",
                    DbName = "id",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = true,
                    NotNull = true,
                    DefaultValue = "0",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "ID",
                    HeaderTextEn = "ID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 0
                }
                },
                { "panel", new ColumnMetadata()
                {
                    Name = "panel",
                    DbName = "panel",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = "0",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "PANEL",
                    HeaderTextEn = "PANEL",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 1
                }
                },
                { "reference_year_set", new ColumnMetadata()
                {
                    Name = "reference_year_set",
                    DbName = "reference_year_set",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = "0",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "REFERENCE_YEAR_SET",
                    HeaderTextEn = "REFERENCE_YEAR_SET",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 2
                }
                },
                { "comment", new ColumnMetadata()
                {
                    Name = "comment",
                    DbName = "comment",
                    DataType = "System.String",
                    DbDataType = "text",
                    NewDataType = "text",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = null,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "COMMENT",
                    HeaderTextEn = "COMMENT",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 3
                }
                }
            };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column panel metadata
                /// </summary>
                public static readonly ColumnMetadata ColPanelId = Cols["panel"];

                /// <summary>
                /// Column reference_year_set metadata
                /// </summary>
                public static readonly ColumnMetadata ColReferenceYearSetId = Cols["reference_year_set"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public ReferenceYearSetToPanelMappingList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public ReferenceYearSetToPanelMappingList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// List of mappings between panel and reference year set (read-only)
                /// </summary>
                public List<ReferenceYearSetToPanelMapping> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new ReferenceYearSetToPanelMapping(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// Mapping between panel and reference year set from list by identifier (read-only)
                /// </summary>
                /// <param name="id">Mapping between panel and reference year set identifier</param>
                /// <returns>Mapping betweenpanel and reference year set from list by identifier (null if not found)</returns>
                public ReferenceYearSetToPanelMapping this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new ReferenceYearSetToPanelMapping(composite: this, data: a))
                                .FirstOrDefault<ReferenceYearSetToPanelMapping>();
                    }
                }

                #endregion Indexer


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public ReferenceYearSetToPanelMappingList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new ReferenceYearSetToPanelMappingList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new ReferenceYearSetToPanelMappingList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                #endregion Methods

            }

        }
    }
}
