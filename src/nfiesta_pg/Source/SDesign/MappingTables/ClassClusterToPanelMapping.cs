﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace SDesign
        {
            // cm_cluster2panel_mapping

            /// <summary>
            /// Mapping between cluster and panel
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class ClusterToPanelMapping(
                ClusterToPanelMappingList composite = null,
                DataRow data = null)
                    : AMappingTableEntry<ClusterToPanelMappingList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Mapping between cluster and panel identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: ClusterToPanelMappingList.ColId.Name,
                            defaultValue: Int32.Parse(s: ClusterToPanelMappingList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: ClusterToPanelMappingList.ColId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Sampling panel identifier
                /// </summary>
                public int PanelId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: ClusterToPanelMappingList.ColPanelId.Name,
                            defaultValue: Int32.Parse(s: ClusterToPanelMappingList.ColPanelId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: ClusterToPanelMappingList.ColPanelId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Sampling panel object
                /// </summary>
                public Panel Panel
                {
                    get
                    {
                        return ((NfiEstaDB)Composite.Database).SDesign.TPanel[PanelId];
                    }
                }


                /// <summary>
                /// Sampling cluster identifier
                /// </summary>
                public int ClusterId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: ClusterToPanelMappingList.ColClusterId.Name,
                            defaultValue: Int32.Parse(s: ClusterToPanelMappingList.ColClusterId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: ClusterToPanelMappingList.ColClusterId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Sampling cluster object
                /// </summary>
                public Cluster Cluster
                {
                    get
                    {
                        return ((NfiEstaDB)Composite.Database).SDesign.TCluster[ClusterId];
                    }
                }


                /// <summary>
                /// Sampling weight - for each cluster can vary among panels
                /// </summary>
                public double SamplingWeight
                {
                    get
                    {
                        return Functions.GetDoubleArg(
                            row: Data,
                            name: ClusterToPanelMappingList.ColSamplingWeight.Name,
                            defaultValue: Double.Parse(s: ClusterToPanelMappingList.ColSamplingWeight.DefaultValue));
                    }
                    set
                    {
                        Functions.SetDoubleArg(
                            row: Data,
                            name: ClusterToPanelMappingList.ColSamplingWeight.Name,
                            val: value);
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true|false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not ClusterToPanelMapping Type, return False
                    if (obj is not ClusterToPanelMapping)
                    {
                        return false;
                    }

                    return ((ClusterToPanelMapping)obj).Id == Id;
                }

                /// <summary>
                /// Returns a hash code for the current object
                /// </summary>
                /// <returns>Hash code for the current object</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                /// <summary>
                /// Returns a string that represents the current object
                /// </summary>
                /// <returns>String that represents the current object</returns>
                public override string ToString()
                {
                    return String.Concat(
                        $"{nameof(ClusterToPanelMapping)}: {{",
                        $"{nameof(Id)}: {Functions.PrepIntArg(arg: Id)}; ",
                        $"{nameof(PanelId)}: {Functions.PrepIntArg(arg: PanelId)}; ",
                        $"{nameof(ClusterId)}: {Functions.PrepIntArg(arg: ClusterId)}; ",
                        $"{nameof(SamplingWeight)}: {Functions.PrepDoubleArg(arg: SamplingWeight)}}}");
                }

                #endregion Methods

            }


            /// <summary>
            /// List of mappings between cluster and panel
            /// </summary>
            public class ClusterToPanelMappingList
                : AMappingTable
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema = SDesignSchema.Name;

                /// <summary>
                /// Table name
                /// </summary>
                public const string Name = "cm_cluster2panel_mapping";

                /// <summary>
                /// Table alias
                /// </summary>
                public const string Alias = "cm_cluster2panel_mapping";

                /// <summary>
                /// Description in national language
                /// </summary>
                public const string CaptionCs = "Seznam mapování klastrů do panelů";

                /// <summary>
                /// Description in English
                /// </summary>
                public const string CaptionEn = "List of mappings between cluster and panel";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
            {
                { "id", new ColumnMetadata()
                {
                    Name = "id",
                    DbName = "id",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = true,
                    NotNull = true,
                    DefaultValue = "0",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "ID",
                    HeaderTextEn = "ID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 0
                }
                },
                { "panel", new ColumnMetadata()
                {
                    Name = "panel",
                    DbName = "panel",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = "0",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "PANEL",
                    HeaderTextEn = "PANEL",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 1
                }
                },
                { "cluster", new ColumnMetadata()
                {
                    Name = "cluster",
                    DbName = "cluster",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = "0",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "CLUSTER",
                    HeaderTextEn = "CLUSTER",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 2
                }
                },
                { "sampling_weight_ha", new ColumnMetadata()
                {
                    Name = "sampling_weight_ha",
                    DbName = "sampling_weight_ha",
                    DataType = "System.Double",
                    DbDataType = "float8",
                    NewDataType = "float8",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = "0.0",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "N3",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "SAMPLING_WEIGHT",
                    HeaderTextEn = "SAMPLING_WEIGHT",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 3
                }
                }
            };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column panel metadata
                /// </summary>
                public static readonly ColumnMetadata ColPanelId = Cols["panel"];

                /// <summary>
                /// Column cluster metadata
                /// </summary>
                public static readonly ColumnMetadata ColClusterId = Cols["cluster"];

                /// <summary>
                /// Column sampling_weight_ha metadata
                /// </summary>
                public static readonly ColumnMetadata ColSamplingWeight = Cols["sampling_weight_ha"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public ClusterToPanelMappingList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public ClusterToPanelMappingList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// List of mappings between cluster and panel(read-only)
                /// </summary>
                public List<ClusterToPanelMapping> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new ClusterToPanelMapping(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// Mapping between cluster and panel from list by identifier (read-only)
                /// </summary>
                /// <param name="id">Mapping between cluster and panel identifier</param>
                /// <returns>Mapping between cluster and panel from list by identifier (null if not found)</returns>
                public ClusterToPanelMapping this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new ClusterToPanelMapping(composite: this, data: a))
                                .FirstOrDefault<ClusterToPanelMapping>();
                    }
                }

                #endregion Indexer


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public ClusterToPanelMappingList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new ClusterToPanelMappingList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new ClusterToPanelMappingList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                #endregion Methods

            }

        }
    }
}
