﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi.NfiEstaPg.SDesign
{

    partial class FormSDesign
    {

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.msMain = new System.Windows.Forms.MenuStrip();
            this.tsmiLookupTables = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiCCountry = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiMappingTables = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiCmClusterToPanelMapping = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiCmPlotToClusterConfigurationMapping = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiCmReferenceYearSetToPanelMapping = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiSpatialTables = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFpPlot = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiTClusterConfiguration = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiTStratum = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiDataTables = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiTCluster = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiTInventoryCampaign = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiTPanel = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiTPlotMeasurementDate = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiTReferenceYearSet = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiTStrataSet = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiETL = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiExtractData = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiGetDifferences = new System.Windows.Forms.ToolStripMenuItem();
            this.tlpMain = new System.Windows.Forms.TableLayoutPanel();
            this.tlpButtons = new System.Windows.Forms.TableLayoutPanel();
            this.pnlClose = new System.Windows.Forms.Panel();
            this.btnClose = new System.Windows.Forms.Button();
            this.grpMain = new System.Windows.Forms.GroupBox();
            this.splitMain = new System.Windows.Forms.SplitContainer();
            this.pnlData = new System.Windows.Forms.Panel();
            this.txtSQL = new System.Windows.Forms.RichTextBox();
            this.msMain.SuspendLayout();
            this.tlpMain.SuspendLayout();
            this.tlpButtons.SuspendLayout();
            this.pnlClose.SuspendLayout();
            this.grpMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitMain)).BeginInit();
            this.splitMain.Panel1.SuspendLayout();
            this.splitMain.Panel2.SuspendLayout();
            this.splitMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // msMain
            // 
            this.msMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiLookupTables,
            this.tsmiMappingTables,
            this.tsmiSpatialTables,
            this.tsmiDataTables,
            this.tsmiETL});
            this.msMain.Location = new System.Drawing.Point(0, 0);
            this.msMain.Name = "msMain";
            this.msMain.Size = new System.Drawing.Size(944, 24);
            this.msMain.TabIndex = 0;
            // 
            // tsmiLookupTables
            // 
            this.tsmiLookupTables.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiCCountry});
            this.tsmiLookupTables.Name = "tsmiLookupTables";
            this.tsmiLookupTables.Size = new System.Drawing.Size(114, 20);
            this.tsmiLookupTables.Text = "tsmiLookupTables";
            // 
            // tsmiCCountry
            // 
            this.tsmiCCountry.Name = "tsmiCCountry";
            this.tsmiCCountry.Size = new System.Drawing.Size(180, 22);
            this.tsmiCCountry.Text = "tsmiCCountry";
            // 
            // tsmiMappingTables
            // 
            this.tsmiMappingTables.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiCmClusterToPanelMapping,
            this.tsmiCmPlotToClusterConfigurationMapping,
            this.tsmiCmReferenceYearSetToPanelMapping});
            this.tsmiMappingTables.Name = "tsmiMappingTables";
            this.tsmiMappingTables.Size = new System.Drawing.Size(122, 20);
            this.tsmiMappingTables.Text = "tsmiMappingTables";
            // 
            // tsmiCmClusterToPanelMapping
            // 
            this.tsmiCmClusterToPanelMapping.Name = "tsmiCmClusterToPanelMapping";
            this.tsmiCmClusterToPanelMapping.Size = new System.Drawing.Size(308, 22);
            this.tsmiCmClusterToPanelMapping.Text = "tsmiCmClusterToPanelMapping";
            // 
            // tsmiCmPlotToClusterConfigurationMapping
            // 
            this.tsmiCmPlotToClusterConfigurationMapping.Name = "tsmiCmPlotToClusterConfigurationMapping";
            this.tsmiCmPlotToClusterConfigurationMapping.Size = new System.Drawing.Size(308, 22);
            this.tsmiCmPlotToClusterConfigurationMapping.Text = "tsmiCmPlotToClusterConfigurationMapping";
            // 
            // tsmiCmReferenceYearSetToPanelMapping
            // 
            this.tsmiCmReferenceYearSetToPanelMapping.Name = "tsmiCmReferenceYearSetToPanelMapping";
            this.tsmiCmReferenceYearSetToPanelMapping.Size = new System.Drawing.Size(308, 22);
            this.tsmiCmReferenceYearSetToPanelMapping.Text = "tsmiCmReferenceYearSetToPanelMapping";
            // 
            // tsmiSpatialTables
            // 
            this.tsmiSpatialTables.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiFpPlot,
            this.tsmiTClusterConfiguration,
            this.tsmiTStratum});
            this.tsmiSpatialTables.Name = "tsmiSpatialTables";
            this.tsmiSpatialTables.Size = new System.Drawing.Size(109, 20);
            this.tsmiSpatialTables.Text = "tsmiSpatialTables";
            // 
            // tsmiFpPlot
            // 
            this.tsmiFpPlot.Name = "tsmiFpPlot";
            this.tsmiFpPlot.Size = new System.Drawing.Size(213, 22);
            this.tsmiFpPlot.Text = "tsmiFpPlot";
            // 
            // tsmiTClusterConfiguration
            // 
            this.tsmiTClusterConfiguration.Name = "tsmiTClusterConfiguration";
            this.tsmiTClusterConfiguration.Size = new System.Drawing.Size(213, 22);
            this.tsmiTClusterConfiguration.Text = "tsmiTClusterConfiguration";
            // 
            // tsmiTStratum
            // 
            this.tsmiTStratum.Name = "tsmiTStratum";
            this.tsmiTStratum.Size = new System.Drawing.Size(213, 22);
            this.tsmiTStratum.Text = "tsmiTStratum";
            // 
            // tsmiDataTables
            // 
            this.tsmiDataTables.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiTCluster,
            this.tsmiTInventoryCampaign,
            this.tsmiTPanel,
            this.tsmiTPlotMeasurementDate,
            this.tsmiTReferenceYearSet,
            this.tsmiTStrataSet});
            this.tsmiDataTables.Name = "tsmiDataTables";
            this.tsmiDataTables.Size = new System.Drawing.Size(98, 20);
            this.tsmiDataTables.Text = "tsmiDataTables";
            // 
            // tsmiTCluster
            // 
            this.tsmiTCluster.Name = "tsmiTCluster";
            this.tsmiTCluster.Size = new System.Drawing.Size(221, 22);
            this.tsmiTCluster.Text = "tsmiTCluster";
            // 
            // tsmiTInventoryCampaign
            // 
            this.tsmiTInventoryCampaign.Name = "tsmiTInventoryCampaign";
            this.tsmiTInventoryCampaign.Size = new System.Drawing.Size(221, 22);
            this.tsmiTInventoryCampaign.Text = "tsmiTInventoryCampaign";
            // 
            // tsmiTPanel
            // 
            this.tsmiTPanel.Name = "tsmiTPanel";
            this.tsmiTPanel.Size = new System.Drawing.Size(221, 22);
            this.tsmiTPanel.Text = "tsmiTPanel";
            // 
            // tsmiTPlotMeasurementDate
            // 
            this.tsmiTPlotMeasurementDate.Name = "tsmiTPlotMeasurementDate";
            this.tsmiTPlotMeasurementDate.Size = new System.Drawing.Size(221, 22);
            this.tsmiTPlotMeasurementDate.Text = "tsmiTPlotMeasurementDate";
            // 
            // tsmiTReferenceYearSet
            // 
            this.tsmiTReferenceYearSet.Name = "tsmiTReferenceYearSet";
            this.tsmiTReferenceYearSet.Size = new System.Drawing.Size(221, 22);
            this.tsmiTReferenceYearSet.Text = "tsmiTReferenceYearSet";
            // 
            // tsmiTStrataSet
            // 
            this.tsmiTStrataSet.Name = "tsmiTStrataSet";
            this.tsmiTStrataSet.Size = new System.Drawing.Size(221, 22);
            this.tsmiTStrataSet.Text = "tsmiTStrataSet";
            // 
            // tsmiETL
            // 
            this.tsmiETL.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiExtractData,
            this.tsmiGetDifferences});
            this.tsmiETL.Name = "tsmiETL";
            this.tsmiETL.Size = new System.Drawing.Size(60, 20);
            this.tsmiETL.Text = "tsmiETL";
            // 
            // tsmiExtractData
            // 
            this.tsmiExtractData.Name = "tsmiExtractData";
            this.tsmiExtractData.Size = new System.Drawing.Size(174, 22);
            this.tsmiExtractData.Text = "tsmiExtractData";
            // 
            // tsmiGetDifferences
            // 
            this.tsmiGetDifferences.Name = "tsmiGetDifferences";
            this.tsmiGetDifferences.Size = new System.Drawing.Size(174, 22);
            this.tsmiGetDifferences.Text = "tsmiGetDifferences";
            // 
            // tlpMain
            // 
            this.tlpMain.ColumnCount = 1;
            this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpMain.Controls.Add(this.tlpButtons, 0, 1);
            this.tlpMain.Controls.Add(this.grpMain, 0, 0);
            this.tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpMain.Location = new System.Drawing.Point(0, 24);
            this.tlpMain.Name = "tlpMain";
            this.tlpMain.RowCount = 2;
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlpMain.Size = new System.Drawing.Size(944, 477);
            this.tlpMain.TabIndex = 5;
            // 
            // tlpButtons
            // 
            this.tlpButtons.BackColor = System.Drawing.SystemColors.Control;
            this.tlpButtons.ColumnCount = 2;
            this.tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            this.tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpButtons.Controls.Add(this.pnlClose, 1, 0);
            this.tlpButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpButtons.Location = new System.Drawing.Point(0, 437);
            this.tlpButtons.Margin = new System.Windows.Forms.Padding(0);
            this.tlpButtons.Name = "tlpButtons";
            this.tlpButtons.RowCount = 1;
            this.tlpButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpButtons.Size = new System.Drawing.Size(944, 40);
            this.tlpButtons.TabIndex = 28;
            // 
            // pnlClose
            // 
            this.pnlClose.Controls.Add(this.btnClose);
            this.pnlClose.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlClose.Location = new System.Drawing.Point(784, 0);
            this.pnlClose.Margin = new System.Windows.Forms.Padding(0);
            this.pnlClose.Name = "pnlClose";
            this.pnlClose.Padding = new System.Windows.Forms.Padding(5);
            this.pnlClose.Size = new System.Drawing.Size(160, 40);
            this.pnlClose.TabIndex = 12;
            // 
            // btnClose
            // 
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.btnClose.Location = new System.Drawing.Point(5, 5);
            this.btnClose.Margin = new System.Windows.Forms.Padding(0);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(150, 30);
            this.btnClose.TabIndex = 13;
            this.btnClose.Text = "btnClose";
            this.btnClose.UseVisualStyleBackColor = true;
            // 
            // grpMain
            // 
            this.grpMain.Controls.Add(this.splitMain);
            this.grpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpMain.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.grpMain.ForeColor = System.Drawing.SystemColors.ControlText;
            this.grpMain.Location = new System.Drawing.Point(3, 3);
            this.grpMain.Name = "grpMain";
            this.grpMain.Size = new System.Drawing.Size(938, 431);
            this.grpMain.TabIndex = 5;
            this.grpMain.TabStop = false;
            // 
            // splitMain
            // 
            this.splitMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitMain.Location = new System.Drawing.Point(3, 18);
            this.splitMain.Name = "splitMain";
            this.splitMain.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitMain.Panel1
            // 
            this.splitMain.Panel1.Controls.Add(this.pnlData);
            // 
            // splitMain.Panel2
            // 
            this.splitMain.Panel2.Controls.Add(this.txtSQL);
            this.splitMain.Size = new System.Drawing.Size(932, 410);
            this.splitMain.SplitterDistance = 204;
            this.splitMain.SplitterWidth = 1;
            this.splitMain.TabIndex = 11;
            // 
            // pnlData
            // 
            this.pnlData.AutoScroll = true;
            this.pnlData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlData.Location = new System.Drawing.Point(0, 0);
            this.pnlData.Name = "pnlData";
            this.pnlData.Size = new System.Drawing.Size(932, 204);
            this.pnlData.TabIndex = 3;
            // 
            // txtSQL
            // 
            this.txtSQL.BackColor = System.Drawing.SystemColors.Window;
            this.txtSQL.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtSQL.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtSQL.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.txtSQL.Location = new System.Drawing.Point(0, 0);
            this.txtSQL.Name = "txtSQL";
            this.txtSQL.ReadOnly = true;
            this.txtSQL.Size = new System.Drawing.Size(932, 205);
            this.txtSQL.TabIndex = 0;
            this.txtSQL.Text = "";
            // 
            // FormSDesign
            // 
            this.AcceptButton = this.btnClose;
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(944, 501);
            this.Controls.Add(this.tlpMain);
            this.Controls.Add(this.msMain);
            this.MainMenuStrip = this.msMain;
            this.Name = "FormSDesign";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.msMain.ResumeLayout(false);
            this.msMain.PerformLayout();
            this.tlpMain.ResumeLayout(false);
            this.tlpButtons.ResumeLayout(false);
            this.pnlClose.ResumeLayout(false);
            this.grpMain.ResumeLayout(false);
            this.splitMain.Panel1.ResumeLayout(false);
            this.splitMain.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitMain)).EndInit();
            this.splitMain.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip msMain;
        private System.Windows.Forms.ToolStripMenuItem tsmiLookupTables;
        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.GroupBox grpMain;
        private System.Windows.Forms.SplitContainer splitMain;
        private System.Windows.Forms.Panel pnlData;
        private System.Windows.Forms.RichTextBox txtSQL;
        private System.Windows.Forms.ToolStripMenuItem tsmiMappingTables;
        private System.Windows.Forms.ToolStripMenuItem tsmiDataTables;
        private System.Windows.Forms.ToolStripMenuItem tsmiSpatialTables;
        private System.Windows.Forms.ToolStripMenuItem tsmiCCountry;
        private System.Windows.Forms.ToolStripMenuItem tsmiCmClusterToPanelMapping;
        private System.Windows.Forms.ToolStripMenuItem tsmiCmPlotToClusterConfigurationMapping;
        private System.Windows.Forms.ToolStripMenuItem tsmiCmReferenceYearSetToPanelMapping;
        private System.Windows.Forms.ToolStripMenuItem tsmiFpPlot;
        private System.Windows.Forms.ToolStripMenuItem tsmiTClusterConfiguration;
        private System.Windows.Forms.ToolStripMenuItem tsmiTStratum;
        private System.Windows.Forms.ToolStripMenuItem tsmiTCluster;
        private System.Windows.Forms.ToolStripMenuItem tsmiTInventoryCampaign;
        private System.Windows.Forms.ToolStripMenuItem tsmiTPanel;
        private System.Windows.Forms.ToolStripMenuItem tsmiTPlotMeasurementDate;
        private System.Windows.Forms.ToolStripMenuItem tsmiTReferenceYearSet;
        private System.Windows.Forms.ToolStripMenuItem tsmiTStrataSet;
        private System.Windows.Forms.ToolStripMenuItem tsmiETL;
        private System.Windows.Forms.ToolStripMenuItem tsmiGetDifferences;
        private System.Windows.Forms.TableLayoutPanel tlpButtons;
        private System.Windows.Forms.Panel pnlClose;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.ToolStripMenuItem tsmiExtractData;
    }

}