﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Versioning;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.Catalog;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace SDesign
        {

            /// <summary>
            /// Schema sdesign
            /// </summary>
            public class SDesignSchema
                : IDatabaseSchema
            {

                #region Constants

                /// <summary>
                /// Schema name,
                /// (Contains default value,
                /// value is not constant now,
                /// extension can be created with different schemas)
                /// </summary>
                public static string Name = "sdesign";

                /// <summary>
                /// Extension name
                /// </summary>
                public const string ExtensionName = "nfiesta_sdesign";

                /// <summary>
                /// Extension version
                /// </summary>
                public static readonly ExtensionVersion ExtensionVersion
                    = new(version: "1.1.15");

                #endregion Constants


                #region Private Fields

                #region General

                /// <summary>
                /// Database
                /// </summary>
                private IDatabase database;

                /// <summary>
                /// Omitted tables
                /// </summary>
                private List<string> omittedTables;

                #endregion General


                #region Lookup Tables

                /// <summary>
                /// List of countries
                /// </summary>
                private ZaJi.NfiEstaPg.SDesign.CountryList cCountry;

                #endregion Lookup Tables


                #region Mapping Tables

                /// <summary>
                /// List of mappings between cluster and panel
                /// </summary>
                private ZaJi.NfiEstaPg.SDesign.ClusterToPanelMappingList cmClusterToPanelMapping;

                /// <summary>
                /// List of mappings between plot and cluster configuration
                /// </summary>
                private ZaJi.NfiEstaPg.SDesign.PlotToClusterConfigurationMappingList cmPlotToClusterConfigurationMapping;

                /// <summary>
                /// List of mappings between reference year set and panel
                /// </summary>
                private ZaJi.NfiEstaPg.SDesign.ReferenceYearSetToPanelMappingList cmReferenceYearSetToPanelMapping;

                #endregion Mapping Tables


                #region Spatial Data Tables

                /// <summary>
                /// List of inventarization plots
                /// </summary>
                private ZaJi.NfiEstaPg.SDesign.PlotList fpPlot;

                /// <summary>
                /// List of sampling cluster configurations
                /// </summary>
                private ZaJi.NfiEstaPg.SDesign.ClusterConfigurationList tClusterConfiguration;

                /// <summary>
                /// List of stratas
                /// </summary>
                private ZaJi.NfiEstaPg.SDesign.StratumList tStratum;

                #endregion Spatial Data Tables


                #region Data Tables

                /// <summary>
                /// List of sampling clusters
                /// </summary>
                private ZaJi.NfiEstaPg.SDesign.ClusterList tCluster;

                /// <summary>
                /// List of inventory campaigns
                /// </summary>
                private ZaJi.NfiEstaPg.SDesign.InventoryCampaignList tInventoryCampaign;

                /// <summary>
                /// List of sampling panels
                /// </summary>
                private ZaJi.NfiEstaPg.SDesign.PanelList tPanel;

                /// <summary>
                /// List of inventory plot measurement dates
                /// </summary>
                private ZaJi.NfiEstaPg.SDesign.PlotMeasurementDateList tPlotMeasurementDate;

                /// <summary>
                /// List of reference year or season sets
                /// </summary>
                private ZaJi.NfiEstaPg.SDesign.ReferenceYearSetList tReferenceYearSet;

                /// <summary>
                /// List of agregation of stratas
                /// </summary>
                private ZaJi.NfiEstaPg.SDesign.StrataSetList tStrataSet;

                #endregion Data Tables

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database</param>
                public SDesignSchema(NfiEstaDB database)
                {

                    #region General

                    Database = database;

                    OmittedTables = [];

                    #endregion General


                    #region Lookup Tables

                    CCountry = new ZaJi.NfiEstaPg.SDesign.CountryList(
                        database: (NfiEstaDB)Database);

                    #endregion Lookup Tables


                    #region Mapping tables

                    CmClusterToPanelMapping = new ZaJi.NfiEstaPg.SDesign.ClusterToPanelMappingList(
                        database: (NfiEstaDB)Database);
                    CmPlotToClusterConfigurationMapping = new ZaJi.NfiEstaPg.SDesign.PlotToClusterConfigurationMappingList(
                        database: (NfiEstaDB)Database);
                    CmReferenceYearSetToPanelMapping = new ZaJi.NfiEstaPg.SDesign.ReferenceYearSetToPanelMappingList(
                        database: (NfiEstaDB)Database);

                    #endregion Mapping tables


                    #region Spatial data tables

                    FpPlot = new ZaJi.NfiEstaPg.SDesign.PlotList(
                        database: (NfiEstaDB)Database);
                    TClusterConfiguration = new ZaJi.NfiEstaPg.SDesign.ClusterConfigurationList(
                        database: (NfiEstaDB)Database);
                    TStratum = new ZaJi.NfiEstaPg.SDesign.StratumList(
                        database: (NfiEstaDB)Database);

                    #endregion Spatial data tables


                    #region Data tables

                    TCluster = new ZaJi.NfiEstaPg.SDesign.ClusterList(
                        database: (NfiEstaDB)Database);
                    TInventoryCampaign = new ZaJi.NfiEstaPg.SDesign.InventoryCampaignList(
                        database: (NfiEstaDB)Database);
                    TPanel = new ZaJi.NfiEstaPg.SDesign.PanelList(
                        database: (NfiEstaDB)Database);
                    TPlotMeasurementDate = new ZaJi.NfiEstaPg.SDesign.PlotMeasurementDateList(
                        database: (NfiEstaDB)Database);
                    TReferenceYearSet = new ZaJi.NfiEstaPg.SDesign.ReferenceYearSetList(
                        database: (NfiEstaDB)Database);
                    TStrataSet = new ZaJi.NfiEstaPg.SDesign.StrataSetList(
                        database: (NfiEstaDB)Database);

                    #endregion Data tables

                }

                #endregion Constructor


                #region Properties

                #region General

                /// <summary>
                /// Schema name (read-only)
                /// </summary>
                public string SchemaName
                {
                    get
                    {
                        return SDesignSchema.Name;
                    }
                }

                /// <summary>
                /// Database (not-null) (read-only)
                /// </summary>
                public IDatabase Database
                {
                    get
                    {
                        if (database == null)
                        {
                            throw new ArgumentNullException(
                                message: $"Argument {nameof(Database)} must not be null.",
                                paramName: nameof(Database));
                        }

                        if (database is not NfiEstaDB)
                        {
                            throw new ArgumentException(
                                message: $"Argument {nameof(Database)} must be type of {nameof(NfiEstaDB)}.",
                                paramName: nameof(Database));
                        }

                        return database;
                    }
                    private set
                    {
                        if (value == null)
                        {
                            throw new ArgumentNullException(
                                message: $"Argument {nameof(Database)} must not be null.",
                                paramName: nameof(Database));
                        }

                        if (value is not NfiEstaDB)
                        {
                            throw new ArgumentException(
                                message: $"Argument {nameof(Database)} must be type of {nameof(NfiEstaDB)}.",
                                paramName: nameof(Database));
                        }

                        database = value;
                    }
                }

                /// <summary>
                /// List of lookup tables (not-null) (read-only)
                /// </summary>
                public List<ILookupTable> LookupTables
                {
                    get
                    {
                        return [CCountry];
                    }
                }

                /// <summary>
                /// List of mapping tables (not-null) (read-only)
                /// </summary>
                public List<IMappingTable> MappingTables
                {
                    get
                    {
                        return
                        [
                            CmClusterToPanelMapping,
                            CmPlotToClusterConfigurationMapping,
                            CmReferenceYearSetToPanelMapping
                        ];
                    }
                }

                /// <summary>
                /// List of spatial data tables (not-null) (read-only)
                /// </summary>
                public List<ISpatialTable> SpatialTables
                {
                    get
                    {
                        return
                        [
                           FpPlot,
                           TClusterConfiguration,
                           TStratum
                        ];
                    }
                }

                /// <summary>
                ///List of data tables (not-null) (read-only)
                /// </summary>
                public List<IDataTable> DataTables
                {
                    get
                    {
                        return
                        [
                          TCluster,
                          TInventoryCampaign,
                          TPanel,
                          TPlotMeasurementDate,
                          TReferenceYearSet,
                          TStrataSet
                        ];
                    }
                }

                /// <summary>
                /// List of all tables (not-null) (read-only)
                /// </summary>
                public List<IDatabaseTable> Tables
                {
                    get
                    {
                        List<IDatabaseTable> tables = [];
                        tables.AddRange(collection: LookupTables);
                        tables.AddRange(collection: MappingTables);
                        tables.AddRange(collection: SpatialTables);
                        tables.AddRange(collection: DataTables);
                        tables.Sort(comparison: (a, b) => a.TableName.CompareTo(strB: b.TableName));
                        return tables;
                    }
                }

                /// <summary>
                /// Omitted tables
                /// </summary>
                public List<string> OmittedTables
                {
                    get
                    {
                        return omittedTables ?? [];
                    }
                    set
                    {
                        omittedTables = value ?? [];
                    }
                }

                #endregion General


                #region Lookup Tables

                /// <summary>
                /// List of countries (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.SDesign.CountryList CCountry
                {
                    get
                    {
                        return cCountry ??
                            new ZaJi.NfiEstaPg.SDesign.CountryList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        cCountry = value ??
                            new ZaJi.NfiEstaPg.SDesign.CountryList(database: (NfiEstaDB)Database);
                    }
                }

                #endregion Lookup Tables


                #region Mapping Tables

                /// <summary>
                /// List of mappings between cluster and panel (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.SDesign.ClusterToPanelMappingList CmClusterToPanelMapping
                {
                    get
                    {
                        return cmClusterToPanelMapping ??
                            new ZaJi.NfiEstaPg.SDesign.ClusterToPanelMappingList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        cmClusterToPanelMapping = value ??
                            new ZaJi.NfiEstaPg.SDesign.ClusterToPanelMappingList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of mappings between plot and cluster configuration (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.SDesign.PlotToClusterConfigurationMappingList CmPlotToClusterConfigurationMapping
                {
                    get
                    {
                        return cmPlotToClusterConfigurationMapping ??
                            new ZaJi.NfiEstaPg.SDesign.PlotToClusterConfigurationMappingList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        cmPlotToClusterConfigurationMapping = value ??
                            new ZaJi.NfiEstaPg.SDesign.PlotToClusterConfigurationMappingList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of mappings between reference year set and panel (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.SDesign.ReferenceYearSetToPanelMappingList CmReferenceYearSetToPanelMapping
                {
                    get
                    {
                        return cmReferenceYearSetToPanelMapping ??
                            new ZaJi.NfiEstaPg.SDesign.ReferenceYearSetToPanelMappingList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        cmReferenceYearSetToPanelMapping = value ??
                            new ZaJi.NfiEstaPg.SDesign.ReferenceYearSetToPanelMappingList(database: (NfiEstaDB)Database);
                    }
                }

                #endregion Mapping Tables


                #region Spatial Data Tables

                /// <summary>
                /// List of inventarization plots (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.SDesign.PlotList FpPlot
                {
                    get
                    {
                        return fpPlot ??
                            new ZaJi.NfiEstaPg.SDesign.PlotList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        fpPlot = value ??
                            new ZaJi.NfiEstaPg.SDesign.PlotList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of sampling cluster configurations (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.SDesign.ClusterConfigurationList TClusterConfiguration
                {
                    get
                    {
                        return tClusterConfiguration ??
                            new ZaJi.NfiEstaPg.SDesign.ClusterConfigurationList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        tClusterConfiguration = value ??
                            new ZaJi.NfiEstaPg.SDesign.ClusterConfigurationList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of stratas  (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.SDesign.StratumList TStratum
                {
                    get
                    {
                        return tStratum ??
                            new ZaJi.NfiEstaPg.SDesign.StratumList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        tStratum = value ??
                            new ZaJi.NfiEstaPg.SDesign.StratumList(database: (NfiEstaDB)Database);
                    }
                }

                #endregion Spatial Data Tables


                #region Data Tables

                /// <summary>
                /// List of sampling clusters  (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.SDesign.ClusterList TCluster
                {
                    get
                    {
                        return tCluster ??
                            new ZaJi.NfiEstaPg.SDesign.ClusterList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        tCluster = value ??
                            new ZaJi.NfiEstaPg.SDesign.ClusterList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of inventory campaigns  (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.SDesign.InventoryCampaignList TInventoryCampaign
                {
                    get
                    {
                        return tInventoryCampaign ??
                            new ZaJi.NfiEstaPg.SDesign.InventoryCampaignList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        tInventoryCampaign = value ??
                            new ZaJi.NfiEstaPg.SDesign.InventoryCampaignList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of sampling panels  (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.SDesign.PanelList TPanel
                {
                    get
                    {
                        return tPanel ??
                            new ZaJi.NfiEstaPg.SDesign.PanelList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        tPanel = value ??
                            new ZaJi.NfiEstaPg.SDesign.PanelList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of inventory plot measurement dates  (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.SDesign.PlotMeasurementDateList TPlotMeasurementDate
                {
                    get
                    {
                        return tPlotMeasurementDate ??
                            new ZaJi.NfiEstaPg.SDesign.PlotMeasurementDateList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        tPlotMeasurementDate = value ??
                            new ZaJi.NfiEstaPg.SDesign.PlotMeasurementDateList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of reference year or season sets  (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.SDesign.ReferenceYearSetList TReferenceYearSet
                {
                    get
                    {
                        return tReferenceYearSet ??
                            new ZaJi.NfiEstaPg.SDesign.ReferenceYearSetList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        tReferenceYearSet = value ??
                            new ZaJi.NfiEstaPg.SDesign.ReferenceYearSetList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of agregation of stratas  (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.SDesign.StrataSetList TStrataSet
                {
                    get
                    {
                        return tStrataSet ??
                            new ZaJi.NfiEstaPg.SDesign.StrataSetList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        tStrataSet = value ??
                            new ZaJi.NfiEstaPg.SDesign.StrataSetList(database: (NfiEstaDB)Database);
                    }
                }

                #endregion Data Tables


                #region Differences

                /// <summary>
                /// Differences in database schema (not-null) (read-only)
                /// </summary>
                public List<string> Differences
                {
                    get
                    {
                        List<string> result = [];
                        Database.Postgres.Catalog.Load();

                        DBExtension dbExtension = Database.Postgres.Catalog.Extensions.Items
                            .Where(a => a.Name == SDesignSchema.ExtensionName)
                            .FirstOrDefault();

                        if (dbExtension == null)
                        {
                            result.Add(item: String.Concat(
                                $"Extension {SDesignSchema.ExtensionName} does not exist in database."));
                            return result;
                        }
                        else
                        {
                            ExtensionVersion dbExtensionVersion = new(
                                version: dbExtension.Version);
                            if (!dbExtensionVersion.Equals(obj: SDesignSchema.ExtensionVersion))
                            {
                                result.Add(item: String.Concat(
                                    $"Database extension {SDesignSchema.ExtensionName} is in version {dbExtension.Version}. ",
                                    $"But the version {SDesignSchema.ExtensionVersion} was expected."));
                            }

                            DBSchema dbSchema = Database.Postgres.Catalog.Schemas.Items
                                .Where(a => a.Name == SDesignSchema.Name)
                                .FirstOrDefault();

                            if (dbSchema == null)
                            {
                                result.Add(item: String.Concat(
                                    $"Schema {SDesignSchema.Name} does not exist in database."));
                                return result;
                            }
                            else
                            {
                                foreach (DBTable dbTable in dbSchema.Tables.Items.OrderBy(a => a.Name))
                                {
                                    if (OmittedTables.Contains(item: dbTable.Name))
                                    {
                                        continue;
                                    }

                                    IDatabaseTable iTable = ((NfiEstaDB)Database).SDesign.Tables
                                        .Where(a => a.TableName == dbTable.Name)
                                        .FirstOrDefault();

                                    if (iTable == null)
                                    {
                                        result.Add(item: String.Concat(
                                            $"Class for table {dbTable.Name} does not exist in dll library."));
                                    }
                                }

                                foreach (IDatabaseTable iTable in ((NfiEstaDB)Database).SDesign.Tables.OrderBy(a => a.TableName))
                                {
                                    DBTable dbTable = dbSchema.Tables.Items
                                            .Where(a => a.Name == iTable.TableName)
                                            .FirstOrDefault();

                                    if (dbTable == null)
                                    {
                                        if (iTable.Columns.Values.Where(a => a.Elemental).Any())
                                        {
                                            result.Add(item: String.Concat(
                                                $"Table {iTable.TableName} does not exist in database."));
                                        }
                                        else
                                        {
                                            // Table doesn't contain any column that is loaded from database.
                                        }
                                    }
                                    else
                                    {
                                        foreach (DBColumn dbColumn in dbTable.Columns.Items.OrderBy(a => a.Name))
                                        {
                                            ColumnMetadata iColumn = iTable.Columns.Values
                                                .Where(a => a.Elemental)
                                                .Where(a => a.DbName == dbColumn.Name)
                                                .FirstOrDefault();

                                            if (iColumn == null)
                                            {
                                                result.Add(item: String.Concat(
                                                    $"Column {dbTable.Name}.{dbColumn.Name} does not exist in dll library."));
                                            }
                                        }

                                        foreach (ColumnMetadata iColumn in iTable.Columns.Values.Where(a => a.Elemental).OrderBy(a => a.DbName))
                                        {
                                            DBColumn dbColumn = dbTable.Columns.Items
                                                .Where(a => a.Name == iColumn.DbName)
                                                .FirstOrDefault();

                                            if (dbColumn == null)
                                            {
                                                result.Add(item: String.Concat(
                                                    $"Column {iTable.TableName}.{iColumn.DbName} does not exist in database."));
                                            }
                                            else
                                            {
                                                if (dbColumn.TypeName != iColumn.DbDataType)
                                                {
                                                    result.Add(item: String.Concat(
                                                        $"Column {iTable.TableName}.{iColumn.DbName} is type of {dbColumn.TypeName}. ",
                                                        $"But type of {iColumn.DbDataType} was expected."));
                                                }

                                                if (dbColumn.NotNull != iColumn.NotNull)
                                                {
                                                    string strDbNull = (bool)dbColumn.NotNull ? "has NOT NULL constraint" : "is NULLABLE";
                                                    string strINull = iColumn.NotNull ? "NOT NULL contraint" : "NULLABLE";
                                                    result.Add(item: String.Concat(
                                                        $"Column {iTable.TableName}.{iColumn.DbName} {strDbNull}. ",
                                                        $"But {strINull} was expected."));
                                                }
                                            }
                                        }
                                    }
                                }

                                Dictionary<string, string> iStoredProcedures = [];
                                foreach (
                                    Type nestedType in typeof(ZaJi.NfiEstaPg.SDesign.SDesignFunctions)
                                    .GetNestedTypes()
                                    .AsEnumerable()
                                    .OrderBy(a => a.FullName)
                                    )
                                {
                                    PropertyInfo pSignature = nestedType.GetProperty(name: "Signature");
                                    PropertyInfo pName = nestedType.GetProperty(name: "Name");
                                    if ((pSignature != null) && (pName != null))
                                    {
                                        // signature = null označeje uložené procedury,
                                        // pro které není požadována implementace v databázi
                                        // nekontrolují se
                                        if (pSignature.GetValue(obj: null) != null)
                                        {
                                            string a = pSignature.GetValue(obj: null).ToString();
                                            string b = pName.GetValue(obj: null).ToString();

                                            iStoredProcedures.TryAdd(key: a, value: b);
                                        }
                                    }
                                }

                                foreach (DBStoredProcedure dbStoredProcedure in dbSchema.StoredProcedures.Items.OrderBy(a => a.Name))
                                {
                                    if (!iStoredProcedures.ContainsKey(dbStoredProcedure.Signature))
                                    {
                                        result.Add(item: String.Concat(
                                           $"Class for stored procedure {dbStoredProcedure.Name} does not exist in dll library."));
                                    }
                                }

                                foreach (KeyValuePair<string, string> iStoredProcedure in iStoredProcedures)
                                {
                                    DBStoredProcedure dbStoredProcedure = dbSchema.StoredProcedures.Items
                                            .Where(a => a.Signature == iStoredProcedure.Key)
                                            .FirstOrDefault();
                                    if (dbStoredProcedure == null)
                                    {
                                        result.Add(item: String.Concat(
                                               $"Stored procedure {iStoredProcedure.Value} does not exist in database."));
                                    }
                                }

                                return result;
                            }
                        }
                    }
                }

                #endregion Differences

                #endregion Properties


                #region Methods

                /// <summary>
                /// Returns the string that represents current object
                /// </summary>
                /// <returns>Returns the string that represents current object</returns>
                public override string ToString()
                {
                    return Database.Postgres.Setting.LanguageVersion switch
                    {
                        LanguageVersion.National => String.Concat($"Data schématu {SDesignSchema.Name}."),
                        LanguageVersion.International => String.Concat($"Data from schema {SDesignSchema.Name}."),
                        _ => String.Concat($"Data from schema {SDesignSchema.Name}."),
                    };
                }

                #endregion Methods


                #region Static Methods

                /// <summary>
                /// Exports data of the schema
                /// </summary>
                /// <param name="database">Database tables</param>
                /// <param name="fileFormat">Format of the file with exported data</param>
                /// <param name="tableNames">List of table names to export (if null than all tables)</param>
                [SupportedOSPlatform("windows")]
                public static void ExportData(
                    NfiEstaDB database,
                    ExportFileFormat fileFormat,
                    List<string> tableNames = null)
                {
                    System.Windows.Forms.FolderBrowserDialog dlg
                        = new();

                    if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        ExportData(
                            database: database,
                            fileFormat: fileFormat,
                            folder: dlg.SelectedPath,
                            tableNames: tableNames);
                    }
                }

                /// <summary>
                /// Exports data of the schema
                /// </summary>
                /// <param name="database">Database tables</param>
                /// <param name="fileFormat">Format of the file with exported data</param>
                /// <param name="folder">Folder for writing file with data</param>
                /// <param name="tableNames">List of table names to export (if null than all tables)</param>
                public static void ExportData(
                    NfiEstaDB database,
                    ExportFileFormat fileFormat,
                    string folder,
                    List<string> tableNames = null)
                {
                    List<ADatabaseTable> databaseTables =
                    [
                        // c_country
                        new ZaJi.NfiEstaPg.SDesign.CountryList(database: database),

                        // cm_cluster2panel_mapping
                        new ZaJi.NfiEstaPg.SDesign.ClusterToPanelMappingList(database: database),

                        // cm_plot2cluster_config_mapping
                        new ZaJi.NfiEstaPg.SDesign.PlotToClusterConfigurationMappingList(database: database),

                        // cm_refyearset2panel_mapping
                        new ZaJi.NfiEstaPg.SDesign.ReferenceYearSetToPanelMappingList(database: database),

                        // f_p_plot
                        new ZaJi.NfiEstaPg.SDesign.PlotList(database: database),

                        // t_cluster
                        new ZaJi.NfiEstaPg.SDesign.ClusterList(database: database),

                        // t_cluster_configuration
                        new ZaJi.NfiEstaPg.SDesign.ClusterConfigurationList(database: database),

                        // t_inventory_campaign
                        new ZaJi.NfiEstaPg.SDesign.InventoryCampaignList(database: database),

                        // t_panel
                        new ZaJi.NfiEstaPg.SDesign.PanelList(database: database),

                        // t_plot_measurement_dates
                        new ZaJi.NfiEstaPg.SDesign.PlotMeasurementDateList(database: database),

                        // t_reference_year_set
                        new ZaJi.NfiEstaPg.SDesign.ReferenceYearSetList(database: database),

                        // t_strata_set
                        new ZaJi.NfiEstaPg.SDesign.StrataSetList(database: database),

                        // t_stratum
                        new ZaJi.NfiEstaPg.SDesign.StratumList(database: database)
                    ];

                    foreach (ADatabaseTable databaseTable in databaseTables)
                    {
                        if ((tableNames == null) ||
                             tableNames.Contains(item: databaseTable.TableName))
                        {
                            if (databaseTable is ALookupTable lookupTable)
                            {
                                lookupTable.ReLoad(extended: false);
                            }
                            else if (databaseTable is AMappingTable mappingTable)
                            {
                                mappingTable.ReLoad();
                            }
                            else if (databaseTable is ASpatialTable spatialTable)
                            {
                                spatialTable.ReLoad(withGeom: false);
                            }
                            else if (databaseTable is ADataTable dataTable)
                            {
                                dataTable.ReLoad();
                            }
                            else
                            {
                                throw new Exception(message: $"Argument {nameof(databaseTable)} unknown type.");
                            }

                            databaseTable.ExportData(
                                fileFormat: fileFormat,
                                folder: folder);
                        }
                    }
                }

                #endregion Static Methods

            }

        }
    }
}
