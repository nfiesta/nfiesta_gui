﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi.NfiEstaPg.SDesign
{

    /// <summary>
    /// <para lang="cs">Formulář zobrazení dat extenze nfiesta_sdesign</para>
    /// <para lang="en">Form for displaying nfiesta_sdesign extension data</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    public partial class FormSDesign
        : Form, INfiEstaControl
    {

        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Omezení počtu řádků</para>
        /// <para lang="en">Row count limit</para>
        /// </summary>
        private Nullable<int> limit;

        private string msgDataExportComplete = String.Empty;
        private string msgNoDifferencesFound = String.Empty;

        #endregion Private Fields


        #region Contructor

        /// <summary>
        /// <para lang="cs">Konstruktor formuláře</para>
        /// <para lang="en">Form constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        public FormSDesign(Control controlOwner)
        {
            InitializeComponent();
            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                if (controlOwner is not INfiEstaControl)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(INfiEstaControl)}.",
                        paramName: nameof(ControlOwner));
                }

                return controlOwner;
            }
            set
            {
                if (value is not INfiEstaControl)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(INfiEstaControl)}.",
                        paramName: nameof(ControlOwner));
                }

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables (read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">Omezení počtu řádků</para>
        /// <para lang="en">Row count limit</para>
        /// </summary>
        public Nullable<int> Limit
        {
            get
            {
                return limit;
            }
            set
            {
                limit = value;
            }
        }

        #endregion Properties


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace formuláře</para>
        /// <para lang="en">Initializing the form</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        private void Initialize(Control controlOwner)
        {
            ControlOwner = controlOwner;
            Limit = null;

            InitializeLabels();

            btnClose.Click += new EventHandler(
                (sender, e) =>
                {
                    DialogResult = DialogResult.Cancel;
                    Close();
                });

            tsmiCCountry.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.SDesign.CountryList(database: Database),
                        columns: ZaJi.NfiEstaPg.SDesign.CountryList.Cols);
                });

            tsmiCmClusterToPanelMapping.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.SDesign.ClusterToPanelMappingList(database: Database),
                        columns: ZaJi.NfiEstaPg.SDesign.ClusterToPanelMappingList.Cols);
                });

            tsmiCmPlotToClusterConfigurationMapping.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.SDesign.PlotToClusterConfigurationMappingList(database: Database),
                        columns: ZaJi.NfiEstaPg.SDesign.PlotToClusterConfigurationMappingList.Cols);
                });

            tsmiCmReferenceYearSetToPanelMapping.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.SDesign.ReferenceYearSetToPanelMappingList(database: Database),
                        columns: ZaJi.NfiEstaPg.SDesign.ReferenceYearSetToPanelMappingList.Cols);
                });

            tsmiFpPlot.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.SDesign.PlotList(database: Database),
                        columns: ZaJi.NfiEstaPg.SDesign.PlotList.Cols);
                });

            tsmiTClusterConfiguration.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.SDesign.ClusterConfigurationList(database: Database),
                        columns: ZaJi.NfiEstaPg.SDesign.ClusterConfigurationList.Cols);
                });

            tsmiTStratum.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.SDesign.StratumList(database: Database),
                        columns: ZaJi.NfiEstaPg.SDesign.StratumList.Cols);
                });

            tsmiTCluster.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.SDesign.ClusterList(database: Database),
                        columns: ZaJi.NfiEstaPg.SDesign.ClusterList.Cols);
                });

            tsmiTInventoryCampaign.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.SDesign.InventoryCampaignList(database: Database),
                        columns: ZaJi.NfiEstaPg.SDesign.InventoryCampaignList.Cols);
                });

            tsmiTPanel.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.SDesign.PanelList(database: Database),
                        columns: ZaJi.NfiEstaPg.SDesign.PanelList.Cols);
                });

            tsmiTPlotMeasurementDate.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.SDesign.PlotMeasurementDateList(database: Database),
                        columns: ZaJi.NfiEstaPg.SDesign.PlotMeasurementDateList.Cols);
                });

            tsmiTReferenceYearSet.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.SDesign.ReferenceYearSetList(database: Database),
                        columns: ZaJi.NfiEstaPg.SDesign.ReferenceYearSetList.Cols);
                });

            tsmiTStrataSet.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.SDesign.StrataSetList(database: Database),
                        columns: ZaJi.NfiEstaPg.SDesign.StrataSetList.Cols);
                });

            tsmiExtractData.Click += new EventHandler(
              (sender, e) => { ExportData(); });

            tsmiGetDifferences.Click += new EventHandler(
              (sender, e) => { DisplayDifferences(); });
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků formuláře</para>
        /// <para lang="en">Initializing form labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            switch (LanguageVersion)
            {
                case LanguageVersion.National:
                    Text = $"{SDesignSchema.ExtensionName} ({SDesignSchema.ExtensionVersion})";

                    tsmiLookupTables.Text = "Číselníky";
                    tsmiCCountry.Text = ZaJi.NfiEstaPg.SDesign.CountryList.Name;

                    tsmiMappingTables.Text = "Mapovací tabulky";
                    tsmiCmClusterToPanelMapping.Text = ZaJi.NfiEstaPg.SDesign.ClusterToPanelMappingList.Name;
                    tsmiCmPlotToClusterConfigurationMapping.Text = ZaJi.NfiEstaPg.SDesign.PlotToClusterConfigurationMappingList.Name;
                    tsmiCmReferenceYearSetToPanelMapping.Text = ZaJi.NfiEstaPg.SDesign.ReferenceYearSetToPanelMappingList.Name;

                    tsmiSpatialTables.Text = "Prostorové tabulky";
                    tsmiFpPlot.Text = ZaJi.NfiEstaPg.SDesign.PlotList.Name;
                    tsmiTClusterConfiguration.Text = ZaJi.NfiEstaPg.SDesign.ClusterConfigurationList.Name;
                    tsmiTStratum.Text = ZaJi.NfiEstaPg.SDesign.StratumList.Name;

                    tsmiDataTables.Text = "Datové tabulky";
                    tsmiTCluster.Text = ZaJi.NfiEstaPg.SDesign.ClusterList.Name;
                    tsmiTInventoryCampaign.Text = ZaJi.NfiEstaPg.SDesign.InventoryCampaignList.Name;
                    tsmiTPanel.Text = ZaJi.NfiEstaPg.SDesign.PanelList.Name;
                    tsmiTPlotMeasurementDate.Text = ZaJi.NfiEstaPg.SDesign.PlotMeasurementDateList.Name;
                    tsmiTReferenceYearSet.Text = ZaJi.NfiEstaPg.SDesign.ReferenceYearSetList.Name;
                    tsmiTStrataSet.Text = ZaJi.NfiEstaPg.SDesign.StrataSetList.Name;

                    tsmiETL.Text = "ETL";
                    tsmiExtractData.Text = "Export dat do vybrané složky";
                    tsmiGetDifferences.Text = "Rozdíly mezi dll a databázovou extenzí";

                    btnClose.Text = "Zavřít";

                    msgDataExportComplete = "Export dat byl dokončen.";
                    msgNoDifferencesFound = "Nebyly nalezeny žádné rozdíly.";

                    break;

                case LanguageVersion.International:
                    Text = $"{SDesignSchema.ExtensionName} ({SDesignSchema.ExtensionVersion})";

                    tsmiLookupTables.Text = "Lookup tables";
                    tsmiCCountry.Text = ZaJi.NfiEstaPg.SDesign.CountryList.Name;

                    tsmiMappingTables.Text = "Mapping tables";
                    tsmiCmClusterToPanelMapping.Text = ZaJi.NfiEstaPg.SDesign.ClusterToPanelMappingList.Name;
                    tsmiCmPlotToClusterConfigurationMapping.Text = ZaJi.NfiEstaPg.SDesign.PlotToClusterConfigurationMappingList.Name;
                    tsmiCmReferenceYearSetToPanelMapping.Text = ZaJi.NfiEstaPg.SDesign.ReferenceYearSetToPanelMappingList.Name;

                    tsmiSpatialTables.Text = "Spatial tables";
                    tsmiFpPlot.Text = ZaJi.NfiEstaPg.SDesign.PlotList.Name;
                    tsmiTClusterConfiguration.Text = ZaJi.NfiEstaPg.SDesign.ClusterConfigurationList.Name;
                    tsmiTStratum.Text = ZaJi.NfiEstaPg.SDesign.StratumList.Name;

                    tsmiDataTables.Text = "Data tables";
                    tsmiTCluster.Text = ZaJi.NfiEstaPg.SDesign.ClusterList.Name;
                    tsmiTInventoryCampaign.Text = ZaJi.NfiEstaPg.SDesign.InventoryCampaignList.Name;
                    tsmiTPanel.Text = ZaJi.NfiEstaPg.SDesign.PanelList.Name;
                    tsmiTPlotMeasurementDate.Text = ZaJi.NfiEstaPg.SDesign.PlotMeasurementDateList.Name;
                    tsmiTReferenceYearSet.Text = ZaJi.NfiEstaPg.SDesign.ReferenceYearSetList.Name;
                    tsmiTStrataSet.Text = ZaJi.NfiEstaPg.SDesign.StrataSetList.Name;

                    tsmiETL.Text = "ETL";
                    tsmiExtractData.Text = "Export data to folder";
                    tsmiGetDifferences.Text = "Differences between dll database extension";

                    btnClose.Text = "Close";

                    msgDataExportComplete = "Data export has been completed.";
                    msgNoDifferencesFound = "No differences have been found.";

                    break;

                default:
                    Text = nameof(FormSDesign);

                    tsmiLookupTables.Text = nameof(tsmiLookupTables);
                    tsmiCCountry.Text = nameof(tsmiCCountry);

                    tsmiMappingTables.Text = nameof(tsmiMappingTables);
                    tsmiCmClusterToPanelMapping.Text = nameof(tsmiCmClusterToPanelMapping);
                    tsmiCmPlotToClusterConfigurationMapping.Text = nameof(tsmiCmPlotToClusterConfigurationMapping);
                    tsmiCmReferenceYearSetToPanelMapping.Text = nameof(tsmiCmReferenceYearSetToPanelMapping);

                    tsmiSpatialTables.Text = nameof(tsmiSpatialTables);
                    tsmiFpPlot.Text = nameof(tsmiFpPlot);
                    tsmiTClusterConfiguration.Text = nameof(tsmiTClusterConfiguration);
                    tsmiTStratum.Text = nameof(tsmiTStratum);

                    tsmiDataTables.Text = nameof(tsmiDataTables);
                    tsmiTCluster.Text = nameof(tsmiTCluster);
                    tsmiTInventoryCampaign.Text = nameof(tsmiTInventoryCampaign);
                    tsmiTPanel.Text = nameof(tsmiTPanel);
                    tsmiTPlotMeasurementDate.Text = nameof(tsmiTPlotMeasurementDate);
                    tsmiTReferenceYearSet.Text = nameof(tsmiTReferenceYearSet);
                    tsmiTStrataSet.Text = nameof(tsmiTStrataSet);

                    tsmiETL.Text = nameof(tsmiETL);
                    tsmiExtractData.Text = nameof(tsmiExtractData);
                    tsmiGetDifferences.Text = nameof(tsmiGetDifferences);

                    btnClose.Text = nameof(btnClose);

                    msgDataExportComplete = nameof(msgDataExportComplete);
                    msgNoDifferencesFound = nameof(msgNoDifferencesFound);

                    break;
            }
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek</para>
        /// <para lang="en">Uploading database table data</para>
        /// </summary>
        public void LoadContent() { }

        /// <summary>
        /// <para lang="cs">Zobrazí data číselníku</para>
        /// <para lang="en">Display lookup table data</para>
        /// </summary>
        /// <param name="table">
        /// <para lang="cs">Tabulka</para>
        /// <para lang="en">Table</para>
        /// </param>
        /// <param name="columns">
        /// <para lang="cs">Sloupce tabulky</para>
        /// <para lang="en">Table columns</para>
        /// </param>
        private void DisplayTable(
            ALookupTable table,
            Dictionary<string, ColumnMetadata> columns)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(value: dgvData);
            table.ReLoad(limit: Limit, extended: false);
            ALookupTable.SetColumnsVisibility(columns: columns);
            table.SetDataGridViewDataSource(dataGridView: dgvData);
            table.FormatDataGridView(dataGridView: dgvData);
            grpMain.Text = table.Caption;
            txtSQL.Text = $"{Environment.NewLine}{table.SQL}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(box: txtSQL);
        }

        /// <summary>
        /// <para lang="cs">Zobrazí data mapovací tabulky</para>
        /// <para lang="en">Display mapping table data</para>
        /// </summary>
        /// <param name="table">
        /// <para lang="cs">Tabulka</para>
        /// <para lang="en">Table</para>
        /// </param>
        /// <param name="columns">
        /// <para lang="cs">Sloupce tabulky</para>
        /// <para lang="en">Table columns</para>
        /// </param>
        private void DisplayTable(
            AMappingTable table,
            Dictionary<string, ColumnMetadata> columns)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(value: dgvData);
            table.ReLoad(limit: Limit);
            AMappingTable.SetColumnsVisibility(columns: columns);
            table.SetDataGridViewDataSource(dataGridView: dgvData);
            table.FormatDataGridView(dataGridView: dgvData);
            grpMain.Text = table.Caption;
            txtSQL.Text = $"{Environment.NewLine}{table.SQL}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(box: txtSQL);
        }

        /// <summary>
        /// <para lang="cs">Zobrazí data tabulky s prostorovými daty</para>
        /// <para lang="en">Display data table with spatial data</para>
        /// </summary>
        /// <param name="table">
        /// <para lang="cs">Tabulka</para>
        /// <para lang="en">Table</para>
        /// </param>
        /// <param name="columns">
        /// <para lang="cs">Sloupce tabulky</para>
        /// <para lang="en">Table columns</para>
        /// </param>
        private void DisplayTable(
            ASpatialTable table,
            Dictionary<string, ColumnMetadata> columns)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(value: dgvData);
            table.ReLoad(limit: Limit, withGeom: false);
            ASpatialTable.SetColumnsVisibility(columns: columns);
            table.SetDataGridViewDataSource(dataGridView: dgvData);
            table.FormatDataGridView(dataGridView: dgvData);
            grpMain.Text = table.Caption;
            txtSQL.Text = $"{Environment.NewLine}{table.SQL}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(box: txtSQL);
        }

        /// <summary>
        /// <para lang="cs">Zobrazí data tabulky</para>
        /// <para lang="en">Display table data</para>
        /// </summary>
        /// <param name="table">
        /// <para lang="cs">Tabulka</para>
        /// <para lang="en">Table</para>
        /// </param>
        /// <param name="columns">
        /// <para lang="cs">Sloupce tabulky</para>
        /// <para lang="en">Table columns</para>
        /// </param>
        private void DisplayTable(
            ADataTable table,
            Dictionary<string, ColumnMetadata> columns)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(value: dgvData);
            table.ReLoad(limit: Limit);
            ADataTable.SetColumnsVisibility(columns: columns);
            table.SetDataGridViewDataSource(dataGridView: dgvData);
            table.FormatDataGridView(dataGridView: dgvData);
            grpMain.Text = table.Caption;
            txtSQL.Text = $"{Environment.NewLine}{table.SQL}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(box: txtSQL);
        }

        /// <summary>
        /// <para lang="cs">Zobrazí data vrácená uloženou procedurou</para>
        /// <para lang="en">Display data returned from stored procedure</para>
        /// </summary>
        /// <param name="table">
        /// <para lang="cs">Tabulka s daty vrácenými uloženou procedurou</para>
        /// <para lang="en">Table with data returned from stored procedure</para>
        /// </param>
        /// <param name="columns">
        /// <para lang="cs">Sloupce tabulky</para>
        /// <para lang="en">Table columns</para>
        /// </param>
        private void DisplayTable(
            AParametrizedView table,
            Dictionary<string, ColumnMetadata> columns)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(value: dgvData);
            ADataTable.SetColumnsVisibility(columns: columns);
            table.SetDataGridViewDataSource(dataGridView: dgvData);
            table.FormatDataGridView(dataGridView: dgvData);
            grpMain.Text = table.Caption;
            txtSQL.Text = $"{Environment.NewLine}{table.SQL}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(box: txtSQL);
        }

        /// <summary>
        /// <para lang="cs">Export dat</para>
        /// <para lang="en">Data export</para>
        /// </summary>
        private void ExportData()
        {
            SDesignSchema.ExportData(
               database: Database,
               fileFormat: ExportFileFormat.Xml);

            MessageBox.Show(
                caption: String.Empty,
                text: msgDataExportComplete,
                buttons: MessageBoxButtons.OK,
                icon: MessageBoxIcon.Information);
        }

        /// <summary>
        /// <para lang="cs">Zobrazí provedené změny v databázi</para>
        /// <para lang="en">Displays changes made to the database</para>
        /// </summary>
        private void DisplayDifferences()
        {
            grpMain.Text = String.Empty;
            pnlData.Controls.Clear();
            txtSQL.Text = String.Empty;

            List<string> differences = Database.SDesign.Differences;
            System.Text.StringBuilder stringBuilder = new();
            if ((differences == null) || (differences.Count == 0))
            {
                stringBuilder.AppendLine(value: msgNoDifferencesFound);
            }
            else
            {
                foreach (string difference in differences)
                {
                    stringBuilder.AppendLine(value: $"* {difference}");
                }
            }

            Label lblReport = new()
            {
                Dock = DockStyle.Fill,
                Text = stringBuilder.ToString()
            };
            pnlData.Controls.Add(value: lblReport);
        }

        #endregion Methods

    }

}