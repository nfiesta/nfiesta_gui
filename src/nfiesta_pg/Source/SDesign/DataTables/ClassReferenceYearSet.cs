﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace SDesign
        {
            // t_reference_year_set

            /// <summary>
            /// Reference year or season set
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class ReferenceYearSet(
                ReferenceYearSetList composite = null,
                DataRow data = null)
                    : ADataTableEntry<ReferenceYearSetList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Reference year or season set identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: ReferenceYearSetList.ColId.Name,
                            defaultValue: Int32.Parse(s: ReferenceYearSetList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: ReferenceYearSetList.ColId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Inventory campaign identifier
                /// </summary>
                public int InventoryCampaingId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: ReferenceYearSetList.ColInventoryCampaignId.Name,
                            defaultValue: Int32.Parse(s: ReferenceYearSetList.ColInventoryCampaignId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: ReferenceYearSetList.ColInventoryCampaignId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Inventory campaign object (read-only)
                /// </summary>
                public InventoryCampaign InventoryCampaign
                {
                    get
                    {
                        return
                            ((NfiEstaDB)Composite.Database)
                            .SDesign.TInventoryCampaign[InventoryCampaingId];
                    }
                }


                /// <summary>
                /// Reference year or season set name
                /// </summary>
                public string Name
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: ReferenceYearSetList.ColName.Name,
                            defaultValue: ReferenceYearSetList.ColName.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: ReferenceYearSetList.ColName.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Date from which the period set begins
                /// </summary>
                public Nullable<DateTime> ReferenceDateBegin
                {
                    get
                    {
                        return Functions.GetNDateTimeArg(
                            row: Data,
                            name: ReferenceYearSetList.ColReferenceDateBegin.Name,
                            defaultValue: String.IsNullOrEmpty(value: ReferenceYearSetList.ColReferenceDateBegin.DefaultValue) ?
                                            (Nullable<DateTime>)null :
                                            DateTime.Parse(s: ReferenceYearSetList.ColReferenceDateBegin.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNDateTimeArg(
                            row: Data,
                            name: ReferenceYearSetList.ColReferenceDateBegin.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Date from which the period set begins as text
                /// </summary>
                public string ReferenceDateBeginText
                {
                    get
                    {
                        return
                            (ReferenceDateBegin == null) ?
                                Functions.StrNull :
                                ((DateTime)ReferenceDateBegin).ToString(
                                    format: ReferenceYearSetList.ColReferenceDateBegin.NumericFormat);
                    }
                }


                /// <summary>
                /// Date to which the period set ends
                /// </summary>
                public Nullable<DateTime> ReferenceDateEnd
                {
                    get
                    {
                        return Functions.GetNDateTimeArg(
                            row: Data,
                            name: ReferenceYearSetList.ColReferenceDateEnd.Name,
                            defaultValue: String.IsNullOrEmpty(value: ReferenceYearSetList.ColReferenceDateEnd.DefaultValue) ?
                                            (Nullable<DateTime>)null :
                                            DateTime.Parse(s: ReferenceYearSetList.ColReferenceDateEnd.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNDateTimeArg(
                            row: Data,
                            name: ReferenceYearSetList.ColReferenceDateEnd.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Date to which the period set ends as text
                /// </summary>
                public string ReferenceDateEndText
                {
                    get
                    {
                        return
                            (ReferenceDateEnd == null) ?
                                Functions.StrNull :
                                ((DateTime)ReferenceDateEnd).ToString(
                                    format: ReferenceYearSetList.ColReferenceDateEnd.NumericFormat);
                    }
                }


                /// <summary>
                /// Reference year or season set label
                /// </summary>
                public string Label
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: ReferenceYearSetList.ColLabel.Name,
                            defaultValue: ReferenceYearSetList.ColLabel.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: ReferenceYearSetList.ColLabel.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Reference year or season set comment
                /// </summary>
                public string Comment
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: ReferenceYearSetList.ColComment.Name,
                            defaultValue: ReferenceYearSetList.ColComment.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: ReferenceYearSetList.ColComment.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Reference year set begin
                /// </summary>
                public Nullable<int> ReferenceYearSetBegin
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: ReferenceYearSetList.ColReferenceYearSetBegin.Name,
                            defaultValue: String.IsNullOrEmpty(value: ReferenceYearSetList.ColReferenceYearSetBegin.DefaultValue) ?
                                (Nullable<int>)null :
                                 Int32.Parse(s: ReferenceYearSetList.ColReferenceYearSetBegin.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: ReferenceYearSetList.ColReferenceYearSetBegin.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Reference year set end
                /// </summary>
                public Nullable<int> ReferenceYearSetEnd
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: ReferenceYearSetList.ColReferenceYearSetEnd.Name,
                            defaultValue: String.IsNullOrEmpty(value: ReferenceYearSetList.ColReferenceYearSetEnd.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: ReferenceYearSetList.ColReferenceYearSetEnd.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: ReferenceYearSetList.ColReferenceYearSetEnd.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Indication of type of target variables which may refer to reference year set:
                /// true for variables of current status, false for dynamic and change variables.
                /// </summary>
                public bool StatusVariables
                {
                    get
                    {
                        return Functions.GetBoolArg(
                            row: Data,
                            name: ReferenceYearSetList.ColStatusVariables.Name,
                            defaultValue: Boolean.Parse(value: ReferenceYearSetList.ColStatusVariables.DefaultValue));
                    }
                    set
                    {
                        Functions.SetBoolArg(
                            row: Data,
                            name: ReferenceYearSetList.ColStatusVariables.Name,
                            val: value);
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true|false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not ReferenceYearSet Type, return False
                    if (obj is not ReferenceYearSet)
                    {
                        return false;
                    }

                    return ((ReferenceYearSet)obj).Id == Id;
                }

                /// <summary>
                /// Returns a hash code for the current object
                /// </summary>
                /// <returns>Hash code for the current object</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                /// <summary>
                /// Returns a string that represents the current object
                /// </summary>
                /// <returns>String that represents the current object</returns>
                public override string ToString()
                {
                    return String.Concat(
                        $"{nameof(ReferenceYearSet)}: {{",
                        $"{nameof(Id)}: {Functions.PrepIntArg(arg: Id)}; ",
                        $"{nameof(InventoryCampaingId)}: {Functions.PrepIntArg(arg: InventoryCampaingId)}; ",
                        $"{nameof(Name)}: {Functions.PrepStringArg(arg: Name)}; ",
                        $"{nameof(ReferenceDateBegin)}: {Functions.PrepStringArg(arg: ReferenceDateBeginText)}; ",
                        $"{nameof(ReferenceDateEnd)}: {Functions.PrepStringArg(arg: ReferenceDateEndText)}; ",
                        $"{nameof(Label)}: {Functions.PrepStringArg(arg: Label)}; ",
                        $"{nameof(Comment)}: {Functions.PrepStringArg(arg: Comment)}; ",
                        $"{nameof(ReferenceYearSetBegin)}: {Functions.PrepNIntArg(arg: ReferenceYearSetBegin)}; ",
                        $"{nameof(ReferenceYearSetEnd)}: {Functions.PrepNIntArg(arg: ReferenceYearSetEnd)}; ",
                        $"{nameof(StatusVariables)}: {Functions.PrepBoolArg(arg: StatusVariables)}}}");
                }

                #endregion Methods

            }


            /// <summary>
            /// List of reference year or season sets
            /// </summary>
            public class ReferenceYearSetList
                : ADataTable
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema = SDesignSchema.Name;

                /// <summary>
                /// Table name
                /// </summary>
                public const string Name = "t_reference_year_set";

                /// <summary>
                /// Table alias
                /// </summary>
                public const string Alias = "t_reference_year_set";

                /// <summary>
                /// Description in national language
                /// </summary>
                public const string CaptionCs = "Seznam roků nebo sezón měření";

                /// <summary>
                /// Description in English
                /// </summary>
                public const string CaptionEn = "List of reference year or season sets";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
            {
                { "id", new ColumnMetadata()
                {
                    Name = "id",
                    DbName = "id",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = true,
                    NotNull = true,
                    DefaultValue = "0",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "ID",
                    HeaderTextEn = "ID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 0
                }
                },
                { "inventory_campaign", new ColumnMetadata()
                {
                    Name = "inventory_campaign",
                    DbName = "inventory_campaign",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = "0",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "INVENTORY_CAMPAIGN",
                    HeaderTextEn = "INVENTORY_CAMPAIGN",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 1
                }
                },
                { "reference_year_set", new ColumnMetadata()
                {
                    Name = "reference_year_set",
                    DbName = "reference_year_set",
                    DataType = "System.String",
                    DbDataType = "varchar",
                    NewDataType = "varchar",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = String.Empty,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "String.Empty",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "REFERENCE_YEAR_SET",
                    HeaderTextEn = "REFERENCE_YEAR_SET",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 2
                }
                },
                { "reference_date_begin", new ColumnMetadata()
                {
                    Name = "reference_date_begin",
                    DbName = "reference_date_begin",
                    DataType = "System.DateTime",
                    DbDataType = "date",
                    NewDataType = "date",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = null,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "yyyy-MM-dd",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "REFERENCE_DATE_BEGIN",
                    HeaderTextEn = "REFERENCE_DATE_BEGIN",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 3
                }
                },
                { "reference_date_end", new ColumnMetadata()
                {
                    Name = "reference_date_end",
                    DbName = "reference_date_end",
                    DataType = "System.DateTime",
                    DbDataType = "date",
                    NewDataType = "date",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = null,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "yyyy-MM-dd",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "REFERENCE_DATE_END",
                    HeaderTextEn = "REFERENCE_DATE_END",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 4
                }
                },
                { "label", new ColumnMetadata()
                {
                    Name = "label",
                    DbName = "label",
                    DataType = "System.String",
                    DbDataType = "varchar",
                    NewDataType = "varchar",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = String.Empty,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "String.Empty",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "LABEL",
                    HeaderTextEn = "LABEL",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 5
                }
                },
                { "comment", new ColumnMetadata()
                {
                    Name = "comment",
                    DbName = "comment",
                    DataType = "System.String",
                    DbDataType = "text",
                    NewDataType = "text",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = String.Empty,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "String.Empty",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "COMMENT",
                    HeaderTextEn = "COMMENT",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 6
                }
                },
                { "reference_year_set_begin", new ColumnMetadata()
                {
                    Name = "reference_year_set_begin",
                    DbName = "reference_year_set_begin",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = null,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "REFERENCE_YEAR_SET_BEGIN",
                    HeaderTextEn = "REFERENCE_YEAR_SET_BEGIN",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 7
                }
                },
                { "reference_year_set_end", new ColumnMetadata()
                {
                    Name = "reference_year_set_end",
                    DbName = "reference_year_set_end",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = null,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "REFERENCE_YEAR_SET_END",
                    HeaderTextEn = "REFERENCE_YEAR_SET_END",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 8
                }
                },
                { "status_variables", new ColumnMetadata()
                {
                    Name = "status_variables",
                    DbName = "status_variables",
                    DataType = "System.Boolean",
                    DbDataType = "bool",
                    NewDataType = "bool",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = "false",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "STATUS_VARIABLES",
                    HeaderTextEn = "STATUS_VARIABLES",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 9
                }
                }
            };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column inventory_campaign metadata
                /// </summary>
                public static readonly ColumnMetadata ColInventoryCampaignId = Cols["inventory_campaign"];

                /// <summary>
                /// Column reference_year_set metadata
                /// </summary>
                public static readonly ColumnMetadata ColName = Cols["reference_year_set"];

                /// <summary>
                /// Column reference_date_begin metadata
                /// </summary>
                public static readonly ColumnMetadata ColReferenceDateBegin = Cols["reference_date_begin"];

                /// <summary>
                /// Column reference_date_end metadata
                /// </summary>
                public static readonly ColumnMetadata ColReferenceDateEnd = Cols["reference_date_end"];

                /// <summary>
                /// Column label metadata
                /// </summary>
                public static readonly ColumnMetadata ColLabel = Cols["label"];

                /// <summary>
                /// Column comment metadata
                /// </summary>
                public static readonly ColumnMetadata ColComment = Cols["comment"];

                /// <summary>
                /// Column reference_year_set_begin metadata
                /// </summary>
                public static readonly ColumnMetadata ColReferenceYearSetBegin = Cols["reference_year_set_begin"];

                /// <summary>
                /// Column reference_year_set_end metadata
                /// </summary>
                public static readonly ColumnMetadata ColReferenceYearSetEnd = Cols["reference_year_set_end"];

                /// <summary>
                /// Column status_variables metadata
                /// </summary>
                public static readonly ColumnMetadata ColStatusVariables = Cols["status_variables"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public ReferenceYearSetList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public ReferenceYearSetList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// List of reference year or season sets (read-only)
                /// </summary>
                public List<ReferenceYearSet> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new ReferenceYearSet(composite: this, data: a))];
                    }
                }

                /// <summary>
                /// List of begin reference dates
                /// </summary>
                public List<DateTime> BeginReferenceDates
                {
                    get
                    {
                        return
                            Items
                            .Where(a => a.ReferenceDateBegin != null)
                            .Select(a => (DateTime)a.ReferenceDateBegin)
                            .Distinct<DateTime>()
                            .ToList<DateTime>();
                    }
                }

                /// <summary>
                /// List of end reference dates
                /// </summary>
                public List<DateTime> EndReferenceDates
                {
                    get
                    {
                        return
                            Items
                            .Where(a => a.ReferenceDateEnd != null)
                            .Select(a => (DateTime)a.ReferenceDateEnd)
                            .Distinct<DateTime>()
                            .ToList<DateTime>();
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// Reference year or season set from list by identifier (read-only)
                /// </summary>
                /// <param name="id">Reference year or season set identifier</param>
                /// <returns>Reference year or season set from list by identifier (null if not found)</returns>
                public ReferenceYearSet this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new ReferenceYearSet(composite: this, data: a))
                                .FirstOrDefault<ReferenceYearSet>();
                    }
                }

                #endregion Indexer


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public ReferenceYearSetList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new ReferenceYearSetList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new ReferenceYearSetList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                #endregion Methods

            }

        }
    }
}