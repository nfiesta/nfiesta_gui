﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace SDesign
        {
            // t_panel

            /// <summary>
            /// Sampling panel
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class Panel(
                PanelList composite = null,
                DataRow data = null)
                    : ADataTableEntry<PanelList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Sampling panel identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: PanelList.ColId.Name,
                            defaultValue: Int32.Parse(s: PanelList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: PanelList.ColId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Stratum identifier
                /// </summary>
                public int StratumId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: PanelList.ColStratumId.Name,
                            defaultValue: Int32.Parse(s: PanelList.ColStratumId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: PanelList.ColStratumId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Stratum object
                /// </summary>
                public Stratum Stratum
                {
                    get
                    {
                        return
                             ((NfiEstaDB)Composite.Database)
                             .SDesign.TStratum[StratumId];
                    }
                }


                /// <summary>
                /// Sampling panel name
                /// </summary>
                public string Name
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: PanelList.ColName.Name,
                            defaultValue: PanelList.ColName.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: PanelList.ColName.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Sampling cluster configuration identifier
                /// </summary>
                public int ClusterConfigurationId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: PanelList.ColClusterConfigurationId.Name,
                            defaultValue: Int32.Parse(s: PanelList.ColClusterConfigurationId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: PanelList.ColClusterConfigurationId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Sampling cluster configuration object
                /// </summary>
                public ClusterConfiguration ClusterConfiguration
                {
                    get
                    {
                        return
                             ((NfiEstaDB)Composite.Database)
                             .SDesign.TClusterConfiguration[ClusterConfigurationId];
                    }
                }


                /// <summary>
                /// Label of the panel
                /// </summary>
                public string Label
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: PanelList.ColLabel.Name,
                            defaultValue: PanelList.ColLabel.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: PanelList.ColLabel.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Optional commentary for panel
                /// </summary>
                public string Comment
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: PanelList.ColComment.Name,
                            defaultValue: PanelList.ColComment.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: PanelList.ColComment.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Total number of clusters on panel
                /// </summary>
                public int ClusterCount
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: PanelList.ColClusterCount.Name,
                            defaultValue: Int32.Parse(s: PanelList.ColClusterCount.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: PanelList.ColClusterCount.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Total number of plots on panel
                /// </summary>
                public int PlotCount
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: PanelList.ColPlotCount.Name,
                            defaultValue: Int32.Parse(s: PanelList.ColPlotCount.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: PanelList.ColPlotCount.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Identifier of panel from which is the current panel subseted.
                /// </summary>
                public Nullable<int> PanelSubsetId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: PanelList.ColPanelSubsetId.Name,
                            defaultValue: String.IsNullOrEmpty(value: PanelList.ColPanelSubsetId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: PanelList.ColPanelSubsetId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: PanelList.ColPanelSubsetId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Panel from which is the current panel subseted. (read-only)
                /// </summary>
                public Panel PanelSubset
                {
                    get
                    {
                        return
                            (PanelSubsetId != null) ?
                             ((NfiEstaDB)Composite.Database)
                             .SDesign.TPanel[(int)PanelSubsetId] : null;
                    }
                }


                /// <summary>
                /// Sum of sampling weights for all sampling units in the panel.
                /// </summary>
                public double SWeightPanelSum
                {
                    get
                    {
                        return Functions.GetDoubleArg(
                            row: Data,
                            name: PanelList.ColSWeightPanelSum.Name,
                            defaultValue: Int32.Parse(s: PanelList.ColSWeightPanelSum.DefaultValue));
                    }
                    set
                    {
                        Functions.SetDoubleArg(
                            row: Data,
                            name: PanelList.ColSWeightPanelSum.Name,
                            val: value);
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true|false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not Panel Type, return False
                    if (obj is not Panel)
                    {
                        return false;
                    }

                    return ((Panel)obj).Id == Id;
                }

                /// <summary>
                /// Returns a hash code for the current object
                /// </summary>
                /// <returns>Hash code for the current object</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                /// <summary>
                /// Returns a string that represents the current object
                /// </summary>
                /// <returns>String that represents the current object</returns>
                public override string ToString()
                {
                    return String.Concat(
                        $"{nameof(Panel)}: {{",
                        $"{nameof(Id)}: {Functions.PrepIntArg(arg: Id)}; ",
                        $"{nameof(StratumId)}: {Functions.PrepIntArg(arg: StratumId)}; ",
                        $"{nameof(Name)}: {Functions.PrepStringArg(arg: Name)}; ",
                        $"{nameof(ClusterConfigurationId)}: {Functions.PrepIntArg(arg: ClusterConfigurationId)}; ",
                        $"{nameof(Label)}: {Functions.PrepStringArg(arg: Label)}; ",
                        $"{nameof(Comment)}: {Functions.PrepStringArg(arg: Comment)}; ",
                        $"{nameof(ClusterCount)}: {Functions.PrepIntArg(arg: ClusterCount)}; ",
                        $"{nameof(PlotCount)}: {Functions.PrepIntArg(arg: PlotCount)}; ",
                        $"{nameof(PanelSubsetId)}: {Functions.PrepNIntArg(arg: PanelSubsetId)}; ",
                        $"{nameof(SWeightPanelSum)}: {Functions.PrepDoubleArg(arg: SWeightPanelSum)}}}");
                }

                #endregion Methods

            }


            /// <summary>
            /// List of sampling panels
            /// </summary>
            public class PanelList
                : ADataTable
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema = SDesignSchema.Name;

                /// <summary>
                /// Table name
                /// </summary>
                public const string Name = "t_panel";

                /// <summary>
                /// Table alias
                /// </summary>
                public const string Alias = "t_panel";

                /// <summary>
                /// Description in national language
                /// </summary>
                public const string CaptionCs = "Seznam panelů";

                /// <summary>
                /// Description in English
                /// </summary>
                public const string CaptionEn = "List of sampling panels";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
            {
                { "id", new ColumnMetadata()
                {
                    Name = "id",
                    DbName = "id",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = true,
                    NotNull = true,
                    DefaultValue = "0",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "ID",
                    HeaderTextEn = "ID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 0
                }
                },
                { "stratum", new ColumnMetadata()
                {
                    Name = "stratum",
                    DbName = "stratum",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = "0",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "STRATUM",
                    HeaderTextEn = "STRATUM",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 1
                }
                },
                { "panel", new ColumnMetadata()
                {
                    Name = "panel",
                    DbName = "panel",
                    DataType = "System.String",
                    DbDataType = "varchar",
                    NewDataType = "varchar",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = String.Empty,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat =  String.Empty,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "PANEL",
                    HeaderTextEn = "PANEL",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 2
                }
                },
                { "cluster_configuration", new ColumnMetadata()
                {
                    Name = "cluster_configuration",
                    DbName = "cluster_configuration",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = "0",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "CLUSTER_CONFIGURATION",
                    HeaderTextEn = "CLUSTER_CONFIGURATION",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 3
                }
                },
                { "label", new ColumnMetadata()
                {
                    Name = "label",
                    DbName = "label",
                    DataType = "System.String",
                    DbDataType = "varchar",
                    NewDataType = "varchar",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = String.Empty,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "LABEL",
                    HeaderTextEn = "LABEL",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 4
                }
                },
                { "comment", new ColumnMetadata()
                {
                    Name = "comment",
                    DbName = "comment",
                    DataType = "System.String",
                    DbDataType = "text",
                    NewDataType = "text",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = String.Empty,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "COMMENT",
                    HeaderTextEn = "COMMENT",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 5
                }
                },
                { "cluster_count", new ColumnMetadata()
                {
                    Name = "cluster_count",
                    DbName = "cluster_count",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = "0",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "CLUSTER_COUNT",
                    HeaderTextEn = "CLUSTER_COUNT",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 6
                }
                },
                { "plot_count", new ColumnMetadata()
                {
                    Name = "plot_count",
                    DbName = "plot_count",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = "0",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "PLOT_COUNT",
                    HeaderTextEn = "PLOT_COUNT",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 7
                }
                },
                { "panel_subset", new ColumnMetadata()
                {
                    Name = "panel_subset",
                    DbName = "panel_subset",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = null,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "PANEL_SUBSET",
                    HeaderTextEn = "PANEL_SUBSET",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 8
                }
                },
                { "sweight_panel_sum", new ColumnMetadata()
                {
                    Name = "sweight_panel_sum",
                    DbName = "sweight_panel_sum",
                    DataType = "System.Double",
                    DbDataType = "float8",
                    NewDataType = "float8",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = null,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "N3",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "SWEIGHT_PANEL_SUM",
                    HeaderTextEn = "SWEIGHT_PANEL_SUM",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 9
                }
                }
            };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column stratum metadata
                /// </summary>
                public static readonly ColumnMetadata ColStratumId = Cols["stratum"];

                /// <summary>
                /// Column panel metadata
                /// </summary>
                public static readonly ColumnMetadata ColName = Cols["panel"];

                /// <summary>
                /// Column cluster_configuration metadata
                /// </summary>
                public static readonly ColumnMetadata ColClusterConfigurationId = Cols["cluster_configuration"];

                /// <summary>
                /// Column label metadata
                /// </summary>
                public static readonly ColumnMetadata ColLabel = Cols["label"];

                /// <summary>
                /// Column comment metadata
                /// </summary>
                public static readonly ColumnMetadata ColComment = Cols["comment"];

                /// <summary>
                /// Column cluster_count metadata
                /// </summary>
                public static readonly ColumnMetadata ColClusterCount = Cols["cluster_count"];

                /// <summary>
                /// Column plot_count metadata
                /// </summary>
                public static readonly ColumnMetadata ColPlotCount = Cols["plot_count"];

                /// <summary>
                /// Column panel_subset metadata
                /// </summary>
                public static readonly ColumnMetadata ColPanelSubsetId = Cols["panel_subset"];

                /// <summary>
                /// Column sweight_panel_sum metadata
                /// </summary>
                public static readonly ColumnMetadata ColSWeightPanelSum = Cols["sweight_panel_sum"];

                #endregion Constants


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public PanelList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public PanelList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// List of sampling panels (read-only)
                /// </summary>
                public List<Panel> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new Panel(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// Sampling panel from list by identifier (read-only)
                /// </summary>
                /// <param name="id">Sampling panel identifier</param>
                /// <returns>Sampling panel from list by identifier (null if not found)</returns>
                public Panel this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new Panel(composite: this, data: a))
                                .FirstOrDefault<Panel>();
                    }
                }

                #endregion Indexer


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public PanelList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new PanelList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new PanelList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                #endregion Methods

            }

        }
    }
}