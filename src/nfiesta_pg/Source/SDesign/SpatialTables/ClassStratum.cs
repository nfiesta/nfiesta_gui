﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace SDesign
        {
            // t_stratum

            /// <summary>
            /// Stratum
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class Stratum(
                StratumList composite = null,
                DataRow data = null)
                    : ASpatialTableEntry<StratumList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Stratum identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: StratumList.ColId.Name,
                            defaultValue: Int32.Parse(StratumList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: StratumList.ColId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Stratum name
                /// </summary>
                public string Name
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: StratumList.ColName.Name,
                            defaultValue: StratumList.ColName.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: StratumList.ColName.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Area of the stratum, [ha].
                /// </summary>
                public Nullable<double> Area
                {
                    get
                    {
                        return Functions.GetNDoubleArg(
                            row: Data,
                            name: StratumList.ColAreaHa.Name,
                            defaultValue: String.IsNullOrEmpty(StratumList.ColAreaHa.DefaultValue) ?
                                (Nullable<double>)null :
                                Double.Parse(s: StratumList.ColAreaHa.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNDoubleArg(
                            row: Data,
                            name: StratumList.ColAreaHa.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Commentary of the stratum
                /// </summary>
                public string Comment
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: StratumList.ColComment.Name,
                            defaultValue: StratumList.ColComment.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: StratumList.ColComment.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Multipolygon geometry of the stratum (format WKT)
                /// </summary>
                public string Geom
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: StratumList.ColGeom.Name,
                            defaultValue: StratumList.ColGeom.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: StratumList.ColGeom.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Aggregation of statas identifier
                /// </summary>
                public Nullable<int> StrataSetId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: StratumList.ColStrataSetId.Name,
                            defaultValue: String.IsNullOrEmpty(StratumList.ColStrataSetId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: StratumList.ColStrataSetId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: StratumList.ColStrataSetId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Aggregation of stratas object (read-only)
                /// </summary>
                public StrataSet StrataSet
                {
                    get
                    {
                        return (StrataSetId != null) ?
                             ((NfiEstaDB)Composite.Database).SDesign.TStrataSet[(int)StrataSetId] : null;
                    }
                }


                /// <summary>
                /// Stratum label
                /// </summary>
                public string Label
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: StratumList.ColLabel.Name,
                            defaultValue: StratumList.ColLabel.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: StratumList.ColLabel.Name,
                            val: value);
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true|false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not Stratum Type, return False
                    if (obj is not Stratum)
                    {
                        return false;
                    }

                    return ((Stratum)obj).Id == Id;
                }

                /// <summary>
                /// Returns a hash code for the current object
                /// </summary>
                /// <returns>Hash code for the current object</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                /// <summary>
                /// Returns a string that represents the current object
                /// </summary>
                /// <returns>String that represents the current object</returns>
                public override string ToString()
                {
                    return String.Concat(
                        $"{nameof(ClusterConfiguration)}: {{",
                        $"{nameof(Id)}: {Functions.PrepIntArg(arg: Id)}; ",
                        $"{nameof(Name)}: {Functions.PrepStringArg(arg: Name)}; ",
                        $"{nameof(Area)}: {Functions.PrepNDoubleArg(arg: Area)}; ",
                        $"{nameof(Comment)}: {Functions.PrepStringArg(arg: Comment)}; ",
                        $"{nameof(StrataSetId)}: {Functions.PrepNIntArg(arg: StrataSetId)}; ",
                        $"{nameof(Label)}: {Functions.PrepStringArg(arg: Label)}}}");
                }

                #endregion Methods

            }


            /// <summary>
            /// List of strata
            /// </summary>
            public class StratumList
                : ASpatialTable
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema = SDesignSchema.Name;

                /// <summary>
                /// Table name
                /// </summary>
                public const string Name = "t_stratum";

                /// <summary>
                /// Table alias
                /// </summary>
                public const string Alias = "t_stratum";

                /// <summary>
                /// Description in national language
                /// </summary>
                public const string CaptionCs = "Seznam strat";

                /// <summary>
                /// Description in English
                /// </summary>
                public const string CaptionEn = "List of strata";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
            {
                { "id", new ColumnMetadata()
                {
                    Name = "id",
                    DbName = "id",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = true,
                    NotNull = true,
                    DefaultValue = "0",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "ID",
                    HeaderTextEn = "ID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 0
                }
                },
                { "stratum", new ColumnMetadata()
                {
                    Name = "stratum",
                    DbName = "stratum",
                    DataType = "System.String",
                    DbDataType = "varchar",
                    NewDataType = "varchar",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = String.Empty,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "STRATUM",
                    HeaderTextEn = "STRATUM",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 1
                }
                },
                { "area_ha", new ColumnMetadata()
                {
                    Name = "area_ha",
                    DbName = "area_ha",
                    DataType = "System.Double",
                    DbDataType = "float8",
                    NewDataType = "float8",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = null,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "N3",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "AREA_HA",
                    HeaderTextEn = "AREA_HA",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 2
                }
                },
                { "comment", new ColumnMetadata()
                {
                    Name = "comment",
                    DbName = "comment",
                    DataType = "System.String",
                    DbDataType = "text",
                    NewDataType = "text",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = String.Empty,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "COMMENT",
                    HeaderTextEn = "COMMENT",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 3
                }
                },
                { "geom", new ColumnMetadata()
                {
                    Name = "geom",
                    DbName = "geom",
                    DataType = "System.String",
                    DbDataType = "geometry",
                    NewDataType = "bytea",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = String.Empty,
                    FuncCall = "ST_AsBinary({0})",
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "GEOM",
                    HeaderTextEn = "GEOM",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 4
                }
                },
                { "strata_set", new ColumnMetadata()
                {
                    Name = "strata_set",
                    DbName = "strata_set",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = null,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "STRATA_SET",
                    HeaderTextEn = "STRATA_SET",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 5
                }
                },
                { "label", new ColumnMetadata()
                {
                    Name = "label",
                    DbName = "label",
                    DataType = "System.String",
                    DbDataType = "varchar",
                    NewDataType = "varchar",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = String.Empty,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "LABEL",
                    HeaderTextEn = "LABEL",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 6
                }
                }
            };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column stratum metadata
                /// </summary>
                public static readonly ColumnMetadata ColName = Cols["stratum"];

                /// <summary>
                /// Column area_ha metadata
                /// </summary>
                public static readonly ColumnMetadata ColAreaHa = Cols["area_ha"];

                /// <summary>
                /// Column comment metadata
                /// </summary>
                public static readonly ColumnMetadata ColComment = Cols["comment"];

                /// <summary>
                /// Column geom metadata
                /// </summary>
                public static readonly ColumnMetadata ColGeom = Cols["geom"];

                /// <summary>
                /// Column strata_set metadata
                /// </summary>
                public static readonly ColumnMetadata ColStrataSetId = Cols["strata_set"];

                /// <summary>
                /// Column label metadata
                /// </summary>
                public static readonly ColumnMetadata ColLabel = Cols["label"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="withGeom">Download geometries from database as WKT?</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public StratumList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    bool withGeom = false,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            withGeom: withGeom,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="withGeom">Download geometries from database as WKT?</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public StratumList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    bool withGeom = false,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            withGeom: withGeom,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// List of strata (read-only)
                /// </summary>
                public List<Stratum> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new Stratum(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// Stratum from list by identifier (read-only)
                /// </summary>
                /// <param name="id">Stratum identifier</param>
                /// <returns>Stratum from list by identifier (null if not found)</returns>
                public Stratum this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new Stratum(composite: this, data: a))
                                .FirstOrDefault<Stratum>();
                    }
                }

                #endregion Indexer


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public StratumList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new StratumList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            withGeom: WithGeom,
                            loadingTime: LoadingTime) :
                        new StratumList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            withGeom: WithGeom,
                            loadingTime: null);
                }

                #endregion Methods

            }

        }
    }
}