﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace SDesign
        {
            // f_p_plot

            /// <summary>
            /// Inventory plot
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class Plot(
                PlotList composite = null,
                DataRow data = null)
                    : ASpatialTableEntry<PlotList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Inventory plot identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: PlotList.ColId.Name,
                            defaultValue: Int32.Parse(PlotList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: PlotList.ColId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Identifier of the inventory plot in stratum
                /// </summary>
                public string Pid
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: PlotList.ColPid.Name,
                            defaultValue: PlotList.ColPid.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: PlotList.ColPid.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Identifier of the plot within cluster
                /// </summary>
                public string PlotId
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: PlotList.ColPlotId.Name,
                            defaultValue: PlotList.ColPlotId.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: PlotList.ColPlotId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Point geometry of the inventory plot (format WKT)
                /// </summary>
                public string Geom
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: PlotList.ColGeom.Name,
                            defaultValue: PlotList.ColGeom.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: PlotList.ColGeom.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Cluster identifier
                /// </summary>
                public Nullable<int> ClusterId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: PlotList.ColClusterId.Name,
                            defaultValue: String.IsNullOrEmpty(value: PlotList.ColClusterId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: PlotList.ColClusterId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: PlotList.ColClusterId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Cluster object
                /// </summary>
                public Cluster Cluster
                {

                    get
                    {
                        return (ClusterId != null) ?
                             ((NfiEstaDB)Composite.Database).SDesign.TCluster[(int)ClusterId] : null;
                    }
                }


                /// <summary>
                /// Contains unprecised plot coordinates true/false
                /// </summary>
                public Nullable<bool> CoordinatesDegraded
                {
                    get
                    {
                        return Functions.GetNBoolArg(
                            row: Data,
                            name: PlotList.ColCoordinatesDegraded.Name,
                            defaultValue: String.IsNullOrEmpty(value: PlotList.ColCoordinatesDegraded.DefaultValue) ?
                                (Nullable<bool>)null :
                                Boolean.Parse(value: PlotList.ColCoordinatesDegraded.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNBoolArg(
                            row: Data,
                            name: PlotList.ColCoordinatesDegraded.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Comment for inventory plot
                /// </summary>
                public string Comment
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: PlotList.ColComment.Name,
                            defaultValue: PlotList.ColComment.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: PlotList.ColComment.Name,
                            val: value);
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true|false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not Plot Type, return False
                    if (obj is not Plot)
                    {
                        return false;
                    }

                    return ((Plot)obj).Id == Id;
                }

                /// <summary>
                /// Returns a hash code for the current object
                /// </summary>
                /// <returns>Hash code for the current object</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                /// <summary>
                /// Returns a string that represents the current object
                /// </summary>
                /// <returns>String that represents the current object</returns>
                public override string ToString()
                {
                    return String.Concat(
                        $"{nameof(ClusterConfiguration)}: {{",
                        $"{nameof(Id)}: {Functions.PrepIntArg(arg: Id)}; ",
                        $"{nameof(Pid)}: {Functions.PrepStringArg(arg: Pid)}; ",
                        $"{nameof(PlotId)}: {Functions.PrepStringArg(arg: PlotId)}; ",
                        $"{nameof(ClusterId)}: {Functions.PrepNIntArg(arg: ClusterId)}; ",
                        $"{nameof(CoordinatesDegraded)}: {Functions.PrepNBoolArg(arg: CoordinatesDegraded)}; ",
                        $"{nameof(Comment)}: {Functions.PrepStringArg(arg: Comment)}}}");
                }

                #endregion Methods

            }


            /// <summary>
            /// List of inventory plots
            /// </summary>
            public class PlotList
                : ASpatialTable
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema = SDesignSchema.Name;

                /// <summary>
                /// Table name
                /// </summary>
                public const string Name = "f_p_plot";

                /// <summary>
                /// Table alias
                /// </summary>
                public const string Alias = "f_p_plot";

                /// <summary>
                /// Description in national language
                /// </summary>
                public const string CaptionCs = "Seznam inventarizačních ploch";

                /// <summary>
                /// Description in English
                /// </summary>
                public const string CaptionEn = "List of inventory plots";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
            {
                { "gid", new ColumnMetadata()
                {
                    Name = "gid",
                    DbName = "gid",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = true,
                    NotNull = true,
                    DefaultValue = "0",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "GID",
                    HeaderTextEn = "GID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 0
                }
                },
                { "pid", new ColumnMetadata()
                {
                    Name = "pid",
                    DbName = "pid",
                    DataType = "System.String",
                    DbDataType = "varchar",
                    NewDataType = "varchar",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = String.Empty,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "PID",
                    HeaderTextEn = "PID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 1
                }
                },
                { "plot", new ColumnMetadata()
                {
                    Name = "plot",
                    DbName = "plot",
                    DataType = "System.String",
                    DbDataType = "varchar",
                    NewDataType = "varchar",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = String.Empty,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "PLOT",
                    HeaderTextEn = "PLOT",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 2
                }
                },
                { "geom", new ColumnMetadata()
                {
                    Name = "geom",
                    DbName = "geom",
                    DataType = "System.String",
                    DbDataType = "geometry",
                    NewDataType = "bytea",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = String.Empty,
                    FuncCall = "ST_AsBinary({0})",
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "GEOM",
                    HeaderTextEn = "GEOM",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 3
                }
                },
                { "cluster", new ColumnMetadata()
                {
                    Name = "cluster",
                    DbName = "cluster",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = null,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "CLUSTER",
                    HeaderTextEn = "CLUSTER",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 4
                }
                },
                { "coordinates_degraded", new ColumnMetadata()
                {
                    Name = "coordinates_degraded",
                    DbName = "coordinates_degraded",
                    DataType = "System.Boolean",
                    DbDataType = "bool",
                    NewDataType = "bool",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = null,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "COORDINATES_DEGRADED",
                    HeaderTextEn = "COORDINATES_DEGRADED",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 5
                }
                },
                { "comment", new ColumnMetadata()
                {
                    Name = "comment",
                    DbName = "comment",
                    DataType = "System.String",
                    DbDataType = "text",
                    NewDataType = "text",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = "",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "COMMENT",
                    HeaderTextEn = "COMMENT",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 6
                }
                }
            };

                /// <summary>
                /// Column gid metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["gid"];

                /// <summary>
                /// Column pid metadata
                /// </summary>
                public static readonly ColumnMetadata ColPid = Cols["pid"];

                /// <summary>
                /// Column plot metadata
                /// </summary>
                public static readonly ColumnMetadata ColPlotId = Cols["plot"];

                /// <summary>
                /// Column geom metadata
                /// </summary>
                public static readonly ColumnMetadata ColGeom = Cols["geom"];

                /// <summary>
                /// Column cluster metadata
                /// </summary>
                public static readonly ColumnMetadata ColClusterId = Cols["cluster"];

                /// <summary>
                /// Column coordinates_degraded metadata
                /// </summary>
                public static readonly ColumnMetadata ColCoordinatesDegraded = Cols["coordinates_degraded"];

                /// <summary>
                /// Column comment metadata
                /// </summary>
                public static readonly ColumnMetadata ColComment = Cols["comment"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="withGeom">Download geometries from database as WKT?</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public PlotList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    bool withGeom = false,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            withGeom: withGeom,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="withGeom">Download geometries from database as WKT?</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public PlotList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    bool withGeom = false,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            withGeom: withGeom,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// List of inventory plots (read-only)
                /// </summary>
                public List<Plot> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new Plot(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// Inventory plot from list by identifier (read-only)
                /// </summary>
                /// <param name="id">Inventory plot identifier</param>
                /// <returns>Inventory plot from list by identifier (null if not found)</returns>
                public Plot this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new Plot(composite: this, data: a))
                                .FirstOrDefault<Plot>();
                    }
                }

                #endregion Indexer


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public PlotList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new PlotList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            withGeom: WithGeom,
                            loadingTime: LoadingTime) :
                        new PlotList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            withGeom: WithGeom,
                            loadingTime: null);
                }

                #endregion Methods

            }

        }
    }
}
