﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace SDesign
        {
            // t_cluster_configuration

            /// <summary>
            /// Sampling cluster configuration
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class ClusterConfiguration(
                ClusterConfigurationList composite = null,
                DataRow data = null)
                    : ASpatialTableEntry<ClusterConfigurationList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Sampling cluster configuration identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: ClusterConfigurationList.ColId.Name,
                            defaultValue: Int32.Parse(s: ClusterConfigurationList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: ClusterConfigurationList.ColId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Sampling cluster configuration name
                /// </summary>
                public string Name
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: ClusterConfigurationList.ColName.Name,
                            defaultValue: ClusterConfigurationList.ColName.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: ClusterConfigurationList.ColName.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Boolean value coding if clusters are used
                /// </summary>
                public bool ClusterDesign
                {
                    get
                    {
                        return Functions.GetBoolArg(
                            row: Data,
                            name: ClusterConfigurationList.ColClusterDesign.Name,
                            defaultValue: Boolean.Parse(value: ClusterConfigurationList.ColClusterDesign.DefaultValue));
                    }
                    set
                    {
                        Functions.SetBoolArg(
                            row: Data,
                            name: ClusterConfigurationList.ColClusterDesign.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Boolean value coding if there is random rotation of plots
                /// </summary>
                public Nullable<bool> ClusterRotation
                {
                    get
                    {
                        return Functions.GetNBoolArg(
                            row: Data,
                            name: ClusterConfigurationList.ColClusterRotation.Name,
                            defaultValue: String.IsNullOrEmpty(value: ClusterConfigurationList.ColClusterRotation.DefaultValue) ?
                                (Nullable<bool>)null :
                                Boolean.Parse(value: ClusterConfigurationList.ColClusterRotation.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNBoolArg(
                            row: Data,
                            name: ClusterConfigurationList.ColClusterRotation.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// MultiPoint geometry of the sampling cluster (format WKT)
                /// </summary>
                public string Geom
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: ClusterConfigurationList.ColGeom.Name,
                            defaultValue: ClusterConfigurationList.ColGeom.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: ClusterConfigurationList.ColGeom.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Nominal (and fixed) number of sample plots in each cluster
                /// </summary>
                public int PlotsPerCluster
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: ClusterConfigurationList.ColPlotsPerCluster.Name,
                            defaultValue: Int32.Parse(s: ClusterConfigurationList.ColPlotsPerCluster.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: ClusterConfigurationList.ColPlotsPerCluster.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Label of sampling cluster configuration
                /// </summary>
                public string Label
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: ClusterConfigurationList.ColLabel.Name,
                            defaultValue: ClusterConfigurationList.ColLabel.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: ClusterConfigurationList.ColLabel.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Commentary of sampling cluster configuration
                /// </summary>
                public string Comment
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: ClusterConfigurationList.ColComment.Name,
                            defaultValue: ClusterConfigurationList.ColComment.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: ClusterConfigurationList.ColComment.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Multipolygon geometry of the buffered stratum (format WKT)
                /// </summary>
                public string GeomBuffered
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: ClusterConfigurationList.ColStratumGeomBuffered.Name,
                            defaultValue: ClusterConfigurationList.ColStratumGeomBuffered.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: ClusterConfigurationList.ColStratumGeomBuffered.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Area of the buffered stratum (in case it is buffered), [ha].
                /// </summary>
                public Nullable<double> FrameAreaHa
                {
                    get
                    {
                        return Functions.GetNDoubleArg(
                            row: Data,
                            name: ClusterConfigurationList.ColFrameAreaHa.Name,
                            defaultValue: String.IsNullOrEmpty(ClusterConfigurationList.ColFrameAreaHa.DefaultValue) ?
                                (Nullable<double>)null :
                                Double.Parse(s: ClusterConfigurationList.ColFrameAreaHa.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNDoubleArg(
                            row: Data,
                            name: ClusterConfigurationList.ColFrameAreaHa.Name,
                            val: value);
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true|false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not ClusterConfiguration Type, return False
                    if (obj is not ClusterConfiguration)
                    {
                        return false;
                    }

                    return ((ClusterConfiguration)obj).Id == Id;
                }

                /// <summary>
                /// Returns a hash code for the current object
                /// </summary>
                /// <returns>Hash code for the current object</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                /// <summary>
                /// Returns a string that represents the current object
                /// </summary>
                /// <returns>String that represents the current object</returns>
                public override string ToString()
                {
                    return String.Concat(
                        $"{nameof(ClusterConfiguration)}: {{",
                        $"{nameof(Id)}: {Functions.PrepIntArg(arg: Id)}; ",
                        $"{nameof(Name)}: {Functions.PrepStringArg(arg: Name)}; ",
                        $"{nameof(ClusterDesign)}: {Functions.PrepBoolArg(arg: ClusterDesign)}; ",
                        $"{nameof(ClusterRotation)}: {Functions.PrepNBoolArg(arg: ClusterRotation)}; ",
                        $"{nameof(PlotsPerCluster)}: {Functions.PrepIntArg(arg: PlotsPerCluster)}; ",
                        $"{nameof(Label)}: {Functions.PrepStringArg(arg: Label)}; ",
                        $"{nameof(Comment)}: {Functions.PrepStringArg(arg: Comment)}; ",
                        $"{nameof(FrameAreaHa)}: {Functions.PrepNDoubleArg(arg: FrameAreaHa)}}}");
                }

                #endregion Methods

            }


            /// <summary>
            /// List of sampling cluster configurations
            /// </summary>
            public class ClusterConfigurationList
                : ASpatialTable
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema = SDesignSchema.Name;

                /// <summary>
                /// Table name
                /// </summary>
                public const string Name = "t_cluster_configuration";

                /// <summary>
                /// Table alias
                /// </summary>
                public const string Alias = "t_cluster_configuration";

                /// <summary>
                /// Description in national language
                /// </summary>
                public const string CaptionCs = "Seznam konfigurací výběrového klastru";

                /// <summary>
                /// Description in English
                /// </summary>
                public const string CaptionEn = "List of sampling cluster configurations";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
            {
                { "id", new ColumnMetadata()
                {
                    Name = "id",
                    DbName = "id",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = true,
                    NotNull = true,
                    DefaultValue = "0",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "ID",
                    HeaderTextEn = "ID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 0
                }
                },
                { "cluster_configuration", new ColumnMetadata()
                {
                    Name = "cluster_configuration",
                    DbName = "cluster_configuration",
                    DataType = "System.String",
                    DbDataType = "varchar",
                    NewDataType = "varchar",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = String.Empty,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat =  String.Empty,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "CLUSTER_CONFIGURATION",
                    HeaderTextEn = "CLUSTER_CONFIGURATION",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 1
                }
                },
                { "cluster_design", new ColumnMetadata()
                {
                    Name = "cluster_design",
                    DbName = "cluster_design",
                    DataType = "System.Boolean",
                    DbDataType = "bool",
                    NewDataType = "bool",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = "false",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat =  String.Empty,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "CLUSTER_DESIGN",
                    HeaderTextEn = "CLUSTER_DESIGN",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 2
                }
                },
                { "cluster_rotation", new ColumnMetadata()
                {
                    Name = "cluster_rotation",
                    DbName = "cluster_rotation",
                    DataType = "System.Boolean",
                    DbDataType = "bool",
                    NewDataType = "bool",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = null,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat =  String.Empty,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "CLUSTER_ROTATION",
                    HeaderTextEn = "CLUSTER_ROTATION",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 3
                }
                },
                { "geom", new ColumnMetadata()
                {
                    Name = "geom",
                    DbName = "geom",
                    DataType = "System.String",
                    DbDataType = "geometry",
                    NewDataType = "bytea",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = String.Empty,
                    FuncCall = "ST_AsBinary({0})",
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "GEOM",
                    HeaderTextEn = "GEOM",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 4
                }
                },
                { "plots_per_cluster", new ColumnMetadata()
                {
                    Name = "plots_per_cluster",
                    DbName = "plots_per_cluster",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = "0",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "PLOTS_PER_CLUSTER",
                    HeaderTextEn = "PLOTS_PER_CLUSTER",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 5
                }
                },
                { "label", new ColumnMetadata()
                {
                    Name = "label",
                    DbName = "label",
                    DataType = "System.String",
                    DbDataType = "varchar",
                    NewDataType = "varchar",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = String.Empty,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "LABEL",
                    HeaderTextEn = "LABEL",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 6
                }
                },
                { "comment", new ColumnMetadata()
                {
                    Name = "comment",
                    DbName = "comment",
                    DataType = "System.String",
                    DbDataType = "text",
                    NewDataType = "text",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = String.Empty,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "COMMENT",
                    HeaderTextEn = "COMMENT",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 7
                }
                },
                { "stratum_geom_buffered", new ColumnMetadata()
                {
                    Name = "stratum_geom_buffered",
                    DbName = "stratum_geom_buffered",
                    DataType = "System.String",
                    DbDataType = "geometry",
                    NewDataType = "bytea",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = null,
                    FuncCall = "ST_AsBinary({0})",
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "STRATUM_GEOM_BUFFERED",
                    HeaderTextEn = "STRATUM_GEOM_BUFFERED",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 8
                }
                },
                { "frame_area_ha", new ColumnMetadata()
                {
                    Name = "frame_area_ha",
                    DbName = "frame_area_ha",
                    DataType = "System.Double",
                    DbDataType = "float8",
                    NewDataType = "float8",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = null,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "N3",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "FRAME_AREA_HA",
                    HeaderTextEn = "FRAME_AREA_HA",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 9
                }
                }
            };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column cluster_configuration metadata
                /// </summary>
                public static readonly ColumnMetadata ColName = Cols["cluster_configuration"];

                /// <summary>
                /// Column cluster_design metadata
                /// </summary>
                public static readonly ColumnMetadata ColClusterDesign = Cols["cluster_design"];

                /// <summary>
                /// Column cluster_rotation metadata
                /// </summary>
                public static readonly ColumnMetadata ColClusterRotation = Cols["cluster_rotation"];

                /// <summary>
                /// Column geom metadata
                /// </summary>
                public static readonly ColumnMetadata ColGeom = Cols["geom"];

                /// <summary>
                /// Column plots_per_cluster metadata
                /// </summary>
                public static readonly ColumnMetadata ColPlotsPerCluster = Cols["plots_per_cluster"];

                /// <summary>
                /// Column label metadata
                /// </summary>
                public static readonly ColumnMetadata ColLabel = Cols["label"];

                /// <summary>
                /// Column comment metadata
                /// </summary>
                public static readonly ColumnMetadata ColComment = Cols["comment"];

                /// <summary>
                /// Column stratum_geom_buffered metadata
                /// </summary>
                public static readonly ColumnMetadata ColStratumGeomBuffered = Cols["stratum_geom_buffered"];

                /// <summary>
                /// Column frame_area_ha metadata
                /// </summary>
                public static readonly ColumnMetadata ColFrameAreaHa = Cols["frame_area_ha"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="withGeom">Download geometries from database as WKT?</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public ClusterConfigurationList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    bool withGeom = false,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            withGeom: withGeom,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="withGeom">Download geometries from database as WKT?</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public ClusterConfigurationList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    bool withGeom = false,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            withGeom: withGeom,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// List of sampling cluster configurations (read-only)
                /// </summary>
                public List<ClusterConfiguration> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new ClusterConfiguration(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// Sampling cluster configuration from list by identifier (read-only)
                /// </summary>
                /// <param name="id">Sampling cluster configuration identifier</param>
                /// <returns>Sampling cluster configuration from list by identifier (null if not found)</returns>
                public ClusterConfiguration this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new ClusterConfiguration(composite: this, data: a))
                                .FirstOrDefault<ClusterConfiguration>();
                    }
                }

                #endregion Indexer


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public ClusterConfigurationList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new ClusterConfigurationList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            withGeom: WithGeom,
                            loadingTime: LoadingTime) :
                        new ClusterConfigurationList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            withGeom: WithGeom,
                            loadingTime: null);
                }

                #endregion Methods

            }

        }
    }
}
