﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace AuxiliaryData
        {
            // f_p_plot

            /// <summary>
            /// Inventory plot
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class Plot(
                PlotList composite = null,
                DataRow data = null)
                    : ASpatialTableEntry<PlotList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Inventory plot identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: PlotList.ColId.Name,
                            defaultValue: Int32.Parse(s: PlotList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: PlotList.ColId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Configuration collection identifier
                /// </summary>
                public int ConfigCollectionId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: PlotList.ColConfigCollectionId.Name,
                            defaultValue: Int32.Parse(s: PlotList.ColConfigCollectionId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: PlotList.ColConfigCollectionId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Configuration collection object (read-only)
                /// </summary>
                public ConfigCollection ConfigCollection
                {
                    get
                    {
                        return ((NfiEstaDB)Composite.Database).SAuxiliaryData.TConfigCollection[ConfigCollectionId];
                    }
                }


                /// <summary>
                /// Country identifier
                /// </summary>
                public string Country
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: PlotList.ColCountry.Name,
                            defaultValue: PlotList.ColCountry.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: PlotList.ColCountry.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Aggregation of stratas label
                /// </summary>
                public string StrataSet
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: PlotList.ColStrataSet.Name,
                            defaultValue: PlotList.ColStrataSet.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: PlotList.ColStrataSet.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Stratum label
                /// </summary>
                public string Stratum
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: PlotList.ColStratum.Name,
                            defaultValue: PlotList.ColStratum.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: PlotList.ColStratum.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Sampling panel label
                /// </summary>
                public string Panel
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: PlotList.ColPanel.Name,
                            defaultValue: PlotList.ColPanel.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: PlotList.ColPanel.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Sampling cluster identifer
                /// </summary>
                public string Cluster
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: PlotList.ColCluster.Name,
                            defaultValue: PlotList.ColCluster.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: PlotList.ColCluster.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Inventory plot identifier
                /// </summary>
                public string PlotId
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: PlotList.ColPlot.Name,
                            defaultValue: PlotList.ColPlot.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: PlotList.ColPlot.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Point geometry of the inventory plot (format WKT)
                /// </summary>
                public string Geom
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: PlotList.ColGeom.Name,
                            defaultValue: PlotList.ColGeom.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: PlotList.ColGeom.Name,
                            val: value);
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not Plot Type, return False
                    if (obj is not Plot)
                    {
                        return false;
                    }

                    return
                        Id == ((Plot)obj).Id;
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                #endregion Methods

            }


            /// <summary>
            /// List of inventory plots
            /// </summary>
            public class PlotList
                : ASpatialTable
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema = ADSchema.Name;

                /// <summary>
                /// Table name
                /// </summary>
                public const string Name = "f_p_plot";

                /// <summary>
                /// Table alias
                /// </summary>
                public const string Alias = "f_p_plot";

                /// <summary>
                /// Description in national language
                /// </summary>
                public const string CaptionCs = "Seznam inventarizačních ploch";

                /// <summary>
                /// Description in English
                /// </summary>
                public const string CaptionEn = "List of inventory plots";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
            {
                { "gid", new ColumnMetadata()
                {
                    Name = "gid",
                    DbName = "gid",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = true,
                    NotNull = true,
                    DefaultValue = "0",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "GID",
                    HeaderTextEn = "GID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 0
                }
                },
                { "config_collection_id", new ColumnMetadata()
                {
                    Name = "config_collection_id",
                    DbName = "config_collection",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = "0",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "CONFIG_COLLECTION_ID",
                    HeaderTextEn = "CONFIG_COLLECTION_ID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 1
                }
                },
                { "country", new ColumnMetadata()
                {
                    Name = "country",
                    DbName = "country",
                    DataType = "System.String",
                    DbDataType = "varchar",
                    NewDataType = "varchar",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = null,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "COUNTRY",
                    HeaderTextEn = "COUNTRY",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 2
                }
                },
                { "strata_set", new ColumnMetadata()
                {
                    Name = "strata_set",
                    DbName = "strata_set",
                    DataType = "System.String",
                    DbDataType = "varchar",
                    NewDataType = "varchar",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = null,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "STRATA_SET",
                    HeaderTextEn = "STRATA_SET",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 3
                }
                },
                { "stratum", new ColumnMetadata()
                {
                    Name = "stratum",
                    DbName = "stratum",
                    DataType = "System.String",
                    DbDataType = "varchar",
                    NewDataType = "varchar",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = null,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "STRATUM",
                    HeaderTextEn = "STRATUM",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 4
                }
                },
                { "panel", new ColumnMetadata()
                {
                    Name = "panel",
                    DbName = "panel",
                    DataType = "System.String",
                    DbDataType = "varchar",
                    NewDataType = "varchar",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = null,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "PANEL",
                    HeaderTextEn = "PANEL",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 5
                }
                },
                { "cluster", new ColumnMetadata()
                {
                    Name = "cluster",
                    DbName = "cluster",
                    DataType = "System.String",
                    DbDataType = "varchar",
                    NewDataType = "varchar",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = null,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "CLUSTER",
                    HeaderTextEn = "CLUSTER",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 6
                }
                },
                { "plot", new ColumnMetadata()
                {
                    Name = "plot",
                    DbName = "plot",
                    DataType = "System.String",
                    DbDataType = "varchar",
                    NewDataType = "varchar",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = String.Empty,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "PLOT",
                    HeaderTextEn = "PLOT",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 7
                }
                },
                { "geom", new ColumnMetadata()
                {
                    Name = "geom",
                    DbName = "geom",
                    DataType = "System.String",
                    DbDataType = "geometry",
                    NewDataType = "bytea",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = String.Empty,
                    FuncCall = "ST_AsBinary({0})",
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "GEOM",
                    HeaderTextEn = "GEOM",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 8
                }
                }
            };

                /// <summary>
                /// Column gid metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["gid"];

                /// <summary>
                /// Column config_collection_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColConfigCollectionId = Cols["config_collection_id"];

                /// <summary>
                /// Column country metadata
                /// </summary>
                public static readonly ColumnMetadata ColCountry = Cols["country"];

                /// <summary>
                /// Column strata_set metadata
                /// </summary>
                public static readonly ColumnMetadata ColStrataSet = Cols["strata_set"];

                /// <summary>
                /// Column stratum metadata
                /// </summary>
                public static readonly ColumnMetadata ColStratum = Cols["stratum"];

                /// <summary>
                /// Column panel metadata
                /// </summary>
                public static readonly ColumnMetadata ColPanel = Cols["panel"];

                /// <summary>
                /// Column cluster metadata
                /// </summary>
                public static readonly ColumnMetadata ColCluster = Cols["cluster"];

                /// <summary>
                /// Column plot metadata
                /// </summary>
                public static readonly ColumnMetadata ColPlot = Cols["plot"];

                /// <summary>
                /// Column geom metadata
                /// </summary>
                public static readonly ColumnMetadata ColGeom = Cols["geom"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="withGeom">Download geometries from database as WKT?</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public PlotList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    bool withGeom = false,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            withGeom: withGeom,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="withGeom">Download geometries from database as WKT?</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public PlotList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    bool withGeom = false,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            withGeom: withGeom,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// List of inventory plots (read-only)
                /// </summary>
                public List<Plot> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new Plot(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// Inventory plot from list by identifier (read-only)
                /// </summary>
                /// <param name="id">Inventory plot identifier</param>
                /// <returns>Inventory plot from list by identifier (null if not found)</returns>
                public Plot this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new Plot(composite: this, data: a))
                                .FirstOrDefault<Plot>();
                    }
                }

                #endregion Indexer


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public PlotList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new PlotList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            withGeom: WithGeom,
                            loadingTime: LoadingTime) :
                        new PlotList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            withGeom: WithGeom,
                            loadingTime: null);
                }

                #endregion Methods

            }

        }
    }
}