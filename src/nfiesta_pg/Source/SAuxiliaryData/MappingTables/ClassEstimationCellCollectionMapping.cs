﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace AuxiliaryData
        {
            // cm_estimation_cell_collection

            /// <summary>
            /// Mapping between estimation cell collections
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class EstimationCellCollectionMapping(
                EstimationCellCollectionMappingList composite = null,
                DataRow data = null)
                    : AMappingTableEntry<EstimationCellCollectionMappingList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Mapping between estimation cell collections identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: EstimationCellCollectionMappingList.ColId.Name,
                            defaultValue: Int32.Parse(s: EstimationCellCollectionMappingList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: EstimationCellCollectionMappingList.ColId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Estimation cell collection identifier
                /// </summary>
                public int EstimationCellCollectionId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: EstimationCellCollectionMappingList.ColEstimationCellCollectionId.Name,
                            defaultValue: Int32.Parse(s: EstimationCellCollectionMappingList.ColEstimationCellCollectionId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: EstimationCellCollectionMappingList.ColEstimationCellCollectionId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Estimation cell collection object
                /// </summary>
                public EstimationCellCollection EstimationCellCollection
                {
                    get
                    {
                        return ((NfiEstaDB)Composite.Database).SAuxiliaryData.CEstimationCellCollection[EstimationCellCollectionId];
                    }
                }



                /// <summary>
                /// Inferior estimation cell collection identifier
                /// </summary>
                public int EstimationCellCollectionLowestId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: EstimationCellCollectionMappingList.ColEstimationCellCollectionLowestId.Name,
                            defaultValue: Int32.Parse(s: EstimationCellCollectionMappingList.ColEstimationCellCollectionLowestId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: EstimationCellCollectionMappingList.ColEstimationCellCollectionLowestId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Inferior estimation cell collection object
                /// </summary>
                public EstimationCellCollection InferiorEstimationCellCollection
                {
                    get
                    {
                        return ((NfiEstaDB)Composite.Database).SAuxiliaryData.CEstimationCellCollection[EstimationCellCollectionLowestId];
                    }
                }


                /// <summary>
                /// Superior estimation cell collection identifier
                /// </summary>
                public Nullable<int> EstimationCellCollectionHighestId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: EstimationCellCollectionMappingList.ColEstimationCellCollectionHighestId.Name,
                            defaultValue: String.IsNullOrEmpty(value: EstimationCellCollectionMappingList.ColEstimationCellCollectionHighestId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: EstimationCellCollectionMappingList.ColEstimationCellCollectionHighestId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: EstimationCellCollectionMappingList.ColEstimationCellCollectionHighestId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Inferior estimation cell collection object
                /// </summary>
                public EstimationCellCollection SuperiorEstimationCellCollection
                {
                    get
                    {
                        return (EstimationCellCollectionHighestId != null) ?
                             ((NfiEstaDB)Composite.Database).SAuxiliaryData.CEstimationCellCollection[(int)EstimationCellCollectionHighestId] : null;
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not EstimationCellCollectionMapping Type, return False
                    if (obj is not EstimationCellCollectionMapping)
                    {
                        return false;
                    }

                    return
                        Id == ((EstimationCellCollectionMapping)obj).Id;
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                #endregion Methods

            }


            /// <summary>
            /// List of mappings between estimation cell collections
            /// </summary>
            public class EstimationCellCollectionMappingList
                : AMappingTable
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema = ADSchema.Name;

                /// <summary>
                /// Table name
                /// </summary>
                public const string Name = "cm_estimation_cell_collection";

                /// <summary>
                /// Table alias
                /// </summary>
                public const string Alias = "cm_estimation_cell_collection";

                /// <summary>
                /// Description in national language
                /// </summary>
                public const string CaptionCs = "Seznam mapování mezi skupinami výpočetních buněk";

                /// <summary>
                /// Description in English
                /// </summary>
                public const string CaptionEn = "List of mappings between estimation cell collections";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
            {
                { "id", new ColumnMetadata()
                {
                    Name = "id",
                    DbName = "id",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = true,
                    NotNull = true,
                    DefaultValue = "0",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "ID",
                    HeaderTextEn = "ID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 0
                }
                },
                { "estimation_cell_collection_id", new ColumnMetadata()
                {
                    Name = "estimation_cell_collection_id",
                    DbName = "estimation_cell_collection",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = "0",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "ESTIMATION_CELL_COLLECTION_ID",
                    HeaderTextEn = "ESTIMATION_CELL_COLLECTION_ID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 1
                }
                },
                { "estimation_cell_collection_lowest_id", new ColumnMetadata()
                {
                    Name = "estimation_cell_collection_lowest_id",
                    DbName = "estimation_cell_collection_lowest",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = "0",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "ESTIMATION_CELL_COLLECTION_LOWEST_ID",
                    HeaderTextEn = "ESTIMATION_CELL_COLLECTION_LOWEST_ID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 2
                }
                },
                { "estimation_cell_collection_highest_id", new ColumnMetadata()
                {
                    Name = "estimation_cell_collection_highest_id",
                    DbName = "estimation_cell_collection_highest",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = null,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "ESTIMATION_CELL_COLLECTION_HIGHEST_ID",
                    HeaderTextEn = "ESTIMATION_CELL_COLLECTION_HIGHEST_ID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 3
                }
                }
            };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column estimation_cell_collection_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationCellCollectionId = Cols["estimation_cell_collection_id"];

                /// <summary>
                /// Column estimation_cell_collection_lowest_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationCellCollectionLowestId = Cols["estimation_cell_collection_lowest_id"];

                /// <summary>
                /// Column estimation_cell_collection_highest_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationCellCollectionHighestId = Cols["estimation_cell_collection_highest_id"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public EstimationCellCollectionMappingList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public EstimationCellCollectionMappingList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// List of mappings between estimation cell collections (read-only)
                /// </summary>
                public List<EstimationCellCollectionMapping> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new EstimationCellCollectionMapping(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// Mapping between estimation cell collections from list by identifier (read-only)
                /// </summary>
                /// <param name="id">Mapping between estimation cell collections identifier</param>
                /// <returns>Mapping between estimation cell collections from list by identifier (null if not found)</returns>
                public EstimationCellCollectionMapping this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new EstimationCellCollectionMapping(composite: this, data: a))
                                .FirstOrDefault<EstimationCellCollectionMapping>();
                    }
                }

                #endregion Indexer


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public EstimationCellCollectionMappingList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new EstimationCellCollectionMappingList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new EstimationCellCollectionMappingList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                #endregion Methods

            }

        }
    }
}