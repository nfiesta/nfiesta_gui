﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace AuxiliaryData
        {
            // cm_ext_config_function

            /// <summary>
            /// Mapping between extension version and configuration function
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class ExtensionToConfigFunction(
                ExtensionToConfigFunctionList composite = null,
                DataRow data = null)
                    : AMappingTableEntry<ExtensionToConfigFunctionList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Mapping between extension version and configuration function identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: ExtensionToConfigFunctionList.ColId.Name,
                            defaultValue: Int32.Parse(s: ExtensionToConfigFunctionList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: ExtensionToConfigFunctionList.ColId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Configuration function identifier
                /// </summary>
                public int ConfigFunctionId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: ExtensionToConfigFunctionList.ColConfigFunctionId.Name,
                            defaultValue: Int32.Parse(s: ExtensionToConfigFunctionList.ColConfigFunctionId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: ExtensionToConfigFunctionList.ColConfigFunctionId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Configuration function object
                /// </summary>
                public ConfigFunction ConfigFunction
                {
                    get
                    {
                        return ((NfiEstaDB)Composite.Database).SAuxiliaryData.CConfigFunction[ConfigFunctionId];
                    }
                }


                /// <summary>
                /// Extension version identifier
                /// </summary>
                public int ExtensionVersionValidFromId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: ExtensionToConfigFunctionList.ColExtensionVersionValidFromId.Name,
                            defaultValue: Int32.Parse(s: ExtensionToConfigFunctionList.ColExtensionVersionValidFromId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: ExtensionToConfigFunctionList.ColExtensionVersionValidFromId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Extension version object
                /// </summary>
                public ExtensionVersion ExtensionVersionValidFrom
                {
                    get
                    {
                        return ((NfiEstaDB)Composite.Database).SAuxiliaryData.CExtensionVersion[ExtensionVersionValidFromId];
                    }
                }


                /// <summary>
                /// Extension version identifier
                /// </summary>
                public int ExtensionVersionValidUntilId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: ExtensionToConfigFunctionList.ColExtensionVersionValidUntilId.Name,
                            defaultValue: Int32.Parse(s: ExtensionToConfigFunctionList.ColExtensionVersionValidUntilId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: ExtensionToConfigFunctionList.ColExtensionVersionValidUntilId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Extension version object
                /// </summary>
                public ExtensionVersion ExtensionVersionValidUntil
                {
                    get
                    {
                        return ((NfiEstaDB)Composite.Database).SAuxiliaryData.CExtensionVersion[ExtensionVersionValidUntilId];
                    }
                }


                /// <summary>
                /// Active
                /// </summary>
                public bool Active
                {
                    get
                    {
                        return Functions.GetBoolArg(
                            row: Data,
                            name: ExtensionToConfigFunctionList.ColActive.Name,
                            defaultValue: Boolean.Parse(value: ExtensionToConfigFunctionList.ColActive.DefaultValue));
                    }
                    set
                    {
                        Functions.SetBoolArg(
                            row: Data,
                            name: ExtensionToConfigFunctionList.ColActive.Name,
                            val: value);
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not ExtensionToConfigFunction Type, return False
                    if (obj is not ExtensionToConfigFunction)
                    {
                        return false;
                    }

                    return
                        Id == ((ExtensionToConfigFunction)obj).Id;
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                #endregion Methods

            }


            /// <summary>
            /// List of mappings between extension version and configuration function
            /// </summary>
            public class ExtensionToConfigFunctionList
                : AMappingTable
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema = ADSchema.Name;

                /// <summary>
                /// Table name
                /// </summary>
                public const string Name = "cm_ext_config_function";

                /// <summary>
                /// Table alias
                /// </summary>
                public const string Alias = "cm_ext_config_function";

                /// <summary>
                /// Description in national language
                /// </summary>
                public const string CaptionCs = "Seznam mapování mezi verzí databázové extenze a funkcí pro výpočet úhrnů pomocných proměnných";

                /// <summary>
                /// Description in English
                /// </summary>
                public const string CaptionEn = "List of mappings between extension version and configuration function";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
            {
                { "id", new ColumnMetadata()
                {
                    Name = "id",
                    DbName = "id",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = true,
                    NotNull = true,
                    DefaultValue = "0",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "ID",
                    HeaderTextEn = "ID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 0
                }
                },
                { "config_function_id", new ColumnMetadata()
                {
                    Name = "config_function_id",
                    DbName = "config_function",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = "0",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "CONFIG_FUNCTION_ID",
                    HeaderTextEn = "CONFIG_FUNCTION_ID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 1
                }
                },
                { "ext_version_valid_from_id", new ColumnMetadata()
                {
                    Name = "ext_version_valid_from_id",
                    DbName = "ext_version_valid_from",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = "0",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "EXT_VERSION_VALID_FROM_ID",
                    HeaderTextEn = "EXT_VERSION_VALID_FROM_ID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 2
                }
                },
                { "ext_version_valid_until_id", new ColumnMetadata()
                {
                    Name = "ext_version_valid_until_id",
                    DbName = "ext_version_valid_until",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = "0",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "EXT_VERSION_VALID_UNTIL_ID",
                    HeaderTextEn = "EXT_VERSION_VALID_UNTIL_ID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 3
                }
                },
                { "active", new ColumnMetadata()
                {
                    Name = "active",
                    DbName = "active",
                    DataType = "System.Boolean",
                    DbDataType = "bool",
                    NewDataType = "bool",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = "false",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "ACTIVE",
                    HeaderTextEn = "ACTIVE",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 4
                }
                }
            };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column config_function_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColConfigFunctionId = Cols["config_function_id"];

                /// <summary>
                /// Column ext_version_valid_from_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColExtensionVersionValidFromId = Cols["ext_version_valid_from_id"];

                /// <summary>
                /// Column ext_version_valid_to_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColExtensionVersionValidUntilId = Cols["ext_version_valid_until_id"];

                /// <summary>
                /// Column active metadata
                /// </summary>
                public static readonly ColumnMetadata ColActive = Cols["active"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public ExtensionToConfigFunctionList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public ExtensionToConfigFunctionList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// List of mappings between extension version and configuration function(read-only)
                /// </summary>
                public List<ExtensionToConfigFunction> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new ExtensionToConfigFunction(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                ///  Mapping between extension version and configuration function from list by identifier (read-only)
                /// </summary>
                /// <param name="id"> Mapping between extension version and configuration function identifier</param>
                /// <returns> Mapping between extension version and configuration function from list by identifier (null if not found)</returns>
                public ExtensionToConfigFunction this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new ExtensionToConfigFunction(composite: this, data: a))
                                .FirstOrDefault<ExtensionToConfigFunction>();
                    }
                }

                #endregion Indexer


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public ExtensionToConfigFunctionList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new ExtensionToConfigFunctionList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new ExtensionToConfigFunctionList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                #endregion Methods

            }

        }
    }
}