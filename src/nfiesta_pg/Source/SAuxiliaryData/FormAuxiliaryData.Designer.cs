﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi.NfiEstaPg.AuxiliaryData
{
    partial class FormAuxiliaryData
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            msMain = new System.Windows.Forms.MenuStrip();
            tsmiLookupTables = new System.Windows.Forms.ToolStripMenuItem();
            tsmiCConfigFunction = new System.Windows.Forms.ToolStripMenuItem();
            tsmiCConfigQuery = new System.Windows.Forms.ToolStripMenuItem();
            tsmiCEstimationCell = new System.Windows.Forms.ToolStripMenuItem();
            tsmiCEstimationCellCollection = new System.Windows.Forms.ToolStripMenuItem();
            tsmiCExtVersion = new System.Windows.Forms.ToolStripMenuItem();
            tsmiCGUIVersion = new System.Windows.Forms.ToolStripMenuItem();
            tsmiMappingTables = new System.Windows.Forms.ToolStripMenuItem();
            tsmiCmEstimationCell = new System.Windows.Forms.ToolStripMenuItem();
            tsmiCmEstimationCellCollection = new System.Windows.Forms.ToolStripMenuItem();
            tsmiCmExtConfigFunction = new System.Windows.Forms.ToolStripMenuItem();
            tsmiCmExtGUIVersion = new System.Windows.Forms.ToolStripMenuItem();
            tsmiSpatialTables = new System.Windows.Forms.ToolStripMenuItem();
            tsmiFaCell = new System.Windows.Forms.ToolStripMenuItem();
            tsmiFpPlot = new System.Windows.Forms.ToolStripMenuItem();
            tsmiDataTables = new System.Windows.Forms.ToolStripMenuItem();
            tsmiTAuxTotal = new System.Windows.Forms.ToolStripMenuItem();
            tsmiTAuxData = new System.Windows.Forms.ToolStripMenuItem();
            tsmiTConfig = new System.Windows.Forms.ToolStripMenuItem();
            tsmiTConfigCollection = new System.Windows.Forms.ToolStripMenuItem();
            tsmiTEstimationCellHierarchy = new System.Windows.Forms.ToolStripMenuItem();
            tsmiStoredProcedures = new System.Windows.Forms.ToolStripMenuItem();
            tsmiFnApiGetEstimationCellHierarchy = new System.Windows.Forms.ToolStripMenuItem();
            tsmiFnApiGetEstimationCellHierarchyStage = new System.Windows.Forms.ToolStripMenuItem();
            tsmiFnGetAuxDataApp = new System.Windows.Forms.ToolStripMenuItem();
            tsmiFnGetAuxTotal = new System.Windows.Forms.ToolStripMenuItem();
            tsmiFnGetAuxTotalCount = new System.Windows.Forms.ToolStripMenuItem();
            tsmiFnGetConfigsForAuxDataApp = new System.Windows.Forms.ToolStripMenuItem();
            tsmiFnGetGidsForAuxTotalApp = new System.Windows.Forms.ToolStripMenuItem();
            tsmiFnGetIntervalPointsApp = new System.Windows.Forms.ToolStripMenuItem();
            tsmiETL = new System.Windows.Forms.ToolStripMenuItem();
            tsmiExtractData = new System.Windows.Forms.ToolStripMenuItem();
            tsmiGetDifferences = new System.Windows.Forms.ToolStripMenuItem();
            tlpMain = new System.Windows.Forms.TableLayoutPanel();
            tlpButtons = new System.Windows.Forms.TableLayoutPanel();
            pnlClose = new System.Windows.Forms.Panel();
            btnClose = new System.Windows.Forms.Button();
            grpMain = new System.Windows.Forms.GroupBox();
            splitMain = new System.Windows.Forms.SplitContainer();
            pnlData = new System.Windows.Forms.Panel();
            txtSQL = new System.Windows.Forms.RichTextBox();
            tsmiFnGetAuxDataCount = new System.Windows.Forms.ToolStripMenuItem();
            msMain.SuspendLayout();
            tlpMain.SuspendLayout();
            tlpButtons.SuspendLayout();
            pnlClose.SuspendLayout();
            grpMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)splitMain).BeginInit();
            splitMain.Panel1.SuspendLayout();
            splitMain.Panel2.SuspendLayout();
            splitMain.SuspendLayout();
            SuspendLayout();
            // 
            // msMain
            // 
            msMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { tsmiLookupTables, tsmiMappingTables, tsmiSpatialTables, tsmiDataTables, tsmiStoredProcedures, tsmiETL });
            msMain.Location = new System.Drawing.Point(0, 0);
            msMain.Name = "msMain";
            msMain.Padding = new System.Windows.Forms.Padding(7, 2, 0, 2);
            msMain.Size = new System.Drawing.Size(1101, 24);
            msMain.TabIndex = 1;
            msMain.Text = "menuStrip1";
            // 
            // tsmiLookupTables
            // 
            tsmiLookupTables.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] { tsmiCConfigFunction, tsmiCConfigQuery, tsmiCEstimationCell, tsmiCEstimationCellCollection, tsmiCExtVersion, tsmiCGUIVersion });
            tsmiLookupTables.Name = "tsmiLookupTables";
            tsmiLookupTables.Size = new System.Drawing.Size(114, 20);
            tsmiLookupTables.Text = "tsmiLookupTables";
            // 
            // tsmiCConfigFunction
            // 
            tsmiCConfigFunction.Name = "tsmiCConfigFunction";
            tsmiCConfigFunction.Size = new System.Drawing.Size(235, 22);
            tsmiCConfigFunction.Text = "tsmiCConfigFunction";
            // 
            // tsmiCConfigQuery
            // 
            tsmiCConfigQuery.Name = "tsmiCConfigQuery";
            tsmiCConfigQuery.Size = new System.Drawing.Size(235, 22);
            tsmiCConfigQuery.Text = "tsmiCConfigQuery";
            // 
            // tsmiCEstimationCell
            // 
            tsmiCEstimationCell.Name = "tsmiCEstimationCell";
            tsmiCEstimationCell.Size = new System.Drawing.Size(235, 22);
            tsmiCEstimationCell.Text = "tsmiCEstimationCell";
            // 
            // tsmiCEstimationCellCollection
            // 
            tsmiCEstimationCellCollection.Name = "tsmiCEstimationCellCollection";
            tsmiCEstimationCellCollection.Size = new System.Drawing.Size(235, 22);
            tsmiCEstimationCellCollection.Text = "tsmiCEstimationCellCollection";
            // 
            // tsmiCExtVersion
            // 
            tsmiCExtVersion.Name = "tsmiCExtVersion";
            tsmiCExtVersion.Size = new System.Drawing.Size(235, 22);
            tsmiCExtVersion.Text = "tsmiCExtVersion";
            // 
            // tsmiCGUIVersion
            // 
            tsmiCGUIVersion.Name = "tsmiCGUIVersion";
            tsmiCGUIVersion.Size = new System.Drawing.Size(235, 22);
            tsmiCGUIVersion.Text = "tsmiCGUIVersion";
            // 
            // tsmiMappingTables
            // 
            tsmiMappingTables.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] { tsmiCmEstimationCell, tsmiCmEstimationCellCollection, tsmiCmExtConfigFunction, tsmiCmExtGUIVersion });
            tsmiMappingTables.Name = "tsmiMappingTables";
            tsmiMappingTables.Size = new System.Drawing.Size(122, 20);
            tsmiMappingTables.Text = "tsmiMappingTables";
            // 
            // tsmiCmEstimationCell
            // 
            tsmiCmEstimationCell.Name = "tsmiCmEstimationCell";
            tsmiCmEstimationCell.Size = new System.Drawing.Size(246, 22);
            tsmiCmEstimationCell.Text = "tsmiCmEstimationCell";
            // 
            // tsmiCmEstimationCellCollection
            // 
            tsmiCmEstimationCellCollection.Name = "tsmiCmEstimationCellCollection";
            tsmiCmEstimationCellCollection.Size = new System.Drawing.Size(246, 22);
            tsmiCmEstimationCellCollection.Text = "tsmiCmEstimationCellCollection";
            // 
            // tsmiCmExtConfigFunction
            // 
            tsmiCmExtConfigFunction.Name = "tsmiCmExtConfigFunction";
            tsmiCmExtConfigFunction.Size = new System.Drawing.Size(246, 22);
            tsmiCmExtConfigFunction.Text = "tsmiCmExtConfigFunction";
            // 
            // tsmiCmExtGUIVersion
            // 
            tsmiCmExtGUIVersion.Name = "tsmiCmExtGUIVersion";
            tsmiCmExtGUIVersion.Size = new System.Drawing.Size(246, 22);
            tsmiCmExtGUIVersion.Text = "tsmiCmExtGUIVersion";
            // 
            // tsmiSpatialTables
            // 
            tsmiSpatialTables.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] { tsmiFaCell, tsmiFpPlot });
            tsmiSpatialTables.Name = "tsmiSpatialTables";
            tsmiSpatialTables.Size = new System.Drawing.Size(109, 20);
            tsmiSpatialTables.Text = "tsmiSpatialTables";
            // 
            // tsmiFaCell
            // 
            tsmiFaCell.Name = "tsmiFaCell";
            tsmiFaCell.Size = new System.Drawing.Size(131, 22);
            tsmiFaCell.Text = "tsmiFaCell";
            // 
            // tsmiFpPlot
            // 
            tsmiFpPlot.Name = "tsmiFpPlot";
            tsmiFpPlot.Size = new System.Drawing.Size(131, 22);
            tsmiFpPlot.Text = "tsmiFpPlot";
            // 
            // tsmiDataTables
            // 
            tsmiDataTables.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] { tsmiTAuxTotal, tsmiTAuxData, tsmiTConfig, tsmiTConfigCollection, tsmiTEstimationCellHierarchy });
            tsmiDataTables.Name = "tsmiDataTables";
            tsmiDataTables.Size = new System.Drawing.Size(98, 20);
            tsmiDataTables.Text = "tsmiDataTables";
            // 
            // tsmiTAuxTotal
            // 
            tsmiTAuxTotal.Name = "tsmiTAuxTotal";
            tsmiTAuxTotal.Size = new System.Drawing.Size(230, 22);
            tsmiTAuxTotal.Text = "tsmiTAuxTotal";
            // 
            // tsmiTAuxData
            // 
            tsmiTAuxData.Name = "tsmiTAuxData";
            tsmiTAuxData.Size = new System.Drawing.Size(230, 22);
            tsmiTAuxData.Text = "tsmiTAuxData";
            // 
            // tsmiTConfig
            // 
            tsmiTConfig.Name = "tsmiTConfig";
            tsmiTConfig.Size = new System.Drawing.Size(230, 22);
            tsmiTConfig.Text = "tsmiTConfig";
            // 
            // tsmiTConfigCollection
            // 
            tsmiTConfigCollection.Name = "tsmiTConfigCollection";
            tsmiTConfigCollection.Size = new System.Drawing.Size(230, 22);
            tsmiTConfigCollection.Text = "tsmiTConfigCollection";
            // 
            // tsmiTEstimationCellHierarchy
            // 
            tsmiTEstimationCellHierarchy.Name = "tsmiTEstimationCellHierarchy";
            tsmiTEstimationCellHierarchy.Size = new System.Drawing.Size(230, 22);
            tsmiTEstimationCellHierarchy.Text = "tsmiTEstimationCellHierarchy";
            // 
            // tsmiStoredProcedures
            // 
            tsmiStoredProcedures.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] { tsmiFnApiGetEstimationCellHierarchy, tsmiFnApiGetEstimationCellHierarchyStage, tsmiFnGetAuxDataApp, tsmiFnGetAuxDataCount, tsmiFnGetAuxTotal, tsmiFnGetAuxTotalCount, tsmiFnGetConfigsForAuxDataApp, tsmiFnGetGidsForAuxTotalApp, tsmiFnGetIntervalPointsApp });
            tsmiStoredProcedures.Name = "tsmiStoredProcedures";
            tsmiStoredProcedures.Size = new System.Drawing.Size(135, 20);
            tsmiStoredProcedures.Text = "tsmiStoredProcedures";
            // 
            // tsmiFnApiGetEstimationCellHierarchy
            // 
            tsmiFnApiGetEstimationCellHierarchy.Name = "tsmiFnApiGetEstimationCellHierarchy";
            tsmiFnApiGetEstimationCellHierarchy.Size = new System.Drawing.Size(302, 22);
            tsmiFnApiGetEstimationCellHierarchy.Text = "tsmiFnApiGetEstimationCellHierarchy";
            // 
            // tsmiFnApiGetEstimationCellHierarchyStage
            // 
            tsmiFnApiGetEstimationCellHierarchyStage.Name = "tsmiFnApiGetEstimationCellHierarchyStage";
            tsmiFnApiGetEstimationCellHierarchyStage.Size = new System.Drawing.Size(302, 22);
            tsmiFnApiGetEstimationCellHierarchyStage.Text = "tsmiFnApiGetEstimationCellHierarchyStage";
            // 
            // tsmiFnGetAuxDataApp
            // 
            tsmiFnGetAuxDataApp.Name = "tsmiFnGetAuxDataApp";
            tsmiFnGetAuxDataApp.Size = new System.Drawing.Size(302, 22);
            tsmiFnGetAuxDataApp.Text = "tsmiFnGetAuxDataApp";
            // 
            // tsmiFnGetAuxTotal
            // 
            tsmiFnGetAuxTotal.Name = "tsmiFnGetAuxTotal";
            tsmiFnGetAuxTotal.Size = new System.Drawing.Size(302, 22);
            tsmiFnGetAuxTotal.Text = "tsmiFnGetAuxTotal";
            // 
            // tsmiFnGetAuxTotalCount
            // 
            tsmiFnGetAuxTotalCount.Name = "tsmiFnGetAuxTotalCount";
            tsmiFnGetAuxTotalCount.Size = new System.Drawing.Size(302, 22);
            tsmiFnGetAuxTotalCount.Text = "tsmiFnGetAuxTotalCount";
            // 
            // tsmiFnGetConfigsForAuxDataApp
            // 
            tsmiFnGetConfigsForAuxDataApp.Name = "tsmiFnGetConfigsForAuxDataApp";
            tsmiFnGetConfigsForAuxDataApp.Size = new System.Drawing.Size(302, 22);
            tsmiFnGetConfigsForAuxDataApp.Text = "tsmiFnGetConfigsForAuxDataApp";
            // 
            // tsmiFnGetGidsForAuxTotalApp
            // 
            tsmiFnGetGidsForAuxTotalApp.Name = "tsmiFnGetGidsForAuxTotalApp";
            tsmiFnGetGidsForAuxTotalApp.Size = new System.Drawing.Size(302, 22);
            tsmiFnGetGidsForAuxTotalApp.Text = "tsmiFnGetGidsForAuxTotalApp";
            // 
            // tsmiFnGetIntervalPointsApp
            // 
            tsmiFnGetIntervalPointsApp.Name = "tsmiFnGetIntervalPointsApp";
            tsmiFnGetIntervalPointsApp.Size = new System.Drawing.Size(302, 22);
            tsmiFnGetIntervalPointsApp.Text = "tsmiFnGetIntervalPointsApp";
            // 
            // tsmiETL
            // 
            tsmiETL.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] { tsmiExtractData, tsmiGetDifferences });
            tsmiETL.Name = "tsmiETL";
            tsmiETL.Size = new System.Drawing.Size(60, 20);
            tsmiETL.Text = "tsmiETL";
            // 
            // tsmiExtractData
            // 
            tsmiExtractData.Name = "tsmiExtractData";
            tsmiExtractData.Size = new System.Drawing.Size(174, 22);
            tsmiExtractData.Text = "tsmiExtractData";
            // 
            // tsmiGetDifferences
            // 
            tsmiGetDifferences.Name = "tsmiGetDifferences";
            tsmiGetDifferences.Size = new System.Drawing.Size(174, 22);
            tsmiGetDifferences.Text = "tsmiGetDifferences";
            // 
            // tlpMain
            // 
            tlpMain.ColumnCount = 1;
            tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.Controls.Add(tlpButtons, 0, 1);
            tlpMain.Controls.Add(grpMain, 0, 0);
            tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpMain.Location = new System.Drawing.Point(0, 24);
            tlpMain.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            tlpMain.Name = "tlpMain";
            tlpMain.RowCount = 2;
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 46F));
            tlpMain.Size = new System.Drawing.Size(1101, 554);
            tlpMain.TabIndex = 6;
            // 
            // tlpButtons
            // 
            tlpButtons.BackColor = System.Drawing.SystemColors.Control;
            tlpButtons.ColumnCount = 2;
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 187F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            tlpButtons.Controls.Add(pnlClose, 1, 0);
            tlpButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpButtons.Location = new System.Drawing.Point(0, 508);
            tlpButtons.Margin = new System.Windows.Forms.Padding(0);
            tlpButtons.Name = "tlpButtons";
            tlpButtons.RowCount = 1;
            tlpButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.Size = new System.Drawing.Size(1101, 46);
            tlpButtons.TabIndex = 28;
            // 
            // pnlClose
            // 
            pnlClose.Controls.Add(btnClose);
            pnlClose.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlClose.Location = new System.Drawing.Point(914, 0);
            pnlClose.Margin = new System.Windows.Forms.Padding(0);
            pnlClose.Name = "pnlClose";
            pnlClose.Padding = new System.Windows.Forms.Padding(6);
            pnlClose.Size = new System.Drawing.Size(187, 46);
            pnlClose.TabIndex = 12;
            // 
            // btnClose
            // 
            btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            btnClose.Dock = System.Windows.Forms.DockStyle.Fill;
            btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            btnClose.Location = new System.Drawing.Point(6, 6);
            btnClose.Margin = new System.Windows.Forms.Padding(0);
            btnClose.Name = "btnClose";
            btnClose.Size = new System.Drawing.Size(175, 34);
            btnClose.TabIndex = 13;
            btnClose.Text = "btnClose";
            btnClose.UseVisualStyleBackColor = true;
            // 
            // grpMain
            // 
            grpMain.Controls.Add(splitMain);
            grpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            grpMain.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            grpMain.ForeColor = System.Drawing.SystemColors.ControlText;
            grpMain.Location = new System.Drawing.Point(4, 3);
            grpMain.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            grpMain.Name = "grpMain";
            grpMain.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            grpMain.Size = new System.Drawing.Size(1093, 502);
            grpMain.TabIndex = 5;
            grpMain.TabStop = false;
            // 
            // splitMain
            // 
            splitMain.Dock = System.Windows.Forms.DockStyle.Fill;
            splitMain.Location = new System.Drawing.Point(4, 18);
            splitMain.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            splitMain.Name = "splitMain";
            splitMain.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitMain.Panel1
            // 
            splitMain.Panel1.Controls.Add(pnlData);
            // 
            // splitMain.Panel2
            // 
            splitMain.Panel2.Controls.Add(txtSQL);
            splitMain.Size = new System.Drawing.Size(1085, 481);
            splitMain.SplitterDistance = 238;
            splitMain.SplitterWidth = 1;
            splitMain.TabIndex = 11;
            // 
            // pnlData
            // 
            pnlData.AutoScroll = true;
            pnlData.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlData.Location = new System.Drawing.Point(0, 0);
            pnlData.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            pnlData.Name = "pnlData";
            pnlData.Size = new System.Drawing.Size(1085, 238);
            pnlData.TabIndex = 3;
            // 
            // txtSQL
            // 
            txtSQL.BackColor = System.Drawing.SystemColors.Window;
            txtSQL.BorderStyle = System.Windows.Forms.BorderStyle.None;
            txtSQL.Dock = System.Windows.Forms.DockStyle.Fill;
            txtSQL.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            txtSQL.Location = new System.Drawing.Point(0, 0);
            txtSQL.Name = "txtSQL";
            txtSQL.ReadOnly = true;
            txtSQL.Size = new System.Drawing.Size(1085, 242);
            txtSQL.TabIndex = 0;
            txtSQL.Text = "";
            // 
            // tsmiFnGetAuxDataCount
            // 
            tsmiFnGetAuxDataCount.Name = "tsmiFnGetAuxDataCount";
            tsmiFnGetAuxDataCount.Size = new System.Drawing.Size(302, 22);
            tsmiFnGetAuxDataCount.Text = "tsmiFnGetAuxDataCount";
            // 
            // FormAuxiliaryData
            // 
            AcceptButton = btnClose;
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            CancelButton = btnClose;
            ClientSize = new System.Drawing.Size(1101, 578);
            Controls.Add(tlpMain);
            Controls.Add(msMain);
            MainMenuStrip = msMain;
            Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            Name = "FormAuxiliaryData";
            ShowIcon = false;
            StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            Text = "FormAuxiliaryData";
            msMain.ResumeLayout(false);
            msMain.PerformLayout();
            tlpMain.ResumeLayout(false);
            tlpButtons.ResumeLayout(false);
            pnlClose.ResumeLayout(false);
            grpMain.ResumeLayout(false);
            splitMain.Panel1.ResumeLayout(false);
            splitMain.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)splitMain).EndInit();
            splitMain.ResumeLayout(false);
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private System.Windows.Forms.MenuStrip msMain;
        private System.Windows.Forms.ToolStripMenuItem tsmiLookupTables;
        private System.Windows.Forms.ToolStripMenuItem tsmiCConfigFunction;
        private System.Windows.Forms.ToolStripMenuItem tsmiMappingTables;
        private System.Windows.Forms.ToolStripMenuItem tsmiCmEstimationCellCollection;
        private System.Windows.Forms.ToolStripMenuItem tsmiSpatialTables;
        private System.Windows.Forms.ToolStripMenuItem tsmiFaCell;
        private System.Windows.Forms.ToolStripMenuItem tsmiDataTables;
        private System.Windows.Forms.ToolStripMenuItem tsmiTAuxData;
        private System.Windows.Forms.ToolStripMenuItem tsmiETL;
        private System.Windows.Forms.ToolStripMenuItem tsmiExtractData;
        private System.Windows.Forms.ToolStripMenuItem tsmiGetDifferences;
        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.TableLayoutPanel tlpButtons;
        private System.Windows.Forms.Panel pnlClose;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.GroupBox grpMain;
        private System.Windows.Forms.SplitContainer splitMain;
        private System.Windows.Forms.Panel pnlData;
        private System.Windows.Forms.RichTextBox txtSQL;
        private System.Windows.Forms.ToolStripMenuItem tsmiCConfigQuery;
        private System.Windows.Forms.ToolStripMenuItem tsmiCEstimationCell;
        private System.Windows.Forms.ToolStripMenuItem tsmiCEstimationCellCollection;
        private System.Windows.Forms.ToolStripMenuItem tsmiCExtVersion;
        private System.Windows.Forms.ToolStripMenuItem tsmiCGUIVersion;
        private System.Windows.Forms.ToolStripMenuItem tsmiCmExtConfigFunction;
        private System.Windows.Forms.ToolStripMenuItem tsmiCmExtGUIVersion;
        private System.Windows.Forms.ToolStripMenuItem tsmiFpPlot;
        private System.Windows.Forms.ToolStripMenuItem tsmiTAuxTotal;
        private System.Windows.Forms.ToolStripMenuItem tsmiTConfig;
        private System.Windows.Forms.ToolStripMenuItem tsmiTConfigCollection;
        private System.Windows.Forms.ToolStripMenuItem tsmiCmEstimationCell;
        private System.Windows.Forms.ToolStripMenuItem tsmiTEstimationCellHierarchy;
        private System.Windows.Forms.ToolStripMenuItem tsmiStoredProcedures;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnGetAuxDataApp;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnGetAuxTotal;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnGetConfigsForAuxDataApp;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnGetGidsForAuxTotalApp;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnApiGetEstimationCellHierarchy;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnApiGetEstimationCellHierarchyStage;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnGetIntervalPointsApp;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnGetAuxTotalCount;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnGetAuxDataCount;
    }

}