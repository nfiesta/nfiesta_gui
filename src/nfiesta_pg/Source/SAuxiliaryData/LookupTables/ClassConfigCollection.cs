﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Versioning;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace AuxiliaryData
        {
            // t_config_collection

            /// <summary>
            /// Configuration collection
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class ConfigCollection(
                ConfigCollectionList composite = null,
                DataRow data = null)
                    : ALookupTableEntry<ConfigCollectionList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Configuration collection identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: ConfigCollectionList.ColId.Name,
                            defaultValue: Int32.Parse(s: ConfigCollectionList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: ConfigCollectionList.ColId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Auxiliary variable identifier
                /// </summary>
                public Nullable<int> AuxiliaryVariableId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: ConfigCollectionList.ColAuxiliaryVariableId.Name,
                            defaultValue: String.IsNullOrEmpty(value: ConfigCollectionList.ColAuxiliaryVariableId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: ConfigCollectionList.ColAuxiliaryVariableId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: ConfigCollectionList.ColAuxiliaryVariableId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Auxiliary variable object (read-only)
                /// </summary>
                public Core.AuxiliaryVariable AuxiliaryVariable
                {
                    get
                    {
                        return (AuxiliaryVariableId != null) ?
                             ((NfiEstaDB)Composite.Database).SNfiEsta.CAuxiliaryVariable[(int)AuxiliaryVariableId] : null;
                    }
                }


                /// <summary>
                /// Configuration function identifier
                /// </summary>
                public int ConfigFunctionId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: ConfigCollectionList.ColConfigFunctionId.Name,
                            defaultValue: Int32.Parse(s: ConfigCollectionList.ColConfigFunctionId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: ConfigCollectionList.ColConfigFunctionId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Configuration function (read-only)
                /// </summary>
                public ConfigFunctionEnum ConfigFunctionValue
                {
                    get
                    {
                        return (ConfigFunctionEnum)ConfigFunctionId;
                    }
                }

                /// <summary>
                /// Configuration function object (read-only)
                /// </summary>
                public ConfigFunction ConfigFunction
                {
                    get
                    {
                        return ((NfiEstaDB)Composite.Database).SAuxiliaryData.CConfigFunction[ConfigFunctionId];
                    }
                }

                /// <summary>
                /// Configuration collection contains gis layers for calculation totals (read-only)
                /// </summary>
                public bool IsForTotals
                {
                    get
                    {
                        // Seznam skupin konfigurací pro výpočet úhrnu
                        List<ConfigFunctionEnum> layersForTotals =
                        [
                            ConfigFunctionEnum.VectorTotal,
                            ConfigFunctionEnum.RasterTotal,
                            ConfigFunctionEnum.RasterTotalWithinVector,
                            ConfigFunctionEnum.RastersProductTotal
                        ];
                        return layersForTotals.Contains(item: ConfigFunctionValue);
                    }
                }

                /// <summary>
                /// Configuration collection contains point gis layer (read-only)
                /// </summary>
                public bool IsPointLayer
                {
                    get
                    {
                        // Seznam skupin konfigurací s bodovou gis vrstvou
                        List<ConfigFunctionEnum> pointLayers =
                        [
                            ConfigFunctionEnum.PointLayer
                        ];
                        return pointLayers.Contains(item: ConfigFunctionValue);
                    }
                }

                /// <summary>
                /// Configuration collection contains gis layers for calculation intersection values (read-only)
                /// </summary>
                public bool IsForIntersection
                {
                    get
                    {
                        // Seznam skupin konfigurací pro výpočet úhrnu
                        List<ConfigFunctionEnum> layersForIntersection =
                        [
                            ConfigFunctionEnum.PointTotalCombination
                        ];
                        return layersForIntersection.Contains(item: ConfigFunctionValue);
                    }
                }


                /// <summary>
                /// Label of configuration collection in national language
                /// </summary>
                public override string LabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: ConfigCollectionList.ColLabelCs.Name,
                            defaultValue: ConfigCollectionList.ColLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: ConfigCollectionList.ColLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Description of configuration collection in national language
                /// </summary>
                public override string DescriptionCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: ConfigCollectionList.ColDescriptionCs.Name,
                            defaultValue: ConfigCollectionList.ColDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: ConfigCollectionList.ColDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Extended label of configuration collection in national language (read-only)
                /// </summary>
                public override string ExtendedLabelCs
                {
                    get
                    {
                        return String.IsNullOrEmpty(value: LabelCs)
                            ? $"{Id}"
                            : $"{Id} - {LabelCs}";
                    }
                }


                /// <summary>
                /// Label of configuration collection in English
                /// </summary>
                public override string LabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: ConfigCollectionList.ColLabelEn.Name,
                            defaultValue: ConfigCollectionList.ColLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: ConfigCollectionList.ColLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Description of configuration collection in English
                /// </summary>
                public override string DescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: ConfigCollectionList.ColDescriptionEn.Name,
                            defaultValue: ConfigCollectionList.ColDescriptionEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: ConfigCollectionList.ColDescriptionEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Extended label of configuration collection in English (read-only)
                /// </summary>
                public override string ExtendedLabelEn
                {
                    get
                    {
                        return String.IsNullOrEmpty(value: LabelEn)
                            ? $"{Id}"
                            : $"{Id} - {LabelEn}";
                    }
                }


                /// <summary>
                /// Aggregated collection of configurations
                /// </summary>
                public bool Aggregated
                {
                    get
                    {
                        return Functions.GetBoolArg(
                            row: Data,
                            name: ConfigCollectionList.ColAggregated.Name,
                            defaultValue: Boolean.Parse(value: ConfigCollectionList.ColAggregated.DefaultValue));
                    }
                    set
                    {
                        Functions.SetBoolArg(
                            row: Data,
                            name: ConfigCollectionList.ColAggregated.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Name of database with GIS layer
                /// </summary>
                public string CatalogName
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: ConfigCollectionList.ColCatalogName.Name,
                            defaultValue: ConfigCollectionList.ColCatalogName.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: ConfigCollectionList.ColCatalogName.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Name of schema with GIS layer
                /// </summary>
                public string SchemaName
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: ConfigCollectionList.ColSchemaName.Name,
                            defaultValue: ConfigCollectionList.ColSchemaName.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: ConfigCollectionList.ColSchemaName.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Name of table with GIS layer
                /// </summary>
                public string TableName
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: ConfigCollectionList.ColTableName.Name,
                            defaultValue: ConfigCollectionList.ColTableName.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: ConfigCollectionList.ColTableName.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Name of column with identifier in table with GIS layer
                /// </summary>
                public string ColumnIdent
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: ConfigCollectionList.ColColumnIdent.Name,
                            defaultValue: ConfigCollectionList.ColColumnIdent.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: ConfigCollectionList.ColColumnIdent.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Name of column with geometry in table with GIS layer
                /// </summary>
                public string ColumnName
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: ConfigCollectionList.ColColumnName.Name,
                            defaultValue: ConfigCollectionList.ColColumnName.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: ConfigCollectionList.ColColumnName.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Coefficient for conversion of units
                /// </summary>
                public Nullable<double> Unit
                {
                    get
                    {
                        return Functions.GetNDoubleArg(
                            row: Data,
                            name: ConfigCollectionList.ColUnit.Name,
                            defaultValue: String.IsNullOrEmpty(value: ConfigCollectionList.ColUnit.DefaultValue) ?
                                (Nullable<double>)null :
                                Double.Parse(s: ConfigCollectionList.ColUnit.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNDoubleArg(
                            row: Data,
                            name: ConfigCollectionList.ColUnit.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Additional information
                /// </summary>
                public string Tag
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: ConfigCollectionList.ColTag.Name,
                            defaultValue: ConfigCollectionList.ColTag.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: ConfigCollectionList.ColTag.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Configuration collection is closed for editations
                /// </summary>
                public bool Closed
                {
                    get
                    {
                        return Functions.GetBoolArg(
                            row: Data,
                            name: ConfigCollectionList.ColClosed.Name,
                            defaultValue: Boolean.Parse(value: ConfigCollectionList.ColClosed.DefaultValue));
                    }
                    set
                    {
                        Functions.SetBoolArg(
                            row: Data,
                            name: ConfigCollectionList.ColClosed.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Last change date
                /// </summary>
                public DateTime EditDate
                {
                    get
                    {
                        return Functions.GetDateTimeArg(
                            row: Data,
                            name: ConfigCollectionList.ColEditDate.Name,
                            defaultValue: DateTime.Parse(s: ConfigCollectionList.ColEditDate.DefaultValue));
                    }
                    set
                    {
                        Functions.SetDateTimeArg(
                            row: Data,
                            name: ConfigCollectionList.ColEditDate.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Last change date as text (read-only)
                /// </summary>
                public string EditDateText
                {
                    get
                    {
                        return
                            EditDate.ToString(
                                format: ConfigCollectionList.ColEditDate.NumericFormat);
                    }
                }

                /// <summary>
                /// User
                /// </summary>
                public string EditUser
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: ConfigCollectionList.ColEditUser.Name,
                            defaultValue: ConfigCollectionList.ColEditUser.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: ConfigCollectionList.ColEditUser.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Condition for point selection from GIS layer
                /// </summary>
                public string Condition
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: ConfigCollectionList.ColCondition.Name,
                            defaultValue: ConfigCollectionList.ColCondition.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: ConfigCollectionList.ColCondition.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Number of points selected from GIS layer (corresponds to the specified condition)
                /// </summary>
                public Nullable<int> TotalPoints
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: ConfigCollectionList.ColTotalPoints.Name,
                            defaultValue: String.IsNullOrEmpty(value: ConfigCollectionList.ColTotalPoints.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(ConfigCollectionList.ColTotalPoints.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: ConfigCollectionList.ColTotalPoints.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Configuration collection identifier (for auxiliary variable totals)
                /// </summary>
                public Nullable<int> RefTotalId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: ConfigCollectionList.ColRefTotalId.Name,
                            defaultValue: String.IsNullOrEmpty(value: ConfigCollectionList.ColRefTotalId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: ConfigCollectionList.ColRefTotalId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: ConfigCollectionList.ColRefTotalId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Configuration collection object (for auxiliary variable totals) (read-only)
                /// </summary>
                public ConfigCollection RefTotal
                {
                    get
                    {
                        return (RefTotalId != null) ?
                             ((NfiEstaDB)Composite.Database).SAuxiliaryData.TConfigCollection[(int)RefTotalId] : null;
                    }
                }


                /// <summary>
                /// Configuration collection identifier (for point layer)
                /// </summary>
                public Nullable<int> RefLayerPointsId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: ConfigCollectionList.ColRefLayerPointsId.Name,
                            defaultValue: String.IsNullOrEmpty(value: ConfigCollectionList.ColRefLayerPointsId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: ConfigCollectionList.ColRefLayerPointsId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: ConfigCollectionList.ColRefLayerPointsId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Configuration collection object (for point layer) (read-only)
                /// </summary>
                public ConfigCollection RefLayerPoints
                {
                    get
                    {
                        return (RefLayerPointsId != null) ?
                             ((NfiEstaDB)Composite.Database).SAuxiliaryData.TConfigCollection[(int)RefLayerPointsId]
                             : null;
                    }
                }


                /// <summary>
                /// List of configurations in this collection
                /// </summary>
                public List<Config> Configurations
                {
                    get
                    {
                        return
                             ((NfiEstaDB)Composite.Database).SAuxiliaryData.TConfig.Items
                             .Where(a => a.ConfigCollectionId == Id)
                             .ToList<Config>();
                    }
                }

                /// <summary>
                /// List of configurations in this collection as text
                /// </summary>
                public string ConfigurationsText
                {
                    get
                    {
                        if (Configurations == null)
                        {
                            return String.Empty;
                        }

                        if (IsEmpty)
                        {
                            return String.Empty;
                        }

                        return
                            Configurations
                                .Select(a => a.Id.ToString())
                                .Aggregate((a, b) => $"{a},{b}");
                    }
                }

                /// <summary>
                /// List of configurations in this collection is empty
                /// </summary>
                public bool IsEmpty
                {
                    get
                    {
                        return Configurations.Count == 0;
                    }
                }


                /// <summary>
                /// List of basic configurations (configurations for gis layer)
                /// </summary>
                public List<Config> BasicConfigurations
                {
                    get
                    {
                        return
                             ((NfiEstaDB)Composite.Database).SAuxiliaryData.TConfig.Items
                             .Where(a => a.ConfigCollectionId == Id)
                             .Where(a => a.ConfigQueryValue == ConfigQueryEnum.GisLayer)
                             .ToList<Config>();
                    }
                }

                /// <summary>
                /// List of basic configurations is not empty
                /// </summary>
                public bool ContainsBasicConfigurations
                {
                    get
                    {
                        return BasicConfigurations.Count != 0;
                    }
                }


                /// <summary>
                /// List of dependent configuration collections
                /// </summary>
                public List<ConfigCollection> DependentConfigCollections
                {
                    get
                    {
                        return
                            [.. ((NfiEstaDB)Composite.Database).SAuxiliaryData.TConfigCollection.Items
                            .Where(a => a.Id != Id)
                            .Where(a =>
                                ((a.RefLayerPointsId ?? 0) == Id) ||
                                ((a.RefTotalId ?? 0) == Id))
                            .Distinct<ConfigCollection>()
                            .OrderBy(a => a.Id)];
                    }
                }

                /// <summary>
                /// List of dependent configuration collections is not empty
                /// </summary>
                public bool ContainsDependentConfigCollections
                {
                    get
                    {
                        return DependentConfigCollections.Count != 0;
                    }
                }


                /// <summary>
                /// Number of calculated auxiliary variable totals in configuration collection
                /// </summary>
                public long AuxTotalCount
                {
                    get
                    {
                        return
                            Configurations.Sum(a => a.AuxTotalCount);
                    }
                }

                /// <summary>
                /// Configuration collection contains some calculated auxiliary variable totals
                /// </summary>
                public bool ContainsCalculatedAuxTotals
                {
                    get
                    {
                        return AuxTotalCount != 0;
                    }
                }


                /// <summary>
                /// Number of calculated auxiliary variable values in configuration collection
                /// </summary>
                public long AuxDataCount
                {
                    get
                    {
                        return
                            Configurations.Sum(a => a.AuxDataCount);
                    }
                }

                /// <summary>
                /// Configuration collection contains some calculated auxiliary variable values
                /// </summary>
                public bool ContainsCalculatedAuxData
                {
                    get
                    {
                        return AuxDataCount != 0;
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Reset all configuration collection properties to default values
                /// </summary>
                public void FullReset()
                {
                    AuxiliaryVariableId = null;
                    ConfigFunctionId = (int)ConfigFunctionEnum.VectorTotal;
                    LabelCs = null;
                    DescriptionCs = null;
                    LabelEn = null;
                    DescriptionEn = null;
                    Closed = false;
                    Aggregated = false;
                    CatalogName = null;
                    SchemaName = null;
                    TableName = null;
                    ColumnIdent = null;
                    ColumnName = null;
                    Unit = null;
                    Condition = null;
                    TotalPoints = null;
                    Tag = null;
                    RefTotalId = null;
                    RefLayerPointsId = null;
                    EditDate = DateTime.Now;
                }

                /// <summary>
                /// Reset layer setting of configuration collection to default values
                /// </summary>
                public void Reset()
                {
                    CatalogName = null;
                    SchemaName = null;
                    TableName = null;
                    ColumnIdent = null;
                    ColumnName = null;
                    Unit = null;
                    Condition = null;
                    TotalPoints = null;
                    Tag = null;
                    RefTotalId = null;
                    RefLayerPointsId = null;
                    EditDate = DateTime.Now;
                }


                /// <summary>
                /// Insert new configuration collection into database
                /// </summary>
                public void Insert()
                {
                    List<string> commands = [];

                    commands.Add(item: String.Concat(
                        $"INSERT INTO {ConfigCollectionList.Schema}.{ConfigCollectionList.Name}{Environment.NewLine}",
                        $"(",
                        $"{ConfigCollectionList.ColId.DbName},{Environment.NewLine}",                                                        //  0
                        $"{ConfigCollectionList.ColAuxiliaryVariableId.DbName},{Environment.NewLine}",                                       //  1
                        $"{ConfigCollectionList.ColConfigFunctionId.DbName},{Environment.NewLine}",                                          //  2
                        $"{ConfigCollectionList.ColLabelCs.DbName},{Environment.NewLine}",                                                   //  3
                        $"{ConfigCollectionList.ColLabelEn.DbName},{Environment.NewLine}",                                                   //  4
                        $"{ConfigCollectionList.ColAggregated.DbName},{Environment.NewLine}",                                                //  5
                        $"{ConfigCollectionList.ColCatalogName.DbName},{Environment.NewLine}",                                               //  6
                        $"{ConfigCollectionList.ColSchemaName.DbName},{Environment.NewLine}",                                                //  7
                        $"{ConfigCollectionList.ColTableName.DbName},{Environment.NewLine}",                                                 //  8
                        $"{ConfigCollectionList.ColColumnIdent.DbName},{Environment.NewLine}",                                               //  9
                        $"{ConfigCollectionList.ColColumnName.DbName},{Environment.NewLine}",                                                // 10
                        $"{ConfigCollectionList.ColUnit.DbName},{Environment.NewLine}",                                                      // 11
                        $"{ConfigCollectionList.ColTag.DbName},{Environment.NewLine}",                                                       // 12
                        $"{ConfigCollectionList.ColDescriptionCs.DbName},{Environment.NewLine}",                                             // 13
                        $"{ConfigCollectionList.ColDescriptionEn.DbName},{Environment.NewLine}",                                             // 14
                        $"{ConfigCollectionList.ColClosed.DbName},{Environment.NewLine}",                                                    // 15
                        $"{ConfigCollectionList.ColEditDate.DbName},{Environment.NewLine}",                                                  // 16
                        $"{ConfigCollectionList.ColCondition.DbName},{Environment.NewLine}",                                                 // 17
                        $"{ConfigCollectionList.ColTotalPoints.DbName},{Environment.NewLine}",                                               // 18
                        $"{ConfigCollectionList.ColRefLayerPointsId.DbName},{Environment.NewLine}",                                          // 19
                        $"{ConfigCollectionList.ColRefTotalId.DbName},{Environment.NewLine}",                                                // 20
                        $"{ConfigCollectionList.ColEditUser.DbName}{Environment.NewLine}",                                                   // 21
                        $"){Environment.NewLine}",
                        $"VALUES{Environment.NewLine}({Environment.NewLine}",
                        $"{Functions.PrepIntArg(arg: Id)},{Environment.NewLine}",                                                            //  0
                        $"{Functions.PrepNIntArg(arg: AuxiliaryVariableId)},{Environment.NewLine}",                                          //  1
                        $"{Functions.PrepIntArg(arg: ConfigFunctionId)},{Environment.NewLine}",                                              //  2
                        $"{Functions.PrepStringArg(arg: LabelCs, replaceEmptyStringByNull: true)},{Environment.NewLine}",                    //  3
                        $"{Functions.PrepStringArg(arg: LabelEn, replaceEmptyStringByNull: true)},{Environment.NewLine}",                    //  4
                        $"{Functions.PrepBoolArg(arg: Aggregated)},{Environment.NewLine}",                                                   //  5
                        $"{Functions.PrepStringArg(arg: CatalogName, replaceEmptyStringByNull: true)},{Environment.NewLine}",                //  6
                        $"{Functions.PrepStringArg(arg: SchemaName, replaceEmptyStringByNull: true)},{Environment.NewLine}",                 //  7
                        $"{Functions.PrepStringArg(arg: TableName, replaceEmptyStringByNull: true)},{Environment.NewLine}",                  //  8
                        $"{Functions.PrepStringArg(arg: ColumnIdent, replaceEmptyStringByNull: true)},{Environment.NewLine}",                //  9
                        $"{Functions.PrepStringArg(arg: ColumnName, replaceEmptyStringByNull: true)},{Environment.NewLine}",                 // 10
                        $"{Functions.PrepNDoubleArg(arg: Unit)},{Environment.NewLine}",                                                      // 11
                        $"{Functions.PrepStringArg(arg: Tag, replaceEmptyStringByNull: true)},{Environment.NewLine}",                        // 12
                        $"{Functions.PrepStringArg(arg: DescriptionCs, replaceEmptyStringByNull: true)}{Environment.NewLine},",              // 13
                        $"{Functions.PrepStringArg(arg: DescriptionEn, replaceEmptyStringByNull: true)}{Environment.NewLine},",              // 14
                        $"{Functions.PrepBoolArg(arg: Closed)},{Environment.NewLine}",                                                       // 15
                        $"pg_catalog.now(),{Environment.NewLine}",                                                                           // 16
                        $"{Functions.PrepStringArg(arg: Condition, replaceEmptyStringByNull: true)},{Environment.NewLine}",                  // 17
                        $"{Functions.PrepNIntArg(arg: TotalPoints)},{Environment.NewLine}",                                                  // 18
                        $"{Functions.PrepNIntArg(arg: RefLayerPointsId)},{Environment.NewLine}",                                             // 19
                        $"{Functions.PrepNIntArg(arg: RefTotalId)},{Environment.NewLine}",                                                   // 20
                        $"pg_catalog.current_user(){Environment.NewLine}",                                                                   // 21
                        $");{Environment.NewLine}"
                        ));

                    Composite.Database.Postgres.ExecuteList(
                        sqlCommands: commands);

                    Composite.ReLoad();
                }

                /// <summary>
                /// Update configuration collection in database
                /// </summary>
                public void Update()
                {
                    List<string> commands = [];

                    commands.Add(item: String.Concat(
                        $"UPDATE {ConfigCollectionList.Schema}.{ConfigCollectionList.Name}{Environment.NewLine}",
                        $"SET{Environment.NewLine}",
                        $"    {ConfigCollectionList.ColAuxiliaryVariableId.DbName} = ",
                        $"{Functions.PrepNIntArg(arg: AuxiliaryVariableId)},{Environment.NewLine}",                                 //  1
                        $"    {ConfigCollectionList.ColConfigFunctionId.DbName} = ",
                        $"{Functions.PrepIntArg(arg: ConfigFunctionId)},{Environment.NewLine}",                                     //  2
                        $"    {ConfigCollectionList.ColLabelCs.DbName} = ",
                        $"{Functions.PrepStringArg(arg: LabelCs, replaceEmptyStringByNull: true)},{Environment.NewLine}",           //  3
                        $"    {ConfigCollectionList.ColLabelEn.DbName} = ",
                        $"{Functions.PrepStringArg(arg: LabelEn, replaceEmptyStringByNull: true)},{Environment.NewLine}",           //  4
                        $"    {ConfigCollectionList.ColAggregated.DbName} = ",
                        $"{Functions.PrepBoolArg(arg: Aggregated)},{Environment.NewLine}",                                          //  5
                        $"    {ConfigCollectionList.ColCatalogName.DbName} = ",
                        $"{Functions.PrepStringArg(arg: CatalogName, replaceEmptyStringByNull: true)},{Environment.NewLine}",       //  6
                        $"    {ConfigCollectionList.ColSchemaName.DbName} = ",
                        $"{Functions.PrepStringArg(arg: SchemaName, replaceEmptyStringByNull: true)},{Environment.NewLine}",        //  7
                        $"    {ConfigCollectionList.ColTableName.DbName} = ",
                        $"{Functions.PrepStringArg(arg: TableName, replaceEmptyStringByNull: true)},{Environment.NewLine}",         //  8
                        $"    {ConfigCollectionList.ColColumnIdent.DbName} = ",
                        $"{Functions.PrepStringArg(arg: ColumnIdent, replaceEmptyStringByNull: true)},{Environment.NewLine}",       //  9
                        $"    {ConfigCollectionList.ColColumnName.DbName} = ",
                        $"{Functions.PrepStringArg(arg: ColumnName, replaceEmptyStringByNull: true)},{Environment.NewLine}",        // 10
                        $"    {ConfigCollectionList.ColUnit.DbName} = ",
                        $"{Functions.PrepNDoubleArg(Unit)},{Environment.NewLine}",                                                  // 11
                        $"    {ConfigCollectionList.ColTag.DbName} = ",
                        $"{Functions.PrepStringArg(arg: Tag, replaceEmptyStringByNull: true)},{Environment.NewLine}",               // 12
                        $"    {ConfigCollectionList.ColDescriptionCs.DbName} = ",
                        $"{Functions.PrepStringArg(arg: DescriptionCs, replaceEmptyStringByNull: true)},{Environment.NewLine}",     // 13
                        $"    {ConfigCollectionList.ColDescriptionEn.DbName} = ",
                        $"{Functions.PrepStringArg(arg: DescriptionEn, replaceEmptyStringByNull: true)},{Environment.NewLine}",     // 14
                        $"    {ConfigCollectionList.ColClosed.DbName} = ",
                        $"{Functions.PrepBoolArg(arg: Closed)},{Environment.NewLine}",                                              // 15
                        $"    {ConfigCollectionList.ColEditDate.DbName} = ",
                        $"pg_catalog.now(),{Environment.NewLine}",                                                                  // 16
                        $"    {ConfigCollectionList.ColCondition.DbName} = ",
                        $"{Functions.PrepStringArg(arg: Condition, replaceEmptyStringByNull: true)},{Environment.NewLine}",         // 17
                        $"    {ConfigCollectionList.ColTotalPoints.DbName} = ",
                        $"{Functions.PrepNIntArg(arg: TotalPoints)},{Environment.NewLine}",                                         // 18
                        $"    {ConfigCollectionList.ColRefLayerPointsId.DbName} = ",
                        $"{Functions.PrepNIntArg(arg: RefLayerPointsId)},{Environment.NewLine}",                                    // 19
                        $"    {ConfigCollectionList.ColRefTotalId.DbName} = ",
                        $"{Functions.PrepNIntArg(arg: RefTotalId)},{Environment.NewLine}",                                          // 20
                        $"    {ConfigCollectionList.ColEditUser.DbName} = ",
                        $"pg_catalog.current_user(){Environment.NewLine}",                                                          // 21
                        $"WHERE{Environment.NewLine}",
                        $"    ({ConfigCollectionList.ColId.DbName} = {Functions.PrepIntArg(arg: Id)});{Environment.NewLine}"        //  0
                        ));

                    Composite.Database.Postgres.ExecuteList(
                        sqlCommands: commands);

                    Composite.ReLoad();
                }


                /// <summary>
                /// Is it possible to delete this configuration collection?
                /// </summary>
                /// <param name="status">Configuration collection status</param>
                /// <returns>true|false</returns>
                public bool TryDelete(out ConfigCollectionStatus status)
                {
                    if (ContainsDependentConfigCollections)
                    {
                        // "Skupinu konfigurací nelze smazat, protože se na ni odkazují další skupiny konfigurací."
                        // "Configuration collection cannot be deleted because it is referenced by other configuration collections."
                        status = ConfigCollectionStatus.ContainsDependentConfigCollections;
                        return false;
                    }

                    if (ContainsCalculatedAuxTotals)
                    {
                        // "Skupinu konfigurací nelze smazat, protože obsahuje konfigurace s vypočtenými úhrny pomocné proměnné."
                        // "Configuration collection cannot be deleted because it contains configurations with calculated auxiliary variable totals."
                        status = ConfigCollectionStatus.ContainsCalculatedAuxTotals;
                        return false;
                    }

                    if (ContainsCalculatedAuxData)
                    {
                        // "Skupinu konfigurací nelze smazat, protože obsahuje konfigurace s provedeným protínáním bodové vrstvy s vrstvou úhrnů pomocné proměnné."
                        // "Configuration collection cannot be deleted because it contains configurations with calculated intersections between point layer and auxiliary variable totals layer."
                        status = ConfigCollectionStatus.ContainsCalculatedAuxData;
                        return false;
                    }

                    if (!IsEmpty)
                    {
                        // "Skupinu konfigurací nelze smazat, protože není prázdná."
                        // "Configuration collection cannot be deleted because it is not empty."
                        status = ConfigCollectionStatus.IsNotEmpty;
                        return false;
                    }

                    if (Closed)
                    {
                        // "Skupinu konfigurací nelze smazat, protože je uzavřená."
                        // "Configuration collection cannot be deleted because it is closed."
                        status = ConfigCollectionStatus.IsClosed;
                        return false;
                    }

                    status = ConfigCollectionStatus.None;
                    return true;
                }

                /// <summary>
                /// Delete configuration collection
                /// </summary>
                public void Delete()
                {
                    if (!TryDelete(status: out ConfigCollectionStatus _))
                    {
                        return;
                    }

                    List<string> commands = [];

                    if (IsPointLayer)
                    {
                        // Pro bodové vrstvy je nutné smazat také body z tabulky f_p_plot
                        commands.Add(
                            item: String.Concat(
                                $"DELETE{Environment.NewLine}",
                                $"FROM{Environment.NewLine}",
                                $"    {PlotList.Schema}.{PlotList.Name}{Environment.NewLine}",
                                $"WHERE{Environment.NewLine}",
                                $"    ({PlotList.ColConfigCollectionId.DbName} = {Id});{Environment.NewLine}"
                                ));
                    }

                    commands.Add(
                        item: String.Concat(
                                $"DELETE{Environment.NewLine}",
                                $"FROM{Environment.NewLine}",
                                $"    {ConfigCollectionList.Schema}.{ConfigCollectionList.Name}{Environment.NewLine}",
                                $"WHERE{Environment.NewLine}",
                                $"    ({ConfigCollectionList.ColId.DbName} = {Id});{Environment.NewLine}"
                                ));

                    Composite.Database.Postgres.ExecuteList(
                        sqlCommands: commands);

                    if (ConfigFunctionValue == ConfigFunctionEnum.PointLayer)
                    {
                        ((NfiEstaDB)Composite.Database).SAuxiliaryData.FpPlot.ReLoad();
                    }

                    Composite.ReLoad();
                }


                /// <summary>
                /// Is it possible to close this configuration collection?
                /// </summary>
                /// <param name="status">Configuration collectioin status</param>
                /// <returns>true|false</returns>
                public bool TryClose(out ConfigCollectionStatus status)
                {
                    if (IsForTotals && IsEmpty)
                    {
                        // "Skupinu konfigurací nelze uzavřít, protože je prázdná."
                        // "Configuration collection cannot be closed because it is empty."
                        status = ConfigCollectionStatus.IsEmpty;
                        return false;
                    }

                    if (Closed)
                    {
                        // "Skupinu konfigurací nelze uzavřít, protože už je uzavřená."
                        // "Configuration collection cannot be closed because it is already closed."
                        status = ConfigCollectionStatus.IsClosed;
                        return false;
                    }

                    status = ConfigCollectionStatus.None;
                    return true;
                }

                /// <summary>
                /// Close config collection for editing and deleting
                /// </summary>
                [SupportedOSPlatform("windows")]
                public void Close()
                {
                    if (!TryClose(status: out ConfigCollectionStatus _))
                    {
                        return;
                    }

                    List<string> commands = [];

                    if (IsPointLayer)
                    {
                        // Vymazání starých záznamů bodové vrstvy
                        commands.Add(
                            item: String.Concat(
                                $"DELETE{Environment.NewLine}",
                                $"FROM{Environment.NewLine}",
                                $"    {PlotList.Schema}.{PlotList.Name}{Environment.NewLine}",
                                $"WHERE{Environment.NewLine}",
                                $"    ({PlotList.ColConfigCollectionId.DbName} = {Id});{Environment.NewLine}"
                                ));

                        // Vložení nových záznamů bodové vrstvy
                        PointAdditionalColumns info = PointAdditionalColumns.Deserialize(json: Tag);

                        string colGid = String.Concat(
                             $"(SELECT max({PlotList.ColId.Name}) FROM {PlotList.Schema}.{PlotList.Name})",
                             $" + row_number() over() AS {PlotList.ColId.Name},");

                        string colConfigCollectionId = String.IsNullOrEmpty(value: Id.ToString())
                                   ? Functions.StrNull
                                   : Id.ToString();
                        colConfigCollectionId = $"{colConfigCollectionId}::{PlotList.ColConfigCollectionId.NewDataType} AS {PlotList.ColConfigCollectionId.DbName},{Environment.NewLine}";

                        string colCountry = String.IsNullOrEmpty(value: info.Country)
                                  ? Functions.StrNull
                                  : info.Country;
                        colCountry = $"{colCountry}::{PlotList.ColCountry.NewDataType} AS {PlotList.ColCountry.DbName},{Environment.NewLine}";

                        string colStrataSet = String.IsNullOrEmpty(value: info.StrataSet)
                                   ? Functions.StrNull
                                   : info.StrataSet;
                        colStrataSet = $"{colStrataSet}::{PlotList.ColStrataSet.NewDataType} AS {PlotList.ColStrataSet.DbName},{Environment.NewLine}";

                        string colStratum = String.IsNullOrEmpty(value: info.Stratum)
                                     ? Functions.StrNull
                                     : info.Stratum;
                        colStratum = $"{colStratum}::{PlotList.ColStratum.NewDataType} AS {PlotList.ColStratum.DbName},{Environment.NewLine}";

                        string colPanel = String.IsNullOrEmpty(value: info.Panel)
                                   ? Functions.StrNull
                                   : info.Panel;
                        colPanel = $"{colPanel}::{PlotList.ColPanel.NewDataType} AS {PlotList.ColPanel.DbName},{Environment.NewLine}";

                        string colCluster = String.IsNullOrEmpty(value: info.Cluster)
                                   ? Functions.StrNull
                                   : info.Cluster;
                        colCluster = $"{colCluster}::{PlotList.ColCluster.NewDataType} AS {PlotList.ColCluster.DbName},{Environment.NewLine}";

                        string colPlot = String.IsNullOrEmpty(value: info.Plot)
                                  ? Functions.StrNull
                                  : info.Plot;
                        colPlot = $"{colPlot}::{PlotList.ColPlot.NewDataType} AS {PlotList.ColPlot.DbName},{Environment.NewLine}";

                        string colGeom = String.IsNullOrEmpty(value: ColumnName)
                                   ? Functions.StrNull
                                   : ColumnName;
                        colGeom = $"{colGeom}::{PlotList.ColGeom.NewDataType} AS {PlotList.ColGeom.DbName}{Environment.NewLine}";

                        string filter = String.IsNullOrEmpty(value: Condition)
                            ? String.Empty
                            : String.Concat(
                                $"    WHERE{Environment.NewLine}",
                                $"        {Condition}{Environment.NewLine}");

                        commands.Add(item: String.Concat(
                            $"WITH w_point_layer AS{Environment.NewLine}",
                            $"({Environment.NewLine}",
                            $"    SELECT{Environment.NewLine}",
                            $"        {colGid}",
                            $"        {colConfigCollectionId}",
                            $"        {colCountry}",
                            $"        {colStrataSet}",
                            $"        {colStratum}",
                            $"        {colPanel}",
                            $"        {colCluster}",
                            $"        {colPlot}",
                            $"        {colGeom}",
                            $"    FROM{Environment.NewLine}",
                            $"        {SchemaName}.{TableName}{Environment.NewLine}",
                            $"{filter}",
                            $"){Environment.NewLine}",
                            $"INSERT INTO{Environment.NewLine}",
                            $"    {PlotList.Schema}.{PlotList.Name}{Environment.NewLine}",
                            $"    ({Environment.NewLine}",
                            $"        {PlotList.ColId.DbName},{Environment.NewLine}",
                            $"        {PlotList.ColConfigCollectionId.DbName},{Environment.NewLine}",
                            $"        {PlotList.ColCountry.DbName},{Environment.NewLine}",
                            $"        {PlotList.ColStrataSet.DbName},{Environment.NewLine}",
                            $"        {PlotList.ColStratum.DbName},{Environment.NewLine}",
                            $"        {PlotList.ColPanel.DbName},{Environment.NewLine}",
                            $"        {PlotList.ColCluster.DbName},{Environment.NewLine}",
                            $"        {PlotList.ColPlot.DbName},{Environment.NewLine}",
                            $"        {PlotList.ColGeom.DbName}{Environment.NewLine}",
                            $"    ){Environment.NewLine}",
                            $"SELECT{Environment.NewLine}",
                            $"    {PlotList.Alias}.{PlotList.ColId.DbName},{Environment.NewLine}",
                            $"    {PlotList.Alias}.{PlotList.ColConfigCollectionId.DbName},{Environment.NewLine}",
                            $"    {PlotList.Alias}.{PlotList.ColCountry.DbName},{Environment.NewLine}",
                            $"    {PlotList.Alias}.{PlotList.ColStrataSet.DbName},{Environment.NewLine}",
                            $"    {PlotList.Alias}.{PlotList.ColStratum.DbName},{Environment.NewLine}",
                            $"    {PlotList.Alias}.{PlotList.ColPanel.DbName},{Environment.NewLine}",
                            $"    {PlotList.Alias}.{PlotList.ColCluster.DbName},{Environment.NewLine}",
                            $"    {PlotList.Alias}.{PlotList.ColPlot.DbName},{Environment.NewLine}",
                            $"    {PlotList.Alias}.{PlotList.ColGeom.DbName}{Environment.NewLine}",
                            $"FROM w_point_layer AS {PlotList.Alias};{Environment.NewLine}"
                            ));
                    }

                    commands.Add(
                       item: String.Concat(
                            $"UPDATE {ConfigCollectionList.Schema}.{ConfigCollectionList.Name}{Environment.NewLine}",
                            $"SET{Environment.NewLine}",
                            $"    {ConfigCollectionList.ColClosed.DbName} = {Functions.PrepBoolArg(arg: true)},{Environment.NewLine}",
                            $"    {ConfigCollectionList.ColEditDate.DbName} = now(){Environment.NewLine}",
                            $"WHERE{Environment.NewLine}",
                            $"    ({ConfigCollectionList.ColId.DbName} = {Functions.PrepIntArg(arg: Id)});{Environment.NewLine}"
                        ));

                    Composite.Database.Postgres.ExecuteList(
                        sqlCommands: commands);

                    if (IsPointLayer)
                    {
                        ((NfiEstaDB)Composite.Database).SAuxiliaryData.FpPlot.ReLoad();
                    }

                    Composite.ReLoad();
                }


                /// <summary>
                /// Is it possible to open this configuration collection?
                /// </summary>
                /// <param name="status">Configuration collectioin status</param>
                /// <returns>true|false</returns>
                public bool TryOpen(out ConfigCollectionStatus status)
                {
                    if (ContainsDependentConfigCollections)
                    {
                        // "Skupinu konfigurací nelze otevřít, protože se na ni odkazují další skupiny konfigurací."
                        // "Configuration collection cannot be opened because it is referenced by other configuration collections."
                        status = ConfigCollectionStatus.ContainsDependentConfigCollections;
                        return false;
                    }

                    if (ContainsCalculatedAuxTotals)
                    {
                        // "Skupinu konfigurací nelze otevřít, protože obsahuje konfigurace s vypočtenými úhrny pomocné proměnné."
                        // "Configuration collection cannot be opened because it contains configurations with calculated auxiliary variable totals."
                        status = ConfigCollectionStatus.ContainsCalculatedAuxTotals;
                        return false;
                    }

                    if (ContainsCalculatedAuxData)
                    {
                        // "Skupinu konfigurací nelze otevřít, protože obsahuje konfigurace s provedeným protínáním bodové vrstvy s vrstvou úhrnů pomocné proměnné."
                        // "Configuration collection cannot be opened because it contains configurations with calculated intersections between point layer and auxiliary variable totals layer."
                        status = ConfigCollectionStatus.ContainsCalculatedAuxData;
                        return false;
                    }

                    if (!Closed)
                    {
                        // "Skupinu konfigurací nelze otevřít, protože už je otevřená."
                        // "Configuration collection cannot be opened because it is opened."
                        status = ConfigCollectionStatus.IsOpened;
                        return false;
                    }

                    status = ConfigCollectionStatus.None;
                    return true;
                }

                /// <summary>
                /// Open config collection for editing and deleting
                /// </summary>
                public void Open()
                {
                    if (!TryOpen(status: out ConfigCollectionStatus _))
                    {
                        return;
                    }

                    List<string> commands = [];

                    commands.Add(
                       item: String.Concat(
                            $"UPDATE {ConfigCollectionList.Schema}.{ConfigCollectionList.Name}{Environment.NewLine}",
                            $"SET{Environment.NewLine}",
                            $"    {ConfigCollectionList.ColClosed.DbName} = {Functions.PrepBoolArg(arg: false)},{Environment.NewLine}",
                            $"    {ConfigCollectionList.ColEditDate.DbName} = now(){Environment.NewLine}",
                            $"WHERE{Environment.NewLine}",
                            $"    ({ConfigCollectionList.ColId.DbName} = {Functions.PrepIntArg(arg: Id)});{Environment.NewLine}"
                        ));

                    Composite.Database.Postgres.ExecuteList(
                        sqlCommands: commands);

                    Composite.ReLoad();
                }


                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not ConfigCollection Type, return False
                    if (obj is not ConfigCollection)
                    {
                        return false;
                    }

                    return
                        Id == ((ConfigCollection)obj).Id;
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                #endregion Methods

            }


            /// <summary>
            /// List of configuration collections
            /// </summary>
            public class ConfigCollectionList
                : ALookupTable
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema = ADSchema.Name;

                /// <summary>
                /// Table name
                /// </summary>
                public const string Name = "t_config_collection";

                /// <summary>
                /// Table alias
                /// </summary>
                public const string Alias = "t_config_collection";

                /// <summary>
                /// Description in national language
                /// </summary>
                public const string CaptionCs = "Seznam skupin konfigurací výpočtu úhrnů pomocných proměnných";

                /// <summary>
                /// Description in English
                /// </summary>
                public const string CaptionEn = "List of configuration collections";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
                {
                    { "id", new ColumnMetadata()
                    {
                        Name = "id",
                        DbName = "id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = true,
                        NotNull = true,
                        DefaultValue = "0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ID",
                        HeaderTextEn = "ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 0
                    }
                    },
                    { "auxiliary_variable_id", new ColumnMetadata()
                    {
                        Name = "auxiliary_variable_id",
                        DbName = "auxiliary_variable",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "AUXILIARY_VARIABLE_ID",
                        HeaderTextEn = "AUXILIARY_VARIABLE_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 1
                    }
                    },
                    { "config_function_id", new ColumnMetadata()
                    {
                        Name = "config_function_id",
                        DbName = "config_function",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = "0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "CONFIG_FUNCTION_ID",
                        HeaderTextEn = "CONFIG_FUNCTION_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 2
                    }
                    },
                    { "label_cs", new ColumnMetadata()
                    {
                        Name = "label_cs",
                        DbName = "label",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "LABEL_CS",
                        HeaderTextEn = "LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 3
                    }
                    },
                    { "label_en", new ColumnMetadata()
                    {
                        Name = "label_en",
                        DbName = "label_en",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "LABEL_EN",
                        HeaderTextEn = "LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 4
                    }
                    },
                    { "aggregated", new ColumnMetadata()
                    {
                        Name = "aggregated",
                        DbName = "aggregated",
                        DataType = "System.Boolean",
                        DbDataType = "bool",
                        NewDataType = "bool",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = "false",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "AGGREGATED",
                        HeaderTextEn = "AGGREGATED",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 5
                    }
                    },
                    { "catalog_name", new ColumnMetadata()
                    {
                        Name = "catalog_name",
                        DbName = "catalog_name",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "CATALOG_NAME",
                        HeaderTextEn = "CATALOG_NAME",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 6
                    }
                    },
                    { "schema_name", new ColumnMetadata()
                    {
                        Name = "schema_name",
                        DbName = "schema_name",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "SCHEMA_NAME",
                        HeaderTextEn = "SCHEMA_NAME",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 7
                    }
                    },
                    { "table_name", new ColumnMetadata()
                    {
                        Name = "table_name",
                        DbName = "table_name",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "TABLE_NAME",
                        HeaderTextEn = "TABLE_NAME",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 8
                    }
                    },
                    { "column_ident", new ColumnMetadata()
                    {
                        Name = "column_ident",
                        DbName = "column_ident",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "COLUMN_IDENT",
                        HeaderTextEn = "COLUMN_IDENT",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 9
                    }
                    },
                    { "column_name", new ColumnMetadata()
                    {
                        Name = "column_name",
                        DbName = "column_name",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "COLUMN_NAME",
                        HeaderTextEn = "COLUMN_NAME",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 10
                    }
                    },
                    { "unit", new ColumnMetadata()
                    {
                        Name = "unit",
                        DbName = "unit",
                        DataType = "System.Double",
                        DbDataType = "float8",
                        NewDataType = "float8",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "N5",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "UNIT",
                        HeaderTextEn = "UNIT",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 11
                    }
                    },
                    { "tag", new ColumnMetadata()
                    {
                        Name = "tag",
                        DbName = "tag",
                        DataType = "System.String",
                        DbDataType = "text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "TAG",
                        HeaderTextEn = "TAG",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 12
                    }
                    },
                    { "description_cs", new ColumnMetadata()
                    {
                        Name = "description_cs",
                        DbName = "description",
                        DataType = "System.String",
                        DbDataType = "text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "DESCRIPTION_CS",
                        HeaderTextEn = "DESCRIPTION_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 13
                    }
                    },
                    { "description_en", new ColumnMetadata()
                    {
                        Name = "description_en",
                        DbName = "description_en",
                        DataType = "System.String",
                        DbDataType = "text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "DESCRIPTION_EN",
                        HeaderTextEn = "DESCRIPTION_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 14
                    }
                    },
                    { "closed", new ColumnMetadata()
                    {
                        Name = "closed",
                        DbName = "closed",
                        DataType = "System.Boolean",
                        DbDataType = "bool",
                        NewDataType = "bool",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = "false",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "CLOSED",
                        HeaderTextEn = "CLOSED",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 15
                    }
                    },
                    { "edit_date", new ColumnMetadata()
                    {
                        Name = "edit_date",
                        DbName = "edit_date",
                        DataType = "System.DateTime",
                        DbDataType = "timestamp",
                        NewDataType = "timestamp",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = new DateTime().ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "yyyy-MM-dd HH:mm:ss",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "EDIT_DATE",
                        HeaderTextEn = "EDIT_DATE",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 16
                    }
                    },
                    { "condition", new ColumnMetadata()
                    {
                        Name = "condition",
                        DbName = "condition",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "CONDITION",
                        HeaderTextEn = "CONDITION",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 17
                    }
                    },
                    { "total_points", new ColumnMetadata()
                    {
                        Name = "total_points",
                        DbName = "total_points",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "TOTAL_POINTS",
                        HeaderTextEn = "TOTAL_POINTS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 18
                    }
                    },
                    { "ref_id_layer_points", new ColumnMetadata()
                    {
                        Name = "ref_id_layer_points",
                        DbName = "ref_id_layer_points",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "REF_ID_LAYER_POINTS",
                        HeaderTextEn = "REF_ID_LAYER_POINTS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 19
                    }
                    },
                    { "ref_id_total", new ColumnMetadata()
                    {
                        Name = "ref_id_total",
                        DbName = "ref_id_total",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "REF_ID_TOTAL",
                        HeaderTextEn = "REF_ID_TOTAL",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 20
                    }
                    },
                    { "edit_user", new ColumnMetadata()
                    {
                        Name = "edit_user",
                        DbName = "edit_user",
                        DataType = "System.String",
                        DbDataType = "name",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "EDIT_USER",
                        HeaderTextEn = "EDIT_USER",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 21
                    }
                    }
                };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column auxiliary_variable_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColAuxiliaryVariableId = Cols["auxiliary_variable_id"];

                /// <summary>
                /// Column config_function_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColConfigFunctionId = Cols["config_function_id"];

                /// <summary>
                /// Column label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColLabelCs = Cols["label_cs"];

                /// <summary>
                /// Column label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColLabelEn = Cols["label_en"];

                /// <summary>
                /// Column aggregated metadata
                /// </summary>
                public static readonly ColumnMetadata ColAggregated = Cols["aggregated"];

                /// <summary>
                /// Column catalog_name metadata
                /// </summary>
                public static readonly ColumnMetadata ColCatalogName = Cols["catalog_name"];

                /// <summary>
                /// Column schema_name metadata
                /// </summary>
                public static readonly ColumnMetadata ColSchemaName = Cols["schema_name"];

                /// <summary>
                /// Column table_name metadata
                /// </summary>
                public static readonly ColumnMetadata ColTableName = Cols["table_name"];

                /// <summary>
                /// Column column_ident metadata
                /// </summary>
                public static readonly ColumnMetadata ColColumnIdent = Cols["column_ident"];

                /// <summary>
                /// Column column_name metadata
                /// </summary>
                public static readonly ColumnMetadata ColColumnName = Cols["column_name"];

                /// <summary>
                /// Column unit metadata
                /// </summary>
                public static readonly ColumnMetadata ColUnit = Cols["unit"];

                /// <summary>
                /// Column tag metadata
                /// </summary>
                public static readonly ColumnMetadata ColTag = Cols["tag"];

                /// <summary>
                /// Column description_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColDescriptionCs = Cols["description_cs"];

                /// <summary>
                /// Column description_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColDescriptionEn = Cols["description_en"];

                /// <summary>
                /// Column closed metadata
                /// </summary>
                public static readonly ColumnMetadata ColClosed = Cols["closed"];

                /// <summary>
                /// Column edit_date metadata
                /// </summary>
                public static readonly ColumnMetadata ColEditDate = Cols["edit_date"];

                /// <summary>
                /// Column condition metadata
                /// </summary>
                public static readonly ColumnMetadata ColCondition = Cols["condition"];

                /// <summary>
                /// Column total_points metadata
                /// </summary>
                public static readonly ColumnMetadata ColTotalPoints = Cols["total_points"];

                /// <summary>
                /// Column ref_id_layer_points metadata
                /// </summary>
                public static readonly ColumnMetadata ColRefLayerPointsId = Cols["ref_id_layer_points"];

                /// <summary>
                /// Column ref_id_total metadata
                /// </summary>
                public static readonly ColumnMetadata ColRefTotalId = Cols["ref_id_total"];

                /// <summary>
                /// Column edit_user metadata
                /// </summary>
                public static readonly ColumnMetadata ColEditUser = Cols["edit_user"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public ConfigCollectionList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public ConfigCollectionList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// List of configuration collections (read-only)
                /// </summary>
                public List<ConfigCollection> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new ConfigCollection(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// Configuration collection from list by identifier (read-only)
                /// </summary>
                /// <param name="id">Configuration collection identifier</param>
                /// <returns>Configuration collection from list by identifier (null if not found)</returns>
                public ConfigCollection this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new ConfigCollection(composite: this, data: a))
                                .FirstOrDefault<ConfigCollection>();
                    }
                }

                #endregion Indexer


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public ConfigCollectionList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new ConfigCollectionList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new ConfigCollectionList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                /// <summary>
                /// Creates new item with default parameters
                /// </summary>
                /// <returns>Returns new item with default parameters</returns>
                public ConfigCollection CreateNewItem()
                {
                    ConfigCollection newConfigCollection =
                        new(composite: this,
                            data: Data.NewRow())
                        {
                            Id = (Items.Count > 0)
                            ? Items.Select(a => a.Id).Max() + 1
                            : 1
                        };
                    newConfigCollection.FullReset();

                    return newConfigCollection;
                }

                #endregion Methods

            }


            /// <summary>
            /// Configuration collection status
            /// </summary>
            public enum ConfigCollectionStatus
            {
                /// <summary>
                /// None
                /// </summary>
                None = 0,

                /// <summary>
                /// List of dependent configuration collections is not empty
                /// </summary>
                ContainsDependentConfigCollections = 1,

                /// <summary>
                /// Configuration collection contains some calculated auxiliary variable totals
                /// </summary>
                ContainsCalculatedAuxTotals = 2,

                /// <summary>
                /// Configuration collection contains some calculated auxiliary variable values
                /// </summary>
                ContainsCalculatedAuxData = 3,

                /// <summary>
                /// List of configurations in this collection is empty
                /// </summary>
                IsEmpty = 4,

                /// <summary>
                /// List of configurations in this collection is not empty
                /// </summary>
                IsNotEmpty = 5,

                /// <summary>
                /// Configuration collection is closed for editations
                /// </summary>
                IsClosed = 6,

                /// <summary>
                /// Configuration collection is opened for editations
                /// </summary>
                IsOpened = 7
            }

        }
    }
}