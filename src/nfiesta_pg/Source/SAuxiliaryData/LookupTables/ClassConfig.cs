﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace AuxiliaryData
        {
            // t_config

            /// <summary>
            /// Configuration for the auxiliary variable totals calculation
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class Config(
                ConfigList composite = null,
                DataRow data = null)
                    : ALookupTableEntry<ConfigList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Configuration for the auxiliary variable totals calculation identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: ConfigList.ColId.Name,
                            defaultValue: Int32.Parse(s: ConfigList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: ConfigList.ColId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Auxiliary variable category identifier
                /// </summary>
                public Nullable<int> AuxiliaryVariableCategoryId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: ConfigList.ColAuxiliaryVariableCategoryId.Name,
                            defaultValue: String.IsNullOrEmpty(value: ConfigList.ColAuxiliaryVariableCategoryId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: ConfigList.ColAuxiliaryVariableCategoryId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: ConfigList.ColAuxiliaryVariableCategoryId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Auxiliary variable category object (read-only)
                /// </summary>
                public Core.AuxiliaryVariableCategory AuxiliaryVariableCategory
                {
                    get
                    {
                        return (AuxiliaryVariableCategoryId != null) ?
                             ((NfiEstaDB)Composite.Database).SNfiEsta.CAuxiliaryVariableCategory
                            [(int)AuxiliaryVariableCategoryId] : null;
                    }
                }


                /// <summary>
                /// Configuration collection identifier
                /// </summary>
                public int ConfigCollectionId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: ConfigList.ColConfigCollectionId.Name,
                            defaultValue: Int32.Parse(s: ConfigList.ColConfigCollectionId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: ConfigList.ColConfigCollectionId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Configuration collection object (read-only)
                /// </summary>
                public ConfigCollection ConfigCollection
                {
                    get
                    {
                        return ((NfiEstaDB)Composite.Database).SAuxiliaryData.TConfigCollection[(int)ConfigCollectionId];
                    }
                }


                /// <summary>
                /// Variant of the auxiliary variables totals calculation identifier
                /// </summary>
                public int ConfigQueryId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: ConfigList.ColConfigQueryId.Name,
                            defaultValue: Int32.Parse(s: ConfigList.ColConfigQueryId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: ConfigList.ColConfigQueryId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Variant of the auxiliary variables totals calculation (read-only)
                /// </summary>
                public ConfigQueryEnum ConfigQueryValue
                {
                    get
                    {
                        return (ConfigQueryEnum)ConfigQueryId;
                    }
                }

                /// <summary>
                /// Variant of the auxiliary variables totals calculation object (read-only)
                /// </summary>
                public ConfigQuery ConfigQuery
                {
                    get
                    {
                        return ((NfiEstaDB)Composite.Database).SAuxiliaryData.CConfigQuery[(int)ConfigQueryId];
                    }
                }


                /// <summary>
                /// Label of configuration in national language
                /// </summary>
                public override string LabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: ConfigList.ColLabelCs.Name,
                            defaultValue: ConfigList.ColLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: ConfigList.ColLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Description of configuration in national language
                /// </summary>
                public override string DescriptionCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: ConfigList.ColDescriptionCs.Name,
                            defaultValue: ConfigList.ColDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: ConfigList.ColDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Extended label of configuration in national language (read-only)
                /// </summary>
                public override string ExtendedLabelCs
                {
                    get
                    {
                        return String.IsNullOrEmpty(value: LabelCs)
                            ? $"{Id}"
                            : $"{Id} - {LabelCs}";
                    }
                }


                /// <summary>
                /// Label of configuration in English
                /// </summary>
                public override string LabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: ConfigList.ColLabelEn.Name,
                            defaultValue: ConfigList.ColLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: ConfigList.ColLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Description of configuration in English
                /// </summary>
                public override string DescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: ConfigList.ColDescriptionEn.Name,
                            defaultValue: ConfigList.ColDescriptionEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: ConfigList.ColDescriptionEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Extended label of configuration in English (read-only)
                /// </summary>
                public override string ExtendedLabelEn
                {
                    get
                    {
                        return String.IsNullOrEmpty(value: LabelEn)
                           ? $"{Id}"
                           : $"{Id} - {LabelEn}";
                    }
                }


                /// <summary>
                /// List of categories of auxiliary variables
                /// forming the whole territory for the calculation (as text)
                /// </summary>
                public string Complete
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: ConfigList.ColComplete.Name,
                            defaultValue: ConfigList.ColComplete.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: ConfigList.ColComplete.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// List of categories of auxiliary variables
                /// forming the whole territory for the calculation (as list of identifiers)
                /// </summary>
                public List<int> CompleteList
                {
                    get
                    {
                        if (String.IsNullOrEmpty(Complete))
                        {
                            return [];
                        }

                        return
                            Functions.StringToIntList(
                                text: Complete,
                                separator: ',',
                                defaultValue: null);
                    }
                    set
                    {
                        if (value == null)
                        {
                            Complete = null;
                        }

                        if (value.Count == 0)
                        {
                            Complete = null;
                        }

                        Complete =
                            value
                            .Select(a => a.ToString())
                            .Aggregate((a, b) => $"{a},{b}");
                    }
                }

                /// <summary>
                /// List of categories of auxiliary variables
                /// forming the whole territory for the calculation (as list of configurations) (read-only)
                /// </summary>
                public List<Config> CompleteConfigurations
                {
                    get
                    {
                        return
                            CompleteList
                                .Select(id =>
                                    ((NfiEstaDB)Composite.Database).SAuxiliaryData.TConfig[id])
                                .Where(a => a != null)
                                .ToList<Config>();
                    }
                }


                /// <summary>
                /// List of categories of auxiliary variables (as text)
                /// </summary>
                public string Categories
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: ConfigList.ColCategories.Name,
                            defaultValue: ConfigList.ColCategories.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: ConfigList.ColCategories.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// List of categories of auxiliary variables (as list of identifiers)
                /// </summary>
                public List<int> CategoriesList
                {
                    get
                    {
                        if (String.IsNullOrEmpty(Categories))
                        {
                            return [];
                        }

                        return
                            Functions.StringToIntList(
                                text: Categories,
                                separator: ',',
                                defaultValue: null);
                    }
                    set
                    {
                        if (value == null)
                        {
                            Categories = null;
                        }

                        if (value.Count == 0)
                        {
                            Categories = null;
                        }

                        Categories =
                            value
                            .Select(a => a.ToString())
                            .Aggregate((a, b) => $"{a},{b}");
                    }
                }

                /// <summary>
                /// List of categories of auxiliary variables as configuration objects (as list of configurations) (read-only)
                /// </summary>
                public List<Config> CategoriesConfigurations
                {
                    get
                    {
                        return
                            CategoriesList
                                .Select(id =>
                                    ((NfiEstaDB)Composite.Database).SAuxiliaryData.TConfig[id])
                                .Where(a => a != null)
                                .ToList<Config>();
                    }
                }


                /// <summary>
                /// Configuration of the vector layer to calculate interactions - identifier
                /// </summary>
                public Nullable<int> VectorId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: ConfigList.ColVector.Name,
                            defaultValue: String.IsNullOrEmpty(value: ConfigList.ColVector.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: ConfigList.ColVector.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: ConfigList.ColVector.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Configuration of the vector layer to calculate interactions - object (read-only)
                /// </summary>
                public Config Vector
                {
                    get
                    {
                        return (VectorId != null) ?
                             ((NfiEstaDB)Composite.Database).SAuxiliaryData.TConfig
                            [(int)VectorId] : null;
                    }
                }


                /// <summary>
                /// Configuration of the first raster layer layer to calculate interactions - identifier
                /// </summary>
                public Nullable<int> RasterAId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: ConfigList.ColRasterA.Name,
                            defaultValue: String.IsNullOrEmpty(value: ConfigList.ColRasterA.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: ConfigList.ColRasterA.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: ConfigList.ColRasterA.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Configuration of the first raster layer layer to calculate interactions - object (read-only)
                /// </summary>
                public Config RasterA
                {
                    get
                    {
                        return (RasterAId != null) ?
                             ((NfiEstaDB)Composite.Database).SAuxiliaryData.TConfig
                            [(int)RasterAId] : null;
                    }
                }


                /// <summary>
                /// Configuration of the second raster layer layer to calculate interactions - identifier
                /// </summary>
                public Nullable<int> RasterBId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: ConfigList.ColRasterB.Name,
                            defaultValue: String.IsNullOrEmpty(value: ConfigList.ColRasterB.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: ConfigList.ColRasterB.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: ConfigList.ColRasterB.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Configuration of the second raster layer layer to calculate interactions object (read-only)
                /// </summary>
                public Config RasterB
                {
                    get
                    {
                        return (RasterBId != null) ?
                             ((NfiEstaDB)Composite.Database).SAuxiliaryData.TConfig
                            [(int)RasterBId] : null;
                    }
                }


                /// <summary>
                /// Raster band identifier
                /// </summary>
                public Nullable<int> Band
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: ConfigList.ColBand.Name,
                            defaultValue: String.IsNullOrEmpty(value: ConfigList.ColBand.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: ConfigList.ColBand.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: ConfigList.ColBand.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Raster reclassification value
                /// </summary>
                public Nullable<int> Reclass
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: ConfigList.ColReclass.Name,
                            defaultValue: String.IsNullOrEmpty(value: ConfigList.ColReclass.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: ConfigList.ColReclass.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: ConfigList.ColReclass.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Selection condition from GIS layer
                /// </summary>
                public string Condition
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: ConfigList.ColCondition.Name,
                            defaultValue: ConfigList.ColCondition.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: ConfigList.ColCondition.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Additional information
                /// </summary>
                public string Tag
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: ConfigList.ColTag.Name,
                            defaultValue: ConfigList.ColTag.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: ConfigList.ColTag.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Last change date
                /// </summary>
                public DateTime EditDate
                {
                    get
                    {
                        return Functions.GetDateTimeArg(
                            row: Data,
                            name: ConfigList.ColEditDate.Name,
                            defaultValue: DateTime.Parse(s: ConfigList.ColEditDate.DefaultValue));
                    }
                    set
                    {
                        Functions.SetDateTimeArg(
                            row: Data,
                            name: ConfigList.ColEditDate.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Last change date as text
                /// </summary>
                public string EditDateText
                {
                    get
                    {
                        return
                            EditDate.ToString(
                                format: ConfigList.ColEditDate.NumericFormat);
                    }
                }

                /// <summary>
                /// User
                /// </summary>
                public string EditUser
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: ConfigList.ColEditUser.Name,
                            defaultValue: ConfigList.ColEditUser.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: ConfigList.ColEditUser.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// SQL command to calculate auxiliary variable totals
                /// </summary>
                public string Command
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: ConfigList.ColCommand.Name,
                            defaultValue: ConfigList.ColCommand.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: ConfigList.ColCommand.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// List of dependent configurations
                /// </summary>
                public List<Config> DependentConfigurations
                {
                    get
                    {
                        return
                            ((NfiEstaDB)Composite.Database).SAuxiliaryData.TConfig.Items
                            .Where(a => a.Id != Id)
                            .Where(a =>
                                ((a.VectorId ?? 0) == Id) ||
                                ((a.RasterAId ?? 0) == Id) ||
                                ((a.RasterBId ?? 0) == Id) ||
                                a.CompleteList.Contains(item: Id) ||
                                a.CategoriesList.Contains(item: Id))
                            .Distinct<Config>()
                            .ToList<Config>();
                    }
                }

                /// <summary>
                /// List of dependent configuration is not empty
                /// </summary>
                public bool ContainsDependentConfigurations
                {
                    get
                    {
                        return DependentConfigurations.Count != 0;
                    }
                }


                /// <summary>
                /// Number of calculated auxiliary variable totals in configuration
                /// </summary>
                public long AuxTotalCount
                {
                    get
                    {
                        return
                            ((NfiEstaDB)Composite.Database).SAuxiliaryData.VAuxTotalCount
                                .AuxTotalCount(config: this);
                    }
                }

                /// <summary>
                /// Configuration contains some calculated auxiliary variable totals
                /// </summary>
                public bool ContainsCalculatedAuxTotals
                {
                    get
                    {
                        return AuxTotalCount != 0;
                    }
                }


                /// <summary>
                /// Number of calculated auxiliary variable values in configuration
                /// </summary>
                public long AuxDataCount
                {
                    get
                    {
                        return
                            ((NfiEstaDB)Composite.Database).SAuxiliaryData.VAuxDataCount
                                .AuxDataCountForTotals(config: this);
                    }
                }

                /// <summary>
                /// Configuration contains some calculated auxiliary variable values
                /// </summary>
                public bool ContainsCalculatedAuxData
                {
                    get
                    {
                        return AuxDataCount != 0;
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Reset all configuration properties to default values
                /// </summary>
                public void FullReset(int configCollectionId = 0)
                {
                    AuxiliaryVariableCategoryId = null;
                    ConfigCollectionId = configCollectionId;
                    ConfigQueryId = (int)ConfigQueryEnum.GisLayer;
                    LabelCs = null;
                    DescriptionCs = null;
                    LabelEn = null;
                    DescriptionEn = null;
                    Complete = null;
                    Categories = null;
                    VectorId = null;
                    RasterAId = null;
                    RasterBId = null;
                    Band = null;
                    Reclass = null;
                    Condition = null;
                    Tag = null;
                    EditDate = DateTime.Now;
                }

                /// <summary>
                /// Reset setting of configuration to default values
                /// </summary>
                public void Reset()
                {
                    Complete = null;
                    Categories = null;
                    VectorId = null;
                    RasterAId = null;
                    RasterBId = null;
                    Band = null;
                    Reclass = null;
                    Condition = null;
                    Tag = null;
                    EditDate = DateTime.Now;
                }

                /// <summary>
                /// Insert new configuration into database
                /// </summary>
                public void Insert()
                {
                    List<string> commands = [];

                    commands.Add(item: String.Concat(
                        $"INSERT INTO {ConfigList.Schema}.{ConfigList.Name}{Environment.NewLine}",
                        $"(",
                        $"{ConfigList.ColId.DbName},{Environment.NewLine}",                                                     //  0
                        $"{ConfigList.ColAuxiliaryVariableCategoryId.DbName},{Environment.NewLine}",                            //  1
                        $"{ConfigList.ColConfigCollectionId.DbName},{Environment.NewLine}",                                     //  2
                        $"{ConfigList.ColConfigQueryId.DbName},{Environment.NewLine}",                                          //  3
                        $"{ConfigList.ColLabelCs.DbName},{Environment.NewLine}",                                                //  4
                        $"{ConfigList.ColLabelEn.DbName},{Environment.NewLine}",                                                //  5
                        $"{ConfigList.ColComplete.DbName},{Environment.NewLine}",                                               //  6
                        $"{ConfigList.ColCategories.DbName},{Environment.NewLine}",                                             //  7
                        $"{ConfigList.ColVector.DbName},{Environment.NewLine}",                                                 //  8
                        $"{ConfigList.ColRasterA.DbName},{Environment.NewLine}",                                                //  9
                        $"{ConfigList.ColRasterB.DbName},{Environment.NewLine}",                                                // 10
                        $"{ConfigList.ColBand.DbName},{Environment.NewLine}",                                                   // 11
                        $"{ConfigList.ColReclass.DbName},{Environment.NewLine}",                                                // 12
                        $"{ConfigList.ColCondition.DbName},{Environment.NewLine}",                                              // 13
                        $"{ConfigList.ColTag.DbName},{Environment.NewLine}",                                                    // 14
                        $"{ConfigList.ColDescriptionCs.DbName},{Environment.NewLine}",                                          // 15
                        $"{ConfigList.ColDescriptionEn.DbName},{Environment.NewLine}",                                          // 16
                        $"{ConfigList.ColEditDate.DbName},{Environment.NewLine}",                                               // 17
                        $"{ConfigList.ColEditUser.DbName}{Environment.NewLine}",                                                // 18
                        $"){Environment.NewLine}",
                        $"VALUES{Environment.NewLine}({Environment.NewLine}",
                        $"{Functions.PrepIntArg(arg: Id)},{Environment.NewLine}",                                               //  0
                        $"{Functions.PrepNIntArg(arg: AuxiliaryVariableCategoryId)},{Environment.NewLine}",                     //  1
                        $"{Functions.PrepIntArg(arg: ConfigCollectionId)},{Environment.NewLine}",                               //  2
                        $"{Functions.PrepIntArg(arg: ConfigQueryId)},{Environment.NewLine}",                                    //  3
                        $"{Functions.PrepStringArg(arg: LabelCs, replaceEmptyStringByNull: true)},{Environment.NewLine}",       //  4
                        $"{Functions.PrepStringArg(arg: LabelEn, replaceEmptyStringByNull: true)},{Environment.NewLine}",       //  5
                        $"{Functions.PrepStringArg(arg: Complete, replaceEmptyStringByNull: true)},{Environment.NewLine}",      //  6
                        $"{Functions.PrepStringArg(arg: Categories, replaceEmptyStringByNull: true)},{Environment.NewLine}",    //  7
                        $"{Functions.PrepNIntArg(arg: VectorId)},{Environment.NewLine}",                                        //  8
                        $"{Functions.PrepNIntArg(arg: RasterAId)},{Environment.NewLine}",                                       //  9
                        $"{Functions.PrepNIntArg(arg: RasterBId)},{Environment.NewLine}",                                       // 10
                        $"{Functions.PrepNIntArg(arg: Band)},{Environment.NewLine}",                                            // 11
                        $"{Functions.PrepNIntArg(arg: Reclass)},{Environment.NewLine}",                                         // 12
                        $"{Functions.PrepStringArg(arg: Condition, replaceEmptyStringByNull: true)},{Environment.NewLine}",     // 13
                        $"{Functions.PrepStringArg(arg: Tag, replaceEmptyStringByNull: true)},{Environment.NewLine}",           // 14
                        $"{Functions.PrepStringArg(arg: DescriptionCs, replaceEmptyStringByNull: true)},{Environment.NewLine}", // 15
                        $"{Functions.PrepStringArg(arg: DescriptionEn, replaceEmptyStringByNull: true)},{Environment.NewLine}", // 16
                        $"pg_catalog.now(),{Environment.NewLine}",                                                              // 17
                        $"pg_catalog.current_user(){Environment.NewLine}",                                                      // 18
                        $");{Environment.NewLine}"
                        ));

                    Composite.Database.Postgres.ExecuteList(
                        sqlCommands: commands);

                    Composite.ReLoad();
                }

                /// <summary>
                /// Update configuration in database
                /// </summary>
                public void Update()
                {
                    List<string> commands = [];

                    commands.Add(item: String.Concat(
                        $"UPDATE {ConfigList.Schema}.{ConfigList.Name}{Environment.NewLine}",
                        $"SET{Environment.NewLine}",
                        $"    {ConfigList.ColAuxiliaryVariableCategoryId.DbName} = ",
                        $"{Functions.PrepNIntArg(arg: AuxiliaryVariableCategoryId)},{Environment.NewLine}",                         //  1
                        $"    {ConfigList.ColConfigCollectionId.DbName} = ",
                        $"{Functions.PrepIntArg(arg: ConfigCollectionId)},{Environment.NewLine}",                                   //  2
                        $"    {ConfigList.ColConfigQueryId.DbName} = ",
                        $"{Functions.PrepIntArg(arg: ConfigQueryId)},{Environment.NewLine}",                                        //  3
                        $"    {ConfigList.ColLabelCs.DbName} = ",
                        $"{Functions.PrepStringArg(arg: LabelCs, replaceEmptyStringByNull: true)},{Environment.NewLine}",           //  4
                        $"    {ConfigList.ColLabelEn.DbName} = ",
                        $"{Functions.PrepStringArg(arg: LabelEn, replaceEmptyStringByNull: true)},{Environment.NewLine}",           //  5
                        $"    {ConfigList.ColComplete.DbName} = ",
                        $"{Functions.PrepStringArg(arg: Complete, replaceEmptyStringByNull: true)},{Environment.NewLine}",          //  6
                        $"    {ConfigList.ColCategories.DbName} = ",
                        $"{Functions.PrepStringArg(arg: Categories, replaceEmptyStringByNull: true)},{Environment.NewLine}",        //  7
                        $"    {ConfigList.ColVector.DbName} = ",
                        $"{Functions.PrepNIntArg(arg: VectorId)},{Environment.NewLine}",                                            //  8
                        $"    {ConfigList.ColRasterA.DbName} = ",
                        $"{Functions.PrepNIntArg(arg: RasterAId)},{Environment.NewLine}",                                           //  9
                        $"    {ConfigList.ColRasterB.DbName} = ",
                        $"{Functions.PrepNIntArg(arg: RasterBId)},{Environment.NewLine}",                                           // 10
                        $"    {ConfigList.ColBand.DbName} = ",
                        $"{Functions.PrepNIntArg(arg: Band)},{Environment.NewLine}",                                                // 11
                        $"    {ConfigList.ColReclass.DbName} = ",
                        $"{Functions.PrepNIntArg(arg: Reclass)},{Environment.NewLine}",                                             // 12
                        $"    {ConfigList.ColCondition.DbName} = ",
                        $"{Functions.PrepStringArg(arg: Condition, replaceEmptyStringByNull: true)},{Environment.NewLine}",         // 13
                        $"    {ConfigList.ColTag.DbName} = ",
                        $"{Functions.PrepStringArg(arg: Tag, replaceEmptyStringByNull: true)},{Environment.NewLine}",               // 14
                        $"    {ConfigList.ColDescriptionCs.DbName} = ",
                        $"{Functions.PrepStringArg(arg: DescriptionCs, replaceEmptyStringByNull: true)},{Environment.NewLine}",     // 15
                        $"    {ConfigList.ColDescriptionEn.DbName} = ",
                        $"{Functions.PrepStringArg(arg: DescriptionEn, replaceEmptyStringByNull: true)},{Environment.NewLine}",     // 16
                        $"    {ConfigList.ColEditDate.DbName} = ",
                        $"pg_catalog.now(),{Environment.NewLine}",                                                                  // 17
                        $"    {ConfigList.ColEditUser.DbName} = ",
                        $"pg_catalog.current_user(){Environment.NewLine}",                                                          // 18
                        $"WHERE{Environment.NewLine}",
                        $"    ({ConfigList.ColId.DbName} = {Functions.PrepIntArg(arg: Id)});{Environment.NewLine}"                  //  0
                        ));

                    Composite.Database.Postgres.ExecuteList(
                        sqlCommands: commands);

                    Composite.ReLoad();
                }

                /// <summary>
                /// Is it possible to delete this configuration?
                /// </summary>
                /// <param name="status">Configuration status</param>
                /// <returns>true|false</returns>
                public bool TryDelete(out ConfigurationStatus status)
                {
                    if (ContainsDependentConfigurations)
                    {
                        // "Konfiguraci nelze smazat, protože se na ni odkazují další konfigurace."
                        // "Configuration cannot be deleted because it is referenced by other configurations."
                        status = ConfigurationStatus.ContainsDependentConfigurations;
                        return false;
                    }

                    if (ContainsCalculatedAuxTotals)
                    {
                        // "Konfiguraci nelze smazat, protože obsahuje konfigurace s vypočtenými úhrny pomocné proměnné."
                        // "Configuration cannot be deleted because it contains configurations with calculated auxiliary variable totals."
                        status = ConfigurationStatus.ContainsCalculatedAuxTotals;
                        return false;
                    }

                    if (ContainsCalculatedAuxData)
                    {
                        // "Konfiguraci nelze smazat, protože obsahuje konfigurace s provedeným protínáním bodové vrstvy s vrstvou úhrnů pomocné proměnné."
                        // "Configuration cannot be deleted because it contains configurations with calculated intersections between point layer and auxiliary variable totals layer."
                        status = ConfigurationStatus.ContainsCalculatedAuxData;
                        return false;
                    }

                    status = ConfigurationStatus.None;
                    return true;
                }

                /// <summary>
                /// Delete configuration
                /// </summary>
                public void Delete()
                {
                    if (!TryDelete(status: out ConfigurationStatus _))
                    {
                        return;
                    }

                    List<string> commands = [];

                    commands.Add(
                        item: String.Concat(
                                $"DELETE{Environment.NewLine}",
                                $"FROM{Environment.NewLine}",
                                $"    {ConfigList.Schema}.{ConfigList.Name}{Environment.NewLine}",
                                $"WHERE{Environment.NewLine}",
                                $"    ({ConfigList.ColId.DbName} = {Id});{Environment.NewLine}"
                                ));

                    Composite.Database.Postgres.ExecuteList(
                        sqlCommands: commands);

                    Composite.ReLoad();
                }

                /// <summary>
                /// Reloads data into column "command" from database
                /// </summary>
                /// <param name="database">Database tables</param>
                /// <param name="cellGid">Cell identifier</param>
                /// <param name="transaction">Transaction</param>
                public void ReloadColumnCommand(
                    NfiEstaDB database,
                    string cellGid = "#CELL_GID#",
                    NpgsqlTransaction transaction = null)
                {
                    if (ConfigCollection == null)
                    {
                        Command = null;
                        return;
                    }

                    switch (ConfigQueryValue)
                    {
                        case ConfigQueryEnum.GisLayer:

                            switch (ConfigCollection.ConfigFunctionValue)
                            {
                                case ConfigFunctionEnum.VectorTotal:
                                    Command = ADFunctions.FnSqlAuxTotalVectorApp.Execute(
                                        database: database,
                                        configId: Id,
                                        schemaName: ConfigCollection.SchemaName,
                                        tableName: ConfigCollection.TableName,
                                        columnName: ConfigCollection.ColumnName,
                                        condition: Condition,
                                        unit: ConfigCollection.Unit,
                                        gid: cellGid,
                                        transaction: transaction);
                                    return;

                                case ConfigFunctionEnum.RasterTotal:
                                    Command = ADFunctions.FnSqlAuxTotalRasterApp.Execute(
                                        database: database,
                                        configId: Id,
                                        schemaName: ConfigCollection.SchemaName,
                                        tableName: ConfigCollection.TableName,
                                        columnName: ConfigCollection.ColumnName,
                                        band: Band,
                                        reclass: Reclass,
                                        condition: Condition,
                                        unit: ConfigCollection.Unit,
                                        gid: cellGid,
                                        transaction: transaction);
                                    return;

                                case ConfigFunctionEnum.RasterTotalWithinVector:
                                    if ((Vector == null) || (RasterA == null) ||
                                        (Vector.ConfigCollection == null) || (RasterA.ConfigCollection == null))
                                    {
                                        Command = null;
                                        return;
                                    }

                                    Command = ADFunctions.FnSqlAuxTotalVectorCombApp.Execute(
                                        database: database,
                                        configId: Id,
                                        schemaNameA: Vector.ConfigCollection.SchemaName,
                                        tableNameA: Vector.ConfigCollection.TableName,
                                        columnNameA: Vector.ConfigCollection.ColumnName,
                                        conditionA: Vector.ConfigCollection.Condition,
                                        unitA: Vector.ConfigCollection.Unit,
                                        schemaNameB: RasterA.ConfigCollection.SchemaName,
                                        tableNameB: RasterA.ConfigCollection.TableName,
                                        columnNameB: RasterA.ConfigCollection.ColumnName,
                                        bandB: RasterA.Band,
                                        reclassB: RasterA.Reclass,
                                        conditionB: RasterA.Condition,
                                        unitB: RasterA.ConfigCollection.Unit,
                                        gid: cellGid,
                                        transaction: transaction);
                                    return;

                                case ConfigFunctionEnum.RastersProductTotal:
                                    if ((RasterA == null) || (RasterB == null) ||
                                        (RasterA.ConfigCollection == null) || (RasterB.ConfigCollection == null))
                                    {
                                        Command = null;
                                        return;
                                    }

                                    Command = ADFunctions.FnSqlAuxTotalRasterCombApp.Execute(
                                        database: database,
                                        configId: Id,
                                        schemaNameA: RasterA.ConfigCollection.SchemaName,
                                        tableNameA: RasterA.ConfigCollection.TableName,
                                        columnNameA: RasterA.ConfigCollection.ColumnName,
                                        bandA: RasterA.Band,
                                        reclassA: RasterA.Reclass,
                                        conditionA: RasterA.Condition,
                                        unitA: RasterA.ConfigCollection.Unit,
                                        schemaNameB: RasterB.ConfigCollection.SchemaName,
                                        tableNameB: RasterB.ConfigCollection.TableName,
                                        columnNameB: RasterB.ConfigCollection.ColumnName,
                                        bandB: RasterB.Band,
                                        reclassB: RasterB.Reclass,
                                        conditionB: RasterB.Condition,
                                        unitB: RasterB.ConfigCollection.Unit,
                                        gid: cellGid,
                                        transaction: transaction);
                                    return;

                                case ConfigFunctionEnum.PointLayer:
                                    Command = null;
                                    return;

                                case ConfigFunctionEnum.PointTotalCombination:
                                    Command = null;
                                    return;

                                default:
                                    Command = null;
                                    return;
                            }

                        case ConfigQueryEnum.SumOfVariables:
                            Command = null;
                            break;

                        case ConfigQueryEnum.AdditionToCellArea:
                            Command = null;
                            break;

                        case ConfigQueryEnum.AdditionToTotal:
                            Command = null;
                            break;

                        case ConfigQueryEnum.LinkToVariable:
                            Command = null;
                            break;

                        default:
                            Command = null;
                            break;
                    }
                }

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not Config Type, return False
                    if (obj is not Config)
                    {
                        return false;
                    }

                    return
                        Id == ((Config)obj).Id;
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                #endregion Methods

            }


            /// <summary>
            /// List of configurations for the auxiliary variable totals calculation
            /// </summary>
            public class ConfigList
                : ALookupTable
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema = ADSchema.Name;

                /// <summary>
                /// Table name
                /// </summary>
                public const string Name = "t_config";

                /// <summary>
                /// Table alias
                /// </summary>
                public const string Alias = "t_config";

                /// <summary>
                /// Description in national language
                /// </summary>
                public const string CaptionCs = "Seznam konfigurací výpočtu úhrnů pomocných proměnných";

                /// <summary>
                /// Description in English
                /// </summary>
                public const string CaptionEn = "List of configurations for the auxiliary variable totals calculation";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
                {
                    { "id", new ColumnMetadata()
                    {
                        Name = "id",
                        DbName = "id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = true,
                        NotNull = true,
                        DefaultValue = "0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ID",
                        HeaderTextEn = "ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 0
                    }
                    },
                    { "auxiliary_variable_category_id", new ColumnMetadata()
                    {
                        Name = "auxiliary_variable_category_id",
                        DbName = "auxiliary_variable_category",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "AUXILIARY_VARIABLE_CATEGORY_ID",
                        HeaderTextEn = "AUXILIARY_VARIABLE_CATEGORY_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 1
                    }
                    },
                    { "config_collection_id", new ColumnMetadata()
                    {
                        Name = "config_collection_id",
                        DbName = "config_collection",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = "0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "CONFIG_COLLECTION_ID",
                        HeaderTextEn = "CONFIG_COLLECTION_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 2
                    }
                    },
                    { "config_query_id", new ColumnMetadata()
                    {
                        Name = "config_query_id",
                        DbName = "config_query",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = "0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "CONFIG_QUERY_ID",
                        HeaderTextEn = "CONFIG_QUERY_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 3
                    }
                    },
                    { "label_cs", new ColumnMetadata()
                    {
                        Name = "label_cs",
                        DbName = "label",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "LABEL_CS",
                        HeaderTextEn = "LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 4
                    }
                    },
                    { "label_en", new ColumnMetadata()
                    {
                        Name = "label_en",
                        DbName = "label_en",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "LABEL_EN",
                        HeaderTextEn = "LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 5
                    }
                    },
                    { "complete", new ColumnMetadata()
                    {
                        Name = "complete",
                        DbName = "complete",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "COMPLETE",
                        HeaderTextEn = "COMPLETE",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 6
                    }
                    },
                    { "categories", new ColumnMetadata()
                    {
                        Name = "categories",
                        DbName = "categories",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "CATEGORIES",
                        HeaderTextEn = "CATEGORIES",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 7
                    }
                    },
                    { "vector", new ColumnMetadata()
                    {
                        Name = "vector",
                        DbName = "vector",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "VECTOR",
                        HeaderTextEn = "VECTOR",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 8
                    }
                    },
                    { "raster_a", new ColumnMetadata()
                    {
                        Name = "raster_a",
                        DbName = "raster",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "RASTER_A",
                        HeaderTextEn = "RASTER_A",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 9
                    }
                    },
                    { "raster_b", new ColumnMetadata()
                    {
                        Name = "raster_b",
                        DbName = "raster_1",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "RASTER_B",
                        HeaderTextEn = "RASTER_B",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 10
                    }
                    },
                    { "band", new ColumnMetadata()
                    {
                        Name = "band",
                        DbName = "band",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "BAND",
                        HeaderTextEn = "BAND",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 11
                    }
                    },
                    { "reclass", new ColumnMetadata()
                    {
                        Name = "reclass",
                        DbName = "reclass",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "RECLASS",
                        HeaderTextEn = "RECLASS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 12
                    }
                    },
                    { "condition", new ColumnMetadata()
                    {
                        Name = "condition",
                        DbName = "condition",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "CONDITION",
                        HeaderTextEn = "CONDITION",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 13
                    }
                    },
                    { "tag", new ColumnMetadata()
                    {
                        Name = "tag",
                        DbName = "tag",
                        DataType = "System.String",
                        DbDataType = "text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "TAG",
                        HeaderTextEn = "TAG",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 14
                    }
                    },
                    { "description_cs", new ColumnMetadata()
                    {
                        Name = "description_cs",
                        DbName = "description",
                        DataType = "System.String",
                        DbDataType = "text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "DESCRIPTION_CS",
                        HeaderTextEn = "DESCRIPTION_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 15
                    }
                    },
                    { "description_en", new ColumnMetadata()
                    {
                        Name = "description_en",
                        DbName = "description_en",
                        DataType = "System.String",
                        DbDataType = "text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "DESCRIPTION_EN",
                        HeaderTextEn = "DESCRIPTION_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 16
                    }
                    },
                    { "edit_date", new ColumnMetadata()
                    {
                        Name = "edit_date",
                        DbName = "edit_date",
                        DataType = "System.DateTime",
                        DbDataType = "timestamp",
                        NewDataType = "timestamp",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = new DateTime().ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "yyyy-MM-dd HH:mm:ss",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "EDIT_DATE",
                        HeaderTextEn = "EDIT_DATE",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 17
                    }
                    },
                    { "edit_user", new ColumnMetadata()
                    {
                        Name = "edit_user",
                        DbName = "edit_user",
                        DataType = "System.String",
                        DbDataType = "name",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "EDIT_USER",
                        HeaderTextEn = "EDIT_USER",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 18
                    }
                    },
                    { "command", new ColumnMetadata()
                    {
                        Name = "command",
                        DbName = "command",
                        DataType = "System.String",
                        DbDataType = "text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "COMMAND",
                        HeaderTextEn = "COMMAND",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 19
                    }
                    }
                };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column auxiliary_variable_category_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColAuxiliaryVariableCategoryId = Cols["auxiliary_variable_category_id"];

                /// <summary>
                /// Column config_collection_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColConfigCollectionId = Cols["config_collection_id"];

                /// <summary>
                /// Column config_query_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColConfigQueryId = Cols["config_query_id"];

                /// <summary>
                /// Column label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColLabelCs = Cols["label_cs"];

                /// <summary>
                /// Column label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColLabelEn = Cols["label_en"];

                /// <summary>
                /// Column complete metadata
                /// </summary>
                public static readonly ColumnMetadata ColComplete = Cols["complete"];

                /// <summary>
                /// Column categories metadata
                /// </summary>
                public static readonly ColumnMetadata ColCategories = Cols["categories"];

                /// <summary>
                /// Column vector metadata
                /// </summary>
                public static readonly ColumnMetadata ColVector = Cols["vector"];

                /// <summary>
                /// Column raster metadata
                /// </summary>
                public static readonly ColumnMetadata ColRasterA = Cols["raster_a"];

                /// <summary>
                /// Column raster_1 metadata
                /// </summary>
                public static readonly ColumnMetadata ColRasterB = Cols["raster_b"];

                /// <summary>
                /// Column band metadata
                /// </summary>
                public static readonly ColumnMetadata ColBand = Cols["band"];

                /// <summary>
                /// Column reclass metadata
                /// </summary>
                public static readonly ColumnMetadata ColReclass = Cols["reclass"];

                /// <summary>
                /// Column condition metadata
                /// </summary>
                public static readonly ColumnMetadata ColCondition = Cols["condition"];

                /// <summary>
                /// Column tag metadata
                /// </summary>
                public static readonly ColumnMetadata ColTag = Cols["tag"];

                /// <summary>
                /// Column description_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColDescriptionCs = Cols["description_cs"];

                /// <summary>
                /// Column description_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColDescriptionEn = Cols["description_en"];

                /// <summary>
                /// Column edit_date metadata
                /// </summary>
                public static readonly ColumnMetadata ColEditDate = Cols["edit_date"];

                /// <summary>
                /// Column edit_user metadata
                /// </summary>
                public static readonly ColumnMetadata ColEditUser = Cols["edit_user"];

                /// <summary>
                /// Column command metadata
                /// </summary>
                public static readonly ColumnMetadata ColCommand = Cols["command"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public ConfigList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public ConfigList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// List of configurations for the auxiliary variable totals calculation (read-only)
                /// </summary>
                public List<Config> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new Config(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// Configuration for the auxiliary variable totals calculation from list by identifier (read-only)
                /// </summary>
                /// <param name="id">Configuration for the auxiliary variable totals calculation identifier</param>
                /// <returns>Configuration for the auxiliary variable totals calculation from list by identifier (null if not found)</returns>
                public Config this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new Config(composite: this, data: a))
                                .FirstOrDefault<Config>();
                    }
                }

                #endregion Indexer


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public ConfigList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new ConfigList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new ConfigList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                /// <summary>
                /// Reloads data into column "command" from database
                /// </summary>
                public void ReloadColumnCommand()
                {
                    System.Diagnostics.Stopwatch stopWatch = new();
                    stopWatch.Start();

                    foreach (Config config in Items)
                    {
                        config.ReloadColumnCommand(
                            database: (NfiEstaDB)Database);
                    }

                    stopWatch.Stop();
                    LoadingTime ??= 0.0 + 0.001 * stopWatch.ElapsedMilliseconds;
                }

                /// <summary>
                /// Creates new item with default parameters
                /// </summary>
                /// <returns>Returns new item with default parameters</returns>
                public Config CreateNewItem(ConfigCollection configCollection)
                {
                    Config newConfig =
                        new(composite: this,
                            data: Data.NewRow())
                        {
                            Id = (Items.Count > 0)
                            ? Items.Select(a => a.Id).Max() + 1
                            : 1
                        };

                    newConfig.FullReset(
                        configCollectionId: configCollection?.Id ?? 0);

                    return newConfig;
                }

                #endregion Methods

            }


            /// <summary>
            /// Configuration status
            /// </summary>
            public enum ConfigurationStatus
            {
                /// <summary>
                /// None
                /// </summary>
                None = 0,

                /// <summary>
                /// List of dependent configurations is not empty
                /// </summary>
                ContainsDependentConfigurations = 1,

                /// <summary>
                /// Configuration contains some calculated auxiliary variable totals
                /// </summary>
                ContainsCalculatedAuxTotals = 2,

                /// <summary>
                /// Configuration contains some calculated auxiliary variable values
                /// </summary>
                ContainsCalculatedAuxData = 3
            }

        }
    }
}