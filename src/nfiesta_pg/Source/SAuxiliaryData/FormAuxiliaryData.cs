﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;
using static ZaJi.NfiEstaPg.AuxiliaryData.ADFunctions;

namespace ZaJi.NfiEstaPg.AuxiliaryData
{

    /// <summary>
    /// <para lang="cs">Formulář zobrazení dat extenze nfiesta_gisdata</para>
    /// <para lang="en">Form for displaying nfiesta_gisdata extension data</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    public partial class FormAuxiliaryData
        : Form, INfiEstaControl
    {

        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Omezení počtu řádků</para>
        /// <para lang="en">Row count limit</para>
        /// </summary>
        private Nullable<int> limit;

        private string msgDataExportComplete = String.Empty;
        private string msgNoDifferencesFound = String.Empty;

        #endregion Private Fields


        #region Contructor

        /// <summary>
        /// <para lang="cs">Konstruktor formuláře</para>
        /// <para lang="en">Form constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        public FormAuxiliaryData(Control controlOwner)
        {
            InitializeComponent();
            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                if (controlOwner is not INfiEstaControl)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(INfiEstaControl)}.",
                        paramName: nameof(ControlOwner));
                }

                return controlOwner;
            }
            set
            {
                if (value is not INfiEstaControl)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(INfiEstaControl)}.",
                        paramName: nameof(ControlOwner));
                }

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables (read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">Omezení počtu řádků</para>
        /// <para lang="en">Row count limit</para>
        /// </summary>
        public Nullable<int> Limit
        {
            get
            {
                return limit;
            }
            set
            {
                limit = value;
            }
        }

        #endregion Properties


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace formuláře</para>
        /// <para lang="en">Initializing the form</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        private void Initialize(Control controlOwner)
        {
            ControlOwner = controlOwner;
            Limit = null;

            InitializeLabels();

            btnClose.Click += new EventHandler(
                (sender, e) =>
                {
                    DialogResult = DialogResult.Cancel;
                    Close();
                });

            tsmiCConfigFunction.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.AuxiliaryData.ConfigFunctionList(database: Database),
                        columns: ZaJi.NfiEstaPg.AuxiliaryData.ConfigFunctionList.Cols);
                });

            tsmiCConfigQuery.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.AuxiliaryData.ConfigQueryList(database: Database),
                        columns: ZaJi.NfiEstaPg.AuxiliaryData.ConfigQueryList.Cols);
                });

            tsmiCEstimationCell.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.AuxiliaryData.EstimationCellList(database: Database),
                        columns: ZaJi.NfiEstaPg.AuxiliaryData.EstimationCellList.Cols);
                });

            tsmiCEstimationCellCollection.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.AuxiliaryData.EstimationCellCollectionList(database: Database),
                        columns: ZaJi.NfiEstaPg.AuxiliaryData.EstimationCellCollectionList.Cols);
                });

            tsmiCExtVersion.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.AuxiliaryData.ExtensionVersionList(database: Database),
                        columns: ZaJi.NfiEstaPg.AuxiliaryData.ExtensionVersionList.Cols);
                });

            tsmiCGUIVersion.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.AuxiliaryData.GUIVersionList(database: Database),
                        columns: ZaJi.NfiEstaPg.AuxiliaryData.GUIVersionList.Cols);
                });

            tsmiCmEstimationCell.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.AuxiliaryData.EstimationCellMappingList(database: Database),
                        columns: ZaJi.NfiEstaPg.AuxiliaryData.EstimationCellMappingList.Cols);
                });

            tsmiCmEstimationCellCollection.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.AuxiliaryData.EstimationCellCollectionMappingList(database: Database),
                        columns: ZaJi.NfiEstaPg.AuxiliaryData.EstimationCellCollectionMappingList.Cols);
                });

            tsmiCmExtConfigFunction.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.AuxiliaryData.ExtensionToConfigFunctionList(database: Database),
                        columns: ZaJi.NfiEstaPg.AuxiliaryData.ExtensionToConfigFunctionList.Cols);
                });

            tsmiCmExtGUIVersion.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.AuxiliaryData.ExtensionToGUIVersionList(database: Database),
                        columns: ZaJi.NfiEstaPg.AuxiliaryData.ExtensionToGUIVersionList.Cols);
                });

            tsmiFaCell.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.AuxiliaryData.CellList(database: Database),
                        columns: ZaJi.NfiEstaPg.AuxiliaryData.CellList.Cols);
                });

            tsmiFpPlot.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.AuxiliaryData.PlotList(database: Database),
                        columns: ZaJi.NfiEstaPg.AuxiliaryData.PlotList.Cols);
                });

            tsmiTAuxTotal.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.AuxiliaryData.AuxTotalList(database: Database),
                        columns: ZaJi.NfiEstaPg.AuxiliaryData.AuxTotalList.Cols);
                });

            tsmiTAuxData.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.AuxiliaryData.AuxDataList(database: Database),
                        columns: ZaJi.NfiEstaPg.AuxiliaryData.AuxDataList.Cols);
                });

            tsmiTConfig.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.AuxiliaryData.ConfigList(database: Database),
                        columns: ZaJi.NfiEstaPg.AuxiliaryData.ConfigList.Cols);
                });

            tsmiTConfigCollection.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.AuxiliaryData.ConfigCollectionList(database: Database),
                        columns: ZaJi.NfiEstaPg.AuxiliaryData.ConfigCollectionList.Cols);
                });

            tsmiTEstimationCellHierarchy.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.AuxiliaryData.EstimationCellHierarchyList(database: Database),
                        columns: ZaJi.NfiEstaPg.AuxiliaryData.EstimationCellHierarchyList.Cols);
                });

            tsmiFnApiGetEstimationCellHierarchy.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: ADFunctions.FnApiGetEstimationCellHierarchy.Execute(database: Database),
                        columns: ZaJi.NfiEstaPg.AuxiliaryData.TFnApiGetEstimationCellHierarchyList.Cols);
                });

            tsmiFnApiGetEstimationCellHierarchyStage.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: ADFunctions.FnApiGetEstimationCellHierarchyStage.Execute(
                            database: Database,
                            configCollectionId: 1),
                        columns: ZaJi.NfiEstaPg.AuxiliaryData.TFnApiGetEstimationCellHierarchyStageList.Cols);
                });

            tsmiFnGetAuxDataApp.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: ADFunctions.FnGetAuxDataApp.Execute(
                            database: Database,
                            configCollectionId: 1,
                            configId: 1,
                            intersects: true,
                            gidStart: 1,
                            gidEnd: 2),
                        columns: ZaJi.NfiEstaPg.AuxiliaryData.TFnGetAuxDataAppList.Cols);
                });

            tsmiFnGetAuxDataCount.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: ADFunctions.FnGetAuxDataCount.Execute(database: Database),
                        columns: ZaJi.NfiEstaPg.AuxiliaryData.TFnGetAuxDataCountList.Cols);
                });

            tsmiFnGetAuxTotal.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: ADFunctions.FnGetAuxTotal.Execute(
                            database: Database,
                            configurationIds: [1, 2, 3],
                            estimationCellIds: [8672191, 8677277, 8672192, 8677278, 8728256],
                            extensionVersionIds: [1900],
                            guiVersionIds: [800]),
                        columns: ZaJi.NfiEstaPg.AuxiliaryData.TFnGetAuxTotalList.Cols);
                });

            tsmiFnGetAuxTotalCount.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: ADFunctions.FnGetAuxTotalCount.Execute(database: Database),
                        columns: ZaJi.NfiEstaPg.AuxiliaryData.TFnGetAuxTotalCountList.Cols);
                });

            tsmiFnGetConfigsForAuxDataApp.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: ADFunctions.FnGetConfigsForAuxDataApp.Execute(
                            database: Database,
                            configCollectionId: 1,
                            guiVersionId: 900,
                            intervalPoints: 0,
                            recalc: false),
                        columns: ZaJi.NfiEstaPg.AuxiliaryData.TFnGetConfigsForAuxDataAppList.Cols);
                });

            tsmiFnGetGidsForAuxTotalApp.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: ADFunctions.FnGetGidsForAuxTotalApp.Execute(
                            database: Database,
                            configId: 1,
                            estimationCellIds: [12, 25],
                            extensionVersionId: 2000,
                            guiVersionId: 900,
                            recalc: false),
                        columns: ZaJi.NfiEstaPg.AuxiliaryData.TFnGetGidsForAuxTotalAppList.Cols);
                });

            tsmiFnGetIntervalPointsApp.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: ADFunctions.FnGetIntervalPointsApp.Execute(
                            database: Database,
                            configCollectionId: 1,
                            intervalPoints: 100),
                        columns: ZaJi.NfiEstaPg.AuxiliaryData.TFnGetIntervalPointsAppList.Cols);
                });

            tsmiExtractData.Click += new EventHandler(
              (sender, e) => { ExportData(); });

            tsmiGetDifferences.Click += new EventHandler(
              (sender, e) => { DisplayDifferences(); });
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků formuláře</para>
        /// <para lang="en">Initializing form labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            switch (LanguageVersion)
            {
                case LanguageVersion.National:
                    Text = $"{ADSchema.ExtensionName} ({ADSchema.ExtensionVersion})";

                    tsmiLookupTables.Text = "Číselníky";
                    tsmiCConfigFunction.Text = ZaJi.NfiEstaPg.AuxiliaryData.ConfigFunctionList.Name;
                    tsmiCConfigQuery.Text = ZaJi.NfiEstaPg.AuxiliaryData.ConfigQueryList.Name;
                    tsmiCEstimationCell.Text = ZaJi.NfiEstaPg.AuxiliaryData.EstimationCellList.Name;
                    tsmiCEstimationCellCollection.Text = ZaJi.NfiEstaPg.AuxiliaryData.EstimationCellCollectionList.Name;
                    tsmiCExtVersion.Text = ZaJi.NfiEstaPg.AuxiliaryData.ExtensionVersionList.Name;
                    tsmiCGUIVersion.Text = ZaJi.NfiEstaPg.AuxiliaryData.GUIVersionList.Name;

                    tsmiMappingTables.Text = "Mapovací tabulky";
                    tsmiCmEstimationCell.Text = ZaJi.NfiEstaPg.AuxiliaryData.EstimationCellMappingList.Name;
                    tsmiCmEstimationCellCollection.Text = ZaJi.NfiEstaPg.AuxiliaryData.EstimationCellCollectionMappingList.Name;
                    tsmiCmExtConfigFunction.Text = ZaJi.NfiEstaPg.AuxiliaryData.ExtensionToConfigFunctionList.Name;
                    tsmiCmExtGUIVersion.Text = ZaJi.NfiEstaPg.AuxiliaryData.ExtensionToGUIVersionList.Name;

                    tsmiSpatialTables.Text = "Prostorové tabulky";
                    tsmiFaCell.Text = ZaJi.NfiEstaPg.AuxiliaryData.CellList.Name;
                    tsmiFpPlot.Text = ZaJi.NfiEstaPg.AuxiliaryData.PlotList.Name;

                    tsmiDataTables.Text = "Datové tabulky";
                    tsmiTAuxData.Text = ZaJi.NfiEstaPg.AuxiliaryData.AuxDataList.Name;
                    tsmiTAuxTotal.Text = ZaJi.NfiEstaPg.AuxiliaryData.AuxTotalList.Name;
                    tsmiTConfig.Text = ZaJi.NfiEstaPg.AuxiliaryData.ConfigList.Name;
                    tsmiTConfigCollection.Text = ZaJi.NfiEstaPg.AuxiliaryData.ConfigCollectionList.Name;
                    tsmiTEstimationCellHierarchy.Text = ZaJi.NfiEstaPg.AuxiliaryData.EstimationCellHierarchyList.Name;

                    tsmiStoredProcedures.Text = "Uložené procedury";
                    tsmiFnApiGetEstimationCellHierarchy.Text = FnApiGetEstimationCellHierarchy.Name;
                    tsmiFnApiGetEstimationCellHierarchyStage.Text = FnApiGetEstimationCellHierarchyStage.Name;
                    tsmiFnGetAuxDataApp.Text = FnGetAuxDataApp.Name;
                    tsmiFnGetAuxDataCount.Text = FnGetAuxDataCount.Name;
                    tsmiFnGetAuxTotal.Text = FnGetAuxTotal.Name;
                    tsmiFnGetAuxTotalCount.Text = FnGetAuxTotalCount.Name;
                    tsmiFnGetConfigsForAuxDataApp.Text = FnGetConfigsForAuxDataApp.Name;
                    tsmiFnGetGidsForAuxTotalApp.Text = FnGetGidsForAuxTotalApp.Name;
                    tsmiFnGetIntervalPointsApp.Text = FnGetIntervalPointsApp.Name;

                    tsmiETL.Text = "ETL";
                    tsmiExtractData.Text = "Export dat do vybrané složky";
                    tsmiGetDifferences.Text = "Rozdíly mezi dll a databázovou extenzí";

                    btnClose.Text = "Zavřít";

                    msgDataExportComplete = "Export dat byl dokončen.";
                    msgNoDifferencesFound = "Nebyly nalezeny žádné rozdíly.";

                    break;

                case LanguageVersion.International:
                    Text = $"{ADSchema.ExtensionName} ({ADSchema.ExtensionVersion})";

                    tsmiLookupTables.Text = "Lookup tables";
                    tsmiCConfigFunction.Text = ZaJi.NfiEstaPg.AuxiliaryData.ConfigFunctionList.Name;
                    tsmiCConfigQuery.Text = ZaJi.NfiEstaPg.AuxiliaryData.ConfigQueryList.Name;
                    tsmiCEstimationCell.Text = ZaJi.NfiEstaPg.AuxiliaryData.EstimationCellList.Name;
                    tsmiCEstimationCellCollection.Text = ZaJi.NfiEstaPg.AuxiliaryData.EstimationCellCollectionList.Name;
                    tsmiCExtVersion.Text = ZaJi.NfiEstaPg.AuxiliaryData.ExtensionVersionList.Name;
                    tsmiCGUIVersion.Text = ZaJi.NfiEstaPg.AuxiliaryData.GUIVersionList.Name;

                    tsmiMappingTables.Text = "Mapping tables";
                    tsmiCmEstimationCell.Text = ZaJi.NfiEstaPg.AuxiliaryData.EstimationCellMappingList.Name;
                    tsmiCmEstimationCellCollection.Text = ZaJi.NfiEstaPg.AuxiliaryData.EstimationCellCollectionMappingList.Name;
                    tsmiCmExtConfigFunction.Text = ZaJi.NfiEstaPg.AuxiliaryData.ExtensionToConfigFunctionList.Name;
                    tsmiCmExtGUIVersion.Text = ZaJi.NfiEstaPg.AuxiliaryData.ExtensionToGUIVersionList.Name;

                    tsmiSpatialTables.Text = "Spatial tables";
                    tsmiFaCell.Text = ZaJi.NfiEstaPg.AuxiliaryData.CellList.Name;
                    tsmiFpPlot.Text = ZaJi.NfiEstaPg.AuxiliaryData.PlotList.Name;

                    tsmiDataTables.Text = "Data tables";
                    tsmiTAuxData.Text = ZaJi.NfiEstaPg.AuxiliaryData.AuxDataList.Name;
                    tsmiTAuxTotal.Text = ZaJi.NfiEstaPg.AuxiliaryData.AuxTotalList.Name;
                    tsmiTConfig.Text = ZaJi.NfiEstaPg.AuxiliaryData.ConfigList.Name;
                    tsmiTConfigCollection.Text = ZaJi.NfiEstaPg.AuxiliaryData.ConfigCollectionList.Name;
                    tsmiTEstimationCellHierarchy.Text = ZaJi.NfiEstaPg.AuxiliaryData.EstimationCellHierarchyList.Name;

                    tsmiStoredProcedures.Text = "Stored procedures";
                    tsmiFnApiGetEstimationCellHierarchy.Text = FnApiGetEstimationCellHierarchy.Name;
                    tsmiFnApiGetEstimationCellHierarchyStage.Text = FnApiGetEstimationCellHierarchyStage.Name;
                    tsmiFnGetAuxDataApp.Text = FnGetAuxDataApp.Name;
                    tsmiFnGetAuxDataCount.Text = FnGetAuxDataCount.Name;
                    tsmiFnGetAuxTotal.Text = FnGetAuxTotal.Name;
                    tsmiFnGetAuxTotalCount.Text = FnGetAuxTotalCount.Name;
                    tsmiFnGetConfigsForAuxDataApp.Text = FnGetConfigsForAuxDataApp.Name;
                    tsmiFnGetGidsForAuxTotalApp.Text = FnGetGidsForAuxTotalApp.Name;
                    tsmiFnGetIntervalPointsApp.Text = FnGetIntervalPointsApp.Name;

                    tsmiETL.Text = "ETL";
                    tsmiExtractData.Text = "Export data to folder";
                    tsmiGetDifferences.Text = "Differences between dll database extension";

                    btnClose.Text = "Close";

                    msgDataExportComplete = "Data export has been completed.";
                    msgNoDifferencesFound = "No differences have been found.";

                    break;

                default:
                    Text = nameof(FormAuxiliaryData);

                    tsmiLookupTables.Text = nameof(tsmiLookupTables);
                    tsmiCConfigFunction.Text = nameof(tsmiCConfigFunction);
                    tsmiCConfigQuery.Text = nameof(tsmiCConfigQuery);
                    tsmiCEstimationCell.Text = nameof(tsmiCEstimationCell);
                    tsmiCEstimationCellCollection.Text = nameof(tsmiCEstimationCellCollection);
                    tsmiCExtVersion.Text = nameof(tsmiCExtVersion);
                    tsmiCGUIVersion.Text = nameof(tsmiCGUIVersion);

                    tsmiMappingTables.Text = nameof(tsmiMappingTables);
                    tsmiCmEstimationCellCollection.Text = nameof(tsmiCmEstimationCellCollection);
                    tsmiCmExtConfigFunction.Text = nameof(tsmiCmExtConfigFunction);
                    tsmiCmExtGUIVersion.Text = nameof(tsmiCmExtGUIVersion);

                    tsmiSpatialTables.Text = nameof(tsmiSpatialTables);
                    tsmiFaCell.Text = nameof(tsmiFaCell);
                    tsmiFpPlot.Text = nameof(tsmiFpPlot);

                    tsmiDataTables.Text = nameof(tsmiDataTables);
                    tsmiTAuxData.Text = nameof(tsmiTAuxData);
                    tsmiTAuxTotal.Text = nameof(tsmiTAuxTotal);
                    tsmiTConfig.Text = nameof(tsmiTConfig);
                    tsmiTConfigCollection.Text = nameof(tsmiTConfigCollection);
                    tsmiTEstimationCellHierarchy.Text = nameof(tsmiTEstimationCellHierarchy);

                    tsmiStoredProcedures.Text = nameof(tsmiStoredProcedures);
                    tsmiFnApiGetEstimationCellHierarchy.Text = nameof(tsmiFnApiGetEstimationCellHierarchy);
                    tsmiFnApiGetEstimationCellHierarchyStage.Text = nameof(tsmiFnApiGetEstimationCellHierarchyStage);
                    tsmiFnGetAuxDataApp.Text = nameof(tsmiFnGetAuxDataApp);
                    tsmiFnGetAuxDataCount.Text = nameof(tsmiFnGetAuxDataCount);
                    tsmiFnGetAuxTotal.Text = nameof(tsmiFnGetAuxTotal);
                    tsmiFnGetAuxTotalCount.Text = nameof(tsmiFnGetAuxTotalCount);
                    tsmiFnGetConfigsForAuxDataApp.Text = nameof(tsmiFnGetConfigsForAuxDataApp);
                    tsmiFnGetGidsForAuxTotalApp.Text = nameof(tsmiFnGetGidsForAuxTotalApp);
                    tsmiFnGetIntervalPointsApp.Text = nameof(tsmiFnGetIntervalPointsApp);

                    tsmiETL.Text = nameof(tsmiETL);
                    tsmiExtractData.Text = nameof(tsmiExtractData);
                    tsmiGetDifferences.Text = nameof(tsmiGetDifferences);

                    btnClose.Text = nameof(btnClose);

                    msgDataExportComplete = nameof(msgDataExportComplete);
                    msgNoDifferencesFound = nameof(msgNoDifferencesFound);

                    break;
            }
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek</para>
        /// <para lang="en">Uploading database table data</para>
        /// </summary>
        public void LoadContent() { }

        /// <summary>
        /// <para lang="cs">Zobrazí data číselníku</para>
        /// <para lang="en">Display lookup table data</para>
        /// </summary>
        /// <param name="table">
        /// <para lang="cs">Tabulka</para>
        /// <para lang="en">Table</para>
        /// </param>
        /// <param name="columns">
        /// <para lang="cs">Sloupce tabulky</para>
        /// <para lang="en">Table columns</para>
        /// </param>
        private void DisplayTable(
            ALookupTable table,
            Dictionary<string, ColumnMetadata> columns)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(value: dgvData);
            table.ReLoad(limit: Limit, extended: false);
            ALookupTable.SetColumnsVisibility(columns: columns);
            table.SetDataGridViewDataSource(dataGridView: dgvData);
            table.FormatDataGridView(dataGridView: dgvData);
            grpMain.Text = table.Caption;
            txtSQL.Text = $"{Environment.NewLine}{table.SQL}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(box: txtSQL);
        }

        /// <summary>
        /// <para lang="cs">Zobrazí data mapovací tabulky</para>
        /// <para lang="en">Display mapping table data</para>
        /// </summary>
        /// <param name="table">
        /// <para lang="cs">Tabulka</para>
        /// <para lang="en">Table</para>
        /// </param>
        /// <param name="columns">
        /// <para lang="cs">Sloupce tabulky</para>
        /// <para lang="en">Table columns</para>
        /// </param>
        private void DisplayTable(
            AMappingTable table,
            Dictionary<string, ColumnMetadata> columns)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(value: dgvData);
            table.ReLoad(limit: Limit);
            AMappingTable.SetColumnsVisibility(columns: columns);
            table.SetDataGridViewDataSource(dataGridView: dgvData);
            table.FormatDataGridView(dataGridView: dgvData);
            grpMain.Text = table.Caption;
            txtSQL.Text = $"{Environment.NewLine}{table.SQL}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(box: txtSQL);
        }

        /// <summary>
        /// <para lang="cs">Zobrazí data tabulky s prostorovými daty</para>
        /// <para lang="en">Display data table with spatial data</para>
        /// </summary>
        /// <param name="table">
        /// <para lang="cs">Tabulka</para>
        /// <para lang="en">Table</para>
        /// </param>
        /// <param name="columns">
        /// <para lang="cs">Sloupce tabulky</para>
        /// <para lang="en">Table columns</para>
        /// </param>
        private void DisplayTable(
            ASpatialTable table,
            Dictionary<string, ColumnMetadata> columns)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(value: dgvData);
            table.ReLoad(limit: Limit, withGeom: false);
            ASpatialTable.SetColumnsVisibility(columns: columns);
            table.SetDataGridViewDataSource(dataGridView: dgvData);
            table.FormatDataGridView(dataGridView: dgvData);
            grpMain.Text = table.Caption;
            txtSQL.Text = $"{Environment.NewLine}{table.SQL}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(box: txtSQL);
        }

        /// <summary>
        /// <para lang="cs">Zobrazí data tabulky</para>
        /// <para lang="en">Display table data</para>
        /// </summary>
        /// <param name="table">
        /// <para lang="cs">Tabulka</para>
        /// <para lang="en">Table</para>
        /// </param>
        /// <param name="columns">
        /// <para lang="cs">Sloupce tabulky</para>
        /// <para lang="en">Table columns</para>
        /// </param>
        private void DisplayTable(
            ADataTable table,
            Dictionary<string, ColumnMetadata> columns)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(value: dgvData);
            table.ReLoad(limit: Limit);
            ADataTable.SetColumnsVisibility(columns: columns);
            table.SetDataGridViewDataSource(dataGridView: dgvData);
            table.FormatDataGridView(dataGridView: dgvData);
            grpMain.Text = table.Caption;
            txtSQL.Text = $"{Environment.NewLine}{table.SQL}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(box: txtSQL);
        }

        /// <summary>
        /// <para lang="cs">Zobrazí data vrácená uloženou procedurou</para>
        /// <para lang="en">Display data returned from stored procedure</para>
        /// </summary>
        /// <param name="table">
        /// <para lang="cs">Tabulka s daty vrácenými uloženou procedurou</para>
        /// <para lang="en">Table with data returned from stored procedure</para>
        /// </param>
        /// <param name="columns">
        /// <para lang="cs">Sloupce tabulky</para>
        /// <para lang="en">Table columns</para>
        /// </param>
        private void DisplayTable(
            AParametrizedView table,
            Dictionary<string, ColumnMetadata> columns)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(value: dgvData);
            ADataTable.SetColumnsVisibility(columns: columns);
            table.SetDataGridViewDataSource(dataGridView: dgvData);
            table.FormatDataGridView(dataGridView: dgvData);
            grpMain.Text = table.Caption;
            txtSQL.Text = $"{Environment.NewLine}{table.SQL}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(box: txtSQL);
        }

        /// <summary>
        /// <para lang="cs">Export dat</para>
        /// <para lang="en">Data export</para>
        /// </summary>
        private void ExportData()
        {
            ADSchema.ExportData(
               database: Database,
               fileFormat: ExportFileFormat.SqlInsertAll);

            MessageBox.Show(
                caption: String.Empty,
                text: msgDataExportComplete,
                buttons: MessageBoxButtons.OK,
                icon: MessageBoxIcon.Information);
        }

        /// <summary>
        /// <para lang="cs">Zobrazí provedené změny v databázi</para>
        /// <para lang="en">Displays changes made to the database</para>
        /// </summary>
        private void DisplayDifferences()
        {
            grpMain.Text = String.Empty;
            pnlData.Controls.Clear();
            txtSQL.Text = String.Empty;

            List<string> differences = Database.SAuxiliaryData.Differences;
            System.Text.StringBuilder stringBuilder = new();
            if ((differences == null) || (differences.Count == 0))
            {
                stringBuilder.AppendLine(value: msgNoDifferencesFound);
            }
            else
            {
                foreach (string difference in differences)
                {
                    stringBuilder.AppendLine(value: $"* {difference}");
                }
            }

            Label lblReport = new()
            {
                Dock = DockStyle.Fill,
                Text = stringBuilder.ToString()
            };
            pnlData.Controls.Add(value: lblReport);
        }

        #endregion Methods

    }

}