﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Versioning;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.Catalog;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace AuxiliaryData
        {

            /// <summary>
            /// Schema gisdata
            /// </summary>
            public class ADSchema
                : IDatabaseSchema
            {

                #region Constants

                /// <summary>
                /// Schema name,
                /// (Contains default value,
                /// value is not constant now,
                /// extension can be created with different schemas)
                /// </summary>
                public static string Name = "gisdata";

                /// <summary>
                /// Extension name
                /// </summary>
                public const string ExtensionName = "nfiesta_gisdata";

                /// <summary>
                /// Extension version
                /// </summary>
                public static readonly ZaJi.PostgreSQL.DataModel.ExtensionVersion ExtensionVersion =
                    new(version: "3.1.0");

                #endregion Constants


                #region Private Fields

                #region General

                /// <summary>
                /// Database
                /// </summary>
                private IDatabase database;

                /// <summary>
                /// Omitted tables
                /// </summary>
                private List<string> omittedTables;

                #endregion General


                #region Lookup Tables

                /// <summary>
                /// List of configurations for the auxiliary variable totals calculation
                /// </summary>
                private ZaJi.NfiEstaPg.AuxiliaryData.ConfigList tConfig;

                /// <summary>
                /// List of configuration collections
                /// </summary>
                private ZaJi.NfiEstaPg.AuxiliaryData.ConfigCollectionList tConfigCollection;

                /// <summary>
                /// List of configuration functions
                /// </summary>
                private ZaJi.NfiEstaPg.AuxiliaryData.ConfigFunctionList cConfigFunction;

                /// <summary>
                /// List of variants of the auxiliary variables totals calculation
                /// </summary>
                private ZaJi.NfiEstaPg.AuxiliaryData.ConfigQueryList cConfigQuery;

                /// <summary>
                /// List of estimation cells
                /// </summary>
                private ZaJi.NfiEstaPg.AuxiliaryData.EstimationCellList cEstimationCell;

                /// <summary>
                /// List of estimation cell collections
                /// </summary>
                private ZaJi.NfiEstaPg.AuxiliaryData.EstimationCellCollectionList cEstimationCellCollection;

                /// <summary>
                /// List of extension versions
                /// </summary>
                private ZaJi.NfiEstaPg.AuxiliaryData.ExtensionVersionList cExtensionVersion;

                /// <summary>
                /// List of GUI versions
                /// </summary>
                private ZaJi.NfiEstaPg.AuxiliaryData.GUIVersionList cGUIVersion;

                #endregion Lookup Tables


                #region Mapping Tables

                /// <summary>
                /// List of estimation cell mappings
                /// </summary>
                private ZaJi.NfiEstaPg.AuxiliaryData.EstimationCellHierarchyList tEstimationCellHierarchy;

                /// <summary>
                /// List of mappings estimation cell into estimation cell collection
                /// </summary>
                private ZaJi.NfiEstaPg.AuxiliaryData.EstimationCellMappingList cmEstimationCell;

                /// <summary>
                /// List of mappings between estimation cell collections
                /// </summary>
                private ZaJi.NfiEstaPg.AuxiliaryData.EstimationCellCollectionMappingList cmEstimationCellCollection;

                /// <summary>
                /// List of mappings between extension version and configuration function
                /// </summary>
                private ZaJi.NfiEstaPg.AuxiliaryData.ExtensionToConfigFunctionList cmExtConfigFunction;

                /// <summary>
                /// List of mappings between extension version and GUI version
                /// </summary>
                private ZaJi.NfiEstaPg.AuxiliaryData.ExtensionToGUIVersionList cmExtGUIVersion;

                #endregion Mapping Tables


                #region Spatial Data Tables

                /// <summary>
                /// List of estimation cell segments
                /// </summary>
                private ZaJi.NfiEstaPg.AuxiliaryData.CellList faCell;

                /// <summary>
                /// List of inventory plots
                /// </summary>
                private ZaJi.NfiEstaPg.AuxiliaryData.PlotList fpPlot;

                #endregion Spatial Data Tables


                #region Data Tables

                /// <summary>
                /// List of totals of auxiliary variable within estimation cell
                /// </summary>
                private ZaJi.NfiEstaPg.AuxiliaryData.AuxTotalList tAuxTotal;

                /// <summary>
                /// List of auxiliary plot data
                /// </summary>
                private ZaJi.NfiEstaPg.AuxiliaryData.AuxDataList tAuxiliaryData;

                #endregion Data Tables


                #region Data Views

                /// <summary>
                /// Estimation cells with hierachy - data view
                /// </summary>
                private ZaJi.NfiEstaPg.AuxiliaryData.TFnApiGetEstimationCellHierarchyList vEstimationCell;

                /// <summary>
                /// Count of auxiliary variable totals for configuration, extension version and gui version - data view
                /// </summary>
                private ZaJi.NfiEstaPg.AuxiliaryData.TFnGetAuxTotalCountList vAuxTotalCount;

                /// <summary>
                /// Count of auxiliary variable values for configuration, extension version and gui version - data view
                /// </summary>
                private ZaJi.NfiEstaPg.AuxiliaryData.TFnGetAuxDataCountList vAuxDataCount;

                #endregion Data Views

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database</param>
                public ADSchema(NfiEstaDB database)
                {

                    #region General

                    Database = database;

                    OmittedTables = [];

                    #endregion General


                    #region Lookup Tables

                    TConfig = new ZaJi.NfiEstaPg.AuxiliaryData.ConfigList(
                        database: (NfiEstaDB)Database);
                    TConfigCollection = new ZaJi.NfiEstaPg.AuxiliaryData.ConfigCollectionList(
                        database: (NfiEstaDB)Database);
                    CConfigFunction = new ZaJi.NfiEstaPg.AuxiliaryData.ConfigFunctionList(
                        database: (NfiEstaDB)Database);
                    CConfigQuery = new ZaJi.NfiEstaPg.AuxiliaryData.ConfigQueryList(
                        database: (NfiEstaDB)Database);
                    CEstimationCell = new ZaJi.NfiEstaPg.AuxiliaryData.EstimationCellList(
                        database: (NfiEstaDB)Database);
                    CEstimationCellCollection = new ZaJi.NfiEstaPg.AuxiliaryData.EstimationCellCollectionList(
                        database: (NfiEstaDB)Database);
                    CExtensionVersion = new ZaJi.NfiEstaPg.AuxiliaryData.ExtensionVersionList(
                        database: (NfiEstaDB)Database);
                    CGUIVersion = new ZaJi.NfiEstaPg.AuxiliaryData.GUIVersionList(
                        database: (NfiEstaDB)Database);

                    #endregion Lookup Tables


                    #region Mapping Tables

                    TEstimationCellHierarchy = new ZaJi.NfiEstaPg.AuxiliaryData.EstimationCellHierarchyList(
                        database: (NfiEstaDB)Database);
                    CmEstimationCell = new ZaJi.NfiEstaPg.AuxiliaryData.EstimationCellMappingList(
                        database: (NfiEstaDB)Database);
                    CmEstimationCellCollection = new ZaJi.NfiEstaPg.AuxiliaryData.EstimationCellCollectionMappingList(
                        database: (NfiEstaDB)Database);
                    CmExtConfigFunction = new ZaJi.NfiEstaPg.AuxiliaryData.ExtensionToConfigFunctionList(
                        database: (NfiEstaDB)Database);
                    CmExtGUIVersion = new ZaJi.NfiEstaPg.AuxiliaryData.ExtensionToGUIVersionList(
                        database: (NfiEstaDB)Database);

                    #endregion Mapping Tables


                    #region Spatial Data Tables

                    FaCell = new ZaJi.NfiEstaPg.AuxiliaryData.CellList(
                        database: (NfiEstaDB)Database);
                    FpPlot = new ZaJi.NfiEstaPg.AuxiliaryData.PlotList(
                        database: (NfiEstaDB)Database);

                    #endregion Spatial Data Tables


                    #region Data Tables

                    TAuxTotal = new ZaJi.NfiEstaPg.AuxiliaryData.AuxTotalList(
                        database: (NfiEstaDB)Database);
                    TAuxiliaryData = new ZaJi.NfiEstaPg.AuxiliaryData.AuxDataList(
                        database: (NfiEstaDB)Database);

                    #endregion Data Tables


                    #region Data Views

                    VEstimationCell =
                        new ZaJi.NfiEstaPg.AuxiliaryData.TFnApiGetEstimationCellHierarchyList(
                            database: (NfiEstaDB)Database);

                    VAuxTotalCount =
                        new ZaJi.NfiEstaPg.AuxiliaryData.TFnGetAuxTotalCountList(
                            database: (NfiEstaDB)Database);

                    VAuxDataCount =
                        new ZaJi.NfiEstaPg.AuxiliaryData.TFnGetAuxDataCountList(
                            database: (NfiEstaDB)Database);

                    #endregion Data Views

                }

                #endregion Constructor


                #region Properties

                #region General

                /// <summary>
                /// Schema name (read-only)
                /// </summary>
                public string SchemaName
                {
                    get
                    {
                        return ADSchema.Name;
                    }
                }

                /// <summary>
                /// Database (not-null) (read-only)
                /// </summary>
                public IDatabase Database
                {
                    get
                    {
                        if (database == null)
                        {
                            throw new ArgumentNullException(
                                message: $"Argument {nameof(Database)} must not be null.",
                                paramName: nameof(Database));
                        }

                        if (database is not NfiEstaDB)
                        {
                            throw new ArgumentException(
                                message: $"Argument {nameof(Database)} must be type of {nameof(NfiEstaDB)}.",
                                paramName: nameof(Database));
                        }

                        return database;
                    }
                    private set
                    {
                        if (value == null)
                        {
                            throw new ArgumentNullException(
                                message: $"Argument {nameof(Database)} must not be null.",
                                paramName: nameof(Database));
                        }

                        if (value is not NfiEstaDB)
                        {
                            throw new ArgumentException(
                                message: $"Argument {nameof(Database)} must be type of {nameof(NfiEstaDB)}.",
                                paramName: nameof(Database));
                        }

                        database = value;
                    }
                }

                /// <summary>
                /// List of lookup tables (not-null) (read-only)
                /// </summary>
                public List<ILookupTable> LookupTables
                {
                    get
                    {
                        return
                        [
                            TConfig,
                            TConfigCollection,
                            CConfigFunction,
                            CConfigQuery,
                            CEstimationCell,
                            CEstimationCellCollection,
                            CExtensionVersion,
                            CGUIVersion
                        ];
                    }
                }

                /// <summary>
                /// List of mapping tables (not-null) (read-only)
                /// </summary>
                public List<IMappingTable> MappingTables
                {
                    get
                    {
                        return
                        [
                            TEstimationCellHierarchy,
                            CmEstimationCell,
                            CmEstimationCellCollection,
                            CmExtConfigFunction,
                            CmExtGUIVersion
                        ];
                    }
                }

                /// <summary>
                /// List of spatial data tables (not-null) (read-only)
                /// </summary>
                public List<ISpatialTable> SpatialTables
                {
                    get
                    {
                        return
                        [
                            FaCell,
                            FpPlot
                        ];
                    }
                }

                /// <summary>
                /// List of data tables (not-null) (read-only)
                /// </summary>
                public List<IDataTable> DataTables
                {
                    get
                    {
                        return
                        [
                            TAuxTotal,
                            TAuxiliaryData
                        ];
                    }
                }

                /// <summary>
                /// List of all tables (not-null) (read-only)
                /// </summary>
                public List<IDatabaseTable> Tables
                {
                    get
                    {
                        List<IDatabaseTable> tables = [];
                        tables.AddRange(collection: LookupTables);
                        tables.AddRange(collection: MappingTables);
                        tables.AddRange(collection: SpatialTables);
                        tables.AddRange(collection: DataTables);
                        tables.Sort(comparison: (a, b) => a.TableName.CompareTo(strB: b.TableName));
                        return tables;
                    }
                }

                /// <summary>
                /// Omitted tables
                /// </summary>
                public List<string> OmittedTables
                {
                    get
                    {
                        return omittedTables ?? [];
                    }
                    set
                    {
                        omittedTables = value ?? [];
                    }
                }

                #endregion General


                #region Lookup Tables

                /// <summary>
                /// List of configurations for the auxiliary variable totals calculation  (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.AuxiliaryData.ConfigList TConfig
                {
                    get
                    {
                        return tConfig ??
                            new ZaJi.NfiEstaPg.AuxiliaryData.ConfigList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        tConfig = value ??
                            new ZaJi.NfiEstaPg.AuxiliaryData.ConfigList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of configuration collections  (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.AuxiliaryData.ConfigCollectionList TConfigCollection
                {
                    get
                    {
                        return tConfigCollection ??
                            new ZaJi.NfiEstaPg.AuxiliaryData.ConfigCollectionList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        tConfigCollection = value ??
                            new ZaJi.NfiEstaPg.AuxiliaryData.ConfigCollectionList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of configuration functions (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.AuxiliaryData.ConfigFunctionList CConfigFunction
                {
                    get
                    {
                        return cConfigFunction ??
                            new ZaJi.NfiEstaPg.AuxiliaryData.ConfigFunctionList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        cConfigFunction = value ??
                            new ZaJi.NfiEstaPg.AuxiliaryData.ConfigFunctionList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of variants of the auxiliary variables totals calculation (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.AuxiliaryData.ConfigQueryList CConfigQuery
                {
                    get
                    {
                        return cConfigQuery ??
                            new ZaJi.NfiEstaPg.AuxiliaryData.ConfigQueryList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        cConfigQuery = value ??
                            new ZaJi.NfiEstaPg.AuxiliaryData.ConfigQueryList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of estimation cells (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.AuxiliaryData.EstimationCellList CEstimationCell
                {
                    get
                    {
                        return cEstimationCell ??
                            new ZaJi.NfiEstaPg.AuxiliaryData.EstimationCellList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        cEstimationCell = value ??
                            new ZaJi.NfiEstaPg.AuxiliaryData.EstimationCellList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of estimation cell collections (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.AuxiliaryData.EstimationCellCollectionList CEstimationCellCollection
                {
                    get
                    {
                        return cEstimationCellCollection ??
                            new ZaJi.NfiEstaPg.AuxiliaryData.EstimationCellCollectionList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        cEstimationCellCollection = value ??
                            new ZaJi.NfiEstaPg.AuxiliaryData.EstimationCellCollectionList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of extension versions (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.AuxiliaryData.ExtensionVersionList CExtensionVersion
                {
                    get
                    {
                        return cExtensionVersion ??
                            new ZaJi.NfiEstaPg.AuxiliaryData.ExtensionVersionList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        cExtensionVersion = value ??
                            new ZaJi.NfiEstaPg.AuxiliaryData.ExtensionVersionList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of GUI versions (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.AuxiliaryData.GUIVersionList CGUIVersion
                {
                    get
                    {
                        return cGUIVersion ??
                            new ZaJi.NfiEstaPg.AuxiliaryData.GUIVersionList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        cGUIVersion = value ??
                            new ZaJi.NfiEstaPg.AuxiliaryData.GUIVersionList(database: (NfiEstaDB)Database);
                    }
                }

                #endregion Lookup Tables


                #region Mapping Tables

                /// <summary>
                /// List of estimation cell mappings
                /// </summary>
                public ZaJi.NfiEstaPg.AuxiliaryData.EstimationCellHierarchyList TEstimationCellHierarchy
                {
                    get
                    {
                        return tEstimationCellHierarchy ??
                            new ZaJi.NfiEstaPg.AuxiliaryData.EstimationCellHierarchyList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        tEstimationCellHierarchy = value ??
                            new ZaJi.NfiEstaPg.AuxiliaryData.EstimationCellHierarchyList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of mappings estimation cell into estimation cell collection (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.AuxiliaryData.EstimationCellMappingList CmEstimationCell
                {
                    get
                    {
                        return cmEstimationCell ??
                            new ZaJi.NfiEstaPg.AuxiliaryData.EstimationCellMappingList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        cmEstimationCell = value ??
                            new ZaJi.NfiEstaPg.AuxiliaryData.EstimationCellMappingList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of mappings between estimation cell collections (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.AuxiliaryData.EstimationCellCollectionMappingList CmEstimationCellCollection
                {
                    get
                    {
                        return cmEstimationCellCollection ??
                            new ZaJi.NfiEstaPg.AuxiliaryData.EstimationCellCollectionMappingList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        cmEstimationCellCollection = value ??
                            new ZaJi.NfiEstaPg.AuxiliaryData.EstimationCellCollectionMappingList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of mappings between extension version and configuration function (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.AuxiliaryData.ExtensionToConfigFunctionList CmExtConfigFunction
                {
                    get
                    {
                        return cmExtConfigFunction ??
                            new ZaJi.NfiEstaPg.AuxiliaryData.ExtensionToConfigFunctionList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        cmExtConfigFunction = value ??
                            new ZaJi.NfiEstaPg.AuxiliaryData.ExtensionToConfigFunctionList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of mappings between extension version and GUI version (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.AuxiliaryData.ExtensionToGUIVersionList CmExtGUIVersion
                {
                    get
                    {
                        return cmExtGUIVersion ??
                            new ZaJi.NfiEstaPg.AuxiliaryData.ExtensionToGUIVersionList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        cmExtGUIVersion = value ??
                            new ZaJi.NfiEstaPg.AuxiliaryData.ExtensionToGUIVersionList(database: (NfiEstaDB)Database);
                    }
                }

                #endregion Mapping Tables


                #region Spatial Data Tables

                /// <summary>
                /// List of estimation cell segments  (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.AuxiliaryData.CellList FaCell
                {
                    get
                    {
                        return faCell ??
                            new ZaJi.NfiEstaPg.AuxiliaryData.CellList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        faCell = value ??
                            new ZaJi.NfiEstaPg.AuxiliaryData.CellList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of inventory plots  (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.AuxiliaryData.PlotList FpPlot
                {
                    get
                    {
                        return fpPlot ??
                            new ZaJi.NfiEstaPg.AuxiliaryData.PlotList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        fpPlot = value ??
                            new ZaJi.NfiEstaPg.AuxiliaryData.PlotList(database: (NfiEstaDB)Database);
                    }
                }

                #endregion Spatial Data Tables


                #region Data Tables

                /// <summary>
                /// List of totals of auxiliary variable within estimation cell  (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.AuxiliaryData.AuxTotalList TAuxTotal
                {
                    get
                    {
                        return tAuxTotal ??
                            new ZaJi.NfiEstaPg.AuxiliaryData.AuxTotalList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        tAuxTotal = value ??
                            new ZaJi.NfiEstaPg.AuxiliaryData.AuxTotalList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of auxiliary plot data  (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.AuxiliaryData.AuxDataList TAuxiliaryData
                {
                    get
                    {
                        return tAuxiliaryData ??
                            new ZaJi.NfiEstaPg.AuxiliaryData.AuxDataList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        tAuxiliaryData = value ??
                            new ZaJi.NfiEstaPg.AuxiliaryData.AuxDataList(database: (NfiEstaDB)Database);
                    }
                }

                #endregion Data Tables


                #region Data Views

                /// <summary>
                /// Estimation cells with hierachy - data view
                /// </summary>
                public ZaJi.NfiEstaPg.AuxiliaryData.TFnApiGetEstimationCellHierarchyList VEstimationCell
                {
                    get
                    {
                        return vEstimationCell ??
                            new ZaJi.NfiEstaPg.AuxiliaryData.TFnApiGetEstimationCellHierarchyList(database: (NfiEstaDB)Database);
                    }
                    set
                    {
                        vEstimationCell = value ??
                            new ZaJi.NfiEstaPg.AuxiliaryData.TFnApiGetEstimationCellHierarchyList(database: (NfiEstaDB)Database);
                    }
                }


                /// <summary>
                /// Count of auxiliary variable totals for configuration, extension version and gui version - data view
                /// </summary>
                public ZaJi.NfiEstaPg.AuxiliaryData.TFnGetAuxTotalCountList VAuxTotalCount
                {
                    get
                    {
                        return vAuxTotalCount ??
                            new ZaJi.NfiEstaPg.AuxiliaryData.TFnGetAuxTotalCountList(database: (NfiEstaDB)Database);
                    }
                    set
                    {
                        vAuxTotalCount = value ??
                            new ZaJi.NfiEstaPg.AuxiliaryData.TFnGetAuxTotalCountList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// Count of auxiliary variable values for configuration, extension version and gui version - data view
                /// </summary>
                public ZaJi.NfiEstaPg.AuxiliaryData.TFnGetAuxDataCountList VAuxDataCount
                {
                    get
                    {
                        return vAuxDataCount ??
                            new ZaJi.NfiEstaPg.AuxiliaryData.TFnGetAuxDataCountList(database: (NfiEstaDB)Database);
                    }
                    set
                    {
                        vAuxDataCount = value ??
                            new ZaJi.NfiEstaPg.AuxiliaryData.TFnGetAuxDataCountList(database: (NfiEstaDB)Database);
                    }
                }

                #endregion Data Views


                #region Differences

                /// <summary>
                /// Differences in database schema (not-null) (read-only)
                /// </summary>
                public List<string> Differences
                {
                    get
                    {
                        List<string> result = [];
                        Database.Postgres.Catalog.Load();

                        DBExtension dbExtension = Database.Postgres.Catalog.Extensions.Items
                            .Where(a => a.Name == ADSchema.ExtensionName)
                            .FirstOrDefault();

                        if (dbExtension == null)
                        {
                            result.Add(item: String.Concat(
                                $"Extension {ADSchema.ExtensionName} does not exist in database."));
                            return result;
                        }
                        else
                        {
                            ZaJi.PostgreSQL.DataModel.ExtensionVersion dbExtensionVersion =
                                new(
                                 version: dbExtension.Version);
                            if (!dbExtensionVersion.Equals(obj: ADSchema.ExtensionVersion))
                            {
                                result.Add(item: String.Concat(
                                    $"Database extension {ADSchema.ExtensionName} is in version {dbExtension.Version}. ",
                                    $"But the version {ADSchema.ExtensionVersion} was expected."));
                            }

                            DBSchema dbSchema = Database.Postgres.Catalog.Schemas.Items
                                .Where(a => a.Name == ADSchema.Name)
                                .FirstOrDefault();

                            if (dbSchema == null)
                            {
                                result.Add(item: String.Concat(
                                    $"Schema {ADSchema.Name} does not exist in database."));
                                return result;
                            }
                            else
                            {
                                foreach (DBTable dbTable in dbSchema.Tables.Items.OrderBy(a => a.Name))
                                {
                                    if (OmittedTables.Contains(item: dbTable.Name))
                                    {
                                        continue;
                                    }

                                    IDatabaseTable iTable = ((NfiEstaDB)Database).SAuxiliaryData.Tables
                                        .Where(a => a.TableName == dbTable.Name)
                                        .FirstOrDefault();

                                    if (iTable == null)
                                    {
                                        result.Add(item: String.Concat(
                                            $"Class for table {dbTable.Name} does not exist in dll library."));
                                    }
                                }

                                foreach (IDatabaseTable iTable in ((NfiEstaDB)Database).SAuxiliaryData.Tables.OrderBy(a => a.TableName))
                                {
                                    DBTable dbTable = dbSchema.Tables.Items
                                            .Where(a => a.Name == iTable.TableName)
                                            .FirstOrDefault();

                                    if (dbTable == null)
                                    {
                                        if (iTable.Columns.Values.Where(a => a.Elemental).Any())
                                        {
                                            result.Add(item: String.Concat(
                                                $"Table {iTable.TableName} does not exist in database."));
                                        }
                                        else
                                        {
                                            // Table doesn't contain any column that is loaded from database.
                                        }
                                    }
                                    else
                                    {
                                        foreach (DBColumn dbColumn in dbTable.Columns.Items.OrderBy(a => a.Name))
                                        {
                                            ColumnMetadata iColumn = iTable.Columns.Values
                                                .Where(a => a.Elemental)
                                                .Where(a => a.DbName == dbColumn.Name)
                                                .FirstOrDefault();

                                            if (iColumn == null)
                                            {
                                                result.Add(item: String.Concat(
                                                    $"Column {dbTable.Name}.{dbColumn.Name} does not exist in dll library."));
                                            }
                                        }

                                        foreach (ColumnMetadata iColumn in iTable.Columns.Values.Where(a => a.Elemental).OrderBy(a => a.DbName))
                                        {
                                            DBColumn dbColumn = dbTable.Columns.Items
                                                .Where(a => a.Name == iColumn.DbName)
                                                .FirstOrDefault();

                                            if (dbColumn == null)
                                            {
                                                result.Add(item: String.Concat(
                                                    $"Column {iTable.TableName}.{iColumn.DbName} does not exist in database."));
                                            }
                                            else
                                            {
                                                if (dbColumn.TypeName != iColumn.DbDataType)
                                                {
                                                    result.Add(String.Concat(
                                                        $"Column {iTable.TableName}.{iColumn.DbName} is type of {dbColumn.TypeName}. ",
                                                        $"But type of {iColumn.DbDataType} was expected."));
                                                }

                                                if (dbColumn.NotNull != iColumn.NotNull)
                                                {
                                                    string strDbNull = (bool)dbColumn.NotNull ? "has NOT NULL constraint" : "is NULLABLE";
                                                    string strINull = iColumn.NotNull ? "NOT NULL contraint" : "NULLABLE";
                                                    result.Add(String.Concat(
                                                        $"Column {iTable.TableName}.{iColumn.DbName} {strDbNull}. ",
                                                        $"But {strINull} was expected."));
                                                }
                                            }
                                        }
                                    }
                                }

                                Dictionary<string, string> iStoredProcedures = [];
                                foreach (
                                    Type nestedType in typeof(ZaJi.NfiEstaPg.AuxiliaryData.ADFunctions)
                                    .GetNestedTypes()
                                    .AsEnumerable()
                                    .OrderBy(a => a.FullName)
                                    )
                                {
                                    FieldInfo fieldSignature = nestedType.GetField(name: "Signature");
                                    FieldInfo fieldName = nestedType.GetField(name: "Name");
                                    if ((fieldSignature != null) && (fieldName != null))
                                    {
                                        string a = fieldSignature.GetValue(obj: null).ToString();
                                        string b = fieldName.GetValue(obj: null).ToString();

                                        iStoredProcedures.TryAdd(key: a, value: b);
                                    }

                                    PropertyInfo pSignature = nestedType.GetProperty(name: "Signature");
                                    PropertyInfo pName = nestedType.GetProperty(name: "Name");
                                    if ((pSignature != null) && (pName != null))
                                    {
                                        // signature = null označeje uložené procedury,
                                        // pro které není požadována implementace v databázi
                                        // nekontrolují se
                                        if (pSignature.GetValue(obj: null) != null)
                                        {
                                            string a = pSignature.GetValue(obj: null).ToString();
                                            string b = pName.GetValue(obj: null).ToString();

                                            iStoredProcedures.TryAdd(key: a, value: b);
                                        }
                                    }
                                }

                                foreach (DBStoredProcedure dbStoredProcedure in dbSchema.StoredProcedures.Items.OrderBy(a => a.Name))
                                {
                                    if (!iStoredProcedures.ContainsKey(dbStoredProcedure.Signature))
                                    {
                                        result.Add(String.Concat(
                                           $"Class for stored procedure {dbStoredProcedure.Name} does not exist in dll library."));
                                    }
                                }

                                foreach (KeyValuePair<string, string> iStoredProcedure in iStoredProcedures)
                                {
                                    DBStoredProcedure dbStoredProcedure = dbSchema.StoredProcedures.Items
                                            .Where(a => a.Signature == iStoredProcedure.Key)
                                            .FirstOrDefault();
                                    if (dbStoredProcedure == null)
                                    {
                                        result.Add(String.Concat(
                                               $"Stored procedure {iStoredProcedure.Value} does not exist in database."));
                                    }
                                }

                                return result;
                            }
                        }
                    }
                }

                #endregion Differences

                #endregion Properties


                #region Methods

                /// <summary>
                /// Returns the string that represents current object
                /// </summary>
                /// <returns>Returns the string that represents current object</returns>
                public override string ToString()
                {
                    return Database.Postgres.Setting.LanguageVersion switch
                    {
                        LanguageVersion.National => String.Concat($"Data schématu {ADSchema.Name}."),
                        LanguageVersion.International => String.Concat($"Data from schema {ADSchema.Name}."),
                        _ => String.Concat($"Data from schema {ADSchema.Name}."),
                    };
                }

                #endregion Methods


                #region Static Methods

                /// <summary>
                /// Exports data of the schema
                /// </summary>
                /// <param name="database">Database tables</param>
                /// <param name="fileFormat">Format of the file with exported data</param>
                /// <param name="tableNames">List of table names to export (if null than all tables)</param>
                [SupportedOSPlatform("windows")]
                public static void ExportData(
                    NfiEstaDB database,
                    ExportFileFormat fileFormat,
                    List<string> tableNames = null)
                {
                    System.Windows.Forms.FolderBrowserDialog dlg
                        = new();

                    if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        ExportData(
                            database: database,
                            fileFormat: fileFormat,
                            folder: dlg.SelectedPath,
                            tableNames: tableNames);
                    }
                }

                /// <summary>
                /// Exports data of the schema
                /// </summary>
                /// <param name="database">Database tables</param>
                /// <param name="fileFormat">Format of the file with exported data</param>
                /// <param name="folder">Folder for writing file with data</param>
                /// <param name="tableNames">List of table names to export (if null than all tables)</param>
                /// <param name="pageSize">Number of rowns in one file</param>
                public static void ExportData(
                    NfiEstaDB database,
                    ExportFileFormat fileFormat,
                    string folder,
                    List<string> tableNames = null,
                    int pageSize = 1000000)
                {
                    List<ADatabaseTable> databaseTables =
                    [
                            // c_config_function (statická data - vytvořená při instalaci extenze)
                            // new ZaJi.NfiEstaPg.AuxiliaryData.ConfigFunctionList(database: database),

                            // c_config_query (statická data - vytvořená při instalaci extenze)
                            // new ZaJi.NfiEstaPg.AuxiliaryData.ConfigQueryList(database: database),

                        // c_estimation_cell
                        new ZaJi.NfiEstaPg.AuxiliaryData.EstimationCellList(database: database),

                        // c_estimation_cell_collection
                        new ZaJi.NfiEstaPg.AuxiliaryData.EstimationCellCollectionList(database: database),

                            // c_ext_version (statická data - vytvořená při instalaci extenze)
                            // new ZaJi.NfiEstaPg.AuxiliaryData.ExtensionVersionList(database: database),

                            // c_gui_version (statická data - vytvořená při instalaci extenze)
                            // new ZaJi.NfiEstaPg.AuxiliaryData.GUIVersionList(database: database),

                        // t_estimation_cell_hierarchy
                        new ZaJi.NfiEstaPg.AuxiliaryData.EstimationCellHierarchyList(database: database),

                        // cm_estimation_cell
                        new ZaJi.NfiEstaPg.AuxiliaryData.EstimationCellMappingList(database: database),

                        // cm_estimation_cell_collection
                        new ZaJi.NfiEstaPg.AuxiliaryData.EstimationCellCollectionMappingList(database: database),

                            // cm_ext_config_function (statická data - vytvořená při instalaci extenze)
                            // new ZaJi.NfiEstaPg.AuxiliaryData.ExtensionToConfigFunctionList(database: database),

                            // cm_ext_gui_version (statická data - vytvořená při instalaci extenze)
                            // new ZaJi.NfiEstaPg.AuxiliaryData.ExtensionToGUIVersionList(database: database),

                        // f_a_cell
                        new ZaJi.NfiEstaPg.AuxiliaryData.CellList(database: database),

                        // f_p_plot
                        new ZaJi.NfiEstaPg.AuxiliaryData.PlotList(database: database),

                        // t_aux_total
                        new ZaJi.NfiEstaPg.AuxiliaryData.AuxTotalList(database: database),

                        // t_auxiliary_data
                        new ZaJi.NfiEstaPg.AuxiliaryData.AuxDataList(database: database),

                        // t_config
                        new ZaJi.NfiEstaPg.AuxiliaryData.ConfigList(database: database),

                        // t_config_collection
                        new ZaJi.NfiEstaPg.AuxiliaryData.ConfigCollectionList(database: database)
                    ];

                    tableNames ??= databaseTables.Select(a => a.TableName).ToList<string>();

                    foreach (ADatabaseTable databaseTable in databaseTables)
                    {
                        if (tableNames.Contains(item: databaseTable.TableName))
                        {
                            if (databaseTable is ALookupTable lookupTable)
                            {
                                lookupTable.LoadIdentifiers();

                                ColumnMetadata pkey =
                                    lookupTable.Columns.Values
                                    .Where(a => a.PKey)
                                    .FirstOrDefault<ColumnMetadata>();

                                if (lookupTable.Identifiers.Count != 0)
                                {
                                    int pageNumber = 1;
                                    while (pageSize * (pageNumber - 1) < lookupTable.Identifiers.Count)
                                    {
                                        string ids = lookupTable.Identifiers
                                            .Skip(pageSize * (pageNumber - 1))
                                            .Take(pageSize)
                                            .Select(a => a.ToString())
                                            .Aggregate((a, b) => $"{a},{b}");

                                        lookupTable.ReLoad(
                                            condition: $"{pkey.DbName} IN ({ids})",
                                            extended: false);

                                        lookupTable.ExportData(
                                            fileFormat: fileFormat,
                                            folder: folder,
                                            page: pageNumber.ToString());

                                        pageNumber++;
                                    }
                                }
                            }

                            else if (databaseTable is AMappingTable mappingTable)
                            {
                                mappingTable.LoadIdentifiers();

                                ColumnMetadata pkey =
                                    mappingTable.Columns.Values
                                    .Where(a => a.PKey)
                                    .FirstOrDefault<ColumnMetadata>();

                                if (mappingTable.Identifiers.Count != 0)
                                {
                                    int pageNumber = 1;
                                    while (pageSize * (pageNumber - 1) < mappingTable.Identifiers.Count)
                                    {
                                        string ids = mappingTable.Identifiers
                                            .Skip(pageSize * (pageNumber - 1))
                                            .Take(pageSize)
                                            .Select(a => a.ToString())
                                            .Aggregate((a, b) => $"{a},{b}");

                                        mappingTable.ReLoad(condition: $"{pkey.DbName} IN ({ids})");

                                        mappingTable.ExportData(
                                            fileFormat: fileFormat,
                                            folder: folder,
                                            page: pageNumber.ToString());

                                        pageNumber++;
                                    }
                                }
                            }

                            else if (databaseTable is ASpatialTable spatialTable)
                            {
                                spatialTable.LoadIdentifiers();

                                ColumnMetadata pkey =
                                    spatialTable.Columns.Values
                                    .Where(a => a.PKey)
                                    .FirstOrDefault<ColumnMetadata>();

                                if (spatialTable.Identifiers.Count != 0)
                                {
                                    int pageNumber = 1;
                                    while (pageSize * (pageNumber - 1) < spatialTable.Identifiers.Count)
                                    {
                                        string ids = spatialTable.Identifiers
                                            .Skip(pageSize * (pageNumber - 1))
                                            .Take(pageSize)
                                            .Select(a => a.ToString())
                                            .Aggregate((a, b) => $"{a},{b}");

                                        spatialTable.ReLoad(
                                            condition: $"{pkey.DbName} IN ({ids})",
                                            withGeom: true);

                                        spatialTable.ExportData(
                                            fileFormat: fileFormat,
                                            folder: folder,
                                            page: pageNumber.ToString());

                                        if (pageNumber > 2)
                                        {
                                            break;
                                        }

                                        pageNumber++;
                                    }
                                }
                            }

                            else if (databaseTable is ADataTable dataTable)
                            {
                                dataTable.LoadIdentifiers();

                                ColumnMetadata pkey =
                                    dataTable.Columns.Values
                                    .Where(a => a.PKey)
                                    .FirstOrDefault<ColumnMetadata>();

                                if (dataTable.Identifiers.Count != 0)
                                {
                                    int pageNumber = 1;
                                    while (pageSize * (pageNumber - 1) < dataTable.Identifiers.Count)
                                    {
                                        string ids = dataTable.Identifiers
                                            .Skip(pageSize * (pageNumber - 1))
                                            .Take(pageSize)
                                            .Select(a => a.ToString())
                                            .Aggregate((a, b) => $"{a},{b}");

                                        dataTable.ReLoad(condition: $"{pkey.DbName} IN ({ids})");

                                        dataTable.ExportData(
                                            fileFormat: fileFormat,
                                            folder: folder,
                                            page: pageNumber.ToString());

                                        pageNumber++;
                                    }
                                }
                            }

                            else
                            {
                                throw new Exception(message: $"Argument {nameof(databaseTable)} unknown type.");
                            }
                        }
                    }
                }

                #endregion Static Methods

            }

        }
    }
}