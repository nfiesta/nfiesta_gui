﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace AuxiliaryData
        {
            // t_auxiliary_data

            /// <summary>
            /// Auxiliary plot data
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class AuxData(
                AuxDataList composite = null,
                DataRow data = null)
                    : ADataTableEntry<AuxDataList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Auxiliary plot data identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: AuxDataList.ColId.Name,
                            defaultValue: Int32.Parse(s: AuxDataList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: AuxDataList.ColId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Configuration collection identifier
                /// </summary>
                public int ConfigCollectionId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: AuxDataList.ColConfigCollectionId.Name,
                            defaultValue: Int32.Parse(s: AuxDataList.ColConfigCollectionId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: AuxDataList.ColConfigCollectionId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Configuration collection object (read-only)
                /// </summary>
                public ConfigCollection ConfigCollection
                {
                    get
                    {
                        return
                            ((NfiEstaDB)Composite.Database)
                            .SAuxiliaryData.TConfigCollection[ConfigCollectionId];
                    }
                }


                /// <summary>
                /// Configuration for the auxiliary variable totals calculation identifier
                /// </summary>
                public int ConfigId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: AuxDataList.ColConfigId.Name,
                            defaultValue: Int32.Parse(s: AuxDataList.ColConfigId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: AuxDataList.ColConfigId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Configuration for the auxiliary variable totals calculation object (read-only)
                /// </summary>
                public Config Config
                {
                    get
                    {
                        return
                            ((NfiEstaDB)Composite.Database)
                            .SAuxiliaryData.TConfig[ConfigId];
                    }
                }


                /// <summary>
                /// Point identification within the configuration collection and configuration
                /// </summary>
                public Nullable<int> IdentPoint
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: AuxDataList.ColIdentPoint.Name,
                            defaultValue: String.IsNullOrEmpty(value: AuxDataList.ColIdentPoint.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: AuxDataList.ColIdentPoint.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: AuxDataList.ColIdentPoint.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Auxiliary variable value
                /// </summary>
                public double AuxValue
                {
                    get
                    {
                        return Functions.GetDoubleArg(
                            row: Data,
                            name: AuxDataList.ColAuxValue.Name,
                            defaultValue: Double.Parse(s: AuxDataList.ColAuxValue.DefaultValue));
                    }
                    set
                    {
                        Functions.SetDoubleArg(
                            row: Data,
                            name: AuxDataList.ColAuxValue.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Database extension version identifier
                /// </summary>
                public int ExtensionVersionId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: AuxDataList.ColExtensionVersionId.Name,
                            defaultValue: Int32.Parse(s: AuxDataList.ColExtensionVersionId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: AuxDataList.ColExtensionVersionId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Database extension version object (read-only)
                /// </summary>
                public ExtensionVersion ExtensionVersion
                {
                    get
                    {
                        return
                            ((NfiEstaDB)Composite.Database)
                            .SAuxiliaryData.CExtensionVersion[ExtensionVersionId];
                    }
                }


                /// <summary>
                /// GUI version identifier
                /// </summary>
                public int GUIVersionId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: AuxDataList.ColGUIVersionId.Name,
                            defaultValue: Int32.Parse(s: AuxDataList.ColGUIVersionId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: AuxDataList.ColGUIVersionId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// GUI version object (read-only)
                /// </summary>
                public GUIVersion GUIVersion
                {
                    get
                    {
                        return
                            ((NfiEstaDB)Composite.Database)
                            .SAuxiliaryData.CGUIVersion[GUIVersionId];
                    }
                }


                /// <summary>
                /// Point identification
                /// </summary>
                public Nullable<int> Gid
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: AuxDataList.ColGid.Name,
                            defaultValue: String.IsNullOrEmpty(value: AuxDataList.ColGid.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: AuxDataList.ColGid.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: AuxDataList.ColGid.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Point object (read-only)
                /// </summary>
                public Plot Plot
                {
                    get
                    {
                        return
                            (Gid != null)
                                ? ((NfiEstaDB)Composite.Database)
                                    .SAuxiliaryData.FpPlot[(int)Gid]
                                : null;
                    }
                }


                /// <summary>
                /// Date and time of calculation of the auxiliary variable
                /// </summary>
                public DateTime EstDate
                {
                    get
                    {
                        return Functions.GetDateTimeArg(
                            row: Data,
                            name: AuxDataList.ColEstDate.Name,
                            defaultValue: DateTime.Parse(s: AuxDataList.ColEstDate.DefaultValue));
                    }
                    set
                    {
                        Functions.SetDateTimeArg(
                            row: Data,
                            name: AuxDataList.ColEstDate.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Date and time of calculation of the auxiliary variable as text
                /// </summary>
                public string EstDateText
                {
                    get
                    {
                        return
                            EstDate.ToString(
                                format: AuxDataList.ColEstDate.NumericFormat);
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true|false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not AuxData Type, return False
                    if (obj is not AuxData)
                    {
                        return false;
                    }

                    return ((AuxData)obj).Id == Id;
                }

                /// <summary>
                /// Returns a hash code for the current object
                /// </summary>
                /// <returns>Hash code for the current object</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                /// <summary>
                /// Returns a string that represents the current object
                /// </summary>
                /// <returns>String that represents the current object</returns>
                public override string ToString()
                {
                    return String.Concat(
                        $"{nameof(AuxData)}: {{",
                        $"{nameof(Id)}: {Functions.PrepIntArg(arg: Id)}; ",
                        $"{nameof(ConfigCollectionId)}: {Functions.PrepIntArg(arg: ConfigCollectionId)}; ",
                        $"{nameof(ConfigId)}: {Functions.PrepIntArg(arg: ConfigId)}; ",
                        $"{nameof(IdentPoint)}: {Functions.PrepNIntArg(arg: IdentPoint)}; ",
                        $"{nameof(AuxValue)}: {Functions.PrepDoubleArg(arg: AuxValue)}; ",
                        $"{nameof(ExtensionVersionId)}: {Functions.PrepIntArg(arg: ExtensionVersionId)}; ",
                        $"{nameof(GUIVersionId)}: {Functions.PrepIntArg(arg: GUIVersionId)}; ",
                        $"{nameof(Gid)}: {Functions.PrepNIntArg(arg: Gid)}; ",
                        $"{nameof(EstDate)}: {Functions.PrepStringArg(arg: EstDateText)}}}");
                }

                #endregion Methods

            }


            /// <summary>
            /// List of auxiliary inventory plot data
            /// </summary>
            public class AuxDataList
                : ADataTable
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema = ADSchema.Name;

                /// <summary>
                /// Table name
                /// </summary>
                public const string Name = "t_auxiliary_data";

                /// <summary>
                /// Table alias
                /// </summary>
                public const string Alias = "t_auxiliary_data";

                /// <summary>
                /// Description in national language
                /// </summary>
                public const string CaptionCs = "Pomocná data na inventarizačních plochách";

                /// <summary>
                /// Description in English
                /// </summary>
                public const string CaptionEn = "List of auxiliary inventory plot data";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
            {
                { "id", new ColumnMetadata()
                {
                    Name = "id",
                    DbName = "id",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = true,
                    NotNull = true,
                    DefaultValue = "0",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "ID",
                    HeaderTextEn = "ID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 0
                }
                },
                { "config_collection_id", new ColumnMetadata()
                {
                    Name = "config_collection_id",
                    DbName = "config_collection",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = "0",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "CONFIG_COLLECTION_ID",
                    HeaderTextEn = "CONFIG_COLLECTION_ID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 1
                }
                },
                { "config_id", new ColumnMetadata()
                {
                    Name = "config_id",
                    DbName = "config",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = "0",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "CONFIG_ID",
                    HeaderTextEn = "CONFIG_ID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 2
                }
                },
                { "ident_point", new ColumnMetadata()
                {
                    Name = "ident_point",
                    DbName = "ident_point",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = null,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "IDENT_POINT",
                    HeaderTextEn = "IDENT_POINT",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 3
                }
                },
                { "aux_value", new ColumnMetadata()
                {
                    Name = "aux_value",
                    DbName = "value",
                    DataType = "System.Double",
                    DbDataType = "float8",
                    NewDataType = "float8",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = "0.0",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "N3",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "VALUE",
                    HeaderTextEn = "VALUE",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 4
                }
                },
                { "extension_version_id", new ColumnMetadata()
                {
                    Name = "extension_version_id",
                    DbName = "ext_version",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = "0",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "EXTENSION_VERSION_ID",
                    HeaderTextEn = "EXTENSION_VERSION_ID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 5
                }
                },
                { "gui_version_id", new ColumnMetadata()
                {
                    Name = "gui_version_id",
                    DbName = "gui_version",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = "0",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "GUI_VERSION_ID",
                    HeaderTextEn = "GUI_VERSION_ID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 6
                }
                },
                { "gid", new ColumnMetadata()
                {
                    Name = "gid",
                    DbName = "gid",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = null,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "GID",
                    HeaderTextEn = "GID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 7
                }
                },
                { "est_date", new ColumnMetadata()
                {
                    Name = "est_date",
                    DbName = "est_date",
                    DataType = "System.DateTime",
                    DbDataType = "timestamp",
                    NewDataType = "timestamp",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = new DateTime().ToString(),
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "yyyy-MM-dd HH:mm:ss",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "EST_DATE",
                    HeaderTextEn = "EST_DATE",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 8
                }
                }
            };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column config_collection_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColConfigCollectionId = Cols["config_collection_id"];

                /// <summary>
                /// Column config_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColConfigId = Cols["config_id"];

                /// <summary>
                /// Column ident_point metadata
                /// </summary>
                public static readonly ColumnMetadata ColIdentPoint = Cols["ident_point"];

                /// <summary>
                /// Column aux_value metadata
                /// </summary>
                public static readonly ColumnMetadata ColAuxValue = Cols["aux_value"];

                /// <summary>
                /// Column extension_version_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColExtensionVersionId = Cols["extension_version_id"];

                /// <summary>
                /// Column gui_version_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColGUIVersionId = Cols["gui_version_id"];

                /// <summary>
                /// Column gid metadata
                /// </summary>
                public static readonly ColumnMetadata ColGid = Cols["gid"];

                /// <summary>
                /// Column est_date metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstDate = Cols["est_date"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public AuxDataList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public AuxDataList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// List of auxiliary plot data (read-only)
                /// </summary>
                public List<AuxData> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new AuxData(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// Auxiliary plot data from list by identifier (read-only)
                /// </summary>
                /// <param name="id">Auxiliary plot data identifier</param>
                /// <returns>Auxiliary plot data from list by identifier (null if not found)</returns>
                public AuxData this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new AuxData(composite: this, data: a))
                                .FirstOrDefault<AuxData>();
                    }
                }

                #endregion Indexer


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public AuxDataList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new AuxDataList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new AuxDataList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                #endregion Methods

            }

        }
    }
}