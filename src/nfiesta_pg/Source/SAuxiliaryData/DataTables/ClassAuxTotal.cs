﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace AuxiliaryData
        {
            // t_aux_total

            /// <summary>
            /// Total of auxiliary variable within estimation cell
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class AuxTotal(
                AuxTotalList composite = null,
                DataRow data = null)
                    : ADataTableEntry<AuxTotalList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Total of auxiliary variable within estimation cell identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: AuxTotalList.ColId.Name,
                            defaultValue: Int32.Parse(s: AuxTotalList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: AuxTotalList.ColId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Configuration for the auxiliary variable totals calculation identifier
                /// </summary>
                public int ConfigId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: AuxTotalList.ColConfigId.Name,
                            defaultValue: Int32.Parse(s: AuxTotalList.ColConfigId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: AuxTotalList.ColConfigId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Configuration for the auxiliary variable totals calculation object (read-only)
                /// </summary>
                public Config Config
                {
                    get
                    {
                        return
                             ((NfiEstaDB)Composite.Database)
                             .SAuxiliaryData.TConfig[ConfigId];
                    }
                }


                /// <summary>
                /// Estimation cell identifier
                /// </summary>
                public Nullable<int> EstimationCellId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: AuxTotalList.ColEstimationCellId.Name,
                            defaultValue: String.IsNullOrEmpty(value: AuxTotalList.ColEstimationCellId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: AuxTotalList.ColEstimationCellId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: AuxTotalList.ColEstimationCellId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Estimation cell object (read-only)
                /// </summary>
                public EstimationCell EstimationCell
                {
                    get
                    {
                        return
                            (EstimationCellId != null) ?
                             ((NfiEstaDB)Composite.Database)
                             .SAuxiliaryData.CEstimationCell[(int)EstimationCellId] : null;
                    }
                }


                /// <summary>
                /// Estimation cell segment identifier
                /// </summary>
                public Nullable<int> CellId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: AuxTotalList.ColCellId.Name,
                            defaultValue: String.IsNullOrEmpty(value: AuxTotalList.ColCellId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: AuxTotalList.ColCellId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: AuxTotalList.ColCellId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Estimation cell segment object (read-only)
                /// </summary>
                public Cell Cell
                {
                    get
                    {
                        return
                            (CellId != null) ?
                             ((NfiEstaDB)Composite.Database)
                             .SAuxiliaryData.FaCell[(int)CellId] : null;
                    }
                }


                /// <summary>
                /// Auxiliary variable total
                /// </summary>
                public double AuxTotalValue
                {
                    get
                    {
                        return Functions.GetDoubleArg(
                            row: Data,
                            name: AuxTotalList.ColAuxTotal.Name,
                            defaultValue: Double.Parse(s: AuxTotalList.ColAuxTotal.DefaultValue));
                    }
                    set
                    {
                        Functions.SetDoubleArg(
                            row: Data,
                            name: AuxTotalList.ColAuxTotal.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Date and time of calculation of the auxiliary variable total
                /// </summary>
                public DateTime EstDate
                {
                    get
                    {
                        return Functions.GetDateTimeArg(
                            row: Data,
                            name: AuxTotalList.ColEstDate.Name,
                            defaultValue: DateTime.Parse(s: AuxTotalList.ColEstDate.DefaultValue));
                    }
                    set
                    {
                        Functions.SetDateTimeArg(
                            row: Data,
                            name: AuxTotalList.ColEstDate.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Date and time of calculation of the auxiliary variable total as text
                /// </summary>
                public string EstDateText
                {
                    get
                    {
                        return
                            EstDate.ToString(
                                format: AuxTotalList.ColEstDate.NumericFormat);
                    }
                }


                /// <summary>
                /// Database extension version identifier
                /// </summary>
                public int ExtensionVersionId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: AuxTotalList.ColExtensionVersionId.Name,
                            defaultValue: Int32.Parse(s: AuxTotalList.ColExtensionVersionId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: AuxTotalList.ColExtensionVersionId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Database extension version object (read-only)
                /// </summary>
                public ExtensionVersion ExtensionVersion
                {
                    get
                    {
                        return
                             ((NfiEstaDB)Composite.Database).SAuxiliaryData.CExtensionVersion[ExtensionVersionId];
                    }
                }


                /// <summary>
                /// GUI version identifier
                /// </summary>
                public int GUIVersionId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: AuxTotalList.ColGUIVersionId.Name,
                            defaultValue: Int32.Parse(s: AuxTotalList.ColGUIVersionId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: AuxTotalList.ColGUIVersionId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// GUI version object (read-only)
                /// </summary>
                public GUIVersion GUIVersion
                {
                    get
                    {
                        return
                             ((NfiEstaDB)Composite.Database).SAuxiliaryData.CGUIVersion[GUIVersionId];
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true|false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not AuxTotal Type, return False
                    if (obj is not AuxTotal)
                    {
                        return false;
                    }

                    return ((AuxTotal)obj).Id == Id;
                }

                /// <summary>
                /// Returns a hash code for the current object
                /// </summary>
                /// <returns>Hash code for the current object</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                #endregion Methods

            }


            /// <summary>
            /// List of totals of auxiliary variable within estimation cell
            /// </summary>
            public class AuxTotalList
                : ADataTable
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema = ADSchema.Name;

                /// <summary>
                /// Table name
                /// </summary>
                public const string Name = "t_aux_total";

                /// <summary>
                /// Table alias
                /// </summary>
                public const string Alias = "t_aux_total";

                /// <summary>
                /// Description in national language
                /// </summary>
                public const string CaptionCs = "Seznam úhrnů pomocných proměnných uvnitř výpočetních buněk";

                /// <summary>
                /// Description in English
                /// </summary>
                public const string CaptionEn = "List of totals of auxiliary variable within estimation cell";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
                {
                    { "id", new ColumnMetadata()
                    {
                        Name = "id",
                        DbName = "id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = true,
                        NotNull = true,
                        DefaultValue = "0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ID",
                        HeaderTextEn = "ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 0
                    }
                    },
                    { "config_id", new ColumnMetadata()
                    {
                        Name = "config_id",
                        DbName = "config",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = "0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "CONFIG_ID",
                        HeaderTextEn = "CONFIG_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 1
                    }
                    },
                    { "estimation_cell_id", new ColumnMetadata()
                    {
                        Name = "estimation_cell_id",
                        DbName = "estimation_cell",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATION_CELL_ID",
                        HeaderTextEn = "ESTIMATION_CELL_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 2
                    }
                    },
                    { "cell_id", new ColumnMetadata()
                    {
                        Name = "cell_id",
                        DbName = "cell",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "CELL_ID",
                        HeaderTextEn = "CELL_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 3
                    }
                    },
                    { "aux_total", new ColumnMetadata()
                    {
                        Name = "aux_total",
                        DbName = "aux_total",
                        DataType = "System.Double",
                        DbDataType = "float8",
                        NewDataType = "float8",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = "0.0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "N3",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "AUX_TOTAL",
                        HeaderTextEn = "AUX_TOTAL",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 4
                    }
                    },
                    { "est_date", new ColumnMetadata()
                    {
                        Name = "est_date",
                        DbName = "est_date",
                        DataType = "System.DateTime",
                        DbDataType = "timestamp",
                        NewDataType = "timestamp",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = new DateTime().ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "yyyy-MM-dd HH:mm:ss",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "EST_DATE",
                        HeaderTextEn = "EST_DATE",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 5
                    }
                    },
                    { "extension_version_id", new ColumnMetadata()
                    {
                        Name = "extension_version_id",
                        DbName = "ext_version",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = "0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "EXTENSION_VERSION_ID",
                        HeaderTextEn = "EXTENSION_VERSION_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 6
                    }
                    },
                    { "gui_version_id", new ColumnMetadata()
                    {
                        Name = "gui_version_id",
                        DbName = "gui_version",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = "0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "GUI_VERSION_ID",
                        HeaderTextEn = "GUI_VERSION_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 7
                    }
                    }
                };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column config_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColConfigId = Cols["config_id"];

                /// <summary>
                /// Column estimation_cell_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationCellId = Cols["estimation_cell_id"];

                /// <summary>
                /// Column cell_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColCellId = Cols["cell_id"];

                /// <summary>
                /// Column aux_total metadata
                /// </summary>
                public static readonly ColumnMetadata ColAuxTotal = Cols["aux_total"];

                /// <summary>
                /// Column est_date metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstDate = Cols["est_date"];

                /// <summary>
                /// Column extension_version_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColExtensionVersionId = Cols["extension_version_id"];

                /// <summary>
                /// Column gui_version_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColGUIVersionId = Cols["gui_version_id"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                /// <summary>
                /// <para lang="cs">
                /// SQL příkaz pro ověření existence vypočteného úhrnu pomocné proměnné v tabulce t_aux_total
                /// pro segment výpočetní buňky, konfiguraci a verzi extenze
                /// </para>
                /// <para lang="en">
                /// SQL statement to verify the existence of the calculated total of the auxiliary variable in the t_aux_total table
                /// for estimation cell segment, configuration and database extension version
                /// </para>
                /// </summary>
                /// <param name="gidForAuxTotal">
                /// <para lang="cs">Segment výpočetní buňky</para>
                /// <para lang="en">Estimation cell segment</para>
                /// </param>
                /// <returns>
                /// <para lang="cs">
                /// SQL příkaz pro ověření existence vypočteného úhrnu pomocné proměnné v tabulce t_aux_total
                /// pro segment výpočetní buňky, konfiguraci a verzi extenze
                /// </para>
                /// <para lang="en">
                /// SQL statement to verify the existence of the calculated total of the auxiliary variable in the t_aux_total table
                /// for estimation cell segment, configuration and database extension version
                /// </para>
                /// </returns>
                public static string SQLTestExists(TFnGetGidsForAuxTotalApp gidForAuxTotal)
                {
                    return
                        (gidForAuxTotal.CellId == null)
                        ? String.Concat(
                            $"SELECT EXISTS({Environment.NewLine}",
                            $"    SELECT {AuxTotalList.Alias}.{AuxTotalList.ColId.DbName}{Environment.NewLine}",
                            $"    FROM {AuxTotalList.Schema}.{AuxTotalList.Name} AS {AuxTotalList.Alias}{Environment.NewLine}",
                            $"    WHERE{Environment.NewLine}",
                            $"        ({AuxTotalList.Alias}.{AuxTotalList.ColConfigId.DbName} = {Functions.PrepNIntArg(arg: gidForAuxTotal.ConfigId)}) AND {Environment.NewLine}",
                            $"        ({AuxTotalList.Alias}.{AuxTotalList.ColEstimationCellId.DbName} = {Functions.PrepNIntArg(arg: gidForAuxTotal.EstimationCellId)}) AND {Environment.NewLine}",
                            $"        ({AuxTotalList.Alias}.{AuxTotalList.ColCellId.DbName} IS NULL) AND {Environment.NewLine}",
                            $"        ({AuxTotalList.Alias}.{AuxTotalList.ColExtensionVersionId.DbName} = {Functions.PrepNIntArg(arg: gidForAuxTotal.ExtensionVersionId)}) AND {Environment.NewLine}",
                            $"        ({AuxTotalList.Alias}.{AuxTotalList.ColGUIVersionId.DbName} = {Functions.PrepNIntArg(arg: gidForAuxTotal.GuiVersionId)}){Environment.NewLine}",
                            $");")
                        : String.Concat(
                            $"SELECT EXISTS({Environment.NewLine}",
                            $"    SELECT {AuxTotalList.Alias}.{AuxTotalList.ColId.DbName}{Environment.NewLine}",
                            $"    FROM {AuxTotalList.Schema} . {AuxTotalList.Name} AS {AuxTotalList.Alias}{Environment.NewLine}",
                            $"    WHERE{Environment.NewLine}",
                            $"        ({AuxTotalList.Alias}.{AuxTotalList.ColConfigId.DbName} = {Functions.PrepNIntArg(arg: gidForAuxTotal.ConfigId)}) AND {Environment.NewLine}",
                            $"        ({AuxTotalList.Alias}.{AuxTotalList.ColEstimationCellId.DbName} = {Functions.PrepNIntArg(arg: gidForAuxTotal.EstimationCellId)}) AND {Environment.NewLine}",
                            $"        ({AuxTotalList.Alias}.{AuxTotalList.ColCellId.DbName} = {Functions.PrepNIntArg(gidForAuxTotal.CellId)}) AND{Environment.NewLine}",
                            $"        ({AuxTotalList.Alias}.{AuxTotalList.ColExtensionVersionId.DbName} = {Functions.PrepNIntArg(arg: gidForAuxTotal.ExtensionVersionId)}) AND {Environment.NewLine}",
                            $"        ({AuxTotalList.Alias}.{AuxTotalList.ColGUIVersionId.DbName} = {Functions.PrepNIntArg(arg: gidForAuxTotal.GuiVersionId)}){Environment.NewLine}",
                            $");");
                }

                #endregion Static Methods


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public AuxTotalList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public AuxTotalList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// List of totals of auxiliary variable within estimation cell (read-only)
                /// </summary>
                public List<AuxTotal> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new AuxTotal(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// Total of auxiliary variable within estimation cell from list by identifier (read-only)
                /// </summary>
                /// <param name="id">Total of auxiliary variable within estimation cell identifier</param>
                /// <returns>Total of auxiliary variable within estimation cell from list by identifier (null if not found)</returns>
                public AuxTotal this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new AuxTotal(composite: this, data: a))
                                .FirstOrDefault<AuxTotal>();
                    }
                }

                #endregion Indexer


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public AuxTotalList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new AuxTotalList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new AuxTotalList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                #endregion Methods

            }

        }
    }
}