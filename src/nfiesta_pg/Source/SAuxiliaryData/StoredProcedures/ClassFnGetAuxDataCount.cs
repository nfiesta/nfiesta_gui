﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using System;
using System.Data;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace AuxiliaryData
        {

            /// <summary>
            /// Stored procedures of schema gisdata
            /// </summary>
            public static partial class ADFunctions
            {
                // *** fn_get_aux_data_count

                #region FnGetAuxDataCount

                /// <summary>
                /// Wrapper for stored procedure fn_get_aux_data_count.
                /// Stored procedure that returns counts of auxiliary variable values for
                /// point layer, configuration, extension version and gui version.
                /// </summary>
                public static class FnGetAuxDataCount
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName = ADSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name = "fn_get_aux_data_count";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature = null;

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    ?.Replace("$SchemaName", schemaName)
                                    ?.Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                String.Concat(
                                    $"SELECT{Environment.NewLine}",
                                    $"    {TFnGetAuxDataCountList.ColId.FuncCall}           AS {TFnGetAuxDataCountList.ColId.Name},{Environment.NewLine}",
                                    $"    {AuxDataList.ColConfigCollectionId.DbName}        AS {TFnGetAuxDataCountList.ColConfigCollectionId.Name},{Environment.NewLine}",
                                    $"    {AuxDataList.ColConfigId.DbName}                  AS {TFnGetAuxDataCountList.ColConfigId.Name},{Environment.NewLine}",
                                    $"    {AuxDataList.ColExtensionVersionId.DbName}        AS {TFnGetAuxDataCountList.ColExtVersionId.Name},{Environment.NewLine}",
                                    $"    {AuxDataList.ColGUIVersionId.DbName}              AS {TFnGetAuxDataCountList.ColGuiVersionId.Name},{Environment.NewLine}",
                                    $"    {TFnGetAuxDataCountList.ColAuxDataCount.FuncCall} AS {TFnGetAuxDataCountList.ColAuxDataCount.Name}{Environment.NewLine}",
                                    $"FROM{Environment.NewLine}",
                                    $"    {AuxDataList.Schema}.{AuxDataList.Name}{Environment.NewLine}",
                                    $"GROUP BY{Environment.NewLine}",
                                    $"    {AuxDataList.ColConfigCollectionId.DbName},{Environment.NewLine}",
                                    $"    {AuxDataList.ColConfigId.DbName},{Environment.NewLine}",
                                    $"    {AuxDataList.ColExtensionVersionId.DbName},{Environment.NewLine}",
                                    $"    {AuxDataList.ColGUIVersionId.DbName}{Environment.NewLine}",
                                    $"ORDER BY{Environment.NewLine}",
                                    $"    {AuxDataList.ColConfigCollectionId.DbName},{Environment.NewLine}",
                                    $"    {AuxDataList.ColConfigId.DbName},{Environment.NewLine}",
                                    $"    {AuxDataList.ColExtensionVersionId.DbName},{Environment.NewLine}",
                                    $"    {AuxDataList.ColGUIVersionId.DbName};{Environment.NewLine}");
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <returns>Command text</returns>
                    public static string GetCommandText()
                    {
                        return SQL;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_get_aux_data_count
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table with counts of auxiliary variable values for point layer, configuration, extension version and gui version</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText();

                        return database.Postgres.ExecuteQuery(
                            sqlCommand: SQL,
                            transaction: transaction);
                    }

                    /// <summary>
                    /// Execute stored procedure fn_get_aux_data_count
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>List with counts of auxiliary variable values for point layer, configuration, extension version and gui version</returns>
                    public static TFnGetAuxDataCountList Execute(
                        NfiEstaDB database,
                        NpgsqlTransaction transaction = null)
                    {
                        System.Diagnostics.Stopwatch stopWatch = new();
                        stopWatch.Start();

                        DataTable data = ExecuteQuery(
                            database: database,
                            transaction: transaction);
                        data.TableName = Name;

                        stopWatch.Stop();

                        return new TFnGetAuxDataCountList(
                            database: database,
                            data: data,
                            loadingTime: 0.001 * stopWatch.ElapsedMilliseconds,
                            storedProcedure: CommandText);
                    }

                    #endregion Methods

                }

                #endregion FnGetAuxDataCount

            }

        }
    }
}