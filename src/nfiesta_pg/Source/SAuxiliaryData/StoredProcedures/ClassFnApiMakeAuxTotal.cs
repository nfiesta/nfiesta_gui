﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace AuxiliaryData
        {

            /// <summary>
            /// Stored procedures of schema gisdata
            /// </summary>
            public static partial class ADFunctions
            {
                // fn_api_make_aux_total

                #region FnApiMakeAuxTotal

                /// <summary>
                /// Wrapper for stored procedure fn_api_make_aux_total.
                /// The function on a base of input arguments calls either a function
                /// fn_get_aux_total_app or function fn_get_aux_total4estimation_cell_app.
                /// </summary>
                public static class FnApiMakeAuxTotal
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName = ADSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name = "fn_api_make_aux_total";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_config_id integer, ",
                            $"_estimation_cell integer, ",
                            $"_gid integer, ",
                            $"_gui_version integer, ",
                            $"_recount boolean DEFAULT false; ",
                            $"returns: void");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    ?.Replace("$SchemaName", schemaName)
                                    ?.Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    {SchemaName}.{Name}",
                            $"(@configurationId, @estimationCellId, @cellId, @guiVersionId, @recalc) AS {Name};{Environment.NewLine}");
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="configurationId">Configuration identifier</param>
                    /// <param name="estimationCellId">Estimation cell identifier</param>
                    /// <param name="cellId">Estimation cell segment identifier</param>
                    /// <param name="guiVersionId">Module identifier</param>
                    /// <param name="recalc">Recalculate</param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        Nullable<int> configurationId,
                        Nullable<int> estimationCellId,
                        Nullable<int> cellId,
                        Nullable<int> guiVersionId,
                        Nullable<bool> recalc)
                    {
                        string result = SQL;

                        result = result.Replace(
                             oldValue: "@configurationId",
                             newValue: Functions.PrepNIntArg(arg: configurationId));

                        result = result.Replace(
                             oldValue: "@estimationCellId",
                             newValue: Functions.PrepNIntArg(arg: estimationCellId));

                        result = result.Replace(
                             oldValue: "@cellId",
                             newValue: Functions.PrepNIntArg(arg: cellId));

                        result = result.Replace(
                            oldValue: "@guiVersionId",
                            newValue: Functions.PrepNIntArg(arg: guiVersionId));

                        result = result.Replace(
                            oldValue: "@recalc",
                            newValue: Functions.PrepNBoolArg(arg: recalc));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_api_make_aux_total
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="configurationId">Configuration identifier</param>
                    /// <param name="estimationCellId">Estimation cell identifier</param>
                    /// <param name="cellId">Estimation cell segment identifier</param>
                    /// <param name="guiVersionId">Module identifier</param>
                    /// <param name="recalc">Recalculate</param>
                    /// <param name="transaction">Transaction</param>
                    public static void Execute(
                        NfiEstaDB database,
                        Nullable<int> configurationId,
                        Nullable<int> estimationCellId,
                        Nullable<int> cellId,
                        Nullable<int> guiVersionId,
                        Nullable<bool> recalc,
                        NpgsqlTransaction transaction = null)
                    {
                        ExecuteQuery(
                            database: database,
                            configurationId: configurationId,
                            estimationCellId: estimationCellId,
                            cellId: cellId,
                            guiVersionId: guiVersionId,
                            recalc: recalc,
                            transaction: transaction);
                    }

                    /// <summary>
                    /// Execute stored procedure fn_api_make_aux_total
                    /// </summary>
                    /// <param name="connection">Database connection</param>
                    /// <param name="configurationId">Configuration identifier</param>
                    /// <param name="estimationCellId">Estimation cell identifier</param>
                    /// <param name="cellId">Estimation cell segment identifier</param>
                    /// <param name="guiVersionId">Module identifier</param>
                    /// <param name="recalc">Recalculate</param>
                    /// <param name="transaction">Transaction</param>
                    public static void Execute(
                        ZaJi.PostgreSQL.PostgreSQLWrapper connection,
                        Nullable<int> configurationId,
                        Nullable<int> estimationCellId,
                        Nullable<int> cellId,
                        Nullable<int> guiVersionId,
                        Nullable<bool> recalc,
                        NpgsqlTransaction transaction = null)
                    {
                        ExecuteQuery(
                            connection: connection,
                            configurationId: configurationId,
                            estimationCellId: estimationCellId,
                            cellId: cellId,
                            guiVersionId: guiVersionId,
                            recalc: recalc,
                            transaction: transaction);
                    }

                    /// <summary>
                    /// Execute stored procedure fn_api_make_aux_total
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="configurationId">Configuration identifier</param>
                    /// <param name="estimationCellId">Estimation cell identifier</param>
                    /// <param name="cellId">Estimation cell segment identifier</param>
                    /// <param name="guiVersionId">Module identifier</param>
                    /// <param name="recalc">Recalculate</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Empty data table</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> configurationId,
                        Nullable<int> estimationCellId,
                        Nullable<int> cellId,
                        Nullable<int> guiVersionId,
                        Nullable<bool> recalc,
                        NpgsqlTransaction transaction = null)
                    {
                        return ExecuteQuery(
                            connection: database.Postgres,
                            configurationId: configurationId,
                            estimationCellId: estimationCellId,
                            cellId: cellId,
                            guiVersionId: guiVersionId,
                            recalc: recalc,
                            transaction: transaction);
                    }

                    /// <summary>
                    /// Execute stored procedure fn_api_make_aux_total
                    /// </summary>
                    /// <param name="connection">Database connection</param>
                    /// <param name="configurationId">Configuration identifier</param>
                    /// <param name="estimationCellId">Estimation cell identifier</param>
                    /// <param name="cellId">Estimation cell segment identifier</param>
                    /// <param name="guiVersionId">Module identifier</param>
                    /// <param name="recalc">Recalculate</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Empty data table</returns>
                    public static DataTable ExecuteQuery(
                        ZaJi.PostgreSQL.PostgreSQLWrapper connection,
                        Nullable<int> configurationId,
                        Nullable<int> estimationCellId,
                        Nullable<int> cellId,
                        Nullable<int> guiVersionId,
                        Nullable<bool> recalc,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText
                            (configurationId: configurationId,
                             estimationCellId: estimationCellId,
                             cellId: cellId,
                             guiVersionId: guiVersionId,
                             recalc: recalc);

                        connection.ExecuteNonQuery(
                            sqlCommand: GetCommandText
                                (configurationId: configurationId,
                                 estimationCellId: estimationCellId,
                                 cellId: cellId,
                                 guiVersionId: guiVersionId,
                                 recalc: recalc),
                            transaction: transaction);

                        return new DataTable();
                    }

                    #endregion Methods

                }

                #endregion FnApiMakeAuxTotal

            }

        }
    }
}