﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace AuxiliaryData
        {

            /// <summary>
            /// Stored procedures of schema gisdata
            /// </summary>
            public static partial class ADFunctions
            {
                // *** fn_get_aux_total

                #region FnGetAuxTotal

                /// <summary>
                /// Wrapper for stored procedure fn_get_aux_total.
                /// Stored procedure that returns a list of calculated auxiliary variable totals
                /// for one configuration collection
                /// </summary>
                public static class FnGetAuxTotal
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName = ADSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name = "fn_get_aux_total";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature = null;

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    ?.Replace("$SchemaName", schemaName)
                                    ?.Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            string cteConfig = String.Concat(
                                $"WITH cteConfig AS MATERIALIZED{Environment.NewLine}",
                                $"({Environment.NewLine}",
                                $"    SELECT{Environment.NewLine}",
                                $"        a.{ConfigList.ColConfigCollectionId.DbName}               AS {TFnGetAuxTotalList.ColConfigCollectionId.Name},{Environment.NewLine}",
                                $"        a.{ConfigList.ColId.DbName}                               AS {TFnGetAuxTotalList.ColConfigId.Name},{Environment.NewLine}",
                                $"        a.{ConfigList.ColLabelCs.DbName}                          AS {TFnGetAuxTotalList.ColConfigLabelCs.Name},{Environment.NewLine}",
                                $"        a.{ConfigList.ColLabelEn.DbName}                          AS {TFnGetAuxTotalList.ColConfigLabelEn.Name},{Environment.NewLine}",
                                $"        a.{ConfigList.ColDescriptionCs.DbName}                    AS {TFnGetAuxTotalList.ColConfigDescriptionCs.Name},{Environment.NewLine}",
                                $"        a.{ConfigList.ColDescriptionEn.DbName}                    AS {TFnGetAuxTotalList.ColConfigDescriptionEn.Name}{Environment.NewLine}",
                                $"    FROM{Environment.NewLine}",
                                $"        {ConfigList.Schema}.{ConfigList.Name}                     AS a{Environment.NewLine}",
                                $"    WHERE{Environment.NewLine}",
                                $"        (a.{ConfigList.ColId.DbName} = ANY(@configurationIds)){Environment.NewLine}",
                                $"){Environment.NewLine}");

                            string cteEstimationCell = String.Concat(
                                $",cteEstimationCell AS MATERIALIZED{Environment.NewLine}",
                                $"({Environment.NewLine}",
                                $"    SELECT{Environment.NewLine}",
                                $"        a.{EstimationCellList.ColId.DbName}                       AS {TFnGetAuxTotalList.ColEstimationCellId.Name},{Environment.NewLine}",
                                $"        a.{EstimationCellList.ColLabelCs.DbName}                  AS {TFnGetAuxTotalList.ColEstimationCellLabelCs.Name},{Environment.NewLine}",
                                $"        a.{EstimationCellList.ColLabelEn.DbName}                  AS {TFnGetAuxTotalList.ColEstimationCellLabelEn.Name},{Environment.NewLine}",
                                $"        a.{EstimationCellList.ColDescriptionCs.DbName}            AS {TFnGetAuxTotalList.ColEstimationCellDescriptionCs.Name},{Environment.NewLine}",
                                $"        a.{EstimationCellList.ColDescriptionEn.DbName}            AS {TFnGetAuxTotalList.ColEstimationCellDescriptionEn.Name}{Environment.NewLine}",
                                $"    FROM{Environment.NewLine}",
                                $"        {EstimationCellList.Schema}.{EstimationCellList.Name}     AS a{Environment.NewLine}",
                                $"    WHERE{Environment.NewLine}",
                                $"        (a.{EstimationCellList.ColId.DbName} = ANY(@estimationCellIds)){Environment.NewLine}",
                                $"){Environment.NewLine}");

                            string cteExtVersion = String.Concat(
                                $",cteExtVersion AS MATERIALIZED{Environment.NewLine}",
                                $"({Environment.NewLine}",
                                $"    SELECT{Environment.NewLine}",
                                $"        a.{ExtensionVersionList.ColId.DbName}                     AS {TFnGetAuxTotalList.ColExtVersionId.Name},{Environment.NewLine}",
                                $"        a.{ExtensionVersionList.ColLabelCs.DbName}                AS {TFnGetAuxTotalList.ColExtVersionLabelCs.Name},{Environment.NewLine}",
                                $"        a.{ExtensionVersionList.ColLabelEn.DbName}                AS {TFnGetAuxTotalList.ColExtVersionLabelEn.Name},{Environment.NewLine}",
                                $"        a.{ExtensionVersionList.ColDescriptionCs.DbName}          AS {TFnGetAuxTotalList.ColExtVersionDescriptionCs.Name},{Environment.NewLine}",
                                $"        a.{ExtensionVersionList.ColDescriptionEn.DbName}          AS {TFnGetAuxTotalList.ColExtVersionDescriptionEn.Name}{Environment.NewLine}",
                                $"    FROM{Environment.NewLine}",
                                $"        {ExtensionVersionList.Schema}.{ExtensionVersionList.Name} AS a{Environment.NewLine}",
                                $"    WHERE{Environment.NewLine}",
                                $"        (a.{ExtensionVersionList.ColId.DbName} = ANY(@extensionVersionIds)){Environment.NewLine}",
                                $"){Environment.NewLine}");

                            string cteGuiVersion = String.Concat(
                                $",cteGuiVersion AS MATERIALIZED{Environment.NewLine}",
                                $"({Environment.NewLine}",
                                $"    SELECT{Environment.NewLine}",
                                $"        a.{GUIVersionList.ColId.DbName}                     AS {TFnGetAuxTotalList.ColGuiVersionId.Name},{Environment.NewLine}",
                                $"        a.{GUIVersionList.ColLabelCs.DbName}                AS {TFnGetAuxTotalList.ColGuiVersionLabelCs.Name},{Environment.NewLine}",
                                $"        a.{GUIVersionList.ColLabelEn.DbName}                AS {TFnGetAuxTotalList.ColGuiVersionLabelEn.Name},{Environment.NewLine}",
                                $"        a.{GUIVersionList.ColDescriptionCs.DbName}          AS {TFnGetAuxTotalList.ColGuiVersionDescriptionCs.Name},{Environment.NewLine}",
                                $"        a.{GUIVersionList.ColDescriptionEn.DbName}          AS {TFnGetAuxTotalList.ColGuiVersionDescriptionEn.Name}{Environment.NewLine}",
                                $"    FROM{Environment.NewLine}",
                                $"        {GUIVersionList.Schema}.{GUIVersionList.Name} AS a{Environment.NewLine}",
                                $"    WHERE{Environment.NewLine}",
                                $"        (a.{GUIVersionList.ColId.DbName} = ANY(@guiVersionIds)){Environment.NewLine}",
                                $"){Environment.NewLine}");

                            string cteAuxTotal = String.Concat(
                                $",cteAuxTotal AS MATERIALIZED{Environment.NewLine}",
                                $"({Environment.NewLine}",
                                $"    SELECT",
                                $"        a.{AuxTotalList.ColId.DbName}                         AS id,{Environment.NewLine}",
                                $"        a.{AuxTotalList.ColConfigId.DbName}                   AS config_id,{Environment.NewLine}",
                                $"        a.{AuxTotalList.ColEstimationCellId.DbName}           AS estimation_cell_id,{Environment.NewLine}",
                                $"        a.{AuxTotalList.ColCellId.DbName}                     AS cell_id,{Environment.NewLine}",
                                $"        a.{AuxTotalList.ColAuxTotal.DbName}                   AS aux_total,{Environment.NewLine}",
                                $"        a.{AuxTotalList.ColEstDate.DbName}                    AS est_date,{Environment.NewLine}",
                                $"        a.{AuxTotalList.ColExtensionVersionId.DbName}         AS ext_version_id,{Environment.NewLine}",
                                $"        a.{AuxTotalList.ColGUIVersionId.DbName}               AS gui_version_id{Environment.NewLine}",
                                $"    FROM{Environment.NewLine}",
                                $"        {AuxTotalList.Schema}.{AuxTotalList.Name}             AS a{Environment.NewLine}",
                                $"    WHERE",
                                $"        (a.{AuxTotalList.ColConfigId.DbName} IN (SELECT {TFnGetAuxTotalList.ColConfigId.Name} FROM cteConfig)){Environment.NewLine}",
                                $"    AND{Environment.NewLine}",
                                $"        (a.{AuxTotalList.ColEstimationCellId.DbName} IN (SELECT {TFnGetAuxTotalList.ColEstimationCellId.Name} FROM cteEstimationCell)){Environment.NewLine}",
                                $"    AND{Environment.NewLine}",
                                $"        (a.{AuxTotalList.ColExtensionVersionId.DbName} IN (SELECT {TFnGetAuxTotalList.ColExtVersionId.Name} FROM cteExtVersion)){Environment.NewLine}",
                                $"    AND{Environment.NewLine}",
                                $"        (a.{AuxTotalList.ColGUIVersionId.DbName} IN (SELECT {TFnGetAuxTotalList.ColGuiVersionId.Name} FROM cteGuiVersion)){Environment.NewLine}",
                                $"){Environment.NewLine}");

                            string result = String.Concat(
                                cteConfig,
                                cteEstimationCell,
                                cteExtVersion,
                                cteGuiVersion,
                                cteAuxTotal,
                                $"SELECT{Environment.NewLine}",
                                $"    a.{TFnGetAuxTotalList.ColId.Name}                             AS {TFnGetAuxTotalList.ColId.Name},{Environment.NewLine}",
                                $"    b.{TFnGetAuxTotalList.ColConfigCollectionId.Name}             AS {TFnGetAuxTotalList.ColConfigCollectionId.Name},{Environment.NewLine}",
                                $"    b.{TFnGetAuxTotalList.ColConfigId.Name}                       AS {TFnGetAuxTotalList.ColConfigId.Name},{Environment.NewLine}",
                                $"    b.{TFnGetAuxTotalList.ColConfigLabelCs.Name}                  AS {TFnGetAuxTotalList.ColConfigLabelCs.Name},{Environment.NewLine}",
                                $"    b.{TFnGetAuxTotalList.ColConfigDescriptionCs.Name}            AS {TFnGetAuxTotalList.ColConfigDescriptionCs.Name},{Environment.NewLine}",
                                $"    b.{TFnGetAuxTotalList.ColConfigLabelEn.Name}                  AS {TFnGetAuxTotalList.ColConfigLabelEn.Name},{Environment.NewLine}",
                                $"    b.{TFnGetAuxTotalList.ColConfigDescriptionEn.Name}            AS {TFnGetAuxTotalList.ColConfigDescriptionEn.Name},{Environment.NewLine}",
                                $"    c.{TFnGetAuxTotalList.ColEstimationCellId.Name}               AS {TFnGetAuxTotalList.ColEstimationCellId.Name},{Environment.NewLine}",
                                $"    c.{TFnGetAuxTotalList.ColEstimationCellLabelCs.Name}          AS {TFnGetAuxTotalList.ColEstimationCellLabelCs.Name},{Environment.NewLine}",
                                $"    c.{TFnGetAuxTotalList.ColEstimationCellDescriptionCs.Name}    AS {TFnGetAuxTotalList.ColEstimationCellDescriptionCs.Name},{Environment.NewLine}",
                                $"    c.{TFnGetAuxTotalList.ColEstimationCellLabelEn.Name}          AS {TFnGetAuxTotalList.ColEstimationCellLabelEn.Name},{Environment.NewLine}",
                                $"    c.{TFnGetAuxTotalList.ColEstimationCellDescriptionEn.Name}    AS {TFnGetAuxTotalList.ColEstimationCellDescriptionEn.Name},{Environment.NewLine}",
                                $"    a.{TFnGetAuxTotalList.ColCellId.Name}                         AS {TFnGetAuxTotalList.ColCellId.Name},{Environment.NewLine}",
                                $"    a.{TFnGetAuxTotalList.ColAuxTotal.Name}                       AS {TFnGetAuxTotalList.ColAuxTotal.Name},{Environment.NewLine}",
                                $"    e.{TFnGetAuxTotalList.ColExtVersionId.Name}                   AS {TFnGetAuxTotalList.ColExtVersionId.Name},{Environment.NewLine}",
                                $"    e.{TFnGetAuxTotalList.ColExtVersionLabelCs.Name}              AS {TFnGetAuxTotalList.ColExtVersionLabelCs.Name},{Environment.NewLine}",
                                $"    e.{TFnGetAuxTotalList.ColExtVersionDescriptionCs.Name}        AS {TFnGetAuxTotalList.ColExtVersionDescriptionCs.Name},{Environment.NewLine}",
                                $"    e.{TFnGetAuxTotalList.ColExtVersionLabelEn.Name}              AS {TFnGetAuxTotalList.ColExtVersionLabelEn.Name},{Environment.NewLine}",
                                $"    e.{TFnGetAuxTotalList.ColExtVersionDescriptionEn.Name}        AS {TFnGetAuxTotalList.ColExtVersionDescriptionEn.Name},{Environment.NewLine}",
                                $"    g.{TFnGetAuxTotalList.ColGuiVersionId.Name}                   AS {TFnGetAuxTotalList.ColGuiVersionId.Name},{Environment.NewLine}",
                                $"    g.{TFnGetAuxTotalList.ColGuiVersionLabelCs.Name}              AS {TFnGetAuxTotalList.ColGuiVersionLabelCs.Name},{Environment.NewLine}",
                                $"    g.{TFnGetAuxTotalList.ColGuiVersionDescriptionCs.Name}        AS {TFnGetAuxTotalList.ColGuiVersionDescriptionCs.Name},{Environment.NewLine}",
                                $"    g.{TFnGetAuxTotalList.ColGuiVersionLabelEn.Name}              AS {TFnGetAuxTotalList.ColGuiVersionLabelEn.Name},{Environment.NewLine}",
                                $"    g.{TFnGetAuxTotalList.ColGuiVersionDescriptionEn.Name}        AS {TFnGetAuxTotalList.ColGuiVersionDescriptionEn.Name},{Environment.NewLine}",
                                $"    a.{TFnGetAuxTotalList.ColEstDate.Name}                        AS {TFnGetAuxTotalList.ColEstDate.Name}{Environment.NewLine}",
                                $"FROM{Environment.NewLine}",
                                $"    cteAuxTotal AS a{Environment.NewLine}",
                                $"LEFT JOIN{Environment.NewLine}",
                                $"    cteConfig AS b{Environment.NewLine}",
                                $"ON{Environment.NewLine}",
                                $"    (a.{TFnGetAuxTotalList.ColConfigId.Name} = b.{TFnGetAuxTotalList.ColConfigId.Name}){Environment.NewLine}",
                                $"LEFT JOIN{Environment.NewLine}",
                                $"    cteEstimationCell AS c{Environment.NewLine}",
                                $"ON{Environment.NewLine}",
                                $"    (a.{TFnGetAuxTotalList.ColEstimationCellId.Name} = c.{TFnGetAuxTotalList.ColEstimationCellId.Name}){Environment.NewLine}",
                                $"LEFT JOIN{Environment.NewLine}",
                                $"    cteExtVersion AS e{Environment.NewLine}",
                                $"ON{Environment.NewLine}",
                                $"    (a.{TFnGetAuxTotalList.ColExtVersionId.Name} = e.{TFnGetAuxTotalList.ColExtVersionId.Name}){Environment.NewLine}",
                                $"LEFT JOIN{Environment.NewLine}",
                                $"    cteGuiVersion AS g{Environment.NewLine}",
                                $"ON{Environment.NewLine}",
                                $"    (a.{TFnGetAuxTotalList.ColGuiVersionId.Name} = g.{TFnGetAuxTotalList.ColGuiVersionId.Name}){Environment.NewLine}",
                                $"ORDER BY{Environment.NewLine}",
                                $"    b.{TFnGetAuxTotalList.ColConfigCollectionId.Name},{Environment.NewLine}",
                                $"    b.{TFnGetAuxTotalList.ColConfigId.Name},{Environment.NewLine}",
                                $"    c.{TFnGetAuxTotalList.ColEstimationCellId.Name},{Environment.NewLine}",
                                $"    a.{TFnGetAuxTotalList.ColCellId.Name},{Environment.NewLine}",
                                $"    e.{TFnGetAuxTotalList.ColExtVersionId.Name},{Environment.NewLine}",
                                $"    g.{TFnGetAuxTotalList.ColGuiVersionId.Name};{Environment.NewLine}");

                            return result;
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="configurationIds">List of configuration identifiers</param>
                    /// <param name="estimationCellIds">List of estimation cell identifiers</param>
                    /// <param name="extensionVersionIds">List of extension version identifiers</param>
                    /// <param name="guiVersionIds">List of gui version identifiers</param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        List<int> configurationIds,
                        List<int> estimationCellIds,
                        List<int> extensionVersionIds,
                        List<int> guiVersionIds)
                    {
                        string result = SQL;

                        result = result.Replace(
                            oldValue: "@configurationIds",
                            newValue: Functions.PrepIntArrayArg(args: configurationIds, dbType: "int4"));

                        result = result.Replace(
                            oldValue: "@estimationCellIds",
                            newValue: Functions.PrepIntArrayArg(args: estimationCellIds, dbType: "int4"));

                        result = result.Replace(
                            oldValue: "@extensionVersionIds",
                            newValue: Functions.PrepIntArrayArg(args: extensionVersionIds, dbType: "int4"));

                        result = result.Replace(
                            oldValue: "@guiVersionIds",
                            newValue: Functions.PrepIntArrayArg(args: guiVersionIds, dbType: "int4"));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_get_aux_total
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="configurationIds">List of configuration identifiers</param>
                    /// <param name="estimationCellIds">List of estimation cell identifiers</param>
                    /// <param name="extensionVersionIds">List of extension version identifiers</param>
                    /// <param name="guiVersionIds">List of gui version identifiers</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table with calculated auxiliary variable totals</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        List<int> configurationIds,
                        List<int> estimationCellIds,
                        List<int> extensionVersionIds,
                        List<int> guiVersionIds,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(
                            configurationIds: configurationIds,
                            estimationCellIds: estimationCellIds,
                            extensionVersionIds: extensionVersionIds,
                            guiVersionIds: guiVersionIds);

                        NpgsqlParameter pConfigurationIds = new(
                            parameterName: "configurationIds",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        NpgsqlParameter pEstimationCellIds = new(
                            parameterName: "estimationCellIds",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        NpgsqlParameter pExtensionVersionIds = new(
                            parameterName: "extensionVersionIds",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        NpgsqlParameter pGuiVersionIds = new(
                            parameterName: "guiVersionIds",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        if (configurationIds != null)
                        {
                            pConfigurationIds.Value = configurationIds.ToArray<int>();
                        }
                        else
                        {
                            pConfigurationIds.Value = DBNull.Value;
                        }

                        if (estimationCellIds != null)
                        {
                            pEstimationCellIds.Value = estimationCellIds.ToArray<int>();
                        }
                        else
                        {
                            pEstimationCellIds.Value = DBNull.Value;
                        }

                        if (extensionVersionIds != null)
                        {
                            pExtensionVersionIds.Value = extensionVersionIds.ToArray<int>();
                        }
                        else
                        {
                            pExtensionVersionIds.Value = DBNull.Value;
                        }

                        if (guiVersionIds != null)
                        {
                            pGuiVersionIds.Value = guiVersionIds.ToArray<int>();
                        }
                        else
                        {
                            pGuiVersionIds.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteQuery(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pConfigurationIds, pEstimationCellIds, pExtensionVersionIds, pGuiVersionIds);
                    }

                    /// <summary>
                    ///  Execute stored procedure fn_get_aux_total
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="configurationIds">List of configuration identifiers</param>
                    /// <param name="estimationCellIds">List of estimation cell identifiers</param>
                    /// <param name="extensionVersionIds">List of extension version identifiers</param>
                    /// <param name="guiVersionIds">List of gui version identifiers</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>List with calculated auxiliary variable totals</returns>
                    public static TFnGetAuxTotalList Execute(
                        NfiEstaDB database,
                        List<int> configurationIds,
                        List<int> estimationCellIds,
                        List<int> extensionVersionIds,
                        List<int> guiVersionIds,
                        NpgsqlTransaction transaction = null)
                    {
                        System.Diagnostics.Stopwatch stopWatch = new();
                        stopWatch.Start();

                        DataTable data = ExecuteQuery(
                            database: database,
                            configurationIds: configurationIds,
                            estimationCellIds: estimationCellIds,
                            extensionVersionIds: extensionVersionIds,
                            guiVersionIds: guiVersionIds,
                            transaction: transaction);
                        data.TableName = Name;

                        stopWatch.Stop();

                        return new TFnGetAuxTotalList(
                            database: database,
                            data: data,
                            loadingTime: 0.001 * stopWatch.ElapsedMilliseconds,
                            storedProcedure: CommandText)
                        {
                            ConfigurationIds = configurationIds,
                            EstimationCellIds = estimationCellIds,
                            ExtensionVersionIds = extensionVersionIds,
                            GuiVersionIds = guiVersionIds
                        };
                    }

                    #endregion Methods

                }

                #endregion FnGetAuxTotal

            }

        }
    }
}