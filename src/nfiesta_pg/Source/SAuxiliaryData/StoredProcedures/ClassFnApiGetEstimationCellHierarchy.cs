﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using System;
using System.Data;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace AuxiliaryData
        {

            /// <summary>
            /// Stored procedures of schema gisdata
            /// </summary>
            public static partial class ADFunctions
            {
                // fn_api_get_estimation_cell_hierarchy

                #region FnApiGetEstimationCellHierarchy

                /// <summary>
                /// Wrapper for stored procedure fn_api_get_estimation_cell_hierarchy.
                /// Funkce vraci tabulku hierarchii cell z tabulky t_estimation_cell_hierarchy.
                /// Ve vystupu jsou cely, ktere maji v tabulce c_estimation_cell_collection nastaveno use4estimates = TRUE.
                /// </summary>
                public static class FnApiGetEstimationCellHierarchy
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName = ADSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name = "fn_api_get_estimation_cell_hierarchy";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ; ",
                            $"returns: ",
                            $"TABLE(",
                            $"id integer, ",
                            $"estimation_cell_id integer, ",
                            $"estimation_cell_collection_id integer, ",
                            $"estimation_cell_label_cs character varying, ",
                            $"estimation_cell_description_cs text, ",
                            $"estimation_cell_label_en character varying, ",
                            $"estimation_cell_description_en text, ",
                            $"superior_estimation_cell_id integer)");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return String.Concat(
                            $"WITH cte AS MATERIALIZED{Environment.NewLine}",
                            $"({Environment.NewLine}",
                            $"SELECT{Environment.NewLine}",
                            $"    {TFnApiGetEstimationCellHierarchyList.ColId.SQL(cols: TFnApiGetEstimationCellHierarchyList.Cols, alias: Name)}",
                            $"    {TFnApiGetEstimationCellHierarchyList.ColEstimationCellCollectionId.SQL(cols: TFnApiGetEstimationCellHierarchyList.Cols, alias: Name)}",
                            $"    {TFnApiGetEstimationCellHierarchyList.ColEstimationCellId.SQL(cols: TFnApiGetEstimationCellHierarchyList.Cols, alias: Name)}",
                            $"    {TFnApiGetEstimationCellHierarchyList.ColEstimationCellLabelCs.SQL(cols: TFnApiGetEstimationCellHierarchyList.Cols, alias: Name)}",
                            $"    {TFnApiGetEstimationCellHierarchyList.ColEstimationCellDescriptionCs.SQL(cols: TFnApiGetEstimationCellHierarchyList.Cols, alias: Name)}",
                            $"    {TFnApiGetEstimationCellHierarchyList.ColEstimationCellLabelEn.SQL(cols: TFnApiGetEstimationCellHierarchyList.Cols, alias: Name)}",
                            $"    {TFnApiGetEstimationCellHierarchyList.ColEstimationCellDescriptionEn.SQL(cols: TFnApiGetEstimationCellHierarchyList.Cols, alias: Name)}",
                            $"    {TFnApiGetEstimationCellHierarchyList.ColSuperiorEstimationCellId.SQL(cols: TFnApiGetEstimationCellHierarchyList.Cols, alias: Name, isLastOne: true)}",
                            $"FROM{Environment.NewLine}",
                            $"    {SchemaName}.{Name}() AS {Name} {Environment.NewLine}",
                            $"){Environment.NewLine}",
                            $"SELECT{Environment.NewLine}",
                            $"    a.{TFnApiGetEstimationCellHierarchyList.ColId.Name} AS {TFnApiGetEstimationCellHierarchyList.ColId.Name},{Environment.NewLine}",
                            $"    a.{TFnApiGetEstimationCellHierarchyList.ColEstimationCellCollectionId.Name} AS {TFnApiGetEstimationCellHierarchyList.ColEstimationCellCollectionId.Name},{Environment.NewLine}",
                            $"    a.{TFnApiGetEstimationCellHierarchyList.ColEstimationCellId.Name} AS {TFnApiGetEstimationCellHierarchyList.ColEstimationCellId.Name},{Environment.NewLine}",
                            $"    a.{TFnApiGetEstimationCellHierarchyList.ColEstimationCellLabelCs.Name} AS {TFnApiGetEstimationCellHierarchyList.ColEstimationCellLabelCs.Name},{Environment.NewLine}",
                            $"    a.{TFnApiGetEstimationCellHierarchyList.ColEstimationCellDescriptionCs.Name} AS {TFnApiGetEstimationCellHierarchyList.ColEstimationCellDescriptionCs.Name},{Environment.NewLine}",
                            $"    a.{TFnApiGetEstimationCellHierarchyList.ColEstimationCellLabelEn.Name} AS {TFnApiGetEstimationCellHierarchyList.ColEstimationCellLabelEn.Name},{Environment.NewLine}",
                            $"    a.{TFnApiGetEstimationCellHierarchyList.ColEstimationCellDescriptionEn.Name} AS {TFnApiGetEstimationCellHierarchyList.ColEstimationCellDescriptionEn.Name},{Environment.NewLine}",
                            $"    s.{TFnApiGetEstimationCellHierarchyList.ColId.Name} AS {TFnApiGetEstimationCellHierarchyList.ColSuperiorId.Name},{Environment.NewLine}",
                            $"    s.{TFnApiGetEstimationCellHierarchyList.ColEstimationCellCollectionId.Name} AS {TFnApiGetEstimationCellHierarchyList.ColSuperiorEstimationCellCollectionId.Name},{Environment.NewLine}",
                            $"    s.{TFnApiGetEstimationCellHierarchyList.ColEstimationCellId.Name} AS {TFnApiGetEstimationCellHierarchyList.ColSuperiorEstimationCellId.Name},{Environment.NewLine}",
                            $"    s.{TFnApiGetEstimationCellHierarchyList.ColEstimationCellLabelCs.Name} AS {TFnApiGetEstimationCellHierarchyList.ColSuperiorEstimationCellLabelCs.Name},{Environment.NewLine}",
                            $"    s.{TFnApiGetEstimationCellHierarchyList.ColEstimationCellDescriptionCs.Name} AS {TFnApiGetEstimationCellHierarchyList.ColSuperiorEstimationCellDescriptionCs.Name},{Environment.NewLine}",
                            $"    s.{TFnApiGetEstimationCellHierarchyList.ColEstimationCellLabelEn.Name} AS {TFnApiGetEstimationCellHierarchyList.ColSuperiorEstimationCellLabelEn.Name},{Environment.NewLine}",
                            $"    s.{TFnApiGetEstimationCellHierarchyList.ColEstimationCellDescriptionEn.Name} AS {TFnApiGetEstimationCellHierarchyList.ColSuperiorEstimationCellDescriptionEn.Name}{Environment.NewLine}",
                            $"FROM{Environment.NewLine}",
                            $"    cte AS a {Environment.NewLine}",
                            $"LEFT JOIN{Environment.NewLine}",
                            $"    cte AS s {Environment.NewLine}",
                            $"ON{Environment.NewLine}",
                            $"    (a.{TFnApiGetEstimationCellHierarchyList.ColSuperiorEstimationCellId.Name} = s.{TFnApiGetEstimationCellHierarchyList.ColEstimationCellId.Name}){Environment.NewLine}",
                            $"ORDER BY{Environment.NewLine}",
                            $"    a.{TFnApiGetEstimationCellHierarchyList.ColId.Name};{Environment.NewLine}"
                            );
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <returns>Command text</returns>
                    public static string GetCommandText()
                    {
                        return SQL;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_api_get_estimation_cell_hierarchy
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table of estimation cells with hierarchies</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText();

                        return database.Postgres.ExecuteQuery(
                            sqlCommand: SQL,
                            transaction: transaction);
                    }

                    /// <summary>
                    /// Execute stored procedure fn_api_get_estimation_cell_hierarchy
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>List of estimation cells with hierarchies</returns>
                    public static TFnApiGetEstimationCellHierarchyList Execute(
                        NfiEstaDB database,
                        NpgsqlTransaction transaction = null)
                    {
                        System.Diagnostics.Stopwatch stopWatch = new();
                        stopWatch.Start();

                        DataTable data = ExecuteQuery(
                            database: database,
                            transaction: transaction);
                        data.TableName = Name;

                        stopWatch.Stop();

                        return new TFnApiGetEstimationCellHierarchyList(
                            database: database,
                            data: data,
                            loadingTime: 0.001 * stopWatch.ElapsedMilliseconds,
                            storedProcedure: CommandText);
                    }

                    #endregion Methods

                }

                #endregion FnApiGetEstimationCellHierarchy

            }

        }
    }
}