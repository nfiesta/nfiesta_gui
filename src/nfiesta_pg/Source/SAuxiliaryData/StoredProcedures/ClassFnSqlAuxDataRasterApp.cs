﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace AuxiliaryData
        {

            /// <summary>
            /// Stored procedures of schema gisdata
            /// </summary>
            public static partial class ADFunctions
            {
                // fn_sql_aux_data_raster_app

                #region FnSqlAuxDataRasterApp

                /// <summary>
                /// Wrapper for stored procedure fn_sql_aux_data_raster_app.
                /// Funkce vrací SQL textový řetězec pro získání pomocné proměnné z rasterové vrstvy protínáním bodů.
                /// </summary>
                public static class FnSqlAuxDataRasterApp
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName = ADSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name = "fn_sql_aux_data_raster_app";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"config_collection integer, ",
                            $"config integer, ",
                            $"schema_name character varying, ",
                            $"table_name character varying, ",
                            $"column_ident character varying, ",
                            $"column_name character varying, ",
                            $"band integer, ",
                            $"reclass integer, ",
                            $"condition character varying, ",
                            $"unit double precision, ",
                            $"gid_start integer, ",
                            $"gid_end integer; ",
                            $"returns: text");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    {SchemaName}.{Name}",
                            $"(@configCollection, @config, ",
                            $"@schemaName, @tableName, @columnIdent, @columnName, ",
                            $"@band, @reclass, @condition, @unit, @gidStart, @gidEnd)::text AS {Name};{Environment.NewLine}");
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="configCollection"></param>
                    /// <param name="config"></param>
                    /// <param name="schemaName"></param>
                    /// <param name="tableName"></param>
                    /// <param name="columnIdent"></param>
                    /// <param name="columnName"></param>
                    /// <param name="band"></param>
                    /// <param name="reclass"></param>
                    /// <param name="condition"></param>
                    /// <param name="unit"></param>
                    /// <param name="gidStart"></param>
                    /// <param name="gidEnd"></param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        Nullable<int> configCollection,
                        Nullable<int> config,
                        string schemaName,
                        string tableName,
                        string columnIdent,
                        string columnName,
                        Nullable<int> band,
                        Nullable<int> reclass,
                        string condition,
                        Nullable<double> unit,
                        Nullable<int> gidStart,
                        Nullable<int> gidEnd)
                    {
                        string result = SQL;

                        result = result.Replace(
                            oldValue: "@configCollection",
                            newValue: Functions.PrepNIntArg(arg: configCollection));

                        result = result.Replace(
                            oldValue: "@config",
                            newValue: Functions.PrepNIntArg(arg: config));

                        result = result.Replace(
                            oldValue: "@schemaName",
                            newValue: Functions.PrepStringArg(arg: schemaName));

                        result = result.Replace(
                            oldValue: "@tableName",
                            newValue: Functions.PrepStringArg(arg: tableName));

                        result = result.Replace(
                            oldValue: "@columnIdent",
                            newValue: Functions.PrepStringArg(arg: columnIdent));

                        result = result.Replace(
                            oldValue: "@columnName",
                            newValue: Functions.PrepStringArg(arg: columnName));

                        result = result.Replace(
                            oldValue: "@band",
                            newValue: Functions.PrepNIntArg(arg: band));

                        result = result.Replace(
                            oldValue: "@reclass",
                            newValue: Functions.PrepNIntArg(arg: reclass));

                        result = result.Replace(
                            oldValue: "@condition",
                            newValue: Functions.PrepStringArg(arg: condition));

                        result = result.Replace(
                            oldValue: "@unit",
                            newValue: Functions.PrepNDoubleArg(arg: unit));

                        result = result.Replace(
                            oldValue: "@gidStart",
                            newValue: Functions.PrepNIntArg(arg: gidStart));

                        result = result.Replace(
                            oldValue: "@gidEnd",
                            newValue: Functions.PrepNIntArg(arg: gidEnd));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_sql_aux_data_raster_app
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="configCollection"></param>
                    /// <param name="config"></param>
                    /// <param name="schemaName"></param>
                    /// <param name="tableName"></param>
                    /// <param name="columnIdent"></param>
                    /// <param name="columnName"></param>
                    /// <param name="band"></param>
                    /// <param name="reclass"></param>
                    /// <param name="condition"></param>
                    /// <param name="unit"></param>
                    /// <param name="gidStart"></param>
                    /// <param name="gidEnd"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> configCollection,
                        Nullable<int> config,
                        string schemaName,
                        string tableName,
                        string columnIdent,
                        string columnName,
                        Nullable<int> band,
                        Nullable<int> reclass,
                        string condition,
                        Nullable<double> unit,
                        Nullable<int> gidStart,
                        Nullable<int> gidEnd,
                        NpgsqlTransaction transaction = null)
                    {
                        string val = Execute(
                            database: database,
                            configCollection: configCollection,
                            config: config,
                            schemaName: schemaName,
                            tableName: tableName,
                            columnIdent: columnIdent,
                            columnName: columnName,
                            band: band,
                            reclass: reclass,
                            condition: condition,
                            unit: unit,
                            gidStart: gidStart,
                            gidEnd: gidEnd,
                            transaction: transaction);

                        DataTable dt = new();

                        dt.Columns.Add(column: new DataColumn()
                        {
                            ColumnName = Name,
                            DataType = Type.GetType(typeName: "System.String")
                        });

                        DataRow row = dt.NewRow();

                        Functions.SetStringArg(
                            row: row,
                            name: Name,
                            val: val);

                        dt.Rows.Add(row: row);

                        return dt;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_sql_aux_data_raster_app
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="configCollection"></param>
                    /// <param name="config"></param>
                    /// <param name="schemaName"></param>
                    /// <param name="tableName"></param>
                    /// <param name="columnIdent"></param>
                    /// <param name="columnName"></param>
                    /// <param name="band"></param>
                    /// <param name="reclass"></param>
                    /// <param name="condition"></param>
                    /// <param name="unit"></param>
                    /// <param name="gidStart"></param>
                    /// <param name="gidEnd"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Text</returns>
                    public static string Execute(
                        NfiEstaDB database,
                        Nullable<int> configCollection,
                        Nullable<int> config,
                        string schemaName,
                        string tableName,
                        string columnIdent,
                        string columnName,
                        Nullable<int> band,
                        Nullable<int> reclass,
                        string condition,
                        Nullable<double> unit,
                        Nullable<int> gidStart,
                        Nullable<int> gidEnd,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(
                            configCollection: configCollection,
                            config: config,
                            schemaName: schemaName,
                            tableName: tableName,
                            columnIdent: columnIdent,
                            columnName: columnName,
                            band: band,
                            reclass: reclass,
                            condition: condition,
                            unit: unit,
                            gidStart: gidStart,
                            gidEnd: gidEnd);

                        NpgsqlParameter pConfigCollection = new(
                            parameterName: "configCollection",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pConfig = new(
                            parameterName: "config",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pSchemaName = new(
                            parameterName: "schemaName",
                            parameterType: NpgsqlDbType.Varchar);

                        NpgsqlParameter pTableName = new(
                            parameterName: "tableName",
                            parameterType: NpgsqlDbType.Varchar);

                        NpgsqlParameter pColumnIdent = new(
                           parameterName: "columnIdent",
                           parameterType: NpgsqlDbType.Varchar);

                        NpgsqlParameter pColumnName = new(
                           parameterName: "columnName",
                           parameterType: NpgsqlDbType.Varchar);

                        NpgsqlParameter pBand = new(
                            parameterName: "band",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pReclass = new(
                            parameterName: "reclass",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pCondition = new(
                           parameterName: "condition",
                           parameterType: NpgsqlDbType.Varchar);

                        NpgsqlParameter pUnit = new(
                            parameterName: "unit",
                            parameterType: NpgsqlDbType.Double);

                        NpgsqlParameter pGidStart = new(
                            parameterName: "gidStart",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pGidEnd = new(
                            parameterName: "gidEnd",
                            parameterType: NpgsqlDbType.Integer);

                        if (configCollection != null)
                        {
                            pConfigCollection.Value = (int)configCollection;
                        }
                        else
                        {
                            pConfigCollection.Value = DBNull.Value;
                        }

                        if (config != null)
                        {
                            pConfig.Value = (int)config;
                        }
                        else
                        {
                            pConfig.Value = DBNull.Value;
                        }

                        if (schemaName != null)
                        {
                            pSchemaName.Value = schemaName;
                        }
                        else
                        {
                            pSchemaName.Value = DBNull.Value;
                        }

                        if (tableName != null)
                        {
                            pTableName.Value = tableName;
                        }
                        else
                        {
                            pTableName.Value = DBNull.Value;
                        }

                        if (columnIdent != null)
                        {
                            pColumnIdent.Value = columnIdent;
                        }
                        else
                        {
                            pColumnIdent.Value = DBNull.Value;
                        }

                        if (columnName != null)
                        {
                            pColumnName.Value = columnName;
                        }
                        else
                        {
                            pColumnName.Value = DBNull.Value;
                        }

                        if (band != null)
                        {
                            pBand.Value = (int)band;
                        }
                        else
                        {
                            pBand.Value = DBNull.Value;
                        }

                        if (reclass != null)
                        {
                            pReclass.Value = (int)reclass;
                        }
                        else
                        {
                            pReclass.Value = DBNull.Value;
                        }

                        if (condition != null)
                        {
                            pCondition.Value = condition;
                        }
                        else
                        {
                            pCondition.Value = DBNull.Value;
                        }

                        if (unit != null)
                        {
                            pUnit.Value = (double)unit;
                        }
                        else
                        {
                            pUnit.Value = DBNull.Value;
                        }

                        if (gidStart != null)
                        {
                            pGidStart.Value = (int)gidStart;
                        }
                        else
                        {
                            pGidStart.Value = DBNull.Value;
                        }

                        if (gidEnd != null)
                        {
                            pGidEnd.Value = (int)gidEnd;
                        }
                        else
                        {
                            pGidEnd.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteScalar(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pConfigCollection, pConfig,
                            pSchemaName, pTableName, pColumnIdent, pColumnName,
                            pBand, pReclass, pCondition, pUnit,
                            pGidStart, pGidEnd);
                    }

                    #endregion Methods

                }

                #endregion FnSqlAuxDataRasterApp

            }

        }
    }
}