﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace AuxiliaryData
        {

            /// <summary>
            /// Stored procedures of schema gisdata
            /// </summary>
            public static partial class ADFunctions
            {
                // fn_get_aux_total_app

                #region FnGetAuxTotalApp

                /// <summary>
                /// Wrapper for stored procedure fn_get_aux_total_app.
                /// Funkce ukládá vypočítanou hodnotu aux_total pro zadanou konfiguraci (config_id)
                /// a celu (estimation_cell) nebo gid,
                /// a další metadata do tabulky t_aux_total.
                /// </summary>
                public static class FnGetAuxTotalApp
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName = ADSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name = "fn_get_aux_total_app";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_config_id integer, ",
                            $"_estimation_cell integer, ",
                            $"_gid integer, ",
                            $"_gui_version integer; ",
                            $"returns: void");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                String.Concat(
                                    $"SELECT{Environment.NewLine}",
                                    $"     {SchemaName}.{Name}(@configId, @estimationCellId, @cellId, @guiVersionId) AS {Name};{Environment.NewLine}");
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="configId">Configuration for auxiliary variable category calculation identifier</param>
                    /// <param name="estimationCellId">Estimation cell identifier</param>
                    /// <param name="cellId">Estimation cell segment identifier</param>
                    /// <param name="guiVersionId">Module version identifier</param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        Nullable<int> configId,
                        Nullable<int> estimationCellId,
                        Nullable<int> cellId,
                        Nullable<int> guiVersionId)
                    {
                        string result = SQL;

                        result = result.Replace(
                             oldValue: "@configId",
                             newValue: Functions.PrepNIntArg(arg: configId));

                        result = result.Replace(
                            oldValue: "@estimationCellId",
                            newValue: Functions.PrepNIntArg(arg: estimationCellId));

                        result = result.Replace(
                            oldValue: "@cellId",
                            newValue: Functions.PrepNIntArg(arg: cellId));

                        result = result.Replace(
                           oldValue: "@guiVersionId",
                           newValue: Functions.PrepNIntArg(arg: guiVersionId));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_get_aux_total_app
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="configId">Configuration for auxiliary variable category calculation identifier</param>
                    /// <param name="estimationCellId">Estimation cell identifier</param>
                    /// <param name="cellId">Estimation cell segment identifier</param>
                    /// <param name="guiVersionId">Module version identifier</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Empty data table</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> configId,
                        Nullable<int> estimationCellId,
                        Nullable<int> cellId,
                        Nullable<int> guiVersionId,
                        NpgsqlTransaction transaction = null)
                    {
                        Execute(
                            database: database,
                            configId: configId,
                            estimationCellId: estimationCellId,
                            cellId: cellId,
                            guiVersionId: guiVersionId,
                            transaction: transaction);

                        return new DataTable();
                    }

                    /// <summary>
                    /// Execute stored procedure fn_get_aux_total_app
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="configId">Configuration for auxiliary variable category calculation identifier</param>
                    /// <param name="estimationCellId">Estimation cell identifier</param>
                    /// <param name="cellId">Estimation cell segment identifier</param>
                    /// <param name="guiVersionId">Module version identifier</param>
                    /// <param name="transaction">Transaction</param>
                    public static void Execute(
                        NfiEstaDB database,
                        Nullable<int> configId,
                        Nullable<int> estimationCellId,
                        Nullable<int> cellId,
                        Nullable<int> guiVersionId,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(
                            configId: configId,
                            estimationCellId: estimationCellId,
                            cellId: cellId,
                            guiVersionId: guiVersionId);

                        NpgsqlParameter pConfigId = new(
                            parameterName: "configId",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pEstimationCellId = new(
                            parameterName: "estimationCellId",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pCellId = new(
                            parameterName: "cellId",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pGuiVersionId = new(
                            parameterName: "guiVersionId",
                            parameterType: NpgsqlDbType.Integer);

                        if (configId != null)
                        {
                            pConfigId.Value = (int)configId;
                        }
                        else
                        {
                            pConfigId.Value = DBNull.Value;
                        }

                        if (estimationCellId != null)
                        {
                            pEstimationCellId.Value = (int)estimationCellId;
                        }
                        else
                        {
                            pEstimationCellId.Value = DBNull.Value;
                        }

                        if (cellId != null)
                        {
                            pCellId.Value = (int)cellId;
                        }
                        else
                        {
                            pCellId.Value = DBNull.Value;
                        }

                        if (guiVersionId != null)
                        {
                            pGuiVersionId.Value = (int)guiVersionId;
                        }
                        else
                        {
                            pGuiVersionId.Value = DBNull.Value;
                        }

                        database.Postgres.ExecuteNonQuery(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pConfigId, pEstimationCellId, pCellId, pGuiVersionId);
                    }

                    #endregion Methods

                }

                #endregion FnGetAuxTotalApp

            }

        }
    }
}