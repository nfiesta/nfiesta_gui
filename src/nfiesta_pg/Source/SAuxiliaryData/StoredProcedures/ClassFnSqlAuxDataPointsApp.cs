﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace AuxiliaryData
        {

            /// <summary>
            /// Stored procedures of schema gisdata
            /// </summary>
            public static partial class ADFunctions
            {
                // fn_sql_aux_data_points_app

                #region FnSqlAuxDataPointsApp

                /// <summary>
                /// Wrapper for stored procedure fn_sql_aux_data_points_app.
                /// Funkce vrací SQL textový řetězec pro získání seznamu bodů pro bodové protínání.
                /// </summary>
                public static class FnSqlAuxDataPointsApp
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName = ADSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name = "fn_sql_aux_data_points_app";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_ref_id_layer_points integer, ",
                            $"_gid_start integer, ",
                            $"_gid_end integer, ",
                            $"_config_collection integer, ",
                            $"_config integer; ",
                            $"returns: text");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    {SchemaName}.{Name}",
                            $"(@refIdLayerPoints, @cellIdStart, @cellIdEnd, @configCollectionId, @configId)::text AS {Name};{Environment.NewLine}");
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="refIdLayerPoints">Point identifier</param>
                    /// <param name="cellIdStart">First estimation cell segment identifier</param>
                    /// <param name="cellIdEnd">Last estimation cell segment identifier</param>
                    /// <param name="configCollectionId">Configuration collection for auxiliary variable identifier</param>
                    /// <param name="configId">Configuration for auxiliary variable category identifier </param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        Nullable<int> refIdLayerPoints,
                        Nullable<int> cellIdStart,
                        Nullable<int> cellIdEnd,
                        Nullable<int> configCollectionId,
                        Nullable<int> configId)
                    {
                        string result = SQL;

                        result = result.Replace(
                            oldValue: "@refIdLayerPoints",
                            newValue: Functions.PrepNIntArg(arg: refIdLayerPoints));

                        result = result.Replace(
                            oldValue: "@cellIdStart",
                            newValue: Functions.PrepNIntArg(arg: cellIdStart));

                        result = result.Replace(
                            oldValue: "@cellIdEnd",
                            newValue: Functions.PrepNIntArg(arg: cellIdEnd));

                        result = result.Replace(
                            oldValue: "@configCollectionId",
                            newValue: Functions.PrepNIntArg(arg: configCollectionId));

                        result = result.Replace(
                            oldValue: "@configId",
                            newValue: Functions.PrepNIntArg(arg: configId));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_sql_aux_data_points_app
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="refIdLayerPoints">Point identifier</param>
                    /// <param name="cellIdStart">First estimation cell segment identifier</param>
                    /// <param name="cellIdEnd">Last estimation cell segment identifier</param>
                    /// <param name="configCollectionId">Configuration collection for auxiliary variable identifier</param>
                    /// <param name="configId">Configuration for auxiliary variable category identifier </param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> refIdLayerPoints,
                        Nullable<int> cellIdStart,
                        Nullable<int> cellIdEnd,
                        Nullable<int> configCollectionId,
                        Nullable<int> configId,
                        NpgsqlTransaction transaction = null)
                    {
                        string val = Execute(
                            database: database,
                            refIdLayerPoints: refIdLayerPoints,
                            cellIdStart: cellIdStart,
                            cellIdEnd: cellIdEnd,
                            configCollectionId: configCollectionId,
                            configId: configId,
                            transaction: transaction);

                        DataTable dt = new();

                        dt.Columns.Add(column: new DataColumn()
                        {
                            ColumnName = Name,
                            DataType = Type.GetType(typeName: "System.String")
                        });

                        DataRow row = dt.NewRow();

                        Functions.SetStringArg(
                            row: row,
                            name: Name,
                            val: val);

                        dt.Rows.Add(row: row);

                        return dt;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_sql_aux_data_points_app
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="refIdLayerPoints">Point identifier</param>
                    /// <param name="cellIdStart">First estimation cell segment identifier</param>
                    /// <param name="cellIdEnd">Last estimation cell segment identifier</param>
                    /// <param name="configCollectionId">Configuration collection for auxiliary variable identifier</param>
                    /// <param name="configId">Configuration for auxiliary variable category identifier </param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Text</returns>
                    public static string Execute(
                        NfiEstaDB database,
                        Nullable<int> refIdLayerPoints,
                        Nullable<int> cellIdStart,
                        Nullable<int> cellIdEnd,
                        Nullable<int> configCollectionId,
                        Nullable<int> configId,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(
                            refIdLayerPoints: refIdLayerPoints,
                            cellIdStart: cellIdStart,
                            cellIdEnd: cellIdEnd,
                            configCollectionId: configCollectionId,
                            configId: configId);

                        NpgsqlParameter pRefIdLayerPoints = new(
                            parameterName: "refIdLayerPoints",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pCellIdStart = new(
                            parameterName: "cellIdStart",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pCellIdEnd = new(
                            parameterName: "cellIdEnd",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pConfigCollectionId = new(
                            parameterName: "configCollectionId",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pConfigId = new(
                            parameterName: "configId",
                            parameterType: NpgsqlDbType.Integer);

                        if (refIdLayerPoints != null)
                        {
                            pRefIdLayerPoints.Value = (int)refIdLayerPoints;
                        }
                        else
                        {
                            pRefIdLayerPoints.Value = DBNull.Value;
                        }

                        if (cellIdStart != null)
                        {
                            pCellIdStart.Value = (int)cellIdStart;
                        }
                        else
                        {
                            pCellIdStart.Value = DBNull.Value;
                        }

                        if (cellIdEnd != null)
                        {
                            pCellIdEnd.Value = (int)cellIdEnd;
                        }
                        else
                        {
                            pCellIdEnd.Value = DBNull.Value;
                        }

                        if (configCollectionId != null)
                        {
                            pConfigCollectionId.Value = (int)configCollectionId;
                        }
                        else
                        {
                            pConfigCollectionId.Value = DBNull.Value;
                        }

                        if (configId != null)
                        {
                            pConfigId.Value = (int)configId;
                        }
                        else
                        {
                            pConfigId.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteScalar(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pRefIdLayerPoints,
                            pCellIdStart, pCellIdEnd,
                            pConfigCollectionId, pConfigId);
                    }

                    #endregion Methods

                }

                #endregion FnSqlAuxDataPointsApp

            }

        }
    }
}