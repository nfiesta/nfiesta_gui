﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace AuxiliaryData
        {

            /// <summary>
            /// Stored procedures of schema gisdata
            /// </summary>
            public static partial class ADFunctions
            {
                // fn_get_aux_data_app

                #region FnGetAuxDataApp

                /// <summary>
                /// Wrapper for stored procedure fn_get_aux_data_app.
                /// The function returns data for insert into the table t_auxiliary_data.
                /// </summary>
                public static class FnGetAuxDataApp
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName = ADSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name = "fn_get_aux_data_app";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_config_collection integer, ",
                            $"_config integer, ",
                            $"_intersects boolean, ",
                            $"_gid_start integer, ",
                            $"_gid_end integer; ",
                            $"returns: ",
                            $"TABLE(",
                            $"config_collection integer, ",
                            $"config integer, ",
                            $"gid integer, ",
                            $"value double precision, ",
                            $"ext_version integer)");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return String.Concat(
                                $"SELECT{Environment.NewLine}",
                                $"    {TFnGetAuxDataAppList.ColId.SQL(cols: TFnGetAuxDataAppList.Cols, alias: Name)}",
                                $"    {TFnGetAuxDataAppList.ColConfigCollectionId.SQL(cols: TFnGetAuxDataAppList.Cols, alias: Name)}",
                                $"    {TFnGetAuxDataAppList.ColConfigId.SQL(cols: TFnGetAuxDataAppList.Cols, alias: Name)}",
                                $"    {TFnGetAuxDataAppList.ColGid.SQL(cols: TFnGetAuxDataAppList.Cols, alias: Name)}",
                                $"    {TFnGetAuxDataAppList.ColAuxValue.SQL(cols: TFnGetAuxDataAppList.Cols, alias: Name)}",
                                $"    {TFnGetAuxDataAppList.ColExtensionVersionId.SQL(cols: TFnGetAuxDataAppList.Cols, alias: Name, isLastOne: true)}",
                                $"FROM{Environment.NewLine}",
                                $"    {SchemaName}.{Name}(@configCollectionId, @configId, @intersects, @gidStart, @gidEnd) AS {Name} {Environment.NewLine}",
                                $"ORDER BY{Environment.NewLine}",
                                $"    {Name}.{TFnGetAuxDataAppList.ColConfigCollectionId.DbName},{Environment.NewLine}",
                                $"    {Name}.{TFnGetAuxDataAppList.ColConfigId.DbName},{Environment.NewLine}",
                                $"    {Name}.{TFnGetAuxDataAppList.ColGid.DbName},{Environment.NewLine}",
                                $"    {Name}.{TFnGetAuxDataAppList.ColExtensionVersionId.DbName};{Environment.NewLine}"
                                );
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="configCollectionId">Configuration collection for auxiliary variable identifier</param>
                    /// <param name="configId">Configuration for auxiliary variable category identifier</param>
                    /// <param name="intersects">Intersects (true/false)</param>
                    /// <param name="gidStart">First estimation cell segment identifier</param>
                    /// <param name="gidEnd">Last estimation cell segment identifier</param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        Nullable<int> configCollectionId,
                        Nullable<int> configId,
                        Nullable<bool> intersects,
                        Nullable<int> gidStart,
                        Nullable<int> gidEnd)
                    {
                        string result = SQL;

                        result = result.Replace(
                            oldValue: "@configCollectionId",
                            newValue: Functions.PrepNIntArg(arg: configCollectionId));

                        result = result.Replace(
                             oldValue: "@configId",
                             newValue: Functions.PrepNIntArg(arg: configId));

                        result = result.Replace(
                            oldValue: "@intersects",
                            newValue: Functions.PrepNBoolArg(arg: intersects));

                        result = result.Replace(
                            oldValue: "@gidStart",
                            newValue: Functions.PrepNIntArg(arg: gidStart));

                        result = result.Replace(
                            oldValue: "@gidEnd",
                            newValue: Functions.PrepNIntArg(arg: gidEnd));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_get_aux_data_app
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="configCollectionId">Configuration collection for auxiliary variable identifier</param>
                    /// <param name="configId">Configuration for auxiliary variable category identifier</param>
                    /// <param name="intersects">Intersects (true/false)</param>
                    /// <param name="gidStart">First estimation cell segment identifier</param>
                    /// <param name="gidEnd">Last estimation cell segment identifier</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database, Nullable<int> configCollectionId,
                        Nullable<int> configId,
                        Nullable<bool> intersects,
                        Nullable<int> gidStart,
                        Nullable<int> gidEnd,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(
                            configCollectionId: configCollectionId,
                            configId: configId,
                            intersects: intersects,
                            gidStart: gidStart,
                            gidEnd: gidEnd);

                        NpgsqlParameter pConfigCollectionId = new(
                            parameterName: "configCollectionId",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pConfigId = new(
                            parameterName: "configId",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pIntersects = new(
                          parameterName: "intersects",
                          parameterType: NpgsqlDbType.Boolean);

                        NpgsqlParameter pGidStart = new(
                            parameterName: "gidStart",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pGidEnd = new(
                            parameterName: "gidEnd",
                            parameterType: NpgsqlDbType.Integer);

                        if (configCollectionId != null)
                        {
                            pConfigCollectionId.Value = (int)configCollectionId;
                        }
                        else
                        {
                            pConfigCollectionId.Value = DBNull.Value;
                        }

                        if (configId != null)
                        {
                            pConfigId.Value = (int)configId;
                        }
                        else
                        {
                            pConfigId.Value = DBNull.Value;
                        }

                        if (intersects != null)
                        {
                            pIntersects.Value = (bool)intersects;
                        }
                        else
                        {
                            pIntersects.Value = DBNull.Value;
                        }

                        if (gidStart != null)
                        {
                            pGidStart.Value = (int)gidStart;
                        }
                        else
                        {
                            pGidStart.Value = DBNull.Value;
                        }

                        if (gidEnd != null)
                        {
                            pGidEnd.Value = (int)gidEnd;
                        }
                        else
                        {
                            pGidEnd.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteQuery(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pConfigCollectionId, pConfigId, pIntersects, pGidStart, pGidEnd);
                    }

                    /// <summary>
                    /// Execute stored procedure fn_get_aux_data_app
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="configCollectionId">Configuration collection for auxiliary variable identifier</param>
                    /// <param name="configId">Configuration for auxiliary variable category identifier</param>
                    /// <param name="intersects">Intersects (true/false)</param>
                    /// <param name="gidStart">First estimation cell segment identifier</param>
                    /// <param name="gidEnd">Last estimation cell segment identifier</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>List of data for insert into the table t_auxiliary_data</returns>
                    public static TFnGetAuxDataAppList Execute(
                        NfiEstaDB database,
                        Nullable<int> configCollectionId,
                        Nullable<int> configId,
                        Nullable<bool> intersects,
                        Nullable<int> gidStart,
                        Nullable<int> gidEnd,
                        NpgsqlTransaction transaction = null)
                    {
                        System.Diagnostics.Stopwatch stopWatch = new();
                        stopWatch.Start();

                        DataTable data = ExecuteQuery(
                            database: database,
                            configCollectionId: configCollectionId,
                            configId: configId,
                            intersects: intersects,
                            gidStart: gidStart,
                            gidEnd: gidEnd,
                            transaction: transaction);
                        data.TableName = Name;

                        stopWatch.Stop();

                        return new TFnGetAuxDataAppList(
                            database: database,
                            data: data,
                            loadingTime: 0.001 * stopWatch.ElapsedMilliseconds,
                            storedProcedure: CommandText)
                        {
                            ConfigCollectionId = configCollectionId,
                            ConfigId = configId,
                            Intersects = intersects,
                            GidStart = gidStart,
                            GidEnd = gidEnd
                        };
                    }

                    #endregion Methods

                }

                #endregion FnGetAuxDataApp

            }

        }
    }
}