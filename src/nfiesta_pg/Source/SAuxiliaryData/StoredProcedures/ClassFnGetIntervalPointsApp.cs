﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace AuxiliaryData
        {

            /// <summary>
            /// Stored procedures of schema gisdata
            /// </summary>
            public static partial class ADFunctions
            {
                // fn_get_interval_points_app

                #region FnGetIntervalPointsApp

                /// <summary>
                /// Wrapper for stored procedure fn_get_interval_points_app.
                /// The function returns an interval list for the point layer according to the set interval
                /// interval_points specifying the number of points.
                /// </summary>
                public static class FnGetIntervalPointsApp
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName = ADSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name = "fn_get_interval_points_app";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_config_collection integer, ",
                            $"_interval_points integer; ",
                            $"returns: ",
                            $"TABLE(",
                            $"step4interval integer, ",
                            $"gid_start integer, ",
                            $"gid_end integer, ",
                            $"interval_points integer)");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    step4interval      AS step4interval,{Environment.NewLine}",
                            $"    gid_start          AS gid_start,{Environment.NewLine}",
                            $"    gid_end            AS gid_end,{Environment.NewLine}",
                            $"    interval_points    AS interval_points{Environment.NewLine}",
                            $"FROM{Environment.NewLine}",
                            $"    {SchemaName}.{Name}(@configCollectionId, @intervalPoints);{Environment.NewLine}"
                            );
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="configCollectionId">Config collection identifier</param>
                    /// <param name="intervalPoints">Number of points in interval</param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        Nullable<int> configCollectionId,
                        Nullable<int> intervalPoints)
                    {
                        string result = SQL;

                        result = result.Replace(
                             oldValue: "@configCollectionId",
                             newValue: Functions.PrepNIntArg(arg: configCollectionId));

                        result = result.Replace(
                            oldValue: "@intervalPoints",
                            newValue: Functions.PrepNIntArg(arg: intervalPoints));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_get_interval_points_app
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="configCollectionId">Config collection identifier</param>
                    /// <param name="intervalPoints">Number of points in interval</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table with intervals for the point layer</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> configCollectionId,
                        Nullable<int> intervalPoints,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(
                            configCollectionId: configCollectionId,
                            intervalPoints: intervalPoints);

                        NpgsqlParameter pConfigCollectionId = new(
                            parameterName: "configCollectionId",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pIntervalPoints = new(
                            parameterName: "intervalPoints",
                            parameterType: NpgsqlDbType.Integer);

                        if (configCollectionId != null)
                        {
                            pConfigCollectionId.Value = (int)configCollectionId;
                        }
                        else
                        {
                            pConfigCollectionId.Value = DBNull.Value;
                        }

                        if (intervalPoints != null)
                        {
                            pIntervalPoints.Value = (int)intervalPoints;
                        }
                        else
                        {
                            pIntervalPoints.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteQuery(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pConfigCollectionId, pIntervalPoints);
                    }

                    /// <summary>
                    /// Execute stored procedure fn_get_interval_points_app
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="configCollectionId">Config collection identifier</param>
                    /// <param name="intervalPoints">Number of points in interval</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Interval list for the point layer</returns>
                    public static TFnGetIntervalPointsAppList Execute(
                        NfiEstaDB database,
                        Nullable<int> configCollectionId,
                        Nullable<int> intervalPoints,
                        NpgsqlTransaction transaction = null)
                    {
                        System.Diagnostics.Stopwatch stopWatch = new();
                        stopWatch.Start();

                        DataTable data = ExecuteQuery(
                            database: database,
                            configCollectionId: configCollectionId,
                            intervalPoints: intervalPoints,
                            transaction: transaction);
                        data.TableName = Name;

                        stopWatch.Stop();

                        return new TFnGetIntervalPointsAppList(
                            database: database,
                            data: data,
                            loadingTime: 0.001 * stopWatch.ElapsedMilliseconds,
                            storedProcedure: CommandText)
                        {
                            ConfigCollectionId = configCollectionId,
                            IntervalPoints = intervalPoints
                        };
                    }

                    #endregion Methods

                }

                #endregion FnGetIntervalPointsApp

            }

        }
    }
}