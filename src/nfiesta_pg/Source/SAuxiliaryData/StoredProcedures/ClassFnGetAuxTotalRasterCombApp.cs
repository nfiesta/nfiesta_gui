﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace AuxiliaryData
        {

            /// <summary>
            /// Stored procedures of schema gisdata
            /// </summary>
            public static partial class ADFunctions
            {
                // fn_get_aux_total_raster_comb_app

                #region FnGetAuxTotalRasterCombApp

                /// <summary>
                /// Wrapper for stored procedure fn_get_aux_total_raster_comb_app.
                /// Funkce vrací vypočítanou hodnotu aux_total_raster_combination pro zadanou konfiguraci (config_id)
                /// a zadaný gid geometrie z tabulky f_a_cell.
                /// </summary>
                public static class FnGetAuxTotalRasterCombApp
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName = ADSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name = "fn_get_aux_total_raster_comb_app";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_config_id integer, ",
                            $"_gid integer; ",
                            $"returns: ",
                            $"double precision");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return String.Concat(
                                $"SELECT{Environment.NewLine}",
                                $"    {SchemaName}.{Name}(@configId, @cellId)::double precision AS {Name};{Environment.NewLine}");
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="configId">Configuration identifier</param>
                    /// <param name="cellId">Estimation cell segment identifier</param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        Nullable<int> configId,
                        Nullable<int> cellId)
                    {
                        string result = SQL;

                        result = result.Replace(
                            oldValue: "@configId",
                            newValue: Functions.PrepNIntArg(arg: configId));

                        result = result.Replace(
                            oldValue: "@cellId",
                            newValue: Functions.PrepNIntArg(arg: cellId));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_get_aux_total_raster_comb_app
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="configId">Configuration identifier</param>
                    /// <param name="cellId">Estimation cell segment identifier</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> configId,
                        Nullable<int> cellId,
                        NpgsqlTransaction transaction = null)
                    {
                        Nullable<double> val = Execute(
                            database: database,
                            configId: configId,
                            cellId: cellId,
                            transaction: transaction);

                        DataTable dt = new();

                        dt.Columns.Add(column: new DataColumn()
                        {
                            ColumnName = Name,
                            DataType = Type.GetType(typeName: "System.Double")
                        });

                        DataRow row = dt.NewRow();

                        Functions.SetNDoubleArg(
                            row: row,
                            name: Name,
                            val: val);

                        dt.Rows.Add(row: row);

                        return dt;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_get_aux_total_raster_comb_app
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="configId">Configuration identifier</param>
                    /// <param name="cellId">Estimation cell segment identifier</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Calculated value of aux_total_raster_combination</returns>
                    public static Nullable<double> Execute(
                        NfiEstaDB database,
                        Nullable<int> configId,
                        Nullable<int> cellId,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(
                            configId: configId,
                            cellId: cellId);

                        NpgsqlParameter pConfigId = new(
                            parameterName: "configId",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pCellId = new(
                            parameterName: "cellId",
                            parameterType: NpgsqlDbType.Integer);

                        if (configId != null)
                        {
                            pConfigId.Value = (int)configId;
                        }
                        else
                        {
                            pConfigId.Value = DBNull.Value;
                        }

                        if (cellId != null)
                        {
                            pCellId.Value = (int)cellId;
                        }
                        else
                        {
                            pCellId.Value = DBNull.Value;
                        }

                        string strResult = database.Postgres.ExecuteScalar(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pConfigId, pCellId);

                        if (String.IsNullOrEmpty(value: strResult))
                        {
                            return null;
                        }
                        else
                        {
                            if (Double.TryParse(s: strResult, result: out double doubleResult))
                            {
                                return doubleResult;
                            }
                            else
                            {
                                return null;
                            }
                        }
                    }

                    #endregion Methods

                }

                #endregion FnGetAuxTotalRasterCombApp

            }

        }
    }
}