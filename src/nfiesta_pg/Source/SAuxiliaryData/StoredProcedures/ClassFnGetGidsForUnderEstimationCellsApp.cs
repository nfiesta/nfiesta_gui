﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace AuxiliaryData
        {

            /// <summary>
            /// Stored procedures of schema gisdata
            /// </summary>
            public static partial class ADFunctions
            {
                // fn_get_gids4under_estimation_cells_app

                #region FnGetGidsForUnderEstimationCellsApp

                /// <summary>
                /// Wrapper for stored procedure fn_get_gids4under_estimation_cells_app.
                /// Funkce pro zadané gidy (druhý vstupní argument funkce _gids), které tvoří estimation_cell,
                /// vrací gidy za všechny podřízené estimation_cell.
                /// Vrácené gidy jsou cizím klíčem na pole gid do tabulky f_a_cell.
                /// </summary>
                public static class FnGetGidsForUnderEstimationCellsApp
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName = ADSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name = "fn_get_gids4under_estimation_cells_app";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_gids4check integer[], ",
                            $"_gids integer[]; ",
                            $"returns: integer[]");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    array_to_string({SchemaName}.{Name}",
                            $"(@cellIdsForCheck, @cellIds),'{(char)59}','{Functions.StrNull}')::text AS {Name};{Environment.NewLine}");
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="cellIdsForCheck">Estimation cell segments for check identifiers</param>
                    /// <param name="cellIds">Estimation cell segments identifiers</param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        List<Nullable<int>> cellIdsForCheck,
                        List<Nullable<int>> cellIds)
                    {
                        string result = SQL;

                        result = result.Replace(
                             oldValue: "@cellIdsForCheck",
                             newValue: Functions.PrepNIntArrayArg(args: cellIdsForCheck, dbType: "int4"));

                        result = result.Replace(
                             oldValue: "@cellIds",
                             newValue: Functions.PrepNIntArrayArg(args: cellIds, dbType: "int4"));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_get_gids4under_estimation_cells_app
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="cellIdsForCheck">Estimation cell segments for check identifiers</param>
                    /// <param name="cellIds">Estimation cell segments identifiers</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        List<Nullable<int>> cellIdsForCheck,
                        List<Nullable<int>> cellIds,
                        NpgsqlTransaction transaction = null)
                    {
                        List<Nullable<int>> list = Execute(
                            database: database,
                            cellIdsForCheck: cellIdsForCheck,
                            cellIds: cellIds,
                            transaction: transaction);

                        DataTable dt = new();

                        dt.Columns.Add(column: new DataColumn()
                        {
                            ColumnName = Name,
                            DataType = Type.GetType(typeName: "System.Int32")
                        });

                        if (list == null)
                        {
                            return dt;
                        }

                        foreach (Nullable<int> val in list)
                        {
                            DataRow row = dt.NewRow();

                            Functions.SetNIntArg(
                                row: row,
                                name: Name,
                                val: val);

                            dt.Rows.Add(row: row);
                        }

                        return dt;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_get_gids4under_estimation_cells_app
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="cellIdsForCheck">Estimation cell segments for check identifiers</param>
                    /// <param name="cellIds">Estimation cell segments identifiers</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns></returns>
                    public static List<Nullable<int>> Execute(
                        NfiEstaDB database,
                        List<Nullable<int>> cellIdsForCheck,
                        List<Nullable<int>> cellIds,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(
                            cellIdsForCheck: cellIdsForCheck,
                            cellIds: cellIds);

                        NpgsqlParameter pCellIdsForCheck = new(
                           parameterName: "cellIdsForCheck",
                           parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        NpgsqlParameter pCellIds = new(
                           parameterName: "cellIds",
                           parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        if (cellIdsForCheck != null)
                        {
                            pCellIdsForCheck.Value = cellIdsForCheck.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pCellIdsForCheck.Value = DBNull.Value;
                        }

                        if (cellIds != null)
                        {
                            pCellIds.Value = cellIds.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pCellIds.Value = DBNull.Value;
                        }

                        string strResult = database.Postgres.ExecuteScalar(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pCellIdsForCheck, pCellIds);

                        if (String.IsNullOrEmpty(value: strResult))
                        {
                            return null;
                        }
                        else
                        {
                            return
                            Functions.StringToNIntList(
                                text: strResult,
                                separator: (char)59,
                                defaultValue: null);
                        }
                    }

                    #endregion Methods

                }

                #endregion FnGetGidsForUnderEstimationCellsApp

            }

        }
    }
}