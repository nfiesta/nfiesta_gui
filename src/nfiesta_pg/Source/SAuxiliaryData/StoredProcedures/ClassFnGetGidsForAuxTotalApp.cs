﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace AuxiliaryData
        {

            /// <summary>
            /// Stored procedures of schema gisdata
            /// </summary>
            public static partial class ADFunctions
            {
                // fn_get_gids4aux_total_app

                #region FnGetGidsForAuxTotalApp

                /// <summary>
                /// Wrapper for stored procedure fn_get_gids4aux_total_app.
                /// Funkce vrací seznam výpočetních údajů pro funkci fn_get_aux_total_app.
                /// </summary>
                public static class FnGetGidsForAuxTotalApp
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName = ADSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name = "fn_get_gids4aux_total_app";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_config_id integer, ",
                            $"_estimation_cell integer[], ",
                            $"_gui_version integer, ",
                            $"_recount boolean DEFAULT false; ",
                            $"returns: ",
                            $"TABLE(",
                            $"step integer, ",
                            $"config_id integer, ",
                            $"estimation_cell integer, ",
                            $"gid integer, ",
                            $"estimation_cell_label character varying, ",
                            $"estimation_cell_description text, ",
                            $"estimation_cell_label_en character varying, ",
                            $"estimation_cell_description_en text)");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    {TFnGetGidsForAuxTotalAppList.ColId.SQL(cols: TFnGetGidsForAuxTotalAppList.Cols, alias: Name)}",
                            $"    {TFnGetGidsForAuxTotalAppList.ColConfigId.SQL(cols: TFnGetGidsForAuxTotalAppList.Cols, alias: Name)}",
                            $"    {TFnGetGidsForAuxTotalAppList.ColStep.SQL(cols: TFnGetGidsForAuxTotalAppList.Cols, alias: Name)}",
                            $"    {TFnGetGidsForAuxTotalAppList.ColEstimationCellId.SQL(cols: TFnGetGidsForAuxTotalAppList.Cols, alias: Name)}",
                            $"    {TFnGetGidsForAuxTotalAppList.ColEstimationCellLabelCs.SQL(cols: TFnGetGidsForAuxTotalAppList.Cols, alias: Name)}",
                            $"    {TFnGetGidsForAuxTotalAppList.ColEstimationCellDescriptionCs.SQL(cols: TFnGetGidsForAuxTotalAppList.Cols, alias: Name)}",
                            $"    {TFnGetGidsForAuxTotalAppList.ColEstimationCellLabelEn.SQL(cols: TFnGetGidsForAuxTotalAppList.Cols, alias: Name)}",
                            $"    {TFnGetGidsForAuxTotalAppList.ColEstimationCellDescriptionEn.SQL(cols: TFnGetGidsForAuxTotalAppList.Cols, alias: Name)}",
                            $"    {TFnGetGidsForAuxTotalAppList.ColCellId.SQL(cols: TFnGetGidsForAuxTotalAppList.Cols, alias: Name)}",
                            $"    @extensionVersionId   AS {TFnGetGidsForAuxTotalAppList.ColExtensionVersionId.Name},{Environment.NewLine}",
                            $"    @guiVersionId         AS {TFnGetGidsForAuxTotalAppList.ColGuiVersionId.Name},{Environment.NewLine}",
                            $"    @recalc               AS {TFnGetGidsForAuxTotalAppList.ColRecalc.Name}{Environment.NewLine}",
                            $"FROM{Environment.NewLine}",
                            $"    {SchemaName}.{Name}(@configId, @estimationCellIds, @guiVersionId, @recalc) AS {Name}{Environment.NewLine}",
                            $"ORDER BY{Environment.NewLine}",
                            $"    {Name}.{TFnGetGidsForAuxTotalAppList.ColConfigId.DbName},{Environment.NewLine}",
                            $"    {Name}.{TFnGetGidsForAuxTotalAppList.ColStep.DbName},{Environment.NewLine}",
                            $"    {Name}.{TFnGetGidsForAuxTotalAppList.ColEstimationCellId.DbName},{Environment.NewLine}",
                            $"    {Name}.{TFnGetGidsForAuxTotalAppList.ColCellId.DbName};{Environment.NewLine}");
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="configId">Configuration identifier</param>
                    /// <param name="estimationCellIds">List of estimation cells identifiers</param>
                    /// <param name="extensionVersionId">Extension version indentifier</param>
                    /// <param name="guiVersionId">Module version indentifier</param>
                    /// <param name="recalc">Recalculate existing auxiliary variable totals in estimation cells</param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        Nullable<int> configId,
                        List<Nullable<int>> estimationCellIds,
                        Nullable<int> extensionVersionId,
                        Nullable<int> guiVersionId,
                        Nullable<bool> recalc)
                    {
                        string result = SQL;

                        result = result.Replace(
                             oldValue: "@configId",
                             newValue: Functions.PrepNIntArg(arg: configId));

                        result = result.Replace(
                             oldValue: "@estimationCellIds",
                             newValue: Functions.PrepNIntArrayArg(args: estimationCellIds, dbType: "int4"));

                        result = result.Replace(
                             oldValue: "@extensionVersionId",
                             newValue: Functions.PrepNIntArg(arg: extensionVersionId));

                        result = result.Replace(
                             oldValue: "@guiVersionId",
                             newValue: Functions.PrepNIntArg(arg: guiVersionId));

                        result = result.Replace(
                             oldValue: "@recalc",
                             newValue: Functions.PrepNBoolArg(arg: recalc));

                        return result;
                    }

                    /// <summary>
                    ///  Execute stored procedure fn_get_gids4aux_total_app
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="configId">Configuration identifier</param>
                    /// <param name="estimationCellIds">List of estimation cells identifiers</param>
                    /// <param name="extensionVersionId">Extension version indentifier</param>
                    /// <param name="guiVersionId">Module version indentifier</param>
                    /// <param name="recalc">Recalculate existing auxiliary variable totals in estimation cells</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table with estimation cell segments for auxiliary variable total calculation</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> configId,
                        List<Nullable<int>> estimationCellIds,
                        Nullable<int> extensionVersionId,
                        Nullable<int> guiVersionId,
                        Nullable<bool> recalc,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(
                            configId: configId,
                            estimationCellIds: estimationCellIds,
                            extensionVersionId: extensionVersionId,
                            guiVersionId: guiVersionId,
                            recalc: recalc);

                        NpgsqlParameter pConfigId = new(
                            parameterName: "configId",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pEstimationCellIds = new(
                            parameterName: "estimationCellIds",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        NpgsqlParameter pExtensionVersionId = new(
                            parameterName: "extensionVersionId",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pGuiVersionId = new(
                            parameterName: "guiVersionId",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pRecalc = new(
                            parameterName: "recalc",
                            parameterType: NpgsqlDbType.Boolean);

                        if (configId != null)
                        {
                            pConfigId.Value = (int)configId;
                        }
                        else
                        {
                            pConfigId.Value = DBNull.Value;
                        }

                        if (estimationCellIds != null)
                        {
                            pEstimationCellIds.Value = estimationCellIds.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pEstimationCellIds.Value = DBNull.Value;
                        }

                        if (extensionVersionId != null)
                        {
                            pExtensionVersionId.Value = (int)extensionVersionId;
                        }
                        else
                        {
                            pExtensionVersionId.Value = DBNull.Value;
                        }

                        if (guiVersionId != null)
                        {
                            pGuiVersionId.Value = (int)guiVersionId;
                        }
                        else
                        {
                            pGuiVersionId.Value = DBNull.Value;
                        }

                        if (recalc != null)
                        {
                            pRecalc.Value = (bool)recalc;
                        }
                        else
                        {
                            pRecalc.Value = DBNull.Value;
                        }

                        return
                            database.Postgres.ExecuteQuery(
                                sqlCommand: SQL,
                                transaction: transaction,
                                pConfigId, pEstimationCellIds, pExtensionVersionId, pGuiVersionId, pRecalc);
                    }

                    /// <summary>
                    /// Execute stored procedure fn_get_gids4aux_total_app
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="configId">Configuration identifier</param>
                    /// <param name="estimationCellIds">List of estimation cells identifiers</param>
                    /// <param name="extensionVersionId">Extension version indentifier</param>
                    /// <param name="guiVersionId">Module version indentifier</param>
                    /// <param name="recalc">Recalculate existing auxiliary variable totals in estimation cells</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>List of estimation cell segments for auxiliary variable total calculation</returns>
                    public static TFnGetGidsForAuxTotalAppList Execute(
                        NfiEstaDB database,
                        Nullable<int> configId,
                        List<Nullable<int>> estimationCellIds,
                        Nullable<int> extensionVersionId,
                        Nullable<int> guiVersionId,
                        Nullable<bool> recalc,
                        NpgsqlTransaction transaction = null)
                    {
                        System.Diagnostics.Stopwatch stopWatch = new();
                        stopWatch.Start();

                        DataTable data = ExecuteQuery(
                            database: database,
                            configId: configId,
                            estimationCellIds: estimationCellIds,
                            extensionVersionId: extensionVersionId,
                            guiVersionId: guiVersionId,
                            recalc: recalc,
                            transaction: transaction);
                        data.TableName = Name;

                        stopWatch.Stop();

                        return new TFnGetGidsForAuxTotalAppList(
                            database: database,
                            data: data,
                            loadingTime: 0.001 * stopWatch.ElapsedMilliseconds,
                            storedProcedure: CommandText)
                        {
                            ConfigId = configId,
                            EstimationCellIds = estimationCellIds,
                            ExtensionVersionId = extensionVersionId,
                            GuiVersionId = guiVersionId,
                            Recalc = recalc
                        };
                    }

                    #endregion Methods

                }

                #endregion FnGetGidsForAuxTotalApp

            }

        }
    }
}