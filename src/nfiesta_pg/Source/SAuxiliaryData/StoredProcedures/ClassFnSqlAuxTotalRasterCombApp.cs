﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace AuxiliaryData
        {

            /// <summary>
            /// Stored procedures of schema gisdata
            /// </summary>
            public static partial class ADFunctions
            {
                // fn_sql_aux_total_raster_comb_app

                #region FnSqlAuxTotalRasterCombApp

                /// <summary>
                /// Wrapper for stored procedure fn_sql_aux_total_raster_comb_app.
                /// Funkce vrací SQL textový řetězec pro výpočet úhrnu pomocné proměnné z rasterové kategorie v rámci vektorové vrstvy.
                /// </summary>
                public static class FnSqlAuxTotalRasterCombApp
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName = ADSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name = "fn_sql_aux_total_raster_comb_app";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"config_id integer, ",
                            $"schema_name character varying, ",
                            $"table_name character varying, ",
                            $"column_name character varying, ",
                            $"band integer, ",
                            $"reclass integer, ",
                            $"condition character varying, ",
                            $"unit double precision, ",
                            $"schema_name_1 character varying, ",
                            $"table_name_1 character varying, ",
                            $"column_name_1 character varying, ",
                            $"band_1 integer, ",
                            $"reclass_1 integer, ",
                            $"condition_1 character varying, ",
                            $"unit_1 double precision, ",
                            $"gid character varying; ",
                            $"returns: text");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    {SchemaName}.{Name}",
                            $"($@configId, @schemaNameA, @tableNameA, @columnNameA, ",
                            $"@bandA, @reclassA, @conditionA, @unitA, ",
                            $"@schemaNameB, @tableNameB, @columnNameB, ",
                            $"@bandB, @reclassB, @conditionB, @unitB, @gid)::text AS {Name};{Environment.NewLine}");
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="configId"></param>
                    /// <param name="schemaNameA"></param>
                    /// <param name="tableNameA"></param>
                    /// <param name="columnNameA"></param>
                    /// <param name="bandA"></param>
                    /// <param name="reclassA"></param>
                    /// <param name="conditionA"></param>
                    /// <param name="unitA"></param>
                    /// <param name="schemaNameB"></param>
                    /// <param name="tableNameB"></param>
                    /// <param name="columnNameB"></param>
                    /// <param name="bandB"></param>
                    /// <param name="reclassB"></param>
                    /// <param name="conditionB"></param>
                    /// <param name="unitB"></param>
                    /// <param name="gid"></param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        Nullable<int> configId,
                        string schemaNameA,
                        string tableNameA,
                        string columnNameA,
                        Nullable<int> bandA,
                        Nullable<int> reclassA,
                        string conditionA,
                        Nullable<double> unitA,
                        string schemaNameB,
                        string tableNameB,
                        string columnNameB,
                        Nullable<int> bandB,
                        Nullable<int> reclassB,
                        string conditionB,
                        Nullable<double> unitB,
                        string gid)
                    {
                        string result = SQL;

                        result = result.Replace(
                            oldValue: "@configId",
                            newValue: Functions.PrepNIntArg(arg: configId));

                        result = result.Replace(
                            oldValue: "@schemaNameA",
                            newValue: Functions.PrepStringArg(arg: schemaNameA));

                        result = result.Replace(
                            oldValue: "@tableNameA",
                            newValue: Functions.PrepStringArg(arg: tableNameA));

                        result = result.Replace(
                            oldValue: "@columnNameA",
                            newValue: Functions.PrepStringArg(arg: columnNameA));

                        result = result.Replace(
                           oldValue: "@bandA",
                           newValue: Functions.PrepNIntArg(arg: bandA));

                        result = result.Replace(
                           oldValue: "@reclassA",
                           newValue: Functions.PrepNIntArg(arg: reclassA));

                        result = result.Replace(
                            oldValue: "@conditionA",
                            newValue: Functions.PrepStringArg(arg: conditionA));

                        result = result.Replace(
                          oldValue: "@unitA",
                          newValue: Functions.PrepNDoubleArg(arg: unitA));

                        result = result.Replace(
                            oldValue: "@schemaNameB",
                            newValue: Functions.PrepStringArg(arg: schemaNameB));

                        result = result.Replace(
                            oldValue: "@tableNameB",
                            newValue: Functions.PrepStringArg(arg: tableNameB));

                        result = result.Replace(
                            oldValue: "@columnNameB",
                            newValue: Functions.PrepStringArg(arg: columnNameB));

                        result = result.Replace(
                           oldValue: "@bandB",
                           newValue: Functions.PrepNIntArg(arg: bandB));

                        result = result.Replace(
                           oldValue: "@reclassB",
                           newValue: Functions.PrepNIntArg(arg: reclassB));

                        result = result.Replace(
                            oldValue: "@conditionB",
                            newValue: Functions.PrepStringArg(arg: conditionB));

                        result = result.Replace(
                          oldValue: "@unitB",
                          newValue: Functions.PrepNDoubleArg(arg: unitB));

                        result = result.Replace(
                            oldValue: "@gid",
                            newValue: Functions.PrepStringArg(arg: gid));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_sql_aux_total_raster_comb_app
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="configId"></param>
                    /// <param name="schemaNameA"></param>
                    /// <param name="tableNameA"></param>
                    /// <param name="columnNameA"></param>
                    /// <param name="bandA"></param>
                    /// <param name="reclassA"></param>
                    /// <param name="conditionA"></param>
                    /// <param name="unitA"></param>
                    /// <param name="schemaNameB"></param>
                    /// <param name="tableNameB"></param>
                    /// <param name="columnNameB"></param>
                    /// <param name="bandB"></param>
                    /// <param name="reclassB"></param>
                    /// <param name="conditionB"></param>
                    /// <param name="unitB"></param>
                    /// <param name="gid"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> configId,
                        string schemaNameA,
                        string tableNameA,
                        string columnNameA,
                        Nullable<int> bandA,
                        Nullable<int> reclassA,
                        string conditionA,
                        Nullable<double> unitA,
                        string schemaNameB,
                        string tableNameB,
                        string columnNameB,
                        Nullable<int> bandB,
                        Nullable<int> reclassB,
                        string conditionB,
                        Nullable<double> unitB,
                        string gid,
                        NpgsqlTransaction transaction = null)
                    {
                        string val = Execute(
                            database: database,
                            configId: configId,
                            schemaNameA: schemaNameA,
                            tableNameA: tableNameA,
                            columnNameA: columnNameA,
                            bandA: bandA,
                            reclassA: reclassA,
                            conditionA: conditionA,
                            unitA: unitA,
                            schemaNameB: schemaNameB,
                            tableNameB: tableNameB,
                            columnNameB: columnNameB,
                            bandB: bandB,
                            reclassB: reclassB,
                            conditionB: conditionB,
                            unitB: unitB,
                            gid: gid,
                            transaction: transaction);

                        DataTable dt = new();

                        dt.Columns.Add(column: new DataColumn()
                        {
                            ColumnName = Name,
                            DataType = Type.GetType(typeName: "System.String")
                        });

                        DataRow row = dt.NewRow();

                        Functions.SetStringArg(
                            row: row,
                            name: Name,
                            val: val);

                        dt.Rows.Add(row: row);

                        return dt;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_sql_aux_total_raster_comb_app
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="configId"></param>
                    /// <param name="schemaNameA"></param>
                    /// <param name="tableNameA"></param>
                    /// <param name="columnNameA"></param>
                    /// <param name="bandA"></param>
                    /// <param name="reclassA"></param>
                    /// <param name="conditionA"></param>
                    /// <param name="unitA"></param>
                    /// <param name="schemaNameB"></param>
                    /// <param name="tableNameB"></param>
                    /// <param name="columnNameB"></param>
                    /// <param name="bandB"></param>
                    /// <param name="reclassB"></param>
                    /// <param name="conditionB"></param>
                    /// <param name="unitB"></param>
                    /// <param name="gid"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Text</returns>
                    public static string Execute(
                        NfiEstaDB database,
                        Nullable<int> configId,
                        string schemaNameA,
                        string tableNameA,
                        string columnNameA,
                        Nullable<int> bandA,
                        Nullable<int> reclassA,
                        string conditionA,
                        Nullable<double> unitA,
                        string schemaNameB,
                        string tableNameB,
                        string columnNameB,
                        Nullable<int> bandB,
                        Nullable<int> reclassB,
                        string conditionB,
                        Nullable<double> unitB,
                        string gid,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(
                            configId: configId,
                            schemaNameA: schemaNameA,
                            tableNameA: tableNameA,
                            columnNameA: columnNameA,
                            bandA: bandA,
                            reclassA: reclassA,
                            conditionA: conditionA,
                            unitA: unitA,
                            schemaNameB: schemaNameB,
                            tableNameB: tableNameB,
                            columnNameB: columnNameB,
                            bandB: bandB,
                            reclassB: reclassB,
                            conditionB: conditionB,
                            unitB: unitB,
                            gid: gid);

                        NpgsqlParameter pConfigId = new(
                            parameterName: "configId",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pSchemaNameA = new(
                            parameterName: "schemaNameA",
                            parameterType: NpgsqlDbType.Varchar);

                        NpgsqlParameter pTableNameA = new(
                            parameterName: "tableNameA",
                            parameterType: NpgsqlDbType.Varchar);

                        NpgsqlParameter pColumnNameA = new(
                           parameterName: "columnNameA",
                           parameterType: NpgsqlDbType.Varchar);

                        NpgsqlParameter pBandA = new(
                            parameterName: "bandA",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pReclassA = new(
                            parameterName: "reclassA",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pConditionA = new(
                           parameterName: "conditionA",
                           parameterType: NpgsqlDbType.Varchar);

                        NpgsqlParameter pUnitA = new(
                            parameterName: "unitA",
                            parameterType: NpgsqlDbType.Double);

                        NpgsqlParameter pSchemaNameB = new(
                            parameterName: "schemaNameB",
                            parameterType: NpgsqlDbType.Varchar);

                        NpgsqlParameter pTableNameB = new(
                            parameterName: "tableNameB",
                            parameterType: NpgsqlDbType.Varchar);

                        NpgsqlParameter pColumnNameB = new(
                           parameterName: "columnNameB",
                           parameterType: NpgsqlDbType.Varchar);

                        NpgsqlParameter pBandB = new(
                            parameterName: "bandB",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pReclassB = new(
                            parameterName: "reclassB",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pConditionB = new(
                           parameterName: "conditionB",
                           parameterType: NpgsqlDbType.Varchar);

                        NpgsqlParameter pUnitB = new(
                            parameterName: "unitB",
                            parameterType: NpgsqlDbType.Double);

                        NpgsqlParameter pGid = new(
                            parameterName: "gid",
                            parameterType: NpgsqlDbType.Varchar);

                        if (configId != null)
                        {
                            pConfigId.Value = (int)configId;
                        }
                        else
                        {
                            pConfigId.Value = DBNull.Value;
                        }

                        if (schemaNameA != null)
                        {
                            pSchemaNameA.Value = schemaNameA;
                        }
                        else
                        {
                            pSchemaNameA.Value = DBNull.Value;
                        }

                        if (tableNameA != null)
                        {
                            pTableNameA.Value = tableNameA;
                        }
                        else
                        {
                            pTableNameA.Value = DBNull.Value;
                        }

                        if (columnNameA != null)
                        {
                            pColumnNameA.Value = columnNameA;
                        }
                        else
                        {
                            pColumnNameA.Value = DBNull.Value;
                        }

                        if (bandA != null)
                        {
                            pBandA.Value = (int)bandA;
                        }
                        else
                        {
                            pBandA.Value = DBNull.Value;
                        }

                        if (reclassA != null)
                        {
                            pReclassA.Value = (int)reclassA;
                        }
                        else
                        {
                            pReclassA.Value = DBNull.Value;
                        }

                        if (conditionA != null)
                        {
                            pConditionA.Value = conditionA;
                        }
                        else
                        {
                            pConditionA.Value = DBNull.Value;
                        }

                        if (unitA != null)
                        {
                            pUnitA.Value = (double)unitA;
                        }
                        else
                        {
                            pUnitA.Value = DBNull.Value;
                        }

                        if (schemaNameB != null)
                        {
                            pSchemaNameB.Value = schemaNameB;
                        }
                        else
                        {
                            pSchemaNameB.Value = DBNull.Value;
                        }

                        if (tableNameB != null)
                        {
                            pTableNameB.Value = tableNameB;
                        }
                        else
                        {
                            pTableNameB.Value = DBNull.Value;
                        }

                        if (columnNameB != null)
                        {
                            pColumnNameB.Value = columnNameB;
                        }
                        else
                        {
                            pColumnNameB.Value = DBNull.Value;
                        }

                        if (bandB != null)
                        {
                            pBandB.Value = (int)bandB;
                        }
                        else
                        {
                            pBandB.Value = DBNull.Value;
                        }

                        if (reclassB != null)
                        {
                            pReclassB.Value = (int)reclassB;
                        }
                        else
                        {
                            pReclassB.Value = DBNull.Value;
                        }

                        if (conditionB != null)
                        {
                            pConditionB.Value = conditionB;
                        }
                        else
                        {
                            pConditionB.Value = DBNull.Value;
                        }

                        if (unitB != null)
                        {
                            pUnitB.Value = (double)unitB;
                        }
                        else
                        {
                            pUnitB.Value = DBNull.Value;
                        }

                        if (gid != null)
                        {
                            pGid.Value = gid;
                        }
                        else
                        {
                            pGid.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteScalar(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pConfigId,
                            pSchemaNameA, pTableNameA, pColumnNameA,
                            pBandA, pReclassA, pConditionA, pUnitA,
                            pSchemaNameB, pTableNameB, pColumnNameB,
                            pBandB, pReclassB, pConditionB, pUnitB,
                            pGid);
                    }

                    #endregion Methods

                }

                #endregion FnSqlAuxTotalRasterCombApp

            }

        }
    }
}