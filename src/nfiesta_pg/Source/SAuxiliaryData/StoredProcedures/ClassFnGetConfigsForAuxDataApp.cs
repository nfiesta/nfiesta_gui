﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace AuxiliaryData
        {

            /// <summary>
            /// Stored procedures of schema gisdata
            /// </summary>
            public static partial class ADFunctions
            {
                // fn_get_configs4aux_data_app

                #region FnGetConfigsForAuxDataApp

                /// <summary>
                /// Wrapper for stored procedure fn_get_configs4aux_data_app.
                /// Function returns list of configurations for function fn_get_aux_data_app.
                /// </summary>
                public static class FnGetConfigsForAuxDataApp
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName = ADSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name = "fn_get_configs4aux_data_app";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_config_collection integer, ",
                            $"_gui_version integer, ",
                            $"_interval_points integer DEFAULT 0, ",
                            $"_recount boolean DEFAULT false; ",
                            $"returns: ",
                            $"TABLE(",
                            $"step integer, ",
                            $"config_collection integer, ",
                            $"config integer, ",
                            $"intersects boolean, ",
                            $"step4interval integer, ",
                            $"gid_start integer, ",
                            $"gid_end integer)");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    {TFnGetConfigsForAuxDataAppList.ColId.SQL(cols: TFnGetConfigsForAuxDataAppList.Cols, alias: Name)}",
                            $"    {TFnGetConfigsForAuxDataAppList.ColStep.SQL(cols: TFnGetConfigsForAuxDataAppList.Cols, alias: Name)}",
                            $"    {TFnGetConfigsForAuxDataAppList.ColConfigCollectionId.SQL(cols: TFnGetConfigsForAuxDataAppList.Cols, alias: Name)}",
                            $"    {TFnGetConfigsForAuxDataAppList.ColConfigId.SQL(cols: TFnGetConfigsForAuxDataAppList.Cols, alias: Name)}",
                            $"    {TFnGetConfigsForAuxDataAppList.ColIntersects.SQL(cols: TFnGetConfigsForAuxDataAppList.Cols, alias: Name)}",
                            $"    {TFnGetConfigsForAuxDataAppList.ColStepForInterval.SQL(cols: TFnGetConfigsForAuxDataAppList.Cols, alias: Name)}",
                            $"    {TFnGetConfigsForAuxDataAppList.ColGidStart.SQL(cols: TFnGetConfigsForAuxDataAppList.Cols, alias: Name)}",
                            $"    {TFnGetConfigsForAuxDataAppList.ColGidEnd.SQL(cols: TFnGetConfigsForAuxDataAppList.Cols, alias: Name, isLastOne: true)}",
                            $"FROM{Environment.NewLine}",
                            $"    {SchemaName}.{Name}(@configCollectionId, @guiVersionId, @intervalPoints, @recalc) AS {Name} {Environment.NewLine}",
                            $"ORDER BY{Environment.NewLine}",
                            $"    {Name}.{TFnGetConfigsForAuxDataAppList.ColConfigCollectionId.DbName},{Environment.NewLine}",
                            $"    {Name}.{TFnGetConfigsForAuxDataAppList.ColConfigId.DbName},{Environment.NewLine}",
                            $"    {Name}.{TFnGetConfigsForAuxDataAppList.ColStep.DbName};{Environment.NewLine}"
                            );
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="configCollectionId">Configuration collection identifier</param>
                    /// <param name="guiVersionId">Module version identifier</param>
                    /// <param name="intervalPoints">Interval points</param>
                    /// <param name="recalc">Recalculate existing values</param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        Nullable<int> configCollectionId,
                        Nullable<int> guiVersionId,
                        Nullable<int> intervalPoints,
                        Nullable<bool> recalc)
                    {
                        string result = SQL;

                        result = result.Replace(
                            oldValue: "@configCollectionId",
                            newValue: Functions.PrepNIntArg(arg: configCollectionId));

                        result = result.Replace(
                             oldValue: "@guiVersionId",
                             newValue: Functions.PrepNIntArg(arg: guiVersionId));

                        result = result.Replace(
                            oldValue: "@intervalPoints",
                            newValue: Functions.PrepNIntArg(arg: intervalPoints));

                        result = result.Replace(
                            oldValue: "@recalc",
                            newValue: Functions.PrepNBoolArg(arg: recalc));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_get_configs4aux_data_app
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="configCollectionId">Configuration collection identifier</param>
                    /// <param name="guiVersionId">Module version identifier</param>
                    /// <param name="intervalPoints">Interval points</param>
                    /// <param name="recalc">Recalculate existing values</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> configCollectionId,
                        Nullable<int> guiVersionId,
                        Nullable<int> intervalPoints,
                        Nullable<bool> recalc,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(
                            configCollectionId: configCollectionId,
                            guiVersionId: guiVersionId,
                            intervalPoints: intervalPoints,
                            recalc: recalc);

                        NpgsqlParameter pConfigCollectionId = new(
                            parameterName: "configCollectionId",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pGuiVersionId = new(
                            parameterName: "guiVersionId",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pIntervalPoints = new(
                          parameterName: "intervalPoints",
                          parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pRecalc = new(
                            parameterName: "recalc",
                            parameterType: NpgsqlDbType.Boolean);

                        if (configCollectionId != null)
                        {
                            pConfigCollectionId.Value = (int)configCollectionId;
                        }
                        else
                        {
                            pConfigCollectionId.Value = DBNull.Value;
                        }

                        if (guiVersionId != null)
                        {
                            pGuiVersionId.Value = (int)guiVersionId;
                        }
                        else
                        {
                            pGuiVersionId.Value = DBNull.Value;
                        }

                        if (intervalPoints != null)
                        {
                            pIntervalPoints.Value = (int)intervalPoints;
                        }
                        else
                        {
                            pIntervalPoints.Value = DBNull.Value;
                        }

                        if (recalc != null)
                        {
                            pRecalc.Value = (bool)recalc;
                        }
                        else
                        {
                            pRecalc.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteQuery(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pConfigCollectionId, pGuiVersionId, pIntervalPoints, pRecalc);
                    }

                    /// <summary>
                    /// Execute stored procedure fn_get_configs4aux_data_app
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="configCollectionId">Configuration collection identifier</param>
                    /// <param name="guiVersionId">Module version identifier</param>
                    /// <param name="intervalPoints">Interval points</param>
                    /// <param name="recalc">Recalculate existing values</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>List of configurations for function fn_get_aux_data_app</returns>
                    public static TFnGetConfigsForAuxDataAppList Execute(
                        NfiEstaDB database,
                        Nullable<int> configCollectionId,
                        Nullable<int> guiVersionId,
                        Nullable<int> intervalPoints,
                        Nullable<bool> recalc,
                        NpgsqlTransaction transaction = null)
                    {
                        System.Diagnostics.Stopwatch stopWatch = new();
                        stopWatch.Start();

                        DataTable data = ExecuteQuery(
                            database: database,
                            configCollectionId: configCollectionId,
                            guiVersionId: guiVersionId,
                            intervalPoints: intervalPoints,
                            recalc: recalc,
                            transaction: transaction);
                        data.TableName = Name;

                        stopWatch.Stop();

                        return new TFnGetConfigsForAuxDataAppList(
                            database: database,
                            data: data,
                            loadingTime: 0.001 * stopWatch.ElapsedMilliseconds,
                            storedProcedure: CommandText)
                        {
                            ConfigCollectionId = configCollectionId,
                            GuiVersionId = guiVersionId,
                            IntervalPoints = intervalPoints,
                            Recalc = recalc
                        };
                    }

                    #endregion Methods

                }

                #endregion FnGetConfigsForAuxDataApp

            }

        }
    }
}