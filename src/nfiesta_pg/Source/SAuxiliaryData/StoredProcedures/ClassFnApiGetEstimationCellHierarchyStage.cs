﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace AuxiliaryData
        {

            /// <summary>
            /// Stored procedures of schema gisdata
            /// </summary>
            public static partial class ADFunctions
            {
                // fn_api_get_estimation_cell_hierarchy_stage

                #region FnApiGetEstimationCellHierarchyStage

                /// <summary>
                /// Wrapper for stored procedure fn_api_get_estimation_cell_hierarchy_stage.
                /// Funkce vraci tabulku hierarchii cell z tabulky t_estimation_cell_hierarchy
                /// s identifikaci stage [null = pro celu nebolo dopusud nic pocitano, true = cela je vypocitana, false = cela je rozpracovana].
                /// Ve vystupu jsou cely, ktere maji v tabulce c_estimation_cell_collection nastaveno use4estimates = TRUE.
                /// </summary>
                public static class FnApiGetEstimationCellHierarchyStage
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName = ADSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name = "fn_api_get_estimation_cell_hierarchy_stage";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_config_collection integer; ",
                            $"returns: ",
                            $"TABLE(",
                            $"id integer, ",
                            $"estimation_cell_id integer, ",
                            $"stage boolean)");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    {TFnApiGetEstimationCellHierarchyStageList.ColId.SQL(cols: TFnApiGetEstimationCellHierarchyStageList.Cols, alias: Name)}",
                            $"    {TFnApiGetEstimationCellHierarchyStageList.ColEstimationCellId.SQL(cols: TFnApiGetEstimationCellHierarchyStageList.Cols, alias: Name)}",
                            $"    {TFnApiGetEstimationCellHierarchyStageList.ColStage.SQL(cols: TFnApiGetEstimationCellHierarchyStageList.Cols, alias: Name, isLastOne: true)}",
                            $"FROM{Environment.NewLine}",
                            $"    {SchemaName}.{Name}(@configCollectionId) AS {Name} {Environment.NewLine}",
                            $"ORDER BY{Environment.NewLine}",
                            $"    {Name}.{TFnApiGetEstimationCellHierarchyStageList.ColEstimationCellId.DbName};{Environment.NewLine}"
                            );
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="configCollectionId">Configuration collection identifier</param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(Nullable<int> configCollectionId)
                    {
                        string result = SQL;

                        result = result.Replace(
                            oldValue: "@configCollectionId",
                            newValue: Functions.PrepNIntArg(arg: configCollectionId));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_api_get_estimation_cell_hierarchy_stage
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="configCollectionId">Configuration collection identifier</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table of states of the calculation of the auxiliary variable totals in the estimation cells</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> configCollectionId,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(configCollectionId: configCollectionId);

                        NpgsqlParameter pConfigCollectionId = new(
                            parameterName: "configCollectionId",
                            parameterType: NpgsqlDbType.Integer);

                        if (configCollectionId != null)
                        {
                            pConfigCollectionId.Value = (int)configCollectionId;
                        }
                        else
                        {
                            pConfigCollectionId.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteQuery(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pConfigCollectionId);
                    }

                    /// <summary>
                    /// Execute stored procedure fn_api_get_estimation_cell_hierarchy_stage
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="configCollectionId">Configuration collection identifier</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>List of states of the calculation of the auxiliary variable totals in the estimation cells</returns>
                    public static TFnApiGetEstimationCellHierarchyStageList Execute(
                        NfiEstaDB database,
                        Nullable<int> configCollectionId,
                        NpgsqlTransaction transaction = null)
                    {
                        System.Diagnostics.Stopwatch stopWatch = new();
                        stopWatch.Start();

                        DataTable data = ExecuteQuery(
                            database: database,
                            configCollectionId: configCollectionId,
                            transaction: transaction);
                        data.TableName = Name;

                        stopWatch.Stop();

                        return new TFnApiGetEstimationCellHierarchyStageList(
                            database: database,
                            data: data,
                            loadingTime: 0.001 * stopWatch.ElapsedMilliseconds,
                            storedProcedure: CommandText)
                        {
                            ConfigCollectionId = configCollectionId
                        };
                    }

                    #endregion Methods

                }

                #endregion FnApiGetEstimationCellHierarchyStage

            }

        }
    }
}