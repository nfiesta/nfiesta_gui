﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace AuxiliaryData
        {

            /// <summary>
            /// Stored procedures of schema gisdata
            /// </summary>
            public static partial class ADFunctions
            {
                // fn_sql_aux_total_vector_app

                #region FnSqlAuxTotalVectorApp

                /// <summary>
                /// Wrapper for stored procedure fn_sql_aux_total_vector_app.
                /// Funkce vrací SQL textový řetězec pro výpočet úhrnu pomocné proměnné z vektorové vrstvy.
                /// </summary>
                public static class FnSqlAuxTotalVectorApp
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName = ADSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name = "fn_sql_aux_total_vector_app";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"config_id integer, ",
                            $"schema_name character varying, ",
                            $"table_name character varying, ",
                            $"column_name character varying, ",
                            $"condition character varying, ",
                            $"unit double precision, ",
                            $"gid character varying; ",
                            $"returns: text");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    {SchemaName}.{Name}",
                            $"(@configId, @schemaName, @tableName, @columnName, @condition, @unit, @gid)::text AS {Name};{Environment.NewLine}");
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="configId"></param>
                    /// <param name="schemaName"></param>
                    /// <param name="tableName"></param>
                    /// <param name="columnName"></param>
                    /// <param name="condition"></param>
                    /// <param name="unit"></param>
                    /// <param name="gid"></param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        Nullable<int> configId,
                        string schemaName,
                        string tableName,
                        string columnName,
                        string condition,
                        Nullable<double> unit,
                        string gid)
                    {
                        string result = SQL;

                        result = result.Replace(
                            oldValue: "@configId",
                            newValue: Functions.PrepNIntArg(arg: configId));

                        result = result.Replace(
                            oldValue: "@schemaName",
                            newValue: Functions.PrepStringArg(arg: schemaName));

                        result = result.Replace(
                            oldValue: "@tableName",
                            newValue: Functions.PrepStringArg(arg: tableName));

                        result = result.Replace(
                            oldValue: "@columnName",
                            newValue: Functions.PrepStringArg(arg: columnName));

                        result = result.Replace(
                            oldValue: "@condition",
                            newValue: Functions.PrepStringArg(arg: condition));

                        result = result.Replace(
                          oldValue: "@unit",
                          newValue: Functions.PrepNDoubleArg(arg: unit));

                        result = result.Replace(
                            oldValue: "@gid",
                            newValue: Functions.PrepStringArg(arg: gid));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_sql_aux_total_vector_app
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="configId"></param>
                    /// <param name="schemaName"></param>
                    /// <param name="tableName"></param>
                    /// <param name="columnName"></param>
                    /// <param name="condition"></param>
                    /// <param name="unit"></param>
                    /// <param name="gid"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> configId,
                        string schemaName,
                        string tableName,
                        string columnName,
                        string condition,
                        Nullable<double> unit,
                        string gid,
                        NpgsqlTransaction transaction = null)
                    {
                        string val = Execute(
                            database: database,
                            configId: configId,
                            schemaName: schemaName,
                            tableName: tableName,
                            columnName: columnName,
                            condition: condition,
                            unit: unit,
                            gid: gid,
                            transaction: transaction);

                        DataTable dt = new();

                        dt.Columns.Add(column: new DataColumn()
                        {
                            ColumnName = Name,
                            DataType = Type.GetType(typeName: "System.String")
                        });

                        DataRow row = dt.NewRow();

                        Functions.SetStringArg(
                            row: row,
                            name: Name,
                            val: val);

                        dt.Rows.Add(row: row);

                        return dt;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_sql_aux_total_vector_app
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="configId"></param>
                    /// <param name="schemaName"></param>
                    /// <param name="tableName"></param>
                    /// <param name="columnName"></param>
                    /// <param name="condition"></param>
                    /// <param name="unit"></param>
                    /// <param name="gid"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Text</returns>
                    public static string Execute(
                        NfiEstaDB database,
                        Nullable<int> configId,
                        string schemaName,
                        string tableName,
                        string columnName,
                        string condition,
                        Nullable<double> unit,
                        string gid,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(
                            configId: configId,
                            schemaName: schemaName,
                            tableName: tableName,
                            columnName: columnName,
                            condition: condition,
                            unit: unit,
                            gid: gid);

                        NpgsqlParameter pConfigId = new(
                            parameterName: "configId",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pSchemaName = new(
                            parameterName: "schemaName",
                            parameterType: NpgsqlDbType.Varchar);

                        NpgsqlParameter pTableName = new(
                            parameterName: "tableName",
                            parameterType: NpgsqlDbType.Varchar);

                        NpgsqlParameter pColumnName = new(
                           parameterName: "columnName",
                           parameterType: NpgsqlDbType.Varchar);

                        NpgsqlParameter pCondition = new(
                           parameterName: "condition",
                           parameterType: NpgsqlDbType.Varchar);

                        NpgsqlParameter pUnit = new(
                            parameterName: "unit",
                            parameterType: NpgsqlDbType.Double);

                        NpgsqlParameter pGid = new(
                            parameterName: "gid",
                            parameterType: NpgsqlDbType.Varchar);

                        if (configId != null)
                        {
                            pConfigId.Value = (int)configId;
                        }
                        else
                        {
                            pConfigId.Value = DBNull.Value;
                        }

                        if (schemaName != null)
                        {
                            pSchemaName.Value = schemaName;
                        }
                        else
                        {
                            pSchemaName.Value = DBNull.Value;
                        }

                        if (tableName != null)
                        {
                            pTableName.Value = tableName;
                        }
                        else
                        {
                            pTableName.Value = DBNull.Value;
                        }

                        if (columnName != null)
                        {
                            pColumnName.Value = columnName;
                        }
                        else
                        {
                            pColumnName.Value = DBNull.Value;
                        }

                        if (condition != null)
                        {
                            pCondition.Value = condition;
                        }
                        else
                        {
                            pCondition.Value = DBNull.Value;
                        }

                        if (unit != null)
                        {
                            pUnit.Value = (double)unit;
                        }
                        else
                        {
                            pUnit.Value = DBNull.Value;
                        }

                        if (gid != null)
                        {
                            pGid.Value = gid;
                        }
                        else
                        {
                            pGid.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteScalar(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pConfigId, pSchemaName, pTableName, pColumnName,
                            pCondition, pUnit, pGid);
                    }

                    #endregion Methods

                }

                #endregion FnSqlAuxTotalVectorApp

            }

        }
    }
}