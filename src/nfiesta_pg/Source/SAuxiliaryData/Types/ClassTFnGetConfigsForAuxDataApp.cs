﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace AuxiliaryData
        {
            // fn_get_configs4aux_data_app

            /// <summary>
            /// Configuration for function fn_get_aux_data_app
            /// (return type of the stored procedure fn_get_configs4aux_data_app)
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class TFnGetConfigsForAuxDataApp(
                TFnGetConfigsForAuxDataAppList composite = null,
                DataRow data = null)
                    : AParametrizedViewEntry<TFnGetConfigsForAuxDataAppList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TFnGetConfigsForAuxDataAppList.ColId.Name,
                            defaultValue: Int32.Parse(s: TFnGetConfigsForAuxDataAppList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TFnGetConfigsForAuxDataAppList.ColId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Step
                /// </summary>
                public Nullable<int> Step
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TFnGetConfigsForAuxDataAppList.ColStep.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnGetConfigsForAuxDataAppList.ColStep.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TFnGetConfigsForAuxDataAppList.ColStep.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TFnGetConfigsForAuxDataAppList.ColStep.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Configuration collection identifier
                /// </summary>
                public Nullable<int> ConfigCollectionId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TFnGetConfigsForAuxDataAppList.ColConfigCollectionId.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnGetConfigsForAuxDataAppList.ColConfigCollectionId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TFnGetConfigsForAuxDataAppList.ColConfigCollectionId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TFnGetConfigsForAuxDataAppList.ColConfigCollectionId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Configuration collection object (read-only)
                /// </summary>
                public ConfigCollection ConfigCollection
                {
                    get
                    {
                        return
                            (ConfigCollectionId != null)
                                ? ((NfiEstaDB)Composite.Database)
                                    .SAuxiliaryData.TConfigCollection[(int)ConfigCollectionId]
                                : null;
                    }
                }


                /// <summary>
                /// Configuration identifier
                /// </summary>
                public Nullable<int> ConfigId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TFnGetConfigsForAuxDataAppList.ColConfigId.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnGetConfigsForAuxDataAppList.ColConfigId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TFnGetConfigsForAuxDataAppList.ColConfigId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TFnGetConfigsForAuxDataAppList.ColConfigId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Configuration object (read-only)
                /// </summary>
                public Config Config
                {
                    get
                    {
                        return
                            (ConfigId != null)
                                ? ((NfiEstaDB)Composite.Database)
                                    .SAuxiliaryData.TConfig[(int)ConfigId]
                                : null;
                    }
                }


                /// <summary>
                /// Intersects
                /// </summary>
                public Nullable<bool> Intersects
                {
                    get
                    {
                        return Functions.GetNBoolArg(
                            row: Data,
                            name: TFnGetConfigsForAuxDataAppList.ColIntersects.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnGetConfigsForAuxDataAppList.ColIntersects.DefaultValue)
                                ? (Nullable<bool>)null
                                : Boolean.Parse(value: TFnGetConfigsForAuxDataAppList.ColIntersects.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNBoolArg(
                            row: Data,
                            name: TFnGetConfigsForAuxDataAppList.ColIntersects.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Step for interval
                /// </summary>
                public Nullable<int> StepForInterval
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TFnGetConfigsForAuxDataAppList.ColStepForInterval.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnGetConfigsForAuxDataAppList.ColStepForInterval.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TFnGetConfigsForAuxDataAppList.ColStepForInterval.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TFnGetConfigsForAuxDataAppList.ColStepForInterval.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Gid start
                /// </summary>
                public Nullable<int> GidStart
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TFnGetConfigsForAuxDataAppList.ColGidStart.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnGetConfigsForAuxDataAppList.ColGidStart.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TFnGetConfigsForAuxDataAppList.ColGidStart.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TFnGetConfigsForAuxDataAppList.ColGidStart.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Gid end
                /// </summary>
                public Nullable<int> GidEnd
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TFnGetConfigsForAuxDataAppList.ColGidEnd.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnGetConfigsForAuxDataAppList.ColGidEnd.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TFnGetConfigsForAuxDataAppList.ColGidEnd.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TFnGetConfigsForAuxDataAppList.ColGidEnd.Name,
                            val: value);
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true|false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not TFnGetConfigsForAuxDataApp, return False
                    if (obj is not TFnGetConfigsForAuxDataApp)
                    {
                        return false;
                    }

                    return
                        Id == ((TFnGetConfigsForAuxDataApp)obj).Id;
                }

                /// <summary>
                /// Returns a hash code for the current object
                /// </summary>
                /// <returns>Hash code for the current object</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                /// <summary>
                /// Returns a string that represents the current object
                /// </summary>
                /// <returns>String that represents the current object</returns>
                public override string ToString()
                {
                    return String.Concat(
                        $"{nameof(TFnGetConfigsForAuxDataApp)}: {{",
                        $"{nameof(Id)}: {Functions.PrepIntArg(arg: Id)}; ",
                        $"{nameof(Step)}: {Functions.PrepNIntArg(arg: Step)}; ",
                        $"{nameof(ConfigCollectionId)}: {Functions.PrepNIntArg(arg: ConfigCollectionId)}; ",
                        $"{nameof(ConfigId)}: {Functions.PrepNIntArg(arg: ConfigId)}; ",
                        $"{nameof(Intersects)}: {Functions.PrepNBoolArg(arg: Intersects)}; ",
                        $"{nameof(StepForInterval)}: {Functions.PrepNIntArg(arg: StepForInterval)}; ",
                        $"{nameof(GidStart)}: {Functions.PrepNIntArg(arg: GidStart)}; ",
                        $"{nameof(GidEnd)}: {Functions.PrepNIntArg(arg: GidEnd)}}}");
                }

                #endregion Methods

            }


            /// <summary>
            /// List of configurations for function fn_get_aux_data_app
            /// (return type of the stored procedure fn_get_configs4aux_data_app)
            /// </summary>
            public class TFnGetConfigsForAuxDataAppList
                 : AParametrizedView
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema =
                    ADFunctions.FnGetConfigsForAuxDataApp.SchemaName;

                /// <summary>
                /// Stored procedure name
                /// </summary>
                public static readonly string Name =
                    ADFunctions.FnGetConfigsForAuxDataApp.Name;

                /// <summary>
                /// Stored procedure alias
                /// </summary>
                public static readonly string Alias =
                    ADFunctions.FnGetConfigsForAuxDataApp.Name;

                /// <summary>
                /// Description in national language
                /// </summary>
                public static readonly string CaptionCs = String.Concat(
                    "Seznam konfigurací pro funkci  fn_get_aux_data_app ",
                    "(návratový typ uložené procedury fn_get_configs4aux_data_app)");

                /// <summary>
                /// Description in English
                /// </summary>
                public static readonly string CaptionEn = String.Concat(
                    "List of configurations for function fn_get_aux_data_app ",
                    "(return type of the stored procedure fn_get_configs4aux_data_app)");

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
                {
                    { "id", new ColumnMetadata()
                    {
                        Name = "id",
                        DbName = "id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = true,
                        NotNull = true,
                        DefaultValue = default(int).ToString(),
                        FuncCall = "row_number() over()",
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ID",
                        HeaderTextEn = "ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 0
                    }
                    },
                    { "step", new ColumnMetadata()
                    {
                        Name = "step",
                        DbName = "step",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "STEP",
                        HeaderTextEn = "STEP",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 1
                    }
                    },
                    { "config_collection_id", new ColumnMetadata()
                    {
                        Name = "config_collection_id",
                        DbName = "config_collection",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "CONFIG_COLLECTION_ID",
                        HeaderTextEn = "CONFIG_COLLECTION_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 2
                    }
                    },
                    { "config_id", new ColumnMetadata()
                    {
                        Name = "config_id",
                        DbName = "config",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "CONFIG_ID",
                        HeaderTextEn = "CONFIG_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 3
                    }
                    },
                    { "intersects", new ColumnMetadata()
                    {
                        Name = "intersects",
                        DbName = "intersects",
                        DataType = "System.Boolean",
                        DbDataType = "bool",
                        NewDataType = "bool",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "INTERSECTS",
                        HeaderTextEn = "INTERSECTS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 4
                    }
                    },
                    { "step4interval", new ColumnMetadata()
                    {
                        Name = "step4interval",
                        DbName = "step4interval",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "STEP_FOR_INTERVAL",
                        HeaderTextEn = "STEP_FOR_INTERVAL",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 5
                    }
                    },
                    { "gid_start", new ColumnMetadata()
                    {
                        Name = "gid_start",
                        DbName = "gid_start",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "GID_START",
                        HeaderTextEn = "GID_START",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 6
                    }
                    },
                    { "gid_end", new ColumnMetadata()
                    {
                        Name = "gid_end",
                        DbName = "gid_end",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "GID_END",
                        HeaderTextEn = "GID_END",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 7
                    }
                    }
                };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column step metadata
                /// </summary>
                public static readonly ColumnMetadata ColStep = Cols["step"];

                /// <summary>
                /// Column config_collection_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColConfigCollectionId = Cols["config_collection_id"];

                /// <summary>
                /// Column config_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColConfigId = Cols["config_id"];

                /// <summary>
                /// Column intersects metadata
                /// </summary>
                public static readonly ColumnMetadata ColIntersects = Cols["intersects"];

                /// <summary>
                /// Column step4interval metadata
                /// </summary>
                public static readonly ColumnMetadata ColStepForInterval = Cols["step4interval"];

                /// <summary>
                /// Column gid_start metadata
                /// </summary>
                public static readonly ColumnMetadata ColGidStart = Cols["gid_start"];

                /// <summary>
                /// Column gid_end metadata
                /// </summary>
                public static readonly ColumnMetadata ColGidEnd = Cols["gid_end"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Private Fields

                /// <summary>
                /// 1st parameter: Configuration collection identifier
                /// </summary>
                private Nullable<int> configCollectionId;

                /// <summary>
                /// 2nd parameter: Module version identifier
                /// </summary>
                private Nullable<int> guiVersionId;

                /// <summary>
                /// 3rd parameter: Interval points
                /// </summary>
                private Nullable<int> intervalPoints;

                /// <summary>
                /// 4th parameter: Recalculate existing values
                /// </summary>
                private Nullable<bool> recalc;

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TFnGetConfigsForAuxDataAppList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                {
                    ConfigCollectionId = null;
                    GuiVersionId = null;
                    IntervalPoints = null;
                    Recalc = null;
                }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TFnGetConfigsForAuxDataAppList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                {
                    ConfigCollectionId = null;
                    GuiVersionId = null;
                    IntervalPoints = null;
                    Recalc = null;
                }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// 1st parameter: Configuration collection identifier
                /// </summary>
                public Nullable<int> ConfigCollectionId
                {
                    get
                    {
                        return configCollectionId;
                    }
                    set
                    {
                        configCollectionId = value;
                    }
                }

                /// <summary>
                /// 2nd parameter: Module version identifier
                /// </summary>
                public Nullable<int> GuiVersionId
                {
                    get
                    {
                        return guiVersionId;
                    }
                    set
                    {
                        guiVersionId = value;
                    }
                }

                /// <summary>
                /// 3rd parameter: Interval points
                /// </summary>
                public Nullable<int> IntervalPoints
                {
                    get
                    {
                        return intervalPoints;
                    }
                    set
                    {
                        intervalPoints = value;
                    }
                }

                /// <summary>
                /// 4th parameter: Recalculate existing values
                /// </summary>
                public Nullable<bool> Recalc
                {
                    get
                    {
                        return recalc;
                    }
                    set
                    {
                        recalc = value;
                    }
                }

                /// <summary>
                /// List of items (read-only)
                /// </summary>
                public List<TFnGetConfigsForAuxDataApp> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new TFnGetConfigsForAuxDataApp(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexers

                /// <summary>
                /// Row from list of rows by identifier (read-only)
                /// </summary>
                /// <param name="id">Row identifier</param>
                /// <returns>Row from list of rows by identifier (null if not found)</returns>
                public TFnGetConfigsForAuxDataApp this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new TFnGetConfigsForAuxDataApp(composite: this, data: a))
                                .FirstOrDefault<TFnGetConfigsForAuxDataApp>();
                    }
                }

                #endregion Indexers


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public TFnGetConfigsForAuxDataAppList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any()
                        ? new TFnGetConfigsForAuxDataAppList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime)
                        {
                            ConfigCollectionId = ConfigCollectionId,
                            GuiVersionId = GuiVersionId,
                            IntervalPoints = IntervalPoints,
                            Recalc = Recalc
                        }
                        : new TFnGetConfigsForAuxDataAppList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null)
                        {
                            ConfigCollectionId = ConfigCollectionId,
                            GuiVersionId = GuiVersionId,
                            IntervalPoints = IntervalPoints,
                            Recalc = Recalc
                        };
                }

                #endregion Methods

            }

        }
    }
}