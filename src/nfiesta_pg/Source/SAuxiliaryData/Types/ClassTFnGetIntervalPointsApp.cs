﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace AuxiliaryData
        {
            // fn_get_interval_points_app

            /// <summary>
            /// Interval for the point layer
            /// (return type of the stored procedure fn_get_interval_points_app)
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class TFnGetIntervalPointsApp(
                TFnGetIntervalPointsAppList composite = null,
                DataRow data = null)
                    : AParametrizedViewEntry<TFnGetIntervalPointsAppList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TFnGetIntervalPointsAppList.ColId.Name,
                            defaultValue: Int32.Parse(s: TFnGetIntervalPointsAppList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TFnGetIntervalPointsAppList.ColId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// StepForInterval
                /// </summary>
                public Nullable<int> StepForInterval
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TFnGetIntervalPointsAppList.ColStepForInterval.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnGetIntervalPointsAppList.ColStepForInterval.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TFnGetIntervalPointsAppList.ColStepForInterval.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TFnGetIntervalPointsAppList.ColStepForInterval.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// GidStart
                /// </summary>
                public Nullable<int> GidStart
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TFnGetIntervalPointsAppList.ColGidStart.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnGetIntervalPointsAppList.ColGidStart.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TFnGetIntervalPointsAppList.ColGidStart.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TFnGetIntervalPointsAppList.ColGidStart.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// GidEnd
                /// </summary>
                public Nullable<int> GidEnd
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TFnGetIntervalPointsAppList.ColGidEnd.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnGetIntervalPointsAppList.ColGidEnd.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TFnGetIntervalPointsAppList.ColGidEnd.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TFnGetIntervalPointsAppList.ColGidEnd.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// GidEnd
                /// </summary>
                public Nullable<int> IntervalPoints
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TFnGetIntervalPointsAppList.ColIntervalPoints.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnGetIntervalPointsAppList.ColIntervalPoints.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TFnGetIntervalPointsAppList.ColIntervalPoints.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TFnGetIntervalPointsAppList.ColIntervalPoints.Name,
                            val: value);
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true|false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not TFnGetIntervalPointsApp, return False
                    if (obj is not TFnGetIntervalPointsApp)
                    {
                        return false;
                    }

                    return
                        Id == ((TFnGetIntervalPointsApp)obj).Id;
                }

                /// <summary>
                /// Returns a hash code for the current object
                /// </summary>
                /// <returns>Hash code for the current object</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                /// <summary>
                /// Returns a string that represents the current object
                /// </summary>
                /// <returns>String that represents the current object</returns>
                public override string ToString()
                {
                    return String.Concat(
                        $"{nameof(TFnGetIntervalPointsApp)}: {{",
                        $"{nameof(Id)}: {Functions.PrepIntArg(arg: Id)}; ",
                        $"{nameof(StepForInterval)}: {Functions.PrepNIntArg(arg: StepForInterval)}; ",
                        $"{nameof(GidStart)}: {Functions.PrepNIntArg(arg: GidStart)}; ",
                        $"{nameof(GidEnd)}: {Functions.PrepNIntArg(arg: GidEnd)}; ",
                        $"{nameof(IntervalPoints)}: {Functions.PrepNIntArg(arg: IntervalPoints)}}}");
                }

                #endregion Methods

            }


            /// <summary>
            /// Interval list for the point layer
            /// (return type of the stored procedure fn_get_interval_points_app)
            /// </summary>
            public class TFnGetIntervalPointsAppList
                 : AParametrizedView
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema =
                    ADFunctions.FnGetIntervalPointsApp.SchemaName;

                /// <summary>
                /// Stored procedure name
                /// </summary>
                public static readonly string Name =
                    ADFunctions.FnGetIntervalPointsApp.Name;

                /// <summary>
                /// Stored procedure alias
                /// </summary>
                public static readonly string Alias =
                    ADFunctions.FnGetIntervalPointsApp.Name;

                /// <summary>
                /// Description in national language
                /// </summary>
                public static readonly string CaptionCs = String.Concat(
                    "Interval z bodové vrstvy ",
                    "(návratový typ uložené procedury fn_get_interval_points_app)");

                /// <summary>
                /// Description in English
                /// </summary>
                public static readonly string CaptionEn = String.Concat(
                    "Interval list for the point layer ",
                    "(return type of the stored procedure fn_get_interval_points_app)");

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
                {
                    { "id", new ColumnMetadata()
                    {
                        Name = "id",
                        DbName = "id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = true,
                        NotNull = true,
                        DefaultValue = default(int).ToString(),
                        FuncCall = "row_number() over()",
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ID",
                        HeaderTextEn = "ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 0
                    }
                    },
                    { "step4interval", new ColumnMetadata()
                    {
                        Name = "step4interval",
                        DbName = "step4interval",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "STEP_FOR_INTERVAL",
                        HeaderTextEn = "STEP_FOR_INTERVAL",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 1
                    }
                    },
                    { "gid_start", new ColumnMetadata()
                    {
                        Name = "gid_start",
                        DbName = "gid_start",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "GID_START",
                        HeaderTextEn = "GID_START",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 2
                    }
                    },
                    { "gid_end", new ColumnMetadata()
                    {
                        Name = "gid_end",
                        DbName = "gid_end",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "GID_END",
                        HeaderTextEn = "GID_END",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 3
                    }
                    },
                    { "interval_points", new ColumnMetadata()
                    {
                        Name = "interval_points",
                        DbName = "interval_points",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "INTERVAL_POINTS",
                        HeaderTextEn = "INTERVAL_POINTS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 4
                    }
                    }
                };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column step4interval metadata
                /// </summary>
                public static readonly ColumnMetadata ColStepForInterval = Cols["step4interval"];

                /// <summary>
                /// Column gid_start metadata
                /// </summary>
                public static readonly ColumnMetadata ColGidStart = Cols["gid_start"];

                /// <summary>
                /// Column gid_end metadata
                /// </summary>
                public static readonly ColumnMetadata ColGidEnd = Cols["gid_end"];

                /// <summary>
                /// Column interval_points metadata
                /// </summary>
                public static readonly ColumnMetadata ColIntervalPoints = Cols["interval_points"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Private Fields

                /// <summary>
                /// 1st parameter: Config collection identifier
                /// </summary>
                private Nullable<int> configCollectionId;

                /// <summary>
                /// 2nd parameter: Number of points in interval
                /// </summary>
                private Nullable<int> intervalPoints;

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TFnGetIntervalPointsAppList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                {
                    ConfigCollectionId = null;
                    IntervalPoints = null;
                }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TFnGetIntervalPointsAppList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                {
                    ConfigCollectionId = null;
                    IntervalPoints = null;
                }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// 1st parameter: Configuration collection identifier
                /// </summary>
                public Nullable<int> ConfigCollectionId
                {
                    get
                    {
                        return configCollectionId;
                    }
                    set
                    {
                        configCollectionId = value;
                    }
                }

                /// <summary>
                /// 2nd parameter: Number of points in interval
                /// </summary>
                public Nullable<int> IntervalPoints
                {
                    get
                    {
                        return intervalPoints;
                    }
                    set
                    {
                        intervalPoints = value;
                    }
                }

                /// <summary>
                /// List of items (read-only)
                /// </summary>
                public List<TFnGetIntervalPointsApp> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new TFnGetIntervalPointsApp(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexers

                /// <summary>
                /// Row from list of rows by identifier (read-only)
                /// </summary>
                /// <param name="id">Row identifier</param>
                /// <returns>Row from list of rows by identifier (null if not found)</returns>
                public TFnGetIntervalPointsApp this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new TFnGetIntervalPointsApp(composite: this, data: a))
                                .FirstOrDefault<TFnGetIntervalPointsApp>();
                    }
                }

                #endregion Indexers


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public TFnGetIntervalPointsAppList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any()
                        ? new TFnGetIntervalPointsAppList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime)
                        {
                            ConfigCollectionId = ConfigCollectionId,
                            IntervalPoints = IntervalPoints
                        }
                        : new TFnGetIntervalPointsAppList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null)
                        {
                            ConfigCollectionId = ConfigCollectionId,
                            IntervalPoints = IntervalPoints
                        };
                }

                #endregion Methods

            }

        }
    }
}