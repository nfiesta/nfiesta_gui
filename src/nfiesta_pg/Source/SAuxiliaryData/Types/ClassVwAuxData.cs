﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace AuxiliaryData
        {

            /// <summary>
            /// Auxiliary plot data - data view
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class VwAuxData(
                VwAuxDataList composite = null,
                DataRow data = null)
                    : ALinqViewEntry<VwAuxDataList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: VwAuxDataList.ColId.Name,
                            defaultValue: Int32.Parse(s: VwAuxDataList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: VwAuxDataList.ColId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Configuration collection identifier
                /// </summary>
                public int ConfigCollectionId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: VwAuxDataList.ColConfigCollectionId.Name,
                            defaultValue: Int32.Parse(s: VwAuxDataList.ColConfigCollectionId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: VwAuxDataList.ColConfigCollectionId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Configuration collection object (read-only)
                /// </summary>
                public ConfigCollection ConfigCollection
                {
                    get
                    {
                        return ((NfiEstaDB)Composite.Database).SAuxiliaryData.TConfigCollection[(int)ConfigCollectionId];
                    }
                }


                /// <summary>
                /// Configuration identifier
                /// </summary>
                public int ConfigId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: VwAuxDataList.ColConfigId.Name,
                            defaultValue: Int32.Parse(s: VwAuxDataList.ColConfigId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: VwAuxDataList.ColConfigId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Configuration object (read-only)
                /// </summary>
                public Config Config
                {
                    get
                    {
                        return ((NfiEstaDB)Composite.Database).SAuxiliaryData.TConfig[(int)ConfigId];
                    }
                }

                /// <summary>
                /// Label of configuration in national language
                /// </summary>
                public string ConfigLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: VwAuxDataList.ColConfigLabelCs.Name,
                            defaultValue: VwAuxDataList.ColConfigLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: VwAuxDataList.ColConfigLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Description of configuration in national language
                /// </summary>
                public string ConfigDescriptionCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: VwAuxDataList.ColConfigDescriptionCs.Name,
                            defaultValue: VwAuxDataList.ColConfigDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: VwAuxDataList.ColConfigDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Label of configuration in English
                /// </summary>
                public string ConfigLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: VwAuxDataList.ColConfigLabelEn.Name,
                            defaultValue: VwAuxDataList.ColConfigLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: VwAuxDataList.ColConfigLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Description of configuration in English
                /// </summary>
                public string ConfigDescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: VwAuxDataList.ColConfigDescriptionEn.Name,
                            defaultValue: VwAuxDataList.ColConfigDescriptionEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: VwAuxDataList.ColConfigDescriptionEn.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Extension version identifier
                /// </summary>
                public int ExtensionVersionId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: VwAuxDataList.ColExtensionVersionId.Name,
                            defaultValue: Int32.Parse(s: VwAuxDataList.ColExtensionVersionId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: VwAuxDataList.ColExtensionVersionId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Extension version object (read-only)
                /// </summary>
                public ExtensionVersion ExtensionVersion
                {
                    get
                    {
                        return ((NfiEstaDB)Composite.Database).SAuxiliaryData.CExtensionVersion[(int)ExtensionVersionId];
                    }
                }

                /// <summary>
                /// Label of extension version in national language
                /// </summary>
                public string ExtensionVersionLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: VwAuxDataList.ColExtensionVersionLabelCs.Name,
                            defaultValue: VwAuxDataList.ColExtensionVersionLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: VwAuxDataList.ColExtensionVersionLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Description of extension version in national language
                /// </summary>
                public string ExtensionVersionDescriptionCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: VwAuxDataList.ColExtensionVersionDescriptionCs.Name,
                            defaultValue: VwAuxDataList.ColExtensionVersionDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: VwAuxDataList.ColExtensionVersionDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Label of extension version in English
                /// </summary>
                public string ExtensionVersionLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: VwAuxDataList.ColExtensionVersionLabelEn.Name,
                            defaultValue: VwAuxDataList.ColExtensionVersionLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: VwAuxDataList.ColExtensionVersionLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Description of extension version in English
                /// </summary>
                public string ExtensionVersionDescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: VwAuxDataList.ColExtensionVersionDescriptionEn.Name,
                            defaultValue: VwAuxDataList.ColExtensionVersionDescriptionEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: VwAuxDataList.ColExtensionVersionDescriptionEn.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Module version identifier
                /// </summary>
                public int GUIVersionId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: VwAuxDataList.ColGUIVersionId.Name,
                            defaultValue: Int32.Parse(s: VwAuxDataList.ColGUIVersionId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: VwAuxDataList.ColGUIVersionId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Module version object (read-only)
                /// </summary>
                public GUIVersion GUIVersion
                {
                    get
                    {
                        return ((NfiEstaDB)Composite.Database).SAuxiliaryData.CGUIVersion[(int)GUIVersionId];
                    }
                }

                /// <summary>
                /// Label of module version in national language
                /// </summary>
                public string GUIVersionLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: VwAuxDataList.ColGUIVersionLabelCs.Name,
                            defaultValue: VwAuxDataList.ColGUIVersionLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: VwAuxDataList.ColGUIVersionLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Description of module version in national language
                /// </summary>
                public string GUIVersionDescriptionCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: VwAuxDataList.ColGUIVersionDescriptionCs.Name,
                            defaultValue: VwAuxDataList.ColGUIVersionDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: VwAuxDataList.ColGUIVersionDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Label of module version in English
                /// </summary>
                public string GUIVersionLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: VwAuxDataList.ColGUIVersionLabelEn.Name,
                            defaultValue: VwAuxDataList.ColGUIVersionLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: VwAuxDataList.ColGUIVersionLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Description of module version in English
                /// </summary>
                public string GUIVersionDescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: VwAuxDataList.ColGUIVersionDescriptionEn.Name,
                            defaultValue: VwAuxDataList.ColGUIVersionDescriptionEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: VwAuxDataList.ColGUIVersionDescriptionEn.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Point identification within the configuration collection and configuration
                /// </summary>
                public Nullable<int> IdentPoint
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: VwAuxDataList.ColIdentPoint.Name,
                            defaultValue: String.IsNullOrEmpty(value: VwAuxDataList.ColIdentPoint.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: VwAuxDataList.ColIdentPoint.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: VwAuxDataList.ColIdentPoint.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Point identification
                /// </summary>
                public Nullable<int> Gid
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: VwAuxDataList.ColGid.Name,
                            defaultValue: String.IsNullOrEmpty(value: VwAuxDataList.ColGid.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: VwAuxDataList.ColGid.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: VwAuxDataList.ColGid.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Point object (read-only)
                /// </summary>
                public Plot Plot
                {
                    get
                    {
                        return
                            (Gid != null)
                                ? ((NfiEstaDB)Composite.Database)
                                    .SAuxiliaryData.FpPlot[(int)Gid]
                                : null;
                    }
                }


                /// <summary>
                /// Auxiliary variable value
                /// </summary>
                public double AuxValue
                {
                    get
                    {
                        return Functions.GetDoubleArg(
                            row: Data,
                            name: VwAuxDataList.ColAuxValue.Name,
                            defaultValue: Double.Parse(s: VwAuxDataList.ColAuxValue.DefaultValue));
                    }
                    set
                    {
                        Functions.SetDoubleArg(
                            row: Data,
                            name: VwAuxDataList.ColAuxValue.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Date and time of calculation of the auxiliary variable total
                /// </summary>
                public DateTime EstDate
                {
                    get
                    {
                        return Functions.GetDateTimeArg(
                            row: Data,
                            name: VwAuxDataList.ColEstDate.Name,
                            defaultValue: DateTime.Parse(s: VwAuxDataList.ColEstDate.DefaultValue));
                    }
                    set
                    {
                        Functions.SetDateTimeArg(
                            row: Data,
                            name: VwAuxDataList.ColEstDate.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Date and time of calculation of the auxiliary variable total as text
                /// </summary>
                public string EstDateText
                {
                    get
                    {
                        return
                            EstDate.ToString(
                                format: VwAuxDataList.ColEstDate.NumericFormat);
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not VwAuxData Type, return False
                    if (obj is not VwAuxData)
                    {
                        return false;
                    }

                    return
                        Id == ((VwAuxData)obj).Id;
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                #endregion Methods

            }


            /// <summary>
            /// List of auxiliary plot data - data view
            /// </summary>
            public class VwAuxDataList
                : ALinqView
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema = ADSchema.Name;

                /// <summary>
                /// Table name
                /// </summary>
                public const string Name = "v_auxiliary_data";

                /// <summary>
                /// Table alias
                /// </summary>
                public const string Alias = "v_auxiliary_data";

                /// <summary>
                /// Description in national language
                /// </summary>
                public const string CaptionCs = "Pomocná data na inventarizačních plochách - pohled";

                /// <summary>
                /// Description in English
                /// </summary>
                public const string CaptionEn = "Auxiliary inventory plot data - data view";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
                {
                    { "id", new ColumnMetadata()
                    {
                        Name = "id",
                        DbName = "id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = true,
                        NotNull = true,
                        DefaultValue = "0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ID",
                        HeaderTextEn = "ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 0
                    }
                    },
                    { "config_collection_id", new ColumnMetadata()
                    {
                        Name = "config_collection_id",
                        DbName = "config_collection_id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = "0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "CONFIG_COLLECTION_ID",
                        HeaderTextEn = "CONFIG_COLLECTION_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 1
                    }
                    },
                    { "config_id", new ColumnMetadata()
                    {
                        Name = "config_id",
                        DbName = "config_id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = "0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "CONFIG_ID",
                        HeaderTextEn = "CONFIG_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 2
                    }
                    },
                    { "config_label_cs", new ColumnMetadata()
                    {
                        Name = "config_label_cs",
                        DbName = "config_label_cs",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "CONFIG_LABEL_CS",
                        HeaderTextEn = "CONFIG_LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 3
                    }
                    },
                    { "config_description_cs", new ColumnMetadata()
                    {
                        Name = "config_description_cs",
                        DbName = "config_description_cs",
                        DataType = "System.String",
                        DbDataType = "text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "CONFIG_DESCRIPTION_CS",
                        HeaderTextEn = "CONFIG_DESCRIPTION_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 4
                    }
                    },
                    { "config_label_en", new ColumnMetadata()
                    {
                        Name = "config_label_en",
                        DbName = "config_label_en",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "CONFIG_LABEL_EN",
                        HeaderTextEn = "CONFIG_LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 5
                    }
                    },
                    { "config_description_en", new ColumnMetadata()
                    {
                        Name = "config_description_en",
                        DbName = "config_description_en",
                        DataType = "System.String",
                        DbDataType = "text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "CONFIG_DESCRIPTION_EN",
                        HeaderTextEn = "CONFIG_DESCRIPTION_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 6
                    }
                    },
                    { "extension_version_id", new ColumnMetadata()
                    {
                        Name = "extension_version_id",
                        DbName = "extension_version_id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = "0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "EXTENSION_VERSION_ID",
                        HeaderTextEn = "EXTENSION_VERSION_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 7
                    }
                    },
                    { "extension_version_label_cs", new ColumnMetadata()
                    {
                        Name = "extension_version_label_cs",
                        DbName = "extension_version_label_cs",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "EXTENSION_VERSION_LABEL_CS",
                        HeaderTextEn = "EXTENSION_VERSION_LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 8
                    }
                    },
                    { "extension_version_description_cs", new ColumnMetadata()
                    {
                        Name = "extension_version_description_cs",
                        DbName = "extension_version_description_cs",
                        DataType = "System.String",
                        DbDataType = "text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "EXTENSION_VERSION_DESCRIPTION_CS",
                        HeaderTextEn = "EXTENSION_VERSION_DESCRIPTION_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 9
                    }
                    },
                    { "extension_version_label_en", new ColumnMetadata()
                    {
                        Name = "extension_version_label_en",
                        DbName = "extension_version_label_en",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "EXTENSION_VERSION_LABEL_EN",
                        HeaderTextEn = "EXTENSION_VERSION_LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 10
                    }
                    },
                    { "extension_version_description_en", new ColumnMetadata()
                    {
                        Name = "extension_version_description_en",
                        DbName = "extension_version_description_en",
                        DataType = "System.String",
                        DbDataType = "text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "EXTENSION_VERSION_DESCRIPTION_EN",
                        HeaderTextEn = "EXTENSION_VERSION_DESCRIPTION_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 11
                    }
                    },
                    { "gui_version_id", new ColumnMetadata()
                    {
                        Name = "gui_version_id",
                        DbName = "gui_version",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = "0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "GUI_VERSION_ID",
                        HeaderTextEn = "GUI_VERSION_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 12
                    }
                    },
                    { "gui_version_label_cs", new ColumnMetadata()
                    {
                        Name = "gui_version_label_cs",
                        DbName = "gui_version_label_cs",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "GUI_VERSION_LABEL_CS",
                        HeaderTextEn = "GUI_VERSION_LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 13
                    }
                    },
                    { "gui_version_description_cs", new ColumnMetadata()
                    {
                        Name = "gui_version_description_cs",
                        DbName = "gui_version_description_cs",
                        DataType = "System.String",
                        DbDataType = "text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "GUI_VERSION_DESCRIPTION_CS",
                        HeaderTextEn = "GUI_VERSION_DESCRIPTION_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 14
                    }
                    },
                    { "gui_version_label_en", new ColumnMetadata()
                    {
                        Name = "gui_version_label_en",
                        DbName = "gui_version_label_en",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "GUI_VERSION_LABEL_EN",
                        HeaderTextEn = "GUI_VERSION_LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 15
                    }
                    },
                    { "gui_version_description_en", new ColumnMetadata()
                    {
                        Name = "gui_version_description_en",
                        DbName = "gui_version_description_en",
                        DataType = "System.String",
                        DbDataType = "text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "GUI_VERSION_DESCRIPTION_EN",
                        HeaderTextEn = "GUI_VERSION_DESCRIPTION_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 16
                    }
                    },
                    { "ident_point", new ColumnMetadata()
                    {
                        Name = "ident_point",
                        DbName = "ident_point",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "IDENT_POINT",
                        HeaderTextEn = "IDENT_POINT",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 17
                    }
                    },
                    { "gid", new ColumnMetadata()
                    {
                        Name = "gid",
                        DbName = "gid",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "GID",
                        HeaderTextEn = "GID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 18
                    }
                    },
                    { "aux_value", new ColumnMetadata()
                    {
                        Name = "aux_value",
                        DbName = "value",
                        DataType = "System.Double",
                        DbDataType = "float8",
                        NewDataType = "float8",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = "0.0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "N3",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "VALUE",
                        HeaderTextEn = "VALUE",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 19
                    }
                    },
                    { "est_date", new ColumnMetadata()
                    {
                        Name = "est_date",
                        DbName = "est_date",
                        DataType = "System.DateTime",
                        DbDataType = "timestamp",
                        NewDataType = "timestamp",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = new DateTime().ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "yyyy-MM-dd HH:mm:ss",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "EST_DATE",
                        HeaderTextEn = "EST_DATE",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 20
                    }
                    }
                };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column config_collection_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColConfigCollectionId = Cols["config_collection_id"];

                /// <summary>
                /// Column config_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColConfigId = Cols["config_id"];

                /// <summary>
                /// Column config_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColConfigLabelCs = Cols["config_label_cs"];

                /// <summary>
                /// Column config_description_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColConfigDescriptionCs = Cols["config_description_cs"];

                /// <summary>
                /// Column config_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColConfigLabelEn = Cols["config_label_en"];

                /// <summary>
                /// Column config_description_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColConfigDescriptionEn = Cols["config_description_en"];

                /// <summary>
                /// Column extension_version_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColExtensionVersionId = Cols["extension_version_id"];

                /// <summary>
                /// Column extension_version_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColExtensionVersionLabelCs = Cols["extension_version_label_cs"];

                /// <summary>
                /// Column extension_version_description_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColExtensionVersionDescriptionCs = Cols["extension_version_description_cs"];

                /// <summary>
                /// Column extension_version_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColExtensionVersionLabelEn = Cols["extension_version_label_en"];

                /// <summary>
                /// Column extension_version_description_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColExtensionVersionDescriptionEn = Cols["extension_version_description_en"];

                /// <summary>
                /// Column gui_version_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColGUIVersionId = Cols["gui_version_id"];

                /// <summary>
                /// Column gui_version_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColGUIVersionLabelCs = Cols["gui_version_label_cs"];

                /// <summary>
                /// Column gui_version_description_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColGUIVersionDescriptionCs = Cols["gui_version_description_cs"];

                /// <summary>
                /// Column gui_version_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColGUIVersionLabelEn = Cols["gui_version_label_en"];

                /// <summary>
                /// Column gui_version_description_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColGUIVersionDescriptionEn = Cols["gui_version_description_en"];

                /// <summary>
                /// Column ident_point metadata
                /// </summary>
                public static readonly ColumnMetadata ColIdentPoint = Cols["ident_point"];

                /// <summary>
                /// Column gid metadata
                /// </summary>
                public static readonly ColumnMetadata ColGid = Cols["gid"];

                /// <summary>
                /// Column aux_value metadata
                /// </summary>
                public static readonly ColumnMetadata ColAuxValue = Cols["aux_value"];

                /// <summary>
                /// Column est_date metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstDate = Cols["est_date"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public VwAuxDataList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public VwAuxDataList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// List of auxiliary inventory plot data (read-only)
                /// </summary>
                public List<VwAuxData> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new VwAuxData(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// Auxiliary inventory plot data from list by identifier (read-only)
                /// </summary>
                /// <param name="id">Auxiliary inventory plot data identifier</param>
                /// <returns>Auxiliary inventory plot data from list by identifier (null if not found)</returns>
                public VwAuxData this[string id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<string>(ColId.Name) == id)
                                .Select(a => new VwAuxData(composite: this, data: a))
                                .FirstOrDefault<VwAuxData>();
                    }
                }

                #endregion Indexer


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public VwAuxDataList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new VwAuxDataList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new VwAuxDataList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                /// <summary>
                /// Loads data (if it was not done before)
                /// </summary>
                /// <param name="tAuxData">List of auxiliary inventory plot data</param>
                /// <param name="tConfig">List of configurations for the auxiliary variable totals calculation</param>
                /// <param name="cExtVersion">List of extension versions</param>
                /// <param name="cGuiVersion">List of GUI versions</param>
                /// <param name="condition">Condition for selection data</param>
                /// <param name="limit">Rows count limit</param>
                public void Load(
                    AuxDataList tAuxData,
                    ConfigList tConfig,
                    ExtensionVersionList cExtVersion,
                    GUIVersionList cGuiVersion,
                    string condition = null,
                    Nullable<int> limit = null)
                {
                    if (!Loaded)
                    {
                        ReLoad(
                            tAuxData: tAuxData,
                            tConfig: tConfig,
                            cExtVersion: cExtVersion,
                            cGuiVersion: cGuiVersion,
                            condition: condition,
                            limit: limit);
                    }
                }

                /// <summary>
                /// Loads data
                /// </summary>
                /// <param name="tAuxData">List of auxiliary inventory plot data</param>
                /// <param name="tConfig">List of configurations for the auxiliary variable totals calculation</param>
                /// <param name="cExtVersion">List of extension versions</param>
                /// <param name="cGuiVersion">List of GUI versions</param>
                /// <param name="condition">Condition for selection data</param>
                /// <param name="limit">Rows count limit</param>
                public void ReLoad(
                    AuxDataList tAuxData,
                    ConfigList tConfig,
                    ExtensionVersionList cExtVersion,
                    GUIVersionList cGuiVersion,
                    string condition = null,
                    Nullable<int> limit = null)
                {
                    Data = EmptyDataTable(
                        tableName: TableName,
                        columns: Columns);
                    Condition = condition;
                    Limit = limit;

                    System.Diagnostics.Stopwatch stopWatch = new();
                    stopWatch.Start();

                    #region cteAuxData
                    var cteAuxData =
                        tAuxData.Data.AsEnumerable()
                        .Select(a => new
                        {
                            Id = a.Field<int>(columnName: AuxDataList.ColId.Name),
                            ConfigCollectionId = a.Field<int>(columnName: AuxDataList.ColConfigCollectionId.Name),
                            ConfigId = a.Field<int>(columnName: AuxDataList.ColConfigId.Name),
                            ExtensionVersionId = a.Field<int>(columnName: AuxDataList.ColExtensionVersionId.Name),
                            GUIVersionId = a.Field<int>(columnName: AuxDataList.ColGUIVersionId.Name),
                            IdentPoint = a.Field<Nullable<int>>(columnName: AuxDataList.ColIdentPoint.Name),
                            Gid = a.Field<Nullable<int>>(columnName: AuxDataList.ColGid.Name),
                            AuxValue = a.Field<double>(columnName: AuxDataList.ColAuxValue.Name),
                            EstDate = a.Field<DateTime>(columnName: AuxDataList.ColEstDate.Name),
                        });
                    #endregion cteAuxData

                    #region cteConfig
                    var cteConfig =
                        tConfig.Data.AsEnumerable()
                        .Select(a => new
                        {
                            ConfigId = a.Field<int>(columnName: ConfigList.ColId.Name),
                            ConfigLabelCs = a.Field<string>(columnName: ConfigList.ColLabelCs.Name),
                            ConfigLabelEn = a.Field<string>(columnName: ConfigList.ColLabelEn.Name),
                            ConfigDescriptionCs = a.Field<string>(columnName: ConfigList.ColDescriptionEn.Name),
                            ConfigDescriptionEn = a.Field<string>(columnName: ConfigList.ColDescriptionEn.Name)
                        });
                    #endregion cteConfig

                    #region cteExtVersion
                    var cteExtVersion =
                        cExtVersion.Data.AsEnumerable()
                        .Select(a => new
                        {
                            ExtensionVersionId = a.Field<int>(columnName: ExtensionVersionList.ColId.Name),
                            ExtensionVersionLabelCs = a.Field<string>(columnName: ExtensionVersionList.ColLabelCs.Name),
                            ExtensionVersionLabelEn = a.Field<string>(columnName: ExtensionVersionList.ColLabelEn.Name),
                            ExtensionVersionDescriptionCs = a.Field<string>(columnName: ExtensionVersionList.ColDescriptionEn.Name),
                            ExtensionVersionDescriptionEn = a.Field<string>(columnName: ExtensionVersionList.ColDescriptionEn.Name)
                        });
                    #endregion cteExtVersion

                    #region cteGuiVersion
                    var cteGuiVersion =
                        cGuiVersion.Data.AsEnumerable()
                        .Select(a => new
                        {
                            GUIVersionId = a.Field<int>(columnName: GUIVersionList.ColId.Name),
                            GUIVersionLabelCs = a.Field<string>(columnName: GUIVersionList.ColLabelCs.Name),
                            GUIVersionLabelEn = a.Field<string>(columnName: GUIVersionList.ColLabelEn.Name),
                            GUIVersionDescriptionCs = a.Field<string>(columnName: GUIVersionList.ColDescriptionEn.Name),
                            GUIVersionDescriptionEn = a.Field<string>(columnName: GUIVersionList.ColDescriptionEn.Name)
                        });
                    #endregion cteGuiVersion

                    #region cteView

                    var cteView =
                        from a in cteAuxData
                        join b in cteConfig on a.ConfigId equals b.ConfigId into ljB
                        from b in ljB.DefaultIfEmpty()
                        join c in cteExtVersion on a.ExtensionVersionId equals c.ExtensionVersionId into ljC
                        from c in ljC.DefaultIfEmpty()
                        join d in cteGuiVersion on a.GUIVersionId equals d.GUIVersionId into ljD
                        from d in ljD.DefaultIfEmpty()
                        select new
                        {
                            a.Id,
                            a.ConfigCollectionId,
                            a.ConfigId,
                            b?.ConfigLabelCs,
                            b?.ConfigDescriptionCs,
                            b?.ConfigLabelEn,
                            b?.ConfigDescriptionEn,
                            a.ExtensionVersionId,
                            c?.ExtensionVersionLabelCs,
                            c?.ExtensionVersionDescriptionCs,
                            c?.ExtensionVersionLabelEn,
                            c?.ExtensionVersionDescriptionEn,
                            a.GUIVersionId,
                            d?.GUIVersionLabelCs,
                            d?.GUIVersionDescriptionCs,
                            d?.GUIVersionLabelEn,
                            d?.GUIVersionDescriptionEn,
                            a.IdentPoint,
                            a.Gid,
                            a.AuxValue,
                            a.EstDate
                        };

                    #endregion cteView

                    #region Zápis výsledku do DataTable
                    if (cteView.Any())
                    {
                        Data =
                            cteView
                            .OrderBy(a => a.ConfigId)
                            .ThenBy(a => a.ExtensionVersionId)
                            .ThenBy(a => a.GUIVersionId)
                            .ThenBy(a => a.Gid ?? 0)
                            .Select(a =>
                                Data.LoadDataRow(
                                    values:
                                    [
                                    a.Id,
                                    a.ConfigCollectionId,
                                    a.ConfigId,
                                    a.ConfigLabelCs,
                                    a.ConfigDescriptionCs,
                                    a.ConfigLabelEn,
                                    a.ConfigDescriptionEn,
                                    a.ExtensionVersionId,
                                    a.ExtensionVersionLabelCs,
                                    a.ExtensionVersionDescriptionCs,
                                    a.ExtensionVersionLabelEn,
                                    a.ExtensionVersionDescriptionEn,
                                    a.GUIVersionId,
                                    a.GUIVersionLabelCs,
                                    a.GUIVersionDescriptionCs,
                                    a.GUIVersionLabelEn,
                                    a.GUIVersionDescriptionEn,
                                    a.IdentPoint,
                                    a.Gid,
                                    a.AuxValue,
                                    a.EstDate
                                    ],
                                    fAcceptChanges: false))
                            .CopyToDataTable();
                        Data.TableName = Name;
                    }
                    #endregion Zápis výsledku do DataTable

                    #region Omezení podmínkou
                    if (!String.IsNullOrEmpty(value: Condition))
                    {
                        DataView view = new(table: Data);
                        try
                        {
                            view.RowFilter = Condition;
                        }
                        catch
                        {
                            view.RowFilter = String.Empty;
                            Condition = String.Empty;
                        }
                        Data = view.ToTable();
                    }
                    #endregion Omezení podmínkou

                    #region Omezení počtem
                    if (Limit != null)
                    {
                        Data = Data.AsEnumerable()
                            .Take(count: (int)Limit)
                            .CopyToDataTable();
                    }
                    #endregion  Omezení počtem

                    stopWatch.Stop();
                    LoadingTime = 0.001 * stopWatch.ElapsedMilliseconds;
                    Data.TableName = TableName;
                }

                #endregion Methods

            }

        }
    }
}