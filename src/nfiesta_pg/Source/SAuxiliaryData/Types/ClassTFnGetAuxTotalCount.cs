﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace AuxiliaryData
        {
            // fn_get_aux_total_count

            /// <summary>
            /// Count of auxiliary variable totals for configuration, extension version and gui version
            /// (return type of the stored procedure fn_get_aux_total_count)
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class TFnGetAuxTotalCount(
                TFnGetAuxTotalCountList composite = null,
                DataRow data = null)
                    : AParametrizedViewEntry<TFnGetAuxTotalCountList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TFnGetAuxTotalCountList.ColId.Name,
                            defaultValue: Int32.Parse(s: TFnGetAuxTotalCountList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TFnGetAuxTotalCountList.ColId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Configuration identifier
                /// </summary>
                public Nullable<int> ConfigId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TFnGetAuxTotalCountList.ColConfigId.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnGetAuxTotalCountList.ColConfigId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TFnGetAuxTotalCountList.ColConfigId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TFnGetAuxTotalCountList.ColConfigId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Configuration object (read-only)
                /// </summary>
                public Config Config
                {
                    get
                    {
                        return
                            (ConfigId != null)
                                ? ((NfiEstaDB)Composite.Database)
                                    .SAuxiliaryData.TConfig[(int)ConfigId]
                                : null;
                    }
                }


                /// <summary>
                /// Extension version identifier
                /// </summary>
                public Nullable<int> ExtVersionId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TFnGetAuxTotalCountList.ColExtVersionId.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnGetAuxTotalCountList.ColExtVersionId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TFnGetAuxTotalCountList.ColExtVersionId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TFnGetAuxTotalCountList.ColExtVersionId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Extension version object (read-only)
                /// </summary>
                public ExtensionVersion ExtVersion
                {
                    get
                    {
                        return
                            (ExtVersionId != null)
                                ? ((NfiEstaDB)Composite.Database)
                                    .SAuxiliaryData.CExtensionVersion[(int)ExtVersionId]
                                : null;
                    }
                }


                /// <summary>
                /// GUI version identifier
                /// </summary>
                public Nullable<int> GuiVersionId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TFnGetAuxTotalCountList.ColGuiVersionId.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnGetAuxTotalCountList.ColGuiVersionId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TFnGetAuxTotalCountList.ColGuiVersionId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TFnGetAuxTotalCountList.ColGuiVersionId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// GUI version object (read-only)
                /// </summary>
                public GUIVersion GuiVersion
                {
                    get
                    {
                        return
                            (GuiVersionId != null)
                                ? ((NfiEstaDB)Composite.Database)
                                    .SAuxiliaryData.CGUIVersion[(int)GuiVersionId]
                                : null;
                    }
                }


                /// <summary>
                /// Count of auxiliary variable totals for configuration, extension version and gui version
                /// </summary>
                public Nullable<long> AuxTotalCount
                {
                    get
                    {
                        return Functions.GetNLongArg(
                            row: Data,
                            name: TFnGetAuxTotalCountList.ColAuxTotalCount.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnGetAuxTotalCountList.ColAuxTotalCount.DefaultValue) ?
                                (Nullable<long>)null :
                                Int64.Parse(s: TFnGetAuxTotalCountList.ColAuxTotalCount.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNLongArg(
                            row: Data,
                            name: TFnGetAuxTotalCountList.ColAuxTotalCount.Name,
                            val: value);
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true|false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not TFnGetAuxTotalCount, return False
                    if (obj is not TFnGetAuxTotalCount)
                    {
                        return false;
                    }

                    return
                        Id == ((TFnGetAuxTotalCount)obj).Id;
                }

                /// <summary>
                /// Returns a hash code for the current object
                /// </summary>
                /// <returns>Hash code for the current object</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                /// <summary>
                /// Returns a string that represents the current object
                /// </summary>
                /// <returns>String that represents the current object</returns>
                public override string ToString()
                {
                    return String.Concat(
                        $"{nameof(TFnGetAuxTotalCount)}: {{",
                        $"{nameof(Id)}: {Functions.PrepIntArg(arg: Id)}; ",
                        $"{nameof(ConfigId)}: {Functions.PrepNIntArg(arg: ConfigId)}; ",
                        $"{nameof(ExtVersionId)}: {Functions.PrepNIntArg(arg: ExtVersionId)}; ",
                        $"{nameof(GuiVersionId)}: {Functions.PrepNIntArg(arg: GuiVersionId)}; ",
                        $"{nameof(AuxTotalCount)}: {Functions.PrepNLongArg(arg: AuxTotalCount)}}}");
                }

                #endregion Methods

            }

            /// <summary>
            /// List of counts of auxiliary variable totals for configuration, extension version and gui version
            /// </summary>
            public class TFnGetAuxTotalCountList
                : AParametrizedView
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema =
                    ADFunctions.FnGetAuxTotalCount.SchemaName;

                /// <summary>
                /// Stored procedure name
                /// </summary>
                public static readonly string Name =
                    ADFunctions.FnGetAuxTotalCount.Name;

                /// <summary>
                /// Stored procedure alias
                /// </summary>
                public static readonly string Alias =
                    ADFunctions.FnGetAuxTotalCount.Name;

                /// <summary>
                /// Description in national language
                /// </summary>
                public static readonly string CaptionCs = String.Concat(
                    "Počet vypočtených úhrnů pomocné proměnné pro jednu konfiguraci, verzi db. extenze a aplikace");

                /// <summary>
                /// Description in English
                /// </summary>
                public static readonly string CaptionEn = String.Concat(
                    "Counts of auxiliary variable totals for configuration, extension version and gui version");

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
                {
                    { "id", new ColumnMetadata()
                    {
                        Name = "id",
                        DbName = "id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = true,
                        NotNull = true,
                        DefaultValue = default(int).ToString(),
                        FuncCall = "row_number() over()",
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ID",
                        HeaderTextEn = "ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 0
                    }
                    },
                    { "config_id", new ColumnMetadata()
                    {
                        Name = "config_id",
                        DbName = "config",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "CONFIG_ID",
                        HeaderTextEn = "CONFIG_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 1
                    }
                    },
                    { "ext_version_id", new ColumnMetadata()
                    {
                        Name = "ext_version_id",
                        DbName = "ext_version",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "EXT_VERSION_ID",
                        HeaderTextEn = "EXT_VERSION_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 2
                    }
                    },
                    { "gui_version_id", new ColumnMetadata()
                    {
                        Name = "gui_version_id",
                        DbName = "gui_version",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "GUI_VERSION_ID",
                        HeaderTextEn = "GUI_VERSION_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 3
                    }
                    },
                    { "aux_total_count", new ColumnMetadata()
                    {
                        Name = "aux_total_count",
                        DbName = "aux_total_count",
                        DataType = "System.Int64",
                        DbDataType = "int8",
                        NewDataType = "int8",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = "count(*)",
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "AUX_TOTAL_COUNT",
                        HeaderTextEn = "AUX_TOTAL_COUNT",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 4
                    }
                    }
                };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column config_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColConfigId = Cols["config_id"];

                /// <summary>
                /// Column ext_version_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColExtVersionId = Cols["ext_version_id"];

                /// <summary>
                /// Column gui_version_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColGuiVersionId = Cols["gui_version_id"];

                /// <summary>
                /// Column aux_total_count metadata
                /// </summary>
                public static readonly ColumnMetadata ColAuxTotalCount = Cols["aux_total_count"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TFnGetAuxTotalCountList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TFnGetAuxTotalCountList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// List of items (read-only)
                /// </summary>
                public List<TFnGetAuxTotalCount> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new TFnGetAuxTotalCount(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// Item from list by identifier (read-only)
                /// </summary>
                /// <param name="id">Item identifier</param>
                /// <returns>Item from list by identifier (null if not found)</returns>
                public TFnGetAuxTotalCount this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new TFnGetAuxTotalCount(composite: this, data: a))
                                .FirstOrDefault<TFnGetAuxTotalCount>();
                    }
                }

                #endregion Indexer


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public TFnGetAuxTotalCountList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new TFnGetAuxTotalCountList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new TFnGetAuxTotalCountList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                /// <summary>
                /// Count of auxiliary variable totals
                /// for configuration, extension version and module version
                /// </summary>
                /// <param name="config">Configuration</param>
                /// <param name="extensionVersion">Extension version</param>
                /// <param name="guiVersion">Module version</param>
                /// <returns>
                /// Count of auxiliary variable totals
                /// for configuration, extension version and module version
                /// </returns>
                public long AuxTotalCount(
                    Config config = null,
                    ExtensionVersion extensionVersion = null,
                    GUIVersion guiVersion = null)
                {
                    if (config != null)
                    {
                        if (extensionVersion != null)
                        {
                            if (guiVersion != null)
                            {
                                return
                                    Items
                                        .Where(a =>
                                            (a.ConfigId == config.Id) &&
                                            (a.ExtVersionId == extensionVersion.Id) &&
                                            (a.GuiVersionId == guiVersion.Id))
                                        .Sum(a => a.AuxTotalCount) ?? 0;
                            }
                            else
                            {
                                return
                                    Items
                                        .Where(a =>
                                            (a.ConfigId == config.Id) &&
                                            (a.ExtVersionId == extensionVersion.Id))
                                        .Sum(a => a.AuxTotalCount) ?? 0;
                            }
                        }
                        else
                        {
                            if (guiVersion != null)
                            {
                                return
                                    Items
                                        .Where(a =>
                                            (a.ConfigId == config.Id) &&
                                            (a.GuiVersionId == guiVersion.Id))
                                        .Sum(a => a.AuxTotalCount) ?? 0;
                            }
                            else
                            {
                                return
                                    Items
                                        .Where(a =>
                                            (a.ConfigId == config.Id))
                                        .Sum(a => a.AuxTotalCount) ?? 0;
                            }
                        }
                    }
                    else
                    {
                        if (extensionVersion != null)
                        {
                            if (guiVersion != null)
                            {
                                return
                                    Items
                                        .Where(a =>
                                            (a.ExtVersionId == extensionVersion.Id) &&
                                            (a.GuiVersionId == guiVersion.Id))
                                        .Sum(a => a.AuxTotalCount) ?? 0;
                            }
                            else
                            {
                                return
                                    Items
                                        .Where(a =>
                                            (a.ExtVersionId == extensionVersion.Id))
                                        .Sum(a => a.AuxTotalCount) ?? 0;
                            }
                        }
                        else
                        {
                            if (guiVersion != null)
                            {
                                return
                                    Items
                                        .Where(a =>
                                            (a.GuiVersionId == guiVersion.Id))
                                        .Sum(a => a.AuxTotalCount) ?? 0;
                            }
                            else
                            {
                                return
                                    Items
                                        .Sum(a => a.AuxTotalCount) ?? 0;
                            }
                        }
                    }
                }

                #endregion Methods

            }

        }
    }
}