﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace AuxiliaryData
        {
            // fn_api_get_estimation_cell_hierarchy

            /// <summary>
            /// Estimation cell with hierarchy
            /// (return type of the stored procedure fn_api_get_estimation_cell_hierarchy)
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class TFnApiGetEstimationCellHierarchy(
                TFnApiGetEstimationCellHierarchyList composite = null,
                DataRow data = null)
                    : AParametrizedViewEntry<TFnApiGetEstimationCellHierarchyList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TFnApiGetEstimationCellHierarchyList.ColId.Name,
                            defaultValue: Int32.Parse(s: TFnApiGetEstimationCellHierarchyList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TFnApiGetEstimationCellHierarchyList.ColId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Estimation cell collection identifier
                /// </summary>
                public Nullable<int> EstimationCellCollectionId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TFnApiGetEstimationCellHierarchyList.ColEstimationCellCollectionId.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnApiGetEstimationCellHierarchyList.ColEstimationCellCollectionId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TFnApiGetEstimationCellHierarchyList.ColEstimationCellCollectionId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TFnApiGetEstimationCellHierarchyList.ColEstimationCellCollectionId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Estimation cell collection object (read-only)
                /// </summary>
                public EstimationCellCollection EstimationCellCollection
                {
                    get
                    {
                        return
                            (EstimationCellCollectionId != null)
                                ? ((NfiEstaDB)Composite.Database)
                                    .SAuxiliaryData.CEstimationCellCollection[(int)EstimationCellCollectionId]
                                : null;
                    }
                }

                /// <summary>
                /// Estimation cell identifier
                /// </summary>
                public Nullable<int> EstimationCellId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TFnApiGetEstimationCellHierarchyList.ColEstimationCellId.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnApiGetEstimationCellHierarchyList.ColEstimationCellId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TFnApiGetEstimationCellHierarchyList.ColEstimationCellId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TFnApiGetEstimationCellHierarchyList.ColEstimationCellId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Estimation cell object (read-only)
                /// </summary>
                public EstimationCell EstimationCell
                {
                    get
                    {
                        return
                            (EstimationCellId != null)
                                ? ((NfiEstaDB)Composite.Database)
                                    .SAuxiliaryData.CEstimationCell[(int)EstimationCellId]
                                : null;
                    }
                }

                /// <summary>
                /// Label of estimation cell in national language
                /// </summary>
                public string EstimationCellLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetEstimationCellHierarchyList.ColEstimationCellLabelCs.Name,
                            defaultValue: TFnApiGetEstimationCellHierarchyList.ColEstimationCellLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetEstimationCellHierarchyList.ColEstimationCellLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Description of estimation cell in national language
                /// </summary>
                public string EstimationCellDescriptionCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetEstimationCellHierarchyList.ColEstimationCellDescriptionCs.Name,
                            defaultValue: TFnApiGetEstimationCellHierarchyList.ColEstimationCellDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetEstimationCellHierarchyList.ColEstimationCellDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Extended label of estimation cell in national language (read-only)
                /// </summary>
                public string EstimationCellExtendedLabelCs
                {
                    get
                    {
                        return $"{EstimationCellId} - {EstimationCellLabelCs}";
                    }
                }

                /// <summary>
                /// Label of estimation cell in English
                /// </summary>
                public string EstimationCellLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetEstimationCellHierarchyList.ColEstimationCellLabelEn.Name,
                            defaultValue: TFnApiGetEstimationCellHierarchyList.ColEstimationCellLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetEstimationCellHierarchyList.ColEstimationCellLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Description of estimation cell in English
                /// </summary>
                public string EstimationCellDescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetEstimationCellHierarchyList.ColEstimationCellDescriptionEn.Name,
                            defaultValue: TFnApiGetEstimationCellHierarchyList.ColEstimationCellDescriptionEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetEstimationCellHierarchyList.ColEstimationCellDescriptionEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Extended label of estimation cell in English (read-only)
                /// </summary>
                public string EstimationCellExtendedLabelEn
                {
                    get
                    {
                        return $"{EstimationCellId} - {EstimationCellLabelEn}";
                    }
                }


                /// <summary>
                /// Superior identifier
                /// </summary>
                public Nullable<int> SuperiorId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TFnApiGetEstimationCellHierarchyList.ColSuperiorId.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnApiGetEstimationCellHierarchyList.ColSuperiorId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TFnApiGetEstimationCellHierarchyList.ColSuperiorId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TFnApiGetEstimationCellHierarchyList.ColSuperiorId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Superior estimation cell collection identifier
                /// </summary>
                public Nullable<int> SuperiorEstimationCellCollectionId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TFnApiGetEstimationCellHierarchyList.ColSuperiorEstimationCellCollectionId.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnApiGetEstimationCellHierarchyList.ColSuperiorEstimationCellCollectionId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TFnApiGetEstimationCellHierarchyList.ColSuperiorEstimationCellCollectionId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TFnApiGetEstimationCellHierarchyList.ColSuperiorEstimationCellCollectionId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Superior estimation cell collection object (read-only)
                /// </summary>
                public EstimationCellCollection SuperiorEstimationCellCollection
                {
                    get
                    {
                        return
                            (SuperiorEstimationCellCollectionId != null)
                                ? ((NfiEstaDB)Composite.Database)
                                    .SAuxiliaryData.CEstimationCellCollection[(int)SuperiorEstimationCellCollectionId]
                                : null;
                    }
                }

                /// <summary>
                /// Superior estimation cell identifier
                /// </summary>
                public Nullable<int> SuperiorEstimationCellId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TFnApiGetEstimationCellHierarchyList.ColSuperiorEstimationCellId.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnApiGetEstimationCellHierarchyList.ColSuperiorEstimationCellId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TFnApiGetEstimationCellHierarchyList.ColSuperiorEstimationCellId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TFnApiGetEstimationCellHierarchyList.ColSuperiorEstimationCellId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Superior estimation cell object (read-only)
                /// </summary>
                public EstimationCell SuperiorEstimationCell
                {
                    get
                    {
                        return
                            (SuperiorEstimationCellId != null)
                                ? ((NfiEstaDB)Composite.Database)
                                    .SAuxiliaryData.CEstimationCell[(int)SuperiorEstimationCellId]
                                : null;
                    }
                }

                /// <summary>
                /// Label of superior estimation cell in national language
                /// </summary>
                public string SuperiorEstimationCellLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetEstimationCellHierarchyList.ColSuperiorEstimationCellLabelCs.Name,
                            defaultValue: TFnApiGetEstimationCellHierarchyList.ColSuperiorEstimationCellLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetEstimationCellHierarchyList.ColSuperiorEstimationCellLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Description of superior estimation cell in national language
                /// </summary>
                public string SuperiorEstimationCellDescriptionCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetEstimationCellHierarchyList.ColSuperiorEstimationCellDescriptionCs.Name,
                            defaultValue: TFnApiGetEstimationCellHierarchyList.ColSuperiorEstimationCellDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetEstimationCellHierarchyList.ColSuperiorEstimationCellDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Extended label of superior estimation cell in national language (read-only)
                /// </summary>
                public string SuperiorEstimationCellExtendedLabelCs
                {
                    get
                    {
                        return $"{SuperiorEstimationCellId} - {SuperiorEstimationCellLabelCs}";
                    }
                }

                /// <summary>
                /// Label of superior estimation cell in English
                /// </summary>
                public string SuperiorEstimationCellLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetEstimationCellHierarchyList.ColSuperiorEstimationCellLabelEn.Name,
                            defaultValue: TFnApiGetEstimationCellHierarchyList.ColSuperiorEstimationCellLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetEstimationCellHierarchyList.ColSuperiorEstimationCellLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Description of superior estimation cell in English
                /// </summary>
                public string SuperiorEstimationCellDescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetEstimationCellHierarchyList.ColSuperiorEstimationCellDescriptionEn.Name,
                            defaultValue: TFnApiGetEstimationCellHierarchyList.ColSuperiorEstimationCellDescriptionEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetEstimationCellHierarchyList.ColSuperiorEstimationCellDescriptionEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Extended label of superior estimation cell in English (read-only)
                /// </summary>
                public string SuperiorEstimationCellExtendedLabelEn
                {
                    get
                    {
                        return $"{SuperiorEstimationCellId} - {SuperiorEstimationCellLabelEn}";
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true|false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not TFnApiGetEstimationCellHierarchy, return False
                    if (obj is not TFnApiGetEstimationCellHierarchy)
                    {
                        return false;
                    }

                    return
                        Id == ((TFnApiGetEstimationCellHierarchy)obj).Id;
                }

                /// <summary>
                /// Returns a hash code for the current object
                /// </summary>
                /// <returns>Hash code for the current object</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                /// <summary>
                /// Returns a string that represents the current object
                /// </summary>
                /// <returns>String that represents the current object</returns>
                public override string ToString()
                {
                    return String.Concat(
                        $"{nameof(TFnApiGetEstimationCellHierarchy)}: {{",
                        $"{nameof(Id)}: {Functions.PrepIntArg(arg: Id)}; ",
                        $"{nameof(EstimationCellCollectionId)}: {Functions.PrepNIntArg(arg: EstimationCellCollectionId)}; ",
                        $"{nameof(EstimationCellId)}: {Functions.PrepNIntArg(arg: EstimationCellId)}; ",
                        $"{nameof(EstimationCellLabelCs)}: {Functions.PrepStringArg(arg: EstimationCellLabelCs)}; ",
                        $"{nameof(EstimationCellLabelEn)}: {Functions.PrepStringArg(arg: EstimationCellLabelEn)}; ",
                        $"{nameof(EstimationCellDescriptionCs)}: {Functions.PrepStringArg(arg: EstimationCellDescriptionCs)}; ",
                        $"{nameof(EstimationCellDescriptionEn)}: {Functions.PrepStringArg(arg: EstimationCellDescriptionEn)}; ",
                        $"{nameof(SuperiorId)}: {Functions.PrepNIntArg(arg: SuperiorId)}; ",
                        $"{nameof(SuperiorEstimationCellCollectionId)}: {Functions.PrepNIntArg(arg: SuperiorEstimationCellCollectionId)}; ",
                        $"{nameof(SuperiorEstimationCellId)}: {Functions.PrepNIntArg(arg: SuperiorEstimationCellId)}; ",
                        $"{nameof(SuperiorEstimationCellLabelCs)}: {Functions.PrepStringArg(arg: SuperiorEstimationCellLabelCs)}; ",
                        $"{nameof(SuperiorEstimationCellLabelEn)}: {Functions.PrepStringArg(arg: SuperiorEstimationCellLabelEn)}; ",
                        $"{nameof(SuperiorEstimationCellDescriptionCs)}: {Functions.PrepStringArg(arg: SuperiorEstimationCellDescriptionCs)}; ",
                        $"{nameof(SuperiorEstimationCellDescriptionEn)}: {Functions.PrepStringArg(arg: SuperiorEstimationCellDescriptionEn)}}}");
                }

                #endregion Methods

            }


            /// <summary>
            /// List of estimation cells with hierarchies
            /// (return type of the stored procedure fn_api_get_estimation_cell_hierarchy)
            /// </summary>
            public class TFnApiGetEstimationCellHierarchyList
                 : AParametrizedView
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema =
                    ADFunctions.FnApiGetEstimationCellHierarchy.SchemaName;

                /// <summary>
                /// Stored procedure name
                /// </summary>
                public static readonly string Name =
                    ADFunctions.FnApiGetEstimationCellHierarchy.Name;

                /// <summary>
                /// Stored procedure alias
                /// </summary>
                public static readonly string Alias =
                    ADFunctions.FnApiGetEstimationCellHierarchy.Name;

                /// <summary>
                /// Description in national language
                /// </summary>
                public static readonly string CaptionCs = String.Concat(
                    "Seznam výpočetních buněk s hierachií ",
                    "(návratový typ uložené procedury fn_api_get_estimation_cell_hierarchy)");

                /// <summary>
                /// Description in English
                /// </summary>
                public static readonly string CaptionEn = String.Concat(
                    "List of estimation cells with hierarchies ",
                    "(return type of the stored procedure fn_api_get_estimation_cell_hierarchy)");

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
                {
                    { "id", new ColumnMetadata()
                    {
                        Name = "id",
                        DbName = "id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = true,
                        NotNull = true,
                        DefaultValue = default(int).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ID",
                        HeaderTextEn = "ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 0
                    }
                    },
                    { "estimation_cell_collection_id", new ColumnMetadata()
                    {
                        Name = "estimation_cell_collection_id",
                        DbName = "estimation_cell_collection_id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATION_CELL_COLLECTION_ID",
                        HeaderTextEn = "ESTIMATION_CELL_COLLECTION_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 1
                    }
                    },
                    { "estimation_cell_id", new ColumnMetadata()
                    {
                        Name = "estimation_cell_id",
                        DbName = "estimation_cell_id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATION_CELL_ID",
                        HeaderTextEn = "ESTIMATION_CELL_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 2
                    }
                    },
                    { "estimation_cell_label_cs", new ColumnMetadata()
                    {
                        Name = "estimation_cell_label_cs",
                        DbName = "estimation_cell_label_cs",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATION_CELL_LABEL_CS",
                        HeaderTextEn = "ESTIMATION_CELL_LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 3
                    }
                    },
                    { "estimation_cell_description_cs", new ColumnMetadata()
                    {
                        Name = "estimation_cell_description_cs",
                        DbName = "estimation_cell_description_cs",
                        DataType = "System.String",
                        DbDataType = "text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATION_CELL_DESCRIPTION_CS",
                        HeaderTextEn = "ESTIMATION_CELL_DESCRIPTION_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 4
                    }
                    },
                    { "estimation_cell_label_en", new ColumnMetadata()
                    {
                        Name = "estimation_cell_label_en",
                        DbName = "estimation_cell_label_en",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATION_CELL_LABEL_EN",
                        HeaderTextEn = "ESTIMATION_CELL_LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 5
                    }
                    },
                    { "estimation_cell_description_en", new ColumnMetadata()
                    {
                        Name = "estimation_cell_description_en",
                        DbName = "estimation_cell_description_en",
                        DataType = "System.String",
                        DbDataType = "text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATION_CELL_DESCRIPTION_EN",
                        HeaderTextEn = "ESTIMATION_CELL_DESCRIPTION_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 6
                    }
                    },
                    { "superior_id", new ColumnMetadata()
                    {
                        Name = "superior_id",
                        DbName = "superior_id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "SUPERIOR_ID",
                        HeaderTextEn = "SUPERIOR_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 7
                    }
                    },
                    { "superior_estimation_cell_collection_id", new ColumnMetadata()
                    {
                        Name = "superior_estimation_cell_collection_id",
                        DbName = "superior_estimation_cell_collection_id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "SUPERIOR_ESTIMATION_CELL_COLLECTION_ID",
                        HeaderTextEn = "SUPERIOR_ESTIMATION_CELL_COLLECTION_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 8
                    }
                    },
                    { "superior_estimation_cell_id", new ColumnMetadata()
                    {
                        Name = "superior_estimation_cell_id",
                        DbName = "superior_estimation_cell_id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "SUPERIOR_ESTIMATION_CELL_ID",
                        HeaderTextEn = "SUPERIOR_ESTIMATION_CELL_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 9
                    }
                    },
                    { "superior_estimation_cell_label_cs", new ColumnMetadata()
                    {
                        Name = "superior_estimation_cell_label_cs",
                        DbName = "superior_estimation_cell_label_cs",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "SUPERIOR_ESTIMATION_CELL_LABEL_CS",
                        HeaderTextEn = "SUPERIOR_ESTIMATION_CELL_LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 10
                    }
                    },
                    { "superior_estimation_cell_description_cs", new ColumnMetadata()
                    {
                        Name = "superior_estimation_cell_description_cs",
                        DbName = "superior_estimation_cell_description_cs",
                        DataType = "System.String",
                        DbDataType = "text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "SUPERIOR_ESTIMATION_CELL_DESCRIPTION_CS",
                        HeaderTextEn = "SUPERIOR_ESTIMATION_CELL_DESCRIPTION_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 11
                    }
                    },
                    { "superior_estimation_cell_label_en", new ColumnMetadata()
                    {
                        Name = "superior_estimation_cell_label_en",
                        DbName = "superior_estimation_cell_label_en",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "SUPERIOR_ESTIMATION_CELL_LABEL_EN",
                        HeaderTextEn = "SUPERIOR_ESTIMATION_CELL_LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 12
                    }
                    },
                    { "superior_estimation_cell_description_en", new ColumnMetadata()
                    {
                        Name = "superior_estimation_cell_description_en",
                        DbName = "superior_estimation_cell_description_en",
                        DataType = "System.String",
                        DbDataType = "text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "SUPERIOR_ESTIMATION_CELL_DESCRIPTION_EN",
                        HeaderTextEn = "SUPERIOR_ESTIMATION_CELL_DESCRIPTION_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 13
                    }
                    }
                };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column estimation_cell_collection_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationCellCollectionId = Cols["estimation_cell_collection_id"];

                /// <summary>
                /// Column estimation_cell_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationCellId = Cols["estimation_cell_id"];

                /// <summary>
                /// Column estimation_cell_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationCellLabelCs = Cols["estimation_cell_label_cs"];

                /// <summary>
                /// Column estimation_cell_description_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationCellDescriptionCs = Cols["estimation_cell_description_cs"];

                /// <summary>
                /// Column estimation_cell_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationCellLabelEn = Cols["estimation_cell_label_en"];

                /// <summary>
                /// Column estimation_cell_description_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationCellDescriptionEn = Cols["estimation_cell_description_en"];

                /// <summary>
                /// Column superior_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColSuperiorId = Cols["superior_id"];

                /// <summary>
                /// Column superior_estimation_cell_collection_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColSuperiorEstimationCellCollectionId = Cols["superior_estimation_cell_collection_id"];

                /// <summary>
                /// Column superior_estimation_cell_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColSuperiorEstimationCellId = Cols["superior_estimation_cell_id"];

                /// <summary>
                /// Column superior_estimation_cell_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColSuperiorEstimationCellLabelCs = Cols["superior_estimation_cell_label_cs"];

                /// <summary>
                /// Column superior_estimation_cell_description_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColSuperiorEstimationCellDescriptionCs = Cols["superior_estimation_cell_description_cs"];

                /// <summary>
                /// Column superior_estimation_cell_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColSuperiorEstimationCellLabelEn = Cols["superior_estimation_cell_label_en"];

                /// <summary>
                /// Column superior_estimation_cell_description_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColSuperiorEstimationCellDescriptionEn = Cols["superior_estimation_cell_description_en"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TFnApiGetEstimationCellHierarchyList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TFnApiGetEstimationCellHierarchyList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// List of items (read-only)
                /// </summary>
                public List<TFnApiGetEstimationCellHierarchy> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new TFnApiGetEstimationCellHierarchy(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexers

                /// <summary>
                /// Row from list of rows by identifier (read-only)
                /// </summary>
                /// <param name="id">Row identifier</param>
                /// <returns>Row from list of rows by identifier (null if not found)</returns>
                public TFnApiGetEstimationCellHierarchy this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new TFnApiGetEstimationCellHierarchy(composite: this, data: a))
                                .FirstOrDefault<TFnApiGetEstimationCellHierarchy>();
                    }
                }

                #endregion Indexers


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public TFnApiGetEstimationCellHierarchyList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any()
                        ? new TFnApiGetEstimationCellHierarchyList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime)
                        : new TFnApiGetEstimationCellHierarchyList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                #endregion Methods

            }

        }
    }
}