﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Runtime.Versioning;
using System.Text.Encodings.Web;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Text.Unicode;
using System.Windows.Forms;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace AuxiliaryData
        {

            /// <summary>
            /// <para lang="cs">Doplňující informace k bodové vrstvě</para>
            /// <para lang="en">Additional information on the point layer</para>
            /// </summary>
            public class PointAdditionalColumns
            {

                #region Private Fields

                /// <summary>
                /// <para lang="cs">Jméno sloupce s identifikátorem klastru</para>
                /// <para lang="en">Column name with cluster identifier</para>
                /// </summary>
                private string cluster;

                /// <summary>
                /// <para lang="cs">Jméno sloupce s identifikátorem země</para>
                /// <para lang="en">Column name with country identifier</para>
                /// </summary>
                private string country;

                /// <summary>
                /// <para lang="cs">Jméno sloupce s identifikátorem panelu</para>
                /// <para lang="en">Column name with panel identifier</para>
                /// </summary>
                private string panel;

                /// <summary>
                /// <para lang="cs">Jméno sloupce s identifikátorem inventarizační plochy</para>
                /// <para lang="en">Column name with inventory plot identifier</para>
                /// </summary>
                private string plot;

                /// <summary>
                /// <para lang="cs">Jméno sloupce s identifikátorem skupiny strat</para>
                /// <para lang="en">Column name with stata set identifier</para>
                /// </summary>
                private string strataSet;

                /// <summary>
                /// <para lang="cs">Jméno sloupce s identifikátorem strata</para>
                /// <para lang="en">Column name with stratum identifier</para>
                /// </summary>
                private string stratum;

                /// <summary>
                /// <para lang="cs">Volby pro JSON serializer</para>
                /// <para lang="en">JSON serializer options</para>
                /// </summary>
                private JsonSerializerOptions serializerOptions;

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// <para lang="cs">Konstruktor</para>
                /// <para lang="en">Constructor</para>
                /// </summary>
                public PointAdditionalColumns()
                {
                    SerializerOptions = null;
                    Cluster = null;
                    Country = null;
                    Panel = null;
                    Plot = null;
                    StrataSet = null;
                    Stratum = null;
                }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// <para lang="cs">Jméno sloupce s identifikátorem klastru</para>
                /// <para lang="en">Column name with cluster identifier</para>
                /// </summary>
                public string Cluster
                {
                    get
                    {
                        return cluster;
                    }
                    set
                    {
                        cluster = value;
                    }
                }

                /// <summary>
                /// <para lang="cs">Jméno sloupce s identifikátorem země</para>
                /// <para lang="en">Column name with country identifier</para>
                /// </summary>
                public string Country
                {
                    get
                    {
                        return country;
                    }
                    set
                    {
                        country = value;
                    }
                }

                /// <summary>
                /// <para lang="cs">Jméno sloupce s identifikátorem panelu</para>
                /// <para lang="en">Column name with panel identifier</para>
                /// </summary>
                public string Panel
                {
                    get
                    {
                        return panel;
                    }
                    set
                    {
                        panel = value;
                    }
                }

                /// <summary>
                /// <para lang="cs">Jméno sloupce s identifikátorem inventarizační plochy</para>
                /// <para lang="en">Column name with inventory plot identifier</para>
                /// </summary>
                public string Plot
                {
                    get
                    {
                        return plot;
                    }
                    set
                    {
                        plot = value;
                    }
                }

                /// <summary>
                /// <para lang="cs">Jméno sloupce s identifikátorem skupiny strat</para>
                /// <para lang="en">Column name with stata set identifier</para>
                /// </summary>
                public string StrataSet
                {
                    get
                    {
                        return strataSet;
                    }
                    set
                    {
                        strataSet = value;
                    }
                }

                /// <summary>
                /// <para lang="cs">Jméno sloupce s identifikátorem strata</para>
                /// <para lang="en">Column name with stratum identifier</para>
                /// </summary>
                public string Stratum
                {
                    get
                    {
                        return stratum;
                    }
                    set
                    {
                        stratum = value;
                    }
                }

                /// <summary>
                /// <para lang="cs">Volby pro JSON serializer</para>
                /// <para lang="en">JSON serializer options</para>
                /// </summary>
                [JsonIgnore]
                private JsonSerializerOptions SerializerOptions
                {
                    get
                    {
                        return serializerOptions ??
                            new()
                            {
                                Encoder = JavaScriptEncoder.Create(allowedRanges: UnicodeRanges.All),
                                WriteIndented = false
                            };
                    }
                    set
                    {
                        serializerOptions = value ??
                            new()
                            {
                                Encoder = JavaScriptEncoder.Create(allowedRanges: UnicodeRanges.All),
                                WriteIndented = false
                            };
                    }
                }

                /// <summary>
                /// <para lang="cs">Serializace objektu do formátu JSON</para>
                /// <para lang="en">Object serialization to JSON format</para>
                /// </summary>
                [JsonIgnore]
                public string JSON
                {
                    get
                    {
                        return JsonSerializer.Serialize(
                            value: this,
                            options: SerializerOptions);
                    }
                }

                #endregion Properties


                #region Static Methods

                /// <summary>
                /// <para lang="cs">Obnovení objektu z formátu JSON</para>
                /// <para lang="en">Restoring an object from JSON format</para>
                /// </summary>
                /// <param name="json">
                /// <para lang="cs">JSON text</para>
                /// <para lang="en">JSON text</para>
                /// </param>
                /// <returns>
                /// <para lang="cs">Doplňující informace k bodové vrstvě</para>
                /// <para lang="en">Additional information on the point layer</para>
                /// </returns>
                [SupportedOSPlatform("windows")]
                public static PointAdditionalColumns Deserialize(string json)
                {
                    if (String.IsNullOrEmpty(value: json))
                    {
                        return new PointAdditionalColumns();
                    }

                    try
                    {
                        return
                            JsonSerializer.Deserialize<PointAdditionalColumns>(json: json);
                    }
                    catch (Exception exception)
                    {
                        MessageBox.Show(
                            text: exception.Message,
                            caption: String.Empty,
                            buttons: MessageBoxButtons.OK,
                            icon: MessageBoxIcon.Exclamation);

                        return new PointAdditionalColumns();
                    }
                }

                /// <summary>
                /// <para lang="cs">Objekt s defaultními názvy sloupců</para>
                /// <para lang="en">Object with default column names</para>
                /// </summary>
                public static PointAdditionalColumns Default()
                {
                    return new PointAdditionalColumns()
                    {
                        Cluster = "cluster",
                        Country = "country",
                        Panel = "panel",
                        Plot = "plot",
                        StrataSet = "strata_set",
                        Stratum = "stratum"
                    };
                }

                #endregion Static Methods


                #region Methods

                /// <summary>
                /// <para lang="cs">Určuje zda zadaný objekt je stejný jako aktuální objekt</para>
                /// <para lang="en">Determines whether specified object is equal to the current object</para>
                /// </summary>
                /// <param name="obj">
                /// <para lang="cs">Zadaný objekt</para>
                /// <para lang="en">Speicified object</para>
                /// </param>
                /// <returns>
                /// <para lang="cs">ano/ne</para>
                /// <para lang="en">true/false</para>
                /// </returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not PointAdditionalColumns type, return False
                    if (obj is not PointAdditionalColumns)
                    {
                        return false;
                    }

                    return
                        (Cluster ?? String.Empty).Equals(
                            obj: ((PointAdditionalColumns)obj).Cluster ?? String.Empty) &&

                        (Country ?? String.Empty).Equals(
                            obj: ((PointAdditionalColumns)obj).Country ?? String.Empty) &&

                        (Panel ?? String.Empty).Equals(
                            obj: ((PointAdditionalColumns)obj).Panel ?? String.Empty) &&

                        (Plot ?? String.Empty).Equals(
                            obj: ((PointAdditionalColumns)obj).Plot ?? String.Empty) &&

                        (StrataSet ?? String.Empty).Equals(
                            obj: ((PointAdditionalColumns)obj).StrataSet ?? String.Empty) &&

                        (Stratum ?? String.Empty).Equals(
                            obj: ((PointAdditionalColumns)obj).Stratum ?? String.Empty);
                }

                /// <summary>
                /// <para lang="cs">Vrací hash code</para>
                /// <para lang="en">Returns the hash code</para>
                /// </summary>
                /// <returns>
                /// <para lang="cs">Vrací hash code</para>
                /// <para lang="en">Returns the hash code</para>
                /// </returns>
                public override int GetHashCode()
                {
                    return
                        ((Cluster == null) ? 0 : Cluster.GetHashCode()) ^
                        ((Country == null) ? 0 : Country.GetHashCode()) ^
                        ((Panel == null) ? 0 : Panel.GetHashCode()) ^
                        ((Plot == null) ? 0 : Plot.GetHashCode()) ^
                        ((StrataSet == null) ? 0 : StrataSet.GetHashCode()) ^
                        ((Stratum == null) ? 0 : Stratum.GetHashCode());
                }

                /// <summary>
                /// <para lang="cs">Textový popis objektu</para>
                /// <para lang="en">Text representation of the object</para>
                /// </summary>
                /// <returns>
                /// <para lang="cs">Textový popis objektu</para>
                /// <para lang="en">Text representation of the object</para>
                /// </returns>
                public override string ToString()
                {
                    return JSON;
                }

                #endregion Methods

            }

        }
    }
}