﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace AuxiliaryData
        {
            // fn_get_aux_total

            /// <summary>
            /// Calculated auxiliary variable totals
            /// (return type of the stored procedure fn_get_aux_total)
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class TFnGetAuxTotal(
                TFnGetAuxTotalList composite = null,
                DataRow data = null)
                    : AParametrizedViewEntry<TFnGetAuxTotalList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TFnGetAuxTotalList.ColId.Name,
                            defaultValue: Int32.Parse(s: TFnGetAuxTotalList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TFnGetAuxTotalList.ColId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Configuration collection identifier
                /// </summary>
                public Nullable<int> ConfigCollectionId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TFnGetAuxTotalList.ColConfigCollectionId.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnGetAuxTotalList.ColConfigCollectionId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TFnGetAuxTotalList.ColConfigCollectionId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TFnGetAuxTotalList.ColConfigCollectionId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Configuration collection object (read-only)
                /// </summary>
                public ConfigCollection ConfigCollection
                {
                    get
                    {
                        return
                            (ConfigCollectionId != null)
                                ? ((NfiEstaDB)Composite.Database)
                                    .SAuxiliaryData.TConfigCollection[(int)ConfigCollectionId]
                                : null;
                    }
                }


                /// <summary>
                /// Configuration identifier
                /// </summary>
                public Nullable<int> ConfigId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TFnGetAuxTotalList.ColConfigId.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnGetAuxTotalList.ColConfigId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TFnGetAuxTotalList.ColConfigId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TFnGetAuxTotalList.ColConfigId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Configuration object (read-only)
                /// </summary>
                public Config Config
                {
                    get
                    {
                        return
                            (ConfigId != null)
                                ? ((NfiEstaDB)Composite.Database)
                                    .SAuxiliaryData.TConfig[(int)ConfigId]
                                : null;
                    }
                }

                /// <summary>
                /// Label of configuration in national language
                /// </summary>
                public string ConfigLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnGetAuxTotalList.ColConfigLabelCs.Name,
                            defaultValue: TFnGetAuxTotalList.ColConfigLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnGetAuxTotalList.ColConfigLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Description of configuration in national language
                /// </summary>
                public string ConfigDescriptionCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnGetAuxTotalList.ColConfigDescriptionCs.Name,
                            defaultValue: TFnGetAuxTotalList.ColConfigDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnGetAuxTotalList.ColConfigDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Extended label of configuration in national language (read-only)
                /// </summary>
                public string ExtendedConfigLabelCs
                {
                    get
                    {
                        return String.IsNullOrEmpty(value: ConfigLabelCs)
                            ? $"{Id}"
                            : $"{Id} - {ConfigLabelCs}";
                    }
                }

                /// <summary>
                /// Label of configuration in English
                /// </summary>
                public string ConfigLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnGetAuxTotalList.ColConfigLabelEn.Name,
                            defaultValue: TFnGetAuxTotalList.ColConfigLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnGetAuxTotalList.ColConfigLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Description of configuration in English
                /// </summary>
                public string ConfigDescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnGetAuxTotalList.ColConfigDescriptionEn.Name,
                            defaultValue: TFnGetAuxTotalList.ColConfigDescriptionEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnGetAuxTotalList.ColConfigDescriptionEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Extended label of configuration in English (read-only)
                /// </summary>
                public string ExtendedConfigLabelEn
                {
                    get
                    {
                        return String.IsNullOrEmpty(value: ConfigLabelEn)
                            ? $"{Id}"
                            : $"{Id} - {ConfigLabelEn}";
                    }
                }

                /// <summary>
                /// Estimation cell identifier
                /// </summary>
                public Nullable<int> EstimationCellId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TFnGetAuxTotalList.ColEstimationCellId.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnGetAuxTotalList.ColEstimationCellId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TFnGetAuxTotalList.ColEstimationCellId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TFnGetAuxTotalList.ColEstimationCellId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Estimation cell object (read-only)
                /// </summary>
                public EstimationCell EstimationCell
                {
                    get
                    {
                        return
                            (EstimationCellId != null)
                                ? ((NfiEstaDB)Composite.Database)
                                    .SAuxiliaryData.CEstimationCell[(int)EstimationCellId]
                                : null;
                    }
                }

                /// <summary>
                /// Label of estimation cell in national language
                /// </summary>
                public string EstimationCellLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnGetAuxTotalList.ColEstimationCellLabelCs.Name,
                            defaultValue: TFnGetAuxTotalList.ColEstimationCellLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnGetAuxTotalList.ColEstimationCellLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Description of estimation cell in national language
                /// </summary>
                public string EstimationCellDescriptionCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnGetAuxTotalList.ColEstimationCellDescriptionCs.Name,
                            defaultValue: TFnGetAuxTotalList.ColEstimationCellDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnGetAuxTotalList.ColEstimationCellDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Extended label of estimation cell in national language (read-only)
                /// </summary>
                public string ExtendedEstimationCellLabelCs
                {
                    get
                    {
                        return String.IsNullOrEmpty(value: EstimationCellLabelCs)
                            ? $"{Id}"
                            : $"{Id} - {EstimationCellLabelCs}";
                    }
                }

                /// <summary>
                /// Extended description of estimation cell in national language (read-only)
                /// </summary>
                public string ExtendedEstimationCellDescriptionCs
                {
                    get
                    {
                        return String.IsNullOrEmpty(value: EstimationCellDescriptionCs)
                            ? $"{Id}"
                            : $"{Id} - {EstimationCellDescriptionCs}";
                    }
                }

                /// <summary>
                /// Label of estimation cell in English
                /// </summary>
                public string EstimationCellLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnGetAuxTotalList.ColEstimationCellLabelEn.Name,
                            defaultValue: TFnGetAuxTotalList.ColEstimationCellLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnGetAuxTotalList.ColEstimationCellLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Description of estimation cell in English
                /// </summary>
                public string EstimationCellDescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnGetAuxTotalList.ColEstimationCellDescriptionEn.Name,
                            defaultValue: TFnGetAuxTotalList.ColEstimationCellDescriptionEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnGetAuxTotalList.ColEstimationCellDescriptionEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Extended label of estimation cell in English (read-only)
                /// </summary>
                public string ExtendedEstimationCellLabelEn
                {
                    get
                    {
                        return String.IsNullOrEmpty(value: EstimationCellLabelEn)
                            ? $"{Id}"
                            : $"{Id} - {EstimationCellLabelEn}";
                    }
                }

                /// <summary>
                /// Extended label of estimation cell in English (read-only)
                /// </summary>
                public string ExtendedEstimationCellDescriptionEn
                {
                    get
                    {
                        return String.IsNullOrEmpty(value: EstimationCellDescriptionEn)
                            ? $"{Id}"
                            : $"{Id} - {EstimationCellDescriptionEn}";
                    }
                }

                /// <summary>
                /// Estimation cell segment identifier
                /// </summary>
                public Nullable<int> CellId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TFnGetAuxTotalList.ColCellId.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnGetAuxTotalList.ColCellId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TFnGetAuxTotalList.ColCellId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TFnGetAuxTotalList.ColCellId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Estimation cell segment object (read-only)
                /// </summary>
                public Cell Cell
                {
                    get
                    {
                        return
                            (CellId != null)
                                ? ((NfiEstaDB)Composite.Database)
                                    .SAuxiliaryData.FaCell[(int)CellId]
                                : null;
                    }
                }


                /// <summary>
                /// Auxiliary variable total
                /// </summary>
                public Nullable<double> AuxTotalValue
                {
                    get
                    {
                        return Functions.GetNDoubleArg(
                            row: Data,
                            name: TFnGetAuxTotalList.ColAuxTotal.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnGetAuxTotalList.ColAuxTotal.DefaultValue) ?
                                (Nullable<double>)null :
                                Double.Parse(s: TFnGetAuxTotalList.ColAuxTotal.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNDoubleArg(
                            row: Data,
                            name: TFnGetAuxTotalList.ColAuxTotal.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Extension version identifier
                /// </summary>
                public Nullable<int> ExtVersionId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TFnGetAuxTotalList.ColExtVersionId.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnGetAuxTotalList.ColExtVersionId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TFnGetAuxTotalList.ColExtVersionId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TFnGetAuxTotalList.ColExtVersionId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Extension version object (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.AuxiliaryData.ExtensionVersion ExtVersion
                {
                    get
                    {
                        return
                            (ExtVersionId != null)
                                ? ((NfiEstaDB)Composite.Database)
                                    .SAuxiliaryData.CExtensionVersion[(int)ExtVersionId]
                                : null;
                    }
                }

                /// <summary>
                /// Label of extension version in national language
                /// </summary>
                public string ExtVersionLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnGetAuxTotalList.ColExtVersionLabelCs.Name,
                            defaultValue: TFnGetAuxTotalList.ColExtVersionLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnGetAuxTotalList.ColExtVersionLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Description of extension version in national language
                /// </summary>
                public string ExtVersionDescriptionCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnGetAuxTotalList.ColExtVersionDescriptionCs.Name,
                            defaultValue: TFnGetAuxTotalList.ColExtVersionDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnGetAuxTotalList.ColExtVersionDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Extended label of extension version in national language (read-only)
                /// </summary>
                public string ExtendedExtVersionLabelCs
                {
                    get
                    {
                        return String.IsNullOrEmpty(value: ExtVersionLabelCs)
                            ? $"{Id}"
                            : $"{Id} - {ExtVersionLabelCs}";
                    }
                }

                /// <summary>
                /// Label of extension version in English
                /// </summary>
                public string ExtVersionLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnGetAuxTotalList.ColExtVersionLabelEn.Name,
                            defaultValue: TFnGetAuxTotalList.ColExtVersionLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnGetAuxTotalList.ColExtVersionLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Description of extension version in English
                /// </summary>
                public string ExtVersionDescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnGetAuxTotalList.ColExtVersionDescriptionEn.Name,
                            defaultValue: TFnGetAuxTotalList.ColExtVersionDescriptionEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnGetAuxTotalList.ColExtVersionDescriptionEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Extended label of extension version in English (read-only)
                /// </summary>
                public string ExtendedExtVersionLabelEn
                {
                    get
                    {
                        return String.IsNullOrEmpty(value: ExtVersionLabelEn)
                            ? $"{Id}"
                            : $"{Id} - {ExtVersionLabelEn}";
                    }
                }


                /// <summary>
                /// Module identifier
                /// </summary>
                public Nullable<int> GuiVersionId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TFnGetAuxTotalList.ColGuiVersionId.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnGetAuxTotalList.ColGuiVersionId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TFnGetAuxTotalList.ColGuiVersionId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TFnGetAuxTotalList.ColGuiVersionId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Module object (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.AuxiliaryData.GUIVersion GuiVersion
                {
                    get
                    {
                        return
                            (GuiVersionId != null)
                                ? ((NfiEstaDB)Composite.Database)
                                    .SAuxiliaryData.CGUIVersion[(int)GuiVersionId]
                                : null;
                    }
                }

                /// <summary>
                /// Label of module in national language
                /// </summary>
                public string GuiVersionLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnGetAuxTotalList.ColGuiVersionLabelCs.Name,
                            defaultValue: TFnGetAuxTotalList.ColGuiVersionLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnGetAuxTotalList.ColGuiVersionLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Description of module in national language
                /// </summary>
                public string GuiVersionDescriptionCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnGetAuxTotalList.ColGuiVersionDescriptionCs.Name,
                            defaultValue: TFnGetAuxTotalList.ColGuiVersionDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnGetAuxTotalList.ColGuiVersionDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Extended label of module in national language (read-only)
                /// </summary>
                public string ExtendedGuiVersionLabelCs
                {
                    get
                    {
                        return String.IsNullOrEmpty(value: GuiVersionLabelCs)
                            ? $"{Id}"
                            : $"{Id} - {GuiVersionLabelCs}";
                    }
                }

                /// <summary>
                /// Label of module in English
                /// </summary>
                public string GuiVersionLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnGetAuxTotalList.ColGuiVersionLabelEn.Name,
                            defaultValue: TFnGetAuxTotalList.ColGuiVersionLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnGetAuxTotalList.ColGuiVersionLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Description of module in English
                /// </summary>
                public string GuiVersionDescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnGetAuxTotalList.ColGuiVersionDescriptionEn.Name,
                            defaultValue: TFnGetAuxTotalList.ColGuiVersionDescriptionEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnGetAuxTotalList.ColGuiVersionDescriptionEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Extended label of module in English (read-only)
                /// </summary>
                public string ExtendedGuiVersionLabelEn
                {
                    get
                    {
                        return String.IsNullOrEmpty(value: GuiVersionLabelEn)
                            ? $"{Id}"
                            : $"{Id} - {GuiVersionLabelEn}";
                    }
                }


                /// <summary>
                /// Date and time of calculation of the auxiliary variable total
                /// </summary>
                public DateTime EstDate
                {
                    get
                    {
                        return Functions.GetDateTimeArg(
                            row: Data,
                            name: TFnGetAuxTotalList.ColEstDate.Name,
                            defaultValue: DateTime.Parse(s: TFnGetAuxTotalList.ColEstDate.DefaultValue));
                    }
                    set
                    {
                        Functions.SetDateTimeArg(
                            row: Data,
                            name: TFnGetAuxTotalList.ColEstDate.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Date and time of calculation of the auxiliary variable total as text
                /// </summary>
                public string EstDateText
                {
                    get
                    {
                        return
                            EstDate.ToString(
                                format: TFnGetAuxTotalList.ColEstDate.NumericFormat);
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true|false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not TFnGetAuxTotal, return False
                    if (obj is not TFnGetAuxTotal)
                    {
                        return false;
                    }

                    return
                        Id == ((TFnGetAuxTotal)obj).Id;
                }

                /// <summary>
                /// Returns a hash code for the current object
                /// </summary>
                /// <returns>Hash code for the current object</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                /// <summary>
                /// Returns a string that represents the current object
                /// </summary>
                /// <returns>String that represents the current object</returns>
                public override string ToString()
                {
                    return String.Concat(
                       $"{nameof(TFnGetAuxTotal)}: {{",
                       $"{nameof(Id)}: {Functions.PrepIntArg(arg: Id)}; ",
                       $"{nameof(ConfigId)}: {Functions.PrepNIntArg(arg: ConfigId)}; ",
                       $"{nameof(EstimationCellId)}: {Functions.PrepNIntArg(arg: EstimationCellId)}; ",
                       $"{nameof(AuxTotalValue)}: {Functions.PrepNDoubleArg(arg: AuxTotalValue)}; ",
                       $"{nameof(ExtVersionId)}: {Functions.PrepNIntArg(arg: ExtVersionId)}; ",
                       $"{nameof(GuiVersionId)}: {Functions.PrepNIntArg(arg: GuiVersionId)}; ",
                       $"{nameof(EstDate)}: {Functions.PrepStringArg(arg: EstDateText)}}}");
                }

                #endregion Methods

            }


            /// <summary>
            /// List of calculated auxiliary variable totals
            /// (return type of the stored procedure fn_get_aux_total)
            /// </summary>
            public class TFnGetAuxTotalList
                 : AParametrizedView
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema =
                    ADFunctions.FnGetAuxTotal.SchemaName;

                /// <summary>
                /// Stored procedure name
                /// </summary>
                public static readonly string Name =
                    ADFunctions.FnGetAuxTotal.Name;

                /// <summary>
                /// Stored procedure alias
                /// </summary>
                public static readonly string Alias =
                    ADFunctions.FnGetAuxTotal.Name;

                /// <summary>
                /// Description in national language
                /// </summary>
                public static readonly string CaptionCs = String.Concat(
                    "Seznam vypočtených úhrnů pomocné proměnné ",
                    "(návratový typ uložené procedury fn_get_aux_total)");

                /// <summary>
                /// Description in English
                /// </summary>
                public static readonly string CaptionEn = String.Concat(
                    "List of calculated auxiliary variable totals ",
                    "(return type of the stored procedure fn_get_aux_total)");

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
                {
                    { "id", new ColumnMetadata()
                    {
                        Name = "id",
                        DbName = "id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = true,
                        NotNull = true,
                        DefaultValue = default(int).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ID",
                        HeaderTextEn = "ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 0
                    }
                    },
                    { "config_collection_id", new ColumnMetadata()
                    {
                        Name = "config_collection_id",
                        DbName = "config_collection_id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "CONFIG_COLLECTION_ID",
                        HeaderTextEn = "CONFIG_COLLECTION_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 1
                    }
                    },
                    { "config_id", new ColumnMetadata()
                    {
                        Name = "config_id",
                        DbName = "config_id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "CONFIG_ID",
                        HeaderTextEn = "CONFIG_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 2
                    }
                    },
                    { "config_label_cs", new ColumnMetadata()
                    {
                        Name = "config_label_cs",
                        DbName = "config_label_cs",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "CONFIG_LABEL_CS",
                        HeaderTextEn = "CONFIG_LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 3
                    }
                    },
                    { "config_description_cs", new ColumnMetadata()
                    {
                        Name = "config_description_cs",
                        DbName = "config_description_cs",
                        DataType = "System.String",
                        DbDataType = "text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "CONFIG_DESCRIPTION_CS",
                        HeaderTextEn = "CONFIG_DESCRIPTION_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 4
                    }
                    },
                    { "config_label_en", new ColumnMetadata()
                    {
                        Name = "config_label_en",
                        DbName = "config_label_en",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "CONFIG_LABEL_EN",
                        HeaderTextEn = "CONFIG_LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 5
                    }
                    },
                    { "config_description_en", new ColumnMetadata()
                    {
                        Name = "config_description_en",
                        DbName = "config_description_en",
                        DataType = "System.String",
                        DbDataType = "text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "CONFIG_DESCRIPTION_EN",
                        HeaderTextEn = "CONFIG_DESCRIPTION_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 6
                    }
                    },
                    { "estimation_cell_id", new ColumnMetadata()
                    {
                        Name = "estimation_cell_id",
                        DbName = "estimation_cell_id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATION_CELL_ID",
                        HeaderTextEn = "ESTIMATION_CELL_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 7
                    }
                    },
                    { "estimation_cell_label_cs", new ColumnMetadata()
                    {
                        Name = "estimation_cell_label_cs",
                        DbName = "estimation_cell_label_cs",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATION_CELL_LABEL_CS",
                        HeaderTextEn = "ESTIMATION_CELL_LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 8
                    }
                    },
                    { "estimation_cell_description_cs", new ColumnMetadata()
                    {
                        Name = "estimation_cell_description_cs",
                        DbName = "estimation_cell_description_cs",
                        DataType = "System.String",
                        DbDataType = "text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATION_CELL_DESCRIPTION_CS",
                        HeaderTextEn = "ESTIMATION_CELL_DESCRIPTION_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 9
                    }
                    },
                    { "estimation_cell_label_en", new ColumnMetadata()
                    {
                        Name = "estimation_cell_label_en",
                        DbName = "estimation_cell_label_en",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATION_CELL_LABEL_EN",
                        HeaderTextEn = "ESTIMATION_CELL_LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 10
                    }
                    },
                    { "estimation_cell_description_en", new ColumnMetadata()
                    {
                        Name = "estimation_cell_description_en",
                        DbName = "estimation_cell_description_en",
                        DataType = "System.String",
                        DbDataType = "text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATION_CELL_DESCRIPTION_EN",
                        HeaderTextEn = "ESTIMATION_CELL_DESCRIPTION_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 11
                    }
                    },
                    { "cell_id", new ColumnMetadata()
                    {
                        Name = "cell_id",
                        DbName = "cell_id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "CELL_ID",
                        HeaderTextEn = "CELL_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 12
                    }
                    },
                    { "aux_total", new ColumnMetadata()
                    {
                        Name = "aux_total",
                        DbName = "aux_total",
                        DataType = "System.Double",
                        DbDataType = "float8",
                        NewDataType = "float8",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "N3",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "AUX_TOTAL",
                        HeaderTextEn = "AUX_TOTAL",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 13
                    }
                    },
                    { "ext_version_id", new ColumnMetadata()
                    {
                        Name = "ext_version_id",
                        DbName = "ext_version_id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "EXT_VERSION_ID",
                        HeaderTextEn = "EXT_VERSION_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 14
                    }
                    },
                    { "ext_version_label_cs", new ColumnMetadata()
                    {
                        Name = "ext_version_label_cs",
                        DbName = "ext_version_label_cs",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "EXT_LABEL_CS",
                        HeaderTextEn = "EXT_LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 15
                    }
                    },
                    { "ext_version_description_cs", new ColumnMetadata()
                    {
                        Name = "ext_version_description_cs",
                        DbName = "ext_version_description_cs",
                        DataType = "System.String",
                        DbDataType = "text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "EXT_DESCRIPTION_CS",
                        HeaderTextEn = "EXT_DESCRIPTION_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 16
                    }
                    },
                    { "ext_version_label_en", new ColumnMetadata()
                    {
                        Name = "ext_version_label_en",
                        DbName = "ext_version_label_en",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "EXT_LABEL_EN",
                        HeaderTextEn = "EXT_LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 17
                    }
                    },
                    { "ext_version_description_en", new ColumnMetadata()
                    {
                        Name = "ext_version_description_en",
                        DbName = "ext_version_description_en",
                        DataType = "System.String",
                        DbDataType = "text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "EXT_DESCRIPTION_EN",
                        HeaderTextEn = "EXT_DESCRIPTION_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 18
                    }
                    },
                    { "gui_version_id", new ColumnMetadata()
                    {
                        Name = "gui_version_id",
                        DbName = "gui_version_id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "GUI_VERSION_ID",
                        HeaderTextEn = "GUI_VERSION_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 19
                    }
                    },
                    { "gui_version_label_cs", new ColumnMetadata()
                    {
                        Name = "gui_version_label_cs",
                        DbName = "gui_version_label_cs",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "GUI_LABEL_CS",
                        HeaderTextEn = "GUI_LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 20
                    }
                    },
                    { "gui_version_description_cs", new ColumnMetadata()
                    {
                        Name = "gui_version_description_cs",
                        DbName = "gui_version_description_cs",
                        DataType = "System.String",
                        DbDataType = "text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "GUI_DESCRIPTION_CS",
                        HeaderTextEn = "GUI_DESCRIPTION_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 21
                    }
                    },
                    { "gui_version_label_en", new ColumnMetadata()
                    {
                        Name = "gui_version_label_en",
                        DbName = "gui_version_label_en",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "GUI_LABEL_EN",
                        HeaderTextEn = "GUI_LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 22
                    }
                    },
                    { "gui_version_description_en", new ColumnMetadata()
                    {
                        Name = "gui_version_description_en",
                        DbName = "gui_version_description_en",
                        DataType = "System.String",
                        DbDataType = "text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "GUI_DESCRIPTION_EN",
                        HeaderTextEn = "GUI_DESCRIPTION_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 23
                    }
                    },
                    { "est_date", new ColumnMetadata()
                    {
                        Name = "est_date",
                        DbName = "est_date",
                        DataType = "System.DateTime",
                        DbDataType = "timestamp",
                        NewDataType = "timestamp",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = new DateTime().ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "yyyy-MM-dd HH:mm:ss",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "EST_DATE",
                        HeaderTextEn = "EST_DATE",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 24
                    }
                    }
                };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column config_collection_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColConfigCollectionId = Cols["config_collection_id"];

                /// <summary>
                /// Column config_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColConfigId = Cols["config_id"];

                /// <summary>
                /// Column config_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColConfigLabelCs = Cols["config_label_cs"];

                /// <summary>
                /// Column config_description_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColConfigDescriptionCs = Cols["config_description_cs"];

                /// <summary>
                /// Column config_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColConfigLabelEn = Cols["config_label_en"];

                /// <summary>
                /// Column config_description_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColConfigDescriptionEn = Cols["config_description_en"];

                /// <summary>
                /// Column estimation_cell_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationCellId = Cols["estimation_cell_id"];

                /// <summary>
                /// Column estimation_cell_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationCellLabelCs = Cols["estimation_cell_label_cs"];

                /// <summary>
                /// Column estimation_cell_description_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationCellDescriptionCs = Cols["estimation_cell_description_cs"];

                /// <summary>
                /// Column estimation_cell_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationCellLabelEn = Cols["estimation_cell_label_en"];

                /// <summary>
                /// Column estimation_cell_description_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationCellDescriptionEn = Cols["estimation_cell_description_en"];

                /// <summary>
                /// Column cell_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColCellId = Cols["cell_id"];

                /// <summary>
                /// Column aux_total metadata
                /// </summary>
                public static readonly ColumnMetadata ColAuxTotal = Cols["aux_total"];

                /// <summary>
                /// Column ext_version_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColExtVersionId = Cols["ext_version_id"];

                /// <summary>
                /// Column ext_version_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColExtVersionLabelCs = Cols["ext_version_label_cs"];

                /// <summary>
                /// Column ext_version_description_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColExtVersionDescriptionCs = Cols["ext_version_description_cs"];

                /// <summary>
                /// Column ext_version_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColExtVersionLabelEn = Cols["ext_version_label_en"];

                /// <summary>
                /// Column ext_version_description_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColExtVersionDescriptionEn = Cols["ext_version_description_en"];

                /// <summary>
                /// Column gui_version_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColGuiVersionId = Cols["gui_version_id"];

                /// <summary>
                /// Column gui_version_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColGuiVersionLabelCs = Cols["gui_version_label_cs"];

                /// <summary>
                /// Column gui_version_description_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColGuiVersionDescriptionCs = Cols["gui_version_description_cs"];

                /// <summary>
                /// Column gui_version_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColGuiVersionLabelEn = Cols["gui_version_label_en"];

                /// <summary>
                /// Column gui_version_description_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColGuiVersionDescriptionEn = Cols["gui_version_description_en"];

                /// <summary>
                /// Column est_date metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstDate = Cols["est_date"];


                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Private Fields

                /// <summary>
                /// 1st parameter: List of configuration identifiers
                /// </summary>
                private List<int> configurationIds;

                /// <summary>
                /// 2nd parameter: List of estimation cell identifiers
                /// </summary>
                private List<int> estimationCellIds;

                /// <summary>
                /// 3rd parameter: List of extension version identifiers
                /// </summary>
                private List<int> extensionVersionIds;

                /// <summary>
                /// 4th parameter: List of gui version identifiers
                /// </summary>
                private List<int> guiVersionIds;

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TFnGetAuxTotalList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                {
                    ConfigurationIds = null;
                    EstimationCellIds = null;
                    ExtensionVersionIds = null;
                    GuiVersionIds = null;
                }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TFnGetAuxTotalList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                {
                    ConfigurationIds = null;
                    EstimationCellIds = null;
                    ExtensionVersionIds = null;
                    GuiVersionIds = null;
                }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// 1st parameter: List of configuration identifiers
                /// </summary>
                public List<int> ConfigurationIds
                {
                    get
                    {
                        return configurationIds;
                    }
                    set
                    {
                        configurationIds = value;
                    }
                }

                /// <summary>
                /// 2nd parameter: List of estimation cell identifiers
                /// </summary>
                public List<int> EstimationCellIds
                {
                    get
                    {
                        return estimationCellIds;
                    }
                    set
                    {
                        estimationCellIds = value;
                    }
                }

                /// <summary>
                /// 3rd parameter: List of extension version identifiers
                /// </summary>
                public List<int> ExtensionVersionIds
                {
                    get
                    {
                        return extensionVersionIds;
                    }
                    set
                    {
                        extensionVersionIds = value;
                    }
                }

                /// <summary>
                /// 4th parameter: List of gui version identifiers
                /// </summary>
                public List<int> GuiVersionIds
                {
                    get
                    {
                        return guiVersionIds;
                    }
                    set
                    {
                        guiVersionIds = value;
                    }
                }

                /// <summary>
                /// List of items (read-only)
                /// </summary>
                public List<TFnGetAuxTotal> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new TFnGetAuxTotal(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexers

                /// <summary>
                /// Row from list of rows by identifier (read-only)
                /// </summary>
                /// <param name="id">Row identifier</param>
                /// <returns>Row from list of rows by identifier (null if not found)</returns>
                public TFnGetAuxTotal this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new TFnGetAuxTotal(composite: this, data: a))
                                .FirstOrDefault<TFnGetAuxTotal>();
                    }
                }

                #endregion Indexers


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public TFnGetAuxTotalList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any()
                        ? new TFnGetAuxTotalList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime)
                        {
                            ConfigurationIds = ConfigurationIds,
                            EstimationCellIds = EstimationCellIds,
                            ExtensionVersionIds = ExtensionVersionIds,
                            GuiVersionIds = GuiVersionIds
                        }
                        : new TFnGetAuxTotalList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null)
                        {
                            ConfigurationIds = ConfigurationIds,
                            EstimationCellIds = EstimationCellIds,
                            ExtensionVersionIds = ExtensionVersionIds,
                            GuiVersionIds = GuiVersionIds
                        };
                }

                /// <summary>
                /// List of items for selected extension version, gui version and estimation cell
                /// </summary>
                /// <param name="extVersionId">Extension version identifier (null or 0 means altogether)</param>
                /// <param name="guiVersionId">Gui version identifier (null or 0 means altogether)</param>
                /// <param name="estimationCellId">Estimation cell identifier (null or 0 means altogether)</param>
                /// <returns>List of items for selected extension version, gui version and estimation cell</returns>
                public IEnumerable<TFnGetAuxTotal> GetItems(
                    Nullable<int> extVersionId,
                    Nullable<int> guiVersionId,
                    Nullable<int> estimationCellId)
                {
                    // null a 0 označují kategorii bez rozlišení
                    extVersionId ??= 0;
                    guiVersionId ??= 0;
                    estimationCellId ??= 0;

                    if (extVersionId == 0)
                    {
                        if (guiVersionId == 0)
                        {
                            if (estimationCellId == 0)
                            {
                                return
                                Data.AsEnumerable()
                                    .Select(a => new TFnGetAuxTotal(composite: this, data: a));
                            }
                            else
                            {
                                return
                                Data.AsEnumerable()
                                    .Where(a => (a.Field<Nullable<int>>(columnName: ColEstimationCellId.Name) ?? -1) == estimationCellId)
                                    .Select(a => new TFnGetAuxTotal(composite: this, data: a));
                            }
                        }
                        else
                        {
                            if (estimationCellId == 0)
                            {
                                return
                                Data.AsEnumerable()
                                    .Where(a => (a.Field<Nullable<int>>(columnName: ColGuiVersionId.Name) ?? -1) == guiVersionId)
                                    .Select(a => new TFnGetAuxTotal(composite: this, data: a));
                            }
                            else
                            {
                                return
                                Data.AsEnumerable()
                                    .Where(a => (a.Field<Nullable<int>>(columnName: ColGuiVersionId.Name) ?? -1) == guiVersionId)
                                    .Where(a => (a.Field<Nullable<int>>(columnName: ColEstimationCellId.Name) ?? -1) == estimationCellId)
                                    .Select(a => new TFnGetAuxTotal(composite: this, data: a));
                            }
                        }
                    }
                    else
                    {
                        if (guiVersionId == 0)
                        {
                            if (estimationCellId == 0)
                            {
                                return
                                Data.AsEnumerable()
                                    .Where(a => (a.Field<Nullable<int>>(columnName: ColExtVersionId.Name) ?? -1) == extVersionId)
                                    .Select(a => new TFnGetAuxTotal(composite: this, data: a));
                            }
                            else
                            {
                                return
                                Data.AsEnumerable()
                                    .Where(a => (a.Field<Nullable<int>>(columnName: ColExtVersionId.Name) ?? -1) == extVersionId)
                                    .Where(a => (a.Field<Nullable<int>>(columnName: ColEstimationCellId.Name) ?? -1) == estimationCellId)
                                    .Select(a => new TFnGetAuxTotal(composite: this, data: a));
                            }
                        }
                        else
                        {
                            if (estimationCellId == 0)
                            {
                                return
                                Data.AsEnumerable()
                                    .Where(a => (a.Field<Nullable<int>>(columnName: ColExtVersionId.Name) ?? -1) == extVersionId)
                                    .Where(a => (a.Field<Nullable<int>>(columnName: ColGuiVersionId.Name) ?? -1) == guiVersionId)
                                    .Select(a => new TFnGetAuxTotal(composite: this, data: a));
                            }
                            else
                            {
                                return
                                Data.AsEnumerable()
                                    .Where(a => (a.Field<Nullable<int>>(columnName: ColExtVersionId.Name) ?? -1) == extVersionId)
                                    .Where(a => (a.Field<Nullable<int>>(columnName: ColGuiVersionId.Name) ?? -1) == guiVersionId)
                                    .Where(a => (a.Field<Nullable<int>>(columnName: ColEstimationCellId.Name) ?? -1) == estimationCellId)
                                    .Select(a => new TFnGetAuxTotal(composite: this, data: a));
                            }
                        }
                    }
                }

                #endregion Methods

            }

        }
    }
}