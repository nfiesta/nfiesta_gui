﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace AuxiliaryData
        {
            // fn_get_aux_data_app

            /// <summary>
            /// Data for insert into the table t_auxiliary_data
            /// (return type of the stored procedure fn_get_aux_data_app)
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class TFnGetAuxDataApp(
                TFnGetAuxDataAppList composite = null,
                DataRow data = null)
                    : AParametrizedViewEntry<TFnGetAuxDataAppList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TFnGetAuxDataAppList.ColId.Name,
                            defaultValue: Int32.Parse(s: TFnGetAuxDataAppList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TFnGetAuxDataAppList.ColId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Configuration collection identifier
                /// </summary>
                public Nullable<int> ConfigCollectionId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TFnGetAuxDataAppList.ColConfigCollectionId.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnGetAuxDataAppList.ColConfigCollectionId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TFnGetAuxDataAppList.ColConfigCollectionId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TFnGetAuxDataAppList.ColConfigCollectionId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Configuration collection object (read-only)
                /// </summary>
                public ConfigCollection ConfigCollection
                {
                    get
                    {
                        return
                            (ConfigCollectionId != null)
                                ? ((NfiEstaDB)Composite.Database)
                                    .SAuxiliaryData.TConfigCollection[(int)ConfigCollectionId]
                                : null;
                    }
                }


                /// <summary>
                /// Configuration for the auxiliary variable totals calculation identifier
                /// </summary>
                public Nullable<int> ConfigId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TFnGetAuxDataAppList.ColConfigId.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnGetAuxDataAppList.ColConfigId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TFnGetAuxDataAppList.ColConfigId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TFnGetAuxDataAppList.ColConfigId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Configuration for the auxiliary variable totals calculation object (read-only)
                /// </summary>
                public Config Config
                {
                    get
                    {
                        return
                            (ConfigId != null)
                                ? ((NfiEstaDB)Composite.Database)
                                    .SAuxiliaryData.TConfig[(int)ConfigId]
                                : null;
                    }
                }


                /// <summary>
                /// Point identification
                /// </summary>
                public Nullable<int> Gid
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TFnGetAuxDataAppList.ColGid.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnGetAuxDataAppList.ColGid.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TFnGetAuxDataAppList.ColGid.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TFnGetAuxDataAppList.ColGid.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Point object (read-only)
                /// </summary>
                public Plot Plot
                {
                    get
                    {
                        return
                            (Gid != null)
                                ? ((NfiEstaDB)Composite.Database)
                                    .SAuxiliaryData.FpPlot[(int)Gid]
                                : null;
                    }
                }


                /// <summary>
                /// Auxiliary variable value
                /// </summary>
                public Nullable<double> AuxValue
                {
                    get
                    {
                        return Functions.GetNDoubleArg(
                            row: Data,
                            name: TFnGetAuxDataAppList.ColGid.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnGetAuxDataAppList.ColGid.DefaultValue) ?
                                (Nullable<double>)null :
                                Double.Parse(s: TFnGetAuxDataAppList.ColGid.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNDoubleArg(
                            row: Data,
                            name: TFnGetAuxDataAppList.ColGid.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Database extension version identifier
                /// </summary>
                public Nullable<int> ExtensionVersionId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TFnGetAuxDataAppList.ColExtensionVersionId.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnGetAuxDataAppList.ColExtensionVersionId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TFnGetAuxDataAppList.ColExtensionVersionId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TFnGetAuxDataAppList.ColExtensionVersionId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Database extension version object (read-only)
                /// </summary>
                public ExtensionVersion ExtensionVersion
                {
                    get
                    {
                        return
                            (ExtensionVersionId != null)
                                ? ((NfiEstaDB)Composite.Database)
                                    .SAuxiliaryData.CExtensionVersion[(int)ExtensionVersionId]
                                : null;
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true|false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not TFnGetAuxDataApp, return False
                    if (obj is not TFnGetAuxDataApp)
                    {
                        return false;
                    }

                    return
                        Id == ((TFnGetAuxDataApp)obj).Id;
                }

                /// <summary>
                /// Returns a hash code for the current object
                /// </summary>
                /// <returns>Hash code for the current object</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                /// <summary>
                /// Returns a string that represents the current object
                /// </summary>
                /// <returns>String that represents the current object</returns>
                public override string ToString()
                {
                    return String.Concat(
                        $"{nameof(TFnGetAuxDataApp)}: {{",
                        $"{nameof(Id)}: {Functions.PrepIntArg(arg: Id)}; ",
                        $"{nameof(ConfigCollectionId)}: {Functions.PrepNIntArg(arg: ConfigCollectionId)}; ",
                        $"{nameof(ConfigId)}: {Functions.PrepNIntArg(arg: ConfigId)}; ",
                        $"{nameof(Gid)}: {Functions.PrepNIntArg(arg: Gid)}; ",
                        $"{nameof(AuxValue)}: {Functions.PrepNDoubleArg(arg: AuxValue)}; ",
                        $"{nameof(ExtensionVersionId)}: {Functions.PrepNIntArg(arg: ExtensionVersionId)}}}");
                }

                #endregion Methods

            }


            /// <summary>
            /// List of data for insert into the table t_auxiliary_data
            /// (return type of the stored procedure fn_get_aux_data_app)
            /// </summary>
            public class TFnGetAuxDataAppList
                 : AParametrizedView
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema =
                    ADFunctions.FnGetAuxDataApp.SchemaName;

                /// <summary>
                /// Stored procedure name
                /// </summary>
                public static readonly string Name =
                    ADFunctions.FnGetAuxDataApp.Name;

                /// <summary>
                /// Stored procedure alias
                /// </summary>
                public static readonly string Alias =
                    ADFunctions.FnGetAuxDataApp.Name;

                /// <summary>
                /// Description in national language
                /// </summary>
                public static readonly string CaptionCs = String.Concat(
                    "Data pro insert do tabulky t_auxiliary_data ",
                    "(návratový typ uložené procedury fn_get_aux_data_app)");

                /// <summary>
                /// Description in English
                /// </summary>
                public static readonly string CaptionEn = String.Concat(
                    "List of data for insert into the table t_auxiliary_data ",
                    "(return type of the stored procedure fn_get_aux_data_app)");

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
                {
                    { "id", new ColumnMetadata()
                    {
                        Name = "id",
                        DbName = "id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = true,
                        NotNull = true,
                        DefaultValue = default(int).ToString(),
                        FuncCall = "row_number() over()",
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ID",
                        HeaderTextEn = "ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 0
                    }
                    },
                    { "config_collection_id", new ColumnMetadata()
                    {
                        Name = "config_collection_id",
                        DbName = "config_collection",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "CONFIG_COLLECTION_ID",
                        HeaderTextEn = "CONFIG_COLLECTION_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 1
                    }
                    },
                    { "config_id", new ColumnMetadata()
                    {
                        Name = "config_id",
                        DbName = "config",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "CONFIG_ID",
                        HeaderTextEn = "CONFIG_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 2
                    }
                    },
                    { "gid", new ColumnMetadata()
                    {
                        Name = "gid",
                        DbName = "gid",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "GID",
                        HeaderTextEn = "GID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 3
                    }
                    },
                    { "aux_value", new ColumnMetadata()
                    {
                        Name = "aux_value",
                        DbName = "value",
                        DataType = "System.Double",
                        DbDataType = "double precision",
                        NewDataType = "double precision",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "N3",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "AUX_VALUE",
                        HeaderTextEn = "AUX_VALUE",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 4
                    }
                    },
                    { "extension_version_id", new ColumnMetadata()
                    {
                        Name = "extension_version_id",
                        DbName = "ext_version",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "EXTENSION_VERSION_ID",
                        HeaderTextEn = "EXTENSION_VERSION_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 5
                    }
                    }
                };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column config_collection_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColConfigCollectionId = Cols["config_collection_id"];

                /// <summary>
                /// Column config_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColConfigId = Cols["config_id"];

                /// <summary>
                /// Column gid metadata
                /// </summary>
                public static readonly ColumnMetadata ColGid = Cols["gid"];

                /// <summary>
                /// Column aux_value metadata
                /// </summary>
                public static readonly ColumnMetadata ColAuxValue = Cols["aux_value"];

                /// <summary>
                /// Column extension_version_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColExtensionVersionId = Cols["extension_version_id"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Private Fields

                /// <summary>
                /// 1st parameter: Configuration collection for auxiliary variable identifier
                /// </summary>
                private Nullable<int> configCollectionId;

                /// <summary>
                /// 2nd parameter: Configuration for auxiliary variable category identifier
                /// </summary>
                private Nullable<int> configId;

                /// <summary>
                /// 3rd parameter: Intersects (true/false)
                /// </summary>
                private Nullable<bool> intersects;

                /// <summary>
                /// 4th parameter: First estimation cell segment identifier
                /// </summary>
                private Nullable<int> gidStart;

                /// <summary>
                /// 54th parameter: Last estimation cell segment identifier
                /// </summary>
                private Nullable<int> gidEnd;

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TFnGetAuxDataAppList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                {
                    ConfigCollectionId = null;
                    ConfigId = null;
                    Intersects = null;
                    GidStart = null;
                    GidEnd = null;
                }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TFnGetAuxDataAppList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                {
                    ConfigCollectionId = null;
                    ConfigId = null;
                    Intersects = null;
                    GidStart = null;
                    GidEnd = null;
                }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// 1st parameter: Configuration collection for auxiliary variable identifier
                /// </summary>
                public Nullable<int> ConfigCollectionId
                {
                    get
                    {
                        return configCollectionId;
                    }
                    set
                    {
                        configCollectionId = value;
                    }
                }

                /// <summary>
                /// 2nd parameter: Configuration for auxiliary variable category identifier
                /// </summary>
                public Nullable<int> ConfigId
                {
                    get
                    {
                        return configId;
                    }
                    set
                    {
                        configId = value;
                    }
                }

                /// <summary>
                /// 3rd parameter: Intersects (true/false)
                /// </summary>
                public Nullable<bool> Intersects
                {
                    get
                    {
                        return intersects;
                    }
                    set
                    {
                        intersects = value;
                    }
                }

                /// <summary>
                /// 4th parameter: First estimation cell segment identifier
                /// </summary>
                public Nullable<int> GidStart
                {
                    get
                    {
                        return gidStart;
                    }
                    set
                    {
                        gidStart = value;
                    }
                }

                /// <summary>
                /// 54th parameter: Last estimation cell segment identifier
                /// </summary>
                public Nullable<int> GidEnd
                {
                    get
                    {
                        return gidEnd;
                    }
                    set
                    {
                        gidEnd = value;
                    }
                }

                /// <summary>
                /// List of items (read-only)
                /// </summary>
                public List<TFnGetAuxDataApp> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new TFnGetAuxDataApp(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexers

                /// <summary>
                /// Row from list of rows by identifier (read-only)
                /// </summary>
                /// <param name="id">Row identifier</param>
                /// <returns>Row from list of rows by identifier (null if not found)</returns>
                public TFnGetAuxDataApp this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new TFnGetAuxDataApp(composite: this, data: a))
                                .FirstOrDefault<TFnGetAuxDataApp>();
                    }
                }

                #endregion Indexers


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public TFnGetAuxDataAppList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any()
                        ? new TFnGetAuxDataAppList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime)
                        {
                            ConfigCollectionId = ConfigCollectionId,
                            ConfigId = ConfigId,
                            Intersects = Intersects,
                            GidStart = GidStart,
                            GidEnd = GidEnd
                        }
                        : new TFnGetAuxDataAppList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null)
                        {
                            ConfigCollectionId = ConfigCollectionId,
                            ConfigId = ConfigId,
                            Intersects = Intersects,
                            GidStart = GidStart,
                            GidEnd = GidEnd
                        };
                }

                #endregion Methods

            }

        }
    }
}