﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace AuxiliaryData
        {
            // fn_get_gids4aux_total_app

            /// <summary>
            /// Estimation cell segments for auxiliary variable total calculation
            /// (return type of the stored procedure fn_get_gids4aux_total_app)
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class TFnGetGidsForAuxTotalApp(
                TFnGetGidsForAuxTotalAppList composite = null,
                DataRow data = null)
                    : AParametrizedViewEntry<TFnGetGidsForAuxTotalAppList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TFnGetGidsForAuxTotalAppList.ColId.Name,
                            defaultValue: Int32.Parse(s: TFnGetGidsForAuxTotalAppList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TFnGetGidsForAuxTotalAppList.ColId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Configuration identifier
                /// </summary>
                public Nullable<int> ConfigId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TFnGetGidsForAuxTotalAppList.ColConfigId.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnGetGidsForAuxTotalAppList.ColConfigId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TFnGetGidsForAuxTotalAppList.ColConfigId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TFnGetGidsForAuxTotalAppList.ColConfigId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Configuration object (read-only)
                /// </summary>
                public Config Config
                {
                    get
                    {
                        return
                            (ConfigId != null)
                                ? ((NfiEstaDB)Composite.Database)
                                    .SAuxiliaryData.TConfig[(int)ConfigId]
                                : null;
                    }
                }


                /// <summary>
                /// Step number
                /// </summary>
                public Nullable<int> Step
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TFnGetGidsForAuxTotalAppList.ColStep.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnGetGidsForAuxTotalAppList.ColStep.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TFnGetGidsForAuxTotalAppList.ColStep.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TFnGetGidsForAuxTotalAppList.ColStep.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Estimation cell identifier
                /// </summary>
                public Nullable<int> EstimationCellId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TFnGetGidsForAuxTotalAppList.ColEstimationCellId.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnGetGidsForAuxTotalAppList.ColEstimationCellId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TFnGetGidsForAuxTotalAppList.ColEstimationCellId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TFnGetGidsForAuxTotalAppList.ColEstimationCellId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Estimation cell object (read-only)
                /// </summary>
                public EstimationCell EstimationCell
                {
                    get
                    {
                        return
                            (EstimationCellId != null)
                                ? ((NfiEstaDB)Composite.Database)
                                    .SAuxiliaryData.CEstimationCell[(int)EstimationCellId]
                                : null;
                    }
                }

                /// <summary>
                /// Label of estimation cell
                /// </summary>
                public string EstimationCellLabel
                {
                    get
                    {
                        return ((NfiEstaDB)Composite.Database).Postgres.Setting.LanguageVersion switch
                        {
                            LanguageVersion.International => EstimationCellLabelEn,
                            LanguageVersion.National => EstimationCellLabelCs,
                            _ => EstimationCellLabelEn,
                        };
                    }
                }

                /// <summary>
                /// Label of estimation cell
                /// </summary>
                public string EstimationCellDescription
                {
                    get
                    {
                        return ((NfiEstaDB)Composite.Database).Postgres.Setting.LanguageVersion switch
                        {
                            LanguageVersion.International => EstimationCellDescriptionEn,
                            LanguageVersion.National => EstimationCellDescriptionCs,
                            _ => EstimationCellDescriptionEn,
                        };
                    }
                }

                /// <summary>
                /// Extended label of estimation cell
                /// </summary>
                public string ExtendedEstimationCellLabel
                {
                    get
                    {
                        return ((NfiEstaDB)Composite.Database).Postgres.Setting.LanguageVersion switch
                        {
                            LanguageVersion.International => ExtendedEstimationCellLabelEn,
                            LanguageVersion.National => ExtendedEstimationCellLabelCs,
                            _ => ExtendedEstimationCellLabelEn,
                        };
                    }
                }

                /// <summary>
                /// Extended description of estimation cell
                /// </summary>
                public string ExtendedEstimationCellDescription
                {
                    get
                    {
                        return ((NfiEstaDB)Composite.Database).Postgres.Setting.LanguageVersion switch
                        {
                            LanguageVersion.International => ExtendedEstimationCellDescriptionEn,
                            LanguageVersion.National => ExtendedEstimationCellDescriptionCs,
                            _ => ExtendedEstimationCellDescriptionEn,
                        };
                    }
                }

                /// <summary>
                /// Label of estimation cell in national language
                /// </summary>
                public string EstimationCellLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnGetGidsForAuxTotalAppList.ColEstimationCellLabelCs.Name,
                            defaultValue: TFnGetGidsForAuxTotalAppList.ColEstimationCellLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnGetGidsForAuxTotalAppList.ColEstimationCellLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Description of estimation cell in national language
                /// </summary>
                public string EstimationCellDescriptionCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnGetGidsForAuxTotalAppList.ColEstimationCellDescriptionCs.Name,
                            defaultValue: TFnGetGidsForAuxTotalAppList.ColEstimationCellDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnGetGidsForAuxTotalAppList.ColEstimationCellDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Extended label of estimation cell in national language (read-only)
                /// </summary>
                public string ExtendedEstimationCellLabelCs
                {
                    get
                    {
                        return String.IsNullOrEmpty(value: EstimationCellLabelCs)
                            ? $"{Id}"
                            : $"{Id} - {EstimationCellLabelCs}";
                    }
                }

                /// <summary>
                /// Extended description of estimation cell in national language (read-only)
                /// </summary>
                public string ExtendedEstimationCellDescriptionCs
                {
                    get
                    {
                        return String.IsNullOrEmpty(value: EstimationCellDescriptionCs)
                            ? $"{Id}"
                            : $"{Id} - {EstimationCellDescriptionCs}";
                    }
                }

                /// <summary>
                /// Label of estimation cell in English
                /// </summary>
                public string EstimationCellLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnGetGidsForAuxTotalAppList.ColEstimationCellLabelEn.Name,
                            defaultValue: TFnGetGidsForAuxTotalAppList.ColEstimationCellLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnGetGidsForAuxTotalAppList.ColEstimationCellLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Description of estimation cell in English
                /// </summary>
                public string EstimationCellDescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnGetGidsForAuxTotalAppList.ColEstimationCellDescriptionEn.Name,
                            defaultValue: TFnGetGidsForAuxTotalAppList.ColEstimationCellDescriptionEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnGetGidsForAuxTotalAppList.ColEstimationCellDescriptionEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Extended label of estimation cell in English (read-only)
                /// </summary>
                public string ExtendedEstimationCellLabelEn
                {
                    get
                    {
                        return String.IsNullOrEmpty(value: EstimationCellLabelEn)
                            ? $"{Id}"
                            : $"{Id} - {EstimationCellLabelEn}";
                    }
                }

                /// <summary>
                /// Extended label of estimation cell in English (read-only)
                /// </summary>
                public string ExtendedEstimationCellDescriptionEn
                {
                    get
                    {
                        return String.IsNullOrEmpty(value: EstimationCellDescriptionEn)
                            ? $"{Id}"
                            : $"{Id} - {EstimationCellDescriptionEn}";
                    }
                }


                /// <summary>
                /// Estimation cell segment identifier
                /// </summary>
                public Nullable<int> CellId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TFnGetGidsForAuxTotalAppList.ColCellId.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnGetGidsForAuxTotalAppList.ColCellId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TFnGetGidsForAuxTotalAppList.ColCellId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TFnGetGidsForAuxTotalAppList.ColCellId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Estimation cell segment object (read-only)
                /// </summary>
                public Cell Cell
                {
                    get
                    {
                        return
                            (CellId != null)
                                ? ((NfiEstaDB)Composite.Database)
                                    .SAuxiliaryData.FaCell[(int)CellId]
                                : null;
                    }
                }


                /// <summary>
                /// Extension version identifier
                /// </summary>
                public Nullable<int> ExtensionVersionId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TFnGetGidsForAuxTotalAppList.ColExtensionVersionId.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnGetGidsForAuxTotalAppList.ColExtensionVersionId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TFnGetGidsForAuxTotalAppList.ColExtensionVersionId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TFnGetGidsForAuxTotalAppList.ColExtensionVersionId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Extension version object (read-only)
                /// </summary>
                public ExtensionVersion ExtensionVersion
                {
                    get
                    {
                        return
                            (ExtensionVersionId != null)
                                ? ((NfiEstaDB)Composite.Database)
                                    .SAuxiliaryData.CExtensionVersion[(int)ExtensionVersionId]
                                : null;
                    }
                }


                /// <summary>
                /// Module version identifier
                /// </summary>
                public Nullable<int> GuiVersionId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TFnGetGidsForAuxTotalAppList.ColGuiVersionId.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnGetGidsForAuxTotalAppList.ColGuiVersionId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TFnGetGidsForAuxTotalAppList.ColGuiVersionId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TFnGetGidsForAuxTotalAppList.ColGuiVersionId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Module version object (read-only)
                /// </summary>
                public GUIVersion GuiVersion
                {
                    get
                    {
                        return
                            (GuiVersionId != null)
                                ? ((NfiEstaDB)Composite.Database)
                                    .SAuxiliaryData.CGUIVersion[(int)GuiVersionId]
                                : null;
                    }
                }


                /// <summary>
                /// Recalculation request
                /// </summary>
                public Nullable<bool> Recalc
                {
                    get
                    {
                        return Functions.GetNBoolArg(
                            row: Data,
                            name: TFnGetGidsForAuxTotalAppList.ColRecalc.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnGetGidsForAuxTotalAppList.ColRecalc.DefaultValue) ?
                                (Nullable<bool>)null :
                                Boolean.Parse(value: TFnGetGidsForAuxTotalAppList.ColRecalc.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNBoolArg(
                            row: Data,
                            name: TFnGetGidsForAuxTotalAppList.ColRecalc.Name,
                            val: value);
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true|false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not TFnGetGidsForAuxTotalApp, return False
                    if (obj is not TFnGetGidsForAuxTotalApp)
                    {
                        return false;
                    }

                    return
                        Id == ((TFnGetGidsForAuxTotalApp)obj).Id;
                }

                /// <summary>
                /// Returns a hash code for the current object
                /// </summary>
                /// <returns>Hash code for the current object</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                /// <summary>
                /// Returns a string that represents the current object
                /// </summary>
                /// <returns>String that represents the current object</returns>
                public override string ToString()
                {
                    return String.Concat(
                        $"{nameof(TFnGetGidsForAuxTotalApp)}: {{",
                        $"{nameof(Id)}: {Functions.PrepIntArg(arg: Id)}; ",
                        $"{nameof(ConfigId)}: {Functions.PrepNIntArg(arg: ConfigId)}; ",
                        $"{nameof(Step)}: {Functions.PrepNIntArg(arg: Step)}; ",
                        $"{nameof(EstimationCellId)}: {Functions.PrepNIntArg(arg: EstimationCellId)}; ",
                        $"{nameof(EstimationCellLabelCs)}: {Functions.PrepStringArg(arg: EstimationCellLabelCs)}; ",
                        $"{nameof(EstimationCellLabelEn)}: {Functions.PrepStringArg(arg: EstimationCellLabelEn)}; ",
                        $"{nameof(EstimationCellDescriptionCs)}: {Functions.PrepStringArg(arg: EstimationCellDescriptionCs)}; ",
                        $"{nameof(EstimationCellDescriptionEn)}: {Functions.PrepStringArg(arg: EstimationCellDescriptionEn)}; ",
                        $"{nameof(CellId)}: {Functions.PrepNIntArg(arg: CellId)}; ",
                        $"{nameof(ExtensionVersionId)}: {Functions.PrepNIntArg(arg: ExtensionVersionId)}; ",
                        $"{nameof(GuiVersionId)}: {Functions.PrepNIntArg(arg: GuiVersionId)}; ",
                        $"{nameof(Recalc)}: {Functions.PrepNBoolArg(arg: Recalc)}}}");
                }

                #endregion Methods

            }


            /// <summary>
            /// List of estimation cell segments for calculation of auxiliary variable totals
            /// (return type of the stored procedure fn_get_gids4aux_total_app)
            /// </summary>
            public class TFnGetGidsForAuxTotalAppList
                 : AParametrizedView
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema =
                    ADFunctions.FnGetGidsForAuxTotalApp.SchemaName;

                /// <summary>
                /// Stored procedure name
                /// </summary>
                public static readonly string Name =
                    ADFunctions.FnGetGidsForAuxTotalApp.Name;

                /// <summary>
                /// Stored procedure alias
                /// </summary>
                public static readonly string Alias =
                    ADFunctions.FnGetGidsForAuxTotalApp.Name;

                /// <summary>
                /// Description in national language
                /// </summary>
                public static readonly string CaptionCs = String.Concat(
                    $"Seznam segmentů výpočetních buněk pro výpočet úhrnů pomocné proměnné ",
                    $"vrácených uloženou procedurou {Name}.");

                /// <summary>
                /// Description in English
                /// </summary>
                public static readonly string CaptionEn = String.Concat(
                    $"List of estimation cell segments for auxiliary variable total calculation ",
                    $"from stored procedure {Name}.");

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
                {
                    { "id", new ColumnMetadata()
                    {
                        Name = "id",
                        DbName = "id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = true,
                        NotNull = true,
                        DefaultValue = "0",
                        FuncCall = "row_number() over()",
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ID",
                        HeaderTextEn = "ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 0
                    }
                    },
                    { "config_id", new ColumnMetadata()
                    {
                        Name = "config_id",
                        DbName = "config_id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "CONFIG_ID",
                        HeaderTextEn = "CONFIG_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 1
                    }
                    },
                    { "step", new ColumnMetadata()
                    {
                        Name = "step",
                        DbName = "step",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "STEP",
                        HeaderTextEn = "STEP",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 2
                    }
                    },
                    { "estimation_cell_id", new ColumnMetadata()
                    {
                        Name = "estimation_cell_id",
                        DbName = "estimation_cell",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATION_CELL_ID",
                        HeaderTextEn = "ESTIMATION_CELL_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 3
                    }
                    },
                    { "estimation_cell_label_cs", new ColumnMetadata()
                    {
                        Name = "estimation_cell_label_cs",
                        DbName = "estimation_cell_label",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATION_CELL_LABEL_CS",
                        HeaderTextEn = "ESTIMATION_CELL_LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 4
                    }
                    },
                    { "estimation_cell_description_cs", new ColumnMetadata()
                    {
                        Name = "estimation_cell_description_cs",
                        DbName = "estimation_cell_description",
                        DataType = "System.String",
                        DbDataType = "text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATION_CELL_DESCRIPTION_CS",
                        HeaderTextEn = "ESTIMATION_CELL_DESCRIPTION_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 5
                    }
                    },
                    { "estimation_cell_label_en", new ColumnMetadata()
                    {
                        Name = "estimation_cell_label_en",
                        DbName = "estimation_cell_label_en",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATION_CELL_LABEL_EN",
                        HeaderTextEn = "ESTIMATION_CELL_LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 6
                    }
                    },
                    { "estimation_cell_description_en", new ColumnMetadata()
                    {
                        Name = "estimation_cell_description_en",
                        DbName = "estimation_cell_description_en",
                        DataType = "System.String",
                        DbDataType = "text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATION_CELL_DESCRIPTION_EN",
                        HeaderTextEn = "ESTIMATION_CELL_DESCRIPTION_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 7
                    }
                    },
                    { "cell_id", new ColumnMetadata()
                    {
                        Name = "cell_id",
                        DbName = "gid",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "CELL_ID",
                        HeaderTextEn = "CELL_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 8
                    }
                    },
                    { "extension_version_id", new ColumnMetadata()
                    {
                        Name = "extension_version_id",
                        DbName = "extension_version_id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "EXTENSION_VERSION_ID",
                        HeaderTextEn = "EXTENSION_VERSION_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 9
                    }
                    },
                    { "gui_version_id", new ColumnMetadata()
                    {
                        Name = "gui_version_id",
                        DbName = "gui_version_id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "GUI_VERSION_ID",
                        HeaderTextEn = "GUI_VERSION_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 10
                    }
                    },
                    { "recalc", new ColumnMetadata()
                    {
                        Name = "recalc",
                        DbName = "recalc",
                        DataType = "System.Boolean",
                        DbDataType = "bool",
                        NewDataType = "bool",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "RECALC",
                        HeaderTextEn = "RECALC",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 11
                    }
                    }
                };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column config_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColConfigId = Cols["config_id"];

                /// <summary>
                /// Column step metadata
                /// </summary>
                public static readonly ColumnMetadata ColStep = Cols["step"];

                /// <summary>
                /// Column estimation_cell_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationCellId = Cols["estimation_cell_id"];

                /// <summary>
                /// Column estimation_cell_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationCellLabelCs = Cols["estimation_cell_label_cs"];

                /// <summary>
                /// Column estimation_cell_description_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationCellDescriptionCs = Cols["estimation_cell_description_cs"];

                /// <summary>
                /// Column estimation_cell_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationCellLabelEn = Cols["estimation_cell_label_en"];

                /// <summary>
                /// Column estimation_cell_description_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationCellDescriptionEn = Cols["estimation_cell_description_en"];

                /// <summary>
                /// Column cell_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColCellId = Cols["cell_id"];

                /// <summary>
                /// Column extension_version_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColExtensionVersionId = Cols["extension_version_id"];

                /// <summary>
                /// Column gui_version_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColGuiVersionId = Cols["gui_version_id"];

                /// <summary>
                /// Column recalc metadata
                /// </summary>
                public static readonly ColumnMetadata ColRecalc = Cols["recalc"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Private Fields

                /// <summary>
                /// 1st parameter: Configuration identifier
                /// </summary>
                private Nullable<int> configId;

                /// <summary>
                /// 2nd parameter: List of estimation cell identifiers
                /// </summary>
                private List<Nullable<int>> estimationCellIds;

                /// <summary>
                /// 3rd parameter: Extension version identifier
                /// </summary>
                private Nullable<int> extensionVersionId;

                /// <summary>
                /// 4th parameter: Module version identifier
                /// </summary>
                private Nullable<int> guiVersionId;

                /// <summary>
                /// 5th parameter: Recalculation request
                /// </summary>
                private Nullable<bool> recalc;

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TFnGetGidsForAuxTotalAppList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                {
                    ConfigId = null;
                    EstimationCellIds = null;
                    ExtensionVersionId = null;
                    GuiVersionId = null;
                    Recalc = null;
                }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TFnGetGidsForAuxTotalAppList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                {
                    ConfigId = null;
                    EstimationCellIds = null;
                    ExtensionVersionId = null;
                    GuiVersionId = null;
                    Recalc = null;
                }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// 1st parameter: Configuration identifier
                /// </summary>
                public Nullable<int> ConfigId
                {
                    get
                    {
                        return configId;
                    }
                    set
                    {
                        configId = value;
                    }
                }

                /// <summary>
                /// 2nd parameter: List of estimation cell identifiers
                /// </summary>
                public List<Nullable<int>> EstimationCellIds
                {
                    get
                    {
                        return estimationCellIds ?? [];
                    }
                    set
                    {
                        estimationCellIds = value ?? [];
                    }
                }

                /// <summary>
                /// 3rd parameter: Extension version identifier
                /// </summary>
                public Nullable<int> ExtensionVersionId
                {
                    get
                    {
                        return extensionVersionId;
                    }
                    set
                    {
                        extensionVersionId = value;
                    }
                }

                /// <summary>
                /// 4th parameter: Module version identifier
                /// </summary>
                public Nullable<int> GuiVersionId
                {
                    get
                    {
                        return guiVersionId;
                    }
                    set
                    {
                        guiVersionId = value;
                    }
                }

                /// <summary>
                /// 5th parameter: Recalculation request
                /// </summary>
                public Nullable<bool> Recalc
                {
                    get
                    {
                        return recalc;
                    }
                    set
                    {
                        recalc = value;
                    }
                }

                /// <summary>
                /// List of rows (read-only)
                /// </summary>
                public List<TFnGetGidsForAuxTotalApp> Items
                {
                    get
                    {
                        return
                            [..
                                Data.AsEnumerable()
                                .Select(a => new TFnGetGidsForAuxTotalApp(composite: this, data: a))
                            ];
                    }
                }

                /// <summary>
                /// Ordered list of steps (read-only)
                /// </summary>
                public List<int> Steps
                {
                    get
                    {
                        return
                            [.. Items
                                .Where(a => a.Step != null)
                                .Select(a => (int)a.Step)
                                .Distinct<int>()
                                .OrderBy(a => a)
                            ];
                    }
                }

                #endregion Properties


                #region Indexers

                /// <summary>
                /// Row from list of rows by identifier (read-only)
                /// </summary>
                /// <param name="id">Row identifier</param>
                /// <returns>Row from list of rows by identifier (null if not found)</returns>
                public TFnGetGidsForAuxTotalApp this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new TFnGetGidsForAuxTotalApp(composite: this, data: a))
                                .FirstOrDefault<TFnGetGidsForAuxTotalApp>();
                    }
                }

                #endregion Indexers


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public TFnGetGidsForAuxTotalAppList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any()
                        ? new TFnGetGidsForAuxTotalAppList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime)
                        {
                            ConfigId = ConfigId,
                            EstimationCellIds = EstimationCellIds,
                            ExtensionVersionId = ExtensionVersionId,
                            GuiVersionId = GuiVersionId,
                            Recalc = Recalc,
                        }
                        : new TFnGetGidsForAuxTotalAppList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null)
                        {
                            ConfigId = ConfigId,
                            EstimationCellIds = EstimationCellIds,
                            ExtensionVersionId = ExtensionVersionId,
                            GuiVersionId = GuiVersionId,
                            Recalc = Recalc,
                        };
                }

                /// <summary>
                /// List of estimation cell segments for calculation of auxiliary variable totals
                /// in one step
                /// </summary>
                /// <param name="step">Step number</param>
                /// <returns>
                /// List of estimation cell segments for calculation of auxiliary variable totals
                /// in one step
                /// </returns>
                public TFnGetGidsForAuxTotalAppList GidsInStep(int step)
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable()
                        .Where(a => a.Field<Nullable<int>>(columnName: ColStep.Name) == step);

                    return
                        rows.Any()
                        ? new TFnGetGidsForAuxTotalAppList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime)
                        {
                            ConfigId = ConfigId,
                            EstimationCellIds = EstimationCellIds,
                            ExtensionVersionId = ExtensionVersionId,
                            GuiVersionId = GuiVersionId,
                            Recalc = Recalc,
                        }
                        : new TFnGetGidsForAuxTotalAppList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null)
                        {
                            ConfigId = ConfigId,
                            EstimationCellIds = EstimationCellIds,
                            ExtensionVersionId = ExtensionVersionId,
                            GuiVersionId = GuiVersionId,
                            Recalc = Recalc,
                        };
                }

                /// <summary>
                /// List of estimation cell segments for calculation of auxiliary variable totals
                /// for one thread
                /// </summary>
                /// <param name="id">Thread identifier</param>
                /// <param name="countOfThreads">Count of threads</param>
                /// <returns>
                /// List of estimation cell segments for calculation of auxiliary variable totals
                /// for one thread
                /// </returns>
                public TFnGetGidsForAuxTotalAppList GidsForThread(int id, int countOfThreads)
                {
                    if (id <= 0)
                    {
                        return
                            new TFnGetGidsForAuxTotalAppList(
                                database: (NfiEstaDB)Database,
                                data: EmptyDataTable(),
                                condition: Condition,
                                limit: Limit,
                                loadingTime: null)
                            {
                                ConfigId = ConfigId,
                                EstimationCellIds = EstimationCellIds,
                                ExtensionVersionId = ExtensionVersionId,
                                GuiVersionId = GuiVersionId,
                                Recalc = Recalc,
                            };
                    }

                    int countOfSegmentsPerOneThread =
                        ((int)(Data.Rows.Count / countOfThreads)) + 1;

                    IEnumerable<DataRow> rows = Data.AsEnumerable()
                      .Skip(count: (id - 1) * countOfSegmentsPerOneThread)
                      .Take(count: countOfSegmentsPerOneThread);

                    return
                        rows.Any()
                        ? new TFnGetGidsForAuxTotalAppList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime)
                        {
                            ConfigId = ConfigId,
                            EstimationCellIds = EstimationCellIds,
                            ExtensionVersionId = ExtensionVersionId,
                            GuiVersionId = GuiVersionId,
                            Recalc = Recalc,
                        }
                        : new TFnGetGidsForAuxTotalAppList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null)
                        {
                            ConfigId = ConfigId,
                            EstimationCellIds = EstimationCellIds,
                            ExtensionVersionId = ExtensionVersionId,
                            GuiVersionId = GuiVersionId,
                            Recalc = Recalc,
                        };
                }

                #endregion Methods

            }

        }
    }
}
