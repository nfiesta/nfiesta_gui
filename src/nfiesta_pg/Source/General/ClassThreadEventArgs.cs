﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;

namespace ZaJi
{
    namespace NfiEstaPg
    {

        /// <summary>
        /// <para lang="cs">Data události poslaná vláknem</para>
        /// <para lang="en">Event data sent by thread</para>
        /// </summary>
        public class ThreadEventArgs : EventArgs
        {

            #region Private Fields

            /// <summary>
            /// <para lang="cs">Identifikační číslo vlákna, (0 označuje řídící vlákno)</para>
            /// <para lang="en">Thread identifier (0 indicates the control thread)</para>
            /// </summary>
            private Nullable<int> id = null;

            /// <summary>
            /// <para lang="cs">Zpracovávaný úkol</para>
            /// <para lang="en">Task for processing</para>
            /// </summary>
            private object task = null;

            /// <summary>
            /// <para lang="cs">Celkový počet úkolů</para>
            /// <para lang="en">Total tasks number</para>
            /// </summary>
            private Nullable<int> tasksTotal = null;

            /// <summary>
            /// <para lang="cs">Počet dokončených úkolů</para>
            /// <para lang="en">Completed tasks number</para>
            /// </summary>
            private Nullable<int> tasksCompleted = null;

            /// <summary>
            /// <para lang="cs">Doba uplynulá od spuštění vláken</para>
            /// <para lang="en">Time elapsed since threads were started</para>
            /// </summary>
            private Nullable<double> elapsedTime = null;

            /// <summary>
            /// <para lang="cs">Data události z databázového připojení</para>
            /// <para lang="en">Event data from database connection</para>
            /// </summary>
            private ZaJi.PostgreSQL.PostgreSQLWrapper.ExceptionEventArgs exceptionEventArgs = null;

            /// <summary>
            /// <para lang="cs">Zachycená zpráva z databázového připojení</para>
            /// <para lang="en">Captured notice from database connection</para>
            /// </summary>
            private Npgsql.PostgresNotice notice = null;

            /// <summary>
            /// <para lang="cs">Zpráva odeslaná vláknem</para>
            /// <para lang="en">Message send by thread</para>
            /// </summary>
            private string message = null;

            /// <summary>
            /// <para lang="cs">Informace určující kdy událost vznikla</para>
            /// <para lang="en">Information identifying when a certain event occurred</para>
            /// </summary>

            private DateTime timeStamp = DateTime.Now;

            /// <summary>
            /// <para lang="cs">Rozšiřující data události poslaná vláknem</para>
            /// <para lang="en">Event extended data sent by thread</para>
            /// </summary>
            private object tag = null;

            #endregion Private Fields


            #region Properties

            /// <summary>
            /// <para lang="cs">Identifikační číslo vlákna, (0 označuje řídící vlákno)</para>
            /// <para lang="en">Thread identifier (0 indicates the control thread)</para>
            /// </summary>
            public Nullable<int> Id
            {
                get
                {
                    return id;
                }
                set
                {
                    id = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Zpracovávaný úkol</para>
            /// <para lang="en">Task for processing</para>
            /// </summary>
            public object Task
            {
                get
                {
                    return task;
                }
                set
                {
                    task = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Celkový počet úkolů</para>
            /// <para lang="en">Total tasks number</para>
            /// </summary>
            public Nullable<int> TasksTotal
            {
                get
                {
                    return tasksTotal;
                }
                set
                {
                    tasksTotal = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Počet dokončených úkolů</para>
            /// <para lang="en">Completed tasks number</para>
            /// </summary>
            public Nullable<int> TasksCompleted
            {
                get
                {
                    return tasksCompleted;
                }
                set
                {
                    tasksCompleted = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Doba uplynulá od spuštění vláken</para>
            /// <para lang="en">Time elapsed since threads were started</para>
            /// </summary>
            public Nullable<double> ElapsedTime
            {
                get
                {
                    return elapsedTime;
                }
                set
                {
                    elapsedTime = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Data události z databázového připojení</para>
            /// <para lang="en">Event data from database connection</para>
            /// </summary>
            public ZaJi.PostgreSQL.PostgreSQLWrapper.ExceptionEventArgs ExceptionEventArgs
            {
                get
                {
                    return exceptionEventArgs;
                }
                set
                {
                    exceptionEventArgs = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Zachycená zpráva z databázového připojení</para>
            /// <para lang="en">Captured notice from database connection</para>
            /// </summary>
            public Npgsql.PostgresNotice Notice
            {
                get
                {
                    return notice;
                }
                set
                {
                    notice = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Zpráva odeslaná vláknem</para>
            /// <para lang="en">Message send by thread</para>
            /// </summary>
            public string Message
            {
                get
                {
                    return message;
                }
                set
                {
                    message = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Informace určující kdy událost vznikla</para>
            /// <para lang="en">Information identifying when a certain event occurred</para>
            /// </summary>

            public DateTime TimeStamp
            {
                get
                {
                    return timeStamp;
                }
                set
                {
                    timeStamp = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Rozšiřující data události poslaná vláknem</para>
            /// <para lang="en">Event extended data sent by thread</para>
            /// </summary>
            public object Tag
            {
                get
                {
                    return tag;
                }
                set
                {
                    tag = value;
                }
            }

            #endregion Properties


            #region Methods

            /// <summary>
            /// Returns a string that represents the current object.
            /// </summary>
            /// <returns>String that represents the current object.</returns>
            public override string ToString()
            {
                return Message;
            }

            #endregion Methods

        }

    }
}
