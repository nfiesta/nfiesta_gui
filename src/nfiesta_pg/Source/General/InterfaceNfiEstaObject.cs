﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi
{
    namespace NfiEstaPg
    {

        /// <summary>
        /// <para lang="cs">Rozhraní objektů v modulech</para>
        /// <para lang="en">Interface for objects in modules</para>
        /// </summary>
        public interface INfiEstaObject
        {

            #region Properties

            /// <summary>
            /// <para lang="cs">Vlastník objektu</para>
            /// <para lang="en">Object owner</para>
            /// </summary>
            object Owner { get; set; }

            /// <summary>
            /// <para lang="cs">Databázové tabulky (read-only)</para>
            /// <para lang="en">Database tables(read-only)</para>
            /// </summary>
            NfiEstaDB Database { get; }

            /// <summary>
            /// <para lang="cs">Jazyk zpráv (read-only)</para>
            /// <para lang="en">Language of messages (read-only)</para>
            /// </summary>
            ZaJi.PostgreSQL.LanguageVersion LanguageVersion { get; }

            #endregion Properties

        }

    }
}
