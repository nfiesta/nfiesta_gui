﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System.Collections.Generic;

namespace ZaJi
{
    namespace NfiEstaPg
    {

        /// <summary>
        /// Thread safe queue
        /// </summary>
        /// <typeparam name="T"></typeparam>
        public class ThreadSafeQueue<T>
        {

            #region Private Fields

            /// <summary>
            /// Locking object
            /// </summary>
            private readonly object locker = new();

            /// <summary>
            /// Queue object
            /// </summary>
            private readonly Queue<T> queue = new();

            #endregion Private Fields


            #region Properties

            /// <summary>
            /// The number of elements contained in the queue
            /// </summary>
            public int Count
            {
                get
                {
                    lock (locker)
                    {
                        return queue.Count;
                    }
                }
            }

            #endregion Properties


            #region Methods

            /// <summary>
            /// Determinates whether a sequence contains any elements
            /// </summary>
            /// <returns>true if the source sequence contains any elements; otherwise, false</returns>
            public bool Any()
            {
                lock (locker)
                {
                    return queue.Count != 0;
                }
            }

            /// <summary>
            /// Removes all objects from the queue
            /// </summary>
            public void Clear()
            {
                lock (locker)
                {
                    queue.Clear();
                }
            }

            /// <summary>
            /// Removes and returns the object at the beginning of the queue
            /// </summary>
            /// <returns>The object at the beginning of the queue</returns>
            public T Dequeue()
            {
                lock (locker)
                {
                    return queue.Dequeue();
                }
            }

            /// <summary>
            /// Add an object to the end of the queue
            /// </summary>
            /// <param name="item">Object</param>
            public void Enqueue(T item)
            {
                lock (locker)
                {
                    queue.Enqueue(item: item);
                }
            }

            /// <summary>
            /// Add objects to the end of the queue
            /// </summary>
            /// <param name="items">Collection of objects</param>
            public void EnqueueRange(IEnumerable<T> items)
            {
                lock (locker)
                {
                    if (items == null)
                    {
                        return;
                    }

                    foreach (T item in items)
                    {
                        queue.Enqueue(item: item);
                    }
                }
            }


            /// <summary>
            /// Removes and returns the object at the beginning of the queue
            /// </summary>
            /// <param name="item">The object at the beginning of the queue</param>
            /// <returns>true if the object was returned; otherwise, false</returns>
            public bool TryDequeue(out T item)
            {
                lock (locker)
                {
                    if (queue.Count > 0)
                    {
                        item = queue.Dequeue();
                        return true;
                    }
                    else
                    {
                        item = default;
                        return false;
                    }
                }
            }

            #endregion Methods

        }

    }
}