﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi
{
    namespace NfiEstaPg
    {

        /// <summary>
        /// <para lang="cs">Rozhraní ovládacích prvků</para>
        /// <para lang="en">Interface for user controls</para>
        /// </summary>
        public interface INfiEstaControl
        {

            #region Properties

            /// <summary>
            /// <para lang="cs">Vlastník ovládacího prvku</para>
            /// <para lang="en">Control owner</para>
            /// </summary>
            System.Windows.Forms.Control ControlOwner { get; set; }

            /// <summary>
            /// <para lang="cs">Databázové tabulky (read-only)</para>
            /// <para lang="en">Database tables(read-only)</para>
            /// </summary>
            NfiEstaDB Database { get; }

            /// <summary>
            /// <para lang="cs">Jazyk popisků (read-only)</para>
            /// <para lang="en">Language of labels (read-only)</para>
            /// </summary>
            ZaJi.PostgreSQL.LanguageVersion LanguageVersion { get; }

            #endregion Properties


            #region Methods

            /// <summary>
            /// <para lang="cs">Inicializace popisků ovládacího prvku</para>
            /// <para lang="en">Initializing module control labels</para>
            /// </summary>
            void InitializeLabels();

            /// <summary>
            /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
            /// <para lang="en">Loading database table data and displaying it in the control</para>
            /// </summary>
            void LoadContent();

            #endregion Methods

        }

    }
}
