﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;

namespace ZaJi
{
    namespace NfiEstaPg
    {

        /// <summary>
        /// <para lang="cs">Rozhraní řídícího vlákna</para>
        /// <para lang="en">Control thread interface</para>
        /// </summary>
        public interface IControlThread
        {

            #region Events

            /// <summary>
            /// <para lang="cs">Událost "Zahájení řídícího vlákna"</para>
            /// <para lang="en">"Starting a control thread" event</para>
            /// </summary>
            event EventHandler OnControlStart;

            /// <summary>
            /// <para lang="cs">Událost "Zahájení pracovního vlákna"</para>
            /// <para lang="en">"Starting a worker thread" event</para>
            /// </summary>
            event EventHandler OnWorkerStart;

            /// <summary>
            /// <para lang="cs">Událost "Chyba při připojení k databázi"</para>
            /// <para lang="en">"Database connection error" event</para>
            /// </summary>
            event EventHandler OnConnectionException;

            /// <summary>
            /// <para lang="cs">Událost "Zahájení jednoho úkolu pracovního vlákna"</para>
            /// <para lang="en">"Starting one task of a worker thread" event</para>
            /// </summary>
            event EventHandler OnTaskStart;

            /// <summary>
            /// <para lang="cs">Událost "Chyba při zpracování úkolu"</para>
            /// <para lang="en">"Task processing error" event</para>
            /// </summary>
            event EventHandler OnTaskException;

            /// <summary>
            /// <para lang="cs">Událost "Ukončení jednoho úkolu pracovního vlákna"</para>
            /// <para lang="en">"Terminating one task of a worker thread" event</para>
            /// </summary>
            event EventHandler OnTaskComplete;

            /// <summary>
            /// <para lang="cs">Událost "Ukončení pracovního vlákna"</para>
            /// <para lang="en">"Terminating a worker thread" event</para>
            /// </summary>
            event EventHandler OnWorkerComplete;

            /// <summary>
            /// <para lang="cs">Událost "Ukončení řídícího vlákna"</para>
            /// <para lang="en">"Terminating a control thread" event</para>
            /// </summary>
            event EventHandler OnControlComplete;

            #endregion Events


            #region Properties

            /// <summary>
            /// <para lang="cs">Výsledek</para>
            /// <para lang="en">Result</para>
            /// </summary>
            ThreadSafeQueue<object> Result { get; }

            /// <summary>
            /// <para lang="cs">
            /// Seznam zpráv z pracovních vláken
            /// </para>
            /// <para lang="en">
            /// List of messages from worker threads
            /// </para>
            /// </summary>
            ThreadSafeList<string> Log { get; }

            #endregion Properties


            #region Methods

            /// <summary>
            /// <para lang="cs">Zastavení řídícího vlákna</para>
            /// <para lang="en">Stopping a control thread</para>
            /// </summary>
            void Stop();

            #endregion Methods

        }

    }
}
