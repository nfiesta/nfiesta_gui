﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Versioning;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {

        /// <summary>
        /// Combination of area domain categories
        /// or subpopulation categories
        /// </summary>
        /// <remarks>
        /// Contructor
        /// </remarks>
        /// <param name="database">Database tables</param>
        [SupportedOSPlatform("windows")]
        public class TDDomainCategoryCombination<TCategory>(NfiEstaDB database)
            where TCategory : IArealOrPopulationCategory
        {

            #region Constants

            /// <summary>
            /// Maximal count of area domain categories or subpopulation categories in combination
            /// </summary>
            private const int MaxSize = 6;

            #endregion Constants


            #region Private Fields

            /// <summary>
            /// Database tables
            /// </summary>
            private readonly NfiEstaDB database = database;

            /// <summary>
            /// List of area domain categories
            /// or subpopulation categories
            /// </summary>
            private readonly List<TCategory> items = [];

            #endregion Private Fields


            #region Properties

            /// <summary>
            /// Database tables (read-only)
            /// </summary>
            public NfiEstaDB Database
            {
                get
                {
                    return database;
                }
            }

            /// <summary>
            /// List of area domain categories
            /// or subpopulation categories(read-only)
            /// </summary>
            public List<TCategory> Items
            {
                get
                {
                    return this.items;
                }
            }

            #endregion Properties


            #region Derived Properties

            /// <summary>
            /// Combination of area domain
            /// or subpopulation categories identifier (read-only)
            /// </summary>
            public string Id
            {
                get
                {
                    if (items == null)
                    {
                        return String.Empty;
                    }
                    else if (items.Count == 0)
                    {
                        return String.Empty;
                    }
                    else
                    {
                        return items
                             .Select(a => a.Id.ToString())
                             .Aggregate((a, b) => $"{a}:{b}");
                    }
                }
            }


            /// <summary>
            /// Label of combination of area domain
            /// or subpopulation categories in national language (read-only)
            /// </summary>
            public string LabelCs
            {
                get
                {
                    if (items == null)
                    {
                        return String.Empty;
                    }
                    else if (items.Count == 0)
                    {
                        return String.Empty;
                    }
                    else
                    {
                        return items
                             .Select(a => a.LabelCs.ToString())
                             .Aggregate((a, b) => $"{a}:{b}");
                    }
                }
            }

            /// <summary>
            /// Description of combination of area domain
            /// or subpopulation categories in national language (read-only)
            /// </summary>
            public string DescriptionCs
            {
                get
                {
                    if (items == null)
                    {
                        return String.Empty;
                    }
                    else if (items.Count == 0)
                    {
                        return String.Empty;
                    }
                    else
                    {
                        return items
                             .Select(a => a.DescriptionCs.ToString())
                             .Aggregate((a, b) => $"{a}:{b}");
                    }
                }
            }

            /// <summary>
            /// Extended label of combination of area domain
            /// or subpopulation categories in national language (read-only)
            /// </summary>
            public string ExtendedLabelCs
            {
                get
                {
                    if (String.IsNullOrEmpty(value: Id))
                    {
                        return String.Empty;
                    }
                    else if (String.IsNullOrEmpty(value: LabelCs))
                    {
                        return String.Empty;
                    }
                    else
                    {
                        return $"{Id} - {LabelCs}";
                    }
                }
            }


            /// <summary>
            /// Label of combination of area domain
            /// or subpopulation categories in English (read-only)
            /// </summary>
            public string LabelEn
            {
                get
                {
                    if (items == null)
                    {
                        return String.Empty;
                    }
                    else if (items.Count == 0)
                    {
                        return String.Empty;
                    }
                    else
                    {
                        return items
                             .Select(a => a.LabelEn.ToString())
                             .Aggregate((a, b) => $"{a}:{b}");
                    }
                }
            }

            /// <summary>
            /// Description of combination of area domain
            /// or subpopulation categories in English (read-only)
            /// </summary>
            public string DescriptionEn
            {
                get
                {
                    if (items == null)
                    {
                        return String.Empty;
                    }
                    else if (items.Count == 0)
                    {
                        return String.Empty;
                    }
                    else
                    {
                        return items
                             .Select(a => a.DescriptionEn.ToString())
                             .Aggregate((a, b) => $"{a}:{b}");
                    }
                }
            }

            /// <summary>
            /// Extended label of combination of area domain
            /// or subpopulation categories in English (read-only)
            /// </summary>
            public string ExtendedLabelEn
            {
                get
                {
                    if (String.IsNullOrEmpty(value: Id))
                    {
                        return String.Empty;
                    }
                    else if (String.IsNullOrEmpty(value: LabelEn))
                    {
                        return String.Empty;
                    }
                    else
                    {
                        return $"{Id} - {LabelEn}";
                    }
                }
            }

            #endregion Derived Properties


            #region Methods

            /// <summary>
            /// Adds area domain or subpopulation category in the combination
            /// Combination max. size is set to 5 area domain or subpopulation categories
            /// </summary>
            /// <param name="category">Area domain or subpopulation category</param>
            public void Add(TCategory category)
            {
                if (items.Count < TDDomainCategoryCombination<TCategory>.MaxSize)
                {
                    items.Add(category);
                }
            }

            /// <summary>
            /// Returns a string that represents the current object
            /// </summary>
            /// <returns>Returns a string that represents the current object</returns>
            public override string ToString()
            {
                return Database.Postgres.Setting.LanguageVersion switch
                {
                    LanguageVersion.National => String.Concat(
                        $"{"ID"} = {Functions.PrepStringArg(Id)},{Environment.NewLine}",
                        $"{"Name"} = {Functions.PrepStringArg(LabelCs)}.{Environment.NewLine}"
                        ),
                    LanguageVersion.International => String.Concat(
                        $"{"ID"} = {Functions.PrepStringArg(Id)},{Environment.NewLine}",
                        $"{"Name"} = {Functions.PrepStringArg(LabelEn)}.{Environment.NewLine}"
                        ),
                    _ => String.Concat(
                        $"{"ID"} = {Functions.PrepStringArg(Id)},{Environment.NewLine}",
                        $"{"Name"} = {Functions.PrepStringArg(LabelEn)}.{Environment.NewLine}"
                        ),
                };
            }

            /// <summary>
            /// Determines whether specified object is equal to the current object
            /// </summary>
            /// <param name="obj">Speciefied object</param>
            /// <returns>true/false</returns>
            public override bool Equals(object obj)
            {
                // If the passed object is null, return False
                if (obj == null)
                {
                    return false;
                }

                // If the passed object is not TDDomainCategoryCombination<TCategory> Type, return False
                if (obj is not TDDomainCategoryCombination<TCategory>)
                {
                    return false;
                }

                return
                    Id == ((TDDomainCategoryCombination<TCategory>)obj).Id;
            }

            /// <summary>
            /// Returns the hash code
            /// </summary>
            /// <returns>Hash code</returns>
            public override int GetHashCode()
            {
                return
                    Id.GetHashCode();
            }

            #endregion Methods

        }


        /// <summary>
        /// List of combinations of area domain or subpopulation categories
        /// </summary>
        /// <remarks>
        /// Contructor
        /// </remarks>
        /// <param name="database">Database tables</param>
        /// <param name="items">List of combinations of area domain or subpopulation categories</param>
        [SupportedOSPlatform("windows")]
        public class TDDomainCategoryCombinationList<TCategory>(
            NfiEstaDB database,
            List<TDDomainCategoryCombination<TCategory>> items = null)
            where TCategory : IArealOrPopulationCategory
        {

            #region Static Fields

            /// <summary>
            /// Description in national language
            /// </summary>
            private const string CaptionCs = "Seznam kombinací kategorií plošných domén nebo subpopulací";

            /// <summary>
            /// Description in English
            /// </summary>
            private const string CaptionEn = "List of combinations of area domain or subpopulation categories";

            #endregion Static Fields


            #region Private Fields

            /// <summary>
            /// Database tables
            /// </summary>
            private readonly NfiEstaDB database = database;

            /// <summary>
            /// List of combinations of area domain or subpopulation categories
            /// </summary>
            private readonly List<TDDomainCategoryCombination<TCategory>> items = items ?? [];

            #endregion Private Fields


            #region Properties

            /// <summary>
            /// Short description of object (read-only)
            /// </summary>
            public string Caption
            {
                get
                {
                    return Database.Postgres.Setting.LanguageVersion switch
                    {
                        LanguageVersion.National => CaptionCs,
                        LanguageVersion.International => CaptionEn,
                        _ => CaptionEn,
                    };
                }
            }

            /// <summary>
            /// List of attribute categories (read-only)
            /// </summary>
            public List<TDDomainCategoryCombination<TCategory>> Items
            {
                get
                {
                    return this.items;
                }
            }

            /// <summary>
            /// Database tables (read-only)
            /// </summary>
            public NfiEstaDB Database
            {
                get
                {
                    return database;
                }
            }

            #endregion Properties


            #region Methods

            /// <summary>
            /// Displays list of combinations of area domain or subpopulation categories in panel
            /// </summary>
            /// <param name="panel">Panel</param>
            /// <param name="languageVersion">Language</param>
            public void DisplayInPanel(System.Windows.Forms.Panel panel, LanguageVersion languageVersion)
            {
                panel.Controls.Clear();
                System.Windows.Forms.ListBox box = new()
                {
                    DataSource = this.Items,
                    DisplayMember =
                        (languageVersion == LanguageVersion.National) ? "ExtendedLabelCs" :
                        (languageVersion == LanguageVersion.International) ? "ExtendedLabelEn" :
                        "Id",
                    Dock = System.Windows.Forms.DockStyle.Fill,
                    Font = new System.Drawing.Font(
                        familyName: "Microsoft Sans Serif",
                        emSize: 9F,
                        style: System.Drawing.FontStyle.Regular,
                        unit: System.Drawing.GraphicsUnit.Point,
                        gdiCharSet: ((byte)(238))),
                    SelectionMode = System.Windows.Forms.SelectionMode.None,
                    ValueMember =
                        (languageVersion == LanguageVersion.National) ? "ExtendedLabelCs" :
                        (languageVersion == LanguageVersion.International) ? "ExtendedLabelEn" :
                        "Id",
                };
                panel.Controls.Add(box);
            }

            /// <summary>
            /// Returns a string that represents the current object
            /// </summary>
            /// <returns>Returns a string that represents the current object</returns>
            public override string ToString()
            {
                return Database.Postgres.Setting.LanguageVersion switch
                {
                    LanguageVersion.National => String.Concat($"{Caption}. Počet = {Items.Count}."),
                    LanguageVersion.International => String.Concat($"{Caption}. Count = {Items.Count}."),
                    _ => String.Concat($"{Caption}. Count = {Items.Count}."),
                };
            }

            #endregion Methods

        }

    }
}
