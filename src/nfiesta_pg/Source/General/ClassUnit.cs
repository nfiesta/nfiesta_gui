﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {

        /// <summary>
        /// Unit
        /// </summary>
        public class Unit
        {

            #region Private Fields

            /// <summary>
            /// Database tables
            /// </summary>
            private NfiEstaDB database;

            /// <summary>
            /// Numerator unit
            /// </summary>
            private MetadataElement numerator;

            /// <summary>
            /// Denominator unit
            /// </summary>
            private MetadataElement denominator;

            #endregion Private Fields


            #region Constructor

            /// <summary>
            /// Constructor of the unit object for total estimate
            /// </summary>
            /// <param name="database">Database tables</param>
            /// <param name="numerator">Numerator unit</param>
            public Unit(
                NfiEstaDB database,
                MetadataElement numerator)
            {
                Database = database;
                Numerator = numerator;
                Denominator = null;
            }

            /// <summary>
            /// Constructor of the unit object for ratio estimate
            /// </summary>
            /// <param name="database">Database tables</param>
            /// <param name="numerator">Numerator unit</param>
            /// <param name="denominator">Denominator unit</param>
            public Unit(
                NfiEstaDB database,
                MetadataElement numerator,
                MetadataElement denominator)
            {
                Database = database;
                Numerator = numerator;
                Denominator = denominator;
            }

            #endregion Constructor


            #region Properties

            /// <summary>
            /// Database tables
            /// </summary>
            public NfiEstaDB Database
            {
                get
                {
                    return database;
                }
                private set
                {
                    database = value ??
                        throw new ArgumentException(
                            message: $"Argument {nameof(Database)} must not be null.",
                            paramName: nameof(Database));
                }
            }

            /// <summary>
            /// Numerator unit
            /// </summary>
            public MetadataElement Numerator
            {
                get
                {
                    return numerator;
                }
                set
                {
                    numerator = value ??
                        throw new ArgumentException(
                            message: $"Argument {nameof(Numerator)} must not be null.",
                            paramName: nameof(Numerator));
                }
            }

            /// <summary>
            /// Denominator unit
            /// </summary>
            public MetadataElement Denominator
            {
                get
                {
                    return denominator;
                }
                set
                {
                    denominator = value;
                }
            }

            #endregion Properties


            #region Derived Properties

            /// <summary>
            /// Unit is for total estimate (read-only)
            /// </summary>
            public bool IsTotal
            {
                get
                {
                    return Denominator == null;
                }
            }

            /// <summary>
            /// Unit is for ratio estimate (read-only)
            /// </summary>
            public bool IsRatio
            {
                get
                {
                    return Denominator != null;
                }
            }

            /// <summary>
            /// Label of the unit in national language
            /// </summary>
            public string LabelCs
            {
                get
                {
                    if (IsTotal)
                    {
                        return Numerator.LabelCs;
                    }

                    else
                    {
                        if (Numerator.Equals(obj: Denominator))
                        {
                            return "%";
                        }
                        else
                        {
                            return $"{Numerator.LabelCs}/{Denominator.LabelCs}";
                        }
                    }
                }
            }

            /// <summary>
            /// Label of the unit in English
            /// </summary>
            public string LabelEn
            {
                get
                {
                    if (IsTotal)
                    {
                        return Numerator.LabelEn;
                    }

                    else
                    {
                        if (Numerator.Equals(obj: Denominator))
                        {
                            return "%";
                        }
                        else
                        {
                            return $"{Numerator.LabelEn}/{Denominator.LabelEn}";
                        }
                    }
                }
            }

            #endregion Derived Properties


            #region Methods

            /// <summary>
            /// Text description of unit
            /// </summary>
            /// <returns>Text description of unit</returns>
            public override string ToString()
            {
                return Database.Postgres.Setting.LanguageVersion switch
                {
                    LanguageVersion.National => LabelCs,
                    LanguageVersion.International => LabelEn,
                    _ => LabelEn,
                };
            }

            /// <summary>
            /// Determines whether specified object is equal to the current object
            /// </summary>
            /// <param name="obj">Speciefied object</param>
            /// <returns>true/false</returns>
            public override bool Equals(object obj)
            {
                // If the passed object is null, return False
                if (obj == null)
                {
                    return false;
                }

                // If the passed object is not Unit Type, return False
                if (obj is not Unit)
                {
                    return false;
                }

                Unit objUnit = (Unit)obj;

                if ((Numerator != null) && (Denominator != null) &&
                   (objUnit.Numerator != null) && (objUnit.Denominator != null))
                {
                    return Numerator.Equals(obj: objUnit.Numerator) &&
                           Denominator.Equals(obj: objUnit.Denominator);
                }

                else if ((Numerator != null) && (Denominator == null) &&
                   (objUnit.Numerator != null) && (objUnit.Denominator == null))
                {
                    return Numerator.Equals(obj: objUnit.Numerator);
                }

                else if ((Numerator == null) && (Denominator != null) &&
                   (objUnit.Numerator == null) && (objUnit.Denominator != null))
                {
                    return Denominator.Equals(obj: objUnit.Denominator);
                }

                else if ((Numerator == null) && (Denominator == null) &&
                   (objUnit.Numerator == null) && (objUnit.Denominator == null))
                {
                    return true;
                }

                else
                {
                    return false;
                }
            }

            /// <summary>
            /// Returns the hash code
            /// </summary>
            /// <returns>Hash code</returns>
            public override int GetHashCode()
            {
                if ((Numerator != null) && (Denominator != null))
                {
                    return Numerator.GetHashCode() ^ Denominator.GetHashCode();
                }
                else if ((Numerator != null) && (Denominator == null))
                {
                    return Numerator.GetHashCode();
                }
                else if ((Numerator == null) && (Denominator != null))
                {
                    return Denominator.GetHashCode();
                }
                else
                {
                    return 0;
                }
            }

            #endregion Methods

        }

    }
}
