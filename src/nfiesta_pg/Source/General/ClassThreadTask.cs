﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;

namespace ZaJi
{
    namespace NfiEstaPg
    {

        /// <summary>
        /// <para lang="cs">Úkol zpracovávaný vláknem</para>
        /// <para lang="en">Task processed by thread</para>
        /// </summary>
        /// <typeparam name="U">
        /// <para lang="cs">Typ úkolu</para>
        /// <para lang="en">Task type</para>
        /// </typeparam>
        public class ThreadTask<U>
        {

            #region Private Fields

            /// <summary>
            /// <para lang="cs">Úkol zpracovávaný vláknem</para>
            /// <para lang="en">Task processed by thread</para>
            /// </summary>
            private U element;

            /// <summary>
            /// <para lang="cs">Priorita úkolu</para>
            /// <para lang="en">Task priority</para>
            /// </summary>
            private byte priority;

            /// <summary>
            /// <para lang="cs">SQL příkaz</para>
            /// <para lang="en">SQL command</para>
            /// </summary>
            private string sql;

            /// <summary>
            /// <para lang="cs">Výsledek uložené procedury</para>
            /// <para lang="en">Result from stored procedure</para>
            /// </summary>
            private object result;

            #endregion Private Fields


            #region Constructor

            /// <summary>
            /// <para lang="cs">Konstruktor</para>
            /// <para lang="en">Constructor</para>
            /// </summary>
            /// <param name="element">
            /// <para lang="cs">Úkol zpracovávaný vláknem</para>
            /// <para lang="en">Task processed by thread</para>
            /// </param>
            /// <param name="priority">
            /// <para lang="cs">Priorita úkolu</para>
            /// <para lang="en">Task priority</para>
            /// </param>
            public ThreadTask(U element, byte priority)
            {
                Element = element;
                Priority = priority;
                SQL = String.Empty;
                Result = null;
            }

            #endregion Constructor


            #region Properties

            /// <summary>
            /// <para lang="cs">Úkol zpracovávaný vláknem</para>
            /// <para lang="en">Task processed by thread</para>
            /// </summary>
            public U Element
            {
                get
                {
                    return element;
                }
                private set
                {
                    element = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Priorita úkolu</para>
            /// <para lang="en">Task priority</para>
            /// </summary>
            public byte Priority
            {
                get
                {
                    return priority;
                }
                private set
                {
                    priority = value;
                }
            }

            /// <summary>
            /// <para lang="cs">SQL příkaz</para>
            /// <para lang="en">SQL command</para>
            /// </summary>
            public string SQL
            {
                get
                {
                    return sql ?? String.Empty;
                }
                set
                {
                    sql = value ?? String.Empty;
                }
            }

            /// <summary>
            /// <para lang="cs">Výsledek uložené procedury</para>
            /// <para lang="en">Result from stored procedure</para>
            /// </summary>
            public object Result
            {
                get
                {
                    return result;
                }
                set
                {
                    result = value;
                }
            }

            #endregion Properties


            #region Methods

            /// <summary>
            /// Determines whether specified object is equal to the current object
            /// </summary>
            /// <param name="obj">Speciefied object</param>
            /// <returns>true|false</returns>
            public override bool Equals(object obj)
            {
                // If the passed object is null, return False
                if (obj == null)
                {
                    return false;
                }

                // If the passed object is not ThreadTask<U> Type, return False
                if (obj is not ThreadTask<U>)
                {
                    return false;
                }

                return
                    ((ThreadTask<U>)obj).Element.Equals(obj: Element) &&
                    ((ThreadTask<U>)obj).Priority.Equals(obj: Priority);
            }

            /// <summary>
            /// Returns a hash code for the current object
            /// </summary>
            /// <returns>Hash code for the current object</returns>
            public override int GetHashCode()
            {
                return
                    Element.GetHashCode() ^
                    Priority.GetHashCode();
            }

            #endregion Methods

        }

    }
}
