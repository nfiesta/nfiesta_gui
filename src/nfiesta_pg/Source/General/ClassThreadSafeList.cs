﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System.Collections.Generic;

namespace ZaJi
{
    namespace NfiEstaPg
    {

        /// <summary>
        /// Thread safe list
        /// </summary>
        /// <typeparam name="T"></typeparam>
        public class ThreadSafeList<T>
        {

            #region Private Fields

            /// <summary>
            /// Locking object
            /// </summary>
            private readonly object locker = new();

            /// <summary>
            /// List object
            /// </summary>
            private readonly List<T> list = [];

            #endregion Private Fields


            #region Properties

            /// <summary>
            /// The number of elements contained in the list
            /// </summary>
            public int Count
            {
                get
                {
                    lock (locker)
                    {
                        return list.Count;
                    }
                }
            }

            /// <summary>
            /// Collection of elements contained in the list
            /// </summary>
            public IEnumerable<T> Items
            {
                get
                {
                    lock (locker)
                    {
                        return list;
                    }
                }
            }

            #endregion Properties


            #region Methods

            /// <summary>
            /// Adds an object to the end of the list
            /// </summary>
            /// <param name="item">Object</param>
            public void Add(T item)
            {
                lock (locker)
                {
                    list.Add(item: item);
                }
            }

            /// <summary>
            /// Add objects to the end of the queue
            /// </summary>
            /// <param name="items">Collection of objects</param>
            public void AddRange(IEnumerable<T> items)
            {
                lock (locker)
                {
                    if (items == null)
                    {
                        return;
                    }

                    foreach (T item in items)
                    {
                        list.Add(item: item);
                    }
                }
            }

            /// <summary>
            /// Determinates whether a sequence contains any elements
            /// </summary>
            /// <returns>true if the source sequence contains any elements; otherwise, false</returns>
            public bool Any()
            {
                lock (locker)
                {
                    return list.Count != 0;
                }
            }

            /// <summary>
            /// Removes all objects from the list
            /// </summary>
            public void Clear()
            {
                lock (locker)
                {
                    list.Clear();
                }
            }

            /// <summary>
            /// Removes the first occurrence of a specific object from the list
            /// </summary>
            public bool Remove(T item)
            {
                lock (locker)
                {
                    return
                        list.Remove(item: item);
                }
            }


            #endregion Methods

        }

    }
}