﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi
{
    namespace NfiEstaPg
    {

        /// <summary>
        /// Interface for target variable metadata table
        /// </summary>
        public interface ITargetVariableMetadataList
            : PostgreSQL.DataModel.ILinqView
        {

            #region Properties

            /// <summary>
            /// List of target variable metadata (read-only)
            /// </summary>
            System.Collections.Generic.List<ITargetVariableMetadata> Items { get; }

            #endregion Properties


            #region Indexer

            /// <summary>
            /// Target variable metadata from list by identifier (read-only)
            /// </summary>
            /// <param name="id">Target variable metadata identifier</param>
            /// <returns>Target variable metadata from list by identifier (null if not found)</returns>
            ITargetVariableMetadata this[int id] { get; }

            #endregion Indexer


            #region Methods

            /// <summary>
            /// Create new empty target variable metadata table
            /// </summary>
            /// <returns>New empty target variable metadata table</returns>
            ITargetVariableMetadataList Empty();

            /// <summary>
            /// List of target variable metadata
            /// for selected metadata element
            /// </summary>
            /// <param name="elementType">Selected metadata element type</param>
            /// <param name="element">Selected metadata element value</param>
            /// <returns>
            /// List of target variable metadata
            /// for selected metadata element
            /// </returns>
            ITargetVariableMetadataList Reduce(
                MetadataElementType elementType,
                MetadataElement element);

            /// <summary>
            /// List of metadata elements for selected element type
            /// </summary>
            /// <param name="elementType">Selected metadata element type</param>
            /// <returns>List of metadata elements for selected element type</returns>
            System.Collections.Generic.List<MetadataElement> GetMetadataElementsOfType(
                MetadataElementType elementType);

            #endregion Methods

        }


        /// <summary>
        /// Interface for row in target variable metadata table
        /// </summary>
        public interface ITargetVariableMetadata
            : PostgreSQL.DataModel.ILinqViewEntry
        {

            #region Properties

            /// <summary>
            /// List of metadata elements
            /// </summary>
            MetadataElementList MetadataElements { get; }

            #endregion Properties


            #region Derived Properties

            /// <summary>
            /// Identifier
            /// </summary>
            int Id { get; set; }

            /// <summary>
            /// Target variable identifier
            /// </summary>
            int TargetVariableId { get; set; }

            /// <summary>
            /// Generated local density identifier
            /// </summary>
            int LocalDensityId { get; set; }

            /// <summary>
            /// JsonValidity
            /// </summary>
            bool JsonValidity { get; set; }


            #region Indicator

            /// <summary>
            /// Indicator label in national language
            /// </summary>
            string IndicatorLabelCs { get; set; }

            /// <summary>
            /// Indicator description in national language
            /// </summary>
            string IndicatorDescriptionCs { get; set; }

            /// <summary>
            /// Indicator label in English
            /// </summary>
            string IndicatorLabelEn { get; set; }

            /// <summary>
            /// Indicator description in English
            /// </summary>
            string IndicatorDescriptionEn { get; set; }

            #endregion Indicator


            #region StateOrChange

            /// <summary>
            /// State or change label in national language
            /// </summary>
            string StateOrChangeLabelCs { get; set; }

            /// <summary>
            /// State or change description in national language
            /// </summary>
            string StateOrChangeDescriptionCs { get; set; }

            /// <summary>
            /// State or change label in English
            /// </summary>
            string StateOrChangeLabelEn { get; set; }

            /// <summary>
            /// State or change description in English
            /// </summary>
            string StateOrChangeDescriptionEn { get; set; }

            #endregion StateOrChange


            #region Unit

            /// <summary>
            /// Unit label in national language
            /// </summary>
            string UnitLabelCs { get; set; }

            /// <summary>
            /// Unit description in national language
            /// </summary>
            string UnitDescriptionCs { get; set; }

            /// <summary>
            /// Unit label in English
            /// </summary>
            string UnitLabelEn { get; set; }

            /// <summary>
            /// Unit description in English
            /// </summary>
            string UnitDescriptionEn { get; set; }

            #endregion Unit


            #region LocalDensityObjectType

            /// <summary>
            /// Local density object type label in national language
            /// </summary>
            string LocalDensityObjectTypeLabelCs { get; set; }

            /// <summary>
            /// Local density object type description in national language
            /// </summary>
            string LocalDensityObjectTypeDescriptionCs { get; set; }

            /// <summary>
            /// Local density object type label in English
            /// </summary>
            string LocalDensityObjectTypeLabelEn { get; set; }

            /// <summary>
            /// Local density object type description in English
            /// </summary>
            string LocalDensityObjectTypeDescriptionEn { get; set; }

            #endregion LocalDensityObjectType


            #region LocalDensityObject

            /// <summary>
            /// Local density object label in national language
            /// </summary>
            string LocalDensityObjectLabelCs { get; set; }

            /// <summary>
            /// Local density object description in national language
            /// </summary>
            string LocalDensityObjectDescriptionCs { get; set; }

            /// <summary>
            /// Local density object label in English
            /// </summary>
            string LocalDensityObjectLabelEn { get; set; }

            /// <summary>
            /// Local density object description in English
            /// </summary>
            string LocalDensityObjectDescriptionEn { get; set; }

            #endregion LocalDensityObject


            #region LocalDensity

            /// <summary>
            /// Local density contribution label in national language
            /// </summary>
            string LocalDensityLabelCs { get; set; }

            /// <summary>
            /// Local density contribution description in national language
            /// </summary>
            string LocalDensityDescriptionCs { get; set; }

            /// <summary>
            /// Local density contribution label in English
            /// </summary>
            string LocalDensityLabelEn { get; set; }

            /// <summary>
            /// Local density contribution description in English
            /// </summary>
            string LocalDensityDescriptionEn { get; set; }

            #endregion LocalDensity


            #region UseNegative

            /// <summary>
            /// Use negative value in national section of JSON file
            /// </summary>
            System.Nullable<bool> UseNegativeCs { get; set; }

            /// <summary>
            /// Use negative value in English section of JSON file
            /// </summary>
            System.Nullable<bool> UseNegativeEn { get; set; }

            #endregion UseNegative


            #region Version

            /// <summary>
            /// Version label in national language
            /// </summary>
            string VersionLabelCs { get; set; }

            /// <summary>
            /// Version description in national language
            /// </summary>
            string VersionDescriptionCs { get; set; }

            /// <summary>
            /// Version label in English
            /// </summary>
            string VersionLabelEn { get; set; }

            /// <summary>
            /// Version description in English
            /// </summary>
            string VersionDescriptionEn { get; set; }

            #endregion Version


            #region DefinitionVariant

            /// <summary>
            /// Definition variant label in national language
            /// </summary>
            string DefinitionVariantLabelCs { get; set; }

            /// <summary>
            /// Definition variant description in national language
            /// </summary>
            string DefinitionVariantDescriptionCs { get; set; }

            /// <summary>
            /// Definition variant label in English
            /// </summary>
            string DefinitionVariantLabelEn { get; set; }

            /// <summary>
            /// Definition variant description in English
            /// </summary>
            string DefinitionVariantDescriptionEn { get; set; }

            #endregion DefinitionVariant


            #region AreaDomain

            /// <summary>
            /// Area domain label in national language
            /// </summary>
            string AreaDomainLabelCs { get; set; }

            /// <summary>
            /// Area domain description in national language
            /// </summary>
            string AreaDomainDescriptionCs { get; set; }

            /// <summary>
            /// Area domain label in English
            /// </summary>
            string AreaDomainLabelEn { get; set; }

            /// <summary>
            /// Area domain description in English
            /// </summary>
            string AreaDomainDescriptionEn { get; set; }

            #endregion AreaDomain


            #region Population

            /// <summary>
            /// Population label in national language
            /// </summary>
            string PopulationLabelCs { get; set; }

            /// <summary>
            /// Population description in national language
            /// </summary>
            string PopulationDescriptionCs { get; set; }

            /// <summary>
            /// Population label in English
            /// </summary>
            string PopulationLabelEn { get; set; }

            /// <summary>
            /// Population description in English
            /// </summary>
            string PopulationDescriptionEn { get; set; }

            #endregion Population

            #endregion Derived Properties

        }


        /// <summary>
        /// Generic interface for row in target variable metadata table
        /// </summary>
        public interface ITargetVariableMetadata<T>
            : ITargetVariableMetadata,
            PostgreSQL.DataModel.ILinqViewEntry<T>
             where T : ITargetVariableMetadataList
        {
        }

    }
}
