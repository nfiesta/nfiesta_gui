﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Linq;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {

        /// <summary>
        /// Metadata element
        /// </summary>
        public class MetadataElement
        {

            #region Static Methods

            /// <summary>
            /// Metadata element caption
            /// </summary>
            /// <param name="elementType">Metadata element type</param>
            /// <param name="languageVersion">Language version</param>
            /// <returns>Metadata element caption</returns>
            public static string Caption(
                MetadataElementType elementType,
                LanguageVersion languageVersion)
            {
                return languageVersion switch
                {
                    LanguageVersion.National => elementType switch
                    {
                        MetadataElementType.Indicator => "indikátor",
                        MetadataElementType.StateOrChange => "stav nebo změna",
                        MetadataElementType.Unit => "jednotka",
                        MetadataElementType.LocalDensityObjectType => "typ objektu lokální hustoty",
                        MetadataElementType.LocalDensityObject => "objekt lokální hustoty",
                        MetadataElementType.LocalDensity => "příspěvek lokální hustoty",
                        MetadataElementType.Version => "verze",
                        MetadataElementType.DefinitionVariant => "definiční varianta",
                        MetadataElementType.AreaDomain => "plošná doména",
                        MetadataElementType.Population => "populace",
                        _ => String.Empty,
                    },
                    LanguageVersion.International => elementType switch
                    {
                        MetadataElementType.Indicator => "indicator",
                        MetadataElementType.StateOrChange => "state or change",
                        MetadataElementType.Unit => "unit",
                        MetadataElementType.LocalDensityObjectType => "local density object type",
                        MetadataElementType.LocalDensityObject => "local density object",
                        MetadataElementType.LocalDensity => "local density contribution",
                        MetadataElementType.Version => "version",
                        MetadataElementType.DefinitionVariant => "definition variant",
                        MetadataElementType.AreaDomain => "area domain",
                        MetadataElementType.Population => "population",
                        _ => String.Empty,
                    },
                    _ => String.Empty,
                };
            }

            #endregion  Static Methods


            #region Private Fields

            /// <summary>
            /// Metadata element type
            /// </summary>
            private MetadataElementType metadataElementType;

            /// <summary>
            /// Control for display metadata element
            /// </summary>
            private MetadataElementControl metadataElementControl;

            /// <summary>
            /// Target variable metadata
            /// </summary>
            private ITargetVariableMetadata metadata;

            #endregion Private Fields


            #region Constructor

            /// <summary>
            /// Metadata element constructor
            /// </summary>
            /// <param name="metadataElementType">Metadata element type</param>
            /// <param name="metadataElementControl">Control for display metadata element</param>
            /// <param name="metadata">Target variable metadata</param>
            public MetadataElement(
                MetadataElementType metadataElementType,
                MetadataElementControl metadataElementControl,
                ITargetVariableMetadata metadata = null
                )
            {
                MetadataElementType = metadataElementType;
                MetadataElementControl = metadataElementControl;
                Metadata = metadata;
            }

            #endregion Constructor


            #region Properties

            /// <summary>
            /// Metadata element identifier (read-only)
            /// </summary>
            public int Id
            {
                get
                {
                    return (int)MetadataElementType;
                }
            }

            /// <summary>
            /// Target variable identifier (read-only)
            /// </summary>
            public int TargetVariableId
            {
                get
                {
                    return (Metadata == null) ? 0 : Metadata.TargetVariableId;
                }
            }

            /// <summary>
            /// Metadata element type (read-only)
            /// </summary>
            public MetadataElementType MetadataElementType
            {
                get
                {
                    return metadataElementType;
                }
                private set
                {
                    metadataElementType = value;
                }
            }

            /// <summary>
            /// Control for display metadata element on form (read-only)
            /// </summary>
            public MetadataElementControl MetadataElementControl
            {
                get
                {
                    return metadataElementControl;
                }
                private set
                {
                    metadataElementControl = value;
                }
            }

            /// <summary>
            /// Target variable metadata (read-only) (nullable)
            /// </summary>
            public ITargetVariableMetadata Metadata
            {
                get
                {
                    return metadata;
                }
                private set
                {
                    metadata = value;
                }
            }


            /// <summary>
            /// Metadata element caption in national language (read-only)
            /// </summary>
            public string CaptionCs
            {
                get
                {
                    return Caption(
                        elementType: MetadataElementType,
                        languageVersion: LanguageVersion.National);
                }
            }

            /// <summary>
            /// Metadata element label in national language (read-only)
            /// </summary>
            public string LabelCs
            {
                get
                {
                    if (Metadata == null)
                    {
                        return String.Empty;
                    }

                    return MetadataElementType switch
                    {
                        MetadataElementType.Indicator => Metadata.IndicatorLabelCs,
                        MetadataElementType.StateOrChange => Metadata.StateOrChangeLabelCs,
                        MetadataElementType.Unit => Metadata.UnitLabelCs,
                        MetadataElementType.LocalDensityObjectType => Metadata.LocalDensityObjectTypeLabelCs,
                        MetadataElementType.LocalDensityObject => Metadata.LocalDensityObjectLabelCs,
                        MetadataElementType.LocalDensity => Metadata.LocalDensityLabelCs,
                        MetadataElementType.Version => Metadata.VersionLabelCs,
                        MetadataElementType.DefinitionVariant => Metadata.DefinitionVariantLabelCs,
                        MetadataElementType.AreaDomain => Metadata.AreaDomainLabelCs,
                        MetadataElementType.Population => Metadata.PopulationLabelCs,
                        _ => String.Empty,
                    };
                }
            }

            /// <summary>
            /// Metadata element description in national language (read-only)
            /// </summary>
            public string DescriptionCs
            {
                get
                {
                    if (Metadata == null)
                    {
                        return String.Empty;
                    }

                    return MetadataElementType switch
                    {
                        MetadataElementType.Indicator => Metadata.IndicatorDescriptionCs,
                        MetadataElementType.StateOrChange => Metadata.StateOrChangeDescriptionCs,
                        MetadataElementType.Unit => Metadata.UnitDescriptionCs,
                        MetadataElementType.LocalDensityObjectType => Metadata.LocalDensityObjectTypeDescriptionCs,
                        MetadataElementType.LocalDensityObject => Metadata.LocalDensityObjectDescriptionCs,
                        MetadataElementType.LocalDensity => Metadata.LocalDensityDescriptionCs,
                        MetadataElementType.Version => Metadata.VersionDescriptionCs,
                        MetadataElementType.DefinitionVariant => Metadata.DefinitionVariantDescriptionCs,
                        MetadataElementType.AreaDomain => Metadata.AreaDomainDescriptionCs,
                        MetadataElementType.Population => Metadata.PopulationDescriptionCs,
                        _ => String.Empty,
                    };
                }
            }


            /// <summary>
            /// Metadata element caption in international language (read-only)
            /// </summary>
            public string CaptionEn
            {
                get
                {
                    return Caption(
                        elementType: MetadataElementType,
                        languageVersion: LanguageVersion.International);
                }
            }

            /// <summary>
            /// Metadata element label in international language (read-only)
            /// </summary>
            public string LabelEn
            {
                get
                {
                    if (Metadata == null)
                    {
                        return String.Empty;
                    }

                    return MetadataElementType switch
                    {
                        MetadataElementType.Indicator => Metadata.IndicatorLabelEn,
                        MetadataElementType.StateOrChange => Metadata.StateOrChangeLabelEn,
                        MetadataElementType.Unit => Metadata.UnitLabelEn,
                        MetadataElementType.LocalDensityObjectType => Metadata.LocalDensityObjectTypeLabelEn,
                        MetadataElementType.LocalDensityObject => Metadata.LocalDensityObjectLabelEn,
                        MetadataElementType.LocalDensity => Metadata.LocalDensityLabelEn,
                        MetadataElementType.Version => Metadata.VersionLabelEn,
                        MetadataElementType.DefinitionVariant => Metadata.DefinitionVariantLabelEn,
                        MetadataElementType.AreaDomain => Metadata.AreaDomainLabelEn,
                        MetadataElementType.Population => Metadata.PopulationLabelEn,
                        _ => String.Empty,
                    };
                }
            }

            /// <summary>
            /// Metadata element description in international language (read-only)
            /// </summary>
            public string DescriptionEn
            {
                get
                {
                    if (Metadata == null)
                    {
                        return String.Empty;
                    }

                    return MetadataElementType switch
                    {
                        MetadataElementType.Indicator => Metadata.IndicatorDescriptionEn,
                        MetadataElementType.StateOrChange => Metadata.StateOrChangeDescriptionEn,
                        MetadataElementType.Unit => Metadata.UnitDescriptionEn,
                        MetadataElementType.LocalDensityObjectType => Metadata.LocalDensityObjectTypeDescriptionEn,
                        MetadataElementType.LocalDensityObject => Metadata.LocalDensityObjectDescriptionEn,
                        MetadataElementType.LocalDensity => Metadata.LocalDensityDescriptionEn,
                        MetadataElementType.Version => Metadata.VersionDescriptionEn,
                        MetadataElementType.DefinitionVariant => Metadata.DefinitionVariantDescriptionEn,
                        MetadataElementType.AreaDomain => Metadata.AreaDomainDescriptionEn,
                        MetadataElementType.Population => Metadata.PopulationDescriptionEn,
                        _ => String.Empty,
                    };
                }
            }

            #endregion Properties


            #region Methods

            /// <summary>
            /// Determines whether specified object is equal to the current object.
            /// </summary>
            /// <param name="obj">Speciefied object</param>
            /// <returns>true/false</returns>
            public override bool Equals(object obj)
            {
                // If the passed object is null, return False
                if (obj == null)
                {
                    return false;
                }

                // If the passed object is not MetadataElement Type, return False
                if (obj is not MetadataElement)
                {
                    return false;
                }

                return
                    (MetadataElementType == ((MetadataElement)obj).MetadataElementType) &&
                    ((LabelCs ?? String.Empty) == (((MetadataElement)obj).LabelCs ?? String.Empty)) &&
                    ((DescriptionCs ?? String.Empty) == (((MetadataElement)obj).DescriptionCs ?? String.Empty)) &&
                    ((LabelEn ?? String.Empty) == (((MetadataElement)obj).LabelEn ?? String.Empty)) &&
                    ((DescriptionEn ?? String.Empty) == (((MetadataElement)obj).DescriptionEn ?? String.Empty));
            }

            /// <summary>
            /// Serves as the default hash function.
            /// </summary>
            /// <returns>Hash code</returns>
            public override int GetHashCode()
            {
                return
                    MetadataElementType.GetHashCode() ^
                    LabelCs.GetHashCode() ^
                    DescriptionCs.GetHashCode() ^
                    LabelEn.GetHashCode() ^
                    DescriptionEn.GetHashCode();
            }

            /// <summary>
            /// Returns a string that represents the current object.
            /// </summary>
            /// <returns>String that represents the current object.</returns>
            public override string ToString()
            {
                return
                    String.Concat(
                        $"{"Name"} = {Functions.PrepStringArg(MetadataElementType.ToString())}.{Environment.NewLine}",
                        $"{"LabelCs"} = {Functions.PrepStringArg(LabelCs)}.{Environment.NewLine}",
                        $"{"DescriptionCs"} = {Functions.PrepStringArg(DescriptionCs)}.{Environment.NewLine}",
                        $"{"LabelEn"} = {Functions.PrepStringArg(LabelEn)}.{Environment.NewLine}",
                        $"{"DescriptionEn"} = {Functions.PrepStringArg(DescriptionEn)}.{Environment.NewLine}");
            }

            #endregion Methods

        }


        /// <summary>
        /// List of metadata elements
        /// </summary>
        public class MetadataElementList
        {

            #region Static Methods

            /// <summary>
            /// List of metadata elements (standard version)
            /// </summary>
            /// <param name="metadata">Target variable metadata</param>
            /// <returns>List of metadata elements (standard version)</returns>
            public static MetadataElementList Standard(
                ITargetVariableMetadata metadata = null)
            {
                return new MetadataElementList(
                    metadata: metadata,
                    design: TargetVariableSelectorDesign.Standard);
            }

            /// <summary>
            /// List of metadata elements (simple version)
            /// </summary>
            /// <param name="metadata">Target variable metadata</param>
            /// <returns>List of metadata elements (simple version)</returns>
            public static MetadataElementList Simple(
                ITargetVariableMetadata metadata = null)
            {
                return new MetadataElementList(
                    metadata: metadata,
                    design: TargetVariableSelectorDesign.Simple);
            }

            #endregion Static Methods


            #region Private Fields

            /// <summary>
            /// Target variable metadata
            /// </summary>
            private ITargetVariableMetadata metadata;

            /// <summary>
            /// Design of the user control "TargetVariableSelector"
            /// </summary>
            private TargetVariableSelectorDesign design;

            #endregion Private Fields


            #region Constructor

            /// <summary>
            /// Constructor
            /// </summary>
            /// <param name="metadata">Target variable metadata</param>
            /// <param name="design">Design of the user control "TargetVariableSelector"</param>
            private MetadataElementList(
                ITargetVariableMetadata metadata,
                TargetVariableSelectorDesign design)
            {
                Metadata = metadata;
                Design = design;
            }

            #endregion Constructor


            #region Properties

            /// <summary>
            /// Target variable metadata (read-only) (nullable)
            /// </summary>
            public ITargetVariableMetadata Metadata
            {
                get
                {
                    return metadata;
                }
                private set
                {
                    metadata = value;
                }
            }

            /// <summary>
            /// Design of the user control "TargetVariableSelector" (read-only)
            /// </summary>
            public TargetVariableSelectorDesign Design
            {
                get
                {
                    return design;
                }
                private set
                {
                    design = value;
                }
            }

            /// <summary>
            /// List of metadata elements (read-only)
            /// </summary>
            public List<MetadataElement> Items
            {
                get
                {
                    return
                    ((MetadataElementType[])Enum
                        .GetValues(typeof(MetadataElementType)))
                        .Select(a => new MetadataElement(
                            metadataElementType: a,
                            metadataElementControl:
                                GetMetadataElementControl(elementType: a),
                            metadata: Metadata))
                        .ToList<MetadataElement>();
                }
            }

            #endregion Properties


            #region Indexers

            /// <summary>
            /// Metadata element from list by its type (read-only)
            /// </summary>
            /// <param name="type">Metadata element type</param>
            /// <returns>Metadata element from list by its type</returns>
            public MetadataElement this[MetadataElementType type]
            {
                get
                {
                    return
                        new MetadataElement(
                            metadataElementType: type,
                            metadataElementControl:
                                GetMetadataElementControl(elementType: type),
                            metadata: Metadata);
                }
            }

            #endregion Indexers


            #region Methods

            /// <summary>
            /// Control for display metadata element
            /// </summary>
            /// <param name="elementType">Metadata element type</param>
            /// <returns>Returns control for display metadata element</returns>
            private MetadataElementControl GetMetadataElementControl(
                MetadataElementType elementType)
            {
                return elementType switch
                {
                    MetadataElementType.Indicator => Design switch
                    {
                        TargetVariableSelectorDesign.Standard => MetadataElementControl.ListBox,
                        TargetVariableSelectorDesign.Simple => MetadataElementControl.ComboBox,
                        _ => MetadataElementControl.ListBox,
                    },
                    MetadataElementType.StateOrChange => MetadataElementControl.ComboBox,
                    MetadataElementType.Unit => MetadataElementControl.ComboBox,
                    MetadataElementType.LocalDensityObjectType => MetadataElementControl.None,
                    MetadataElementType.LocalDensityObject => MetadataElementControl.None,
                    MetadataElementType.LocalDensity => MetadataElementControl.ComboBox,
                    MetadataElementType.Version => MetadataElementControl.ComboBox,
                    MetadataElementType.DefinitionVariant => MetadataElementControl.ComboBox,
                    MetadataElementType.AreaDomain => MetadataElementControl.ComboBox,
                    MetadataElementType.Population => MetadataElementControl.ComboBox,
                    _ => MetadataElementControl.None,
                };
            }

            #endregion Methods

        }


        /// <summary>
        /// Metadata element type
        /// </summary>
        public enum MetadataElementType
        {

            /// <summary>
            /// Indicator
            /// </summary>
            Indicator = 100,

            /// <summary>
            /// State or change
            /// </summary>
            StateOrChange = 200,

            /// <summary>
            /// Unit
            /// </summary>
            Unit = 300,

            /// <summary>
            /// Local density object type
            /// </summary>
            LocalDensityObjectType = 400,

            /// <summary>
            /// Local density object
            /// </summary>
            LocalDensityObject = 500,

            /// <summary>
            /// Local density contribution
            /// </summary>
            LocalDensity = 600,

            /// <summary>
            /// Version
            /// </summary>
            Version = 700,

            /// <summary>
            /// Definition variant
            /// </summary>
            DefinitionVariant = 800,

            /// <summary>
            /// Area domain
            /// </summary>
            AreaDomain = 900,

            /// <summary>
            /// Population
            /// </summary>
            Population = 1000

        }


        /// <summary>
        /// Control for display metadata element on form
        /// </summary>
        public enum MetadataElementControl
        {

            /// <summary>
            /// None
            /// </summary>
            None = 100,

            /// <summary>
            /// ListBox
            /// </summary>
            ListBox = 200,

            /// <summary>
            /// ComboBox
            /// </summary>
            ComboBox = 300

        }


        /// <summary>
        /// Design of the user control "TargetVariableSelector"
        /// </summary>
        public enum TargetVariableSelectorDesign
        {

            /// <summary>
            /// Display Indicator in ListBox
            /// </summary>
            Standard = 100,

            /// <summary>
            /// Display Indicator in ComboBox
            /// </summary>
            Simple = 200
        }

    }
}
