﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi
{
    namespace NfiEstaPg
    {

        /// <summary>
        /// Interface for lookup table with areal or population categories
        /// </summary>
        public interface IArealOrPopulationCategoryTable
            : PostgreSQL.DataModel.ILookupTable
        {
        }


        /// <summary>
        /// Interface for row in lookup table with areal or population categories
        /// </summary>
        public interface IArealOrPopulationCategory
            : PostgreSQL.DataModel.ILookupTableEntry
        {
        }


        /// <summary>
        /// Generic interface for row in lookup table with areal or population categories
        /// </summary>
        public interface IArealOrPopulationCategory<T>
            : IArealOrPopulationCategory,
            PostgreSQL.DataModel.ILookupTableEntry<T>
             where T : IArealOrPopulationCategoryTable
        {
        }

    }
}
