﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {

        /// <summary>
        /// <para lang="cs">Řídící vlákno</para>
        /// <para lang="en">Control thread</para>
        /// </summary>
        /// <typeparam name="W">
        /// <para lang="cs">Pracovní vlákno</para>
        /// <para lang="en">Worker thread</para>
        /// </typeparam>
        /// <typeparam name="T">
        /// <para lang="cs">Úkol zpracovávaný vláknem</para>
        /// <para lang="en">Task processed by thread</para>
        /// </typeparam>
        /// <typeparam name="U">
        /// <para lang="cs">Typ úkolu</para>
        /// <para lang="en">Task type</para>
        /// </typeparam>
        public class ControlThread<W, T, U>
            : IControlThread
            where W : WorkerThread<T, U>, new()
            where T : ThreadTask<U>
        {

            #region Constants

            /// <summary>
            /// <para lang="cs">Hodnota určuje, zda vlákno je nebo není na pozadí</para>
            /// <para lang="en">Value indicating whether or not a thread is a background thread</para>
            /// </summary>
            private const bool isBackground = true;

            #endregion Constants


            #region Events

            /// <summary>
            /// <para lang="cs">Událost "Zahájení řídícího vlákna"</para>
            /// <para lang="en">"Starting a control thread" event</para>
            /// </summary>
            public event EventHandler OnControlStart;

            /// <summary>
            /// <para lang="cs">Událost "Zahájení pracovního vlákna"</para>
            /// <para lang="en">"Starting a worker thread" event</para>
            /// </summary>
            public event EventHandler OnWorkerStart;

            /// <summary>
            /// <para lang="cs">Událost "Chyba při připojení k databázi"</para>
            /// <para lang="en">"Database connection error" event</para>
            /// </summary>
            public event EventHandler OnConnectionException;

            /// <summary>
            /// <para lang="cs">Událost "Zahájení jednoho úkolu pracovního vlákna"</para>
            /// <para lang="en">"Starting one task of a worker thread" event</para>
            /// </summary>
            public event EventHandler OnTaskStart;

            /// <summary>
            /// <para lang="cs">Událost "Chyba při zpracování úkolu"</para>
            /// <para lang="en">"Task processing error" event</para>
            /// </summary>
            public event EventHandler OnTaskException;

            /// <summary>
            /// <para lang="cs">Událost "Ukončení jednoho úkolu pracovního vlákna"</para>
            /// <para lang="en">"Terminating one task of a worker thread" event</para>
            /// </summary>
            public event EventHandler OnTaskComplete;

            /// <summary>
            /// <para lang="cs">Událost "Ukončení pracovního vlákna"</para>
            /// <para lang="en">"Terminating a worker thread" event</para>
            /// </summary>
            public event EventHandler OnWorkerComplete;

            /// <summary>
            /// <para lang="cs">Událost "Ukončení řídícího vlákna"</para>
            /// <para lang="en">"Terminating a control thread" event</para>
            /// </summary>
            public event EventHandler OnControlComplete;

            #endregion Events


            #region Private Fields

            /// <summary>
            /// <para lang="cs">Metoda spouštěná vláknem</para>
            /// <para lang="en">Method triggered by a thread</para>
            /// </summary>
            private ParameterizedThreadStart method = null;

            /// <summary>
            /// <para lang="cs">Objekt vlákna</para>
            /// <para lang="en">Thread object</para>
            /// </summary>
            private Thread thread = null;

            /// <summary>
            /// <para lang="cs">Úkoly čekající na zpracování</para>
            /// <para lang="en">Tasks awaiting processing</para>
            /// </summary>
            private ThreadSafeQueue<T> tasksToDo = null;

            /// <summary>
            /// <para lang="cs">Úkoly zpracované</para>
            /// <para lang="en">Tasks processed</para>
            /// </summary>
            private ThreadSafeQueue<T> tasksDone = null;

            /// <summary>
            /// <para lang="cs">Celkový počet úkolů</para>
            /// <para lang="en">Total tasks number</para>
            /// </summary>
            private Nullable<int> tasksTotal = null;

            /// <summary>
            /// <para lang="cs">Výsledek</para>
            /// <para lang="en">Result</para>
            /// </summary>
            private ThreadSafeQueue<object> result = null;

            /// <summary>
            /// <para lang="cs">
            /// Seznam zpráv z pracovních vláken
            /// </para>
            /// <para lang="en">
            /// List of messages from worker threads
            /// </para>
            /// </summary>
            private ThreadSafeList<string> log;

            /// <summary>
            /// <para lang="cs">Seznam pracovních vláken</para>
            /// <para lang="en">List of worker threads</para>
            /// </summary>
            private ThreadSafeList<W> workers = null;

            /// <summary>
            /// <para lang="cs">Zámek</para>
            /// <para lang="en">Locking object</para>
            /// </summary>
            private readonly object locker = new();

            /// <summary>
            /// <para lang="cs">Stopky</para>
            /// <para lang="en">Stopwatch</para>
            /// </summary>
            private System.Diagnostics.Stopwatch stopWatch = null;

            #endregion Private Fields


            #region Properties

            /// <summary>
            /// <para lang="cs">Výsledek</para>
            /// <para lang="en">Result</para>
            /// </summary>
            public ThreadSafeQueue<object> Result
            {
                get
                {
                    return result ??
                        new ThreadSafeQueue<object>();
                }
                set
                {
                    result = value ??
                        new ThreadSafeQueue<object>();
                }
            }

            /// <summary>
            /// <para lang="cs">
            /// Seznam zpráv z pracovních vláken
            /// </para>
            /// <para lang="en">
            /// List of messages from worker threads
            /// </para>
            /// </summary>
            public ThreadSafeList<string> Log
            {
                get
                {
                    return log ??
                        new ThreadSafeList<string>();
                }
            }

            #endregion Properties


            #region Methods

            /// <summary>
            /// <para lang="cs">Spuštění řídícího vlákna</para>
            /// <para lang="en">Starting a control thread</para>
            /// </summary>
            /// <param name="workersCount">
            /// <para lang="cs">Počet pracovních vláken</para>
            /// <para lang="en">Worker threads count</para>
            /// </param>
            /// <param name="tasksToDo">
            /// <para lang="cs">Úkoly čekající na zpracování</para>
            /// <para lang="en">Tasks awaiting processing</para>
            /// </param>
            /// <param name="connection">
            /// <para lang="cs">Připojení k databázi</para>
            /// <para lang="en">Database connection</para>
            /// </param>
            public void Start(
                int workersCount,
                ThreadSafeQueue<T> tasksToDo,
                ZaJi.PostgreSQL.PostgreSQLWrapper connection = null)
            {
                method = new ParameterizedThreadStart(BackgroundTask);

                thread =
                    new Thread(start: method)
                    { IsBackground = isBackground };

                this.tasksToDo = tasksToDo;

                tasksTotal = this.tasksToDo.Count;

                tasksDone = new ThreadSafeQueue<T>();

                result = new ThreadSafeQueue<object>();

                log = new ThreadSafeList<string>();

                CreateWorkers(
                    workersCount: workersCount,
                    connection: connection);

                stopWatch = new System.Diagnostics.Stopwatch();
                stopWatch.Start();

                thread.Start(parameter: null);
            }

            /// <summary>
            /// <para lang="cs">Zastavení řídícího vlákna</para>
            /// <para lang="en">Stopping a control thread</para>
            /// </summary>
            public void Stop()
            {
                StopWorkers();
            }

            /// <summary>
            /// <para lang="cs">Vytvoření pracovních vláken</para>
            /// <para lang="en">Create worker threads</para>
            /// </summary>
            /// <param name="workersCount">
            /// <para lang="cs">Počet pracovních vláken</para>
            /// <para lang="en">Worker threads count</para>
            /// </param>
            /// <param name="connection">
            /// <para lang="cs">Připojení k databázi</para>
            /// <para lang="en">Database connection</para>
            /// </param>
            private void CreateWorkers(
                int workersCount,
                ZaJi.PostgreSQL.PostgreSQLWrapper connection = null)
            {
                workers = new ThreadSafeList<W>();

                workersCount =
                    (workersCount <= tasksToDo.Count)
                        ? workersCount
                        : tasksToDo.Count;

                for (int i = 1; i <= workersCount; i++)
                {
                    W worker = new()
                    {
                        Id = i
                    };

                    if (connection != null)
                    {
                        worker.Connection = connection;
                    }

                    workers.Add(item: worker);

                    worker.OnWorkerStart += new EventHandler(
                        (sender, e) =>
                        {
                            // Invoke
                            ThreadEventArgs args = (ThreadEventArgs)e;
                            OnWorkerStart?.Invoke(
                                sender: sender,
                                e: new ThreadEventArgs()
                                {
                                    Id = args.Id,
                                    Task = null,
                                    TasksTotal = tasksTotal,
                                    TasksCompleted = tasksDone?.Count,
                                    ElapsedTime = null,
                                    ExceptionEventArgs = null,
                                    Notice = null,
                                    Tag = null
                                });
                        });

                    worker.OnConnectionException += new EventHandler(
                        (sender, e) =>
                        {
                            ThreadEventArgs args = (ThreadEventArgs)e;

                            // Log
                            string strWorkerThreadId =
                                (args.Id != null)
                                    ? args.Id.ToString()
                                    : Functions.StrNull;

                            string strTask =
                                (args.Task != null)
                                    ? args.Task.ToString()
                                    : Functions.StrNull;

                            string message =
                                (args.ExceptionEventArgs != null)
                                    ? (args.ExceptionEventArgs.MessageBoxText ?? Functions.StrNull)
                                    : Functions.StrNull;

                            log.Add(item: String.Concat(
                                $"----------------------------------------------------------------------------------------------------{Environment.NewLine}",
                                $"WorkerThread: {strWorkerThreadId} {Environment.NewLine}",
                                $"{strTask} {Environment.NewLine}",
                                $"Message: \"{message}\"; {Environment.NewLine}",
                                $"----------------------------------------------------------------------------------------------------{Environment.NewLine}"
                                ));

                            // Invoke
                            OnConnectionException?.Invoke(
                                sender: sender,
                                e: new ThreadEventArgs()
                                {
                                    Id = args.Id,
                                    Task = null,
                                    TasksTotal = tasksTotal,
                                    TasksCompleted = tasksDone?.Count,
                                    ElapsedTime = null,
                                    ExceptionEventArgs = args.ExceptionEventArgs,
                                    Notice = null,
                                    Tag = null
                                });
                        });

                    worker.OnTaskStart += new EventHandler(
                        (sender, e) =>
                        {
                            // Set Worker Task
                            if (tasksToDo.TryDequeue(out T task))
                            {
                                worker.Task = (T)task;
                            }
                            else
                            {
                                //ukončí pracovní vlákno
                                worker.Task = null;
                            }

                            // Invoke
                            if (worker.Task != null)
                            {
                                ThreadEventArgs args = (ThreadEventArgs)e;
                                OnTaskStart?.Invoke(
                                    sender: sender,
                                    e: new ThreadEventArgs()
                                    {
                                        Id = args.Id,
                                        Task = worker.Task,
                                        TasksTotal = tasksTotal,
                                        TasksCompleted = tasksDone?.Count,
                                        ElapsedTime = null,
                                        ExceptionEventArgs = null,
                                        Notice = null,
                                        Tag = null
                                    });
                            }
                        });

                    worker.OnTaskException += new EventHandler(
                       (sender, e) =>
                       {
                           ThreadEventArgs args = (ThreadEventArgs)e;

                           // Log
                           string strWorkerThreadId =
                                (args.Id != null)
                                    ? args.Id.ToString()
                                    : Functions.StrNull;

                           string strTask =
                               (args.Task != null)
                                   ? args.Task.ToString()
                                   : Functions.StrNull;

                           string message =
                               (args.ExceptionEventArgs != null)
                                   ? (args.ExceptionEventArgs.MessageBoxText ?? Functions.StrNull)
                                   : Functions.StrNull;

                           log.Add(item: String.Concat(
                               $"----------------------------------------------------------------------------------------------------{Environment.NewLine}",
                               $"WorkerThread: {strWorkerThreadId} {Environment.NewLine}",
                               $"{strTask} {Environment.NewLine}",
                               $"Message: \"{message}\"; {Environment.NewLine}",
                               $"----------------------------------------------------------------------------------------------------{Environment.NewLine}"
                               ));

                           // Invoke
                           OnTaskException?.Invoke(
                                sender: sender,
                                e: new ThreadEventArgs()
                                {
                                    Id = args.Id,
                                    Task = args.Task,
                                    TasksTotal = tasksTotal,
                                    TasksCompleted = tasksDone?.Count,
                                    ElapsedTime = null,
                                    ExceptionEventArgs = args.ExceptionEventArgs,
                                    Notice = null,
                                    Tag = null
                                });
                       });

                    worker.OnTaskComplete += new EventHandler(
                       (sender, e) =>
                       {
                           if (worker.Task != null)
                           {
                               ThreadEventArgs args = (ThreadEventArgs)e;

                               tasksDone.Enqueue(item: worker.Task);
                               result.Enqueue(item: worker.Task.Result);

                               OnTaskComplete?.Invoke(
                                    sender: sender,
                                    e: new ThreadEventArgs()
                                    {
                                        Id = args.Id,
                                        Task = args.Task,
                                        TasksTotal = tasksTotal,
                                        TasksCompleted = tasksDone?.Count,
                                        ElapsedTime = null,
                                        ExceptionEventArgs = null,
                                        Notice = args.Notice,
                                        Tag = null
                                    });
                           }
                       });

                    worker.OnWorkerComplete += new EventHandler(
                       (sender, e) =>
                       {
                           ThreadEventArgs args = (ThreadEventArgs)e;

                           // Invoke WorkerComplete
                           OnWorkerComplete?.Invoke(
                               sender: sender,
                               e: new ThreadEventArgs()
                               {
                                   Id = args.Id,
                                   Task = null,
                                   TasksTotal = tasksTotal,
                                   TasksCompleted = tasksDone?.Count,
                                   ElapsedTime = null,
                                   ExceptionEventArgs = null,
                                   Notice = null,
                                   Tag = null
                               });

                           // Invoke ControlComplete
                           lock (locker)
                           {
                               workers.Remove(item: worker);
                               if (!workers.Any())
                               {
                                   stopWatch.Stop();

                                   OnControlComplete?.Invoke(
                                    sender: sender,
                                    e: new ThreadEventArgs()
                                    {
                                        Id = 0,
                                        Task = null,
                                        TasksTotal = tasksTotal,
                                        TasksCompleted = tasksDone?.Count,
                                        ElapsedTime = 0.001 * stopWatch.ElapsedMilliseconds,
                                        ExceptionEventArgs = null,
                                        Notice = null,
                                        Tag = null
                                    });
                               }
                           }
                       });
                }
            }

            /// <summary>
            /// <para lang="cs">Spuštění pracovních vláken</para>
            /// <para lang="en">Starting worker threads</para>
            /// </summary>
            private void StartWorkers()
            {
                if (workers == null)
                {
                    throw new Exception("No workers to start");
                }

                // Nutno udělat kopii seznamu pracovních vláken
                // Pokud první vlákno dokončí práci dříve než je poslední spuštěno
                // je ze seznamu odebráno, tj. změní se kolekce přes kterou se provádí foreach,
                // a vyvolá to výjimku
                List<W> workersCopy = workers.Items.ToList<W>();

                foreach (W worker in workersCopy)
                {
                    worker.Start();
                }
            }

            /// <summary>
            /// <para lang="cs">Zastavení pracovních vláken</para>
            /// <para lang="en">Stopping worker threads</para>
            /// </summary>
            private void StopWorkers()
            {
                if (workers == null)
                {
                    throw new Exception("No workers to stop");
                }

                // Nutno udělat kopii seznamu pracovních vláken
                // Pokud první vlákno dokončí práci dříve než je poslední spuštěno
                // je ze seznamu odebráno, tj. změní se kolekce přes kterou se provádí foreach,
                // a vyvolá to výjimku
                List<W> workersCopy = workers.Items.ToList<W>();

                foreach (W worker in workersCopy)
                {
                    worker.Stop();
                }
            }

            /// <summary>
            /// <para lang="cs">Operace vykonávané řídícím vláknem</para>
            /// <para lang="en">Operations performed by the control thread</para>
            /// </summary>
            /// <param name="obj">
            /// <para lang="cs">Objekt s daty, která se předávají dovnitř vlákna</para>
            /// <para lang="en">Object with data that is passed inside the thread</para>
            /// </param>
            private void BackgroundTask(object obj)
            {
                OnControlStart?.Invoke(
                    sender: this,
                    e: new ThreadEventArgs()
                    {
                        Id = 0,
                        Task = null,
                        TasksTotal = tasksTotal,
                        TasksCompleted = tasksDone?.Count,
                        ElapsedTime = null,
                        ExceptionEventArgs = null,
                        Notice = null,
                        Tag = null
                    });

                StartWorkers();
            }

            #endregion Methods

        }

    }
}