﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {

        /// <summary>
        /// Combination of target variable and sub-population category and area domain category
        /// for numerator and denumerator in case of ratio estimate
        /// </summary>
        public class VariablePair
        {

            #region Private Fields

            /// <summary>
            /// Combination of target variable and sub-population and area domain category (extended-version)
            /// for numerator
            /// </summary>
            private Core.VwVariable numerator;

            /// <summary>
            /// Combination of target variable and sub-population and area domain category (extended-version)
            /// for denominator
            /// </summary>
            private Core.VwVariable denominator;

            #endregion Private Fields


            #region Constructor

            /// <summary>
            /// Constructor of combination of target variable and sub-population category and area domain category
            /// for numerator and denumerator in case of ratio estimate
            /// </summary>
            public VariablePair()
            {
                Numerator = null;
                Denominator = null;
            }

            /// <summary>
            /// Constructor of combination of target variable and sub-population category and area domain category
            /// for numerator and denumerator in case of ratio estimate
            /// </summary>
            /// <param name="numerator">Combination of target variable and sub-population and area domain category (extended-version) for numerator</param>
            public VariablePair(Core.VwVariable numerator)
            {
                Numerator = numerator;
                Denominator = null;
            }

            /// <summary>
            /// Constructor of combination of target variable and sub-population category and area domain category
            /// for numerator and denumerator in case of ratio estimate
            /// </summary>
            /// <param name="numerator">Combination of target variable and sub-population and area domain category for numerator (extended-version)</param>
            /// <param name="denominator">Combination of target variable and sub-population and area domain category for denominator (extended-version)</param>
            public VariablePair(Core.VwVariable numerator, Core.VwVariable denominator)
            {
                Numerator = numerator;
                Denominator = denominator;
            }

            #endregion Constructor


            #region Properties

            /// <summary>
            /// Combination of target variable and sub-population and area domain category (extended-version)
            /// for numerator
            /// </summary>
            public Core.VwVariable Numerator
            {
                get
                {
                    return numerator;
                }
                set
                {
                    numerator = value;
                }
            }


            /// <summary>
            /// Combination of target variable and sub-population and area domain category (extended-version)
            /// for denominator
            /// </summary>
            public Core.VwVariable Denominator
            {
                get
                {
                    return denominator;
                }
                set
                {
                    denominator = value;
                }
            }

            #endregion Properties


            #region Derived Properties

            /// <summary>
            /// Variable pair identifier (read-only)
            /// </summary>
            public string Id
            {
                get
                {
                    if ((Numerator != null) && (Denominator != null))
                    {
                        return $"{Numerator.VariableId} / {Denominator.VariableId}";
                    }
                    else if ((Numerator != null) && (Denominator == null))
                    {
                        return $"{Numerator.VariableId}";
                    }
                    else if ((Numerator == null) && (Denominator != null))
                    {
                        return $"{Denominator.VariableId}";
                    }
                    else
                    {
                        return String.Empty;
                    }
                }
            }


            /// <summary>
            /// Label of variable pair in national language (read-only)
            /// </summary>
            public string LabelCs
            {
                get
                {
                    if ((Numerator != null) && (Denominator != null))
                    {
                        return $"{Numerator.VariableLabelCs} / {Denominator.VariableLabelCs}";
                    }
                    else if ((Numerator != null) && (Denominator == null))
                    {
                        return $"{Numerator.VariableLabelCs}";
                    }
                    else if ((Numerator == null) && (Denominator != null))
                    {
                        return $"{Denominator.VariableLabelCs}";
                    }
                    else
                    {
                        return String.Empty;
                    }
                }
            }

            /// <summary>
            /// Extended label of variable pair in national language (read-only)
            /// </summary>
            public string ExtendedLabelCs
            {
                get
                {
                    return $"{Id} - {LabelCs}";
                }
            }


            /// <summary>
            /// Label of variable pair in English (read-only)
            /// </summary>
            public string LabelEn
            {
                get
                {
                    if ((Numerator != null) && (Denominator != null))
                    {
                        return $"{Numerator.VariableLabelEn} / {Denominator.VariableLabelEn}";
                    }
                    else if ((Numerator != null) && (Denominator == null))
                    {
                        return $"{Numerator.VariableLabelEn}";
                    }
                    else if ((Numerator == null) && (Denominator != null))
                    {
                        return $"{Denominator.VariableLabelEn}";
                    }
                    else
                    {
                        return String.Empty;
                    }
                }
            }

            /// <summary>
            /// Extended label of area domain in English (read-only)
            /// </summary>
            public string ExtendedLabelEn
            {
                get
                {
                    return $"{Id} - {LabelEn}";
                }
            }

            #endregion Derived Properties


            #region Methods

            /// <summary>
            /// Text description of combination of target variable and sub-population category and area domain category
            /// for numerator and denumerator in case of ratio estimate
            /// </summary>
            /// <returns>Text description of combination of target variable and sub-population category and area domain category
            /// for numerator and denumerator in case of ratio estimate</returns>
            public override string ToString()
            {
                return ((NfiEstaDB)Numerator.Composite.Database).Postgres.Setting.LanguageVersion switch
                {
                    LanguageVersion.National => String.Concat(
                                            $"{"ID"} = {Functions.PrepStringArg(arg: Id)},{Environment.NewLine}",
                                            $"{"Name"} = {Functions.PrepStringArg(arg: LabelCs)}.{Environment.NewLine}"
                                            ),
                    LanguageVersion.International => String.Concat(
                                            $"{"ID"} = {Functions.PrepStringArg(arg: Id)},{Environment.NewLine}",
                                            $"{"Name"} = {Functions.PrepStringArg(arg: LabelEn)}.{Environment.NewLine}"
                                            ),
                    _ => String.Concat(
                                            $"{"ID"} = {Functions.PrepStringArg(arg: Id)},{Environment.NewLine}",
                                            $"{"Name"} = {Functions.PrepStringArg(arg: LabelEn)}.{Environment.NewLine}"
                                            ),
                };
            }

            /// <summary>
            /// Determines whether specified object is equal to the current object
            /// </summary>
            /// <param name="obj">Speciefied object</param>
            /// <returns>true/false</returns>
            public override bool Equals(object obj)
            {
                // If the passed object is null, return False
                if (obj == null)
                {
                    return false;
                }

                // If the passed object is not VariablePair Type, return False
                if (obj is not VariablePair)
                {
                    return false;
                }

                VariablePair objVariable = (VariablePair)obj;

                if ((Numerator != null) && (Denominator != null) &&
                    (objVariable.Numerator != null) && (objVariable.Denominator != null))
                {
                    return Numerator.Equals(obj: objVariable.Numerator) &&
                           Denominator.Equals(obj: objVariable.Denominator);
                }

                else if ((Numerator != null) && (Denominator == null) &&
                   (objVariable.Numerator != null) && (objVariable.Denominator == null))
                {
                    return Numerator.Equals(obj: objVariable.Numerator);
                }

                else if ((Numerator == null) && (Denominator != null) &&
                   (objVariable.Numerator == null) && (objVariable.Denominator != null))
                {
                    return Denominator.Equals(obj: objVariable.Denominator);
                }

                else if ((Numerator == null) && (Denominator == null) &&
                    (objVariable.Numerator == null) && (objVariable.Denominator == null))
                {
                    return true;
                }

                else
                {
                    return false;
                }
            }

            /// <summary>
            /// Returns the hash code
            /// </summary>
            /// <returns>Hash code</returns>
            public override int GetHashCode()
            {
                if ((Numerator != null) && (Denominator != null))
                {
                    return Numerator.GetHashCode() ^ Denominator.GetHashCode();
                }
                else if ((Numerator != null) && (Denominator == null))
                {
                    return Numerator.GetHashCode();
                }
                else if ((Numerator == null) && (Denominator != null))
                {
                    return Denominator.GetHashCode();
                }
                else
                {
                    return 0;
                }
            }

            #endregion Methods

        }


        /// <summary>
        /// List of combinations of target variable and sub-population category and area domain category
        /// for numerator and denumerator in case of ratio estimate
        /// </summary>
        public class VariablePairList
        {

            #region Private Fields

            /// <summary>
            /// Database tables
            /// </summary>
            private NfiEstaDB database;

            /// <summary>
            /// List of combinations of target variable and sub-population category and area domain category
            /// for numerator and denumerator in case of ratio estimate
            /// </summary>
            private List<VariablePair> variablePairs;

            #endregion Private Fields


            #region Constructor

            /// <summary>
            /// Constructor for list of combinations of target variable and sub-population category and area domain category
            /// for numerator and denumerator in case of ratio estimate
            /// </summary>
            /// <param name="database">Database tables</param>
            public VariablePairList(NfiEstaDB database)
            {
                Database = database;
                VariablePairs = [];
            }

            /// <summary>
            /// Constructor for list of combinations of target variable and sub-population category and area domain category
            /// for numerator and denumerator in case of ratio estimate
            /// </summary>
            /// <param name="database">Database tables</param>
            /// <param name="variablePairs">List of combinations of target variable and sub-population category and area domain category
            /// for numerator and denumerator in case of ratio estimate</param>
            public VariablePairList(NfiEstaDB database, IEnumerable<VariablePair> variablePairs)
            {
                Database = database;
                VariablePairs = variablePairs.ToList();
            }

            #endregion Constructor


            #region Properties

            /// <summary>
            /// Short description of object (read-only)
            /// </summary>
            public string Caption
            {
                get
                {
                    return database.Postgres.Setting.LanguageVersion switch
                    {
                        LanguageVersion.National => "Seznam párů kombinací cílové proměnné a kategorie sub-populace a kategorie plošné domény pro čitatel a jmenovatel",
                        LanguageVersion.International => "List of pairs of combination of target variable and sub-population category and area domain category for numerator and denominator",
                        _ => "List of pairs of combination of target variable and sub-population category and area domain category for numerator and denominator",
                    };
                }
            }

            /// <summary>
            /// Database tables (read-only)
            /// </summary>
            public NfiEstaDB Database
            {
                get
                {
                    return database;
                }
                private set
                {
                    database = value ??
                        throw new ArgumentException(
                            message: $"Argument {nameof(Database)} must not be null.",
                            paramName: nameof(Database));
                }
            }

            /// <summary>
            /// List of combinations of target variable and sub-population category and area domain category
            /// for numerator and denumerator in case of ratio estimate (read-only)
            /// </summary>
            public List<VariablePair> VariablePairs
            {
                get
                {
                    return variablePairs;
                }
                private set
                {
                    variablePairs = value ??
                        throw new ArgumentException(
                            message: $"Argument {nameof(VariablePairs)} must not be null.",
                            paramName: nameof(VariablePairs));
                }
            }

            /// <summary>
            /// Number of elements (read-only)
            /// </summary>
            public int Count
            {
                get
                {
                    return VariablePairs.Count;
                }
            }

            /// <summary>
            /// List of combinations of target variable and sub-population and area domain category
            /// for numerator (read-only)
            /// </summary>
            public Core.VwVariableList Numerator
            {
                get
                {
                    IEnumerable<Core.VwVariable> eVariable = VariablePairs
                        .Where(a => a.Numerator != null)
                        .Select(a => a.Numerator)
                        .Distinct<Core.VwVariable>();

                    if (eVariable.Any())
                    {
                        DataTable attributeCategories = Core.VwVariableList.EmptyDataTable();

                        IEnumerable<DataRow> query =
                            from variable in database.SNfiEsta.VVariable.Data.AsEnumerable()
                            where (from a in eVariable select a.VariableId).Contains(variable.Field<int>(Core.VwVariableList.ColVariableId.Name))
                            select variable;

                        attributeCategories = query.Any() ?
                            query.CopyToDataTable() :
                            Core.VwVariableList.EmptyDataTable();

                        return new Core.VwVariableList(
                            database: Database,
                            data: attributeCategories);
                    }
                    else
                    {
                        return null;
                    }
                }
            }

            /// <summary>
            /// List of combinations of target variable and sub-population and area domain category
            /// for denominator (read-only)
            /// </summary>
            public Core.VwVariableList Denominator
            {
                get
                {
                    IEnumerable<Core.VwVariable> eVariable = VariablePairs
                        .Where(a => a.Denominator != null)
                        .Select(a => a.Denominator)
                        .Distinct<Core.VwVariable>();

                    if (eVariable.Any())
                    {
                        DataTable attributeCategories = Core.VwVariableList.EmptyDataTable();

                        IEnumerable<DataRow> query =
                            from variable in database.SNfiEsta.VVariable.Data.AsEnumerable()
                            where (from a in eVariable select a.VariableId).Contains(variable.Field<int>(Core.VwVariableList.ColVariableId.Name))
                            select variable;

                        attributeCategories = query.Any() ?
                            query.CopyToDataTable() :
                            Core.VwVariableList.EmptyDataTable();

                        return new Core.VwVariableList(
                            database: Database,
                            data: attributeCategories);
                    }
                    else
                    {
                        return null;
                    }
                }
            }

            #endregion Properties


            #region Methods

            /// <summary>
            /// Text description of the list of combinations of target variable and sub-population category and area domain category
            /// for numerator and denumerator in case of ratio estimate
            /// </summary>
            /// <returns>Text description of the list of combinations of target variable and sub-population category and area domain category
            /// for numerator and denumerator in case of ratio estimate</returns>
            public override string ToString()
            {
                return Database.Postgres.Setting.LanguageVersion switch
                {
                    LanguageVersion.National => String.Concat(
                                                $"{Caption}. Počet = {Count}."),
                    LanguageVersion.International => String.Concat(
                                                $"{Caption}. Count = {Count}."),
                    _ => String.Concat(
                                                $"{Caption}. Count = {Count}."),
                };
            }

            #endregion Methods

        }

    }
}

