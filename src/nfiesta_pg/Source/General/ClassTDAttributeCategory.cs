﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Versioning;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {

        /// <summary>
        /// Attribute category
        /// </summary>
        [SupportedOSPlatform("windows")]
        public class TDAttributeCategory
        {

            #region Private Fields

            /// <summary>
            /// List of attribute categories
            /// </summary>
            private readonly TDAttributeCategoryList composite;

            /// <summary>
            /// Combination of area domain categories
            /// </summary>
            private TDDomainCategoryCombination<TargetData.TDAreaDomainCategory> areaDomainCategoryCombination;

            /// <summary>
            /// Combination of subpopulation categories
            /// </summary>
            private TDDomainCategoryCombination<TargetData.TDSubPopulationCategory> subPopulationCategoryCombination;

            #endregion Private Fields


            #region Constructor

            /// <summary>
            /// Contructor
            /// </summary>
            /// <param name="composite">List of attribute categories</param>
            /// <param name="areaDomainCategoryCombination">Combination of area domain categories</param>
            /// <param name="subPopulationCategoryCombination">Combination of subpopulation categories</param>
            public TDAttributeCategory(
                TDAttributeCategoryList composite,
                TDDomainCategoryCombination<TargetData.TDAreaDomainCategory> areaDomainCategoryCombination = null,
                TDDomainCategoryCombination<TargetData.TDSubPopulationCategory> subPopulationCategoryCombination = null
                )
            {
                this.composite = composite;
                this.areaDomainCategoryCombination =
                    areaDomainCategoryCombination ??
                    new TDDomainCategoryCombination<TargetData.TDAreaDomainCategory>(database: ((NfiEstaDB)Composite.Database));
                this.subPopulationCategoryCombination =
                    subPopulationCategoryCombination ??
                    new TDDomainCategoryCombination<TargetData.TDSubPopulationCategory>(database: ((NfiEstaDB)Composite.Database));
            }

            #endregion Constructor


            #region Properties

            /// <summary>
            /// List of attribute categories (read-only)
            /// </summary>
            public TDAttributeCategoryList Composite
            {
                get
                {
                    return composite;
                }
            }

            /// <summary>
            /// Combination of area domain categories
            /// </summary>
            public TDDomainCategoryCombination<TargetData.TDAreaDomainCategory> AreaDomainCategoryCombination
            {
                get
                {
                    return areaDomainCategoryCombination;
                }
                set
                {
                    areaDomainCategoryCombination = value;
                }
            }

            /// <summary>
            /// Combination of subpopulation categories
            /// </summary>
            public TDDomainCategoryCombination<TargetData.TDSubPopulationCategory> SubPopulationCategoryCombination
            {
                get
                {
                    return subPopulationCategoryCombination;
                }
                set
                {
                    subPopulationCategoryCombination = value;
                }
            }

            #endregion Properties


            #region Derived Properties

            /// <summary>
            /// Attribute category identifier (read-only)
            /// </summary>
            public string Id
            {
                get
                {
                    if
                        (((areaDomainCategoryCombination == null) || (areaDomainCategoryCombination.Items.Count == 0)) &&
                         ((subPopulationCategoryCombination == null) || (subPopulationCategoryCombination.Items.Count == 0)))
                    {
                        return String.Empty;
                    }
                    else if
                        ((areaDomainCategoryCombination == null) || (areaDomainCategoryCombination.Items.Count == 0))
                    {
                        return $"({subPopulationCategoryCombination.Id})";
                    }
                    else if
                        ((subPopulationCategoryCombination == null) || (subPopulationCategoryCombination.Items.Count == 0))
                    {
                        return $"({areaDomainCategoryCombination.Id})";
                    }
                    else
                    {
                        return $"({areaDomainCategoryCombination.Id}) ({subPopulationCategoryCombination.Id})";
                    }
                }
            }


            /// <summary>
            /// Label of attribute category in national language (read-only)
            /// </summary>
            public string LabelCs
            {
                get
                {
                    if
                        (((areaDomainCategoryCombination == null) || (areaDomainCategoryCombination.Items.Count == 0)) &&
                         ((subPopulationCategoryCombination == null) || (subPopulationCategoryCombination.Items.Count == 0)))
                    {
                        return String.Empty;
                    }
                    else if
                        ((areaDomainCategoryCombination == null) || (areaDomainCategoryCombination.Items.Count == 0))
                    {
                        return $"({subPopulationCategoryCombination.LabelCs})";
                    }
                    else if
                        ((subPopulationCategoryCombination == null) || (subPopulationCategoryCombination.Items.Count == 0))
                    {
                        return $"({areaDomainCategoryCombination.LabelCs})";
                    }
                    else
                    {
                        return $"({areaDomainCategoryCombination.LabelCs}) ({subPopulationCategoryCombination.LabelCs})";
                    }
                }
            }

            /// <summary>
            /// Description of attribute category in national language (read-only)
            /// </summary>
            public string DescriptionCs
            {
                get
                {
                    if
                        (((areaDomainCategoryCombination == null) || (areaDomainCategoryCombination.Items.Count == 0)) &&
                         ((subPopulationCategoryCombination == null) || (subPopulationCategoryCombination.Items.Count == 0)))
                    {
                        return String.Empty;
                    }
                    else if
                        ((areaDomainCategoryCombination == null) || (areaDomainCategoryCombination.Items.Count == 0))
                    {
                        return $"({subPopulationCategoryCombination.DescriptionCs})";
                    }
                    else if
                        ((subPopulationCategoryCombination == null) || (subPopulationCategoryCombination.Items.Count == 0))
                    {
                        return $"({areaDomainCategoryCombination.DescriptionCs})";
                    }
                    else
                    {
                        return $"({areaDomainCategoryCombination.DescriptionCs}) ({subPopulationCategoryCombination.DescriptionCs})";
                    }
                }
            }

            /// <summary>
            /// Extended label of attribute category in national language (read-only)
            /// </summary>
            public string ExtendedLabelCs
            {
                get
                {
                    if (String.IsNullOrEmpty(value: Id))
                    {
                        return String.Empty;
                    }
                    else if (String.IsNullOrEmpty(value: LabelCs))
                    {
                        return String.Empty;
                    }
                    else
                    {
                        return $"{Id} - {LabelCs}";
                    }
                }
            }


            /// <summary>
            /// Label of attribute category in English (read-only)
            /// </summary>
            public string LabelEn
            {
                get
                {
                    if
                        (((areaDomainCategoryCombination == null) || (areaDomainCategoryCombination.Items.Count == 0)) &&
                         ((subPopulationCategoryCombination == null) || (subPopulationCategoryCombination.Items.Count == 0)))
                    {
                        return String.Empty;
                    }
                    else if
                        ((areaDomainCategoryCombination == null) || (areaDomainCategoryCombination.Items.Count == 0))
                    {
                        return $"({subPopulationCategoryCombination.LabelEn})";
                    }
                    else if
                        ((subPopulationCategoryCombination == null) || (subPopulationCategoryCombination.Items.Count == 0))
                    {
                        return $"({areaDomainCategoryCombination.LabelEn})";
                    }
                    else
                    {
                        return $"({areaDomainCategoryCombination.LabelEn}) ({subPopulationCategoryCombination.LabelEn})";
                    }
                }
            }

            /// <summary>
            /// Description of attribute category in English (read-only)
            /// </summary>
            public string DescriptionEn
            {
                get
                {
                    if
                        (((areaDomainCategoryCombination == null) || (areaDomainCategoryCombination.Items.Count == 0)) &&
                         ((subPopulationCategoryCombination == null) || (subPopulationCategoryCombination.Items.Count == 0)))
                    {
                        return String.Empty;
                    }
                    else if
                        ((areaDomainCategoryCombination == null) || (areaDomainCategoryCombination.Items.Count == 0))
                    {
                        return $"({subPopulationCategoryCombination.DescriptionEn})";
                    }
                    else if
                        ((subPopulationCategoryCombination == null) || (subPopulationCategoryCombination.Items.Count == 0))
                    {
                        return $"({areaDomainCategoryCombination.DescriptionEn})";
                    }
                    else
                    {
                        return $"({areaDomainCategoryCombination.DescriptionEn}) ({subPopulationCategoryCombination.DescriptionEn})";
                    }
                }
            }

            /// <summary>
            /// Extended label of attribute category in English (read-only)
            /// </summary>
            public string ExtendedLabelEn
            {
                get
                {
                    if (String.IsNullOrEmpty(value: Id))
                    {
                        return String.Empty;
                    }
                    else if (String.IsNullOrEmpty(value: LabelEn))
                    {
                        return String.Empty;
                    }
                    else
                    {
                        return $"{Id} - {LabelEn}";
                    }
                }
            }

            #endregion Derived Properties


            #region Methods

            /// <summary>
            /// Returns a string that represents the current object
            /// </summary>
            /// <returns>Returns a string that represents the current object</returns>
            public override string ToString()
            {
                return ((NfiEstaDB)Composite.Database).Postgres.Setting.LanguageVersion switch
                {
                    LanguageVersion.National => String.Concat(
                        $"{"ID"} = {Functions.PrepStringArg(arg: Id)},{Environment.NewLine}",
                        $"{"Name"} = {Functions.PrepStringArg(arg: LabelCs)}.{Environment.NewLine}"
                        ),
                    LanguageVersion.International => String.Concat(
                        $"{"ID"} = {Functions.PrepStringArg(arg: Id)},{Environment.NewLine}",
                        $"{"Name"} = {Functions.PrepStringArg(arg: LabelEn)}.{Environment.NewLine}"
                        ),
                    _ => String.Concat(
                        $"{"ID"} = {Functions.PrepStringArg(arg: Id)},{Environment.NewLine}",
                        $"{"Name"} = {Functions.PrepStringArg(arg: LabelEn)}.{Environment.NewLine}"
                        ),
                };
            }

            /// <summary>
            /// Determines whether specified object is equal to the current object
            /// </summary>
            /// <param name="obj">Speciefied object</param>
            /// <returns>true/false</returns>
            public override bool Equals(object obj)
            {
                // If the passed object is null, return False
                if (obj == null)
                {
                    return false;
                }

                // If the passed object is not TDAttributeCategory Type, return False
                if (obj is not TDAttributeCategory)
                {
                    return false;
                }

                return
                    Id == ((TDAttributeCategory)obj).Id;
            }

            /// <summary>
            /// Returns the hash code
            /// </summary>
            /// <returns>Hash code</returns>
            public override int GetHashCode()
            {
                return
                    Id.GetHashCode();
            }

            #endregion Methods

        }


        /// <summary>
        /// List of attribute categories
        /// </summary>
        [SupportedOSPlatform("windows")]
        public class TDAttributeCategoryList
        {

            #region Static Fields

            /// <summary>
            /// Description in national language
            /// </summary>
            private const string CaptionCs = "Seznam atributových kategorií";

            /// <summary>
            /// Description in English
            /// </summary>
            private const string CaptionEn = "List of attribute categories";

            #endregion Static Fields


            #region Private Fields

            /// <summary>
            /// Database tables
            /// </summary>
            private readonly NfiEstaDB database;

            /// <summary>
            /// List of attribute categories
            /// </summary>
            private readonly List<TDAttributeCategory> items;

            #endregion Private Fields


            #region Constructor

            /// <summary>
            /// Contructor
            /// </summary>
            /// <param name="database">Database tables</param>
            /// <param name="areaDomainCategoryCombinations">List of combinations of area domain categories</param>
            /// <param name="subPopulationCategoryCombinations">List of combinations of subpopulation categories</param>
            public TDAttributeCategoryList(
                NfiEstaDB database,
                TDDomainCategoryCombinationList<TargetData.TDAreaDomainCategory> areaDomainCategoryCombinations = null,
                TDDomainCategoryCombinationList<TargetData.TDSubPopulationCategory> subPopulationCategoryCombinations = null
                )
            {
                this.database = database;

                if
                    (((areaDomainCategoryCombinations == null) || (areaDomainCategoryCombinations.Items.Count == 0)) &&
                     ((subPopulationCategoryCombinations == null) || (subPopulationCategoryCombinations.Items.Count == 0)))
                {
                    items = [];
                }
                else if
                    ((areaDomainCategoryCombinations == null) || (areaDomainCategoryCombinations.Items.Count == 0))
                {
                    items = subPopulationCategoryCombinations.Items
                                 .Select(a => new TDAttributeCategory(
                                     composite: this,
                                     areaDomainCategoryCombination: null,
                                     subPopulationCategoryCombination: a))
                                 .ToList();
                }
                else if
                   ((subPopulationCategoryCombinations == null) || (subPopulationCategoryCombinations.Items.Count == 0))
                {
                    items = areaDomainCategoryCombinations.Items
                                 .Select(a => new TDAttributeCategory(
                                     composite: this,
                                     areaDomainCategoryCombination: a,
                                     subPopulationCategoryCombination: null))
                                 .ToList();
                }
                else
                {
                    items = (
                        from a in areaDomainCategoryCombinations.Items
                        from b in subPopulationCategoryCombinations.Items
                        select new TDAttributeCategory(
                            composite: this,
                            areaDomainCategoryCombination: a,
                            subPopulationCategoryCombination: b)
                    ).ToList();
                }
            }

            #endregion Constructor


            #region Properties

            /// <summary>
            /// Short description of object (read-only)
            /// </summary>
            public string Caption
            {
                get
                {
                    return Database.Postgres.Setting.LanguageVersion switch
                    {
                        LanguageVersion.National => CaptionCs,
                        LanguageVersion.International => CaptionEn,
                        _ => CaptionEn,
                    };
                }
            }

            /// <summary>
            /// List of attribute categories (read-only)
            /// </summary>
            public List<TDAttributeCategory> Items
            {
                get
                {
                    return items;
                }
            }

            /// <summary>
            /// Database tables (read-only)
            /// </summary>
            public NfiEstaDB Database
            {
                get
                {
                    return database;
                }
            }

            #endregion Properties


            #region Methods

            /// <summary>
            /// Displays list of attribute categories in panel
            /// </summary>
            /// <param name="panel">Panel</param>
            /// <param name="languageVersion">Language version</param>
            public void DisplayInPanel(
                System.Windows.Forms.Panel panel,
                LanguageVersion languageVersion)
            {
                panel.Controls.Clear();
                System.Windows.Forms.ListBox box =
                    new()
                    {
                        DataSource = Items,
                        DisplayMember =
                        (languageVersion == LanguageVersion.National) ? "ExtendedLabelCs" :
                        (languageVersion == LanguageVersion.International) ? "ExtendedLabelEn" :
                        "Id",
                        Dock = System.Windows.Forms.DockStyle.Fill,
                        Font = new System.Drawing.Font(
                        familyName: "Microsoft Sans Serif",
                        emSize: 9F,
                        style: System.Drawing.FontStyle.Regular,
                        unit: System.Drawing.GraphicsUnit.Point,
                        gdiCharSet: ((byte)(238))),
                        SelectionMode = System.Windows.Forms.SelectionMode.None,
                        ValueMember =
                        (languageVersion == LanguageVersion.National) ? "ExtendedLabelCs" :
                        (languageVersion == LanguageVersion.International) ? "ExtendedLabelEn" :
                        "Id",
                    };
                panel.Controls.Add(value: box);
            }

            /// <summary>
            /// Returns a string that represents the current object
            /// </summary>
            /// <returns>Returns a string that represents the current object</returns>
            public override string ToString()
            {
                return Database.Postgres.Setting.LanguageVersion switch
                {
                    LanguageVersion.National =>
                        String.Concat($"{Caption}. Počet = {Items.Count}."),
                    LanguageVersion.International =>
                        String.Concat($"{Caption}. Count = {Items.Count}."),
                    _ =>
                        String.Concat($"{Caption}. Count = {Items.Count}."),
                };
            }

            #endregion Methods

        }

    }
}