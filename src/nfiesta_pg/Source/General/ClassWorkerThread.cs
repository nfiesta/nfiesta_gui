﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Threading;

namespace ZaJi
{
    namespace NfiEstaPg
    {

        /// <summary>
        /// <para lang="cs">Pracovní vlákno</para>
        /// <para lang="en">Worker thread</para>
        /// </summary>
        /// <typeparam name="T">
        /// <para lang="cs">Úkol zpracovávaný vláknem</para>
        /// <para lang="en">Task processed by thread</para>
        /// </typeparam>
        /// <typeparam name="U">
        /// <para lang="cs">Typ úkolu</para>
        /// <para lang="en">Task type</para>
        /// </typeparam>
        public class WorkerThread<T, U>
            where T : ThreadTask<U>
        {

            #region Constants

            /// <summary>
            /// <para lang="cs">Hodnota určuje, zda vlákno je nebo není na pozadí</para>
            /// <para lang="en">Value indicating whether or not a thread is a background thread</para>
            /// </summary>
            private const bool isBackground = true;

            /// <summary>
            /// <para lang="cs">Udržovat připojení k databázi</para>
            /// <para lang="en">Keep database connection alive</para>
            /// </summary>
            private const bool stayConnected = true;

            #endregion Constants


            #region Events

            /// <summary>
            /// <para lang="cs">Událost "Zahájení pracovního vlákna"</para>
            /// <para lang="en">"Starting a worker thread" event</para>
            /// </summary>
            public event EventHandler OnWorkerStart;

            /// <summary>
            /// <para lang="cs">Událost "Chyba při připojení k databázi"</para>
            /// <para lang="en">"Database connection error" event</para>
            /// </summary>
            public event EventHandler OnConnectionException;

            /// <summary>
            /// <para lang="cs">Událost "Zahájení jednoho úkolu pracovního vlákna"</para>
            /// <para lang="en">"Starting one task of a worker thread" event</para>
            /// </summary>
            public event EventHandler OnTaskStart;

            /// <summary>
            /// <para lang="cs">Událost "Chyba při zpracování úkolu"</para>
            /// <para lang="en">"Task processing error" event</para>
            /// </summary>
            public event EventHandler OnTaskException;

            /// <summary>
            /// <para lang="cs">Událost "Ukončení jednoho úkolu pracovního vlákna"</para>
            /// <para lang="en">"Terminating one task of a worker thread" event</para>
            /// </summary>
            public event EventHandler OnTaskComplete;

            /// <summary>
            /// <para lang="cs">Událost "Ukončení pracovního vlákna"</para>
            /// <para lang="en">"Terminating a worker thread" event</para>
            /// </summary>
            public event EventHandler OnWorkerComplete;

            #endregion Events


            #region Private Fields

            /// <summary>
            /// <para lang="cs">Metoda spouštěná vláknem</para>
            /// <para lang="en">Method triggered by a thread</para>
            /// </summary>
            private ParameterizedThreadStart method = null;

            /// <summary>
            /// <para lang="cs">Objekt vlákna</para>
            /// <para lang="en">Thread object</para>
            /// </summary>
            private Thread thread = null;

            /// <summary>
            /// <para lang="cs">Požadavek na zastavení vlákna</para>
            /// <para lang="en">Request to stop the thread</para>
            /// </summary>
            private bool stopRequest = false;

            /// <summary>
            /// <para lang="cs">Provádí toto vlákno právě výpočet?</para>
            /// <para lang="en">Is this thread doing calculation?</para>
            /// </summary>
            private bool isWorking = false;

            /// <summary>
            /// <para lang="cs">Zpracovávaný úkol</para>
            /// <para lang="en">Task for processing</para>
            /// </summary>
            private T task = null;

            /// <summary>
            /// <para lang="cs">Zámek</para>
            /// <para lang="en">Locking object</para>
            /// </summary>
            private readonly object locker = new();

            /// <summary>
            /// <para lang="cs">Identifikační číslo pracovního vlákna</para>
            /// <para lang="en">Worker thread identifier</para>
            /// </summary>
            private Nullable<int> id = null;

            /// <summary>
            /// <para lang="cs">Připojení k databázi pro pracovní vlákna</para>
            /// <para lang="en">Database connection for worker thread</para>
            /// </summary>
            private ZaJi.PostgreSQL.PostgreSQLWrapper threadConnection = null;

            /// <summary>
            /// <para lang="cs">Připojení k databázi</para>
            /// <para lang="en">Database connection</para>
            /// </summary>
            private ZaJi.PostgreSQL.PostgreSQLWrapper connection = null;

            #endregion Private Fields


            #region Properties

            /// <summary>
            /// <para lang="cs">Identifikační číslo pracovního vlákna</para>
            /// <para lang="en">Worker thread identifier</para>
            /// </summary>
            public Nullable<int> Id
            {
                get
                {
                    lock (locker)
                    {
                        return id;
                    }
                }
                set
                {
                    lock (locker)
                    {
                        id = value;
                    }
                }
            }

            /// <summary>
            /// <para lang="cs">Zpracovávaný úkol</para>
            /// <para lang="en">Task for processing</para>
            /// </summary>
            public T Task
            {
                get
                {
                    lock (locker)
                    {
                        return task;
                    }
                }
                set
                {
                    lock (locker)
                    {
                        task = value;
                    }
                }
            }

            /// <summary>
            /// <para lang="cs">Provádí toto vlákno právě výpočet?</para>
            /// <para lang="en">Is this thread doing calculation?</para>
            /// </summary>
            public bool IsWorking
            {
                get
                {
                    lock (locker)
                    {
                        return isWorking;
                    }
                }
                private set
                {
                    lock (locker)
                    {
                        isWorking = value;
                    }
                }
            }

            /// <summary>
            /// <para lang="cs">Připojení k databázi</para>
            /// <para lang="en">Database connection</para>
            /// </summary>
            public virtual ZaJi.PostgreSQL.PostgreSQLWrapper Connection
            {
                get
                {
                    lock (locker)
                    {
                        return connection;
                    }
                }
                set
                {
                    lock (locker)
                    {
                        connection = value;
                    }
                }
            }

            /// <summary>
            /// <para lang="cs">Připojení k databázi pro pracovní vlákna</para>
            /// <para lang="en">Database connection for worker thread</para>
            /// </summary>
            protected ZaJi.PostgreSQL.PostgreSQLWrapper ThreadConnection
            {
                get
                {
                    lock (locker)
                    {
                        return threadConnection;
                    }
                }
            }

            #endregion Properties


            #region Methods

            /// <summary>
            /// <para lang="cs">Spuštění pracovního vlákna</para>
            /// <para lang="en">Starting a worker thread</para>
            /// </summary>
            public void Start()
            {
                method = new ParameterizedThreadStart(BackgroundTask);

                thread =
                    new Thread(start: method)
                    { IsBackground = isBackground };

                stopRequest = false;

                task = null;

                thread.Start(parameter: null);
            }

            /// <summary>
            /// <para lang="cs">Zastavení pracovního vlákna</para>
            /// <para lang="en">Stopping a worker thread</para>
            /// </summary>
            public void Stop()
            {
                stopRequest = true;
            }

            /// <summary>
            /// <para lang="cs">Čeká na dokončení činnosti vlákna</para>
            /// <para lang="en">Waits until all thread finishes its work</para>
            /// </summary>
            public void Join()
            {
                thread?.Join();
            }

            /// <summary>
            /// <para lang="cs">Připojení k databázi</para>
            /// <para lang="en">Database connection</para>
            /// </summary>
            private bool Connect()
            {
                if (Connection == null)
                {
                    return false;
                }

                threadConnection = new ZaJi.PostgreSQL.PostgreSQLWrapper()
                {
                    StayConnected = stayConnected
                };

                threadConnection.ConnectionError +=
                    new ZaJi.PostgreSQL.PostgreSQLWrapper.ConnectionErrorHandler(
                    (sender, e) =>
                    {
                        OnConnectionException?.Invoke(
                            sender: this,
                            e: new ThreadEventArgs()
                            {
                                Id = Id,
                                Task = null,
                                TasksTotal = null,
                                TasksCompleted = null,
                                ElapsedTime = null,
                                ExceptionEventArgs = e,
                                Notice = null,
                                Tag = null
                            });
                    });

                threadConnection.ExceptionReceived +=
                    new ZaJi.PostgreSQL.PostgreSQLWrapper.ExceptionReceivedHandler(
                    (sender, e) =>
                    {
                        OnTaskException?.Invoke(
                        sender: this,
                        e: new ThreadEventArgs()
                        {
                            Id = Id,
                            Task = Task,
                            TasksTotal = null,
                            TasksCompleted = null,
                            ElapsedTime = null,
                            ExceptionEventArgs = e,
                            Notice = null,
                            Tag = null
                        });
                    });

                threadConnection.SetConnection(
                    applicationName: $"{Connection.ApplicationName} WorkerThread: {Id}",
                    host: Connection.Host,
                    port: Connection.Port,
                    database: Connection.Database,
                    userName: Connection.UserName,
                    password: Connection.Password,
                    commandTimeout: Connection.CommandTimeout,
                    keepAlive: Connection.KeepAlive,
                    tcpKeepAlive: Connection.TcpKeepAlive,
                    tcpKeepAliveTime: Connection.TcpKeepAliveTime);

                return threadConnection.Initialized;
            }

            /// <summary>
            /// <para lang="cs">Ukončení činnosti pracovního vlákna</para>
            /// <para lang="en">Worker thread termination</para>
            /// </summary>
            private void Terminate()
            {
                if (threadConnection != null)
                {
                    threadConnection.CloseConnection();
                    threadConnection.Connection.Dispose();
                }
            }

            /// <summary>
            /// <para lang="cs">Operace vykonávané pracovním vláknem</para>
            /// <para lang="en">Operations performed by the worker thread</para>
            /// </summary>
            /// <param name="obj">
            /// <para lang="cs">Objekt s daty, která se předávají dovnitř vlákna</para>
            /// <para lang="en">Object with data that is passed inside the thread</para>
            /// </param>
            private void BackgroundTask(object obj)
            {
                IsWorking = true;
                OnWorkerStart?.Invoke(
                    sender: this,
                    e: new ThreadEventArgs()
                    {
                        Id = Id,
                        Task = null,
                        TasksTotal = null,
                        TasksCompleted = null,
                        ElapsedTime = null,
                        ExceptionEventArgs = null,
                        Notice = null,
                        Tag = null
                    });

                if (Connect())
                {
                    do
                    {
                        // Obsluha události OnTaskStart
                        // musí přiřadit TaskId (číslo úkolu pro zpracování)
                        // nebo null pokud jsou všechny úkoly hotové
                        OnTaskStart?.Invoke(
                            sender: this,
                            e: new ThreadEventArgs()
                            {
                                Id = Id,
                                Task = Task,
                                TasksTotal = null,
                                TasksCompleted = null,
                                ElapsedTime = null,
                                ExceptionEventArgs = null,
                                Notice = null,
                                Tag = null
                            });

                        object result = null;

                        ThreadEventArgs e = new()
                        {
                            Id = Id,
                            Task = Task,
                            TasksTotal = null,
                            TasksCompleted = null,
                            ElapsedTime = null,
                            ExceptionEventArgs = null,
                            Notice = null,
                            Tag = null
                        };

                        // Spuštění uložené procedury
                        if (Task != null)
                        {
                            result = Execute(e: ref e);
                            e.Notice = connection.Notice;
                        }

                        // Odeslání výsledku řídícímu vláknu
                        OnTaskComplete?.Invoke(sender: this, e: e);
                    }
                    while ((Task != null) && (!stopRequest));
                }

                Terminate();

                OnWorkerComplete?.Invoke(
                    sender: this,
                    e: new ThreadEventArgs()
                    {
                        Id = Id,
                        Task = null,
                        TasksTotal = null,
                        TasksCompleted = null,
                        ElapsedTime = null,
                        ExceptionEventArgs = null,
                        Notice = null,
                        Tag = null
                    });
                IsWorking = false;
            }

            /// <summary>
            /// <para lang="cs">
            /// Metoda určená přepsání v odvozené třídě.
            /// V odvozené třídě by měla obsahovat volání
            /// specifické uložené procedury z databáze.
            /// </para>
            /// <para lang="en">
            ///  Method for overriding in the derived class.
            ///  The derived class should contain a call
            ///  a specific stored procedure from the database.
            /// </para>
            /// </summary>
            public virtual object Execute(ref ThreadEventArgs e)
            {
                object result = null;

                // zde vložit volání nějaké uložené procedury
                Thread.Sleep(millisecondsTimeout: 1000);

                return result;
            }

            #endregion Methods

        }

    }
}