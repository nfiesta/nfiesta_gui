﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi
{
    namespace NfiEstaPg
    {

        /// <summary>
        /// Interface for lookup table with areal or population domain
        /// </summary>
        public interface IArealOrPopulationDomainTable
            : PostgreSQL.DataModel.ILookupTable
        {
        }


        /// <summary>
        /// Interface for row in lookup table with areal or population domain
        /// </summary>
        public interface IArealOrPopulationDomain
            : PostgreSQL.DataModel.ILookupTableEntry
        {

            #region Properties

            /// <summary>
            /// Classification type identifier
            /// </summary>
            int ClassificationTypeId { get; set; }

            /// <summary>
            /// Classification type value (read-only)
            /// </summary>
            TargetData.TDClassificationTypeEnum ClassificationTypeValue { get; }

            /// <summary>
            /// Classification type (read-only)
            /// </summary>
            TargetData.TDClassificationType ClassificationType { get; }

            #endregion Properties

        }


        /// <summary>
        /// Generic interface for row in lookup table with areal or population domain
        /// </summary>
        public interface IArealOrPopulationDomain<T>
            : IArealOrPopulationDomain,
            PostgreSQL.DataModel.ILookupTableEntry<T>
             where T : IArealOrPopulationDomainTable
        {
        }

    }
}
