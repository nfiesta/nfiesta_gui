﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi
{
    namespace NfiEstaPg
    {

        /// <summary>
        /// Interface for target variable table
        /// </summary>
        public interface ITargetVariableList
            : PostgreSQL.DataModel.IDatabaseTable
        {

            #region Properties

            /// <summary>
            /// List of target variables (read-only)
            /// </summary>
            System.Collections.Generic.List<ITargetVariable> Items { get; }

            #endregion Properties


            #region Indexers

            /// <summary>
            /// Target variable from list by identifier (read-only)
            /// </summary>
            /// <param name="id">Target variable identifier</param>
            /// <returns>Target variable from list by identifier (null if not found)</returns>
            ITargetVariable this[int id] { get; }

            #endregion Indexers


            #region Methods

            /// <summary>
            /// Create new empty target variable table
            /// </summary>
            /// <returns>New empty target variable table</returns>
            ITargetVariableList Empty();

            #endregion Methods

        }


        /// <summary>
        /// Interface for row in target variable table
        /// </summary>
        public interface ITargetVariable
            : PostgreSQL.DataModel.IDatabaseTableEntry
        {

            #region Derived Properties

            /// <summary>
            /// Identifier
            /// </summary>
            int Id { get; set; }

            /// <summary>
            /// Metadata of the target variable (json format as text)
            /// </summary>
            string MetadataText { get; set; }

            /// <summary>
            /// Metadata of the target variable (json format as object)
            /// </summary>
            Core.TargetVariableMetadataJson MetadataObject { get; }

            #endregion Derived Properties

        }


        /// <summary>
        /// Generic interface for row in target variable table
        /// </summary>
        public interface ITargetVariable<T>
            : ITargetVariable,
            PostgreSQL.DataModel.IDatabaseTableEntry<T>
             where T : ITargetVariableList
        {
        }

    }
}
