﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {

            /// <summary>
            /// Stored procedures of schema nfiesta
            /// </summary>
            public static partial class NfiEstaFunctions
            {
                // fn_api_update_panel_refyearset_group

                #region FnApiUpdatePanelRefYearSetGroup

                /// <summary>
                /// Wrapper for stored procedure fn_api_update_panel_refyearset_group.
                /// The function updates label, description, label_en, description_en (passed as arguments 2-5)
                /// in lookup table nfiesta.c_panel_refyearset_group, where id = _id passed as an argument 1.
                /// </summary>
                public static class FnApiUpdatePanelRefYearSetGroup
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName = NfiEstaSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name = "fn_api_update_panel_refyearset_group";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_id integer, ",
                            $"_label character varying, ",
                            $"_description text, ",
                            $"_label_en character varying, ",
                            $"_description_en text; ",
                            $"returns: void");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    ?.Replace("$SchemaName", schemaName)
                                    ?.Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    {SchemaName}.{Name}",
                            $"(@panelRefYearSetGroupId, @panelRefYearSetGroupLabelCs, @panelRefYearSetGroupDescriptionCs, @panelRefYearSetGroupLabelEn, @panelRefYearSetGroupDescriptionEn);{Environment.NewLine}");
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="panelRefYearSetGroupId">Aggregated sets of panels and corresponding reference year sets identifier</param>
                    /// <param name="panelRefYearSetGroupLabelCs">Label of aggregated sets of panels and corresponding reference year sets in national language</param>
                    /// <param name="panelRefYearSetGroupDescriptionCs">Description of aggregated sets of panels and corresponding reference year sets in national language</param>
                    /// <param name="panelRefYearSetGroupLabelEn">Label of aggregated sets of panels and corresponding reference year sets in English</param>
                    /// <param name="panelRefYearSetGroupDescriptionEn">Description of aggregated sets of panels and corresponding reference year sets in English</param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        Nullable<int> panelRefYearSetGroupId,
                        string panelRefYearSetGroupLabelCs,
                        string panelRefYearSetGroupDescriptionCs,
                        string panelRefYearSetGroupLabelEn,
                        string panelRefYearSetGroupDescriptionEn)
                    {
                        string result = SQL;

                        result = result.Replace(
                             oldValue: "@panelRefYearSetGroupId",
                             newValue: Functions.PrepNIntArg(arg: panelRefYearSetGroupId));

                        result = result.Replace(
                             oldValue: "@panelRefYearSetGroupLabelCs",
                             newValue: Functions.PrepStringArg(arg: panelRefYearSetGroupLabelCs));

                        result = result.Replace(
                             oldValue: "@panelRefYearSetGroupDescriptionCs",
                             newValue: Functions.PrepStringArg(arg: panelRefYearSetGroupDescriptionCs));

                        result = result.Replace(
                            oldValue: "@panelRefYearSetGroupLabelEn",
                            newValue: Functions.PrepStringArg(arg: panelRefYearSetGroupLabelEn));

                        result = result.Replace(
                            oldValue: "@panelRefYearSetGroupDescriptionEn",
                            newValue: Functions.PrepStringArg(arg: panelRefYearSetGroupDescriptionEn));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_api_update_panel_refyearset_group
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="panelRefYearSetGroupId">Aggregated sets of panels and corresponding reference year sets identifier</param>
                    /// <param name="panelRefYearSetGroupLabelCs">Label of aggregated sets of panels and corresponding reference year sets in national language</param>
                    /// <param name="panelRefYearSetGroupDescriptionCs">Description of aggregated sets of panels and corresponding reference year sets in national language</param>
                    /// <param name="panelRefYearSetGroupLabelEn">Label of aggregated sets of panels and corresponding reference year sets in English</param>
                    /// <param name="panelRefYearSetGroupDescriptionEn">Description of aggregated sets of panels and corresponding reference year sets in English</param>
                    /// <param name="transaction">Transaction</param>
                    public static void Execute(
                        NfiEstaDB database,
                        Nullable<int> panelRefYearSetGroupId,
                        string panelRefYearSetGroupLabelCs,
                        string panelRefYearSetGroupDescriptionCs,
                        string panelRefYearSetGroupLabelEn,
                        string panelRefYearSetGroupDescriptionEn,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(
                            panelRefYearSetGroupId: panelRefYearSetGroupId,
                            panelRefYearSetGroupLabelCs: panelRefYearSetGroupLabelCs,
                            panelRefYearSetGroupDescriptionCs: panelRefYearSetGroupDescriptionCs,
                            panelRefYearSetGroupLabelEn: panelRefYearSetGroupLabelEn,
                            panelRefYearSetGroupDescriptionEn: panelRefYearSetGroupDescriptionEn);

                        NpgsqlParameter pPanelRefYearSetGroupId = new(
                            parameterName: "panelRefYearSetGroupId",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pPanelRefYearSetGroupLabelCs = new(
                            parameterName: "panelRefYearSetGroupLabelCs",
                            parameterType: NpgsqlDbType.Varchar);

                        NpgsqlParameter pPanelRefYearSetGroupDescriptionCs = new(
                           parameterName: "panelRefYearSetGroupDescriptionCs",
                           parameterType: NpgsqlDbType.Text);

                        NpgsqlParameter pPanelRefYearSetGroupLabelEn = new(
                            parameterName: "panelRefYearSetGroupLabelEn",
                            parameterType: NpgsqlDbType.Varchar);

                        NpgsqlParameter pPanelRefYearSetGroupDescriptionEn = new(
                            parameterName: "panelRefYearSetGroupDescriptionEn",
                            parameterType: NpgsqlDbType.Text);

                        if (panelRefYearSetGroupId != null)
                        {
                            pPanelRefYearSetGroupId.Value = (int)panelRefYearSetGroupId;
                        }
                        else
                        {
                            pPanelRefYearSetGroupId.Value = DBNull.Value;
                        }

                        if (panelRefYearSetGroupLabelCs != null)
                        {
                            pPanelRefYearSetGroupLabelCs.Value = (string)panelRefYearSetGroupLabelCs;
                        }
                        else
                        {
                            pPanelRefYearSetGroupLabelCs.Value = DBNull.Value;
                        }

                        if (panelRefYearSetGroupDescriptionCs != null)
                        {
                            pPanelRefYearSetGroupDescriptionCs.Value = (string)panelRefYearSetGroupDescriptionCs;
                        }
                        else
                        {
                            pPanelRefYearSetGroupDescriptionCs.Value = DBNull.Value;
                        }

                        if (panelRefYearSetGroupLabelEn != null)
                        {
                            pPanelRefYearSetGroupLabelEn.Value = (string)panelRefYearSetGroupLabelEn;
                        }
                        else
                        {
                            pPanelRefYearSetGroupLabelEn.Value = DBNull.Value;
                        }

                        if (panelRefYearSetGroupDescriptionEn != null)
                        {
                            pPanelRefYearSetGroupDescriptionEn.Value = (string)panelRefYearSetGroupDescriptionEn;
                        }
                        else
                        {
                            pPanelRefYearSetGroupDescriptionEn.Value = DBNull.Value;
                        }

                        database.Postgres.ExecuteNonQuery(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pPanelRefYearSetGroupId,
                            pPanelRefYearSetGroupLabelCs, pPanelRefYearSetGroupDescriptionCs,
                            pPanelRefYearSetGroupLabelEn, pPanelRefYearSetGroupDescriptionEn);
                    }

                    /// <summary>
                    /// Execute stored procedure fn_api_update_panel_refyearset_group
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="panelRefYearSetGroupId">Aggregated sets of panels and corresponding reference year sets identifier</param>
                    /// <param name="panelRefYearSetGroupLabelCs">Label of aggregated sets of panels and corresponding reference year sets in national language</param>
                    /// <param name="panelRefYearSetGroupDescriptionCs">Description of aggregated sets of panels and corresponding reference year sets in national language</param>
                    /// <param name="panelRefYearSetGroupLabelEn">Label of aggregated sets of panels and corresponding reference year sets in English</param>
                    /// <param name="panelRefYearSetGroupDescriptionEn">Description of aggregated sets of panels and corresponding reference year sets in English</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Empty data table</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> panelRefYearSetGroupId,
                        string panelRefYearSetGroupLabelCs,
                        string panelRefYearSetGroupDescriptionCs,
                        string panelRefYearSetGroupLabelEn,
                        string panelRefYearSetGroupDescriptionEn,
                        NpgsqlTransaction transaction = null)
                    {
                        Execute(
                            database: database,
                            panelRefYearSetGroupId: panelRefYearSetGroupId,
                            panelRefYearSetGroupLabelCs: panelRefYearSetGroupLabelCs,
                            panelRefYearSetGroupDescriptionCs: panelRefYearSetGroupDescriptionCs,
                            panelRefYearSetGroupLabelEn: panelRefYearSetGroupLabelEn,
                            panelRefYearSetGroupDescriptionEn: panelRefYearSetGroupDescriptionEn,
                            transaction: transaction);

                        return new DataTable();
                    }

                    #endregion Methods

                }

                #endregion FnApiUpdatePanelRefYearSetGroup

            }

        }
    }
}