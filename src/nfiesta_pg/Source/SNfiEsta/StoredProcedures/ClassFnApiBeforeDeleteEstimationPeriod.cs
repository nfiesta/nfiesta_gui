﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {

            /// <summary>
            /// Stored procedures of schema nfiesta
            /// </summary>
            public static partial class NfiEstaFunctions
            {
                // fn_api_before_delete_estimation_period

                #region FnApiBeforeDeleteEstimationPeriod

                /// <summary>
                /// Wrapper for stored procedure fn_api_before_delete_estimation_period.
                /// The function checks if there is a row in the nfiesta.t_total_estimate_conf table,
                /// where estimation_period = _id passed as an argument. If the function returns TRUE,
                /// the row with id = _id in the nfiesta.c_estimation_period table cannot be deleted.
                /// </summary>
                public static class FnApiBeforeDeleteEstimationPeriod
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = NfiEstaSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_api_before_delete_estimation_period";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_id integer, ",
                            $"OUT _t_total_estimate_conf_exi boolean; ",
                            $"returns: boolean");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                String.Concat(
                                    $"SELECT{Environment.NewLine}",
                                    $"    {TFnApiBeforeDeleteEstimationPeriodList.ColId.SQL(cols: TFnApiBeforeDeleteEstimationPeriodList.Cols, alias: Name)}",
                                    $"    {TFnApiBeforeDeleteEstimationPeriodList.ColTotalEstimateConfExist.SQL(cols: TFnApiBeforeDeleteEstimationPeriodList.Cols, alias: Name, isLastOne: true)}",
                                    $"FROM{Environment.NewLine}",
                                    $"    {SchemaName}.{Name}(@estimationPeriodId);{Environment.NewLine}");
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="estimationPeriodId">Estimation period identifier</param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(Nullable<int> estimationPeriodId)
                    {
                        string result = SQL;

                        result = result.Replace(
                           oldValue: "@estimationPeriodId",
                           newValue: Functions.PrepNIntArg(arg: estimationPeriodId));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_api_before_estimation_period
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="estimationPeriodId">Estimation period identifier</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table of checks if there is a row in the nfiesta.t_total_estimate_conf table</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> estimationPeriodId,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(estimationPeriodId: estimationPeriodId);

                        NpgsqlParameter pEstimationPeriodId = new(
                            parameterName: "estimationPeriodId",
                            parameterType: NpgsqlDbType.Integer);

                        if (estimationPeriodId != null)
                        {
                            pEstimationPeriodId.Value = (int)estimationPeriodId;
                        }
                        else
                        {
                            pEstimationPeriodId.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteQuery(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pEstimationPeriodId);
                    }

                    /// <summary>
                    /// Execute stored procedure fn_api_before_delete_estimation_period
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="estimationPeriodId">Estimation period identifier</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>List of checks if there is a row in the nfiesta.t_total_estimate_conf table</returns>
                    public static TFnApiBeforeDeleteEstimationPeriodList Execute(
                        NfiEstaDB database,
                        Nullable<int> estimationPeriodId,
                        NpgsqlTransaction transaction = null)
                    {
                        System.Diagnostics.Stopwatch stopWatch = new();
                        stopWatch.Start();

                        DataTable data = ExecuteQuery(
                            database: database,
                            estimationPeriodId: estimationPeriodId,
                            transaction: transaction);
                        data.TableName = Name;

                        stopWatch.Stop();

                        return new TFnApiBeforeDeleteEstimationPeriodList(
                            database: database,
                            data: data,
                            loadingTime: 0.001 * stopWatch.ElapsedMilliseconds,
                            storedProcedure: CommandText)
                        {
                            EstimationPeriodId = estimationPeriodId
                        };
                    }
                    #endregion Methods

                }

                #endregion FnApiBeforeDeleteEstimationPeriod

            }

        }
    }
}
