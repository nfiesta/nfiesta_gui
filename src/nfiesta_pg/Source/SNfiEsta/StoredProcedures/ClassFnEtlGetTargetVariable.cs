﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {

            /// <summary>
            /// Stored procedures of schema nfiesta
            /// </summary>
            public static partial class NfiEstaFunctions
            {
                // fn_etl_get_target_variable

                #region FnEtlGetTargetVariable

                /// <summary>
                /// Wrapper for stored procedure fn_etl_get_target_variable.
                /// Function returns records from table c_target_variable.
                /// </summary>
                public static class FnEtlGetTargetVariable
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = NfiEstaSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_etl_get_target_variable";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_national_language character varying DEFAULT 'en'::character varying, ",
                            $"_etl_id integer[] DEFAULT NULL::integer[]; ",
                            $"returns: ",
                            $"TABLE(",
                            $"id integer, ",
                            $"metadata json)");
                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql =
                         String.Concat(
                                 $"SELECT{Environment.NewLine}",
                                 $"    $Name.id::int4           AS id,{Environment.NewLine}",
                                 $"    $Name.metadata::text     AS metadata{Environment.NewLine}",
                                 $"FROM{Environment.NewLine}",
                                 $"    $SchemaName.$Name(@nationalLanguage, @etlId) AS $Name{Environment.NewLine}",
                                 $"ORDER BY{Environment.NewLine}",
                                 $"    $Name.id;{Environment.NewLine}"
                                 );

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="nationalLanguage"></param>
                    /// <param name="etlId"></param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        Language nationalLanguage = PostgreSQL.Language.CS,
                        List<Nullable<int>> etlId = null)
                    {
                        string result = SQL;

                        result = result.Replace(
                            oldValue: "@nationalLanguage",
                            newValue: Functions.PrepStringArg(
                                arg: LanguageList.ISO_639_1(language: nationalLanguage)));

                        result = result.Replace(
                            oldValue: "@etlId",
                            newValue: Functions.PrepNIntArrayArg(args: etlId, dbType: "int4"));

                        return result;
                    }

                    /// <summary>
                    ///  Execute stored procedure fn_etl_get_target_variable
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="nationalLanguage"></param>
                    /// <param name="etlId"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Language nationalLanguage = PostgreSQL.Language.CS,
                        List<Nullable<int>> etlId = null,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(nationalLanguage: nationalLanguage, etlId: etlId);

                        NpgsqlParameter pNationalLanguage = new(
                            parameterName: "nationalLanguage",
                            parameterType: NpgsqlDbType.Varchar);

                        NpgsqlParameter pEtlId = new(
                            parameterName: "etlId",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        pNationalLanguage.Value = LanguageList.ISO_639_1(language: nationalLanguage);

                        if (etlId != null)
                        {
                            pEtlId.Value = etlId.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pEtlId.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteQuery(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pNationalLanguage, pEtlId);
                    }

                    /// <summary>
                    /// Execute stored procedure fn_etl_get_target_variable
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="nationalLanguage"></param>
                    /// <param name="etlId"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>List of target variables for ETL in the NfiEsta schema</returns>
                    public static FnEtlGetTargetVariableTypeList Execute(
                        NfiEstaDB database,
                        Language nationalLanguage = PostgreSQL.Language.CS,
                        List<Nullable<int>> etlId = null,
                        NpgsqlTransaction transaction = null)
                    {
                        System.Diagnostics.Stopwatch stopWatch = new();
                        stopWatch.Start();

                        DataTable data = ExecuteQuery(
                            database: database,
                            nationalLanguage: nationalLanguage,
                            etlId: etlId,
                            transaction: transaction);
                        data.TableName = Name;

                        stopWatch.Stop();

                        return new FnEtlGetTargetVariableTypeList(
                            database: database,
                            data: data,
                            loadingTime: 0.001 * stopWatch.ElapsedMilliseconds,
                            storedProcedure: CommandText)
                        {
                            NationalLanguage = nationalLanguage,
                            EtlId = etlId
                        };
                    }

                    #endregion Methods

                }

                #endregion FnEtlGetTargetVariable

            }

        }
    }
}