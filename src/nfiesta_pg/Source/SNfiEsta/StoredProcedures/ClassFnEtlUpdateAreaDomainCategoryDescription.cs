﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {

            /// <summary>
            /// Stored procedures of schema nfiesta
            /// </summary>
            public static partial class NfiEstaFunctions
            {
                // fn_etl_update_area_domain_category_description

                #region FnEtlUpdateAreaDomainCategoryDescription

                /// <summary>
                /// Wrapper for stored procedure fn_etl_update_area_domain_category_description.
                /// The function updates an area domain category description
                /// in c_area_domain_category table for given area domain category.
                /// </summary>
                public static class FnEtlUpdateAreaDomainCategoryDescription
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = NfiEstaSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_etl_update_area_domain_category_description";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_area_domain_category integer, ",
                            $"_national_language character varying, ",
                            $"_description text; ",
                            $"returns: text");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    $SchemaName.$Name(",
                            $"@areaDomainCategory, ",
                            $"@nationalLanguage, ",
                            $"@description)::text AS $Name;");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="areaDomainCategory"></param>
                    /// <param name="nationalLanguage"></param>
                    /// <param name="description"></param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        Nullable<int> areaDomainCategory,
                        string nationalLanguage,
                        string description)
                    {
                        string result = SQL;

                        result = result.Replace(
                            oldValue: "@areaDomainCategory",
                            newValue: Functions.PrepNIntArg(arg: areaDomainCategory));

                        result = result.Replace(
                            oldValue: "@nationalLanguage",
                            newValue: Functions.PrepStringArg(arg: nationalLanguage));

                        result = result.Replace(
                            oldValue: "@description",
                            newValue: Functions.PrepStringArg(arg: description));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_etl_update_area_domain_category_description
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="areaDomainCategory"></param>
                    /// <param name="nationalLanguage"></param>
                    /// <param name="description"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> areaDomainCategory,
                        string nationalLanguage,
                        string description,
                        NpgsqlTransaction transaction = null)
                    {
                        string val = Execute(
                            database: database,
                            areaDomainCategory: areaDomainCategory,
                            nationalLanguage: nationalLanguage,
                            description: description,
                            transaction: transaction);

                        DataTable dt = new();

                        dt.Columns.Add(column: new DataColumn()
                        {
                            ColumnName = Name,
                            DataType = Type.GetType(typeName: "System.String")
                        });

                        DataRow row = dt.NewRow();

                        Functions.SetStringArg(
                            row: row,
                            name: Name,
                            val: val);

                        dt.Rows.Add(row: row);

                        return dt;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_etl_update_area_domain_category_description
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="areaDomainCategory"></param>
                    /// <param name="nationalLanguage"></param>
                    /// <param name="description"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Text</returns>
                    public static string Execute(
                        NfiEstaDB database,
                        Nullable<int> areaDomainCategory,
                        string nationalLanguage,
                        string description,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(
                            areaDomainCategory: areaDomainCategory,
                            nationalLanguage: nationalLanguage,
                            description: description);

                        NpgsqlParameter pAreaDomainCategory = new(
                            parameterName: "areaDomainCategory",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pNationalLanguage = new(
                            parameterName: "nationalLanguage",
                            parameterType: NpgsqlDbType.Varchar);

                        NpgsqlParameter pDescription = new(
                            parameterName: "description",
                            parameterType: NpgsqlDbType.Text);

                        if (areaDomainCategory != null)
                        {
                            pAreaDomainCategory.Value = (int)areaDomainCategory;
                        }
                        else
                        {
                            pAreaDomainCategory.Value = DBNull.Value;
                        }

                        if (nationalLanguage != null)
                        {
                            pNationalLanguage.Value = (string)nationalLanguage;
                        }
                        else
                        {
                            pNationalLanguage.Value = DBNull.Value;
                        }

                        if (description != null)
                        {
                            pDescription.Value = (string)description;
                        }
                        else
                        {
                            pDescription.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteScalar(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pAreaDomainCategory, pNationalLanguage, pDescription);
                    }

                    #endregion Methods

                }

                #endregion FnEtlUpdateAreaDomainCategoryDescription

            }

        }
    }
}