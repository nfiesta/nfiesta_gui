﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {

            /// <summary>
            /// Stored procedures of schema nfiesta
            /// </summary>
            public static partial class NfiEstaFunctions
            {
                // fn_get_area_domains4target_variable

                #region FnGetAreaDomainsForTargetVariable

                /// <summary>
                /// Wrapper for stored procedure fn_get_area_domains4target_variable.
                /// Function returns table with area domains constrained by the availability
                /// for a given target variable and
                /// optionally by a given area domain of numerator if the target variable comes
                /// in the meaning of the denominator.
                /// </summary>
                public static class FnGetAreaDomainsForTargetVariable
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = NfiEstaSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_get_area_domains4target_variable";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_target_variable integer, ",
                            $"_numerator_area_domain integer DEFAULT NULL::integer; ",
                            $"returns: ",
                            $"TABLE(",
                            $"id integer, ",
                            $"label character varying, ",
                            $"description text, ",
                            $"label_en character varying, ",
                            $"description_en text)");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    {AreaDomainList.ColId.SQL(AreaDomainList.Cols, Name)}",
                            $"    {AreaDomainList.ColLabelCs.SQL(AreaDomainList.Cols, Name)}",
                            $"    {AreaDomainList.ColDescriptionCs.SQL(AreaDomainList.Cols, Name)}",
                            $"    {AreaDomainList.ColLabelEn.SQL(AreaDomainList.Cols, Name)}",
                            $"    {AreaDomainList.ColDescriptionEn.SQL(AreaDomainList.Cols, Name, isLastOne: true)}",
                            $"FROM{Environment.NewLine}",
                            $"    $SchemaName.$Name(",
                            $"@targetVariableId, ",
                            $"@numeratorAreaDomainId) AS $Name{Environment.NewLine}",
                            $"ORDER BY {AreaDomainList.ColId.DbName};");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="targetVariableId">Target variable identifier</param>
                    /// <param name="numeratorAreaDomainId">Area domain identifier</param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        Nullable<int> targetVariableId,
                        Nullable<int> numeratorAreaDomainId = null)
                    {
                        string result = SQL;

                        result = result.Replace(
                            oldValue: "@targetVariableId",
                            newValue: Functions.PrepNIntArg(arg: targetVariableId));

                        result = result.Replace(
                            oldValue: "@numeratorAreaDomainId",
                            newValue: Functions.PrepNIntArg(arg: numeratorAreaDomainId));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_get_area_domains4target_variable
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="targetVariableId">Target variable identifier</param>
                    /// <param name="numeratorAreaDomainId">Area domain identifier</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table with records from data table of area domains
                    /// (identifier, label, description, label_en and description_en)</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> targetVariableId,
                        Nullable<int> numeratorAreaDomainId = null,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(
                            targetVariableId: targetVariableId,
                            numeratorAreaDomainId: numeratorAreaDomainId);

                        NpgsqlParameter pTargetVariableId = new(
                            parameterName: "targetVariableId",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pNumeratorAreaDomainId = new(
                            parameterName: "numeratorAreaDomainId",
                            parameterType: NpgsqlDbType.Integer);

                        if (targetVariableId != null)
                        {
                            pTargetVariableId.Value = (int)targetVariableId;
                        }
                        else
                        {
                            pTargetVariableId.Value = DBNull.Value;
                        }

                        if (numeratorAreaDomainId != null)
                        {
                            pNumeratorAreaDomainId.Value = (int)numeratorAreaDomainId;
                        }
                        else
                        {
                            pNumeratorAreaDomainId.Value = DBNull.Value;
                        }

                        return
                            database.Postgres.ExecuteQuery(
                                sqlCommand: SQL,
                                transaction: transaction,
                                pTargetVariableId, pNumeratorAreaDomainId);
                    }

                    /// <summary>
                    /// Execute stored procedure fn_get_area_domains4target_variable
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="targetVariableId">Target variable identifier</param>
                    /// <param name="numeratorAreaDomainId">Area domain identifier</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>List of area domains</returns>
                    public static AreaDomainList Execute(
                        NfiEstaDB database,
                        Nullable<int> targetVariableId,
                        Nullable<int> numeratorAreaDomainId = null,
                        NpgsqlTransaction transaction = null)
                    {
                        return new AreaDomainList(
                            database: database,
                            data: ExecuteQuery(
                                database: database,
                                targetVariableId: targetVariableId,
                                numeratorAreaDomainId: numeratorAreaDomainId,
                                transaction: transaction));
                    }

                    #endregion Methods

                }

                #endregion FnGetAreaDomainsForTargetVariable

            }
        }
    }
}