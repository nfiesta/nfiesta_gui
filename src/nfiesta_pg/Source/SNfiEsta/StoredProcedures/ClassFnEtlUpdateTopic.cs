﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {

            /// <summary>
            /// Stored procedures of schema nfiesta
            /// </summary>
            public static partial class NfiEstaFunctions
            {
                // fn_etl_update_topic

                #region FnEtlUpdateTopic

                /// <summary>
                /// Wrapper for stored procedure fn_etl_update_topic.
                /// The function updates a record in c_topic table.
                /// </summary>
                public static class FnEtlUpdateTopic
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = NfiEstaSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_etl_update_topic";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_id integer, ",
                            $"_label character varying, ",
                            $"_description text, ",
                            $"_label_en character varying, ",
                            $"_description_en text; ",
                            $"returns: integer");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    $SchemaName.$Name(",
                            $"@id, ",
                            $"@label, ",
                            $"@description, ",
                            $"@labelEn, ",
                            $"@descriptionEn)::integer AS $Name;");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="id">Identifier</param>
                    /// <param name="label">Label</param>
                    /// <param name="description">Description</param>
                    /// <param name="labelEn">Label in English</param>
                    /// <param name="descriptionEn">Description in English</param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        Nullable<int> id,
                        string label,
                        string description,
                        string labelEn,
                        string descriptionEn)
                    {
                        string result = SQL;

                        result = result.Replace(
                            oldValue: "@id",
                            newValue: Functions.PrepNIntArg(arg: id));

                        result = result.Replace(
                            oldValue: "@label",
                            newValue: Functions.PrepStringArg(arg: label));

                        result = result.Replace(
                            oldValue: "@description",
                            newValue: Functions.PrepStringArg(arg: description));

                        result = result.Replace(
                            oldValue: "@labelEn",
                            newValue: Functions.PrepStringArg(arg: labelEn));

                        result = result.Replace(
                            oldValue: "@descriptionEn",
                            newValue: Functions.PrepStringArg(arg: descriptionEn));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_etl_update_topic
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="id">Identifier</param>
                    /// <param name="label">Label</param>
                    /// <param name="description">Description</param>
                    /// <param name="labelEn">Label in English</param>
                    /// <param name="descriptionEn">Description in English</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> id,
                        string label,
                        string description,
                        string labelEn,
                        string descriptionEn,
                        NpgsqlTransaction transaction = null)
                    {
                        Nullable<int> val = Execute(
                            database: database,
                            id: id,
                            label: label,
                            description: description,
                            labelEn: labelEn,
                            descriptionEn: descriptionEn,
                            transaction: transaction);

                        DataTable dt = new();

                        dt.Columns.Add(column: new DataColumn()
                        {
                            ColumnName = Name,
                            DataType = Type.GetType(typeName: "System.Int32")
                        });

                        DataRow row = dt.NewRow();

                        Functions.SetNIntArg(
                            row: row,
                            name: Name,
                            val: val);

                        dt.Rows.Add(row: row);

                        return dt;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_etl_update_topic
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="id">Identifier</param>
                    /// <param name="label">Label</param>
                    /// <param name="description">Description</param>
                    /// <param name="labelEn">Label in English</param>
                    /// <param name="descriptionEn">Description in English</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Integer</returns>
                    public static Nullable<int> Execute(
                        NfiEstaDB database,
                        Nullable<int> id,
                        string label,
                        string description,
                        string labelEn,
                        string descriptionEn,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(
                            id: id,
                            label: label,
                            description: description,
                            labelEn: labelEn,
                            descriptionEn: descriptionEn);

                        NpgsqlParameter pId = new(
                            parameterName: "id",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pLabel = new(
                            parameterName: "label",
                            parameterType: NpgsqlDbType.Varchar);

                        NpgsqlParameter pDescription = new(
                            parameterName: "description",
                            parameterType: NpgsqlDbType.Text);

                        NpgsqlParameter pLabelEn = new(
                            parameterName: "labelEn",
                            parameterType: NpgsqlDbType.Varchar);

                        NpgsqlParameter pDescriptionEn = new(
                            parameterName: "descriptionEn",
                            parameterType: NpgsqlDbType.Text);

                        if (id != null)
                        {
                            pId.Value = (int)id;
                        }
                        else
                        {
                            pId.Value = DBNull.Value;
                        }

                        if (label != null)
                        {
                            pLabel.Value = (string)label;
                        }
                        else
                        {
                            pLabel.Value = DBNull.Value;
                        }

                        if (description != null)
                        {
                            pDescription.Value = (string)description;
                        }
                        else
                        {
                            pDescription.Value = DBNull.Value;
                        }

                        if (labelEn != null)
                        {
                            pLabelEn.Value = (string)labelEn;
                        }
                        else
                        {
                            pLabelEn.Value = DBNull.Value;
                        }

                        if (descriptionEn != null)
                        {
                            pDescriptionEn.Value = (string)descriptionEn;
                        }
                        else
                        {
                            pDescriptionEn.Value = DBNull.Value;
                        }

                        string strResult = database.Postgres.ExecuteScalar(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pId, pLabel, pDescription, pLabelEn, pDescriptionEn);

                        if (String.IsNullOrEmpty(value: strResult))
                        {
                            return null;
                        }
                        else
                        {
                            if (Int32.TryParse(s: strResult, result: out int intResult))
                            {
                                return intResult;
                            }
                            else
                            {
                                return null;
                            }
                        }
                    }

                    #endregion Methods

                }

                #endregion FnEtlUpdateTopic

            }

        }
    }
}