﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {

            /// <summary>
            /// Stored procedures of schema nfiesta
            /// </summary>
            public static partial class NfiEstaFunctions
            {
                // fn_etl_update_area_domain_category_label

                #region FnEtlUpdateAreaDomainCategoryLabel

                /// <summary>
                /// Wrapper for stored procedure fn_etl_update_area_domain_category_label.
                /// The function updates an area domain category label
                /// in c_area_domain_category table for given area domain category.
                /// </summary>
                public static class FnEtlUpdateAreaDomainCategoryLabel
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = NfiEstaSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_etl_update_area_domain_category_label";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_area_domain_category integer, ",
                            $"_national_language character varying, ",
                            $"_label character varying; ",
                            $"returns: text");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    $SchemaName.$Name(",
                            $"@areaDomainCategory, ",
                            $"@nationalLanguage, ",
                            $"@label)::text AS $Name;");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="areaDomainCategory"></param>
                    /// <param name="nationalLanguage"></param>
                    /// <param name="label"></param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        Nullable<int> areaDomainCategory,
                        string nationalLanguage,
                        string label)
                    {
                        string result = SQL;

                        result = result.Replace(
                            oldValue: "@areaDomainCategory",
                            newValue: Functions.PrepNIntArg(arg: areaDomainCategory));

                        result = result.Replace(
                            oldValue: "@nationalLanguage",
                            newValue: Functions.PrepStringArg(arg: nationalLanguage));

                        result = result.Replace(
                            oldValue: "@label",
                            newValue: Functions.PrepStringArg(arg: label));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_etl_update_area_domain_category_label
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="areaDomainCategory"></param>
                    /// <param name="nationalLanguage"></param>
                    /// <param name="label"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> areaDomainCategory,
                        string nationalLanguage,
                        string label,
                        NpgsqlTransaction transaction = null)
                    {
                        string val = Execute(
                            database: database,
                            areaDomainCategory: areaDomainCategory,
                            nationalLanguage: nationalLanguage,
                            label: label,
                            transaction: transaction);

                        DataTable dt = new();

                        dt.Columns.Add(column: new DataColumn()
                        {
                            ColumnName = Name,
                            DataType = Type.GetType(typeName: "System.String")
                        });

                        DataRow row = dt.NewRow();

                        Functions.SetStringArg(
                            row: row,
                            name: Name,
                            val: val);

                        dt.Rows.Add(row: row);

                        return dt;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_etl_update_area_domain_category_label
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="areaDomainCategory"></param>
                    /// <param name="nationalLanguage"></param>
                    /// <param name="label"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Text</returns>
                    public static string Execute(
                        NfiEstaDB database,
                        Nullable<int> areaDomainCategory,
                        string nationalLanguage,
                        string label,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(
                            areaDomainCategory: areaDomainCategory,
                            nationalLanguage: nationalLanguage,
                            label: label);

                        NpgsqlParameter pAreaDomainCategory = new(
                            parameterName: "areaDomainCategory",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pNationalLanguage = new(
                            parameterName: "nationalLanguage",
                            parameterType: NpgsqlDbType.Varchar);

                        NpgsqlParameter pLabel = new(
                            parameterName: "label",
                            parameterType: NpgsqlDbType.Varchar);

                        if (areaDomainCategory != null)
                        {
                            pAreaDomainCategory.Value = (int)areaDomainCategory;
                        }
                        else
                        {
                            pAreaDomainCategory.Value = DBNull.Value;
                        }

                        if (nationalLanguage != null)
                        {
                            pNationalLanguage.Value = (string)nationalLanguage;
                        }
                        else
                        {
                            pNationalLanguage.Value = DBNull.Value;
                        }

                        if (label != null)
                        {
                            pLabel.Value = (string)label;
                        }
                        else
                        {
                            pLabel.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteScalar(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pAreaDomainCategory, pNationalLanguage, pLabel);
                    }

                    #endregion Methods

                }

                #endregion FnEtlUpdateAreaDomainCategoryLabel

            }

        }
    }
}