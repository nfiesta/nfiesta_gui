﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {

            /// <summary>
            /// Stored procedures of schema nfiesta
            /// </summary>
            public static partial class NfiEstaFunctions
            {
                // fn_etl_get_topics4target_variable

                #region FnEtlGetTopicsForTargetVariable

                /// <summary>
                /// Wrapper for stored procedure fn_etl_get_topics4target_variable.
                /// The function returns all topics and information which topic is the given target variable linked to.
                /// </summary>
                public static class FnEtlGetTopicsForTargetVariable
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName = NfiEstaSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name = "fn_etl_get_topics4target_variable";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_target_variable integer, ",
                            $"_national_language character varying DEFAULT 'en'::character varying, ",
                            $"_linked boolean DEFAULT NULL::boolean; ",
                            $"returns: ",
                            $"TABLE(",
                            $"id integer, ",
                            $"label character varying, ",
                            $"description text, ",
                            $"label_en character varying, ",
                            $"description_en text, ",
                            $"linked boolean)");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    $Name.id              AS id,{Environment.NewLine}",
                            $"    $Name.label           AS label,{Environment.NewLine}",
                            $"    $Name.description     AS description,{Environment.NewLine}",
                            $"    $Name.label_en        AS label_en,{Environment.NewLine}",
                            $"    $Name.description_en  AS description_en,{Environment.NewLine}",
                            $"    $Name.linked          AS linked{Environment.NewLine}",
                            $"FROM{Environment.NewLine}",
                            $"    $SchemaName.$Name(@targetVariable, @nationalLanguage, @linked) AS $Name;{Environment.NewLine}");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="targetVariable"></param>
                    /// <param name="nationalLanguage">National language</param>
                    /// <param name="linked"></param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        Nullable<int> targetVariable,
                        string nationalLanguage,
                        Nullable<bool> linked)
                    {
                        string result = SQL;

                        result = result.Replace(
                           oldValue: "@targetVariable",
                           newValue: Functions.PrepNIntArg(arg: targetVariable));

                        result = result.Replace(
                            oldValue: "@nationalLanguage",
                            newValue: Functions.PrepStringArg(arg: nationalLanguage));

                        result = result.Replace(
                            oldValue: "@linked",
                            newValue: Functions.PrepNBoolArg(arg: linked));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_etl_get_topics4target_variable
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="targetVariable"></param>
                    /// <param name="nationalLanguage">National language</param>
                    /// <param name="linked"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> targetVariable,
                        string nationalLanguage,
                        Nullable<bool> linked,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(targetVariable: targetVariable, nationalLanguage: nationalLanguage, linked: linked);

                        NpgsqlParameter pTargetVariable = new(
                            parameterName: "targetVariable",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pNationalLanguage = new(
                            parameterName: "nationalLanguage",
                            parameterType: NpgsqlDbType.Varchar);

                        NpgsqlParameter pLinked = new(
                            parameterName: "linked",
                            parameterType: NpgsqlDbType.Boolean);

                        if (targetVariable != null)
                        {
                            pTargetVariable.Value = (int)targetVariable;
                        }
                        else
                        {
                            pTargetVariable.Value = DBNull.Value;
                        }

                        if (nationalLanguage != null)
                        {
                            pNationalLanguage.Value = (string)nationalLanguage;
                        }
                        else
                        {
                            pNationalLanguage.Value = DBNull.Value;
                        }

                        if (linked != null)
                        {
                            pLinked.Value = (bool)linked;
                        }
                        else
                        {
                            pLinked.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteQuery(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pTargetVariable, pNationalLanguage, pLinked);
                    }

                    #endregion Methods

                }

                #endregion FnEtlGetTopicsForTargetVariable

            }

        }
    }
}