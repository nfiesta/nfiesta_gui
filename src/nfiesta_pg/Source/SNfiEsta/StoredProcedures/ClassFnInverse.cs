﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {

            /// <summary>
            /// Stored procedures of schema nfiesta
            /// </summary>
            public static partial class NfiEstaFunctions
            {
                // fn_inverse

                #region FnInverse

                /// <summary>
                /// Wrapper for stored procedure fn_inverse.
                /// Function for computing inverse of matrix using python3.
                /// </summary>
                public static class FnInverse
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName = NfiEstaSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name = "fn_inverse";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: input double precision[]; ",
                            $"returns: double precision[]");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    array_to_string($SchemaName.$Name(",
                            $"@input), '{(char)59}', '{Functions.StrNull}') ",
                            $"AS {Name};{Environment.NewLine}");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="input"></param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(List<Nullable<double>> input)
                    {
                        string result = SQL;

                        result = result.Replace(
                            oldValue: "@input",
                            newValue: Functions.PrepNDoubleArrayArg(args: input, dbType: "float8"));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_inverse
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="input"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns></returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        List<Nullable<double>> input,
                        NpgsqlTransaction transaction = null)
                    {
                        List<Nullable<double>> list = Execute(
                            database: database,
                            input: input,
                            transaction: transaction);

                        DataTable dt = new();

                        dt.Columns.Add(column: new DataColumn()
                        {
                            ColumnName = Name,
                            DataType = Type.GetType(typeName: "System.Double")
                        });

                        if (list == null)
                        {
                            return dt;
                        }

                        foreach (Nullable<double> val in list)
                        {
                            DataRow row = dt.NewRow();

                            Functions.SetNDoubleArg(
                                row: row,
                                name: Name,
                                val: val);

                            dt.Rows.Add(row: row);
                        }

                        return dt;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_inverse
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="input"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns></returns>
                    public static List<Nullable<double>> Execute(
                        NfiEstaDB database,
                        List<Nullable<double>> input,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(input: input);

                        NpgsqlParameter pInput = new(
                            parameterName: "input",
                            parameterType: NpgsqlDbType.Double);

                        if (input != null)
                        {
                            pInput.Value = input.ToArray<Nullable<double>>();
                        }
                        else
                        {
                            pInput.Value = DBNull.Value;
                        }

                        return
                            Functions.StringToNDoubleList(
                                text: database.Postgres.ExecuteScalar(
                                    sqlCommand: SQL,
                                    transaction: transaction,
                                    pInput),
                                separator: (char)59);
                    }

                    #endregion Methods

                }

                #endregion FnInverse

            }

        }
    }
}