﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {

            /// <summary>
            /// Stored procedures of schema nfiesta
            /// </summary>
            public static partial class NfiEstaFunctions
            {
                // fn_etl_update_target_variable_metadata

                #region FnEtlUpdateTargetVariableMetadata

                /// <summary>
                /// Wrapper for stored procedure fn_etl_update_target_variable_metadata.
                /// Function update metadatas of target variable in table c_target_variable for given language elements.
                /// </summary>
                public static class FnEtlUpdateTargetVariableMetadata
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName = NfiEstaSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name = "fn_etl_update_target_variable_metadata";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_target_variable integer, ",
                            $"_metadata json, ",
                            $"_metadata_diff_national_language boolean, ",
                            $"_metadata_diff_english_language boolean, ",
                            $"_missing_national_language boolean, ",
                            $"_english_language_boolean boolean, ",
                            $"_national_language_boolean boolean, ",
                            $"_national_language character varying DEFAULT 'en'::character varying; ",
                            $"returns: text");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    $SchemaName.$Name(",
                            $"@targetVariable, ",
                            $"@metadata, ",
                            $"@metadataDiffNationalLanguage, ",
                            $"@metadataDiffEnglishLanguage, ",
                            $"@missingNationalLanguage, ",
                            $"@englishLanguageBoolean, ",
                            $"@nationalLanguageBoolean, ",
                            $"@nationalLanguage)::text ",
                            $"AS $Name;{Environment.NewLine}");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="targetVariable"></param>
                    /// <param name="metadata"></param>
                    /// <param name="metadataDiffNationalLanguage"></param>
                    /// <param name="metadataDiffEnglishLanguage"></param>
                    /// <param name="missingNationalLanguage"></param>
                    /// <param name="englishLanguageBoolean"></param>
                    /// <param name="nationalLanguageBoolean"></param>
                    /// <param name="nationalLanguage"></param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        Nullable<int> targetVariable,
                        string metadata,
                        Nullable<bool> metadataDiffNationalLanguage,
                        Nullable<bool> metadataDiffEnglishLanguage,
                        Nullable<bool> missingNationalLanguage,
                        Nullable<bool> englishLanguageBoolean,
                        Nullable<bool> nationalLanguageBoolean,
                        Language nationalLanguage = PostgreSQL.Language.CS)
                    {
                        string result = SQL;

                        result = result.Replace(
                            oldValue: "@targetVariable",
                            newValue: Functions.PrepNIntArg(arg: targetVariable));

                        result = result.Replace(
                            oldValue: "@metadata",
                            newValue: Functions.PrepStringArg(arg: metadata));

                        result = result.Replace(
                            oldValue: "@metadataDiffNationalLanguage",
                            newValue: Functions.PrepNBoolArg(arg: metadataDiffNationalLanguage));

                        result = result.Replace(
                            oldValue: "@metadataDiffEnglishLanguage",
                            newValue: Functions.PrepNBoolArg(arg: metadataDiffEnglishLanguage));

                        result = result.Replace(
                            oldValue: "@missingNationalLanguage",
                            newValue: Functions.PrepNBoolArg(arg: missingNationalLanguage));

                        result = result.Replace(
                            oldValue: "@englishLanguageBoolean",
                            newValue: Functions.PrepNBoolArg(arg: englishLanguageBoolean));

                        result = result.Replace(
                            oldValue: "@nationalLanguageBoolean",
                            newValue: Functions.PrepNBoolArg(arg: nationalLanguageBoolean));

                        result = result.Replace(
                            oldValue: "@nationalLanguage",
                            newValue: Functions.PrepStringArg(
                                arg: LanguageList.ISO_639_1(language: nationalLanguage)));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_etl_update_target_variable_metadata
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="targetVariable"></param>
                    /// <param name="metadata"></param>
                    /// <param name="metadataDiffNationalLanguage"></param>
                    /// <param name="metadataDiffEnglishLanguage"></param>
                    /// <param name="missingNationalLanguage"></param>
                    /// <param name="englishLanguageBoolean"></param>
                    /// <param name="nationalLanguageBoolean"></param>
                    /// <param name="nationalLanguage"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns></returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> targetVariable,
                        string metadata,
                        Nullable<bool> metadataDiffNationalLanguage,
                        Nullable<bool> metadataDiffEnglishLanguage,
                        Nullable<bool> missingNationalLanguage,
                        Nullable<bool> englishLanguageBoolean,
                        Nullable<bool> nationalLanguageBoolean,
                        Language nationalLanguage = PostgreSQL.Language.CS,
                        NpgsqlTransaction transaction = null)
                    {
                        string val = Execute(
                            database: database,
                            targetVariable: targetVariable,
                            metadata: metadata,
                            metadataDiffNationalLanguage: metadataDiffNationalLanguage,
                            metadataDiffEnglishLanguage: metadataDiffEnglishLanguage,
                            missingNationalLanguage: missingNationalLanguage,
                            englishLanguageBoolean: englishLanguageBoolean,
                            nationalLanguageBoolean: nationalLanguageBoolean,
                            nationalLanguage: nationalLanguage,
                            transaction: transaction);

                        DataTable dt = new();

                        dt.Columns.Add(column: new DataColumn()
                        {
                            ColumnName = Name,
                            DataType = Type.GetType(typeName: "System.String")
                        });

                        DataRow row = dt.NewRow();

                        Functions.SetStringArg(
                            row: row,
                            name: Name,
                            val: val);

                        dt.Rows.Add(row: row);

                        return dt;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_etl_update_target_variable_metadata
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="targetVariable"></param>
                    /// <param name="metadata"></param>
                    /// <param name="metadataDiffNationalLanguage"></param>
                    /// <param name="metadataDiffEnglishLanguage"></param>
                    /// <param name="missingNationalLanguage"></param>
                    /// <param name="englishLanguageBoolean"></param>
                    /// <param name="nationalLanguageBoolean"></param>
                    /// <param name="nationalLanguage"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns></returns>
                    public static string Execute(
                        NfiEstaDB database,
                        Nullable<int> targetVariable,
                        string metadata,
                        Nullable<bool> metadataDiffNationalLanguage,
                        Nullable<bool> metadataDiffEnglishLanguage,
                        Nullable<bool> missingNationalLanguage,
                        Nullable<bool> englishLanguageBoolean,
                        Nullable<bool> nationalLanguageBoolean,
                        Language nationalLanguage = PostgreSQL.Language.CS,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(
                            targetVariable: targetVariable,
                            metadata: metadata,
                            metadataDiffNationalLanguage: metadataDiffNationalLanguage,
                            metadataDiffEnglishLanguage: metadataDiffEnglishLanguage,
                            missingNationalLanguage: missingNationalLanguage,
                            englishLanguageBoolean: englishLanguageBoolean,
                            nationalLanguageBoolean: nationalLanguageBoolean,
                            nationalLanguage: nationalLanguage);

                        NpgsqlParameter pTargetVariable = new(
                            parameterName: "targetVariable",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pMetadata = new(
                             parameterName: "metadata",
                             parameterType: NpgsqlDbType.Json);

                        NpgsqlParameter pMetadataDiffNationalLanguage = new(
                            parameterName: "metadataDiffNationalLanguage",
                            parameterType: NpgsqlDbType.Boolean);

                        NpgsqlParameter pMetadataDiffEnglishLanguage = new(
                            parameterName: "metadataDiffEnglishLanguage",
                            parameterType: NpgsqlDbType.Boolean);

                        NpgsqlParameter pMissingNationalLanguage = new(
                            parameterName: "missingNationalLanguage",
                            parameterType: NpgsqlDbType.Boolean);

                        NpgsqlParameter pEnglishLanguageBoolean = new(
                           parameterName: "englishLanguageBoolean",
                           parameterType: NpgsqlDbType.Boolean);

                        NpgsqlParameter pNationalLanguageBoolean = new(
                            parameterName: "nationalLanguageBoolean",
                            parameterType: NpgsqlDbType.Boolean);

                        NpgsqlParameter pNationalLanguage = new(
                            parameterName: "nationalLanguage",
                            parameterType: NpgsqlDbType.Varchar);

                        if (targetVariable != null)
                        {
                            pTargetVariable.Value = (int)targetVariable;
                        }
                        else
                        {
                            pTargetVariable.Value = DBNull.Value;
                        }

                        if (metadata != null)
                        {
                            pMetadata.Value = (string)metadata;
                        }
                        else
                        {
                            pMetadata.Value = DBNull.Value;
                        }

                        if (metadataDiffNationalLanguage != null)
                        {
                            pMetadataDiffNationalLanguage.Value = (bool)metadataDiffNationalLanguage;
                        }
                        else
                        {
                            pMetadataDiffNationalLanguage.Value = DBNull.Value;
                        }

                        if (metadataDiffEnglishLanguage != null)
                        {
                            pMetadataDiffEnglishLanguage.Value = (bool)metadataDiffEnglishLanguage;
                        }
                        else
                        {
                            pMetadataDiffEnglishLanguage.Value = DBNull.Value;
                        }

                        if (missingNationalLanguage != null)
                        {
                            pMissingNationalLanguage.Value = (bool)missingNationalLanguage;
                        }
                        else
                        {
                            pMissingNationalLanguage.Value = DBNull.Value;
                        }

                        if (englishLanguageBoolean != null)
                        {
                            pEnglishLanguageBoolean.Value = (bool)englishLanguageBoolean;
                        }
                        else
                        {
                            pEnglishLanguageBoolean.Value = DBNull.Value;
                        }

                        if (nationalLanguageBoolean != null)
                        {
                            pNationalLanguageBoolean.Value = (bool)nationalLanguageBoolean;
                        }
                        else
                        {
                            pNationalLanguageBoolean.Value = DBNull.Value;
                        }

                        pNationalLanguage.Value =
                               LanguageList.ISO_639_1(language: nationalLanguage);

                        return
                            database.Postgres.ExecuteScalar(
                                sqlCommand: SQL,
                                transaction: transaction,
                                pTargetVariable, pMetadata,
                                pMetadataDiffNationalLanguage, pMetadataDiffEnglishLanguage, pMissingNationalLanguage,
                                pEnglishLanguageBoolean, pNationalLanguageBoolean, pNationalLanguage);
                    }

                    #endregion Methods

                }

                #endregion FnEtlUpdateTargetVariableMetadata

            }

        }
    }
}