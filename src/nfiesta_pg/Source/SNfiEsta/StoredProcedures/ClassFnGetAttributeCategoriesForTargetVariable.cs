﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {

            /// <summary>
            /// Stored procedures of schema nfiesta
            /// </summary>
            public static partial class NfiEstaFunctions
            {
                // fn_get_attribute_categories4target_variable

                #region FnGetAttributeCategoriesForTargetVariable

                /// <summary>
                /// Wrapper for stored procedure fn_get_attribute_categories4target_variable.
                /// Function returns table with attribute categories
                /// (area domain and sub population categories)
                /// for given target variable and area domain/sub population.
                /// It also solves the right combination of numerator and denominator categories.
                /// </summary>
                public static class FnGetAttributeCategoriesForTargetVariable
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = NfiEstaSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_get_attribute_categories4target_variable";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_numerator_target_variable integer, ",
                            $"_numerator_area_domain integer DEFAULT NULL::integer, ",
                            $"_numerator_sub_population integer DEFAULT NULL::integer, ",
                            $"_denominator_target_variable integer DEFAULT NULL::integer, ",
                            $"_denominator_area_domain integer DEFAULT NULL::integer, ",
                            $"_denominator_sub_population integer DEFAULT NULL::integer; ",
                            $"returns: ",
                            $"TABLE(",
                            $"nominator_variable integer, ",
                            $"denominator_variable integer, ",
                            $"label character varying, ",
                            $"description text, ",
                            $"label_en character varying, ",
                            $"description_en text)");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    nominator_variable                AS numerator_variable_id,{Environment.NewLine}",
                            $"    denominator_variable              AS denominator_variable_id,{Environment.NewLine}",
                            $"    label                             AS label,{Environment.NewLine}",
                            $"    description                       AS description,{Environment.NewLine}",
                            $"    label_en                          AS label_en,{Environment.NewLine}",
                            $"    description_en                    AS description_en{Environment.NewLine}",
                            $"FROM{Environment.NewLine}",
                            $"    $SchemaName.$Name(",
                            $"@numeratorTargetVariableId, ",
                            $"@numeratorAreaDomainId, ",
                            $"@numeratorSubPopulationId, ",
                            $"@denominatorTargetVariableId, ",
                            $"@denominatorAreaDomainId, ",
                            $"@denominatorSubPopulationId);{Environment.NewLine}");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="numeratorTargetVariableId">Identifier of the target variable in numerator</param>
                    /// <param name="numeratorAreaDomainId">Identifier of the area domain in numerator</param>
                    /// <param name="numeratorSubPopulationId">Identifier of the subpopulation in numerator</param>
                    /// <param name="denominatorTargetVariableId">Identifier of the target variable in numerator</param>
                    /// <param name="denominatorAreaDomainId">Identifier of the area domain in numerator</param>
                    /// <param name="denominatorSubPopulationId">Identifier of the subpopulation in numerator</param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        Nullable<int> numeratorTargetVariableId,
                        Nullable<int> numeratorAreaDomainId = null,
                        Nullable<int> numeratorSubPopulationId = null,
                        Nullable<int> denominatorTargetVariableId = null,
                        Nullable<int> denominatorAreaDomainId = null,
                        Nullable<int> denominatorSubPopulationId = null)
                    {
                        string result = SQL;

                        result = result.Replace(
                            oldValue: "@numeratorTargetVariableId",
                            newValue: Functions.PrepNIntArg(arg: numeratorTargetVariableId));

                        result = result.Replace(
                            oldValue: "@numeratorAreaDomainId",
                            newValue: Functions.PrepNIntArg(arg: numeratorAreaDomainId));

                        result = result.Replace(
                            oldValue: "@numeratorSubPopulationId",
                            newValue: Functions.PrepNIntArg(arg: numeratorSubPopulationId));

                        result = result.Replace(
                            oldValue: "@denominatorTargetVariableId",
                            newValue: Functions.PrepNIntArg(arg: denominatorTargetVariableId));

                        result = result.Replace(
                            oldValue: "@denominatorAreaDomainId",
                            newValue: Functions.PrepNIntArg(arg: denominatorAreaDomainId));

                        result = result.Replace(
                            oldValue: "@denominatorSubPopulationId",
                            newValue: Functions.PrepNIntArg(arg: denominatorSubPopulationId));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_get_attribute_categories4target_variable
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="numeratorTargetVariableId">Identifier of the target variable in numerator</param>
                    /// <param name="numeratorAreaDomainId">Identifier of the area domain in numerator</param>
                    /// <param name="numeratorSubPopulationId">Identifier of the subpopulation in numerator</param>
                    /// <param name="denominatorTargetVariableId">Identifier of the target variable in numerator</param>
                    /// <param name="denominatorAreaDomainId">Identifier of the area domain in numerator</param>
                    /// <param name="denominatorSubPopulationId">Identifier of the subpopulation in numerator</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table with attribute categories</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> numeratorTargetVariableId,
                        Nullable<int> numeratorAreaDomainId = null,
                        Nullable<int> numeratorSubPopulationId = null,
                        Nullable<int> denominatorTargetVariableId = null,
                        Nullable<int> denominatorAreaDomainId = null,
                        Nullable<int> denominatorSubPopulationId = null,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(
                            numeratorTargetVariableId: numeratorTargetVariableId,
                            numeratorAreaDomainId: numeratorAreaDomainId,
                            numeratorSubPopulationId: numeratorSubPopulationId,
                            denominatorTargetVariableId: denominatorTargetVariableId,
                            denominatorAreaDomainId: denominatorAreaDomainId,
                            denominatorSubPopulationId: denominatorSubPopulationId);

                        NpgsqlParameter pNumeratorTargetVariableId = new(
                            parameterName: "numeratorTargetVariableId",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pNumeratorAreaDomainId = new(
                            parameterName: "numeratorAreaDomainId",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pNumeratorSubPopulationId = new(
                            parameterName: "numeratorSubPopulationId",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pDenominatorTargetVariableId = new(
                            parameterName: "denominatorTargetVariableId",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pDenominatorAreaDomainId = new(
                            parameterName: "denominatorAreaDomainId",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pDenominatorSubPopulationId = new(
                            parameterName: "denominatorSubPopulationId",
                            parameterType: NpgsqlDbType.Integer);

                        if (numeratorTargetVariableId != null)
                        {
                            pNumeratorTargetVariableId.Value = (int)numeratorTargetVariableId;
                        }
                        else
                        {
                            pNumeratorTargetVariableId.Value = DBNull.Value;
                        }

                        if (numeratorAreaDomainId != null)
                        {
                            pNumeratorAreaDomainId.Value = (int)numeratorAreaDomainId;
                        }
                        else
                        {
                            pNumeratorAreaDomainId.Value = DBNull.Value;
                        }

                        if (numeratorSubPopulationId != null)
                        {
                            pNumeratorSubPopulationId.Value = (int)numeratorSubPopulationId;
                        }
                        else
                        {
                            pNumeratorSubPopulationId.Value = DBNull.Value;
                        }

                        if (denominatorTargetVariableId != null)
                        {
                            pDenominatorTargetVariableId.Value = (int)denominatorTargetVariableId;
                        }
                        else
                        {
                            pDenominatorTargetVariableId.Value = DBNull.Value;
                        }

                        if (denominatorAreaDomainId != null)
                        {
                            pDenominatorAreaDomainId.Value = (int)denominatorAreaDomainId;
                        }
                        else
                        {
                            pDenominatorAreaDomainId.Value = DBNull.Value;
                        }

                        if (denominatorSubPopulationId != null)
                        {
                            pDenominatorSubPopulationId.Value = (int)denominatorSubPopulationId;
                        }
                        else
                        {
                            pDenominatorSubPopulationId.Value = DBNull.Value;
                        }

                        return
                            database.Postgres.ExecuteQuery(
                                sqlCommand: SQL,
                                transaction: transaction,
                                pNumeratorTargetVariableId, pNumeratorAreaDomainId, pNumeratorSubPopulationId,
                                pDenominatorTargetVariableId, pDenominatorAreaDomainId, pDenominatorSubPopulationId);
                    }

                    /// <summary>
                    /// Execute stored procedure fn_get_attribute_categories4target_variable
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="vwVariable">List of combination of target variable and
                    /// subpopulation category and area domain category (extended version with labels)</param>
                    /// <param name="numeratorTargetVariableId">Identifier of the target variable in numerator</param>
                    /// <param name="numeratorAreaDomainId">Identifier of the area domain in numerator</param>
                    /// <param name="numeratorSubPopulationId">Identifier of the subpopulation in numerator</param>
                    /// <param name="denominatorTargetVariableId">Identifier of the target variable in numerator</param>
                    /// <param name="denominatorAreaDomainId">Identifier of the area domain in numerator</param>
                    /// <param name="denominatorSubPopulationId">Identifier of the subpopulation in numerator</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>List of area domains</returns>
                    public static VariablePairList Execute(
                        NfiEstaDB database,
                        VwVariableList vwVariable,
                        Nullable<int> numeratorTargetVariableId,
                        Nullable<int> numeratorAreaDomainId = null,
                        Nullable<int> numeratorSubPopulationId = null,
                        Nullable<int> denominatorTargetVariableId = null,
                        Nullable<int> denominatorAreaDomainId = null,
                        Nullable<int> denominatorSubPopulationId = null,
                        NpgsqlTransaction transaction = null)
                    {
                        DataTable dt = ExecuteQuery(
                            database: database,
                            numeratorTargetVariableId: numeratorTargetVariableId,
                            numeratorAreaDomainId: numeratorAreaDomainId,
                            numeratorSubPopulationId: numeratorSubPopulationId,
                            denominatorTargetVariableId: denominatorTargetVariableId,
                            denominatorAreaDomainId: denominatorAreaDomainId,
                            denominatorSubPopulationId: denominatorSubPopulationId,
                            transaction: transaction);

                        return
                            new VariablePairList(
                                database: database,
                                variablePairs: dt.AsEnumerable()
                                    .Select(a => new
                                    {
                                        VariableNumeratorId = a.Field<Nullable<int>>("numerator_variable_id"),
                                        VariableDenominatorId = a.Field<Nullable<int>>("denominator_variable_id")
                                    })
                                    .Select(a => new
                                    {
                                        VariableNumerator = vwVariable[a.VariableNumeratorId ?? 0],
                                        VariableDenominator = vwVariable[a.VariableDenominatorId ?? 0],
                                    })
                                    .Select(a => new VariablePair(
                                        numerator: a.VariableNumerator,
                                        denominator: a.VariableDenominator)));
                    }

                    #endregion Methods

                }

                #endregion FnGetAttributeCategoriesForTargetVariable

            }
        }
    }
}