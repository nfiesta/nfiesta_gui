﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {

            /// <summary>
            /// Stored procedures of schema nfiesta
            /// </summary>
            public static partial class NfiEstaFunctions
            {
                // fn_api_update_estimation_period

                #region FnApiUpdateEstimationPeriod

                /// <summary>
                /// Wrapper for stored procedure fn_api_update_estimation_period.
                /// The function updates label, description, label_en, description_en, default_in_olap (passed as arguments 2-6)
                /// in the lookup table nfiesta.c_estimation_period, where id = _id passed as an argument 1.
                /// </summary>
                public static class FnApiUpdateEstimationPeriod
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName = NfiEstaSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name = "fn_api_update_estimation_period";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_id integer, ",
                            $"_label character varying, ",
                            $"_description text, ",
                            $"_label_en character varying, ",
                            $"_description_en text, ",
                            $"_default_in_olap boolean; ",
                            $"returns: void");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    ?.Replace("$SchemaName", schemaName)
                                    ?.Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    {SchemaName}.{Name}",
                            $"(@estimationPeriodId, @estimationPeriodLabelCs, @estimationPeriodDescriptionCs, @estimationPeriodLabelEn, @estimationPeriodDescriptionEn, @estimationPeriodDefaultInOlap);{Environment.NewLine}");
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="estimationPeriodId">Estimation period identifier</param>
                    /// <param name="estimationPeriodLabelCs">Estimation period label in national language</param>
                    /// <param name="estimationPeriodDescriptionCs">Estimation period description in national language</param>
                    /// <param name="estimationPeriodLabelEn">Estimation period label in English</param>
                    /// <param name="estimationPeriodDescriptionEn">Estimation period description in English</param>
                    /// <param name="estimationPeriodDefaultInOlap">Limits the display of estimation periods to the periods with a value of true</param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        Nullable<int> estimationPeriodId,
                        string estimationPeriodLabelCs,
                        string estimationPeriodDescriptionCs,
                        string estimationPeriodLabelEn,
                        string estimationPeriodDescriptionEn,
                        Nullable<bool> estimationPeriodDefaultInOlap)
                    {
                        string result = SQL;

                        result = result.Replace(
                             oldValue: "@estimationPeriodId",
                             newValue: Functions.PrepNIntArg(arg: estimationPeriodId));

                        result = result.Replace(
                             oldValue: "@estimationPeriodLabelCs",
                             newValue: Functions.PrepStringArg(arg: estimationPeriodLabelCs));

                        result = result.Replace(
                             oldValue: "@estimationPeriodDescriptionCs",
                             newValue: Functions.PrepStringArg(arg: estimationPeriodDescriptionCs));

                        result = result.Replace(
                            oldValue: "@estimationPeriodLabelEn",
                            newValue: Functions.PrepStringArg(arg: estimationPeriodLabelEn));

                        result = result.Replace(
                            oldValue: "@estimationPeriodDescriptionEn",
                            newValue: Functions.PrepStringArg(arg: estimationPeriodDescriptionEn));

                        result = result.Replace(
                            oldValue: "@estimationPeriodDefaultInOlap",
                            newValue: Functions.PrepNBoolArg(arg: estimationPeriodDefaultInOlap));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_api_update_estimation_period
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="estimationPeriodId">Estimation period identifier</param>
                    /// <param name="estimationPeriodLabelCs">Estimation period label in national language</param>
                    /// <param name="estimationPeriodDescriptionCs">Estimation period description in national language</param>
                    /// <param name="estimationPeriodLabelEn">Estimation period label in English</param>
                    /// <param name="estimationPeriodDescriptionEn">Estimation period description in English</param>
                    /// <param name="estimationPeriodDefaultInOlap">Limits the display of estimation periods to the periods with a value of true</param>
                    /// <param name="transaction">Transaction</param>
                    public static void Execute(
                        NfiEstaDB database,
                        Nullable<int> estimationPeriodId,
                        string estimationPeriodLabelCs,
                        string estimationPeriodDescriptionCs,
                        string estimationPeriodLabelEn,
                        string estimationPeriodDescriptionEn,
                        Nullable<bool> estimationPeriodDefaultInOlap,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(
                            estimationPeriodId: estimationPeriodId,
                            estimationPeriodLabelCs: estimationPeriodLabelCs,
                            estimationPeriodDescriptionCs: estimationPeriodDescriptionCs,
                            estimationPeriodLabelEn: estimationPeriodLabelEn,
                            estimationPeriodDescriptionEn: estimationPeriodDescriptionEn,
                            estimationPeriodDefaultInOlap: estimationPeriodDefaultInOlap);

                        NpgsqlParameter pEstimationPeriodId = new(
                            parameterName: "estimationPeriodId",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pEstimationPeriodLabelCs = new(
                            parameterName: "estimationPeriodLabelCs",
                            parameterType: NpgsqlDbType.Varchar);

                        NpgsqlParameter pEstimationPeriodDescriptionCs = new(
                           parameterName: "estimationPeriodDescriptionCs",
                           parameterType: NpgsqlDbType.Text);

                        NpgsqlParameter pEstimationPeriodLabelEn = new(
                            parameterName: "estimationPeriodLabelEn",
                            parameterType: NpgsqlDbType.Varchar);

                        NpgsqlParameter pEstimationPeriodDescriptionEn = new(
                            parameterName: "estimationPeriodDescriptionEn",
                            parameterType: NpgsqlDbType.Text);

                        NpgsqlParameter pEstimationPeriodDefaultInOlap = new(
                            parameterName: "estimationPeriodDefaultInOlap",
                            parameterType: NpgsqlDbType.Boolean);

                        if (estimationPeriodId != null)
                        {
                            pEstimationPeriodId.Value = (int)estimationPeriodId;
                        }
                        else
                        {
                            pEstimationPeriodId.Value = DBNull.Value;
                        }

                        if (estimationPeriodLabelCs != null)
                        {
                            pEstimationPeriodLabelCs.Value = (string)estimationPeriodLabelCs;
                        }
                        else
                        {
                            pEstimationPeriodLabelCs.Value = DBNull.Value;
                        }

                        if (estimationPeriodDescriptionCs != null)
                        {
                            pEstimationPeriodDescriptionCs.Value = (string)estimationPeriodDescriptionCs;
                        }
                        else
                        {
                            pEstimationPeriodDescriptionCs.Value = DBNull.Value;
                        }

                        if (estimationPeriodLabelEn != null)
                        {
                            pEstimationPeriodLabelEn.Value = (string)estimationPeriodLabelEn;
                        }
                        else
                        {
                            pEstimationPeriodLabelEn.Value = DBNull.Value;
                        }

                        if (estimationPeriodDescriptionEn != null)
                        {
                            pEstimationPeriodDescriptionEn.Value = (string)estimationPeriodDescriptionEn;
                        }
                        else
                        {
                            pEstimationPeriodDescriptionEn.Value = DBNull.Value;
                        }

                        if (estimationPeriodDefaultInOlap != null)
                        {
                            pEstimationPeriodDefaultInOlap.Value = (bool)estimationPeriodDefaultInOlap;
                        }
                        else
                        {
                            pEstimationPeriodDefaultInOlap.Value = DBNull.Value;
                        }

                        database.Postgres.ExecuteNonQuery(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pEstimationPeriodId,
                            pEstimationPeriodLabelCs, pEstimationPeriodDescriptionCs,
                            pEstimationPeriodLabelEn, pEstimationPeriodDescriptionEn,
                            pEstimationPeriodDefaultInOlap);
                    }

                    /// <summary>
                    /// Execute stored procedure fn_api_update_estimation_period
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="estimationPeriodId">Estimation period identifier</param>
                    /// <param name="estimationPeriodLabelCs">Estimation period label in national language</param>
                    /// <param name="estimationPeriodDescriptionCs">Estimation period description in national language</param>
                    /// <param name="estimationPeriodLabelEn">Estimation period label in English</param>
                    /// <param name="estimationPeriodDescriptionEn">Estimation period description in English</param>
                    /// <param name="estimationPeriodDefaultInOlap">Limits the display of estimation periods to the periods with a value of true</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Empty data table</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> estimationPeriodId,
                        string estimationPeriodLabelCs,
                        string estimationPeriodDescriptionCs,
                        string estimationPeriodLabelEn,
                        string estimationPeriodDescriptionEn,
                        Nullable<bool> estimationPeriodDefaultInOlap,
                        NpgsqlTransaction transaction = null)
                    {
                        Execute(
                            database: database,
                            estimationPeriodId: estimationPeriodId,
                            estimationPeriodLabelCs: estimationPeriodLabelCs,
                            estimationPeriodDescriptionCs: estimationPeriodDescriptionCs,
                            estimationPeriodLabelEn: estimationPeriodLabelEn,
                            estimationPeriodDescriptionEn: estimationPeriodDescriptionEn,
                            estimationPeriodDefaultInOlap: estimationPeriodDefaultInOlap,
                            transaction: transaction);

                        return new DataTable();
                    }

                    #endregion Methods

                }

                #endregion FnApiUpdateEstimationPeriod

            }
        }
    }
}
