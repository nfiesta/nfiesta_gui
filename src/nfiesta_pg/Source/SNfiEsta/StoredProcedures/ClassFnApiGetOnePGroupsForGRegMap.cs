﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {

            /// <summary>
            /// Stored procedures of schema nfiesta
            /// </summary>
            public static partial class NfiEstaFunctions
            {
                // fn_api_get_1pgroups4gregmap

                #region FnApiGetOnePGroupsForGRegMap

                /// <summary>
                /// Wrapper for stored procedure fn_api_get_1pgroups4gregmap.
                /// This function returns list of distinct strata combinations and panel refyearset groups
                /// with a Boolean indicator saying whether there is a single-phase total configuration
                /// (using the given panel refyearset group)
                /// for each GREG-map total estimate in the set of desired estimates
                /// (a cartesian product of the subset of estimation cells intersecting
                /// all strata of the given strata combination and the desired list of variables).
                /// </summary>
                public static class FnApiGetOnePGroupsForGRegMap
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName = NfiEstaSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name = "fn_api_get_1pgroups4gregmap";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_estimation_period integer, ",
                            $"_estimation_cells integer[], ",
                            $"_variables integer[]; ",
                            $"returns: ",
                            $"TABLE(",
                            $"estimation_period_id integer, ",
                            $"estimation_period_label character varying, ",
                            $"estimation_period_label_en character varying, ",
                            $"estimation_period_description text, ",
                            $"estimation_period_description_en text, ",
                            $"country_id integer[], ",
                            $"country_label character varying[], ",
                            $"country_label_en character varying[], ",
                            $"country_description text[], ",
                            $"country_description_en text[], ",
                            $"strata_set_id integer[], ",
                            $"strata_set_label character varying[], ",
                            $"strata_set_label_en character varying[], ",
                            $"strata_set_description text[], ",
                            $"strata_set_description_en text[], ",
                            $"stratum_id integer[], ",
                            $"stratum_label character varying[], ",
                            $"stratum_label_en character varying[], ",
                            $"stratum_description text[], ",
                            $"stratum_description_en text[], ",
                            $"panel_refyearset_group_id integer, ",
                            $"panel_refyearset_group_label character varying, ",
                            $"panel_refyearset_group_label_en character varying, ",
                            $"panel_refyearset_group_description text, ",
                            $"panel_refyearset_group_description_en text, ",
                            $"complete_group boolean)");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", SchemaName)
                                    .Replace("$Name", Name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                String.Concat(
                                $"SELECT{Environment.NewLine}",
                                $"    {TFnApiGetOnePGroupsForGRegMapList.ColId.FuncCall}                                 AS {TFnApiGetOnePGroupsForGRegMapList.ColId.Name},{Environment.NewLine}",
                                $"    {TFnApiGetOnePGroupsForGRegMapList.ColEstimationPeriodId.DbName}                   AS {TFnApiGetOnePGroupsForGRegMapList.ColEstimationPeriodId.Name},{Environment.NewLine}",
                                $"    {TFnApiGetOnePGroupsForGRegMapList.ColEstimationPeriodLabelCs.DbName}              AS {TFnApiGetOnePGroupsForGRegMapList.ColEstimationPeriodLabelCs.Name},{Environment.NewLine}",
                                $"    {TFnApiGetOnePGroupsForGRegMapList.ColEstimationPeriodDescriptionCs.DbName}        AS {TFnApiGetOnePGroupsForGRegMapList.ColEstimationPeriodDescriptionCs.Name},{Environment.NewLine}",
                                $"    {TFnApiGetOnePGroupsForGRegMapList.ColEstimationPeriodLabelEn.DbName}              AS {TFnApiGetOnePGroupsForGRegMapList.ColEstimationPeriodLabelEn.Name},{Environment.NewLine}",
                                $"    {TFnApiGetOnePGroupsForGRegMapList.ColEstimationPeriodDescriptionEn.DbName}        AS {TFnApiGetOnePGroupsForGRegMapList.ColEstimationPeriodDescriptionEn.Name},{Environment.NewLine}",
                                $"    {TFnApiGetOnePGroupsForGRegMapList.ColCountryId.FuncCall}                          AS {TFnApiGetOnePGroupsForGRegMapList.ColCountryId.Name},{Environment.NewLine}",
                                $"    {TFnApiGetOnePGroupsForGRegMapList.ColCountryLabelCs.FuncCall}                     AS {TFnApiGetOnePGroupsForGRegMapList.ColCountryLabelCs.Name},{Environment.NewLine}",
                                $"    {TFnApiGetOnePGroupsForGRegMapList.ColCountryDescriptionCs.FuncCall}               AS {TFnApiGetOnePGroupsForGRegMapList.ColCountryDescriptionCs.Name},{Environment.NewLine}",
                                $"    {TFnApiGetOnePGroupsForGRegMapList.ColCountryLabelEn.FuncCall}                     AS {TFnApiGetOnePGroupsForGRegMapList.ColCountryLabelEn.Name},{Environment.NewLine}",
                                $"    {TFnApiGetOnePGroupsForGRegMapList.ColCountryDescriptionEn.FuncCall}               AS {TFnApiGetOnePGroupsForGRegMapList.ColCountryDescriptionEn.Name},{Environment.NewLine}",
                                $"    {TFnApiGetOnePGroupsForGRegMapList.ColStrataSetId.FuncCall}                        AS {TFnApiGetOnePGroupsForGRegMapList.ColStrataSetId.Name},{Environment.NewLine}",
                                $"    {TFnApiGetOnePGroupsForGRegMapList.ColStrataSetLabelCs.FuncCall}                   AS {TFnApiGetOnePGroupsForGRegMapList.ColStrataSetLabelCs.Name},{Environment.NewLine}",
                                $"    {TFnApiGetOnePGroupsForGRegMapList.ColStrataSetDescriptionCs.FuncCall}             AS {TFnApiGetOnePGroupsForGRegMapList.ColStrataSetDescriptionCs.Name},{Environment.NewLine}",
                                $"    {TFnApiGetOnePGroupsForGRegMapList.ColStrataSetLabelEn.FuncCall}                   AS {TFnApiGetOnePGroupsForGRegMapList.ColStrataSetLabelEn.Name},{Environment.NewLine}",
                                $"    {TFnApiGetOnePGroupsForGRegMapList.ColStrataSetDescriptionEn.FuncCall}             AS {TFnApiGetOnePGroupsForGRegMapList.ColStrataSetDescriptionEn.Name},{Environment.NewLine}",
                                $"    {TFnApiGetOnePGroupsForGRegMapList.ColStratumId.FuncCall}                          AS {TFnApiGetOnePGroupsForGRegMapList.ColStratumId.Name},{Environment.NewLine}",
                                $"    {TFnApiGetOnePGroupsForGRegMapList.ColStratumLabelCs.FuncCall}                     AS {TFnApiGetOnePGroupsForGRegMapList.ColStratumLabelCs.Name},{Environment.NewLine}",
                                $"    {TFnApiGetOnePGroupsForGRegMapList.ColStratumDescriptionCs.FuncCall}               AS {TFnApiGetOnePGroupsForGRegMapList.ColStratumDescriptionCs.Name},{Environment.NewLine}",
                                $"    {TFnApiGetOnePGroupsForGRegMapList.ColStratumLabelEn.FuncCall}                     AS {TFnApiGetOnePGroupsForGRegMapList.ColStratumLabelEn.Name},{Environment.NewLine}",
                                $"    {TFnApiGetOnePGroupsForGRegMapList.ColStratumDescriptionEn.FuncCall}               AS {TFnApiGetOnePGroupsForGRegMapList.ColStratumDescriptionEn.Name},{Environment.NewLine}",
                                $"    {TFnApiGetOnePGroupsForGRegMapList.ColPanelRefYearSetGroupId.DbName}               AS {TFnApiGetOnePGroupsForGRegMapList.ColPanelRefYearSetGroupId.Name},{Environment.NewLine}",
                                $"    {TFnApiGetOnePGroupsForGRegMapList.ColPanelRefYearSetGroupLabelCs.DbName}          AS {TFnApiGetOnePGroupsForGRegMapList.ColPanelRefYearSetGroupLabelCs.Name},{Environment.NewLine}",
                                $"    {TFnApiGetOnePGroupsForGRegMapList.ColPanelRefYearSetGroupDescriptionCs.DbName}    AS {TFnApiGetOnePGroupsForGRegMapList.ColPanelRefYearSetGroupDescriptionCs.Name},{Environment.NewLine}",
                                $"    {TFnApiGetOnePGroupsForGRegMapList.ColPanelRefYearSetGroupLabelEn.DbName}          AS {TFnApiGetOnePGroupsForGRegMapList.ColPanelRefYearSetGroupLabelEn.Name},{Environment.NewLine}",
                                $"    {TFnApiGetOnePGroupsForGRegMapList.ColPanelRefYearSetGroupDescriptionEn.DbName}    AS {TFnApiGetOnePGroupsForGRegMapList.ColPanelRefYearSetGroupDescriptionEn.Name},{Environment.NewLine}",
                                $"    {TFnApiGetOnePGroupsForGRegMapList.ColCompleteGroup.DbName}                        AS {TFnApiGetOnePGroupsForGRegMapList.ColCompleteGroup.Name}{Environment.NewLine}",
                                $"FROM{Environment.NewLine}",
                                $"    {SchemaName}.{Name}(@estimationPeriod, @estimationCells, @variables);{Environment.NewLine}");
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="estimationPeriod">Estimation period identifier</param>
                    /// <param name="estimationCells">List of estimation cells identifiers</param>
                    /// <param name="variables">List of attribute categories identifiers</param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        Nullable<int> estimationPeriod,
                        List<Nullable<int>> estimationCells,
                        List<Nullable<int>> variables)
                    {
                        string result = SQL;

                        result = result.Replace(
                           oldValue: "@estimationPeriod",
                           newValue: Functions.PrepNIntArg(arg: estimationPeriod, dbType: "int4"));

                        result = result.Replace(
                            oldValue: "@estimationCells",
                            newValue: Functions.PrepNIntArrayArg(args: estimationCells, dbType: "int4"));

                        result = result.Replace(
                            oldValue: "@variables",
                            newValue: Functions.PrepNIntArrayArg(args: variables, dbType: "int4"));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_api_get_1pgroups4gregmap
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="estimationPeriod">Estimation period identifier</param>
                    /// <param name="estimationCells">List of estimation cells identifiers</param>
                    /// <param name="variables">List of attribute categories identifiers</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> estimationPeriod,
                        List<Nullable<int>> estimationCells,
                        List<Nullable<int>> variables,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(estimationPeriod: estimationPeriod, estimationCells: estimationCells, variables: variables);

                        NpgsqlParameter pEstimationPeriod = new(
                            parameterName: "estimationPeriod",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pEstimationCells = new(
                            parameterName: "estimationCells",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        NpgsqlParameter pVariables = new(
                           parameterName: "variables",
                           parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        if (estimationPeriod != null)
                        {
                            pEstimationPeriod.Value = estimationPeriod;
                        }
                        else
                        {
                            pEstimationPeriod.Value = DBNull.Value;
                        }

                        if (estimationCells != null)
                        {
                            pEstimationCells.Value = estimationCells.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pEstimationCells.Value = DBNull.Value;
                        }

                        if (variables != null)
                        {
                            pVariables.Value = variables.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pVariables.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteQuery(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pEstimationPeriod, pEstimationCells, pVariables);
                    }

                    /// <summary>
                    /// Execute stored procedure fn_api_get_1pgroups4gregmap
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="estimationPeriod">Estimation period identifier</param>
                    /// <param name="estimationCells">List of estimation cells identifiers</param>
                    /// <param name="variables">List of attribute categories identifiers</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>
                    /// List of distinct strata combinations and panel refyearset groups
                    /// for each GREG-map total estimate in the set of desired estimates
                    /// </returns>
                    public static TFnApiGetOnePGroupsForGRegMapList Execute(
                        NfiEstaDB database,
                        Nullable<int> estimationPeriod,
                        List<Nullable<int>> estimationCells,
                        List<Nullable<int>> variables,
                        NpgsqlTransaction transaction = null)
                    {
                        System.Diagnostics.Stopwatch stopWatch = new();
                        stopWatch.Start();

                        DataTable data = ExecuteQuery(
                            database: database,
                            estimationPeriod: estimationPeriod,
                            estimationCells: estimationCells,
                            variables: variables,
                            transaction: transaction);
                        data.TableName = Name;

                        stopWatch.Stop();

                        return new TFnApiGetOnePGroupsForGRegMapList(
                            database: database,
                            data: data,
                            loadingTime: 0.001 * stopWatch.ElapsedMilliseconds,
                            storedProcedure: CommandText)
                        {
                            EstimationPeriodId = estimationPeriod,
                            EstimationCellIds = estimationCells,
                            VariableIds = variables
                        };
                    }

                    #endregion Methods

                }

                #endregion FnApiGetOnePGroupsForGRegMap

            }

        }
    }
}