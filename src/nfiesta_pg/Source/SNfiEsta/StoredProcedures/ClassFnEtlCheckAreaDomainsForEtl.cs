﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {

            /// <summary>
            /// Stored procedures of schema nfiesta
            /// </summary>
            public static partial class NfiEstaFunctions
            {
                // fn_etl_check_area_domains4etl

                #region FnEtlCheckAreaDomainsForEtl

                /// <summary>
                /// Wrapper for stored procedure fn_etl_check_area_domains4etl.
                /// The function returns one record of area domain if input label of area domain was paired in target DB or the function returns record(s) of area domains that are possible to pair in target DB.
                /// </summary>
                public static class FnEtlCheckAreaDomainsForEtl
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName = NfiEstaSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name = "fn_etl_check_area_domains4etl";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_id integer, ",
                            $"_area_domain integer[], ",
                            $"_label_en character varying, ",
                            $"_national_language character varying DEFAULT 'en'::character varying(2), ",
                            $"_etl_id integer[] DEFAULT NULL::integer[]; ",
                            $"returns: ",
                            $"TABLE(",
                            $"id integer, ",
                            $"etl_id integer, ",
                            $"label character varying, ",
                            $"description text, ",
                            $"label_en character varying, ",
                            $"description_en text, ",
                            $"atomic boolean, ",
                            $"check_label_en boolean)");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    $Name.id                 AS id,{Environment.NewLine}",
                            $"    $Name.etl_id             AS etl_id,{Environment.NewLine}",
                            $"    $Name.label              AS label,{Environment.NewLine}",
                            $"    $Name.description        AS description,{Environment.NewLine}",
                            $"    $Name.label_en           AS label_en,{Environment.NewLine}",
                            $"    $Name.description_en     AS description_en,{Environment.NewLine}",
                            $"    $Name.atomic             AS atomic,{Environment.NewLine}",
                            $"    $Name.check_label_en     AS check_label_en{Environment.NewLine}",
                            $"FROM{Environment.NewLine}",
                            $"    $SchemaName.$Name(@id, @areaDomain, @labelEn, @nationalLanguage, @etlId) AS $Name;{Environment.NewLine}");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="id"></param>
                    /// <param name="areaDomain"></param>
                    /// <param name="labelEn"></param>
                    /// <param name="nationalLanguage"></param>
                    /// <param name="etlId"></param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        Nullable<int> id,
                        List<Nullable<int>> areaDomain,
                        string labelEn,
                        string nationalLanguage,
                        List<Nullable<int>> etlId)
                    {
                        string result = SQL;

                        result = result.Replace(
                             oldValue: "@id",
                             newValue: Functions.PrepNIntArg(arg: id));

                        result = result.Replace(
                            oldValue: "@areaDomain",
                            newValue: Functions.PrepNIntArrayArg(args: areaDomain, dbType: "int4"));

                        result = result.Replace(
                            oldValue: "@labelEn",
                            newValue: Functions.PrepStringArg(arg: labelEn));

                        result = result.Replace(
                            oldValue: "@nationalLanguage",
                            newValue: Functions.PrepStringArg(arg: nationalLanguage));

                        result = result.Replace(
                            oldValue: "@etlId",
                            newValue: Functions.PrepNIntArrayArg(args: etlId, dbType: "int4"));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_etl_check_area_domains4etl
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="id"></param>
                    /// <param name="areaDomain"></param>
                    /// <param name="labelEn"></param>
                    /// <param name="nationalLanguage"></param>
                    /// <param name="etlId"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns></returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> id,
                        List<Nullable<int>> areaDomain,
                        string labelEn,
                        string nationalLanguage,
                        List<Nullable<int>> etlId,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(
                            id: id,
                            areaDomain: areaDomain,
                            labelEn: labelEn,
                            nationalLanguage: nationalLanguage,
                            etlId: etlId);

                        NpgsqlParameter pId = new(
                            parameterName: "id",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pAreaDomain = new(
                            parameterName: "areaDomain",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        NpgsqlParameter pLabelEn = new(
                            parameterName: "labelEn",
                            parameterType: NpgsqlDbType.Varchar);

                        NpgsqlParameter pNationalLanguage = new(
                            parameterName: "nationalLanguage",
                            parameterType: NpgsqlDbType.Varchar);

                        NpgsqlParameter pEtlId = new(
                            parameterName: "etlId",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        if (id != null)
                        {
                            pId.Value = (int)id;
                        }
                        else
                        {
                            pId.Value = DBNull.Value;
                        }

                        if (areaDomain != null)
                        {
                            pAreaDomain.Value = areaDomain.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pAreaDomain.Value = DBNull.Value;
                        }

                        if (labelEn != null)
                        {
                            pLabelEn.Value = (string)labelEn;
                        }
                        else
                        {
                            pLabelEn.Value = DBNull.Value;
                        }

                        if (nationalLanguage != null)
                        {
                            pNationalLanguage.Value = (string)nationalLanguage;
                        }
                        else
                        {
                            pNationalLanguage.Value = DBNull.Value;
                        }

                        if (etlId != null)
                        {
                            pEtlId.Value = etlId.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pEtlId.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteQuery(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pId, pAreaDomain, pLabelEn, pNationalLanguage, pEtlId);
                    }

                    #endregion Methods

                }

                #endregion FnEtlCheckSubPopulationsForEtl

            }
        }
    }
}
