﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {

            /// <summary>
            /// Stored procedures of schema nfiesta
            /// </summary>
            public static partial class NfiEstaFunctions
            {
                // fn_etl_import_ldsity_values

                #region FnEtlImportLDsityValues

                /// <summary>
                /// Wrapper for stored procedure fn_etl_import_ldsity_values.
                /// The function for the specified list of input arguments inserts data
                /// into the t_available_datasets table and inserts data into the t_target_data table (aggregated local density at the plot level).
                /// </summary>
                public static class FnEtlImportLDsityValues
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName = NfiEstaSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name = "fn_etl_import_ldsity_values";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_available_datasets_and_ldsity_values json; ",
                            $"returns: text");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    $SchemaName.$Name(@availableDatasetsAndLdsityValues)::text AS $Name;{Environment.NewLine}");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="availableDatasetsAndLdsityValues"></param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(string availableDatasetsAndLdsityValues)
                    {
                        string result = SQL;

                        result = result.Replace(
                            oldValue: "@availableDatasetsAndLdsityValues",
                            newValue: Functions.PrepStringArg(arg: availableDatasetsAndLdsityValues));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_etl_import_ldsity_values
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="availableDatasetsAndLdsityValues"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns></returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        string availableDatasetsAndLdsityValues,
                        NpgsqlTransaction transaction = null)
                    {
                        string val = Execute(
                            database: database,
                            availableDatasetsAndLdsityValues: availableDatasetsAndLdsityValues,
                            transaction: transaction);

                        DataTable dt = new();

                        dt.Columns.Add(column: new DataColumn()
                        {
                            ColumnName = Name,
                            DataType = Type.GetType(typeName: "System.String")
                        });

                        DataRow row = dt.NewRow();

                        Functions.SetStringArg(
                            row: row,
                            name: Name,
                            val: val);

                        dt.Rows.Add(row: row);

                        return dt;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_etl_import_ldsity_values
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="availableDatasetsAndLdsityValues"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns></returns>
                    public static string Execute(
                        NfiEstaDB database,
                        string availableDatasetsAndLdsityValues,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(availableDatasetsAndLdsityValues: availableDatasetsAndLdsityValues);

                        NpgsqlParameter pAvailableDatasetsAndLdsityValues = new(
                            parameterName: "availableDatasetsAndLdsityValues",
                            parameterType: NpgsqlDbType.Json);

                        if (availableDatasetsAndLdsityValues != null)
                        {
                            pAvailableDatasetsAndLdsityValues.Value = (string)availableDatasetsAndLdsityValues;
                        }
                        else
                        {
                            pAvailableDatasetsAndLdsityValues.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteScalar(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pAvailableDatasetsAndLdsityValues);
                    }

                    #endregion Methods

                }

                #endregion FnEtlImportLDsityValues

            }

        }
    }
}