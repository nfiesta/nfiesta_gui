﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {

            /// <summary>
            /// Stored procedures of schema nfiesta
            /// </summary>
            public static partial class NfiEstaFunctions
            {
                // fn_api_do_labels_and_descriptions_of_panel_refyearset_group_exi

                #region FnApiDoLabelsAndDescriptionsOfPanelRefYearSetGroupExist

                /// <summary>
                /// Wrapper for stored procedure fn_api_do_labels_and_descriptions_of_panel_refyearset_group_exi.
                /// For each of the function arguments (label, label_en, description, description_en)
                /// the function checks whether a record with identical value of the argument exists in the codelist
                /// of groups of panel and reference-year combinations.
                /// The function returns four Booleans.
                /// The TRUE value indicate that a group with the same value of the respective argument was found,
                /// otherwise FALSE is returned.
                /// </summary>
                public static class FnApiDoLabelsAndDescriptionsOfPanelRefYearSetGroupExist
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName = NfiEstaSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name = "fn_api_do_labels_and_descriptions_of_panel_refyearset_group_exi";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_label character varying, ",
                            $"_label_en character varying, ",
                            $"_description text, ",
                            $"_description_en text, ",
                            $"OUT _label_exists boolean, ",
                            $"OUT _label_en_exists boolean, ",
                            $"OUT _description_exists boolean, ",
                            $"OUT _description_en_exists boolean; ",
                            $"returns: ",
                            $"record");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return String.Concat(
                                $"SELECT{Environment.NewLine}",
                                $"    {TFnApiDoLabelsAndDescriptionsOfPanelRefYearSetGroupExistList.ColId.FuncCall}                 AS {TFnApiDoLabelsAndDescriptionsOfPanelRefYearSetGroupExistList.ColId.Name},{Environment.NewLine}",
                                $"    {TFnApiDoLabelsAndDescriptionsOfPanelRefYearSetGroupExistList.ColLabelCsFound.DbName}         AS {TFnApiDoLabelsAndDescriptionsOfPanelRefYearSetGroupExistList.ColLabelCsFound.Name},{Environment.NewLine}",
                                $"    {TFnApiDoLabelsAndDescriptionsOfPanelRefYearSetGroupExistList.ColLabelEnFound.DbName}         AS {TFnApiDoLabelsAndDescriptionsOfPanelRefYearSetGroupExistList.ColLabelEnFound.Name},{Environment.NewLine}",
                                $"    {TFnApiDoLabelsAndDescriptionsOfPanelRefYearSetGroupExistList.ColDescriptionCsFound.DbName}   AS {TFnApiDoLabelsAndDescriptionsOfPanelRefYearSetGroupExistList.ColDescriptionCsFound.Name},{Environment.NewLine}",
                                $"    {TFnApiDoLabelsAndDescriptionsOfPanelRefYearSetGroupExistList.ColDescriptionEnFound.DbName}   AS {TFnApiDoLabelsAndDescriptionsOfPanelRefYearSetGroupExistList.ColDescriptionEnFound.Name}{Environment.NewLine}",
                                $"FROM{Environment.NewLine}",
                                $"    {SchemaName}.{Name}(@labelCs, @labelEn, @descriptionCs, @descriptionEn);{Environment.NewLine}");
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="labelCs">Label of panel reference year set group in national language</param>
                    /// <param name="labelEn">Label of panel reference year set group in English</param>
                    /// <param name="descriptionCs">Description of panel reference year set group in national language</param>
                    /// <param name="descriptionEn">Description of panel reference year set group in English</param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        string labelCs,
                        string labelEn,
                        string descriptionCs,
                        string descriptionEn)
                    {
                        string result = SQL;

                        result = result.Replace(
                            oldValue: "@labelEn",
                            newValue: Functions.PrepStringArg(arg: labelEn, dbType: "character varying"));

                        result = result.Replace(
                            oldValue: "@descriptionEn",
                            newValue: Functions.PrepStringArg(arg: descriptionEn, dbType: "text"));

                        result = result.Replace(
                            oldValue: "@labelCs",
                            newValue: Functions.PrepStringArg(arg: labelCs, dbType: "character varying"));

                        result = result.Replace(
                            oldValue: "@descriptionCs",
                            newValue: Functions.PrepStringArg(arg: descriptionCs, dbType: "text"));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_api_do_labels_and_descriptions_of_panel_refyearset_group_exi
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="labelCs">Label of panel reference year set group in national language</param>
                    /// <param name="labelEn">Label of panel reference year set group in English</param>
                    /// <param name="descriptionCs">Description of panel reference year set group in national language</param>
                    /// <param name="descriptionEn">Description of panel reference year set group in English</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table of panel reference year set group checks of label and description values</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        string labelCs,
                        string labelEn,
                        string descriptionCs,
                        string descriptionEn,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(
                            labelCs: labelCs,
                            labelEn: labelEn,
                            descriptionCs: descriptionCs,
                            descriptionEn: descriptionEn);

                        NpgsqlParameter pLabelCs = new(
                            parameterName: "labelCs",
                            parameterType: NpgsqlDbType.Varchar);

                        NpgsqlParameter pLabelEn = new(
                            parameterName: "labelEn",
                            parameterType: NpgsqlDbType.Varchar);

                        NpgsqlParameter pDescriptionCs = new(
                            parameterName: "descriptionCs",
                            parameterType: NpgsqlDbType.Text);

                        NpgsqlParameter pDescriptionEn = new(
                            parameterName: "descriptionEn",
                            parameterType: NpgsqlDbType.Text);

                        if (labelCs != null)
                        {
                            pLabelCs.Value = labelCs;
                        }
                        else
                        {
                            pLabelCs.Value = DBNull.Value;
                        }

                        if (labelEn != null)
                        {
                            pLabelEn.Value = labelEn;
                        }
                        else
                        {
                            pLabelEn.Value = DBNull.Value;
                        }

                        if (descriptionCs != null)
                        {
                            pDescriptionCs.Value = descriptionCs;
                        }
                        else
                        {
                            pDescriptionCs.Value = DBNull.Value;
                        }

                        if (descriptionEn != null)
                        {
                            pDescriptionEn.Value = descriptionEn;
                        }
                        else
                        {
                            pDescriptionEn.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteQuery(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pLabelCs, pLabelEn, pDescriptionCs, pDescriptionEn);
                    }

                    /// <summary>
                    /// Execute stored procedure fn_api_do_labels_and_descriptions_of_panel_refyearset_group_exi
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="labelCs">Label of panel reference year set group in national language</param>
                    /// <param name="labelEn">Label of panel reference year set group in English</param>
                    /// <param name="descriptionCs">Description of panel reference year set group in national language</param>
                    /// <param name="descriptionEn">Description of panel reference year set group in English</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>List of panel reference year set group checks of label and description values</returns>
                    public static TFnApiDoLabelsAndDescriptionsOfPanelRefYearSetGroupExistList Execute(
                        NfiEstaDB database,
                        string labelCs,
                        string labelEn,
                        string descriptionCs,
                        string descriptionEn,
                        NpgsqlTransaction transaction = null)
                    {
                        System.Diagnostics.Stopwatch stopWatch = new();
                        stopWatch.Start();

                        DataTable data = ExecuteQuery(
                            database: database,
                            labelCs: labelCs,
                            labelEn: labelEn,
                            descriptionCs: descriptionCs,
                            descriptionEn: descriptionEn,
                            transaction: transaction);
                        data.TableName = Name;

                        stopWatch.Stop();

                        return new TFnApiDoLabelsAndDescriptionsOfPanelRefYearSetGroupExistList(
                            database: database,
                            data: data,
                            loadingTime: 0.001 * stopWatch.ElapsedMilliseconds,
                            storedProcedure: CommandText)
                        {
                            LabelCs = labelCs,
                            LabelEn = labelEn,
                            DescriptionCs = descriptionCs,
                            DescriptionEn = descriptionEn
                        };
                    }

                    #endregion Methods

                }

                #endregion FnApiDoLabelsAndDescriptionsOfPanelRefYearSetGroupExist

            }

        }
    }
}