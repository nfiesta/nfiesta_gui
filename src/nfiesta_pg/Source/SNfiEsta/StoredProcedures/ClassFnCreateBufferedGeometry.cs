﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {

            /// <summary>
            /// Stored procedures of schema nfiesta
            /// </summary>
            public static partial class NfiEstaFunctions
            {
                // fn_create_buffered_geometry

                #region FnCreateBufferedGeometry

                /// <summary>
                /// Wrapper for stored procedure fn_create_buffered_geometry.
                /// Functions generates a buffered geometry of original geographical domain for specified tract_type.
                /// 100 -- 2p, 1p rotated -- Two points, the second is rotated around the first.
                /// 200 -- 2p, no rotation -- N-E, Two points, no rotation, the second located North-East.
                /// 300 -- 4p in square, N-E -- Four points in square shape, no rotation. North, North-East, Eeast locations.
                /// </summary>
                public static class FnCreateBufferedGeometry
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName = NfiEstaSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name = "fn_create_buffered_geometry";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_geom geometry, ",
                            $"_tract_type integer, ",
                            $"_point_distance_m integer; ",
                            $"returns: geometry");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    ST_AsBinary($SchemaName.$Name",
                            $"(ST_GeomFromText(@geom), @tractType, @pointDistanceM))::bytea AS $Name;{Environment.NewLine}");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="geom"></param>
                    /// <param name="tractType"></param>
                    /// <param name="pointDistanceM"></param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        string geom,
                        Nullable<int> tractType,
                        Nullable<int> pointDistanceM)
                    {
                        string result = SQL;

                        result = result.Replace(
                            oldValue: "@geom",
                            newValue: Functions.PrepStringArg(arg: geom));

                        result = result.Replace(
                            oldValue: "@tractType",
                            newValue: Functions.PrepNIntArg(arg: tractType));

                        result = result.Replace(
                            oldValue: "@pointDistanceM",
                            newValue: Functions.PrepNIntArg(arg: pointDistanceM));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_create_buffered_geometry
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="geom"></param>
                    /// <param name="tractType"></param>
                    /// <param name="pointDistanceM"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns></returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        string geom,
                        Nullable<int> tractType,
                        Nullable<int> pointDistanceM,
                        NpgsqlTransaction transaction = null)
                    {
                        string val = Execute(
                           database: database,
                           geom: geom,
                           tractType: tractType,
                           pointDistanceM: pointDistanceM,
                           transaction: transaction);

                        DataTable dt = new();

                        dt.Columns.Add(column: new DataColumn()
                        {
                            ColumnName = Name,
                            DataType = Type.GetType(typeName: "System.String")
                        });

                        DataRow row = dt.NewRow();

                        Functions.SetStringArg(
                            row: row,
                            name: Name,
                            val: val);

                        dt.Rows.Add(row: row);

                        return dt;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_create_buffered_geometry
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="geom"></param>
                    /// <param name="tractType"></param>
                    /// <param name="pointDistanceM"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns></returns>
                    public static string Execute(
                       NfiEstaDB database,
                       string geom,
                       Nullable<int> tractType,
                       Nullable<int> pointDistanceM,
                       NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(geom: geom, tractType: tractType, pointDistanceM: pointDistanceM);

                        NpgsqlParameter pGeom = new(
                            parameterName: "geom",
                            parameterType: NpgsqlDbType.Text);

                        NpgsqlParameter pTractType = new(
                            parameterName: "tractType",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pPointDistanceM = new(
                            parameterName: "pointDistanceM",
                            parameterType: NpgsqlDbType.Integer);

                        if (geom != null)
                        {
                            pGeom.Value = (string)geom;
                        }
                        else
                        {
                            pGeom.Value = DBNull.Value;
                        }

                        if (tractType != null)
                        {
                            pTractType.Value = (int)tractType;
                        }
                        else
                        {
                            pTractType.Value = DBNull.Value;
                        }

                        if (pointDistanceM != null)
                        {
                            pPointDistanceM.Value = (int)pointDistanceM;
                        }
                        else
                        {
                            pPointDistanceM.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteScalar(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pGeom, pTractType, pPointDistanceM);
                    }

                    #endregion Methods

                }

                #endregion FnCreateBufferedGeometry

            }

        }
    }
}