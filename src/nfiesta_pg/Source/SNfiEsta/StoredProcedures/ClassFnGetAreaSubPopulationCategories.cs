﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {

            /// <summary>
            /// Stored procedures of schema nfiesta
            /// </summary>
            public static partial class NfiEstaFunctions
            {
                // fn_get_area_sub_population_categories

                #region FnGetAreaSubPopulationCategories

                /// <summary>
                /// Wrapper for stored procedure fn_get_area_sub_population_categories.
                /// Function returns table with all hierarchically superior variables and its complementary
                /// categories within area domain or sub_population. Input parameters are target_variable (id from table c_target_variable),
                /// area_or_sub_pop (100 for area_domain, 200 for sub_population) and id (id from table c_area_domain or id from table c_sub_population)
                /// </summary>
                public static class FnGetAreaSubPopulationCategories
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName = NfiEstaSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name = "fn_get_area_sub_population_categories";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_target_variable integer, ",
                            $"_area_or_sub_pop integer, ",
                            $"_id integer; ",
                            $"returns: ",
                            $"TABLE(",
                            $"variable integer, ",
                            $"attype integer, ",
                            $"category integer, ",
                            $"label character varying, ",
                            $"description text, ",
                            $"label_en character varying, ",
                            $"description_en text)");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    coalesce($Name.variable, 0)       AS variable,{Environment.NewLine}",
                            $"    coalesce($Name.attype, 0)         AS attype,{Environment.NewLine}",
                            $"    coalesce($Name.category, 0)       AS category,{Environment.NewLine}",
                            $"    $Name.label                       AS label,{Environment.NewLine}",
                            $"    $Name.description                 AS description,{Environment.NewLine}",
                            $"    $Name.label_en                    AS label_en,{Environment.NewLine}",
                            $"    $Name.description_en              AS description_en{Environment.NewLine}",
                            $"FROM{Environment.NewLine}",
                            $"    $SchemaName.$Name(@targetVariable, @areaOrSubPop, @domainId) AS $Name;{Environment.NewLine}");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="targetVariable">Id from table c_target_variable</param>
                    /// <param name="areaOrSubPop">100 for area_domain, 200 for sub_population</param>
                    /// <param name="domainId">Id from table c_area_domain or id from table c_sub_population</param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        Nullable<int> targetVariable,
                        Nullable<int> areaOrSubPop,
                        Nullable<int> domainId)
                    {
                        domainId =
                            (domainId == 0)
                                ? null
                                : domainId;

                        string result = SQL;

                        result = result.Replace(
                            oldValue: "@targetVariable",
                            newValue: Functions.PrepNIntArg(arg: targetVariable));

                        result = result.Replace(
                            oldValue: "@areaOrSubPop",
                            newValue: Functions.PrepNIntArg(arg: areaOrSubPop));

                        result = result.Replace(
                            oldValue: "@domainId",
                            newValue: Functions.PrepNIntArg(arg: domainId));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_get_area_sub_population_categories
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="targetVariable">Id from table c_target_variable</param>
                    /// <param name="areaOrSubPop">100 for area_domain, 200 for sub_population</param>
                    /// <param name="domainId">Id from table c_area_domain or id from table c_sub_population</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>
                    /// Data table with all hierarchically superior variables
                    /// and its complementary categories within area domain or sub_population
                    /// </returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> targetVariable,
                        Nullable<int> areaOrSubPop,
                        Nullable<int> domainId,
                        NpgsqlTransaction transaction = null)
                    {
                        domainId =
                            (domainId == 0)
                                ? null
                                : domainId;

                        CommandText = GetCommandText(
                            targetVariable: targetVariable,
                            areaOrSubPop: areaOrSubPop,
                            domainId: domainId);

                        NpgsqlParameter pTargetVariable = new(
                            parameterName: "targetVariable",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pAreaOrSubPop = new(
                            parameterName: "areaOrSubPop",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pDomainId = new(
                            parameterName: "domainId",
                            parameterType: NpgsqlDbType.Integer);

                        if (targetVariable != null)
                        {
                            pTargetVariable.Value = (int)targetVariable;
                        }
                        else
                        {
                            pTargetVariable.Value = DBNull.Value;
                        }

                        if (areaOrSubPop != null)
                        {
                            pAreaOrSubPop.Value = (int)areaOrSubPop;
                        }
                        else
                        {
                            pAreaOrSubPop.Value = DBNull.Value;
                        }

                        if (domainId != null)
                        {
                            pDomainId.Value = (int)domainId;
                        }
                        else
                        {
                            pDomainId.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteQuery(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pTargetVariable, pAreaOrSubPop, pDomainId);
                    }

                    /// <summary>
                    /// List of hierarchically superior area domains
                    /// for selected target variable and area domain
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="cAreaDomain">List of all area domains,
                    /// extended by domain 0 - altogether</param>
                    /// <param name="targetVariable">Selected target variable</param>
                    /// <param name="areaDomain">Selected area domain</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>
                    /// List of hierarchically superior area domains
                    /// for selected target variable and area domain
                    /// </returns>
                    public static AreaDomainList AreaDomains(
                        NfiEstaDB database,
                        AreaDomainList cAreaDomain,
                        ITargetVariable targetVariable,
                        AreaDomain areaDomain,
                        NpgsqlTransaction transaction = null)
                    {
                        DataTable dt = ExecuteQuery(
                            database: database,
                            targetVariable: targetVariable.Id,
                            areaOrSubPop: (int)NfiEstaPg.TargetData.TDArealOrPopulationEnum.AreaDomain,
                            domainId: areaDomain.Id,
                            transaction: transaction);

                        List<int> superiorAreaDomainIds =
                            dt.AsEnumerable()
                            .Select(a => a.Field<int>("attype"))
                            .Distinct()
                            .ToList();

                        return
                            new AreaDomainList(
                                database: database,
                                rows:
                                    cAreaDomain.Data.AsEnumerable()
                                    .Where(a => superiorAreaDomainIds
                                    .Contains(a.Field<int>(AreaDomainList.ColId.Name)))
                                    .OrderBy(a => a.Field<int>(AreaDomainList.ColId.Name)));
                    }

                    /// <summary>
                    /// List of hierarchically superior subpopulations
                    /// for selected target variable and subpopulation
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="cSubPopulation">List of all subpopulations,
                    /// extended by domain 0 - altogether</param>
                    /// <param name="targetVariable">Selected target variable</param>
                    /// <param name="subPopulation">Selected subpopulation</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>
                    /// List of hierarchically superior subpopulations
                    /// for selected target variable and subpopulation
                    /// </returns>
                    public static SubPopulationList SubPopulations(
                        NfiEstaDB database,
                        SubPopulationList cSubPopulation,
                        ITargetVariable targetVariable,
                        SubPopulation subPopulation,
                        NpgsqlTransaction transaction = null)
                    {
                        DataTable dt = ExecuteQuery(
                            database: database,
                            targetVariable: targetVariable.Id,
                            areaOrSubPop: (int)NfiEstaPg.TargetData.TDArealOrPopulationEnum.Population,
                            domainId: subPopulation.Id,
                            transaction: transaction);

                        List<int> superioSubPopulationIds =
                            dt.AsEnumerable()
                            .Select(a => a.Field<int>("attype"))
                            .Distinct()
                            .ToList();

                        return
                            new SubPopulationList(
                                database: database,
                                rows:
                                    cSubPopulation.Data.AsEnumerable()
                                    .Where(a => superioSubPopulationIds
                                    .Contains(a.Field<int>(SubPopulationList.ColId.Name)))
                                    .OrderBy(a => a.Field<int>(SubPopulationList.ColId.Name)));
                    }

                    /// <summary>
                    /// List of hierarchically superior area domain categories
                    /// for selected target variable and area domain
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="cAreaDomainCategory">List of all area domain categories,
                    /// extended by category 0 - altogether</param>
                    /// <param name="targetVariable">Selected target variable</param>
                    /// <param name="areaDomain">Selected area domain</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>
                    /// List of hierarchically superior area domain categories
                    /// for selected target variable and area domain
                    /// </returns>
                    public static AreaDomainCategoryList AreaDomainCategories(
                        NfiEstaDB database,
                        AreaDomainCategoryList cAreaDomainCategory,
                        ITargetVariable targetVariable,
                        AreaDomain areaDomain,
                        NpgsqlTransaction transaction = null)
                    {
                        DataTable dt = ExecuteQuery(
                            database: database,
                            targetVariable: targetVariable.Id,
                            areaOrSubPop: (int)NfiEstaPg.TargetData.TDArealOrPopulationEnum.AreaDomain,
                            domainId: areaDomain.Id,
                            transaction: transaction);

                        List<int> superiorAreaDomainCategoryIds =
                            dt.AsEnumerable()
                            .Select(a => a.Field<int>("category"))
                            .Distinct()
                            .ToList();

                        return
                            new AreaDomainCategoryList(
                                database: database,
                                rows:
                                    cAreaDomainCategory.Data.AsEnumerable()
                                    .Where(a => superiorAreaDomainCategoryIds
                                    .Contains(a.Field<int>(AreaDomainCategoryList.ColId.Name)))
                                    .OrderBy(a => a.Field<int>(AreaDomainCategoryList.ColId.Name)));
                    }

                    /// <summary>
                    /// List of hierarchically superior subpopulation categories
                    /// for selected target variable and subpopulation
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="cSubPopulationCategory">List of all subpopulation categories,
                    /// extended by domain 0 - altogether</param>
                    /// <param name="targetVariable">Selected target variable</param>
                    /// <param name="subPopulation">Selected subpopulation</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>
                    /// List of hierarchically superior subpopulation categories
                    /// for selected target variable and subpopulation
                    /// </returns>
                    public static SubPopulationCategoryList SubPopulationCategories(
                        NfiEstaDB database,
                        SubPopulationCategoryList cSubPopulationCategory,
                        ITargetVariable targetVariable,
                        SubPopulation subPopulation,
                        NpgsqlTransaction transaction = null)
                    {
                        DataTable dt = ExecuteQuery(
                            database: database,
                            targetVariable: targetVariable.Id,
                            areaOrSubPop: (int)NfiEstaPg.TargetData.TDArealOrPopulationEnum.Population,
                            domainId: subPopulation.Id,
                            transaction: transaction);

                        List<int> superioSubPopulationCategoryIds =
                            dt.AsEnumerable()
                            .Select(a => a.Field<int>("category"))
                            .Distinct()
                            .ToList();

                        return
                            new SubPopulationCategoryList(
                                database: database,
                                rows:
                                    cSubPopulationCategory.Data.AsEnumerable()
                                    .Where(a => superioSubPopulationCategoryIds
                                    .Contains(a.Field<int>(SubPopulationCategoryList.ColId.Name)))
                                    .OrderBy(a => a.Field<int>(SubPopulationCategoryList.ColId.Name)));
                    }

                    #endregion Methods

                }

                #endregion FnGetAreaSubPopulationCategories

            }

        }
    }
}