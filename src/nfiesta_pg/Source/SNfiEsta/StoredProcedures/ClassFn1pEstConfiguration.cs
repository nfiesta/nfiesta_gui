﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {

            /// <summary>
            /// Stored procedures of schema nfiesta
            /// </summary>
            public static partial class NfiEstaFunctions
            {
                // fn_1p_est_configuration

                #region Fn1pEstConfiguration

                /// <summary>
                /// Wrapper for stored procedure fn_1p_est_configuration
                /// Function makes the estimate configuration present in the database
                /// - provides inserts into the all necessary tables.
                /// </summary>
                public static partial class Fn1pEstConfiguration
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = NfiEstaSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_1p_est_configuration";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_panel_refyearset_group integer, ",
                            $"_estimation_cell integer, ",
                            $"_estimation_period integer, ",
                            $"_note character varying, ",
                            $"_variable integer; ",
                            $"returns: integer");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    $SchemaName.$Name(",
                            $"@panelRefYearSetGroup, ",
                            $"@estimationCell, ",
                            $"@estimationPeriod,",
                            $"@note, ",
                            $"@variable) AS $Name;{Environment.NewLine}");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="panelRefYearSetGroup"></param>
                    /// <param name="estimationCell"></param>
                    /// <param name="estimationPeriod"></param>
                    /// <param name="note"></param>
                    /// <param name="variable"></param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        Nullable<int> panelRefYearSetGroup,
                        Nullable<int> estimationCell,
                        Nullable<int> estimationPeriod,
                        string note,
                        Nullable<int> variable)
                    {
                        string result = SQL;

                        result = result.Replace(
                            oldValue: "@panelRefYearSetGroup",
                            newValue: Functions.PrepNIntArg(arg: panelRefYearSetGroup));

                        result = result.Replace(
                            oldValue: "@estimationCell",
                            newValue: Functions.PrepNIntArg(arg: estimationCell));

                        result = result.Replace(
                           oldValue: "@estimationPeriod",
                           newValue: Functions.PrepNIntArg(arg: estimationPeriod));

                        result = result.Replace(
                           oldValue: "@note",
                           newValue: Functions.PrepStringArg(arg: note));

                        result = result.Replace(
                           oldValue: "@variable",
                           newValue: Functions.PrepNIntArg(arg: variable));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_1p_est_configuration
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="panelRefYearSetGroup"></param>
                    /// <param name="estimationCell"></param>
                    /// <param name="estimationPeriod"></param>
                    /// <param name="note"></param>
                    /// <param name="variable"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns></returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> panelRefYearSetGroup,
                        Nullable<int> estimationCell,
                        Nullable<int> estimationPeriod,
                        string note,
                        Nullable<int> variable,
                        NpgsqlTransaction transaction = null)
                    {
                        Nullable<int> val = Execute(
                            database: database,
                            panelRefYearSetGroup: panelRefYearSetGroup,
                            estimationCell: estimationCell,
                            estimationPeriod: estimationPeriod,
                            note: note,
                            variable: variable,
                            transaction: transaction);

                        DataTable dt = new();

                        dt.Columns.Add(column: new DataColumn()
                        {
                            ColumnName = Name,
                            DataType = Type.GetType(typeName: "System.Int32")
                        });

                        DataRow row = dt.NewRow();

                        Functions.SetNIntArg(
                            row: row,
                            name: Name,
                            val: val);

                        dt.Rows.Add(row: row);

                        return dt;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_1p_est_configuration
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="panelRefYearSetGroup"></param>
                    /// <param name="estimationCell"></param>
                    /// <param name="estimationPeriod"></param>
                    /// <param name="note"></param>
                    /// <param name="variable"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns></returns>
                    public static Nullable<int> Execute(
                        NfiEstaDB database,
                        Nullable<int> panelRefYearSetGroup,
                        Nullable<int> estimationCell,
                        Nullable<int> estimationPeriod,
                        string note,
                        Nullable<int> variable,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(
                            panelRefYearSetGroup: panelRefYearSetGroup,
                            estimationCell: estimationCell,
                            estimationPeriod: estimationPeriod,
                            note: note,
                            variable: variable);

                        NpgsqlParameter pPanelRefYearSetGroup = new(
                            parameterName: "panelRefYearSetGroup",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pEstimationCell = new(
                           parameterName: "estimationCell",
                           parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pEstimationPeriod = new(
                           parameterName: "estimationPeriod",
                           parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pNote = new(
                           parameterName: "note",
                           parameterType: NpgsqlDbType.Varchar);

                        NpgsqlParameter pVariable = new(
                           parameterName: "variable",
                           parameterType: NpgsqlDbType.Integer);

                        if (panelRefYearSetGroup != null)
                        {
                            pPanelRefYearSetGroup.Value = (int)panelRefYearSetGroup;
                        }
                        else
                        {
                            pPanelRefYearSetGroup.Value = DBNull.Value;
                        }

                        if (estimationCell != null)
                        {
                            pEstimationCell.Value = (int)estimationCell;
                        }
                        else
                        {
                            pEstimationCell.Value = DBNull.Value;
                        }

                        if (estimationPeriod != null)
                        {
                            pEstimationPeriod.Value = (int)estimationPeriod;
                        }
                        else
                        {
                            pEstimationPeriod.Value = DBNull.Value;
                        }

                        if (note != null)
                        {
                            pNote.Value = (string)note;
                        }
                        else
                        {
                            pNote.Value = DBNull.Value;
                        }

                        if (variable != null)
                        {
                            pVariable.Value = (int)variable;
                        }
                        else
                        {
                            pVariable.Value = DBNull.Value;
                        }

                        string strResult = database.Postgres.ExecuteScalar(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pPanelRefYearSetGroup, pEstimationCell,
                            pEstimationPeriod, pNote, pVariable);

                        if (String.IsNullOrEmpty(value: strResult))
                        {
                            return null;
                        }
                        else
                        {
                            if (Int32.TryParse(s: strResult, result: out int intResult))
                            {
                                return intResult;
                            }
                            else
                            {
                                return null;
                            }
                        }
                    }

                    #endregion Methods

                }

                #endregion Fn1pEstConfiguration

            }

        }
    }
}