﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using System;
using System.Data;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {

            /// <summary>
            /// Stored procedures of schema nfiesta
            /// </summary>
            public static partial class NfiEstaFunctions
            {
                // fn_api_get_list_of_estimation_periods

                #region FnApiGetListOfEstimationPeriods

                /// <summary>
                /// Wrapper for stored procedure fn_api_get_list_of_estimation_periods.
                /// The function returns id, estimate_date_begin, estimate_date_end, label, description,
                /// label_en, description_en, default_in_olap from nfiesta.c_estimation_period
                /// of all estimation periods.
                /// </summary>
                public static class FnApiGetListOfEstimationPeriods
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = NfiEstaSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_api_get_list_of_estimation_periods";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ; ",
                            $"returns: ",
                            $"TABLE(",
                            $"id integer, ",
                            $"estimate_date_begin date, ",
                            $"estimate_date_end date, ",
                            $"label character varying, ",
                            $"description text, ",
                            $"label_en character varying, ",
                            $"description_en text, ",
                            $"default_in_olap boolean)");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                String.Concat(
                                    $"SELECT{Environment.NewLine}",
                                    $"    {EstimationPeriodList.ColId.SQL(cols: EstimationPeriodList.Cols, alias: Name)}",
                                    $"    {EstimationPeriodList.ColEstimateDateBegin.SQL(cols: EstimationPeriodList.Cols, alias: Name)}",
                                    $"    {EstimationPeriodList.ColEstimateDateEnd.SQL(cols: EstimationPeriodList.Cols, alias: Name)}",
                                    $"    {EstimationPeriodList.ColLabelCs.SQL(cols: EstimationPeriodList.Cols, alias: Name)}",
                                    $"    {EstimationPeriodList.ColDescriptionCs.SQL(cols: EstimationPeriodList.Cols, alias: Name)}",
                                    $"    {EstimationPeriodList.ColLabelEn.SQL(cols: EstimationPeriodList.Cols, alias: Name)}",
                                    $"    {EstimationPeriodList.ColDescriptionEn.SQL(cols: EstimationPeriodList.Cols, alias: Name)}",
                                    $"    {EstimationPeriodList.ColDefaultInOlap.SQL(cols: EstimationPeriodList.Cols, alias: Name, isLastOne: true)}",
                                    $"FROM{Environment.NewLine}",
                                    $"    {SchemaName}.{Name}(){Environment.NewLine}",
                                    $"ORDER BY{Environment.NewLine}",
                                    $"    {EstimationPeriodList.ColId.DbName};{Environment.NewLine}");
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <returns>Command text</returns>
                    public static string GetCommandText()
                    {
                        return SQL;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_api_get_list_of_estimation_periods
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table of estimation periods</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText();

                        return database.Postgres.ExecuteQuery(
                            sqlCommand: SQL,
                            transaction: transaction);
                    }

                    /// <summary>
                    /// Execute stored procedure fn_api_get_list_of_estimation_periods
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>
                    /// List of estimation periods
                    /// </returns>
                    public static EstimationPeriodList Execute(
                        NfiEstaDB database,
                        NpgsqlTransaction transaction = null)
                    {
                        System.Diagnostics.Stopwatch stopWatch = new();
                        stopWatch.Start();

                        DataTable data = ExecuteQuery(
                            database: database,
                            transaction: transaction);
                        data.TableName = Name;

                        stopWatch.Stop();

                        return new EstimationPeriodList(
                            database: database,
                            data: data,
                            loadingTime: 0.001 * stopWatch.ElapsedMilliseconds,
                            storedProcedure: CommandText)
                        { };
                    }

                    #endregion Methods

                }

                #endregion FnApiGetListOfEstimationPeriods

            }
        }
    }
}
