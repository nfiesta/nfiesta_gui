﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {

            /// <summary>
            /// Stored procedures of schema nfiesta
            /// </summary>
            public static partial class NfiEstaFunctions
            {
                // fn_api_delete_estimation_period

                #region FnApiDeleteEstimationPeriod

                /// <summary>
                /// Wrapper for stored procedure fn_api_delete_estimation_period.
                /// The function deletes row from the nfiesta.c_estimation_period lookup table, where id = _id passed as an argument.
                /// </summary>
                public static class FnApiDeleteEstimationPeriod
                {
                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName = NfiEstaSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name = "fn_api_delete_estimation_period";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_id integer; ",
                            $"returns: void");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    ?.Replace("$SchemaName", schemaName)
                                    ?.Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    {SchemaName}.{Name}",
                            $"(@estimationPeriodId);{Environment.NewLine}");
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="estimationPeriodId">Estimation period identifier</param>
                    public static string GetCommandText(
                        Nullable<int> estimationPeriodId)
                    {
                        string result = SQL;

                        result = result.Replace(
                             oldValue: "@estimationPeriodId",
                             newValue: Functions.PrepNIntArg(arg: estimationPeriodId));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_api_delete_estimation_period
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="estimationPeriodId">Estimation period identifier</param>
                    /// <param name="transaction">Transaction</param>
                    public static void Execute(
                        NfiEstaDB database,
                        Nullable<int> estimationPeriodId,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(
                            estimationPeriodId: estimationPeriodId);

                        NpgsqlParameter pEstimationPeriodId = new(
                            parameterName: "estimationPeriodId",
                            parameterType: NpgsqlDbType.Integer);

                        if (estimationPeriodId != null)
                        {
                            pEstimationPeriodId.Value = (int)estimationPeriodId;
                        }
                        else
                        {
                            pEstimationPeriodId.Value = DBNull.Value;
                        }

                        database.Postgres.ExecuteNonQuery(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pEstimationPeriodId);
                    }

                    /// <summary>
                    /// Execute stored procedure fn_api_delete_estimation_period
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="estimationPeriodId">Estimation period identifier</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Empty data table</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> estimationPeriodId,
                        NpgsqlTransaction transaction = null)
                    {
                        Execute(
                            database: database,
                            estimationPeriodId: estimationPeriodId,
                            transaction: transaction);

                        return new DataTable();
                    }

                    #endregion Methods

                    #endregion Properties
                }

                #endregion FnApiDeleteEstimationPeriod
            }
        }
    }
}