﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {

            /// <summary>
            /// Stored procedures of schema nfiesta
            /// </summary>
            public static partial class NfiEstaFunctions
            {
                // fn_api_get_gregmap_configs4ratio

                #region FnApiGetGRegMapConfigsForRatio

                /// <summary>
                /// Wrapper for stored procedure fn_api_get_gregmap_configs4ratio.
                /// For input estimation period, estimation cells, numerator and denominator variables
                /// the function returns valid combinations of panel-refyearset group, param. area type,
                /// force synthetic, model and sigma of already configured GREG-map totals
                /// that can be combined in the denominator and numerator of the ratio.
                /// Only ratios of GREG-map totals using the same group of panel and reference-year set combinations,
                /// an identical value of force_synthetic
                /// (both the numerator and denominator must use the same inference,
                /// either model-based or design-based), and the same type of parametrisation region
                /// (meaning that the actual parametrisation regions of numerator and denominator of the ratio always match).
                /// Note that the function combines the elements of the input arrays _variables_nom and _variables_denom (considering their order)
                /// to obtain the desired set of estimates. These two input arrays must have an equal length, and their elements must be ordered,
                /// so each combination of their elements on the same position corresponds to one desired combination of variables in the numerator
                /// and the denominator of the ratio. The output parameter all_estimates_configurable is TRUE if such a ratio can be configured
                /// for all input combinations of estimation cell, numerator variable and denominator variable.
                /// The output parameters total_estimate_conf_numerator and total_estimate_conf_denominator are ordered arrays of identifiers
                /// of existing GREG-map configurations (record ids of the table nfiesta.t_total_estimate_conf)
                /// for the numerator and denominator respectively.
                /// </summary>
                public static class FnApiGetGRegMapConfigsForRatio
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName = NfiEstaSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name = "fn_api_get_gregmap_configs4ratio";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_estimation_period integer, ",
                            $"_estimation_cells integer[], ",
                            $"_variables_nom integer[], ",
                            $"_variables_denom integer[]; ",
                            $"returns: ",
                            $"TABLE(",
                            $"panel_refyearset_group_id integer, ",
                            $"panel_refyearset_group_label character varying, ",
                            $"panel_refyearset_group_label_en character varying, ",
                            $"panel_refyearset_group_description text, ",
                            $"panel_refyearset_group_description_en text, ",
                            $"param_area_type_id integer, ",
                            $"param_area_type_label character varying, ",
                            $"param_area_type_label_en character varying, ",
                            $"param_area_type_description text, ",
                            $"param_area_type_description_en text, ",
                            $"force_synthetic boolean, ",
                            $"nominator_model_id integer, ",
                            $"nominator_model_label character varying, ",
                            $"nominator_model_label_en character varying, ",
                            $"nominator_model_description text, ",
                            $"nominator_model_description_en text, ",
                            $"nominator_model_sigma boolean, ",
                            $"denominator_model_id integer, ",
                            $"denominator_model_label character varying, ",
                            $"denominator_model_label_en character varying, ",
                            $"denominator_model_description text, ",
                            $"denominator_model_description_en text, ",
                            $"denominator_model_sigma boolean, ",
                            $"all_estimates_configurable boolean, ",
                            $"total_estimate_conf_nominator integer[], ",
                            $"total_estimate_conf_denominator integer[])");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    {TFnApiGetGRegMapConfigsForRatioList.ColId.FuncCall}                                 AS {TFnApiGetGRegMapConfigsForRatioList.ColId.Name},{Environment.NewLine}",
                            $"    {TFnApiGetGRegMapConfigsForRatioList.ColPanelRefYearSetGroupId.DbName}               AS {TFnApiGetGRegMapConfigsForRatioList.ColPanelRefYearSetGroupId.Name},{Environment.NewLine}",
                            $"    {TFnApiGetGRegMapConfigsForRatioList.ColPanelRefYearSetGroupLabelCs.DbName}          AS {TFnApiGetGRegMapConfigsForRatioList.ColPanelRefYearSetGroupLabelCs.Name},{Environment.NewLine}",
                            $"    {TFnApiGetGRegMapConfigsForRatioList.ColPanelRefYearSetGroupDescriptionCs.DbName}    AS {TFnApiGetGRegMapConfigsForRatioList.ColPanelRefYearSetGroupDescriptionCs.Name},{Environment.NewLine}",
                            $"    {TFnApiGetGRegMapConfigsForRatioList.ColPanelRefYearSetGroupLabelEn.DbName}          AS {TFnApiGetGRegMapConfigsForRatioList.ColPanelRefYearSetGroupLabelEn.Name},{Environment.NewLine}",
                            $"    {TFnApiGetGRegMapConfigsForRatioList.ColPanelRefYearSetGroupDescriptionEn.DbName}    AS {TFnApiGetGRegMapConfigsForRatioList.ColPanelRefYearSetGroupDescriptionEn.Name},{Environment.NewLine}",
                            $"    {TFnApiGetGRegMapConfigsForRatioList.ColParamAreaTypeId.DbName}                      AS {TFnApiGetGRegMapConfigsForRatioList.ColParamAreaTypeId.Name},{Environment.NewLine}",
                            $"    {TFnApiGetGRegMapConfigsForRatioList.ColParamAreaTypeLabelCs.DbName}                 AS {TFnApiGetGRegMapConfigsForRatioList.ColParamAreaTypeLabelCs.Name},{Environment.NewLine}",
                            $"    {TFnApiGetGRegMapConfigsForRatioList.ColParamAreaTypeDescriptionCs.DbName}           AS {TFnApiGetGRegMapConfigsForRatioList.ColParamAreaTypeDescriptionCs.Name},{Environment.NewLine}",
                            $"    {TFnApiGetGRegMapConfigsForRatioList.ColParamAreaTypeLabelEn.DbName}                 AS {TFnApiGetGRegMapConfigsForRatioList.ColParamAreaTypeLabelEn.Name},{Environment.NewLine}",
                            $"    {TFnApiGetGRegMapConfigsForRatioList.ColParamAreaTypeDescriptionEn.DbName}           AS {TFnApiGetGRegMapConfigsForRatioList.ColParamAreaTypeDescriptionEn.Name},{Environment.NewLine}",
                            $"    {TFnApiGetGRegMapConfigsForRatioList.ColForceSynthetic.DbName}                       AS {TFnApiGetGRegMapConfigsForRatioList.ColForceSynthetic.Name},{Environment.NewLine}",
                            $"    {TFnApiGetGRegMapConfigsForRatioList.ColNumeratorModelId.DbName}                     AS {TFnApiGetGRegMapConfigsForRatioList.ColNumeratorModelId.Name},{Environment.NewLine}",
                            $"    {TFnApiGetGRegMapConfigsForRatioList.ColNumeratorModelLabelCs.DbName}                AS {TFnApiGetGRegMapConfigsForRatioList.ColNumeratorModelLabelCs.Name},{Environment.NewLine}",
                            $"    {TFnApiGetGRegMapConfigsForRatioList.ColNumeratorModelDescriptionCs.DbName}          AS {TFnApiGetGRegMapConfigsForRatioList.ColNumeratorModelDescriptionCs.Name},{Environment.NewLine}",
                            $"    {TFnApiGetGRegMapConfigsForRatioList.ColNumeratorModelLabelEn.DbName}                AS {TFnApiGetGRegMapConfigsForRatioList.ColNumeratorModelLabelEn.Name},{Environment.NewLine}",
                            $"    {TFnApiGetGRegMapConfigsForRatioList.ColNumeratorModelDescriptionEn.DbName}          AS {TFnApiGetGRegMapConfigsForRatioList.ColNumeratorModelDescriptionEn.Name},{Environment.NewLine}",
                            $"    {TFnApiGetGRegMapConfigsForRatioList.ColNumeratorModelSigma.DbName}                  AS {TFnApiGetGRegMapConfigsForRatioList.ColNumeratorModelSigma.Name},{Environment.NewLine}",
                            $"    {TFnApiGetGRegMapConfigsForRatioList.ColDenominatorModelId.DbName}                   AS {TFnApiGetGRegMapConfigsForRatioList.ColDenominatorModelId.Name},{Environment.NewLine}",
                            $"    {TFnApiGetGRegMapConfigsForRatioList.ColDenominatorModelLabelCs.DbName}              AS {TFnApiGetGRegMapConfigsForRatioList.ColDenominatorModelLabelCs.Name},{Environment.NewLine}",
                            $"    {TFnApiGetGRegMapConfigsForRatioList.ColDenominatorModelDescriptionCs.DbName}        AS {TFnApiGetGRegMapConfigsForRatioList.ColDenominatorModelDescriptionCs.Name},{Environment.NewLine}",
                            $"    {TFnApiGetGRegMapConfigsForRatioList.ColDenominatorModelLabelEn.DbName}              AS {TFnApiGetGRegMapConfigsForRatioList.ColDenominatorModelLabelEn.Name},{Environment.NewLine}",
                            $"    {TFnApiGetGRegMapConfigsForRatioList.ColDenominatorModelDescriptionEn.DbName}        AS {TFnApiGetGRegMapConfigsForRatioList.ColDenominatorModelDescriptionEn.Name},{Environment.NewLine}",
                            $"    {TFnApiGetGRegMapConfigsForRatioList.ColDenominatorModelSigma.DbName}                AS {TFnApiGetGRegMapConfigsForRatioList.ColDenominatorModelSigma.Name},{Environment.NewLine}",
                            $"    {TFnApiGetGRegMapConfigsForRatioList.ColAllEstimatesConfigurable.DbName}             AS {TFnApiGetGRegMapConfigsForRatioList.ColAllEstimatesConfigurable.Name},{Environment.NewLine}",
                            $"    {TFnApiGetGRegMapConfigsForRatioList.ColTotalEstimateConfNumerator.FuncCall}         AS {TFnApiGetGRegMapConfigsForRatioList.ColTotalEstimateConfNumerator.Name},{Environment.NewLine}",
                            $"    {TFnApiGetGRegMapConfigsForRatioList.ColTotalEstimateConfDenominator.FuncCall}       AS {TFnApiGetGRegMapConfigsForRatioList.ColTotalEstimateConfDenominator.Name}{Environment.NewLine}",
                            $"FROM{Environment.NewLine}",
                            $"    {SchemaName}.{Name}(@estimationPeriod, @estimationCells, @variablesNumerator, @variablesDenominator);{Environment.NewLine}");
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="estimationPeriod">Estimation period identifier</param>
                    /// <param name="estimationCells">List of estimation cells identifiers</param>
                    /// <param name="variablesNumerator">List of attribute categories identifiers for numerator</param>
                    /// <param name="variablesDenominator">List of attribute categories identifiers for denominator</param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        Nullable<int> estimationPeriod,
                        List<Nullable<int>> estimationCells,
                        List<Nullable<int>> variablesNumerator,
                        List<Nullable<int>> variablesDenominator)
                    {
                        string result = SQL;

                        result = result.Replace(
                            oldValue: "@estimationPeriod",
                            newValue: Functions.PrepNIntArg(arg: estimationPeriod, dbType: "int4"));

                        result = result.Replace(
                            oldValue: "@estimationCells",
                            newValue: Functions.PrepNIntArrayArg(args: estimationCells, dbType: "int4"));

                        result = result.Replace(
                            oldValue: "@variablesNumerator",
                            newValue: Functions.PrepNIntArrayArg(args: variablesNumerator, dbType: "int4"));

                        result = result.Replace(
                           oldValue: "@variablesDenominator",
                           newValue: Functions.PrepNIntArrayArg(args: variablesDenominator, dbType: "int4"));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_api_get_gregmap_configs4ratio
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="estimationPeriod">Estimation period identifier</param>
                    /// <param name="estimationCells">List of estimation cells identifiers</param>
                    /// <param name="variablesNumerator">List of attribute categories identifiers for numerator</param>
                    /// <param name="variablesDenominator">List of attribute categories identifiers for denominator</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>
                    /// Data table with valid combinations of panel-refyearset group, param. area type,
                    /// force synthetic, model and sigma of already configured GREG-map totals
                    /// </returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> estimationPeriod,
                        List<Nullable<int>> estimationCells,
                        List<Nullable<int>> variablesNumerator,
                        List<Nullable<int>> variablesDenominator,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(
                            estimationPeriod: estimationPeriod,
                            estimationCells: estimationCells,
                            variablesNumerator: variablesNumerator,
                            variablesDenominator: variablesDenominator);

                        NpgsqlParameter pEstimationPeriod = new(
                            parameterName: "estimationPeriod",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pEstimationCells = new(
                            parameterName: "estimationCells",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        NpgsqlParameter pVariablesNumerator = new(
                           parameterName: "variablesNumerator",
                           parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        NpgsqlParameter pVariablesDenominator = new(
                           parameterName: "variablesDenominator",
                           parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        if (estimationPeriod != null)
                        {
                            pEstimationPeriod.Value = estimationPeriod;
                        }
                        else
                        {
                            pEstimationPeriod.Value = DBNull.Value;
                        }

                        if (estimationCells != null)
                        {
                            pEstimationCells.Value = estimationCells.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pEstimationCells.Value = DBNull.Value;
                        }

                        if (variablesNumerator != null)
                        {
                            pVariablesNumerator.Value = variablesNumerator.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pVariablesNumerator.Value = DBNull.Value;
                        }

                        if (variablesDenominator != null)
                        {
                            pVariablesDenominator.Value = variablesDenominator.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pVariablesDenominator.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteQuery(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pEstimationPeriod, pEstimationCells,
                            pVariablesNumerator, pVariablesDenominator);
                    }

                    /// <summary>
                    /// Execute stored procedure fn_api_get_gregmap_configs4ratio
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="estimationPeriod">Estimation period identifier</param>
                    /// <param name="estimationCells">List of estimation cells identifiers</param>
                    /// <param name="variablesNumerator">List of attribute categories identifiers for numerator</param>
                    /// <param name="variablesDenominator">List of attribute categories identifiers for denominator</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>
                    /// List of valid combinations of panel-refyearset group, param. area type,
                    /// force synthetic, model and sigma of already configured GREG-map totals
                    /// </returns>
                    public static TFnApiGetGRegMapConfigsForRatioList Execute(
                        NfiEstaDB database,
                        Nullable<int> estimationPeriod,
                        List<Nullable<int>> estimationCells,
                        List<Nullable<int>> variablesNumerator,
                        List<Nullable<int>> variablesDenominator,
                        NpgsqlTransaction transaction = null)
                    {
                        System.Diagnostics.Stopwatch stopWatch = new();
                        stopWatch.Start();

                        DataTable data = ExecuteQuery(
                            database: database,
                            estimationPeriod: estimationPeriod,
                            estimationCells: estimationCells,
                            variablesNumerator: variablesNumerator,
                            variablesDenominator: variablesDenominator,
                            transaction: transaction);
                        data.TableName = Name;

                        stopWatch.Stop();

                        return new TFnApiGetGRegMapConfigsForRatioList(
                            database: database,
                            data: data,
                            loadingTime: 0.001 * stopWatch.ElapsedMilliseconds,
                            storedProcedure: CommandText)
                        {
                            EstimationPeriodId = estimationPeriod,
                            EstimationCellsIds = estimationCells,
                            VariablesNumeratorIds = variablesNumerator,
                            VariablesDenominatorIds = variablesDenominator
                        };
                    }

                    #endregion Methods

                }

                #endregion FnApiGetGRegMapConfigsForRatio

            }

        }
    }
}