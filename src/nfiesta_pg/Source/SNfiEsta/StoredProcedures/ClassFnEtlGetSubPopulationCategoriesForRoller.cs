﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {

            /// <summary>
            /// Stored procedures of schema nfiesta
            /// </summary>
            public static partial class NfiEstaFunctions
            {
                // fn_etl_get_sub_population_categories4roller

                #region FnEtlGetSubPopulationCategoriesForRoller

                /// <summary>
                /// Wrapper for stored procedure fn_etl_get_sub_population_categories4roller.
                /// The function returns records of subpopulation categories for given subpopulation into roller.
                /// </summary>
                public static class FnEtlGetSubPopulationCategoriesForRoller
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName = NfiEstaSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name = "fn_etl_get_sub_population_categories4roller";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_id_ordering integer, ",
                            $"_sub_population integer, ",
                            $"_etl_id integer[] DEFAULT NULL::integer[]; ",
                            $"returns: ",
                            $"TABLE(",
                            $"id integer, ",
                            $"etl_id integer, ",
                            $"label character varying, ",
                            $"description text, ",
                            $"label_en character varying, ",
                            $"description_en text)");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                            = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    $Name.id                 AS id,{Environment.NewLine}",
                            $"    $Name.etl_id             AS etl_id,{Environment.NewLine}",
                            $"    $Name.label              AS label,{Environment.NewLine}",
                            $"    $Name.description        AS description,{Environment.NewLine}",
                            $"    $Name.label_en           AS label_en,{Environment.NewLine}",
                            $"    $Name.description_en     AS description_en{Environment.NewLine}",
                            $"FROM{Environment.NewLine}",
                            $"    $SchemaName.$Name(@idOrdering, @subPopulation, @etlId) AS $Name;{Environment.NewLine}");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="idOrdering"></param>
                    /// <param name="subPopulation"></param>
                    /// <param name="etlId"></param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        Nullable<int> idOrdering,
                        Nullable<int> subPopulation,
                        List<Nullable<int>> etlId)
                    {
                        string result = SQL;

                        result = result.Replace(
                           oldValue: "@idOrdering",
                           newValue: Functions.PrepNIntArg(arg: idOrdering));

                        result = result.Replace(
                            oldValue: "@subPopulation",
                            newValue: Functions.PrepNIntArg(arg: subPopulation));

                        result = result.Replace(
                            oldValue: "@etlId",
                            newValue: Functions.PrepNIntArrayArg(args: etlId, dbType: "int4"));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_etl_get_sub_population_categories4roller
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="idOrdering"></param>
                    /// <param name="subPopulation"></param>
                    /// <param name="etlId"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns></returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> idOrdering,
                        Nullable<int> subPopulation,
                        List<Nullable<int>> etlId,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(idOrdering: idOrdering, subPopulation: subPopulation, etlId: etlId);

                        NpgsqlParameter pIdOrdering = new(
                            parameterName: "idOrdering",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pSubPopulation = new(
                            parameterName: "subPopulation",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pEtlId = new(
                            parameterName: "etlId",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        if (idOrdering != null)
                        {
                            pIdOrdering.Value = (int)idOrdering;
                        }
                        else
                        {
                            pIdOrdering.Value = DBNull.Value;
                        }

                        if (subPopulation != null)
                        {
                            pSubPopulation.Value = (int)subPopulation;
                        }
                        else
                        {
                            pSubPopulation.Value = DBNull.Value;
                        }

                        if (etlId != null)
                        {
                            pEtlId.Value = etlId.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pEtlId.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteQuery(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pIdOrdering, pSubPopulation, pEtlId);
                    }

                    #endregion Methods

                }

                #endregion FnEtlGetSubPopulationCategoriesForRoller

            }
        }
    }
}
