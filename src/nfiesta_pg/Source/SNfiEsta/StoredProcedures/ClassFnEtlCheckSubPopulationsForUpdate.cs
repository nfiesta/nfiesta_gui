﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {

            /// <summary>
            /// Stored procedures of schema nfiesta
            /// </summary>
            public static partial class NfiEstaFunctions
            {
                // fn_etl_check_sub_populations4update

                #region FnEtlCheckSubPopulationsForUpdate

                /// <summary>
                /// Wrapper for stored procedure fn_etl_check_sub_populations4update.
                /// The function returns records of sub populations with informations
                /// whether their source labels or descriptions are different from target.
                /// </summary>
                public static class FnEtlCheckSubPopulationsForUpdate
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName = NfiEstaSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name = "fn_etl_check_sub_populations4update";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_metadatas json; ",
                            $"returns: ",
                            $"TABLE(",
                            $"sub_population integer, ",
                            $"label_source character varying, ",
                            $"description_source text, ",
                            $"label_en_source character varying, ",
                            $"description_en_source text, ",
                            $"label_target character varying, ",
                            $"description_target text, ",
                            $"label_en_target character varying, ",
                            $"description_en_target text, ",
                            $"check_label boolean, ",
                            $"check_description boolean, ",
                            $"check_label_en boolean, ",
                            $"check_description_en boolean)");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    $Name.sub_population::integer                 AS sub_population,{Environment.NewLine}",
                            $"    $Name.label_source::character varying         AS label_source,{Environment.NewLine}",
                            $"    $Name.description_source::text                AS description_source,{Environment.NewLine}",
                            $"    $Name.label_en_source::character varying      AS label_en_source,{Environment.NewLine}",
                            $"    $Name.description_en_source::text             AS description_en_source,{Environment.NewLine}",
                            $"    $Name.label_target::character varying         AS label_target,{Environment.NewLine}",
                            $"    $Name.description_target::text                AS description_target,{Environment.NewLine}",
                            $"    $Name.label_en_target::character varying      AS label_en_target,{Environment.NewLine}",
                            $"    $Name.description_en_target::text             AS description_en_target,{Environment.NewLine}",
                            $"    $Name.check_label::boolean                    AS check_label,{Environment.NewLine}",
                            $"    $Name.check_description::boolean              AS check_description,{Environment.NewLine}",
                            $"    $Name.check_label_en::boolean                 AS check_label_en,{Environment.NewLine}",
                            $"    $Name.check_description_en::boolean           AS check_description_en{Environment.NewLine}",
                            $"FROM{Environment.NewLine}",
                            $"    $SchemaName.$Name(@metadatas) AS $Name;{Environment.NewLine}");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="metadatas">Json</param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(string metadatas)
                    {
                        string result = SQL;

                        result = result.Replace(
                            oldValue: "@metadatas",
                            newValue: Functions.PrepStringArg(arg: metadatas));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_etl_check_sub_populations4update
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="metadatas">Json</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        string metadatas,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(metadatas: metadatas);

                        NpgsqlParameter pMetadatas = new(
                            parameterName: "metadatas",
                            parameterType: NpgsqlDbType.Json);


                        if (metadatas != null)
                        {
                            pMetadatas.Value = (string)metadatas;
                        }
                        else
                        {
                            pMetadatas.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteQuery(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pMetadatas);
                    }

                    #endregion Methods

                }

                #endregion FnEtlCheckSubPopulationsForUpdate

            }

        }
    }
}