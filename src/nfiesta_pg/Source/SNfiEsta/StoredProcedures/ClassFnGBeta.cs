﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {

            /// <summary>
            /// Stored procedures of schema nfiesta
            /// </summary>
            public static partial class NfiEstaFunctions
            {
                // fn_g_beta

                #region FnGBeta

                /// <summary>
                /// Wrapper for stored procedure fn_g_beta.
                /// Function computing matrix G_beta used for regression estimators.
                /// G_beta is dependent on model and parametrization domain (not cell).
                /// Matrix is represented by variable (row) and cluster (column) indices.
                /// </summary>
                public static class FnGBeta
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName = NfiEstaSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name = "fn_g_beta";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"conf_id integer; ",
                            $"returns: ",
                            $"TABLE(",
                            $"variable integer, ",
                            $"cluster integer, ",
                            $"val double precision)");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    $Name.variable    AS variable,{Environment.NewLine}",
                            $"    $Name.cluster     AS cluster,{Environment.NewLine}",
                            $"    $Name.val         AS val{Environment.NewLine}",
                            $"FROM{Environment.NewLine}",
                            $"    $SchemaName.$Name(@confId) AS $Name{Environment.NewLine};");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="confId"></param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(Nullable<int> confId)
                    {
                        string result = SQL;

                        result = result.Replace(
                            oldValue: "@confId",
                            newValue: Functions.PrepNIntArg(arg: confId));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_g_beta
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="confId"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns></returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> confId,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(confId: confId);

                        NpgsqlParameter pConfId = new(
                            parameterName: "confId",
                            parameterType: NpgsqlDbType.Integer);

                        if (confId != null)
                        {
                            pConfId.Value = (int)confId;
                        }
                        else
                        {
                            pConfId.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteQuery(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pConfId);
                    }

                    #endregion Methods

                }

                #endregion FnGBeta

            }

        }
    }
}