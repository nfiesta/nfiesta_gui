﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {

            /// <summary>
            /// Stored procedures of schema nfiesta
            /// </summary>
            public static partial class NfiEstaFunctions
            {
                // fn_etl_check_target_variable_metadatas

                #region FnEtlCheckTargetVariableMetadatas

                /// <summary>
                /// Wrapper for stored procedure fn_etl_check_target_variable_metadatas.
                /// Function returns records of target variables for given input arguments. If input argument _metadata_diff = true
                /// then function returns records of target variables that their metadatas are not different.If input argument
                ///_metadata_diff = false then function returns records of target variables that their metadatas are different.
                /// If input argument _metadata_diff is null (_metadata is true or false) then function returns both cases.
                /// </summary>
                public static class FnEtlCheckTargetVariableMetadatas
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName = NfiEstaSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name = "fn_etl_check_target_variable_metadatas";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_metadatas json, ",
                            $"_national_language character varying DEFAULT 'en'::character varying, ",
                            $"_metadata_diff boolean DEFAULT NULL::boolean; ",
                            $"returns: ",
                            $"TABLE(",
                            $"target_variable_source integer, ",
                            $"target_variable_target integer, ",
                            $"metadata_source json, ",
                            $"metadata_target json, ",
                            $"metadata_diff boolean, ",
                            $"metadata_diff_national_language boolean, ",
                            $"metadata_diff_english_language boolean, ",
                            $"missing_metadata_national_language boolean)");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    {FnEtlCheckTargetVariableMetadatasTypeList.ColTargetVariableSourceId.SQL(FnEtlCheckTargetVariableMetadatasTypeList.Cols, Name)}",
                            $"    {FnEtlCheckTargetVariableMetadatasTypeList.ColTargetVariableTargetId.SQL(FnEtlCheckTargetVariableMetadatasTypeList.Cols, Name)}",
                            $"    {FnEtlCheckTargetVariableMetadatasTypeList.ColMetadataSource.SQL(FnEtlCheckTargetVariableMetadatasTypeList.Cols, Name)}",
                            $"    {FnEtlCheckTargetVariableMetadatasTypeList.ColMetadataTarget.SQL(FnEtlCheckTargetVariableMetadatasTypeList.Cols, Name)}",
                            $"    {FnEtlCheckTargetVariableMetadatasTypeList.ColMetadataDiff.SQL(FnEtlCheckTargetVariableMetadatasTypeList.Cols, Name)}",
                            $"    {FnEtlCheckTargetVariableMetadatasTypeList.ColMetadataDiffNationalLanguage.SQL(FnEtlCheckTargetVariableMetadatasTypeList.Cols, Name)}",
                            $"    {FnEtlCheckTargetVariableMetadatasTypeList.ColMetadataDiffEnglishLanguage.SQL(FnEtlCheckTargetVariableMetadatasTypeList.Cols, Name)}",
                            $"    {FnEtlCheckTargetVariableMetadatasTypeList.ColMissingMetadataNationalLanguage.SQL(FnEtlCheckTargetVariableMetadatasTypeList.Cols, Name, isLastOne: true)}",
                            $"FROM{Environment.NewLine}",
                            $"    $SchemaName.$Name(@metadatas, @nationalLanguage, @metadataDiff) AS $Name;{Environment.NewLine}");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="metadatas"></param>
                    /// <param name="nationalLanguage"></param>
                    /// <param name="metadataDiff"></param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        string metadatas,
                        Language nationalLanguage = PostgreSQL.Language.CS,
                        Nullable<bool> metadataDiff = null)
                    {
                        string result = SQL;

                        CommandText = CommandText.Replace(
                           oldValue: "@metadatas",
                           newValue: Functions.PrepStringArg(arg: metadatas));

                        CommandText = CommandText.Replace(
                           oldValue: "@nationalLanguage",
                           newValue: Functions.PrepStringArg(
                               arg: LanguageList.ISO_639_1(language: nationalLanguage)));

                        CommandText = CommandText.Replace(
                            oldValue: "@metadataDiff",
                            newValue: Functions.PrepNBoolArg(arg: metadataDiff));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_etl_check_target_variable_metadatas
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="metadatas"></param>
                    /// <param name="nationalLanguage"></param>
                    /// <param name="metadataDiff"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns></returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        string metadatas,
                        Language nationalLanguage = PostgreSQL.Language.CS,
                        Nullable<bool> metadataDiff = null,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(
                            metadatas: metadatas,
                            nationalLanguage: nationalLanguage,
                            metadataDiff: metadataDiff);

                        NpgsqlParameter pMetadatas = new(
                            parameterName: "metadatas",
                            parameterType: NpgsqlDbType.Json);

                        NpgsqlParameter pNationalLanguage = new(
                           parameterName: "nationalLanguage",
                           parameterType: NpgsqlDbType.Varchar);

                        NpgsqlParameter pMetadataDiff = new(
                            parameterName: "metadataDiff",
                            parameterType: NpgsqlDbType.Boolean);

                        if (metadatas != null)
                        {
                            pMetadatas.Value = (string)metadatas;
                        }
                        else
                        {
                            pMetadatas.Value = DBNull.Value;
                        }

                        pNationalLanguage.Value =
                                LanguageList.ISO_639_1(language: nationalLanguage);

                        if (metadataDiff != null)
                        {
                            pMetadataDiff.Value = (bool)metadataDiff;
                        }
                        else
                        {
                            pMetadataDiff.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteQuery(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pMetadatas, pNationalLanguage, pMetadataDiff);
                    }

                    /// <summary>
                    /// Execute stored procedure fn_etl_check_target_variable_metadatas
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="metadatas"></param>
                    /// <param name="nationalLanguage"></param>
                    /// <param name="metadataDiff"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>List of target variable records</returns>
                    public static FnEtlCheckTargetVariableMetadatasTypeList Execute(
                        NfiEstaDB database,
                        string metadatas,
                        Language nationalLanguage = PostgreSQL.Language.CS,
                        Nullable<bool> metadataDiff = null,
                        NpgsqlTransaction transaction = null)
                    {
                        System.Diagnostics.Stopwatch stopWatch = new();
                        stopWatch.Start();

                        DataTable data = ExecuteQuery(
                            database: database,
                            metadatas: metadatas,
                            nationalLanguage: nationalLanguage,
                            metadataDiff: metadataDiff,
                            transaction: transaction);
                        data.TableName = Name;

                        stopWatch.Stop();

                        return new FnEtlCheckTargetVariableMetadatasTypeList(
                            database: database,
                            data: data,
                            loadingTime: 0.001 * stopWatch.ElapsedMilliseconds,
                            storedProcedure: CommandText)
                        {
                            Metadatas = metadatas,
                            NationalLanguage = nationalLanguage,
                            MetadataDiff = metadataDiff
                        };
                    }

                    #endregion Methods

                }

                #endregion FnEtlCheckTargetVariableMetadatas

            }

        }
    }
}