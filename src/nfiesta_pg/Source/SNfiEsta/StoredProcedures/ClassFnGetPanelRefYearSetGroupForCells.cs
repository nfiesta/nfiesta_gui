﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {

            /// <summary>
            /// Stored procedures of schema nfiesta
            /// </summary>
            public static partial class NfiEstaFunctions
            {
                // fn_get_panel_refyearset_group4cells

                #region FnGetPanelRefYearSetGroupForCells

                /// <summary>
                /// Wrapper for stored procedure fn_get_panel_refyearset_group4cells.
                /// Returns table with estimation cells and belonging panel_refyearset_group (if necessary, provides insert into lookup).
                /// </summary>
                public static class FnGetPanelRefYearSetGroupForCells
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName = NfiEstaSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name = "fn_get_panel_refyearset_group4cells";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_panels integer[], ",
                            $"_refyearsets integer[], ",
                            $"_estimation_cells integer[], ",
                            $"_label character varying DEFAULT NULL::character varying, ",
                            $"_description text DEFAULT NULL::text; ",
                            $"returns: ",
                            $"TABLE(",
                            $"estimation_cell integer, ",
                            $"panel_refyearset_group integer)");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    $Name.estimation_cell         AS estimation_cell,{Environment.NewLine}",
                            $"    $Name.panel_refyearset_group  AS panel_refyearset_group{Environment.NewLine}",
                            $"FROM{Environment.NewLine}",
                            $"    $SchemaName.$Name(@panels, @refYearSets, @estimationCells, @label, @description) ",
                            $"AS $Name;{Environment.NewLine}");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="panels"></param>
                    /// <param name="refYearSets"></param>
                    /// <param name="estimationCells"></param>
                    /// <param name="label"></param>
                    /// <param name="description"></param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        List<Nullable<int>> panels,
                        List<Nullable<int>> refYearSets,
                        List<Nullable<int>> estimationCells,
                        string label,
                        string description)
                    {
                        string result = SQL;

                        result = result.Replace(
                            oldValue: "@panels",
                            newValue: Functions.PrepNIntArrayArg(args: panels, dbType: "int4"));

                        result = result.Replace(
                            oldValue: "@refYearSets",
                            newValue: Functions.PrepNIntArrayArg(args: refYearSets, dbType: "int4"));

                        result = result.Replace(
                            oldValue: "@estimationCells",
                            newValue: Functions.PrepNIntArrayArg(args: estimationCells, dbType: "int4"));

                        result = result.Replace(
                            oldValue: "@label",
                            newValue: Functions.PrepStringArg(arg: label));

                        result = result.Replace(
                            oldValue: "@description",
                            newValue: Functions.PrepStringArg(arg: description));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_get_panel_refyearset_group4cells
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="panels"></param>
                    /// <param name="refYearSets"></param>
                    /// <param name="estimationCells"></param>
                    /// <param name="label"></param>
                    /// <param name="description"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns></returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        List<Nullable<int>> panels,
                        List<Nullable<int>> refYearSets,
                        List<Nullable<int>> estimationCells,
                        string label,
                        string description,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(
                            panels: panels,
                            refYearSets: refYearSets,
                            estimationCells: estimationCells,
                            label: label,
                            description: description);

                        NpgsqlParameter pPanels = new(
                            parameterName: "panels",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        NpgsqlParameter pRefYearSets = new(
                            parameterName: "refYearSets",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        NpgsqlParameter pEstimationCells = new(
                            parameterName: "estimationCells",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        NpgsqlParameter pLabel = new(
                            parameterName: "label",
                            parameterType: NpgsqlDbType.Varchar);

                        NpgsqlParameter pDescription = new(
                            parameterName: "description",
                            parameterType: NpgsqlDbType.Text);

                        if (panels != null)
                        {
                            pPanels.Value = panels.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pPanels.Value = DBNull.Value;
                        }

                        if (refYearSets != null)
                        {
                            pRefYearSets.Value = refYearSets.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pRefYearSets.Value = DBNull.Value;
                        }

                        if (estimationCells != null)
                        {
                            pEstimationCells.Value = estimationCells.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pEstimationCells.Value = DBNull.Value;
                        }

                        if (label != null)
                        {
                            pLabel.Value = (string)label;
                        }
                        else
                        {
                            pLabel.Value = DBNull.Value;
                        }

                        if (description != null)
                        {
                            pDescription.Value = (string)description;
                        }
                        else
                        {
                            pDescription.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteQuery(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pPanels, pRefYearSets, pEstimationCells, pLabel, pDescription);
                    }

                    #endregion Methods

                }

                #endregion FnGetPanelRefYearSetGroupForCells

            }

        }
    }
}