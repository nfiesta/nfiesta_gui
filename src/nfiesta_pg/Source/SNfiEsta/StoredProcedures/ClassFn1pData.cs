﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {

            /// <summary>
            /// Stored procedures of schema nfiesta
            /// </summary>
            public static partial class NfiEstaFunctions
            {
                // fn_1p_data

                #region Fn1pData

                /// <summary>
                /// Wrapper for stored procedure fn_1p_data.
                /// Function preparing data for regression estimate.
                /// </summary>
                public static class Fn1pData
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = NfiEstaSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_1p_data";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"conf_id integer; ",
                            $"returns: ",
                            $"TABLE(",
                            $"gid bigint, ",
                            $"cluster integer, ",
                            $"attribute integer, ",
                            $"stratum integer, ",
                            $"plots_per_cluster integer, ",
                            $"plcount bigint, ",
                            $"cluster_is_in_cell boolean, ",
                            $"ldsity_d double precision, ",
                            $"ldsity_d_plus double precision, ",
                            $"is_aux boolean, ",
                            $"is_target boolean, ",
                            $"geom geometry, ",
                            $"ldsity_res_d double precision, ",
                            $"ldsity_res_d_plus double precision, ",
                            $"pix double precision, ",
                            $"sweight double precision, ",
                            $"delta_t__g_beta double precision, ",
                            $"nb_sampling_units integer, ",
                            $"sweight_strata_sum double precision, ",
                            $"lambda_d_plus double precision)");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    $Name.gid                AS gid,{Environment.NewLine}",
                            $"    $Name.cluster            AS cluster,{Environment.NewLine}",
                            $"    $Name.attribute          AS attribute,{Environment.NewLine}",
                            $"    $Name.stratum            AS stratum,{Environment.NewLine}",
                            $"    $Name.plots_per_cluster  AS plots_per_cluster,{Environment.NewLine}",
                            $"    $Name.plcount            AS plcount,{Environment.NewLine}",
                            $"    $Name.cluster_is_in_cell AS cluster_is_in_cell,{Environment.NewLine}",
                            $"    $Name.ldsity_d           AS ldsity_d,{Environment.NewLine}",
                            $"    $Name.ldsity_d_plus      AS ldsity_d_plus,{Environment.NewLine}",
                            $"    $Name.is_aux             AS is_aux,{Environment.NewLine}",
                            $"    $Name.is_target          AS is_target,{Environment.NewLine}",
                            $"    $Name.geom               AS geom,{Environment.NewLine}",
                            $"    $Name.ldsity_res_d       AS ldsity_res_d,{Environment.NewLine}",
                            $"    $Name.ldsity_res_d_plus  AS ldsity_res_d_plus,{Environment.NewLine}",
                            $"    $Name.pix                AS pix,{Environment.NewLine}",
                            $"    $Name.sweight            AS sweight,{Environment.NewLine}",
                            $"    $Name.delta_t__g_beta    AS delta_t__g_beta,{Environment.NewLine}",
                            $"    $Name.nb_sampling_units  AS nb_sampling_units,{Environment.NewLine}",
                            $"    $Name.sweight_strata_sum AS sweight_strata_sum,{Environment.NewLine}",
                            $"    $Name.lambda_d_plus      AS lambda_d_plus{Environment.NewLine}",
                            $"FROM{Environment.NewLine}",
                            $"    $SchemaName.$Name(@confId) AS $Name;");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="confId"></param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(Nullable<int> confId)
                    {
                        string result = SQL;

                        result = result.Replace(
                            oldValue: "@confId",
                            newValue: Functions.PrepNIntArg(arg: confId));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_1p_data
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="confId"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns></returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> confId,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(confId: confId);

                        NpgsqlParameter pConfId = new(
                            parameterName: "confId",
                            parameterType: NpgsqlDbType.Integer);

                        if (confId != null)
                        {
                            pConfId.Value = (int)confId;
                        }
                        else
                        {
                            pConfId.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteQuery(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pConfId);
                    }

                    #endregion Methods

                }

                #endregion Fn1pData

            }

        }
    }
}