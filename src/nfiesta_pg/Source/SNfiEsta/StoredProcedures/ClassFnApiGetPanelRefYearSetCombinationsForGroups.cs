﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {

            /// <summary>
            /// Stored procedures of schema nfiesta
            /// </summary>
            public static partial class NfiEstaFunctions
            {
                // fn_api_get_panel_refyearset_combinations4groups

                #region FnApiGetPanelRefYearSetCombinationsForGroups

                /// <summary>
                /// Wrapper for stored procedure fn_api_get_panel_refyearset_combinations4groups.
                /// The function returns equaly ordered arrays of panels and reference-year sets belonging
                /// to any group of panels and reference year combinations passed to the function as an argument.
                /// Only distinct pairs of panel and reference year sets are returned.
                /// </summary>
                public static class FnApiGetPanelRefYearSetCombinationsForGroups
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName = NfiEstaSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name = "fn_api_get_panel_refyearset_combinations4groups";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_panel_refyearset_groups integer[]; ",
                            $"returns: ",
                            $"TABLE(",
                            $"panel integer, ",
                            $"panel_label character varying, ",
                            $"panel_label_en character varying, ",
                            $"panel_description text, ",
                            $"panel_description_en text, ",
                            $"reference_year_set integer, ",
                            $"refyearset_label character varying, ",
                            $"refyearset_label_en character varying, ",
                            $"refyearset_description text, ",
                            $"refyearset_description_en text, ",
                            $"cluster_count integer, ",
                            $"plot_count integer)");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                String.Concat(
                                    $"SELECT{Environment.NewLine}",
                                    $"    {TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColId.SQL(cols: TFnApiGetPanelRefYearSetCombinationsForGroupsList.Cols, alias: Name)}",
                                    $"    {TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColPanelId.SQL(cols: TFnApiGetPanelRefYearSetCombinationsForGroupsList.Cols, alias: Name)}",
                                    $"    {TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColPanelLabelCs.SQL(cols: TFnApiGetPanelRefYearSetCombinationsForGroupsList.Cols, alias: Name)}",
                                    $"    {TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColPanelDescriptionCs.SQL(cols: TFnApiGetPanelRefYearSetCombinationsForGroupsList.Cols, alias: Name)}",
                                    $"    {TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColPanelLabelEn.SQL(cols: TFnApiGetPanelRefYearSetCombinationsForGroupsList.Cols, alias: Name)}",
                                    $"    {TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColPanelDescriptionEn.SQL(cols: TFnApiGetPanelRefYearSetCombinationsForGroupsList.Cols, alias: Name)}",
                                    $"    {TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColReferenceYearSetId.SQL(cols: TFnApiGetPanelRefYearSetCombinationsForGroupsList.Cols, alias: Name)}",
                                    $"    {TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColReferenceYearSetLabelCs.SQL(cols: TFnApiGetPanelRefYearSetCombinationsForGroupsList.Cols, alias: Name)}",
                                    $"    {TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColReferenceYearSetDescriptionCs.SQL(cols: TFnApiGetPanelRefYearSetCombinationsForGroupsList.Cols, alias: Name)}",
                                    $"    {TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColReferenceYearSetLabelEn.SQL(cols: TFnApiGetPanelRefYearSetCombinationsForGroupsList.Cols, alias: Name)}",
                                    $"    {TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColReferenceYearSetDescriptionEn.SQL(cols: TFnApiGetPanelRefYearSetCombinationsForGroupsList.Cols, alias: Name)}",
                                    $"    {TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColClusterCount.SQL(cols: TFnApiGetPanelRefYearSetCombinationsForGroupsList.Cols, alias: Name)}",
                                    $"    {TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColPlotCount.SQL(cols: TFnApiGetPanelRefYearSetCombinationsForGroupsList.Cols, alias: Name, isLastOne: true)}",
                                    $"FROM{Environment.NewLine}",
                                    $"    {SchemaName}.{Name}(@panelRefYearSetGroups){Environment.NewLine}",
                                    $"ORDER BY{Environment.NewLine}",
                                    $"    {TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColReferenceYearSetId.DbName},{Environment.NewLine}",
                                    $"    {TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColPanelId.DbName};{Environment.NewLine}");
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="panelRefYearSetGroups">List of panel reference year set group identifiers</param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(List<Nullable<int>> panelRefYearSetGroups)
                    {
                        string result = SQL;

                        result = result.Replace(
                            oldValue: "@panelRefYearSetGroups",
                            newValue: Functions.PrepNIntArrayArg(args: panelRefYearSetGroups, dbType: "int4"));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_api_get_panel_refyearset_combinations4groups
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="panelRefYearSetGroups">List of panel reference year set group identifiers</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>
                    /// Data table of equaly ordered arrays of panels and reference-year sets belonging
                    /// to any group of panels and reference year combinations
                    /// </returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        List<Nullable<int>> panelRefYearSetGroups,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(panelRefYearSetGroups: panelRefYearSetGroups);

                        NpgsqlParameter pPanelRefYearSetGroups = new(
                            parameterName: "panelRefYearSetGroups",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        if (panelRefYearSetGroups != null)
                        {
                            pPanelRefYearSetGroups.Value = panelRefYearSetGroups.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pPanelRefYearSetGroups.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteQuery(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pPanelRefYearSetGroups);
                    }

                    /// <summary>
                    /// Execute stored procedure fn_api_get_panel_refyearset_combinations4groups
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="panelRefYearSetGroups">List of panel reference year set group identifiers</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>
                    /// List of equaly ordered arrays of panels and reference-year sets belonging
                    /// to any group of panels and reference year combinations
                    /// </returns>
                    public static TFnApiGetPanelRefYearSetCombinationsForGroupsList Execute(
                        NfiEstaDB database,
                        List<Nullable<int>> panelRefYearSetGroups,
                        NpgsqlTransaction transaction = null)
                    {
                        System.Diagnostics.Stopwatch stopWatch = new();
                        stopWatch.Start();

                        DataTable data = ExecuteQuery(
                            database: database,
                            panelRefYearSetGroups: panelRefYearSetGroups,
                            transaction: transaction);
                        data.TableName = Name;

                        stopWatch.Stop();

                        return new TFnApiGetPanelRefYearSetCombinationsForGroupsList(
                            database: database,
                            data: data,
                            loadingTime: 0.001 * stopWatch.ElapsedMilliseconds,
                            storedProcedure: CommandText)
                        {
                            PanelRefYearSetGroupsIds = panelRefYearSetGroups,
                        };
                    }

                    #endregion Methods

                }

                #endregion FnApiGetPanelRefYearSetCombinationsForGroups

            }

        }
    }
}