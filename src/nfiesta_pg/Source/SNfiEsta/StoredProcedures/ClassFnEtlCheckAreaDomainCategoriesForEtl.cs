﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {

            /// <summary>
            /// Stored procedures of schema nfiesta
            /// </summary>
            public static partial class NfiEstaFunctions
            {
                // fn_etl_check_area_domain_categories4etl

                #region FnEtlCheckAreaDomainCategoriesForEtl

                /// <summary>
                /// Wrapper for stored procedure fn_etl_check_area_domain_categories4etl.
                /// The function returns records of area domain categories for given input area domain with information if input label of area domain category was paired in target DB or not.
                /// </summary>
                public static class FnEtlCheckAreaDomainCategoriesForEtl
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName = NfiEstaSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name = "fn_etl_check_area_domain_categories4etl";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_area_domain integer, ",
                            $"_area_domain_category json; ",
                            $"returns: ",
                            $"TABLE(",
                            $"id integer, ",
                            $"area_domain_category integer[], ",
                            $"etl_id integer, ",
                            $"label character varying, ",
                            $"description text, ",
                            $"label_en character varying, ",
                            $"description_en text, ",
                            $"check_label boolean)");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    $Name.id                                                      AS id,{Environment.NewLine}",
                            $"    array_to_string({Name}.area_domain_category, ';', 'NULL')     AS area_domain_category,{Environment.NewLine}",
                            $"    $Name.etl_id                                                  AS etl_id,{Environment.NewLine}",
                            $"    $Name.label                                                   AS label,{Environment.NewLine}",
                            $"    $Name.description                                             AS description,{Environment.NewLine}",
                            $"    $Name.label_en                                                AS label_en,{Environment.NewLine}",
                            $"    $Name.description_en                                          AS description_en,{Environment.NewLine}",
                            $"    $Name.check_label                                             AS check_label{Environment.NewLine}",
                            $"FROM{Environment.NewLine}",
                            $"    $SchemaName.$Name(@areaDomain, @areaDomainCategory) AS $Name;{Environment.NewLine}");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="areaDomain"></param>
                    /// <param name="areaDomainCategory"></param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        Nullable<int> areaDomain,
                        string areaDomainCategory)
                    {
                        string result = SQL;

                        result = result.Replace(
                            oldValue: "@areaDomain",
                            newValue: Functions.PrepNIntArg(arg: areaDomain));

                        result = result.Replace(
                            oldValue: "@areaDomainCategory",
                            newValue: Functions.PrepStringArg(arg: areaDomainCategory));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_etl_check_area_domain_categories4etl
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="areaDomain"></param>
                    /// <param name="areaDomainCategory"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns></returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> areaDomain,
                        string areaDomainCategory,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(areaDomain: areaDomain, areaDomainCategory: areaDomainCategory);

                        NpgsqlParameter pAreaDomain = new(
                            parameterName: "areaDomain",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pAreaDomainCategory = new(
                            parameterName: "areaDomainCategory",
                            parameterType: NpgsqlDbType.Json);

                        if (areaDomain != null)
                        {
                            pAreaDomain.Value = (int)areaDomain;
                        }
                        else
                        {
                            pAreaDomain.Value = DBNull.Value;
                        }

                        if (areaDomainCategory != null)
                        {
                            pAreaDomainCategory.Value = (string)areaDomainCategory;
                        }
                        else
                        {
                            pAreaDomainCategory.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteQuery(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pAreaDomain, pAreaDomainCategory);
                    }

                    #endregion Methods

                }

                #endregion FnEtlCheckAreaDomainCategoriesForEtl

            }
        }
    }
}

