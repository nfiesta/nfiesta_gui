﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {

            /// <summary>
            /// Stored procedures of schema nfiesta
            /// </summary>
            public static partial class NfiEstaFunctions
            {
                // fn_make_estimate_table

                #region FnMakeEstimateTable

                /// <summary>
                /// Wrapper for stored procedure fn_make_estimate_table.
                /// Wrapper function for calculation of estimate irrespective of the estimate type.
                /// </summary>
                public static class FnMakeEstimateTable
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName = NfiEstaSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name = "fn_make_estimate_table";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_estimate_conf integer; ",
                            $"returns: ",
                            $"TABLE(",
                            $"estimate_conf integer, ",
                            $"point double precision, ",
                            $"var double precision, ",
                            $"min_ssize double precision, ",
                            $"act_ssize bigint, ",
                            $"version text, ",
                            $"calc_started timestamp with time zone, ",
                            $"calc_duration interval, ",
                            $"est_info json)");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    $Name.estimate_conf       AS estimate_conf,{Environment.NewLine}",
                            $"    $Name.point               AS point,{Environment.NewLine}",
                            $"    $Name.var                 AS var,{Environment.NewLine}",
                            $"    $Name.min_ssize           AS min_ssize,{Environment.NewLine}",
                            $"    $Name.act_ssize           AS act_ssize,{Environment.NewLine}",
                            $"    $Name.version             AS version,{Environment.NewLine}",
                            $"    $Name.calc_started::text  AS calc_started,{Environment.NewLine}",
                            $"    $Name.calc_duration::text AS calc_duration,{Environment.NewLine}",
                            $"    $Name.est_info::text      AS est_info{Environment.NewLine}",
                            $"FROM{Environment.NewLine}",
                            $"    $SchemaName.$Name(@estimateConf) AS $Name;{Environment.NewLine}");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="estimateConf"></param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(Nullable<int> estimateConf)
                    {
                        string result = SQL;

                        result = result.Replace(
                            oldValue: "@estimateConf",
                            newValue: Functions.PrepNIntArg(arg: estimateConf));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_make_estimate_table
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="estimateConf"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns></returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> estimateConf,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(estimateConf: estimateConf);

                        NpgsqlParameter pEstimateConf = new(
                            parameterName: "estimateConf",
                            parameterType: NpgsqlDbType.Integer);

                        if (estimateConf != null)
                        {
                            pEstimateConf.Value = (int)estimateConf;
                        }
                        else
                        {
                            pEstimateConf.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteQuery(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pEstimateConf);
                    }

                    #endregion Methods

                }

                #endregion FnMakeEstimateTable

            }

        }
    }
}