﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {

            /// <summary>
            /// Stored procedures of schema nfiesta
            /// </summary>
            public static partial class NfiEstaFunctions
            {
                // fn_get_panels_in_estimation_cells

                #region FnGetPanelsInEstimationCells

                /// <summary>
                /// Wrapper for stored procedure fn_get_panels_in_estimation_cells.
                /// Function returns list of stratas and panels
                /// in the estimation cell. For each panel there is an indicator (reference_year_set_fit)
                /// if the panel lies within estimation period and indicator max
                /// which tells if the panel is the one with the maximum possible plots.
                /// </summary>
                public static class FnGetPanelsInEstimationCells
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName = NfiEstaSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name = "fn_get_panels_in_estimation_cells";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_estimation_cells integer[], ",
                            $"_estimate_date_begin date, ",
                            $"_estimate_date_end date, ",
                            $"_target_variable integer; ",
                            $"returns: ",
                            $"TABLE(",
                            $"stratum integer, ",
                            $"stratum_label character varying, ",
                            $"panel integer, ",
                            $"panel_label character varying, ",
                            $"panel_subset integer, ",
                            $"reference_year_set integer, ",
                            $"reference_year_set_label character varying, ",
                            $"reference_date_begin date, ",
                            $"reference_date_end date, ",
                            $"reference_year_set_fit boolean, ",
                            $"portion_of_panel_inside double precision, ",
                            $"portion_of_period_covered double precision, ",
                            $"total integer, ",
                            $"is_max boolean)");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return String.Concat(
                                $"SELECT{Environment.NewLine}",
                                $"    {TFnGetPanelsInEstimationCellsList.ColId.SQL(cols: TFnGetPanelsInEstimationCellsList.Cols, alias: Name)}",
                                $"    {TFnGetPanelsInEstimationCellsList.ColStratumId.SQL(cols: TFnGetPanelsInEstimationCellsList.Cols, alias: Name)}",
                                $"    {TFnGetPanelsInEstimationCellsList.ColStratumLabel.SQL(cols: TFnGetPanelsInEstimationCellsList.Cols, alias: Name)}",
                                $"    {TFnGetPanelsInEstimationCellsList.ColPanelId.SQL(cols: TFnGetPanelsInEstimationCellsList.Cols, alias: Name)}",
                                $"    {TFnGetPanelsInEstimationCellsList.ColPanelLabel.SQL(cols: TFnGetPanelsInEstimationCellsList.Cols, alias: Name)}",
                                $"    {TFnGetPanelsInEstimationCellsList.ColPanelSubsetId.SQL(cols: TFnGetPanelsInEstimationCellsList.Cols, alias: Name)}",
                                $"    {TFnGetPanelsInEstimationCellsList.ColReferenceYearSetId.SQL(cols: TFnGetPanelsInEstimationCellsList.Cols, alias: Name)}",
                                $"    {TFnGetPanelsInEstimationCellsList.ColReferenceYearSetLabel.SQL(cols: TFnGetPanelsInEstimationCellsList.Cols, alias: Name)}",
                                $"    {TFnGetPanelsInEstimationCellsList.ColReferenceDateBegin.SQL(cols: TFnGetPanelsInEstimationCellsList.Cols, alias: Name)}",
                                $"    {TFnGetPanelsInEstimationCellsList.ColReferenceDateEnd.SQL(cols: TFnGetPanelsInEstimationCellsList.Cols, alias: Name)}",
                                $"    {TFnGetPanelsInEstimationCellsList.ColReferenceYearSetFit.SQL(cols: TFnGetPanelsInEstimationCellsList.Cols, alias: Name)}",
                                $"    {TFnGetPanelsInEstimationCellsList.ColPortionOfPanelInside.SQL(cols: TFnGetPanelsInEstimationCellsList.Cols, alias: Name)}",
                                $"    {TFnGetPanelsInEstimationCellsList.ColPortionOfPeriodCovered.SQL(cols: TFnGetPanelsInEstimationCellsList.Cols, alias: Name)}",
                                $"    {TFnGetPanelsInEstimationCellsList.ColTotal.SQL(cols: TFnGetPanelsInEstimationCellsList.Cols, alias: Name)}",
                                $"    {TFnGetPanelsInEstimationCellsList.ColIsMax.SQL(cols: TFnGetPanelsInEstimationCellsList.Cols, alias: Name, isLastOne: true)}",
                                $"FROM{Environment.NewLine}",
                                $"    {SchemaName}.{Name}(@estimationCells, @estimateDateBegin, @estimateDateEnd, @targetVariable) AS {Name} {Environment.NewLine}",
                                $"ORDER BY{Environment.NewLine}",
                                $"    {Name}.{TFnGetPanelsInEstimationCellsList.ColStratumId.DbName},{Environment.NewLine}",
                                $"    {Name}.{TFnGetPanelsInEstimationCellsList.ColPanelId.DbName},{Environment.NewLine}",
                                $"    {Name}.{TFnGetPanelsInEstimationCellsList.ColReferenceYearSetId.DbName};{Environment.NewLine}"
                            );
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="estimationCellIds">List of estimation cells identifiers</param>
                    /// <param name="estimateDateBegin">The beginning of the time to witch the estimate is calculated</param>
                    /// <param name="estimateDateEnd">The end of the time to witch the estimate is calculated</param>
                    /// <param name="variableId">Attribute category identifier</param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        List<Nullable<int>> estimationCellIds,
                        Nullable<DateTime> estimateDateBegin,
                        Nullable<DateTime> estimateDateEnd,
                        Nullable<int> variableId)
                    {
                        string result = SQL;

                        result = result.Replace(
                            oldValue: "@estimationCells",
                            newValue: Functions.PrepNIntArrayArg(args: estimationCellIds, dbType: "int4"));

                        result = result.Replace(
                            oldValue: "@estimateDateBegin",
                            newValue: Functions.PrepNDateTimeArg(arg: estimateDateBegin));

                        result = result.Replace(
                            oldValue: "@estimateDateEnd",
                            newValue: Functions.PrepNDateTimeArg(arg: estimateDateEnd));

                        result = result.Replace(
                            oldValue: "@targetVariable",
                            newValue: Functions.PrepNIntArg(arg: variableId));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_get_panels_in_estimation_cells
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="estimationCellIds">List of estimation cells identifiers</param>
                    /// <param name="estimateDateBegin">The beginning of the time to witch the estimate is calculated</param>
                    /// <param name="estimateDateEnd">The end of the time to witch the estimate is calculated</param>
                    /// <param name="variableId">Attribute category identifier</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table with panels in estimation cell</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        List<Nullable<int>> estimationCellIds,
                        Nullable<DateTime> estimateDateBegin,
                        Nullable<DateTime> estimateDateEnd,
                        Nullable<int> variableId,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(
                            estimationCellIds: estimationCellIds,
                            estimateDateBegin: estimateDateBegin,
                            estimateDateEnd: estimateDateEnd,
                            variableId: variableId);

                        NpgsqlParameter pEstimationCellIds = new(
                            parameterName: "estimationCells",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        NpgsqlParameter pEstimateDateBegin = new(
                            parameterName: "estimateDateBegin",
                            parameterType: NpgsqlDbType.Date);

                        NpgsqlParameter pEstimateDateEnd = new(
                            parameterName: "estimateDateEnd",
                            parameterType: NpgsqlDbType.Date);

                        NpgsqlParameter pVariableId = new(
                            parameterName: "targetVariable",
                            parameterType: NpgsqlDbType.Integer);

                        if (estimationCellIds != null)
                        {
                            pEstimationCellIds.Value = estimationCellIds.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pEstimationCellIds.Value = DBNull.Value;
                        }

                        if (estimateDateBegin != null)
                        {
                            pEstimateDateBegin.Value = estimateDateBegin;
                        }
                        else
                        {
                            pEstimateDateBegin.Value = DBNull.Value;
                        }

                        if (estimateDateEnd != null)
                        {
                            pEstimateDateEnd.Value = estimateDateEnd;
                        }
                        else
                        {
                            pEstimateDateEnd.Value = DBNull.Value;
                        }

                        if (variableId != null)
                        {
                            pVariableId.Value = (int)variableId;
                        }
                        else
                        {
                            pVariableId.Value = DBNull.Value;
                        }

                        return
                            database.Postgres.ExecuteQuery(
                                sqlCommand: SQL,
                                transaction: transaction,
                                pEstimationCellIds, pEstimateDateBegin, pEstimateDateEnd, pVariableId);
                    }

                    /// <summary>
                    /// Execute stored procedure fn_get_panels_in_estimation_cells
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="estimationCellIds">List of estimation cells identifiers</param>
                    /// <param name="estimateDateBegin">The beginning of the time to witch the estimate is calculated</param>
                    /// <param name="estimateDateEnd">The end of the time to witch the estimate is calculated</param>
                    /// <param name="variableId">Attribute category identifier</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>List of panels in estimation cell</returns>
                    public static TFnGetPanelsInEstimationCellsList Execute(
                        NfiEstaDB database,
                        List<Nullable<int>> estimationCellIds,
                        Nullable<DateTime> estimateDateBegin,
                        Nullable<DateTime> estimateDateEnd,
                        Nullable<int> variableId,
                        NpgsqlTransaction transaction = null)
                    {
                        System.Diagnostics.Stopwatch stopWatch = new();
                        stopWatch.Start();

                        DataTable data = ExecuteQuery(
                            database: database,
                            estimationCellIds: estimationCellIds,
                            estimateDateBegin: estimateDateBegin,
                            estimateDateEnd: estimateDateEnd,
                            variableId: variableId,
                            transaction: transaction);
                        data.TableName = Name;

                        stopWatch.Stop();

                        return new TFnGetPanelsInEstimationCellsList(
                            database: database,
                            data: data,
                            loadingTime: 0.001 * stopWatch.ElapsedMilliseconds,
                            storedProcedure: CommandText)
                        {
                            EstimationCellIdList = estimationCellIds,
                            EstimateDateBegin = estimateDateBegin,
                            EstimateDateEnd = estimateDateEnd,
                            VariableId = variableId
                        };
                    }

                    #endregion Methods

                }

                #endregion FnGetPanelsInEstimationCells

            }

        }
    }
}