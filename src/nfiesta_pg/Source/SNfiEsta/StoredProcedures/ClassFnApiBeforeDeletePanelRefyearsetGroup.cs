﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {

            /// <summary>
            /// Stored procedures of schema nfiesta
            /// </summary>
            public static partial class NfiEstaFunctions
            {
                // fn_api_before_delete_panel_refyearset_group

                #region FnApiBeforeDeletePanelRefYearSetGroup

                /// <summary>
                /// Wrapper for stored procedure fn_api_before_delete_panel_refyearset_group.
                /// The function controls if there is a row in tables nfiesta.t_aux_conf and nfiesta.t_total_estimate_conf,
                /// where panel_refyearset_group = _id passed as an argument.
                /// If at least one result column is TRUE, the row with id = _id in table nfiesta.c_panel_refyearset_group cannot be deleted.
                /// </summary>
                public static class FnApiBeforeDeletePanelRefYearSetGroup
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = NfiEstaSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_api_before_delete_panel_refyearset_group";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_id integer, ",
                            $"OUT _t_aux_conf_exi boolean, ",
                            $"OUT _t_total_estimate_conf_exi boolean; ",
                            $"returns: record");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                String.Concat(
                                    $"SELECT{Environment.NewLine}",
                                    $"    {TFnApiBeforeDeletePanelRefYearSetGroupList.ColId.SQL(cols: TFnApiBeforeDeletePanelRefYearSetGroupList.Cols, alias: Name)}",
                                    $"    {TFnApiBeforeDeletePanelRefYearSetGroupList.ColAuxConfExist.SQL(cols: TFnApiBeforeDeletePanelRefYearSetGroupList.Cols, alias: Name)}",
                                    $"    {TFnApiBeforeDeletePanelRefYearSetGroupList.ColTotalEstimateConfExist.SQL(cols: TFnApiBeforeDeletePanelRefYearSetGroupList.Cols, alias: Name, isLastOne: true)}",
                                    $"FROM{Environment.NewLine}",
                                    $"    {SchemaName}.{Name}(@panelRefYearSetGroupId);{Environment.NewLine}");
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="panelRefYearSetGroupId">Panel reference year set group identifier</param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(Nullable<int> panelRefYearSetGroupId)
                    {
                        string result = SQL;

                        result = result.Replace(
                           oldValue: "@panelRefYearSetGroupId",
                           newValue: Functions.PrepNIntArg(arg: panelRefYearSetGroupId));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_api_before_delete_panel_refyearset_group
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="panelRefYearSetGroupId">Panel reference year set group identifier</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table of controls if there is a row in tables nfiesta.t_aux_conf and nfiesta.t_total_estimate_conf</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> panelRefYearSetGroupId,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(panelRefYearSetGroupId: panelRefYearSetGroupId);

                        NpgsqlParameter pPanelRefYearSetGroupId = new(
                            parameterName: "panelRefYearSetGroupId",
                            parameterType: NpgsqlDbType.Integer);

                        if (panelRefYearSetGroupId != null)
                        {
                            pPanelRefYearSetGroupId.Value = (int)panelRefYearSetGroupId;
                        }
                        else
                        {
                            pPanelRefYearSetGroupId.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteQuery(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pPanelRefYearSetGroupId);
                    }

                    /// <summary>
                    /// Execute stored procedure fn_api_before_delete_panel_refyearset_group
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="panelRefYearSetGroupId">Panel reference year set group identifier</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>List of controls if there is a row in tables nfiesta.t_aux_conf and nfiesta.t_total_estimate_conf</returns>
                    public static TFnApiBeforeDeletePanelRefYearSetGroupList Execute(
                        NfiEstaDB database,
                        Nullable<int> panelRefYearSetGroupId,
                        NpgsqlTransaction transaction = null)
                    {
                        System.Diagnostics.Stopwatch stopWatch = new();
                        stopWatch.Start();

                        DataTable data = ExecuteQuery(
                            database: database,
                            panelRefYearSetGroupId: panelRefYearSetGroupId,
                            transaction: transaction);
                        data.TableName = Name;

                        stopWatch.Stop();

                        return new TFnApiBeforeDeletePanelRefYearSetGroupList(
                            database: database,
                            data: data,
                            loadingTime: 0.001 * stopWatch.ElapsedMilliseconds,
                            storedProcedure: CommandText)
                        {
                            PanelRefYearSetGroupId = panelRefYearSetGroupId
                        };
                    }
                    #endregion Methods

                }

                #endregion FnApiBeforeDeletePanelRefYearSetGroup

            }

        }
    }
}