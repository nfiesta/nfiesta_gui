﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {

            /// <summary>
            /// Stored procedures of schema nfiesta
            /// </summary>
            public static partial class NfiEstaFunctions
            {
                // fn_2p_est_configuration

                #region Fn2pEstConfiguration

                /// <summary>
                /// Wrapper for stored procedure fn_2p_est_configuration.
                /// Function makes the estimate configuration present in the database - provides inserts into the all necessary tables.
                /// </summary>
                public static class Fn2pEstConfiguration
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = NfiEstaSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_2p_est_configuration";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_estimation_cell integer, ",
                            $"_estimation_period integer, ",
                            $"_note character varying, ",
                            $"_target_variable integer, ",
                            $"_aux_conf integer, ",
                            $"_force_synthetic boolean DEFAULT false; ",
                            $"returns: integer");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    $SchemaName.$Name(",
                            $"@estimationCell, ",
                            $"@estimationPeriod, ",
                            $"@note, ",
                            $"@targetVariable, ",
                            $"@auxConf, ",
                            $"@forceSynthetic) AS $Name;");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="estimationCell"></param>
                    /// <param name="estimationPeriod"></param>
                    /// <param name="note"></param>
                    /// <param name="targetVariable"></param>
                    /// <param name="auxConf"></param>
                    /// <param name="forceSynthetic"></param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        Nullable<int> estimationCell,
                        Nullable<int> estimationPeriod,
                        string note,
                        Nullable<int> targetVariable,
                        Nullable<int> auxConf,
                        Nullable<bool> forceSynthetic)
                    {
                        string result = SQL;

                        result = result.Replace(
                            oldValue: "@estimationCell",
                            newValue: Functions.PrepNIntArg(arg: estimationCell));

                        result = result.Replace(
                           oldValue: "@estimationPeriod",
                           newValue: Functions.PrepNIntArg(arg: estimationPeriod));

                        result = result.Replace(
                           oldValue: "@note",
                           newValue: Functions.PrepStringArg(arg: note));

                        result = result.Replace(
                           oldValue: "@targetVariable",
                           newValue: Functions.PrepNIntArg(arg: targetVariable));

                        result = result.Replace(
                          oldValue: "@auxConf",
                          newValue: Functions.PrepNIntArg(arg: auxConf));

                        result = result.Replace(
                         oldValue: "@forceSynthetic",
                         newValue: Functions.PrepNBoolArg(arg: forceSynthetic));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_2p_est_configuration
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="estimationCell"></param>
                    /// <param name="estimationPeriod"></param>
                    /// <param name="note"></param>
                    /// <param name="targetVariable"></param>
                    /// <param name="auxConf"></param>
                    /// <param name="forceSynthetic"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns></returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> estimationCell,
                        Nullable<int> estimationPeriod,
                        string note,
                        Nullable<int> targetVariable,
                        Nullable<int> auxConf,
                        Nullable<bool> forceSynthetic,
                        NpgsqlTransaction transaction = null)
                    {
                        Nullable<int> val = Execute(
                            database: database,
                            estimationCell: estimationCell,
                            estimationPeriod: estimationPeriod,
                            note: note,
                            targetVariable: targetVariable,
                            auxConf: auxConf,
                            forceSynthetic: forceSynthetic,
                            transaction: transaction);

                        DataTable dt = new();

                        dt.Columns.Add(column: new DataColumn()
                        {
                            ColumnName = Name,
                            DataType = Type.GetType(typeName: "System.Int32")
                        });

                        DataRow row = dt.NewRow();

                        Functions.SetNIntArg(
                            row: row,
                            name: Name,
                            val: val);

                        dt.Rows.Add(row: row);

                        return dt;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_2p_est_configuration
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="estimationCell"></param>
                    /// <param name="estimationPeriod"></param>
                    /// <param name="note"></param>
                    /// <param name="targetVariable"></param>
                    /// <param name="auxConf"></param>
                    /// <param name="forceSynthetic"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns></returns>
                    public static Nullable<int> Execute(
                        NfiEstaDB database,
                        Nullable<int> estimationCell,
                        Nullable<int> estimationPeriod,
                        string note,
                        Nullable<int> targetVariable,
                        Nullable<int> auxConf,
                        Nullable<bool> forceSynthetic,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(
                            estimationCell: estimationCell,
                            estimationPeriod: estimationPeriod,
                            note: note,
                            targetVariable: targetVariable,
                            auxConf: auxConf,
                            forceSynthetic: forceSynthetic);

                        NpgsqlParameter pEstimationCell = new(
                           parameterName: "estimationCell",
                           parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pEstimationPeriod = new(
                           parameterName: "estimationPeriod",
                           parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pNote = new(
                           parameterName: "note",
                           parameterType: NpgsqlDbType.Varchar);

                        NpgsqlParameter pTargetVariable = new(
                           parameterName: "targetVariable",
                           parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pAuxConf = new(
                           parameterName: "auxConf",
                           parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pForceSynthetic = new(
                           parameterName: "forceSynthetic",
                           parameterType: NpgsqlDbType.Boolean);

                        if (estimationCell != null)
                        {
                            pEstimationCell.Value = (int)estimationCell;
                        }
                        else
                        {
                            pEstimationCell.Value = DBNull.Value;
                        }

                        if (estimationPeriod != null)
                        {
                            pEstimationPeriod.Value = (int)estimationPeriod;
                        }
                        else
                        {
                            pEstimationPeriod.Value = DBNull.Value;
                        }

                        if (note != null)
                        {
                            pNote.Value = (string)note;
                        }
                        else
                        {
                            pNote.Value = DBNull.Value;
                        }

                        if (targetVariable != null)
                        {
                            pTargetVariable.Value = (int)targetVariable;
                        }
                        else
                        {
                            pTargetVariable.Value = DBNull.Value;
                        }

                        if (auxConf != null)
                        {
                            pAuxConf.Value = (int)auxConf;
                        }
                        else
                        {
                            pAuxConf.Value = DBNull.Value;
                        }

                        if (forceSynthetic != null)
                        {
                            pForceSynthetic.Value = (bool)forceSynthetic;
                        }
                        else
                        {
                            pForceSynthetic.Value = DBNull.Value;
                        }

                        string strResult = database.Postgres.ExecuteScalar(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pEstimationCell, pEstimationPeriod, pNote,
                            pTargetVariable, pAuxConf, pForceSynthetic);

                        if (String.IsNullOrEmpty(value: strResult))
                        {
                            return null;
                        }
                        else
                        {
                            if (Int32.TryParse(s: strResult, result: out int intResult))
                            {
                                return intResult;
                            }
                            else
                            {
                                return null;
                            }
                        }
                    }

                    #endregion Methods

                }

                #endregion Fn2pEstConfiguration

            }

        }
    }
}