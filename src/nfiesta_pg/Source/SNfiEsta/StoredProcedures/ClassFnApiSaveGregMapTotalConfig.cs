﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {

            /// <summary>
            /// Stored procedures of schema nfiesta
            /// </summary>
            public static partial class NfiEstaFunctions
            {
                // fn_api_save_greg_map_total_config

                #region FnApiSaveGregMapTotalConfig

                /// <summary>
                /// Wrapper for stored procedure fn_api_save_greg_map_total_config
                /// This is a wrapper API function saving the configuration of GREG estimates of total
                /// that use auxiliary data in the form of maps.
                /// </summary>
                public static partial class FnApiSaveGregMapTotalConfig
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = NfiEstaSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_api_save_gregmap_total_config";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                         = String.Concat(
                             $"$SchemaName.$Name; ",
                             $"args: ",
                             $"_estimation_period integer, ",
                             $"_estimation_cell integer, ",
                             $"_variable integer, ",
                             $"_panel_refyearset_group integer, ",
                             $"_working_model integer, ",
                             $"_sigma boolean, ",
                             $"_param_area_type integer, ",
                             $"_force_synthetic boolean; ",
                             $"returns: integer");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                    = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    $SchemaName.$Name(",
                            $"@estimationPeriod, ",
                            $"@estimationCell, ",
                            $"@variable,",
                            $"@panelRefYearSetGroup, ",
                            $"@workingModel, ",
                            $"@sigma, ",
                            $"@paramAreaType, ",
                            $"@forceSynthetic) AS $Name;{Environment.NewLine}");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="estimationPeriod">Estimation period identifier</param>
                    /// <param name="estimationCell">Estimation cell identifier</param>
                    /// <param name="variable">Atribute category identifier</param>
                    /// <param name="panelRefYearSetGroup">Panel reference year set identifier</param>
                    /// <param name="workingModel">Model identifier</param>
                    /// <param name="sigma">Sigma</param>
                    /// <param name="paramAreaType">Parametrization area type identifier</param>
                    /// <param name="forceSynthetic">Synthetic estimate</param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        Nullable<int> estimationPeriod,
                        Nullable<int> estimationCell,
                        Nullable<int> variable,
                        Nullable<int> panelRefYearSetGroup,
                        Nullable<int> workingModel,
                        Nullable<bool> sigma,
                        Nullable<int> paramAreaType,
                        Nullable<bool> forceSynthetic)
                    {
                        string result = SQL;

                        result = result.Replace(
                            oldValue: "@estimationPeriod",
                            newValue: Functions.PrepNIntArg(arg: estimationPeriod));

                        result = result.Replace(
                            oldValue: "@estimationCell",
                            newValue: Functions.PrepNIntArg(arg: estimationCell));

                        result = result.Replace(
                           oldValue: "@variable",
                           newValue: Functions.PrepNIntArg(arg: variable));

                        result = result.Replace(
                            oldValue: "@panelRefYearSetGroup",
                            newValue: Functions.PrepNIntArg(arg: panelRefYearSetGroup));

                        result = result.Replace(
                            oldValue: "@workingModel",
                            newValue: Functions.PrepNIntArg(arg: workingModel));

                        result = result.Replace(
                           oldValue: "@sigma",
                           newValue: Functions.PrepNBoolArg(arg: sigma));

                        result = result.Replace(
                            oldValue: "@paramAreaType",
                            newValue: Functions.PrepNIntArg(arg: paramAreaType));

                        result = result.Replace(
                            oldValue: "@forceSynthetic",
                            newValue: Functions.PrepNBoolArg(arg: forceSynthetic));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_api_save_gregmap_total_config
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="estimationPeriod">Estimation period identifier</param>
                    /// <param name="estimationCell">Estimation cell identifier</param>
                    /// <param name="variable">Atribute category identifier</param>
                    /// <param name="panelRefYearSetGroup">Panel reference year set identifier</param>
                    /// <param name="workingModel">Model identifier</param>
                    /// <param name="sigma">Sigma</param>
                    /// <param name="paramAreaType">Parametrization area type identifier</param>
                    /// <param name="forceSynthetic">Synthetic estimate</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table with identifier of the saved total estimate configuration</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> estimationPeriod,
                        Nullable<int> estimationCell,
                        Nullable<int> variable,
                        Nullable<int> panelRefYearSetGroup,
                        Nullable<int> workingModel,
                        Nullable<bool> sigma,
                        Nullable<int> paramAreaType,
                        Nullable<bool> forceSynthetic,
                        NpgsqlTransaction transaction = null)
                    {
                        Nullable<int> val = Execute(
                            database: database,
                            estimationPeriod: estimationPeriod,
                            estimationCell: estimationCell,
                            variable: variable,
                            panelRefYearSetGroup: panelRefYearSetGroup,
                            workingModel: workingModel,
                            sigma: sigma,
                            paramAreaType: paramAreaType,
                            forceSynthetic: forceSynthetic,
                            transaction: transaction);

                        DataTable dt = new();

                        dt.Columns.Add(column: new DataColumn()
                        {
                            ColumnName = Name,
                            DataType = Type.GetType(typeName: "System.Int32")
                        });

                        DataRow row = dt.NewRow();

                        Functions.SetNIntArg(
                            row: row,
                            name: Name,
                            val: val);

                        dt.Rows.Add(row: row);

                        return dt;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_api_save_greg_map_total_config
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="estimationPeriod">Estimation period identifier</param>
                    /// <param name="estimationCell">Estimation cell identifier</param>
                    /// <param name="variable">Atribute category identifier</param>
                    /// <param name="panelRefYearSetGroup">Panel reference year set identifier</param>
                    /// <param name="workingModel">Model identifier</param>
                    /// <param name="sigma">Sigma</param>
                    /// <param name="paramAreaType">Parametrization area type identifier</param>
                    /// <param name="forceSynthetic">Synthetic estimate</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Identifier of the saved total estimate configuration</returns>
                    public static Nullable<int> Execute(
                        NfiEstaDB database,
                        Nullable<int> estimationPeriod,
                        Nullable<int> estimationCell,
                        Nullable<int> variable,
                        Nullable<int> panelRefYearSetGroup,
                        Nullable<int> workingModel,
                        Nullable<bool> sigma,
                        Nullable<int> paramAreaType,
                        Nullable<bool> forceSynthetic,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(
                            estimationPeriod: estimationPeriod,
                            estimationCell: estimationCell,
                            variable: variable,
                            panelRefYearSetGroup: panelRefYearSetGroup,
                            workingModel: workingModel,
                            sigma: sigma,
                            paramAreaType: paramAreaType,
                            forceSynthetic: forceSynthetic);

                        NpgsqlParameter pEstimationPeriod = new(
                            parameterName: "estimationPeriod",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pEstimationCell = new(
                            parameterName: "estimationCell",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pVariable = new(
                            parameterName: "variable",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pPanelRefYearSetGroup = new(
                            parameterName: "panelRefYearSetGroup",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pWorkingModel = new(
                            parameterName: "workingModel",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pSigma = new(
                            parameterName: "sigma",
                            parameterType: NpgsqlDbType.Boolean);

                        NpgsqlParameter pParamAreaType = new(
                            parameterName: "paramAreaType",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pForceSynthetic = new(
                            parameterName: "forceSynthetic",
                            parameterType: NpgsqlDbType.Boolean);


                        if (estimationPeriod != null)
                        {
                            pEstimationPeriod.Value = (int)estimationPeriod;
                        }
                        else
                        {
                            pEstimationPeriod.Value = DBNull.Value;
                        }

                        if (estimationCell != null)
                        {
                            pEstimationCell.Value = (int)estimationCell;
                        }
                        else
                        {
                            pEstimationCell.Value = DBNull.Value;
                        }

                        if (variable != null)
                        {
                            pVariable.Value = (int)variable;
                        }
                        else
                        {
                            pVariable.Value = DBNull.Value;
                        }

                        if (panelRefYearSetGroup != null)
                        {
                            pPanelRefYearSetGroup.Value = (int)panelRefYearSetGroup;
                        }
                        else
                        {
                            pPanelRefYearSetGroup.Value = DBNull.Value;
                        }

                        if (workingModel != null)
                        {
                            pWorkingModel.Value = (int)workingModel;
                        }
                        else
                        {
                            pWorkingModel.Value = DBNull.Value;
                        }

                        if (sigma != null)
                        {
                            pSigma.Value = (bool)sigma;
                        }
                        else
                        {
                            pSigma.Value = DBNull.Value;
                        }

                        if (paramAreaType != null)
                        {
                            pParamAreaType.Value = (int)paramAreaType;
                        }
                        else
                        {
                            pParamAreaType.Value = DBNull.Value;
                        }

                        if (forceSynthetic != null)
                        {
                            pForceSynthetic.Value = (bool)forceSynthetic;
                        }
                        else
                        {
                            pForceSynthetic.Value = DBNull.Value;
                        }

                        string strResult = database.Postgres.ExecuteScalar(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pEstimationPeriod, pEstimationCell, pVariable, pPanelRefYearSetGroup,
                            pWorkingModel, pSigma, pParamAreaType, pForceSynthetic);

                        if (String.IsNullOrEmpty(value: strResult))
                        {
                            return null;
                        }
                        else
                        {
                            if (Int32.TryParse(s: strResult, result: out int intResult))
                            {
                                return intResult;
                            }
                            else
                            {
                                return null;
                            }
                        }
                    }

                    #endregion Methods

                }

                #endregion FnApiSaveGregMapTotalConfig

            }

        }
    }
}