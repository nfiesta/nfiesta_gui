﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {

            /// <summary>
            /// Stored procedures of schema nfiesta
            /// </summary>
            public static partial class NfiEstaFunctions
            {
                // fn_make_estimate

                #region FnMakeEstimate

                /// <summary>
                /// Wrapper for stored procedure fn_make_estimate.
                /// Wrapper function for fn_make_estimate with storage of results.
                /// </summary>
                public static class FnMakeEstimate
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName = NfiEstaSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name = "fn_make_estimate";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: _estimate_conf integer; ",
                            $"returns: TABLE(estimate_conf integer)");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    $Name.estimate_conf   AS estimate_conf{Environment.NewLine}",
                            $"FROM{Environment.NewLine}",
                            $"    $SchemaName.$Name(@estimateConfId) AS $Name{Environment.NewLine}",
                            $"ORDER BY{Environment.NewLine}",
                            $"   $Name.estimate_conf;{Environment.NewLine}");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="estimateConfId">Estimate configuration identifier</param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(Nullable<int> estimateConfId)
                    {
                        string result = SQL;

                        result = result.Replace(
                            oldValue: "@estimateConfId",
                            newValue: Functions.PrepNIntArg(arg: estimateConfId));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_make_estimate
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="estimateConfId">Estimate configuration identifier</param>
                    /// <param name="transaction">Transaction</param>
                    public static void Execute(
                        NfiEstaDB database,
                        Nullable<int> estimateConfId,
                        NpgsqlTransaction transaction = null)
                    {
                        ExecuteQuery(
                            database: database,
                            estimateConfId: estimateConfId,
                            transaction: transaction);
                    }

                    /// <summary>
                    /// Execute stored procedure fn_make_estimate
                    /// </summary>
                    /// <param name="connection">Database connection</param>
                    /// <param name="estimateConfId">Estimate configuration identifier</param>
                    /// <param name="transaction">Transaction</param>
                    public static void Execute(
                        ZaJi.PostgreSQL.PostgreSQLWrapper connection,
                        Nullable<int> estimateConfId,
                        NpgsqlTransaction transaction = null)
                    {
                        ExecuteQuery(
                            connection: connection,
                            estimateConfId: estimateConfId,
                            transaction: transaction);
                    }

                    /// <summary>
                    /// Execute stored procedure fn_make_estimate
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="estimateConfId">Estimate configuration identifier</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> estimateConfId,
                        NpgsqlTransaction transaction = null)
                    {
                        return ExecuteQuery(
                            connection: database.Postgres,
                            estimateConfId: estimateConfId,
                            transaction: transaction);
                    }

                    /// <summary>
                    /// Execute stored procedure fn_make_estimate
                    /// </summary>
                    /// <param name="connection">Database connection</param>
                    /// <param name="estimateConfId">Estimate configuration identifier</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table</returns>
                    public static DataTable ExecuteQuery(
                        ZaJi.PostgreSQL.PostgreSQLWrapper connection,
                        Nullable<int> estimateConfId,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(estimateConfId: estimateConfId);

                        NpgsqlParameter pEstimateConfId = new(
                            parameterName: "estimateConfId",
                            parameterType: NpgsqlDbType.Integer);

                        if (estimateConfId != null)
                        {
                            pEstimateConfId.Value = (int)estimateConfId;
                        }
                        else
                        {
                            pEstimateConfId.Value = DBNull.Value;
                        }

                        return connection.ExecuteQuery(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pEstimateConfId);
                    }

                    #endregion Methods

                }

                #endregion FnMakeEstimate

            }

        }
    }
}