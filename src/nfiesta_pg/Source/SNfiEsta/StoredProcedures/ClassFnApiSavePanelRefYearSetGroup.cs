﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {

            /// <summary>
            /// Stored procedures of schema nfiesta
            /// </summary>
            public static partial class NfiEstaFunctions
            {
                // fn_api_save_panel_refyearset_group

                #region FnApiSavePanelRefYearSetGroup

                /// <summary>
                /// Wrapper for stored procedure fn_api_save_panel_refyearset_group
                /// This is a wrapper API function.
                /// Unlike the internally called fn_save_panel_refyearset_group this API has no defaults.
                /// More specifically, labels and descriptions must always be provided.
                /// </summary>
                public static partial class FnApiSavePanelRefYearSetGroup
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = NfiEstaSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_api_save_panel_refyearset_group";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_panels integer[], ",
                            $"_refyearsets integer[], ",
                            $"_label character varying, ",
                            $"_description text, ",
                            $"_label_en character varying, ",
                            $"_description_en text; ",
                            $"returns: integer");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    $SchemaName.$Name(",
                            $"@panels, ",
                            $"@refYearSets, ",
                            $"@labelCs, ",
                            $"@descriptionCs, ",
                            $"@labelEn, ",
                            $"@descriptionEn) AS $Name;{Environment.NewLine}");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="panels">List of panel identifiers</param>
                    /// <param name="refYearSets">List of reference year set identifiers</param>
                    /// <param name="labelCs">Panel reference year set group label in national language</param>
                    /// <param name="descriptionCs">Panel reference year set group label in national language</param>
                    /// <param name="labelEn">Panel reference year set group label in English</param>
                    /// <param name="descriptionEn">Panel reference year set group description in English</param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        List<Nullable<int>> panels,
                        List<Nullable<int>> refYearSets,
                        string labelCs,
                        string descriptionCs,
                        string labelEn,
                        string descriptionEn)
                    {
                        string result = SQL;

                        result = result.Replace(
                            oldValue: "@panels",
                            newValue: Functions.PrepNIntArrayArg(args: panels, dbType: "int4"));

                        result = result.Replace(
                            oldValue: "@refYearSets",
                            newValue: Functions.PrepNIntArrayArg(args: refYearSets, dbType: "int4"));

                        result = result.Replace(
                           oldValue: "@labelCs",
                           newValue: Functions.PrepStringArg(arg: labelCs));

                        result = result.Replace(
                            oldValue: "@descriptionCs",
                            newValue: Functions.PrepStringArg(arg: descriptionCs));

                        result = result.Replace(
                            oldValue: "@labelEn",
                            newValue: Functions.PrepStringArg(arg: labelEn));

                        result = result.Replace(
                           oldValue: "@descriptionEn",
                           newValue: Functions.PrepStringArg(arg: descriptionEn));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_api_save_panel_refyearset_group
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="panels">List of panel identifiers</param>
                    /// <param name="refYearSets">List of reference year set identifiers</param>
                    /// <param name="labelCs">Panel reference year set group label in national language</param>
                    /// <param name="descriptionCs">Panel reference year set group label in national language</param>
                    /// <param name="labelEn">Panel reference year set group label in English</param>
                    /// <param name="descriptionEn">Panel reference year set group description in English</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table with identifier of the saved panel reference year set group</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        List<Nullable<int>> panels,
                        List<Nullable<int>> refYearSets,
                        string labelCs,
                        string descriptionCs,
                        string labelEn,
                        string descriptionEn,
                        NpgsqlTransaction transaction = null)
                    {
                        Nullable<int> val = Execute(
                            database: database,
                            panels: panels,
                            refYearSets: refYearSets,
                            labelCs: labelCs,
                            descriptionCs: descriptionCs,
                            labelEn: labelEn,
                            descriptionEn: descriptionEn,
                            transaction: transaction);

                        DataTable dt = new();

                        dt.Columns.Add(column: new DataColumn()
                        {
                            ColumnName = Name,
                            DataType = Type.GetType(typeName: "System.Int32")
                        });

                        DataRow row = dt.NewRow();

                        Functions.SetNIntArg(
                            row: row,
                            name: Name,
                            val: val);

                        dt.Rows.Add(row: row);

                        return dt;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_api_save_panel_refyearset_group
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="panels">List of panel identifiers</param>
                    /// <param name="refYearSets">List of reference year set identifiers</param>
                    /// <param name="labelCs">Panel reference year set group label in national language</param>
                    /// <param name="descriptionCs">Panel reference year set group label in national language</param>
                    /// <param name="labelEn">Panel reference year set group label in English</param>
                    /// <param name="descriptionEn">Panel reference year set group description in English</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Identifier of the saved panel reference year set group</returns>
                    public static Nullable<int> Execute(
                        NfiEstaDB database,
                        List<Nullable<int>> panels,
                        List<Nullable<int>> refYearSets,
                        string labelCs,
                        string descriptionCs,
                        string labelEn,
                        string descriptionEn,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(
                           panels: panels,
                           refYearSets: refYearSets,
                           labelCs: labelCs,
                           descriptionCs: descriptionCs,
                           labelEn: labelEn,
                           descriptionEn: descriptionEn);

                        NpgsqlParameter pPanels = new(
                            parameterName: "panels",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        NpgsqlParameter pRefYearSets = new(
                            parameterName: "refYearSets",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        NpgsqlParameter pLabelCs = new(
                            parameterName: "labelCs",
                            parameterType: NpgsqlDbType.Varchar);

                        NpgsqlParameter pDescriptionCs = new(
                            parameterName: "descriptionCs",
                            parameterType: NpgsqlDbType.Text);

                        NpgsqlParameter pLabelEn = new(
                            parameterName: "labelEn",
                            parameterType: NpgsqlDbType.Varchar);

                        NpgsqlParameter pDescriptionEn = new(
                            parameterName: "descriptionEn",
                            parameterType: NpgsqlDbType.Text);


                        if (panels != null)
                        {
                            pPanels.Value = panels.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pPanels.Value = DBNull.Value;
                        }

                        if (refYearSets != null)
                        {
                            pRefYearSets.Value = refYearSets.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pRefYearSets.Value = DBNull.Value;
                        }

                        if (labelCs != null)
                        {
                            pLabelCs.Value = labelCs;
                        }
                        else
                        {
                            pLabelCs.Value = DBNull.Value;
                        }

                        if (descriptionCs != null)
                        {
                            pDescriptionCs.Value = descriptionCs;
                        }
                        else
                        {
                            pDescriptionCs.Value = DBNull.Value;
                        }

                        if (labelEn != null)
                        {
                            pLabelEn.Value = labelEn;
                        }
                        else
                        {
                            pLabelEn.Value = DBNull.Value;
                        }

                        if (descriptionEn != null)
                        {
                            pDescriptionEn.Value = descriptionEn;
                        }
                        else
                        {
                            pDescriptionEn.Value = DBNull.Value;
                        }

                        string strResult = database.Postgres.ExecuteScalar(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pPanels, pRefYearSets,
                            pLabelCs, pDescriptionCs, pLabelEn, pDescriptionEn);

                        if (String.IsNullOrEmpty(value: strResult))
                        {
                            return null;
                        }
                        else
                        {
                            if (Int32.TryParse(s: strResult, result: out int intResult))
                            {
                                return intResult;
                            }
                            else
                            {
                                return null;
                            }
                        }
                    }

                    #endregion Methods

                }

                #endregion FnApiSavePanelRefYearSetGroup

            }

        }
    }
}