﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {

            /// <summary>
            /// Stored procedures of schema nfiesta
            /// </summary>
            public static partial class NfiEstaFunctions
            {
                // fn_add_res_total_geo

                #region FnAddResTotalGeo

                /// <summary>
                /// Wrapper for stored procedure fn_add_res_total_geo.
                /// Function showing total estimates geographic additivity (aggregated class estimate should be equal to sum of sub-classes estimates). Hierarchy between aggregated classes and its sub-classes is defined in v_estimation_cell_hierarchy.
                /// Function input argument is:
                /// Array of estimate configuration ids.FKEY to t_estimate_conf.id.All est.conf.ids to be checked (aggregated class together with sub-classes) need to be passed.
                /// Minimal amount of difference [%].
                /// Whether include NULL difference -- indicating defined sub-classes missing in data.
                /// Resulting table has following columns:
                /// Estimate attribute -- variable.FKEY to t_variable.id.
                /// Estimate auxiliary configuration. FKEY to t_aux_conf.id.
                /// Parameter showing whether estimate is forced to be synthetic.
                /// Estimate configuration id.FKEY to t_estimate_conf.id.
                /// Estimate estimation cell. FKEY to c_estimation_cell.id.
                /// Aggregated class point estimate.
                /// Sum of sub-classes point estimates(belonging to aggregated class).
                /// Estimation cells defined in hierarchy(v_estimation_cell_hierarchy). Array of FKEYs to c_estimation_cell.id.
                /// Estimation cells found in data(t_result). Array of FKEYs to c_estimation_cell.id.
                /// Estimate configurations found in data(t_result). Array of FKEYs to t_estimate_donf.id.
                /// Relative difference between aggregated class estimate and sum of sub-classes estimates.
                /// </summary>
                public static class FnAddResTotalGeo
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName = NfiEstaSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name = "fn_add_res_total_geo";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"est_confs integer[], ",
                            $"min_diff double precision DEFAULT 0.0, ",
                            $"include_null_diff boolean DEFAULT true; ",
                            $"returns: ",
                            $"TABLE(",
                            $"variable integer, ",
                            $"aux_conf integer, ",
                            $"force_synthetic boolean, ",
                            $"estimate_conf integer, ",
                            $"estimation_cell integer, ",
                            $"point_est double precision, ",
                            $"point_est_sum double precision, ",
                            $"estimation_cells_def integer[], ",
                            $"estimation_cells_found integer[], ",
                            $"estimate_confs_found integer[], ",
                            $"diff double precision)");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    {TFnAddResTotalGeoList.ColId.SQL(cols: TFnAddResTotalGeoList.Cols, alias: Name)}",
                            $"    {TFnAddResTotalGeoList.ColVariableId.SQL(cols: TFnAddResTotalGeoList.Cols, alias: Name)}",
                            $"    {TFnAddResTotalGeoList.ColAuxConfId.SQL(cols: TFnAddResTotalGeoList.Cols, alias: Name)}",
                            $"    {TFnAddResTotalGeoList.ColForceSynthetic.SQL(cols: TFnAddResTotalGeoList.Cols, alias: Name)}",
                            $"    {TFnAddResTotalGeoList.ColEstimateConfId.SQL(cols: TFnAddResTotalGeoList.Cols, alias: Name)}",
                            $"    {TFnAddResTotalGeoList.ColEstimationCellId.SQL(cols: TFnAddResTotalGeoList.Cols, alias: Name)}",
                            $"    {TFnAddResTotalGeoList.ColPointEst.SQL(cols: TFnAddResTotalGeoList.Cols, alias: Name)}",
                            $"    {TFnAddResTotalGeoList.ColPointEstSum.SQL(cols: TFnAddResTotalGeoList.Cols, alias: Name)}",
                            $"    {TFnAddResTotalGeoList.ColEstimationCellsDef.SQL(cols: TFnAddResTotalGeoList.Cols, alias: Name)}",
                            $"    {TFnAddResTotalGeoList.ColEstimationCellsFound.SQL(cols: TFnAddResTotalGeoList.Cols, alias: Name)}",
                            $"    {TFnAddResTotalGeoList.ColEstimateConfsFound.SQL(cols: TFnAddResTotalGeoList.Cols, alias: Name)}",
                            $"    {TFnAddResTotalGeoList.ColDiff.SQL(cols: TFnAddResTotalGeoList.Cols, alias: Name, isLastOne: true)}",
                            $"FROM{Environment.NewLine}",
                            $"    $SchemaName.$Name(@estConfs, @minDiff, @includeNullDiff) AS $Name{Environment.NewLine}",
                            $"ORDER BY{Environment.NewLine}",
                            $"    {Name}.{TFnAddResTotalGeoList.ColEstimateConfId.DbName},{Environment.NewLine}",
                            $"    {Name}.{TFnAddResTotalGeoList.ColEstimateConfsFound.DbName};{Environment.NewLine}"
                            );

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="estConfs">List of estimate configurations</param>
                    /// <param name="minDiff">
                    /// Minimal amount of difference [%].
                    /// Default value is 0 when parameter is NULL.
                    /// </param>
                    /// <param name="includeNullDiff">
                    /// Whether include NULL difference -- indicating defined sub-classes missing in data
                    /// Default value is true when parameter is NULL.</param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        List<Nullable<int>> estConfs,
                        Nullable<double> minDiff,
                        Nullable<bool> includeNullDiff)
                    {
                        string result = SQL;

                        result = result.Replace(
                            oldValue: "@estConfs",
                            newValue: Functions.PrepNIntArrayArg(args: estConfs, dbType: "int4"));

                        result = result.Replace(
                            oldValue: "@minDiff",
                            newValue: Functions.PrepNDoubleArg(arg: minDiff));

                        result = result.Replace(
                            oldValue: "@includeNullDiff",
                            newValue: Functions.PrepNBoolArg(arg: includeNullDiff));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_add_res_total_geo
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="estConfs">List of estimate configurations</param>
                    /// <param name="minDiff">
                    /// Minimal amount of difference [%].
                    /// Default value is 0 when parameter is NULL.
                    /// </param>
                    /// <param name="includeNullDiff">
                    /// Whether include NULL difference -- indicating defined sub-classes missing in data
                    /// Default value is true when parameter is NULL.</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table with list of total estimates geographic additivity</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        List<Nullable<int>> estConfs,
                        Nullable<double> minDiff,
                        Nullable<bool> includeNullDiff,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(estConfs: estConfs, minDiff: minDiff, includeNullDiff: includeNullDiff);

                        NpgsqlParameter pEstConfs = new(
                            parameterName: "estConfs",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        NpgsqlParameter pMinDiff = new(
                            parameterName: "minDiff",
                            parameterType: NpgsqlDbType.Double);

                        NpgsqlParameter pIncludeNullDiff = new(
                            parameterName: "includeNullDiff",
                            parameterType: NpgsqlDbType.Boolean);

                        if (estConfs != null)
                        {
                            pEstConfs.Value = estConfs.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pEstConfs.Value = DBNull.Value;
                        }

                        if (minDiff != null)
                        {
                            pMinDiff.Value = (double)minDiff;
                        }
                        else
                        {
                            pMinDiff.Value = DBNull.Value;
                        }

                        if (includeNullDiff != null)
                        {
                            pIncludeNullDiff.Value = (bool)includeNullDiff;
                        }
                        else
                        {
                            pIncludeNullDiff.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteQuery(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pEstConfs, pMinDiff, pIncludeNullDiff);
                    }

                    /// <summary>
                    /// Execute stored procedure fn_add_res_total_geo
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="estConfs">List of estimate configurations</param>
                    /// <param name="minDiff">
                    /// Minimal amount of difference [%].
                    /// Default value is 0 when parameter is NULL.
                    /// </param>
                    /// <param name="includeNullDiff">
                    /// Whether include NULL difference -- indicating defined sub-classes missing in data
                    /// Default value is true when parameter is NULL.</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>List of total estimates geographic additivity</returns>
                    public static TFnAddResTotalGeoList Execute(
                        NfiEstaDB database,
                        List<Nullable<int>> estConfs,
                        Nullable<double> minDiff,
                        Nullable<bool> includeNullDiff,
                        NpgsqlTransaction transaction = null)
                    {
                        System.Diagnostics.Stopwatch stopWatch = new();
                        stopWatch.Start();

                        DataTable data = ExecuteQuery(
                            database: database,
                            estConfs: estConfs,
                            minDiff: minDiff,
                            includeNullDiff: includeNullDiff,
                            transaction: transaction);
                        data.TableName = Name;

                        stopWatch.Stop();

                        return new TFnAddResTotalGeoList(
                            database: database,
                            data: data,
                            loadingTime: 0.001 * stopWatch.ElapsedMilliseconds,
                            storedProcedure: CommandText)
                        {
                            EstConfs = estConfs,
                            MinDiff = minDiff,
                            IncludeNullDiff = includeNullDiff
                        };
                    }

                    #endregion Methods

                }

                #endregion FnAddResTotalGeo

            }

        }
    }
}