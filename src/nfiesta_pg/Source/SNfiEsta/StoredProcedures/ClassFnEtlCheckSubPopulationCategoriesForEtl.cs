﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {

            /// <summary>
            /// Stored procedures of schema nfiesta
            /// </summary>
            public static partial class NfiEstaFunctions
            {
                // fn_etl_check_sub_population_categories4etl

                #region FnEtlCheckSubPopulationCategoriesForEtl

                /// <summary>
                /// Wrapper for stored procedure fn_etl_check_sub_population_categories4etl.
                /// The function returns records of subpopulation categories for given input subpopulation with information if input label of subpopulation category was paired in target DB or not.
                /// </summary>
                public static class FnEtlCheckSubPopulationCategoriesForEtl
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName = NfiEstaSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name = "fn_etl_check_sub_population_categories4etl";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_sub_population integer, ",
                            $"_sub_population_category json; ",
                            $"returns: ",
                            $"TABLE(",
                            $"id integer, ",
                            $"sub_population_category integer[], ",
                            $"etl_id integer, ",
                            $"label character varying, ",
                            $"description text, ",
                            $"label_en character varying, ",
                            $"description_en text, ",
                            $"check_label boolean)");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    $Name.id                                                          AS id,{Environment.NewLine}",
                            $"    array_to_string({Name}.sub_population_category, ';', 'NULL')      AS sub_population_category,{Environment.NewLine}",
                            $"    $Name.etl_id                                                      AS etl_id,{Environment.NewLine}",
                            $"    $Name.label                                                       AS label,{Environment.NewLine}",
                            $"    $Name.description                                                 AS description,{Environment.NewLine}",
                            $"    $Name.label_en                                                    AS label_en,{Environment.NewLine}",
                            $"    $Name.description_en                                              AS description_en,{Environment.NewLine}",
                            $"    $Name.check_label                                                 AS check_label{Environment.NewLine}",
                            $"FROM{Environment.NewLine}",
                            $"    $SchemaName.$Name(@subPopulation, @subPopulationCategory) AS $Name;{Environment.NewLine}");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="subPopulation"></param>
                    /// <param name="subPopulationCategory"></param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        Nullable<int> subPopulation,
                        string subPopulationCategory)
                    {
                        string result = SQL;

                        result = result.Replace(
                            oldValue: "@subPopulation",
                            newValue: Functions.PrepNIntArg(arg: subPopulation));

                        result = result.Replace(
                            oldValue: "@subPopulationCategory",
                            newValue: Functions.PrepStringArg(arg: subPopulationCategory));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_etl_check_sub_population_categories4etl
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="subPopulation"></param>
                    /// <param name="subPopulationCategory"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns></returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> subPopulation,
                        string subPopulationCategory,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(subPopulation: subPopulation, subPopulationCategory: subPopulationCategory);

                        NpgsqlParameter pSubPopulation = new(
                            parameterName: "subPopulation",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pSubPopulationCategory = new(
                            parameterName: "subPopulationCategory",
                            parameterType: NpgsqlDbType.Json);

                        if (subPopulation != null)
                        {
                            pSubPopulation.Value = (int)subPopulation;
                        }
                        else
                        {
                            pSubPopulation.Value = DBNull.Value;
                        }

                        if (subPopulationCategory != null)
                        {
                            pSubPopulationCategory.Value = (string)subPopulationCategory;
                        }
                        else
                        {
                            pSubPopulationCategory.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteQuery(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pSubPopulation, pSubPopulationCategory);
                    }

                    #endregion Methods

                }

                #endregion FnEtlCheckSubPopulationCategoriesForEtl

            }
        }
    }
}

