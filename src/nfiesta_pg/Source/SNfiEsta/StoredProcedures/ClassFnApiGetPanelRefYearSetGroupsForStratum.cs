﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {

            /// <summary>
            /// Stored procedures of schema nfiesta
            /// </summary>
            public static partial class NfiEstaFunctions
            {
                // fn_api_get_panel_refyearset_groups4stratum

                #region FnApiGetPanelRefYearSetGroupsForStratum

                /// <summary>
                /// Wrapper for stored procedure fn_api_get_panel_refyearset_groups4stratum.
                /// The function returns list of groups of panel and reference yearset combinations
                /// for the stratum identifier passed as input of the function.
                /// </summary>
                public static class FnApiGetPanelRefYearSetGroupsForStratum
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName = NfiEstaSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name = "fn_api_get_panel_refyearset_groups4stratum";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_stratum integer; ",
                            $"returns: ",
                            $"TABLE(",
                            $"_panel_refyearset_group_id integer, ",
                            $"_panel_refyearset_group_label character varying, ",
                            $"_panel_refyearset_group_label_en character varying, ",
                            $"_panel_refyearset_group_description text, ",
                            $"_panel_refyearset_group_description_en text)");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", SchemaName)
                                    .Replace("$Name", Name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                String.Concat(
                                $"SELECT{Environment.NewLine}",
                                $"    _panel_refyearset_group_id                AS {PanelRefYearSetGroupList.ColId.Name},{Environment.NewLine}",
                                $"    _panel_refyearset_group_label             AS {PanelRefYearSetGroupList.ColLabelCs.Name},{Environment.NewLine}",
                                $"    _panel_refyearset_group_description       AS {PanelRefYearSetGroupList.ColDescriptionCs.Name},{Environment.NewLine}",
                                $"    _panel_refyearset_group_label_en          AS {PanelRefYearSetGroupList.ColLabelEn.Name},{Environment.NewLine}",
                                $"    _panel_refyearset_group_description_en    AS {PanelRefYearSetGroupList.ColDescriptionEn.Name}{Environment.NewLine}",
                                $"FROM{Environment.NewLine}",
                                $"    {SchemaName}.{Name}(@stratumId);{Environment.NewLine}");
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="stratumId">Stratum identifier</param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(Nullable<int> stratumId)
                    {
                        string result = SQL;

                        result = result.Replace(
                           oldValue: "@stratumId",
                           newValue: Functions.PrepNIntArg(arg: stratumId, dbType: "int4"));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_api_get_panel_refyearset_groups4stratum
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="stratumId">Stratum identifier</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table with aggregated sets of panels and corresponding reference year sets</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> stratumId,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(stratumId: stratumId);

                        NpgsqlParameter pStratumId = new(
                            parameterName: "stratumId",
                            parameterType: NpgsqlDbType.Integer);

                        if (stratumId != null)
                        {
                            pStratumId.Value = stratumId;
                        }
                        else
                        {
                            pStratumId.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteQuery(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pStratumId);
                    }

                    /// <summary>
                    /// Execute stored procedure fn_api_get_panel_refyearset_groups4stratum
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="stratumId">Stratum identifier</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>List of aggregated sets of panels and corresponding reference year sets</returns>
                    public static PanelRefYearSetGroupList Execute(
                        NfiEstaDB database,
                        Nullable<int> stratumId,
                        NpgsqlTransaction transaction = null)
                    {
                        System.Diagnostics.Stopwatch stopWatch = new();
                        stopWatch.Start();

                        DataTable data = ExecuteQuery(
                            database: database,
                            stratumId: stratumId,
                            transaction: transaction);
                        data.TableName = Name;

                        stopWatch.Stop();

                        return new PanelRefYearSetGroupList(
                            database: database,
                            data: data,
                            loadingTime: 0.001 * stopWatch.ElapsedMilliseconds,
                            storedProcedure: CommandText)
                        { };
                    }

                    #endregion Methods

                }

                #endregion FnApiGetPanelRefYearSetGroupsForStratum

            }

        }
    }
}