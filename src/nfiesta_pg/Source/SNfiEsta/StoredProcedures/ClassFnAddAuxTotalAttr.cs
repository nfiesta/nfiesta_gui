﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {

            /// <summary>
            /// Stored procedures of schema nfiesta
            /// </summary>
            public static partial class NfiEstaFunctions
            {
                // fn_add_aux_total_attr

                #region FnAddAuxTotalAttr

                /// <summary>
                /// Wrapper for stored procedure fn_add_aux_total_attr.
                /// Function showing auxiliary total attribute additivity (aggregated class estimate should be equal to sum of sub-classes estimates). Hierarchy between aggregated classes and its sub-classes is defined in v_variable_hierarchy.
                /// Function input argument is:
                /// Array of attributes -- variables(FKEY to t_variable.id). All variables to be checked (aggregated class together with sub-classes) need to be passed.
                /// Minimal amount of difference [%].
                /// Whether include NULL difference -- indicating defined sub-classes missing in data.
                /// Resulting table has following columns:
                /// Auxiliary total estimation cell. FKEY to c_estimation_cell.id.
                /// Auxiliary total attribute -- variable.FKEY to t_variable.id.
                /// Aggregated class auxiliary total.
                /// Sum of sub-classes auxiliary totals(belonging to aggregated class).
                /// Attributes -- variables defined in hierarchy(v_variable_hierarchy). Array of FKEYs to t_variable.id.
                /// Attributes -- variables found in data(t_result). Array of FKEYs to t_variable.id.
                /// Relative difference between aggregated class auxiliary total and sum of sub-classes auxiliary totals.
                /// </summary>
                public static class FnAddAuxTotalAttr
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName = NfiEstaSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name = "fn_add_aux_total_attr";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"variables integer[], ",
                            $"min_diff double precision DEFAULT 0.0, ",
                            $"include_null_diff boolean DEFAULT true; ",
                            $"returns: ",
                            $"TABLE(",
                            $"estimation_cell integer, ",
                            $"variable integer, ",
                            $"aux_total double precision, ",
                            $"aux_total_sum double precision, ",
                            $"variables_def integer[], ",
                            $"variables_found integer[], ",
                            $"diff double precision)");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    $Name.estimation_cell                                AS estimation_cell,{Environment.NewLine}",
                            $"    $Name.variable                                       AS variable,{Environment.NewLine}",
                            $"    $Name.aux_total                                      AS aux_total,{Environment.NewLine}",
                            $"    $Name.aux_total_sum                                  AS aux_total_sum,{Environment.NewLine}",
                            $"    array_to_string($Name.variables_def, ';', 'NULL')    AS variables_def,{Environment.NewLine}",
                            $"    array_to_string($Name.variables_found, ';', 'NULL')  AS variables_found,{Environment.NewLine}",
                            $"    $Name.diff                                           AS diff{Environment.NewLine}",
                            $"FROM{Environment.NewLine}",
                            $"    $SchemaName.$Name(@variables, @minDiff, @includeNullDiff) AS $Name;{Environment.NewLine}");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="variables"></param>
                    /// <param name="minDiff"></param>
                    /// <param name="includeNullDiff"></param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        List<Nullable<int>> variables,
                        Nullable<double> minDiff,
                        Nullable<bool> includeNullDiff)
                    {
                        string result = SQL;

                        result = result.Replace(
                            oldValue: "@variables",
                            newValue: Functions.PrepNIntArrayArg(args: variables, dbType: "int4"));

                        result = result.Replace(
                            oldValue: "@minDiff",
                            newValue: Functions.PrepNDoubleArg(arg: minDiff));

                        result = result.Replace(
                            oldValue: "@includeNullDiff",
                            newValue: Functions.PrepNBoolArg(arg: includeNullDiff));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_add_aux_total_attr
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="variables"></param>
                    /// <param name="minDiff"></param>
                    /// <param name="includeNullDiff"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns></returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        List<Nullable<int>> variables,
                        Nullable<double> minDiff,
                        Nullable<bool> includeNullDiff,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(variables: variables, minDiff: minDiff, includeNullDiff: includeNullDiff);

                        NpgsqlParameter pVariables = new(
                            parameterName: "variables",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        NpgsqlParameter pMinDiff = new(
                            parameterName: "minDiff",
                            parameterType: NpgsqlDbType.Double);

                        NpgsqlParameter pIncludeNullDiff = new(
                            parameterName: "includeNullDiff",
                            parameterType: NpgsqlDbType.Boolean);

                        if (variables != null)
                        {
                            pVariables.Value = variables.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pVariables.Value = DBNull.Value;
                        }

                        if (minDiff != null)
                        {
                            pMinDiff.Value = (double)minDiff;
                        }
                        else
                        {
                            pMinDiff.Value = DBNull.Value;
                        }

                        if (includeNullDiff != null)
                        {
                            pIncludeNullDiff.Value = (bool)includeNullDiff;
                        }
                        else
                        {
                            pIncludeNullDiff.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteQuery(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pVariables, pMinDiff, pIncludeNullDiff);
                    }

                    #endregion Methods

                }

                #endregion FnAddAuxTotalAttr

            }

        }
    }
}