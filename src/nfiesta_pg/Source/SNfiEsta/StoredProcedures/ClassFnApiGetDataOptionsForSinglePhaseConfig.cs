﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {

            /// <summary>
            /// Stored procedures of schema nfiesta
            /// </summary>
            public static partial class NfiEstaFunctions
            {
                // fn_api_get_data_options4single_phase_config

                #region FnApiGetDataOptionsForSinglePhaseConfig

                /// <summary>
                /// Wrapper for stored procedure fn_api_get_data_options4single_phase_config.
                /// This function returns unique sets of panel and reference yearset pairs by combinations of estimation cells.
                /// Based on this data the user chooses panels and reference yearsets for each group of cells intersecting specific combination of countries,
                /// strata sets and strata. The panels and reference yearsets fully within the estimation period (refyearset_fully_within_estmation_period = TRUE)
                /// and with the maximum sample size (max_ssize_within_estimation_period = TRUE) are to be preselcted by nFIESTA GUI.
                /// </summary>
                public static class FnApiGetDataOptionsForSinglePhaseConfig
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName = NfiEstaSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name = "fn_api_get_data_options4single_phase_config";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_estimation_cells integer[], ",
                            $"_variables integer[], ",
                            $"_estimate_date_begin date, ",
                            $"_estimate_date_end date, ",
                            $"_cell_coverage_tolerance double precision DEFAULT 0.001; ",
                            $"returns: ",
                            $"TABLE(",
                            $"estimation_cells integer[], ",
                            $"country_id integer, ",
                            $"country_label character varying, ",
                            $"country_label_en character varying, ",
                            $"country_description text, ",
                            $"country_description_en text, ",
                            $"strata_set_id integer, ",
                            $"strata_set_label character varying, ",
                            $"strata_set_label_en character varying, ",
                            $"strata_set_description text, ",
                            $"strata_set_description_en text, ",
                            $"stratum_id integer, ",
                            $"stratum_label character varying, ",
                            $"stratum_label_en character varying, ",
                            $"stratum_description text, ",
                            $"stratum_description_en text, ",
                            $"panel_id integer, ",
                            $"panel_label character varying, ",
                            $"panel_label_en character varying, ",
                            $"panel_description text, ",
                            $"panel_description_en text, ",
                            $"reference_year_set_id integer, ",
                            $"reference_year_set_label character varying, ",
                            $"reference_year_set_label_en character varying, ",
                            $"reference_year_set_description text, ",
                            $"reference_year_set_description_en text, ",
                            $"reference_date_begin date, ",
                            $"reference_date_end date, ",
                            $"refyearset_fully_within_estimation_period boolean, ",
                            $"share_of_refyearset_intersected_by_estimation_period double precision, ",
                            $"share_of_estimation_period_intersected_by_refyearset double precision, ",
                            $"ssize integer, ",
                            $"max_ssize_within_estimation_period_and_refyearset boolean)");


                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return String.Concat(
                                $"SELECT{Environment.NewLine}",
                                $"    {TFnApiGetDataOptionsForSinglePhaseConfigList.ColId.SQL(cols: TFnApiGetDataOptionsForSinglePhaseConfigList.Cols, alias: Name)}",
                                $"    {TFnApiGetDataOptionsForSinglePhaseConfigList.ColEstimationCells.SQL(cols: TFnApiGetDataOptionsForSinglePhaseConfigList.Cols, alias: Name)}",
                                $"    {TFnApiGetDataOptionsForSinglePhaseConfigList.ColCountryId.SQL(cols: TFnApiGetDataOptionsForSinglePhaseConfigList.Cols, alias: Name)}",
                                $"    {TFnApiGetDataOptionsForSinglePhaseConfigList.ColCountryLabelCs.SQL(cols: TFnApiGetDataOptionsForSinglePhaseConfigList.Cols, alias: Name)}",
                                $"    {TFnApiGetDataOptionsForSinglePhaseConfigList.ColCountryDescriptionCs.SQL(cols: TFnApiGetDataOptionsForSinglePhaseConfigList.Cols, alias: Name)}",
                                $"    {TFnApiGetDataOptionsForSinglePhaseConfigList.ColCountryLabelEn.SQL(cols: TFnApiGetDataOptionsForSinglePhaseConfigList.Cols, alias: Name)}",
                                $"    {TFnApiGetDataOptionsForSinglePhaseConfigList.ColCountryDescriptionEn.SQL(cols: TFnApiGetDataOptionsForSinglePhaseConfigList.Cols, alias: Name)}",
                                $"    {TFnApiGetDataOptionsForSinglePhaseConfigList.ColStrataSetId.SQL(cols: TFnApiGetDataOptionsForSinglePhaseConfigList.Cols, alias: Name)}",
                                $"    {TFnApiGetDataOptionsForSinglePhaseConfigList.ColStrataSetLabelCs.SQL(cols: TFnApiGetDataOptionsForSinglePhaseConfigList.Cols, alias: Name)}",
                                $"    {TFnApiGetDataOptionsForSinglePhaseConfigList.ColStrataSetDescriptionCs.SQL(cols: TFnApiGetDataOptionsForSinglePhaseConfigList.Cols, alias: Name)}",
                                $"    {TFnApiGetDataOptionsForSinglePhaseConfigList.ColStrataSetLabelEn.SQL(cols: TFnApiGetDataOptionsForSinglePhaseConfigList.Cols, alias: Name)}",
                                $"    {TFnApiGetDataOptionsForSinglePhaseConfigList.ColStrataSetDescriptionEn.SQL(cols: TFnApiGetDataOptionsForSinglePhaseConfigList.Cols, alias: Name)}",
                                $"    {TFnApiGetDataOptionsForSinglePhaseConfigList.ColStratumId.SQL(cols: TFnApiGetDataOptionsForSinglePhaseConfigList.Cols, alias: Name)}",
                                $"    {TFnApiGetDataOptionsForSinglePhaseConfigList.ColStratumLabelCs.SQL(cols: TFnApiGetDataOptionsForSinglePhaseConfigList.Cols, alias: Name)}",
                                $"    {TFnApiGetDataOptionsForSinglePhaseConfigList.ColStratumDescriptionCs.SQL(cols: TFnApiGetDataOptionsForSinglePhaseConfigList.Cols, alias: Name)}",
                                $"    {TFnApiGetDataOptionsForSinglePhaseConfigList.ColStratumLabelEn.SQL(cols: TFnApiGetDataOptionsForSinglePhaseConfigList.Cols, alias: Name)}",
                                $"    {TFnApiGetDataOptionsForSinglePhaseConfigList.ColStratumDescriptionEn.SQL(cols: TFnApiGetDataOptionsForSinglePhaseConfigList.Cols, alias: Name)}",
                                $"    {TFnApiGetDataOptionsForSinglePhaseConfigList.ColPanelId.SQL(cols: TFnApiGetDataOptionsForSinglePhaseConfigList.Cols, alias: Name)}",
                                $"    {TFnApiGetDataOptionsForSinglePhaseConfigList.ColPanelLabelCs.SQL(cols: TFnApiGetDataOptionsForSinglePhaseConfigList.Cols, alias: Name)}",
                                $"    {TFnApiGetDataOptionsForSinglePhaseConfigList.ColPanelDescriptionCs.SQL(cols: TFnApiGetDataOptionsForSinglePhaseConfigList.Cols, alias: Name)}",
                                $"    {TFnApiGetDataOptionsForSinglePhaseConfigList.ColPanelLabelEn.SQL(cols: TFnApiGetDataOptionsForSinglePhaseConfigList.Cols, alias: Name)}",
                                $"    {TFnApiGetDataOptionsForSinglePhaseConfigList.ColPanelDescriptionEn.SQL(cols: TFnApiGetDataOptionsForSinglePhaseConfigList.Cols, alias: Name)}",
                                $"    {TFnApiGetDataOptionsForSinglePhaseConfigList.ColReferenceYearSetId.SQL(cols: TFnApiGetDataOptionsForSinglePhaseConfigList.Cols, alias: Name)}",
                                $"    {TFnApiGetDataOptionsForSinglePhaseConfigList.ColReferenceYearSetLabelCs.SQL(cols: TFnApiGetDataOptionsForSinglePhaseConfigList.Cols, alias: Name)}",
                                $"    {TFnApiGetDataOptionsForSinglePhaseConfigList.ColReferenceYearSetDescriptionCs.SQL(cols: TFnApiGetDataOptionsForSinglePhaseConfigList.Cols, alias: Name)}",
                                $"    {TFnApiGetDataOptionsForSinglePhaseConfigList.ColReferenceYearSetLabelEn.SQL(cols: TFnApiGetDataOptionsForSinglePhaseConfigList.Cols, alias: Name)}",
                                $"    {TFnApiGetDataOptionsForSinglePhaseConfigList.ColReferenceYearSetDescriptionEn.SQL(cols: TFnApiGetDataOptionsForSinglePhaseConfigList.Cols, alias: Name)}",
                                $"    {TFnApiGetDataOptionsForSinglePhaseConfigList.ColReferenceDateBegin.SQL(cols: TFnApiGetDataOptionsForSinglePhaseConfigList.Cols, alias: Name)}",
                                $"    {TFnApiGetDataOptionsForSinglePhaseConfigList.ColReferenceDateEnd.SQL(cols: TFnApiGetDataOptionsForSinglePhaseConfigList.Cols, alias: Name)}",
                                $"    {TFnApiGetDataOptionsForSinglePhaseConfigList.ColRefYearSetFullyWithinEstimationPeriod.SQL(cols: TFnApiGetDataOptionsForSinglePhaseConfigList.Cols, alias: Name)}",
                                $"    {TFnApiGetDataOptionsForSinglePhaseConfigList.ColShareOfRefYearSetIntersectedByEstimationPeriod.SQL(cols: TFnApiGetDataOptionsForSinglePhaseConfigList.Cols, alias: Name)}",
                                $"    {TFnApiGetDataOptionsForSinglePhaseConfigList.ColShareOfEstimationPeriodIntersectedByRefYearSet.SQL(cols: TFnApiGetDataOptionsForSinglePhaseConfigList.Cols, alias: Name)}",
                                $"    {TFnApiGetDataOptionsForSinglePhaseConfigList.ColSSize.SQL(cols: TFnApiGetDataOptionsForSinglePhaseConfigList.Cols, alias: Name)}",
                                $"    {TFnApiGetDataOptionsForSinglePhaseConfigList.ColMaxSSizeWithinEstimationPeriodAndRefYearSet.SQL(cols: TFnApiGetDataOptionsForSinglePhaseConfigList.Cols, alias: Name, isLastOne: true)}",
                                $"FROM{Environment.NewLine}",
                                $"    {SchemaName}.{Name}(@estimationCellsIds, @variablesIds, @estimateDateBegin, @estimateDateEnd, @cellCoverageTolerance) AS {Name} {Environment.NewLine}",
                                $"ORDER BY{Environment.NewLine}",
                                $"    {Name}.{TFnApiGetDataOptionsForSinglePhaseConfigList.ColCountryId.DbName},{Environment.NewLine}",
                                $"    {Name}.{TFnApiGetDataOptionsForSinglePhaseConfigList.ColStrataSetId.DbName},{Environment.NewLine}",
                                $"    {Name}.{TFnApiGetDataOptionsForSinglePhaseConfigList.ColStratumId.DbName},{Environment.NewLine}",
                                $"    {Name}.{TFnApiGetDataOptionsForSinglePhaseConfigList.ColPanelId.DbName},{Environment.NewLine}",
                                $"    {Name}.{TFnApiGetDataOptionsForSinglePhaseConfigList.ColReferenceYearSetId.DbName};{Environment.NewLine}"
                                );
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="estimationCellsIds">List of estimation cells identifiers</param>
                    /// <param name="variablesIds">List of attribute categories identifiers</param>
                    /// <param name="estimateDateBegin">The beginning of the time to witch the estimate is calculated</param>
                    /// <param name="estimateDateEnd">The end of the time to witch the estimate is calculated</param>
                    /// <param name="cellCoverageTolerance">Cell coverage tolerance</param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        List<Nullable<int>> estimationCellsIds,
                        List<Nullable<int>> variablesIds,
                        Nullable<DateTime> estimateDateBegin,
                        Nullable<DateTime> estimateDateEnd,
                        Nullable<double> cellCoverageTolerance)
                    {
                        string result = SQL;

                        result = result.Replace(
                           oldValue: "@estimationCellsIds",
                           newValue: Functions.PrepNIntArrayArg(args: estimationCellsIds, dbType: "int4"));

                        result = result.Replace(
                            oldValue: "@variablesIds",
                            newValue: Functions.PrepNIntArrayArg(args: variablesIds, dbType: "int4"));

                        result = result.Replace(
                            oldValue: "@estimateDateBegin",
                            newValue: Functions.PrepNDateTimeArg(arg: estimateDateBegin));

                        result = result.Replace(
                            oldValue: "@estimateDateEnd",
                            newValue: Functions.PrepNDateTimeArg(arg: estimateDateEnd));

                        result = result.Replace(
                           oldValue: "@cellCoverageTolerance",
                           newValue: Functions.PrepNDoubleArg(arg: cellCoverageTolerance));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_api_get_data_options4single_phase_config
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="estimationCellsIds">List of estimation cells identifiers</param>
                    /// <param name="variablesIds">List of attribute categories identifiers</param>
                    /// <param name="estimateDateBegin">The beginning of the time to witch the estimate is calculated</param>
                    /// <param name="estimateDateEnd">The end of the time to witch the estimate is calculated</param>
                    /// <param name="cellCoverageTolerance">Cell coverage tolerance</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>
                    /// Data table with countries, strata sets, strata, panels, reference year sets that intersect within any estimation cell
                    /// and for which all required variables are available
                    /// </returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        List<Nullable<int>> estimationCellsIds,
                        List<Nullable<int>> variablesIds,
                        Nullable<DateTime> estimateDateBegin,
                        Nullable<DateTime> estimateDateEnd,
                        Nullable<double> cellCoverageTolerance,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(
                            estimationCellsIds: estimationCellsIds,
                            variablesIds: variablesIds,
                            estimateDateBegin: estimateDateBegin,
                            estimateDateEnd: estimateDateEnd,
                            cellCoverageTolerance: cellCoverageTolerance);

                        NpgsqlParameter pEstimationCellsIds = new(
                            parameterName: "estimationCellsIds",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        NpgsqlParameter pVariablesIds = new(
                            parameterName: "variablesIds",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        NpgsqlParameter pEstimateDateBegin = new(
                            parameterName: "estimateDateBegin",
                            parameterType: NpgsqlDbType.Date);

                        NpgsqlParameter pEstimateDateEnd = new(
                            parameterName: "estimateDateEnd",
                            parameterType: NpgsqlDbType.Date);

                        NpgsqlParameter pCellCoverageTolerance = new(
                            parameterName: "cellCoverageTolerance",
                            parameterType: NpgsqlDbType.Double);

                        if (estimationCellsIds != null)
                        {
                            pEstimationCellsIds.Value = estimationCellsIds.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pEstimationCellsIds.Value = DBNull.Value;
                        }

                        if (variablesIds != null)
                        {
                            pVariablesIds.Value = variablesIds.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pVariablesIds.Value = DBNull.Value;
                        }

                        if (estimateDateBegin != null)
                        {
                            pEstimateDateBegin.Value = estimateDateBegin;
                        }
                        else
                        {
                            pEstimateDateBegin.Value = DBNull.Value;
                        }

                        if (estimateDateEnd != null)
                        {
                            pEstimateDateEnd.Value = estimateDateEnd;
                        }
                        else
                        {
                            pEstimateDateEnd.Value = DBNull.Value;
                        }

                        if (cellCoverageTolerance != null)
                        {
                            pCellCoverageTolerance.Value = cellCoverageTolerance;
                        }
                        else
                        {
                            pCellCoverageTolerance.Value = DBNull.Value;
                        }

                        return
                            database.Postgres.ExecuteQuery(
                                sqlCommand: SQL,
                                transaction: transaction,
                                pEstimationCellsIds, pVariablesIds,
                                pEstimateDateBegin, pEstimateDateEnd,
                                pCellCoverageTolerance);
                    }

                    /// <summary>
                    /// Execute stored procedure fn_api_get_data_options4single_phase_config
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="estimationCellsIds">List of estimation cells identifiers</param>
                    /// <param name="variablesIds">List of attribute categories identifiers</param>
                    /// <param name="estimateDateBegin">The beginning of the time to witch the estimate is calculated</param>
                    /// <param name="estimateDateEnd">The end of the time to witch the estimate is calculated</param>
                    /// <param name="cellCoverageTolerance">Cell coverage tolerance</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>
                    /// List of countries, strata sets, strata, panels, reference year sets that intersect within any estimation cell
                    /// and for which all required variables are available
                    /// </returns>
                    public static TFnApiGetDataOptionsForSinglePhaseConfigList Execute(
                        NfiEstaDB database,
                        List<Nullable<int>> estimationCellsIds,
                        List<Nullable<int>> variablesIds,
                        Nullable<DateTime> estimateDateBegin,
                        Nullable<DateTime> estimateDateEnd,
                        Nullable<double> cellCoverageTolerance,
                        NpgsqlTransaction transaction = null)
                    {
                        System.Diagnostics.Stopwatch stopWatch = new();
                        stopWatch.Start();

                        DataTable data = ExecuteQuery(
                            database: database,
                            estimationCellsIds: estimationCellsIds,
                            variablesIds: variablesIds,
                            estimateDateBegin: estimateDateBegin,
                            estimateDateEnd: estimateDateEnd,
                            cellCoverageTolerance: cellCoverageTolerance,
                            transaction: transaction);
                        data.TableName = Name;

                        stopWatch.Stop();

                        return new TFnApiGetDataOptionsForSinglePhaseConfigList(
                            database: database,
                            data: data,
                            loadingTime: 0.001 * stopWatch.ElapsedMilliseconds,
                            storedProcedure: CommandText)
                        {
                            EstimationCellsIds = estimationCellsIds,
                            VariablesIds = variablesIds,
                            EstimateDateBegin = estimateDateBegin,
                            EstimateDateEnd = estimateDateEnd,
                            CellCoverageTolerance = cellCoverageTolerance
                        };
                    }

                    #endregion Methods

                }

                #endregion FnApiGetDataOptionsForSinglePhaseConfig

            }

        }
    }
}