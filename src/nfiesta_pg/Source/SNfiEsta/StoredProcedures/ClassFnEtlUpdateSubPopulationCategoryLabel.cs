﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {

            /// <summary>
            /// Stored procedures of schema nfiesta
            /// </summary>
            public static partial class NfiEstaFunctions
            {
                // fn_etl_update_sub_population_category_label

                #region FnEtlUpdateSubPopulationCategoryLabel

                /// <summary>
                /// Wrapper for stored procedure fn_etl_update_sub_population_category_label.
                /// The function updates a sub population category label
                /// in c_sub_population_category table for given sub population category.
                /// </summary>
                public static class FnEtlUpdateSubPopulationCategoryLabel
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = NfiEstaSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_etl_update_sub_population_category_label";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_sub_population_category integer, ",
                            $"_national_language character varying, ",
                            $"_label character varying; ",
                            $"returns: text");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    $SchemaName.$Name(",
                            $"@subPopulationCategory, ",
                            $"@nationalLanguage, ",
                            $"@label)::text AS $Name;");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="subPopulationCategory"></param>
                    /// <param name="nationalLanguage"></param>
                    /// <param name="label"></param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        Nullable<int> subPopulationCategory,
                        string nationalLanguage,
                        string label)
                    {
                        string result = SQL;

                        result = result.Replace(
                            oldValue: "@subPopulationCategory",
                            newValue: Functions.PrepNIntArg(arg: subPopulationCategory));

                        result = result.Replace(
                            oldValue: "@nationalLanguage",
                            newValue: Functions.PrepStringArg(arg: nationalLanguage));

                        result = result.Replace(
                            oldValue: "@label",
                            newValue: Functions.PrepStringArg(arg: label));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_etl_update_sub_population_category_label
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="subPopulationCategory"></param>
                    /// <param name="nationalLanguage"></param>
                    /// <param name="label"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> subPopulationCategory,
                        string nationalLanguage,
                        string label,
                        NpgsqlTransaction transaction = null)
                    {
                        string val = Execute(
                            database: database,
                            subPopulationCategory: subPopulationCategory,
                            nationalLanguage: nationalLanguage,
                            label: label,
                            transaction: transaction);

                        DataTable dt = new();

                        dt.Columns.Add(column: new DataColumn()
                        {
                            ColumnName = Name,
                            DataType = Type.GetType(typeName: "System.String")
                        });

                        DataRow row = dt.NewRow();

                        Functions.SetStringArg(
                            row: row,
                            name: Name,
                            val: val);

                        dt.Rows.Add(row: row);

                        return dt;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_etl_update_sub_population_category_label
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="subPopulationCategory"></param>
                    /// <param name="nationalLanguage"></param>
                    /// <param name="label"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Text</returns>
                    public static string Execute(
                        NfiEstaDB database,
                        Nullable<int> subPopulationCategory,
                        string nationalLanguage,
                        string label,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(
                            subPopulationCategory: subPopulationCategory,
                            nationalLanguage: nationalLanguage,
                            label: label);

                        NpgsqlParameter pSubPopulationCategory = new(
                            parameterName: "subPopulationCategory",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pNationalLanguage = new(
                            parameterName: "nationalLanguage",
                            parameterType: NpgsqlDbType.Varchar);

                        NpgsqlParameter pLabel = new(
                            parameterName: "label",
                            parameterType: NpgsqlDbType.Varchar);

                        if (subPopulationCategory != null)
                        {
                            pSubPopulationCategory.Value = (int)subPopulationCategory;
                        }
                        else
                        {
                            pSubPopulationCategory.Value = DBNull.Value;
                        }

                        if (nationalLanguage != null)
                        {
                            pNationalLanguage.Value = (string)nationalLanguage;
                        }
                        else
                        {
                            pNationalLanguage.Value = DBNull.Value;
                        }

                        if (label != null)
                        {
                            pLabel.Value = (string)label;
                        }
                        else
                        {
                            pLabel.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteScalar(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pSubPopulationCategory, pNationalLanguage, pLabel);
                    }

                    #endregion Methods

                }

                #endregion FnEtlUpdateSubPopulationCategoryLabel

            }

        }
    }
}