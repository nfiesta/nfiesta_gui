﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {

            /// <summary>
            /// Stored procedures of schema nfiesta
            /// </summary>
            public static partial class NfiEstaFunctions
            {
                // fn_etl_delete_target_variable2topic

                #region FnEtlDeleteTargetVariableToTopic

                /// <summary>
                /// Wrapper for stored procedure fn_etl_delete_target_variable2topic.
                /// Function delete record from cm_tvariable2topic table.
                /// </summary>
                public static class FnEtlDeleteTargetVariableToTopic
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = NfiEstaSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_etl_delete_target_variable2topic";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: _target_variable integer, ",
                            $"_topics integer[]; ",
                            $"returns: void");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                                $"    $SchemaName.$Name(",
                                $"@targetVariable, @topics);");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="targetVariable">Target variable identifier</param>
                    /// <param name="topics">List of topics</param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        Nullable<int> targetVariable,
                        List<Nullable<int>> topics)
                    {
                        string result = SQL;

                        result = result.Replace(
                             oldValue: "@targetVariable",
                             newValue: Functions.PrepNIntArg(arg: targetVariable));

                        result = result.Replace(
                            oldValue: "@topics",
                            newValue: Functions.PrepNIntArrayArg(args: topics, dbType: "int4"));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_etl_delete_target_variable2topic
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="targetVariable">Target variable identifier</param>
                    /// <param name="topics">List of topics</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Empty data table</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> targetVariable,
                        List<Nullable<int>> topics,
                        NpgsqlTransaction transaction = null)
                    {
                        Execute(
                            database: database,
                            targetVariable: targetVariable,
                            topics: topics,
                            transaction: transaction);

                        DataTable dt = new();

                        dt.Columns.Add(column: new DataColumn()
                        {
                            ColumnName = Name,
                            DataType = Type.GetType(typeName: "System.Int32")
                        });

                        return dt;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_etl_delete_target_variable2topic
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="targetVariable">Target variable identifier</param>
                    /// <param name="topics">List of topics</param>
                    /// <param name="transaction">Transaction</param>
                    public static void Execute(
                        NfiEstaDB database,
                        Nullable<int> targetVariable,
                        List<Nullable<int>> topics,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(targetVariable: targetVariable, topics: topics);

                        NpgsqlParameter pTargetVariable = new(
                            parameterName: "targetVariable",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pTopics = new(
                            parameterName: "topics",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        if (targetVariable != null)
                        {
                            pTargetVariable.Value = (int)targetVariable;
                        }
                        else
                        {
                            pTargetVariable.Value = DBNull.Value;
                        }

                        if (topics != null)
                        {
                            pTopics.Value = topics.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pTopics.Value = DBNull.Value;
                        }

                        database.Postgres.ExecuteNonQuery(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pTargetVariable, pTopics);
                    }

                    #endregion Methods

                }

                #endregion FnEtlDeleteTargetVariableToTopic

            }

        }
    }
}