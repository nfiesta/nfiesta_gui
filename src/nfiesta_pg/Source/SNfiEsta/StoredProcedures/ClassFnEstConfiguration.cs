﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {

            /// <summary>
            /// Stored procedures of schema nfiesta
            /// </summary>
            public static partial class NfiEstaFunctions
            {
                // fn_est_configuration

                #region FnEstConfiguration

                /// <summary>
                /// Wrapper for stored procedure fn_est_configuration.
                /// Handles data between fn_get_panels_in_estimation_cells and fn_1p_est_configuration.
                /// </summary>
                public static class FnEstConfiguration
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName = NfiEstaSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name = "fn_est_configuration";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_estimation_cell integer, ",
                            $"_estimation_period integer, ",
                            $"_note character varying, ",
                            $"_target_variable integer, ",
                            $"_panels integer[] DEFAULT NULL::integer[]; ",
                            $"returns: SETOF integer");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return String.Concat(
                                $"SELECT {SchemaName}.{Name}",
                                $"(@estimationCell, @estimationPeriod, @note, @targetVariable, @panels);",
                                $"{Environment.NewLine}");
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="estimationCell"></param>
                    /// <param name="estimationPeriod"></param>
                    /// <param name="note"></param>
                    /// <param name="targetVariable"></param>
                    /// <param name="panels"></param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        Nullable<int> estimationCell,
                        Nullable<int> estimationPeriod,
                        string note,
                        Nullable<int> targetVariable,
                        List<Nullable<int>> panels)
                    {
                        string result = SQL;

                        result = result.Replace(
                            oldValue: "@estimationCell",
                            newValue: Functions.PrepNIntArg(arg: estimationCell));

                        result = result.Replace(
                            oldValue: "@estimationPeriod",
                            newValue: Functions.PrepNIntArg(arg: estimationPeriod));

                        result = result.Replace(
                            oldValue: "@note",
                            newValue: Functions.PrepStringArg(arg: note));

                        result = result.Replace(
                            oldValue: "@targetVariable",
                            newValue: Functions.PrepNIntArg(arg: targetVariable));

                        result = result.Replace(
                            oldValue: "@panels",
                            newValue: Functions.PrepNIntArrayArg(args: panels, dbType: "int4"));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_est_configuration
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="estimationCell"></param>
                    /// <param name="estimationPeriod"></param>
                    /// <param name="note"></param>
                    /// <param name="targetVariable"></param>
                    /// <param name="panels"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns></returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> estimationCell,
                        Nullable<int> estimationPeriod,
                        string note,
                        Nullable<int> targetVariable,
                        List<Nullable<int>> panels,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(
                            estimationCell: estimationCell,
                            estimationPeriod: estimationPeriod,
                            note: note,
                            targetVariable: targetVariable,
                            panels: panels);

                        NpgsqlParameter pEstimationCell = new(
                           parameterName: "estimationCell",
                           parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pEstimationPeriod = new(
                           parameterName: "estimationPeriod",
                           parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pNote = new(
                           parameterName: "note",
                           parameterType: NpgsqlDbType.Varchar);

                        NpgsqlParameter pTargetVariable = new(
                           parameterName: "targetVariable",
                           parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pPanels = new(
                            parameterName: "panels",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        if (estimationCell != null)
                        {
                            pEstimationCell.Value = (int)estimationCell;
                        }
                        else
                        {
                            pEstimationCell.Value = DBNull.Value;
                        }

                        if (estimationPeriod != null)
                        {
                            pEstimationPeriod.Value = (int)estimationPeriod;
                        }
                        else
                        {
                            pEstimationPeriod.Value = DBNull.Value;
                        }

                        if (note != null)
                        {
                            pNote.Value = (string)note;
                        }
                        else
                        {
                            pNote.Value = DBNull.Value;
                        }

                        if (targetVariable != null)
                        {
                            pTargetVariable.Value = (int)targetVariable;
                        }
                        else
                        {
                            pTargetVariable.Value = DBNull.Value;
                        }

                        if (panels != null)
                        {
                            pPanels.Value = panels.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pPanels.Value = DBNull.Value;
                        }

                        return
                            database.Postgres.ExecuteQuery(
                                sqlCommand: SQL,
                                transaction: transaction,
                                pEstimationCell, pEstimationPeriod, pNote, pTargetVariable, pPanels);
                    }

                    /// <summary>
                    /// Execute stored procedure fn_get_heighest_estimation_cell
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="estimationCell"></param>
                    /// <param name="estimationPeriod"></param>
                    /// <param name="note"></param>
                    /// <param name="targetVariable"></param>
                    /// <param name="panels"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns></returns>
                    public static List<Nullable<int>> Execute(
                        NfiEstaDB database,
                        Nullable<int> estimationCell,
                        Nullable<int> estimationPeriod,
                        string note,
                        Nullable<int> targetVariable,
                        List<Nullable<int>> panels,
                        NpgsqlTransaction transaction = null)
                    {
                        return
                            [..
                                ExecuteQuery(
                                    database: database,
                                    estimationCell: estimationCell,
                                    estimationPeriod: estimationPeriod,
                                    note: note,
                                    targetVariable: targetVariable,
                                    panels: panels,
                                    transaction: transaction)
                                .AsEnumerable()
                                .Select(a => a.Field<Nullable<int>>(columnIndex: 0))
                            ];
                    }

                    #endregion Methods

                }

                #endregion FnEstConfiguration

            }

        }
    }
}