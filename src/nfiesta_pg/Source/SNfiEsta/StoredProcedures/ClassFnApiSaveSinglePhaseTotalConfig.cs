﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {

            /// <summary>
            /// Stored procedures of schema nfiesta
            /// </summary>
            public static partial class NfiEstaFunctions
            {
                // fn_api_save_single_phase_total_config

                #region FnApiSaveSinglePhaseTotalConfig

                /// <summary>
                /// Wrapper for stored procedure fn_api_save_single_phase_total_config
                /// The function saves configuration of a single phase total estimator
                /// into tables nfiesta.t_total_estimate_conf and nfiesta.t_estimate_conf.
                /// </summary>
                public static partial class FnApiSaveSinglePhaseTotalConfig
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = NfiEstaSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_api_save_single_phase_total_config";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_estimation_period integer, ",
                            $"_estimation_cell integer, ",
                            $"_variable integer, ",
                            $"_aggregate_panel_refyearset_group integer, ",
                            $"_panel_refyearset_groups_by_strata integer[]; ",
                            $"returns: integer");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    {SchemaName}.{Name}(",
                            $"@estimationPeriodId, ",
                            $"@estimationCellId, ",
                            $"@variableId, ",
                            $"@aggregatePanelRefYearSetGroup, ",
                            $"@panelRefYearSetGroupsByStrata);{Environment.NewLine}");
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="estimationPeriodId">Estimation period identifier</param>
                    /// <param name="estimationCellId">Estimation cell identifier</param>
                    /// <param name="variableId">Attribute category identifier</param>
                    /// <param name="aggregatePanelRefYearSetGroup">Panel reference year set group identifier</param>
                    /// <param name="panelRefYearSetGroupsByStrata">List of panel reference year set groups by strata identifiers</param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        Nullable<int> estimationPeriodId,
                        Nullable<int> estimationCellId,
                        Nullable<int> variableId,
                        Nullable<int> aggregatePanelRefYearSetGroup,
                        List<Nullable<int>> panelRefYearSetGroupsByStrata)
                    {
                        string result = SQL;

                        result = result.Replace(
                            oldValue: "@estimationPeriodId",
                            newValue: Functions.PrepNIntArg(arg: estimationPeriodId));

                        result = result.Replace(
                            oldValue: "@estimationCellId",
                            newValue: Functions.PrepNIntArg(arg: estimationCellId));

                        result = result.Replace(
                           oldValue: "@variableId",
                           newValue: Functions.PrepNIntArg(arg: variableId));

                        result = result.Replace(
                           oldValue: "@aggregatePanelRefYearSetGroup",
                           newValue: Functions.PrepNIntArg(arg: aggregatePanelRefYearSetGroup));

                        result = result.Replace(
                            oldValue: "@panelRefYearSetGroupsByStrata",
                            newValue: Functions.PrepNIntArrayArg(args: panelRefYearSetGroupsByStrata, dbType: "int4"));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_api_save_single_phase_total_config
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="estimationPeriodId">Estimation period identifier</param>
                    /// <param name="estimationCellId">Estimation cell identifier</param>
                    /// <param name="variableId">Attribute category identifier</param>
                    /// <param name="aggregatePanelRefYearSetGroup">Panel reference year set group identifier</param>
                    /// <param name="panelRefYearSetGroupsByStrata">List of panel reference year set groups by strata identifiers</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table with total estimate configuration identifier</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> estimationPeriodId,
                        Nullable<int> estimationCellId,
                        Nullable<int> variableId,
                        Nullable<int> aggregatePanelRefYearSetGroup,
                        List<Nullable<int>> panelRefYearSetGroupsByStrata,
                        NpgsqlTransaction transaction = null)
                    {
                        return ExecuteQuery(
                            connection: database.Postgres,
                            estimationPeriodId: estimationPeriodId,
                            estimationCellId: estimationCellId,
                            variableId: variableId,
                            aggregatePanelRefYearSetGroup: aggregatePanelRefYearSetGroup,
                            panelRefYearSetGroupsByStrata: panelRefYearSetGroupsByStrata,
                            transaction: transaction);
                    }

                    /// <summary>
                    /// Execute stored procedure fn_api_save_single_phase_total_config
                    /// </summary>
                    /// <param name="connection">Database connection</param>
                    /// <param name="estimationPeriodId">Estimation period identifier</param>
                    /// <param name="estimationCellId">Estimation cell identifier</param>
                    /// <param name="variableId">Attribute category identifier</param>
                    /// <param name="aggregatePanelRefYearSetGroup">Panel reference year set group identifier</param>
                    /// <param name="panelRefYearSetGroupsByStrata">List of panel reference year set groups by strata identifiers</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table with total estimate configuration identifier</returns>
                    public static DataTable ExecuteQuery(
                        ZaJi.PostgreSQL.PostgreSQLWrapper connection,
                        Nullable<int> estimationPeriodId,
                        Nullable<int> estimationCellId,
                        Nullable<int> variableId,
                        Nullable<int> aggregatePanelRefYearSetGroup,
                        List<Nullable<int>> panelRefYearSetGroupsByStrata,
                        NpgsqlTransaction transaction = null)
                    {
                        Nullable<int> val = Execute(
                            connection: connection,
                            estimationPeriodId: estimationPeriodId,
                            estimationCellId: estimationCellId,
                            variableId: variableId,
                            aggregatePanelRefYearSetGroup: aggregatePanelRefYearSetGroup,
                            panelRefYearSetGroupsByStrata: panelRefYearSetGroupsByStrata,
                            transaction: transaction);

                        DataTable dt = new();

                        dt.Columns.Add(column: new DataColumn()
                        {
                            ColumnName = Name,
                            DataType = Type.GetType(typeName: "System.Int32")
                        });

                        DataRow row = dt.NewRow();

                        Functions.SetNIntArg(
                            row: row,
                            name: Name,
                            val: val);

                        dt.Rows.Add(row: row);

                        return dt;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_api_save_single_phase_total_config
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="estimationPeriodId">Estimation period identifier</param>
                    /// <param name="estimationCellId">Estimation cell identifier</param>
                    /// <param name="variableId">Attribute category identifier</param>
                    /// <param name="aggregatePanelRefYearSetGroup">Panel reference year set group identifier</param>
                    /// <param name="panelRefYearSetGroupsByStrata">List of panel reference year set groups by strata identifiers</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Total estimate configuration identifier</returns>
                    public static Nullable<int> Execute(
                        NfiEstaDB database,
                        Nullable<int> estimationPeriodId,
                        Nullable<int> estimationCellId,
                        Nullable<int> variableId,
                        Nullable<int> aggregatePanelRefYearSetGroup,
                        List<Nullable<int>> panelRefYearSetGroupsByStrata,
                        NpgsqlTransaction transaction = null)
                    {
                        return Execute(
                            connection: database.Postgres,
                            estimationPeriodId: estimationPeriodId,
                            estimationCellId: estimationCellId,
                            variableId: variableId,
                            aggregatePanelRefYearSetGroup: aggregatePanelRefYearSetGroup,
                            panelRefYearSetGroupsByStrata: panelRefYearSetGroupsByStrata,
                            transaction: transaction);
                    }

                    /// <summary>
                    /// Execute stored procedure fn_api_save_single_phase_total_config
                    /// </summary>
                    /// <param name="connection">Database connection</param>
                    /// <param name="estimationPeriodId">Estimation period identifier</param>
                    /// <param name="estimationCellId">Estimation cell identifier</param>
                    /// <param name="variableId">Attribute category identifier</param>
                    /// <param name="aggregatePanelRefYearSetGroup">Panel reference year set group identifier</param>
                    /// <param name="panelRefYearSetGroupsByStrata">List of panel reference year set groups by strata identifiers</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Total estimate configuration identifier</returns>
                    public static Nullable<int> Execute(
                        ZaJi.PostgreSQL.PostgreSQLWrapper connection,
                        Nullable<int> estimationPeriodId,
                        Nullable<int> estimationCellId,
                        Nullable<int> variableId,
                        Nullable<int> aggregatePanelRefYearSetGroup,
                        List<Nullable<int>> panelRefYearSetGroupsByStrata,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(
                            estimationPeriodId: estimationPeriodId,
                            estimationCellId: estimationCellId,
                            variableId: variableId,
                            aggregatePanelRefYearSetGroup: aggregatePanelRefYearSetGroup,
                            panelRefYearSetGroupsByStrata: panelRefYearSetGroupsByStrata);

                        NpgsqlParameter pEstimationPeriodId = new(
                            parameterName: "estimationPeriodId",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pEstimationCellId = new(
                           parameterName: "estimationCellId",
                           parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pVariableId = new(
                           parameterName: "variableId",
                           parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pAggregatePanelRefYearSetGroup = new(
                           parameterName: "aggregatePanelRefYearSetGroup",
                           parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pPanelRefYearSetGroupsByStrata = new(
                            parameterName: "panelRefYearSetGroupsByStrata",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);


                        if (estimationPeriodId != null)
                        {
                            pEstimationPeriodId.Value = (int)estimationPeriodId;
                        }
                        else
                        {
                            pEstimationPeriodId.Value = DBNull.Value;
                        }

                        if (estimationCellId != null)
                        {
                            pEstimationCellId.Value = (int)estimationCellId;
                        }
                        else
                        {
                            pEstimationCellId.Value = DBNull.Value;
                        }

                        if (variableId != null)
                        {
                            pVariableId.Value = (int)variableId;
                        }
                        else
                        {
                            pVariableId.Value = DBNull.Value;
                        }

                        if (aggregatePanelRefYearSetGroup != null)
                        {
                            pAggregatePanelRefYearSetGroup.Value = (int)aggregatePanelRefYearSetGroup;
                        }
                        else
                        {
                            pAggregatePanelRefYearSetGroup.Value = DBNull.Value;
                        }

                        if (panelRefYearSetGroupsByStrata != null)
                        {
                            pPanelRefYearSetGroupsByStrata.Value = panelRefYearSetGroupsByStrata.ToArray();
                        }
                        else
                        {
                            pPanelRefYearSetGroupsByStrata.Value = DBNull.Value;
                        }

                        string strResult = connection?.ExecuteScalar(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pEstimationPeriodId, pEstimationCellId, pVariableId,
                            pAggregatePanelRefYearSetGroup, pPanelRefYearSetGroupsByStrata);

                        if (String.IsNullOrEmpty(value: strResult))
                        {
                            return null;
                        }
                        else
                        {
                            if (Int32.TryParse(s: strResult, result: out int intResult))
                            {
                                return intResult;
                            }
                            else
                            {
                                return null;
                            }
                        }
                    }

                    #endregion Methods

                }

                #endregion FnApiSaveSinglePhaseTotalConfig

            }

        }
    }
}