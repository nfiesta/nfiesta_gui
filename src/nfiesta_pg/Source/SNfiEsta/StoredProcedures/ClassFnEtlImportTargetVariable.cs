﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {

            /// <summary>
            /// Stored procedures of schema nfiesta
            /// </summary>
            public static partial class NfiEstaFunctions
            {
                // fn_etl_import_target_variable

                #region FnEtlImportTargetVariable

                /// <summary>
                /// Wrapper for stored procedure fn_etl_import_target_variable.
                /// Function inserts a record into table c_target_variable based on given parameters.
                /// </summary>
                public static class FnEtlImportTargetVariable
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName = NfiEstaSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name = "fn_etl_import_target_variable";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_metadata json; ",
                            $"returns: integer");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    $SchemaName.$Name(@metadata)::integer AS $Name;{Environment.NewLine}");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="metadata">Metadata</param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(string metadata)
                    {
                        string result = SQL;

                        result = result.Replace(
                            oldValue: "@metadata",
                            newValue: Functions.PrepStringArg(arg: metadata));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_etl_import_target_variable
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="metadata">Metadata</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        string metadata,
                        NpgsqlTransaction transaction = null)
                    {
                        Nullable<int> val = Execute(
                            database: database,
                            metadata: metadata,
                            transaction: transaction);

                        DataTable dt = new();

                        dt.Columns.Add(column: new DataColumn()
                        {
                            ColumnName = Name,
                            DataType = Type.GetType(typeName: "System.Int32")
                        });

                        DataRow row = dt.NewRow();

                        Functions.SetNIntArg(
                            row: row,
                            name: Name,
                            val: val);

                        dt.Rows.Add(row: row);

                        return dt;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_etl_import_target_variable
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="metadata">Metadata</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>integer</returns>
                    public static Nullable<int> Execute(
                        NfiEstaDB database,
                        string metadata,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(metadata: metadata);

                        NpgsqlParameter pMetadata = new(
                            parameterName: "metadata",
                            parameterType: NpgsqlDbType.Json);

                        if (metadata != null)
                        {
                            pMetadata.Value = (string)metadata;
                        }
                        else
                        {
                            pMetadata.Value = DBNull.Value;
                        }

                        string strResult = database.Postgres.ExecuteScalar(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pMetadata);

                        if (String.IsNullOrEmpty(value: strResult))
                        {
                            return null;
                        }
                        else
                        {
                            if (Int32.TryParse(s: strResult, result: out int intResult))
                            {
                                return intResult;
                            }
                            else
                            {
                                return null;
                            }
                        }
                    }

                    #endregion Methods

                }

                #endregion FnEtlImportTargetVariable

            }

        }
    }
}