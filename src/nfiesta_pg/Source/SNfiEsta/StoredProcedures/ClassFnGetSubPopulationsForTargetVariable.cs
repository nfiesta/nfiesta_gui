﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {

            /// <summary>
            /// Stored procedures of schema nfiesta
            /// </summary>
            public static partial class NfiEstaFunctions
            {
                // fn_get_sub_populations4target_variable

                #region FnGetSubPopulationsForTargetVariable

                /// <summary>
                /// Wrapper for stored procedure fn_get_sub_populations4target_variable.
                /// Function returns table with sub populations constrained by the availability
                /// for a given target variable and optionally by a given sub population of numerator
                /// if the target variable comes in the meaning of the denominator.
                /// </summary>
                public static class FnGetSubPopulationsForTargetVariable
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = NfiEstaSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_get_sub_populations4target_variable";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_target_variable integer, ",
                            $"_numerator_sub_population integer DEFAULT NULL::integer; ",
                            $"returns: ",
                            $"TABLE(",
                            $"id integer, ",
                            $"label character varying, ",
                            $"description text, ",
                            $"label_en character varying, ",
                            $"description_en text)");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    {SubPopulationList.ColId.SQL(SubPopulationList.Cols, Name)}",
                            $"    {SubPopulationList.ColLabelCs.SQL(SubPopulationList.Cols, Name)}",
                            $"    {SubPopulationList.ColDescriptionCs.SQL(SubPopulationList.Cols, Name)}",
                            $"    {SubPopulationList.ColLabelEn.SQL(SubPopulationList.Cols, Name)}",
                            $"    {SubPopulationList.ColDescriptionEn.SQL(SubPopulationList.Cols, Name, isLastOne: true)}",
                            $"FROM{Environment.NewLine}",
                            $"    $SchemaName.$Name(",
                            $"@targetVariableId, ",
                            $"@numeratorSubPopulationId) AS $Name{Environment.NewLine}",
                            $"ORDER BY {SubPopulationList.ColId.DbName};");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="targetVariableId">Target variable identifier</param>
                    /// <param name="numeratorSubPopulationId">Subpopulation identifier</param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        Nullable<int> targetVariableId,
                        Nullable<int> numeratorSubPopulationId = null)
                    {
                        string result = SQL;

                        result = result.Replace(
                           oldValue: "@targetVariableId",
                           newValue: Functions.PrepNIntArg(arg: targetVariableId));

                        result = result.Replace(
                            oldValue: "@numeratorSubPopulationId",
                            newValue: Functions.PrepNIntArg(arg: numeratorSubPopulationId));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_get_sub_populations4target_variable
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="targetVariableId">Target variable identifier</param>
                    /// <param name="numeratorSubPopulationId">Subpopulation identifier</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table with records from data table of subpopulations
                    /// (identifier, label, description, label_en and description_en)</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> targetVariableId,
                        Nullable<int> numeratorSubPopulationId = null,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(
                            targetVariableId: targetVariableId,
                            numeratorSubPopulationId: numeratorSubPopulationId);

                        NpgsqlParameter pTargetVariableId = new(
                            parameterName: "targetVariableId",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pNumeratorSubPopulationId = new(
                            parameterName: "numeratorSubPopulationId",
                            parameterType: NpgsqlDbType.Integer);

                        if (targetVariableId != null)
                        {
                            pTargetVariableId.Value = (int)targetVariableId;
                        }
                        else
                        {
                            pTargetVariableId.Value = DBNull.Value;
                        }

                        if (numeratorSubPopulationId != null)
                        {
                            pNumeratorSubPopulationId.Value = (int)numeratorSubPopulationId;
                        }
                        else
                        {
                            pNumeratorSubPopulationId.Value = DBNull.Value;
                        }

                        return
                            database.Postgres.ExecuteQuery(
                                sqlCommand: SQL,
                                transaction: transaction,
                                pTargetVariableId, pNumeratorSubPopulationId);
                    }

                    /// <summary>
                    /// Execute stored procedure fn_get_sub_populations4target_variable
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="targetVariableId">Target variable identifier</param>
                    /// <param name="numeratorSubPopulationId">Subpopulation identifier</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>List of subpopulations</returns>
                    public static SubPopulationList Execute(
                        NfiEstaDB database,
                        Nullable<int> targetVariableId,
                        Nullable<int> numeratorSubPopulationId = null,
                        NpgsqlTransaction transaction = null)
                    {
                        return new SubPopulationList(
                            database: database,
                            data: ExecuteQuery(
                                database: database,
                                targetVariableId: targetVariableId,
                                numeratorSubPopulationId: numeratorSubPopulationId,
                                transaction: transaction));
                    }

                    #endregion Methods

                }

                #endregion FnGetSubPopulationsForTargetVariable

            }
        }
    }
}