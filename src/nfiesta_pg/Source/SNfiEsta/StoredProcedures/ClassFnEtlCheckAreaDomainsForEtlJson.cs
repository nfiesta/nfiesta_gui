﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {

            /// <summary>
            /// Stored procedures of schema nfiesta
            /// </summary>
            public static partial class NfiEstaFunctions
            {
                // fn_etl_check_area_domains4etl_json

                #region FnEtlCheckAreaDomainsForEtlJson

                /// <summary>
                /// Wrapper for stored procedure fn_etl_check_area_domains4etl_json.
                /// The function returns one record of area domain if input area domain label was paired in target DB or the function returns record(s) of area domains that are possible to pair in target DB. Output is in JSON format.
                /// </summary>
                public static class FnEtlCheckAreaDomainsForEtlJson
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName = NfiEstaSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name = "fn_etl_check_area_domains4etl_json";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_area_domains json, ",
                            $"_etl_id integer[] DEFAULT NULL::integer[]; ",
                            $"returns: json");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    $SchemaName.$Name(@areaDomains, @etlId)::text AS {Name};{Environment.NewLine}");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="areaDomains">Json</param>
                    /// <param name="etlId">List of etl identifiers</param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        string areaDomains,
                        List<Nullable<int>> etlId)
                    {
                        string result = SQL;

                        result = result.Replace(
                           oldValue: "@areaDomains",
                           newValue: Functions.PrepStringArg(arg: areaDomains));

                        result = result.Replace(
                           oldValue: "@etlId",
                           newValue: Functions.PrepNIntArrayArg(args: etlId, dbType: "int4"));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_etl_check_area_domains4etl_json
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="areaDomains">Json</param>
                    /// <param name="etlId">List of etl identifiers</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        string areaDomains,
                        List<Nullable<int>> etlId,
                        NpgsqlTransaction transaction = null)
                    {
                        string val = Execute(
                            database: database,
                            areaDomains: areaDomains,
                            etlId: etlId,
                            transaction: transaction);

                        DataTable dt = new();

                        dt.Columns.Add(column: new DataColumn()
                        {
                            ColumnName = Name,
                            DataType = Type.GetType(typeName: "System.String")
                        });

                        DataRow row = dt.NewRow();

                        Functions.SetStringArg(
                            row: row,
                            name: Name,
                            val: val);

                        dt.Rows.Add(row: row);

                        return dt;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_etl_check_area_domains4etl_json
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="areaDomains">Json</param>
                    /// <param name="etlId">List of etl identifiers</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>json</returns>
                    public static string Execute(
                        NfiEstaDB database,
                        string areaDomains,
                        List<Nullable<int>> etlId,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(areaDomains: areaDomains, etlId: etlId);

                        NpgsqlParameter pAreaDomains = new(
                            parameterName: "areaDomains",
                            parameterType: NpgsqlDbType.Json);

                        NpgsqlParameter pEtlId = new(
                            parameterName: "etlId",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        if (areaDomains != null)
                        {
                            pAreaDomains.Value = (string)areaDomains;
                        }
                        else
                        {
                            pAreaDomains.Value = DBNull.Value;
                        }

                        if (etlId != null)
                        {
                            pEtlId.Value = etlId.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pEtlId.Value = DBNull.Value;
                        }

                        return
                            database.Postgres.ExecuteScalar(
                                sqlCommand: SQL,
                                transaction: transaction,
                                pAreaDomains,
                                pEtlId);
                    }

                    #endregion Methods

                }

                #endregion FnEtlCheckAreaDomainsForEtlJson

            }

        }
    }
}