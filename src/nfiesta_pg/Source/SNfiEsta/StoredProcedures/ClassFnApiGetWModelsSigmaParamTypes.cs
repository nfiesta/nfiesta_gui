﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {

            /// <summary>
            /// Stored procedures of schema nfiesta
            /// </summary>
            public static partial class NfiEstaFunctions
            {
                // fn_api_get_wmodels_sigma_paramtypes

                #region FnApiGetWModelsSigmaParamTypes

                /// <summary>
                /// Wrapper for stored procedure fn_api_get_wmodels_sigma_paramtypes.
                /// For the input arrays of panels and estimation cells the function returns a table
                /// with alternative combinations of working model, sigma and parametrization area type
                /// for which the regression estimates of total can be configured.
                /// Each record in the output table corresponds to a set of existing records in the table t_aux_conf.
                /// </summary>
                public static class FnApiGetWModelsSigmaParamTypes
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName = NfiEstaSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name = "fn_api_get_wmodels_sigma_paramtypes";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_panels integer[], ",
                            $"_estimation_cells integer[]; ",
                            $"returns: ",
                            $"TABLE(",
                            $"working_model_id integer, ",
                            $"working_model_label character varying, ",
                            $"working_model_label_en character varying, ",
                            $"working_model_description text, ",
                            $"working_model_description_en text, ",
                            $"sigma boolean, ",
                            $"param_area_type_id integer, ",
                            $"param_area_type_label character varying, ",
                            $"param_area_type_label_en character varying, ",
                            $"param_area_type_description text, ",
                            $"param_area_type_descriptipn_en text, ",
                            $"no_of_cells_covered bigint, ",
                            $"all_cells_included boolean)");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return String.Concat(
                                $"SELECT{Environment.NewLine}",
                                $"    {TFnApiGetWModelsSigmaParamTypesList.ColId.FuncCall}                          AS {TFnApiGetWModelsSigmaParamTypesList.ColId.Name},{Environment.NewLine}",
                                $"    {TFnApiGetWModelsSigmaParamTypesList.ColWorkingModelId.DbName}                AS {TFnApiGetWModelsSigmaParamTypesList.ColWorkingModelId.Name},{Environment.NewLine}",
                                $"    {TFnApiGetWModelsSigmaParamTypesList.ColWorkingModelLabelCs.DbName}           AS {TFnApiGetWModelsSigmaParamTypesList.ColWorkingModelLabelCs.Name},{Environment.NewLine}",
                                $"    {TFnApiGetWModelsSigmaParamTypesList.ColWorkingModelDescriptionCs.DbName}     AS {TFnApiGetWModelsSigmaParamTypesList.ColWorkingModelDescriptionCs.Name},{Environment.NewLine}",
                                $"    {TFnApiGetWModelsSigmaParamTypesList.ColWorkingModelLabelEn.DbName}           AS {TFnApiGetWModelsSigmaParamTypesList.ColWorkingModelLabelEn.Name},{Environment.NewLine}",
                                $"    {TFnApiGetWModelsSigmaParamTypesList.ColWorkingModelDescriptionEn.DbName}     AS {TFnApiGetWModelsSigmaParamTypesList.ColWorkingModelDescriptionEn.Name},{Environment.NewLine}",
                                $"    {TFnApiGetWModelsSigmaParamTypesList.ColSigma.DbName}                         AS {TFnApiGetWModelsSigmaParamTypesList.ColSigma.Name},{Environment.NewLine}",
                                $"    {TFnApiGetWModelsSigmaParamTypesList.ColParamAreaTypeId.DbName}               AS {TFnApiGetWModelsSigmaParamTypesList.ColParamAreaTypeId.Name},{Environment.NewLine}",
                                $"    {TFnApiGetWModelsSigmaParamTypesList.ColParamAreaTypeLabelCs.DbName}          AS {TFnApiGetWModelsSigmaParamTypesList.ColParamAreaTypeLabelCs.Name},{Environment.NewLine}",
                                $"    {TFnApiGetWModelsSigmaParamTypesList.ColParamAreaTypeDescriptionCs.DbName}    AS {TFnApiGetWModelsSigmaParamTypesList.ColParamAreaTypeDescriptionCs.Name},{Environment.NewLine}",
                                $"    {TFnApiGetWModelsSigmaParamTypesList.ColParamAreaTypeLabelEn.DbName}          AS {TFnApiGetWModelsSigmaParamTypesList.ColParamAreaTypeLabelEn.Name},{Environment.NewLine}",
                                $"    {TFnApiGetWModelsSigmaParamTypesList.ColParamAreaTypeDescriptionEn.DbName}    AS {TFnApiGetWModelsSigmaParamTypesList.ColParamAreaTypeDescriptionEn.Name},{Environment.NewLine}",
                                $"    {TFnApiGetWModelsSigmaParamTypesList.ColNoOfCellsCovered.DbName}              AS {TFnApiGetWModelsSigmaParamTypesList.ColNoOfCellsCovered.Name},{Environment.NewLine}",
                                $"    {TFnApiGetWModelsSigmaParamTypesList.ColAllCellsIncluded.DbName}              AS {TFnApiGetWModelsSigmaParamTypesList.ColAllCellsIncluded.Name}{Environment.NewLine}",
                                $"FROM{Environment.NewLine}",
                                $"    {SchemaName}.{Name}(@panels, @estimationCells);{Environment.NewLine}");
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="panels">List of panel identifiers</param>
                    /// <param name="estimationCells">List of estimation cell identifiers</param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        List<Nullable<int>> panels,
                        List<Nullable<int>> estimationCells)
                    {
                        string result = SQL;

                        result = result.Replace(
                           oldValue: "@panels",
                           newValue: Functions.PrepNIntArrayArg(args: panels, dbType: "int4"));

                        result = result.Replace(
                            oldValue: "@estimationCells",
                            newValue: Functions.PrepNIntArrayArg(args: estimationCells, dbType: "int4"));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_api_get_wmodels_sigma_paramtypes
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="panels">List of panel identifiers</param>
                    /// <param name="estimationCells">List of estimation cell identifiers</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table of alternative combinations of working model, sigma and parametrization area type</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        List<Nullable<int>> panels,
                        List<Nullable<int>> estimationCells,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(panels: panels, estimationCells: estimationCells);

                        NpgsqlParameter pPanels = new(
                            parameterName: "panels",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        NpgsqlParameter pEstimationCells = new(
                           parameterName: "estimationCells",
                           parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        if (panels != null)
                        {
                            pPanels.Value = panels.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pPanels.Value = DBNull.Value;
                        }

                        if (estimationCells != null)
                        {
                            pEstimationCells.Value = estimationCells.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pEstimationCells.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteQuery(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pPanels, pEstimationCells);
                    }

                    /// <summary>
                    /// Execute stored procedure fn_api_get_wmodels_sigma_paramtypes
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="panels">List of panel identifiers</param>
                    /// <param name="estimationCells">List of estimation cell identifiers</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>List of alternative combinations of working model, sigma and parametrization area type</returns>
                    public static TFnApiGetWModelsSigmaParamTypesList Execute(
                         NfiEstaDB database,
                        List<Nullable<int>> panels,
                        List<Nullable<int>> estimationCells,
                        NpgsqlTransaction transaction = null)
                    {
                        System.Diagnostics.Stopwatch stopWatch = new();
                        stopWatch.Start();

                        DataTable data = ExecuteQuery(
                            database: database,
                            panels: panels,
                            estimationCells: estimationCells,
                            transaction: transaction);
                        data.TableName = Name;

                        stopWatch.Stop();

                        return new TFnApiGetWModelsSigmaParamTypesList(
                            database: database,
                            data: data,
                            loadingTime: 0.001 * stopWatch.ElapsedMilliseconds,
                            storedProcedure: CommandText)
                        {
                            PanelsIds = panels,
                            EstimationCellsIds = estimationCells
                        };
                    }

                    #endregion Methods

                }

                #endregion FnApiGetWModelsSigmaParamTypes

            }

        }
    }
}