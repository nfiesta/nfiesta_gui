﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {

            /// <summary>
            /// Stored procedures of schema nfiesta
            /// </summary>
            public static partial class NfiEstaFunctions
            {
                // fn_etl_import_variable_hierarchy

                #region FnEtlImportVariableHierarchy

                /// <summary>
                /// Wrapper for stored procedure fn_etl_import_variable_hierarchy.
                /// The function solves ETL proces for t_variable_hierarchy table.
                /// </summary>
                public static class FnEtlImportVariableHierarchy
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName = NfiEstaSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name = "fn_etl_import_variable_hierarchy";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_variables json; ",
                            $"returns: text");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    $SchemaName.$Name(@variables)::text AS $Name{Environment.NewLine};");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="variables"></param>
                    /// <param name="strNullJson">Stored procedure requires json array with null value instead of DBNull.</param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        string variables,
                        string strNullJson = "[null]")
                    {
                        string result = SQL;

                        result = result.Replace(
                            oldValue: "@variables",
                            newValue: Functions.PrepStringArg(arg:
                                String.IsNullOrEmpty(value: variables)
                                    ? strNullJson
                                    : variables));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_etl_import_variable_hierarchy
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="variables"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <param name="strNullJson">Stored procedure requires json array with null value instead of DBNull.</param>
                    /// <returns></returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        string variables,
                        NpgsqlTransaction transaction = null,
                        string strNullJson = "[null]")
                    {
                        string val = Execute(
                            database: database,
                            variables: variables,
                            transaction: transaction,
                            strNullJson: strNullJson);

                        DataTable dt = new();

                        dt.Columns.Add(column: new DataColumn()
                        {
                            ColumnName = Name,
                            DataType = Type.GetType(typeName: "System.String")
                        });

                        DataRow row = dt.NewRow();

                        Functions.SetStringArg(
                            row: row,
                            name: Name,
                            val: val);

                        dt.Rows.Add(row: row);

                        return dt;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_etl_import_variable_hierarchy
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="variables"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <param name="strNullJson">Stored procedure requires json array with null value instead of DBNull.</param>
                    /// <returns></returns>
                    public static string Execute(
                        NfiEstaDB database,
                        string variables,
                        NpgsqlTransaction transaction = null,
                        string strNullJson = "[null]")
                    {
                        CommandText = GetCommandText(variables: variables, strNullJson: strNullJson);

                        NpgsqlParameter pVariables = new(
                            parameterName: "variables",
                            parameterType: NpgsqlDbType.Json);

                        if (!String.IsNullOrEmpty(value: variables))
                        {
                            pVariables.Value = (string)variables;
                        }
                        else
                        {
                            pVariables.Value = (string)strNullJson;
                        }

                        return database.Postgres.ExecuteScalar(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pVariables);
                    }

                    #endregion Methods

                }

                #endregion FnEtlImportVariableHierarchy

            }

        }
    }
}