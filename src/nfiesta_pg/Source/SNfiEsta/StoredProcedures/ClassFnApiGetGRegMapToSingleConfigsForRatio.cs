﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {

            /// <summary>
            /// Stored procedures of schema nfiesta
            /// </summary>
            public static partial class NfiEstaFunctions
            {
                // fn_api_get_gregmap2single_configs4ratio

                #region FnApiGetGRegMapToSingleConfigsForRatio

                /// <summary>
                /// Wrapper for stored procedure fn_api_get_gregmap2single_configs4ratio.
                /// For input estimation period, estimation cells, numerator and denominator variables
                /// the function returns combinations of panel-refyearset group, type of parametrisation area, force_synthetic
                /// (always FALSE due to the design-based single-phase total in the denominator and avoidance of mixed-minference),
                /// working model and sigma of already configured GREG-map (numerator) and the corresponding single-phase (denominator) total estimates.
                /// The parameter all_estimates_configurable is TRUE if the corresponding ratio can be configured
                /// for all input combinations of estimation cell, numerator variable and denominator variable.
                /// The functuion combines the elements of the input arrays variables_nom and variables_denom (considering their order)
                /// to obtain the desired set of estimates. These two input arrays must have an equal length,
                /// and their elements must be ordered, so each combination of their elements on the same position corresponds
                /// to one desired combination of variables in the numerator and the denominator of the ratio.
                /// The output parameters total_estimate_conf_numerator and total_estimate_conf_denominator are ordered arrays
                /// of identifiers of existing GREG-map or single-phase configurations
                /// (record ids of the table nfiesta.t_total_estimate_conf) for the numerator and denominator respectively.
                /// </summary>
                public static class FnApiGetGRegMapToSingleConfigsForRatio
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName = NfiEstaSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name = "fn_api_get_gregmap2single_configs4ratio";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_estimation_period integer, ",
                            $"_estimation_cells integer[], ",
                            $"_variables_nom integer[], ",
                            $"_variables_denom integer[]; ",
                            $"returns: ",
                            $"TABLE(",
                            $"panel_refyearset_group_id integer, ",
                            $"panel_refyearset_group_label character varying, ",
                            $"panel_refyearset_group_label_en character varying, ",
                            $"panel_refyearset_group_description text, ",
                            $"panel_refyearset_group_description_en text, ",
                            $"nominator_param_area_type_id integer, ",
                            $"nominator_param_area_type_label character varying, ",
                            $"nominator_param_area_type_label_en character varying, ",
                            $"nominator_param_area_type_description text, ",
                            $"nominator_param_area_type_description_en text, ",
                            $"nominator_force_synthetic boolean, ",
                            $"nominator_model_id integer, ",
                            $"nominator_model_label character varying, ",
                            $"nominator_model_label_en character varying, ",
                            $"nominator_model_description text, ",
                            $"nominator_model_description_en text, ",
                            $"nominator_model_sigma boolean, ",
                            $"all_estimates_configurable boolean, ",
                            $"total_estimate_conf_nominator integer[], ",
                            $"total_estimate_conf_denominator integer[])");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    {TFnApiGetGRegMapToSingleConfigsForRatioList.ColId.FuncCall}                                 AS {TFnApiGetGRegMapToSingleConfigsForRatioList.ColId.Name},{Environment.NewLine}",
                            $"    {TFnApiGetGRegMapToSingleConfigsForRatioList.ColPanelRefYearSetGroupId.DbName}               AS {TFnApiGetGRegMapToSingleConfigsForRatioList.ColPanelRefYearSetGroupId.Name},{Environment.NewLine}",
                            $"    {TFnApiGetGRegMapToSingleConfigsForRatioList.ColPanelRefYearSetGroupLabelCs.DbName}          AS {TFnApiGetGRegMapToSingleConfigsForRatioList.ColPanelRefYearSetGroupLabelCs.Name},{Environment.NewLine}",
                            $"    {TFnApiGetGRegMapToSingleConfigsForRatioList.ColPanelRefYearSetGroupDescriptionCs.DbName}    AS {TFnApiGetGRegMapToSingleConfigsForRatioList.ColPanelRefYearSetGroupDescriptionCs.Name},{Environment.NewLine}",
                            $"    {TFnApiGetGRegMapToSingleConfigsForRatioList.ColPanelRefYearSetGroupLabelEn.DbName}          AS {TFnApiGetGRegMapToSingleConfigsForRatioList.ColPanelRefYearSetGroupLabelEn.Name},{Environment.NewLine}",
                            $"    {TFnApiGetGRegMapToSingleConfigsForRatioList.ColPanelRefYearSetGroupDescriptionEn.DbName}    AS {TFnApiGetGRegMapToSingleConfigsForRatioList.ColPanelRefYearSetGroupDescriptionEn.Name},{Environment.NewLine}",
                            $"    {TFnApiGetGRegMapToSingleConfigsForRatioList.ColNumeratorParamAreaTypeId.DbName}             AS {TFnApiGetGRegMapToSingleConfigsForRatioList.ColNumeratorParamAreaTypeId.Name},{Environment.NewLine}",
                            $"    {TFnApiGetGRegMapToSingleConfigsForRatioList.ColNumeratorParamAreaTypeLabelCs.DbName}        AS {TFnApiGetGRegMapToSingleConfigsForRatioList.ColNumeratorParamAreaTypeLabelCs.Name},{Environment.NewLine}",
                            $"    {TFnApiGetGRegMapToSingleConfigsForRatioList.ColNumeratorParamAreaTypeDescriptionCs.DbName}  AS {TFnApiGetGRegMapToSingleConfigsForRatioList.ColNumeratorParamAreaTypeDescriptionCs.Name},{Environment.NewLine}",
                            $"    {TFnApiGetGRegMapToSingleConfigsForRatioList.ColNumeratorParamAreaTypeLabelEn.DbName}        AS {TFnApiGetGRegMapToSingleConfigsForRatioList.ColNumeratorParamAreaTypeLabelEn.Name},{Environment.NewLine}",
                            $"    {TFnApiGetGRegMapToSingleConfigsForRatioList.ColNumeratorParamAreaTypeDescriptionEn.DbName}  AS {TFnApiGetGRegMapToSingleConfigsForRatioList.ColNumeratorParamAreaTypeDescriptionEn.Name},{Environment.NewLine}",
                            $"    {TFnApiGetGRegMapToSingleConfigsForRatioList.ColNumeratorForceSynthetic.DbName}              AS {TFnApiGetGRegMapToSingleConfigsForRatioList.ColNumeratorForceSynthetic.Name},{Environment.NewLine}",
                            $"    {TFnApiGetGRegMapToSingleConfigsForRatioList.ColNumeratorModelId.DbName}                     AS {TFnApiGetGRegMapToSingleConfigsForRatioList.ColNumeratorModelId.Name},{Environment.NewLine}",
                            $"    {TFnApiGetGRegMapToSingleConfigsForRatioList.ColNumeratorModelLabelCs.DbName}                AS {TFnApiGetGRegMapToSingleConfigsForRatioList.ColNumeratorModelLabelCs.Name},{Environment.NewLine}",
                            $"    {TFnApiGetGRegMapToSingleConfigsForRatioList.ColNumeratorModelDescriptionCs.DbName}          AS {TFnApiGetGRegMapToSingleConfigsForRatioList.ColNumeratorModelDescriptionCs.Name},{Environment.NewLine}",
                            $"    {TFnApiGetGRegMapToSingleConfigsForRatioList.ColNumeratorModelLabelEn.DbName}                AS {TFnApiGetGRegMapToSingleConfigsForRatioList.ColNumeratorModelLabelEn.Name},{Environment.NewLine}",
                            $"    {TFnApiGetGRegMapToSingleConfigsForRatioList.ColNumeratorModelDescriptionEn.DbName}          AS {TFnApiGetGRegMapToSingleConfigsForRatioList.ColNumeratorModelDescriptionEn.Name},{Environment.NewLine}",
                            $"    {TFnApiGetGRegMapToSingleConfigsForRatioList.ColNumeratorModelSigma.DbName}                  AS {TFnApiGetGRegMapToSingleConfigsForRatioList.ColNumeratorModelSigma.Name},{Environment.NewLine}",
                            $"    {TFnApiGetGRegMapToSingleConfigsForRatioList.ColAllEstimatesConfigurable.DbName}             AS {TFnApiGetGRegMapToSingleConfigsForRatioList.ColAllEstimatesConfigurable.Name},{Environment.NewLine}",
                            $"    {TFnApiGetGRegMapToSingleConfigsForRatioList.ColTotalEstimateConfNumerator.FuncCall}         AS {TFnApiGetGRegMapToSingleConfigsForRatioList.ColTotalEstimateConfNumerator.Name},{Environment.NewLine}",
                            $"    {TFnApiGetGRegMapToSingleConfigsForRatioList.ColTotalEstimateConfDenominator.FuncCall}       AS {TFnApiGetGRegMapToSingleConfigsForRatioList.ColTotalEstimateConfDenominator.Name}{Environment.NewLine}",
                            $"FROM{Environment.NewLine}",
                            $"    {SchemaName}.{Name}(@estimationPeriod, @estimationCells, @variablesNumerator, @variablesDenominator);{Environment.NewLine}");
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="estimationPeriod">Estimation period identifier</param>
                    /// <param name="estimationCells">List of estimation cells identifiers</param>
                    /// <param name="variablesNumerator">List of attribute categories identifiers for numerator</param>
                    /// <param name="variablesDenominator">List of attribute categories identifiers for denominator</param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        Nullable<int> estimationPeriod,
                        List<Nullable<int>> estimationCells,
                        List<Nullable<int>> variablesNumerator,
                        List<Nullable<int>> variablesDenominator)
                    {
                        string result = SQL;

                        result = result.Replace(
                            oldValue: "@estimationPeriod",
                            newValue: Functions.PrepNIntArg(arg: estimationPeriod, dbType: "int4"));

                        result = result.Replace(
                            oldValue: "@estimationCells",
                            newValue: Functions.PrepNIntArrayArg(args: estimationCells, dbType: "int4"));

                        result = result.Replace(
                            oldValue: "@variablesNumerator",
                            newValue: Functions.PrepNIntArrayArg(args: variablesNumerator, dbType: "int4"));

                        result = result.Replace(
                           oldValue: "@variablesDenominator",
                           newValue: Functions.PrepNIntArrayArg(args: variablesDenominator, dbType: "int4"));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_api_get_gregmap2single_configs4ratio
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="estimationPeriod">Estimation period identifier</param>
                    /// <param name="estimationCells">List of estimation cells identifiers</param>
                    /// <param name="variablesNumerator">List of attribute categories identifiers for numerator</param>
                    /// <param name="variablesDenominator">List of attribute categories identifiers for denominator</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>
                    /// Data table with combinations of panel-refyearset group, type of parametrisation area, force_synthetic
                    /// working model and sigma of already configured GREG-map (numerator)
                    /// and the corresponding single-phase (denominator) total estimates.
                    /// </returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> estimationPeriod,
                        List<Nullable<int>> estimationCells,
                        List<Nullable<int>> variablesNumerator,
                        List<Nullable<int>> variablesDenominator,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(
                           estimationPeriod: estimationPeriod,
                           estimationCells: estimationCells,
                           variablesNumerator: variablesNumerator,
                           variablesDenominator: variablesDenominator);

                        NpgsqlParameter pEstimationPeriod = new(
                            parameterName: "estimationPeriod",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pEstimationCells = new(
                            parameterName: "estimationCells",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        NpgsqlParameter pVariablesNumerator = new(
                           parameterName: "variablesNumerator",
                           parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        NpgsqlParameter pVariablesDenominator = new(
                           parameterName: "variablesDenominator",
                           parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        if (estimationPeriod != null)
                        {
                            pEstimationPeriod.Value = estimationPeriod;
                        }
                        else
                        {
                            pEstimationPeriod.Value = DBNull.Value;
                        }

                        if (estimationCells != null)
                        {
                            pEstimationCells.Value = estimationCells.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pEstimationCells.Value = DBNull.Value;
                        }

                        if (variablesNumerator != null)
                        {
                            pVariablesNumerator.Value = variablesNumerator.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pVariablesNumerator.Value = DBNull.Value;
                        }

                        if (variablesDenominator != null)
                        {
                            pVariablesDenominator.Value = variablesDenominator.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pVariablesDenominator.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteQuery(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pEstimationPeriod, pEstimationCells,
                            pVariablesNumerator, pVariablesDenominator);
                    }

                    /// <summary>
                    /// Execute stored procedure fn_api_get_gregmap2single_configs4ratio
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="estimationPeriod">Estimation period identifier</param>
                    /// <param name="estimationCells">List of estimation cells identifiers</param>
                    /// <param name="variablesNumerator">List of attribute categories identifiers for numerator</param>
                    /// <param name="variablesDenominator">List of attribute categories identifiers for denominator</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>
                    /// List of combinations of panel-refyearset group, type of parametrisation area, force_synthetic
                    /// working model and sigma of already configured GREG-map (numerator)
                    /// and the corresponding single-phase (denominator) total estimates.
                    /// </returns>
                    public static TFnApiGetGRegMapToSingleConfigsForRatioList Execute(
                        NfiEstaDB database,
                        Nullable<int> estimationPeriod,
                        List<Nullable<int>> estimationCells,
                        List<Nullable<int>> variablesNumerator,
                        List<Nullable<int>> variablesDenominator,
                        NpgsqlTransaction transaction = null)
                    {
                        System.Diagnostics.Stopwatch stopWatch = new();
                        stopWatch.Start();

                        DataTable data = ExecuteQuery(
                            database: database,
                            estimationPeriod: estimationPeriod,
                            estimationCells: estimationCells,
                            variablesNumerator: variablesNumerator,
                            variablesDenominator: variablesDenominator,
                            transaction: transaction);
                        data.TableName = Name;

                        stopWatch.Stop();

                        return new TFnApiGetGRegMapToSingleConfigsForRatioList(
                            database: database,
                            data: data,
                            loadingTime: 0.001 * stopWatch.ElapsedMilliseconds,
                            storedProcedure: CommandText)
                        {
                            EstimationPeriodId = estimationPeriod,
                            EstimationCellsIds = estimationCells,
                            VariablesNumeratorIds = variablesNumerator,
                            VariablesDenominatorIds = variablesDenominator
                        };
                    }

                    #endregion Methods

                }

                #endregion FnApiGetGRegMapToSingleConfigsForRatio

            }

        }
    }
}