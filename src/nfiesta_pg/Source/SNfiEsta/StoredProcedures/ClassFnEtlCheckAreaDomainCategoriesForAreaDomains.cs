﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {

            /// <summary>
            /// Stored procedures of schema nfiesta
            /// </summary>
            public static partial class NfiEstaFunctions
            {
                // fn_etl_check_area_domain_categories4area_domains

                #region FnEtlCheckAreaDomainCategoriesForAreaDomains

                /// <summary>
                /// Wrapper for stored procedure fn_etl_check_area_domain_categories4area_domains.
                /// The function returns array of IDs of area domains
                /// if their source label or description categories are different from target.
                /// </summary>
                public static class FnEtlCheckAreaDomainCategoriesForAreaDomains
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = NfiEstaSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_etl_check_area_domain_categories4area_domains";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_metadatas json; ",
                            $"returns: integer[]");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    array_to_string($SchemaName.$Name(",
                            $"@metadatas),'{(char)59}','{Functions.StrNull}')::text AS $Name;{Environment.NewLine}");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="metadatas">Json</param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(string metadatas)
                    {
                        string result = SQL;

                        result = result.Replace(
                            oldValue: "@metadatas",
                            newValue: Functions.PrepStringArg(arg: metadatas));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_etl_check_area_domain_categories4area_domains
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="metadatas">Json</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table with IDs of area domains</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        string metadatas,
                        NpgsqlTransaction transaction = null)
                    {
                        List<Nullable<int>> list = Execute(
                            database: database,
                            metadatas: metadatas,
                            transaction: transaction);

                        DataTable dt = new();

                        dt.Columns.Add(column: new DataColumn()
                        {
                            ColumnName = Name,
                            DataType = Type.GetType(typeName: "System.Int32")
                        });

                        if (list == null)
                        {
                            return dt;
                        }

                        foreach (Nullable<int> val in list)
                        {
                            DataRow row = dt.NewRow();

                            Functions.SetNIntArg(
                                row: row,
                                name: Name,
                                val: val);

                            dt.Rows.Add(row: row);
                        }

                        return dt;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_etl_check_area_domain_categories4area_domains
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="metadatas">Json</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>List with IDs of area domains</returns>
                    public static List<Nullable<int>> Execute(
                        NfiEstaDB database,
                        string metadatas,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(metadatas: metadatas);

                        NpgsqlParameter pMetadatas = new(
                            parameterName: "metadatas",
                            parameterType: NpgsqlDbType.Json);

                        if (metadatas != null)
                        {
                            pMetadatas.Value = (string)metadatas;
                        }
                        else
                        {
                            pMetadatas.Value = DBNull.Value;
                        }

                        string strResult = database.Postgres.ExecuteScalar(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pMetadatas);

                        if (String.IsNullOrEmpty(value: strResult))
                        {
                            return null;
                        }
                        else
                        {
                            return
                            Functions.StringToNIntList(
                                text: strResult,
                                separator: (char)59,
                                defaultValue: null);
                        }
                    }

                    #endregion Methods

                }

                #endregion FnEtlCheckAreaDomainCategoriesForAreaDomains

            }

        }
    }
}