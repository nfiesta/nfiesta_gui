﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {

            /// <summary>
            /// Stored procedures of schema nfiesta
            /// </summary>
            public static partial class NfiEstaFunctions
            {
                // fn_api_get_group4panel_refyearset_combinations

                #region FnApiGetGroupForPanelRefYearSetCombinations

                /// <summary>
                /// Wrapper for stored procedure fn_api_get_group4panel_refyearset_combinations.
                /// The function returns the group of panel and reference year sets corresponding to the input arrays.
                /// If there is no such group, the function returns no records.
                /// It also finds groups with no reference yearsets attached (NULLs in the function argument _refyearsets)
                /// that are used for auxiliary configurations within t_aux_conf.
                /// If the input argument _refyearsets contains only NULLs, it must be explicitly cast to ::int[] within the function call.
                /// </summary>
                public static class FnApiGetGroupForPanelRefYearSetCombinations
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName = NfiEstaSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name = "fn_api_get_group4panel_refyearset_combinations";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_panels integer[], ",
                            $"_refyearsets integer[]; ",
                            $"returns: ",
                            $"TABLE(",
                            $"id integer, ",
                            $"label character varying, ",
                            $"label_en character varying, ",
                            $"description text, ",
                            $"description_en text)");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return String.Concat(
                                $"SELECT{Environment.NewLine}",
                                $"    {TFnApiGetGroupForPanelRefYearSetCombinationsList.ColId.DbName}               AS {TFnApiGetGroupForPanelRefYearSetCombinationsList.ColId.Name},{Environment.NewLine}",
                                $"    {TFnApiGetGroupForPanelRefYearSetCombinationsList.ColLabelCs.DbName}          AS {TFnApiGetGroupForPanelRefYearSetCombinationsList.ColLabelCs.Name},{Environment.NewLine}",
                                $"    {TFnApiGetGroupForPanelRefYearSetCombinationsList.ColDescriptionCs.DbName}    AS {TFnApiGetGroupForPanelRefYearSetCombinationsList.ColDescriptionCs.Name},{Environment.NewLine}",
                                $"    {TFnApiGetGroupForPanelRefYearSetCombinationsList.ColLabelEn.DbName}          AS {TFnApiGetGroupForPanelRefYearSetCombinationsList.ColLabelEn.Name},{Environment.NewLine}",
                                $"    {TFnApiGetGroupForPanelRefYearSetCombinationsList.ColDescriptionEn.DbName}    AS {TFnApiGetGroupForPanelRefYearSetCombinationsList.ColDescriptionEn.Name}{Environment.NewLine}",
                                $"FROM{Environment.NewLine}",
                                $"    {SchemaName}.{Name}(@panels, @refYearSets);{Environment.NewLine}");
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="panels">List of panels identifiers</param>
                    /// <param name="refYearSets">List of reference year sets identifiers</param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        List<Nullable<int>> panels,
                        List<Nullable<int>> refYearSets)
                    {
                        string result = SQL;

                        result = result.Replace(
                           oldValue: "@panels",
                           newValue: Functions.PrepNIntArrayArg(args: panels, dbType: "int4"));

                        result = result.Replace(
                            oldValue: "@refYearSets",
                            newValue: Functions.PrepNIntArrayArg(args: refYearSets, dbType: "int4"));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_api_get_group4panel_refyearset_combinations
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="panels">List of panels identifiers</param>
                    /// <param name="refYearSets">List of reference year sets identifiers</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table with groups of panel and reference year sets corresponding to the input arrays of panels and reference year sets</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        List<Nullable<int>> panels,
                        List<Nullable<int>> refYearSets,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(panels: panels, refYearSets: refYearSets);

                        NpgsqlParameter pPanels = new(
                            parameterName: "panels",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        NpgsqlParameter pRefYearSets = new(
                           parameterName: "refYearSets",
                           parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        if (panels != null)
                        {
                            pPanels.Value = panels.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pPanels.Value = DBNull.Value;
                        }

                        if (refYearSets != null)
                        {
                            pRefYearSets.Value = refYearSets.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pRefYearSets.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteQuery(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pPanels, pRefYearSets);
                    }

                    /// <summary>
                    /// Execute stored procedure fn_api_get_group4panel_refyearset_combinations
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="panels">List of panels identifiers</param>
                    /// <param name="refYearSets">List of reference year sets identifiers</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>List with groups of panel and reference year sets corresponding to the input arrays of panels and reference year sets</returns>
                    public static TFnApiGetGroupForPanelRefYearSetCombinationsList Execute(
                        NfiEstaDB database,
                        List<Nullable<int>> panels,
                        List<Nullable<int>> refYearSets,
                        NpgsqlTransaction transaction = null)
                    {
                        System.Diagnostics.Stopwatch stopWatch = new();
                        stopWatch.Start();

                        DataTable data = ExecuteQuery(
                            database: database,
                            panels: panels,
                            refYearSets: refYearSets,
                            transaction: transaction);
                        data.TableName = Name;

                        stopWatch.Stop();

                        return new TFnApiGetGroupForPanelRefYearSetCombinationsList(
                            database: database,
                            data: data,
                            loadingTime: 0.001 * stopWatch.ElapsedMilliseconds,
                            storedProcedure: CommandText)
                        {
                            PanelsIds = panels,
                            RefYearSetsIds = refYearSets
                        };
                    }

                    #endregion Methods

                }

                #endregion FnApiGetGroupForPanelRefYearSetCombinations

            }

        }
    }
}