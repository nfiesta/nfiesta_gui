﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using System;
using System.Data;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {

            /// <summary>
            /// Stored procedures of schema nfiesta
            /// </summary>
            public static partial class NfiEstaFunctions
            {
                // fn_api_get_list_of_panel_refyearset_groups

                #region FnApiGetListOfPanelRefYearSetGroups

                /// <summary>
                /// Wrapper for stored procedure fn_api_get_list_of_panel_refyearset_groups.
                /// The function returns id, label, description, label_en, description_en
                /// from nfiesta.c_panel_refyearset_group
                /// of all groups of panel and reference-year set combinations.
                /// </summary>
                public static class FnApiGetListOfPanelRefYearSetGroups
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = NfiEstaSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_api_get_list_of_panel_refyearset_groups";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ; ",
                            $"returns: ",
                            $"TABLE(",
                            $"id integer, ",
                            $"label character varying, ",
                            $"description text, ",
                            $"label_en character varying, ",
                            $"description_en text)");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                String.Concat(
                                    $"SELECT{Environment.NewLine}",
                                    $"    {PanelRefYearSetGroupList.ColId.SQL(cols: PanelRefYearSetGroupList.Cols, alias: Name)}",
                                    $"    {PanelRefYearSetGroupList.ColLabelCs.SQL(cols: PanelRefYearSetGroupList.Cols, alias: Name)}",
                                    $"    {PanelRefYearSetGroupList.ColDescriptionCs.SQL(cols: PanelRefYearSetGroupList.Cols, alias: Name)}",
                                    $"    {PanelRefYearSetGroupList.ColLabelEn.SQL(cols: PanelRefYearSetGroupList.Cols, alias: Name)}",
                                    $"    {PanelRefYearSetGroupList.ColDescriptionEn.SQL(cols: PanelRefYearSetGroupList.Cols, alias: Name, isLastOne: true)}",
                                    $"FROM{Environment.NewLine}",
                                    $"    {SchemaName}.{Name}(){Environment.NewLine}",
                                    $"ORDER BY{Environment.NewLine}",
                                    $"    {PanelRefYearSetGroupList.ColId.DbName};{Environment.NewLine}");
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <returns>Command text</returns>
                    public static string GetCommandText()
                    {
                        return SQL;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_api_get_list_of_panel_refyearset_groups
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table of aggregated sets of panels and corresponding reference year sets</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText();

                        return database.Postgres.ExecuteQuery(
                            sqlCommand: SQL,
                            transaction: transaction);
                    }

                    /// <summary>
                    /// Execute stored procedure fn_api_get_list_of_panel_refyearset_groups
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>
                    /// List of aggregated sets of panels and corresponding reference year sets
                    /// </returns>
                    public static PanelRefYearSetGroupList Execute(
                        NfiEstaDB database,
                        NpgsqlTransaction transaction = null)
                    {
                        System.Diagnostics.Stopwatch stopWatch = new();
                        stopWatch.Start();

                        DataTable data = ExecuteQuery(
                            database: database,
                            transaction: transaction);
                        data.TableName = Name;

                        stopWatch.Stop();

                        return new PanelRefYearSetGroupList(
                            database: database,
                            data: data,
                            loadingTime: 0.001 * stopWatch.ElapsedMilliseconds,
                            storedProcedure: CommandText)
                        { };
                    }

                    #endregion Methods

                }

                #endregion FnApiGetListOfPanelRefYearSetGroups

            }

        }
    }
}