﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {

            /// <summary>
            /// Stored procedures of schema nfiesta
            /// </summary>
            public static partial class NfiEstaFunctions
            {
                // fn_2p1p_ratio_var

                #region Fn2p1pRatioVar

                /// <summary>
                /// Wrapper for stored procedure fn_2p1p_ratio_var.
                /// Function computing ratio and variance using regression estimate.
                /// </summary>
                public static class Fn2p1pRatioVar
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = NfiEstaSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_2p1p_ratio_var";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"conf_id integer, ",
                            $"conf_id__denom integer; ",
                            $"returns: ",
                            $"TABLE(",
                            $"attribute integer, ",
                            $"point1p double precision, ",
                            $"var1p double precision, ",
                            $"point2p double precision, ",
                            $"var2p double precision, ",
                            $"min_ssize double precision, ",
                            $"act_ssize bigint, ",
                            $"est_info json)");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    $Name.attribute      AS attribute,{Environment.NewLine}",
                            $"    $Name.point1p        AS point1p,{Environment.NewLine}",
                            $"    $Name.var1p          AS var1p,{Environment.NewLine}",
                            $"    $Name.point2p        AS point2p,{Environment.NewLine}",
                            $"    $Name.var2p          AS var2p,{Environment.NewLine}",
                            $"    $Name.min_ssize      AS min_ssize,{Environment.NewLine}",
                            $"    $Name.act_ssize      AS act_ssize,{Environment.NewLine}",
                            $"    $Name.est_info::text AS est_info{Environment.NewLine}",
                            $"FROM{Environment.NewLine}",
                            $"    $SchemaName.$Name(@confId, @confIdDenom) AS $Name;{Environment.NewLine}");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="confId"></param>
                    /// <param name="confIdDenom"></param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        Nullable<int> confId,
                        Nullable<int> confIdDenom)
                    {
                        string result = SQL;

                        result = result.Replace(
                             oldValue: "@confId",
                             newValue: Functions.PrepNIntArg(arg: confId));

                        result = result.Replace(
                            oldValue: "@confIdDenom",
                            newValue: Functions.PrepNIntArg(arg: confIdDenom));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_2p1p_ratio_var
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="confId"></param>
                    /// <param name="confIdDenom"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns></returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> confId,
                        Nullable<int> confIdDenom,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(
                            confId: confId,
                            confIdDenom: confIdDenom);

                        NpgsqlParameter pConfId = new(
                            parameterName: "confId",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pConfIdDenom = new(
                            parameterName: "confIdDenom",
                            parameterType: NpgsqlDbType.Integer);

                        if (confId != null)
                        {
                            pConfId.Value = (int)confId;
                        }
                        else
                        {
                            pConfId.Value = DBNull.Value;
                        }

                        if (confIdDenom != null)
                        {
                            pConfIdDenom.Value = (int)confIdDenom;
                        }
                        else
                        {
                            pConfIdDenom.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteQuery(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pConfId, pConfIdDenom);
                    }

                    #endregion Methods

                }

                #endregion Fn2p1pRatioVar

            }

        }
    }
}