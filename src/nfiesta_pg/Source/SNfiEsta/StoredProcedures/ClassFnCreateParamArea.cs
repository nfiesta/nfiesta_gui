﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {

            /// <summary>
            /// Stored procedures of schema nfiesta
            /// </summary>
            public static partial class NfiEstaFunctions
            {
                // fn_create_param_area

                #region FnCreateParamArea

                /// <summary>
                /// Wrapper for stored procedure fn_create_param_area.
                /// Function for creation of new parametrization area from given estimation cells.
                /// </summary>
                public static class FnCreateParamArea
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName = NfiEstaSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name = "fn_create_param_area";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_param_area_type integer, ",
                            $"_estimation_cells integer[], ",
                            $"_param_area_code character varying DEFAULT NULL::character varying; ",
                            $"returns: integer");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    $SchemaName.$Name(@paramAreaType, @estimationCells, @paramAreaCode) AS $Name;{Environment.NewLine}");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="paramAreaType"></param>
                    /// <param name="estimationCells"></param>
                    /// <param name="paramAreaCode"></param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        Nullable<int> paramAreaType,
                        List<Nullable<int>> estimationCells,
                        string paramAreaCode)
                    {
                        string result = SQL;

                        result = result.Replace(
                           oldValue: "@paramAreaType",
                           newValue: Functions.PrepNIntArg(arg: paramAreaType));

                        result = result.Replace(
                            oldValue: "@estimationCells",
                            newValue: Functions.PrepNIntArrayArg(args: estimationCells, dbType: "int4"));

                        result = result.Replace(
                            oldValue: "@paramAreaCode",
                            newValue: Functions.PrepStringArg(arg: paramAreaCode));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_create_param_area
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="paramAreaType"></param>
                    /// <param name="estimationCells"></param>
                    /// <param name="paramAreaCode"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns></returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> paramAreaType,
                        List<Nullable<int>> estimationCells,
                        string paramAreaCode,
                        NpgsqlTransaction transaction = null)
                    {
                        Nullable<int> val = Execute(
                            database: database,
                            paramAreaType: paramAreaType,
                            estimationCells: estimationCells,
                            paramAreaCode: paramAreaCode,
                            transaction: transaction);

                        DataTable dt = new();

                        dt.Columns.Add(column: new DataColumn()
                        {
                            ColumnName = Name,
                            DataType = Type.GetType(typeName: "System.Int32")
                        });

                        DataRow row = dt.NewRow();

                        Functions.SetNIntArg(
                            row: row,
                            name: Name,
                            val: val);

                        dt.Rows.Add(row: row);

                        return dt;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_create_param_area
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="paramAreaType"></param>
                    /// <param name="estimationCells"></param>
                    /// <param name="paramAreaCode"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns></returns>
                    public static Nullable<int> Execute(
                        NfiEstaDB database,
                        Nullable<int> paramAreaType,
                        List<Nullable<int>> estimationCells,
                        string paramAreaCode,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(
                            paramAreaType: paramAreaType,
                            estimationCells: estimationCells,
                            paramAreaCode: paramAreaCode);

                        NpgsqlParameter pParamAreaType = new(
                            parameterName: "paramAreaType",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pEstimationCells = new(
                            parameterName: "estimationCells",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        NpgsqlParameter pParamAreaCode = new(
                            parameterName: "paramAreaCode",
                            parameterType: NpgsqlDbType.Varchar);

                        if (paramAreaType != null)
                        {
                            pParamAreaType.Value = (int)paramAreaType;
                        }
                        else
                        {
                            pParamAreaType.Value = DBNull.Value;
                        }

                        if (estimationCells != null)
                        {
                            pEstimationCells.Value = estimationCells.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pEstimationCells.Value = DBNull.Value;
                        }

                        if (paramAreaCode != null)
                        {
                            pParamAreaCode.Value = (string)paramAreaCode;
                        }
                        else
                        {
                            pParamAreaCode.Value = DBNull.Value;
                        }

                        string strResult = database.Postgres.ExecuteScalar(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pParamAreaType, pEstimationCells, pParamAreaCode);

                        if (String.IsNullOrEmpty(value: strResult))
                        {
                            return null;
                        }
                        else
                        {
                            if (Int32.TryParse(s: strResult, result: out int intResult))
                            {
                                return intResult;
                            }
                            else
                            {
                                return null;
                            }
                        }
                    }

                    #endregion Methods

                }

                #endregion FnCreateParamArea

            }

        }
    }
}