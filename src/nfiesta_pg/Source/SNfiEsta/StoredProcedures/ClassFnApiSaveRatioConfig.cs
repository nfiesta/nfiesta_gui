﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {

            /// <summary>
            /// Stored procedures of schema nfiesta
            /// </summary>
            public static partial class NfiEstaFunctions
            {
                // fn_api_save_greg_map_total_config

                #region FnApiSaveRatioConfig

                /// <summary>
                /// Wrapper for stored procedure fn_api_save_greg_map_total_config
                /// The function saves configuration of a ratio estimator into the nfiesta.t_estimate_conf table.
                /// It presumes existence of total configurations for the nominator as well as the denominator of the ratio.
                /// Any type of total estimators can be combined in the nominator and the denominator as long as they use the same group of panels.
                /// </summary>
                public static partial class FnApiSaveRatioConfig
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = NfiEstaSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_api_save_ratio_config";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                         = String.Concat(
                             $"$SchemaName.$Name; ",
                             $"args: ",
                             $"_total_estimate_conf_nominator integer, ",
                             $"_total_estimate_conf_denominator integer; ",
                             $"returns: integer");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                    = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    $SchemaName.$Name(",
                            $"@totalEstimateConfNumerator, ",
                            $"@totalEstimateConfDenominator) AS $Name;{Environment.NewLine}");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="totalEstimateConfNumerator">Numerator total estimate configuration identifier</param>
                    /// <param name="totalEstimateConfDenominator">Denominator total estimate configuration identifier</param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        Nullable<int> totalEstimateConfNumerator,
                        Nullable<int> totalEstimateConfDenominator)
                    {
                        string result = SQL;

                        result = result.Replace(
                            oldValue: "@totalEstimateConfNumerator",
                            newValue: Functions.PrepNIntArg(arg: totalEstimateConfNumerator));

                        result = result.Replace(
                            oldValue: "@totalEstimateConfDenominator",
                            newValue: Functions.PrepNIntArg(arg: totalEstimateConfDenominator));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_api_save_ratio_config
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="totalEstimateConfNumerator">Numerator total estimate configuration identifier</param>
                    /// <param name="totalEstimateConfDenominator">Denominator total estimate configuration identifier</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table with identifier of the saved ratio estimate configuration</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> totalEstimateConfNumerator,
                        Nullable<int> totalEstimateConfDenominator,
                        NpgsqlTransaction transaction = null)
                    {
                        return ExecuteQuery(
                            connection: database.Postgres,
                            totalEstimateConfNumerator: totalEstimateConfNumerator,
                            totalEstimateConfDenominator: totalEstimateConfDenominator,
                            transaction: transaction);
                    }

                    /// <summary>
                    /// Execute stored procedure fn_api_save_ratio_config
                    /// </summary>
                    /// <param name="connection">Database connection</param>
                    /// <param name="totalEstimateConfNumerator">Numerator total estimate configuration identifier</param>
                    /// <param name="totalEstimateConfDenominator">Denominator total estimate configuration identifier</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table with identifier of the saved ratio estimate configuration</returns>
                    public static DataTable ExecuteQuery(
                        ZaJi.PostgreSQL.PostgreSQLWrapper connection,
                        Nullable<int> totalEstimateConfNumerator,
                        Nullable<int> totalEstimateConfDenominator,
                        NpgsqlTransaction transaction = null)
                    {
                        Nullable<int> val = Execute(
                            connection: connection,
                            totalEstimateConfNumerator: totalEstimateConfNumerator,
                            totalEstimateConfDenominator: totalEstimateConfDenominator,
                            transaction: transaction);

                        DataTable dt = new();

                        dt.Columns.Add(column: new DataColumn()
                        {
                            ColumnName = Name,
                            DataType = Type.GetType(typeName: "System.Int32")
                        });

                        DataRow row = dt.NewRow();

                        Functions.SetNIntArg(
                            row: row,
                            name: Name,
                            val: val);

                        dt.Rows.Add(row: row);

                        return dt;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_api_save_ratio_config
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="totalEstimateConfNumerator">Numerator total estimate configuration identifier</param>
                    /// <param name="totalEstimateConfDenominator">Denominator total estimate configuration identifier</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Identifier of the saved ratio estimate configuration</returns>
                    public static Nullable<int> Execute(
                        NfiEstaDB database,
                        Nullable<int> totalEstimateConfNumerator,
                        Nullable<int> totalEstimateConfDenominator,
                        NpgsqlTransaction transaction = null)
                    {
                        return Execute(
                            connection: database.Postgres,
                            totalEstimateConfNumerator: totalEstimateConfNumerator,
                            totalEstimateConfDenominator: totalEstimateConfDenominator,
                            transaction: transaction);
                    }

                    /// <summary>
                    /// Execute stored procedure fn_api_save_ratio_config
                    /// </summary>
                    /// <param name="connection">Database connection</param>
                    /// <param name="totalEstimateConfNumerator">Numerator total estimate configuration identifier</param>
                    /// <param name="totalEstimateConfDenominator">Denominator total estimate configuration identifier</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Identifier of the saved ratio estimate configuration</returns>
                    public static Nullable<int> Execute(
                        ZaJi.PostgreSQL.PostgreSQLWrapper connection,
                        Nullable<int> totalEstimateConfNumerator,
                        Nullable<int> totalEstimateConfDenominator,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(
                            totalEstimateConfNumerator: totalEstimateConfNumerator,
                            totalEstimateConfDenominator: totalEstimateConfDenominator);

                        NpgsqlParameter pTotalEstimateConfNumerator = new(
                            parameterName: "totalEstimateConfNumerator",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pTotalEstimateConfDenominator = new(
                            parameterName: "totalEstimateConfDenominator",
                            parameterType: NpgsqlDbType.Integer);

                        if (totalEstimateConfNumerator != null)
                        {
                            pTotalEstimateConfNumerator.Value = (int)totalEstimateConfNumerator;
                        }
                        else
                        {
                            pTotalEstimateConfNumerator.Value = DBNull.Value;
                        }

                        if (totalEstimateConfDenominator != null)
                        {
                            pTotalEstimateConfDenominator.Value = (int)totalEstimateConfDenominator;
                        }
                        else
                        {
                            pTotalEstimateConfDenominator.Value = DBNull.Value;
                        }

                        string strResult = connection?.ExecuteScalar(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pTotalEstimateConfNumerator, pTotalEstimateConfDenominator);

                        if (String.IsNullOrEmpty(value: strResult))
                        {
                            return null;
                        }
                        else
                        {
                            if (Int32.TryParse(s: strResult, result: out int intResult))
                            {
                                return intResult;
                            }
                            else
                            {
                                return null;
                            }
                        }
                    }

                    #endregion Methods

                }

                #endregion FnApiSaveRatioConfig

            }

        }
    }
}