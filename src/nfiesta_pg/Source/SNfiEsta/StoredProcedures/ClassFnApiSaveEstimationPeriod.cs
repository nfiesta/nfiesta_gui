﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {

            /// <summary>
            /// Stored procedures of schema nfiesta
            /// </summary>
            public static partial class NfiEstaFunctions
            {
                // fn_api_save_estimation_period

                #region FnApiSavePanelEstimationPeriod

                /// <summary>
                /// Wrapper for stored procedure fn_api_save_estimation_period
                /// The function inserts a row into the nfiesta.c_estimation_period lookup table.
                /// The columns estimate_date_begin, estimate_date_end, label, description, label_en,
                /// description_en, default_in_olap are passed as input arguments of the function.
                /// The function returns a unique identifier of the newly inserted estimation period.
                /// </summary>
                public static partial class FnApiSaveEstimationPeriod
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = NfiEstaSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_api_save_estimation_period";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_estimate_date_begin date, ",
                            $"_estimate_date_end date, ",
                            $"_label character varying, ",
                            $"_description text, ",
                            $"_label_en character varying, ",
                            $"_description_en text, ",
                            $"_default_in_olap boolean; ",
                            $"returns: integer");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    $SchemaName.$Name(",
                            $"@estimateDateBegin, ",
                            $"@estimateDateEnd, ",
                            $"@labelCs, ",
                            $"@descriptionCs, ",
                            $"@labelEn, ",
                            $"@descriptionEn, ",
                            $"@defaultInOlap) AS $Name;{Environment.NewLine}");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields

                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="estimateDateBegin">Initial date of the estimation period</param>
                    /// <param name="estimateDateEnd">Final date of the estimation period</param>
                    /// <param name="labelCs">Estimation period label in national language</param>
                    /// <param name="descriptionCs">Estimation period label in national language</param>
                    /// <param name="labelEn">Estimation period label in English</param>
                    /// <param name="descriptionEn">Estimation period description in English</param>
                    /// <param name="defaultInOlap">Limits the display of periods to those set to true (it performs in the next calculation). Default setting is false.</param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        Nullable<DateTime> estimateDateBegin,
                        Nullable<DateTime> estimateDateEnd,
                        string labelCs,
                        string descriptionCs,
                        string labelEn,
                        string descriptionEn,
                        Nullable<bool> defaultInOlap
                        )
                    {
                        string result = SQL;

                        result = result.Replace(
                            oldValue: "@estimateDateBegin",
                            newValue: Functions.PrepNDateTimeArg(arg: estimateDateBegin));

                        result = result.Replace(
                            oldValue: "@estimateDateEnd",
                            newValue: Functions.PrepNDateTimeArg(arg: estimateDateEnd));

                        result = result.Replace(
                            oldValue: "@labelCs",
                            newValue: Functions.PrepStringArg(arg: labelCs));

                        result = result.Replace(
                            oldValue: "@descriptionCs",
                            newValue: Functions.PrepStringArg(arg: descriptionCs));

                        result = result.Replace(
                            oldValue: "@labelEn",
                            newValue: Functions.PrepStringArg(arg: labelEn));

                        result = result.Replace(
                            oldValue: "@descriptionEn",
                            newValue: Functions.PrepStringArg(arg: descriptionEn));

                        result = result.Replace(
                            oldValue: "@defaultInOlap",
                            newValue: Functions.PrepNBoolArg(arg: defaultInOlap));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_api_save_estimation_period
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="estimateDateBegin">Initial date of the estimation period</param>
                    /// <param name="estimateDateEnd">Final date of the estimation period</param>
                    /// <param name="labelCs">Panel reference year set group label in national language</param>
                    /// <param name="descriptionCs">Panel reference year set group label in national language</param>
                    /// <param name="labelEn">Panel reference year set group label in English</param>
                    /// <param name="descriptionEn">Panel reference year set group description in English</param>
                    /// <param name="defaultInOlap">Limits the display of periods to those set to true (it performs in the next calculation). Default setting is false.</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table with identifier of the saved panel reference year set group</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<DateTime> estimateDateBegin,
                        Nullable<DateTime> estimateDateEnd,
                        string labelCs,
                        string descriptionCs,
                        string labelEn,
                        string descriptionEn,
                        Nullable<bool> defaultInOlap,
                        NpgsqlTransaction transaction = null)
                    {
                        Nullable<int> val = Execute(
                            database: database,
                            estimateDateBegin: estimateDateBegin,
                            estimateDateEnd: estimateDateEnd,
                            labelCs: labelCs,
                            descriptionCs: descriptionCs,
                            labelEn: labelEn,
                            descriptionEn: descriptionEn,
                            defaultInOlap: defaultInOlap,
                            transaction: transaction);

                        DataTable dt = new();

                        dt.Columns.Add(column: new DataColumn()
                        {
                            ColumnName = Name,
                            DataType = Type.GetType(typeName: "System.Int32")
                        });

                        DataRow row = dt.NewRow();

                        Functions.SetNIntArg(
                            row: row,
                            name: Name,
                            val: val);

                        dt.Rows.Add(row: row);

                        return dt;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_api_save_estimation_period
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="estimateDateBegin">Initial date of the estimation period</param>
                    /// <param name="estimateDateEnd">Final date of the estimation period</param>
                    /// <param name="labelCs">Panel reference year set group label in national language</param>
                    /// <param name="descriptionCs">Panel reference year set group label in national language</param>
                    /// <param name="labelEn">Panel reference year set group label in English</param>
                    /// <param name="descriptionEn">Panel reference year set group description in English</param>
                    /// <param name="defaultInOlap">Limits the display of periods to those set to true (it performs in the next calculation). Default setting is false.</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Identifier of the saved panel reference year set group</returns>
                    public static Nullable<int> Execute(
                        NfiEstaDB database,
                        Nullable<DateTime> estimateDateBegin,
                        Nullable<DateTime> estimateDateEnd,
                        string labelCs,
                        string descriptionCs,
                        string labelEn,
                        string descriptionEn,
                        Nullable<bool> defaultInOlap,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(
                            estimateDateBegin: estimateDateBegin,
                            estimateDateEnd: estimateDateEnd,
                            labelCs: labelCs,
                            descriptionCs: descriptionCs,
                            labelEn: labelEn,
                            descriptionEn: descriptionEn,
                            defaultInOlap: defaultInOlap);

                        NpgsqlParameter pEstimateDateBegin = new(
                            parameterName: "estimateDateBegin",
                            parameterType: NpgsqlDbType.Date);

                        NpgsqlParameter pEstimateDateEnd = new(
                            parameterName: "estimateDateEnd",
                            parameterType: NpgsqlDbType.Date);

                        NpgsqlParameter pLabelCs = new(
                            parameterName: "labelCs",
                            parameterType: NpgsqlDbType.Varchar);

                        NpgsqlParameter pDescriptionCs = new(
                            parameterName: "descriptionCs",
                            parameterType: NpgsqlDbType.Text);

                        NpgsqlParameter pLabelEn = new(
                            parameterName: "labelEn",
                            parameterType: NpgsqlDbType.Varchar);

                        NpgsqlParameter pDescriptionEn = new(
                            parameterName: "descriptionEn",
                            parameterType: NpgsqlDbType.Text);

                        NpgsqlParameter pDefaultInOlap = new(
                            parameterName: "defaultInOlap",
                            parameterType: NpgsqlDbType.Boolean);

                        if (estimateDateBegin != null)
                        {
                            pEstimateDateBegin.Value = estimateDateBegin;
                        }
                        else
                        {
                            pEstimateDateBegin.Value = DBNull.Value;
                        }

                        if (estimateDateEnd != null)
                        {
                            pEstimateDateEnd.Value = estimateDateEnd;
                        }
                        else
                        {
                            pEstimateDateEnd.Value = DBNull.Value;
                        }

                        if (labelCs != null)
                        {
                            pLabelCs.Value = labelCs;
                        }
                        else
                        {
                            pLabelCs.Value = DBNull.Value;
                        }

                        if (descriptionCs != null)
                        {
                            pDescriptionCs.Value = descriptionCs;
                        }
                        else
                        {
                            pDescriptionCs.Value = DBNull.Value;
                        }

                        if (labelEn != null)
                        {
                            pLabelEn.Value = labelEn;
                        }
                        else
                        {
                            pLabelEn.Value = DBNull.Value;
                        }

                        if (descriptionEn != null)
                        {
                            pDescriptionEn.Value = descriptionEn;
                        }
                        else
                        {
                            pDescriptionEn.Value = DBNull.Value;
                        }

                        if (defaultInOlap != null)
                        {
                            pDefaultInOlap.Value = defaultInOlap;
                        }
                        else
                        {
                            pDefaultInOlap.Value = DBNull.Value;
                        }

                        string strResult = database.Postgres.ExecuteScalar(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pEstimateDateBegin, pEstimateDateEnd,
                            pLabelCs, pDescriptionCs, pLabelEn, pDescriptionEn, pDefaultInOlap);

                        if (String.IsNullOrEmpty(value: strResult))
                        {
                            return null;
                        }
                        else
                        {
                            if (Int32.TryParse(s: strResult, result: out int intResult))
                            {
                                return intResult;
                            }
                            else
                            {
                                return null;
                            }
                        }
                    }

                    #endregion Methods

                }

                #endregion FnApiSaveEstimationPeriod

            }

        }
    }
}
