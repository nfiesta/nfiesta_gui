﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {

            /// <summary>
            /// Stored procedures of schema nfiesta
            /// </summary>
            public static partial class NfiEstaFunctions
            {
                // fn_get_panel_refyearset_group

                #region FnGetPanelRefYearSetGroup

                /// <summary>
                /// Wrapper for stored procedure fn_get_panel_refyearset_group.
                /// Returns identificator of panel_refyearset_group.
                /// </summary>
                public static class FnGetPanelRefYearSetGroup
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName = NfiEstaSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name = "fn_get_panel_refyearset_group";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_panels integer[], ",
                            $"_refyearsets integer[]; ",
                            $"returns: integer");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    $SchemaName.$Name(@panels, @refyearsets)::integer AS $Name;{Environment.NewLine}");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="panels"></param>
                    /// <param name="refyearsets"></param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        List<Nullable<int>> panels,
                        List<Nullable<int>> refyearsets)
                    {
                        string result = SQL;

                        result = result.Replace(
                            oldValue: "@panels",
                            newValue: Functions.PrepNIntArrayArg(args: panels, dbType: "int4"));

                        result = result.Replace(
                           oldValue: "@refyearsets",
                           newValue: Functions.PrepNIntArrayArg(args: refyearsets, dbType: "int4"));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_get_panel_refyearset_group
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="panels"></param>
                    /// <param name="refyearsets"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns></returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        List<Nullable<int>> panels,
                        List<Nullable<int>> refyearsets,
                        NpgsqlTransaction transaction = null)
                    {
                        Nullable<int> val = Execute(
                            database: database,
                            panels: panels,
                            refyearsets: refyearsets,
                            transaction: transaction);

                        DataTable dt = new();

                        dt.Columns.Add(column: new DataColumn()
                        {
                            ColumnName = Name,
                            DataType = Type.GetType(typeName: "System.Int32")
                        });

                        DataRow row = dt.NewRow();

                        Functions.SetNIntArg(
                            row: row,
                            name: Name,
                            val: val);

                        dt.Rows.Add(row: row);

                        return dt;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_get_panel_refyearset_group
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="panels"></param>
                    /// <param name="refyearsets"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns></returns>
                    public static Nullable<int> Execute(
                        NfiEstaDB database,
                        List<Nullable<int>> panels,
                        List<Nullable<int>> refyearsets,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(panels: panels, refyearsets: refyearsets);

                        NpgsqlParameter pPanels = new(
                            parameterName: "panels",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        NpgsqlParameter pRefyearsets = new(
                            parameterName: "refyearsets",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        if (panels != null)
                        {
                            pPanels.Value = panels.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pPanels.Value = DBNull.Value;
                        }

                        if (refyearsets != null)
                        {
                            pRefyearsets.Value = refyearsets.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pRefyearsets.Value = DBNull.Value;
                        }

                        string strResult = database.Postgres.ExecuteScalar(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pPanels, pRefyearsets);

                        if (String.IsNullOrEmpty(value: strResult))
                        {
                            return null;
                        }
                        else
                        {
                            if (Int32.TryParse(s: strResult, result: out int intResult))
                            {
                                return intResult;
                            }
                            else
                            {
                                return null;
                            }
                        }
                    }

                    #endregion Methods

                }

                #endregion FnGetPanelRefYearSetGroup

            }

        }
    }
}