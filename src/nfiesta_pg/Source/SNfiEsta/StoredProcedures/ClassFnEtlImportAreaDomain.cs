﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {

            /// <summary>
            /// Stored procedures of schema nfiesta
            /// </summary>
            public static partial class NfiEstaFunctions
            {
                // fn_etl_import_area_domain

                #region FnEtlImportAreaDomain

                /// <summary>
                /// Wrapper for stored procedure fn_etl_import_area_domain.
                /// Function inserts a record into table c_area_domain based on given parameters.
                /// </summary>
                public static class FnEtlImportAreaDomain
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName = NfiEstaSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name = "fn_etl_import_area_domain";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_id integer, ",
                            $"_label character varying, ",
                            $"_description text, ",
                            $"_label_en character varying, ",
                            $"_description_en text, ",
                            $"_atomic boolean; ",
                            $"returns: ",
                            $"TABLE(",
                            $"id integer, ",
                            $"etl_id integer)");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    $Name.id          AS id,{Environment.NewLine}",
                            $"    $Name.etl_id      AS etl_id{Environment.NewLine}",
                            $"FROM{Environment.NewLine}",
                            $"    $SchemaName.$Name(@id, @label, @description, @labelEn, @descriptionEn, @atomic)",
                            $"AS {Name};{Environment.NewLine}");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="id"></param>
                    /// <param name="label"></param>
                    /// <param name="description"></param>
                    /// <param name="labelEn"></param>
                    /// <param name="descriptionEn"></param>
                    /// <param name="atomic"></param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        Nullable<int> id,
                        string label,
                        string description,
                        string labelEn,
                        string descriptionEn,
                        Nullable<bool> atomic)
                    {
                        string result = SQL;

                        result = result.Replace(
                            oldValue: "@id",
                            newValue: Functions.PrepNIntArg(arg: id));

                        result = result.Replace(
                             oldValue: "@label",
                             newValue: Functions.PrepStringArg(arg: label));

                        result = result.Replace(
                             oldValue: "@description",
                             newValue: Functions.PrepStringArg(arg: description));

                        result = result.Replace(
                             oldValue: "@labelEn",
                             newValue: Functions.PrepStringArg(arg: labelEn));

                        result = result.Replace(
                             oldValue: "@descriptionEn",
                             newValue: Functions.PrepStringArg(arg: descriptionEn));

                        result = result.Replace(
                             oldValue: "@atomic",
                             newValue: Functions.PrepNBoolArg(arg: atomic));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_etl_import_area_domain
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="id"></param>
                    /// <param name="label"></param>
                    /// <param name="description"></param>
                    /// <param name="labelEn"></param>
                    /// <param name="descriptionEn"></param>
                    /// <param name="atomic"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns></returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> id,
                        string label,
                        string description,
                        string labelEn,
                        string descriptionEn,
                        Nullable<bool> atomic,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(
                            id: id,
                            label: label,
                            description: description,
                            labelEn: labelEn,
                            descriptionEn: descriptionEn,
                            atomic: atomic);

                        NpgsqlParameter pId = new(
                            parameterName: "id",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pLabel = new(
                            parameterName: "label",
                            parameterType: NpgsqlDbType.Varchar);

                        NpgsqlParameter pDescription = new(
                            parameterName: "description",
                            parameterType: NpgsqlDbType.Text);

                        NpgsqlParameter pLabelEn = new(
                            parameterName: "labelEn",
                            parameterType: NpgsqlDbType.Varchar);

                        NpgsqlParameter pDescriptionEn = new(
                            parameterName: "descriptionEn",
                            parameterType: NpgsqlDbType.Text);

                        NpgsqlParameter pAtomic = new(
                            parameterName: "atomic",
                            parameterType: NpgsqlDbType.Boolean);

                        if (id != null)
                        {
                            pId.Value = (int)id;
                        }
                        else
                        {
                            pId.Value = DBNull.Value;
                        }

                        if (label != null)
                        {
                            pLabel.Value = (string)label;
                        }
                        else
                        {
                            pLabel.Value = DBNull.Value;
                        }

                        if (description != null)
                        {
                            pDescription.Value = (string)description;
                        }
                        else
                        {
                            pDescription.Value = DBNull.Value;
                        }

                        if (labelEn != null)
                        {
                            pLabelEn.Value = (string)labelEn;
                        }
                        else
                        {
                            pLabelEn.Value = DBNull.Value;
                        }

                        if (descriptionEn != null)
                        {
                            pDescriptionEn.Value = (string)descriptionEn;
                        }
                        else
                        {
                            pDescriptionEn.Value = DBNull.Value;
                        }

                        if (atomic != null)
                        {
                            pAtomic.Value = (bool)atomic;
                        }
                        else
                        {
                            pAtomic.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteQuery(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pId, pLabel, pDescription, pLabelEn, pDescriptionEn, pAtomic);
                    }

                    #endregion Methods

                }

                #endregion FnEtlImportAreaDomain

            }

        }
    }
}