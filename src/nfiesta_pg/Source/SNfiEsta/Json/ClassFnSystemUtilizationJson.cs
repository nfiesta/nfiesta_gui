﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Newtonsoft.Json.Linq;
using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Runtime.Versioning;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {

            /// <summary>
            /// JSON file
            /// from fn_system_utilization function
            /// </summary>
            public class FnSystemUtilizationJson
            {

                #region Constants

                /// <summary>
                /// Table name
                /// </summary>
                public const string TableName = "FnSystemUtilizationJson";

                /// <summary>
                /// Valid json text for test
                /// </summary>
                public const string TestJsonText =
                    "{\"load\": {\"1_min\": 0.0, \"5_min\": 0.0, \"15_min\": 0.0}, \"cpu_count\": 24, \"connection_limit\": -1}";


                /// <summary>
                /// Column identifier
                /// </summary>
                public const string ColId = "id";

                /// <summary>
                /// Element load
                /// </summary>
                public const string ElementLoad = "load";


                /// <summary>
                /// Element cpu_count
                /// </summary>
                public const string ElementCPUCount = "cpu_count";

                /// <summary>
                /// Column cpu_count
                /// </summary>
                public const string ColCPUCount = "cpu_count";


                /// <summary>
                /// Element connection_limit
                /// </summary>
                public const string ElementConnectionLimit = "connection_limit";

                /// <summary>
                /// Column connection_limit
                /// </summary>
                public const string ColConnectionLimit = "connection_limit";


                /// <summary>
                /// Element 1_min
                /// </summary>
                public const string ElementLoad1Min = "1_min";

                /// <summary>
                /// Column 1_min
                /// </summary>
                public const string ColLoad1Min = "load_1_min";


                /// <summary>
                /// Element 5_min
                /// </summary>
                public const string ElementLoad5Min = "5_min";

                /// <summary>
                /// Column 5_min
                /// </summary>
                public const string ColLoad5Min = "load_5_min";


                /// <summary>
                /// Element 15_min
                /// </summary>
                public const string ElementLoad15Min = "15_min";

                /// <summary>
                /// Column 15_min
                /// </summary>
                public const string ColLoad15Min = "load_15_min";

                #endregion Constants


                #region Private Fields

                /// <summary>
                /// Root JSON object
                /// </summary>
                private JObject root;

                /// <summary>
                /// JSON text
                /// </summary>
                private string text;

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                public FnSystemUtilizationJson()
                {
                    Root = null;
                    Text = null;
                }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// Root JSON object
                /// (read-only)
                /// </summary>
                public JObject Root
                {
                    get
                    {
                        return root;
                    }
                    private set
                    {
                        root = value;
                    }
                }

                /// <summary>
                /// JSON text
                /// (read-only)
                /// </summary>
                public string Text
                {
                    get
                    {
                        return text ?? String.Empty;
                    }
                    private set
                    {
                        text = value ?? String.Empty;
                    }
                }

                /// <summary>
                /// Element load value
                /// (read-only)
                /// </summary>
                public JObject JsonLoad
                {
                    get
                    {
                        return
                            Functions.GetJObject(
                                obj: Root,
                                name: ElementLoad,
                                valid: out bool _,
                                message: out string _);
                    }
                }

                /// <summary>
                /// Element 1_min value
                /// (read-only)
                /// </summary>
                public Nullable<double> Load1Min
                {
                    get
                    {
                        return
                            Functions.GetJNDouble(
                                obj: JsonLoad,
                                name: ElementLoad1Min,
                                valid: out bool _,
                                message: out string _);
                    }
                }

                /// <summary>
                /// Element 5_min value
                /// (read-only)
                /// </summary>
                public Nullable<double> Load5Min
                {
                    get
                    {
                        return
                            Functions.GetJNDouble(
                                obj: JsonLoad,
                                name: ElementLoad5Min,
                                valid: out bool _,
                                message: out string _);
                    }
                }

                /// <summary>
                /// Element 15_min value
                /// (read-only)
                /// </summary>
                public Nullable<double> Load15Min
                {
                    get
                    {
                        return
                            Functions.GetJNDouble(
                                obj: JsonLoad,
                                name: ElementLoad15Min,
                                valid: out bool _,
                                message: out string _);
                    }
                }

                /// <summary>
                /// Element cpu_count value
                /// (read-only)
                /// </summary>
                public Nullable<int> CPUCount
                {
                    get
                    {
                        return
                            Functions.GetJNInteger(
                                obj: Root,
                                name: ElementCPUCount,
                                valid: out bool _,
                                message: out string _);
                    }
                }

                /// <summary>
                /// Element connection_limit value
                /// (read-only)
                /// </summary>
                public Nullable<int> ConnectionLimit
                {
                    get
                    {
                        return
                            Functions.GetJNInteger(
                                obj: Root,
                                name: ElementConnectionLimit,
                                valid: out bool _,
                                message: out string _);
                    }
                }

                /// <summary>
                /// Data table with data
                /// that was loaded from JSON file
                /// (read-only)
                /// </summary>
                public DataTable Data
                {
                    get
                    {
                        DataTable result = EmptyDataTable();

                        DataRow row = result.NewRow();
                        Functions.SetIntArg(row: row, name: ColId, val: 0);
                        Functions.SetNDoubleArg(row: row, name: ColLoad1Min, val: Load1Min);
                        Functions.SetNDoubleArg(row: row, name: ColLoad5Min, val: Load5Min);
                        Functions.SetNDoubleArg(row: row, name: ColLoad15Min, val: Load15Min);
                        Functions.SetNIntArg(row: row, name: ColCPUCount, val: CPUCount);
                        Functions.SetNIntArg(row: row, name: ColConnectionLimit, val: ConnectionLimit);
                        result.Rows.Add(row: row);

                        return result;
                    }
                }

                #endregion Properties


                #region Static Methods

                /// <summary>
                /// Empty data table for data
                /// that will be loaded from JSON file
                /// </summary>
                /// <returns> Empty data table for data
                /// that will be loaded from JSON file</returns>
                public static DataTable EmptyDataTable()
                {
                    DataTable result = new()
                    {
                        TableName = TableName
                    };

                    result.Columns.Add(column: new DataColumn()
                    {
                        ColumnName = ColId,
                        DataType = Type.GetType(typeName: "System.Int32")
                    });

                    result.Columns.Add(column: new DataColumn()
                    {
                        ColumnName = ColLoad1Min,
                        DataType = Type.GetType(typeName: "System.Double")
                    });

                    result.Columns.Add(column: new DataColumn()
                    {
                        ColumnName = ColLoad5Min,
                        DataType = Type.GetType(typeName: "System.Double")
                    });

                    result.Columns.Add(column: new DataColumn()
                    {
                        ColumnName = ColLoad15Min,
                        DataType = Type.GetType(typeName: "System.Double")
                    });

                    result.Columns.Add(column: new DataColumn()
                    {
                        ColumnName = ColCPUCount,
                        DataType = Type.GetType(typeName: "System.Int32")
                    });

                    result.Columns.Add(column: new DataColumn()
                    {
                        ColumnName = ColConnectionLimit,
                        DataType = Type.GetType(typeName: "System.Int32")
                    });

                    return result;
                }

                #endregion Static Methods


                #region Methods

                /// <summary>
                /// Function loads object data from text variable
                /// that contains JSON file content
                /// </summary>
                /// <param name="text">Text variable that contains JSON file content</param>
                public void LoadFromText(string text)
                {
                    try
                    {
                        this.text = $"{text}";
                        root = JObject.Parse(json: this.text);
                    }
                    catch
                    {
                        root = null;
                    }
                }

                /// <summary>
                /// Function loads object data from JSON file
                /// </summary>
                /// <param name="path">Path to JSON file</param>
                public void LoadFromFile(string path)
                {
                    try
                    {
                        text = File.ReadAllText(path: path);
                        LoadFromText(text: text);
                    }
                    catch
                    {
                        root = null;
                    }
                }

                /// <summary>
                /// Sets DataSource for given DataGridView
                /// </summary>
                /// <param name="dataGridView">Object DataGridView</param>
                [SupportedOSPlatform("windows")]
                public void SetDataGridViewDataSource(System.Windows.Forms.DataGridView dataGridView)
                {
                    dataGridView.DataSource = Data.DefaultView;
                    dataGridView.Tag = this;
                }

                /// <summary>
                /// Sets header, visibility, formats by data type and alignment in columns of given DataGridView
                /// </summary>
                /// <param name="dataGridView">Object DataGridView</param>
                [SupportedOSPlatform("windows")]
                public static void FormatDataGridView(System.Windows.Forms.DataGridView dataGridView)
                {
                    // General:
                    dataGridView.AllowUserToAddRows = false;
                    dataGridView.AllowUserToDeleteRows = false;
                    dataGridView.AllowUserToResizeRows = false;

                    dataGridView.AlternatingRowsDefaultCellStyle = new System.Windows.Forms.DataGridViewCellStyle()
                    {
                        BackColor = Color.LightCyan
                    };

                    dataGridView.AutoGenerateColumns = false;
                    dataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
                    dataGridView.BackgroundColor = SystemColors.Window;
                    dataGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
                    dataGridView.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
                    dataGridView.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;

                    dataGridView.ColumnHeadersDefaultCellStyle = new System.Windows.Forms.DataGridViewCellStyle()
                    {
                        Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft,
                        BackColor = Color.DarkBlue,
                        Font = new Font("Microsoft Sans Serif", 9.75F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(238))),
                        ForeColor = Color.White,
                        SelectionBackColor = Color.DarkBlue,
                        SelectionForeColor = Color.White,
                        WrapMode = System.Windows.Forms.DataGridViewTriState.True
                    };

                    dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;

                    dataGridView.DefaultCellStyle = new System.Windows.Forms.DataGridViewCellStyle()
                    {
                        Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft,
                        BackColor = Color.White,
                        Font = new Font("Microsoft Sans Serif", 9.75F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(238))),
                        ForeColor = SystemColors.ControlText,
                        Format = "N3",
                        NullValue = "NULL",
                        SelectionBackColor = Color.Bisque,
                        SelectionForeColor = SystemColors.ControlText,
                        WrapMode = System.Windows.Forms.DataGridViewTriState.False
                    };

                    dataGridView.EnableHeadersVisualStyles = false;
                    dataGridView.MultiSelect = false;
                    dataGridView.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
                    dataGridView.RowHeadersVisible = false;
                    dataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;

                    dataGridView.Columns[ColId].Visible = false;
                }

                #endregion Methods

            }

        }
    }
}
