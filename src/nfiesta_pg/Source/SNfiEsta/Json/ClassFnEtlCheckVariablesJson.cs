﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Runtime.Versioning;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {

            /// <summary>
            /// JSON file
            /// from fn_etl_check_variables function
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            public class FnEtlCheckVariablesJson(
                LanguageVersion language = LanguageVersion.International,
                Language elementKeyInternational = PostgreSQL.Language.EN,
                Language elementKeyNational = PostgreSQL.Language.CS)
            {

                #region Constants

                /// <summary>
                /// Table name
                /// </summary>
                public const string TableName = "FnEtlCheckVariablesJson";

                /// <summary>
                /// Identifier column
                /// </summary>
                public const string ColId = "id";

                /// <summary>
                /// Element language international
                /// </summary>
                private readonly string ElementKeyEn = LanguageList.ISO_639_1(
                        language: elementKeyInternational);

                /// <summary>
                /// Element language national
                /// </summary>
                private readonly string ElementKeyCs = LanguageList.ISO_639_1(
                        language: elementKeyNational);

                /// <summary>
                /// Element subpopulation
                /// </summary>
                public const string ElementKeySubPopulation = "sub_population";

                /// <summary>
                /// Element subpopulation category
                /// </summary>
                public const string ElementKeySubPopulationCategory = "sub_population_category";

                /// <summary>
                /// Element area domain
                /// </summary>
                public const string ElementKeyAreaDomain = "area_domain";

                /// <summary>
                /// Element area domain category
                /// </summary>
                public const string ElementKeyAreaDomainCategory = "area_domain_category";

                /// <summary>
                /// Valid json text for test
                /// </summary>
                public const string TestJsonText =
                    "{ \"cs\" : [" +
                    "{ \"sub_population\" : \"null\", \"sub_population_category\" : \"null\", \"area_domain\" : \"katastr nemovitostí\", \"area_domain_category\" : \"les\"}, " +
                    "{ \"sub_population\" : \"null\", \"sub_population_category\" : \"null\", \"area_domain\" : \"katastr nemovitostí\", \"area_domain_category\" : \"neles\"}], " +
                    "\"en\" : [" +
                    "{ \"sub_population\" : \"null\", \"sub_population_category\" : \"null\", \"area_domain\" : \"land registry\", \"area_domain_category\" : \"forest\"}, " +
                    "{ \"sub_population\" : \"null\", \"sub_population_category\" : \"null\", \"area_domain\" : \"land registry\", \"area_domain_category\" : \"non-forest\"}]" +
                    "}";

                #endregion Constants


                #region Private Fields

                /// <summary>
                /// Root JSON object
                /// </summary>
                private JObject root = null;

                /// <summary>
                /// JSON text
                /// </summary>
                private string text = String.Empty;

                /// <summary>
                /// Language version
                /// </summary>
                private LanguageVersion language = language;

                #endregion Private Fields


                #region Properties

                /// <summary>
                /// Language version
                /// </summary>
                public LanguageVersion Language
                {
                    get
                    {
                        return language;
                    }
                    set
                    {
                        language = value;
                    }
                }

                /// <summary>
                /// List of JSON objects
                /// for international version (read-only)
                /// </summary>
                private List<JObject> JsonEn
                {
                    get
                    {
                        return
                            Functions.GetJObjectList(
                                obj: root,
                                name: ElementKeyEn,
                                valid: out bool _,
                                message: out string _);
                    }
                }

                /// <summary>
                /// List of JSON objects
                /// for national version (read-only)
                /// </summary>
                private List<JObject> JsonCs
                {
                    get
                    {
                        return
                            Functions.GetJObjectList(
                                obj: root,
                                name: ElementKeyCs,
                                valid: out bool _,
                                message: out string _)
                            ?? JsonEn;
                    }
                }

                /// <summary>
                /// List of fn_etl_check_variables results (read-only)
                /// </summary>
                public List<TEtlCheckVariablesJson> Items
                {
                    get
                    {
                        List<JObject> items;

                        switch (Language)
                        {
                            case LanguageVersion.International:
                                items = JsonEn;
                                break;

                            case LanguageVersion.National:
                                items = JsonCs;
                                break;

                            default:
                                return null;
                        }

                        if (items == null)
                        {
                            return null;
                        }

                        List<TEtlCheckVariablesJson> result = [];

                        foreach (JObject obj in items)
                        {
                            result.Add(
                                item: new TEtlCheckVariablesJson(
                                    jsonEtlCheckVariable: obj));
                        }

                        return result;
                    }
                }

                /// <summary>
                /// Data table with data that was loaded from JSON file (read-only)
                /// </summary>
                public DataTable Data
                {
                    get
                    {
                        List<TEtlCheckVariablesJson> items = Items;

                        DataTable result = EmptyDataTable();
                        int i = 0;

                        if (items != null)
                        {
                            foreach (TEtlCheckVariablesJson item in items)
                            {
                                DataRow row = result.NewRow();

                                if (item != null)
                                {
                                    Functions.SetIntArg(row: row,
                                        name: ColId, val: ++i);
                                    Functions.SetStringArg(row: row,
                                        name: ElementKeySubPopulation, val: item.SubPopulation);
                                    Functions.SetStringArg(row: row,
                                        name: ElementKeySubPopulationCategory, val: item.SubPopulationCategory);
                                    Functions.SetStringArg(row: row,
                                        name: ElementKeyAreaDomain, val: item.AreaDomain);
                                    Functions.SetStringArg(row: row,
                                        name: ElementKeyAreaDomainCategory, val: item.AreaDomainCategory);
                                }
                                else
                                {
                                    Functions.SetIntArg(row: row,
                                        name: ColId, val: ++i);
                                    Functions.SetStringArg(row: row,
                                        name: ElementKeySubPopulation, val: null);
                                    Functions.SetStringArg(row: row,
                                        name: ElementKeySubPopulationCategory, val: null);
                                    Functions.SetStringArg(row: row,
                                        name: ElementKeyAreaDomain, val: null);
                                    Functions.SetStringArg(row: row,
                                        name: ElementKeyAreaDomainCategory, val: null);
                                }

                                result.Rows.Add(row: row);
                            }
                        }
                        else
                        {
                            DataRow row = result.NewRow();

                            Functions.SetIntArg(row: row,
                                        name: ColId, val: ++i);
                            Functions.SetStringArg(row: row,
                                name: ElementKeySubPopulation, val: null);
                            Functions.SetStringArg(row: row,
                                name: ElementKeySubPopulationCategory, val: null);
                            Functions.SetStringArg(row: row,
                                name: ElementKeyAreaDomain, val: null);
                            Functions.SetStringArg(row: row,
                                name: ElementKeyAreaDomainCategory, val: null);

                            result.Rows.Add(row: row);
                        }

                        return result;
                    }
                }

                /// <summary>
                /// JSON text (read-only)
                /// </summary>
                public string Text
                {
                    get
                    {
                        return text;
                    }
                }

                #endregion Properties


                #region Methods

                /// <summary>
                /// Function loads object data from text variable that contains JSON file content
                /// </summary>
                /// <param name="text">Text variable that contains JSON file content</param>
                public void LoadFromText(string text)
                {
                    try
                    {
                        this.text = $"{text}";
                        root = JObject.Parse(json: this.text);
                    }
                    catch
                    {
                        root = null;
                    }
                }

                /// <summary>
                /// Function loads object data from JSON file
                /// </summary>
                /// <param name="path">Path to JSON file</param>
                public void LoadFromFile(string path)
                {
                    try
                    {
                        text = File.ReadAllText(path: path);
                        LoadFromText(text: text);
                    }
                    catch
                    {
                        root = null;
                    }
                }

                /// <summary>
                /// Empty data table for data that will be loaded from JSON file
                /// </summary>
                /// <returns>Empty data table for data that will be loaded from JSON file</returns>
                public static DataTable EmptyDataTable()
                {
                    DataTable result = new()
                    {
                        TableName = TableName
                    };

                    result.Columns.Add(column: new DataColumn()
                    {
                        ColumnName = ColId,
                        DataType = Type.GetType(typeName: "System.Int32")
                    });

                    result.Columns.Add(column: new DataColumn()
                    {
                        ColumnName = ElementKeySubPopulation,
                        DataType = Type.GetType(typeName: "System.String")
                    });

                    result.Columns.Add(column: new DataColumn()
                    {
                        ColumnName = ElementKeySubPopulationCategory,
                        DataType = Type.GetType(typeName: "System.String")
                    });

                    result.Columns.Add(column: new DataColumn()
                    {
                        ColumnName = ElementKeyAreaDomain,
                        DataType = Type.GetType(typeName: "System.String")
                    });

                    result.Columns.Add(column: new DataColumn()
                    {
                        ColumnName = ElementKeyAreaDomainCategory,
                        DataType = Type.GetType(typeName: "System.String")
                    });

                    return result;
                }

                /// <summary>
                /// Sets DataSource for given DataGridView
                /// </summary>
                /// <param name="dataGridView">Object DataGridView</param>
                [SupportedOSPlatform("windows")]
                public void SetDataGridViewDataSource(System.Windows.Forms.DataGridView dataGridView)
                {
                    dataGridView.DataSource = Data.DefaultView;
                    dataGridView.Tag = this;
                }

                /// <summary>
                /// Sets header, visibility, formats by data type and alignment in columns of given DataGridView
                /// </summary>
                /// <param name="dataGridView">Object DataGridView</param>
                [SupportedOSPlatform("windows")]
                public static void FormatDataGridView(System.Windows.Forms.DataGridView dataGridView)
                {
                    // General:
                    dataGridView.AllowUserToAddRows = false;
                    dataGridView.AllowUserToDeleteRows = false;
                    dataGridView.AllowUserToResizeRows = false;

                    dataGridView.AlternatingRowsDefaultCellStyle = new System.Windows.Forms.DataGridViewCellStyle()
                    {
                        BackColor = Color.LightCyan
                    };

                    dataGridView.AutoGenerateColumns = false;
                    dataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
                    dataGridView.BackgroundColor = SystemColors.Window;
                    dataGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
                    dataGridView.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
                    dataGridView.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;

                    dataGridView.ColumnHeadersDefaultCellStyle = new System.Windows.Forms.DataGridViewCellStyle()
                    {
                        Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft,
                        BackColor = Color.DarkBlue,
                        Font = new Font("Microsoft Sans Serif", 9.75F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(238))),
                        ForeColor = Color.White,
                        SelectionBackColor = Color.DarkBlue,
                        SelectionForeColor = Color.White,
                        WrapMode = System.Windows.Forms.DataGridViewTriState.True
                    };

                    dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;

                    dataGridView.DefaultCellStyle = new System.Windows.Forms.DataGridViewCellStyle()
                    {
                        Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft,
                        BackColor = Color.White,
                        Font = new Font("Microsoft Sans Serif", 9.75F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(238))),
                        ForeColor = SystemColors.ControlText,
                        Format = "N3",
                        NullValue = "NULL",
                        SelectionBackColor = Color.Bisque,
                        SelectionForeColor = SystemColors.ControlText,
                        WrapMode = System.Windows.Forms.DataGridViewTriState.False
                    };

                    dataGridView.EnableHeadersVisualStyles = false;
                    dataGridView.MultiSelect = false;
                    dataGridView.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
                    dataGridView.RowHeadersVisible = false;
                    dataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;

                    dataGridView.Columns[ColId].Visible = false;
                }

                #endregion Methods


                #region Internal Classes

                /// <summary>
                /// fn_etl_check_variables result
                /// </summary>
                /// <remarks>
                /// Constructor
                /// </remarks>
                /// <param name="jsonEtlCheckVariable">JSON object for etl check variable</param>
                public class TEtlCheckVariablesJson(JObject jsonEtlCheckVariable)
                {

                    #region Private Fields

                    /// <summary>
                    /// JSON object for etl check variable
                    /// </summary>
                    private readonly JObject jsonEtlCheckVariable = jsonEtlCheckVariable;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Subpopulation
                    /// </summary>
                    public string SubPopulation
                    {
                        get
                        {
                            return Functions.GetJString(
                                obj: jsonEtlCheckVariable,
                                name: ElementKeySubPopulation,
                                valid: out bool _,
                                message: out string _);
                        }
                    }

                    /// <summary>
                    /// Subpopulation category
                    /// </summary>
                    public string SubPopulationCategory
                    {
                        get
                        {
                            return Functions.GetJString(
                                obj: jsonEtlCheckVariable,
                                name: ElementKeySubPopulationCategory,
                                valid: out bool _,
                                message: out string _);
                        }
                    }

                    /// <summary>
                    /// Area domain
                    /// </summary>
                    public string AreaDomain
                    {
                        get
                        {
                            return Functions.GetJString(
                                obj: jsonEtlCheckVariable,
                                name: ElementKeyAreaDomain,
                                valid: out bool _,
                                message: out string _);
                        }
                    }

                    /// <summary>
                    /// Area domain category
                    /// </summary>
                    public string AreaDomainCategory
                    {
                        get
                        {
                            return Functions.GetJString(
                                obj: jsonEtlCheckVariable,
                                name: ElementKeyAreaDomainCategory,
                                valid: out bool _,
                                message: out string _);
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Returns the string that represents current object
                    /// </summary>
                    /// <returns>Returns the string that represents current object</returns>
                    public override string ToString()
                    {
                        string strSubPopulation = SubPopulation ?? Functions.StrNull;
                        string strSubPopulationCategory = SubPopulationCategory ?? Functions.StrNull;
                        string strAreaDomain = AreaDomain ?? Functions.StrNull;
                        string strAreaDomainCategory = AreaDomainCategory ?? Functions.StrNull;

                        return String.Concat(
                            $"SubPopulation : {strSubPopulation} {Environment.NewLine}",
                            $"SubPopulationCategory: {strSubPopulationCategory} {Environment.NewLine}",
                            $"AreaDomain : {strAreaDomain} {Environment.NewLine}",
                            $"AreaDomainCategory : {strAreaDomainCategory} {Environment.NewLine}");
                    }

                    /// <summary>
                    /// Determines whether specified object is equal to the current object
                    /// </summary>
                    /// <param name="obj">Speciefied object</param>
                    /// <returns>true/false</returns>
                    public override bool Equals(object obj)
                    {
                        // If the passed object is null, return False
                        if (obj == null)
                        {
                            return false;
                        }

                        // If the passed object is not TEtlCheckVariablesJson Type, return False
                        if (obj is not TEtlCheckVariablesJson)
                        {
                            return false;
                        }

                        return
                            (SubPopulation ?? Functions.StrNull) ==
                                (((TEtlCheckVariablesJson)obj).SubPopulation ?? Functions.StrNull) &&
                            (SubPopulationCategory ?? Functions.StrNull) ==
                                (((TEtlCheckVariablesJson)obj).SubPopulationCategory ?? Functions.StrNull) &&
                            (AreaDomain ?? Functions.StrNull) ==
                                (((TEtlCheckVariablesJson)obj).AreaDomain ?? Functions.StrNull) &&
                            (AreaDomainCategory ?? Functions.StrNull) ==
                                (((TEtlCheckVariablesJson)obj).AreaDomainCategory ?? Functions.StrNull);
                    }

                    /// <summary>
                    /// Returns the hash code
                    /// </summary>
                    /// <returns>Hash code</returns>
                    public override int GetHashCode()
                    {
                        return
                            (SubPopulation ?? Functions.StrNull).GetHashCode() ^
                            (SubPopulationCategory ?? Functions.StrNull).GetHashCode() ^
                            (AreaDomain ?? Functions.StrNull).GetHashCode() ^
                            (AreaDomainCategory ?? Functions.StrNull).GetHashCode();
                    }

                    #endregion Methods

                }

                #endregion Internal Classes

            }

        }
    }
}
