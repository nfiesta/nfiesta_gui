﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {

            /// <summary>
            /// JSON file
            /// from table c_target_variable
            /// and column metadata
            /// </summary>
            public class TargetVariableMetadataJson
            {

                #region Constants

                /// <summary>
                /// Element language international
                /// </summary>
                private readonly string ElementKeyEn =
                    LanguageList.ISO_639_1(language: LanguageName.English);

                /// <summary>
                /// Element language national
                /// </summary>
                private readonly string ElementKeyCs =
                    LanguageList.ISO_639_1(language: LanguageName.Czech);

                /// <summary>
                /// NullValue
                /// </summary>
                private const string ElementIsNull = "null";


                /// <summary>
                /// label
                /// </summary>
                private const string ElementKeyLabel = "label";

                /// <summary>
                /// description
                /// </summary>
                private const string ElementKeyDescription = "description";


                /// <summary>
                /// indicator
                /// </summary>
                private const string ElementKeyIndicator = "indicator";

                /// <summary>
                /// state or change
                /// </summary>
                private const string ElementKeyStateOrChange = "state or change";

                /// <summary>
                /// unit
                /// </summary>
                private const string ElementKeyUnit = "unit";

                /// <summary>
                /// local densities
                /// </summary>
                private const string ElementKeyLocalDensities = "local densities";


                /// <summary>
                /// object type label
                /// </summary>
                private const string ElementKeyObjectTypeLabel = "object type label";

                /// <summary>
                /// object type description
                /// </summary>
                private const string ElementKeyObjectTypeDescription = "object type description";


                /// <summary>
                /// object label
                /// </summary>
                private const string ElementKeyObjectLabel = "object label";

                /// <summary>
                /// object description
                /// </summary>
                private const string ElementKeyObjectDescription = "object description";


                /// <summary>
                /// use_negative
                /// </summary>
                private const string ElementKeyUseNegative = "use_negative";


                /// <summary>
                /// version
                /// </summary>
                private const string ElementKeyVersion = "version";

                /// <summary>
                /// definition variant
                /// </summary>
                private const string ElementKeyDefinitionVariant = "definition variant";

                /// <summary>
                /// area domain
                /// </summary>
                private const string ElementKeyAreaDomain = "area domain";

                /// <summary>
                /// population
                /// </summary>
                private const string ElementKeyPopulation = "population";


                /// <summary>
                /// id
                /// </summary>
                public const string ColId = "id";

                /// <summary>
                /// target_variable_id
                /// </summary>
                public const string ColTargetVariableId = "target_variable_id";

                /// <summary>
                /// ldsity_id
                /// </summary>
                public const string ColLocalDensityId = "ldsity_id";


                /// <summary>
                /// json_validity
                /// </summary>
                public const string ColJsonValidity = "json_validity";


                /// <summary>
                /// indicator_label_cs
                /// </summary>
                public const string ColIndicatorLabelCs = "indicator_label_cs";

                /// <summary>
                /// indicator_description_cs
                /// </summary>
                public const string ColIndicatorDescriptionCs = "indicator_description_cs";

                /// <summary>
                /// indicator_label_en
                /// </summary>
                public const string ColIndicatorLabelEn = "indicator_label_en";

                /// <summary>
                /// indicator_description_en
                /// </summary>
                public const string ColIndicatorDescriptionEn = "indicator_description_en";


                /// <summary>
                /// state_or_change_label_cs
                /// </summary>
                public const string ColStateOrChangeLabelCs = "state_or_change_label_cs";

                /// <summary>
                /// state_or_change_description_cs
                /// </summary>
                public const string ColStateOrChangeDescriptionCs = "state_or_change_description_cs";

                /// <summary>
                /// state_or_change_label_en
                /// </summary>
                public const string ColStateOrChangeLabelEn = "state_or_change_label_en";

                /// <summary>
                /// state_or_change_description_en
                /// </summary>
                public const string ColStateOrChangeDescriptionEn = "state_or_change_description_en";


                /// <summary>
                /// unit_label_cs
                /// </summary>
                public const string ColUnitLabelCs = "unit_label_cs";

                /// <summary>
                /// unit_description_cs
                /// </summary>
                public const string ColUnitDescriptionCs = "unit_description_cs";

                /// <summary>
                /// unit_label_en
                /// </summary>
                public const string ColUnitLabelEn = "unit_label_en";

                /// <summary>
                /// unit_description_en
                /// </summary>
                public const string ColUnitDescriptionEn = "unit_description_en";


                /// <summary>
                /// ldsity_object_type_label_cs
                /// </summary>
                public const string ColLocalDensityObjectTypeLabelCs = "ldsity_object_type_label_cs";

                /// <summary>
                /// ldsity_object_type_description_cs
                /// </summary>
                public const string ColLocalDensityObjectTypeDescriptionCs = "ldsity_object_type_description_cs";

                /// <summary>
                /// ldsity_object_type_label_en
                /// </summary>
                public const string ColLocalDensityObjectTypeLabelEn = "ldsity_object_type_label_en";

                /// <summary>
                /// ldsity_object_type_description_en
                /// </summary>
                public const string ColLocalDensityObjectTypeDescriptionEn = "ldsity_object_type_description_en";


                /// <summary>
                /// ldsity_object_label_cs
                /// </summary>
                public const string ColLocalDensityObjectLabelCs = "ldsity_object_label_cs";

                /// <summary>
                /// ldsity_object_description_cs
                /// </summary>
                public const string ColLocalDensityObjectDescriptionCs = "ldsity_object_description_cs";

                /// <summary>
                /// ldsity_object_label_en
                /// </summary>
                public const string ColLocalDensityObjectLabelEn = "ldsity_object_label_en";

                /// <summary>
                /// ldsity_object_description_en
                /// </summary>
                public const string ColLocalDensityObjectDescriptionEn = "ldsity_object_description_en";


                /// <summary>
                /// ldsity_label_cs
                /// </summary>
                public const string ColLocalDensityLabelCs = "ldsity_label_cs";

                /// <summary>
                /// ldsity_description_cs
                /// </summary>
                public const string ColLocalDensityDescriptionCs = "ldsity_description_cs";

                /// <summary>
                /// ldsity_label_en
                /// </summary>
                public const string ColLocalDensityLabelEn = "ldsity_label_en";

                /// <summary>
                /// ldsity_description_en
                /// </summary>
                public const string ColLocalDensityDescriptionEn = "ldsity_description_en";


                /// <summary>
                /// use_negative_cs
                /// </summary>
                public const string ColUseNegativeCs = "use_negative_cs";

                /// <summary>
                /// use_negative_en
                /// </summary>
                public const string ColUseNegativeEn = "use_negative_en";


                /// <summary>
                /// version_label_cs
                /// </summary>
                public const string ColVersionLabelCs = "version_label_cs";

                /// <summary>
                /// version_description_cs
                /// </summary>
                public const string ColVersionDescriptionCs = "version_description_cs";

                /// <summary>
                /// version_label_en
                /// </summary>
                public const string ColVersionLabelEn = "version_label_en";

                /// <summary>
                /// version_description_en
                /// </summary>
                public const string ColVersionDescriptionEn = "version_description_en";


                /// <summary>
                /// definition_variant_label_cs
                /// </summary>
                public const string ColDefinitionVariantLabelCs = "definition_variant_label_cs";

                /// <summary>
                /// definition_variant_description_cs
                /// </summary>
                public const string ColDefinitionVariantDescriptionCs = "definition_variant_description_cs";

                /// <summary>
                /// definition_variant_label_en
                /// </summary>
                public const string ColDefinitionVariantLabelEn = "definition_variant_label_en";

                /// <summary>
                /// definition_variant_description_en
                /// </summary>
                public const string ColDefinitionVariantDescriptionEn = "definition_variant_description_en";


                /// <summary>
                /// area_domain_label_cs
                /// </summary>
                public const string ColAreaDomainLabelCs = "area_domain_label_cs";

                /// <summary>
                /// area_domain_description_cs
                /// </summary>
                public const string ColAreaDomainDescriptionCs = "area_domain_description_cs";

                /// <summary>
                /// area_domain_label_en
                /// </summary>
                public const string ColAreaDomainLabelEn = "area_domain_label_en";

                /// <summary>
                /// area_domain_description_en
                /// </summary>
                public const string ColAreaDomainDescriptionEn = "area_domain_description_en";


                /// <summary>
                /// population_label_cs
                /// </summary>
                public const string ColPopulationLabelCs = "population_label_cs";

                /// <summary>
                /// population_description_cs
                /// </summary>
                public const string ColPopulationDescriptionCs = "population_description_cs";

                /// <summary>
                /// population_label_en
                /// </summary>
                public const string ColPopulationLabelEn = "population_label_en";

                /// <summary>
                /// population_description_en
                /// </summary>
                public const string ColPopulationDescriptionEn = "population_description_en";

                /// <summary>
                /// <para lang="cs">JSON pro nepřiřazenou cílovou proměnnou</para>
                /// <para lang="en">JSON for not assigned target variable</para>
                /// </summary>
                public const string NotAssignedCategory =
                    "{ \"cs\": { " +
                    "\"indicator\": { \"label\": \"hodnota nenalezena\", \"description\": \"hodnota nenalezena\" }, " +
                    "\"state or change\": { \"label\": \"hodnota nenalezena\", \"description\": \"hodnota nenalezena\" }, " +
                    "\"unit\": { \"label\": \"hodnota nenalezena\", \"description\": \"hodnota nenalezena\" }, " +
                    "\"local densities\": [{" +
                    "\"object type label\": \"core\", " +
                    "\"object type description\": \"hodnota nenalezena\", " +
                    "\"object label\": \"hodnota nenalezena\", " +
                    "\"object description\": \"hodnota nenalezena\", " +
                    "\"label\": \"hodnota nenalezena\", \"description\": \"hodnota nenalezena\", " +
                    "\"version\": {\"label\": \"hodnota nenalezena\", \"description\": \"hodnota nenalezena\"}, " +
                    "\"definition variant\": {\"label\": \"hodnota nenalezena\", \"description\": \"hodnota nenalezena\"}, " +
                    "\"area domain\": {\"label\": \"hodnota nenalezena\", \"description\": \"hodnota nenalezena\"}, " +
                    "\"population\": { \"label\": \"hodnota nenalezena\", \"description\": \"hodnota nenalezena\"}, " +
                    "\"use_negative\": false}]}, " +
                    "\"en\": { " +
                    "\"indicator\": { \"label\": \"value not found\", \"description\": \"value not found\"}, " +
                    "\"state or change\": { \"label\": \"value not found\", \"description\": \"value not found\"}, " +
                    "\"unit\": { \"label\": \"value not found\", \"description\": \"value not found\"}, " +
                    "\"local densities\": [{ " +
                    "\"object type label\": \"core\", " +
                    "\"object type description\": \"value not found\", " +
                    "\"object label\": \"value not found\", " +
                    "\"object description\": \"value not found\", " +
                    "\"label\": \"value not found\", " +
                    "\"description\": \"value not found\", " +
                    "\"version\": { \"label\": \"value not found\", \"description\": \"value not found\"}, " +
                    "\"definition variant\": { \"label\": \"value not found\", \"description\": \"value not found\"}, " +
                    "\"area domain\": { \"label\": \"value not found\", \"description\": \"value not found\"}, " +
                    "\"population\": { \"label\": \"value not found\", \"description\": \"value not found\"}, " +
                    "\"use_negative\": false}]}}";

                #endregion Constants


                #region Private Fields

                /// <summary>
                /// Root JSON object
                /// </summary>
                private JObject root;

                /// <summary>
                /// Message about problems found in JSON object
                /// </summary>
                private string errorMessage;

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="elementKeyInternational">Name of the JSON object for international version</param>
                /// <param name="elementKeyNational">Name of the JSON object for national version</param>
                public TargetVariableMetadataJson(
                    Language elementKeyInternational = PostgreSQL.Language.EN,
                    Language elementKeyNational = PostgreSQL.Language.CS)
                {
                    Root = null;
                    ErrorMessage = String.Empty;

                    ElementKeyEn = LanguageList.ISO_639_1(language: elementKeyInternational);
                    ElementKeyCs = LanguageList.ISO_639_1(language: elementKeyNational);
                }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// Root JSON object
                /// </summary>
                private JObject Root
                {
                    get
                    {
                        return root;
                    }
                    set
                    {
                        root = value;
                    }
                }


                /// <summary>
                /// JSON object
                /// for international version (read-only)
                /// </summary>
                private JObject JsonEn
                {
                    get
                    {
                        return
                            TargetVariableMetadataJson.GetJObject(
                                obj: Root,
                                key: ElementKeyEn);
                    }
                }

                /// <summary>
                /// JSON object
                /// for national version (read-only)
                /// </summary>
                private JObject JsonCs
                {
                    get
                    {
                        // JSON object for international version must be always in JSON file
                        // But JSON object for national version is voluntary
                        // If JSON object for national version is missing then
                        // JSON object for international version is returned
                        return
                            TargetVariableMetadataJson.GetJObject(
                                obj: Root,
                                key: ElementKeyCs) ?? JsonEn;
                    }
                }


                /// <summary>
                /// JSON object
                /// for indicator
                /// in international version (read-only)
                /// </summary>
                private JObject JsonIndicatorEn
                {
                    get
                    {
                        return
                            TargetVariableMetadataJson.GetJObject(
                                obj: JsonEn,
                                key: TargetVariableMetadataJson.ElementKeyIndicator);
                    }
                }

                /// <summary>
                /// JSON object
                /// for indicator
                /// in national version (read-only)
                /// </summary>
                private JObject JsonIndicatorCs
                {
                    get
                    {
                        return
                            TargetVariableMetadataJson.GetJObject(
                                obj: JsonCs,
                                key: TargetVariableMetadataJson.ElementKeyIndicator);
                    }
                }


                /// <summary>
                /// JSON object
                /// for state or change
                /// in international version (read-only)
                /// </summary>
                private JObject JsonStateOrChangeEn
                {
                    get
                    {
                        return
                            TargetVariableMetadataJson.GetJObject(
                                obj: JsonEn,
                                key: TargetVariableMetadataJson.ElementKeyStateOrChange);
                    }
                }

                /// <summary>
                /// JSON object
                /// for state or change
                /// in national version (read-only)
                /// </summary>
                private JObject JsonStateOrChangeCs
                {
                    get
                    {
                        return
                            TargetVariableMetadataJson.GetJObject(
                                obj: JsonCs,
                                key: TargetVariableMetadataJson.ElementKeyStateOrChange);
                    }
                }


                /// <summary>
                /// JSON object
                /// for unit
                /// in international version (read-only)
                /// </summary>
                private JObject JsonUnitEn
                {
                    get
                    {
                        return
                            TargetVariableMetadataJson.GetJObject(
                                obj: JsonEn,
                                key: TargetVariableMetadataJson.ElementKeyUnit);
                    }
                }

                /// <summary>
                /// JSON object
                /// for unit
                /// in national version (read-only)
                /// </summary>
                private JObject JsonUnitCs
                {
                    get
                    {
                        return
                            TargetVariableMetadataJson.GetJObject(
                                obj: JsonCs,
                                key: TargetVariableMetadataJson.ElementKeyUnit);
                    }
                }


                /// <summary>
                /// List of JSON objects
                /// for local densities
                /// in international version (read-only)
                /// </summary>
                private List<JObject> JsonLocalDensitiesEn
                {
                    get
                    {
                        return
                            TargetVariableMetadataJson.GetJObjectList(
                                obj: JsonEn,
                                key: TargetVariableMetadataJson.ElementKeyLocalDensities);
                    }
                }

                /// <summary>
                /// List of JSON objects
                /// for local densities
                /// in national version (read-only)
                /// </summary>
                private List<JObject> JsonLocalDensitiesCs
                {
                    get
                    {
                        return
                            TargetVariableMetadataJson.GetJObjectList(
                                obj: JsonCs,
                                key: TargetVariableMetadataJson.ElementKeyLocalDensities);
                    }
                }


                /// <summary>
                /// Indicator
                /// </summary>
                public TIndicator Indicator
                {
                    get
                    {
                        if (JsonIndicatorEn == null)
                        {
                            return null;
                        }
                        else if (JsonIndicatorCs == null)
                        {
                            return null;
                        }
                        else
                        {
                            return
                                new TIndicator(
                                    objectEn: JsonIndicatorEn,
                                    objectCs: JsonIndicatorCs);
                        }
                    }
                }

                /// <summary>
                /// State or change
                /// </summary>
                public TStateOrChange StateOrChange
                {
                    get
                    {
                        if (JsonStateOrChangeEn == null)
                        {
                            return null;
                        }
                        else if (JsonStateOrChangeCs == null)
                        {
                            return null;
                        }
                        else
                        {
                            return
                                new TStateOrChange(
                                    objectEn: JsonStateOrChangeEn,
                                    objectCs: JsonStateOrChangeCs);
                        }
                    }
                }

                /// <summary>
                /// Unit
                /// </summary>
                public TUnit Unit
                {
                    get
                    {
                        if (JsonUnitEn == null)
                        {
                            return null;
                        }
                        else if (JsonUnitCs == null)
                        {
                            return null;
                        }
                        else
                        {
                            return
                                new TUnit(
                                    objectEn: JsonUnitEn,
                                    objectCs: JsonUnitCs);
                        }
                    }
                }

                /// <summary>
                /// List of local densities
                /// </summary>
                public List<TLocalDensity> LocalDensities
                {
                    get
                    {
                        List<TLocalDensity> result = [];

                        List<JObject> objListEn = JsonLocalDensitiesEn;
                        List<JObject> objListCs = JsonLocalDensitiesCs;

                        if (objListEn == null)
                        {
                            return [];
                        }
                        else if (objListCs == null)
                        {
                            return [];
                        }
                        else if (objListEn.Count != objListCs.Count)
                        {
                            return [];
                        }
                        else
                        {
                            for (int i = 0; i < objListEn.Count; i++)
                            {
                                if (objListEn[i] == null)
                                {
                                    return [];
                                }
                                else if (objListCs[i] == null)
                                {
                                    return [];
                                }
                                else
                                {
                                    result.Add(
                                        item: new TLocalDensity(
                                            objectEn: objListEn[i],
                                            objectCs: objListCs[i]));
                                }
                            }
                            return result;
                        }
                    }
                }


                /// <summary>
                /// Message about problems found in JSON object
                /// </summary>
                public string ErrorMessage
                {
                    get
                    {
                        return IsValid ? String.Empty : errorMessage;
                    }
                    set
                    {
                        errorMessage = value;
                    }
                }

                /// <summary>
                /// Does metadata element have valid JSON file?
                /// </summary>
                public bool IsValid
                {
                    get
                    {
                        if (Root == null)
                        {
                            ErrorMessage = "Element root is null!";
                            return false;
                        }

                        if (JsonEn == null)
                        {
                            ErrorMessage = $"Element {ElementKeyEn} is null!";
                            return false;
                        }

                        if (JsonCs == null)
                        {
                            ErrorMessage = $"Element {ElementKeyCs} is null!";
                            return false;
                        }

                        if (!TargetVariableMetadataJson.ValidateJObject(
                                    obj: JsonEn,
                                    key: TargetVariableMetadataJson.ElementKeyIndicator,
                                    msg: out string msgElementKeyIndicatorEn))
                        {
                            ErrorMessage = msgElementKeyIndicatorEn;
                            return false;
                        }

                        if (!TargetVariableMetadataJson.ValidateJObject(
                                    obj: JsonCs,
                                    key: TargetVariableMetadataJson.ElementKeyIndicator,
                                    msg: out string msgElementKeyIndicatorCs))
                        {
                            ErrorMessage = msgElementKeyIndicatorCs;
                            return false;
                        }

                        if (!Indicator.IsValid)
                        {
                            ErrorMessage = Indicator.ErrorMessage;
                            return false;
                        }

                        if (!TargetVariableMetadataJson.ValidateJObject(
                                    obj: JsonEn,
                                    key: TargetVariableMetadataJson.ElementKeyStateOrChange,
                                    msg: out string msgElementKeyStateOrChangeEn))
                        {
                            ErrorMessage = msgElementKeyStateOrChangeEn;
                            return false;
                        }

                        if (!TargetVariableMetadataJson.ValidateJObject(
                                    obj: JsonCs,
                                    key: TargetVariableMetadataJson.ElementKeyStateOrChange,
                                    msg: out string msgElementKeyStateOrChangeCs))
                        {
                            ErrorMessage = msgElementKeyStateOrChangeCs;
                            return false;
                        }


                        if (!StateOrChange.IsValid)
                        {
                            ErrorMessage = StateOrChange.ErrorMessage;
                            return false;
                        }

                        if (!TargetVariableMetadataJson.ValidateJObject(
                                    obj: JsonEn,
                                    key: TargetVariableMetadataJson.ElementKeyUnit,
                                    msg: out string msgElementKeyUnitEn))
                        {
                            ErrorMessage = msgElementKeyUnitEn;
                            return false;
                        }

                        if (!TargetVariableMetadataJson.ValidateJObject(
                                    obj: JsonCs,
                                    key: TargetVariableMetadataJson.ElementKeyUnit,
                                    msg: out string msgElementKeyUnitCs))
                        {
                            ErrorMessage = msgElementKeyUnitCs;
                            return false;
                        }

                        if (!Unit.IsValid)
                        {
                            ErrorMessage = StateOrChange.ErrorMessage;
                            return false;
                        }

                        if (!TargetVariableMetadataJson.ValidateJObjectList(
                                    obj: JsonEn,
                                    key: TargetVariableMetadataJson.ElementKeyLocalDensities,
                                    msg: out string msgElementKeyLocalDensitiesEn))
                        {
                            ErrorMessage = msgElementKeyLocalDensitiesEn;
                            return false;
                        }

                        if (!TargetVariableMetadataJson.ValidateJObjectList(
                                    obj: JsonCs,
                                    key: TargetVariableMetadataJson.ElementKeyLocalDensities,
                                     msg: out string msgElementKeyLocalDensitiesCs))
                        {
                            ErrorMessage = msgElementKeyLocalDensitiesCs;
                            return false;
                        }

                        if (JsonLocalDensitiesEn.Count != JsonLocalDensitiesCs.Count)
                        {
                            ErrorMessage = String.Concat(
                                "Number of local density contributions in national section of the json file ",
                                "is not the same as in international section!");
                            return false;
                        }

                        if (!LocalDensities
                            .Select(localDensity => localDensity.IsValid)
                            .Aggregate((a, b) => a && b))
                        {
                            ErrorMessage = LocalDensities
                                .Where(a => !a.IsValid)
                                .Select(localDensity => localDensity.ErrorMessage)
                                .Aggregate((a, b) => $"{a}; {b}");
                            return false;
                        }

                        return true;
                    }
                }


                /// <summary>
                /// Data table with data that was loaded from JSON file
                /// </summary>
                public DataTable Data
                {
                    get
                    {
                        List<TLocalDensity> ld = LocalDensities;
                        List<TLocalDensity> ldsities =
                            (ld == null) ? [] :
                            ld.Where(a => a != null).ToList();

                        DataTable result = EmptyDataTable();
                        int i = 0;

                        if (ldsities.Count != 0)
                        {
                            foreach (TLocalDensity ldsity in ldsities)
                            {
                                DataRow row = result.NewRow();

                                Functions.SetIntArg(row: row, name: TargetVariableMetadataJson.ColId, val: 0);
                                Functions.SetIntArg(row: row, name: TargetVariableMetadataJson.ColTargetVariableId, val: 0);
                                Functions.SetIntArg(row: row, name: TargetVariableMetadataJson.ColLocalDensityId, val: ++i);
                                Functions.SetBoolArg(row: row, name: TargetVariableMetadataJson.ColJsonValidity, val: IsValid);

                                TIndicator indicator = Indicator;
                                if (indicator != null)
                                {
                                    Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColIndicatorLabelCs, val: indicator.LabelCs);
                                    Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColIndicatorDescriptionCs, val: indicator.DescriptionCs);
                                    Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColIndicatorLabelEn, val: indicator.LabelEn);
                                    Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColIndicatorDescriptionEn, val: indicator.DescriptionEn);
                                }
                                else
                                {
                                    Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColIndicatorLabelCs, val: String.Empty);
                                    Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColIndicatorDescriptionCs, val: String.Empty);
                                    Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColIndicatorLabelEn, val: String.Empty);
                                    Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColIndicatorDescriptionEn, val: String.Empty);
                                }

                                TStateOrChange soc = StateOrChange;
                                if (soc != null)
                                {
                                    Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColStateOrChangeLabelCs, val: soc.LabelCs);
                                    Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColStateOrChangeDescriptionCs, val: soc.DescriptionCs);
                                    Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColStateOrChangeLabelEn, val: soc.LabelEn);
                                    Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColStateOrChangeDescriptionEn, val: soc.DescriptionEn);
                                }
                                else
                                {
                                    Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColStateOrChangeLabelCs, val: String.Empty);
                                    Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColStateOrChangeDescriptionCs, val: String.Empty);
                                    Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColStateOrChangeLabelEn, val: String.Empty);
                                    Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColStateOrChangeDescriptionEn, val: String.Empty);
                                }

                                TUnit unit = Unit;
                                if (unit != null)
                                {
                                    Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColUnitLabelCs, val: unit.LabelCs);
                                    Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColUnitDescriptionCs, val: unit.DescriptionCs);
                                    Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColUnitLabelEn, val: unit.LabelEn);
                                    Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColUnitDescriptionEn, val: unit.DescriptionEn);
                                }
                                else
                                {
                                    Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColUnitLabelCs, val: String.Empty);
                                    Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColUnitDescriptionCs, val: String.Empty);
                                    Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColUnitLabelEn, val: String.Empty);
                                    Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColUnitDescriptionEn, val: String.Empty);
                                }

                                Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColLocalDensityObjectTypeLabelCs, val: ldsity.ObjectTypeLabelCs);
                                Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColLocalDensityObjectTypeDescriptionCs, val: ldsity.ObjectTypeDescriptionCs);
                                Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColLocalDensityObjectTypeLabelEn, val: ldsity.ObjectTypeLabelEn);
                                Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColLocalDensityObjectTypeDescriptionEn, val: ldsity.ObjectTypeDescriptionEn);

                                Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColLocalDensityObjectLabelCs, val: ldsity.ObjectLabelCs);
                                Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColLocalDensityObjectDescriptionCs, val: ldsity.ObjectDescriptionCs);
                                Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColLocalDensityObjectLabelEn, val: ldsity.ObjectLabelEn);
                                Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColLocalDensityObjectDescriptionEn, val: ldsity.ObjectDescriptionEn);

                                Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColLocalDensityLabelCs, val: ldsity.LabelCs);
                                Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColLocalDensityDescriptionCs, val: ldsity.DescriptionCs);
                                Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColLocalDensityLabelEn, val: ldsity.LabelEn);
                                Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColLocalDensityDescriptionEn, val: ldsity.DescriptionEn);

                                Functions.SetNBoolArg(row: row, name: TargetVariableMetadataJson.ColUseNegativeCs, val: ldsity.UseNegativeCs);
                                Functions.SetNBoolArg(row: row, name: TargetVariableMetadataJson.ColUseNegativeEn, val: ldsity.UseNegativeEn);

                                TVersion version = ldsity.Version;
                                if (version != null)
                                {
                                    Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColVersionLabelCs, val: version.LabelCs);
                                    Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColVersionDescriptionCs, val: version.DescriptionCs);
                                    Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColVersionLabelEn, val: version.LabelEn);
                                    Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColVersionDescriptionEn, val: version.DescriptionEn);
                                }
                                else
                                {
                                    Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColVersionLabelCs, val: String.Empty);
                                    Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColVersionDescriptionCs, val: String.Empty);
                                    Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColVersionLabelEn, val: String.Empty);
                                    Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColVersionDescriptionEn, val: String.Empty);
                                }

                                TDefinitionVariant definitionVariant = ldsity.DefinitionVariant;
                                if (definitionVariant != null)
                                {
                                    Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColDefinitionVariantLabelCs, val: definitionVariant.LabelCs);
                                    Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColDefinitionVariantDescriptionCs, val: definitionVariant.DescriptionCs);
                                    Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColDefinitionVariantLabelEn, val: definitionVariant.LabelEn);
                                    Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColDefinitionVariantDescriptionEn, val: definitionVariant.DescriptionEn);
                                }
                                else
                                {
                                    Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColDefinitionVariantLabelCs, val: String.Empty);
                                    Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColDefinitionVariantDescriptionCs, val: String.Empty);
                                    Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColDefinitionVariantLabelEn, val: String.Empty);
                                    Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColDefinitionVariantDescriptionEn, val: String.Empty);
                                }

                                TAreaDomain areaDomain = ldsity.AreaDomain;
                                if (areaDomain != null)
                                {
                                    Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColAreaDomainLabelCs, val: areaDomain.LabelCs);
                                    Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColAreaDomainDescriptionCs, val: areaDomain.DescriptionCs);
                                    Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColAreaDomainLabelEn, val: areaDomain.LabelEn);
                                    Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColAreaDomainDescriptionEn, val: areaDomain.DescriptionEn);
                                }
                                else
                                {
                                    Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColAreaDomainLabelCs, val: String.Empty);
                                    Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColAreaDomainDescriptionCs, val: String.Empty);
                                    Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColAreaDomainLabelEn, val: String.Empty);
                                    Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColAreaDomainDescriptionEn, val: String.Empty);
                                }

                                TPopulation population = ldsity.Population;
                                if (population != null)
                                {
                                    Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColPopulationLabelCs, val: population.LabelCs);
                                    Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColPopulationDescriptionCs, val: population.DescriptionCs);
                                    Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColPopulationLabelEn, val: population.LabelEn);
                                    Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColPopulationDescriptionEn, val: population.DescriptionEn);
                                }
                                else
                                {
                                    Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColPopulationLabelCs, val: String.Empty);
                                    Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColPopulationDescriptionCs, val: String.Empty);
                                    Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColPopulationLabelEn, val: String.Empty);
                                    Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColPopulationDescriptionEn, val: String.Empty);
                                }

                                result.Rows.Add(row: row);
                            }
                        }
                        else
                        {
                            DataRow row = result.NewRow();

                            Functions.SetIntArg(row: row, name: TargetVariableMetadataJson.ColId, val: 0);
                            Functions.SetIntArg(row: row, name: TargetVariableMetadataJson.ColTargetVariableId, val: 0);
                            Functions.SetIntArg(row: row, name: TargetVariableMetadataJson.ColLocalDensityId, val: ++i);
                            Functions.SetBoolArg(row: row, name: TargetVariableMetadataJson.ColJsonValidity, val: IsValid);

                            TIndicator indicator = Indicator;
                            if (indicator != null)
                            {
                                Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColIndicatorLabelCs, val: indicator.LabelCs);
                                Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColIndicatorDescriptionCs, val: indicator.DescriptionCs);
                                Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColIndicatorLabelEn, val: indicator.LabelEn);
                                Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColIndicatorDescriptionEn, val: indicator.DescriptionEn);
                            }
                            else
                            {
                                Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColIndicatorLabelCs, val: String.Empty);
                                Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColIndicatorDescriptionCs, val: String.Empty);
                                Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColIndicatorLabelEn, val: String.Empty);
                                Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColIndicatorDescriptionEn, val: String.Empty);
                            }

                            TStateOrChange soc = StateOrChange;
                            if (soc != null)
                            {
                                Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColStateOrChangeLabelCs, val: soc.LabelCs);
                                Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColStateOrChangeDescriptionCs, val: soc.DescriptionCs);
                                Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColStateOrChangeLabelEn, val: soc.LabelEn);
                                Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColStateOrChangeDescriptionEn, val: soc.DescriptionEn);
                            }
                            else
                            {
                                Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColStateOrChangeLabelCs, val: String.Empty);
                                Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColStateOrChangeDescriptionCs, val: String.Empty);
                                Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColStateOrChangeLabelEn, val: String.Empty);
                                Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColStateOrChangeDescriptionEn, val: String.Empty);
                            }

                            TUnit unit = Unit;
                            if (unit != null)
                            {
                                Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColUnitLabelCs, val: unit.LabelCs);
                                Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColUnitDescriptionCs, val: unit.DescriptionCs);
                                Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColUnitLabelEn, val: unit.LabelEn);
                                Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColUnitDescriptionEn, val: unit.DescriptionEn);
                            }
                            else
                            {
                                Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColUnitLabelCs, val: String.Empty);
                                Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColUnitDescriptionCs, val: String.Empty);
                                Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColUnitLabelEn, val: String.Empty);
                                Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColUnitDescriptionEn, val: String.Empty);
                            }

                            Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColLocalDensityObjectTypeLabelCs, val: String.Empty);
                            Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColLocalDensityObjectTypeDescriptionCs, val: String.Empty);
                            Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColLocalDensityObjectTypeLabelEn, val: String.Empty);
                            Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColLocalDensityObjectTypeDescriptionEn, val: String.Empty);

                            Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColLocalDensityObjectLabelCs, val: String.Empty);
                            Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColLocalDensityObjectDescriptionCs, val: String.Empty);
                            Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColLocalDensityObjectLabelEn, val: String.Empty);
                            Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColLocalDensityObjectDescriptionEn, val: String.Empty);

                            Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColLocalDensityLabelCs, val: String.Empty);
                            Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColLocalDensityDescriptionCs, val: String.Empty);
                            Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColLocalDensityLabelEn, val: String.Empty);
                            Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColLocalDensityDescriptionEn, val: String.Empty);

                            Functions.SetNBoolArg(row: row, name: TargetVariableMetadataJson.ColUseNegativeCs, val: null);
                            Functions.SetNBoolArg(row: row, name: TargetVariableMetadataJson.ColUseNegativeEn, val: null);

                            Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColVersionLabelCs, val: String.Empty);
                            Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColVersionDescriptionCs, val: String.Empty);
                            Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColVersionLabelEn, val: String.Empty);
                            Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColVersionDescriptionEn, val: String.Empty);

                            Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColDefinitionVariantLabelCs, val: String.Empty);
                            Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColDefinitionVariantDescriptionCs, val: String.Empty);
                            Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColDefinitionVariantLabelEn, val: String.Empty);
                            Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColDefinitionVariantDescriptionEn, val: String.Empty);

                            Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColAreaDomainLabelCs, val: String.Empty);
                            Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColAreaDomainDescriptionCs, val: String.Empty);
                            Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColAreaDomainLabelEn, val: String.Empty);
                            Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColAreaDomainDescriptionEn, val: String.Empty);

                            Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColPopulationLabelCs, val: String.Empty);
                            Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColPopulationDescriptionCs, val: String.Empty);
                            Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColPopulationLabelEn, val: String.Empty);
                            Functions.SetStringArg(row: row, name: TargetVariableMetadataJson.ColPopulationDescriptionEn, val: String.Empty);

                            result.Rows.Add(row: row);
                        }

                        return result;
                    }
                }

                #endregion Properties


                #region Methods

                /// <summary>
                /// Empty data table for data that will be loaded from JSON file
                /// </summary>
                /// <returns>Empty data table for data that will be loaded from JSON file</returns>
                public static DataTable EmptyDataTable()
                {
                    DataTable result = new() { TableName = "TargetVariableJson" };

                    result.Columns.Add(column: new DataColumn()
                    {
                        ColumnName = TargetVariableMetadataJson.ColId,
                        DataType = Type.GetType(typeName: "System.Int32")
                    });
                    result.Columns.Add(column: new DataColumn()
                    {
                        ColumnName = TargetVariableMetadataJson.ColTargetVariableId,
                        DataType = Type.GetType(typeName: "System.Int32")
                    });
                    result.Columns.Add(column: new DataColumn()
                    {
                        ColumnName = TargetVariableMetadataJson.ColLocalDensityId,
                        DataType = Type.GetType(typeName: "System.Int32")
                    });
                    result.Columns.Add(column: new DataColumn()
                    {
                        ColumnName = TargetVariableMetadataJson.ColJsonValidity,
                        DataType = Type.GetType(typeName: "System.Boolean")
                    });

                    result.Columns.Add(column: new DataColumn()
                    {
                        ColumnName = TargetVariableMetadataJson.ColIndicatorLabelCs,
                        DataType = Type.GetType(typeName: "System.String")
                    });
                    result.Columns.Add(column: new DataColumn()
                    {
                        ColumnName = TargetVariableMetadataJson.ColIndicatorDescriptionCs,
                        DataType = Type.GetType(typeName: "System.String")
                    });
                    result.Columns.Add(column: new DataColumn()
                    {
                        ColumnName = TargetVariableMetadataJson.ColIndicatorLabelEn,
                        DataType = Type.GetType(typeName: "System.String")
                    });
                    result.Columns.Add(column: new DataColumn()
                    {
                        ColumnName = TargetVariableMetadataJson.ColIndicatorDescriptionEn,
                        DataType = Type.GetType(typeName: "System.String")
                    });

                    result.Columns.Add(column: new DataColumn()
                    {
                        ColumnName = TargetVariableMetadataJson.ColStateOrChangeLabelCs,
                        DataType = Type.GetType(typeName: "System.String")
                    });
                    result.Columns.Add(column: new DataColumn()
                    {
                        ColumnName = TargetVariableMetadataJson.ColStateOrChangeDescriptionCs,
                        DataType = Type.GetType(typeName: "System.String")
                    });
                    result.Columns.Add(column: new DataColumn()
                    {
                        ColumnName = TargetVariableMetadataJson.ColStateOrChangeLabelEn,
                        DataType = Type.GetType(typeName: "System.String")
                    });
                    result.Columns.Add(column: new DataColumn()
                    {
                        ColumnName = TargetVariableMetadataJson.ColStateOrChangeDescriptionEn,
                        DataType = Type.GetType(typeName: "System.String")
                    });

                    result.Columns.Add(column: new DataColumn()
                    {
                        ColumnName = TargetVariableMetadataJson.ColUnitLabelCs,
                        DataType = Type.GetType(typeName: "System.String")
                    });
                    result.Columns.Add(column: new DataColumn()
                    {
                        ColumnName = TargetVariableMetadataJson.ColUnitDescriptionCs,
                        DataType = Type.GetType(typeName: "System.String")
                    });
                    result.Columns.Add(column: new DataColumn()
                    {
                        ColumnName = TargetVariableMetadataJson.ColUnitLabelEn,
                        DataType = Type.GetType(typeName: "System.String")
                    });
                    result.Columns.Add(column: new DataColumn()
                    {
                        ColumnName = TargetVariableMetadataJson.ColUnitDescriptionEn,
                        DataType = Type.GetType(typeName: "System.String")
                    });

                    result.Columns.Add(column: new DataColumn()
                    {
                        ColumnName = TargetVariableMetadataJson.ColLocalDensityObjectTypeLabelCs,
                        DataType = Type.GetType(typeName: "System.String")
                    });
                    result.Columns.Add(column: new DataColumn()
                    {
                        ColumnName = TargetVariableMetadataJson.ColLocalDensityObjectTypeDescriptionCs,
                        DataType = Type.GetType(typeName: "System.String")
                    });
                    result.Columns.Add(column: new DataColumn()
                    {
                        ColumnName = TargetVariableMetadataJson.ColLocalDensityObjectTypeLabelEn,
                        DataType = Type.GetType(typeName: "System.String")
                    });
                    result.Columns.Add(column: new DataColumn()
                    {
                        ColumnName = TargetVariableMetadataJson.ColLocalDensityObjectTypeDescriptionEn,
                        DataType = Type.GetType(typeName: "System.String")
                    });

                    result.Columns.Add(column: new DataColumn()
                    {
                        ColumnName = TargetVariableMetadataJson.ColLocalDensityObjectLabelCs,
                        DataType = Type.GetType(typeName: "System.String")
                    });
                    result.Columns.Add(column: new DataColumn()
                    {
                        ColumnName = TargetVariableMetadataJson.ColLocalDensityObjectDescriptionCs,
                        DataType = Type.GetType(typeName: "System.String")
                    });
                    result.Columns.Add(column: new DataColumn()
                    {
                        ColumnName = TargetVariableMetadataJson.ColLocalDensityObjectLabelEn,
                        DataType = Type.GetType(typeName: "System.String")
                    });
                    result.Columns.Add(column: new DataColumn()
                    {
                        ColumnName = TargetVariableMetadataJson.ColLocalDensityObjectDescriptionEn,
                        DataType = Type.GetType(typeName: "System.String")
                    });

                    result.Columns.Add(column: new DataColumn()
                    {
                        ColumnName = TargetVariableMetadataJson.ColLocalDensityLabelCs,
                        DataType = Type.GetType(typeName: "System.String")
                    });
                    result.Columns.Add(column: new DataColumn()
                    {
                        ColumnName = TargetVariableMetadataJson.ColLocalDensityDescriptionCs,
                        DataType = Type.GetType(typeName: "System.String")
                    });
                    result.Columns.Add(column: new DataColumn()
                    {
                        ColumnName = TargetVariableMetadataJson.ColLocalDensityLabelEn,
                        DataType = Type.GetType(typeName: "System.String")
                    });
                    result.Columns.Add(column: new DataColumn()
                    {
                        ColumnName = TargetVariableMetadataJson.ColLocalDensityDescriptionEn,
                        DataType = Type.GetType(typeName: "System.String")
                    });

                    result.Columns.Add(column: new DataColumn()
                    {
                        ColumnName = TargetVariableMetadataJson.ColUseNegativeCs,
                        DataType = Type.GetType(typeName: "System.Boolean")
                    });
                    result.Columns.Add(column: new DataColumn()
                    {
                        ColumnName = TargetVariableMetadataJson.ColUseNegativeEn,
                        DataType = Type.GetType(typeName: "System.Boolean")
                    });

                    result.Columns.Add(column: new DataColumn()
                    {
                        ColumnName = TargetVariableMetadataJson.ColVersionLabelCs,
                        DataType = Type.GetType(typeName: "System.String")
                    });
                    result.Columns.Add(column: new DataColumn()
                    {
                        ColumnName = TargetVariableMetadataJson.ColVersionDescriptionCs,
                        DataType = Type.GetType(typeName: "System.String")
                    });
                    result.Columns.Add(column: new DataColumn()
                    {
                        ColumnName = TargetVariableMetadataJson.ColVersionLabelEn,
                        DataType = Type.GetType(typeName: "System.String")
                    });
                    result.Columns.Add(column: new DataColumn()
                    {
                        ColumnName = TargetVariableMetadataJson.ColVersionDescriptionEn,
                        DataType = Type.GetType(typeName: "System.String")
                    });

                    result.Columns.Add(column: new DataColumn()
                    {
                        ColumnName = TargetVariableMetadataJson.ColDefinitionVariantLabelCs,
                        DataType = Type.GetType(typeName: "System.String")
                    });
                    result.Columns.Add(column: new DataColumn()
                    {
                        ColumnName = TargetVariableMetadataJson.ColDefinitionVariantDescriptionCs,
                        DataType = Type.GetType(typeName: "System.String")
                    });
                    result.Columns.Add(column: new DataColumn()
                    {
                        ColumnName = TargetVariableMetadataJson.ColDefinitionVariantLabelEn,
                        DataType = Type.GetType(typeName: "System.String")
                    });
                    result.Columns.Add(column: new DataColumn()
                    {
                        ColumnName = TargetVariableMetadataJson.ColDefinitionVariantDescriptionEn,
                        DataType = Type.GetType(typeName: "System.String")
                    });

                    result.Columns.Add(column: new DataColumn()
                    {
                        ColumnName = TargetVariableMetadataJson.ColAreaDomainLabelCs,
                        DataType = Type.GetType(typeName: "System.String")
                    });
                    result.Columns.Add(column: new DataColumn()
                    {
                        ColumnName = TargetVariableMetadataJson.ColAreaDomainDescriptionCs,
                        DataType = Type.GetType(typeName: "System.String")
                    });
                    result.Columns.Add(column: new DataColumn()
                    {
                        ColumnName = TargetVariableMetadataJson.ColAreaDomainLabelEn,
                        DataType = Type.GetType(typeName: "System.String")
                    });
                    result.Columns.Add(column: new DataColumn()
                    {
                        ColumnName = TargetVariableMetadataJson.ColAreaDomainDescriptionEn,
                        DataType = Type.GetType(typeName: "System.String")
                    });

                    result.Columns.Add(column: new DataColumn()
                    {
                        ColumnName = TargetVariableMetadataJson.ColPopulationLabelCs,
                        DataType = Type.GetType(typeName: "System.String")
                    });
                    result.Columns.Add(column: new DataColumn()
                    {
                        ColumnName = TargetVariableMetadataJson.ColPopulationDescriptionCs,
                        DataType = Type.GetType(typeName: "System.String")
                    });
                    result.Columns.Add(column: new DataColumn()
                    {
                        ColumnName = TargetVariableMetadataJson.ColPopulationLabelEn,
                        DataType = Type.GetType(typeName: "System.String")
                    });
                    result.Columns.Add(column: new DataColumn()
                    {
                        ColumnName = TargetVariableMetadataJson.ColPopulationDescriptionEn,
                        DataType = Type.GetType(typeName: "System.String")
                    });

                    return result;
                }


                /// <summary>
                /// Function extracts inner array of strings
                /// by the key
                /// from given JSON object
                /// </summary>
                /// <param name="obj">Given JSON object</param>
                /// <param name="key">Key</param>
                /// <returns>List of inner strings</returns>
                public static List<string> GetStringList(JObject obj, string key)
                {
                    if (obj == null)
                    {
                        return [];
                    }
                    else if (String.IsNullOrEmpty(value: key))
                    {
                        return [];
                    }
                    else if (!obj.ContainsKey(propertyName: key))
                    {
                        return [];
                    }
                    else if (obj[key] == null)
                    {
                        return [];
                    }
                    else
                    {
                        switch (obj[key].Type)
                        {
                            case JTokenType.Null:
                                return [ElementIsNull];

                            case JTokenType.String:
                                return [obj[key].Value<string>()];

                            case JTokenType.Array:
                                List<string> result = [];
                                JArray array = obj[key].ToObject<JArray>();

                                foreach (JToken item in array)
                                {
                                    switch (item.Type)
                                    {
                                        case JTokenType.Null:
                                            result.Add(item: ElementIsNull);
                                            break;

                                        case JTokenType.String:
                                            result.Add(item: item.Value<string>());
                                            break;

                                        default:
                                            break;
                                    }
                                }
                                return result;

                            default:
                                return [];
                        }
                    }
                }

                /// <summary>
                /// Function tests whether given JSON object
                /// that contains array of string
                /// has valid format
                /// </summary>
                /// <param name="obj">Given JSON object</param>
                /// <param name="key">Key</param>
                /// <param name="msg">Message about problems found in JSON object</param>
                /// <returns>true/false</returns>
                public static bool ValidateStringList(JObject obj, string key, out string msg)
                {
                    if (obj == null)
                    {
                        msg = "JObject is null.";
                        return false;
                    }
                    else if (String.IsNullOrEmpty(value: key))
                    {
                        msg = "Key is null or empty.";
                        return false;
                    }
                    else if (!obj.ContainsKey(propertyName: key))
                    {
                        msg = $"JObject does not contain key: {key}.";
                        return false;
                    }
                    else if (obj[key] == null)
                    {
                        msg = $"Value for key: {key} is null.";
                        return false;
                    }
                    else
                    {
                        switch (obj[key].Type)
                        {
                            case JTokenType.Null:
                                msg = String.Empty;
                                return true;

                            case JTokenType.String:
                                msg = String.Empty;
                                return true;

                            case JTokenType.Array:
                                JArray array = obj[key].ToObject<JArray>();
                                foreach (JToken item in array)
                                {
                                    switch (item.Type)
                                    {
                                        case JTokenType.Null:
                                            break;

                                        case JTokenType.String:
                                            break;

                                        default:
                                            msg = $"Array for key: {key} does not contain strings.";
                                            return false;
                                    }
                                }
                                msg = String.Empty;
                                return true;

                            default:
                                msg = $"JTokenType for key: {key} is not string or string array.";
                                return false;
                        }
                    }
                }


                /// <summary>
                /// Function extracts boolean value
                /// by the key
                /// from given JSON object
                /// </summary>
                /// <param name="obj">Given JSON object</param>
                /// <param name="key">Key</param>
                /// <returns>Boolean value</returns>
                public static Nullable<bool> GetNBoolean(JObject obj, string key)
                {
                    if (obj == null)
                    {
                        return null;
                    }
                    else if (String.IsNullOrEmpty(value: key))
                    {
                        return null;
                    }
                    else if (!obj.ContainsKey(propertyName: key))
                    {
                        return null;
                    }
                    else if (obj[key] == null)
                    {
                        return null;
                    }
                    else
                    {
                        return obj[key].Type switch
                        {
                            JTokenType.Null => null,
                            JTokenType.Boolean => obj[key].Value<bool>(),
                            _ => null,
                        };
                    }
                }

                /// <summary>
                /// Function tests whether given JSON object
                /// that contains boolean value
                /// has valid format
                /// </summary>
                /// <param name="obj">Given JSON object</param>
                /// <param name="key">Key</param>
                /// <param name="msg">Message about problems found in JSON object</param>
                /// <returns>true/false</returns>
                public static bool ValidateNBoolean(JObject obj, string key, out string msg)
                {
                    if (obj == null)
                    {
                        msg = "JObject is null.";
                        return false;
                    }
                    else if (String.IsNullOrEmpty(value: key))
                    {
                        msg = "Key is null or empty.";
                        return false;
                    }
                    else if (!obj.ContainsKey(propertyName: key))
                    {
                        msg = $"JObject does not contain key: {key}.";
                        return false;
                    }
                    else if (obj[key] == null)
                    {
                        msg = $"Value for key: {key} is null.";
                        return false;
                    }
                    else
                    {
                        switch (obj[key].Type)
                        {
                            case JTokenType.Null:
                                msg = String.Empty;
                                return true;

                            case JTokenType.Boolean:
                                msg = String.Empty;
                                return true;

                            default:
                                msg = $"JTokenType for key: {key} is not nullable boolean.";
                                return false;
                        }
                    }
                }


                /// <summary>
                /// Function extracts inner JSON object
                /// by the key
                /// from given JSON object
                /// </summary>
                /// <param name="obj">Given JSON object</param>
                /// <param name="key">Key</param>
                /// <returns>Inner JSON object</returns>
                public static JObject GetJObject(JObject obj, string key)
                {
                    if (obj == null)
                    {
                        return null;
                    }
                    else if (String.IsNullOrEmpty(value: key))
                    {
                        return null;
                    }
                    else if (!obj.ContainsKey(propertyName: key))
                    {
                        return null;
                    }
                    else if (obj[key] == null)
                    {
                        return null;
                    }
                    else
                    {
                        return obj[key].Type switch
                        {
                            JTokenType.Null => null,
                            JTokenType.Object => obj[key].ToObject<JObject>(),
                            _ => null,
                        };
                    }
                }

                /// <summary>
                /// Function tests whether given JSON object
                /// that contains inner JSON object
                /// has valid format
                /// </summary>
                /// <param name="obj">Given JSON object</param>
                /// <param name="key">Key</param>
                /// <param name="msg">Message about problems found in JSON object</param>
                /// <returns>true/false</returns>
                public static bool ValidateJObject(JObject obj, string key, out string msg)
                {
                    if (obj == null)
                    {
                        msg = "JObject is null.";
                        return false;
                    }
                    else if (String.IsNullOrEmpty(value: key))
                    {
                        msg = "Key is null or empty.";
                        return false;
                    }
                    else if (!obj.ContainsKey(propertyName: key))
                    {
                        msg = $"JObject does not contain key: {key}.";
                        return false;
                    }
                    else if (obj[key] == null)
                    {
                        msg = $"Value for key: {key} is null.";
                        return false;
                    }
                    else
                    {
                        switch (obj[key].Type)
                        {
                            case JTokenType.Null:
                                msg = $"JTokenType for key: {key} is null.";
                                return false;

                            case JTokenType.Object:
                                msg = String.Empty;
                                return true;

                            default:
                                msg = $"JTokenType for key: {key} is not object.";
                                return false;
                        }
                    }
                }


                /// <summary>
                /// Function extracts inner array of JSON objects
                /// by the key
                /// from given JSON object
                /// </summary>
                /// <param name="obj">Given JSON object</param>
                /// <param name="key">Key</param>
                /// <returns>List of inner JSON objects</returns>
                public static List<JObject> GetJObjectList(JObject obj, string key)
                {
                    if (obj == null)
                    {
                        return [];
                    }
                    else if (String.IsNullOrEmpty(value: key))
                    {
                        return [];
                    }
                    else if (!obj.ContainsKey(propertyName: key))
                    {
                        return [];
                    }
                    else if (obj[key] == null)
                    {
                        return [];
                    }
                    else
                    {
                        switch (obj[key].Type)
                        {
                            case JTokenType.Null:
                                return [];

                            case JTokenType.Object:
                                return [obj[key].ToObject<JObject>()];

                            case JTokenType.Array:
                                List<JObject> result = [];
                                JArray array = obj[key].ToObject<JArray>();

                                foreach (JToken item in array)
                                {
                                    switch (item.Type)
                                    {
                                        case JTokenType.Null:
                                            result.Add(item: null);
                                            break;

                                        case JTokenType.Object:
                                            result.Add(item: item.ToObject<JObject>());
                                            break;

                                        default:
                                            break;
                                    }
                                }
                                return result;

                            default:
                                return [];
                        }
                    }
                }

                /// <summary>
                /// Function tests whether given JSON object
                /// that contains array of inner JSON objects
                /// has valid format
                /// </summary>
                /// <param name="obj">Given JSON object</param>
                /// <param name="key">Key</param>
                /// <param name="msg">Message about problems found in JSON object</param>
                /// <returns>true/false</returns>
                public static bool ValidateJObjectList(JObject obj, string key, out string msg)
                {
                    msg = String.Empty;
                    if (obj == null)
                    {
                        msg = "JObject is null.";
                        return false;
                    }
                    else if (String.IsNullOrEmpty(value: key))
                    {
                        msg = "Key is null or empty.";
                        return false;
                    }
                    else if (!obj.ContainsKey(propertyName: key))
                    {
                        msg = $"JObject does not contain key: {key}.";
                        return false;
                    }
                    else if (obj[key] == null)
                    {
                        msg = $"Value for key: {key} is null.";
                        return false;
                    }
                    else
                    {
                        switch (obj[key].Type)
                        {
                            case JTokenType.Null:
                                msg = $"JTokenType for key: {key} is null.";
                                return false;

                            case JTokenType.Object:
                                msg = String.Empty;
                                return true;

                            case JTokenType.Array:
                                JArray array = obj[key].ToObject<JArray>();

                                foreach (JToken item in array)
                                {
                                    switch (item.Type)
                                    {
                                        case JTokenType.Null:
                                            msg = $"Array for key: {key} contains nulls.";
                                            return false;

                                        case JTokenType.Object:
                                            break;

                                        default:
                                            msg = $"Array for key: {key} does not contain json objects.";
                                            return false;
                                    }
                                }
                                return true;

                            default:
                                msg = $"JTokenType for key: {key} is not object.";
                                return false;
                        }
                    }
                }


                /// <summary>
                /// Function loads object data from text variable that contains JSON file content
                /// </summary>
                /// <param name="text">Text variable that contains JSON file content</param>
                public void LoadFromText(string text)
                {
                    try
                    {
                        Root = JObject.Parse(json: text);
                    }
                    catch
                    {
                        Root = null;
                    }
                }

                /// <summary>
                /// Function loads object data from JSON file
                /// </summary>
                /// <param name="path">Path to JSON file</param>
                public void LoadFromFile(string path)
                {
                    try
                    {
                        string jsonText =
                            File.ReadAllText(path: path);
                        LoadFromText(text: jsonText);
                    }
                    catch
                    {
                        Root = null;
                    }
                }

                #endregion Methods


                #region Internal Classes

                /// <summary>
                /// Metadata element from JSON file
                /// </summary>
                public abstract class MetadataElement
                {

                    #region Private Fields

                    /// <summary>
                    /// JSON object for international version
                    /// </summary>
                    protected readonly JObject objectCs;

                    /// <summary>
                    /// JSON object for national version
                    /// </summary>
                    protected readonly JObject objectEn;

                    /// <summary>
                    /// Message about problems found in JSON object
                    /// </summary>
                    private string errorMessage;

                    #endregion Private Fields


                    #region Constructor

                    /// <summary>
                    /// Metadata element constructor
                    /// </summary>
                    /// <param name="objectEn">JSON object for international version</param>
                    /// <param name="objectCs">JSON object for national version</param>
                    public MetadataElement(
                        JObject objectEn,
                        JObject objectCs)
                    {
                        if (objectEn != null)
                        {
                            this.objectEn = objectEn;
                        }
                        else
                        {
                            throw new ArgumentException(
                                message: $"Argument {nameof(objectEn)} must not be null.",
                                paramName: nameof(objectEn));
                        }

                        if (objectCs != null)
                        {
                            this.objectCs = objectCs;
                        }
                        else
                        {
                            throw new ArgumentException(
                                message: $"Argument {nameof(objectCs)} must not be null.",
                                paramName: nameof(objectCs));
                        }
                    }

                    #endregion Constructor


                    #region Properties

                    /// <summary>
                    /// Metadata element
                    /// labels in international version (read-only)
                    /// </summary>
                    private List<string> LabelListEn
                    {
                        get
                        {
                            return
                                TargetVariableMetadataJson.GetStringList(
                                    obj: objectEn,
                                    key: TargetVariableMetadataJson.ElementKeyLabel);
                        }
                    }

                    /// <summary>
                    /// Metadata element
                    /// descriptions in international version (read-only)
                    /// </summary>
                    private List<string> DescriptionListEn
                    {
                        get
                        {
                            return
                                TargetVariableMetadataJson.GetStringList(
                                    obj: objectEn,
                                    key: TargetVariableMetadataJson.ElementKeyDescription);
                        }
                    }

                    /// <summary>
                    /// Metadata element
                    /// labels in national version (read-only)
                    /// </summary>
                    private List<string> LabelListCs
                    {
                        get
                        {
                            return
                                TargetVariableMetadataJson.GetStringList(
                                    obj: objectCs,
                                    key: TargetVariableMetadataJson.ElementKeyLabel);
                        }
                    }

                    /// <summary>
                    /// Metadata element
                    /// descriptions in national version (read-only)
                    /// </summary>
                    private List<string> DescriptionListCs
                    {
                        get
                        {
                            return
                                TargetVariableMetadataJson.GetStringList(
                                    obj: objectCs,
                                    key: TargetVariableMetadataJson.ElementKeyDescription);
                        }
                    }


                    /// <summary>
                    /// Metadata element
                    /// label in international version (read-only)
                    /// </summary>
                    public string LabelEn
                    {
                        get
                        {
                            return
                                (LabelListEn == null) ? String.Empty :
                                (LabelListEn.Count == 0) ? String.Empty :
                                (LabelListEn.Count == 1) ? LabelListEn[0] :
                                 LabelListEn.Aggregate((a, b) => $"{a}; {b}");
                        }
                    }

                    /// <summary>
                    /// Metadata element
                    /// description in international version(read-only)
                    /// </summary>
                    public string DescriptionEn
                    {
                        get
                        {
                            return
                                (DescriptionListEn == null) ? String.Empty :
                                (DescriptionListEn.Count == 0) ? String.Empty :
                                (DescriptionListEn.Count == 1) ? DescriptionListEn[0] :
                                 DescriptionListEn.Aggregate((a, b) => $"{a}; {b}");
                        }
                    }

                    /// <summary>
                    /// Metadata element
                    /// label in national version (read-only)
                    /// </summary>
                    public string LabelCs
                    {
                        get
                        {
                            return
                                (LabelListCs == null) ? String.Empty :
                                (LabelListCs.Count == 0) ? String.Empty :
                                (LabelListCs.Count == 1) ? LabelListCs[0] :
                                 LabelListCs.Aggregate((a, b) => $"{a}; {b}");
                        }
                    }

                    /// <summary>
                    /// Metadata element
                    /// description in national version (read-only)
                    /// </summary>
                    public string DescriptionCs
                    {
                        get
                        {
                            return
                                (DescriptionListCs == null) ? String.Empty :
                                (DescriptionListCs.Count == 0) ? String.Empty :
                                (DescriptionListCs.Count == 1) ? DescriptionListCs[0] :
                                 DescriptionListCs.Aggregate((a, b) => $"{a}; {b}");
                        }
                    }


                    /// <summary>
                    /// Message about problems found in JSON object
                    /// </summary>
                    public virtual string ErrorMessage
                    {
                        get
                        {
                            return IsValid ? String.Empty : errorMessage;
                        }
                        set
                        {
                            errorMessage = value;
                        }
                    }

                    /// <summary>
                    /// Does this metadata element have valid JSON file?
                    /// </summary>
                    public virtual bool IsValid
                    {
                        get
                        {
                            if (!TargetVariableMetadataJson.ValidateStringList(
                                    obj: objectEn,
                                    key: TargetVariableMetadataJson.ElementKeyLabel,
                                    msg: out string msgElementKeyLabelEn))
                            {
                                ErrorMessage = msgElementKeyLabelEn;
                                return false;
                            }

                            if (!TargetVariableMetadataJson.ValidateStringList(
                                    obj: objectEn,
                                    key: TargetVariableMetadataJson.ElementKeyDescription,
                                    msg: out string msgElementKeyDescriptionEn))
                            {
                                ErrorMessage = msgElementKeyDescriptionEn;
                                return false;
                            }

                            if (!TargetVariableMetadataJson.ValidateStringList(
                                    obj: objectCs,
                                    key: TargetVariableMetadataJson.ElementKeyLabel,
                                    msg: out string msgElementKeyLabelCs))
                            {
                                ErrorMessage = msgElementKeyLabelCs;
                                return false;
                            }

                            if (!TargetVariableMetadataJson.ValidateStringList(
                                    obj: objectCs,
                                    key: TargetVariableMetadataJson.ElementKeyDescription,
                                    msg: out string msgElementKeyDescriptionCs))
                            {
                                ErrorMessage = msgElementKeyDescriptionCs;
                                return false;
                            }

                            if (LabelListEn.Count != LabelListCs.Count)
                            {
                                ErrorMessage = "MetadataElement Label: Number of items in array is not the same in national and international section!";
                                return false;
                            }

                            if (DescriptionListEn.Count != DescriptionListCs.Count)
                            {
                                ErrorMessage = "MetadataElement Description: Number of items in array is not the same in national and international section!";
                                return false;
                            }

                            if (LabelListEn.Count != DescriptionListEn.Count)
                            {
                                ErrorMessage = "MetadataElement: Number of labels and descriptions differ!";
                                return false;
                            }

                            ErrorMessage = String.Empty;
                            return true;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Returns the string that represents current object
                    /// </summary>
                    /// <returns>Returns the string that represents current object</returns>
                    public override string ToString()
                    {
                        return String.Concat(
                            $"LabelEn : {LabelEn ?? String.Empty}{Environment.NewLine}",
                            $"DescriptionEn : {DescriptionEn ?? String.Empty}{Environment.NewLine}",
                            $"LabelCs : {LabelCs ?? String.Empty}{Environment.NewLine}",
                            $"DescriptionCs : {DescriptionCs ?? String.Empty}{Environment.NewLine}");
                    }

                    /// <summary>
                    /// Determines whether specified object is equal to the current object
                    /// </summary>
                    /// <param name="obj">Speciefied object</param>
                    /// <returns>true/false</returns>
                    public override bool Equals(object obj)
                    {
                        // If the passed object is null, return False
                        if (obj == null)
                        {
                            return false;
                        }

                        // If the passed object is not MetadataElement Type, return False
                        if (obj is not MetadataElement)
                        {
                            return false;
                        }

                        return
                            LabelEn == ((MetadataElement)obj).LabelEn &&
                            DescriptionEn == ((MetadataElement)obj).DescriptionEn &&
                            LabelCs == ((MetadataElement)obj).LabelCs &&
                            DescriptionCs == ((MetadataElement)obj).DescriptionCs;
                    }

                    /// <summary>
                    /// Returns the hash code
                    /// </summary>
                    /// <returns>Hash code</returns>
                    public override int GetHashCode()
                    {
                        return
                            LabelEn.GetHashCode() ^
                            DescriptionEn.GetHashCode() ^
                            LabelCs.GetHashCode() ^
                            DescriptionCs.GetHashCode();
                    }

                    #endregion Methods

                }

                /// <summary>
                /// Metadata element for indicator
                /// </summary>
                /// <remarks>
                /// Constructor
                /// </remarks>
                /// <param name="objectEn">JSON object for international version</param>
                /// <param name="objectCs">JSON object for national version</param>
                public class TIndicator(JObject objectEn, JObject objectCs) : MetadataElement(objectEn: objectEn, objectCs: objectCs) { }

                /// <summary>
                /// Metadata element for state or change
                /// </summary>
                /// <remarks>
                /// Constructor
                /// </remarks>
                /// <param name="objectEn">JSON object for international version</param>
                /// <param name="objectCs">JSON object for national version</param>
                public class TStateOrChange(JObject objectEn, JObject objectCs) : MetadataElement(objectEn: objectEn, objectCs: objectCs) { }

                /// <summary>
                /// Metadata element for unit
                /// </summary>
                /// <remarks>
                /// Constructor
                /// </remarks>
                /// <param name="objectEn">JSON object for international version</param>
                /// <param name="objectCs">JSON object for national version</param>
                public class TUnit(JObject objectEn, JObject objectCs) : MetadataElement(objectEn: objectEn, objectCs: objectCs) { }

                /// <summary>
                /// Metadata element for local denisty
                /// </summary>
                /// <remarks>
                /// Constructor
                /// </remarks>
                /// <param name="objectEn">JSON object for international version</param>
                /// <param name="objectCs">JSON object for national version</param>
                public class TLocalDensity(JObject objectEn, JObject objectCs) : MetadataElement(objectEn: objectEn, objectCs: objectCs)
                {

                    #region Private Fields

                    /// <summary>
                    /// Message about problems found in JSON object
                    /// </summary>
                    private string errorMessage;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Metadata element
                    /// local density object type
                    /// labels in international version (read-only)
                    /// </summary>
                    private List<string> ObjectTypeLabelListEn
                    {
                        get
                        {
                            return
                                TargetVariableMetadataJson.GetStringList(
                                    obj: base.objectEn,
                                    key: TargetVariableMetadataJson.ElementKeyObjectTypeLabel);
                        }
                    }

                    /// <summary>
                    /// Metadata element
                    /// local density object type
                    /// descriptions in international version (read-only)
                    /// </summary>
                    private List<string> ObjectTypeDescriptionListEn
                    {
                        get
                        {
                            return
                                TargetVariableMetadataJson.GetStringList(
                                    obj: base.objectEn,
                                    key: TargetVariableMetadataJson.ElementKeyObjectTypeDescription);
                        }
                    }

                    /// <summary>
                    /// Metadata element
                    /// local density object type
                    /// labels in national version (read-only)
                    /// </summary>
                    private List<string> ObjectTypeLabelListCs
                    {
                        get
                        {
                            return
                                TargetVariableMetadataJson.GetStringList(
                                    obj: base.objectCs,
                                    key: TargetVariableMetadataJson.ElementKeyObjectTypeLabel);
                        }
                    }

                    /// <summary>
                    /// Metadata element
                    /// local density object type
                    /// descriptions in national version (read-only)
                    /// </summary>
                    private List<string> ObjectTypeDescriptionListCs
                    {
                        get
                        {
                            return
                                TargetVariableMetadataJson.GetStringList(
                                    obj: base.objectCs,
                                    key: TargetVariableMetadataJson.ElementKeyObjectTypeDescription);
                        }
                    }


                    /// <summary>
                    /// Metadata element
                    /// local density object type
                    /// label in international version (read-only)
                    /// </summary>
                    public string ObjectTypeLabelEn
                    {
                        get
                        {
                            return
                                (ObjectTypeLabelListEn == null) ? String.Empty :
                                (ObjectTypeLabelListEn.Count == 0) ? String.Empty :
                                (ObjectTypeLabelListEn.Count == 1) ? ObjectTypeLabelListEn[0] :
                                 ObjectTypeLabelListEn.Aggregate((a, b) => $"{a}; {b}");
                        }
                    }

                    /// <summary>
                    /// Metadata element
                    /// local density object type
                    /// description in international version(read-only)
                    /// </summary>
                    public string ObjectTypeDescriptionEn
                    {
                        get
                        {
                            return
                                (ObjectTypeDescriptionListEn == null) ? String.Empty :
                                (ObjectTypeDescriptionListEn.Count == 0) ? String.Empty :
                                (ObjectTypeDescriptionListEn.Count == 1) ? ObjectTypeDescriptionListEn[0] :
                                 ObjectTypeDescriptionListEn.Aggregate((a, b) => $"{a}; {b}");
                        }
                    }

                    /// <summary>
                    /// Metadata element
                    /// local density object type
                    /// label in national version (read-only)
                    /// </summary>
                    public string ObjectTypeLabelCs
                    {
                        get
                        {
                            return
                                (ObjectTypeLabelListCs == null) ? String.Empty :
                                (ObjectTypeLabelListCs.Count == 0) ? String.Empty :
                                (ObjectTypeLabelListCs.Count == 1) ? ObjectTypeLabelListCs[0] :
                                 ObjectTypeLabelListCs.Aggregate((a, b) => $"{a}; {b}");
                        }
                    }

                    /// <summary>
                    /// Metadata element
                    /// local density object type
                    /// description in national version (read-only)
                    /// </summary>
                    public string ObjectTypeDescriptionCs
                    {
                        get
                        {
                            return
                                (ObjectTypeDescriptionListCs == null) ? String.Empty :
                                (ObjectTypeDescriptionListCs.Count == 0) ? String.Empty :
                                (ObjectTypeDescriptionListCs.Count == 1) ? ObjectTypeDescriptionListCs[0] :
                                 ObjectTypeDescriptionListCs.Aggregate((a, b) => $"{a}; {b}");
                        }
                    }


                    /// <summary>
                    /// Metadata element
                    /// local density object
                    /// labels in international version (read-only)
                    /// </summary>
                    private List<string> ObjectLabelListEn
                    {
                        get
                        {
                            return
                                TargetVariableMetadataJson.GetStringList(
                                    obj: base.objectEn,
                                    key: TargetVariableMetadataJson.ElementKeyObjectLabel);
                        }
                    }

                    /// <summary>
                    /// Metadata element
                    /// local density object
                    /// descriptions in international version (read-only)
                    /// </summary>
                    private List<string> ObjectDescriptionListEn
                    {
                        get
                        {
                            return
                                TargetVariableMetadataJson.GetStringList(
                                    obj: base.objectEn,
                                    key: TargetVariableMetadataJson.ElementKeyObjectDescription);
                        }
                    }

                    /// <summary>
                    /// Metadata element
                    /// local density object
                    /// labels in national version (read-only)
                    /// </summary>
                    private List<string> ObjectLabelListCs
                    {
                        get
                        {
                            return
                                TargetVariableMetadataJson.GetStringList(
                                    obj: base.objectCs,
                                    key: TargetVariableMetadataJson.ElementKeyObjectLabel);
                        }
                    }

                    /// <summary>
                    /// Metadata element
                    /// local density object
                    /// descriptions in national version (read-only)
                    /// </summary>
                    private List<string> ObjectDescriptionListCs
                    {
                        get
                        {
                            return
                                TargetVariableMetadataJson.GetStringList(
                                    obj: base.objectCs,
                                    key: TargetVariableMetadataJson.ElementKeyObjectDescription);
                        }
                    }


                    /// <summary>
                    /// Metadata element
                    /// local density object
                    /// label in international version (read-only)
                    /// </summary>
                    public string ObjectLabelEn
                    {
                        get
                        {
                            return
                                (ObjectLabelListEn == null) ? String.Empty :
                                (ObjectLabelListEn.Count == 0) ? String.Empty :
                                (ObjectLabelListEn.Count == 1) ? ObjectLabelListEn[0] :
                                 ObjectLabelListEn.Aggregate((a, b) => $"{a}; {b}");
                        }
                    }

                    /// <summary>
                    /// Metadata element
                    /// local density object
                    /// description in international version(read-only)
                    /// </summary>
                    public string ObjectDescriptionEn
                    {
                        get
                        {
                            return
                                (ObjectDescriptionListEn == null) ? String.Empty :
                                (ObjectDescriptionListEn.Count == 0) ? String.Empty :
                                (ObjectDescriptionListEn.Count == 1) ? ObjectDescriptionListEn[0] :
                                 ObjectDescriptionListEn.Aggregate((a, b) => $"{a}; {b}");
                        }
                    }

                    /// <summary>
                    /// Metadata element
                    /// local density object
                    /// label in national version (read-only)
                    /// </summary>
                    public string ObjectLabelCs
                    {
                        get
                        {
                            return
                                (ObjectLabelListCs == null) ? String.Empty :
                                (ObjectLabelListCs.Count == 0) ? String.Empty :
                                (ObjectLabelListCs.Count == 1) ? ObjectLabelListCs[0] :
                                 ObjectLabelListCs.Aggregate((a, b) => $"{a}; {b}");
                        }
                    }

                    /// <summary>
                    /// Metadata element
                    /// local density object
                    /// description in national version (read-only)
                    /// </summary>
                    public string ObjectDescriptionCs
                    {
                        get
                        {
                            return
                                (ObjectDescriptionListCs == null) ? String.Empty :
                                (ObjectDescriptionListCs.Count == 0) ? String.Empty :
                                (ObjectDescriptionListCs.Count == 1) ? ObjectDescriptionListCs[0] :
                                 ObjectDescriptionListCs.Aggregate((a, b) => $"{a}; {b}");
                        }
                    }


                    /// <summary>
                    /// Metadata element
                    /// use negative
                    /// in international version(read-only)
                    /// </summary>
                    public Nullable<bool> UseNegativeEn
                    {
                        get
                        {
                            return
                                TargetVariableMetadataJson.GetNBoolean(
                                    obj: base.objectEn,
                                    key: TargetVariableMetadataJson.ElementKeyUseNegative);
                        }
                    }

                    /// <summary>
                    /// Metadata element
                    /// use negative
                    /// in national version(read-only)
                    /// </summary>
                    public Nullable<bool> UseNegativeCs
                    {
                        get
                        {
                            return
                                TargetVariableMetadataJson.GetNBoolean(
                                    obj: base.objectCs,
                                    key: TargetVariableMetadataJson.ElementKeyUseNegative);
                        }
                    }


                    /// <summary>
                    /// JSON object
                    /// for version
                    /// in international version
                    /// </summary>
                    private JObject JsonVersionEn
                    {
                        get
                        {
                            return
                                TargetVariableMetadataJson.GetJObject(
                                    obj: base.objectEn,
                                    key: TargetVariableMetadataJson.ElementKeyVersion);
                        }
                    }

                    /// <summary>
                    /// JSON object
                    /// for version
                    /// in national version
                    /// </summary>
                    private JObject JsonVersionCs
                    {
                        get
                        {
                            return
                                TargetVariableMetadataJson.GetJObject(
                                    obj: base.objectCs,
                                    key: TargetVariableMetadataJson.ElementKeyVersion);
                        }
                    }


                    /// <summary>
                    /// JSON object
                    /// for definition varinat
                    /// in international version
                    /// </summary>
                    private JObject JsonDefinitionVariantEn
                    {
                        get
                        {
                            return
                                TargetVariableMetadataJson.GetJObject(
                                    obj: base.objectEn,
                                    key: TargetVariableMetadataJson.ElementKeyDefinitionVariant);
                        }
                    }

                    /// <summary>
                    /// JSON object
                    /// for definition variant
                    /// in national version
                    /// </summary>
                    private JObject JsonDefinitionVariantCs
                    {
                        get
                        {
                            return
                                TargetVariableMetadataJson.GetJObject(
                                    obj: base.objectCs,
                                    key: TargetVariableMetadataJson.ElementKeyDefinitionVariant);
                        }
                    }


                    /// <summary>
                    /// JSON object
                    /// for area domain
                    /// in international version
                    /// </summary>
                    private JObject JsonAreaDomainEn
                    {
                        get
                        {
                            return
                                TargetVariableMetadataJson.GetJObject(
                                    obj: base.objectEn,
                                    key: TargetVariableMetadataJson.ElementKeyAreaDomain);
                        }
                    }

                    /// <summary>
                    /// JSON object
                    /// for area domain
                    /// in national version
                    /// </summary>
                    private JObject JsonAreaDomainCs
                    {
                        get
                        {
                            return
                                TargetVariableMetadataJson.GetJObject(
                                    obj: objectCs,
                                    key: TargetVariableMetadataJson.ElementKeyAreaDomain);
                        }
                    }


                    /// <summary>
                    /// JSON object
                    /// for population
                    /// in internatioinal version
                    /// </summary>
                    private JObject JsonPopulationEn
                    {
                        get
                        {
                            return
                                TargetVariableMetadataJson.GetJObject(
                                    obj: base.objectEn,
                                    key: TargetVariableMetadataJson.ElementKeyPopulation);
                        }
                    }

                    /// <summary>
                    /// JSON object
                    /// for population
                    /// in national version
                    /// </summary>
                    private JObject JsonPopulationCs
                    {
                        get
                        {
                            return
                                TargetVariableMetadataJson.GetJObject(
                                    obj: base.objectCs,
                                    key: TargetVariableMetadataJson.ElementKeyPopulation);
                        }
                    }


                    /// <summary>
                    /// Version
                    /// </summary>
                    public TVersion Version
                    {
                        get
                        {
                            if (JsonVersionEn == null)
                            {
                                return null;
                            }
                            else if (JsonVersionCs == null)
                            {
                                return null;
                            }
                            else
                            {
                                return
                                    new TVersion(
                                        objectEn: JsonVersionEn,
                                        objectCs: JsonVersionCs);
                            }
                        }
                    }

                    /// <summary>
                    /// Definition variant
                    /// </summary>
                    public TDefinitionVariant DefinitionVariant
                    {
                        get
                        {
                            if (JsonDefinitionVariantEn == null)
                            {
                                return null;
                            }
                            else if (JsonDefinitionVariantCs == null)
                            {
                                return null;
                            }
                            else
                            {
                                return
                                    new TDefinitionVariant(
                                        objectEn: JsonDefinitionVariantEn,
                                        objectCs: JsonDefinitionVariantCs);
                            }
                        }
                    }

                    /// <summary>
                    /// Area domain
                    /// </summary>
                    public TAreaDomain AreaDomain
                    {
                        get
                        {
                            if (JsonAreaDomainEn == null)
                            {
                                return null;
                            }
                            else if (JsonAreaDomainCs == null)
                            {
                                return null;
                            }
                            else
                            {
                                return
                                    new TAreaDomain(
                                        objectEn: JsonAreaDomainEn,
                                        objectCs: JsonAreaDomainCs);
                            }
                        }
                    }

                    /// <summary>
                    /// Population
                    /// </summary>
                    public TPopulation Population
                    {
                        get
                        {
                            if (JsonPopulationEn == null)
                            {
                                return null;
                            }
                            else if (JsonPopulationCs == null)
                            {
                                return null;
                            }
                            else
                            {
                                return
                                    new TPopulation(
                                        objectEn: JsonPopulationEn,
                                        objectCs: JsonPopulationCs);
                            }
                        }
                    }


                    /// <summary>
                    /// Message about problems found in JSON object
                    /// </summary>
                    public override string ErrorMessage
                    {
                        get
                        {
                            return IsValid ? String.Empty : errorMessage;
                        }
                        set
                        {
                            errorMessage = value;
                        }
                    }


                    /// <summary>
                    /// Does this metadata element have valid JSON file?
                    /// </summary>
                    public override bool IsValid
                    {
                        get
                        {
                            if (!base.IsValid)
                            {
                                ErrorMessage = base.ErrorMessage;
                                return false;
                            }


                            if (!TargetVariableMetadataJson.ValidateStringList(
                                obj: objectEn,
                                key: TargetVariableMetadataJson.ElementKeyObjectTypeLabel,
                                msg: out string msgElementKeyObjectTypeLabelEn))
                            {
                                ErrorMessage = msgElementKeyObjectTypeLabelEn;
                                return false;
                            }

                            if (!TargetVariableMetadataJson.ValidateStringList(
                               obj: objectEn,
                               key: TargetVariableMetadataJson.ElementKeyObjectTypeDescription,
                               msg: out string msgElementKeyObjectTypeDescriptionEn))
                            {
                                ErrorMessage = msgElementKeyObjectTypeDescriptionEn;
                                return false;
                            }

                            if (!TargetVariableMetadataJson.ValidateStringList(
                               obj: objectCs,
                               key: TargetVariableMetadataJson.ElementKeyObjectTypeLabel,
                               msg: out string msgElementKeyObjectTypeLabelCs))
                            {
                                ErrorMessage = msgElementKeyObjectTypeLabelCs;
                                return false;
                            }

                            if (!TargetVariableMetadataJson.ValidateStringList(
                               obj: objectCs,
                               key: TargetVariableMetadataJson.ElementKeyObjectTypeDescription,
                               msg: out string msgElementKeyObjectTypeDescriptionCs))
                            {
                                ErrorMessage = msgElementKeyObjectTypeDescriptionCs;
                                return false;
                            }

                            if (ObjectTypeLabelListEn.Count != ObjectTypeLabelListCs.Count)
                            {
                                ErrorMessage = "Počet";
                                return false;
                            }

                            if (ObjectTypeDescriptionListEn.Count != ObjectTypeDescriptionListCs.Count)
                            {
                                ErrorMessage = "Počet";
                                return false;
                            }

                            if (ObjectTypeLabelListEn.Count != ObjectTypeDescriptionListEn.Count)
                            {
                                ErrorMessage = "Počet";
                                return false;
                            }


                            if (!TargetVariableMetadataJson.ValidateStringList(
                                obj: objectEn,
                                key: TargetVariableMetadataJson.ElementKeyObjectLabel,
                                msg: out string msgElementKeyObjectLabelEn))
                            {
                                ErrorMessage = msgElementKeyObjectLabelEn;
                                return false;
                            }

                            if (!TargetVariableMetadataJson.ValidateStringList(
                               obj: objectEn,
                               key: TargetVariableMetadataJson.ElementKeyObjectDescription,
                               msg: out string msgElementKeyObjectDescriptionEn))
                            {
                                ErrorMessage = msgElementKeyObjectDescriptionEn;
                                return false;
                            }

                            if (!TargetVariableMetadataJson.ValidateStringList(
                               obj: objectCs,
                               key: TargetVariableMetadataJson.ElementKeyObjectLabel,
                               msg: out string msgElementKeyObjectLabelCs))
                            {
                                ErrorMessage = msgElementKeyObjectLabelCs;
                                return false;
                            }

                            if (!TargetVariableMetadataJson.ValidateStringList(
                               obj: objectCs,
                               key: TargetVariableMetadataJson.ElementKeyObjectDescription,
                               msg: out string msgElementKeyObjectDescriptionCs))
                            {
                                ErrorMessage = msgElementKeyObjectDescriptionCs;
                                return false;
                            }

                            if (ObjectLabelListEn.Count != ObjectLabelListCs.Count)
                            {
                                ErrorMessage = "Počet";
                                return false;
                            }

                            if (ObjectDescriptionListEn.Count != ObjectDescriptionListCs.Count)
                            {
                                ErrorMessage = "Počet";
                                return false;
                            }

                            if (ObjectLabelListEn.Count != ObjectDescriptionListEn.Count)
                            {
                                ErrorMessage = "Počet";
                                return false;
                            }


                            if (!TargetVariableMetadataJson.ValidateNBoolean(
                                obj: objectEn,
                                key: TargetVariableMetadataJson.ElementKeyUseNegative,
                                msg: out string msgElementKeyUseNegativeEn))
                            {
                                ErrorMessage = msgElementKeyUseNegativeEn;
                                return false;
                            }

                            if (!TargetVariableMetadataJson.ValidateNBoolean(
                                obj: objectCs,
                                key: TargetVariableMetadataJson.ElementKeyUseNegative,
                                msg: out string msgElementKeyUseNegativeCs))
                            {
                                ErrorMessage = msgElementKeyUseNegativeCs;
                                return false;
                            }


                            if (!TargetVariableMetadataJson.ValidateJObject(
                                obj: objectEn,
                                key: TargetVariableMetadataJson.ElementKeyVersion,
                                msg: out string msgElementKeyVersionEn))
                            {
                                ErrorMessage = msgElementKeyVersionEn;
                                return false;
                            }

                            if (!TargetVariableMetadataJson.ValidateJObject(
                                obj: objectCs,
                                key: TargetVariableMetadataJson.ElementKeyVersion,
                                msg: out string msgElementKeyVersionCs))
                            {
                                ErrorMessage = msgElementKeyVersionCs;
                                return false;
                            }

                            if (!Version.IsValid)
                            {
                                ErrorMessage = Version.ErrorMessage;
                                return false;
                            }


                            if (!TargetVariableMetadataJson.ValidateJObject(
                                obj: objectEn,
                                key: TargetVariableMetadataJson.ElementKeyDefinitionVariant,
                                msg: out string msgElementKeyDefinitionVariantEn))
                            {
                                ErrorMessage = msgElementKeyDefinitionVariantEn;
                                return false;
                            }

                            if (!TargetVariableMetadataJson.ValidateJObject(
                                obj: objectCs,
                                key: TargetVariableMetadataJson.ElementKeyDefinitionVariant,
                                msg: out string msgElementKeyDefinitionVariantCs))
                            {
                                ErrorMessage = msgElementKeyDefinitionVariantCs;
                                return false;
                            }

                            if (!DefinitionVariant.IsValid)
                            {
                                ErrorMessage = DefinitionVariant.ErrorMessage;
                                return false;
                            }


                            if (!TargetVariableMetadataJson.ValidateJObject(
                                obj: objectEn,
                                key: TargetVariableMetadataJson.ElementKeyAreaDomain,
                                msg: out string msgElementKeyAreaDomainEn))
                            {
                                ErrorMessage = msgElementKeyAreaDomainEn;
                                return false;
                            }

                            if (!TargetVariableMetadataJson.ValidateJObject(
                                obj: objectCs,
                                key: TargetVariableMetadataJson.ElementKeyAreaDomain,
                                msg: out string msgElementKeyAreaDomainCs))
                            {
                                ErrorMessage = msgElementKeyAreaDomainCs;
                                return false;
                            }

                            if (!AreaDomain.IsValid)
                            {
                                ErrorMessage = AreaDomain.ErrorMessage;
                                return false;
                            }


                            if (!TargetVariableMetadataJson.ValidateJObject(
                                obj: objectEn,
                                key: TargetVariableMetadataJson.ElementKeyPopulation,
                                msg: out string msgElementKeyPopulationEn))
                            {
                                ErrorMessage = msgElementKeyPopulationEn;
                                return false;
                            }

                            if (!TargetVariableMetadataJson.ValidateJObject(
                                obj: objectCs,
                                key: TargetVariableMetadataJson.ElementKeyPopulation,
                                msg: out string msgElementKeyPopulationCs))
                            {
                                ErrorMessage = msgElementKeyPopulationCs;
                                return false;
                            }

                            if (!Population.IsValid)
                            {
                                ErrorMessage = Population.ErrorMessage;
                                return false;
                            }

                            return true;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Returns the string that represents current object
                    /// </summary>
                    /// <returns>Returns the string that represents current object</returns>
                    public override string ToString()
                    {
                        return String.Concat(
                            $"ObjectTypeLabelEn : {ObjectTypeLabelEn ?? String.Empty}{Environment.NewLine}",
                            $"ObjectTypeDescriptionEn : {ObjectTypeDescriptionEn ?? String.Empty}{Environment.NewLine}",
                            $"ObjectTypeLabelCs : {ObjectTypeLabelCs ?? String.Empty}{Environment.NewLine}",
                            $"ObjectTypeDescriptionCs : {ObjectTypeDescriptionCs ?? String.Empty}{Environment.NewLine}",
                            $"ObjectLabelEn : {ObjectLabelEn ?? String.Empty}{Environment.NewLine}",
                            $"ObjectDescriptionEn : {ObjectDescriptionEn ?? String.Empty}{Environment.NewLine}",
                            $"ObjectLabelCs : {ObjectLabelCs ?? String.Empty}{Environment.NewLine}",
                            $"ObjectDescriptionCs : {ObjectDescriptionCs ?? String.Empty}{Environment.NewLine}",
                            $"UseNegativeEn : {UseNegativeEn?.ToString() ?? String.Empty}{Environment.NewLine}",
                            $"UseNegativeCs : {UseNegativeCs?.ToString() ?? String.Empty}{Environment.NewLine}",
                            base.ToString());
                    }

                    /// <summary>
                    /// Determines whether specified object is equal to the current object
                    /// </summary>
                    /// <param name="obj">Speciefied object</param>
                    /// <returns>true/false</returns>
                    public override bool Equals(object obj)
                    {
                        // If the passed object is null, return False
                        if (obj == null)
                        {
                            return false;
                        }

                        // If the passed object is not TLocalDensity Type, return False
                        if (obj is not TLocalDensity)
                        {
                            return false;
                        }

                        return
                            LabelCs == ((TLocalDensity)obj).LabelCs &&
                            DescriptionCs == ((TLocalDensity)obj).DescriptionCs &&
                            LabelEn == ((TLocalDensity)obj).LabelEn &&
                            DescriptionEn == ((TLocalDensity)obj).DescriptionEn &&
                            ObjectTypeLabelCs == ((TLocalDensity)obj).ObjectTypeLabelCs &&
                            ObjectTypeDescriptionCs == ((TLocalDensity)obj).ObjectTypeDescriptionCs &&
                            ObjectTypeLabelEn == ((TLocalDensity)obj).ObjectTypeLabelEn &&
                            ObjectTypeDescriptionEn == ((TLocalDensity)obj).ObjectTypeDescriptionEn &&
                            ObjectLabelCs == ((TLocalDensity)obj).ObjectLabelCs &&
                            ObjectDescriptionCs == ((TLocalDensity)obj).ObjectDescriptionCs &&
                            ObjectLabelEn == ((TLocalDensity)obj).ObjectLabelEn &&
                            ObjectDescriptionEn == ((TLocalDensity)obj).ObjectDescriptionEn &&
                            (UseNegativeEn?.ToString() ?? String.Empty) == (((TLocalDensity)obj).UseNegativeEn?.ToString() ?? String.Empty) &&
                            (UseNegativeCs?.ToString() ?? String.Empty) == (((TLocalDensity)obj).UseNegativeCs?.ToString() ?? String.Empty) &&
                            Version.Equals(((TLocalDensity)obj).Version) &&
                            DefinitionVariant.Equals(((TLocalDensity)obj).DefinitionVariant) &&
                            AreaDomain.Equals(((TLocalDensity)obj).AreaDomain) &&
                            Population.Equals(((TLocalDensity)obj).Population);
                    }

                    /// <summary>
                    /// Returns the hash code
                    /// </summary>
                    /// <returns>Hash code</returns>
                    public override int GetHashCode()
                    {
                        return
                            LabelCs.GetHashCode() ^
                            DescriptionCs.GetHashCode() ^
                            LabelEn.GetHashCode() ^
                            DescriptionEn.GetHashCode() ^
                            ObjectTypeLabelCs.GetHashCode() ^
                            ObjectTypeDescriptionCs.GetHashCode() ^
                            ObjectTypeLabelEn.GetHashCode() ^
                            ObjectTypeDescriptionEn.GetHashCode() ^
                            ObjectLabelCs.GetHashCode() ^
                            ObjectDescriptionCs.GetHashCode() ^
                            ObjectLabelEn.GetHashCode() ^
                            ObjectDescriptionEn.GetHashCode() ^
                            (UseNegativeEn?.GetHashCode() ?? 0) ^
                            (UseNegativeCs?.GetHashCode() ?? 0) ^
                            Version.GetHashCode() ^
                            DefinitionVariant.GetHashCode() ^
                            AreaDomain.GetHashCode() ^
                            Population.GetHashCode();
                    }

                    #endregion Methods

                }

                /// <summary>
                /// Metadata element for version
                /// </summary>
                /// <remarks>
                /// Constructor
                /// </remarks>
                /// <param name="objectEn">JSON object for international version</param>
                /// <param name="objectCs">JSON object for national version</param>
                public class TVersion(JObject objectEn, JObject objectCs) : MetadataElement(objectEn: objectEn, objectCs: objectCs) { }

                /// <summary>
                /// Metadata element for definition variant
                /// </summary>
                /// <remarks>
                /// Constructor
                /// </remarks>
                /// <param name="objectEn">JSON object for international version</param>
                /// <param name="objectCs">JSON object for national version</param>
                public class TDefinitionVariant(JObject objectEn, JObject objectCs) : MetadataElement(objectEn: objectEn, objectCs: objectCs) { }

                /// <summary>
                /// Metadata element for area domain
                /// </summary>
                /// <remarks>
                /// Constructor
                /// </remarks>
                /// <param name="objectEn">JSON object for international version</param>
                /// <param name="objectCs">JSON object for national version</param>
                public class TAreaDomain(JObject objectEn, JObject objectCs) : MetadataElement(objectEn: objectEn, objectCs: objectCs) { }

                /// <summary>
                /// Metadata element for population
                /// </summary>
                /// <remarks>
                /// Constructor
                /// </remarks>
                /// <param name="objectEn">JSON object for international version</param>
                /// <param name="objectCs">JSON object for national version</param>
                public class TPopulation(JObject objectEn, JObject objectCs) : MetadataElement(objectEn: objectEn, objectCs: objectCs) { }

                #endregion Internal Classes

            }

        }
    }
}
