﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {
            // v_result_sampling_units

            /// <summary>
            /// Sampling unit
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class ResultSamplingUnit(
                ResultSamplingUnitList composite = null,
                DataRow data = null)
                    : ALinqViewEntry<ResultSamplingUnitList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Id
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: ResultSamplingUnitList.ColId.Name,
                            defaultValue: Int32.Parse(s: ResultSamplingUnitList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: ResultSamplingUnitList.ColId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// ResultId
                /// </summary>
                public int ResultId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: ResultSamplingUnitList.ColResultId.Name,
                            defaultValue: Int32.Parse(s: ResultSamplingUnitList.ColResultId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: ResultSamplingUnitList.ColResultId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// JsonValidity
                /// </summary>
                public bool JsonValidity
                {
                    get
                    {
                        return Functions.GetBoolArg(
                            row: Data,
                            name: ResultSamplingUnitList.ColJsonValidity.Name,
                            defaultValue: Boolean.Parse(value: ResultSamplingUnitList.ColJsonValidity.DefaultValue));
                    }
                    set
                    {
                        Functions.SetBoolArg(
                            row: Data,
                            name: ResultSamplingUnitList.ColJsonValidity.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Attribute
                /// </summary>
                public Nullable<int> Attribute
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: ResultSamplingUnitList.ColAttribute.Name,
                            defaultValue: String.IsNullOrEmpty(value: ResultSamplingUnitList.ColAttribute.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: ResultSamplingUnitList.ColAttribute.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: ResultSamplingUnitList.ColAttribute.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Stratum
                /// </summary>
                public Nullable<int> Stratum
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: ResultSamplingUnitList.ColStratum.Name,
                            defaultValue: String.IsNullOrEmpty(value: ResultSamplingUnitList.ColStratum.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: ResultSamplingUnitList.ColStratum.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: ResultSamplingUnitList.ColStratum.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// UnitsParamArea
                /// </summary>
                public Nullable<int> UnitsParamArea
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: ResultSamplingUnitList.ColUnitsParamArea.Name,
                            defaultValue: String.IsNullOrEmpty(value: ResultSamplingUnitList.ColUnitsParamArea.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: ResultSamplingUnitList.ColUnitsParamArea.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: ResultSamplingUnitList.ColUnitsParamArea.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// UnitsCell
                /// </summary>
                public Nullable<int> UnitsCell
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: ResultSamplingUnitList.ColUnitsCell.Name,
                            defaultValue: String.IsNullOrEmpty(value: ResultSamplingUnitList.ColUnitsCell.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: ResultSamplingUnitList.ColUnitsCell.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: ResultSamplingUnitList.ColUnitsCell.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// UnitsCellNonZero
                /// </summary>
                public Nullable<int> UnitsCellNonZero
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: ResultSamplingUnitList.ColUnitsCellNonZero.Name,
                            defaultValue: String.IsNullOrEmpty(value: ResultSamplingUnitList.ColUnitsCellNonZero.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: ResultSamplingUnitList.ColUnitsCellNonZero.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: ResultSamplingUnitList.ColUnitsCellNonZero.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// AttributeNom
                /// </summary>
                public Nullable<int> AttributeNom
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: ResultSamplingUnitList.ColAttributeNom.Name,
                            defaultValue: String.IsNullOrEmpty(value: ResultSamplingUnitList.ColAttributeNom.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: ResultSamplingUnitList.ColAttributeNom.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: ResultSamplingUnitList.ColAttributeNom.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// StratumNom
                /// </summary>
                public Nullable<int> StratumNom
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: ResultSamplingUnitList.ColStratumNom.Name,
                            defaultValue: String.IsNullOrEmpty(value: ResultSamplingUnitList.ColStratumNom.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: ResultSamplingUnitList.ColStratumNom.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: ResultSamplingUnitList.ColStratumNom.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// UnitsParamAreaNom
                /// </summary>
                public Nullable<int> UnitsParamAreaNom
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: ResultSamplingUnitList.ColUnitsParamAreaNom.Name,
                            defaultValue: String.IsNullOrEmpty(value: ResultSamplingUnitList.ColUnitsParamAreaNom.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: ResultSamplingUnitList.ColUnitsParamAreaNom.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: ResultSamplingUnitList.ColUnitsParamAreaNom.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// UnitsCellNom
                /// </summary>
                public Nullable<int> UnitsCellNom
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: ResultSamplingUnitList.ColUnitsCellNom.Name,
                            defaultValue: String.IsNullOrEmpty(value: ResultSamplingUnitList.ColUnitsCellNom.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: ResultSamplingUnitList.ColUnitsCellNom.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: ResultSamplingUnitList.ColUnitsCellNom.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// UnitsCellNonZeroNom
                /// </summary>
                public Nullable<int> UnitsCellNonZeroNom
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: ResultSamplingUnitList.ColUnitsCellNonZeroNom.Name,
                            defaultValue: String.IsNullOrEmpty(value: ResultSamplingUnitList.ColUnitsCellNonZeroNom.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: ResultSamplingUnitList.ColUnitsCellNonZeroNom.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: ResultSamplingUnitList.ColUnitsCellNonZeroNom.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// AttributeDenom
                /// </summary>
                public Nullable<int> AttributeDenom
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: ResultSamplingUnitList.ColAttributeDenom.Name,
                            defaultValue: String.IsNullOrEmpty(value: ResultSamplingUnitList.ColAttributeDenom.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: ResultSamplingUnitList.ColAttributeDenom.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: ResultSamplingUnitList.ColAttributeDenom.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// StratumDenom
                /// </summary>
                public Nullable<int> StratumDenom
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: ResultSamplingUnitList.ColStratumDenom.Name,
                            defaultValue: String.IsNullOrEmpty(value: ResultSamplingUnitList.ColStratumDenom.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: ResultSamplingUnitList.ColStratumDenom.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: ResultSamplingUnitList.ColStratumDenom.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// UnitsParamAreaDenom
                /// </summary>
                public Nullable<int> UnitsParamAreaDenom
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: ResultSamplingUnitList.ColUnitsParamAreaDenom.Name,
                            defaultValue: String.IsNullOrEmpty(value: ResultSamplingUnitList.ColUnitsParamAreaDenom.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: ResultSamplingUnitList.ColUnitsParamAreaDenom.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            ResultSamplingUnitList.ColUnitsParamAreaDenom.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// UnitsCellDenom
                /// </summary>
                public Nullable<int> UnitsCellDenom
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: ResultSamplingUnitList.ColUnitsCellDenom.Name,
                            defaultValue: String.IsNullOrEmpty(value: ResultSamplingUnitList.ColUnitsCellDenom.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: ResultSamplingUnitList.ColUnitsCellDenom.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: ResultSamplingUnitList.ColUnitsCellDenom.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// UnitsCellNonZeroDenom
                /// </summary>
                public Nullable<int> UnitsCellNonZeroDenom
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: ResultSamplingUnitList.ColUnitsCellNonZeroDenom.Name,
                            defaultValue: String.IsNullOrEmpty(value: ResultSamplingUnitList.ColUnitsCellNonZeroDenom.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: ResultSamplingUnitList.ColUnitsCellNonZeroDenom.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: ResultSamplingUnitList.ColUnitsCellNonZeroDenom.Name,
                            val: value);
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not ResultSamplingUnit Type, return False
                    if (obj is not ResultSamplingUnit)
                    {
                        return false;
                    }

                    return
                        Id == ((ResultSamplingUnit)obj).Id;
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                #endregion Methods

            }


            /// <summary>
            /// List of sampling units
            /// </summary>
            public class ResultSamplingUnitList
                : ALinqView
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema = NfiEstaSchema.Name;

                /// <summary>
                /// Table name
                /// </summary>
                public const string Name = "v_result_sampling_units";

                /// <summary>
                /// Table alias
                /// </summary>
                public const string Alias = "v_result_sampling_units";

                /// <summary>
                /// Description in national language
                /// </summary>
                public const string CaptionCs = "Metadata vypočteného odhadu";

                /// <summary>
                /// Description in English
                /// </summary>
                public const string CaptionEn = "Calculated result metadata";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
                {
                    { ResultSamplingUnitsJson.ColId, new ColumnMetadata()
                    {
                        Name = ResultSamplingUnitsJson.ColId,
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = true,
                        NotNull = true,
                        DefaultValue = default(int).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = ResultSamplingUnitsJson.ColId.ToUpper(),
                        HeaderTextEn = ResultSamplingUnitsJson.ColId.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 0
                    }
                    },
                    { ResultSamplingUnitsJson.ColResultId, new ColumnMetadata()
                    {
                        Name = ResultSamplingUnitsJson.ColResultId,
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue = default(int).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = ResultSamplingUnitsJson.ColResultId.ToUpper(),
                        HeaderTextEn = ResultSamplingUnitsJson.ColResultId.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 1
                    }
                    },
                    { ResultSamplingUnitsJson.ColJsonValidity, new ColumnMetadata()
                    {
                        Name = ResultSamplingUnitsJson.ColJsonValidity,
                        DbName = null,
                        DataType = "System.Boolean",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue = default(bool).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = ResultSamplingUnitsJson.ColJsonValidity.ToUpper(),
                        HeaderTextEn = ResultSamplingUnitsJson.ColJsonValidity.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 2
                    }
                    },

                    { ResultSamplingUnitsJson.ElementKeyAttribute, new ColumnMetadata()
                    {
                        Name = ResultSamplingUnitsJson.ElementKeyAttribute,
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = ResultSamplingUnitsJson.ElementKeyAttribute.ToUpper(),
                        HeaderTextEn = ResultSamplingUnitsJson.ElementKeyAttribute.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 3
                    }
                    },
                    { ResultSamplingUnitsJson.ElementKeyStratum, new ColumnMetadata()
                    {
                        Name = ResultSamplingUnitsJson.ElementKeyStratum,
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = ResultSamplingUnitsJson.ElementKeyStratum.ToUpper(),
                        HeaderTextEn = ResultSamplingUnitsJson.ElementKeyStratum.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 4
                    }
                    },
                    { ResultSamplingUnitsJson.ElementKeyUnitsParamArea, new ColumnMetadata()
                    {
                        Name = ResultSamplingUnitsJson.ElementKeyUnitsParamArea,
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = ResultSamplingUnitsJson.ElementKeyUnitsParamArea.ToUpper(),
                        HeaderTextEn = ResultSamplingUnitsJson.ElementKeyUnitsParamArea.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 5
                    }
                    },
                    { ResultSamplingUnitsJson.ElementKeyUnitsCell, new ColumnMetadata()
                    {
                        Name = ResultSamplingUnitsJson.ElementKeyUnitsCell,
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = ResultSamplingUnitsJson.ElementKeyUnitsCell.ToUpper(),
                        HeaderTextEn = ResultSamplingUnitsJson.ElementKeyUnitsCell.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 6
                    }
                    },
                    { ResultSamplingUnitsJson.ElementKeyUnitsCellNonZero, new ColumnMetadata()
                    {
                        Name = ResultSamplingUnitsJson.ElementKeyUnitsCellNonZero,
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = ResultSamplingUnitsJson.ElementKeyUnitsCellNonZero.ToUpper(),
                        HeaderTextEn = ResultSamplingUnitsJson.ElementKeyUnitsCellNonZero.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 7
                    }
                    },

                    { ResultSamplingUnitsJson.ElementKeyAttributeNom, new ColumnMetadata()
                    {
                        Name = ResultSamplingUnitsJson.ElementKeyAttributeNom,
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = ResultSamplingUnitsJson.ElementKeyAttributeNom.ToUpper(),
                        HeaderTextEn = ResultSamplingUnitsJson.ElementKeyAttributeNom.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 8
                    }
                    },
                    { ResultSamplingUnitsJson.ElementKeyStratumNom, new ColumnMetadata()
                    {
                        Name = ResultSamplingUnitsJson.ElementKeyStratumNom,
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = ResultSamplingUnitsJson.ElementKeyStratumNom.ToUpper(),
                        HeaderTextEn = ResultSamplingUnitsJson.ElementKeyStratumNom.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 9
                    }
                    },
                    { ResultSamplingUnitsJson.ElementKeyUnitsParamAreaNom, new ColumnMetadata()
                    {
                        Name = ResultSamplingUnitsJson.ElementKeyUnitsParamAreaNom,
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = ResultSamplingUnitsJson.ElementKeyUnitsParamAreaNom.ToUpper(),
                        HeaderTextEn = ResultSamplingUnitsJson.ElementKeyUnitsParamAreaNom.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 10
                    }
                    },
                    { ResultSamplingUnitsJson.ElementKeyUnitsCellNom, new ColumnMetadata()
                    {
                        Name = ResultSamplingUnitsJson.ElementKeyUnitsCellNom,
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = ResultSamplingUnitsJson.ElementKeyUnitsCellNom.ToUpper(),
                        HeaderTextEn = ResultSamplingUnitsJson.ElementKeyUnitsCellNom.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 11
                    }
                    },
                    { ResultSamplingUnitsJson.ElementKeyUnitsCellNonZeroNom, new ColumnMetadata()
                    {
                        Name = ResultSamplingUnitsJson.ElementKeyUnitsCellNonZeroNom,
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = ResultSamplingUnitsJson.ElementKeyUnitsCellNonZeroNom.ToUpper(),
                        HeaderTextEn = ResultSamplingUnitsJson.ElementKeyUnitsCellNonZeroNom.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 12
                    }
                    },

                    { ResultSamplingUnitsJson.ElementKeyAttributeDenom, new ColumnMetadata()
                    {
                        Name = ResultSamplingUnitsJson.ElementKeyAttributeDenom,
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = ResultSamplingUnitsJson.ElementKeyAttributeDenom.ToUpper(),
                        HeaderTextEn = ResultSamplingUnitsJson.ElementKeyAttributeDenom.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 13
                    }
                    },
                    { ResultSamplingUnitsJson.ElementKeyStratumDenom, new ColumnMetadata()
                    {
                        Name = ResultSamplingUnitsJson.ElementKeyStratumDenom,
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = ResultSamplingUnitsJson.ElementKeyStratumDenom.ToUpper(),
                        HeaderTextEn = ResultSamplingUnitsJson.ElementKeyStratumDenom.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 14
                    }
                    },
                    { ResultSamplingUnitsJson.ElementKeyUnitsParamAreaDenom, new ColumnMetadata()
                    {
                        Name = ResultSamplingUnitsJson.ElementKeyUnitsParamAreaDenom,
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = ResultSamplingUnitsJson.ElementKeyUnitsParamAreaDenom.ToUpper(),
                        HeaderTextEn = ResultSamplingUnitsJson.ElementKeyUnitsParamAreaDenom.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 15
                    }
                    },
                    { ResultSamplingUnitsJson.ElementKeyUnitsCellDenom, new ColumnMetadata()
                    {
                        Name = ResultSamplingUnitsJson.ElementKeyUnitsCellDenom,
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = ResultSamplingUnitsJson.ElementKeyUnitsCellDenom.ToUpper(),
                        HeaderTextEn = ResultSamplingUnitsJson.ElementKeyUnitsCellDenom.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 16
                    }
                    },
                    { ResultSamplingUnitsJson.ElementKeyUnitsCellNonZeroDenom, new ColumnMetadata()
                    {
                        Name = ResultSamplingUnitsJson.ElementKeyUnitsCellNonZeroDenom,
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = ResultSamplingUnitsJson.ElementKeyUnitsCellNonZeroDenom.ToUpper(),
                        HeaderTextEn = ResultSamplingUnitsJson.ElementKeyUnitsCellNonZeroDenom.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 17
                    }
                    }
                };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols[ResultSamplingUnitsJson.ColId];

                /// <summary>
                /// Column result_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColResultId = Cols[ResultSamplingUnitsJson.ColResultId];

                /// <summary>
                /// Column json_validity metadata
                /// </summary>
                public static readonly ColumnMetadata ColJsonValidity = Cols[ResultSamplingUnitsJson.ColJsonValidity];


                /// <summary>
                /// Column attribute metadata
                /// </summary>
                public static readonly ColumnMetadata ColAttribute = Cols[ResultSamplingUnitsJson.ElementKeyAttribute];

                /// <summary>
                /// Column stratum metadata
                /// </summary>
                public static readonly ColumnMetadata ColStratum = Cols[ResultSamplingUnitsJson.ElementKeyStratum];

                /// <summary>
                /// Column s_units_param_area metadata
                /// </summary>
                public static readonly ColumnMetadata ColUnitsParamArea = Cols[ResultSamplingUnitsJson.ElementKeyUnitsParamArea];

                /// <summary>
                /// Column s_units_cell metadata
                /// </summary>
                public static readonly ColumnMetadata ColUnitsCell = Cols[ResultSamplingUnitsJson.ElementKeyUnitsCell];

                /// <summary>
                /// Column s_units_cell_nonzero metadata
                /// </summary>
                public static readonly ColumnMetadata ColUnitsCellNonZero = Cols[ResultSamplingUnitsJson.ElementKeyUnitsCellNonZero];


                /// <summary>
                /// Column attribute_nom metadata
                /// </summary>
                public static readonly ColumnMetadata ColAttributeNom = Cols[ResultSamplingUnitsJson.ElementKeyAttributeNom];

                /// <summary>
                /// Column stratum_nom metadata
                /// </summary>
                public static readonly ColumnMetadata ColStratumNom = Cols[ResultSamplingUnitsJson.ElementKeyStratumNom];

                /// <summary>
                /// Column s_units_param_area_nom metadata
                /// </summary>
                public static readonly ColumnMetadata ColUnitsParamAreaNom = Cols[ResultSamplingUnitsJson.ElementKeyUnitsParamAreaNom];

                /// <summary>
                /// Column s_units_cell_nom metadata
                /// </summary>
                public static readonly ColumnMetadata ColUnitsCellNom = Cols[ResultSamplingUnitsJson.ElementKeyUnitsCellNom];

                /// <summary>
                /// Column s_units_cell_nonzero_nom metadata
                /// </summary>
                public static readonly ColumnMetadata ColUnitsCellNonZeroNom = Cols[ResultSamplingUnitsJson.ElementKeyUnitsCellNonZeroNom];


                /// <summary>
                /// Column attribute_denom metadata
                /// </summary>
                public static readonly ColumnMetadata ColAttributeDenom = Cols[ResultSamplingUnitsJson.ElementKeyAttributeDenom];

                /// <summary>
                /// Column stratum_denom metadata
                /// </summary>
                public static readonly ColumnMetadata ColStratumDenom = Cols[ResultSamplingUnitsJson.ElementKeyStratumDenom];

                /// <summary>
                /// Column s_units_param_area_denom metadata
                /// </summary>
                public static readonly ColumnMetadata ColUnitsParamAreaDenom = Cols[ResultSamplingUnitsJson.ElementKeyUnitsParamAreaDenom];

                /// <summary>
                /// Column s_units_cell_denom metadata
                /// </summary>
                public static readonly ColumnMetadata ColUnitsCellDenom = Cols[ResultSamplingUnitsJson.ElementKeyUnitsCellDenom];

                /// <summary>
                /// Column s_units_cell_nonzero_denom metadata
                /// </summary>
                public static readonly ColumnMetadata ColUnitsCellNonZeroDenom = Cols[ResultSamplingUnitsJson.ElementKeyUnitsCellNonZeroDenom];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public ResultSamplingUnitList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public ResultSamplingUnitList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// List of sampling units (read-only)
                /// </summary>
                public List<ResultSamplingUnit> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new ResultSamplingUnit(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// Sampling unit from list by identifier (read-only)
                /// </summary>
                /// <param name="id">Sampling unit identifier</param>
                /// <returns>Sampling unit from list by identifier (null if not found)</returns>
                public ResultSamplingUnit this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new ResultSamplingUnit(composite: this, data: a))
                                .FirstOrDefault<ResultSamplingUnit>();
                    }
                }

                #endregion Indexer


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public ResultSamplingUnitList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new ResultSamplingUnitList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new ResultSamplingUnitList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                /// <summary>
                /// List of sampling units
                /// for selected final estimates
                /// </summary>
                /// <param name="resultId">Selected final estimates identifiers</param>
                /// <returns>
                /// List of sampling units
                /// for selected final estimates
                /// </returns>
                public ResultSamplingUnitList Reduce(
                    List<int> resultId = null)
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();

                    if ((resultId != null) && resultId.Count != 0)
                    {
                        rows = rows.Where(a => resultId.Contains<int>(
                                value: (int)(a.Field<Nullable<int>>(columnName: ColResultId.Name) ?? 0)));
                    }

                    return
                        rows.Any() ?
                        new ResultSamplingUnitList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new ResultSamplingUnitList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                /// <summary>
                /// Loads object data
                /// (if it was not done before)
                /// </summary>
                /// <param name="tResult">Database table t_result</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                public void Load(
                    ResultList tResult,
                    string condition = null,
                    Nullable<int> limit = null)
                {
                    if (!Loaded)
                    {
                        ReLoad(
                            tResult: tResult,
                            condition: condition,
                            limit: limit);
                    }
                }

                /// <summary>
                /// Loads object data
                /// </summary>
                /// <param name="tResult">Database table t_result</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                public void ReLoad(
                    ResultList tResult,
                    string condition = null,
                    Nullable<int> limit = null)
                {
                    Data = EmptyDataTable(
                                tableName: TableName,
                                columns: Columns);
                    Condition = condition;
                    Limit = limit;

                    System.Diagnostics.Stopwatch stopWatch = new();
                    stopWatch.Start();

                    int i = 0;
                    foreach (Result item in tResult.Items)
                    {
                        ResultSamplingUnitsJson json = new();
                        json.LoadFromText(text: item.SamplingUnits);

                        foreach (ResultSamplingUnitsJson.TResultSamplingUnit unit in json.ResultSamplingUnits)
                        {
                            DataRow row = Data.NewRow();

                            Functions.SetIntArg(row: row, name: ResultSamplingUnitList.ColId.Name, val: ++i);
                            Functions.SetIntArg(row: row, name: ResultSamplingUnitList.ColResultId.Name, val: item.Id);
                            Functions.SetBoolArg(row: row, name: ResultSamplingUnitList.ColJsonValidity.Name, val: unit.Validate());

                            Functions.SetNIntArg(row: row, name: ResultSamplingUnitList.ColAttribute.Name, val: unit.Attribute);
                            Functions.SetNIntArg(row: row, name: ResultSamplingUnitList.ColStratum.Name, val: unit.Stratum);
                            Functions.SetNIntArg(row: row, name: ResultSamplingUnitList.ColUnitsParamArea.Name, val: unit.UnitsParamArea);
                            Functions.SetNIntArg(row: row, name: ResultSamplingUnitList.ColUnitsCell.Name, val: unit.UnitsCell);
                            Functions.SetNIntArg(row: row, name: ResultSamplingUnitList.ColUnitsCellNonZero.Name, val: unit.UnitsCellNonZero);

                            Functions.SetNIntArg(row: row, name: ResultSamplingUnitList.ColAttributeNom.Name, val: unit.AttributeNom);
                            Functions.SetNIntArg(row: row, name: ResultSamplingUnitList.ColStratumNom.Name, val: unit.StratumNom);
                            Functions.SetNIntArg(row: row, name: ResultSamplingUnitList.ColUnitsParamAreaNom.Name, val: unit.UnitsParamAreaNom);
                            Functions.SetNIntArg(row: row, name: ResultSamplingUnitList.ColUnitsCellNom.Name, val: unit.UnitsCellNom);
                            Functions.SetNIntArg(row: row, name: ResultSamplingUnitList.ColUnitsCellNonZeroNom.Name, val: unit.UnitsCellNonZeroNom);

                            Functions.SetNIntArg(row: row, name: ResultSamplingUnitList.ColAttributeDenom.Name, val: unit.AttributeDenom);
                            Functions.SetNIntArg(row: row, name: ResultSamplingUnitList.ColStratumDenom.Name, val: unit.StratumDenom);
                            Functions.SetNIntArg(row: row, name: ResultSamplingUnitList.ColUnitsParamAreaDenom.Name, val: unit.UnitsParamAreaDenom);
                            Functions.SetNIntArg(row: row, name: ResultSamplingUnitList.ColUnitsCellDenom.Name, val: unit.UnitsCellDenom);
                            Functions.SetNIntArg(row: row, name: ResultSamplingUnitList.ColUnitsCellNonZeroDenom.Name, val: unit.UnitsCellNonZeroDenom);

                            Data.Rows.Add(row: row);
                        }
                    }

                    if (!String.IsNullOrEmpty(value: Condition))
                    {
                        DataView view = new(table: Data);
                        try
                        {
                            view.RowFilter = Condition;
                        }
                        catch
                        {
                            view.RowFilter = String.Empty;
                            Condition = String.Empty;
                        }
                        Data = view.ToTable();
                    }

                    stopWatch.Stop();
                    LoadingTime = 0.001 * stopWatch.ElapsedMilliseconds;
                    Data.TableName = TableName;
                }

                #endregion Methods

            }

        }
    }
}