﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {

            /// <summary>
            /// JSON file
            /// from table t_result
            /// and column sampling_units
            /// </summary>
            public class ResultSamplingUnitsJson
            {

                #region Constants

                /// <summary>
                /// Table name
                /// </summary>
                public const string TableName = "ResultSamplingUnitsJson";

                /// <summary>
                /// Identifier
                /// </summary>
                public const string ColId = "id";

                /// <summary>
                /// Result identifier
                /// </summary>
                public const string ColResultId = "result_id";

                /// <summary>
                /// Json validity
                /// </summary>
                public const string ColJsonValidity = "json_validity";

                /// <summary>
                /// root
                /// </summary>
                private const string ElementKeyRoot = "root";


                /// <summary>
                /// attribute_nom
                /// </summary>
                public const string ElementKeyAttribute = "attribute";

                /// <summary>
                /// stratum_nom
                /// </summary>
                public const string ElementKeyStratum = "stratum";

                /// <summary>
                /// s_units_param_area_nom
                /// </summary>
                public const string ElementKeyUnitsParamArea = "s_units_param_area";

                /// <summary>
                /// s_units_cell_nom
                /// </summary>
                public const string ElementKeyUnitsCell = "s_units_cell";

                /// <summary>
                /// s_units_cell_nonzero_nom
                /// </summary>
                public const string ElementKeyUnitsCellNonZero = "s_units_cell_nonzero";


                /// <summary>
                /// attribute_nom
                /// </summary>
                public const string ElementKeyAttributeNom = "attribute_nom";

                /// <summary>
                /// stratum_nom
                /// </summary>
                public const string ElementKeyStratumNom = "stratum_nom";

                /// <summary>
                /// s_units_param_area_nom
                /// </summary>
                public const string ElementKeyUnitsParamAreaNom = "s_units_param_area_nom";

                /// <summary>
                /// s_units_cell_nom
                /// </summary>
                public const string ElementKeyUnitsCellNom = "s_units_cell_nom";

                /// <summary>
                /// s_units_cell_nonzero_nom
                /// </summary>
                public const string ElementKeyUnitsCellNonZeroNom = "s_units_cell_nonzero_nom";


                /// <summary>
                /// attribute_denom
                /// </summary>
                public const string ElementKeyAttributeDenom = "attribute_denom";

                /// <summary>
                /// stratum_denom
                /// </summary>
                public const string ElementKeyStratumDenom = "stratum_denom";

                /// <summary>
                /// s_units_param_area_denom
                /// </summary>
                public const string ElementKeyUnitsParamAreaDenom = "s_units_param_area_denom";

                /// <summary>
                /// s_units_cell_denom
                /// </summary>
                public const string ElementKeyUnitsCellDenom = "s_units_cell_denom";

                /// <summary>
                /// s_units_cell_nonzero_denom
                /// </summary>
                public const string ElementKeyUnitsCellNonZeroDenom = "s_units_cell_nonzero_denom";

                #endregion Constants


                #region Private Fields

                /// <summary>
                /// Root JSON object
                /// </summary>
                private JObject root = null;

                /// <summary>
                /// JSON text
                /// </summary>
                private string text = String.Empty;

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                public ResultSamplingUnitsJson()
                {
                    root = null;
                    text = String.Empty;
                }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// List of JSON objects for sampling units (read-only)
                /// </summary>
                private List<JObject> JsonSamplingUnits
                {
                    get
                    {
                        return
                            Functions.GetJObjectList(
                                obj: root,
                                name: ElementKeyRoot,
                                valid: out bool _,
                                message: out string _);
                    }
                }

                /// <summary>
                /// List of sampling units (read-only)
                /// </summary>
                public List<TResultSamplingUnit> ResultSamplingUnits
                {
                    get
                    {
                        if (JsonSamplingUnits == null)
                        {
                            return null;
                        }

                        List<TResultSamplingUnit> result = [];

                        foreach (JObject obj in JsonSamplingUnits)
                        {
                            result.Add(
                                item: new TResultSamplingUnit(
                                    jsonSamplingUnit: obj));
                        }

                        return result;
                    }
                }

                /// <summary>
                /// Data table with data that was loaded from JSON file (read-only)
                /// </summary>
                public DataTable Data
                {
                    get
                    {
                        List<TResultSamplingUnit> units = ResultSamplingUnits;

                        DataTable result = EmptyDataTable();

                        int i = 0;

                        if (units != null)
                        {
                            foreach (TResultSamplingUnit unit in units)
                            {
                                DataRow row = result.NewRow();

                                if (unit != null)
                                {
                                    Functions.SetIntArg(row: row, name: ColId, val: ++i);
                                    Functions.SetBoolArg(row: row, name: ColJsonValidity, val: Validate());
                                    Functions.SetNIntArg(row: row, name: ElementKeyAttribute, val: unit.Attribute);
                                    Functions.SetNIntArg(row: row, name: ElementKeyStratum, val: unit.Stratum);
                                    Functions.SetNIntArg(row: row, name: ElementKeyUnitsParamArea, val: unit.UnitsParamArea);
                                    Functions.SetNIntArg(row: row, name: ElementKeyUnitsCell, val: unit.UnitsCell);
                                    Functions.SetNIntArg(row: row, name: ElementKeyUnitsCellNonZero, val: unit.UnitsCellNonZero);
                                    Functions.SetNIntArg(row: row, name: ElementKeyAttributeNom, val: unit.AttributeNom);
                                    Functions.SetNIntArg(row: row, name: ElementKeyStratumNom, val: unit.StratumNom);
                                    Functions.SetNIntArg(row: row, name: ElementKeyUnitsParamAreaNom, val: unit.UnitsParamAreaNom);
                                    Functions.SetNIntArg(row: row, name: ElementKeyUnitsCellNom, val: unit.UnitsCellNom);
                                    Functions.SetNIntArg(row: row, name: ElementKeyUnitsCellNonZeroNom, val: unit.UnitsCellNonZeroNom);
                                    Functions.SetNIntArg(row: row, name: ElementKeyAttributeDenom, val: unit.AttributeDenom);
                                    Functions.SetNIntArg(row: row, name: ElementKeyStratumDenom, val: unit.StratumDenom);
                                    Functions.SetNIntArg(row: row, name: ElementKeyUnitsParamAreaDenom, val: unit.UnitsParamAreaDenom);
                                    Functions.SetNIntArg(row: row, name: ElementKeyUnitsCellDenom, val: unit.UnitsCellDenom);
                                    Functions.SetNIntArg(row: row, name: ElementKeyUnitsCellNonZeroDenom, val: unit.UnitsCellNonZeroDenom);
                                }
                                else
                                {
                                    Functions.SetIntArg(row: row, name: ColId, val: ++i);
                                    Functions.SetBoolArg(row: row, name: ColJsonValidity, val: Validate());
                                    Functions.SetNIntArg(row: row, name: ElementKeyAttribute, val: null);
                                    Functions.SetNIntArg(row: row, name: ElementKeyStratum, val: null);
                                    Functions.SetNIntArg(row: row, name: ElementKeyUnitsParamArea, val: null);
                                    Functions.SetNIntArg(row: row, name: ElementKeyUnitsCell, val: null);
                                    Functions.SetNIntArg(row: row, name: ElementKeyUnitsCellNonZero, val: null);
                                    Functions.SetNIntArg(row: row, name: ElementKeyAttributeNom, val: null);
                                    Functions.SetNIntArg(row: row, name: ElementKeyStratumNom, val: null);
                                    Functions.SetNIntArg(row: row, name: ElementKeyUnitsParamAreaNom, val: null);
                                    Functions.SetNIntArg(row: row, name: ElementKeyUnitsCellNom, val: null);
                                    Functions.SetNIntArg(row: row, name: ElementKeyUnitsCellNonZeroNom, val: null);
                                    Functions.SetNIntArg(row: row, name: ElementKeyAttributeDenom, val: null);
                                    Functions.SetNIntArg(row: row, name: ElementKeyStratumDenom, val: null);
                                    Functions.SetNIntArg(row: row, name: ElementKeyUnitsParamAreaDenom, val: null);
                                    Functions.SetNIntArg(row: row, name: ElementKeyUnitsCellDenom, val: null);
                                    Functions.SetNIntArg(row: row, name: ElementKeyUnitsCellNonZeroDenom, val: null);
                                }

                                result.Rows.Add(row: row);
                            }
                        }
                        else
                        {
                            DataRow row = result.NewRow();

                            Functions.SetIntArg(row: row, name: ColId, val: ++i);
                            Functions.SetBoolArg(row: row, name: ColJsonValidity, val: false);
                            Functions.SetNIntArg(row: row, name: ElementKeyAttribute, val: null);
                            Functions.SetNIntArg(row: row, name: ElementKeyStratum, val: null);
                            Functions.SetNIntArg(row: row, name: ElementKeyUnitsParamArea, val: null);
                            Functions.SetNIntArg(row: row, name: ElementKeyUnitsCell, val: null);
                            Functions.SetNIntArg(row: row, name: ElementKeyUnitsCellNonZero, val: null);
                            Functions.SetNIntArg(row: row, name: ElementKeyAttributeNom, val: null);
                            Functions.SetNIntArg(row: row, name: ElementKeyStratumNom, val: null);
                            Functions.SetNIntArg(row: row, name: ElementKeyUnitsParamAreaNom, val: null);
                            Functions.SetNIntArg(row: row, name: ElementKeyUnitsCellNom, val: null);
                            Functions.SetNIntArg(row: row, name: ElementKeyUnitsCellNonZeroNom, val: null);
                            Functions.SetNIntArg(row: row, name: ElementKeyAttributeDenom, val: null);
                            Functions.SetNIntArg(row: row, name: ElementKeyStratumDenom, val: null);
                            Functions.SetNIntArg(row: row, name: ElementKeyUnitsParamAreaDenom, val: null);
                            Functions.SetNIntArg(row: row, name: ElementKeyUnitsCellDenom, val: null);
                            Functions.SetNIntArg(row: row, name: ElementKeyUnitsCellNonZeroDenom, val: null);

                            result.Rows.Add(row: row);
                        }

                        return result;
                    }
                }

                /// <summary>
                /// JSON text (read-only)
                /// </summary>
                public string Text
                {
                    get
                    {
                        return text;
                    }
                }

                #endregion Properties


                #region Methods

                /// <summary>
                /// Function loads object data from text variable that contains JSON file content
                /// </summary>
                /// <param name="text">Text variable that contains JSON file content</param>
                public void LoadFromText(string text)
                {
                    try
                    {
                        this.text = String.Concat("{ \"", ElementKeyRoot, "\": ", text, " }");
                        root = JObject.Parse(json: this.text);
                    }
                    catch
                    {
                        root = null;
                    }
                }

                /// <summary>
                /// Function loads object data from JSON file
                /// </summary>
                /// <param name="path">Path to JSON file</param>
                public void LoadFromFile(string path)
                {
                    try
                    {
                        text =
                            File.ReadAllText(path: path);
                        LoadFromText(text: text);
                    }
                    catch
                    {
                        root = null;
                    }
                }

                /// <summary>
                /// Function tests whether JSON file contains expected elements
                /// </summary>
                /// <returns>JSON file has expected format then true else false.</returns>
                public bool Validate()
                {
                    return
                        (root != null) &&
                        (ResultSamplingUnits != null) &&
                        (ResultSamplingUnits
                            .Select(a => a.Validate())
                            .Aggregate((a, b) => a && b));
                }

                /// <summary>
                /// Empty data table for data that will be loaded from JSON file
                /// </summary>
                /// <returns>Empty data table for data that will be loaded from JSON file</returns>
                public static DataTable EmptyDataTable()
                {
                    DataTable result = new()
                    {
                        TableName = TableName
                    };

                    result.Columns.Add(column: new DataColumn()
                    {
                        ColumnName = ColId,
                        DataType = Type.GetType(typeName: "System.Int32")
                    });

                    result.Columns.Add(column: new DataColumn()
                    {
                        ColumnName = ColJsonValidity,
                        DataType = Type.GetType(typeName: "System.Boolean")
                    });

                    result.Columns.Add(column: new DataColumn()
                    {
                        ColumnName = ElementKeyAttribute,
                        DataType = Type.GetType(typeName: "System.Int32")
                    });

                    result.Columns.Add(column: new DataColumn()
                    {
                        ColumnName = ElementKeyStratum,
                        DataType = Type.GetType(typeName: "System.Int32")
                    });

                    result.Columns.Add(column: new DataColumn()
                    {
                        ColumnName = ElementKeyUnitsParamArea,
                        DataType = Type.GetType(typeName: "System.Int32")
                    });

                    result.Columns.Add(column: new DataColumn()
                    {
                        ColumnName = ElementKeyUnitsCell,
                        DataType = Type.GetType(typeName: "System.Int32")
                    });

                    result.Columns.Add(column: new DataColumn()
                    {
                        ColumnName = ElementKeyUnitsCellNonZero,
                        DataType = Type.GetType(typeName: "System.Int32")
                    });

                    result.Columns.Add(column: new DataColumn()
                    {
                        ColumnName = ElementKeyAttributeNom,
                        DataType = Type.GetType(typeName: "System.Int32")
                    });

                    result.Columns.Add(column: new DataColumn()
                    {
                        ColumnName = ElementKeyStratumNom,
                        DataType = Type.GetType(typeName: "System.Int32")
                    });

                    result.Columns.Add(column: new DataColumn()
                    {
                        ColumnName = ElementKeyUnitsParamAreaNom,
                        DataType = Type.GetType(typeName: "System.Int32")
                    });

                    result.Columns.Add(column: new DataColumn()
                    {
                        ColumnName = ElementKeyUnitsCellNom,
                        DataType = Type.GetType(typeName: "System.Int32")
                    });

                    result.Columns.Add(column: new DataColumn()
                    {
                        ColumnName = ElementKeyUnitsCellNonZeroNom,
                        DataType = Type.GetType(typeName: "System.Int32")
                    });

                    result.Columns.Add(column: new DataColumn()
                    {
                        ColumnName = ElementKeyAttributeDenom,
                        DataType = Type.GetType(typeName: "System.Int32")
                    });

                    result.Columns.Add(column: new DataColumn()
                    {
                        ColumnName = ElementKeyStratumDenom,
                        DataType = Type.GetType(typeName: "System.Int32")
                    });

                    result.Columns.Add(column: new DataColumn()
                    {
                        ColumnName = ElementKeyUnitsParamAreaDenom,
                        DataType = Type.GetType(typeName: "System.Int32")
                    });

                    result.Columns.Add(column: new DataColumn()
                    {
                        ColumnName = ElementKeyUnitsCellDenom,
                        DataType = Type.GetType(typeName: "System.Int32")
                    });

                    result.Columns.Add(column: new DataColumn()
                    {
                        ColumnName = ElementKeyUnitsCellNonZeroDenom,
                        DataType = Type.GetType(typeName: "System.Int32")
                    });

                    return result;
                }

                #endregion Methods


                #region Internal Classes

                /// <summary>
                /// Sampling unit
                /// </summary>
                /// <remarks>
                /// Constructor
                /// </remarks>
                /// <param name="jsonSamplingUnit"> JSON object for sampling unit</param>
                public class TResultSamplingUnit(JObject jsonSamplingUnit)
                {

                    #region Private Fields

                    /// <summary>
                    /// JSON object for sampling unit
                    /// </summary>
                    private readonly JObject jsonSamplingUnit = jsonSamplingUnit;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Attribute
                    /// </summary>
                    public Nullable<int> Attribute
                    {
                        get
                        {
                            return Functions.GetJNInteger(
                                obj: jsonSamplingUnit,
                                name: ElementKeyAttribute,
                                valid: out bool _,
                                message: out string _);
                        }
                    }

                    /// <summary>
                    /// Stratum
                    /// </summary>
                    public Nullable<int> Stratum
                    {
                        get
                        {
                            return Functions.GetJNInteger(
                                obj: jsonSamplingUnit,
                                name: ElementKeyStratum,
                                valid: out bool _,
                                message: out string _);
                        }
                    }

                    /// <summary>
                    /// UnitsParamArea
                    /// </summary>
                    public Nullable<int> UnitsParamArea
                    {
                        get
                        {
                            return Functions.GetJNInteger(
                                obj: jsonSamplingUnit,
                                name: ElementKeyUnitsParamArea,
                                valid: out bool _,
                                message: out string _);
                        }
                    }

                    /// <summary>
                    /// UnitsCell
                    /// </summary>
                    public Nullable<int> UnitsCell
                    {
                        get
                        {
                            return Functions.GetJNInteger(
                                obj: jsonSamplingUnit,
                                name: ElementKeyUnitsCell,
                                valid: out bool _,
                                message: out string _);
                        }
                    }

                    /// <summary>
                    /// UnitsCellNonZero
                    /// </summary>
                    public Nullable<int> UnitsCellNonZero
                    {
                        get
                        {
                            return Functions.GetJNInteger(
                                obj: jsonSamplingUnit,
                                name: ElementKeyUnitsCellNonZero,
                                valid: out bool _,
                                message: out string _);
                        }
                    }

                    /// <summary>
                    /// AttributeNom
                    /// </summary>
                    public Nullable<int> AttributeNom
                    {
                        get
                        {
                            return Functions.GetJNInteger(
                                obj: jsonSamplingUnit,
                                name: ElementKeyAttributeNom,
                                valid: out bool _,
                                message: out string _);
                        }
                    }

                    /// <summary>
                    /// StratumNom
                    /// </summary>
                    public Nullable<int> StratumNom
                    {
                        get
                        {
                            return Functions.GetJNInteger(
                                obj: jsonSamplingUnit,
                                name: ElementKeyStratumNom,
                                valid: out bool _,
                                message: out string _);
                        }
                    }

                    /// <summary>
                    /// UnitsParamAreaNom
                    /// </summary>
                    public Nullable<int> UnitsParamAreaNom
                    {
                        get
                        {
                            return Functions.GetJNInteger(
                                obj: jsonSamplingUnit,
                                name: ElementKeyUnitsParamAreaNom,
                                valid: out bool _,
                                message: out string _);
                        }
                    }

                    /// <summary>
                    /// UnitsCellNom
                    /// </summary>
                    public Nullable<int> UnitsCellNom
                    {
                        get
                        {
                            return Functions.GetJNInteger(
                                obj: jsonSamplingUnit,
                                name: ElementKeyUnitsCellNom,
                                valid: out bool _,
                                message: out string _);
                        }
                    }

                    /// <summary>
                    /// UnitsCellNonZeroNom
                    /// </summary>
                    public Nullable<int> UnitsCellNonZeroNom
                    {
                        get
                        {
                            return Functions.GetJNInteger(
                                obj: jsonSamplingUnit,
                                name: ElementKeyUnitsCellNonZeroNom,
                                valid: out bool _,
                                message: out string _);
                        }
                    }

                    /// <summary>
                    /// AttributeDenom
                    /// </summary>
                    public Nullable<int> AttributeDenom
                    {
                        get
                        {
                            return Functions.GetJNInteger(
                                obj: jsonSamplingUnit,
                                name: ElementKeyAttributeDenom,
                                valid: out bool _,
                                message: out string _);
                        }
                    }

                    /// <summary>
                    /// StratumDenom
                    /// </summary>
                    public Nullable<int> StratumDenom
                    {
                        get
                        {
                            return Functions.GetJNInteger(
                                obj: jsonSamplingUnit,
                                name: ElementKeyStratumDenom,
                                valid: out bool _,
                                message: out string _);
                        }
                    }

                    /// <summary>
                    /// UnitsParamAreaDenom
                    /// </summary>
                    public Nullable<int> UnitsParamAreaDenom
                    {
                        get
                        {
                            return Functions.GetJNInteger(
                                obj: jsonSamplingUnit,
                                name: ElementKeyUnitsParamAreaDenom,
                                valid: out bool _,
                                message: out string _);
                        }
                    }

                    /// <summary>
                    /// UnitsCellDenom
                    /// </summary>
                    public Nullable<int> UnitsCellDenom
                    {
                        get
                        {
                            return Functions.GetJNInteger(
                                obj: jsonSamplingUnit,
                                name: ElementKeyUnitsCellDenom,
                                valid: out bool _,
                                message: out string _);
                        }
                    }

                    /// <summary>
                    /// UnitsCellNonZeroDenom
                    /// </summary>
                    public Nullable<int> UnitsCellNonZeroDenom
                    {
                        get
                        {
                            return Functions.GetJNInteger(
                                obj: jsonSamplingUnit,
                                name: ElementKeyUnitsCellNonZeroDenom,
                                valid: out bool _,
                                message: out string _);
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Tests whether sampling unit has valid format
                    /// </summary>
                    /// <returns>true/false</returns>
                    public bool Validate()
                    {
                        return
                            (jsonSamplingUnit != null) &&
                            ((
                            (AttributeNom != null) &&
                            (StratumNom != null) &&
                            (UnitsParamAreaNom != null) &&
                            (UnitsCellNom != null) &&
                            (UnitsCellNonZeroNom != null) &&
                            (AttributeDenom != null) &&
                            (StratumDenom != null) &&
                            (UnitsParamAreaDenom != null) &&
                            (UnitsCellDenom != null) &&
                            (UnitsCellNonZeroDenom != null)
                            )
                            ||
                            (
                            (Attribute != null) &&
                            (Stratum != null) &&
                            (UnitsParamArea != null) &&
                            (UnitsCell != null) &&
                            (UnitsCellNonZero != null)
                            ));
                    }

                    /// <summary>
                    /// Returns the string that represents current object
                    /// </summary>
                    /// <returns>Returns the string that represents current object</returns>
                    public override string ToString()
                    {
                        string strAttribute = (Attribute == null) ? Functions.StrNull : Attribute.ToString();
                        string strStratum = (Stratum == null) ? Functions.StrNull : Stratum.ToString();
                        string strUnitsParamArea = (UnitsParamArea == null) ? Functions.StrNull : UnitsParamArea.ToString();
                        string strUnitCell = (UnitsCell == null) ? Functions.StrNull : UnitsCell.ToString();
                        string strUnitsCellNonZero = (UnitsCellNonZero == null) ? Functions.StrNull : UnitsCellNonZero.ToString();
                        string strAttributeNom = (AttributeNom == null) ? Functions.StrNull : AttributeNom.ToString();
                        string strStratumNom = (StratumNom == null) ? Functions.StrNull : StratumNom.ToString();
                        string strUnitsParamAreaNom = (UnitsParamAreaNom == null) ? Functions.StrNull : UnitsParamAreaNom.ToString();
                        string strUnitsCellNom = (UnitsCellNom == null) ? Functions.StrNull : UnitsCellNom.ToString();
                        string strUnitsCellNonZeroNom = (UnitsCellNonZeroNom == null) ? Functions.StrNull : UnitsCellNonZeroNom.ToString();
                        string strAttributeDenom = (AttributeDenom == null) ? Functions.StrNull : AttributeDenom.ToString();
                        string strStratumDenom = (StratumDenom == null) ? Functions.StrNull : StratumDenom.ToString();
                        string strUnitsParamAreaDenom = (UnitsParamAreaDenom == null) ? Functions.StrNull : UnitsParamAreaDenom.ToString();
                        string strUnitsCellDenom = (UnitsCellDenom == null) ? Functions.StrNull : UnitsCellDenom.ToString();
                        string strUnitsCellNonZeroDenom = (UnitsCellNonZeroDenom == null) ? Functions.StrNull : UnitsCellNonZeroDenom.ToString();

                        return String.Concat(
                            $"Attribute : {strAttribute} {Environment.NewLine}",
                            $"Stratum: {strStratum} {Environment.NewLine}",
                            $"UnitsParamArea : {strUnitsParamArea} {Environment.NewLine}",
                            $"UnitsCell : {strUnitCell} {Environment.NewLine}",
                            $"UnitsCellNonZero : {strUnitsCellNonZero} {Environment.NewLine}",
                            $"AttributeNom : {strAttributeNom} {Environment.NewLine}",
                            $"StratumNom : {strStratumNom} {Environment.NewLine}",
                            $"UnitsParamAreaNom : {strUnitsParamAreaNom} {Environment.NewLine}",
                            $"UnitsCellNom : {strUnitsCellNom} {Environment.NewLine}",
                            $"UnitsCellNonZeroNom : {strUnitsCellNonZeroNom} {Environment.NewLine}",
                            $"AttributeDenom : {strAttributeDenom} {Environment.NewLine}",
                            $"StratumDenom : {strStratumDenom} {Environment.NewLine}",
                            $"UnitsParamAreaDenom : {strUnitsParamAreaDenom} {Environment.NewLine}",
                            $"UnitsCellDenom : {strUnitsCellDenom} {Environment.NewLine}",
                            $"UnitsCellNonZeroDenom : {strUnitsCellNonZeroDenom} {Environment.NewLine}");
                    }

                    /// <summary>
                    /// Determines whether specified object is equal to the current object
                    /// </summary>
                    /// <param name="obj">Speciefied object</param>
                    /// <returns>true/false</returns>
                    public override bool Equals(object obj)
                    {
                        // If the passed object is null, return False
                        if (obj == null)
                        {
                            return false;
                        }

                        // If the passed object is not TResultSamplingUnit Type, return False
                        if (obj is not TResultSamplingUnit)
                        {
                            return false;
                        }

                        return
                            (Attribute ?? 0) == (((TResultSamplingUnit)obj).Attribute ?? 0) &&
                            (Stratum ?? 0) == (((TResultSamplingUnit)obj).Stratum ?? 0) &&
                            (UnitsParamArea ?? 0) == (((TResultSamplingUnit)obj).UnitsParamArea ?? 0) &&
                            (UnitsCell ?? 0) == (((TResultSamplingUnit)obj).UnitsCell ?? 0) &&
                            (UnitsCellNonZero ?? 0) == (((TResultSamplingUnit)obj).UnitsCellNonZero ?? 0) &&
                            (AttributeNom ?? 0) == (((TResultSamplingUnit)obj).AttributeNom ?? 0) &&
                            (StratumNom ?? 0) == (((TResultSamplingUnit)obj).StratumNom ?? 0) &&
                            (UnitsParamAreaNom ?? 0) == (((TResultSamplingUnit)obj).UnitsParamAreaNom ?? 0) &&
                            (UnitsCellNom ?? 0) == (((TResultSamplingUnit)obj).UnitsCellNom ?? 0) &&
                            (UnitsCellNonZeroNom ?? 0) == (((TResultSamplingUnit)obj).UnitsCellNonZeroNom ?? 0) &&
                            (AttributeDenom ?? 0) == (((TResultSamplingUnit)obj).AttributeDenom ?? 0) &&
                            (StratumDenom ?? 0) == (((TResultSamplingUnit)obj).StratumDenom ?? 0) &&
                            (UnitsParamAreaDenom ?? 0) == (((TResultSamplingUnit)obj).UnitsParamAreaDenom ?? 0) &&
                            (UnitsCellDenom ?? 0) == (((TResultSamplingUnit)obj).UnitsCellDenom ?? 0) &&
                            (UnitsCellNonZeroDenom ?? 0) == (((TResultSamplingUnit)obj).UnitsCellNonZeroDenom ?? 0);
                    }

                    /// <summary>
                    /// Returns the hash code
                    /// </summary>
                    /// <returns>Hash code</returns>
                    public override int GetHashCode()
                    {
                        return
                            (Attribute ?? 0).GetHashCode() ^
                            (Stratum ?? 0).GetHashCode() ^
                            (UnitsParamArea ?? 0).GetHashCode() ^
                            (UnitsCell ?? 0).GetHashCode() ^
                            (UnitsCellNonZero ?? 0).GetHashCode() ^
                            (AttributeNom ?? 0).GetHashCode() ^
                            (StratumNom ?? 0).GetHashCode() ^
                            (UnitsParamAreaNom ?? 0).GetHashCode() ^
                            (UnitsCellNom ?? 0).GetHashCode() ^
                            (UnitsCellNonZeroNom ?? 0).GetHashCode() ^
                            (AttributeDenom ?? 0).GetHashCode() ^
                            (StratumDenom ?? 0).GetHashCode() ^
                            (UnitsParamAreaDenom ?? 0).GetHashCode() ^
                            (UnitsCellDenom ?? 0).GetHashCode() ^
                            (UnitsCellNonZeroDenom ?? 0).GetHashCode();
                    }

                    #endregion Methods

                }

                #endregion Internal Classes

            }

        }
    }
}
