﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi.NfiEstaPg.Core
{
    partial class FormNfiEsta
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            msMain = new System.Windows.Forms.MenuStrip();
            tsmiLookupTables = new System.Windows.Forms.ToolStripMenuItem();
            tsmiCAreaDomain = new System.Windows.Forms.ToolStripMenuItem();
            tsmiCAreaDomainCategory = new System.Windows.Forms.ToolStripMenuItem();
            tsmiCAuxiliaryVariable = new System.Windows.Forms.ToolStripMenuItem();
            tsmiCAuxiliaryVariableCategory = new System.Windows.Forms.ToolStripMenuItem();
            tsmiCEstimateConfStatus = new System.Windows.Forms.ToolStripMenuItem();
            tsmiCEstimateType = new System.Windows.Forms.ToolStripMenuItem();
            tsmiCEstimationCell = new System.Windows.Forms.ToolStripMenuItem();
            tsmiCEstimationCellCollection = new System.Windows.Forms.ToolStripMenuItem();
            tsmiCEstimationPeriod = new System.Windows.Forms.ToolStripMenuItem();
            tsmiCPanelRefYearSetGroup = new System.Windows.Forms.ToolStripMenuItem();
            tsmiCParamAreaType = new System.Windows.Forms.ToolStripMenuItem();
            tsmiCPhaseEstimateType = new System.Windows.Forms.ToolStripMenuItem();
            tsmiCSubPopulation = new System.Windows.Forms.ToolStripMenuItem();
            tsmiCSubPopulationCategory = new System.Windows.Forms.ToolStripMenuItem();
            tsmiCTargetVariable = new System.Windows.Forms.ToolStripMenuItem();
            tsmiCTopic = new System.Windows.Forms.ToolStripMenuItem();
            tsmiCLanguage = new System.Windows.Forms.ToolStripMenuItem();
            tsmiMappingTables = new System.Windows.Forms.ToolStripMenuItem();
            tsmiCmAreaDomainCategoryMapping = new System.Windows.Forms.ToolStripMenuItem();
            tsmiCmEstimationCellToParamAreaMapping = new System.Windows.Forms.ToolStripMenuItem();
            tsmiCmPlotToEstimationCellMapping = new System.Windows.Forms.ToolStripMenuItem();
            tsmiCmPlotToParametrizationAreaMapping = new System.Windows.Forms.ToolStripMenuItem();
            tsmiCmSubPopulationCategoryMapping = new System.Windows.Forms.ToolStripMenuItem();
            tsmiCmTargetVariableToTopic = new System.Windows.Forms.ToolStripMenuItem();
            tsmiSpatialTables = new System.Windows.Forms.ToolStripMenuItem();
            tsmiFaCell = new System.Windows.Forms.ToolStripMenuItem();
            tsmiFaParamArea = new System.Windows.Forms.ToolStripMenuItem();
            tsmiTStratumInEstimationCell = new System.Windows.Forms.ToolStripMenuItem();
            tsmiDataTables = new System.Windows.Forms.ToolStripMenuItem();
            tsmiTAdditivitySetPlot = new System.Windows.Forms.ToolStripMenuItem();
            tsmiTAuxConf = new System.Windows.Forms.ToolStripMenuItem();
            tsmiTAuxTotal = new System.Windows.Forms.ToolStripMenuItem();
            tsmiTAuxiliaryData = new System.Windows.Forms.ToolStripMenuItem();
            tsmiTAvailableDataSets = new System.Windows.Forms.ToolStripMenuItem();
            tsmiTEstimateConf = new System.Windows.Forms.ToolStripMenuItem();
            tsmiTEstimationCellHierarchy = new System.Windows.Forms.ToolStripMenuItem();
            tsmiTGBeta = new System.Windows.Forms.ToolStripMenuItem();
            tsmiTModel = new System.Windows.Forms.ToolStripMenuItem();
            tsmiTModelVariables = new System.Windows.Forms.ToolStripMenuItem();
            tsmiTPanelToTotalEstimateConfFirstPhase = new System.Windows.Forms.ToolStripMenuItem();
            tsmiTPanelRefYearSetGroup = new System.Windows.Forms.ToolStripMenuItem();
            tsmiTResult = new System.Windows.Forms.ToolStripMenuItem();
            tsmiTTargetData = new System.Windows.Forms.ToolStripMenuItem();
            tsmiTTotalEstimateConf = new System.Windows.Forms.ToolStripMenuItem();
            tsmiTVariable = new System.Windows.Forms.ToolStripMenuItem();
            tsmiTVariableHierarchy = new System.Windows.Forms.ToolStripMenuItem();
            tsmiJSON = new System.Windows.Forms.ToolStripMenuItem();
            tsmiResultSamplingUnits = new System.Windows.Forms.ToolStripMenuItem();
            tsmiTargetVariableMetadata = new System.Windows.Forms.ToolStripMenuItem();
            tsmiTargetVariableMetadataAggregated = new System.Windows.Forms.ToolStripMenuItem();
            tsmiFnEtlCheckVariablesJson = new System.Windows.Forms.ToolStripMenuItem();
            tsmiStoredProcedures = new System.Windows.Forms.ToolStripMenuItem();
            tsmiFnAddResRatioAttr = new System.Windows.Forms.ToolStripMenuItem();
            tsmiFnAddResTotalAttr = new System.Windows.Forms.ToolStripMenuItem();
            tsmiFnAddResTotalGeo = new System.Windows.Forms.ToolStripMenuItem();
            tsmiFnEtlGetTargetVariable = new System.Windows.Forms.ToolStripMenuItem();
            tsmiFnEtlGetTargetVariableMetadata = new System.Windows.Forms.ToolStripMenuItem();
            tsmiFnEtlGetTargetVariableMetadataAggregated = new System.Windows.Forms.ToolStripMenuItem();
            tsmiControlTargetVariableSelector = new System.Windows.Forms.ToolStripMenuItem();
            tsmiFnPanelInEstimationCell = new System.Windows.Forms.ToolStripMenuItem();
            tsmiFnApiBeforeDeletePanelRefYearSetGroup = new System.Windows.Forms.ToolStripMenuItem();
            tsmiFnApiGetListOfPanelRefYearSetGroups = new System.Windows.Forms.ToolStripMenuItem();
            tsmiFnApiGetPanelRefYearSetCombinationsForGroups = new System.Windows.Forms.ToolStripMenuItem();
            tsmiLINQ = new System.Windows.Forms.ToolStripMenuItem();
            tsmiOLAPTotalEstimateDimension = new System.Windows.Forms.ToolStripMenuItem();
            tsmiOLAPTotalEstimateValue = new System.Windows.Forms.ToolStripMenuItem();
            tsmiOLAPTotalEstimate = new System.Windows.Forms.ToolStripMenuItem();
            tsmiOLAPRatioEstimate = new System.Windows.Forms.ToolStripMenuItem();
            tsmiOLAPRatioEstimateDimension = new System.Windows.Forms.ToolStripMenuItem();
            tsmiVwPanelRefYearSetPair = new System.Windows.Forms.ToolStripMenuItem();
            tsmiVwVariable = new System.Windows.Forms.ToolStripMenuItem();
            tsmiVwEstimationCellsCount = new System.Windows.Forms.ToolStripMenuItem();
            tsmiETL = new System.Windows.Forms.ToolStripMenuItem();
            tsmiExtractData = new System.Windows.Forms.ToolStripMenuItem();
            tsmiGetDifferences = new System.Windows.Forms.ToolStripMenuItem();
            tlpMain = new System.Windows.Forms.TableLayoutPanel();
            tlpButtons = new System.Windows.Forms.TableLayoutPanel();
            pnlClose = new System.Windows.Forms.Panel();
            btnClose = new System.Windows.Forms.Button();
            grpMain = new System.Windows.Forms.GroupBox();
            splitMain = new System.Windows.Forms.SplitContainer();
            pnlData = new System.Windows.Forms.Panel();
            txtSQL = new System.Windows.Forms.RichTextBox();
            msMain.SuspendLayout();
            tlpMain.SuspendLayout();
            tlpButtons.SuspendLayout();
            pnlClose.SuspendLayout();
            grpMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)splitMain).BeginInit();
            splitMain.Panel1.SuspendLayout();
            splitMain.Panel2.SuspendLayout();
            splitMain.SuspendLayout();
            SuspendLayout();
            // 
            // msMain
            // 
            msMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { tsmiLookupTables, tsmiMappingTables, tsmiSpatialTables, tsmiDataTables, tsmiJSON, tsmiStoredProcedures, tsmiLINQ, tsmiETL });
            msMain.Location = new System.Drawing.Point(0, 0);
            msMain.Name = "msMain";
            msMain.Padding = new System.Windows.Forms.Padding(7, 2, 0, 2);
            msMain.Size = new System.Drawing.Size(1101, 24);
            msMain.TabIndex = 0;
            msMain.Text = "menuStrip1";
            // 
            // tsmiLookupTables
            // 
            tsmiLookupTables.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] { tsmiCAreaDomain, tsmiCAreaDomainCategory, tsmiCAuxiliaryVariable, tsmiCAuxiliaryVariableCategory, tsmiCEstimateConfStatus, tsmiCEstimateType, tsmiCEstimationCell, tsmiCEstimationCellCollection, tsmiCEstimationPeriod, tsmiCPanelRefYearSetGroup, tsmiCParamAreaType, tsmiCPhaseEstimateType, tsmiCSubPopulation, tsmiCSubPopulationCategory, tsmiCTargetVariable, tsmiCTopic, tsmiCLanguage });
            tsmiLookupTables.Name = "tsmiLookupTables";
            tsmiLookupTables.Size = new System.Drawing.Size(114, 20);
            tsmiLookupTables.Text = "tsmiLookupTables";
            // 
            // tsmiCAreaDomain
            // 
            tsmiCAreaDomain.Name = "tsmiCAreaDomain";
            tsmiCAreaDomain.Size = new System.Drawing.Size(240, 22);
            tsmiCAreaDomain.Text = "tsmiCAreaDomain";
            // 
            // tsmiCAreaDomainCategory
            // 
            tsmiCAreaDomainCategory.Name = "tsmiCAreaDomainCategory";
            tsmiCAreaDomainCategory.Size = new System.Drawing.Size(240, 22);
            tsmiCAreaDomainCategory.Text = "tsmiCAreaDomainCategory";
            // 
            // tsmiCAuxiliaryVariable
            // 
            tsmiCAuxiliaryVariable.Name = "tsmiCAuxiliaryVariable";
            tsmiCAuxiliaryVariable.Size = new System.Drawing.Size(240, 22);
            tsmiCAuxiliaryVariable.Text = "tsmiCAuxiliaryVariable";
            // 
            // tsmiCAuxiliaryVariableCategory
            // 
            tsmiCAuxiliaryVariableCategory.Name = "tsmiCAuxiliaryVariableCategory";
            tsmiCAuxiliaryVariableCategory.Size = new System.Drawing.Size(240, 22);
            tsmiCAuxiliaryVariableCategory.Text = "tsmiCAuxiliaryVariableCategory";
            // 
            // tsmiCEstimateConfStatus
            // 
            tsmiCEstimateConfStatus.Name = "tsmiCEstimateConfStatus";
            tsmiCEstimateConfStatus.Size = new System.Drawing.Size(240, 22);
            tsmiCEstimateConfStatus.Text = "tsmiCEstimateConfStatus";
            // 
            // tsmiCEstimateType
            // 
            tsmiCEstimateType.Name = "tsmiCEstimateType";
            tsmiCEstimateType.Size = new System.Drawing.Size(240, 22);
            tsmiCEstimateType.Text = "tsmiCEstimateType";
            // 
            // tsmiCEstimationCell
            // 
            tsmiCEstimationCell.Name = "tsmiCEstimationCell";
            tsmiCEstimationCell.Size = new System.Drawing.Size(240, 22);
            tsmiCEstimationCell.Text = "tsmiCEstimationCell";
            // 
            // tsmiCEstimationCellCollection
            // 
            tsmiCEstimationCellCollection.Name = "tsmiCEstimationCellCollection";
            tsmiCEstimationCellCollection.Size = new System.Drawing.Size(240, 22);
            tsmiCEstimationCellCollection.Text = "tsmiCEstimationCellCollection";
            // 
            // tsmiCEstimationPeriod
            // 
            tsmiCEstimationPeriod.Name = "tsmiCEstimationPeriod";
            tsmiCEstimationPeriod.Size = new System.Drawing.Size(240, 22);
            tsmiCEstimationPeriod.Text = "tsmiCEstimationPeriod";
            // 
            // tsmiCPanelRefYearSetGroup
            // 
            tsmiCPanelRefYearSetGroup.Name = "tsmiCPanelRefYearSetGroup";
            tsmiCPanelRefYearSetGroup.Size = new System.Drawing.Size(240, 22);
            tsmiCPanelRefYearSetGroup.Text = "tsmiCPanelRefYearSetGroup";
            // 
            // tsmiCParamAreaType
            // 
            tsmiCParamAreaType.Name = "tsmiCParamAreaType";
            tsmiCParamAreaType.Size = new System.Drawing.Size(240, 22);
            tsmiCParamAreaType.Text = "tsmiCParamAreaType";
            // 
            // tsmiCPhaseEstimateType
            // 
            tsmiCPhaseEstimateType.Name = "tsmiCPhaseEstimateType";
            tsmiCPhaseEstimateType.Size = new System.Drawing.Size(240, 22);
            tsmiCPhaseEstimateType.Text = "tsmiCPhaseEstimateType";
            // 
            // tsmiCSubPopulation
            // 
            tsmiCSubPopulation.Name = "tsmiCSubPopulation";
            tsmiCSubPopulation.Size = new System.Drawing.Size(240, 22);
            tsmiCSubPopulation.Text = "tsmiCSubPopulation";
            // 
            // tsmiCSubPopulationCategory
            // 
            tsmiCSubPopulationCategory.Name = "tsmiCSubPopulationCategory";
            tsmiCSubPopulationCategory.Size = new System.Drawing.Size(240, 22);
            tsmiCSubPopulationCategory.Text = "tsmiCSubPopulationCategory";
            // 
            // tsmiCTargetVariable
            // 
            tsmiCTargetVariable.Name = "tsmiCTargetVariable";
            tsmiCTargetVariable.Size = new System.Drawing.Size(240, 22);
            tsmiCTargetVariable.Text = "tsmiCTargetVariable";
            // 
            // tsmiCTopic
            // 
            tsmiCTopic.Name = "tsmiCTopic";
            tsmiCTopic.Size = new System.Drawing.Size(240, 22);
            tsmiCTopic.Text = "tsmiCTopic";
            // 
            // tsmiCLanguage
            // 
            tsmiCLanguage.Name = "tsmiCLanguage";
            tsmiCLanguage.Size = new System.Drawing.Size(240, 22);
            tsmiCLanguage.Text = "tsmiCLanguage";
            // 
            // tsmiMappingTables
            // 
            tsmiMappingTables.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] { tsmiCmAreaDomainCategoryMapping, tsmiCmEstimationCellToParamAreaMapping, tsmiCmPlotToEstimationCellMapping, tsmiCmPlotToParametrizationAreaMapping, tsmiCmSubPopulationCategoryMapping, tsmiCmTargetVariableToTopic });
            tsmiMappingTables.Name = "tsmiMappingTables";
            tsmiMappingTables.Size = new System.Drawing.Size(122, 20);
            tsmiMappingTables.Text = "tsmiMappingTables";
            // 
            // tsmiCmAreaDomainCategoryMapping
            // 
            tsmiCmAreaDomainCategoryMapping.Name = "tsmiCmAreaDomainCategoryMapping";
            tsmiCmAreaDomainCategoryMapping.Size = new System.Drawing.Size(310, 22);
            tsmiCmAreaDomainCategoryMapping.Text = "tsmiCmAreaDomainCategoryMapping";
            // 
            // tsmiCmEstimationCellToParamAreaMapping
            // 
            tsmiCmEstimationCellToParamAreaMapping.Name = "tsmiCmEstimationCellToParamAreaMapping";
            tsmiCmEstimationCellToParamAreaMapping.Size = new System.Drawing.Size(310, 22);
            tsmiCmEstimationCellToParamAreaMapping.Text = "tsmiCmEstimationCellToParamAreaMapping";
            // 
            // tsmiCmPlotToEstimationCellMapping
            // 
            tsmiCmPlotToEstimationCellMapping.Name = "tsmiCmPlotToEstimationCellMapping";
            tsmiCmPlotToEstimationCellMapping.Size = new System.Drawing.Size(310, 22);
            tsmiCmPlotToEstimationCellMapping.Text = "tsmiCmPlotToEstimationCellMapping";
            // 
            // tsmiCmPlotToParametrizationAreaMapping
            // 
            tsmiCmPlotToParametrizationAreaMapping.Name = "tsmiCmPlotToParametrizationAreaMapping";
            tsmiCmPlotToParametrizationAreaMapping.Size = new System.Drawing.Size(310, 22);
            tsmiCmPlotToParametrizationAreaMapping.Text = "tsmiCmPlotToParametrizationAreaMapping";
            // 
            // tsmiCmSubPopulationCategoryMapping
            // 
            tsmiCmSubPopulationCategoryMapping.Name = "tsmiCmSubPopulationCategoryMapping";
            tsmiCmSubPopulationCategoryMapping.Size = new System.Drawing.Size(310, 22);
            tsmiCmSubPopulationCategoryMapping.Text = "tsmiCmSubPopulationCategoryMapping";
            // 
            // tsmiCmTargetVariableToTopic
            // 
            tsmiCmTargetVariableToTopic.Name = "tsmiCmTargetVariableToTopic";
            tsmiCmTargetVariableToTopic.Size = new System.Drawing.Size(310, 22);
            tsmiCmTargetVariableToTopic.Text = "tsmiCmTargetVariableToTopic";
            // 
            // tsmiSpatialTables
            // 
            tsmiSpatialTables.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] { tsmiFaCell, tsmiFaParamArea, tsmiTStratumInEstimationCell });
            tsmiSpatialTables.Name = "tsmiSpatialTables";
            tsmiSpatialTables.Size = new System.Drawing.Size(109, 20);
            tsmiSpatialTables.Text = "tsmiSpatialTables";
            // 
            // tsmiFaCell
            // 
            tsmiFaCell.Name = "tsmiFaCell";
            tsmiFaCell.Size = new System.Drawing.Size(231, 22);
            tsmiFaCell.Text = "tsmiFaCell";
            // 
            // tsmiFaParamArea
            // 
            tsmiFaParamArea.Name = "tsmiFaParamArea";
            tsmiFaParamArea.RightToLeft = System.Windows.Forms.RightToLeft.No;
            tsmiFaParamArea.Size = new System.Drawing.Size(231, 22);
            tsmiFaParamArea.Text = "tsmiFaParamArea";
            // 
            // tsmiTStratumInEstimationCell
            // 
            tsmiTStratumInEstimationCell.Name = "tsmiTStratumInEstimationCell";
            tsmiTStratumInEstimationCell.Size = new System.Drawing.Size(231, 22);
            tsmiTStratumInEstimationCell.Text = "tsmiTStratumInEstimationCell";
            // 
            // tsmiDataTables
            // 
            tsmiDataTables.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] { tsmiTAdditivitySetPlot, tsmiTAuxConf, tsmiTAuxTotal, tsmiTAuxiliaryData, tsmiTAvailableDataSets, tsmiTEstimateConf, tsmiTEstimationCellHierarchy, tsmiTGBeta, tsmiTModel, tsmiTModelVariables, tsmiTPanelToTotalEstimateConfFirstPhase, tsmiTPanelRefYearSetGroup, tsmiTResult, tsmiTTargetData, tsmiTTotalEstimateConf, tsmiTVariable, tsmiTVariableHierarchy });
            tsmiDataTables.Name = "tsmiDataTables";
            tsmiDataTables.Size = new System.Drawing.Size(98, 20);
            tsmiDataTables.Text = "tsmiDataTables";
            // 
            // tsmiTAdditivitySetPlot
            // 
            tsmiTAdditivitySetPlot.Name = "tsmiTAdditivitySetPlot";
            tsmiTAdditivitySetPlot.Size = new System.Drawing.Size(293, 22);
            tsmiTAdditivitySetPlot.Text = "tsmiTAdditivitySetPlot";
            // 
            // tsmiTAuxConf
            // 
            tsmiTAuxConf.Name = "tsmiTAuxConf";
            tsmiTAuxConf.Size = new System.Drawing.Size(293, 22);
            tsmiTAuxConf.Text = "tsmiTAuxConf";
            // 
            // tsmiTAuxTotal
            // 
            tsmiTAuxTotal.Name = "tsmiTAuxTotal";
            tsmiTAuxTotal.Size = new System.Drawing.Size(293, 22);
            tsmiTAuxTotal.Text = "tsmiTAuxTotal";
            // 
            // tsmiTAuxiliaryData
            // 
            tsmiTAuxiliaryData.Name = "tsmiTAuxiliaryData";
            tsmiTAuxiliaryData.Size = new System.Drawing.Size(293, 22);
            tsmiTAuxiliaryData.Text = "tsmiTAuxiliaryData";
            // 
            // tsmiTAvailableDataSets
            // 
            tsmiTAvailableDataSets.Name = "tsmiTAvailableDataSets";
            tsmiTAvailableDataSets.Size = new System.Drawing.Size(293, 22);
            tsmiTAvailableDataSets.Text = "tsmiTAvailableDataSets";
            // 
            // tsmiTEstimateConf
            // 
            tsmiTEstimateConf.Name = "tsmiTEstimateConf";
            tsmiTEstimateConf.Size = new System.Drawing.Size(293, 22);
            tsmiTEstimateConf.Text = "tsmiTEstimateConf";
            // 
            // tsmiTEstimationCellHierarchy
            // 
            tsmiTEstimationCellHierarchy.Name = "tsmiTEstimationCellHierarchy";
            tsmiTEstimationCellHierarchy.Size = new System.Drawing.Size(293, 22);
            tsmiTEstimationCellHierarchy.Text = "tsmiTEstimationCellHierarchy";
            // 
            // tsmiTGBeta
            // 
            tsmiTGBeta.Name = "tsmiTGBeta";
            tsmiTGBeta.Size = new System.Drawing.Size(293, 22);
            tsmiTGBeta.Text = "tsmiTGBeta";
            // 
            // tsmiTModel
            // 
            tsmiTModel.Name = "tsmiTModel";
            tsmiTModel.Size = new System.Drawing.Size(293, 22);
            tsmiTModel.Text = "tsmiTModel";
            // 
            // tsmiTModelVariables
            // 
            tsmiTModelVariables.Name = "tsmiTModelVariables";
            tsmiTModelVariables.Size = new System.Drawing.Size(293, 22);
            tsmiTModelVariables.Text = "tsmiTModelVariables";
            // 
            // tsmiTPanelToTotalEstimateConfFirstPhase
            // 
            tsmiTPanelToTotalEstimateConfFirstPhase.Name = "tsmiTPanelToTotalEstimateConfFirstPhase";
            tsmiTPanelToTotalEstimateConfFirstPhase.Size = new System.Drawing.Size(293, 22);
            tsmiTPanelToTotalEstimateConfFirstPhase.Text = "tsmiTPanelToTotalEstimateConfFirstPhase";
            // 
            // tsmiTPanelRefYearSetGroup
            // 
            tsmiTPanelRefYearSetGroup.Name = "tsmiTPanelRefYearSetGroup";
            tsmiTPanelRefYearSetGroup.Size = new System.Drawing.Size(293, 22);
            tsmiTPanelRefYearSetGroup.Text = "tsmiTPanelRefYearSetGroup";
            // 
            // tsmiTResult
            // 
            tsmiTResult.Name = "tsmiTResult";
            tsmiTResult.Size = new System.Drawing.Size(293, 22);
            tsmiTResult.Text = "tsmiTResult";
            // 
            // tsmiTTargetData
            // 
            tsmiTTargetData.Name = "tsmiTTargetData";
            tsmiTTargetData.Size = new System.Drawing.Size(293, 22);
            tsmiTTargetData.Text = "tsmiTTargetData";
            // 
            // tsmiTTotalEstimateConf
            // 
            tsmiTTotalEstimateConf.Name = "tsmiTTotalEstimateConf";
            tsmiTTotalEstimateConf.Size = new System.Drawing.Size(293, 22);
            tsmiTTotalEstimateConf.Text = "tsmiTTotalEstimateConf";
            // 
            // tsmiTVariable
            // 
            tsmiTVariable.Name = "tsmiTVariable";
            tsmiTVariable.Size = new System.Drawing.Size(293, 22);
            tsmiTVariable.Text = "tsmiTVariable";
            // 
            // tsmiTVariableHierarchy
            // 
            tsmiTVariableHierarchy.Name = "tsmiTVariableHierarchy";
            tsmiTVariableHierarchy.Size = new System.Drawing.Size(293, 22);
            tsmiTVariableHierarchy.Text = "tsmiTVariableHierarchy";
            // 
            // tsmiJSON
            // 
            tsmiJSON.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] { tsmiResultSamplingUnits, tsmiTargetVariableMetadata, tsmiTargetVariableMetadataAggregated, tsmiFnEtlCheckVariablesJson });
            tsmiJSON.Name = "tsmiJSON";
            tsmiJSON.Size = new System.Drawing.Size(70, 20);
            tsmiJSON.Text = "tsmiJSON";
            // 
            // tsmiResultSamplingUnits
            // 
            tsmiResultSamplingUnits.Name = "tsmiResultSamplingUnits";
            tsmiResultSamplingUnits.Size = new System.Drawing.Size(282, 22);
            tsmiResultSamplingUnits.Text = "tsmiResultSamplingUnits";
            tsmiResultSamplingUnits.Click += ItemResultSamplingUnits_Click;
            // 
            // tsmiTargetVariableMetadata
            // 
            tsmiTargetVariableMetadata.Name = "tsmiTargetVariableMetadata";
            tsmiTargetVariableMetadata.Size = new System.Drawing.Size(282, 22);
            tsmiTargetVariableMetadata.Text = "tsmiTargetVariableMetadata";
            tsmiTargetVariableMetadata.Click += ItemTargetVariableMetadata_Click;
            // 
            // tsmiTargetVariableMetadataAggregated
            // 
            tsmiTargetVariableMetadataAggregated.Name = "tsmiTargetVariableMetadataAggregated";
            tsmiTargetVariableMetadataAggregated.Size = new System.Drawing.Size(282, 22);
            tsmiTargetVariableMetadataAggregated.Text = "tsmiTargetVariableMetadataAggregated";
            tsmiTargetVariableMetadataAggregated.Click += ItemTargetVariableMetadataAggregated_Click;
            // 
            // tsmiFnEtlCheckVariablesJson
            // 
            tsmiFnEtlCheckVariablesJson.Name = "tsmiFnEtlCheckVariablesJson";
            tsmiFnEtlCheckVariablesJson.Size = new System.Drawing.Size(282, 22);
            tsmiFnEtlCheckVariablesJson.Text = "tsmiFnEtlCheckVariablesJson";
            tsmiFnEtlCheckVariablesJson.Click += ItemFnEtlCheckVariablesJson_Click;
            // 
            // tsmiStoredProcedures
            // 
            tsmiStoredProcedures.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] { tsmiFnAddResRatioAttr, tsmiFnAddResTotalAttr, tsmiFnAddResTotalGeo, tsmiFnEtlGetTargetVariable, tsmiFnEtlGetTargetVariableMetadata, tsmiFnEtlGetTargetVariableMetadataAggregated, tsmiControlTargetVariableSelector, tsmiFnPanelInEstimationCell, tsmiFnApiBeforeDeletePanelRefYearSetGroup, tsmiFnApiGetListOfPanelRefYearSetGroups, tsmiFnApiGetPanelRefYearSetCombinationsForGroups });
            tsmiStoredProcedures.Name = "tsmiStoredProcedures";
            tsmiStoredProcedures.Size = new System.Drawing.Size(135, 20);
            tsmiStoredProcedures.Text = "tsmiStoredProcedures";
            // 
            // tsmiFnAddResRatioAttr
            // 
            tsmiFnAddResRatioAttr.Name = "tsmiFnAddResRatioAttr";
            tsmiFnAddResRatioAttr.Size = new System.Drawing.Size(360, 22);
            tsmiFnAddResRatioAttr.Text = "tsmiFnAddResRatioAttr";
            tsmiFnAddResRatioAttr.Click += ItemFnAddResRatioAttr_Click;
            // 
            // tsmiFnAddResTotalAttr
            // 
            tsmiFnAddResTotalAttr.Name = "tsmiFnAddResTotalAttr";
            tsmiFnAddResTotalAttr.Size = new System.Drawing.Size(360, 22);
            tsmiFnAddResTotalAttr.Text = "tsmiFnAddResTotalAttr";
            tsmiFnAddResTotalAttr.Click += ItemFnAddResTotalAttr_Click;
            // 
            // tsmiFnAddResTotalGeo
            // 
            tsmiFnAddResTotalGeo.Name = "tsmiFnAddResTotalGeo";
            tsmiFnAddResTotalGeo.Size = new System.Drawing.Size(360, 22);
            tsmiFnAddResTotalGeo.Text = "tsmiFnAddResTotalGeo";
            tsmiFnAddResTotalGeo.Click += ItemFnAddResTotalGeo_Click;
            // 
            // tsmiFnEtlGetTargetVariable
            // 
            tsmiFnEtlGetTargetVariable.Name = "tsmiFnEtlGetTargetVariable";
            tsmiFnEtlGetTargetVariable.Size = new System.Drawing.Size(360, 22);
            tsmiFnEtlGetTargetVariable.Text = "tsmiFnEtlGetTargetVariable";
            tsmiFnEtlGetTargetVariable.Click += ItemFnEtlGetTargetVariable_Click;
            // 
            // tsmiFnEtlGetTargetVariableMetadata
            // 
            tsmiFnEtlGetTargetVariableMetadata.Name = "tsmiFnEtlGetTargetVariableMetadata";
            tsmiFnEtlGetTargetVariableMetadata.Size = new System.Drawing.Size(360, 22);
            tsmiFnEtlGetTargetVariableMetadata.Text = "tsmiFnEtlGetTargetVariableMetadata";
            tsmiFnEtlGetTargetVariableMetadata.Click += ItemFnEtlGetTargetVariableMetadata_Click;
            // 
            // tsmiFnEtlGetTargetVariableMetadataAggregated
            // 
            tsmiFnEtlGetTargetVariableMetadataAggregated.Name = "tsmiFnEtlGetTargetVariableMetadataAggregated";
            tsmiFnEtlGetTargetVariableMetadataAggregated.Size = new System.Drawing.Size(360, 22);
            tsmiFnEtlGetTargetVariableMetadataAggregated.Text = "tsmiFnEtlGetTargetVariableMetadataAggregated";
            tsmiFnEtlGetTargetVariableMetadataAggregated.Click += ItemFnEtlGetTargetVariableMetadataAggregated_Click;
            // 
            // tsmiControlTargetVariableSelector
            // 
            tsmiControlTargetVariableSelector.Name = "tsmiControlTargetVariableSelector";
            tsmiControlTargetVariableSelector.Size = new System.Drawing.Size(360, 22);
            tsmiControlTargetVariableSelector.Text = "tsmiControlTargetVariableSelector";
            tsmiControlTargetVariableSelector.Click += ItemControlTargetVariableSelector_Click;
            // 
            // tsmiFnPanelInEstimationCell
            // 
            tsmiFnPanelInEstimationCell.Name = "tsmiFnPanelInEstimationCell";
            tsmiFnPanelInEstimationCell.Size = new System.Drawing.Size(360, 22);
            tsmiFnPanelInEstimationCell.Text = "tsmiFnPanelInEstimationCell";
            tsmiFnPanelInEstimationCell.Click += ItemFnPanelInEstimationCell_Click;
            // 
            // tsmiFnApiBeforeDeletePanelRefYearSetGroup
            // 
            tsmiFnApiBeforeDeletePanelRefYearSetGroup.Name = "tsmiFnApiBeforeDeletePanelRefYearSetGroup";
            tsmiFnApiBeforeDeletePanelRefYearSetGroup.Size = new System.Drawing.Size(360, 22);
            tsmiFnApiBeforeDeletePanelRefYearSetGroup.Text = "tsmiFnApiBeforeDeletePanelRefYearSetGroup";
            tsmiFnApiBeforeDeletePanelRefYearSetGroup.Click += ItemFnApiBeforeDeletePanelRefYearSetGroup;
            // 
            // tsmiFnApiGetListOfPanelRefYearSetGroups
            // 
            tsmiFnApiGetListOfPanelRefYearSetGroups.Name = "tsmiFnApiGetListOfPanelRefYearSetGroups";
            tsmiFnApiGetListOfPanelRefYearSetGroups.Size = new System.Drawing.Size(360, 22);
            tsmiFnApiGetListOfPanelRefYearSetGroups.Text = "tsmiFnApiGetListOfPanelRefYearSetGroups";
            tsmiFnApiGetListOfPanelRefYearSetGroups.Click += ItemFnApiGetListOfPanelRefYearSetGroups_Click;
            // s
            // tsmiFnApiGetPanelRefYearSetCombinationsForGroups
            // 
            tsmiFnApiGetPanelRefYearSetCombinationsForGroups.Name = "tsmiFnApiGetPanelRefYearSetCombinationsForGroups";
            tsmiFnApiGetPanelRefYearSetCombinationsForGroups.Size = new System.Drawing.Size(360, 22);
            tsmiFnApiGetPanelRefYearSetCombinationsForGroups.Text = "tsmiFnApiGetPanelRefYearSetCombinationsForGroups";
            tsmiFnApiGetPanelRefYearSetCombinationsForGroups.Click += ItemFnApiGetPanelRefYearSetCombinationsForGroups_Click;
            // 
            // tsmiLINQ
            // 
            tsmiLINQ.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] { tsmiOLAPTotalEstimateDimension, tsmiOLAPTotalEstimateValue, tsmiOLAPTotalEstimate, tsmiOLAPRatioEstimate, tsmiOLAPRatioEstimateDimension, tsmiVwPanelRefYearSetPair, tsmiVwVariable, tsmiVwEstimationCellsCount });
            tsmiLINQ.Name = "tsmiLINQ";
            tsmiLINQ.Size = new System.Drawing.Size(69, 20);
            tsmiLINQ.Text = "tsmiLINQ";
            // 
            // tsmiOLAPTotalEstimateDimension
            // 
            tsmiOLAPTotalEstimateDimension.Name = "tsmiOLAPTotalEstimateDimension";
            tsmiOLAPTotalEstimateDimension.Size = new System.Drawing.Size(256, 22);
            tsmiOLAPTotalEstimateDimension.Text = "tsmiOLAPTotalEstimateDimension";
            tsmiOLAPTotalEstimateDimension.Click += ItemOLAPTotalEstimateDimension_Click;
            // 
            // tsmiOLAPTotalEstimateValue
            // 
            tsmiOLAPTotalEstimateValue.Name = "tsmiOLAPTotalEstimateValue";
            tsmiOLAPTotalEstimateValue.Size = new System.Drawing.Size(256, 22);
            tsmiOLAPTotalEstimateValue.Text = "tsmiOLAPTotalEstimateValue";
            tsmiOLAPTotalEstimateValue.Click += ItemOLAPTotalEstimateValue_Click;
            // 
            // tsmiOLAPTotalEstimate
            // 
            tsmiOLAPTotalEstimate.Name = "tsmiOLAPTotalEstimate";
            tsmiOLAPTotalEstimate.Size = new System.Drawing.Size(256, 22);
            tsmiOLAPTotalEstimate.Text = "tsmiOLAPTotalEstimate";
            tsmiOLAPTotalEstimate.Click += ItemOLAPTotalEstimate_Click;
            // 
            // tsmiOLAPRatioEstimate
            // 
            tsmiOLAPRatioEstimate.Name = "tsmiOLAPRatioEstimate";
            tsmiOLAPRatioEstimate.Size = new System.Drawing.Size(256, 22);
            tsmiOLAPRatioEstimate.Text = "tsmiOLAPRatioEstimate";
            tsmiOLAPRatioEstimate.Click += ItemOLAPRatioEstimate_Click;
            // 
            // tsmiOLAPRatioEstimateDimension
            // 
            tsmiOLAPRatioEstimateDimension.Name = "tsmiOLAPRatioEstimateDimension";
            tsmiOLAPRatioEstimateDimension.Size = new System.Drawing.Size(256, 22);
            tsmiOLAPRatioEstimateDimension.Text = "tsmiOLAPRatioEstimateDimension";
            tsmiOLAPRatioEstimateDimension.Click += ItemOLAPRatioEstimateDimension_Click;
            // 
            // tsmiVwPanelRefYearSetPair
            // 
            tsmiVwPanelRefYearSetPair.Name = "tsmiVwPanelRefYearSetPair";
            tsmiVwPanelRefYearSetPair.Size = new System.Drawing.Size(256, 22);
            tsmiVwPanelRefYearSetPair.Text = "tsmiVwPanelRefYearSetPair";
            tsmiVwPanelRefYearSetPair.Click += ItemVwPanelRefYearSetPair_Click;
            // 
            // tsmiVwVariable
            // 
            tsmiVwVariable.Name = "tsmiVwVariable";
            tsmiVwVariable.Size = new System.Drawing.Size(256, 22);
            tsmiVwVariable.Text = "tsmiVwVariable";
            tsmiVwVariable.Click += ItemVwVariable_Click;
            // 
            // tsmiVwEstimationCellsCount
            // 
            tsmiVwEstimationCellsCount.Name = "tsmiVwEstimationCellsCount";
            tsmiVwEstimationCellsCount.Size = new System.Drawing.Size(256, 22);
            tsmiVwEstimationCellsCount.Text = "tsmiVwEstimationCellsCount";
            tsmiVwEstimationCellsCount.Click += ItemVwEstimationCellsCount_Click;
            // 
            // tsmiETL
            // 
            tsmiETL.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] { tsmiExtractData, tsmiGetDifferences });
            tsmiETL.Name = "tsmiETL";
            tsmiETL.Size = new System.Drawing.Size(60, 20);
            tsmiETL.Text = "tsmiETL";
            // 
            // tsmiExtractData
            // 
            tsmiExtractData.Name = "tsmiExtractData";
            tsmiExtractData.Size = new System.Drawing.Size(174, 22);
            tsmiExtractData.Text = "tsmiExtractData";
            // 
            // tsmiGetDifferences
            // 
            tsmiGetDifferences.Name = "tsmiGetDifferences";
            tsmiGetDifferences.Size = new System.Drawing.Size(174, 22);
            tsmiGetDifferences.Text = "tsmiGetDifferences";
            // 
            // tlpMain
            // 
            tlpMain.ColumnCount = 1;
            tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.Controls.Add(tlpButtons, 0, 1);
            tlpMain.Controls.Add(grpMain, 0, 0);
            tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpMain.Location = new System.Drawing.Point(0, 24);
            tlpMain.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            tlpMain.Name = "tlpMain";
            tlpMain.RowCount = 2;
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 46F));
            tlpMain.Size = new System.Drawing.Size(1101, 554);
            tlpMain.TabIndex = 5;
            // 
            // tlpButtons
            // 
            tlpButtons.BackColor = System.Drawing.SystemColors.Control;
            tlpButtons.ColumnCount = 2;
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 187F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            tlpButtons.Controls.Add(pnlClose, 1, 0);
            tlpButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpButtons.Location = new System.Drawing.Point(0, 508);
            tlpButtons.Margin = new System.Windows.Forms.Padding(0);
            tlpButtons.Name = "tlpButtons";
            tlpButtons.RowCount = 1;
            tlpButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.Size = new System.Drawing.Size(1101, 46);
            tlpButtons.TabIndex = 29;
            // 
            // pnlClose
            // 
            pnlClose.Controls.Add(btnClose);
            pnlClose.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlClose.Location = new System.Drawing.Point(914, 0);
            pnlClose.Margin = new System.Windows.Forms.Padding(0);
            pnlClose.Name = "pnlClose";
            pnlClose.Padding = new System.Windows.Forms.Padding(6);
            pnlClose.Size = new System.Drawing.Size(187, 46);
            pnlClose.TabIndex = 12;
            // 
            // btnClose
            // 
            btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            btnClose.Dock = System.Windows.Forms.DockStyle.Fill;
            btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            btnClose.Location = new System.Drawing.Point(6, 6);
            btnClose.Margin = new System.Windows.Forms.Padding(0);
            btnClose.Name = "btnClose";
            btnClose.Size = new System.Drawing.Size(175, 34);
            btnClose.TabIndex = 13;
            btnClose.Text = "btnClose";
            btnClose.UseVisualStyleBackColor = true;
            // 
            // grpMain
            // 
            grpMain.Controls.Add(splitMain);
            grpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            grpMain.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            grpMain.ForeColor = System.Drawing.SystemColors.ControlText;
            grpMain.Location = new System.Drawing.Point(4, 3);
            grpMain.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            grpMain.Name = "grpMain";
            grpMain.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            grpMain.Size = new System.Drawing.Size(1093, 502);
            grpMain.TabIndex = 5;
            grpMain.TabStop = false;
            // 
            // splitMain
            // 
            splitMain.Dock = System.Windows.Forms.DockStyle.Fill;
            splitMain.Location = new System.Drawing.Point(4, 18);
            splitMain.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            splitMain.Name = "splitMain";
            splitMain.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitMain.Panel1
            // 
            splitMain.Panel1.Controls.Add(pnlData);
            // 
            // splitMain.Panel2
            // 
            splitMain.Panel2.Controls.Add(txtSQL);
            splitMain.Size = new System.Drawing.Size(1085, 481);
            splitMain.SplitterDistance = 239;
            splitMain.SplitterWidth = 1;
            splitMain.TabIndex = 11;
            // 
            // pnlData
            // 
            pnlData.AutoScroll = true;
            pnlData.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlData.Location = new System.Drawing.Point(0, 0);
            pnlData.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            pnlData.Name = "pnlData";
            pnlData.Size = new System.Drawing.Size(1085, 239);
            pnlData.TabIndex = 3;
            // 
            // txtSQL
            // 
            txtSQL.BackColor = System.Drawing.SystemColors.Window;
            txtSQL.BorderStyle = System.Windows.Forms.BorderStyle.None;
            txtSQL.Dock = System.Windows.Forms.DockStyle.Fill;
            txtSQL.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 238);
            txtSQL.Location = new System.Drawing.Point(0, 0);
            txtSQL.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            txtSQL.Name = "txtSQL";
            txtSQL.ReadOnly = true;
            txtSQL.Size = new System.Drawing.Size(1085, 241);
            txtSQL.TabIndex = 0;
            txtSQL.Text = "";
            // 
            // FormNfiEsta
            // 
            AcceptButton = btnClose;
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            CancelButton = btnClose;
            ClientSize = new System.Drawing.Size(1101, 578);
            Controls.Add(tlpMain);
            Controls.Add(msMain);
            MainMenuStrip = msMain;
            Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            Name = "FormNfiEsta";
            ShowIcon = false;
            StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            msMain.ResumeLayout(false);
            msMain.PerformLayout();
            tlpMain.ResumeLayout(false);
            tlpButtons.ResumeLayout(false);
            pnlClose.ResumeLayout(false);
            grpMain.ResumeLayout(false);
            splitMain.Panel1.ResumeLayout(false);
            splitMain.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)splitMain).EndInit();
            splitMain.ResumeLayout(false);
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private System.Windows.Forms.MenuStrip msMain;
        private System.Windows.Forms.ToolStripMenuItem tsmiLookupTables;
        private System.Windows.Forms.ToolStripMenuItem tsmiCAreaDomain;
        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.GroupBox grpMain;
        private System.Windows.Forms.SplitContainer splitMain;
        private System.Windows.Forms.Panel pnlData;
        private System.Windows.Forms.RichTextBox txtSQL;
        private System.Windows.Forms.ToolStripMenuItem tsmiCAreaDomainCategory;
        private System.Windows.Forms.ToolStripMenuItem tsmiCAuxiliaryVariable;
        private System.Windows.Forms.ToolStripMenuItem tsmiCAuxiliaryVariableCategory;
        private System.Windows.Forms.ToolStripMenuItem tsmiCEstimateType;
        private System.Windows.Forms.ToolStripMenuItem tsmiCEstimationCell;
        private System.Windows.Forms.ToolStripMenuItem tsmiCEstimationCellCollection;
        private System.Windows.Forms.ToolStripMenuItem tsmiCEstimationPeriod;
        private System.Windows.Forms.ToolStripMenuItem tsmiCPanelRefYearSetGroup;
        private System.Windows.Forms.ToolStripMenuItem tsmiCParamAreaType;
        private System.Windows.Forms.ToolStripMenuItem tsmiCPhaseEstimateType;
        private System.Windows.Forms.ToolStripMenuItem tsmiCSubPopulation;
        private System.Windows.Forms.ToolStripMenuItem tsmiCSubPopulationCategory;
        private System.Windows.Forms.ToolStripMenuItem tsmiCTargetVariable;
        private System.Windows.Forms.ToolStripMenuItem tsmiCTopic;
        private System.Windows.Forms.ToolStripMenuItem tsmiMappingTables;
        private System.Windows.Forms.ToolStripMenuItem tsmiCmEstimationCellToParamAreaMapping;
        private System.Windows.Forms.ToolStripMenuItem tsmiCmPlotToEstimationCellMapping;
        private System.Windows.Forms.ToolStripMenuItem tsmiCmPlotToParametrizationAreaMapping;
        private System.Windows.Forms.ToolStripMenuItem tsmiCmTargetVariableToTopic;
        private System.Windows.Forms.ToolStripMenuItem tsmiDataTables;
        private System.Windows.Forms.ToolStripMenuItem tsmiTAuxConf;
        private System.Windows.Forms.ToolStripMenuItem tsmiTAuxTotal;
        private System.Windows.Forms.ToolStripMenuItem tsmiTAuxiliaryData;
        private System.Windows.Forms.ToolStripMenuItem tsmiTAvailableDataSets;
        private System.Windows.Forms.ToolStripMenuItem tsmiTEstimateConf;
        private System.Windows.Forms.ToolStripMenuItem tsmiTEstimationCellHierarchy;
        private System.Windows.Forms.ToolStripMenuItem tsmiTGBeta;
        private System.Windows.Forms.ToolStripMenuItem tsmiTModel;
        private System.Windows.Forms.ToolStripMenuItem tsmiTModelVariables;
        private System.Windows.Forms.ToolStripMenuItem tsmiTPanelRefYearSetGroup;
        private System.Windows.Forms.ToolStripMenuItem tsmiTResult;
        private System.Windows.Forms.ToolStripMenuItem tsmiTTargetData;
        private System.Windows.Forms.ToolStripMenuItem tsmiTTotalEstimateConf;
        private System.Windows.Forms.ToolStripMenuItem tsmiTVariable;
        private System.Windows.Forms.ToolStripMenuItem tsmiTVariableHierarchy;
        private System.Windows.Forms.ToolStripMenuItem tsmiCEstimateConfStatus;
        private System.Windows.Forms.ToolStripMenuItem tsmiTPanelToTotalEstimateConfFirstPhase;
        private System.Windows.Forms.ToolStripMenuItem tsmiSpatialTables;
        private System.Windows.Forms.ToolStripMenuItem tsmiFaCell;
        private System.Windows.Forms.ToolStripMenuItem tsmiFaParamArea;
        private System.Windows.Forms.ToolStripMenuItem tsmiTStratumInEstimationCell;
        private System.Windows.Forms.ToolStripMenuItem tsmiETL;
        private System.Windows.Forms.ToolStripMenuItem tsmiGetDifferences;
        private System.Windows.Forms.ToolStripMenuItem tsmiJSON;
        private System.Windows.Forms.ToolStripMenuItem tsmiTargetVariableMetadata;
        private System.Windows.Forms.ToolStripMenuItem tsmiResultSamplingUnits;
        private System.Windows.Forms.ToolStripMenuItem tsmiLINQ;
        private System.Windows.Forms.ToolStripMenuItem tsmiOLAPTotalEstimateDimension;
        private System.Windows.Forms.ToolStripMenuItem tsmiOLAPTotalEstimateValue;
        private System.Windows.Forms.ToolStripMenuItem tsmiVwVariable;
        private System.Windows.Forms.ToolStripMenuItem tsmiVwPanelRefYearSetPair;
        private System.Windows.Forms.ToolStripMenuItem tsmiOLAPTotalEstimate;
        private System.Windows.Forms.ToolStripMenuItem tsmiOLAPRatioEstimate;
        private System.Windows.Forms.ToolStripMenuItem tsmiOLAPRatioEstimateDimension;
        private System.Windows.Forms.ToolStripMenuItem tsmiStoredProcedures;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnAddResRatioAttr;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnAddResTotalAttr;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnAddResTotalGeo;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnPanelInEstimationCell;
        private System.Windows.Forms.ToolStripMenuItem tsmiTargetVariableMetadataAggregated;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnEtlCheckVariablesJson;
        private System.Windows.Forms.ToolStripMenuItem tsmiCLanguage;
        private System.Windows.Forms.ToolStripMenuItem tsmiTAdditivitySetPlot;
        private System.Windows.Forms.ToolStripMenuItem tsmiCmAreaDomainCategoryMapping;
        private System.Windows.Forms.ToolStripMenuItem tsmiCmSubPopulationCategoryMapping;
        private System.Windows.Forms.TableLayoutPanel tlpButtons;
        private System.Windows.Forms.Panel pnlClose;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.ToolStripMenuItem tsmiExtractData;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnEtlGetTargetVariable;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnEtlGetTargetVariableMetadata;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnEtlGetTargetVariableMetadataAggregated;
        private System.Windows.Forms.ToolStripMenuItem tsmiControlTargetVariableSelector;
        private System.Windows.Forms.ToolStripMenuItem tsmiVwEstimationCellsCount;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnApiBeforeDeletePanelRefYearSetGroup;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnApiGetListOfPanelRefYearSetGroups;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnApiGetPanelRefYearSetCombinationsForGroups;
    }
}