﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {
            // t_target_data

            /// <summary>
            /// Local density for inventory plot
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class TargetData(
                TargetDataList composite = null,
                DataRow data = null)
                    : ADataTableEntry<TargetDataList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Local density for inventory plot identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TargetDataList.ColId.Name,
                            defaultValue: Int32.Parse(s: TargetDataList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TargetDataList.ColId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Inventory plot identifier
                /// </summary>
                public int PlotId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TargetDataList.ColPlotId.Name,
                            defaultValue: Int32.Parse(s: TargetDataList.ColPlotId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TargetDataList.ColPlotId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Inventory plot object (read-only)
                /// </summary>
                public SDesign.Plot Plot
                {
                    get
                    {
                        return
                             ((NfiEstaDB)Composite.Database).SDesign.FpPlot[PlotId];
                    }
                }


                /// <summary>
                /// Value of local density for inventory plot
                /// </summary>
                public double Val
                {
                    get
                    {
                        return Functions.GetDoubleArg(
                            row: Data,
                            name: AuxTotalList.ColVal.Name,
                            defaultValue: Double.Parse(s: AuxTotalList.ColVal.DefaultValue));
                    }
                    set
                    {
                        Functions.SetDoubleArg(
                            row: Data,
                            name: AuxTotalList.ColVal.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Date and time when local density for inventory plot was inserted into table
                /// </summary>
                public Nullable<DateTime> ValueInserted
                {
                    get
                    {
                        return Functions.GetNDateTimeArg(
                            row: Data,
                            name: TargetDataList.ColValueInserted.Name,
                            defaultValue: String.IsNullOrEmpty(value: TargetDataList.ColValueInserted.DefaultValue) ?
                                            (Nullable<DateTime>)null :
                                            DateTime.Parse(s: TargetDataList.ColValueInserted.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNDateTimeArg(
                            row: Data,
                            name: TargetDataList.ColValueInserted.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Date and time when local density for inventory plot was inserted into table as text
                /// </summary>
                public string ValueInsertedText
                {
                    get
                    {
                        return
                            (ValueInserted == null) ? Functions.StrNull :
                                ((DateTime)ValueInserted).ToString(
                                format: TargetDataList.ColValueInserted.NumericFormat);
                    }
                }

                /// <summary>
                /// Identifier of latest local density value (true/false)
                /// </summary>
                public bool IsLatest
                {
                    get
                    {
                        return Functions.GetBoolArg(
                            row: Data,
                            name: TargetDataList.ColIsLatest.Name,
                            defaultValue: Boolean.Parse(value: TargetDataList.ColIsLatest.DefaultValue));
                    }
                    set
                    {
                        Functions.SetBoolArg(
                            row: Data,
                            name: TargetDataList.ColIsLatest.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Available dataset identifier
                /// </summary>
                public int AvailableDataSetsId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TargetDataList.ColAvailableDataSets.Name,
                            defaultValue: Int32.Parse(s: TargetDataList.ColAvailableDataSets.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TargetDataList.ColAvailableDataSets.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Available dataset object
                /// </summary>
                public AvailableDataSet AvailableDataSet
                {
                    get
                    {
                        return ((NfiEstaDB)Composite.Database).SNfiEsta
                            .TAvailableDataSet[AvailableDataSetsId];
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not TargetData Type, return False
                    if (obj is not TargetData)
                    {
                        return false;
                    }

                    return
                        Id == ((TargetData)obj).Id;
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                #endregion Methods

            }


            /// <summary>
            /// List of local densities for inventory plots
            /// </summary>
            public class TargetDataList
                : ADataTable
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema = NfiEstaSchema.Name;

                /// <summary>
                /// Table name
                /// </summary>
                public const string Name = "t_target_data";

                /// <summary>
                /// Table alias
                /// </summary>
                public const string Alias = "t_target_data";

                /// <summary>
                /// Description in national language
                /// </summary>
                public const string CaptionCs = "Seznam lokálních hustot pro inventarizační plochy";

                /// <summary>
                /// Description in English
                /// </summary>
                public const string CaptionEn = "List of local densities for inventory plots";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
                {
                    { "id", new ColumnMetadata()
                    {
                        Name = "id",
                        DbName = "id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = true,
                        NotNull = true,
                        DefaultValue = default(int).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ID",
                        HeaderTextEn = "ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 0
                    }
                    },
                    { "plot", new ColumnMetadata()
                    {
                        Name = "plot",
                        DbName = "plot",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = default(int).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PLOT",
                        HeaderTextEn = "PLOT",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 1
                    }
                    },
                    { "value", new ColumnMetadata()
                    {
                        Name = "value",
                        DbName = "value",
                        DataType = "System.Double",
                        DbDataType = "float8",
                        NewDataType = "float8",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = default(double).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "N3",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "VALUE",
                        HeaderTextEn = "VALUE",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 2
                    }
                    },
                    { "value_inserted", new ColumnMetadata()
                    {
                        Name = "value_inserted",
                        DbName = "value_inserted",
                        DataType = "System.DateTime",
                        DbDataType = "timestamptz",
                        NewDataType = "timestamptz",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "yyyy-MM-dd HH:mm:ss",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "VALUE_INSERTED",
                        HeaderTextEn = "VALUE_INSERTED",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 3
                    }
                    },
                    { "is_latest", new ColumnMetadata()
                    {
                        Name = "is_latest",
                        DbName = "is_latest",
                        DataType = "System.Boolean",
                        DbDataType = "bool",
                        NewDataType = "bool",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = default(bool).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "IS_LATEST",
                        HeaderTextEn = "IS_LATEST",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 4
                    }
                    },
                    { "available_datasets", new ColumnMetadata()
                    {
                        Name = "available_datasets",
                        DbName = "available_datasets",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = default(int).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "AVAILABLE_DATASETS",
                        HeaderTextEn = "AVAILABLE_DATASETS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 5
                    }
                    }
                };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column plot metadata
                /// </summary>
                public static readonly ColumnMetadata ColPlotId = Cols["plot"];

                /// <summary>
                /// Column value metadata
                /// </summary>
                public static readonly ColumnMetadata ColVal = Cols["value"];

                /// <summary>
                /// Column value_inserted metadata
                /// </summary>
                public static readonly ColumnMetadata ColValueInserted = Cols["value_inserted"];

                /// <summary>
                /// Column is_latest metadata
                /// </summary>
                public static readonly ColumnMetadata ColIsLatest = Cols["is_latest"];

                /// <summary>
                /// Column available_datasets metadata
                /// </summary>
                public static readonly ColumnMetadata ColAvailableDataSets = Cols["available_datasets"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TargetDataList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TargetDataList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// List of local densities for inventory plots (read-only)
                /// </summary>
                public List<TargetData> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new TargetData(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// Local density for inventory plot from list by identifier (read-only)
                /// </summary>
                /// <param name="id">Local density for inventory plot identifier</param>
                /// <returns>Local density for inventory plot object (null if not found)</returns>
                public TargetData this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new TargetData(composite: this, data: a))
                                .FirstOrDefault<TargetData>();
                    }
                }

                #endregion Indexer


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public TargetDataList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new TargetDataList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new TargetDataList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                /// <summary>
                /// List of local densities
                /// for available datasets
                /// </summary>
                /// <param name="availableDataSetsId">Selected available datasets identifiers</param>
                /// <param name="isLatest">Identification of the actual value</param>
                /// <returns>
                /// List of local densities
                /// for available datasets
                /// </returns>
                public TargetDataList Reduce(
                    List<int> availableDataSetsId = null,
                    Nullable<bool> isLatest = null)
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();

                    if ((availableDataSetsId != null) && availableDataSetsId.Count != 0)
                    {
                        rows = rows.Where(a => availableDataSetsId.Contains<int>(
                                value: (int)(a.Field<Nullable<int>>(columnName: ColAvailableDataSets.Name) ?? 0)));
                    }

                    if (isLatest != null)
                    {
                        rows = rows.Where(a => (bool)(a.Field<Nullable<bool>>(columnName: ColIsLatest.Name) ?? false));
                    }

                    return
                        rows.Any() ?
                        new TargetDataList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new TargetDataList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                #endregion Methods

            }

        }
    }
}
