﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {
            // t_available_datasets

            /// <summary>
            /// Available dataset
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class AvailableDataSet(
                AvailableDataSetList composite = null,
                DataRow data = null)
                    : ADataTableEntry<AvailableDataSetList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Available dataset identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: AvailableDataSetList.ColId.Name,
                            defaultValue: Int32.Parse(s: AvailableDataSetList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: AvailableDataSetList.ColId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Panel identifier
                /// </summary>
                public int PanelId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: AvailableDataSetList.ColPanelId.Name,
                            defaultValue: Int32.Parse(s: AvailableDataSetList.ColPanelId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: AvailableDataSetList.ColPanelId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Panel object (read-only)
                /// </summary>
                public SDesign.Panel Panel
                {
                    get
                    {
                        return
                             ((NfiEstaDB)Composite.Database).SDesign.TPanel[PanelId];
                    }
                }


                /// <summary>
                /// Reference year or season set identifier
                /// </summary>
                public Nullable<int> ReferenceYearSetId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: AvailableDataSetList.ColReferenceYearSetId.Name,
                            defaultValue: String.IsNullOrEmpty(value: AvailableDataSetList.ColReferenceYearSetId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: AvailableDataSetList.ColReferenceYearSetId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: AvailableDataSetList.ColReferenceYearSetId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Reference year or season set object (read-only)
                /// </summary>
                public SDesign.ReferenceYearSet ReferenceYearSet
                {
                    get
                    {
                        return (ReferenceYearSetId != null) ?
                             ((NfiEstaDB)Composite.Database).SDesign.TReferenceYearSet[(int)ReferenceYearSetId] : null;
                    }
                }


                /// <summary>
                /// Combination of target variable and sub-population category and area domain category identifier
                /// </summary>
                public int VariableId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: AvailableDataSetList.ColVariableId.Name,
                            defaultValue: Int32.Parse(s: AvailableDataSetList.ColVariableId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: AvailableDataSetList.ColVariableId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Combination of target variable and sub-population category and area domain category object (read-only)
                /// </summary>
                public Variable Variable
                {
                    get
                    {
                        return
                             ((NfiEstaDB)Composite.Database).SNfiEsta.TVariable[VariableId];
                    }
                }

                /// <summary>
                /// Combination of target variable and sub-population category and area domain category (extended-version) object (read-only)
                /// </summary>
                public VwVariable VwVariable
                {
                    get
                    {
                        return
                             ((NfiEstaDB)Composite.Database).SNfiEsta.VVariable[VariableId];
                    }
                }


                /// <summary>
                /// Time with last change date
                /// </summary>
                public DateTime LastChange
                {
                    get
                    {
                        return Functions.GetDateTimeArg(
                            row: Data,
                            name: AvailableDataSetList.ColLastChange.Name,
                            defaultValue: DateTime.Parse(s: AvailableDataSetList.ColLastChange.DefaultValue));
                    }
                    set
                    {
                        Functions.SetDateTimeArg(
                            row: Data,
                            name: AvailableDataSetList.ColLastChange.Name,
                            val: value);
                    }
                }

                /// <summary>
                ///  Time with last change date as text
                /// </summary>
                public string LastChangeText
                {
                    get
                    {
                        return
                            LastChange.ToString(
                                format: AvailableDataSetList.ColLastChange.NumericFormat);
                    }
                }

                /// <summary>
                /// Saved threshold for given combination panel, reference_year_set and variable.
                /// </summary>
                public double LDsityThreshold
                {
                    get
                    {
                        return Functions.GetDoubleArg(
                            row: Data,
                            name: AvailableDataSetList.ColLDsityThreshold.Name,
                            defaultValue: Double.Parse(s: AvailableDataSetList.ColLDsityThreshold.DefaultValue));
                    }
                    set
                    {
                        Functions.SetDoubleArg(
                            row: Data,
                            name: AvailableDataSetList.ColLDsityThreshold.Name,
                            val: value);
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not AvailableDataSet Type, return False
                    if (obj is not AvailableDataSet)
                    {
                        return false;
                    }

                    return
                        Id == ((AvailableDataSet)obj).Id;
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                #endregion Methods

            }


            /// <summary>
            /// List of available datasets
            /// </summary>
            public class AvailableDataSetList
                : ADataTable
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema = NfiEstaSchema.Name;

                /// <summary>
                /// Table name
                /// </summary>
                public const string Name = "t_available_datasets";

                /// <summary>
                /// Table alias
                /// </summary>
                public const string Alias = "t_available_datasets";

                /// <summary>
                /// Description in national language
                /// </summary>
                public const string CaptionCs = "Seznam dostupných datových sad";

                /// <summary>
                /// Description in English
                /// </summary>
                public const string CaptionEn = "List of available datasets";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
                {
                    { "id", new ColumnMetadata()
                    {
                        Name = "id",
                        DbName = "id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = true,
                        NotNull = true,
                        DefaultValue = default(int).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ID",
                        HeaderTextEn = "ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 0
                    }
                    },
                    { "panel", new ColumnMetadata()
                    {
                        Name = "panel",
                        DbName = "panel",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = default(int).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PANEL",
                        HeaderTextEn = "PANEL",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 1
                    }
                    },
                    { "reference_year_set", new ColumnMetadata()
                    {
                        Name = "reference_year_set",
                        DbName = "reference_year_set",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "REFERENCE_YEAR_SET",
                        HeaderTextEn = "REFERENCE_YEAR_SET",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 2
                    }
                    },
                    { "variable", new ColumnMetadata()
                    {
                        Name = "variable",
                        DbName = "variable",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = default(int).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "VARIABLE",
                        HeaderTextEn = "VARIABLE",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 3
                    }
                    },
                    { "last_change", new ColumnMetadata()
                    {
                        Name = "last_change",
                        DbName = "last_change",
                        DataType = "System.DateTime",
                        DbDataType = "timestamptz",
                        NewDataType = "timestamptz",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = new DateTime().ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "yyyy-MM-dd HH:mm:ss",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "LAST_CHANGE",
                        HeaderTextEn = "LAST_CHANGE",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 4
                    }
                    },
                    { "ldsity_threshold", new ColumnMetadata()
                    {
                        Name = "ldsity_threshold",
                        DbName = "ldsity_threshold",
                        DataType = "System.Double",
                        DbDataType = "float8",
                        NewDataType = "float8",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = default(double).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "N3",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "LDSITY_THRESHOLD",
                        HeaderTextEn = "LDSITY_THRESHOLD",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 5
                    }
                    }
                };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column panel metadata
                /// </summary>
                public static readonly ColumnMetadata ColPanelId = Cols["panel"];

                /// <summary>
                /// Column reference_year_set metadata
                /// </summary>
                public static readonly ColumnMetadata ColReferenceYearSetId = Cols["reference_year_set"];

                /// <summary>
                /// Column variable metadata
                /// </summary>
                public static readonly ColumnMetadata ColVariableId = Cols["variable"];

                /// <summary>
                /// Column last_change metadata
                /// </summary>
                public static readonly ColumnMetadata ColLastChange = Cols["last_change"];

                /// <summary>
                /// Column ldsity_threshold metadata
                /// </summary>
                public static readonly ColumnMetadata ColLDsityThreshold = Cols["ldsity_threshold"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public AvailableDataSetList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public AvailableDataSetList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// List of available datasets (read-only)
                /// </summary>
                public List<AvailableDataSet> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new AvailableDataSet(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// Available dataset from list by identifier (read-only)
                /// </summary>
                /// <param name="id">Available dataset identifier</param>
                /// <returns>Available dataset from list by identifier (null if not found)</returns>
                public AvailableDataSet this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new AvailableDataSet(composite: this, data: a))
                                .FirstOrDefault<AvailableDataSet>();
                    }
                }

                #endregion Indexer


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public AvailableDataSetList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new AvailableDataSetList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new AvailableDataSetList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                /// <summary>
                /// List of available datasets
                /// for selected panels, reference year or season sets and variables
                /// </summary>
                /// <param name="panelId">Selected panels identifiers</param>
                /// <param name="referenceYearSetId">Selected reference year or season sets identifiers</param>
                /// <param name="variableId">Selected variables identifiers</param>
                /// <returns>
                /// List of available datasets
                /// for selected panels, reference year or season sets and variables
                /// </returns>
                public AvailableDataSetList Reduce(
                    List<int> panelId = null,
                    List<int> referenceYearSetId = null,
                    List<int> variableId = null)
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();

                    if ((panelId != null) && panelId.Count != 0)
                    {
                        rows = rows.Where(a => panelId.Contains<int>(
                                value: (int)(a.Field<Nullable<int>>(columnName: ColPanelId.Name) ?? 0)));
                    }

                    if ((referenceYearSetId != null) && referenceYearSetId.Count != 0)
                    {
                        rows = rows.Where(a => referenceYearSetId.Contains<int>(
                                value: (int)(a.Field<Nullable<int>>(columnName: ColReferenceYearSetId.Name) ?? 0)));
                    }

                    if ((variableId != null) && variableId.Count != 0)
                    {
                        rows = rows.Where(a => variableId.Contains<int>(
                                value: (int)(a.Field<Nullable<int>>(columnName: ColVariableId.Name) ?? 0)));
                    }

                    return
                        rows.Any() ?
                        new AvailableDataSetList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new AvailableDataSetList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                #endregion Methods

            }

        }
    }
}
