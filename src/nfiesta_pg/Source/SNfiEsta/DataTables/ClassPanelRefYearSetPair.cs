﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {
            // t_panel_refyearset_group

            /// <summary>
            /// Pair of panel with corresponding reference year set
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class PanelRefYearSetPair(
                PanelRefYearSetPairList composite = null,
                DataRow data = null)
                    : ADataTableEntry<PanelRefYearSetPairList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Pair of panel with corresponding reference year set identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: PanelRefYearSetPairList.ColId.Name,
                            defaultValue: Int32.Parse(s: PanelRefYearSetPairList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: PanelRefYearSetPairList.ColId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Aggregated sets of panels and corresponding reference year sets identifier
                /// </summary>
                public int PanelRefYearSetGroupId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: PanelRefYearSetPairList.ColPanelRefYearSetGroupId.Name,
                            defaultValue: Int32.Parse(s: PanelRefYearSetPairList.ColPanelRefYearSetGroupId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: PanelRefYearSetPairList.ColPanelRefYearSetGroupId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Aggregated sets of panels and corresponding reference year sets object (read-only)
                /// </summary>
                public PanelRefYearSetGroup PanelRefYearSetGroup
                {
                    get
                    {
                        return ((NfiEstaDB)Composite.Database).SNfiEsta.CPanelRefYearSetGroup[PanelRefYearSetGroupId];
                    }
                }


                /// <summary>
                /// Sampling panel identifier
                /// </summary>
                public int PanelId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: PanelRefYearSetPairList.ColPanelId.Name,
                            defaultValue: Int32.Parse(s: PanelRefYearSetPairList.ColPanelId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: PanelRefYearSetPairList.ColPanelId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Sampling panel object (read-only)
                /// </summary>
                public SDesign.Panel Panel
                {
                    get
                    {
                        return ((NfiEstaDB)Composite.Database).SDesign.TPanel[PanelId];
                    }
                }


                /// <summary>
                /// Reference year or season set identifier
                /// </summary>
                public Nullable<int> ReferenceYearSetId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: PanelRefYearSetPairList.ColReferenceYearSetId.Name,
                            defaultValue: String.IsNullOrEmpty(value: PanelRefYearSetPairList.ColReferenceYearSetId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: PanelRefYearSetPairList.ColReferenceYearSetId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: PanelRefYearSetPairList.ColReferenceYearSetId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Reference year or season set object (read-only)
                /// </summary>
                public SDesign.ReferenceYearSet ReferenceYearSet
                {
                    get
                    {
                        return (ReferenceYearSetId != null) ?
                             ((NfiEstaDB)Composite.Database).SDesign.TReferenceYearSet[(int)ReferenceYearSetId] : null;
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not PanelRefYearSetPair Type, return False
                    if (obj is not PanelRefYearSetPair)
                    {
                        return false;
                    }

                    return
                        Id == ((PanelRefYearSetPair)obj).Id;
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                #endregion Methods

            }


            /// <summary>
            /// List of pairs of panel with corresponding reference year set
            /// </summary>
            public class PanelRefYearSetPairList
                : ADataTable
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema = NfiEstaSchema.Name;

                /// <summary>
                /// Table name
                /// </summary>
                public const string Name = "t_panel_refyearset_group";

                /// <summary>
                /// Table alias
                /// </summary>
                public const string Alias = "t_panel_refyearset_group";

                /// <summary>
                /// Description in national language
                /// </summary>
                public const string CaptionCs = "Seznam párů panelu s odpovídajícím rokem měření";

                /// <summary>
                /// Description in English
                /// </summary>
                public const string CaptionEn = "List of pairs of panel with corresponding reference year set";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
                {
                    { "id", new ColumnMetadata()
                    {
                        Name = "id",
                        DbName = "id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = true,
                        NotNull = true,
                        DefaultValue = default(int).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ID",
                        HeaderTextEn = "ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 0
                    }
                    },
                    { "panel_refyearset_group", new ColumnMetadata()
                    {
                        Name = "panel_refyearset_group",
                        DbName = "panel_refyearset_group",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = default(int).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PANEL_REFYEARSET_GROUP",
                        HeaderTextEn = "PANEL_REFYEARSET_GROUP",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 1
                    }
                    },
                    { "panel", new ColumnMetadata()
                    {
                        Name = "panel",
                        DbName = "panel",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = default(int).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PANEL",
                        HeaderTextEn = "PANEL",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 2
                    }
                    },
                    { "reference_year_set", new ColumnMetadata()
                    {
                        Name = "reference_year_set",
                        DbName = "reference_year_set",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "REFERENCE_YEAR_SET",
                        HeaderTextEn = "REFERENCE_YEAR_SET",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 3
                    }
                    }
                };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column panel_refyearset_group metadata
                /// </summary>
                public static readonly ColumnMetadata ColPanelRefYearSetGroupId = Cols["panel_refyearset_group"];

                /// <summary>
                /// Column panel metadata
                /// </summary>
                public static readonly ColumnMetadata ColPanelId = Cols["panel"];

                /// <summary>
                /// Column reference_year_set metadata
                /// </summary>
                public static readonly ColumnMetadata ColReferenceYearSetId = Cols["reference_year_set"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public PanelRefYearSetPairList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public PanelRefYearSetPairList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// List of pairs of panel with corresponding reference year set (read-only)
                /// </summary>
                public List<PanelRefYearSetPair> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new PanelRefYearSetPair(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// Pair of panel with corresponding reference year set from list by identifier (read-only)
                /// </summary>
                /// <param name="id">Pair of panel with corresponding reference year set identifier</param>
                /// <returns>Pair of panel with corresponding reference year set from list by identifier (null if not found)</returns>
                public PanelRefYearSetPair this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new PanelRefYearSetPair(composite: this, data: a))
                                .FirstOrDefault<PanelRefYearSetPair>();
                    }
                }

                #endregion Indexer


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public PanelRefYearSetPairList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new PanelRefYearSetPairList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new PanelRefYearSetPairList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                /// <summary>
                /// List of pairs of panel with corresponding reference year set
                /// for selected panel reference year set groups, sampling panels
                /// and reference year or season sets
                /// </summary>
                /// <param name="panelRefYearSetGroupId">Selected panel reference year set groups identifiers</param>
                /// <param name="panelId">Selected sampling panels identifiers</param>
                /// <param name="referenceYearSetId">Selected reference year or season sets identifiers</param>
                /// <returns>
                /// List of pairs of panel with corresponding reference year set
                /// for selected panel reference year set groups, sampling panels
                /// and reference year or season sets
                /// </returns>
                public PanelRefYearSetPairList Reduce(
                    List<int> panelRefYearSetGroupId = null,
                    List<int> panelId = null,
                    List<int> referenceYearSetId = null)
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();

                    if ((panelRefYearSetGroupId != null) && panelRefYearSetGroupId.Count != 0)
                    {
                        rows = rows.Where(a => panelRefYearSetGroupId.Contains<int>(
                                value: (int)(a.Field<Nullable<int>>(columnName: ColPanelRefYearSetGroupId.Name) ?? 0)));
                    }

                    if ((panelId != null) && panelId.Count != 0)
                    {
                        rows = rows.Where(a => panelId.Contains<int>(
                                value: (int)(a.Field<Nullable<int>>(columnName: ColPanelId.Name) ?? 0)));
                    }

                    if ((referenceYearSetId != null) && referenceYearSetId.Count != 0)
                    {
                        rows = rows.Where(a => referenceYearSetId.Contains<int>(
                                value: (int)(a.Field<Nullable<int>>(columnName: ColReferenceYearSetId.Name) ?? 0)));
                    }

                    return
                        rows.Any() ?
                        new PanelRefYearSetPairList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new PanelRefYearSetPairList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                #endregion Methods

            }

        }
    }
}