﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {
            // t_estimate_conf

            /// <summary>
            /// Estimate configuration
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class EstimateConf(
                EstimateConfList composite = null,
                DataRow data = null)
                        : ADataTableEntry<EstimateConfList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Estimate configuration identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: EstimateConfList.ColId.Name,
                            defaultValue: Int32.Parse(s: EstimateConfList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: EstimateConfList.ColId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Estimate type (total or ratio) identifier
                /// </summary>
                public int EstimateTypeId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: EstimateConfList.ColEstimateTypeId.Name,
                            defaultValue: Int32.Parse(s: EstimateConfList.ColEstimateTypeId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: EstimateConfList.ColEstimateTypeId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Estimate type (total or ratio) object (read-only)
                /// </summary>
                public EstimateType EstimateType
                {
                    get
                    {
                        return ((NfiEstaDB)Composite.Database).SNfiEsta.CEstimateType[EstimateTypeId];
                    }
                }


                /// <summary>
                /// Total estimate identifier
                /// </summary>
                public int TotalEstimateConfId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: EstimateConfList.ColTotalEstimateConfId.Name,
                            defaultValue: Int32.Parse(s: EstimateConfList.ColTotalEstimateConfId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: EstimateConfList.ColTotalEstimateConfId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Total estimate object (read-only)
                /// </summary>
                public TotalEstimateConf TotalEstimateConf
                {
                    get
                    {
                        return
                             ((NfiEstaDB)Composite.Database).SNfiEsta.TTotalEstimateConf[TotalEstimateConfId];
                    }
                }


                /// <summary>
                /// Total estimate used as a denominator - identifier
                /// </summary>
                public Nullable<int> DenominatorId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: EstimateConfList.ColDenominatorId.Name,
                            defaultValue: String.IsNullOrEmpty(value: EstimateConfList.ColDenominatorId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: EstimateConfList.ColDenominatorId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: EstimateConfList.ColDenominatorId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Total estimate used as a denominator - object (read-only)
                /// </summary>
                public TotalEstimateConf Denominator
                {
                    get
                    {
                        return (DenominatorId != null) ?
                             ((NfiEstaDB)Composite.Database).SNfiEsta.TTotalEstimateConf[(int)DenominatorId] : null;
                    }
                }


                /// <summary>
                /// User, who configured estimate
                /// </summary>
                public string Executor
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: EstimateConfList.ColExecutor.Name,
                            defaultValue: EstimateConfList.ColExecutor.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: EstimateConfList.ColExecutor.Name,
                            val: value);
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not EstimateConf Type, return False
                    if (obj is not EstimateConf)
                    {
                        return false;
                    }

                    return
                        Id == ((EstimateConf)obj).Id;
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                #endregion Methods

            }


            /// <summary>
            /// List of estimate configurations
            /// </summary>
            public class EstimateConfList
                : ADataTable
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema = NfiEstaSchema.Name;

                /// <summary>
                /// Table name
                /// </summary>
                public const string Name = "t_estimate_conf";

                /// <summary>
                /// Table alias
                /// </summary>
                public const string Alias = "t_estimate_conf";

                /// <summary>
                /// Description in national language
                /// </summary>
                public const string CaptionCs = "Seznam konfigurací odhadů";

                /// <summary>
                /// Description in English
                /// </summary>
                public const string CaptionEn = "List of estimate configurations";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
                {
                    { "id", new ColumnMetadata()
                    {
                        Name = "id",
                        DbName = "id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = true,
                        NotNull = true,
                        DefaultValue = default(int).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ID",
                        HeaderTextEn = "ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 0
                    }
                    },
                    { "estimate_type", new ColumnMetadata()
                    {
                        Name = "estimate_type",
                        DbName = "estimate_type",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = default(int).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATE_TYPE",
                        HeaderTextEn = "ESTIMATE_TYPE",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 1
                    }
                    },
                    { "total_estimate_conf", new ColumnMetadata()
                    {
                        Name = "total_estimate_conf",
                        DbName = "total_estimate_conf",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = default(int).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "TOTAL_ESTIMATE_TYPE",
                        HeaderTextEn = "TOTAL_ESTIMATE_TYPE",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 2
                    }
                    },
                    { "denominator", new ColumnMetadata()
                    {
                        Name = "denominator",
                        DbName = "denominator",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "DENOMINATOR",
                        HeaderTextEn = "DENOMINATOR",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 3
                    }
                    },
                    { "executor", new ColumnMetadata()
                    {
                        Name = "executor",
                        DbName = "executor",
                        DataType = "System.String",
                        DbDataType = "name",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "EXECUTOR",
                        HeaderTextEn = "EXECUTOR",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 4
                    }
                    }
                };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column estimate_type metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimateTypeId = Cols["estimate_type"];

                /// <summary>
                /// Column total_estimate_conf metadata
                /// </summary>
                public static readonly ColumnMetadata ColTotalEstimateConfId = Cols["total_estimate_conf"];

                /// <summary>
                /// Column denominator metadata
                /// </summary>
                public static readonly ColumnMetadata ColDenominatorId = Cols["denominator"];

                /// <summary>
                /// Column executor metadata
                /// </summary>
                public static readonly ColumnMetadata ColExecutor = Cols["executor"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public EstimateConfList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public EstimateConfList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// List of estimate configurations (read-only)
                /// </summary>
                public List<EstimateConf> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new EstimateConf(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// Estimate configuration from list by identifier (read-only)
                /// </summary>
                /// <param name="id">Estimate configuration identifier</param>
                /// <returns>Estimate configuration from list by identifier (null if not found)</returns>
                public EstimateConf this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new EstimateConf(composite: this, data: a))
                                .FirstOrDefault<EstimateConf>();
                    }
                }

                #endregion Indexer


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public EstimateConfList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new EstimateConfList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new EstimateConfList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                /// <summary>
                /// List of estimate configurations
                /// for selected estimate types and total estimate configurations
                /// </summary>
                /// <param name="estimateTypeId">Selected estimate types identifiers</param>
                /// <param name="totalEstimateConfId">Selected total estimate configurations identifiers</param>
                /// <returns>
                /// List of estimate configurations
                /// for selected estimate types and total estimate configurations
                /// </returns>
                public EstimateConfList Reduce(
                    List<int> estimateTypeId = null,
                    List<int> totalEstimateConfId = null)
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();

                    if ((estimateTypeId != null) && estimateTypeId.Count != 0)
                    {
                        rows = rows.Where(a => estimateTypeId.Contains<int>(
                                value: (int)(a.Field<Nullable<int>>(columnName: ColEstimateTypeId.Name) ?? 0)));
                    }

                    if ((totalEstimateConfId != null) && totalEstimateConfId.Count != 0)
                    {
                        rows = rows.Where(a => totalEstimateConfId.Contains<int>(
                                value: (int)(a.Field<Nullable<int>>(columnName: ColTotalEstimateConfId.Name) ?? 0)));
                    }

                    return
                        rows.Any() ?
                        new EstimateConfList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new EstimateConfList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                #endregion Methods

            }

        }
    }
}