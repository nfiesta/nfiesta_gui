﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {
            // t_aux_total

            /// <summary>
            /// Total of auxiliary variable within estimation cell
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class AuxTotal(
                AuxTotalList composite = null,
                DataRow data = null)
                    : ADataTableEntry<AuxTotalList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Total of auxiliary variable within estimation cell identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: AuxTotalList.ColId.Name,
                            defaultValue: Int32.Parse(s: AuxTotalList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: AuxTotalList.ColId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Estimation cell identifier
                /// </summary>
                public int EstimationCellId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: AuxTotalList.ColEstimationCellId.Name,
                            defaultValue: Int32.Parse(s: AuxTotalList.ColEstimationCellId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: AuxTotalList.ColEstimationCellId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Estimation cell object (read-only)
                /// </summary>
                public EstimationCell EstimationCell
                {
                    get
                    {
                        return ((NfiEstaDB)Composite.Database).SNfiEsta.CEstimationCell[EstimationCellId];
                    }
                }


                /// <summary>
                /// Total of auxilliary local density
                /// </summary>
                public Nullable<double> Val
                {
                    get
                    {
                        return Functions.GetNDoubleArg(
                            row: Data,
                            name: AuxTotalList.ColVal.Name,
                            defaultValue: String.IsNullOrEmpty(value: AuxTotalList.ColVal.DefaultValue) ?
                                (Nullable<double>)null :
                                Double.Parse(s: AuxTotalList.ColVal.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNDoubleArg(
                            row: Data,
                            name: AuxTotalList.ColVal.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Combination of target variable and sub-population category and area domain category identifier
                /// </summary>
                public Nullable<int> VariableId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: AuxTotalList.ColVariableId.Name,
                            defaultValue: String.IsNullOrEmpty(value: AuxTotalList.ColVariableId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: AuxTotalList.ColVariableId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: AuxTotalList.ColVariableId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Combination of target variable and sub-population category and area domain category object (read-only)
                /// </summary>
                public Variable Variable
                {
                    get
                    {
                        return (VariableId != null) ?
                             ((NfiEstaDB)Composite.Database).SNfiEsta.TVariable[(int)VariableId] : null;
                    }
                }

                /// <summary>
                /// Combination of target variable and sub-population category and area domain category (extended-version) object (read-only)
                /// </summary>
                public VwVariable VwVariable
                {
                    get
                    {
                        return (VariableId != null) ?
                             ((NfiEstaDB)Composite.Database).SNfiEsta.VVariable[(int)VariableId] : null;
                    }
                }


                /// <summary>
                /// Time when value of auxiliary variable total was written in database
                /// </summary>
                public Nullable<DateTime> AuxTotalInserted
                {
                    get
                    {
                        return Functions.GetNDateTimeArg(
                            row: Data,
                            name: AuxTotalList.ColAuxTotalInserted.Name,
                            defaultValue: String.IsNullOrEmpty(value: AuxTotalList.ColAuxTotalInserted.DefaultValue) ?
                                            (Nullable<DateTime>)null :
                                            DateTime.Parse(s: AuxTotalList.ColAuxTotalInserted.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNDateTimeArg(
                            row: Data,
                            name: AuxTotalList.ColAuxTotalInserted.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Time when value of auxiliary variable total was written in database as text
                /// </summary>
                public string AuxTotalInsertedText
                {
                    get
                    {
                        return
                            (AuxTotalInserted == null) ?
                                Functions.StrNull :
                                ((DateTime)AuxTotalInserted).ToString(
                                    format: AuxTotalList.ColAuxTotalInserted.NumericFormat);
                    }
                }

                /// <summary>
                /// Flag for identification actual value of auxiliary variable total
                /// </summary>
                public bool IsLatest
                {
                    get
                    {
                        return Functions.GetBoolArg(
                            row: Data,
                            name: AuxTotalList.ColIsLatest.Name,
                            defaultValue: Boolean.Parse(value: AuxTotalList.ColIsLatest.DefaultValue));
                    }
                    set
                    {
                        Functions.SetBoolArg(
                            row: Data,
                            name: AuxTotalList.ColIsLatest.Name,
                            val: value);
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not AuxTotal Type, return False
                    if (obj is not AuxTotal)
                    {
                        return false;
                    }

                    return
                        Id == ((AuxTotal)obj).Id;
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                #endregion Methods

            }


            /// <summary>
            /// List of totals of auxiliary variable within estimation cell
            /// </summary>
            public class AuxTotalList
                : ADataTable
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema = NfiEstaSchema.Name;

                /// <summary>
                /// Table name
                /// </summary>
                public const string Name = "t_aux_total";

                /// <summary>
                /// Table alias
                /// </summary>
                public const string Alias = "t_aux_total";

                /// <summary>
                /// Description in national language
                /// </summary>
                public const string CaptionCs = "Seznam úhrnů pomocných proměnných uvnitř výpočetních buněk";

                /// <summary>
                /// Description in English
                /// </summary>
                public const string CaptionEn = "List of totals of auxiliary variable within estimation cell";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
                {
                    { "id", new ColumnMetadata()
                    {
                        Name = "id",
                        DbName = "id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = true,
                        NotNull = true,
                        DefaultValue = default(int).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ID",
                        HeaderTextEn = "ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 0
                    }
                    },
                    { "estimation_cell", new ColumnMetadata()
                    {
                        Name = "estimation_cell",
                        DbName = "estimation_cell",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = default(int).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATION_CELL",
                        HeaderTextEn = "ESTIMATION_CELL",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 1
                    }
                    },
                    { "aux_total", new ColumnMetadata()
                    {
                        Name = "aux_total",
                        DbName = "aux_total",
                        DataType = "System.Double",
                        DbDataType = "float8",
                        NewDataType = "float8",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "N3",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "AUX_TOTAL",
                        HeaderTextEn = "AUX_TOTAL",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 2
                    }
                    },
                    { "variable", new ColumnMetadata()
                    {
                        Name = "variable",
                        DbName = "variable",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = default(int).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "VARIABLE",
                        HeaderTextEn = "VARIABLE",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 3
                    }
                    },
                    { "aux_total_inserted", new ColumnMetadata()
                    {
                        Name = "aux_total_inserted",
                        DbName = "aux_total_inserted",
                        DataType = "System.DateTime",
                        DbDataType = "timestamptz",
                        NewDataType = "timestamptz",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "yyyy-MM-dd HH:mm:ss",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "AUX_TOTAL_INSERTED",
                        HeaderTextEn = "AUX_TOTAL_INSERTED",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 4
                    }
                    },
                    { "is_latest", new ColumnMetadata()
                    {
                        Name = "is_latest",
                        DbName = "is_latest",
                        DataType = "System.Boolean",
                        DbDataType = "bool",
                        NewDataType = "bool",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = default(bool).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "IS_LATEST",
                        HeaderTextEn = "IS_LATEST",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 5
                    }
                    }
                };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column estimation_cell metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationCellId = Cols["estimation_cell"];

                /// <summary>
                /// Column aux_total metadata
                /// </summary>
                public static readonly ColumnMetadata ColVal = Cols["aux_total"];

                /// <summary>
                /// Column variable metadata
                /// </summary>
                public static readonly ColumnMetadata ColVariableId = Cols["variable"];

                /// <summary>
                /// Column aux_total_inserted metadata
                /// </summary>
                public static readonly ColumnMetadata ColAuxTotalInserted = Cols["aux_total_inserted"];

                /// <summary>
                /// Column is_latest metadata
                /// </summary>
                public static readonly ColumnMetadata ColIsLatest = Cols["is_latest"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public AuxTotalList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public AuxTotalList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// List of totals of auxiliary variable within estimation cell (read-only)
                /// </summary>
                public List<AuxTotal> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new AuxTotal(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// Total of auxiliary variable within estimation cell from list by identifier (read-only)
                /// </summary>
                /// <param name="id">Total of auxiliary variable within estimation cell identifier</param>
                /// <returns>Total of auxiliary variable within estimation cell from list by identifier (null if not found)</returns>
                public AuxTotal this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new AuxTotal(composite: this, data: a))
                                .FirstOrDefault<AuxTotal>();
                    }
                }

                #endregion Indexer


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public AuxTotalList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new AuxTotalList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new AuxTotalList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }



                /// <summary>
                /// List of totals of auxiliary variable within estimation cell
                /// for variables and estimation cells
                /// </summary>
                /// <param name="variableId">Selected variables identifiers</param>
                /// <param name="estimationCellId">Selected estimation cells identifiers</param>
                /// <param name="isLatest">Identification of the actual value</param>
                /// <returns>
                /// List of totals of auxiliary variable within estimation cell
                /// for variable and estimation cell
                /// </returns>
                public AuxTotalList Reduce(
                    List<int> variableId = null,
                    List<int> estimationCellId = null,
                    Nullable<bool> isLatest = null)
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();

                    if ((variableId != null) && variableId.Count != 0)
                    {
                        rows = rows.Where(a => variableId.Contains<int>(
                                value: (int)(a.Field<Nullable<int>>(columnName: ColVariableId.Name) ?? 0)));
                    }

                    if ((estimationCellId != null) && estimationCellId.Count != 0)
                    {
                        rows = rows.Where(a => estimationCellId.Contains<int>(
                                value: (int)(a.Field<Nullable<int>>(columnName: ColEstimationCellId.Name) ?? 0)));
                    }

                    if (isLatest != null)
                    {
                        rows = rows.Where(a => (bool)(a.Field<Nullable<bool>>(columnName: ColIsLatest.Name) ?? false));
                    }

                    return
                        rows.Any() ?
                        new AuxTotalList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new AuxTotalList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                #endregion Methods

            }

        }
    }
}
