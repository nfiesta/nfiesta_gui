﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {
            // t_additivity_set_plot

            /// <summary>
            /// Additivity check
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class AdditivitySetPlot(
                AdditivitySetPlotList composite = null,
                DataRow data = null)
                    : ADataTableEntry<AdditivitySetPlotList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Additivity check identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: AdditivitySetPlotList.ColId.Name,
                            defaultValue: Int32.Parse(s: AdditivitySetPlotList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: AdditivitySetPlotList.ColId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Available datasets identifier
                /// </summary>
                public Nullable<int> AvailableDataSetsId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: AdditivitySetPlotList.ColAvailableDataSets.Name,
                            defaultValue: String.IsNullOrEmpty(value: AdditivitySetPlotList.ColAvailableDataSets.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: AdditivitySetPlotList.ColAvailableDataSets.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: AdditivitySetPlotList.ColAvailableDataSets.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Available datasets identifier object (read-only)
                /// </summary>
                public AvailableDataSet AvailableDataSet
                {
                    get
                    {
                        return (AvailableDataSetsId != null) ?
                             ((NfiEstaDB)Composite.Database).SNfiEsta.TAvailableDataSet[(int)AvailableDataSetsId] : null;
                    }
                }

                /// <summary>
                ///
                /// </summary>
                public string Edges
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: AdditivitySetPlotList.ColEdges.Name,
                            defaultValue: AdditivitySetPlotList.ColEdges.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: AdditivitySetPlotList.ColEdges.Name,
                            val: value);
                    }
                }

                /// <summary>
                ///
                /// </summary>
                public List<int> EdgesList
                {
                    get
                    {
                        return Functions.StringToIntList(
                            text: Edges,
                            separator: (char)59,
                            defaultValue: null);
                    }
                }

                /// <summary>
                ///
                /// </summary>
                public Nullable<double> AddCheckThreshold
                {
                    get
                    {
                        return Functions.GetNDoubleArg(
                            row: Data,
                            name: AdditivitySetPlotList.ColAddCheckThreshold.Name,
                            defaultValue: String.IsNullOrEmpty(value: AdditivitySetPlotList.ColAddCheckThreshold.DefaultValue) ?
                                (Nullable<double>)null :
                                Double.Parse(s: AdditivitySetPlotList.ColAddCheckThreshold.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNDoubleArg(
                            row: Data,
                            name: AdditivitySetPlotList.ColAddCheckThreshold.Name,
                            val: value);
                    }
                }

                /// <summary>
                ///
                /// </summary>
                public Nullable<DateTime> ValidFrom
                {
                    get
                    {
                        return Functions.GetNDateTimeArg(
                            row: Data,
                            name: AdditivitySetPlotList.ColValidFrom.Name,
                            defaultValue: String.IsNullOrEmpty(value: AdditivitySetPlotList.ColValidFrom.DefaultValue) ?
                                            (Nullable<DateTime>)null :
                                            DateTime.Parse(s: AdditivitySetPlotList.ColValidFrom.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNDateTimeArg(
                            row: Data,
                            name: AdditivitySetPlotList.ColValidFrom.Name,
                            val: value);
                    }
                }

                /// <summary>
                ///
                /// </summary>
                public string ValidFromText
                {
                    get
                    {
                        return
                            (ValidFrom == null) ?
                                Functions.StrNull :
                                ((DateTime)ValidFrom).ToString(
                                    format: AdditivitySetPlotList.ColValidFrom.NumericFormat);
                    }
                }

                /// <summary>
                ///
                /// </summary>
                public string DbgData
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: AdditivitySetPlotList.ColDbgData.Name,
                            defaultValue: AdditivitySetPlotList.ColDbgData.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: AdditivitySetPlotList.ColDbgData.Name,
                            val: value);
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not AuxConf Type, return False
                    if (obj is not AdditivitySetPlot)
                    {
                        return false;
                    }

                    return
                        Id == ((AdditivitySetPlot)obj).Id;
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                #endregion Methods

            }


            /// <summary>
            /// List of additivity checks
            /// </summary>
            public class AdditivitySetPlotList
                : ADataTable
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema = NfiEstaSchema.Name;

                /// <summary>
                /// Table name
                /// </summary>
                public const string Name = "t_additivity_set_plot";

                /// <summary>
                /// Table alias
                /// </summary>
                public const string Alias = "t_additivity_set_plot";

                /// <summary>
                /// Description in national language
                /// </summary>
                public const string CaptionCs = "Seznam spustěných kontrol aditivity";

                /// <summary>
                /// Description in English
                /// </summary>
                public const string CaptionEn = "List of additivity checks triggers";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
                {
                    { "id", new ColumnMetadata()
                    {
                        Name = "id",
                        DbName = "id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = true,
                        NotNull = true,
                        DefaultValue = default(int).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ID",
                        HeaderTextEn = "ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 0
                    }
                    },
                    { "available_datasets", new ColumnMetadata()
                    {
                        Name = "available_datasets",
                        DbName = "available_datasets",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "AVAILABLE_DATASETS",
                        HeaderTextEn = "AVAILABLE_DATASETS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 1
                    }
                    },
                    { "edges", new ColumnMetadata()
                    {
                        Name = "edges",
                        DbName = "edges",
                        DataType = "System.String",
                        DbDataType = "_int4",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = "array_to_string({0}, ';', 'NULL')",
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "EDGES",
                        HeaderTextEn = "EDGES",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 2
                    }
                    },
                    { "add_check_threshold", new ColumnMetadata()
                    {
                        Name = "add_check_threshold",
                        DbName = "add_check_threshold",
                        DataType = "System.Double",
                        DbDataType = "float8",
                        NewDataType = "float8",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "N3",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ADD_CHECK_THRESHOLD",
                        HeaderTextEn = "ADD_CHECK_THRESHOLD",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 3
                    }
                    },
                    { "valid_from", new ColumnMetadata()
                    {
                        Name = "valid_from",
                        DbName = "valid_from",
                        DataType = "System.DateTime",
                        DbDataType = "timestamptz",
                        NewDataType = "timestamptz",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "yyyy-MM-dd HH:mm:ss",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "VALID_FROM",
                        HeaderTextEn = "VALID_FROM",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 4
                    }
                    },
                    { "dbg_data", new ColumnMetadata()
                    {
                        Name = "dbg_data",
                        DbName = "dbg_data",
                        DataType = "System.String",
                        DbDataType = "jsonb",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "DBG_DATA",
                        HeaderTextEn = "DBG_DATA",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 5
                    }
                    }
                };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column available_datasets metadata
                /// </summary>
                public static readonly ColumnMetadata ColAvailableDataSets = Cols["available_datasets"];

                /// <summary>
                /// Column edges metadata
                /// </summary>
                public static readonly ColumnMetadata ColEdges = Cols["edges"];

                /// <summary>
                /// Column add_check_threshold metadata
                /// </summary>
                public static readonly ColumnMetadata ColAddCheckThreshold = Cols["add_check_threshold"];

                /// <summary>
                /// Column valid_from metadata
                /// </summary>
                public static readonly ColumnMetadata ColValidFrom = Cols["valid_from"];

                /// <summary>
                /// Column dbg_data metadata
                /// </summary>
                public static readonly ColumnMetadata ColDbgData = Cols["dbg_data"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public AdditivitySetPlotList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public AdditivitySetPlotList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// List of additivity checks (read-only)
                /// </summary>
                public List<AdditivitySetPlot> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new AdditivitySetPlot(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// Additivity checks from list by identifier (read-only)
                /// </summary>
                /// <param name="id">Additivity checks identifier</param>
                /// <returns>Additivity checks from list by identifier (null if not found)</returns>
                public AdditivitySetPlot this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new AdditivitySetPlot(composite: this, data: a))
                                .FirstOrDefault<AdditivitySetPlot>();
                    }
                }

                #endregion Indexer


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public AdditivitySetPlotList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new AdditivitySetPlotList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new AdditivitySetPlotList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                /// <summary>
                /// List of additivity checks
                /// for available datasets
                /// </summary>
                /// <param name="availableDataSetsId">Selected available datasets identifiers</param>
                /// <returns>
                /// List of additivity checks
                /// for available datasets
                /// </returns>
                public AdditivitySetPlotList Reduce(
                    List<int> availableDataSetsId = null)
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();

                    if ((availableDataSetsId != null) && availableDataSetsId.Count != 0)
                    {
                        rows = rows.Where(a => availableDataSetsId.Contains<int>(
                                value: (int)(a.Field<Nullable<int>>(columnName: ColAvailableDataSets.Name) ?? 0)));
                    }

                    return
                        rows.Any() ?
                        new AdditivitySetPlotList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new AdditivitySetPlotList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                #endregion Methods

            }

        }
    }
}
