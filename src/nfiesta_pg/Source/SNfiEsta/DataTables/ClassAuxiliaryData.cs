﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {
            // t_auxiliary_data

            /// <summary>
            /// Auxiliary plot data
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class AuxData(
                AuxDataList composite = null,
                DataRow data = null)
                    : ADataTableEntry<AuxDataList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Auxiliary plot data identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: AuxDataList.ColId.Name,
                            defaultValue: Int32.Parse(s: AuxDataList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: AuxDataList.ColId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Inventory plot identifier
                /// </summary>
                public int PlotId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: AuxDataList.ColPlotId.Name,
                            defaultValue: Int32.Parse(s: AuxDataList.ColPlotId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: AuxDataList.ColPlotId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Inventory plot object
                /// </summary>
                public SDesign.Plot Plot
                {
                    get
                    {
                        return ((NfiEstaDB)Composite.Database).SDesign.FpPlot[PlotId];
                    }
                }


                /// <summary>
                /// Value of auxiliary variable on inventory plot
                /// </summary>
                public double AuxValue
                {
                    get
                    {
                        return Functions.GetDoubleArg(
                            row: Data,
                            name: AuxDataList.ColAuxValue.Name,
                            defaultValue: Double.Parse(s: AuxDataList.ColAuxValue.DefaultValue));
                    }
                    set
                    {
                        Functions.SetDoubleArg(
                            row: Data,
                            name: AuxDataList.ColAuxValue.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Time when value of auxiliary variable was written in database
                /// </summary>
                public Nullable<DateTime> ValueInserted
                {
                    get
                    {
                        return Functions.GetNDateTimeArg(
                            row: Data,
                            name: AuxDataList.ColValueInserted.Name,
                            defaultValue: String.IsNullOrEmpty(value: AuxDataList.ColValueInserted.DefaultValue) ?
                                            (Nullable<DateTime>)null :
                                            DateTime.Parse(s: AuxDataList.ColValueInserted.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNDateTimeArg(
                            row: Data,
                            name: AuxDataList.ColValueInserted.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Time when value of auxiliary variable was written in database as text
                /// </summary>
                public string ValueInsertedText
                {
                    get
                    {
                        return
                            (ValueInserted == null) ?
                                Functions.StrNull :
                                ((DateTime)ValueInserted).ToString(
                                    format: AuxDataList.ColValueInserted.NumericFormat);
                    }
                }

                /// <summary>
                /// Flag for identification actual value of auxiliary variable on inventory plot
                /// </summary>
                public bool IsLatest
                {
                    get
                    {
                        return Functions.GetBoolArg(
                            row: Data,
                            name: AuxDataList.ColIsLatest.Name,
                            defaultValue: Boolean.Parse(value: AuxDataList.ColIsLatest.DefaultValue));
                    }
                    set
                    {
                        Functions.SetBoolArg(
                            row: Data,
                            name: AuxDataList.ColIsLatest.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Available dataset identifier
                /// </summary>
                public int AvailableDataSetsId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: AuxDataList.ColAvailableDataSets.Name,
                            defaultValue: Int32.Parse(s: AuxDataList.ColAvailableDataSets.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: AuxDataList.ColAvailableDataSets.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Available dataset object
                /// </summary>
                public AvailableDataSet AvailableDataSet
                {
                    get
                    {
                        return ((NfiEstaDB)Composite.Database).SNfiEsta
                            .TAvailableDataSet[AvailableDataSetsId];
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not AuxData Type, return False
                    if (obj is not AuxData)
                    {
                        return false;

                    }

                    return
                        Id == ((AuxData)obj).Id;
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                #endregion Methods

            }


            /// <summary>
            /// List of auxiliary plot data
            /// </summary>
            public class AuxDataList
                : ADataTable
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema = NfiEstaSchema.Name;

                /// <summary>
                /// Table name
                /// </summary>
                public const string Name = "t_auxiliary_data";

                /// <summary>
                /// Table alias
                /// </summary>
                public const string Alias = "t_auxiliary_data";

                /// <summary>
                /// Description in national language
                /// </summary>
                public const string CaptionCs = "Pomocná data na inventarizačních plochách";

                /// <summary>
                /// Description in English
                /// </summary>
                public const string CaptionEn = "Auxiliary inventory plot data";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
                {
                    { "id", new ColumnMetadata()
                    {
                        Name = "id",
                        DbName = "id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        NotNull = true,
                        PKey = true,
                        DefaultValue = default(int).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ID",
                        HeaderTextEn = "ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 0
                    }
                    },
                    { "plot", new ColumnMetadata()
                    {
                        Name = "plot",
                        DbName = "plot",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = default(int).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PLOT",
                        HeaderTextEn = "PLOT",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 1
                    }
                    },
                    { "aux_value", new ColumnMetadata()
                    {
                        Name = "aux_value",
                        DbName = "value",
                        DataType = "System.Double",
                        DbDataType = "float8",
                        NewDataType = "float8",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = default(double).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "N3",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "VALUE",
                        HeaderTextEn = "VALUE",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 2
                    }
                    },
                    { "value_inserted", new ColumnMetadata()
                    {
                        Name = "value_inserted",
                        DbName = "value_inserted",
                        DataType = "System.DateTime",
                        DbDataType = "timestamptz",
                        NewDataType = "timestamptz",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "yyyy-MM-dd HH:mm:ss",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "VALUE_INSERTED",
                        HeaderTextEn = "VALUE_INSERTED",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 3
                    }
                    },
                    { "is_latest", new ColumnMetadata()
                    {
                        Name = "is_latest",
                        DbName = "is_latest",
                        DataType = "System.Boolean",
                        DbDataType = "bool",
                        NewDataType = "bool",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = default(bool).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "IS_LATEST",
                        HeaderTextEn = "IS_LATEST",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 4
                    }
                    },
                    { "available_datasets", new ColumnMetadata()
                    {
                        Name = "available_datasets",
                        DbName = "available_datasets",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = default(int).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "AVAILABLE_DATASETS",
                        HeaderTextEn = "AVAILABLE_DATASETS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 5
                    }
                    }
                };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column plot metadata
                /// </summary>
                public static readonly ColumnMetadata ColPlotId = Cols["plot"];

                /// <summary>
                /// Column aux_value metadata
                /// </summary>
                public static readonly ColumnMetadata ColAuxValue = Cols["aux_value"];

                /// <summary>
                /// Column value_inserted metadata
                /// </summary>
                public static readonly ColumnMetadata ColValueInserted = Cols["value_inserted"];

                /// <summary>
                /// Column is_latest metadata
                /// </summary>
                public static readonly ColumnMetadata ColIsLatest = Cols["is_latest"];

                /// <summary>
                /// Column available_datasets metadata
                /// </summary>
                public static readonly ColumnMetadata ColAvailableDataSets = Cols["available_datasets"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public AuxDataList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public AuxDataList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// List of auxiliary plot data (read-only)
                /// </summary>
                public List<AuxData> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new AuxData(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// Auxiliary plot data from list by identifier (read-only)
                /// </summary>
                /// <param name="id">Auxiliary plot data identifier</param>
                /// <returns>Auxiliary plot data from list by identifier (null if not found)</returns>
                public AuxData this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new AuxData(composite: this, data: a))
                                .FirstOrDefault<AuxData>();
                    }
                }

                #endregion Indexer


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public AuxDataList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new AuxDataList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new AuxDataList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                /// <summary>
                /// List of auxiliary plot data
                /// for available datasets
                /// </summary>
                /// <param name="availableDataSetsId">Selected available datasets identifiers</param>
                /// <param name="isLatest">Identification of the actual value</param>
                /// <returns>
                /// List of auxiliary plot data
                /// for available datasets
                /// </returns>
                public AuxDataList Reduce(
                    List<int> availableDataSetsId = null,
                    Nullable<bool> isLatest = null)
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();

                    if ((availableDataSetsId != null) && availableDataSetsId.Count != 0)
                    {
                        rows = rows.Where(a => availableDataSetsId.Contains<int>(
                                value: (int)(a.Field<Nullable<int>>(columnName: ColAvailableDataSets.Name) ?? 0)));
                    }

                    if (isLatest != null)
                    {
                        rows = rows.Where(a => (bool)(a.Field<Nullable<bool>>(columnName: ColIsLatest.Name) ?? false));
                    }

                    return
                        rows.Any() ?
                        new AuxDataList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new AuxDataList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                #endregion Methods

            }

        }
    }
}