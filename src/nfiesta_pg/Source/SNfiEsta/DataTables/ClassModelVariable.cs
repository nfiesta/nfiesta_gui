﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {
            // t_model_variables

            /// <summary>
            /// Explanatory variable used in model
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class ModelVariable(
                ModelVariableList composite = null,
                DataRow data = null)
                    : ADataTableEntry<ModelVariableList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Explanatory variable identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: ModelVariableList.ColId.Name,
                            defaultValue: Int32.Parse(s: ModelVariableList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: ModelVariableList.ColId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Combination of target variable and sub-population category and area domain category identifier
                /// </summary>
                public Nullable<int> VariableId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: ModelVariableList.ColVariableId.Name,
                            defaultValue: String.IsNullOrEmpty(value: ModelVariableList.ColVariableId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: ModelVariableList.ColVariableId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: ModelVariableList.ColVariableId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Combination of target variable and sub-population category and area domain category object (read-only)
                /// </summary>
                public Variable Variable
                {
                    get
                    {
                        return (VariableId != null) ?
                            ((NfiEstaDB)Composite.Database).SNfiEsta.TVariable[(int)VariableId] : null;
                    }
                }

                /// <summary>
                /// Combination of target variable and sub-population category and area domain category (extended-version) object (read-only)
                /// </summary>
                public VwVariable VwVariable
                {
                    get
                    {
                        return (VariableId != null) ?
                            ((NfiEstaDB)Composite.Database).SNfiEsta.VVariable[(int)VariableId] : null;
                    }
                }


                /// <summary>
                /// Model identifier
                /// </summary>
                public Nullable<int> ModelId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: ModelVariableList.ColModelId.Name,
                            defaultValue: String.IsNullOrEmpty(value: ModelVariableList.ColModelId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: ModelVariableList.ColModelId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: ModelVariableList.ColModelId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Model object (read-only)
                /// </summary>
                public Model Model
                {
                    get
                    {
                        return (ModelId != null) ?
                            ((NfiEstaDB)Composite.Database).SNfiEsta.TModel[(int)ModelId] : null;
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not ModelVariable Type, return False
                    if (obj is not ModelVariable)
                    {
                        return false;
                    }

                    return
                        Id == ((ModelVariable)obj).Id;
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                #endregion Methods

            }


            /// <summary>
            /// List of explanatory variables
            /// </summary>
            public class ModelVariableList
                : ADataTable
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema = NfiEstaSchema.Name;

                /// <summary>
                /// Table name
                /// </summary>
                public const string Name = "t_model_variables";

                /// <summary>
                /// Table alias
                /// </summary>
                public const string Alias = "t_model_variables";

                /// <summary>
                /// Description in national language
                /// </summary>
                public const string CaptionCs = "Seznam vysvětlujících proměnných použitých v modelu";

                /// <summary>
                /// Description in English
                /// </summary>
                public const string CaptionEn = "List of explanatory variables used in model";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
                {
                    { "id", new ColumnMetadata()
                    {
                        Name = "id",
                        DbName = "id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = true,
                        NotNull = true,
                        DefaultValue = default(int).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ID",
                        HeaderTextEn = "ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 0
                    }
                    },
                    { "variable", new ColumnMetadata()
                    {
                        Name = "variable",
                        DbName = "variable",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "VARIABLE",
                        HeaderTextEn = "VARIABLE",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 1
                    }
                    },
                    { "model", new ColumnMetadata()
                    {
                        Name = "model",
                        DbName = "model",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "MODEL",
                        HeaderTextEn = "MODEL",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 2
                    }
                    }
                };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column variable metadata
                /// </summary>
                public static readonly ColumnMetadata ColVariableId = Cols["variable"];

                /// <summary>
                /// Column model metadata
                /// </summary>
                public static readonly ColumnMetadata ColModelId = Cols["model"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public ModelVariableList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public ModelVariableList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// List of explanatory variables (read-only)
                /// </summary>
                public List<ModelVariable> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new ModelVariable(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// Explanatory variable from list by identifier (read-only)
                /// </summary>
                /// <param name="id">Explanatory variable identifier</param>
                /// <returns>Explanatory variable from list by identifier (null if not found)</returns>
                public ModelVariable this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new ModelVariable(composite: this, data: a))
                                .FirstOrDefault<ModelVariable>();
                    }
                }

                #endregion Indexer


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public ModelVariableList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new ModelVariableList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new ModelVariableList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                /// <summary>
                /// List of explanatory variables
                /// for selected variables and models
                /// </summary>
                /// <param name="variableId">Selected variables identifiers</param>
                /// <param name="modelId">Selected models identifiers</param>
                /// <returns>
                /// List of explanatory variables
                /// for selected variables and models
                /// </returns>
                public ModelVariableList Reduce(
                    List<int> variableId = null,
                    List<int> modelId = null)
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();

                    if ((variableId != null) && variableId.Count != 0)
                    {
                        rows = rows.Where(a => variableId.Contains<int>(
                                value: (int)(a.Field<Nullable<int>>(columnName: ColVariableId.Name) ?? 0)));
                    }

                    if ((modelId != null) && modelId.Count != 0)
                    {
                        rows = rows.Where(a => modelId.Contains<int>(
                                value: (int)(a.Field<Nullable<int>>(columnName: ColModelId.Name) ?? 0)));
                    }

                    return
                        rows.Any() ?
                        new ModelVariableList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new ModelVariableList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                #endregion Methods

            }

        }
    }
}