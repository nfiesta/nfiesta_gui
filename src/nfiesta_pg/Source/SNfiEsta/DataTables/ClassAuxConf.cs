﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {
            // t_aux_conf

            /// <summary>
            /// Parametrization configuration
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class AuxConf(
                AuxConfList composite = null,
                DataRow data = null)
                    : ADataTableEntry<AuxConfList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Parametrization configuration identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: AuxConfList.ColId.Name,
                            defaultValue: Int32.Parse(s: AuxConfList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: AuxConfList.ColId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Parametrization area identifier
                /// </summary>
                public Nullable<int> ParamAreaId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: AuxConfList.ColParamAreaId.Name,
                            defaultValue: String.IsNullOrEmpty(value: AuxConfList.ColParamAreaId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: AuxConfList.ColParamAreaId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: AuxConfList.ColParamAreaId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Parametrization area object (read-only)
                /// </summary>
                public ParamArea ParamArea
                {
                    get
                    {
                        return (ParamAreaId != null) ?
                             ((NfiEstaDB)Composite.Database).SNfiEsta.FaParamArea[(int)ParamAreaId] : null;
                    }
                }


                /// <summary>
                /// Model identifier
                /// </summary>
                public Nullable<int> ModelId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: AuxConfList.ColModelId.Name,
                            defaultValue: String.IsNullOrEmpty(value: AuxConfList.ColModelId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: AuxConfList.ColModelId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: AuxConfList.ColModelId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Model object (read-only)
                /// </summary>
                public Model Model
                {
                    get
                    {
                        return (ModelId != null) ?
                             ((NfiEstaDB)Composite.Database).SNfiEsta.TModel[(int)ModelId] : null;
                    }
                }


                /// <summary>
                /// Description
                /// </summary>
                public string Description
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: AuxConfList.ColDescription.Name,
                            defaultValue: AuxConfList.ColDescription.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: AuxConfList.ColDescription.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Sigma
                /// </summary>
                public bool Sigma
                {
                    get
                    {
                        return Functions.GetBoolArg(
                            row: Data,
                            name: AuxConfList.ColSigma.Name,
                            defaultValue: Boolean.Parse(value: AuxConfList.ColSigma.DefaultValue));
                    }
                    set
                    {
                        Functions.SetBoolArg(
                            row: Data,
                            name: AuxConfList.ColSigma.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Aggregated sets of panels and corresponding reference year sets identifier
                /// </summary>
                public Nullable<int> PanelRefYearSetGroupId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: AuxConfList.ColPanelRefYearSetGroupId.Name,
                            defaultValue: String.IsNullOrEmpty(value: AuxConfList.ColPanelRefYearSetGroupId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: AuxConfList.ColPanelRefYearSetGroupId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: AuxConfList.ColPanelRefYearSetGroupId.Name,
                            val: value);
                    }
                }

                /// <summary>
                ///  Aggregated sets of panels and corresponding reference year sets object (read-only)
                /// </summary>
                public PanelRefYearSetGroup PanelRefYearSetGroup
                {
                    get
                    {
                        return (PanelRefYearSetGroupId != null) ?
                             ((NfiEstaDB)Composite.Database).SNfiEsta.CPanelRefYearSetGroup[(int)PanelRefYearSetGroupId] : null;
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not AuxConf Type, return False
                    if (obj is not AuxConf)
                    {
                        return false;
                    }

                    return
                        Id == ((AuxConf)obj).Id;
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                #endregion Methods

            }


            /// <summary>
            /// List of parametrization configurations
            /// </summary>
            public class AuxConfList
                : ADataTable
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema = NfiEstaSchema.Name;

                /// <summary>
                /// Table name
                /// </summary>
                public const string Name = "t_aux_conf";

                /// <summary>
                /// Table alias
                /// </summary>
                public const string Alias = "t_aux_conf";

                /// <summary>
                /// Description in national language
                /// </summary>
                public const string CaptionCs = "Seznam kombinací parametrizační oblasti a modelu";

                /// <summary>
                /// Description in English
                /// </summary>
                public const string CaptionEn = "List of parametrization configurations - combination of parametrization area and model (explanatory variables)";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
                {
                    { "id", new ColumnMetadata()
                    {
                        Name = "id",
                        DbName = "id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = true,
                        NotNull = true,
                        DefaultValue = default(int).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ID",
                        HeaderTextEn = "ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 0
                    }
                    },
                    { "param_area", new ColumnMetadata()
                    {
                        Name = "param_area",
                        DbName = "param_area",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PARAM_AREA",
                        HeaderTextEn = "PARAM_AREA",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 1
                    }
                    },
                    { "model", new ColumnMetadata()
                    {
                        Name = "model",
                        DbName = "model",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "MODEL",
                        HeaderTextEn = "MODEL",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 2
                    }
                    },
                    { "description", new ColumnMetadata()
                    {
                        Name = "description",
                        DbName = "description",
                        DataType = "System.String",
                        DbDataType = "text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "DESCRIPTION",
                        HeaderTextEn = "DESCRIPTION",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 3
                    }
                    },
                    { "sigma", new ColumnMetadata()
                    {
                        Name = "sigma",
                        DbName = "sigma",
                        DataType = "System.Boolean",
                        DbDataType = "bool",
                        NewDataType = "bool",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = default(bool).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "SIGMA",
                        HeaderTextEn = "SIGMA",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 4
                    }
                    },
                    { "panel_refyearset_group", new ColumnMetadata()
                    {
                        Name = "panel_refyearset_group",
                        DbName = "panel_refyearset_group",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = default(int).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PANEL_REFYEARSET_GROUP",
                        HeaderTextEn = "PANEL_REFYEARSET_GROUP",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 5
                    }
                    }
                };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column param_area metadata
                /// </summary>
                public static readonly ColumnMetadata ColParamAreaId = Cols["param_area"];

                /// <summary>
                /// Column model metadata
                /// </summary>
                public static readonly ColumnMetadata ColModelId = Cols["model"];

                /// <summary>
                /// Column description metadata
                /// </summary>
                public static readonly ColumnMetadata ColDescription = Cols["description"];

                /// <summary>
                /// Column sigma metadata
                /// </summary>
                public static readonly ColumnMetadata ColSigma = Cols["sigma"];

                /// <summary>
                /// Column panel_refyearset_group metadata
                /// </summary>
                public static readonly ColumnMetadata ColPanelRefYearSetGroupId = Cols["panel_refyearset_group"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public AuxConfList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public AuxConfList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// List of parametrization configurations (read-only)
                /// </summary>
                public List<AuxConf> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new AuxConf(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// Parametrization configuration from list by identifier (read-only)
                /// </summary>
                /// <param name="id">Parametrization configuration identifier</param>
                /// <returns>Parametrization configuration from list by identifier (null if not found)</returns>
                public AuxConf this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new AuxConf(composite: this, data: a))
                                .FirstOrDefault<AuxConf>();
                    }
                }

                #endregion Indexer


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public AuxConfList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new AuxConfList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new AuxConfList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                /// <summary>
                /// List of parametrization configurations
                /// for models, parametrization areas and panel reference year set groups
                /// </summary>
                /// <param name="modelId">Selected models identifiers</param>
                /// <param name="paramAreaId">Selected parametrization areas identifiers</param>
                /// <param name="panelRefYearSetGroupId">Selected panel reference year set groups identifiers</param>
                /// <returns>
                /// List of parametrization configurations
                /// for models, parametrization areas and panel reference year set groups
                /// </returns>
                public AuxConfList Reduce(
                    List<int> modelId = null,
                    List<int> paramAreaId = null,
                    List<int> panelRefYearSetGroupId = null)
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();

                    if ((modelId != null) && modelId.Count != 0)
                    {
                        rows = rows.Where(a => modelId.Contains<int>(
                                value: (int)(a.Field<Nullable<int>>(columnName: ColModelId.Name) ?? 0)));
                    }

                    if ((paramAreaId != null) && paramAreaId.Count != 0)
                    {
                        rows = rows.Where(a => paramAreaId.Contains<int>(
                                value: (int)(a.Field<Nullable<int>>(columnName: ColParamAreaId.Name) ?? 0)));
                    }

                    if ((panelRefYearSetGroupId != null) && panelRefYearSetGroupId.Count != 0)
                    {
                        rows = rows.Where(a => panelRefYearSetGroupId.Contains<int>(
                                value: (int)(a.Field<Nullable<int>>(columnName: ColPanelRefYearSetGroupId.Name) ?? 0)));
                    }

                    return
                        rows.Any() ?
                        new AuxConfList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new AuxConfList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                #endregion Methods

            }

        }
    }
}