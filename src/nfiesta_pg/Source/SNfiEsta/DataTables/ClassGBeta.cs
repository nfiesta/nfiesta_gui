﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {
            // t_g_beta

            /// <summary>
            /// Precalculated G_Beta for given auxiliary configuration
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class GBeta(
                GBetaList composite = null,
                DataRow data = null)
                    : ADataTableEntry<GBetaList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Precalculated G_Beta identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: GBetaList.ColId.Name,
                            defaultValue: Int32.Parse(s: GBetaList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: GBetaList.ColId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Parametrization configuration identifier
                /// </summary>
                public Nullable<int> AuxConfId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: GBetaList.ColAuxConfId.Name,
                            defaultValue: String.IsNullOrEmpty(value: GBetaList.ColAuxConfId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: GBetaList.ColAuxConfId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: GBetaList.ColAuxConfId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Parametrization configuration object (read-only)
                /// </summary>
                public AuxConf AuxConf
                {
                    get
                    {
                        return (AuxConfId != null) ?
                             ((NfiEstaDB)Composite.Database).SNfiEsta.TAuxConf[(int)AuxConfId] : null;
                    }
                }


                /// <summary>
                /// Combination of target variable and sub-population category and area domain category identifier
                /// </summary>
                public Nullable<int> VariableId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: GBetaList.ColVariableId.Name,
                            defaultValue: String.IsNullOrEmpty(value: GBetaList.ColVariableId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: GBetaList.ColVariableId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: GBetaList.ColVariableId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Combination of target variable and sub-population category and area domain category object
                /// </summary>
                public Variable Variable
                {
                    get
                    {
                        return (VariableId != null) ?
                            ((NfiEstaDB)Composite.Database).SNfiEsta.TVariable[(int)VariableId] : null;
                    }
                }

                /// <summary>
                /// Combination of target variable and sub-population category and area domain category (extended-version) object (read-only)
                /// </summary>
                public VwVariable VwVariable
                {
                    get
                    {
                        return (VariableId != null) ?
                            ((NfiEstaDB)Composite.Database).SNfiEsta.VVariable[(int)VariableId] : null;
                    }
                }


                /// <summary>
                /// Sampling cluster identifier
                /// </summary>
                public Nullable<int> ClusterId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: GBetaList.ColClusterId.Name,
                            defaultValue: String.IsNullOrEmpty(GBetaList.ColClusterId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: GBetaList.ColClusterId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: GBetaList.ColClusterId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Sampling cluster object
                /// </summary>
                public SDesign.Cluster Cluster
                {
                    get
                    {
                        return (ClusterId != null) ?
                            ((NfiEstaDB)Composite.Database).SDesign.TCluster[(int)ClusterId] : null;
                    }
                }


                /// <summary>
                /// G_Beta value
                /// </summary>
                public Nullable<double> Val
                {
                    get
                    {
                        return Functions.GetNDoubleArg(
                            row: Data,
                            name: GBetaList.ColVal.Name,
                            defaultValue: String.IsNullOrEmpty(value: GBetaList.ColVal.DefaultValue) ?
                                (Nullable<double>)null :
                                Double.Parse(s: GBetaList.ColVal.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNDoubleArg(
                            row: Data,
                            name: GBetaList.ColVal.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Time when value of G_Beta was written in database
                /// </summary>
                public Nullable<DateTime> ValueInserted
                {
                    get
                    {
                        return Functions.GetNDateTimeArg(
                            row: Data,
                            name: GBetaList.ColValueInserted.Name,
                            defaultValue: String.IsNullOrEmpty(value: GBetaList.ColValueInserted.DefaultValue) ?
                                            (Nullable<DateTime>)null :
                                            DateTime.Parse(s: GBetaList.ColValueInserted.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNDateTimeArg(
                            row: Data,
                            name: GBetaList.ColValueInserted.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Time when value of G_Beta was written in database as text
                /// </summary>
                public string ValueInsertedText
                {
                    get
                    {
                        return
                            (ValueInserted == null) ?
                                Functions.StrNull :
                                ((DateTime)ValueInserted).ToString(
                                    format: GBetaList.ColValueInserted.NumericFormat);
                    }
                }

                /// <summary>
                /// Flag for identification actual value of G_Beta
                /// </summary>
                public bool IsLatest
                {
                    get
                    {
                        return Functions.GetBoolArg(
                            row: Data,
                            name: GBetaList.ColIsLatest.Name,
                            defaultValue: Boolean.Parse(value: GBetaList.ColIsLatest.DefaultValue));
                    }
                    set
                    {
                        Functions.SetBoolArg(
                            row: Data,
                            name: GBetaList.ColIsLatest.Name,
                            val: value);
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not GBeta Type, return False
                    if (obj is not GBeta)
                    {
                        return false;
                    }

                    return
                        Id == ((GBeta)obj).Id;
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                #endregion Methods

            }


            /// <summary>
            /// List of precalculated G_Beta for given auxiliary configuration
            /// </summary>
            public class GBetaList
                : ADataTable
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema = NfiEstaSchema.Name;

                /// <summary>
                /// Table name
                /// </summary>
                public const string Name = "t_g_beta";

                /// <summary>
                /// Table alias
                /// </summary>
                public const string Alias = "t_g_beta";

                /// <summary>
                /// Description in national language
                /// </summary>
                public const string CaptionCs = "Předvypočtené hodnoty G_Beta pro kombinaci parametrizační oblasti a modelu";

                /// <summary>
                /// Description in English
                /// </summary>
                public const string CaptionEn = "List of precalculated G_Beta for given auxiliary configuration";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
                {
                    { "id", new ColumnMetadata()
                    {
                        Name = "id",
                        DbName = "id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = true,
                        NotNull = true,
                        DefaultValue = default(int).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ID",
                        HeaderTextEn = "ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 0
                    }
                    },
                    { "aux_conf", new ColumnMetadata()
                    {
                        Name = "aux_conf",
                        DbName = "aux_conf",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "AUX_CONF",
                        HeaderTextEn = "AUX_CONF",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 1
                    }
                    },
                    { "variable", new ColumnMetadata()
                    {
                        Name = "variable",
                        DbName = "variable",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "VARIABLE",
                        HeaderTextEn = "VARIABLE",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 2
                    }
                    },
                    { "cluster", new ColumnMetadata()
                    {
                        Name = "cluster",
                        DbName = "cluster",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "CLUSTER",
                        HeaderTextEn = "CLUSTER",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 3
                    }
                    },
                    { "val", new ColumnMetadata()
                    {
                        Name = "val",
                        DbName = "val",
                        DataType = "System.Double",
                        DbDataType = "float8",
                        NewDataType = "float8",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "N3",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "VAL",
                        HeaderTextEn = "VAL",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 4
                    }
                    },
                    { "value_inserted", new ColumnMetadata()
                    {
                        Name = "value_inserted",
                        DbName = "value_inserted",
                        DataType = "System.DateTime",
                        DbDataType = "timestamptz",
                        NewDataType = "timestamptz",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "yyyy-MM-dd HH:mm:ss",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "VALUE_INSERTED",
                        HeaderTextEn = "VALUE_INSERTED",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 5
                    }
                    },
                    { "is_latest", new ColumnMetadata()
                    {
                        Name = "is_latest",
                        DbName = "is_latest",
                        DataType = "System.Boolean",
                        DbDataType = "bool",
                        NewDataType = "bool",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = default(bool).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "IS_LATEST",
                        HeaderTextEn = "IS_LATEST",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 6
                    }
                    }
                };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column aux_conf metadata
                /// </summary>
                public static readonly ColumnMetadata ColAuxConfId = Cols["aux_conf"];

                /// <summary>
                /// Column variable metadata
                /// </summary>
                public static readonly ColumnMetadata ColVariableId = Cols["variable"];

                /// <summary>
                /// Column cluster metadata
                /// </summary>
                public static readonly ColumnMetadata ColClusterId = Cols["cluster"];

                /// <summary>
                /// Column val metadata
                /// </summary>
                public static readonly ColumnMetadata ColVal = Cols["val"];

                /// <summary>
                /// Column value_inserted metadata
                /// </summary>
                public static readonly ColumnMetadata ColValueInserted = Cols["value_inserted"];

                /// <summary>
                /// Column is_latest metadata
                /// </summary>
                public static readonly ColumnMetadata ColIsLatest = Cols["is_latest"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public GBetaList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public GBetaList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// List of precalculated G_Beta (read-only)
                /// </summary>
                public List<GBeta> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new GBeta(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// Precalculated G_Beta from list by identifier (read-only)
                /// </summary>
                /// <param name="id">Precalculated G_Beta identifier</param>
                /// <returns>Precalculated G_Beta from list by identifier (null if not found)</returns>
                public GBeta this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new GBeta(composite: this, data: a))
                                .FirstOrDefault<GBeta>();
                    }
                }

                #endregion Indexer


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public GBetaList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new GBetaList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new GBetaList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                /// <summary>
                /// List of precalculated G_Beta
                /// for given auxiliary configurations, selected variables and clusters
                /// </summary>
                /// <param name="auxConfId">Selected auxiliary configurations identifiers</param>
                /// <param name="variableId">Selected variables identifiers</param>
                /// <param name="clusterId">Selected clusters identifiers</param>
                /// <param name="isLatest">Identification of the actual value</param>
                /// <returns>
                /// List of precalculated G_Beta
                /// for given auxiliary configurations, selected variables and clusters
                /// </returns>
                public AuxTotalList Reduce(
                    List<int> auxConfId = null,
                    List<int> variableId = null,
                    List<int> clusterId = null,
                    Nullable<bool> isLatest = null)
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();

                    if ((auxConfId != null) && auxConfId.Count != 0)
                    {
                        rows = rows.Where(a => auxConfId.Contains<int>(
                                value: (int)(a.Field<Nullable<int>>(columnName: ColAuxConfId.Name) ?? 0)));
                    }

                    if ((variableId != null) && variableId.Count != 0)
                    {
                        rows = rows.Where(a => variableId.Contains<int>(
                                value: (int)(a.Field<Nullable<int>>(columnName: ColVariableId.Name) ?? 0)));
                    }

                    if ((clusterId != null) && clusterId.Count != 0)
                    {
                        rows = rows.Where(a => clusterId.Contains<int>(
                                value: (int)(a.Field<Nullable<int>>(columnName: ColClusterId.Name) ?? 0)));
                    }

                    if (isLatest != null)
                    {
                        rows = rows.Where(a => (bool)(a.Field<Nullable<bool>>(columnName: ColIsLatest.Name) ?? false));
                    }

                    return
                        rows.Any() ?
                        new AuxTotalList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new AuxTotalList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                #endregion Methods

            }

        }
    }
}