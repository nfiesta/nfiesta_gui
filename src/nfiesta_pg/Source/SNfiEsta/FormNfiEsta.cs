﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.NfiEstaPg.Controls;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;
using static ZaJi.NfiEstaPg.Core.NfiEstaFunctions;

namespace ZaJi.NfiEstaPg.Core
{

    /// <summary>
    /// <para lang="cs">Formulář zobrazení dat extenze nfiesta</para>
    /// <para lang="en">Form for displaying nfiesta extension data</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    public partial class FormNfiEsta
        : Form, INfiEstaControl
    {

        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Omezení počtu řádků</para>
        /// <para lang="en">Row count limit</para>
        /// </summary>
        private Nullable<int> limit;

        private string msgDataExportComplete = String.Empty;
        private string msgNoDifferencesFound = String.Empty;

        #endregion Private Fields


        #region Contructor

        /// <summary>
        /// <para lang="cs">Konstruktor formuláře</para>
        /// <para lang="en">Form constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        public FormNfiEsta(Control controlOwner)
        {
            InitializeComponent();
            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                if (controlOwner is not INfiEstaControl)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(INfiEstaControl)}.",
                        paramName: nameof(ControlOwner));
                }

                return controlOwner;
            }
            set
            {
                if (value is not INfiEstaControl)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(INfiEstaControl)}.",
                        paramName: nameof(ControlOwner));
                }

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables (read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">Omezení počtu řádků</para>
        /// <para lang="en">Row count limit</para>
        /// </summary>
        public Nullable<int> Limit
        {
            get
            {
                return limit;
            }
            set
            {
                limit = value;
            }
        }

        #endregion Properties


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace formuláře</para>
        /// <para lang="en">Initializing the form</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        private void Initialize(Control controlOwner)
        {
            ControlOwner = controlOwner;
            Limit = null;

            InitializeLabels();

            btnClose.Click += new EventHandler(
                (sender, e) =>
                {
                    DialogResult = DialogResult.Cancel;
                    Close();
                });

            tsmiCAreaDomain.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.Core.AreaDomainList(database: Database),
                        columns: ZaJi.NfiEstaPg.Core.AreaDomainList.Cols);
                });

            tsmiCAreaDomainCategory.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.Core.AreaDomainCategoryList(database: Database),
                        columns: ZaJi.NfiEstaPg.Core.AreaDomainCategoryList.Cols);
                });

            tsmiCAuxiliaryVariable.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.Core.AuxiliaryVariableList(database: Database),
                        columns: ZaJi.NfiEstaPg.Core.AuxiliaryVariableList.Cols);
                });

            tsmiCAuxiliaryVariableCategory.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.Core.AuxiliaryVariableCategoryList(database: Database),
                        columns: ZaJi.NfiEstaPg.Core.AuxiliaryVariableCategoryList.Cols);
                });

            tsmiCEstimateConfStatus.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.Core.EstimateConfStatusList(database: Database),
                        columns: ZaJi.NfiEstaPg.Core.EstimateConfStatusList.Cols);
                });

            tsmiCEstimateType.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.Core.EstimateTypeList(database: Database),
                        columns: ZaJi.NfiEstaPg.Core.EstimateTypeList.Cols);
                });

            tsmiCEstimationCell.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.Core.EstimationCellList(database: Database),
                        columns: ZaJi.NfiEstaPg.Core.EstimationCellList.Cols);
                });

            tsmiCEstimationCellCollection.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.Core.EstimationCellCollectionList(database: Database),
                        columns: ZaJi.NfiEstaPg.Core.EstimationCellCollectionList.Cols);
                });

            tsmiCEstimationPeriod.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.Core.EstimationPeriodList(database: Database),
                        columns: ZaJi.NfiEstaPg.Core.EstimationPeriodList.Cols);
                });

            tsmiCPanelRefYearSetGroup.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.Core.PanelRefYearSetGroupList(database: Database),
                        columns: ZaJi.NfiEstaPg.Core.PanelRefYearSetGroupList.Cols);
                });

            tsmiCParamAreaType.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.Core.ParamAreaTypeList(database: Database),
                        columns: ZaJi.NfiEstaPg.Core.ParamAreaTypeList.Cols);
                });

            tsmiCPhaseEstimateType.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.Core.PhaseEstimateTypeList(database: Database),
                        columns: ZaJi.NfiEstaPg.Core.PhaseEstimateTypeList.Cols);
                });

            tsmiCSubPopulation.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.Core.SubPopulationList(database: Database),
                        columns: ZaJi.NfiEstaPg.Core.SubPopulationList.Cols);
                });

            tsmiCSubPopulationCategory.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.Core.SubPopulationCategoryList(database: Database),
                        columns: ZaJi.NfiEstaPg.Core.SubPopulationCategoryList.Cols);
                });

            tsmiCTargetVariable.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.Core.TargetVariableList(database: Database),
                        columns: ZaJi.NfiEstaPg.Core.TargetVariableList.Cols);
                });

            tsmiCTopic.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.Core.TopicList(database: Database),
                        columns: ZaJi.NfiEstaPg.Core.TopicList.Cols);
                });

            tsmiCLanguage.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayLanguageTable();
                });

            tsmiCmAreaDomainCategoryMapping.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.Core.AreaDomainCategoryMappingList(database: Database),
                        columns: ZaJi.NfiEstaPg.Core.AreaDomainCategoryMappingList.Cols);
                });

            tsmiCmEstimationCellToParamAreaMapping.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.Core.EstimationCellToParamAreaMappingList(database: Database),
                        columns: ZaJi.NfiEstaPg.Core.EstimationCellToParamAreaMappingList.Cols);
                });

            tsmiCmPlotToEstimationCellMapping.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.Core.PlotToEstimationCellMappingList(database: Database),
                        columns: ZaJi.NfiEstaPg.Core.PlotToEstimationCellMappingList.Cols);
                });

            tsmiCmPlotToParametrizationAreaMapping.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.Core.PlotToParametrizationAreaMappingList(database: Database),
                        columns: ZaJi.NfiEstaPg.Core.PlotToParametrizationAreaMappingList.Cols);
                });

            tsmiCmSubPopulationCategoryMapping.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.Core.SubPopulationCategoryMappingList(database: Database),
                        columns: ZaJi.NfiEstaPg.Core.SubPopulationCategoryMappingList.Cols);
                });

            tsmiCmTargetVariableToTopic.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.Core.TargetVariableToTopicList(database: Database),
                        columns: ZaJi.NfiEstaPg.Core.TargetVariableToTopicList.Cols);
                });

            tsmiFaCell.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.Core.CellList(database: Database),
                        columns: ZaJi.NfiEstaPg.Core.CellList.Cols);
                });

            tsmiFaParamArea.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.Core.ParamAreaList(database: Database),
                        columns: ZaJi.NfiEstaPg.Core.ParamAreaList.Cols);
                });

            tsmiTStratumInEstimationCell.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.Core.StratumInEstimationCellList(database: Database),
                        columns: ZaJi.NfiEstaPg.Core.StratumInEstimationCellList.Cols);
                });

            tsmiTAdditivitySetPlot.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.Core.AdditivitySetPlotList(database: Database),
                        columns: ZaJi.NfiEstaPg.Core.AdditivitySetPlotList.Cols);
                });

            tsmiTAuxConf.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.Core.AuxConfList(database: Database),
                        columns: ZaJi.NfiEstaPg.Core.AuxConfList.Cols);
                });

            tsmiTAuxTotal.Click += new EventHandler(
               (sender, e) =>
               {
                   DisplayTable(
                       table: new ZaJi.NfiEstaPg.Core.AuxTotalList(database: Database),
                       columns: ZaJi.NfiEstaPg.Core.AuxTotalList.Cols);
               });

            tsmiTAuxiliaryData.Click += new EventHandler(
               (sender, e) =>
               {
                   DisplayTable(
                       table: new ZaJi.NfiEstaPg.Core.AuxDataList(database: Database),
                       columns: ZaJi.NfiEstaPg.Core.AuxDataList.Cols);
               });

            tsmiTAvailableDataSets.Click += new EventHandler(
               (sender, e) =>
               {
                   DisplayTable(
                       table: new ZaJi.NfiEstaPg.Core.AvailableDataSetList(database: Database),
                       columns: ZaJi.NfiEstaPg.Core.AvailableDataSetList.Cols);
               });

            tsmiTEstimateConf.Click += new EventHandler(
               (sender, e) =>
               {
                   DisplayTable(
                       table: new ZaJi.NfiEstaPg.Core.EstimateConfList(database: Database),
                       columns: ZaJi.NfiEstaPg.Core.EstimateConfList.Cols);
               });

            tsmiTEstimationCellHierarchy.Click += new EventHandler(
               (sender, e) =>
               {
                   DisplayTable(
                       table: new ZaJi.NfiEstaPg.Core.EstimationCellHierarchyList(database: Database),
                       columns: ZaJi.NfiEstaPg.Core.EstimationCellHierarchyList.Cols);
               });

            tsmiTGBeta.Click += new EventHandler(
               (sender, e) =>
               {
                   DisplayTable(
                       table: new ZaJi.NfiEstaPg.Core.GBetaList(database: Database),
                       columns: ZaJi.NfiEstaPg.Core.GBetaList.Cols);
               });

            tsmiTModel.Click += new EventHandler(
               (sender, e) =>
               {
                   DisplayTable(
                       table: new ZaJi.NfiEstaPg.Core.ModelList(database: Database),
                       columns: ZaJi.NfiEstaPg.Core.ModelList.Cols);
               });

            tsmiTModelVariables.Click += new EventHandler(
               (sender, e) =>
               {
                   DisplayTable(
                       table: new ZaJi.NfiEstaPg.Core.ModelVariableList(database: Database),
                       columns: ZaJi.NfiEstaPg.Core.ModelVariableList.Cols);
               });

            tsmiTPanelToTotalEstimateConfFirstPhase.Click += new EventHandler(
               (sender, e) =>
               {
                   DisplayTable(
                       table: new ZaJi.NfiEstaPg.Core.PanelToTotalEstimateConfList(database: Database),
                       columns: ZaJi.NfiEstaPg.Core.PanelToTotalEstimateConfList.Cols);
               });

            tsmiTPanelRefYearSetGroup.Click += new EventHandler(
               (sender, e) =>
               {
                   DisplayTable(
                       table: new ZaJi.NfiEstaPg.Core.PanelRefYearSetGroupList(database: Database),
                       columns: ZaJi.NfiEstaPg.Core.PanelRefYearSetGroupList.Cols);
               });

            tsmiTResult.Click += new EventHandler(
               (sender, e) =>
               {
                   DisplayTable(
                       table: new ZaJi.NfiEstaPg.Core.ResultList(database: Database),
                       columns: ZaJi.NfiEstaPg.Core.ResultList.Cols);
               });

            tsmiTTargetData.Click += new EventHandler(
               (sender, e) =>
               {
                   DisplayTable(
                       table: new ZaJi.NfiEstaPg.Core.TargetDataList(database: Database),
                       columns: ZaJi.NfiEstaPg.Core.TargetDataList.Cols);
               });

            tsmiTTotalEstimateConf.Click += new EventHandler(
               (sender, e) =>
               {
                   DisplayTable(
                       table: new ZaJi.NfiEstaPg.Core.TotalEstimateConfList(database: Database),
                       columns: ZaJi.NfiEstaPg.Core.TotalEstimateConfList.Cols);
               });

            tsmiTVariable.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.Core.VariableList(database: Database),
                        columns: ZaJi.NfiEstaPg.Core.VariableList.Cols);
                });

            tsmiTVariableHierarchy.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.Core.VariableHierarchyList(database: Database),
                        columns: ZaJi.NfiEstaPg.Core.VariableHierarchyList.Cols);
                });

            tsmiExtractData.Click += new EventHandler(
              (sender, e) => { ExportData(); });

            tsmiGetDifferences.Click += new EventHandler(
              (sender, e) => { DisplayDifferences(); });
        }

        /// <summary>
        /// Initialize labels
        /// </summary>
        public void InitializeLabels()
        {
            switch (LanguageVersion)
            {
                case LanguageVersion.National:
                    Text = $"{NfiEstaSchema.ExtensionName} ({NfiEstaSchema.ExtensionVersion})";

                    tsmiLookupTables.Text = "Číselníky";
                    tsmiCAreaDomain.Text = ZaJi.NfiEstaPg.Core.AreaDomainList.Name;
                    tsmiCAreaDomainCategory.Text = ZaJi.NfiEstaPg.Core.AreaDomainCategoryList.Name;
                    tsmiCAuxiliaryVariable.Text = ZaJi.NfiEstaPg.Core.AuxiliaryVariableList.Name;
                    tsmiCAuxiliaryVariableCategory.Text = ZaJi.NfiEstaPg.Core.AuxiliaryVariableCategoryList.Name;
                    tsmiCEstimateConfStatus.Text = ZaJi.NfiEstaPg.Core.EstimateConfStatusList.Name;
                    tsmiCEstimateType.Text = ZaJi.NfiEstaPg.Core.EstimateTypeList.Name;
                    tsmiCEstimationCell.Text = ZaJi.NfiEstaPg.Core.EstimationCellList.Name;
                    tsmiCEstimationCellCollection.Text = ZaJi.NfiEstaPg.Core.EstimationCellCollectionList.Name;
                    tsmiCEstimationPeriod.Text = ZaJi.NfiEstaPg.Core.EstimationPeriodList.Name;
                    tsmiCPanelRefYearSetGroup.Text = ZaJi.NfiEstaPg.Core.PanelRefYearSetGroupList.Name;
                    tsmiCParamAreaType.Text = ZaJi.NfiEstaPg.Core.ParamAreaTypeList.Name;
                    tsmiCPhaseEstimateType.Text = ZaJi.NfiEstaPg.Core.PhaseEstimateTypeList.Name;
                    tsmiCSubPopulation.Text = ZaJi.NfiEstaPg.Core.SubPopulationList.Name;
                    tsmiCSubPopulationCategory.Text = ZaJi.NfiEstaPg.Core.SubPopulationCategoryList.Name;
                    tsmiCTargetVariable.Text = ZaJi.NfiEstaPg.Core.TargetVariableList.Name;
                    tsmiCTopic.Text = ZaJi.NfiEstaPg.Core.TopicList.Name;
                    tsmiCLanguage.Text = "c_language";

                    tsmiMappingTables.Text = "Mapovací tabulky";
                    tsmiCmAreaDomainCategoryMapping.Text = ZaJi.NfiEstaPg.Core.AreaDomainCategoryMappingList.Name;
                    tsmiCmEstimationCellToParamAreaMapping.Text = ZaJi.NfiEstaPg.Core.EstimationCellToParamAreaMappingList.Name;
                    tsmiCmPlotToEstimationCellMapping.Text = ZaJi.NfiEstaPg.Core.PlotToEstimationCellMappingList.Name;
                    tsmiCmPlotToParametrizationAreaMapping.Text = ZaJi.NfiEstaPg.Core.PlotToParametrizationAreaMappingList.Name;
                    tsmiCmSubPopulationCategoryMapping.Text = ZaJi.NfiEstaPg.Core.SubPopulationCategoryMappingList.Name;
                    tsmiCmTargetVariableToTopic.Text = ZaJi.NfiEstaPg.Core.TargetVariableToTopicList.Name;

                    tsmiSpatialTables.Text = "Prostorové tabulky";
                    tsmiFaCell.Text = ZaJi.NfiEstaPg.Core.CellList.Name;
                    tsmiFaParamArea.Text = ZaJi.NfiEstaPg.Core.ParamAreaList.Name;
                    tsmiTStratumInEstimationCell.Text = ZaJi.NfiEstaPg.Core.StratumInEstimationCellList.Name;

                    tsmiDataTables.Text = "Datové tabulky";
                    tsmiTAdditivitySetPlot.Text = ZaJi.NfiEstaPg.Core.AdditivitySetPlotList.Name;
                    tsmiTAuxConf.Text = ZaJi.NfiEstaPg.Core.AuxConfList.Name;
                    tsmiTAuxTotal.Text = ZaJi.NfiEstaPg.Core.AuxTotalList.Name;
                    tsmiTAuxiliaryData.Text = ZaJi.NfiEstaPg.Core.AuxDataList.Name;
                    tsmiTAvailableDataSets.Text = ZaJi.NfiEstaPg.Core.AvailableDataSetList.Name;
                    tsmiTEstimateConf.Text = ZaJi.NfiEstaPg.Core.EstimateConfList.Name;
                    tsmiTEstimationCellHierarchy.Text = ZaJi.NfiEstaPg.Core.EstimationCellHierarchyList.Name;
                    tsmiTGBeta.Text = ZaJi.NfiEstaPg.Core.GBetaList.Name;
                    tsmiTModel.Text = ZaJi.NfiEstaPg.Core.ModelList.Name;
                    tsmiTModelVariables.Text = ZaJi.NfiEstaPg.Core.ModelVariableList.Name;
                    tsmiTPanelToTotalEstimateConfFirstPhase.Text = ZaJi.NfiEstaPg.Core.PanelToTotalEstimateConfList.Name;
                    tsmiTPanelRefYearSetGroup.Text = ZaJi.NfiEstaPg.Core.PanelRefYearSetGroupList.Name;
                    tsmiTResult.Text = ZaJi.NfiEstaPg.Core.ResultList.Name;
                    tsmiTTargetData.Text = ZaJi.NfiEstaPg.Core.TargetDataList.Name;
                    tsmiTTotalEstimateConf.Text = ZaJi.NfiEstaPg.Core.TotalEstimateConfList.Name;
                    tsmiTVariable.Text = ZaJi.NfiEstaPg.Core.VariableList.Name;
                    tsmiTVariableHierarchy.Text = ZaJi.NfiEstaPg.Core.VariableHierarchyList.Name;

                    tsmiJSON.Text = "JSON";
                    tsmiResultSamplingUnits.Text = "v_result_sampling_units";
                    tsmiTargetVariableMetadata.Text = "v_target_variable_metadata";
                    tsmiTargetVariableMetadataAggregated.Text = "v_target_variable_metadata_aggregated";
                    tsmiFnEtlCheckVariablesJson.Text = "fn_etl_check_variables";

                    tsmiStoredProcedures.Text = "Uložené procedury";
                    tsmiFnAddResRatioAttr.Text = "fn_add_res_ratio_attr";
                    tsmiFnAddResTotalAttr.Text = "fn_add_res_total_attr";
                    tsmiFnAddResTotalGeo.Text = "fn_add_res_total_geo";
                    tsmiFnPanelInEstimationCell.Text = "fn_get_panels_in_estimation_cells";
                    tsmiFnEtlGetTargetVariable.Text = "fn_etl_get_target_variable";
                    tsmiFnEtlGetTargetVariableMetadata.Text = "fn_etl_get_target_variable_metadata";
                    tsmiFnEtlGetTargetVariableMetadataAggregated.Text = "fn_etl_get_target_variable_metadata_agg";
                    tsmiControlTargetVariableSelector.Text = "target_variable_selector_test";
                    tsmiFnApiBeforeDeletePanelRefYearSetGroup.Text = FnApiBeforeDeletePanelRefYearSetGroup.Name;
                    tsmiFnApiGetListOfPanelRefYearSetGroups.Text = FnApiGetListOfPanelRefYearSetGroups.Name;
                    tsmiFnApiGetPanelRefYearSetCombinationsForGroups.Text = FnApiGetPanelRefYearSetCombinationsForGroups.Name;

                    tsmiLINQ.Text = "LINQ";
                    tsmiOLAPRatioEstimate.Text = "v_olap_ratio_estimate";
                    tsmiOLAPRatioEstimateDimension.Text = "v_olap_ratio_estimate_dimension";
                    tsmiOLAPTotalEstimate.Text = "v_olap_total_estimate";
                    tsmiOLAPTotalEstimateDimension.Text = "v_olap_total_estimate_dimension";
                    tsmiOLAPTotalEstimateValue.Text = "v_olap_total_estimate_value";
                    tsmiVwPanelRefYearSetPair.Text = "v_panel_refyearset_group";
                    tsmiVwVariable.Text = "v_variable";
                    tsmiVwEstimationCellsCount.Text = "v_estimation_cells_count";

                    tsmiETL.Text = "ETL";
                    tsmiExtractData.Text = "Export dat do vybrané složky";
                    tsmiGetDifferences.Text = "Rozdíly mezi dll a databázovou extenzí";

                    btnClose.Text = "Zavřít";

                    msgDataExportComplete = "Export dat byl dokončen.";
                    msgNoDifferencesFound = "Nebyly nalezeny žádné rozdíly.";

                    break;

                case LanguageVersion.International:
                    Text = $"{NfiEstaSchema.ExtensionName} ({NfiEstaSchema.ExtensionVersion})";

                    tsmiLookupTables.Text = "Lookup tables";
                    tsmiCAreaDomain.Text = ZaJi.NfiEstaPg.Core.AreaDomainList.Name;
                    tsmiCAreaDomainCategory.Text = ZaJi.NfiEstaPg.Core.AreaDomainCategoryList.Name;
                    tsmiCAuxiliaryVariable.Text = ZaJi.NfiEstaPg.Core.AuxiliaryVariableList.Name;
                    tsmiCAuxiliaryVariableCategory.Text = ZaJi.NfiEstaPg.Core.AuxiliaryVariableCategoryList.Name;
                    tsmiCEstimateConfStatus.Text = ZaJi.NfiEstaPg.Core.EstimateConfStatusList.Name;
                    tsmiCEstimateType.Text = ZaJi.NfiEstaPg.Core.EstimateTypeList.Name;
                    tsmiCEstimationCell.Text = ZaJi.NfiEstaPg.Core.EstimationCellList.Name;
                    tsmiCEstimationCellCollection.Text = ZaJi.NfiEstaPg.Core.EstimationCellCollectionList.Name;
                    tsmiCEstimationPeriod.Text = ZaJi.NfiEstaPg.Core.EstimationPeriodList.Name;
                    tsmiCPanelRefYearSetGroup.Text = ZaJi.NfiEstaPg.Core.PanelRefYearSetGroupList.Name;
                    tsmiCParamAreaType.Text = ZaJi.NfiEstaPg.Core.ParamAreaTypeList.Name;
                    tsmiCPhaseEstimateType.Text = ZaJi.NfiEstaPg.Core.PhaseEstimateTypeList.Name;
                    tsmiCSubPopulation.Text = ZaJi.NfiEstaPg.Core.SubPopulationList.Name;
                    tsmiCSubPopulationCategory.Text = ZaJi.NfiEstaPg.Core.SubPopulationCategoryList.Name;
                    tsmiCTargetVariable.Text = ZaJi.NfiEstaPg.Core.TargetVariableList.Name;
                    tsmiCTopic.Text = ZaJi.NfiEstaPg.Core.TopicList.Name;
                    tsmiCLanguage.Text = "c_language";

                    tsmiMappingTables.Text = "Mapping tables";
                    tsmiCmAreaDomainCategoryMapping.Text = ZaJi.NfiEstaPg.Core.AreaDomainCategoryMappingList.Name;
                    tsmiCmEstimationCellToParamAreaMapping.Text = ZaJi.NfiEstaPg.Core.EstimationCellToParamAreaMappingList.Name;
                    tsmiCmPlotToEstimationCellMapping.Text = ZaJi.NfiEstaPg.Core.PlotToEstimationCellMappingList.Name;
                    tsmiCmPlotToParametrizationAreaMapping.Text = ZaJi.NfiEstaPg.Core.PlotToParametrizationAreaMappingList.Name;
                    tsmiCmSubPopulationCategoryMapping.Text = ZaJi.NfiEstaPg.Core.SubPopulationCategoryMappingList.Name;
                    tsmiCmTargetVariableToTopic.Text = ZaJi.NfiEstaPg.Core.TargetVariableToTopicList.Name;

                    tsmiSpatialTables.Text = "Spatial tables";
                    tsmiFaCell.Text = ZaJi.NfiEstaPg.Core.CellList.Name;
                    tsmiFaParamArea.Text = ZaJi.NfiEstaPg.Core.ParamAreaList.Name;
                    tsmiTStratumInEstimationCell.Text = ZaJi.NfiEstaPg.Core.StratumInEstimationCellList.Name;

                    tsmiDataTables.Text = "Data tables";
                    tsmiTAdditivitySetPlot.Text = ZaJi.NfiEstaPg.Core.AdditivitySetPlotList.Name;
                    tsmiTAuxConf.Text = ZaJi.NfiEstaPg.Core.AuxConfList.Name;
                    tsmiTAuxTotal.Text = ZaJi.NfiEstaPg.Core.AuxTotalList.Name;
                    tsmiTAuxiliaryData.Text = ZaJi.NfiEstaPg.Core.AuxDataList.Name;
                    tsmiTAvailableDataSets.Text = ZaJi.NfiEstaPg.Core.AvailableDataSetList.Name;
                    tsmiTEstimateConf.Text = ZaJi.NfiEstaPg.Core.EstimateConfList.Name;
                    tsmiTEstimationCellHierarchy.Text = ZaJi.NfiEstaPg.Core.EstimationCellHierarchyList.Name;
                    tsmiTGBeta.Text = ZaJi.NfiEstaPg.Core.GBetaList.Name;
                    tsmiTModel.Text = ZaJi.NfiEstaPg.Core.ModelList.Name;
                    tsmiTModelVariables.Text = ZaJi.NfiEstaPg.Core.ModelVariableList.Name;
                    tsmiTPanelToTotalEstimateConfFirstPhase.Text = ZaJi.NfiEstaPg.Core.PanelToTotalEstimateConfList.Name;
                    tsmiTPanelRefYearSetGroup.Text = ZaJi.NfiEstaPg.Core.PanelRefYearSetGroupList.Name;
                    tsmiTResult.Text = ZaJi.NfiEstaPg.Core.ResultList.Name;
                    tsmiTTargetData.Text = ZaJi.NfiEstaPg.Core.TargetDataList.Name;
                    tsmiTTotalEstimateConf.Text = ZaJi.NfiEstaPg.Core.TotalEstimateConfList.Name;
                    tsmiTVariable.Text = ZaJi.NfiEstaPg.Core.VariableList.Name;
                    tsmiTVariableHierarchy.Text = ZaJi.NfiEstaPg.Core.VariableHierarchyList.Name;

                    tsmiJSON.Text = "JSON";
                    tsmiResultSamplingUnits.Text = "v_result_sampling_units";
                    tsmiTargetVariableMetadata.Text = "v_target_variable_metadata";
                    tsmiTargetVariableMetadataAggregated.Text = "v_target_variable_metadata_aggregated";
                    tsmiFnEtlCheckVariablesJson.Text = "fn_etl_check_variables";

                    tsmiStoredProcedures.Text = "Stored procedures";
                    tsmiFnAddResRatioAttr.Text = "fn_add_res_ratio_attr";
                    tsmiFnAddResTotalAttr.Text = "fn_add_res_total_attr";
                    tsmiFnAddResTotalGeo.Text = "fn_add_res_total_geo";
                    tsmiFnPanelInEstimationCell.Text = "fn_get_panels_in_estimation_cells";
                    tsmiFnEtlGetTargetVariable.Text = "fn_etl_get_target_variable";
                    tsmiFnEtlGetTargetVariableMetadata.Text = "fn_etl_get_target_variable_metadata";
                    tsmiFnEtlGetTargetVariableMetadataAggregated.Text = "fn_etl_get_target_variable_metadata_agg";
                    tsmiControlTargetVariableSelector.Text = "target_variable_selector_test";
                    tsmiFnApiBeforeDeletePanelRefYearSetGroup.Text = FnApiBeforeDeletePanelRefYearSetGroup.Name;
                    tsmiFnApiGetListOfPanelRefYearSetGroups.Text = FnApiGetListOfPanelRefYearSetGroups.Name;
                    tsmiFnApiGetPanelRefYearSetCombinationsForGroups.Text = FnApiGetPanelRefYearSetCombinationsForGroups.Name;

                    tsmiLINQ.Text = "LINQ";
                    tsmiOLAPRatioEstimate.Text = "v_olap_ratio_estimate";
                    tsmiOLAPRatioEstimateDimension.Text = "v_olap_ratio_estimate_dimension";
                    tsmiOLAPTotalEstimate.Text = "v_olap_total_estimate";
                    tsmiOLAPTotalEstimateDimension.Text = "v_olap_total_estimate_dimension";
                    tsmiOLAPTotalEstimateValue.Text = "v_olap_total_estimate_value";
                    tsmiVwPanelRefYearSetPair.Text = "v_panel_refyearset_group";
                    tsmiVwVariable.Text = "v_variable";
                    tsmiVwEstimationCellsCount.Text = "v_estimation_cells_count";

                    tsmiETL.Text = "ETL";
                    tsmiExtractData.Text = "Export data to folder";
                    tsmiGetDifferences.Text = "Differences between dll database extension";

                    btnClose.Text = "Close";

                    msgDataExportComplete = "Data export has been completed.";
                    msgNoDifferencesFound = "No differences have been found.";

                    break;

                default:
                    Text = nameof(FormNfiEsta);

                    tsmiLookupTables.Text = nameof(tsmiLookupTables);
                    tsmiCAreaDomain.Text = nameof(tsmiCAreaDomain);
                    tsmiCAreaDomainCategory.Text = nameof(tsmiCAreaDomainCategory);
                    tsmiCAuxiliaryVariable.Text = nameof(tsmiCAuxiliaryVariable);
                    tsmiCAuxiliaryVariableCategory.Text = nameof(tsmiCAuxiliaryVariableCategory);
                    tsmiCEstimateConfStatus.Text = nameof(tsmiCEstimateConfStatus);
                    tsmiCEstimateType.Text = nameof(tsmiCEstimateType);
                    tsmiCEstimationCell.Text = nameof(tsmiCEstimationCell);
                    tsmiCEstimationCellCollection.Text = nameof(tsmiCEstimationCellCollection);
                    tsmiCEstimationPeriod.Text = nameof(tsmiCEstimationPeriod);
                    tsmiCPanelRefYearSetGroup.Text = nameof(tsmiCPanelRefYearSetGroup);
                    tsmiCParamAreaType.Text = nameof(tsmiCParamAreaType);
                    tsmiCPhaseEstimateType.Text = nameof(tsmiCPhaseEstimateType);
                    tsmiCSubPopulation.Text = nameof(tsmiCSubPopulation);
                    tsmiCSubPopulationCategory.Text = nameof(tsmiCSubPopulationCategory);
                    tsmiCTargetVariable.Text = nameof(tsmiCTargetVariable);
                    tsmiCTopic.Text = nameof(tsmiCTopic);
                    tsmiCLanguage.Text = nameof(tsmiCLanguage);

                    tsmiMappingTables.Text = nameof(tsmiMappingTables);
                    tsmiCmEstimationCellToParamAreaMapping.Text = nameof(tsmiCmEstimationCellToParamAreaMapping);
                    tsmiCmPlotToEstimationCellMapping.Text = nameof(tsmiCmPlotToEstimationCellMapping);
                    tsmiCmPlotToParametrizationAreaMapping.Text = nameof(tsmiCmPlotToParametrizationAreaMapping);
                    tsmiCmTargetVariableToTopic.Text = nameof(tsmiCmTargetVariableToTopic);

                    tsmiSpatialTables.Text = nameof(tsmiSpatialTables);
                    tsmiFaCell.Text = nameof(tsmiFaCell);
                    tsmiFaParamArea.Text = nameof(tsmiFaParamArea);
                    tsmiTStratumInEstimationCell.Text = nameof(tsmiTStratumInEstimationCell);

                    tsmiDataTables.Text = nameof(tsmiDataTables);
                    tsmiTAdditivitySetPlot.Text = nameof(tsmiTAdditivitySetPlot);
                    tsmiTAuxConf.Text = nameof(tsmiTAuxConf);
                    tsmiTAuxTotal.Text = nameof(tsmiTAuxTotal);
                    tsmiTAuxiliaryData.Text = nameof(tsmiTAuxiliaryData);
                    tsmiTAvailableDataSets.Text = nameof(tsmiTAvailableDataSets);
                    tsmiTEstimateConf.Text = nameof(tsmiTEstimateConf);
                    tsmiTEstimationCellHierarchy.Text = nameof(tsmiTEstimationCellHierarchy);
                    tsmiTGBeta.Text = nameof(tsmiTGBeta);
                    tsmiTModel.Text = nameof(tsmiTModel);
                    tsmiTModelVariables.Text = nameof(tsmiTModelVariables);
                    tsmiTPanelToTotalEstimateConfFirstPhase.Text = nameof(tsmiTPanelToTotalEstimateConfFirstPhase);
                    tsmiTPanelRefYearSetGroup.Text = nameof(tsmiTPanelRefYearSetGroup);
                    tsmiTResult.Text = nameof(tsmiTResult);
                    tsmiTTargetData.Text = nameof(tsmiTTargetData);
                    tsmiTTotalEstimateConf.Text = nameof(tsmiTTotalEstimateConf);
                    tsmiTVariable.Text = nameof(tsmiTVariable);
                    tsmiTVariableHierarchy.Text = nameof(tsmiTVariableHierarchy);

                    tsmiJSON.Text = nameof(tsmiJSON);
                    tsmiResultSamplingUnits.Text = nameof(tsmiResultSamplingUnits);
                    tsmiTargetVariableMetadata.Text = nameof(tsmiTargetVariableMetadata);
                    tsmiTargetVariableMetadataAggregated.Text = nameof(tsmiTargetVariableMetadataAggregated);
                    tsmiFnEtlCheckVariablesJson.Text = nameof(tsmiFnEtlCheckVariablesJson);

                    tsmiStoredProcedures.Text = nameof(tsmiStoredProcedures);
                    tsmiFnAddResRatioAttr.Text = nameof(tsmiFnAddResRatioAttr);
                    tsmiFnAddResTotalAttr.Text = nameof(tsmiFnAddResTotalAttr);
                    tsmiFnAddResTotalGeo.Text = nameof(tsmiFnAddResTotalGeo);
                    tsmiFnPanelInEstimationCell.Text = nameof(tsmiFnPanelInEstimationCell);
                    tsmiFnEtlGetTargetVariable.Text = nameof(tsmiFnEtlGetTargetVariable);
                    tsmiFnEtlGetTargetVariableMetadata.Text = nameof(tsmiFnEtlGetTargetVariableMetadata);
                    tsmiFnEtlGetTargetVariableMetadataAggregated.Text = nameof(tsmiFnEtlGetTargetVariableMetadataAggregated);
                    tsmiFnApiBeforeDeletePanelRefYearSetGroup.Text = nameof(tsmiFnApiBeforeDeletePanelRefYearSetGroup);
                    tsmiFnApiGetListOfPanelRefYearSetGroups.Text = nameof(tsmiFnApiGetListOfPanelRefYearSetGroups);
                    tsmiFnApiGetPanelRefYearSetCombinationsForGroups.Text = nameof(tsmiFnApiGetPanelRefYearSetCombinationsForGroups);

                    tsmiLINQ.Text = nameof(tsmiLINQ);
                    tsmiOLAPRatioEstimate.Text = nameof(tsmiOLAPRatioEstimate);
                    tsmiOLAPRatioEstimateDimension.Text = nameof(tsmiOLAPRatioEstimateDimension);
                    tsmiOLAPTotalEstimate.Text = nameof(tsmiOLAPTotalEstimate);
                    tsmiOLAPTotalEstimateDimension.Text = nameof(tsmiOLAPTotalEstimateDimension);
                    tsmiOLAPTotalEstimateValue.Text = nameof(tsmiOLAPTotalEstimateValue);
                    tsmiVwPanelRefYearSetPair.Text = nameof(tsmiVwPanelRefYearSetPair);
                    tsmiVwVariable.Text = nameof(tsmiVwVariable);
                    tsmiVwEstimationCellsCount.Text = nameof(tsmiVwEstimationCellsCount);

                    tsmiETL.Text = nameof(tsmiETL);
                    tsmiExtractData.Text = nameof(tsmiExtractData);
                    tsmiGetDifferences.Text = nameof(tsmiGetDifferences);

                    btnClose.Text = nameof(btnClose);

                    msgDataExportComplete = nameof(msgDataExportComplete);
                    msgNoDifferencesFound = nameof(msgNoDifferencesFound);

                    break;
            }
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent() { }

        /// <summary>
        /// <para lang="cs">Zobrazí data číselníku</para>
        /// <para lang="en">Display lookup table data</para>
        /// </summary>
        /// <param name="table">
        /// <para lang="cs">Tabulka</para>
        /// <para lang="en">Table</para>
        /// </param>
        /// <param name="columns">
        /// <para lang="cs">Sloupce tabulky</para>
        /// <para lang="en">Table columns</para>
        /// </param>
        private void DisplayTable(
            ALookupTable table,
            Dictionary<string, ColumnMetadata> columns)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(value: dgvData);
            table.ReLoad(limit: Limit, extended: false);
            ALookupTable.SetColumnsVisibility(columns: columns);
            table.SetDataGridViewDataSource(dataGridView: dgvData);
            table.FormatDataGridView(dataGridView: dgvData);
            grpMain.Text = table.Caption;
            txtSQL.Text = $"{Environment.NewLine}{table.SQL}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(box: txtSQL);
        }

        /// <summary>
        /// <para lang="cs">Zobrazí data mapovací tabulky</para>
        /// <para lang="en">Display mapping table data</para>
        /// </summary>
        /// <param name="table">
        /// <para lang="cs">Tabulka</para>
        /// <para lang="en">Table</para>
        /// </param>
        /// <param name="columns">
        /// <para lang="cs">Sloupce tabulky</para>
        /// <para lang="en">Table columns</para>
        /// </param>
        private void DisplayTable(
            AMappingTable table,
            Dictionary<string, ColumnMetadata> columns)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(value: dgvData);
            table.ReLoad(limit: Limit);
            AMappingTable.SetColumnsVisibility(columns: columns);
            table.SetDataGridViewDataSource(dataGridView: dgvData);
            table.FormatDataGridView(dataGridView: dgvData);
            grpMain.Text = table.Caption;
            txtSQL.Text = $"{Environment.NewLine}{table.SQL}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(box: txtSQL);
        }

        /// <summary>
        /// <para lang="cs">Zobrazí data tabulky s prostorovými daty</para>
        /// <para lang="en">Display data table with spatial data</para>
        /// </summary>
        /// <param name="table">
        /// <para lang="cs">Tabulka</para>
        /// <para lang="en">Table</para>
        /// </param>
        /// <param name="columns">
        /// <para lang="cs">Sloupce tabulky</para>
        /// <para lang="en">Table columns</para>
        /// </param>
        private void DisplayTable(
            ASpatialTable table,
            Dictionary<string, ColumnMetadata> columns)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(value: dgvData);
            table.ReLoad(limit: Limit, withGeom: false);
            ASpatialTable.SetColumnsVisibility(columns: columns);
            table.SetDataGridViewDataSource(dataGridView: dgvData);
            table.FormatDataGridView(dataGridView: dgvData);
            grpMain.Text = table.Caption;
            txtSQL.Text = $"{Environment.NewLine}{table.SQL}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(box: txtSQL);
        }

        /// <summary>
        /// <para lang="cs">Zobrazí data tabulky</para>
        /// <para lang="en">Display table data</para>
        /// </summary>
        /// <param name="table">
        /// <para lang="cs">Tabulka</para>
        /// <para lang="en">Table</para>
        /// </param>
        /// <param name="columns">
        /// <para lang="cs">Sloupce tabulky</para>
        /// <para lang="en">Table columns</para>
        /// </param>
        private void DisplayTable(
            ADataTable table,
            Dictionary<string, ColumnMetadata> columns)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(value: dgvData);
            table.ReLoad(limit: Limit);
            ADataTable.SetColumnsVisibility(columns: columns);
            table.SetDataGridViewDataSource(dataGridView: dgvData);
            table.FormatDataGridView(dataGridView: dgvData);
            grpMain.Text = table.Caption;
            txtSQL.Text = $"{Environment.NewLine}{table.SQL}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(box: txtSQL);
        }

        /// <summary>
        /// <para lang="cs">Zobrazí data vrácená uloženou procedurou</para>
        /// <para lang="en">Display data returned from stored procedure</para>
        /// </summary>
        /// <param name="table">
        /// <para lang="cs">Tabulka s daty vrácenými uloženou procedurou</para>
        /// <para lang="en">Table with data returned from stored procedure</para>
        /// </param>
        /// <param name="columns">
        /// <para lang="cs">Sloupce tabulky</para>
        /// <para lang="en">Table columns</para>
        /// </param>
        private void DisplayTable(
            AParametrizedView table,
            Dictionary<string, ColumnMetadata> columns)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(value: dgvData);
            ADataTable.SetColumnsVisibility(columns: columns);
            table.SetDataGridViewDataSource(dataGridView: dgvData);
            table.FormatDataGridView(dataGridView: dgvData);
            grpMain.Text = table.Caption;
            txtSQL.Text = $"{Environment.NewLine}{table.SQL}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(box: txtSQL);
        }

        /// <summary>
        /// <para lang="cs">Zobrazí data tabulky jazyků</para>
        /// <para lang="en">Display languages table data</para>
        /// </summary>
        private void DisplayLanguageTable()
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(value: dgvData);
            dgvData.DataSource = LanguageList.Data;
            grpMain.Text = "Languages";
            txtSQL.Text = String.Empty;
        }

        /// <summary>
        /// <para lang="cs">Export dat</para>
        /// <para lang="en">Data export</para>
        /// </summary>
        private void ExportData()
        {
            NfiEstaSchema.ExportData(
               database: Database,
               fileFormat: ExportFileFormat.Xml);

            MessageBox.Show(
                caption: String.Empty,
                text: msgDataExportComplete,
                buttons: MessageBoxButtons.OK,
                icon: MessageBoxIcon.Information);
        }

        /// <summary>
        /// <para lang="cs">Zobrazí provedené změny v databázi</para>
        /// <para lang="en">Displays changes made to the database</para>
        /// </summary>
        private void DisplayDifferences()
        {
            grpMain.Text = String.Empty;
            pnlData.Controls.Clear();
            txtSQL.Text = String.Empty;

            List<string> differences = Database.SNfiEsta.Differences;
            System.Text.StringBuilder stringBuilder = new();
            if ((differences == null) || (differences.Count == 0))
            {
                stringBuilder.AppendLine(value: msgNoDifferencesFound);
            }
            else
            {
                foreach (string difference in differences)
                {
                    stringBuilder.AppendLine(value: $"* {difference}");
                }
            }

            Label lblReport = new()
            {
                Dock = DockStyle.Fill,
                Text = stringBuilder.ToString()
            };
            pnlData.Controls.Add(value: lblReport);
        }

        #endregion Methods


        #region Event Handlers

        #region JSON

        /// <summary>
        /// Gets data from table t_result column sampling_units
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemResultSamplingUnits_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(dgvData);

            Database.SNfiEsta.TResult.ReLoad(condition: "id <= 10000");
            ResultSamplingUnitList data = new(this.Database);
            data.ReLoad(Database.SNfiEsta.TResult);

            ResultSamplingUnitList.SetColumnsVisibility(
                (from a in ResultSamplingUnitList.Cols
                 orderby a.Value.DisplayIndex
                 select a.Value.Name).ToArray<string>());
            data.SetDataGridViewDataSource(dgvData);
            data.FormatDataGridView(dgvData);
            grpMain.Text = data.Caption;
            txtSQL.Text = $"{Environment.NewLine}{data.SQL}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }

        /// <summary>
        /// Gets data from table c_target_variable column metadata
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemTargetVariableMetadata_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(dgvData);

            Database.SNfiEsta.CTargetVariable.ReLoad();
            Database.SNfiEsta.CTargetVariableMetadata.ReLoad(
                cTargetVariable: Database.SNfiEsta.CTargetVariable);

            TargetVariableMetadataList.SetColumnsVisibility(
                (from a in TargetVariableMetadataList.Cols
                 orderby a.Value.DisplayIndex
                 select a.Value.Name).ToArray<string>());
            Database.SNfiEsta.CTargetVariableMetadata.SetDataGridViewDataSource(dgvData);
            Database.SNfiEsta.CTargetVariableMetadata.FormatDataGridView(dgvData);
            grpMain.Text = Database.SNfiEsta.CTargetVariableMetadata.Caption;
            txtSQL.Text = $"{Environment.NewLine}{Database.SNfiEsta.CTargetVariableMetadata.SQL}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }

        /// <summary>
        /// Gets data from table c_target_variable column metadata and aggregates by target_variable_id
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemTargetVariableMetadataAggregated_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(dgvData);

            Database.SNfiEsta.CTargetVariable.ReLoad();
            Database.SNfiEsta.CTargetVariableMetadata.ReLoad(
                cTargetVariable: Database.SNfiEsta.CTargetVariable);
            TargetVariableMetadataList result = Database.SNfiEsta.CTargetVariableMetadata.Aggregated();
            TargetVariableMetadataList.SetColumnsVisibility(
                (from a in TargetVariableMetadataList.Cols
                 orderby a.Value.DisplayIndex
                 select a.Value.Name).ToArray<string>());
            result.SetDataGridViewDataSource(dgvData);
            result.FormatDataGridView(dgvData);
            grpMain.Text = result.Caption;
            txtSQL.Text = $"{Environment.NewLine}{result.SQL}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }

        /// <summary>
        /// Gets data from stored procedure fn_etl_check_variables
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemFnEtlCheckVariablesJson_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(value: dgvData);

            FnEtlCheckVariablesJson json = new(language: LanguageVersion.National);
            json.LoadFromText(text: FnEtlCheckVariablesJson.TestJsonText);
            json.SetDataGridViewDataSource(dataGridView: dgvData);
            FnEtlCheckVariablesJson.FormatDataGridView(dataGridView: dgvData);
            grpMain.Text = FnEtlCheckVariablesJson.TableName;
            txtSQL.Text = $"{Environment.NewLine}{json.Text}{Environment.NewLine}";
        }

        #endregion JSON


        #region Stored Procedures

        /// <summary>
        /// Get data from stored procedure fn_add_res_ratio_attr
        /// </summary>
        /// <returns>Data from stored procedure fn_add_res_ratio_attr</returns>
        private TFnAddResRatioAttrList GetFnAddResRatioAttr()
        {
            EstimateConfList tEstimateConf = new(
                database: Database);
            tEstimateConf.ReLoad(condition: null);

            TFnAddResRatioAttrList fnAddResRatioAttr =
                NfiEstaFunctions.FnAddResRatioAttr.Execute(
                    database: Database,
                    estConfs: tEstimateConf.Items.Select(a => (Nullable<int>)a.Id).ToList<Nullable<int>>(),
                    minDiff: 0.0,
                    includeNullDiff: false);

            return fnAddResRatioAttr;
        }

        /// <summary>
        /// Stored procedure fn_add_res_ratio_attr
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemFnAddResRatioAttr_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(dgvData);

            TFnAddResRatioAttrList fnAddResRatioAttr = GetFnAddResRatioAttr();

            TFnAddResRatioAttrList.SetColumnsVisibility(
                columnNames: TFnAddResRatioAttrList.Cols
                                .OrderBy(a => a.Value.DisplayIndex)
                                .Select(a => a.Value.Name)
                                .ToArray<string>());
            fnAddResRatioAttr.SetDataGridViewDataSource(dataGridView: dgvData);
            fnAddResRatioAttr.FormatDataGridView(dataGridView: dgvData);
            grpMain.Text = fnAddResRatioAttr.Caption;
            txtSQL.Text = $"{Environment.NewLine}{fnAddResRatioAttr.SQL}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }


        /// <summary>
        /// Get data from stored procedure fn_add_res_total_attr
        /// </summary>
        /// <returns>Data from stored procedure fn_add_res_total_attr</returns>
        private TFnAddResTotalAttrList GetFnAddResTotalAttr()
        {
            EstimateConfList tEstimateConf = new(
                database: Database);
            tEstimateConf.ReLoad(condition: null);

            TFnAddResTotalAttrList fnAddResTotalAttr =
                NfiEstaFunctions.FnAddResTotalAttr.Execute(
                    database: Database,
                    estConfs: tEstimateConf.Items.Select(a => (Nullable<int>)a.Id).ToList<Nullable<int>>(),
                    minDiff: 0.0,
                    includeNullDiff: false);

            return fnAddResTotalAttr;
        }

        /// <summary>
        /// Stored procedure fn_add_res_total_attr
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemFnAddResTotalAttr_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(dgvData);

            TFnAddResTotalAttrList fnAddResTotalAttr = GetFnAddResTotalAttr();

            TFnAddResTotalAttrList.SetColumnsVisibility(
               columnNames: TFnAddResTotalAttrList.Cols
                                .OrderBy(a => a.Value.DisplayIndex)
                                .Select(a => a.Value.Name)
                                .ToArray<string>());
            fnAddResTotalAttr.SetDataGridViewDataSource(dataGridView: dgvData);
            fnAddResTotalAttr.FormatDataGridView(dataGridView: dgvData);
            grpMain.Text = fnAddResTotalAttr.Caption;
            txtSQL.Text = $"{Environment.NewLine}{fnAddResTotalAttr.SQL}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }


        /// <summary>
        /// Get data from stored procedure fn_add_res_total_geo
        /// </summary>
        /// <returns>Data from stored procedure fn_add_res_total_geo</returns>
        private TFnAddResTotalGeoList GetFnAddResTotalGeo()
        {
            EstimateConfList tEstimateConf = new(
                database: Database);
            tEstimateConf.ReLoad(condition: null);

            TFnAddResTotalGeoList fnAddResTotalGeo =
                NfiEstaFunctions.FnAddResTotalGeo.Execute(
                    database: Database,
                    estConfs: tEstimateConf.Items.Select(a => (Nullable<int>)a.Id).ToList<Nullable<int>>(),
                    minDiff: 0.0,
                    includeNullDiff: false);

            return fnAddResTotalGeo;
        }

        /// <summary>
        /// Stored procedure fn_add_res_total_geo
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemFnAddResTotalGeo_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(dgvData);

            TFnAddResTotalGeoList fnAddResTotalGeo = GetFnAddResTotalGeo();

            TFnAddResTotalGeoList.SetColumnsVisibility(
                columnNames: TFnAddResTotalGeoList.Cols
                                .OrderBy(a => a.Value.DisplayIndex)
                                .Select(a => a.Value.Name)
                                .ToArray<string>());
            fnAddResTotalGeo.SetDataGridViewDataSource(dataGridView: dgvData);
            fnAddResTotalGeo.FormatDataGridView(dataGridView: dgvData);
            grpMain.Text = fnAddResTotalGeo.Caption;
            txtSQL.Text = $"{Environment.NewLine}{fnAddResTotalGeo.SQL}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }


        /// <summary>
        /// Get data from stored procedure fn_get_panels_in_estimation_cells
        /// </summary>
        /// <returns>Data from stored procedure fn_get_panels_in_estimation_cells</returns>
        private TFnGetPanelsInEstimationCellsList GetFnPanelInEstimationCell()
        {
            EstimationCellList cEstimationCell = new(
                database: Database);
            cEstimationCell.ReLoad(condition: null, extended: false);
            EstimationCell ec = cEstimationCell.Items.OrderBy(a => a.Id).FirstOrDefault();
            if (ec == null)
            {
                return null;
            }

            EstimationPeriodList cEstimationPeriod = new(
                database: Database);
            cEstimationPeriod.ReLoad(condition: null, extended: false);
            EstimationPeriod ep = cEstimationPeriod.Items.OrderBy(a => a.Id).FirstOrDefault();
            if (ep == null)
            {
                return null;
            }

            VwVariableList vVariable = GetVwVariable();
            VwVariable vvar = vVariable.Items.OrderBy(a => a.VariableId).FirstOrDefault();
            if (vvar == null)
            {
                return null;
            }

            TFnGetPanelsInEstimationCellsList fnGetPanelsInEstimationCells =
               NfiEstaFunctions.FnGetPanelsInEstimationCells.Execute(
                   database: Database,
                   estimationCellIds: [ec.Id],
                   estimateDateBegin: ep.EstimateDateBegin,
                   estimateDateEnd: ep.EstimateDateEnd,
                   variableId: vvar.VariableId
                   );

            return fnGetPanelsInEstimationCells;
        }

        /// <summary>
        /// Stored procedure fn_get_panels_in_estimation_cells
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemFnPanelInEstimationCell_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(dgvData);

            TFnGetPanelsInEstimationCellsList fnGetPanelsInEstimationCells =
                GetFnPanelInEstimationCell();

            TFnGetPanelsInEstimationCellsList.SetColumnsVisibility(
                columnNames: (from a in TFnGetPanelsInEstimationCellsList.Cols
                              orderby a.Value.DisplayIndex
                              select a.Value.Name).ToArray<string>());
            fnGetPanelsInEstimationCells.SetDataGridViewDataSource(dataGridView: dgvData);
            fnGetPanelsInEstimationCells.FormatDataGridView(dataGridView: dgvData);
            grpMain.Text = fnGetPanelsInEstimationCells.Caption;
            txtSQL.Text = $"{Environment.NewLine}{fnGetPanelsInEstimationCells.SQL}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }

        /// <summary>
        /// Stored procedure fn_api_before_delete_panel_refyearset_group
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemFnApiBeforeDeletePanelRefYearSetGroup(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(dgvData);

            TFnApiBeforeDeletePanelRefYearSetGroupList fnApiBeforeDeletePanelRefYearSetGroupList =
                NfiEstaFunctions.FnApiBeforeDeletePanelRefYearSetGroup.Execute(database: Database, panelRefYearSetGroupId: 5);

            TFnApiBeforeDeletePanelRefYearSetGroupList.SetColumnsVisibility(
                columnNames: (from a in TFnApiBeforeDeletePanelRefYearSetGroupList.Cols
                              orderby a.Value.DisplayIndex
                              select a.Value.Name).ToArray<string>());
            fnApiBeforeDeletePanelRefYearSetGroupList.SetDataGridViewDataSource(dataGridView: dgvData);
            fnApiBeforeDeletePanelRefYearSetGroupList.FormatDataGridView(dataGridView: dgvData);
            grpMain.Text = FnApiBeforeDeletePanelRefYearSetGroup.Name;
            txtSQL.Text = $"{Environment.NewLine}{FnApiBeforeDeletePanelRefYearSetGroup.CommandText}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }

        /// <summary>
        /// Stored procedure fn_api_get_list_of_panel_refyearset_groups
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemFnApiGetListOfPanelRefYearSetGroups_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(dgvData);

            PanelRefYearSetGroupList panelRefYearSetGroupList =
                NfiEstaFunctions.FnApiGetListOfPanelRefYearSetGroups.Execute(database: Database);

            //NfiEstaFunctions.FnApiUpdatePanelRefYearSetGroup.Execute(
            //    database: Database,
            //    panelRefYearSetGroupId: panelRefYearSetGroupList[5].Id,
            //    panelRefYearSetGroupLabelCs: $"A - {panelRefYearSetGroupList[5].LabelCs}",
            //    panelRefYearSetGroupDescriptionCs: $"B - {panelRefYearSetGroupList[5].DescriptionCs}",
            //    panelRefYearSetGroupLabelEn: $"C - {panelRefYearSetGroupList[5].LabelEn}",
            //    panelRefYearSetGroupDescriptionEn: $"D - {panelRefYearSetGroupList[5].DescriptionEn}");

            //panelRefYearSetGroupList =
            //    NfiEstaFunctions.FnApiGetListOfPanelRefYearSetGroups.Execute(database: Database);

            PanelRefYearSetGroupList.SetColumnsVisibility(
                columnNames: (from a in PanelRefYearSetGroupList.Cols
                              orderby a.Value.DisplayIndex
                              select a.Value.Name).ToArray<string>());
            panelRefYearSetGroupList.SetDataGridViewDataSource(dataGridView: dgvData);
            panelRefYearSetGroupList.FormatDataGridView(dataGridView: dgvData);
            grpMain.Text = FnApiGetListOfPanelRefYearSetGroups.Name;
            txtSQL.Text = $"{Environment.NewLine}{FnApiGetListOfPanelRefYearSetGroups.CommandText}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }

        /// <summary>
        /// Stored procedure fn_api_get_panel_refyearset_combinations4groups
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemFnApiGetPanelRefYearSetCombinationsForGroups_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(dgvData);

            TFnApiGetPanelRefYearSetCombinationsForGroupsList combinations =
                NfiEstaFunctions.FnApiGetPanelRefYearSetCombinationsForGroups.Execute(
                    database: Database,
                    panelRefYearSetGroups: [5]);

            TFnApiGetPanelRefYearSetCombinationsForGroupsList.SetColumnsVisibility(
                columnNames: (from a in TFnApiGetPanelRefYearSetCombinationsForGroupsList.Cols
                              orderby a.Value.DisplayIndex
                              select a.Value.Name).ToArray<string>());
            combinations.SetDataGridViewDataSource(dataGridView: dgvData);
            combinations.FormatDataGridView(dataGridView: dgvData);
            grpMain.Text = FnApiGetPanelRefYearSetCombinationsForGroups.Name;
            txtSQL.Text = $"{Environment.NewLine}{FnApiGetPanelRefYearSetCombinationsForGroups.CommandText}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }

        /// <summary>
        /// Get data from stored procedure fn_etl_get_target_variable
        /// </summary>
        /// <returns>Data from stored procedure fn_etl_get_target_variable</returns>
        private FnEtlGetTargetVariableTypeList GetEtlGetTargetVariable()
        {
            FnEtlGetTargetVariableTypeList fnEtlGetTargetVariable =
                NfiEstaFunctions.FnEtlGetTargetVariable.Execute(
                    database: Database,
                    nationalLanguage: Language.CS,
                    etlId: [1]);

            return fnEtlGetTargetVariable;
        }

        /// <summary>
        /// Stored procedure fn_etl_get_target_variable
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemFnEtlGetTargetVariable_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(dgvData);

            FnEtlGetTargetVariableTypeList fnEtlGetTargetVariable =
                GetEtlGetTargetVariable();

            FnEtlGetTargetVariableTypeList.SetColumnsVisibility(
                columnNames: (from a in FnEtlGetTargetVariableTypeList.Cols
                              orderby a.Value.DisplayIndex
                              select a.Value.Name).ToArray<string>());
            fnEtlGetTargetVariable.SetDataGridViewDataSource(dataGridView: dgvData);
            fnEtlGetTargetVariable.FormatDataGridView(dataGridView: dgvData);
            grpMain.Text = fnEtlGetTargetVariable.Caption;
            txtSQL.Text = $"{Environment.NewLine}{fnEtlGetTargetVariable.SQL}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }

        /// <summary>
        /// Stored procedure fn_etl_get_target_variable_metadata
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemFnEtlGetTargetVariableMetadata_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(dgvData);

            FnEtlGetTargetVariableMetadataList fnEtlGetTargetVariableMetadata =
                new(database: Database);
            fnEtlGetTargetVariableMetadata.ReLoad(
                fnEtlGetTargetVariable: GetEtlGetTargetVariable());

            FnEtlGetTargetVariableMetadataList.SetColumnsVisibility(
                columnNames: (from a in FnEtlGetTargetVariableMetadataList.Cols
                              orderby a.Value.DisplayIndex
                              select a.Value.Name).ToArray<string>());
            fnEtlGetTargetVariableMetadata.SetDataGridViewDataSource(dataGridView: dgvData);
            fnEtlGetTargetVariableMetadata.FormatDataGridView(dataGridView: dgvData);
            grpMain.Text = fnEtlGetTargetVariableMetadata.Caption;
            txtSQL.Text = $"{Environment.NewLine}{fnEtlGetTargetVariableMetadata.SQL}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }

        /// <summary>
        /// Stored procedure fn_etl_get_target_variable_metadata aggregated
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemFnEtlGetTargetVariableMetadataAggregated_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(dgvData);

            FnEtlGetTargetVariableMetadataList fnEtlGetTargetVariableMetadata =
                new(database: Database);
            fnEtlGetTargetVariableMetadata.ReLoad(
                fnEtlGetTargetVariable: GetEtlGetTargetVariable());
            fnEtlGetTargetVariableMetadata = fnEtlGetTargetVariableMetadata.Aggregated();

            FnEtlGetTargetVariableMetadataList.SetColumnsVisibility(
                columnNames: (from a in FnEtlGetTargetVariableMetadataList.Cols
                              orderby a.Value.DisplayIndex
                              select a.Value.Name).ToArray<string>());
            fnEtlGetTargetVariableMetadata.SetDataGridViewDataSource(dataGridView: dgvData);
            fnEtlGetTargetVariableMetadata.FormatDataGridView(dataGridView: dgvData);
            grpMain.Text = fnEtlGetTargetVariableMetadata.Caption;
            txtSQL.Text = $"{Environment.NewLine}{fnEtlGetTargetVariableMetadata.SQL}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }

        /// <summary>
        /// Stored procedure fn_etl_get_target_variable_metadata aggregated
        /// displayed in target variable selector
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemControlTargetVariableSelector_Click(object sender, EventArgs e)
        {
            FnEtlGetTargetVariableTypeList fnEtlGetTargetVariable =
              NfiEstaFunctions.FnEtlGetTargetVariable.Execute(
                  database: Database,
                  nationalLanguage: Language.CS,
                  etlId: [1]);

            pnlData.Controls.Clear();
            ControlTargetVariableSelector ctrTargetVariableSelector
                = new(controlOwner: this)
                {
                    Design = TargetVariableSelectorDesign.Simple,
                    Dock = DockStyle.Fill,
                    MetadataElementsText = "Metadata",
                    MetadataIndicatorText = "Indicator",
                    NotAssignedCategory = true,
                    Spacing = [5]
                };
            ctrTargetVariableSelector.CreateControl();
            pnlData.Controls.Add(value: ctrTargetVariableSelector);

            ctrTargetVariableSelector.Data = fnEtlGetTargetVariable;
            ctrTargetVariableSelector.LoadContent();
        }

        #endregion Stored Procedures


        #region LINQ

        /// <summary>
        /// Prepare OLAP-cube for estimates of ratio
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemOLAPRatioEstimate_Click(object sender, EventArgs e)
        {
            //pnlData.Controls.Clear();
            //DataGridView dgvData = new DataGridView() { Dock = DockStyle.Fill };
            //dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            //pnlData.Controls.Add(dgvData);

            //OLAPRatioEstimateList olapRatioEstimate = GetOLAPRatioEstimate();

            //OLAPRatioEstimateList.SetColumnsVisibility(
            //    columnNames: (from a in OLAPRatioEstimateList.Cols
            //                  orderby a.Value.DisplayIndex
            //                  select a.Value.Name).ToArray<string>());
            //olapRatioEstimate.SetDataGridViewDataSource(dataGridView: dgvData);
            //olapRatioEstimate.FormatDataGridView(dataGridView: dgvData);
            //grpMain.Text = olapRatioEstimate.Caption;
            //txtSQL.Text = $"{Environment.NewLine}{olapRatioEstimate.SQL}{Environment.NewLine}";
            //PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }


        /// <summary>
        /// Prepare OLAP-dimensions for estimates of ratio
        /// </summary>
        /// <returns>OLAP-dimensions for estimates of ratio</returns>
        private OLAPRatioEstimateDimensionList GetOLAPRatioEstimateDimension()
        {
            PhaseEstimateTypeList cPhaseEstimateType = new(
               database: Database);
            cPhaseEstimateType.ReLoad(condition: null, extended: false);

            EstimationPeriodList cEstimationPeriod = new(
                database: Database);
            cEstimationPeriod.ReLoad(condition: null, extended: false);

            EstimationCellList cEstimationCell = new(
                database: Database);
            cEstimationCell.ReLoad(
                condition: "estimation_cell_collection IN (1)",
                extended: false);

            VwVariableList vwVariable = GetVwVariable();
            VariablePairList variablePairs = new(
                database: Database,
                variablePairs:
                    vwVariable.Items.Where(a => (new List<int>() { 18, 19, 20 }).Contains(a.VariableId))
                    .Select(a => new VariablePair(
                       numerator: a,
                       denominator: vwVariable.Items.Where(b => b.VariableId == 29).FirstOrDefault())));

            EstimationCellCollectionList cEstimationCellCollection = new(
                database: Database);
            cEstimationCellCollection.ReLoad(condition: null, extended: false);

            OLAPRatioEstimateDimensionList olapRatioEstimateDimension
                 = new(
                     database: Database);
            olapRatioEstimateDimension.ReLoad(
                cPhaseEstimateTypeNumerator: cPhaseEstimateType,
                cPhaseEstimateTypeDenominator: cPhaseEstimateType,
                cEstimationPeriod: cEstimationPeriod,
                variablePairs: variablePairs,
                cEstimationCell: cEstimationCell,
                cEstimationCellCollection: cEstimationCellCollection,
                condition: null);

            return olapRatioEstimateDimension;
        }

        /// <summary>
        /// Prepare OLAP-dimensions for estimates of ratio
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemOLAPRatioEstimateDimension_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(dgvData);

            OLAPRatioEstimateDimensionList olapRatioEstimateDimension = GetOLAPRatioEstimateDimension();

            OLAPRatioEstimateDimensionList.SetColumnsVisibility(
                columnNames: (from a in OLAPRatioEstimateDimensionList.Cols
                              orderby a.Value.DisplayIndex
                              select a.Value.Name).ToArray<string>());
            olapRatioEstimateDimension.SetDataGridViewDataSource(dataGridView: dgvData);
            olapRatioEstimateDimension.FormatDataGridView(dataGridView: dgvData);
            grpMain.Text = olapRatioEstimateDimension.Caption;
            txtSQL.Text = $"{Environment.NewLine}{olapRatioEstimateDimension.SQL}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }


        /// <summary>
        /// Prepare OLAP-cube for estimates of total
        /// </summary>
        /// <returns>OLAP-cube for estimates of total</returns>
        private OLAPTotalEstimateList GetOLAPEstimate()
        {
            OLAPTotalEstimateDimensionList olapTotalEstimateDimension =
                GetOLAPTotalEstimateDimension();
            OLAPTotalEstimateValueList olapTotalEstimateValue =
                GetOLAPTotalEstimateValue();

            EstimateConfStatusList cEstimateConfStatus =
                new(
                    database: Database);
            cEstimateConfStatus.ReLoad(
                condition: null,
                limit: null,
                extended: false);

            OLAPTotalEstimateList olapTotalEstimate =
                new(
                    database: Database);
            olapTotalEstimate.ReLoad(
                olapDimensions: olapTotalEstimateDimension,
                olapValues: olapTotalEstimateValue,
                cEstimateConfStatus: cEstimateConfStatus,
                additivityLimit: 0.00000001,
                condition: null,
                limit: null);

            return olapTotalEstimate;
        }

        /// <summary>
        /// Prepare OLAP-cube for estimates of total
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemOLAPTotalEstimate_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(dgvData);

            OLAPTotalEstimateList olapTotalEstimate =
                GetOLAPEstimate();

            OLAPTotalEstimateList.SetColumnsVisibility(
                columnNames: (from a in OLAPTotalEstimateList.Cols
                              orderby a.Value.DisplayIndex
                              select a.Value.Name).ToArray<string>());
            olapTotalEstimate.SetDataGridViewDataSource(dataGridView: dgvData);
            olapTotalEstimate.FormatDataGridView(dataGridView: dgvData);
            grpMain.Text = olapTotalEstimate.Caption;
            txtSQL.Text = $"{Environment.NewLine}{olapTotalEstimate.SQL}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }


        /// <summary>
        /// Prepare OLAP-dimensions for estimates of total
        /// </summary>
        /// <returns>OLAP-dimensions for estimates of total</returns>
        private OLAPTotalEstimateDimensionList GetOLAPTotalEstimateDimension()
        {
            // Fáze odhadu (1.dimenze)
            PhaseEstimateTypeList cPhaseEstimateType =
                new(
                    database: Database);
            cPhaseEstimateType.ReLoad(
                condition: "id IN (1, 3)",
                limit: null,
                extended: false);

            // Období odhadu (2.dimenze)
            EstimationPeriodList cEstimationPeriod =
                new(
                    database: Database);
            cEstimationPeriod.ReLoad(
                condition: "id IN (1, 9)",
                limit: null,
                extended: false);

            // Atributové kategorie (3.dimenze)
            VwVariableList vwVariable =
                new(
                    database: Database,
                    rows: GetVwVariable().Data.AsEnumerable()
                        .Where(a => (new List<int>() { 3415, 3416, 3421, 3422, 3426 })
                        .Contains(a.Field<int>(VwVariableList.ColVariableId.Name))));

            // Výpočetní buňka (4.dimenze) (14 krajů)
            EstimationCellList cEstimationCell =
                new(
                    database: Database);
            cEstimationCell.ReLoad(
                condition: "estimation_cell_collection IN (3)",
                limit: null,
                extended: false);

            EstimationCellCollectionList cEstimationCellCollection =
                new(
                    database: Database);
            cEstimationCellCollection.ReLoad(
                condition: "id IN (3)",
                limit: null,
                extended: false);

            // 168 možných odhadů
            OLAPTotalEstimateDimensionList olapTotalEstimateDimension
                = new(database: Database);
            olapTotalEstimateDimension.ReLoad(
                cPhaseEstimateType: cPhaseEstimateType,
                cEstimationPeriod: cEstimationPeriod,
                variables: vwVariable.Items,
                cEstimationCell: cEstimationCell,
                cEstimationCellCollection: cEstimationCellCollection,
                condition: null,
                limit: null);

            return olapTotalEstimateDimension;
        }

        /// <summary>
        /// Prepare OLAP-dimensions for estimates of total
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemOLAPTotalEstimateDimension_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(value: dgvData);

            OLAPTotalEstimateDimensionList olapTotalEstimateDimension =
                GetOLAPTotalEstimateDimension();

            OLAPTotalEstimateDimensionList.SetColumnsVisibility();
            olapTotalEstimateDimension.SetDataGridViewDataSource(dataGridView: dgvData);
            olapTotalEstimateDimension.FormatDataGridView(dataGridView: dgvData);
            grpMain.Text = olapTotalEstimateDimension.Caption;
            txtSQL.Text = $"{Environment.NewLine}{olapTotalEstimateDimension.SQL}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }



        /// <summary>
        /// Prepare OLAP-values for estimates of total
        /// </summary>
        /// <returns>OLAP-values for estimates of total</returns>
        private OLAPTotalEstimateValueList GetOLAPTotalEstimateValue()
        {
            // Fáze odhadu (1.dimenze)
            PhaseEstimateTypeList cPhaseEstimateType =
                new(
                    database: Database);
            cPhaseEstimateType.ReLoad(
                condition: "id IN (1, 3)",
                limit: null,
                extended: false);

            // Období odhadu (2.dimenze)
            EstimationPeriodList cEstimationPeriod =
                new(
                    database: Database);
            cEstimationPeriod.ReLoad(
                condition: "id IN (1, 9)",
                limit: null,
                extended: false);

            // Atributové kategorie (3.dimenze)
            VwVariableList vwVariable =
                new(
                    database: Database,
                    rows: GetVwVariable().Data.AsEnumerable()
                        .Where(a => (new List<int>() { 3415, 3416, 3421, 3422, 3426 })
                        .Contains(a.Field<int>(VwVariableList.ColVariableId.Name))));

            // Výpočetní buňka (4.dimenze) (14 krajů)
            EstimationCellList cEstimationCell =
                new(
                    database: Database);
            cEstimationCell.ReLoad(
                condition: "estimation_cell_collection IN (3)",
                limit: null,
                extended: false);

            // Skupiny panelů pro období odhadu
            PanelRefYearSetGroupList cPanelRefYearSetGroup =
                new(
                    database: Database);
            cPanelRefYearSetGroup.ReLoad(
                condition: null,
                limit: null,
                extended: false);

            // Odhady
            EstimateConfList tEstimateConf =
                new(
                    database: Database);
            tEstimateConf.ReLoad(
                condition: null,
                limit: null);

            // Odhady úhrnu
            TotalEstimateConfList tTotalEstimateConf =
                new(
                    database: Database);
            tTotalEstimateConf.ReLoad(
                condition: null,
                limit: null);

            // Výsledky odhadu úhrnu
            ResultList tResult = new(
                database: Database);
            tResult.ReLoad(
                condition: null,
                limit: null);

            // Atributová aditivita
            TFnAddResTotalAttrList fnAddResTotalAttr =
                GetFnAddResTotalAttr();

            // Geografická aditivita
            TFnAddResTotalGeoList fnAddResTotalGeo =
                GetFnAddResTotalGeo();

            OLAPTotalEstimateValueList olapTotalEstimateValue =
                new(database: Database);

            olapTotalEstimateValue.ReLoad(
                cPhaseEstimateType: cPhaseEstimateType,
                cEstimationPeriod: cEstimationPeriod,
                variables: vwVariable.Items,
                cEstimationCell: cEstimationCell,
                cPanelRefYearSetGroup: cPanelRefYearSetGroup,
                tEstimateConf: tEstimateConf,
                tTotalEstimateConf: tTotalEstimateConf,
                tResult: tResult,
                fnAddResTotalAttr: fnAddResTotalAttr,
                fnAddResTotalGeo: fnAddResTotalGeo,
                additivityLimit: 0.00000001,
                isLatest: true,
                condition: null,
                limit: null,
                tAuxConf: null,
                tModel: null,
                faParamArea: null,
                cParamAreaType: null);

            return olapTotalEstimateValue;
        }

        /// <summary>
        /// Prepare OLAP-values for estimates of total
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemOLAPTotalEstimateValue_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(dgvData);

            OLAPTotalEstimateValueList olapTotalEstimateValue = GetOLAPTotalEstimateValue();

            OLAPTotalEstimateValueList.SetColumnsVisibility(
                columnNames: (from a in OLAPTotalEstimateValueList.Cols
                              orderby a.Value.DisplayIndex
                              select a.Value.Name).ToArray<string>());
            olapTotalEstimateValue.SetDataGridViewDataSource(dataGridView: dgvData);
            olapTotalEstimateValue.FormatDataGridView(dataGridView: dgvData);
            grpMain.Text = olapTotalEstimateValue.Caption;
            txtSQL.Text = $"{Environment.NewLine}{olapTotalEstimateValue.SQL}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }


        /// <summary>
        /// View PanelRefYearSetPair
        /// </summary>
        /// <returns>View PanelRefYearSetPair</returns>
        private VwPanelRefYearSetPairList GetVwPanelRefYearSetPair()
        {
            PanelRefYearSetPairList tPanelRefYearSetGroup = new(
                database: Database);
            tPanelRefYearSetGroup.ReLoad(condition: null);

            SDesign.PanelList tPanel = new(database: Database);
            tPanel.ReLoad(condition: null);

            SDesign.ReferenceYearSetList tReferenceYearSet = new(
                database: Database);
            tReferenceYearSet.ReLoad(condition: null);

            VwPanelRefYearSetPairList vwPanelRefYearSetPair = new(
                database: Database);
            vwPanelRefYearSetPair.ReLoad(
                tPanelRefYearSetGroup: tPanelRefYearSetGroup,
                tPanel: tPanel,
                tReferenceYearSet: tReferenceYearSet,
                condition: null);

            return vwPanelRefYearSetPair;
        }

        /// <summary>
        /// View PanelRefYearSetPair
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemVwPanelRefYearSetPair_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(dgvData);

            VwPanelRefYearSetPairList vwPanelRefYearSetPair = GetVwPanelRefYearSetPair();

            VwPanelRefYearSetPairList.SetColumnsVisibility(
                columnNames: (from a in VwPanelRefYearSetPairList.Cols
                              orderby a.Value.DisplayIndex
                              select a.Value.Name).ToArray<string>());
            vwPanelRefYearSetPair.SetDataGridViewDataSource(dataGridView: dgvData);
            vwPanelRefYearSetPair.FormatDataGridView(dataGridView: dgvData);
            grpMain.Text = vwPanelRefYearSetPair.Caption;
            txtSQL.Text = $"{Environment.NewLine}{vwPanelRefYearSetPair.SQL}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }


        /// <summary>
        /// View Variable
        /// </summary>
        /// <returns>View Variable</returns>
        private VwVariableList GetVwVariable()
        {
            VariableList tVariable = new(database: Database);
            tVariable.ReLoad(condition: null);

            SubPopulationCategoryList cSubPopulationCategory = new(database: Database);
            cSubPopulationCategory.ReLoad(condition: null, extended: true);

            SubPopulationList cSubPopulation = new(database: Database);
            cSubPopulation.ReLoad(condition: null, extended: true);

            AreaDomainCategoryList cAreaDomainCategory = new(database: Database);
            cAreaDomainCategory.ReLoad(condition: null, extended: true);

            AreaDomainList cAreaDomain = new(database: Database);
            cAreaDomain.ReLoad(condition: null, extended: true);

            AuxiliaryVariableCategoryList cAuxiliaryVariableCategory = new(database: Database);
            cAuxiliaryVariableCategory.ReLoad(condition: null, extended: true);

            AuxiliaryVariableList cAuxiliaryVariable = new(database: Database);
            cAuxiliaryVariable.ReLoad(condition: null, extended: true);

            VwVariableList vwVariable = new(database: Database);
            vwVariable.ReLoad(
                tVariable: tVariable,
                cSubPopulationCategory: cSubPopulationCategory,
                cSubPopulation: cSubPopulation,
                cAreaDomainCategory: cAreaDomainCategory,
                cAreaDomain: cAreaDomain,
                cAuxiliaryVariableCategory: cAuxiliaryVariableCategory,
                cAuxiliaryVariable: cAuxiliaryVariable,
                condition: null);

            return vwVariable;
        }

        /// <summary>
        /// View Variable
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemVwVariable_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(dgvData);

            VwVariableList vwVariable = GetVwVariable();

            VwVariableList.SetColumnsVisibility(
                columnNames: (from a in VwVariableList.Cols
                              orderby a.Value.DisplayIndex
                              select a.Value.Name).ToArray<string>());
            vwVariable.SetDataGridViewDataSource(dataGridView: dgvData);
            vwVariable.FormatDataGridView(dataGridView: dgvData);
            grpMain.Text = vwVariable.Caption;
            txtSQL.Text = $"{Environment.NewLine}{vwVariable.SQL}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }

        /// <summary>
        /// View Estimation Cells Count
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemVwEstimationCellsCount_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(dgvData);

            TFnGetEstimationCellsNumberInCollectionList vwEstimationCellsCount =
                new(
                    database: Database);
            vwEstimationCellsCount = NfiEstaFunctions.FnGetEstimationCellsNumberInCollection.Execute(database: Database);

            TFnGetEstimationCellsNumberInCollectionList.SetColumnsVisibility(
                columnNames: (from a in TFnGetEstimationCellsNumberInCollectionList.Cols
                              orderby a.Value.DisplayIndex
                              select a.Value.Name).ToArray<string>());
            vwEstimationCellsCount.SetDataGridViewDataSource(dataGridView: dgvData);
            vwEstimationCellsCount.FormatDataGridView(dataGridView: dgvData);
            grpMain.Text = vwEstimationCellsCount.Caption;
            txtSQL.Text = $"{Environment.NewLine}{vwEstimationCellsCount.SQL}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }

        #endregion LINQ

        #endregion Event Handlers

    }

}