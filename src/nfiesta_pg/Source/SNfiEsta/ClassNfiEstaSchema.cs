﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Versioning;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.Catalog;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {

            /// <summary>
            /// Schema nfiesta
            /// </summary>
            public class NfiEstaSchema
                : IDatabaseSchema
            {

                #region Constants

                /// <summary>
                /// Schema name,
                /// contains default value,
                /// value is not constant now,
                /// extension can be created with different schemas
                /// </summary>
                public static string Name = "nfiesta";

                /// <summary>
                /// Extension name
                /// </summary>
                public const string ExtensionName = "nfiesta";

                /// <summary>
                /// Extension version
                /// </summary>
                public static readonly ExtensionVersion ExtensionVersion =
                    new(version: "3.17.0");

                #endregion Constants


                #region Private Fields

                #region General

                /// <summary>
                /// Database
                /// </summary>
                private IDatabase database;

                /// <summary>
                /// Omitted tables
                /// </summary>
                private List<string> omittedTables;

                #endregion General


                #region Lookup Tables

                /// <summary>
                /// List of area domains
                /// </summary>
                private ZaJi.NfiEstaPg.Core.AreaDomainList cAreaDomain;

                /// <summary>
                /// List of area domain categories
                /// </summary>
                private ZaJi.NfiEstaPg.Core.AreaDomainCategoryList cAreaDomainCategory;

                /// <summary>
                /// List of auxiliary variables
                /// </summary>
                private ZaJi.NfiEstaPg.Core.AuxiliaryVariableList cAuxiliaryVariable;

                /// <summary>
                /// List of auxiliary variable categories
                /// </summary>
                private ZaJi.NfiEstaPg.Core.AuxiliaryVariableCategoryList cAuxiliaryVariableCategory;

                /// <summary>
                /// Estimate configuration status
                /// </summary>
                private ZaJi.NfiEstaPg.Core.EstimateConfStatusList cEstimateConfStatus;

                /// <summary>
                /// List of estimate types
                /// </summary>
                private ZaJi.NfiEstaPg.Core.EstimateTypeList cEstimateType;

                /// <summary>
                /// List of estimation cells
                /// </summary>
                private ZaJi.NfiEstaPg.Core.EstimationCellList cEstimationCell;

                /// <summary>
                /// List of estimation cell collections
                /// </summary>
                private ZaJi.NfiEstaPg.Core.EstimationCellCollectionList cEstimationCellCollection;

                /// <summary>
                /// List of estimation periods
                /// </summary>
                private ZaJi.NfiEstaPg.Core.EstimationPeriodList cEstimationPeriod;

                /// <summary>
                /// List of aggregated sets of panels and corresponding reference year sets
                /// </summary>
                private ZaJi.NfiEstaPg.Core.PanelRefYearSetGroupList cPanelRefYearSetGroup;

                /// <summary>
                /// List of parametrization area types
                /// </summary>
                private ZaJi.NfiEstaPg.Core.ParamAreaTypeList cParamAreaType;

                /// <summary>
                /// List of estimate types
                /// </summary>
                private ZaJi.NfiEstaPg.Core.PhaseEstimateTypeList cPhaseEstimateType;

                /// <summary>
                /// List of sub-populations
                /// </summary>
                private ZaJi.NfiEstaPg.Core.SubPopulationList cSubPopulation;

                /// <summary>
                /// List of sub-population categories
                /// </summary>
                private ZaJi.NfiEstaPg.Core.SubPopulationCategoryList cSubPopulationCategory;

                /// <summary>
                /// List of target variables
                /// </summary>
                private ZaJi.NfiEstaPg.Core.TargetVariableList cTargetVariable;

                /// <summary>
                /// List of topics
                /// </summary>
                private ZaJi.NfiEstaPg.Core.TopicList cTopic;

                #endregion Lookup Tables


                #region Mapping Tables

                /// <summary>
                /// List of mappings area domain categories
                /// </summary>
                private ZaJi.NfiEstaPg.Core.AreaDomainCategoryMappingList cmAreaDomainCategoryMapping;

                /// <summary>
                /// List of mappings between estimation cell and parametrization area
                /// </summary>
                private ZaJi.NfiEstaPg.Core.EstimationCellToParamAreaMappingList cmEstimationCellToParamAreaMapping;

                /// <summary>
                /// List of mappings between plot and estimation cell
                /// </summary>
                private ZaJi.NfiEstaPg.Core.PlotToEstimationCellMappingList cmPlotToEstimationCellMapping;

                /// <summary>
                /// List of mappings between plot and parametrization area
                /// </summary>
                private ZaJi.NfiEstaPg.Core.PlotToParametrizationAreaMappingList cmPlotToParametrizationAreaMapping;

                /// <summary>
                /// List of mappings subpopulation categories
                /// </summary>
                private ZaJi.NfiEstaPg.Core.SubPopulationCategoryMappingList cmSubPopulationCategoryMapping;

                /// <summary>
                /// List of mappings between target variable and topic
                /// </summary>
                private ZaJi.NfiEstaPg.Core.TargetVariableToTopicList cmTargetVariableToTopic;

                #endregion Mapping Tables


                #region Spatial Data Tables

                /// <summary>
                /// List of estimation cell segments
                /// </summary>
                private ZaJi.NfiEstaPg.Core.CellList faCell;

                /// <summary>
                /// List of parametrization areas
                /// </summary>
                private ZaJi.NfiEstaPg.Core.ParamAreaList faParamArea;

                /// <summary>
                /// List of intersections between stratum and estimation cell
                /// </summary>
                private ZaJi.NfiEstaPg.Core.StratumInEstimationCellList tStratumInEstimationCell;

                #endregion Spatial Data Tables


                #region Data Tables

                /// <summary>
                /// List of additivity checks
                /// </summary>
                private ZaJi.NfiEstaPg.Core.AdditivitySetPlotList tAdditivitySetPlot;

                /// <summary>
                /// List of parametrization configurations
                /// </summary>
                private ZaJi.NfiEstaPg.Core.AuxConfList tAuxConf;

                /// <summary>
                /// List of totals of auxiliary variable within estimation cell
                /// </summary>
                private ZaJi.NfiEstaPg.Core.AuxTotalList tAuxTotal;

                /// <summary>
                /// List of auxiliary plot data
                /// </summary>
                private ZaJi.NfiEstaPg.Core.AuxDataList tAuxiliaryData;

                /// <summary>
                /// List of available datasets
                /// </summary>
                private ZaJi.NfiEstaPg.Core.AvailableDataSetList tAvailableDataSet;

                /// <summary>
                /// List of estimate configurations
                /// </summary>
                private ZaJi.NfiEstaPg.Core.EstimateConfList tEstimateConf;

                /// <summary>
                /// List of estimation cell mappings
                /// </summary>
                private ZaJi.NfiEstaPg.Core.EstimationCellHierarchyList tEstimationCellHierarchy;

                /// <summary>
                /// List of precalculated G_Betas for given auxiliary configuration
                /// </summary>
                private ZaJi.NfiEstaPg.Core.GBetaList tgBeta;

                /// <summary>
                /// List of models
                /// </summary>
                private ZaJi.NfiEstaPg.Core.ModelList tModel;

                /// <summary>
                /// List of explanatory variables
                /// </summary>
                private ZaJi.NfiEstaPg.Core.ModelVariableList tModelVariable;

                /// <summary>
                /// List of mappings between panels and total estimate configuration (zero and the first phase)
                /// </summary>
                private ZaJi.NfiEstaPg.Core.PanelToTotalEstimateConfList tPanelToTotalEstimateConfFirstPhase;

                /// <summary>
                /// List of pairs of panel with corresponding reference year set
                /// </summary>
                private ZaJi.NfiEstaPg.Core.PanelRefYearSetPairList tPanelRefYearSetGroup;

                /// <summary>
                /// List of final estimates
                /// </summary>
                private ZaJi.NfiEstaPg.Core.ResultList tResult;

                /// <summary>
                /// List of measured plot data
                /// </summary>
                private ZaJi.NfiEstaPg.Core.TargetDataList tTargetData;

                /// <summary>
                /// List of total estimate configurations
                /// </summary>
                private ZaJi.NfiEstaPg.Core.TotalEstimateConfList tTotalEstimateConf;

                /// <summary>
                /// List of combinations of target variable and sub-population category and area domain category
                /// </summary>
                private ZaJi.NfiEstaPg.Core.VariableList tVariable;

                /// <summary>
                /// List of variable mappings
                /// </summary>
                private ZaJi.NfiEstaPg.Core.VariableHierarchyList tVariableHierarchy;

                #endregion Data Tables


                #region Data Views

                /// <summary>
                /// List of  target variable metadata
                /// </summary>
                private ZaJi.NfiEstaPg.Core.TargetVariableMetadataList cTargetVariableMetadata;

                /// <summary>
                /// List of  aggregated target variable metadata
                /// </summary>
                private ZaJi.NfiEstaPg.Core.TargetVariableMetadataList cAggregateTargetVariableMetadata;


                /// <summary>
                /// List of pairs of panel with corresponding reference year set (extended version)
                /// </summary>
                private ZaJi.NfiEstaPg.Core.VwPanelRefYearSetPairList vPanelRefYearSetPairs;

                /// <summary>
                /// List of combinations of target variable and sub-population category and area domain category (extended version)
                /// </summary>
                private ZaJi.NfiEstaPg.Core.VwVariableList vVariable;

                /// <summary>
                /// Count of estimation cells in estimation cell collection
                /// </summary>
                private ZaJi.NfiEstaPg.Core.TFnGetEstimationCellsNumberInCollectionList vEstimationCellsCount;

                /// <summary>
                /// List of total estimates attribute additivity for ratios
                /// </summary>
                private ZaJi.NfiEstaPg.Core.TFnAddResRatioAttrList vRatioAdditivityAttr;

                /// <summary>
                /// List of total estimates attribute additivity for totals
                /// </summary>
                private ZaJi.NfiEstaPg.Core.TFnAddResTotalAttrList vTotalAdditivityAttr;

                /// <summary>
                /// List of total estimates geographic additivity for totals
                /// </summary>
                private ZaJi.NfiEstaPg.Core.TFnAddResTotalGeoList vTotalAdditivityGeo;

                /// <summary>
                /// OLAP Cube - List of Total Estimates
                /// </summary>
                private ZaJi.NfiEstaPg.Core.OLAPTotalEstimateList vTotalOLAP;

                /// <summary>
                /// OLAP Cube - List of Ratio Estimates
                /// </summary>
                private ZaJi.NfiEstaPg.Core.OLAPRatioEstimateList vRatioOLAP;

                #endregion Data Views

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database</param>
                public NfiEstaSchema(NfiEstaDB database)
                {

                    #region General

                    Database = database;

                    OmittedTables = [];

                    #endregion General


                    #region Lookup Tables

                    CAreaDomain = new ZaJi.NfiEstaPg.Core.AreaDomainList(
                        database: (NfiEstaDB)Database);
                    CAreaDomainCategory = new ZaJi.NfiEstaPg.Core.AreaDomainCategoryList(
                        database: (NfiEstaDB)Database);
                    CAuxiliaryVariable = new ZaJi.NfiEstaPg.Core.AuxiliaryVariableList(
                        database: (NfiEstaDB)Database);
                    CAuxiliaryVariableCategory = new ZaJi.NfiEstaPg.Core.AuxiliaryVariableCategoryList(
                        database: (NfiEstaDB)Database);
                    CEstimateConfStatus = new ZaJi.NfiEstaPg.Core.EstimateConfStatusList(
                        database: (NfiEstaDB)Database);
                    CEstimateType = new ZaJi.NfiEstaPg.Core.EstimateTypeList(
                        database: (NfiEstaDB)Database);
                    CEstimationCell = new ZaJi.NfiEstaPg.Core.EstimationCellList(
                        database: (NfiEstaDB)Database);
                    CEstimationCellCollection = new ZaJi.NfiEstaPg.Core.EstimationCellCollectionList(
                        database: (NfiEstaDB)Database);
                    CEstimationPeriod = new ZaJi.NfiEstaPg.Core.EstimationPeriodList(
                        database: (NfiEstaDB)Database);
                    CPanelRefYearSetGroup = new ZaJi.NfiEstaPg.Core.PanelRefYearSetGroupList(
                        database: (NfiEstaDB)Database);
                    CParamAreaType = new ZaJi.NfiEstaPg.Core.ParamAreaTypeList(
                        database: (NfiEstaDB)Database);
                    CPhaseEstimateType = new ZaJi.NfiEstaPg.Core.PhaseEstimateTypeList(
                        database: (NfiEstaDB)Database);
                    CSubPopulation = new ZaJi.NfiEstaPg.Core.SubPopulationList(
                        database: (NfiEstaDB)Database);
                    CSubPopulationCategory = new ZaJi.NfiEstaPg.Core.SubPopulationCategoryList(
                        database: (NfiEstaDB)Database);
                    CTargetVariable = new ZaJi.NfiEstaPg.Core.TargetVariableList(
                        database: (NfiEstaDB)Database);
                    CTopic = new ZaJi.NfiEstaPg.Core.TopicList(
                        database: (NfiEstaDB)Database);

                    #endregion Lookup Tables


                    #region Mapping tables

                    CmAreaDomainCategoryMapping = new ZaJi.NfiEstaPg.Core.AreaDomainCategoryMappingList(
                        database: (NfiEstaDB)Database);
                    CmEstimationCellToParamAreaMapping = new ZaJi.NfiEstaPg.Core.EstimationCellToParamAreaMappingList(
                        database: (NfiEstaDB)Database);
                    CmPlotToEstimationCellMapping = new ZaJi.NfiEstaPg.Core.PlotToEstimationCellMappingList(
                        database: (NfiEstaDB)Database);
                    CmPlotToParametrizationAreaMapping = new ZaJi.NfiEstaPg.Core.PlotToParametrizationAreaMappingList(
                        database: (NfiEstaDB)Database);
                    CmSubPopulationCategoryMapping = new ZaJi.NfiEstaPg.Core.SubPopulationCategoryMappingList(
                        database: (NfiEstaDB)Database);
                    CmTargetVariableToTopic = new ZaJi.NfiEstaPg.Core.TargetVariableToTopicList(
                        database: (NfiEstaDB)Database);

                    #endregion Mapping tables


                    #region Spatial data tables

                    FaCell = new ZaJi.NfiEstaPg.Core.CellList(
                        database: (NfiEstaDB)Database);
                    FaParamArea = new ZaJi.NfiEstaPg.Core.ParamAreaList(
                        database: (NfiEstaDB)Database);
                    TStratumInEstimationCell = new ZaJi.NfiEstaPg.Core.StratumInEstimationCellList(
                        database: (NfiEstaDB)Database);

                    #endregion Spatial data tables


                    #region Data tables

                    TAdditivitySetPlot = new ZaJi.NfiEstaPg.Core.AdditivitySetPlotList(
                        database: (NfiEstaDB)Database);
                    TAuxConf = new ZaJi.NfiEstaPg.Core.AuxConfList(
                        database: (NfiEstaDB)Database);
                    TAuxTotal = new ZaJi.NfiEstaPg.Core.AuxTotalList(
                        database: (NfiEstaDB)Database);
                    TAuxiliaryData = new ZaJi.NfiEstaPg.Core.AuxDataList(
                        database: (NfiEstaDB)Database);
                    TAvailableDataSet = new ZaJi.NfiEstaPg.Core.AvailableDataSetList(
                        database: (NfiEstaDB)Database);
                    TEstimateConf = new ZaJi.NfiEstaPg.Core.EstimateConfList(
                        database: (NfiEstaDB)Database);
                    TEstimationCellHierarchy = new ZaJi.NfiEstaPg.Core.EstimationCellHierarchyList(
                        database: (NfiEstaDB)Database);
                    TgBeta = new ZaJi.NfiEstaPg.Core.GBetaList(
                        database: (NfiEstaDB)Database);
                    TModel = new ZaJi.NfiEstaPg.Core.ModelList(
                        database: (NfiEstaDB)Database);
                    TModelVariable = new ZaJi.NfiEstaPg.Core.ModelVariableList(
                        database: (NfiEstaDB)Database);
                    TPanelToTotalEstimateConfFirstPhase = new ZaJi.NfiEstaPg.Core.PanelToTotalEstimateConfList(
                        database: (NfiEstaDB)Database);
                    TPanelRefYearSetGroup = new ZaJi.NfiEstaPg.Core.PanelRefYearSetPairList(
                        database: (NfiEstaDB)Database);
                    TResult = new ZaJi.NfiEstaPg.Core.ResultList(
                        database: (NfiEstaDB)Database);
                    TTargetData = new ZaJi.NfiEstaPg.Core.TargetDataList(
                        database: (NfiEstaDB)Database);
                    TTotalEstimateConf = new ZaJi.NfiEstaPg.Core.TotalEstimateConfList(
                        database: (NfiEstaDB)Database);
                    TVariable = new ZaJi.NfiEstaPg.Core.VariableList(
                        database: (NfiEstaDB)Database);
                    TVariableHierarchy = new ZaJi.NfiEstaPg.Core.VariableHierarchyList(
                        database: (NfiEstaDB)Database);

                    #endregion Data tables


                    #region Data Views

                    CTargetVariableMetadata = new ZaJi.NfiEstaPg.Core.TargetVariableMetadataList(
                        database: (NfiEstaDB)Database);
                    CAggregateTargetVariableMetadata = new ZaJi.NfiEstaPg.Core.TargetVariableMetadataList(
                        database: (NfiEstaDB)Database);

                    VPanelRefYearSetPairs = new ZaJi.NfiEstaPg.Core.VwPanelRefYearSetPairList(
                        database: (NfiEstaDB)Database);
                    VVariable = new ZaJi.NfiEstaPg.Core.VwVariableList(
                        database: (NfiEstaDB)Database);
                    VEstimationCellsCount = new ZaJi.NfiEstaPg.Core.TFnGetEstimationCellsNumberInCollectionList(
                        database: (NfiEstaDB)Database);

                    VTotalAdditivityAttr = null;
                    VRatioAdditivityAttr = null;
                    VTotalAdditivityGeo = null;

                    VTotalOLAP = new ZaJi.NfiEstaPg.Core.OLAPTotalEstimateList(
                        database: (NfiEstaDB)Database);
                    VRatioOLAP = new ZaJi.NfiEstaPg.Core.OLAPRatioEstimateList(
                        database: (NfiEstaDB)Database);

                    #endregion Data Views

                }

                #endregion Constructor


                #region Properties

                #region General

                /// <summary>
                /// Schema name (read-only)
                /// </summary>
                public string SchemaName
                {
                    get
                    {
                        return NfiEstaSchema.Name;
                    }
                }

                /// <summary>
                /// Database (not-null) (read-only)
                /// </summary>
                public IDatabase Database
                {
                    get
                    {
                        if (database == null)
                        {
                            throw new ArgumentNullException(
                                message: $"Argument {nameof(Database)} must not be null.",
                                paramName: nameof(Database));
                        }

                        if (database is not NfiEstaDB)
                        {
                            throw new ArgumentException(
                                message: $"Argument {nameof(Database)} must be type of {nameof(NfiEstaDB)}.",
                                paramName: nameof(Database));
                        }

                        return database;
                    }
                    private set
                    {
                        if (value == null)
                        {
                            throw new ArgumentNullException(
                                message: $"Argument {nameof(Database)} must not be null.",
                                paramName: nameof(Database));
                        }

                        if (value is not NfiEstaDB)
                        {
                            throw new ArgumentException(
                                message: $"Argument {nameof(Database)} must be type of {nameof(NfiEstaDB)}.",
                                paramName: nameof(Database));
                        }

                        database = value;
                    }
                }

                /// <summary>
                /// List of lookup tables (not-null) (read-only)
                /// </summary>
                public List<ILookupTable> LookupTables
                {
                    get
                    {
                        return
                        [
                            CAreaDomain,
                            CAreaDomainCategory,
                            CAuxiliaryVariable,
                            CAuxiliaryVariableCategory,
                            CEstimateConfStatus,
                            CEstimateType,
                            CEstimationCell,
                            CEstimationCellCollection,
                            CEstimationPeriod,
                            CPanelRefYearSetGroup,
                            CParamAreaType,
                            CPhaseEstimateType,
                            CSubPopulation,
                            CSubPopulationCategory,
                            CTopic,
                            TModel
                        ];
                    }
                }

                /// <summary>
                /// List of mapping tables (not-null) (read-only)
                /// </summary>
                public List<IMappingTable> MappingTables
                {
                    get
                    {
                        return
                        [
                            CmAreaDomainCategoryMapping,
                            CmEstimationCellToParamAreaMapping,
                            CmPlotToEstimationCellMapping,
                            CmPlotToParametrizationAreaMapping,
                            CmSubPopulationCategoryMapping,
                            CmTargetVariableToTopic,
                        ];
                    }
                }

                /// <summary>
                /// List of spatial data tables (not-null) (read-only)
                /// </summary>
                public List<ISpatialTable> SpatialTables
                {
                    get
                    {
                        return
                        [
                            FaCell,
                            FaParamArea,
                            TStratumInEstimationCell,
                        ];
                    }
                }

                /// <summary>
                /// List of data tables (not-null) (read-only)
                /// </summary>
                public List<IDataTable> DataTables
                {
                    get
                    {
                        return
                        [
                           CTargetVariable,
                           TAdditivitySetPlot,
                           TAuxConf,
                           TAuxTotal,
                           TAuxiliaryData,
                           TAvailableDataSet,
                           TEstimateConf,
                           TEstimationCellHierarchy,
                           TgBeta,
                           TModelVariable,
                           TPanelToTotalEstimateConfFirstPhase,
                           TPanelRefYearSetGroup,
                           TResult,
                           TTargetData,
                           TTotalEstimateConf,
                           TVariable,
                           TVariableHierarchy
                        ];
                    }
                }

                /// <summary>
                /// List of all tables (not-null) (read-only)
                /// </summary>
                public List<IDatabaseTable> Tables
                {
                    get
                    {
                        List<IDatabaseTable> tables = [];
                        tables.AddRange(collection: LookupTables);
                        tables.AddRange(collection: MappingTables);
                        tables.AddRange(collection: SpatialTables);
                        tables.AddRange(collection: DataTables);
                        tables.Sort(comparison: (a, b) => a.TableName.CompareTo(strB: b.TableName));
                        return tables;
                    }
                }

                /// <summary>
                /// Omitted tables
                /// </summary>
                public List<string> OmittedTables
                {
                    get
                    {
                        return omittedTables ?? [];
                    }
                    set
                    {
                        omittedTables = value ?? [];
                    }
                }

                #endregion General


                #region Lookup Tables

                /// <summary>
                /// List of area domains (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.Core.AreaDomainList CAreaDomain
                {
                    get
                    {
                        return cAreaDomain ??
                            new ZaJi.NfiEstaPg.Core.AreaDomainList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        cAreaDomain = value ??
                            new ZaJi.NfiEstaPg.Core.AreaDomainList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of area domain categories (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.Core.AreaDomainCategoryList CAreaDomainCategory
                {
                    get
                    {
                        return cAreaDomainCategory ??
                            new ZaJi.NfiEstaPg.Core.AreaDomainCategoryList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        cAreaDomainCategory = value ??
                            new ZaJi.NfiEstaPg.Core.AreaDomainCategoryList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of auxiliary variables (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.Core.AuxiliaryVariableList CAuxiliaryVariable
                {
                    get
                    {
                        return cAuxiliaryVariable ??
                            new ZaJi.NfiEstaPg.Core.AuxiliaryVariableList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        cAuxiliaryVariable = value ??
                            new ZaJi.NfiEstaPg.Core.AuxiliaryVariableList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of auxiliary variable categories (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.Core.AuxiliaryVariableCategoryList CAuxiliaryVariableCategory
                {
                    get
                    {
                        return cAuxiliaryVariableCategory ??
                            new ZaJi.NfiEstaPg.Core.AuxiliaryVariableCategoryList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        cAuxiliaryVariableCategory = value ??
                            new ZaJi.NfiEstaPg.Core.AuxiliaryVariableCategoryList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// Estimate configuration status (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.Core.EstimateConfStatusList CEstimateConfStatus
                {
                    get
                    {
                        return cEstimateConfStatus ??
                            new ZaJi.NfiEstaPg.Core.EstimateConfStatusList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        cEstimateConfStatus = value ??
                            new ZaJi.NfiEstaPg.Core.EstimateConfStatusList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of estimate types (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.Core.EstimateTypeList CEstimateType
                {
                    get
                    {
                        return cEstimateType ??
                            new ZaJi.NfiEstaPg.Core.EstimateTypeList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        cEstimateType = value ??
                            new ZaJi.NfiEstaPg.Core.EstimateTypeList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of estimation cells (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.Core.EstimationCellList CEstimationCell
                {
                    get
                    {
                        return cEstimationCell ??
                            new ZaJi.NfiEstaPg.Core.EstimationCellList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        cEstimationCell = value ??
                            new ZaJi.NfiEstaPg.Core.EstimationCellList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of estimation cell collections (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.Core.EstimationCellCollectionList CEstimationCellCollection
                {
                    get
                    {
                        return cEstimationCellCollection ??
                            new ZaJi.NfiEstaPg.Core.EstimationCellCollectionList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        cEstimationCellCollection = value ??
                            new ZaJi.NfiEstaPg.Core.EstimationCellCollectionList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of estimation periods (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.Core.EstimationPeriodList CEstimationPeriod
                {
                    get
                    {
                        return cEstimationPeriod ??
                            new ZaJi.NfiEstaPg.Core.EstimationPeriodList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        cEstimationPeriod = value ??
                            new ZaJi.NfiEstaPg.Core.EstimationPeriodList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of aggregated sets of panels and corresponding reference year sets (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.Core.PanelRefYearSetGroupList CPanelRefYearSetGroup
                {
                    get
                    {
                        return cPanelRefYearSetGroup ??
                            new ZaJi.NfiEstaPg.Core.PanelRefYearSetGroupList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        cPanelRefYearSetGroup = value ??
                            new ZaJi.NfiEstaPg.Core.PanelRefYearSetGroupList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of parametrization area types (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.Core.ParamAreaTypeList CParamAreaType
                {
                    get
                    {
                        return cParamAreaType ??
                            new ZaJi.NfiEstaPg.Core.ParamAreaTypeList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        cParamAreaType = value ??
                            new ZaJi.NfiEstaPg.Core.ParamAreaTypeList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of estimate types (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.Core.PhaseEstimateTypeList CPhaseEstimateType
                {
                    get
                    {
                        return cPhaseEstimateType ??
                            new ZaJi.NfiEstaPg.Core.PhaseEstimateTypeList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        cPhaseEstimateType = value ??
                            new ZaJi.NfiEstaPg.Core.PhaseEstimateTypeList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of sub-populations (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.Core.SubPopulationList CSubPopulation
                {
                    get
                    {
                        return cSubPopulation ??
                            new ZaJi.NfiEstaPg.Core.SubPopulationList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        cSubPopulation = value ??
                            new ZaJi.NfiEstaPg.Core.SubPopulationList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of sub-population categories (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.Core.SubPopulationCategoryList CSubPopulationCategory
                {
                    get
                    {
                        return cSubPopulationCategory ??
                            new ZaJi.NfiEstaPg.Core.SubPopulationCategoryList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        cSubPopulationCategory = value ??
                            new ZaJi.NfiEstaPg.Core.SubPopulationCategoryList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of target variables (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.Core.TargetVariableList CTargetVariable
                {
                    get
                    {
                        return cTargetVariable ??
                            new ZaJi.NfiEstaPg.Core.TargetVariableList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        cTargetVariable = value ??
                            new ZaJi.NfiEstaPg.Core.TargetVariableList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of topics (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.Core.TopicList CTopic
                {
                    get
                    {
                        return cTopic ??
                            new ZaJi.NfiEstaPg.Core.TopicList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        cTopic = value ??
                            new ZaJi.NfiEstaPg.Core.TopicList(database: (NfiEstaDB)Database);
                    }
                }

                #endregion Lookup Tables


                #region Mapping Tables

                /// <summary>
                /// List of mappings area domain categories (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.Core.AreaDomainCategoryMappingList CmAreaDomainCategoryMapping
                {
                    get
                    {
                        return cmAreaDomainCategoryMapping ??
                            new ZaJi.NfiEstaPg.Core.AreaDomainCategoryMappingList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        cmAreaDomainCategoryMapping = value ??
                            new ZaJi.NfiEstaPg.Core.AreaDomainCategoryMappingList(database: (NfiEstaDB)Database);
                    }
                }


                /// <summary>
                /// List of mappings between estimation cell and parametrization area (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.Core.EstimationCellToParamAreaMappingList CmEstimationCellToParamAreaMapping
                {
                    get
                    {
                        return cmEstimationCellToParamAreaMapping ??
                            new ZaJi.NfiEstaPg.Core.EstimationCellToParamAreaMappingList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        cmEstimationCellToParamAreaMapping = value ??
                            new ZaJi.NfiEstaPg.Core.EstimationCellToParamAreaMappingList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of mappings between plot and estimation cell (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.Core.PlotToEstimationCellMappingList CmPlotToEstimationCellMapping
                {
                    get
                    {
                        return cmPlotToEstimationCellMapping ??
                            new ZaJi.NfiEstaPg.Core.PlotToEstimationCellMappingList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        cmPlotToEstimationCellMapping = value ??
                            new ZaJi.NfiEstaPg.Core.PlotToEstimationCellMappingList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of mappings between plot and parametrization area (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.Core.PlotToParametrizationAreaMappingList CmPlotToParametrizationAreaMapping
                {
                    get
                    {
                        return cmPlotToParametrizationAreaMapping ??
                            new ZaJi.NfiEstaPg.Core.PlotToParametrizationAreaMappingList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        cmPlotToParametrizationAreaMapping = value ??
                            new ZaJi.NfiEstaPg.Core.PlotToParametrizationAreaMappingList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of mappings subpopulation categories (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.Core.SubPopulationCategoryMappingList CmSubPopulationCategoryMapping
                {
                    get
                    {
                        return cmSubPopulationCategoryMapping ??
                            new ZaJi.NfiEstaPg.Core.SubPopulationCategoryMappingList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        cmSubPopulationCategoryMapping = value ??
                            new ZaJi.NfiEstaPg.Core.SubPopulationCategoryMappingList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of mappings between target variable and topic (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.Core.TargetVariableToTopicList CmTargetVariableToTopic
                {
                    get
                    {
                        return cmTargetVariableToTopic ??
                            new ZaJi.NfiEstaPg.Core.TargetVariableToTopicList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        cmTargetVariableToTopic = value ??
                            new ZaJi.NfiEstaPg.Core.TargetVariableToTopicList(database: (NfiEstaDB)Database);
                    }
                }

                #endregion Mapping Tables


                #region Spatial Data Tables

                /// <summary>
                /// List of estimation cell segments (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.Core.CellList FaCell
                {
                    get
                    {
                        return faCell ??
                            new ZaJi.NfiEstaPg.Core.CellList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        faCell = value ??
                            new ZaJi.NfiEstaPg.Core.CellList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of parametrization areas (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.Core.ParamAreaList FaParamArea
                {
                    get
                    {
                        return faParamArea ??
                            new ZaJi.NfiEstaPg.Core.ParamAreaList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        faParamArea = value ??
                            new ZaJi.NfiEstaPg.Core.ParamAreaList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of intersections between stratum and estimation cell (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.Core.StratumInEstimationCellList TStratumInEstimationCell
                {
                    get
                    {
                        return tStratumInEstimationCell ??
                            new ZaJi.NfiEstaPg.Core.StratumInEstimationCellList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        tStratumInEstimationCell = value ??
                            new ZaJi.NfiEstaPg.Core.StratumInEstimationCellList(database: (NfiEstaDB)Database);
                    }
                }

                #endregion Spatial Data Tables


                #region Data Tables

                /// <summary>
                /// List of additivity checks (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.Core.AdditivitySetPlotList TAdditivitySetPlot
                {
                    get
                    {
                        return tAdditivitySetPlot ??
                            new ZaJi.NfiEstaPg.Core.AdditivitySetPlotList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        tAdditivitySetPlot = value ??
                            new ZaJi.NfiEstaPg.Core.AdditivitySetPlotList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of parametrization configurations (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.Core.AuxConfList TAuxConf
                {
                    get
                    {
                        return tAuxConf ??
                            new ZaJi.NfiEstaPg.Core.AuxConfList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        tAuxConf = value ??
                            new ZaJi.NfiEstaPg.Core.AuxConfList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of totals of auxiliary variable within estimation cell (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.Core.AuxTotalList TAuxTotal
                {
                    get
                    {
                        return tAuxTotal ??
                            new ZaJi.NfiEstaPg.Core.AuxTotalList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        tAuxTotal = value ??
                            new ZaJi.NfiEstaPg.Core.AuxTotalList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of auxiliary plot data (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.Core.AuxDataList TAuxiliaryData
                {
                    get
                    {
                        return tAuxiliaryData ??
                            new ZaJi.NfiEstaPg.Core.AuxDataList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        tAuxiliaryData = value ??
                            new ZaJi.NfiEstaPg.Core.AuxDataList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of available datasets (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.Core.AvailableDataSetList TAvailableDataSet
                {
                    get
                    {
                        return tAvailableDataSet ??
                            new ZaJi.NfiEstaPg.Core.AvailableDataSetList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        tAvailableDataSet = value ??
                            new ZaJi.NfiEstaPg.Core.AvailableDataSetList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of estimate configurations (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.Core.EstimateConfList TEstimateConf
                {
                    get
                    {
                        return tEstimateConf ??
                            new ZaJi.NfiEstaPg.Core.EstimateConfList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        tEstimateConf = value ??
                            new ZaJi.NfiEstaPg.Core.EstimateConfList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of estimation cell mappings (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.Core.EstimationCellHierarchyList TEstimationCellHierarchy
                {
                    get
                    {
                        return tEstimationCellHierarchy ??
                            new ZaJi.NfiEstaPg.Core.EstimationCellHierarchyList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        tEstimationCellHierarchy = value ??
                            new ZaJi.NfiEstaPg.Core.EstimationCellHierarchyList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of precalculated G_Betas for given auxiliary configuration (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.Core.GBetaList TgBeta
                {
                    get
                    {
                        return tgBeta ??
                            new ZaJi.NfiEstaPg.Core.GBetaList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        tgBeta = value ??
                            new ZaJi.NfiEstaPg.Core.GBetaList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of models (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.Core.ModelList TModel
                {
                    get
                    {
                        return tModel ??
                            new ZaJi.NfiEstaPg.Core.ModelList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        tModel = value ??
                            new ZaJi.NfiEstaPg.Core.ModelList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of explanatory variables (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.Core.ModelVariableList TModelVariable
                {
                    get
                    {
                        return tModelVariable ??
                            new ZaJi.NfiEstaPg.Core.ModelVariableList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        tModelVariable = value ??
                            new ZaJi.NfiEstaPg.Core.ModelVariableList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of mappings between panels and total estimate configuration (zero and the first phase)
                /// (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.Core.PanelToTotalEstimateConfList TPanelToTotalEstimateConfFirstPhase
                {
                    get
                    {
                        return tPanelToTotalEstimateConfFirstPhase ??
                            new ZaJi.NfiEstaPg.Core.PanelToTotalEstimateConfList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        tPanelToTotalEstimateConfFirstPhase = value ??
                            new ZaJi.NfiEstaPg.Core.PanelToTotalEstimateConfList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of pairs of panel with corresponding reference year set (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.Core.PanelRefYearSetPairList TPanelRefYearSetGroup
                {
                    get
                    {
                        return tPanelRefYearSetGroup ??
                            new ZaJi.NfiEstaPg.Core.PanelRefYearSetPairList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        tPanelRefYearSetGroup = value ??
                            new ZaJi.NfiEstaPg.Core.PanelRefYearSetPairList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of final estimates (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.Core.ResultList TResult
                {
                    get
                    {
                        return tResult ??
                            new ZaJi.NfiEstaPg.Core.ResultList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        tResult = value ??
                            new ZaJi.NfiEstaPg.Core.ResultList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of measured plot data (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.Core.TargetDataList TTargetData
                {
                    get
                    {
                        return tTargetData ??
                            new ZaJi.NfiEstaPg.Core.TargetDataList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        tTargetData = value ??
                            new ZaJi.NfiEstaPg.Core.TargetDataList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of total estimate configurations (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.Core.TotalEstimateConfList TTotalEstimateConf
                {
                    get
                    {
                        return tTotalEstimateConf ??
                            new ZaJi.NfiEstaPg.Core.TotalEstimateConfList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        tTotalEstimateConf = value ??
                            new ZaJi.NfiEstaPg.Core.TotalEstimateConfList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of combinations of target variable and sub-population category and area domain category
                /// (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.Core.VariableList TVariable
                {
                    get
                    {
                        return tVariable ??
                            new ZaJi.NfiEstaPg.Core.VariableList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        tVariable = value ??
                            new ZaJi.NfiEstaPg.Core.VariableList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of variable mappings (not-null) (read-only)
                /// </summary>
                public ZaJi.NfiEstaPg.Core.VariableHierarchyList TVariableHierarchy
                {
                    get
                    {
                        return tVariableHierarchy ??
                            new ZaJi.NfiEstaPg.Core.VariableHierarchyList(database: (NfiEstaDB)Database);
                    }
                    private set
                    {
                        tVariableHierarchy = value ??
                            new ZaJi.NfiEstaPg.Core.VariableHierarchyList(database: (NfiEstaDB)Database);
                    }
                }

                #endregion Data Tables


                #region Data Views

                /// <summary>
                /// List of target variables (not-null)
                /// </summary>
                public ZaJi.NfiEstaPg.Core.TargetVariableMetadataList CTargetVariableMetadata
                {
                    get
                    {
                        return cTargetVariableMetadata ??
                            new ZaJi.NfiEstaPg.Core.TargetVariableMetadataList(database: (NfiEstaDB)Database);
                    }
                    set
                    {
                        cTargetVariableMetadata = value ??
                            new ZaJi.NfiEstaPg.Core.TargetVariableMetadataList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of  aggregated target variable metadata (not-null)
                /// </summary>
                public ZaJi.NfiEstaPg.Core.TargetVariableMetadataList CAggregateTargetVariableMetadata
                {
                    get
                    {
                        return cAggregateTargetVariableMetadata ??
                            new ZaJi.NfiEstaPg.Core.TargetVariableMetadataList(database: (NfiEstaDB)Database);
                    }
                    set
                    {
                        cAggregateTargetVariableMetadata = value ??
                            new ZaJi.NfiEstaPg.Core.TargetVariableMetadataList(database: (NfiEstaDB)Database);
                    }
                }


                /// <summary>
                /// List of pairs of panel with corresponding reference year set (extended version)
                /// (not-null)
                /// </summary>
                public ZaJi.NfiEstaPg.Core.VwPanelRefYearSetPairList VPanelRefYearSetPairs
                {
                    get
                    {
                        return vPanelRefYearSetPairs ??
                            new ZaJi.NfiEstaPg.Core.VwPanelRefYearSetPairList(database: (NfiEstaDB)Database);
                    }
                    set
                    {
                        vPanelRefYearSetPairs = value ??
                            new ZaJi.NfiEstaPg.Core.VwPanelRefYearSetPairList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of combinations of target variable and sub-population category and area domain category (extended version)
                /// (not-null)
                /// </summary>
                public ZaJi.NfiEstaPg.Core.VwVariableList VVariable
                {
                    get
                    {
                        return vVariable ??
                            new ZaJi.NfiEstaPg.Core.VwVariableList(database: (NfiEstaDB)Database);
                    }
                    set
                    {
                        vVariable = value ??
                            new ZaJi.NfiEstaPg.Core.VwVariableList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// Count of estimation cells in estimation cell collection
                /// </summary>
                public ZaJi.NfiEstaPg.Core.TFnGetEstimationCellsNumberInCollectionList VEstimationCellsCount
                {
                    get
                    {
                        return vEstimationCellsCount ??
                            new ZaJi.NfiEstaPg.Core.TFnGetEstimationCellsNumberInCollectionList(database: (NfiEstaDB)Database);
                    }
                    set
                    {
                        vEstimationCellsCount = value ??
                            new ZaJi.NfiEstaPg.Core.TFnGetEstimationCellsNumberInCollectionList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of total estimates attribute additivity for ratios
                /// </summary>
                public ZaJi.NfiEstaPg.Core.TFnAddResRatioAttrList VRatioAdditivityAttr
                {
                    get
                    {
                        return vRatioAdditivityAttr;
                    }
                    set
                    {
                        vRatioAdditivityAttr = value;
                    }
                }

                /// <summary>
                /// List of total estimates attribute additivity for totals
                /// </summary>
                public ZaJi.NfiEstaPg.Core.TFnAddResTotalAttrList VTotalAdditivityAttr
                {
                    get
                    {
                        return vTotalAdditivityAttr;
                    }
                    set
                    {
                        vTotalAdditivityAttr = value;
                    }
                }

                /// <summary>
                /// List of total estimates geographic additivity for totals
                /// </summary>
                public ZaJi.NfiEstaPg.Core.TFnAddResTotalGeoList VTotalAdditivityGeo
                {
                    get
                    {
                        return vTotalAdditivityGeo;
                    }
                    set
                    {
                        vTotalAdditivityGeo = value;
                    }
                }


                /// <summary>
                /// OLAP Cube - List of Total Estimates (not-null)
                /// </summary>
                public ZaJi.NfiEstaPg.Core.OLAPTotalEstimateList VTotalOLAP
                {
                    get
                    {
                        return vTotalOLAP ??
                            new ZaJi.NfiEstaPg.Core.OLAPTotalEstimateList(database: (NfiEstaDB)Database);
                    }
                    set
                    {
                        vTotalOLAP = value ??
                            new ZaJi.NfiEstaPg.Core.OLAPTotalEstimateList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// OLAP Cube - List of Ratio Estimates (not-null)
                /// </summary>
                public ZaJi.NfiEstaPg.Core.OLAPRatioEstimateList VRatioOLAP
                {
                    get
                    {
                        return vRatioOLAP ??
                            new ZaJi.NfiEstaPg.Core.OLAPRatioEstimateList(database: (NfiEstaDB)Database);
                    }
                    set
                    {
                        vRatioOLAP = value ??
                            new ZaJi.NfiEstaPg.Core.OLAPRatioEstimateList(database: (NfiEstaDB)Database);
                    }
                }

                #endregion Data Views


                #region Differences

                /// <summary>
                /// Differences in database schema (not-null) (read-only)
                /// </summary>
                public List<string> Differences
                {
                    get
                    {
                        List<string> result = [];
                        Database.Postgres.Catalog.Load();

                        DBExtension dbExtension = Database.Postgres.Catalog.Extensions.Items
                            .Where(a => a.Name == NfiEstaSchema.ExtensionName)
                            .FirstOrDefault();

                        if (dbExtension == null)
                        {
                            result.Add(item: String.Concat(
                                $"Extension {NfiEstaSchema.ExtensionName} does not exist in database."));
                            return result;
                        }
                        else
                        {
                            ExtensionVersion dbExtensionVersion = new(version: dbExtension.Version);
                            if (!dbExtensionVersion.Equals(obj: NfiEstaSchema.ExtensionVersion))
                            {
                                result.Add(item: String.Concat(
                                    $"Database extension {NfiEstaSchema.ExtensionName} is in version {dbExtension.Version}. ",
                                    $"But the version {NfiEstaSchema.ExtensionVersion} was expected."));
                            }

                            DBSchema dbSchema = Database.Postgres.Catalog.Schemas.Items
                                .Where(a => a.Name == NfiEstaSchema.Name)
                                .FirstOrDefault();

                            if (dbSchema == null)
                            {
                                result.Add(item: String.Concat(
                                    $"Schema {NfiEstaSchema.Name} does not exist in database."));
                                return result;
                            }
                            else
                            {
                                foreach (DBTable dbTable in dbSchema.Tables.Items.OrderBy(a => a.Name))
                                {
                                    if (OmittedTables.Contains(item: dbTable.Name))
                                    {
                                        continue;
                                    }

                                    IDatabaseTable iTable = ((NfiEstaDB)Database).SNfiEsta.Tables
                                        .Where(a => a.TableName == dbTable.Name)
                                        .FirstOrDefault();

                                    if (iTable == null)
                                    {
                                        result.Add(item: String.Concat(
                                            $"Class for table {dbTable.Name} does not exist in dll library."));
                                    }
                                }

                                foreach (IDatabaseTable iTable in ((NfiEstaDB)Database).SNfiEsta.Tables.OrderBy(a => a.TableName))
                                {
                                    DBTable dbTable = dbSchema.Tables.Items
                                            .Where(a => a.Name == iTable.TableName)
                                            .FirstOrDefault();

                                    if (dbTable == null)
                                    {
                                        if (iTable.Columns.Values.Where(a => a.Elemental).Any())
                                        {
                                            result.Add(item: String.Concat(
                                                $"Table {iTable.TableName} does not exist in database."));
                                        }
                                        else
                                        {
                                            // Table doesn't contain any column that is loaded from database.
                                        }
                                    }
                                    else
                                    {
                                        foreach (DBColumn dbColumn in dbTable.Columns.Items.OrderBy(a => a.Name))
                                        {
                                            ColumnMetadata iColumn = iTable.Columns.Values
                                                .Where(a => a.Elemental)
                                                .Where(a => a.DbName == dbColumn.Name)
                                                .FirstOrDefault();

                                            if (iColumn == null)
                                            {
                                                result.Add(item: String.Concat(
                                                    $"Column {dbTable.Name}.{dbColumn.Name} does not exist in dll library."));
                                            }
                                        }

                                        foreach (ColumnMetadata iColumn in iTable.Columns.Values.Where(a => a.Elemental).OrderBy(a => a.DbName))
                                        {
                                            DBColumn dbColumn = dbTable.Columns.Items
                                                .Where(a => a.Name == iColumn.DbName)
                                                .FirstOrDefault();

                                            if (dbColumn == null)
                                            {
                                                result.Add(item: String.Concat(
                                                    $"Column {iTable.TableName}.{iColumn.DbName} does not exist in database."));
                                            }
                                            else
                                            {
                                                if (dbColumn.TypeName != iColumn.DbDataType)
                                                {
                                                    result.Add(item: String.Concat(
                                                        $"Column {iTable.TableName}.{iColumn.DbName} is type of {dbColumn.TypeName}. ",
                                                        $"But type of {iColumn.DbDataType} was expected."));
                                                }

                                                if (dbColumn.NotNull != iColumn.NotNull)
                                                {
                                                    string strDbNull = (bool)dbColumn.NotNull ? "has NOT NULL constraint" : "is NULLABLE";
                                                    string strINull = iColumn.NotNull ? "NOT NULL contraint" : "NULLABLE";
                                                    result.Add(String.Concat(
                                                        $"Column {iTable.TableName}.{iColumn.DbName} {strDbNull}. ",
                                                        $"But {strINull} was expected."));
                                                }
                                            }
                                        }
                                    }
                                }

                                Dictionary<string, string> iStoredProcedures = [];
                                foreach (
                                    Type nestedType in typeof(ZaJi.NfiEstaPg.Core.NfiEstaFunctions)
                                    .GetNestedTypes()
                                    .AsEnumerable()
                                    .OrderBy(a => a.FullName)
                                    )
                                {
                                    FieldInfo fieldSignature = nestedType.GetField(name: "Signature");
                                    FieldInfo fieldName = nestedType.GetField(name: "Name");
                                    if ((fieldSignature != null) && (fieldName != null))
                                    {
                                        string a = fieldSignature.GetValue(obj: null).ToString();
                                        string b = fieldName.GetValue(obj: null).ToString();

                                        iStoredProcedures.TryAdd(key: a, value: b);
                                    }

                                    PropertyInfo pSignature = nestedType.GetProperty(name: "Signature");
                                    PropertyInfo pName = nestedType.GetProperty(name: "Name");
                                    if ((pSignature != null) && (pName != null))
                                    {
                                        // signature = null označeje uložené procedury,
                                        // pro které není požadována implementace v databázi
                                        // nekontrolují se
                                        if (pSignature.GetValue(obj: null) != null)
                                        {
                                            string a = pSignature.GetValue(obj: null).ToString();
                                            string b = pName.GetValue(obj: null).ToString();

                                            iStoredProcedures.TryAdd(key: a, value: b);
                                        }
                                    }
                                }

                                foreach (DBStoredProcedure dbStoredProcedure in dbSchema.StoredProcedures.Items.OrderBy(a => a.Name))
                                {
                                    if (!iStoredProcedures.ContainsKey(key: dbStoredProcedure.Signature))
                                    {
                                        result.Add(item: String.Concat(
                                           $"Class for stored procedure {dbStoredProcedure.Name} does not exist in dll library."));
                                    }
                                }

                                foreach (KeyValuePair<string, string> iStoredProcedure in iStoredProcedures)
                                {
                                    DBStoredProcedure dbStoredProcedure = dbSchema.StoredProcedures.Items
                                            .Where(a => a.Signature == iStoredProcedure.Key)
                                            .FirstOrDefault();
                                    if (dbStoredProcedure == null)
                                    {
                                        result.Add(item: String.Concat(
                                               $"Stored procedure {iStoredProcedure.Value} does not exist in database."));
                                    }
                                }

                                return result;
                            }
                        }
                    }
                }

                #endregion Differences

                #endregion Properties


                #region Methods

                /// <summary>
                /// Returns the string that represents current object
                /// </summary>
                /// <returns>Returns the string that represents current object</returns>
                public override string ToString()
                {
                    return Database.Postgres.Setting.LanguageVersion switch
                    {
                        LanguageVersion.National => String.Concat($"Data schématu {NfiEstaSchema.Name}."),
                        LanguageVersion.International => String.Concat($"Data from schema {NfiEstaSchema.Name}."),
                        _ => String.Concat($"Data from schema {NfiEstaSchema.Name}."),
                    };
                }

                #endregion Methods


                #region Static Methods

                /// <summary>
                /// Exports data of the schema
                /// </summary>
                /// <param name="database">Database tables</param>
                /// <param name="fileFormat">Format of the file with exported data</param>
                /// <param name="tableNames">List of table names to export (if null than all tables)</param>
                [SupportedOSPlatform("windows")]
                public static void ExportData(
                    NfiEstaDB database,
                    ExportFileFormat fileFormat,
                    List<string> tableNames = null)
                {
                    System.Windows.Forms.FolderBrowserDialog dlg = new();

                    if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        ExportData(
                            database: database,
                            fileFormat: fileFormat,
                            folder: dlg.SelectedPath,
                            tableNames: tableNames);
                    }
                }

                /// <summary>
                /// Exports data of the schema
                /// </summary>
                /// <param name="database">Database tables</param>
                /// <param name="fileFormat">Format of the file with exported data</param>
                /// <param name="folder">Folder for writing file with data</param>
                /// <param name="tableNames">List of table names to export (if null than all tables)</param>
                public static void ExportData(
                    NfiEstaDB database,
                    ExportFileFormat fileFormat,
                    string folder,
                    List<string> tableNames = null)
                {
                    List<ADatabaseTable> databaseTables =
                    [
                        // c_area_domain
                        new ZaJi.NfiEstaPg.Core.AreaDomainList(database: database),

                        // c_area_domain_category
                        new ZaJi.NfiEstaPg.Core.AreaDomainCategoryList(database: database),

                        // c_auxiliary_variable
                        new ZaJi.NfiEstaPg.Core.AuxiliaryVariableList(database: database),

                        // c_auxiliary_variable_category
                        new ZaJi.NfiEstaPg.Core.AuxiliaryVariableCategoryList(database: database),

                        // * c_estimate_conf_status
                        new ZaJi.NfiEstaPg.Core.EstimateConfStatusList(database: database),

                        // c_estimate_type
                        new ZaJi.NfiEstaPg.Core.EstimateTypeList(database: database),

                        // c_estimation_cell
                        new ZaJi.NfiEstaPg.Core.EstimationCellList(database: database),

                        // c_estimation_cell_collection
                        new ZaJi.NfiEstaPg.Core.EstimationCellCollectionList(database: database),

                        // c_estimation_period
                        new ZaJi.NfiEstaPg.Core.EstimationPeriodList(database: database),

                        // c_panel_refyearset_group
                        new ZaJi.NfiEstaPg.Core.PanelRefYearSetGroupList(database: database),

                        // c_param_area_type
                        new ZaJi.NfiEstaPg.Core.ParamAreaTypeList(database: database),

                        // c_phase_estimate_type
                        new ZaJi.NfiEstaPg.Core.PhaseEstimateTypeList(database: database),

                        // c_sub_population
                        new ZaJi.NfiEstaPg.Core.SubPopulationList(database: database),

                        // c_sub_population_category
                        new ZaJi.NfiEstaPg.Core.SubPopulationCategoryList(database: database),

                        // c_target_variable
                        new ZaJi.NfiEstaPg.Core.TargetVariableList(database: database),

                        // c_topic
                        new ZaJi.NfiEstaPg.Core.TopicList(database: database),


                        // cm_area_domain_category
                        new ZaJi.NfiEstaPg.Core.AreaDomainCategoryMappingList(database: database),

                        // cm_cell2param_area_mapping
                        new ZaJi.NfiEstaPg.Core.EstimationCellToParamAreaMappingList(database: database),

                        // cm_plot2cell_mapping
                        new ZaJi.NfiEstaPg.Core.PlotToEstimationCellMappingList(database: database),

                        // cm_plot2param_area_mapping
                        new ZaJi.NfiEstaPg.Core.PlotToParametrizationAreaMappingList(database: database),

                        // cm_sub_population_category
                        new ZaJi.NfiEstaPg.Core.SubPopulationCategoryMappingList(database: database),

                        // cm_tvariable2topic
                        new ZaJi.NfiEstaPg.Core.TargetVariableToTopicList(database: database),


                        // f_a_cell
                        new ZaJi.NfiEstaPg.Core.CellList(database: database),

                        // f_a_param_area
                        new ZaJi.NfiEstaPg.Core.ParamAreaList(database: database),


                        // t_additivity_set_plot
                        new ZaJi.NfiEstaPg.Core.AdditivitySetPlotList(database: database),

                        // t_aux_conf
                        new ZaJi.NfiEstaPg.Core.AuxConfList(database: database),

                        // t_aux_total
                        new ZaJi.NfiEstaPg.Core.AuxTotalList(database: database),

                        // t_auxiliary_data
                        new ZaJi.NfiEstaPg.Core.AuxDataList(database: database),

                        // t_available_datasets
                        new ZaJi.NfiEstaPg.Core.AvailableDataSetList(database: database),

                        // t_estimate_conf
                        new ZaJi.NfiEstaPg.Core.EstimateConfList(database: database),

                        // t_estimation_cell_hierarchy
                        new ZaJi.NfiEstaPg.Core.EstimationCellHierarchyList(database: database),

                        // t_g_beta
                        new ZaJi.NfiEstaPg.Core.GBetaList(database: database),

                        // t_model
                        new ZaJi.NfiEstaPg.Core.ModelList(database: database),

                        // t_model_variables
                        new ZaJi.NfiEstaPg.Core.ModelVariableList(database: database),

                        // t_panel2total_1stph_estimate_conf
                        new ZaJi.NfiEstaPg.Core.PanelToTotalEstimateConfList(database: database),

                        // t_panel_refyearset_group
                        new ZaJi.NfiEstaPg.Core.PanelRefYearSetPairList(database: database),

                        // t_result
                        new ZaJi.NfiEstaPg.Core.ResultList(database: database),

                        // t_stratum_in_estimation_cell
                        new ZaJi.NfiEstaPg.Core.StratumInEstimationCellList(database: database),

                        // t_target_data
                        new ZaJi.NfiEstaPg.Core.TargetDataList(database: database),

                        // t_total_estimate_conf
                        new ZaJi.NfiEstaPg.Core.TotalEstimateConfList(database: database),

                        // t_variable
                        new ZaJi.NfiEstaPg.Core.VariableList(database: database),

                        // t_variable_hierarchy
                        new ZaJi.NfiEstaPg.Core.VariableHierarchyList(database: database)
                    ];

                    foreach (ADatabaseTable databaseTable in databaseTables)
                    {
                        if ((tableNames == null) ||
                             tableNames.Contains(item: databaseTable.TableName))
                        {
                            if (databaseTable is ALookupTable lookupTable)
                            {
                                lookupTable.ReLoad(extended: false);
                            }
                            else if (databaseTable is AMappingTable mappingTable)
                            {
                                mappingTable.ReLoad();
                            }
                            else if (databaseTable is ASpatialTable spatialTable)
                            {
                                spatialTable.ReLoad(withGeom: false);
                            }
                            else if (databaseTable is ADataTable dataTable)
                            {
                                dataTable.ReLoad();
                            }
                            else
                            {
                                throw new Exception(message: $"Argument {nameof(databaseTable)} unknown type.");
                            }

                            databaseTable.ExportData(
                                fileFormat: fileFormat,
                                folder: folder);
                        }
                    }
                }

                #endregion Static Methods

            }

        }
    }
}