﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {
            // c_estimate_conf_status

            /// <summary>
            /// Estimate configuration status
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class EstimateConfStatus(
                EstimateConfStatusList composite = null,
                DataRow data = null)
                    : ALookupTableEntry<EstimateConfStatusList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Estimate configuration status identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: EstimateConfStatusList.ColId.Name,
                            defaultValue: Int32.Parse(s: EstimateConfStatusList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: EstimateConfStatusList.ColId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Label of estimate configuration status in national language
                /// </summary>
                public override string LabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: EstimateConfStatusList.ColLabelCs.Name,
                            defaultValue: EstimateConfStatusList.ColLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: EstimateConfStatusList.ColLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Description of estimate configuration status in national language
                /// </summary>
                public override string DescriptionCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: EstimateConfStatusList.ColDescriptionCs.Name,
                            defaultValue: EstimateConfStatusList.ColDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: EstimateConfStatusList.ColDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Extended label of estimate configuration status in national language (read-only)
                /// </summary>
                public override string ExtendedLabelCs
                {
                    get
                    {
                        return $"{Id} - {LabelCs}";
                    }
                }


                /// <summary>
                /// Label of estimate configuration status in English
                /// </summary>
                public override string LabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: EstimateConfStatusList.ColLabelEn.Name,
                            defaultValue: EstimateConfStatusList.ColLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: EstimateConfStatusList.ColLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Description of estimate configuration status in English
                /// </summary>
                public override string DescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: EstimateConfStatusList.ColDescriptionEn.Name,
                            defaultValue: EstimateConfStatusList.ColDescriptionEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: EstimateConfStatusList.ColDescriptionEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Extended label of estimate configuration status in English (read-only)
                /// </summary>
                public override string ExtendedLabelEn
                {
                    get
                    {
                        return $"{Id} - {LabelEn}";
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true|false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not EstimateConfStatus Type, return False
                    if (obj is not EstimateConfStatus)
                    {
                        return false;
                    }

                    return ((EstimateConfStatus)obj).Id == Id;
                }

                /// <summary>
                /// Returns a hash code for the current object
                /// </summary>
                /// <returns>Hash code for the current object</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                /// <summary>
                /// Returns a string that represents the current object
                /// </summary>
                /// <returns>String that represents the current object</returns>
                public override string ToString()
                {
                    return String.Concat(
                        $"{nameof(EstimateConfStatus)}: {{",
                        $"{nameof(Id)}: {Functions.PrepIntArg(arg: Id)}; ",
                        $"{nameof(LabelCs)}: {Functions.PrepStringArg(arg: LabelCs)}; ",
                        $"{nameof(LabelEn)}: {Functions.PrepStringArg(arg: LabelEn)}; ",
                        $"{nameof(DescriptionCs)}: {Functions.PrepStringArg(arg: DescriptionCs)}; ",
                        $"{nameof(DescriptionEn)}: {Functions.PrepStringArg(arg: DescriptionEn)}}}");
                }

                #endregion Methods

            }


            /// <summary>
            /// List of estimate configuration status
            /// </summary>
            public class EstimateConfStatusList
                : ALookupTable
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema = NfiEstaSchema.Name;

                /// <summary>
                /// Table name
                /// </summary>
                public const string Name = "c_estimate_conf_status";

                /// <summary>
                /// Table alias
                /// </summary>
                public const string Alias = "c_estimate_conf_status";

                /// <summary>
                /// Description in national language
                /// </summary>
                public const string CaptionCs = "Seznam stavů konfigurací odhadů";

                /// <summary>
                /// Description in English
                /// </summary>
                public const string CaptionEn = "List of estimate configuration status";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
                {
                    { "id", new ColumnMetadata()
                    {
                        Name = "id",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType =  null,
                        NewDataType = "int4",
                        PKey = true,
                        NotNull = true,
                        DefaultValue = default(int).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ID",
                        HeaderTextEn = "ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 0
                    }
                    },
                    { "label_cs", new ColumnMetadata()
                    {
                        Name = "label_cs",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType =  null,
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "LABEL_CS",
                        HeaderTextEn = "LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 1
                    }
                    },
                    { "description_cs", new ColumnMetadata()
                    {
                        Name = "description_cs",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = "text",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "DESCRIPTION_CS",
                        HeaderTextEn = "DESCRIPTION_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 2
                    }
                    },
                    { "label_en", new ColumnMetadata()
                    {
                        Name = "label_en",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "LABEL_EN",
                        HeaderTextEn = "LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 3
                    }
                    },
                    { "description_en", new ColumnMetadata()
                    {
                        Name = "description_en",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = "text",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "DESCRIPTION_EN",
                        HeaderTextEn = "DESCRIPTION_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 4
                    }
                    }
                };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColLabelCs = Cols["label_cs"];

                /// <summary>
                /// Column description_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColDescriptionCs = Cols["description_cs"];

                /// <summary>
                /// Column label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColLabelEn = Cols["label_en"];

                /// <summary>
                /// Column description_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColDescriptionEn = Cols["description_en"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="extended">Altogether category is included - true|false</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public EstimateConfStatusList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    bool extended = false,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            extended: extended,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="extended">Altogether category is included - true|false</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public EstimateConfStatusList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    bool extended = false,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            extended: extended,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// List of estimate configuration status (read-only)
                /// </summary>
                public List<EstimateConfStatus> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new EstimateConfStatus(composite: this, data: a))];
                    }
                }


                /// <summary>
                /// Estimate configuration status: NoConfiguration (ID = 100) (read-only)
                /// </summary>
                public EstimateConfStatus NoConfiguration
                {
                    get
                    {
                        return this[(int)EstimateConfStatusEnum.NoConfiguration];
                    }
                }

                /// <summary>
                /// Estimate configuration status: Configured (ID = 200) (read-only)
                /// </summary>
                public EstimateConfStatus Configured
                {
                    get
                    {
                        return this[(int)EstimateConfStatusEnum.Configured];
                    }
                }

                /// <summary>
                /// Estimate configuration status: Calculated (ID = 300) (read-only)
                /// </summary>
                public EstimateConfStatus Calculated
                {
                    get
                    {
                        return this[(int)EstimateConfStatusEnum.Calculated];
                    }
                }

                #endregion Properties


                #region Indexers

                /// <summary>
                /// Estimate configuration status from list by identifier (read-only)
                /// </summary>
                /// <param name="id">Estimate configuration status identifier</param>
                /// <returns>Estimate configuration status from list by identifier (null if not found)</returns>
                public EstimateConfStatus this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new EstimateConfStatus(composite: this, data: a))
                                .FirstOrDefault<EstimateConfStatus>();
                    }
                }

                #endregion Indexers


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public EstimateConfStatusList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new EstimateConfStatusList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            extended: Extended,
                            loadingTime: LoadingTime) :
                        new EstimateConfStatusList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            extended: Extended,
                            loadingTime: null);
                }

                /// <summary>
                /// Loads data from database table
                /// (if it was not done before)
                /// </summary>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="extended">Altogether category is included - true|false</param>
                public new void Load(
                    string condition = null,
                    Nullable<int> limit = null,
                    bool extended = false)
                {
                    if (!Loaded)
                    {
                        ReLoad(
                            condition: condition,
                            limit: limit,
                            extended: extended);
                    }
                }

                /// <summary>
                /// Loads data from database table
                /// </summary>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="extended">Altogether category is included - true|false</param>
                public new void ReLoad(
                    string condition = null,
                    Nullable<int> limit = null,
                    bool extended = false)
                {
                    Condition = condition;
                    Limit = limit;
                    Extended = extended;

                    System.Diagnostics.Stopwatch stopWatch = new();

                    stopWatch.Start();

                    DataTable newData = EmptyDataTable();

                    DataRow row = newData.NewRow();
                    Functions.SetIntArg(row: row, name: ColId.Name, val: (int)EstimateConfStatusEnum.NoConfiguration);
                    Functions.SetStringArg(row: row, name: ColLabelCs.Name, val: "není provedena konfigurace odhadu");
                    Functions.SetStringArg(row: row, name: ColDescriptionCs.Name, val: "není provedena konfigurace odhadu");
                    Functions.SetStringArg(row: row, name: ColLabelEn.Name, val: "missing estimate configuration");
                    Functions.SetStringArg(row: row, name: ColDescriptionEn.Name, val: "missing estimate configuration");
                    newData.Rows.Add(row: row);

                    row = newData.NewRow();
                    Functions.SetIntArg(row: row, name: ColId.Name, val: (int)EstimateConfStatusEnum.Configured);
                    Functions.SetStringArg(row: row, name: ColLabelCs.Name, val: "není vypočten odhad");
                    Functions.SetStringArg(row: row, name: ColDescriptionCs.Name, val: "není vypočten odhad");
                    Functions.SetStringArg(row: row, name: ColLabelEn.Name, val: "missing estimate calculation");
                    Functions.SetStringArg(row: row, name: ColDescriptionEn.Name, val: "missing estimate calculation");
                    newData.Rows.Add(row: row);

                    row = newData.NewRow();
                    Functions.SetIntArg(row: row, name: ColId.Name, val: (int)EstimateConfStatusEnum.Calculated);
                    Functions.SetStringArg(row: row, name: ColLabelCs.Name, val: "odhad byl vypočten");
                    Functions.SetStringArg(row: row, name: ColDescriptionCs.Name, val: "odhad byl vypočten");
                    Functions.SetStringArg(row: row, name: ColLabelEn.Name, val: "estimate was calculated");
                    Functions.SetStringArg(row: row, name: ColDescriptionEn.Name, val: "estimate was calculated");
                    newData.Rows.Add(row: row);

                    if (Extended)
                    {
                        row = newData.NewRow();
                        Functions.SetIntArg(row: row, name: ColId.Name, val: 0);
                        Functions.SetStringArg(row: row, name: ColLabelCs.Name, val: AltogetherCs);
                        Functions.SetStringArg(row: row, name: ColDescriptionCs.Name, val: AltogetherCs);
                        Functions.SetStringArg(row: row, name: ColLabelEn.Name, val: AltogetherEn);
                        Functions.SetStringArg(row: row, name: ColDescriptionEn.Name, val: AltogetherEn);
                        newData.Rows.Add(row: row);
                    }

                    DataView view = new(table: newData);
                    if (!String.IsNullOrEmpty(value: Condition))
                    {
                        try
                        {
                            view.RowFilter = Condition;
                        }
                        catch
                        {
                            view.RowFilter = String.Empty;
                            Condition = String.Empty;
                        }
                    }
                    else
                    {
                        view.RowFilter = String.Empty;
                    }
                    Data = view.ToTable();

                    stopWatch.Stop();
                    LoadingTime = 0.001 * stopWatch.ElapsedMilliseconds;
                    Data.TableName = TableName;
                }

                #endregion Methods

            }

            /// <summary>
            /// Estimate configuration status
            /// </summary>
            public enum EstimateConfStatusEnum
            {

                /// <summary>
                /// Estimate configuration status is not specified.
                /// </summary>
                Unknown = 0,

                /// <summary>
                /// Estimate without configuration
                /// </summary>
                NoConfiguration = 100,

                /// <summary>
                /// Configured estimate
                /// </summary>
                Configured = 200,

                /// <summary>
                /// Calculated estimate
                /// </summary>
                Calculated = 300

            }

        }
    }
}


