﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {
            // c_sub_population_category

            /// <summary>
            /// Sub-population category
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class SubPopulationCategory(
                SubPopulationCategoryList composite = null,
                DataRow data = null)
                    : ALookupTableEntry<SubPopulationCategoryList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Sub-population category identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: SubPopulationCategoryList.ColId.Name,
                            defaultValue: Int32.Parse(s: SubPopulationCategoryList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: SubPopulationCategoryList.ColId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Sub-population identifier
                /// </summary>
                public int SubPopulationId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: SubPopulationCategoryList.ColSubPopulationId.Name,
                            defaultValue: Int32.Parse(s: SubPopulationCategoryList.ColSubPopulationId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: SubPopulationCategoryList.ColSubPopulationId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Sub-population object (read-only)
                /// </summary>
                public SubPopulation SubPopulation
                {
                    get
                    {
                        return ((NfiEstaDB)Composite.Database).SNfiEsta.CSubPopulation[SubPopulationId];
                    }
                }


                /// <summary>
                /// Label of sub-population category in national language
                /// </summary>
                public override string LabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: SubPopulationCategoryList.ColLabelCs.Name,
                            defaultValue: SubPopulationCategoryList.ColLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: SubPopulationCategoryList.ColLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Description of sub-population category in national language
                /// </summary>
                public override string DescriptionCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: SubPopulationCategoryList.ColDescriptionCs.Name,
                            defaultValue: SubPopulationCategoryList.ColDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: SubPopulationCategoryList.ColDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Extended label of sub-population category in national language (read-only)
                /// </summary>
                public override string ExtendedLabelCs
                {
                    get
                    {
                        return $"{Id} - {LabelCs}";
                    }
                }


                /// <summary>
                /// Label of sub-population category in English
                /// </summary>
                public override string LabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: SubPopulationCategoryList.ColLabelEn.Name,
                            defaultValue: SubPopulationCategoryList.ColLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: SubPopulationCategoryList.ColLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Description of sub-population category in English
                /// </summary>
                public override string DescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: SubPopulationCategoryList.ColDescriptionEn.Name,
                            defaultValue: SubPopulationCategoryList.ColDescriptionEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: SubPopulationCategoryList.ColDescriptionEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Extended label of sub-population category in English (read-only)
                /// </summary>
                public override string ExtendedLabelEn
                {
                    get
                    {
                        return $"{Id} - {LabelEn}";
                    }
                }

                #endregion Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not SubPopulationCategory Type, return False
                    if (obj is not SubPopulationCategory)
                    {
                        return false;
                    }

                    return
                        Id == ((SubPopulationCategory)obj).Id;
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                #endregion Methods

            }


            /// <summary>
            /// List of sub-population categories
            /// </summary>
            public class SubPopulationCategoryList
                : ALookupTable
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema = NfiEstaSchema.Name;

                /// <summary>
                /// Table name
                /// </summary>
                public const string Name = "c_sub_population_category";

                /// <summary>
                /// Table alias
                /// </summary>
                public const string Alias = "c_sub_population_category";

                /// <summary>
                /// Description in national language
                /// </summary>
                public const string CaptionCs = "Seznam kategorií sub-populací";

                /// <summary>
                /// Description in English
                /// </summary>
                public const string CaptionEn = "List of sub-population categories";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
                {
                    { "id", new ColumnMetadata()
                    {
                        Name = "id",
                        DbName = "id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = true,
                        NotNull = true,
                        DefaultValue = default(int).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ID",
                        HeaderTextEn = "ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 0
                    }
                    },
                    { "sub_population", new ColumnMetadata()
                    {
                        Name = "sub_population",
                        DbName = "sub_population",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = default(int).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "SUB_POPULATION",
                        HeaderTextEn = "SUB_POPULATION",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 1
                    }
                    },
                    { "label_cs", new ColumnMetadata()
                    {
                        Name = "label_cs",
                        DbName = "label",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "LABEL_CS",
                        HeaderTextEn = "LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 2
                    }
                    },
                    { "description_cs", new ColumnMetadata()
                    {
                        Name = "description_cs",
                        DbName = "description",
                        DataType = "System.String",
                        DbDataType = "text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "DESCRIPTION_CS",
                        HeaderTextEn = "DESCRIPTION_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 3
                    }
                    },
                    { "label_en", new ColumnMetadata()
                    {
                        Name = "label_en",
                        DbName = "label_en",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "LABEL_EN",
                        HeaderTextEn = "LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 4
                    }
                    },
                    { "description_en", new ColumnMetadata()
                    {
                        Name = "description_en",
                        DbName = "description_en",
                        DataType = "System.String",
                        DbDataType = "text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "DESCRIPTION_EN",
                        HeaderTextEn = "DESCRIPTION_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 5
                    }
                    }
                };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column sub_population metadata
                /// </summary>
                public static readonly ColumnMetadata ColSubPopulationId = Cols["sub_population"];

                /// <summary>
                /// Column label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColLabelCs = Cols["label_cs"];

                /// <summary>
                /// Column description_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColDescriptionCs = Cols["description_cs"];

                /// <summary>
                /// Column label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColLabelEn = Cols["label_en"];

                /// <summary>
                /// Column description_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColDescriptionEn = Cols["description_en"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="extended">Altogether category is included - true|false</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public SubPopulationCategoryList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    bool extended = false,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            extended: extended,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="extended">Altogether category is included - true|false</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public SubPopulationCategoryList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    bool extended = false,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            extended: extended,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// List of sub-population categories (read-only)
                /// </summary>
                public List<SubPopulationCategory> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new SubPopulationCategory(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexers

                /// <summary>
                /// Sub-population category from list by identifier (read-only)
                /// </summary>
                /// <param name="id">Sub-population category identifier</param>
                /// <returns>Sub-population category from list by identifier (null if not found)</returns>
                public SubPopulationCategory this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new SubPopulationCategory(composite: this, data: a))
                                .FirstOrDefault<SubPopulationCategory>();
                    }
                }

                #endregion Indexers


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public SubPopulationCategoryList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new SubPopulationCategoryList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            extended: Extended,
                            loadingTime: LoadingTime) :
                        new SubPopulationCategoryList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            extended: Extended,
                            loadingTime: null);
                }

                /// <summary>
                /// List of sub-population categories
                /// for selected subpopulations
                /// </summary>
                /// <param name="subPopulationId">Selected subpopulations identifiers</param>
                /// <returns>
                /// List of sub-population categories
                /// for selected subpopulations
                /// </returns>
                public SubPopulationCategoryList Reduce(
                    List<int> subPopulationId = null)
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();

                    if ((subPopulationId != null) && subPopulationId.Count != 0)
                    {
                        rows = rows.Where(a => subPopulationId.Contains<int>(
                                value: (int)(a.Field<Nullable<int>>(columnName: ColSubPopulationId.Name) ?? 0)));
                    }

                    return
                        rows.Any() ?
                        new SubPopulationCategoryList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            extended: Extended,
                            loadingTime: LoadingTime) :
                        new SubPopulationCategoryList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            extended: Extended,
                            loadingTime: null);
                }

                /// <summary>
                /// Loads data from database table
                /// (if it was not done before)
                /// </summary>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="extended">Altogether category is included - true|false</param>
                public new void Load(
                    string condition = null,
                    Nullable<int> limit = null,
                    bool extended = false)
                {
                    if (!Loaded)
                    {
                        ReLoad(
                            condition: condition,
                            limit: limit,
                            extended: extended);
                    }
                }

                /// <summary>
                /// Loads data from database table
                /// </summary>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="extended">Altogether category is included - true|false</param>
                public new void ReLoad(
                    string condition = null,
                    Nullable<int> limit = null,
                    bool extended = false)
                {
                    base.ReLoad(
                        condition: condition,
                        limit: limit,
                        extended: extended);

                    foreach (DataRow row in Data.Rows)
                    {
                        if (Functions.GetIntArg(
                            row: row,
                            name: SubPopulationCategoryList.ColId.Name,
                            defaultValue: -1) == 0)
                        {
                            Functions.SetIntArg(
                                row: row,
                                name: SubPopulationCategoryList.ColSubPopulationId.Name,
                                val: 0);
                        }
                    }
                }

                #endregion Methods

            }

        }
    }
}
