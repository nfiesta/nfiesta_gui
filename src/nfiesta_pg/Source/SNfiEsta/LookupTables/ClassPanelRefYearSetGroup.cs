﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {
            // c_panel_refyearset_group

            /// <summary>
            /// Aggregated sets of panels and corresponding reference year sets
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class PanelRefYearSetGroup(
                PanelRefYearSetGroupList composite = null,
                DataRow data = null)
                    : ALookupTableEntry<PanelRefYearSetGroupList>(composite: composite, data: data)
            {

                #region Constants

                /// <summary>
                /// <para lang="cs">Maximální povolená délka popisku v národní jazyce</para>
                /// <para lang="en">Maximum allowed length of the label in national language</para>
                /// </summary>
                public const int LabelCsMaxLength = 200;

                /// <summary>
                /// <para lang="cs">Maximální povolená délka popisku v angličtině</para>
                /// <para lang="en">Maximum allowed length of the label in English</para>
                /// </summary>
                public const int LabelEnMaxLength = 200;

                #endregion Constants


                #region Derived Properties

                /// <summary>
                /// Aggregated sets of panels and corresponding reference year sets identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: PanelRefYearSetGroupList.ColId.Name,
                            defaultValue: Int32.Parse(s: PanelRefYearSetGroupList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: PanelRefYearSetGroupList.ColId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Label of aggregated sets of panels and corresponding reference year sets in national language
                /// </summary>
                public override string LabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: PanelRefYearSetGroupList.ColLabelCs.Name,
                            defaultValue: PanelRefYearSetGroupList.ColLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: PanelRefYearSetGroupList.ColLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Description of aggregated sets of panels and corresponding reference year sets in national language
                /// </summary>
                public override string DescriptionCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: PanelRefYearSetGroupList.ColDescriptionCs.Name,
                            defaultValue: PanelRefYearSetGroupList.ColDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: PanelRefYearSetGroupList.ColDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Extended label of agrregated set of panels and corresponding reference year sets in national language (read-only)
                /// </summary>
                public override string ExtendedLabelCs
                {
                    get
                    {
                        return $"{Id} - {LabelCs}";
                    }
                }


                /// <summary>
                /// Label of aggregated sets of panels and corresponding reference year sets in English
                /// </summary>
                public override string LabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: PanelRefYearSetGroupList.ColLabelEn.Name,
                            defaultValue: PanelRefYearSetGroupList.ColLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: PanelRefYearSetGroupList.ColLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Description of aggregated sets of panels and corresponding reference year sets in English
                /// </summary>
                public override string DescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: PanelRefYearSetGroupList.ColDescriptionEn.Name,
                            defaultValue: PanelRefYearSetGroupList.ColDescriptionEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: PanelRefYearSetGroupList.ColDescriptionEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Extended label of aggregated set of panels and corresponding reference year sets in national language (read-only)
                /// </summary>
                public override string ExtendedLabelEn
                {
                    get
                    {
                        return $"{Id} - {LabelEn}";
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true|false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not PanelRefYearSetGroup Type, return False
                    if (obj is not PanelRefYearSetGroup)
                    {
                        return false;
                    }

                    return ((PanelRefYearSetGroup)obj).Id == Id;
                }

                /// <summary>
                /// Returns a hash code for the current object
                /// </summary>
                /// <returns>Hash code for the current object</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                /// <summary>
                /// Returns a string that represents the current object
                /// </summary>
                /// <returns>String that represents the current object</returns>
                public override string ToString()
                {
                    return String.Concat(
                        $"{nameof(PanelRefYearSetGroup)}: {{",
                        $"{nameof(Id)}: {Functions.PrepIntArg(arg: Id)}; ",
                        $"{nameof(LabelCs)}: {Functions.PrepStringArg(arg: LabelCs)}; ",
                        $"{nameof(LabelEn)}: {Functions.PrepStringArg(arg: LabelEn)}; ",
                        $"{nameof(DescriptionCs)}: {Functions.PrepStringArg(arg: DescriptionCs)}; ",
                        $"{nameof(DescriptionEn)}: {Functions.PrepStringArg(arg: DescriptionEn)}}}");
                }

                #endregion Methods

            }


            /// <summary>
            /// List of aggregated sets of panels and corresponding reference year sets
            /// </summary>
            public class PanelRefYearSetGroupList
                : ALookupTable
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema = NfiEstaSchema.Name;

                /// <summary>
                /// Table name
                /// </summary>
                public const string Name = "c_panel_refyearset_group";

                /// <summary>
                /// Table alias
                /// </summary>
                public const string Alias = "c_panel_refyearset_group";

                /// <summary>
                /// Description in national language
                /// </summary>
                public const string CaptionCs = "Seznam agregací panelů a odpovídajících roků měření";

                /// <summary>
                /// Description in English
                /// </summary>
                public const string CaptionEn = "List of aggregated sets of panels and corresponding reference year sets";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
                {
                    { "id", new ColumnMetadata()
                    {
                        Name = "id",
                        DbName = "id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = true,
                        NotNull = true,
                        DefaultValue = default(int).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ID",
                        HeaderTextEn = "ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 0
                    }
                    },
                    { "label_cs", new ColumnMetadata()
                    {
                        Name = "label_cs",
                        DbName = "label",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "LABEL_CS",
                        HeaderTextEn = "LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 1
                    }
                    },
                    { "description_cs", new ColumnMetadata()
                    {
                        Name = "description_cs",
                        DbName = "description",
                        DataType = "System.String",
                        DbDataType = "text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "DESCRIPTION_CS",
                        HeaderTextEn = "DESCRIPTION_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 2
                    }
                    },
                    { "label_en", new ColumnMetadata()
                    {
                        Name = "label_en",
                        DbName = "label_en",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "LABEL_EN",
                        HeaderTextEn = "LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 3
                    }
                    },
                    { "description_en", new ColumnMetadata()
                    {
                        Name = "description_en",
                        DbName = "description_en",
                        DataType = "System.String",
                        DbDataType = "text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "DESCRIPTION_EN",
                        HeaderTextEn = "DESCRIPTION_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 4
                    }
                    }
                };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColLabelCs = Cols["label_cs"];

                /// <summary>
                /// Column description_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColDescriptionCs = Cols["description_cs"];

                /// <summary>
                /// Column label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColLabelEn = Cols["label_en"];

                /// <summary>
                /// Column description_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColDescriptionEn = Cols["description_en"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="extended">Altogether category is included - true|false</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public PanelRefYearSetGroupList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    bool extended = false,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            extended: extended,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="extended">Altogether category is included - true|false</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public PanelRefYearSetGroupList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    bool extended = false,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            extended: extended,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// List of aggregated sets of panels and corresponding reference year sets (read-only)
                /// </summary>
                public List<PanelRefYearSetGroup> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new PanelRefYearSetGroup(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// Aggregated sets of panels and corresponding reference year sets from list by identifier (read-only)
                /// </summary>
                /// <param name="id">Aggregated sets of panels and corresponding reference year sets identifier</param>
                /// <returns>Aggregated sets of panels and corresponding reference year sets from list by identifier (null if not found)</returns>
                public PanelRefYearSetGroup this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new PanelRefYearSetGroup(composite: this, data: a))
                                .FirstOrDefault<PanelRefYearSetGroup>();
                    }
                }

                #endregion Indexer


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public PanelRefYearSetGroupList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new PanelRefYearSetGroupList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            extended: Extended,
                            loadingTime: LoadingTime) :
                        new PanelRefYearSetGroupList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            extended: Extended,
                            loadingTime: null);
                }

                /// <summary>
                /// Tests whether aggregated sets of panels and corresponding reference year sets exists in database
                /// </summary>
                /// <param name="item">Aggregated sets of panels and corresponding reference year sets</param>
                /// <returns>true/false</returns>
                public bool Exists(PanelRefYearSetGroup item)
                {
                    if (item != null)
                    {
                        string sql = String.Concat(
                            $"SELECT EXISTS {Environment.NewLine}",
                            $"   (SELECT {ColId.DbName}{Environment.NewLine}",
                            $"    FROM {NfiEstaSchema.Name}.{Name}",
                            $"    WHERE {ColId.DbName} = {Functions.PrepIntArg(item.Id)});");

                        if (Boolean.TryParse(Database.Postgres.ExecuteScalar(sql), out bool val))
                        {
                            return val;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }

                /// <summary>
                /// Update aggregated sets of panels and corresponding reference year sets in database
                /// </summary>
                /// <param name="item">Aggregated sets of panels and corresponding reference year sets</param>
                public void Update(PanelRefYearSetGroup item)
                {
                    if (item != null)
                    {
                        int id = item.Id;
                        string labelCs = item.LabelCs;
                        string descriptionCs = item.DescriptionCs;
                        string labelEn = item.LabelEn;
                        string descriptionEn = item.DescriptionEn;

                        if (Items.Where(a => a.Id == id).Any())
                        {
                            PanelRefYearSetGroup panelRefYearSetGroup = Items.Where(a => a.Id == id).First();
                            panelRefYearSetGroup.Id = id;
                            panelRefYearSetGroup.LabelCs = labelCs;
                            panelRefYearSetGroup.DescriptionCs = descriptionCs;
                            panelRefYearSetGroup.LabelEn = labelEn;
                            panelRefYearSetGroup.DescriptionEn = descriptionEn;
                            Data.AcceptChanges();

                            string sql = String.Concat(
                            $"UPDATE {NfiEstaSchema.Name}.{Name}{Environment.NewLine}",
                            $"SET{Environment.NewLine}",
                            $"    {ColLabelCs.DbName} = {Functions.PrepStringArg(labelCs)},{Environment.NewLine}",
                            $"    {ColDescriptionCs.DbName} = {Functions.PrepStringArg(descriptionCs)},{Environment.NewLine}",
                            $"    {ColLabelEn.DbName} = {Functions.PrepStringArg(labelEn)},{Environment.NewLine}",
                            $"    {ColDescriptionEn.DbName} = {Functions.PrepStringArg(descriptionEn)}{Environment.NewLine}",
                            $"WHERE {ColId.DbName} = {Functions.PrepIntArg(id)};{Environment.NewLine}");
                            Database.Postgres.ExecuteNonQuery(sql);
                        }
                        else
                        {
                            throw new ArgumentException(
                                $"Panel with reference year set group id = {id} doesn't exist.");
                        }
                    }
                    else
                    {
                        throw new ArgumentException(
                               $"Panel with reference year set group is null.");
                    }
                }

                /// <summary>
                /// Delete aggregated sets of panels and corresponding reference year sets from database
                /// </summary>
                /// <param name="item">Aggregated sets of panels and corresponding reference year sets</param>
                public void Delete(PanelRefYearSetGroup item)
                {
                    if (item != null)
                    {
                        int id = item.Id;

                        if (Items.Where(a => a.Id == id).Any())
                        {
                            if (!((NfiEstaDB)Database).SNfiEsta.TTotalEstimateConf.Items.Where(a => a.PanelRefYearSetGroupId == id).Any())
                            {
                                if (!((NfiEstaDB)Database).SNfiEsta.TAuxConf.Items.Where(a => a.PanelRefYearSetGroupId == id).Any())
                                {
                                    List<string> commands =
                                    [
                                        String.Concat(
                                        $"DELETE{Environment.NewLine}",
                                        $"FROM{Environment.NewLine}",
                                        $"    {NfiEstaSchema.Name}.{PanelRefYearSetPairList.Name}{Environment.NewLine}",
                                        $"WHERE{Environment.NewLine}",
                                        $"    {PanelRefYearSetPairList.ColPanelRefYearSetGroupId.Name} = {id};"),

                                        String.Concat(
                                        $"DELETE{Environment.NewLine}",
                                        $"FROM{Environment.NewLine}",
                                        $"    {NfiEstaSchema.Name}.{Name}{Environment.NewLine}",
                                        $"WHERE{Environment.NewLine}",
                                        $"    {ColId.Name} = {id};")
                                    ];

                                    Database.Postgres.ExecuteList(sqlCommands: commands);
                                }
                                else
                                {
                                    throw new ArgumentException(
                                        $"Panel with reference year set group id = {id} is referenced from table {NfiEstaSchema.Name}.{AuxConfList.Name}.");
                                }
                            }
                            else
                            {
                                throw new ArgumentException(
                                    $"Panel with reference year set group id = {id} is referenced from table {NfiEstaSchema.Name}.{TotalEstimateConfList.Name}.");
                            }
                        }
                        else
                        {
                            throw new ArgumentException(
                                $"Panel with reference year set group id = {id} doesn't exist.");
                        }
                    }
                    else
                    {
                        throw new ArgumentException(
                                $"Panel with reference year set group is null.");
                    }
                }

                #endregion Methods

            }

        }
    }
}
