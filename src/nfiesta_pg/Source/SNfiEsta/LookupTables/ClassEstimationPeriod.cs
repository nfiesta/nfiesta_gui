﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {
            // c_estimation_period

            /// <summary>
            /// Estimation period
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class EstimationPeriod(
                EstimationPeriodList composite = null,
                DataRow data = null)
                    : ALookupTableEntry<EstimationPeriodList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Estimation period identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: EstimationPeriodList.ColId.Name,
                            defaultValue: Int32.Parse(s: EstimationPeriodList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: EstimationPeriodList.ColId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// The beginning of the time to witch the estimate is calculated
                /// </summary>
                public DateTime EstimateDateBegin
                {
                    get
                    {
                        return Functions.GetDateTimeArg(
                            row: Data,
                            name: EstimationPeriodList.ColEstimateDateBegin.Name,
                            defaultValue: DateTime.Parse(s: EstimationPeriodList.ColEstimateDateBegin.DefaultValue));
                    }
                    set
                    {
                        Functions.SetDateTimeArg(
                            row: Data,
                            name: EstimationPeriodList.ColEstimateDateBegin.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// The beginning of the time to witch the estimate is calculated as text
                /// </summary>
                public string EstimateDateBeginText
                {
                    get
                    {
                        return
                            EstimateDateBegin.ToString(
                                format: EstimationPeriodList.ColEstimateDateBegin.NumericFormat);
                    }
                }


                /// <summary>
                /// The end of the time to witch the estimate is calculated
                /// </summary>
                public DateTime EstimateDateEnd
                {
                    get
                    {
                        return Functions.GetDateTimeArg(
                            row: Data,
                            name: EstimationPeriodList.ColEstimateDateEnd.Name,
                            defaultValue: DateTime.Parse(s: EstimationPeriodList.ColEstimateDateEnd.DefaultValue));
                    }
                    set
                    {
                        Functions.SetDateTimeArg(
                            row: Data,
                            name: EstimationPeriodList.ColEstimateDateEnd.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// The end of the time to witch the estimate is calculated as text
                /// </summary>
                public string EstimateDateEndText
                {
                    get
                    {
                        return
                            EstimateDateEnd.ToString(
                                format: EstimationPeriodList.ColEstimateDateEnd.NumericFormat);
                    }
                }


                /// <summary>
                /// Label of estimation period in national language
                /// </summary>
                public override string LabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: EstimationPeriodList.ColLabelCs.Name,
                            defaultValue: EstimationPeriodList.ColLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: EstimationPeriodList.ColLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Description of estimation period in national language
                /// </summary>
                public override string DescriptionCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: EstimationPeriodList.ColDescriptionCs.Name,
                            defaultValue: EstimationPeriodList.ColDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: EstimationPeriodList.ColDescriptionCs.Name,
                            val: value);
                    }
                }

                ///// <summary>
                ///// Extended label of estimation period in national language (read-only)
                ///// </summary>
                //public override string ExtendedLabelCs
                //{
                //    get
                //    {
                //        return $"{Id} - {LabelCs}";
                //    }
                //}


                /// <summary>
                /// Label of estimation period in English
                /// </summary>
                public override string LabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: EstimationPeriodList.ColLabelEn.Name,
                            defaultValue: EstimationPeriodList.ColLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: EstimationPeriodList.ColLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Description of estimation period in English
                /// </summary>
                public override string DescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: EstimationPeriodList.ColDescriptionEn.Name,
                            defaultValue: EstimationPeriodList.ColDescriptionEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: EstimationPeriodList.ColDescriptionEn.Name,
                            val: value);
                    }
                }

                ///// <summary>
                ///// Extended label of estimation period in English (read-only)
                ///// </summary>
                //public override string ExtendedLabelEn
                //{
                //    get
                //    {
                //        return $"{Id} - {LabelEn}";
                //    }
                //}

                /// <summary>
                /// Limits the display of estimation periods to the periods with a value of true.
                /// </summary>
                public bool DefaultInOlap
                {
                    get
                    {
                        return Functions.GetBoolArg(
                            row: Data,
                            name: EstimationPeriodList.ColDefaultInOlap.Name,
                            defaultValue: Boolean.Parse(value: EstimationPeriodList.ColDefaultInOlap.DefaultValue));
                    }
                    set
                    {
                        Functions.SetBoolArg(
                            row: Data,
                            name: EstimationPeriodList.ColDefaultInOlap.Name,
                            val: value);
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not EstimationPeriod Type, return False
                    if (obj is not EstimationPeriod)
                    {
                        return false;
                    }

                    return
                        Id == ((EstimationPeriod)obj).Id;
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return Id.GetHashCode();
                }

                #endregion Methods

            }


            /// <summary>
            /// List of estimation periods
            /// </summary>
            public class EstimationPeriodList
                : ALookupTable
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema = NfiEstaSchema.Name;

                /// <summary>
                /// Table name
                /// </summary>
                public const string Name = "c_estimation_period";

                /// <summary>
                /// Table alias
                /// </summary>
                public const string Alias = "c_estimation_period";

                /// <summary>
                /// Description in national language
                /// </summary>
                public const string CaptionCs = "Seznam výpočetních období";

                /// <summary>
                /// Description in English
                /// </summary>
                public const string CaptionEn = "List of estimation periods";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
                {
                    { "id", new ColumnMetadata()
                    {
                        Name = "id",
                        DbName = "id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = true,
                        NotNull = true,
                        DefaultValue = default(int).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ID",
                        HeaderTextEn = "ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 0
                    }
                    },
                    { "estimate_date_begin", new ColumnMetadata()
                    {
                        Name = "estimate_date_begin",
                        DbName = "estimate_date_begin",
                        DataType = "System.DateTime",
                        DbDataType = "date",
                        NewDataType = "date",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = new DateTime().ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "yyyy-MM-dd",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATE_DATE_BEGIN",
                        HeaderTextEn = "ESTIMATE_DATE_BEGIN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 1
                    }
                    },
                    { "estimate_date_end", new ColumnMetadata()
                    {
                        Name = "estimate_date_end",
                        DbName = "estimate_date_end",
                        DataType = "System.DateTime",
                        DbDataType = "date",
                        NewDataType = "date",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = new DateTime().ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "yyyy-MM-dd",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATE_DATE_END",
                        HeaderTextEn = "ESTIMATE_DATE_END",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 2
                    }
                    },
                    { "label_cs", new ColumnMetadata()
                    {
                        Name = "label_cs",
                        DbName = "label",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "LABEL_CS",
                        HeaderTextEn = "LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 3
                    }
                    },
                    { "description_cs", new ColumnMetadata()
                    {
                        Name = "description_cs",
                        DbName = "description",
                        DataType = "System.String",
                        DbDataType = "text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "DESCRIPTION_CS",
                        HeaderTextEn = "DESCRIPTION_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 4
                    }
                    },
                    { "label_en", new ColumnMetadata()
                    {
                        Name = "label_en",
                        DbName = "label_en",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "LABEL_EN",
                        HeaderTextEn = "LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 5
                    }
                    },
                    { "description_en", new ColumnMetadata()
                    {
                        Name = "description_en",
                        DbName = "description_en",
                        DataType = "System.String",
                        DbDataType = "text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "DESCRIPTION_EN",
                        HeaderTextEn = "DESCRIPTION_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 6
                    }
                    },
                    { "default_in_olap", new ColumnMetadata()
                    {
                        Name = "default_in_olap",
                        DbName = "default_in_olap",
                        DataType = "System.Boolean",
                        DbDataType = "bool",
                        NewDataType = "bool",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = default(bool).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "DEFAULT_IN_OLAP",
                        HeaderTextEn = "DEFAULT_IN_OLAP",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 7
                    }
                    }
                };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column estimate_date_begin metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimateDateBegin = Cols["estimate_date_begin"];

                /// <summary>
                /// Column estimate_date_end metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimateDateEnd = Cols["estimate_date_end"];

                /// <summary>
                /// Column label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColLabelCs = Cols["label_cs"];

                /// <summary>
                /// Column description_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColDescriptionCs = Cols["description_cs"];

                /// <summary>
                /// Column label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColLabelEn = Cols["label_en"];

                /// <summary>
                /// Column description_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColDescriptionEn = Cols["description_en"];

                /// <summary>
                /// Column default_in_olap metadata
                /// </summary>
                public static readonly ColumnMetadata ColDefaultInOlap = Cols["default_in_olap"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="extended">Altogether category is included - true|false</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public EstimationPeriodList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    bool extended = false,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            extended: extended,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="extended">Altogether category is included - true|false</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public EstimationPeriodList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    bool extended = false,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            extended: extended,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// List of estimation periods (read-only)
                /// </summary>
                public List<EstimationPeriod> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new EstimationPeriod(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexers

                /// <summary>
                /// Estimation period from list by identifier (read-only)
                /// </summary>
                /// <param name="id">Estimation period identifier</param>
                /// <returns>Estimation period from list by identifier (null if not found)</returns>
                public EstimationPeriod this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new EstimationPeriod(composite: this, data: a))
                                .FirstOrDefault<EstimationPeriod>();
                    }
                }

                #endregion Indexers


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public EstimationPeriodList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new EstimationPeriodList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            extended: Extended,
                            loadingTime: LoadingTime) :
                        new EstimationPeriodList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            extended: Extended,
                            loadingTime: null);
                }

                /// <summary>
                /// Tests whether estimation period exists in database
                /// </summary>
                /// <param name="item">Estimation period</param>
                /// <returns>true/false</returns>
                public bool Exists(EstimationPeriod item)
                {
                    if (item != null)
                    {
                        string sql = String.Concat(
                            $"SELECT EXISTS {Environment.NewLine}",
                            $"   (SELECT {ColId.DbName}{Environment.NewLine}",
                            $"    FROM {NfiEstaSchema.Name}.{Name}",
                            $"    WHERE {ColId.DbName} = {Functions.PrepIntArg(item.Id)});");

                        if (Boolean.TryParse(Database.Postgres.ExecuteScalar(sql), out bool val))
                        {
                            return val;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }

                /// <summary>
                /// Update estimation period in database
                /// </summary>
                /// <param name="item">Estimation period</param>
                public void Update(EstimationPeriod item)
                {
                    if (item != null)
                    {
                        int id = item.Id;
                        string labelCs = item.LabelCs;
                        string descriptionCs = item.DescriptionCs;
                        string labelEn = item.LabelEn;
                        string descriptionEn = item.DescriptionEn;

                        if (Items.Where(a => a.Id == id).Any())
                        {
                            EstimationPeriod estimationPeriod = Items.Where(a => a.Id == id).First();
                            estimationPeriod.Id = id;
                            estimationPeriod.LabelCs = labelCs;
                            estimationPeriod.DescriptionCs = descriptionCs;
                            estimationPeriod.LabelEn = labelEn;
                            estimationPeriod.DescriptionEn = descriptionEn;
                            Data.AcceptChanges();

                            string sql = String.Concat(
                            $"UPDATE {NfiEstaSchema.Name}.{Name}{Environment.NewLine}",
                            $"SET{Environment.NewLine}",
                            $"    {ColLabelCs.DbName} = {Functions.PrepStringArg(item.LabelCs)},{Environment.NewLine}",
                            $"    {ColDescriptionCs.DbName} = {Functions.PrepStringArg(item.DescriptionCs)},{Environment.NewLine}",
                            $"    {ColLabelEn.DbName} = {Functions.PrepStringArg(item.LabelEn)},{Environment.NewLine}",
                            $"    {ColDescriptionEn.DbName} = {Functions.PrepStringArg(item.DescriptionEn)}{Environment.NewLine}",
                            $"WHERE {ColId.DbName} = {Functions.PrepIntArg(item.Id)};{Environment.NewLine}");
                            Database.Postgres.ExecuteNonQuery(sql);
                        }
                        else
                        {
                            throw new ArgumentException(
                                $"Estimation period id = {id} doesn't exist.");
                        }
                    }
                    else
                    {
                        throw new ArgumentException(
                                $"Estimation period is null.");
                    }
                }

                /// <summary>
                /// Insert new estimation period into database
                /// </summary>
                /// <param name="item">Estimation period</param>
                public void Insert(EstimationPeriod item)
                {
                    if (item != null)
                    {
                        string sql = String.Concat(
                            $"INSERT INTO {NfiEstaSchema.Name}.{Name}{Environment.NewLine}",
                            $"({Environment.NewLine}",
                            $"    {ColEstimateDateBegin.DbName},{Environment.NewLine}",
                            $"    {ColEstimateDateEnd.DbName},{Environment.NewLine}",
                            $"    {ColLabelCs.DbName},{Environment.NewLine}",
                            $"    {ColDescriptionCs.DbName},{Environment.NewLine}",
                            $"    {ColLabelEn.DbName},{Environment.NewLine}",
                            $"    {ColDescriptionEn.DbName}{Environment.NewLine}",
                            $"){Environment.NewLine}",
                            $"VALUES{Environment.NewLine}",
                            $"({Environment.NewLine}",
                            $"    {Functions.PrepStringArg(item.EstimateDateBeginText)},{Environment.NewLine}",
                            $"    {Functions.PrepStringArg(item.EstimateDateEndText)},{Environment.NewLine}",
                            $"    {Functions.PrepStringArg(item.LabelCs)},{Environment.NewLine}",
                            $"    {Functions.PrepStringArg(item.DescriptionCs)},{Environment.NewLine}",
                            $"    {Functions.PrepStringArg(item.LabelEn)},{Environment.NewLine}",
                            $"    {Functions.PrepStringArg(item.DescriptionEn)}{Environment.NewLine}",
                            $");{Environment.NewLine}");
                        Database.Postgres.ExecuteNonQuery(sql);
                    }
                    else
                    {
                        throw new ArgumentException(
                                $"Estimation period is null.");
                    }
                }

                /// <summary>
                /// Delete estimation period from database
                /// </summary>
                /// <param name="item">Estimation period</param>
                public void Delete(EstimationPeriod item)
                {
                    if (item != null)
                    {
                        string sql = String.Concat(
                        $"DELETE FROM {NfiEstaSchema.Name}.{Name}{Environment.NewLine}",
                        $"WHERE {ColId.DbName} = {Functions.PrepIntArg(item.Id)};{Environment.NewLine}");
                        Database.Postgres.ExecuteNonQuery(sql);
                    }
                    else
                    {
                        throw new ArgumentException(
                                $"Estimation period is null.");
                    }
                }

                #endregion Methods

            }

        }
    }
}


