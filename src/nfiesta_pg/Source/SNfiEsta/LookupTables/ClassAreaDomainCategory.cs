﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {
            // c_area_domain_category

            /// <summary>
            /// Area domain category
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class AreaDomainCategory(
                AreaDomainCategoryList composite = null,
                DataRow data = null)
                    : ALookupTableEntry<AreaDomainCategoryList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Area domain category identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: AreaDomainCategoryList.ColId.Name,
                            defaultValue: Int32.Parse(s: AreaDomainCategoryList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: AreaDomainCategoryList.ColId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Area domain identifier
                /// </summary>
                public int AreaDomainId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: AreaDomainCategoryList.ColAreaDomainId.Name,
                            defaultValue: Int32.Parse(s: AreaDomainCategoryList.ColAreaDomainId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: AreaDomainCategoryList.ColAreaDomainId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Area domain object (read-only)
                /// </summary>
                public AreaDomain AreaDomain
                {
                    get
                    {
                        return ((NfiEstaDB)Composite.Database).SNfiEsta.CAreaDomain[AreaDomainId];
                    }
                }


                /// <summary>
                /// Label of area domain category in national language
                /// </summary>
                public override string LabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: AreaDomainCategoryList.ColLabelCs.Name,
                            defaultValue: AreaDomainCategoryList.ColLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: AreaDomainCategoryList.ColLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Description of area domain category in national language
                /// </summary>
                public override string DescriptionCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: AreaDomainCategoryList.ColDescriptionCs.Name,
                            defaultValue: AreaDomainCategoryList.ColDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: AreaDomainCategoryList.ColDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Extended label of area domain category in national language (read-only)
                /// </summary>
                public override string ExtendedLabelCs
                {
                    get
                    {
                        return $"{Id} - {LabelCs}";
                    }
                }


                /// <summary>
                /// Label of area domain category in English
                /// </summary>
                public override string LabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: AreaDomainCategoryList.ColLabelEn.Name,
                            defaultValue: AreaDomainCategoryList.ColLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: AreaDomainCategoryList.ColLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Description of area domain category in English
                /// </summary>
                public override string DescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: AreaDomainCategoryList.ColDescriptionEn.Name,
                            defaultValue: AreaDomainCategoryList.ColDescriptionEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: AreaDomainCategoryList.ColDescriptionEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Extended label of area domain category in English (read-only)
                /// </summary>
                public override string ExtendedLabelEn
                {
                    get
                    {
                        return $"{Id} - {LabelEn}";
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true|false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not AreaDomainCategory Type, return False
                    if (obj is not AreaDomainCategory)
                    {
                        return false;
                    }

                    return ((AreaDomainCategory)obj).Id == Id;
                }

                /// <summary>
                /// Returns a hash code for the current object
                /// </summary>
                /// <returns>Hash code for the current object</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                /// <summary>
                /// Returns a string that represents the current object
                /// </summary>
                /// <returns>String that represents the current object</returns>
                public override string ToString()
                {
                    return String.Concat(
                        $"{nameof(AreaDomainCategory)}: {{",
                        $"{nameof(Id)}: {Functions.PrepIntArg(arg: Id)}; ",
                        $"{nameof(AreaDomainId)}: {Functions.PrepIntArg(arg: AreaDomainId)}; ",
                        $"{nameof(LabelCs)}: {Functions.PrepStringArg(arg: LabelCs)}; ",
                        $"{nameof(LabelEn)}: {Functions.PrepStringArg(arg: LabelEn)}; ",
                        $"{nameof(DescriptionCs)}: {Functions.PrepStringArg(arg: DescriptionCs)}; ",
                        $"{nameof(DescriptionEn)}: {Functions.PrepStringArg(arg: DescriptionEn)}}}");
                }

                #endregion Methods

            }


            /// <summary>
            /// List of area domain categories
            /// </summary>
            public class AreaDomainCategoryList
                : ALookupTable
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema = NfiEstaSchema.Name;

                /// <summary>
                /// Table name
                /// </summary>
                public const string Name = "c_area_domain_category";

                /// <summary>
                /// Table alias
                /// </summary>
                public const string Alias = "c_area_domain_category";

                /// <summary>
                /// Description in national language
                /// </summary>
                public const string CaptionCs = "Seznam kategorií plošných domén";

                /// <summary>
                /// Description in English
                /// </summary>
                public const string CaptionEn = "List of area domain categories";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
                {
                    { "id", new ColumnMetadata()
                    {
                        Name = "id",
                        DbName = "id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = true,
                        NotNull = true,
                        DefaultValue = default(int).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ID",
                        HeaderTextEn = "ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 0
                    }
                    },
                    { "area_domain", new ColumnMetadata()
                    {
                        Name = "area_domain",
                        DbName = "area_domain",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = default(int).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "AREA_DOMAIN",
                        HeaderTextEn = "AREA_DOMAIN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 1
                    }
                    },
                    { "label_cs", new ColumnMetadata()
                    {
                        Name = "label_cs",
                        DbName = "label",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "LABEL_CS",
                        HeaderTextEn = "LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 2
                    }
                    },
                    { "description_cs", new ColumnMetadata()
                    {
                        Name = "description_cs",
                        DbName = "description",
                        DataType = "System.String",
                        DbDataType = "text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "DESCRIPTION_CS",
                        HeaderTextEn = "DESCRIPTION_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 3
                    }
                    },
                    { "label_en", new ColumnMetadata()
                    {
                        Name = "label_en",
                        DbName = "label_en",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "LABEL_EN",
                        HeaderTextEn = "LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 4
                    }
                    },
                    { "description_en", new ColumnMetadata()
                    {
                        Name = "description_en",
                        DbName = "description_en",
                        DataType = "System.String",
                        DbDataType = "text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "DESCRIPTION_EN",
                        HeaderTextEn = "DESCRIPTION_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 5
                    }
                    }
                };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column area_domain metadata
                /// </summary>
                public static readonly ColumnMetadata ColAreaDomainId = Cols["area_domain"];

                /// <summary>
                /// Column label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColLabelCs = Cols["label_cs"];

                /// <summary>
                /// Column description_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColDescriptionCs = Cols["description_cs"];

                /// <summary>
                /// Column label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColLabelEn = Cols["label_en"];

                /// <summary>
                /// Column description_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColDescriptionEn = Cols["description_en"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="extended">Altogether category is included - true|false</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public AreaDomainCategoryList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    bool extended = false,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            extended: extended,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="extended">Altogether category is included - true|false</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public AreaDomainCategoryList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    bool extended = false,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            extended: extended,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// List of area domain categories (read-only)
                /// </summary>
                public List<AreaDomainCategory> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new AreaDomainCategory(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexers

                /// <summary>
                /// Area domain category from list by identifier (read-only)
                /// </summary>
                /// <param name="id">Area domain category identifier</param>
                /// <returns>Area domain category from list by identifier (null if not found)</returns>
                public AreaDomainCategory this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new AreaDomainCategory(composite: this, data: a))
                                .FirstOrDefault<AreaDomainCategory>();
                    }
                }

                #endregion Indexers


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public AreaDomainCategoryList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new AreaDomainCategoryList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            extended: Extended,
                            loadingTime: LoadingTime) :
                        new AreaDomainCategoryList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            extended: Extended,
                            loadingTime: null);
                }

                /// <summary>
                /// List of area domain categories
                /// for selected area domains
                /// </summary>
                /// <param name="areaDomainId">Selected area domains identifiers</param>
                /// <returns>
                /// List of area domain categories
                /// for selected area domains
                /// </returns>
                public AreaDomainCategoryList Reduce(
                    List<int> areaDomainId = null)
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();

                    if ((areaDomainId != null) && areaDomainId.Count != 0)
                    {
                        rows = rows.Where(a => areaDomainId.Contains<int>(
                                value: (int)(a.Field<Nullable<int>>(columnName: ColAreaDomainId.Name) ?? 0)));
                    }

                    return
                        rows.Any() ?
                        new AreaDomainCategoryList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            extended: Extended,
                            loadingTime: LoadingTime) :
                        new AreaDomainCategoryList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            extended: Extended,
                            loadingTime: null);
                }

                /// <summary>
                /// Loads data from database table
                /// (if it was not done before)
                /// </summary>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="extended">Altogether category is included - true|false</param>
                public new void Load(
                    string condition = null,
                    Nullable<int> limit = null,
                    bool extended = false)
                {
                    if (!Loaded)
                    {
                        ReLoad(
                            condition: condition,
                            limit: limit,
                            extended: extended);
                    }
                }

                /// <summary>
                /// Loads data from database table
                /// </summary>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="extended">Altogether category is included - true|false</param>
                public new void ReLoad(
                    string condition = null,
                    Nullable<int> limit = null,
                    bool extended = false)
                {
                    base.ReLoad(
                        condition: condition,
                        limit: limit,
                        extended: extended);

                    foreach (DataRow row in Data.Rows)
                    {
                        if (Functions.GetIntArg(
                            row: row,
                            name: AreaDomainCategoryList.ColId.Name,
                            defaultValue: -1) == 0)
                        {
                            Functions.SetIntArg(
                                row: row,
                                name: AreaDomainCategoryList.ColAreaDomainId.Name,
                                val: 0);
                        }
                    }
                }

                #endregion Methods

            }

        }
    }
}
