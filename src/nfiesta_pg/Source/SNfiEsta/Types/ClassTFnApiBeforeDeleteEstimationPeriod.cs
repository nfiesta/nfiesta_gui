﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {
            // fn_api_before_delete_estimation_period

            /// <summary>
            /// Controls if there is a row in the nfiesta.t_total_estimate_conf table
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class TFnApiBeforeDeleteEstimationPeriod(
                TFnApiBeforeDeleteEstimationPeriodList composite = null,
                DataRow data = null)
                    : AParametrizedViewEntry<TFnApiBeforeDeleteEstimationPeriodList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TFnApiBeforeDeleteEstimationPeriodList.ColId.Name,
                            defaultValue: Int32.Parse(s: TFnApiBeforeDeleteEstimationPeriodList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TFnApiBeforeDeleteEstimationPeriodList.ColId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Row in table nfiesta.t_total_estimate_conf exists
                /// </summary>
                public Nullable<bool> TotalEstimateConfExist
                {
                    get
                    {
                        return Functions.GetNBoolArg(
                            row: Data,
                            name: TFnApiBeforeDeleteEstimationPeriodList.ColTotalEstimateConfExist.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnApiBeforeDeleteEstimationPeriodList.ColTotalEstimateConfExist.DefaultValue) ?
                                (Nullable<bool>)null :
                                Boolean.Parse(value: TFnApiBeforeDeleteEstimationPeriodList.ColTotalEstimateConfExist.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNBoolArg(
                            row: Data,
                            name: TFnApiBeforeDeleteEstimationPeriodList.ColTotalEstimateConfExist.Name,
                            val: value);
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Specified object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not TFnApiBeforeDeleteEstimationPeriod, return False
                    if (obj is not TFnApiBeforeDeleteEstimationPeriod)
                    {
                        return false;
                    }

                    return
                        Id == ((TFnApiBeforeDeleteEstimationPeriod)obj).Id;
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                /// <summary>
                /// Text description of the object
                /// </summary>
                /// <returns>Text description of the object</returns>
                public override string ToString()
                {
                    string totalEstimateConfExist =
                        (TotalEstimateConfExist == null)
                            ? Functions.StrNull
                            : ((bool)TotalEstimateConfExist).ToString();

                    return String.Concat(
                        $"{nameof(Id)}: {Id}, ",
                        $"{nameof(TotalEstimateConfExist)}: {totalEstimateConfExist}");
                }

                #endregion Methods

            }


            /// <summary>
            /// List of controls if there is a row in the nfiesta.t_total_estimate_conf table
            /// </summary>
            public class TFnApiBeforeDeleteEstimationPeriodList
                : AParametrizedView
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema =
                    NfiEstaFunctions.FnApiBeforeDeleteEstimationPeriod.SchemaName;

                /// <summary>
                /// Stored procedure name
                /// </summary>
                public static readonly string Name =
                    NfiEstaFunctions.FnApiBeforeDeleteEstimationPeriod.Name;

                /// <summary>
                /// Stored procedure alias
                /// </summary>
                public static readonly string Alias =
                    NfiEstaFunctions.FnApiBeforeDeleteEstimationPeriod.Name;

                /// <summary>
                /// Description in national language
                /// </summary>
                public static readonly string CaptionCs =
                    "Kontrola existence záznamu v tabulce nfiesta.t_total_estimate_conf";

                /// <summary>
                /// Description in English
                /// </summary>
                public static readonly string CaptionEn =
                    "Checks if there is a row in the nfiesta.t_total_estimate_conf table";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
                {
                    { "id", new ColumnMetadata()
                    {
                        Name = "id",
                        DbName = "id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = true,
                        NotNull = true,
                        DefaultValue = "0",
                        FuncCall = "row_number() over()",
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ID",
                        HeaderTextEn = "ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 0
                    }
                    },
                    { "total_estimate_conf_exist", new ColumnMetadata()
                    {
                        Name = "total_estimate_conf_exist",
                        DbName = "_t_total_estimate_conf_exi",
                        DataType = "System.Boolean",
                        DbDataType = "bool",
                        NewDataType = "bool",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "TOTAL_ESTIMATE_CONF_EXIST",
                        HeaderTextEn = "TOTAL_ESTIMATE_CONF_EXIST",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 1
                    }
                    }
                };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column total_estimate_conf_exist metadata
                /// </summary>
                public static readonly ColumnMetadata ColTotalEstimateConfExist = Cols["total_estimate_conf_exist"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Private Fields

                /// <summary>
                /// First parameter - Estimation period identifier
                /// </summary>
                private Nullable<int> estimationPeriodId;

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TFnApiBeforeDeleteEstimationPeriodList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                {
                    EstimationPeriodId = null;
                }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TFnApiBeforeDeleteEstimationPeriodList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                {
                    EstimationPeriodId = null;
                }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// First parameter - Estimation period identifier
                /// </summary>
                public Nullable<int> EstimationPeriodId
                {
                    get
                    {
                        return estimationPeriodId;
                    }
                    set
                    {
                        estimationPeriodId = value;
                    }
                }

                /// <summary>
                /// List of checks if there is a row in the nfiesta.t_total_estimate_conf table (read-only)
                /// </summary>
                public List<TFnApiBeforeDeleteEstimationPeriod> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new TFnApiBeforeDeleteEstimationPeriod(composite: this, data: a))];
                    }
                }

                /// <summary>
                /// Row in the nfiesta.t_total_estimate_conf table exists (read-only)
                /// </summary>
                public Nullable<bool> TotalEstimateConfExist
                {
                    get
                    {
                        return
                            Items
                                .FirstOrDefault<TFnApiBeforeDeleteEstimationPeriod>()
                                ?.TotalEstimateConfExist;
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// Check if there is a row in the nfiesta.t_total_estimate_conf table from list by identifier (read-only)
                /// </summary>
                /// <param name="id">Check if there is a row in the nfiesta.t_total_estimate_conf table identifier</param>
                /// <returns>Check if there is a row in the fiesta.t_total_estimate_conf table from list by identifier (null if not found)</returns>
                public TFnApiBeforeDeleteEstimationPeriod this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new TFnApiBeforeDeleteEstimationPeriod(composite: this, data: a))
                                .FirstOrDefault<TFnApiBeforeDeleteEstimationPeriod>();
                    }
                }

                #endregion Indexer


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public TFnApiBeforeDeleteEstimationPeriodList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any()
                        ? new TFnApiBeforeDeleteEstimationPeriodList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime)
                        {
                            EstimationPeriodId = EstimationPeriodId
                        }
                        : new TFnApiBeforeDeleteEstimationPeriodList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null)
                        {
                           EstimationPeriodId = EstimationPeriodId
                        };
                }

                #endregion Methods

            }

        }
    }
}
