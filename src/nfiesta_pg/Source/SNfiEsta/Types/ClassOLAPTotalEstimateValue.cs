﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using MathNet.Numerics.Distributions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {
            // v_olap_total_estimate_value

            /// <summary>
            /// OLAP Cube Value - Total Estimate
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class OLAPTotalEstimateValue(
                OLAPTotalEstimateValueList composite = null,
                DataRow data = null)
                    : ALinqViewEntry<OLAPTotalEstimateValueList>(composite: composite, data: data)
            {

                #region Derived Properties

                #region Dimensions

                /// <summary>
                /// OLAP Cube Dimension - Total Estimate - composite identifier
                /// </summary>
                public new string Id
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColId.Name,
                            defaultValue: OLAPTotalEstimateValueList.ColId.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Estimate phase identifier
                /// </summary>
                public int PhaseEstimateTypeId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColPhaseEstimateTypeId.Name,
                            defaultValue: Int32.Parse(s: OLAPTotalEstimateValueList.ColPhaseEstimateTypeId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColPhaseEstimateTypeId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Estimate phase object (read-only)
                /// </summary>
                public PhaseEstimateType PhaseEstimateType
                {
                    get
                    {
                        return
                             ((NfiEstaDB)Composite.Database).SNfiEsta.CPhaseEstimateType[(int)PhaseEstimateTypeId];
                    }
                }


                /// <summary>
                /// Estimation period identifier
                /// </summary>
                public Nullable<int> EstimationPeriodId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColEstimationPeriodId.Name,
                            defaultValue:
                                String.IsNullOrEmpty(value: OLAPTotalEstimateValueList.ColEstimationPeriodId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: OLAPTotalEstimateValueList.ColEstimationPeriodId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColEstimationPeriodId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Estimation period object (read-only)
                /// </summary>
                public EstimationPeriod EstimationPeriod
                {
                    get
                    {
                        return (EstimationPeriodId != null) ?
                             ((NfiEstaDB)Composite.Database).SNfiEsta.CEstimationPeriod[(int)EstimationPeriodId] : null;
                    }
                }


                /// <summary>
                /// Combination of target variable and sub-population category and area domain category - identifier
                /// </summary>
                public int VariableId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColVariableId.Name,
                            defaultValue: Int32.Parse(s: OLAPTotalEstimateValueList.ColVariableId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColVariableId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Combination of target variable and sub-population category and area domain category - object (read-only)
                /// </summary>
                public Variable Variable
                {
                    get
                    {
                        return
                             ((NfiEstaDB)Composite.Database).SNfiEsta.TVariable[(int)VariableId];
                    }
                }

                /// <summary>
                /// Combination of target variable and sub-population category and area domain category (extended-version) - object (read-only)
                /// </summary>
                public VwVariable VwVariable
                {
                    get
                    {
                        return
                             ((NfiEstaDB)Composite.Database).SNfiEsta.VVariable[(int)VariableId];
                    }
                }


                /// <summary>
                /// Estimation cell identifier
                /// </summary>
                public int EstimationCellId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColEstimationCellId.Name,
                            defaultValue: Int32.Parse(s: OLAPTotalEstimateValueList.ColEstimationCellId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColEstimationCellId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Estimation cell object (read-only)
                /// </summary>
                public EstimationCell EstimationCell
                {
                    get
                    {
                        return
                             ((NfiEstaDB)Composite.Database).SNfiEsta.CEstimationCell[(int)EstimationCellId];
                    }
                }

                #endregion Dimensions

                #region Values

                /// <summary>
                /// Aggregated sets of panels and corresponding reference year sets - identifier
                /// </summary>
                public Nullable<int> PanelRefYearSetGroupId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColPanelRefYearSetGroupId.Name,
                            defaultValue:
                                String.IsNullOrEmpty(value: OLAPTotalEstimateValueList.ColPanelRefYearSetGroupId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: OLAPTotalEstimateValueList.ColPanelRefYearSetGroupId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColPanelRefYearSetGroupId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Aggregated sets of panels and corresponding reference year sets - label (in national language)
                /// </summary>
                public string PanelRefYearSetGroupLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColPanelRefYearSetGroupLabelCs.Name,
                            defaultValue: OLAPTotalEstimateValueList.ColPanelRefYearSetGroupLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColPanelRefYearSetGroupLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Aggregated sets of panels and corresponding reference year sets - label (in English)
                /// </summary>
                public string PanelRefYearSetGroupLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColPanelRefYearSetGroupLabelEn.Name,
                            defaultValue: OLAPTotalEstimateValueList.ColPanelRefYearSetGroupLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColPanelRefYearSetGroupLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Aggregated sets of panels and corresponding reference year sets - object (read-only)
                /// </summary>
                public PanelRefYearSetGroup PanelRefYearSetGroup
                {
                    get
                    {
                        return (PanelRefYearSetGroupId != null) ?
                             ((NfiEstaDB)Composite.Database).SNfiEsta.CPanelRefYearSetGroup[(int)PanelRefYearSetGroupId] : null;
                    }
                }


                /// <summary>
                /// Estimate configuration identifier
                /// </summary>
                public int EstimateConfId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColEstimateConfId.Name,
                            defaultValue: Int32.Parse(s: OLAPTotalEstimateValueList.ColEstimateConfId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColEstimateConfId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Estimate configuration object (read-only)
                /// </summary>
                public EstimateConf EstimateConf
                {
                    get
                    {
                        return
                             ((NfiEstaDB)Composite.Database).SNfiEsta.TEstimateConf[(int)EstimateConfId];
                    }
                }


                /// <summary>
                /// Estimate type (total or ratio) identifier
                /// </summary>
                public int EstimateTypeId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColEstimateTypeId.Name,
                            defaultValue: Int32.Parse(s: OLAPTotalEstimateValueList.ColEstimateTypeId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColEstimateTypeId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Estimate type (total or ratio) object (read-only)
                /// </summary>
                public EstimateType EstimateType
                {
                    get
                    {
                        return
                             ((NfiEstaDB)Composite.Database).SNfiEsta.CEstimateType[(int)EstimateTypeId];
                    }
                }


                /// <summary>
                /// Total estimate configuration identifier
                /// </summary>
                public int TotalEstimateConfId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColTotalEstimateConfId.Name,
                            defaultValue: Int32.Parse(s: OLAPTotalEstimateValueList.ColTotalEstimateConfId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColTotalEstimateConfId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Total estimate configuration object (read-only)
                /// </summary>
                public TotalEstimateConf TotalEstimateConf
                {
                    get
                    {
                        return
                             ((NfiEstaDB)Composite.Database).SNfiEsta.TTotalEstimateConf[(int)TotalEstimateConfId];
                    }
                }


                /// <summary>
                /// User, who configured estimate
                /// </summary>
                public string EstimateExecutor
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColEstimateExecutor.Name,
                            defaultValue: OLAPTotalEstimateValueList.ColEstimateExecutor.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColEstimateExecutor.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Total estimate configuration name (Short description of the estimate)
                /// </summary>
                public string TotalEstimateConfName
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColTotalEstimateConfName.Name,
                            defaultValue: OLAPTotalEstimateValueList.ColTotalEstimateConfName.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColTotalEstimateConfName.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Final estimate identifier
                /// </summary>
                public Nullable<int> ResultId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColResultId.Name,
                            defaultValue:
                                String.IsNullOrEmpty(value: OLAPTotalEstimateValueList.ColResultId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: OLAPTotalEstimateValueList.ColResultId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColResultId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Final estimate object (read-only)
                /// </summary>
                public Result Result
                {
                    get
                    {
                        return (ResultId != null) ?
                             ((NfiEstaDB)Composite.Database).SNfiEsta.TResult[(int)ResultId] : null;
                    }
                }


                /// <summary>
                /// Point estimate
                /// </summary>
                public Nullable<double> PointEstimate
                {
                    get
                    {
                        return Functions.GetNDoubleArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColPointEstimate.Name,
                            defaultValue:
                                String.IsNullOrEmpty(value: OLAPTotalEstimateValueList.ColPointEstimate.DefaultValue) ?
                                (Nullable<double>)null :
                                Double.Parse(s: OLAPTotalEstimateValueList.ColPointEstimate.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNDoubleArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColPointEstimate.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Variance estimate
                /// </summary>
                public Nullable<double> VarianceEstimate
                {
                    get
                    {
                        return Functions.GetNDoubleArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColVarianceEstimate.Name,
                            defaultValue:
                            String.IsNullOrEmpty(
                                value: OLAPTotalEstimateValueList.ColVarianceEstimate.DefaultValue) ?
                                (Nullable<double>)null :
                                Double.Parse(s: OLAPTotalEstimateValueList.ColVarianceEstimate.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNDoubleArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColVarianceEstimate.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Time when calculation started
                /// </summary>
                public Nullable<DateTime> CalcStarted
                {
                    get
                    {
                        return Functions.GetNDateTimeArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColCalcStarted.Name,
                            defaultValue:
                                String.IsNullOrEmpty(value: OLAPTotalEstimateValueList.ColCalcStarted.DefaultValue) ?
                                (Nullable<DateTime>)null :
                                DateTime.Parse(s: OLAPTotalEstimateValueList.ColCalcStarted.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNDateTimeArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColCalcStarted.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Time when calculation started as text
                /// </summary>
                public string CalcStartedText
                {
                    get
                    {
                        return
                            (CalcStarted == null) ? Functions.StrNull :
                                ((DateTime)CalcStarted).ToString(
                                format: OLAPTotalEstimateValueList.ColCalcStarted.NumericFormat);
                    }
                }

                /// <summary>
                /// Version of the extension with which the results were calculated
                /// </summary>
                public string ExtensionVersion
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColExtensionVersion.Name,
                            defaultValue: OLAPTotalEstimateValueList.ColExtensionVersion.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColExtensionVersion.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Time necessary for calculation
                /// </summary>
                public Nullable<TimeSpan> CalcDuration
                {
                    get
                    {
                        return Functions.GetNTimeSpanArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColCalcDuration.Name,
                            defaultValue:
                                String.IsNullOrEmpty(value: OLAPTotalEstimateValueList.ColCalcDuration.DefaultValue) ?
                                (Nullable<TimeSpan>)null :
                                TimeSpan.Parse(s: OLAPTotalEstimateValueList.ColCalcDuration.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNTimeSpanArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColCalcDuration.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Time necessary for calculation as text
                /// </summary>
                public string CalcDurationText
                {
                    get
                    {
                        return
                            (CalcDuration == null) ? Functions.StrNull :
                                ((TimeSpan)CalcDuration).ToString(
                                format: OLAPTotalEstimateValueList.ColCalcDuration.NumericFormat);
                    }
                }

                /// <summary>
                /// Minimal sample size
                /// </summary>
                public Nullable<long> MinSSize
                {
                    get
                    {
                        return Functions.GetNLongArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColMinSSize.Name,
                            defaultValue:
                                String.IsNullOrEmpty(value: OLAPTotalEstimateValueList.ColMinSSize.DefaultValue) ?
                                (Nullable<long>)null :
                                Int64.Parse(s: OLAPTotalEstimateValueList.ColMinSSize.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNLongArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColMinSSize.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Actual sample size
                /// </summary>
                public Nullable<long> ActSSize
                {
                    get
                    {
                        return Functions.GetNLongArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColActSSize.Name,
                            defaultValue:
                                String.IsNullOrEmpty(value: OLAPTotalEstimateValueList.ColActSSize.DefaultValue) ?
                                (Nullable<long>)null :
                                Int64.Parse(s: OLAPTotalEstimateValueList.ColActSSize.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNLongArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColActSSize.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Flag for identification actual value of final estimate
                /// </summary>
                public Nullable<bool> IsLatest
                {
                    get
                    {
                        return Functions.GetNBoolArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColIsLatest.Name,
                            defaultValue:
                                String.IsNullOrEmpty(value: OLAPTotalEstimateValueList.ColIsLatest.DefaultValue) ?
                                (Nullable<bool>)null :
                                Boolean.Parse(value: OLAPTotalEstimateValueList.ColIsLatest.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNBoolArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColIsLatest.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// User, who calculated estimate
                /// </summary>
                public string ResultExecutor
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColResultExecutor.Name,
                            defaultValue: OLAPTotalEstimateValueList.ColResultExecutor.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColResultExecutor.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Standard error
                /// </summary>
                public Nullable<double> StandardError
                {
                    get
                    {
                        return Functions.GetNDoubleArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColStandardError.Name,
                            defaultValue:
                                String.IsNullOrEmpty(value: OLAPTotalEstimateValueList.ColStandardError.DefaultValue) ?
                                (Nullable<double>)null :
                                Double.Parse(s: OLAPTotalEstimateValueList.ColStandardError.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNDoubleArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColStandardError.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Confidence interval
                /// </summary>
                public Nullable<double> ConfidenceInterval
                {
                    get
                    {
                        return Functions.GetNDoubleArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColConfidenceInterval.Name,
                            defaultValue:
                                String.IsNullOrEmpty(value: OLAPTotalEstimateValueList.ColConfidenceInterval.DefaultValue) ?
                                (Nullable<double>)null :
                                Double.Parse(s: OLAPTotalEstimateValueList.ColConfidenceInterval.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNDoubleArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColConfidenceInterval.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Confidence interval - lower bound
                /// </summary>
                public Nullable<double> LowerBound
                {
                    get
                    {
                        return Functions.GetNDoubleArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColLowerBound.Name,
                            defaultValue:
                                String.IsNullOrEmpty(value: OLAPTotalEstimateValueList.ColLowerBound.DefaultValue) ?
                                (Nullable<double>)null :
                                Double.Parse(s: OLAPTotalEstimateValueList.ColLowerBound.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNDoubleArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColLowerBound.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Confidence interval - upper bound
                /// </summary>
                public Nullable<double> UpperBound
                {
                    get
                    {
                        return Functions.GetNDoubleArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColUpperBound.Name,
                            defaultValue:
                                String.IsNullOrEmpty(value: OLAPTotalEstimateValueList.ColUpperBound.DefaultValue) ?
                                (Nullable<double>)null :
                                Double.Parse(s: OLAPTotalEstimateValueList.ColUpperBound.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNDoubleArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColUpperBound.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Attribute additivity difference -  sum of lower categories
                /// </summary>
                public Nullable<double> AttrDiffDown
                {
                    get
                    {
                        return Functions.GetNDoubleArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColAttrDiffDown.Name,
                            defaultValue:
                                String.IsNullOrEmpty(value: OLAPTotalEstimateValueList.ColAttrDiffDown.DefaultValue) ?
                                (Nullable<double>)null :
                                Double.Parse(s: OLAPTotalEstimateValueList.ColAttrDiffDown.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNDoubleArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColAttrDiffDown.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Attribute additivity difference -  superior category
                /// </summary>
                public Nullable<double> AttrDiffUp
                {
                    get
                    {
                        return Functions.GetNDoubleArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColAttrDiffUp.Name,
                            defaultValue:
                                String.IsNullOrEmpty(value: OLAPTotalEstimateValueList.ColAttrDiffUp.DefaultValue) ?
                                (Nullable<double>)null :
                                Double.Parse(s: OLAPTotalEstimateValueList.ColAttrDiffUp.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNDoubleArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColAttrDiffUp.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Geographical additivity difference -  sum of lower categories
                /// </summary>
                public Nullable<double> GeoDiffDown
                {
                    get
                    {
                        return Functions.GetNDoubleArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColGeoDiffDown.Name,
                            defaultValue:
                                String.IsNullOrEmpty(value: OLAPTotalEstimateValueList.ColGeoDiffDown.DefaultValue) ?
                                (Nullable<double>)null :
                                Double.Parse(s: OLAPTotalEstimateValueList.ColGeoDiffDown.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNDoubleArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColGeoDiffDown.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Geographical additivity difference -  superior category
                /// </summary>
                public Nullable<double> GeoDiffUp
                {
                    get
                    {
                        return Functions.GetNDoubleArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColGeoDiffUp.Name,
                            defaultValue: String.IsNullOrEmpty(
                                value: OLAPTotalEstimateValueList.ColGeoDiffUp.DefaultValue) ?
                                (Nullable<double>)null :
                                Double.Parse(s: OLAPTotalEstimateValueList.ColGeoDiffUp.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNDoubleArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColGeoDiffUp.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Model identifier
                /// </summary>
                public Nullable<int> ModelId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColModelId.Name,
                            defaultValue:
                                String.IsNullOrEmpty(value: OLAPTotalEstimateValueList.ColModelId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: OLAPTotalEstimateValueList.ColModelId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColModelId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Model label in national language
                /// </summary>
                public string ModelLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColModelLabelCs.Name,
                            defaultValue: OLAPTotalEstimateValueList.ColModelLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColModelLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Model description in national language
                /// </summary>
                public string ModelDescriptionCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColModelDescriptionCs.Name,
                            defaultValue: OLAPTotalEstimateValueList.ColModelDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColModelDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Model label in English
                /// </summary>
                public string ModelLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColModelLabelEn.Name,
                            defaultValue: OLAPTotalEstimateValueList.ColModelLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColModelLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Model description in English
                /// </summary>
                public string ModelDescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColModelLabelEn.Name,
                            defaultValue: OLAPTotalEstimateValueList.ColModelLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColModelLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Sigma
                /// </summary>
                public Nullable<bool> Sigma
                {
                    get
                    {
                        return Functions.GetNBoolArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColSigma.Name,
                            defaultValue:
                                String.IsNullOrEmpty(value: OLAPTotalEstimateValueList.ColSigma.DefaultValue) ?
                                (Nullable<bool>)null :
                                Boolean.Parse(value: OLAPTotalEstimateValueList.ColSigma.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNBoolArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColSigma.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Parametrization area identifier
                /// </summary>
                public Nullable<int> ParamAreaId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColParamAreaId.Name,
                            defaultValue:
                                String.IsNullOrEmpty(value: OLAPTotalEstimateValueList.ColParamAreaId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: OLAPTotalEstimateValueList.ColParamAreaId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColParamAreaId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Parametrization area label in national language
                /// </summary>
                public string ParamAreaLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColParamAreaLabelCs.Name,
                            defaultValue: OLAPTotalEstimateValueList.ColParamAreaLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColParamAreaLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Parametrization area description in national language
                /// </summary>
                public string ParamAreaDescriptionCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColParamAreaDescriptionCs.Name,
                            defaultValue: OLAPTotalEstimateValueList.ColParamAreaDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColParamAreaDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Parametrization area label in English
                /// </summary>
                public string ParamAreaLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColParamAreaLabelEn.Name,
                            defaultValue: OLAPTotalEstimateValueList.ColParamAreaLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColParamAreaLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Parametrization area description in English
                /// </summary>
                public string ParamAreaDescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColParamAreaDescriptionEn.Name,
                            defaultValue: OLAPTotalEstimateValueList.ColParamAreaDescriptionEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColParamAreaDescriptionEn.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Parametrization area type identifier
                /// </summary>
                public Nullable<int> ParamAreaTypeId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColParamAreaTypeId.Name,
                            defaultValue:
                                String.IsNullOrEmpty(value: OLAPTotalEstimateValueList.ColParamAreaTypeId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: OLAPTotalEstimateValueList.ColParamAreaTypeId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColParamAreaTypeId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Parametrization area type label in national language
                /// </summary>
                public string ParamAreaTypeLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColParamAreaTypeLabelCs.Name,
                            defaultValue: OLAPTotalEstimateValueList.ColParamAreaTypeLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColParamAreaTypeLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Parametrization area type description in national language
                /// </summary>
                public string ParamAreaTypeDescriptionCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColParamAreaTypeDescriptionCs.Name,
                            defaultValue: OLAPTotalEstimateValueList.ColParamAreaTypeDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColParamAreaTypeDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Parametrization area type label in English
                /// </summary>
                public string ParamAreaTypeLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColParamAreaTypeLabelEn.Name,
                            defaultValue: OLAPTotalEstimateValueList.ColParamAreaTypeLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColParamAreaTypeLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Parametrization area type description in English
                /// </summary>
                public string ParamAreaTypeDescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColParamAreaTypeDescriptionEn.Name,
                            defaultValue: OLAPTotalEstimateValueList.ColParamAreaTypeDescriptionEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateValueList.ColParamAreaTypeDescriptionEn.Name,
                            val: value);
                    }
                }

                #endregion Values

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not OLAPTotalEstimateValue Type, return False
                    if (obj is not OLAPTotalEstimateValue)
                    {
                        return false;
                    }

                    return
                        (PhaseEstimateTypeId == ((OLAPTotalEstimateValue)obj).PhaseEstimateTypeId) &&
                        (EstimationPeriodId == ((OLAPTotalEstimateValue)obj).EstimationPeriodId) &&
                        (VariableId == ((OLAPTotalEstimateValue)obj).VariableId) &&
                        (EstimationCellId == ((OLAPTotalEstimateValue)obj).EstimationCellId) &&
                        (PanelRefYearSetGroupId == ((OLAPTotalEstimateValue)obj).PanelRefYearSetGroupId) &&
                        (EstimateConfId == ((OLAPTotalEstimateValue)obj).EstimateConfId) &&
                        (TotalEstimateConfId == ((OLAPTotalEstimateValue)obj).TotalEstimateConfId) &&
                        (ResultId == ((OLAPTotalEstimateValue)obj).ResultId);
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                       PhaseEstimateTypeId.GetHashCode() ^
                       EstimationPeriodId.GetHashCode() ^
                       VariableId.GetHashCode() ^
                       EstimationCellId.GetHashCode() ^
                       PanelRefYearSetGroupId.GetHashCode() ^
                       EstimateConfId.GetHashCode() ^
                       TotalEstimateConfId.GetHashCode() ^
                       ResultId.GetHashCode();
                }

                /// <summary>
                /// Text description of the object
                /// </summary>
                /// <returns>Text description of the object</returns>
                public override string ToString()
                {
                    return String.Concat(
                        $"{"ID"} = {Functions.PrepStringArg(arg: Id)}{Environment.NewLine}");
                }

                #endregion Methods

            }


            /// <summary>
            /// List of OLAP Cube Values - Total Estimate
            /// </summary>
            public class OLAPTotalEstimateValueList
                : ALinqView
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema =
                    NfiEstaSchema.Name;

                /// <summary>
                /// Table name
                /// </summary>
                public static readonly string Name =
                    "v_olap_total_estimate_value";

                /// <summary>
                /// Table alias
                /// </summary>
                public static readonly string Alias =
                    "v_olap_total_estimate_value";

                /// <summary>
                /// Description in national language
                /// </summary>
                public static readonly string CaptionCs =
                    "Seznam hodnot OLAP kostky s daty odhadů úhrnů";

                /// <summary>
                /// Description in English
                /// </summary>
                public static readonly string CaptionEn =
                    "List of OLAP cube values with total estimates data";

                /// <summary>
                /// Significance level for confidence intervals
                /// </summary>
                public const double Alfa = 0.05;

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
                {
                    { "id", new ColumnMetadata()
                    {
                        Name = "id",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = true,
                        NotNull = true,
                        DefaultValue =  String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = String.Empty,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ID",
                        HeaderTextEn = "ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 0
                    }
                    },
                    { "phase_estimate_type_id", new ColumnMetadata()
                    {
                        Name = "phase_estimate_type_id",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue = default(int).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PHASE_ESTIMATE_TYPE_ID",
                        HeaderTextEn = "PHASE_ESTIMATE_TYPE_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 1
                    }
                    },
                    { "estimation_period_id", new ColumnMetadata()
                    {
                        Name = "estimation_period_id",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATION_PERIOD_ID",
                        HeaderTextEn = "ESTIMATION_PERIOD_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 2
                    }
                    },
                    { "variable_id", new ColumnMetadata()
                    {
                        Name = "variable_id",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue =  default(int).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "VARIABLE_ID",
                        HeaderTextEn = "VARIABLE_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 3
                    }
                    },
                    { "estimation_cell_id", new ColumnMetadata()
                    {
                        Name = "estimation_cell_id",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue =  default(int).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATION_CELL_ID",
                        HeaderTextEn = "ESTIMATION_CELL_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 4
                    }
                    },
                    { "panel_refyearset_group_id", new ColumnMetadata()
                    {
                        Name = "panel_refyearset_group_id",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PANEL_REFYEARSET_GROUP_ID",
                        HeaderTextEn = "PANEL_REFYEARSET_GROUP_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 5
                    }
                    },
                    { "panel_refyearset_group_label_cs", new ColumnMetadata()
                    {
                        Name = "panel_refyearset_group_label_cs",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  String.Empty,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PANEL_REFYEARSET_GROUP_LABEL_CS",
                        HeaderTextEn = "PANEL_REFYEARSET_GROUP_LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 6
                    }
                    },
                    { "panel_refyearset_group_label_en", new ColumnMetadata()
                    {
                        Name = "panel_refyearset_group_label_en",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  String.Empty,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PANEL_REFYEARSET_GROUP_LABEL_EN",
                        HeaderTextEn = "PANEL_REFYEARSET_GROUP_LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 7
                    }
                    },
                    { "estimate_conf_id", new ColumnMetadata()
                    {
                        Name = "estimate_conf_id",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue =  default(int).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATE_CONF_ID",
                        HeaderTextEn = "ESTIMATE_CONF_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 8
                    }
                    },
                    { "estimate_type_id", new ColumnMetadata()
                    {
                        Name = "estimate_type_id",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue =  default(int).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATE_TYPE_ID",
                        HeaderTextEn = "ESTIMATE_TYPE_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 9
                    }
                    },
                    { "total_estimate_conf_id", new ColumnMetadata()
                    {
                        Name = "total_estimate_conf_id",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue = default(int).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "TOTAL_ESTIMATE_CONF_ID",
                        HeaderTextEn = "TOTAL_ESTIMATE_CONF_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 10
                    }
                    },
                    { "estimate_executor", new ColumnMetadata()
                    {
                        Name = "estimate_executor",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATE_EXECUTOR",
                        HeaderTextEn = "ESTIMATE_EXECUTOR",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 11
                    }
                    },
                    { "total_estimate_conf_name", new ColumnMetadata()
                    {
                        Name = "total_estimate_conf_name",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  String.Empty,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "TOTAL_ESTIMATE_CONF_NAME",
                        HeaderTextEn = "TOTAL_ESTIMATE_CONF_NAME",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 12
                    }
                    },
                    { "result_id", new ColumnMetadata()
                    {
                        Name = "result_id",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "RESULT_ID",
                        HeaderTextEn = "RESULT_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 13
                    }
                    },
                    { "point_estimate", new ColumnMetadata()
                    {
                        Name = "point_estimate",
                        DbName = null,
                        DataType = "System.Double",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  "N3",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "POINT_ESTIMATE",
                        HeaderTextEn = "POINT_ESTIMATE",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 14
                    }
                    },
                    { "variance_estimate", new ColumnMetadata()
                    {
                        Name = "variance_estimate",
                        DbName = null,
                        DataType = "System.Double",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  "N3",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "VARIANCE_ESTIMATE",
                        HeaderTextEn = "VARIANCE_ESTIMATE",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 15
                    }
                    },
                    { "calc_started", new ColumnMetadata()
                    {
                        Name = "calc_started",
                        DbName = null,
                        DataType = "System.DateTime",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "yyyy-MM-dd HH:mm:ss",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "CALC_STARTED",
                        HeaderTextEn = "CALC_STARTED",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 16
                    }
                    },
                    { "extension_version", new ColumnMetadata()
                    {
                        Name = "extension_version",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "EXTENSION_VERSION",
                        HeaderTextEn = "EXTENSION_VERSION",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 17
                    }
                    },
                    { "calc_duration", new ColumnMetadata()
                    {
                        Name = "calc_duration",
                        DbName = null,
                        DataType = "System.TimeSpan",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "g",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "CALC_DURATION",
                        HeaderTextEn = "CALC_DURATION",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 18
                    }
                    },
                    { "min_ssize", new ColumnMetadata()
                    {
                        Name = "min_ssize",
                        DbName = null,
                        DataType = "System.Int64",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "MIN_SSIZE",
                        HeaderTextEn = "MIN_SSIZE",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 19
                    }
                    },
                    { "act_ssize", new ColumnMetadata()
                    {
                        Name = "act_ssize",
                        DbName = null,
                        DataType = "System.Int64",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ACT_SSIZE",
                        HeaderTextEn = "ACT_SSIZE",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 20
                    }
                    },
                    { "is_latest", new ColumnMetadata()
                    {
                        Name = "is_latest",
                        DbName = null,
                        DataType = "System.Boolean",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "IS_LATEST",
                        HeaderTextEn = "IS_LATEST",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 21
                    }
                    },
                    { "result_executor", new ColumnMetadata()
                    {
                        Name = "result_executor",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "RESULT_EXECUTOR",
                        HeaderTextEn = "RESULT_EXECUTOR",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 22
                    }
                    },
                    { "standard_error", new ColumnMetadata()
                    {
                        Name = "standard_error",
                        DbName = null,
                        DataType = "System.Double",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  "N3",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "STANDARD_ERROR",
                        HeaderTextEn = "STANDARD_ERROR",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 23
                    }
                    },
                    { "confidence_interval", new ColumnMetadata()
                    {
                        Name = "confidence_interval",
                        DbName = null,
                        DataType = "System.Double",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  "N3",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "CONFIDENCE_INTERVAL",
                        HeaderTextEn = "CONFIDENCE_INTERVAL",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 24
                    }
                    },
                    { "lower_bound", new ColumnMetadata()
                    {
                        Name = "lower_bound",
                        DbName = null,
                        DataType = "System.Double",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  "N3",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "LOWER_BOUND",
                        HeaderTextEn = "LOWER_BOUND",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 25
                    }
                    },
                    { "upper_bound", new ColumnMetadata()
                    {
                        Name = "upper_bound",
                        DbName = null,
                        DataType = "System.Double",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  "N3",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "UPPER_BOUND",
                        HeaderTextEn = "UPPER_BOUND",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 26
                    }
                    },
                    { "attr_diff_down", new ColumnMetadata()
                    {
                        Name = "attr_diff_down",
                        DbName = null,
                        DataType = "System.Double",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  "N10",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ATTR_DIFF_DOWN",
                        HeaderTextEn = "ATTR_DIFF_DOWN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 27
                    }
                    },
                    { "attr_diff_up", new ColumnMetadata()
                    {
                        Name = "attr_diff_up",
                        DbName = null,
                        DataType = "System.Double",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  "N10",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ATTR_DIFF_UP",
                        HeaderTextEn = "ATTR_DIFF_UP",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 28
                    }
                    },
                    { "geo_diff_down", new ColumnMetadata()
                    {
                        Name = "geo_diff_down",
                        DbName = null,
                        DataType = "System.Double",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  "N10",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "GEO_DIFF_DOWN",
                        HeaderTextEn = "GEO_DIFF_DOWN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 29
                    }
                    },
                    { "geo_diff_up", new ColumnMetadata()
                    {
                        Name = "geo_diff_up",
                        DbName = null,
                        DataType = "System.Double",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  "N10",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "GEO_DIFF_UP",
                        HeaderTextEn = "GEO_DIFF_UP",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 30
                    }
                    },
                    { "model_id", new ColumnMetadata()
                    {
                        Name = "model_id",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "MODEL_ID",
                        HeaderTextEn = "MODEL_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 31
                    }
                    },
                    { "model_label_cs", new ColumnMetadata()
                    {
                        Name = "model_label_cs",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "MODEL_LABEL_CS",
                        HeaderTextEn = "MODEL_LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 32
                    }
                    },
                    { "model_description_cs", new ColumnMetadata()
                    {
                        Name = "model_description_cs",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "MODEL_DESCRIPTION_CS",
                        HeaderTextEn = "MODEL_DESCRIPTION_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 33
                    }
                    },
                    { "model_label_en", new ColumnMetadata()
                    {
                        Name = "model_label_en",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "MODEL_LABEL_EN",
                        HeaderTextEn = "MODEL_LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 34
                    }
                    },
                    { "model_description_en", new ColumnMetadata()
                    {
                        Name = "model_description_en",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "MODEL_DESCRIPTION_EN",
                        HeaderTextEn = "MODEL_DESCRIPTION_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 35
                    }
                    },
                    { "sigma", new ColumnMetadata()
                    {
                        Name = "sigma",
                        DbName = null,
                        DataType = "System.Boolean",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "SIGMA",
                        HeaderTextEn = "SIGMA",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 36
                    }
                    },
                    { "param_area_id", new ColumnMetadata()
                    {
                        Name = "param_area_id",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PARAM_AREA_ID",
                        HeaderTextEn = "PARAM_AREA_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 37
                    }
                    },
                    { "param_area_label_cs", new ColumnMetadata()
                    {
                        Name = "param_area_label_cs",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PARAM_AREA_LABEL_CS",
                        HeaderTextEn = "PARAM_AREA_LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 38
                    }
                    },
                    { "param_area_description_cs", new ColumnMetadata()
                    {
                        Name = "param_area_description_cs",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PARAM_AREA_DESCRIPTION_CS",
                        HeaderTextEn = "PARAM_AREA_DESCRIPTION_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 39
                    }
                    },
                    { "param_area_label_en", new ColumnMetadata()
                    {
                        Name = "param_area_label_en",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PARAM_AREA_LABEL_EN",
                        HeaderTextEn = "PARAM_AREA_LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 40
                    }
                    },
                    { "param_area_description_en", new ColumnMetadata()
                    {
                        Name = "param_area_description_en",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PARAM_AREA_DESCRIPTION_EN",
                        HeaderTextEn = "PARAM_AREA_DESCRIPTION_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 41
                    }
                    },
                    { "param_area_type_id", new ColumnMetadata()
                    {
                        Name = "param_area_type_id",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PARAM_AREA_TYPE_ID",
                        HeaderTextEn = "PARAM_AREA_TYPE_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 42
                    }
                    },
                    { "param_area_type_label_cs", new ColumnMetadata()
                    {
                        Name = "param_area_type_label_cs",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PARAM_AREA_TYPE_LABEL_CS",
                        HeaderTextEn = "PARAM_AREA_TYPE_LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 43
                    }
                    },
                    { "param_area_type_description_cs", new ColumnMetadata()
                    {
                        Name = "param_area_type_description_cs",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PARAM_AREA_TYPE_DESCRIPTION_CS",
                        HeaderTextEn = "PARAM_AREA_TYPE_DESCRIPTION_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 44
                    }
                    },
                    { "param_area_type_label_en", new ColumnMetadata()
                    {
                        Name = "param_area_type_label_en",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PARAM_AREA_TYPE_LABEL_EN",
                        HeaderTextEn = "PARAM_AREA_TYPE_LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 45
                    }
                    },
                    { "param_area_type_description_en", new ColumnMetadata()
                    {
                        Name = "param_area_type_description_en",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PARAM_AREA_TYPE_DESCRIPTION_EN",
                        HeaderTextEn = "PARAM_AREA_TYPE_DESCRIPTION_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 46
                    }
                    }
                };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column phase_estimate_type_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColPhaseEstimateTypeId = Cols["phase_estimate_type_id"];

                /// <summary>
                /// Column estimation_period_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationPeriodId = Cols["estimation_period_id"];

                /// <summary>
                /// Column variable_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColVariableId = Cols["variable_id"];

                /// <summary>
                /// Column estimation_cell_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationCellId = Cols["estimation_cell_id"];

                /// <summary>
                /// Column panel_refyearset_group_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColPanelRefYearSetGroupId = Cols["panel_refyearset_group_id"];

                /// <summary>
                /// Column panel_refyearset_group_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColPanelRefYearSetGroupLabelCs = Cols["panel_refyearset_group_label_cs"];

                /// <summary>
                /// Column panel_refyearset_group_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColPanelRefYearSetGroupLabelEn = Cols["panel_refyearset_group_label_en"];

                /// <summary>
                /// Column estimate_conf_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimateConfId = Cols["estimate_conf_id"];

                /// <summary>
                /// Column estimate_type_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimateTypeId = Cols["estimate_type_id"];

                /// <summary>
                /// Column total_estimate_conf_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColTotalEstimateConfId = Cols["total_estimate_conf_id"];

                /// <summary>
                /// Column estimate_executor metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimateExecutor = Cols["estimate_executor"];

                /// <summary>
                /// Column total_estimate_conf_name metadata
                /// </summary>
                public static readonly ColumnMetadata ColTotalEstimateConfName = Cols["total_estimate_conf_name"];

                /// <summary>
                /// Column result_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColResultId = Cols["result_id"];

                /// <summary>
                /// Column point_estimate metadata
                /// </summary>
                public static readonly ColumnMetadata ColPointEstimate = Cols["point_estimate"];

                /// <summary>
                /// Column variance_estimate metadata
                /// </summary>
                public static readonly ColumnMetadata ColVarianceEstimate = Cols["variance_estimate"];

                /// <summary>
                /// Column calc_started metadata
                /// </summary>
                public static readonly ColumnMetadata ColCalcStarted = Cols["calc_started"];

                /// <summary>
                /// Column extension_version metadata
                /// </summary>
                public static readonly ColumnMetadata ColExtensionVersion = Cols["extension_version"];

                /// <summary>
                /// Column calc_duration metadata
                /// </summary>
                public static readonly ColumnMetadata ColCalcDuration = Cols["calc_duration"];

                /// <summary>
                /// Column min_ssize metadata
                /// </summary>
                public static readonly ColumnMetadata ColMinSSize = Cols["min_ssize"];

                /// <summary>
                /// Column act_ssize metadata
                /// </summary>
                public static readonly ColumnMetadata ColActSSize = Cols["act_ssize"];

                /// <summary>
                /// Column is_latest metadata
                /// </summary>
                public static readonly ColumnMetadata ColIsLatest = Cols["is_latest"];

                /// <summary>
                /// Column result_executor metadata
                /// </summary>
                public static readonly ColumnMetadata ColResultExecutor = Cols["result_executor"];

                /// <summary>
                /// Column standard_error metadata
                /// </summary>
                public static readonly ColumnMetadata ColStandardError = Cols["standard_error"];

                /// <summary>
                /// Column confidence_interval metadata
                /// </summary>
                public static readonly ColumnMetadata ColConfidenceInterval = Cols["confidence_interval"];

                /// <summary>
                /// Column lower_bound metadata
                /// </summary>
                public static readonly ColumnMetadata ColLowerBound = Cols["lower_bound"];

                /// <summary>
                /// Column upper_bound metadata
                /// </summary>
                public static readonly ColumnMetadata ColUpperBound = Cols["upper_bound"];

                /// <summary>
                /// Column attr_diff_down metadata
                /// </summary>
                public static readonly ColumnMetadata ColAttrDiffDown = Cols["attr_diff_down"];

                /// <summary>
                /// Column attr_diff_up metadata
                /// </summary>
                public static readonly ColumnMetadata ColAttrDiffUp = Cols["attr_diff_up"];

                /// <summary>
                /// Column geo_diff_down metadata
                /// </summary>
                public static readonly ColumnMetadata ColGeoDiffDown = Cols["geo_diff_down"];

                /// <summary>
                /// Column geo_diff_up metadata
                /// </summary>
                public static readonly ColumnMetadata ColGeoDiffUp = Cols["geo_diff_up"];

                /// <summary>
                /// Column model_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColModelId = Cols["model_id"];

                /// <summary>
                /// Column model_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColModelLabelCs = Cols["model_label_cs"];

                /// <summary>
                /// Column model_description_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColModelDescriptionCs = Cols["model_description_cs"];

                /// <summary>
                /// Column model_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColModelLabelEn = Cols["model_label_en"];

                /// <summary>
                /// Column model_description_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColModelDescriptionEn = Cols["model_description_en"];

                /// <summary>
                /// Column sigma metadata
                /// </summary>
                public static readonly ColumnMetadata ColSigma = Cols["sigma"];

                /// <summary>
                /// Column param_area_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColParamAreaId = Cols["param_area_id"];

                /// <summary>
                /// Column param_area_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColParamAreaLabelCs = Cols["param_area_label_cs"];

                /// <summary>
                /// Column param_area_description_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColParamAreaDescriptionCs = Cols["param_area_description_cs"];

                /// <summary>
                /// Column param_area_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColParamAreaLabelEn = Cols["param_area_label_en"];

                /// <summary>
                /// Column param_area_description_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColParamAreaDescriptionEn = Cols["param_area_description_en"];

                /// <summary>
                /// Column param_area_type_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColParamAreaTypeId = Cols["param_area_type_id"];

                /// <summary>
                /// Column param_area_type_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColParamAreaTypeLabelCs = Cols["param_area_type_label_cs"];

                /// <summary>
                /// Column param_area_type_description_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColParamAreaTypeDescriptionCs = Cols["param_area_type_description_cs"];

                /// <summary>
                /// Column param_area_type_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColParamAreaTypeLabelEn = Cols["param_area_type_label_en"];

                /// <summary>
                /// Column param_area_type_description_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColParamAreaTypeDescriptionEn = Cols["param_area_type_description_en"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public OLAPTotalEstimateValueList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public OLAPTotalEstimateValueList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// List of OLAP Cube Values - Total Estimate (read-only)
                /// </summary>
                public List<OLAPTotalEstimateValue> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new OLAPTotalEstimateValue(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// OLAP Cube Value - Total Estimate from list by identifier (read-only)
                /// </summary>
                /// <param name="id"> OLAP Cube Dimension - Total Estimate - composite identifier</param>
                /// <returns> OLAP Cube Value - Total Estimate from list by identifier (null if not found)</returns>
                public OLAPTotalEstimateValue this[string id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<string>(ColId.Name) == id)
                                .Select(a => new OLAPTotalEstimateValue(composite: this, data: a))
                                .FirstOrDefault<OLAPTotalEstimateValue>();
                    }
                }

                #endregion Indexer


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public OLAPTotalEstimateValueList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new OLAPTotalEstimateValueList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new OLAPTotalEstimateValueList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                /// <summary>
                /// Loads data into OLAP Cube Values - Total Estimate (if it was not done before)
                /// </summary>
                /// <param name="cPhaseEstimateType">List of phase estimate types</param>
                /// <param name="cEstimationPeriod">List of estimation periods</param>
                /// <param name="variables">List of attribute categories</param>
                /// <param name="cEstimationCell">List of estimation cells</param>
                /// <param name="cPanelRefYearSetGroup">List of aggregated sets of panels and corresponding reference year sets</param>
                /// <param name="tEstimateConf">List of estimate configurations</param>
                /// <param name="tTotalEstimateConf">List of total estimate configurations</param>
                /// <param name="tResult">List of final estimates</param>
                /// <param name="fnAddResTotalAttr">List of total estimates attribute additivity</param>
                /// <param name="fnAddResTotalGeo">List of total estimates geographical additivity</param>
                /// <param name="additivityLimit">Additivity limit</param>
                /// <param name="isLatest">Only latest estimates</param>
                /// <param name="condition">Condition for selection data</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="tAuxConf">List of parametrization configurations</param>
                /// <param name="tModel">List of models</param>
                /// <param name="faParamArea">List of parametrization areas</param>
                /// <param name="cParamAreaType">List of parametrization area types</param>
                public void Load(
                    PhaseEstimateTypeList cPhaseEstimateType,
                    EstimationPeriodList cEstimationPeriod,
                    List<VwVariable> variables,
                    EstimationCellList cEstimationCell,
                    PanelRefYearSetGroupList cPanelRefYearSetGroup,
                    EstimateConfList tEstimateConf,
                    TotalEstimateConfList tTotalEstimateConf,
                    ResultList tResult,
                    TFnAddResTotalAttrList fnAddResTotalAttr,
                    TFnAddResTotalGeoList fnAddResTotalGeo,
                    double additivityLimit,
                    bool isLatest,
                    string condition = null,
                    Nullable<int> limit = null,
                    AuxConfList tAuxConf = null,
                    ModelList tModel = null,
                    ParamAreaList faParamArea = null,
                    ParamAreaTypeList cParamAreaType = null)
                {
                    if (!Loaded)
                    {
                        ReLoad(
                            cPhaseEstimateType: cPhaseEstimateType,
                            cEstimationPeriod: cEstimationPeriod,
                            variables: variables,
                            cEstimationCell: cEstimationCell,
                            cPanelRefYearSetGroup: cPanelRefYearSetGroup,
                            tEstimateConf: tEstimateConf,
                            tTotalEstimateConf: tTotalEstimateConf,
                            tResult: tResult,
                            fnAddResTotalAttr: fnAddResTotalAttr,
                            fnAddResTotalGeo: fnAddResTotalGeo,
                            additivityLimit: additivityLimit,
                            isLatest: isLatest,
                            condition: condition,
                            limit: limit,
                            tAuxConf: tAuxConf,
                            tModel: tModel,
                            faParamArea: faParamArea,
                            cParamAreaType: cParamAreaType);
                    }
                }

                /// <summary>
                /// Loads data into OLAP Cube Values - Total Estimate
                /// </summary>
                /// <param name="cPhaseEstimateType">List of phase estimate types</param>
                /// <param name="cEstimationPeriod">List of estimation periods</param>
                /// <param name="variables">List of attribute categories</param>
                /// <param name="cEstimationCell">List of estimation cells</param>
                /// <param name="cPanelRefYearSetGroup">List of aggregated sets of panels and corresponding reference year sets</param>
                /// <param name="tEstimateConf">List of estimate configurations</param>
                /// <param name="tTotalEstimateConf">List of total estimate configurations</param>
                /// <param name="tResult">List of final estimates</param>
                /// <param name="fnAddResTotalAttr">List of total estimates attribute additivity</param>
                /// <param name="fnAddResTotalGeo">List of total estimates geographical additivity</param>
                /// <param name="additivityLimit">Additivity limit</param>
                /// <param name="isLatest">Only latest estimates</param>
                /// <param name="condition">Condition for selection data</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="tAuxConf">List of parametrization configurations</param>
                /// <param name="tModel">List of models</param>
                /// <param name="faParamArea">List of parametrization areas</param>
                /// <param name="cParamAreaType">List of parametrization area types</param>
                public void ReLoad(
                    PhaseEstimateTypeList cPhaseEstimateType,
                    EstimationPeriodList cEstimationPeriod,
                    List<VwVariable> variables,
                    EstimationCellList cEstimationCell,
                    PanelRefYearSetGroupList cPanelRefYearSetGroup,
                    EstimateConfList tEstimateConf,
                    TotalEstimateConfList tTotalEstimateConf,
                    ResultList tResult,
                    TFnAddResTotalAttrList fnAddResTotalAttr,
                    TFnAddResTotalGeoList fnAddResTotalGeo,
                    double additivityLimit,
                    bool isLatest,
                    string condition = null,
                    Nullable<int> limit = null,
                    AuxConfList tAuxConf = null,
                    ModelList tModel = null,
                    ParamAreaList faParamArea = null,
                    ParamAreaTypeList cParamAreaType = null)
                {
                    Data = EmptyDataTable(
                                tableName: TableName,
                                columns: Columns);
                    Condition = condition;
                    Limit = limit;

                    System.Diagnostics.Stopwatch stopWatch = new();
                    stopWatch.Start();


                    #region ctePhaseEstimateType: Fáze odhadu (1.dimenze)
                    var ctePhaseEstimateType =
                        cPhaseEstimateType.Data.AsEnumerable()
                        .Select(a => new
                        {
                            PhaseEstimateTypeId = a.Field<int>(columnName: PhaseEstimateTypeList.ColId.Name),
                            PhaseEstimateTypeLabelCs = a.Field<string>(columnName: PhaseEstimateTypeList.ColLabelCs.Name),
                            PhaseEstimateTypeLabelEn = a.Field<string>(columnName: PhaseEstimateTypeList.ColLabelEn.Name),
                            PhaseEstimateTypeDescriptionCs = a.Field<string>(columnName: PhaseEstimateTypeList.ColDescriptionCs.Name),
                            PhaseEstimateTypeDescriptionEn = a.Field<string>(columnName: PhaseEstimateTypeList.ColDescriptionEn.Name)
                        });
                    #endregion ctePhaseEstimateType: Fáze odhadu (1.dimenze)

                    #region cteEstimationPeriod: Období odhadu (2.dimenze)
                    var cteEstimationPeriod =
                        cEstimationPeriod.Data.AsEnumerable()
                        .Select(a => new
                        {
                            EstimationPeriodId = a.Field<int>(columnName: EstimationPeriodList.ColId.Name),
                            EstimateDateBegin = a.Field<DateTime>(columnName: EstimationPeriodList.ColEstimateDateBegin.Name),
                            EstimateDateEnd = a.Field<DateTime>(columnName: EstimationPeriodList.ColEstimateDateEnd.Name),
                            EstimationPeriodLabelCs = a.Field<string>(columnName: EstimationPeriodList.ColLabelCs.Name),
                            EstimationPeriodLabelEn = a.Field<string>(columnName: EstimationPeriodList.ColLabelEn.Name),
                            EstimationPeriodDescriptionCs = a.Field<string>(columnName: EstimationPeriodList.ColDescriptionCs.Name),
                            EstimationPeriodDescriptionEn = a.Field<string>(columnName: EstimationPeriodList.ColDescriptionEn.Name)
                        });
                    #endregion cteEstimationPeriod: Období odhadu (2.dimenze)

                    #region cteAttributeCategory: Atributové kategorie (3.dimenze)
                    var cteAttributeCategory =
                        variables
                        .Select(a => new
                        {
                            a.VariableId,
                            a.VariableLabelCs,
                            a.VariableLabelEn
                        });
                    #endregion cteAttributeCategory: Atributové kategorie (3.dimenze)

                    #region cteEstimationCell: Výpočetní buňky (4.dimenze)
                    var cteEstimationCell =
                        cEstimationCell.Data.AsEnumerable()
                        .Select(a => new
                        {
                            EstimationCellId = a.Field<int>(columnName: EstimationCellList.ColId.Name),
                            EstimationCellCollectionId = a.Field<int>(columnName: EstimationCellList.ColEstimationCellCollectionId.Name),
                            EstimationCellLabelCs = a.Field<string>(columnName: EstimationCellList.ColLabelCs.Name),
                            EstimationCellLabelEn = a.Field<string>(columnName: EstimationCellList.ColLabelEn.Name),
                            EstimationCellDescriptionCs = a.Field<string>(columnName: EstimationCellList.ColDescriptionCs.Name),
                            EstimationCellDescriptionEn = a.Field<string>(columnName: EstimationCellList.ColDescriptionEn.Name)
                        });
                    #endregion cteEstimationCell: Výpočetní buňky (4.dimenze)


                    #region ctePanelRefYearSetGroup: Skupiny panelů pro období odhadu
                    var ctePanelRefYearSetGroup =
                        cPanelRefYearSetGroup.Data.AsEnumerable()
                        .Select(a => new
                        {
                            PanelRefYearSetGroupId = a.Field<int>(columnName: PanelRefYearSetGroupList.ColId.Name),
                            PanelRefYearSetGroupLabelCs = a.Field<string>(columnName: PanelRefYearSetGroupList.ColLabelCs.Name),
                            PanelRefYearSetGroupLabelEn = a.Field<string>(columnName: PanelRefYearSetGroupList.ColLabelEn.Name),
                            PanelRefYearSetGroupDescriptionCs = a.Field<string>(columnName: PanelRefYearSetGroupList.ColDescriptionCs.Name),
                            PanelRefYearSetGroupDescriptionEn = a.Field<string>(columnName: PanelRefYearSetGroupList.ColDescriptionEn.Name)
                        });
                    #endregion ctePanelRefYearSetGroup: Skupiny panelů pro období odhadu

                    #region cteEstimateConf: Konfigurace odhadů (estimate_type_id = 1)
                    var cteEstimateConf =
                        tEstimateConf.Data.AsEnumerable()
                        .Where(a => a.Field<int>(columnName: EstimateConfList.ColEstimateTypeId.Name) == 1)
                        .Select(a => new
                        {
                            EstimateConfId = a.Field<int>(columnName: EstimateConfList.ColId.Name),
                            EstimateTypeId = a.Field<int>(columnName: EstimateConfList.ColEstimateTypeId.Name),
                            NumeratorTotalEstimateConfId = a.Field<int>(columnName: EstimateConfList.ColTotalEstimateConfId.Name),
                            DenominatorTotalEstimateConfId = a.Field<Nullable<int>>(columnName: EstimateConfList.ColDenominatorId.Name),
                            EstimateExecutor = a.Field<string>(columnName: EstimateConfList.ColExecutor.Name)
                        });
                    #endregion cteEstimateConf: Konfigurace odhadů (estimate_type_id = 1)

                    #region cteTotalEstimateConf: Konfigurace odhadů úhrnu (omezené na zvolené dimenze)
                    var cteTotalEstimateConf =
                        tTotalEstimateConf.Data.AsEnumerable()
                        // Omezení první dimenzí (fáze odhadu)
                        .Where(a => ctePhaseEstimateType.Select(f => f.PhaseEstimateTypeId)
                                    .Contains(value: a.Field<int>(columnName: TotalEstimateConfList.ColPhaseEstimateTypeId.Name)))
                        // Omezení druhou dimenzí (období odhadu)
                        .Where(a => cteEstimationPeriod.Select(p => p.EstimationPeriodId)
                                    .Contains(value: (int)(a.Field<Nullable<int>>(columnName: TotalEstimateConfList.ColEstimationPeriodId.Name) ?? 0)))
                        // Omezení třetí dimenzí (atributové kategorie)
                        .Where(a => cteAttributeCategory.Select(v => v.VariableId)
                                    .Contains(value: a.Field<int>(columnName: TotalEstimateConfList.ColVariableId.Name)))
                        // Omezení čtvrtou dimenzí (výpočetní buňky)
                        .Where(a => cteEstimationCell.Select(c => c.EstimationCellId)
                                    .Contains(value: a.Field<int>(columnName: TotalEstimateConfList.ColEstimationCellId.Name)))
                        .Select(a => new
                        {
                            TotalEstimateConfId = a.Field<int>(columnName: TotalEstimateConfList.ColId.Name),
                            EstimationCellId = a.Field<int>(columnName: TotalEstimateConfList.ColEstimationCellId.Name),
                            TotalEstimateConfName = a.Field<string>(columnName: TotalEstimateConfList.ColName.Name),
                            VariableId = a.Field<int>(columnName: TotalEstimateConfList.ColVariableId.Name),
                            PhaseEstimateTypeId = a.Field<int>(columnName: TotalEstimateConfList.ColPhaseEstimateTypeId.Name),
                            ForceSynthetic = a.Field<Nullable<bool>>(columnName: TotalEstimateConfList.ColForceSynthetic.Name),
                            AuxConfId = a.Field<Nullable<int>>(columnName: TotalEstimateConfList.ColAuxConfId.Name),
                            EstimationPeriodId = a.Field<Nullable<int>>(columnName: TotalEstimateConfList.ColEstimationPeriodId.Name),
                            PanelRefYearSetGroupId = a.Field<Nullable<int>>(columnName: TotalEstimateConfList.ColPanelRefYearSetGroupId.Name)
                        });
                    #endregion cteTotalEstimateConf: Konfigurace odhadů úhrnu (omezené na zvolené dimenze)

                    #region cteResult: Výsledky odhadu úhrnu
                    var cteResult = isLatest ?
                        // Výsledky odhadu úhrnu (pouze aktuální výsledky)
                        tResult.Data.AsEnumerable()
                        .Where(a => a.Field<bool>(ResultList.ColIsLatest.Name))
                        .Select(a => new
                        {
                            ResultId = a.Field<int>(columnName: ResultList.ColId.Name),
                            EstimateConfId = a.Field<Nullable<int>>(columnName: ResultList.ColEstimateConfId.Name),
                            PointEstimate = a.Field<Nullable<double>>(columnName: ResultList.ColPointEstimate.Name),
                            VarEstimate = a.Field<Nullable<double>>(columnName: ResultList.ColVarianceEstimate.Name),
                            CalcStarted = a.Field<Nullable<DateTime>>(columnName: ResultList.ColCalcStarted.Name),
                            ExtensionVersion = a.Field<string>(columnName: ResultList.ColExtensionVersion.Name),
                            CalcDuration = a.Field<Nullable<TimeSpan>>(columnName: ResultList.ColCalcDuration.Name),
                            SamplingUnits = a.Field<string>(columnName: ResultList.ColSamplingUnits.Name),
                            MinSSize = a.Field<Nullable<double>>(columnName: ResultList.ColMinSSize.Name),
                            ActSSize = a.Field<Nullable<long>>(columnName: ResultList.ColActSSize.Name),
                            IsLatest = a.Field<bool>(columnName: ResultList.ColIsLatest.Name),
                            ResultExecutor = a.Field<string>(columnName: ResultList.ColExecutor.Name)
                        }) :
                        // Výsledky odhadu úhrnu (všechny včetně historie)
                        tResult.Data.AsEnumerable()
                        .Select(a => new
                        {
                            ResultId = a.Field<int>(columnName: ResultList.ColId.Name),
                            EstimateConfId = a.Field<Nullable<int>>(columnName: ResultList.ColEstimateConfId.Name),
                            PointEstimate = a.Field<Nullable<double>>(columnName: ResultList.ColPointEstimate.Name),
                            VarEstimate = a.Field<Nullable<double>>(columnName: ResultList.ColVarianceEstimate.Name),
                            CalcStarted = a.Field<Nullable<DateTime>>(columnName: ResultList.ColCalcStarted.Name),
                            ExtensionVersion = a.Field<string>(columnName: ResultList.ColExtensionVersion.Name),
                            CalcDuration = a.Field<Nullable<TimeSpan>>(columnName: ResultList.ColCalcDuration.Name),
                            SamplingUnits = a.Field<string>(columnName: ResultList.ColSamplingUnits.Name),
                            MinSSize = a.Field<Nullable<double>>(columnName: ResultList.ColMinSSize.Name),
                            ActSSize = a.Field<Nullable<long>>(columnName: ResultList.ColActSSize.Name),
                            IsLatest = a.Field<bool>(columnName: ResultList.ColIsLatest.Name),
                            ResultExecutor = a.Field<string>(columnName: ResultList.ColExecutor.Name)
                        });
                    #endregion cteResult: Výsledky odhadu úhrnu

                    #region cteResultStandardError: Výsledky odhadu úhrnu doplněné o směrodatnou chybu
                    var cteResultStandardError =
                        cteResult
                        .Select(a => new
                        {
                            a.ResultId,
                            a.EstimateConfId,
                            a.PointEstimate,
                            a.VarEstimate,
                            a.CalcStarted,
                            a.ExtensionVersion,
                            a.CalcDuration,
                            a.SamplingUnits,
                            MinSSize =
                                (a.MinSSize != null) ?
                                    (Nullable<long>)Math.Ceiling((decimal)a.MinSSize) :
                                    (Nullable<long>)null,
                            a.ActSSize,
                            a.IsLatest,
                            a.ResultExecutor,
                            StandardError = (Nullable<double>)(
                                (a.VarEstimate != null) ?
                                    (a.VarEstimate >= 0) ?
                                        Math.Sqrt((double)a.VarEstimate) :
                                    (Nullable<double>)null :
                                (Nullable<double>)null)
                        });
                    #endregion cteResultStandardError: Výsledky odhadu úhrnu doplněné o směrodatnou chybu

                    #region cteResultStudent: Výsledky odhadu úhrnu doplněné o kvantil Studentova t-rozdělení
                    var cteResultStudent =
                        cteResultStandardError
                        .Select(a => new
                        {
                            a.ResultId,
                            a.EstimateConfId,
                            a.PointEstimate,
                            a.VarEstimate,
                            a.CalcStarted,
                            a.ExtensionVersion,
                            a.CalcDuration,
                            a.SamplingUnits,
                            a.MinSSize,
                            a.ActSSize,
                            a.IsLatest,
                            a.ResultExecutor,
                            a.StandardError,
                            Student =
                                ((a.StandardError != null) && (a.MinSSize != null) && (a.ActSSize != null)) ?
                                    ((a.ActSSize >= a.MinSSize) && (a.ActSSize > 1) && (a.MinSSize > 0)) ?
                                        new StudentT(
                                            location: 0,
                                            scale: 1,
                                            freedom: (double)(a.ActSSize - 1)) :
                                    (StudentT)null :
                                (StudentT)null
                        });
                    #endregion cteResultStudent: Výsledky odhadu úhrnu doplněné o kvantil Studentova t-rozdělení

                    #region cteResultConfidenceInterval: Výsledky odhadu úhrnu doplněné o interval spolehlivosti
                    var cteResultConfidenceInterval =
                        cteResultStudent
                        .Select(a => new
                        {
                            a.ResultId,
                            a.EstimateConfId,
                            a.PointEstimate,
                            a.VarEstimate,
                            a.CalcStarted,
                            a.ExtensionVersion,
                            a.CalcDuration,
                            a.SamplingUnits,
                            a.MinSSize,
                            a.ActSSize,
                            a.IsLatest,
                            a.ResultExecutor,
                            a.StandardError,
                            a.Student,
                            ConfidenceInterval =
                                (a.Student != null) ?
                                    a.Student.InverseCumulativeDistribution(p: 1.0 - (Alfa / 2.0)) * a.StandardError :
                                null
                        });
                    #endregion cteResultConfidenceInterval: Výsledky odhadu úhrnu doplněné o interval spolehlivosti

                    #region cteResultExtended: Výsledky odhadu úhrnu doplněné o horní a spodní hranici intervalu spolehlivosti
                    var cteResultExtended = cteResultConfidenceInterval
                        .Select(a => new
                        {
                            a.ResultId,
                            a.EstimateConfId,
                            a.PointEstimate,
                            a.VarEstimate,
                            a.CalcStarted,
                            a.ExtensionVersion,
                            a.CalcDuration,
                            a.SamplingUnits,
                            a.MinSSize,
                            a.ActSSize,
                            a.IsLatest,
                            a.ResultExecutor,
                            a.StandardError,
                            a.Student,
                            a.ConfidenceInterval,
                            LowerBound = ((a.PointEstimate != null) && (a.ConfidenceInterval != null)) ?
                                (a.PointEstimate - a.ConfidenceInterval) : null,
                            UpperBound = ((a.PointEstimate != null) && (a.ConfidenceInterval != null)) ?
                                (a.PointEstimate + a.ConfidenceInterval) : null
                        });
                    #endregion cteResultExtended: Výsledky odhadu úhrnu doplněné o horní a spodní hranici intervalu spolehlivosti

                    #region cteAddResTotalAttrGroups: Atributová aditivita
                    var cteAddResTotalAttr =
                        fnAddResTotalAttr.Data.AsEnumerable()
                        // Rozdíl je větší než je stanovený limit
                        .Where(a => (double)(a.Field<Nullable<double>>(columnName: TFnAddResTotalAttrList.ColDiff.Name) ?? 0.0) >= additivityLimit)
                        .Select(a => new
                        {
                            AddResTotalAttrId = a.Field<int>(columnName: TFnAddResTotalAttrList.ColId.Name),
                            EstimationCellId = a.Field<Nullable<int>>(columnName: TFnAddResTotalAttrList.ColEstimationCellId.Name),
                            AuxConfId = a.Field<Nullable<int>>(columnName: TFnAddResTotalAttrList.ColAuxConfId.Name),
                            ForceSynthetic = a.Field<Nullable<bool>>(columnName: TFnAddResTotalAttrList.ColForceSynthetic.Name),
                            EstimateConfId = a.Field<Nullable<int>>(columnName: TFnAddResTotalAttrList.ColEstimateConfId.Name),
                            VariableId = a.Field<Nullable<int>>(columnName: TFnAddResTotalAttrList.ColVariableId.Name),
                            PointEst = a.Field<Nullable<double>>(columnName: TFnAddResTotalAttrList.ColPointEst.Name),
                            PointEstSum = a.Field<Nullable<double>>(columnName: TFnAddResTotalAttrList.ColPointEstSum.Name),
                            Diff = a.Field<Nullable<double>>(columnName: TFnAddResTotalAttrList.ColDiff.Name),
                            VariablesDef = a.Field<string>(columnName: TFnAddResTotalAttrList.ColVariablesDef.Name),
                            VariablesFound = a.Field<string>(columnName: TFnAddResTotalAttrList.ColVariablesFound.Name),
                            EstimateConfsFound = a.Field<string>(columnName: TFnAddResTotalAttrList.ColEstimateConfsFound.Name),
                            EstimateConfsFoundList = Functions.StringToIntList(
                                text: a.Field<string>(columnName: TFnAddResTotalAttrList.ColEstimateConfsFound.Name),
                                separator: (char)59,
                                defaultValue: null)
                        });

                    var cteAddResTotalAttrGroups =
                        from a in cteAddResTotalAttr
                        group a by a.EstimateConfId into g
                        select new
                        {
                            EstimateConfId = g.Key,
                            Diff = g.Max(b => b.Diff)
                        };
                    #endregion cteAddResTotalAttrGroups: Atributová aditivita

                    #region cteAddResTotalGeoGroups: Geografická aditivita
                    var cteAddResTotalGeo =
                        fnAddResTotalGeo.Data.AsEnumerable()
                        // Rozdíl je větší než je stanovený limit
                        .Where(a => (double)(a.Field<Nullable<double>>(columnName: TFnAddResTotalGeoList.ColDiff.Name) ?? 0.0) >= additivityLimit)
                        .Select(a => new
                        {
                            AddResTotalGeoId = a.Field<int>(columnName: TFnAddResTotalGeoList.ColId.Name),
                            VariableId = a.Field<Nullable<int>>(columnName: TFnAddResTotalGeoList.ColVariableId.Name),
                            AuxConfId = a.Field<Nullable<int>>(columnName: TFnAddResTotalGeoList.ColAuxConfId.Name),
                            ForceSynthetic = a.Field<Nullable<bool>>(columnName: TFnAddResTotalGeoList.ColForceSynthetic.Name),
                            EstimateConfId = a.Field<Nullable<int>>(columnName: TFnAddResTotalGeoList.ColEstimateConfId.Name),
                            EstimationCellId = a.Field<Nullable<int>>(columnName: TFnAddResTotalGeoList.ColEstimationCellId.Name),
                            PointEst = a.Field<Nullable<double>>(columnName: TFnAddResTotalGeoList.ColPointEst.Name),
                            PointEstSum = a.Field<Nullable<double>>(columnName: TFnAddResTotalGeoList.ColPointEstSum.Name),
                            Diff = a.Field<Nullable<double>>(columnName: TFnAddResTotalGeoList.ColDiff.Name),
                            EstimationCellsDef = a.Field<string>(columnName: TFnAddResTotalGeoList.ColEstimationCellsDef.Name),
                            EstimationCellsFound = a.Field<string>(columnName: TFnAddResTotalGeoList.ColEstimationCellsFound.Name),
                            EstimateConfsFound = a.Field<string>(columnName: TFnAddResTotalGeoList.ColEstimateConfsFound.Name),
                            EstimateConfsFoundList = Functions.StringToIntList(
                                text: a.Field<string>(columnName: TFnAddResTotalGeoList.ColEstimateConfsFound.Name),
                                separator: (char)59,
                                defaultValue: null)
                        });

                    var cteAddResTotalGeoGroups =
                        from a in cteAddResTotalGeo
                        group a by a.EstimateConfId into g
                        select new
                        {
                            EstimateConfId = g.Key,
                            Diff = g.Max(b => b.Diff)
                        };
                    #endregion cteAddResTotalGeoGroups: Geografická aditivita


                    #region cteAuxConf
                    var cteAuxConf = (tAuxConf != null)
                        ? tAuxConf.Data.AsEnumerable()
                        .Select(a => new
                        {
                            AuxConfId = a.Field<Nullable<int>>(columnName: AuxConfList.ColId.Name),
                            ModelId = a.Field<Nullable<int>>(columnName: AuxConfList.ColModelId.Name),
                            ParamAreaId = a.Field<Nullable<int>>(columnName: AuxConfList.ColParamAreaId.Name),
                            Sigma = a.Field<Nullable<bool>>(columnName: AuxConfList.ColSigma.Name)
                        })
                        : new List<int>() { 1 }
                        .Select(a => new
                        {
                            AuxConfId = (Nullable<int>)null,
                            ModelId = (Nullable<int>)null,
                            ParamAreaId = (Nullable<int>)null,
                            Sigma = (Nullable<bool>)null
                        });
                    #endregion cteAuxConf

                    #region cteModel
                    var cteModel = (tModel != null)
                        ? tModel.Data.AsEnumerable()
                        .Select(a => new
                        {
                            ModelId = a.Field<Nullable<int>>(columnName: ModelList.ColId.Name),
                            ModelLabelCs = a.Field<string>(columnName: ModelList.ColLabelCs.Name),
                            ModelDescriptionCs = a.Field<string>(columnName: ModelList.ColDescriptionCs.Name),
                            ModelLabelEn = a.Field<string>(columnName: ModelList.ColLabelEn.Name),
                            ModelDescriptionEn = a.Field<string>(columnName: ModelList.ColDescriptionEn.Name),

                        })
                        : new List<int>() { 1 }
                        .Select(a => new
                        {
                            ModelId = (Nullable<int>)null,
                            ModelLabelCs = String.Empty,
                            ModelDescriptionCs = String.Empty,
                            ModelLabelEn = String.Empty,
                            ModelDescriptionEn = String.Empty
                        });
                    #endregion cteModel

                    #region cteParamArea
                    var cteParamArea = (faParamArea != null)
                        ? faParamArea.Data.AsEnumerable()
                        .Select(a => new
                        {
                            ParamAreaId = a.Field<Nullable<int>>(columnName: ParamAreaList.ColId.Name),
                            ParamAreaTypeId = a.Field<Nullable<int>>(columnName: ParamAreaList.ColParamAreaTypeId.Name),
                            ParamAreaLabelCs = a.Field<string>(columnName: ParamAreaList.ColLabelCs.Name),
                            ParamAreaDescriptionCs = a.Field<string>(columnName: ParamAreaList.ColDescriptionCs.Name),
                            ParamAreaLabelEn = a.Field<string>(columnName: ParamAreaList.ColLabelEn.Name),
                            ParamAreaDescriptionEn = a.Field<string>(columnName: ParamAreaList.ColDescriptionEn.Name),

                        })
                        : new List<int>() { 1 }
                        .Select(a => new
                        {
                            ParamAreaId = (Nullable<int>)null,
                            ParamAreaTypeId = (Nullable<int>)null,
                            ParamAreaLabelCs = String.Empty,
                            ParamAreaDescriptionCs = String.Empty,
                            ParamAreaLabelEn = String.Empty,
                            ParamAreaDescriptionEn = String.Empty
                        });
                    #endregion cteParamArea

                    #region cteParamAreaType
                    var cteParamAreaType = (cParamAreaType != null)
                        ? cParamAreaType.Data.AsEnumerable()
                        .Select(a => new
                        {
                            ParamAreaTypeId = a.Field<Nullable<int>>(columnName: ParamAreaTypeList.ColId.Name),
                            ParamAreaTypeLabelCs = a.Field<string>(columnName: ParamAreaTypeList.ColLabelCs.Name),
                            ParamAreaTypeDescriptionCs = a.Field<string>(columnName: ParamAreaTypeList.ColDescriptionCs.Name),
                            ParamAreaTypeLabelEn = a.Field<string>(columnName: ParamAreaTypeList.ColLabelEn.Name),
                            ParamAreaTypeDescriptionEn = a.Field<string>(columnName: ParamAreaTypeList.ColDescriptionEn.Name),

                        })
                        : new List<int>() { 1 }
                        .Select(a => new
                        {
                            ParamAreaTypeId = (Nullable<int>)null,
                            ParamAreaTypeLabelCs = String.Empty,
                            ParamAreaTypeDescriptionCs = String.Empty,
                            ParamAreaTypeLabelEn = String.Empty,
                            ParamAreaTypeDescriptionEn = String.Empty
                        });
                    #endregion cteParamAreaType

                    #region cteAuxConfModelJoin
                    var cteAuxConfModelJoin =
                        from a in cteAuxConf
                        join b in cteModel
                        on a.ModelId equals b.ModelId into lj
                        from b in lj.DefaultIfEmpty()
                        select new
                        {
                            a.AuxConfId,
                            a.ModelId,
                            a.ParamAreaId,
                            a.Sigma,
                            b?.ModelLabelCs,
                            b?.ModelDescriptionCs,
                            b?.ModelLabelEn,
                            b?.ModelDescriptionEn
                        };
                    #endregion cteAuxConfModelJoin

                    #region cteParamAreaJoin
                    var cteParamAreaJoin =
                        from a in cteParamArea
                        join b in cteParamAreaType
                        on a.ParamAreaTypeId equals b.ParamAreaTypeId into lj
                        from b in lj.DefaultIfEmpty()
                        select new
                        {
                            a.ParamAreaId,
                            a.ParamAreaTypeId,
                            a.ParamAreaLabelCs,
                            a.ParamAreaDescriptionCs,
                            a.ParamAreaLabelEn,
                            a.ParamAreaDescriptionEn,
                            b?.ParamAreaTypeLabelCs,
                            b?.ParamAreaTypeDescriptionCs,
                            b?.ParamAreaTypeLabelEn,
                            b?.ParamAreaTypeDescriptionEn
                        };
                    #endregion cteParamAreaJoin

                    #region cteAuxConfJoin
                    var cteAuxConfJoin =
                        from a in cteAuxConfModelJoin
                        join b in cteParamAreaJoin
                        on a.ParamAreaId equals b.ParamAreaId into lj
                        from b in lj.DefaultIfEmpty()
                        select new
                        {
                            a.AuxConfId,
                            a.ModelId,
                            a.ParamAreaId,
                            a.Sigma,
                            a.ModelLabelCs,
                            a.ModelDescriptionCs,
                            a.ModelLabelEn,
                            a.ModelDescriptionEn,
                            b?.ParamAreaTypeId,
                            b?.ParamAreaLabelCs,
                            b?.ParamAreaDescriptionCs,
                            b?.ParamAreaLabelEn,
                            b?.ParamAreaDescriptionEn,
                            b?.ParamAreaTypeLabelCs,
                            b?.ParamAreaTypeDescriptionCs,
                            b?.ParamAreaTypeLabelEn,
                            b?.ParamAreaTypeDescriptionEn
                        };
                    #endregion cteAuxConfJoin


                    #region cteTotalEstimateConfPanel: cteTotalEstimateConf + ctePanelRefYearSetGroup (LEFT JOIN)
                    var cteTotalEstimateConfPanel =
                        from a in cteTotalEstimateConf
                        join b in ctePanelRefYearSetGroup
                        on a.PanelRefYearSetGroupId equals b.PanelRefYearSetGroupId into lj
                        from b in lj.DefaultIfEmpty()
                        select new
                        {
                            a.TotalEstimateConfId,
                            a.EstimationCellId,
                            a.TotalEstimateConfName,
                            a.VariableId,
                            a.PhaseEstimateTypeId,
                            a.ForceSynthetic,
                            a.AuxConfId,
                            a.EstimationPeriodId,
                            a.PanelRefYearSetGroupId,
                            b?.PanelRefYearSetGroupLabelCs,
                            b?.PanelRefYearSetGroupLabelEn,
                            b?.PanelRefYearSetGroupDescriptionCs,
                            b?.PanelRefYearSetGroupDescriptionEn
                        };
                    #endregion cteTotalEstimateConfPanel: cteTotalEstimateConf + ctePanelRefYearSetGroup (LEFT JOIN)

                    #region cteEstimateConfTotalEstimateConf: cteEstimateConf + cteTotalEstimateConfPanel (INNER JOIN)
                    var cteEstimateConfTotalEstimateConf =
                        from a in cteEstimateConf
                        join b in cteTotalEstimateConfPanel
                        on a.NumeratorTotalEstimateConfId equals b.TotalEstimateConfId
                        select new
                        {
                            Id = String.Format(
                                "{0}-{1}-{2}-{3}",
                                b.PhaseEstimateTypeId.ToString(),
                                (b.EstimationPeriodId ?? 0).ToString(),
                                b.VariableId.ToString(),
                                b.EstimationCellId.ToString()
                            ),
                            a.EstimateConfId,
                            a.EstimateTypeId,
                            a.NumeratorTotalEstimateConfId,
                            a.DenominatorTotalEstimateConfId,
                            a.EstimateExecutor,
                            b.EstimationCellId,
                            b.TotalEstimateConfName,
                            b.VariableId,
                            b.PhaseEstimateTypeId,
                            b.ForceSynthetic,
                            b.AuxConfId,
                            b.EstimationPeriodId,
                            b.PanelRefYearSetGroupId,
                            b.PanelRefYearSetGroupLabelCs,
                            b.PanelRefYearSetGroupLabelEn,
                            b.PanelRefYearSetGroupDescriptionCs,
                            b.PanelRefYearSetGroupDescriptionEn
                        };
                    #endregion cteEstimateConfTotalEstimateConf: cteEstimateConf + cteTotalEstimateConfPanel (INNER JOIN)

                    #region cteEstimateConfResult: cteEstimateConfTotalEstimateConf + cteResultExtended (LEFT JOIN)
                    var cteEstimateConfResult =
                        from a in cteEstimateConfTotalEstimateConf
                        join b in cteResultExtended
                        on a.EstimateConfId equals b.EstimateConfId into lj
                        from b in lj.DefaultIfEmpty()
                        select new
                        {
                            a.Id,
                            a.EstimateConfId,
                            a.EstimateTypeId,
                            a.NumeratorTotalEstimateConfId,
                            a.DenominatorTotalEstimateConfId,
                            a.EstimateExecutor,
                            a.EstimationCellId,
                            a.TotalEstimateConfName,
                            a.VariableId,
                            a.PhaseEstimateTypeId,
                            a.ForceSynthetic,
                            a.AuxConfId,
                            a.EstimationPeriodId,
                            a.PanelRefYearSetGroupId,
                            a.PanelRefYearSetGroupLabelCs,
                            a.PanelRefYearSetGroupLabelEn,
                            a.PanelRefYearSetGroupDescriptionCs,
                            a.PanelRefYearSetGroupDescriptionEn,
                            b?.ResultId,
                            b?.PointEstimate,
                            b?.VarEstimate,
                            b?.CalcStarted,
                            b?.ExtensionVersion,
                            b?.CalcDuration,
                            b?.SamplingUnits,
                            b?.MinSSize,
                            b?.ActSSize,
                            b?.IsLatest,
                            b?.ResultExecutor,
                            b?.StandardError,
                            b?.Student,
                            b?.ConfidenceInterval,
                            b?.LowerBound,
                            b?.UpperBound
                        };
                    #endregion cteEstimateConfResult: cteEstimateConfTotalEstimateConf + cteResultExtended (LEFT JOIN)

                    #region cteEstimateConfResultAttrAdditivity: cteEstimateConfResult + cteAddResTotalAttrGroups (LEFT JOIN)
                    var cteEstimateConfResultAttrAdditivity =
                        from a in cteEstimateConfResult
                        join b in cteAddResTotalAttrGroups
                        on a.EstimateConfId equals b.EstimateConfId into lj
                        from b in lj.DefaultIfEmpty()
                        select new
                        {
                            a.Id,
                            a.EstimateConfId,
                            a.EstimateTypeId,
                            a.NumeratorTotalEstimateConfId,
                            a.DenominatorTotalEstimateConfId,
                            a.EstimateExecutor,
                            a.EstimationCellId,
                            a.TotalEstimateConfName,
                            a.VariableId,
                            a.PhaseEstimateTypeId,
                            a.ForceSynthetic,
                            a.AuxConfId,
                            a.EstimationPeriodId,
                            a.PanelRefYearSetGroupId,
                            a.PanelRefYearSetGroupLabelCs,
                            a.PanelRefYearSetGroupLabelEn,
                            a.PanelRefYearSetGroupDescriptionCs,
                            a.PanelRefYearSetGroupDescriptionEn,
                            a.ResultId,
                            a.PointEstimate,
                            a.VarEstimate,
                            a.CalcStarted,
                            a.ExtensionVersion,
                            a.CalcDuration,
                            a.SamplingUnits,
                            a.MinSSize,
                            a.ActSSize,
                            a.IsLatest,
                            a.ResultExecutor,
                            a.StandardError,
                            a.Student,
                            a.ConfidenceInterval,
                            a.LowerBound,
                            a.UpperBound,
                            AttrDiffDown = (b?.Diff) ?? 0.0,
                            AttrDiffUp = 0.0
                        };
                    #endregion cteEstimateConfResultAttrAdditivity: cteEstimateConfResult + cteAddResTotalAttrGroups (LEFT JOIN)

                    #region cteEstimateConfResultGeoAdditivity: cteEstimateConfResultAttrAdditivity + cteAddResTotalGeoGroups (LEFT JOIN)
                    var cteEstimateConfResultGeoAdditivity =
                        from a in cteEstimateConfResultAttrAdditivity
                        join b in cteAddResTotalGeoGroups
                        on a.EstimateConfId equals b.EstimateConfId into lj
                        from b in lj.DefaultIfEmpty()
                        select new
                        {
                            a.Id,
                            a.EstimateConfId,
                            a.EstimateTypeId,
                            a.NumeratorTotalEstimateConfId,
                            a.DenominatorTotalEstimateConfId,
                            a.EstimateExecutor,
                            a.EstimationCellId,
                            a.TotalEstimateConfName,
                            a.VariableId,
                            a.PhaseEstimateTypeId,
                            a.ForceSynthetic,
                            a.AuxConfId,
                            a.EstimationPeriodId,
                            a.PanelRefYearSetGroupId,
                            a.PanelRefYearSetGroupLabelCs,
                            a.PanelRefYearSetGroupLabelEn,
                            a.PanelRefYearSetGroupDescriptionCs,
                            a.PanelRefYearSetGroupDescriptionEn,
                            a.ResultId,
                            a.PointEstimate,
                            a.VarEstimate,
                            a.CalcStarted,
                            a.ExtensionVersion,
                            a.CalcDuration,
                            a.SamplingUnits,
                            a.MinSSize,
                            a.ActSSize,
                            a.IsLatest,
                            a.ResultExecutor,
                            a.StandardError,
                            a.Student,
                            a.ConfidenceInterval,
                            a.LowerBound,
                            a.UpperBound,
                            a.AttrDiffDown,
                            a.AttrDiffUp,
                            GeoDiffDown = (b?.Diff) ?? 0.0,
                            GeoDiffUp = 0.0
                        };
                    #endregion cteEstimateConfResultGeoAdditivity: cteEstimateConfResultAttrAdditivity + cteAddResTotalGeoGroups (LEFT JOIN)

                    #region cteAll: cteEstimateConfResultGeoAdditivity + cteAuxConfJoin (LEFT JOIN)
                    var cteAll =
                        from a in cteEstimateConfResultGeoAdditivity
                        join b in cteAuxConfJoin
                        on a.AuxConfId equals b.AuxConfId into lj
                        from b in lj.DefaultIfEmpty()
                        select new
                        {
                            a.Id,
                            a.EstimateConfId,
                            a.EstimateTypeId,
                            a.NumeratorTotalEstimateConfId,
                            a.DenominatorTotalEstimateConfId,
                            a.EstimateExecutor,
                            a.EstimationCellId,
                            a.TotalEstimateConfName,
                            a.VariableId,
                            a.PhaseEstimateTypeId,
                            a.ForceSynthetic,
                            a.AuxConfId,
                            a.EstimationPeriodId,
                            a.PanelRefYearSetGroupId,
                            a.PanelRefYearSetGroupLabelCs,
                            a.PanelRefYearSetGroupLabelEn,
                            a.PanelRefYearSetGroupDescriptionCs,
                            a.PanelRefYearSetGroupDescriptionEn,
                            a.ResultId,
                            a.PointEstimate,
                            a.VarEstimate,
                            a.CalcStarted,
                            a.ExtensionVersion,
                            a.CalcDuration,
                            a.SamplingUnits,
                            a.MinSSize,
                            a.ActSSize,
                            a.IsLatest,
                            a.ResultExecutor,
                            a.StandardError,
                            a.Student,
                            a.ConfidenceInterval,
                            a.LowerBound,
                            a.UpperBound,
                            a.AttrDiffDown,
                            a.AttrDiffUp,
                            a.GeoDiffDown,
                            a.GeoDiffUp,
                            b?.ModelId,
                            b?.ParamAreaId,
                            b?.Sigma,
                            b?.ModelLabelCs,
                            b?.ModelDescriptionCs,
                            b?.ModelLabelEn,
                            b?.ModelDescriptionEn,
                            b?.ParamAreaTypeId,
                            b?.ParamAreaLabelCs,
                            b?.ParamAreaDescriptionCs,
                            b?.ParamAreaLabelEn,
                            b?.ParamAreaDescriptionEn,
                            b?.ParamAreaTypeLabelCs,
                            b?.ParamAreaTypeDescriptionCs,
                            b?.ParamAreaTypeLabelEn,
                            b?.ParamAreaTypeDescriptionEn
                        };

                    #endregion cteAll


                    #region Zápis výsledku do DataTable
                    if (cteAll.Any())
                    {
                        Data =
                            cteAll
                            .OrderBy(a => a.PhaseEstimateTypeId)
                            .ThenBy(a => a.EstimationPeriodId)
                            .ThenBy(a => a.VariableId)
                            .ThenBy(a => a.EstimationCellId)
                            .ThenBy(a => a.PanelRefYearSetGroupId)
                            .Select(a =>
                                Data.LoadDataRow(
                                    values:
                                    [
                                        a.Id,
                                        a.PhaseEstimateTypeId,
                                        a.EstimationPeriodId,
                                        a.VariableId,
                                        a.EstimationCellId,
                                        a.PanelRefYearSetGroupId,
                                        a.PanelRefYearSetGroupLabelCs,
                                        a.PanelRefYearSetGroupLabelEn,
                                        a.EstimateConfId,
                                        a.EstimateTypeId,
                                        a.NumeratorTotalEstimateConfId,
                                        a.EstimateExecutor,
                                        a.TotalEstimateConfName,
                                        a.ResultId,
                                        a.PointEstimate,
                                        a.VarEstimate,
                                        a.CalcStarted,
                                        a.ExtensionVersion,
                                        a.CalcDuration,
                                        a.MinSSize,
                                        a.ActSSize,
                                        a.IsLatest,
                                        a.ResultExecutor,
                                        a.StandardError,
                                        a.ConfidenceInterval,
                                        a.LowerBound,
                                        a.UpperBound,
                                        a.AttrDiffDown,
                                        a.AttrDiffUp,
                                        a.GeoDiffDown,
                                        a.GeoDiffUp,
                                        a.ModelId,
                                        a.ModelLabelCs,
                                        a.ModelDescriptionCs,
                                        a.ModelLabelEn,
                                        a.ModelDescriptionEn,
                                        a.Sigma,
                                        a.ParamAreaId,
                                        a.ParamAreaLabelCs,
                                        a.ParamAreaDescriptionCs,
                                        a.ParamAreaLabelEn,
                                        a.ParamAreaDescriptionEn,
                                        a.ParamAreaTypeId,
                                        a.ParamAreaTypeLabelCs,
                                        a.ParamAreaTypeDescriptionCs,
                                        a.ParamAreaTypeLabelEn,
                                        a.ParamAreaTypeDescriptionEn
                                    ],
                                    fAcceptChanges: false))
                            .CopyToDataTable();
                        Data.TableName = Name;
                    }
                    #endregion Zápis výsledku do DataTable


                    #region Doplnění AttrDiffUp a GeoDiffUp
                    foreach (var a in cteAll)
                    {
                        foreach (var b in cteAddResTotalAttr)
                        {
                            double diff = b.Diff ?? 0.0;
                            if (b.EstimateConfsFoundList.Contains(item: a.EstimateConfId))
                            {
                                string whereCondition = $"{ColEstimateConfId.Name} = {a.EstimateConfId}";
                                DataRow[] rows = Data.Select(filterExpression: whereCondition);
                                foreach (DataRow row in rows)
                                {
                                    double val = Functions.GetDoubleArg(row: row, name: ColAttrDiffUp.Name, defaultValue: 0);
                                    if (val < diff)
                                    {
                                        Functions.SetNDoubleArg(row: row, name: ColAttrDiffUp.Name, val: diff);
                                    }
                                }
                            }
                        }
                        foreach (var b in cteAddResTotalGeo)
                        {
                            double diff = b.Diff ?? 0.0;
                            if (b.EstimateConfsFoundList.Contains(item: a.EstimateConfId))
                            {
                                string whereCondition = $"{ColEstimateConfId.Name} = {a.EstimateConfId}";
                                DataRow[] rows = Data.Select(filterExpression: whereCondition);
                                foreach (DataRow row in rows)
                                {
                                    double val = Functions.GetDoubleArg(row: row, name: ColGeoDiffUp.Name, defaultValue: 0);
                                    if (val < diff)
                                    {
                                        Functions.SetNDoubleArg(row: row, name: ColGeoDiffUp.Name, val: diff);
                                    }
                                }
                            }
                        }
                    }
                    #endregion


                    #region Omezení podmínkou
                    if (!String.IsNullOrEmpty(value: Condition))
                    {
                        DataView view = new(table: Data);
                        try
                        {
                            view.RowFilter = Condition;
                        }
                        catch
                        {
                            view.RowFilter = String.Empty;
                            Condition = String.Empty;
                        }
                        Data = view.ToTable();
                    }
                    #endregion Omezení podmínkou

                    #region Omezeni počtem
                    if (Limit != null)
                    {
                        Data = Data.AsEnumerable()
                            .Take(count: (int)Limit)
                            .CopyToDataTable();
                    }
                    #endregion Omezeni počtem

                    stopWatch.Stop();
                    LoadingTime = 0.001 * stopWatch.ElapsedMilliseconds;
                    Data.TableName = TableName;
                }

                #endregion Methods

            }

        }
    }
}



