﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Versioning;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {
            // fn_etl_get_target_variable_metadata

            /// <summary>
            /// Target variable for ETL with metadata
            /// (in the NfiEsta schema)
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class FnEtlGetTargetVariableMetadata(
                FnEtlGetTargetVariableMetadataList composite = null,
                DataRow data = null)
                    : ALinqViewEntry<FnEtlGetTargetVariableMetadataList>(composite: composite, data: data),
                      ITargetVariableMetadata<FnEtlGetTargetVariableMetadataList>
            {

                #region Properties

                /// <summary>
                /// List of metadata elements
                /// </summary>
                public MetadataElementList MetadataElements
                {
                    get
                    {
                        return MetadataElementList.Standard(
                            metadata: this);
                    }
                }

                #endregion Properties


                #region Derived Properties

                /// <summary>
                /// Identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColId.Name,
                            defaultValue: Int32.Parse(s: FnEtlGetTargetVariableMetadataList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Target variable identifier
                /// </summary>
                public int TargetVariableId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColTargetVariableId.Name,
                            defaultValue: Int32.Parse(s: FnEtlGetTargetVariableMetadataList.ColTargetVariableId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColTargetVariableId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Generated local density identifier
                /// </summary>
                public int LocalDensityId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColLocalDensityId.Name,
                            defaultValue: Int32.Parse(s: FnEtlGetTargetVariableMetadataList.ColLocalDensityId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColLocalDensityId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// JsonValidity
                /// </summary>
                public bool JsonValidity
                {
                    get
                    {
                        return Functions.GetBoolArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColJsonValidity.Name,
                            defaultValue: Boolean.Parse(value: FnEtlGetTargetVariableMetadataList.ColJsonValidity.DefaultValue));
                    }
                    set
                    {
                        Functions.SetBoolArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColJsonValidity.Name,
                            val: value);
                    }
                }


                #region Indicator

                /// <summary>
                /// Indicator label in national language
                /// </summary>
                public string IndicatorLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColIndicatorLabelCs.Name,
                            defaultValue: FnEtlGetTargetVariableMetadataList.ColIndicatorLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColIndicatorLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Indicator description in national language
                /// </summary>
                public string IndicatorDescriptionCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColIndicatorDescriptionCs.Name,
                            defaultValue: FnEtlGetTargetVariableMetadataList.ColIndicatorDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColIndicatorDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Indicator label in English
                /// </summary>
                public string IndicatorLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColIndicatorLabelEn.Name,
                            defaultValue: FnEtlGetTargetVariableMetadataList.ColIndicatorLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColIndicatorLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Indicator description in English
                /// </summary>
                public string IndicatorDescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColIndicatorDescriptionEn.Name,
                            defaultValue: FnEtlGetTargetVariableMetadataList.ColIndicatorDescriptionEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColIndicatorDescriptionEn.Name,
                            val: value);
                    }
                }

                #endregion Indicator


                #region StateOrChange

                /// <summary>
                /// State or change label in national language
                /// </summary>
                public string StateOrChangeLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColStateOrChangeLabelCs.Name,
                            defaultValue: FnEtlGetTargetVariableMetadataList.ColStateOrChangeLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColStateOrChangeLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// State or change description in national language
                /// </summary>
                public string StateOrChangeDescriptionCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColStateOrChangeDescriptionCs.Name,
                            defaultValue: FnEtlGetTargetVariableMetadataList.ColStateOrChangeDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColStateOrChangeDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// State or change label in English
                /// </summary>
                public string StateOrChangeLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColStateOrChangeLabelEn.Name,
                            defaultValue: FnEtlGetTargetVariableMetadataList.ColStateOrChangeLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColStateOrChangeLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// State or change description in English
                /// </summary>
                public string StateOrChangeDescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColStateOrChangeDescriptionEn.Name,
                            defaultValue: FnEtlGetTargetVariableMetadataList.ColStateOrChangeDescriptionEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColStateOrChangeDescriptionEn.Name,
                            val: value);
                    }
                }

                #endregion StateOrChange


                #region Unit

                /// <summary>
                /// Unit label in national language
                /// </summary>
                public string UnitLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColUnitLabelCs.Name,
                            defaultValue: FnEtlGetTargetVariableMetadataList.ColUnitLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColUnitLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Unit description in national language
                /// </summary>
                public string UnitDescriptionCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColUnitDescriptionCs.Name,
                            defaultValue: FnEtlGetTargetVariableMetadataList.ColUnitDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColUnitDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Unit label in English
                /// </summary>
                public string UnitLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColUnitLabelEn.Name,
                            defaultValue: FnEtlGetTargetVariableMetadataList.ColUnitLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColUnitLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Unit description in English
                /// </summary>
                public string UnitDescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColUnitDescriptionEn.Name,
                            defaultValue: FnEtlGetTargetVariableMetadataList.ColUnitDescriptionEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColUnitDescriptionEn.Name,
                            val: value);
                    }
                }

                #endregion Unit


                #region LocalDensityObjectType

                /// <summary>
                /// Local density object type label in national language
                /// </summary>
                public string LocalDensityObjectTypeLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColLocalDensityObjectTypeLabelCs.Name,
                            defaultValue: FnEtlGetTargetVariableMetadataList.ColLocalDensityObjectTypeLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColLocalDensityObjectTypeLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Local density object type description in national language
                /// </summary>
                public string LocalDensityObjectTypeDescriptionCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColLocalDensityObjectTypeDescriptionCs.Name,
                            defaultValue: FnEtlGetTargetVariableMetadataList.ColLocalDensityObjectTypeDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColLocalDensityObjectTypeDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Local density object type label in English
                /// </summary>
                public string LocalDensityObjectTypeLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColLocalDensityObjectTypeLabelEn.Name,
                            defaultValue: FnEtlGetTargetVariableMetadataList.ColLocalDensityObjectTypeLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColLocalDensityObjectTypeLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Local density object type description in English
                /// </summary>
                public string LocalDensityObjectTypeDescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColLocalDensityObjectTypeDescriptionEn.Name,
                            defaultValue: FnEtlGetTargetVariableMetadataList.ColLocalDensityObjectTypeDescriptionEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColLocalDensityObjectTypeDescriptionEn.Name,
                            val: value);
                    }
                }

                #endregion LocalDensityObjectType


                #region LocalDensityObject

                /// <summary>
                /// Local density object label in national language
                /// </summary>
                public string LocalDensityObjectLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColLocalDensityObjectLabelCs.Name,
                            defaultValue: FnEtlGetTargetVariableMetadataList.ColLocalDensityObjectLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColLocalDensityObjectLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Local density object description in national language
                /// </summary>
                public string LocalDensityObjectDescriptionCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColLocalDensityObjectDescriptionCs.Name,
                            defaultValue: FnEtlGetTargetVariableMetadataList.ColLocalDensityObjectDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColLocalDensityObjectDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Local density object label in English
                /// </summary>
                public string LocalDensityObjectLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColLocalDensityObjectLabelEn.Name,
                            defaultValue: FnEtlGetTargetVariableMetadataList.ColLocalDensityObjectLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColLocalDensityObjectLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Local density object description in English
                /// </summary>
                public string LocalDensityObjectDescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColLocalDensityObjectDescriptionEn.Name,
                            defaultValue: FnEtlGetTargetVariableMetadataList.ColLocalDensityObjectDescriptionEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColLocalDensityObjectDescriptionEn.Name,
                            val: value);
                    }
                }

                #endregion LocalDensityObject


                #region LocalDensity

                /// <summary>
                /// Local density contribution label in national language
                /// </summary>
                public string LocalDensityLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColLocalDensityLabelCs.Name,
                            defaultValue: FnEtlGetTargetVariableMetadataList.ColLocalDensityLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColLocalDensityLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Local density contribution description in national language
                /// </summary>
                public string LocalDensityDescriptionCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColLocalDensityDescriptionCs.Name,
                            defaultValue: FnEtlGetTargetVariableMetadataList.ColLocalDensityDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColLocalDensityDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Local density contribution label in English
                /// </summary>
                public string LocalDensityLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColLocalDensityLabelEn.Name,
                            defaultValue: FnEtlGetTargetVariableMetadataList.ColLocalDensityLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColLocalDensityLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Local density contribution description in English
                /// </summary>
                public string LocalDensityDescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColLocalDensityDescriptionEn.Name,
                            defaultValue: FnEtlGetTargetVariableMetadataList.ColLocalDensityDescriptionEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColLocalDensityDescriptionEn.Name,
                            val: value);
                    }
                }

                #endregion LocalDensity


                #region UseNegative

                /// <summary>
                /// Use negative value in national section of JSON file
                /// </summary>
                public Nullable<bool> UseNegativeCs
                {
                    get
                    {
                        return Functions.GetNBoolArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColUseNegativeCs.Name,
                            defaultValue: String.IsNullOrEmpty(value: FnEtlGetTargetVariableMetadataList.ColUseNegativeCs.DefaultValue) ?
                                (Nullable<bool>)null :
                                Boolean.Parse(value: FnEtlGetTargetVariableMetadataList.ColUseNegativeCs.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNBoolArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColUseNegativeCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Use negative value in English section of JSON file
                /// </summary>
                public Nullable<bool> UseNegativeEn
                {
                    get
                    {
                        return Functions.GetNBoolArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColUseNegativeEn.Name,
                            defaultValue: String.IsNullOrEmpty(value: FnEtlGetTargetVariableMetadataList.ColUseNegativeEn.DefaultValue) ?
                                (Nullable<bool>)null :
                                Boolean.Parse(value: FnEtlGetTargetVariableMetadataList.ColUseNegativeEn.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNBoolArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColUseNegativeEn.Name,
                            val: value);
                    }
                }

                #endregion UseNegative


                #region Version

                /// <summary>
                /// Version label in national language
                /// </summary>
                public string VersionLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColVersionLabelCs.Name,
                            defaultValue: FnEtlGetTargetVariableMetadataList.ColVersionLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColVersionLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Version description in national language
                /// </summary>
                public string VersionDescriptionCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColVersionDescriptionCs.Name,
                            defaultValue: FnEtlGetTargetVariableMetadataList.ColVersionDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColVersionDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Version label in English
                /// </summary>
                public string VersionLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColVersionLabelEn.Name,
                            defaultValue: FnEtlGetTargetVariableMetadataList.ColVersionLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColVersionLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Version description in English
                /// </summary>
                public string VersionDescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColVersionDescriptionEn.Name,
                            defaultValue: FnEtlGetTargetVariableMetadataList.ColVersionDescriptionEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColVersionDescriptionEn.Name,
                            val: value);
                    }
                }

                #endregion Version


                #region DefinitionVariant

                /// <summary>
                /// Definition variant label in national language
                /// </summary>
                public string DefinitionVariantLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColDefinitionVariantLabelCs.Name,
                            defaultValue: FnEtlGetTargetVariableMetadataList.ColDefinitionVariantLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColDefinitionVariantLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Definition variant description in national language
                /// </summary>
                public string DefinitionVariantDescriptionCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColDefinitionVariantDescriptionCs.Name,
                            defaultValue: FnEtlGetTargetVariableMetadataList.ColDefinitionVariantDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColDefinitionVariantDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Definition variant label in English
                /// </summary>
                public string DefinitionVariantLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColDefinitionVariantLabelEn.Name,
                            defaultValue: FnEtlGetTargetVariableMetadataList.ColDefinitionVariantLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColDefinitionVariantLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Definition variant description in English
                /// </summary>
                public string DefinitionVariantDescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColDefinitionVariantDescriptionEn.Name,
                            defaultValue: FnEtlGetTargetVariableMetadataList.ColDefinitionVariantDescriptionEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColDefinitionVariantDescriptionEn.Name,
                            val: value);
                    }
                }

                #endregion DefinitionVariant


                #region AreaDomain

                /// <summary>
                /// Area domain label in national language
                /// </summary>
                public string AreaDomainLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColAreaDomainLabelCs.Name,
                            defaultValue: FnEtlGetTargetVariableMetadataList.ColAreaDomainLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColAreaDomainLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Area domain description in national language
                /// </summary>
                public string AreaDomainDescriptionCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColAreaDomainDescriptionCs.Name,
                            defaultValue: FnEtlGetTargetVariableMetadataList.ColAreaDomainDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColAreaDomainDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Area domain label in English
                /// </summary>
                public string AreaDomainLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColAreaDomainLabelEn.Name,
                            defaultValue: FnEtlGetTargetVariableMetadataList.ColAreaDomainLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColAreaDomainLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Area domain description in English
                /// </summary>
                public string AreaDomainDescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColAreaDomainDescriptionEn.Name,
                            defaultValue: FnEtlGetTargetVariableMetadataList.ColAreaDomainDescriptionEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColAreaDomainDescriptionEn.Name,
                            val: value);
                    }
                }

                #endregion AreaDomain


                #region Population

                /// <summary>
                /// Population label in national language
                /// </summary>
                public string PopulationLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColPopulationLabelCs.Name,
                            defaultValue: FnEtlGetTargetVariableMetadataList.ColPopulationLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColPopulationLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Population description in national language
                /// </summary>
                public string PopulationDescriptionCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColPopulationDescriptionCs.Name,
                            defaultValue: FnEtlGetTargetVariableMetadataList.ColPopulationDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColPopulationDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Population label in English
                /// </summary>
                public string PopulationLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColPopulationLabelEn.Name,
                            defaultValue: FnEtlGetTargetVariableMetadataList.ColPopulationLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColPopulationLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Population description in English
                /// </summary>
                public string PopulationDescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColPopulationDescriptionEn.Name,
                            defaultValue: FnEtlGetTargetVariableMetadataList.ColPopulationDescriptionEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableMetadataList.ColPopulationDescriptionEn.Name,
                            val: value);
                    }
                }

                #endregion Population

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not FnEtlGetTargetVariableMetadata Type, return False
                    if (obj is not FnEtlGetTargetVariableMetadata)
                    {
                        return false;
                    }

                    return
                        Id == ((FnEtlGetTargetVariableMetadata)obj).Id;
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                #endregion Methods

            }


            /// <summary>
            /// List of target variable for ETL with metadata
            /// (in the NfiEsta schema)
            /// </summary>
            public class FnEtlGetTargetVariableMetadataList
                : ALinqView, ITargetVariableMetadataList
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema =
                    NfiEstaSchema.Name;

                /// <summary>
                /// Table name
                /// </summary>
                public static readonly string Name =
                    "fn_etl_get_target_variable_metadata";

                /// <summary>
                /// Table alias
                /// </summary>
                public static readonly string Alias =
                    "fn_etl_get_target_variable_metadata";

                /// <summary>
                /// Description in national language
                /// </summary>
                public static readonly string CaptionCs =
                    "Cílová proměnná pro ETL včetně metadat";

                /// <summary>
                /// Description in English
                /// </summary>
                public static readonly string CaptionEn =
                    "Target variable for ETL with metadata";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
                {
                    { TargetVariableMetadataJson.ColId, new ColumnMetadata()
                    {
                        Name = TargetVariableMetadataJson.ColId,
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = true,
                        NotNull = true,
                        DefaultValue = "0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = TargetVariableMetadataJson.ColId.ToUpper(),
                        HeaderTextEn = TargetVariableMetadataJson.ColId.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 0
                    }
                    },
                    { TargetVariableMetadataJson.ColTargetVariableId, new ColumnMetadata()
                    {
                        Name = TargetVariableMetadataJson.ColTargetVariableId,
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue = "0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = TargetVariableMetadataJson.ColTargetVariableId.ToUpper(),
                        HeaderTextEn = TargetVariableMetadataJson.ColTargetVariableId.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 1
                    }
                    },
                    { TargetVariableMetadataJson.ColLocalDensityId, new ColumnMetadata()
                    {
                        Name = TargetVariableMetadataJson.ColLocalDensityId,
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue = "0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = TargetVariableMetadataJson.ColLocalDensityId.ToUpper(),
                        HeaderTextEn = TargetVariableMetadataJson.ColLocalDensityId.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 2
                    }
                    },
                    { TargetVariableMetadataJson.ColJsonValidity, new ColumnMetadata()
                    {
                        Name = TargetVariableMetadataJson.ColJsonValidity,
                        DbName = null,
                        DataType = "System.Boolean",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue = "false",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = TargetVariableMetadataJson.ColJsonValidity.ToUpper(),
                        HeaderTextEn = TargetVariableMetadataJson.ColJsonValidity.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 3
                    }
                    },
                    { TargetVariableMetadataJson.ColIndicatorLabelCs, new ColumnMetadata()
                    {
                        Name = TargetVariableMetadataJson.ColIndicatorLabelCs,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = TargetVariableMetadataJson.ColIndicatorLabelCs.ToUpper(),
                        HeaderTextEn = TargetVariableMetadataJson.ColIndicatorLabelCs.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 4
                    }
                    },
                    { TargetVariableMetadataJson.ColIndicatorDescriptionCs, new ColumnMetadata()
                    {
                        Name = TargetVariableMetadataJson.ColIndicatorDescriptionCs,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = TargetVariableMetadataJson.ColIndicatorDescriptionCs.ToUpper(),
                        HeaderTextEn = TargetVariableMetadataJson.ColIndicatorDescriptionCs.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 5
                    }
                    },
                    { TargetVariableMetadataJson.ColIndicatorLabelEn, new ColumnMetadata()
                    {
                        Name = TargetVariableMetadataJson.ColIndicatorLabelEn,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = TargetVariableMetadataJson.ColIndicatorLabelEn.ToUpper(),
                        HeaderTextEn = TargetVariableMetadataJson.ColIndicatorLabelEn.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 6
                    }
                    },
                    { TargetVariableMetadataJson.ColIndicatorDescriptionEn, new ColumnMetadata()
                    {
                        Name = TargetVariableMetadataJson.ColIndicatorDescriptionEn,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = TargetVariableMetadataJson.ColIndicatorDescriptionEn.ToUpper(),
                        HeaderTextEn = TargetVariableMetadataJson.ColIndicatorDescriptionEn.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 7
                    }
                    },

                    { TargetVariableMetadataJson.ColStateOrChangeLabelCs, new ColumnMetadata()
                    {
                        Name = TargetVariableMetadataJson.ColStateOrChangeLabelCs,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = TargetVariableMetadataJson.ColStateOrChangeLabelCs.ToUpper(),
                        HeaderTextEn = TargetVariableMetadataJson.ColStateOrChangeLabelCs.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 8
                    }
                    },
                    { TargetVariableMetadataJson.ColStateOrChangeDescriptionCs, new ColumnMetadata()
                    {
                        Name = TargetVariableMetadataJson.ColStateOrChangeDescriptionCs,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = TargetVariableMetadataJson.ColStateOrChangeDescriptionCs.ToUpper(),
                        HeaderTextEn = TargetVariableMetadataJson.ColStateOrChangeDescriptionCs.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 9
                    }
                    },
                    { TargetVariableMetadataJson.ColStateOrChangeLabelEn, new ColumnMetadata()
                    {
                        Name = TargetVariableMetadataJson.ColStateOrChangeLabelEn,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = TargetVariableMetadataJson.ColStateOrChangeLabelEn.ToUpper(),
                        HeaderTextEn = TargetVariableMetadataJson.ColStateOrChangeLabelEn.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 10
                    }
                    },
                    { TargetVariableMetadataJson.ColStateOrChangeDescriptionEn, new ColumnMetadata()
                    {
                        Name = TargetVariableMetadataJson.ColStateOrChangeDescriptionEn,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = TargetVariableMetadataJson.ColStateOrChangeDescriptionEn.ToUpper(),
                        HeaderTextEn = TargetVariableMetadataJson.ColStateOrChangeDescriptionEn.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 11
                    }
                    },

                    { TargetVariableMetadataJson.ColUnitLabelCs, new ColumnMetadata()
                    {
                        Name = TargetVariableMetadataJson.ColUnitLabelCs,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = TargetVariableMetadataJson.ColUnitLabelCs.ToUpper(),
                        HeaderTextEn = TargetVariableMetadataJson.ColUnitLabelCs.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 12
                    }
                    },
                    { TargetVariableMetadataJson.ColUnitDescriptionCs, new ColumnMetadata()
                    {
                        Name = TargetVariableMetadataJson.ColUnitDescriptionCs,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = TargetVariableMetadataJson.ColUnitDescriptionCs.ToUpper(),
                        HeaderTextEn = TargetVariableMetadataJson.ColUnitDescriptionCs.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 13
                    }
                    },
                    { TargetVariableMetadataJson.ColUnitLabelEn, new ColumnMetadata()
                    {
                        Name = TargetVariableMetadataJson.ColUnitLabelEn,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = TargetVariableMetadataJson.ColUnitLabelEn.ToUpper(),
                        HeaderTextEn = TargetVariableMetadataJson.ColUnitLabelEn.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 14
                    }
                    },
                    { TargetVariableMetadataJson.ColUnitDescriptionEn, new ColumnMetadata()
                    {
                        Name = TargetVariableMetadataJson.ColUnitDescriptionEn,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = TargetVariableMetadataJson.ColUnitDescriptionEn.ToUpper(),
                        HeaderTextEn = TargetVariableMetadataJson.ColUnitDescriptionEn.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 15
                    }
                    },

                    { TargetVariableMetadataJson.ColLocalDensityObjectTypeLabelCs, new ColumnMetadata()
                    {
                        Name = TargetVariableMetadataJson.ColLocalDensityObjectTypeLabelCs,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = TargetVariableMetadataJson.ColLocalDensityObjectTypeLabelCs.ToUpper(),
                        HeaderTextEn = TargetVariableMetadataJson.ColLocalDensityObjectTypeLabelCs.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 16
                    }
                    },
                    { TargetVariableMetadataJson.ColLocalDensityObjectTypeDescriptionCs, new ColumnMetadata()
                    {
                        Name = TargetVariableMetadataJson.ColLocalDensityObjectTypeDescriptionCs,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = TargetVariableMetadataJson.ColLocalDensityObjectTypeDescriptionCs.ToUpper(),
                        HeaderTextEn = TargetVariableMetadataJson.ColLocalDensityObjectTypeDescriptionCs.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 17
                    }
                    },
                    { TargetVariableMetadataJson.ColLocalDensityObjectTypeLabelEn, new ColumnMetadata()
                    {
                        Name = TargetVariableMetadataJson.ColLocalDensityObjectTypeLabelEn,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = TargetVariableMetadataJson.ColLocalDensityObjectTypeLabelEn.ToUpper(),
                        HeaderTextEn = TargetVariableMetadataJson.ColLocalDensityObjectTypeLabelEn.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 18
                    }
                    },
                    { TargetVariableMetadataJson.ColLocalDensityObjectTypeDescriptionEn, new ColumnMetadata()
                    {
                        Name = TargetVariableMetadataJson.ColLocalDensityObjectTypeDescriptionEn,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = TargetVariableMetadataJson.ColLocalDensityObjectTypeDescriptionEn.ToUpper(),
                        HeaderTextEn = TargetVariableMetadataJson.ColLocalDensityObjectTypeDescriptionEn.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 19
                    }
                    },

                    { TargetVariableMetadataJson.ColLocalDensityObjectLabelCs, new ColumnMetadata()
                    {
                        Name = TargetVariableMetadataJson.ColLocalDensityObjectLabelCs,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = TargetVariableMetadataJson.ColLocalDensityObjectLabelCs.ToUpper(),
                        HeaderTextEn = TargetVariableMetadataJson.ColLocalDensityObjectLabelCs.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 20
                    }
                    },
                    { TargetVariableMetadataJson.ColLocalDensityObjectDescriptionCs, new ColumnMetadata()
                    {
                        Name = TargetVariableMetadataJson.ColLocalDensityObjectDescriptionCs,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = TargetVariableMetadataJson.ColLocalDensityObjectDescriptionCs.ToUpper(),
                        HeaderTextEn = TargetVariableMetadataJson.ColLocalDensityObjectDescriptionCs.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 21
                    }
                    },
                    { TargetVariableMetadataJson.ColLocalDensityObjectLabelEn, new ColumnMetadata()
                    {
                        Name = TargetVariableMetadataJson.ColLocalDensityObjectLabelEn,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = TargetVariableMetadataJson.ColLocalDensityObjectLabelEn.ToUpper(),
                        HeaderTextEn = TargetVariableMetadataJson.ColLocalDensityObjectLabelEn.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 22
                    }
                    },
                    { TargetVariableMetadataJson.ColLocalDensityObjectDescriptionEn, new ColumnMetadata()
                    {
                        Name = TargetVariableMetadataJson.ColLocalDensityObjectDescriptionEn,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = TargetVariableMetadataJson.ColLocalDensityObjectDescriptionEn.ToUpper(),
                        HeaderTextEn = TargetVariableMetadataJson.ColLocalDensityObjectDescriptionEn.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 23
                    }
                    },

                    { TargetVariableMetadataJson.ColLocalDensityLabelCs, new ColumnMetadata()
                    {
                        Name = TargetVariableMetadataJson.ColLocalDensityLabelCs,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = TargetVariableMetadataJson.ColLocalDensityLabelCs.ToUpper(),
                        HeaderTextEn = TargetVariableMetadataJson.ColLocalDensityLabelCs.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 24
                    }
                    },
                    { TargetVariableMetadataJson.ColLocalDensityDescriptionCs, new ColumnMetadata()
                    {
                        Name = TargetVariableMetadataJson.ColLocalDensityDescriptionCs,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = TargetVariableMetadataJson.ColLocalDensityDescriptionCs.ToUpper(),
                        HeaderTextEn = TargetVariableMetadataJson.ColLocalDensityDescriptionCs.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 25
                    }
                    },
                    { TargetVariableMetadataJson.ColLocalDensityLabelEn, new ColumnMetadata()
                    {
                        Name = TargetVariableMetadataJson.ColLocalDensityLabelEn,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = TargetVariableMetadataJson.ColLocalDensityLabelEn.ToUpper(),
                        HeaderTextEn = TargetVariableMetadataJson.ColLocalDensityLabelEn.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 26
                    }
                    },
                    { TargetVariableMetadataJson.ColLocalDensityDescriptionEn, new ColumnMetadata()
                    {
                        Name = TargetVariableMetadataJson.ColLocalDensityDescriptionEn,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = TargetVariableMetadataJson.ColLocalDensityDescriptionEn.ToUpper(),
                        HeaderTextEn = TargetVariableMetadataJson.ColLocalDensityDescriptionEn.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 27
                    }
                    },

                    { TargetVariableMetadataJson.ColUseNegativeCs, new ColumnMetadata()
                    {
                        Name = TargetVariableMetadataJson.ColUseNegativeCs,
                        DbName = null,
                        DataType = "System.Boolean",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = TargetVariableMetadataJson.ColUseNegativeCs.ToUpper(),
                        HeaderTextEn = TargetVariableMetadataJson.ColUseNegativeCs.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 28
                    }
                    },
                    { TargetVariableMetadataJson.ColUseNegativeEn, new ColumnMetadata()
                    {
                        Name = TargetVariableMetadataJson.ColUseNegativeEn,
                        DbName = null,
                        DataType = "System.Boolean",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = TargetVariableMetadataJson.ColUseNegativeEn.ToUpper(),
                        HeaderTextEn = TargetVariableMetadataJson.ColUseNegativeEn.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 29
                    }
                    },

                    { TargetVariableMetadataJson.ColVersionLabelCs, new ColumnMetadata()
                    {
                        Name = TargetVariableMetadataJson.ColVersionLabelCs,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = TargetVariableMetadataJson.ColVersionLabelCs.ToUpper(),
                        HeaderTextEn = TargetVariableMetadataJson.ColVersionLabelCs.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 30
                    }
                    },
                    { TargetVariableMetadataJson.ColVersionDescriptionCs, new ColumnMetadata()
                    {
                        Name = TargetVariableMetadataJson.ColVersionDescriptionCs,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = TargetVariableMetadataJson.ColVersionDescriptionCs.ToUpper(),
                        HeaderTextEn = TargetVariableMetadataJson.ColVersionDescriptionCs.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 31
                    }
                    },
                    { TargetVariableMetadataJson.ColVersionLabelEn, new ColumnMetadata()
                    {
                        Name = TargetVariableMetadataJson.ColVersionLabelEn,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = TargetVariableMetadataJson.ColVersionLabelEn.ToUpper(),
                        HeaderTextEn = TargetVariableMetadataJson.ColVersionLabelEn.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 32
                    }
                    },
                    { TargetVariableMetadataJson.ColVersionDescriptionEn, new ColumnMetadata()
                    {
                        Name = TargetVariableMetadataJson.ColVersionDescriptionEn,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = TargetVariableMetadataJson.ColVersionDescriptionEn.ToUpper(),
                        HeaderTextEn = TargetVariableMetadataJson.ColVersionDescriptionEn.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 33
                    }
                    },

                    { TargetVariableMetadataJson.ColDefinitionVariantLabelCs, new ColumnMetadata()
                    {
                        Name = TargetVariableMetadataJson.ColDefinitionVariantLabelCs,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = TargetVariableMetadataJson.ColDefinitionVariantLabelCs.ToUpper(),
                        HeaderTextEn = TargetVariableMetadataJson.ColDefinitionVariantLabelCs.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 34
                    }
                    },
                    { TargetVariableMetadataJson.ColDefinitionVariantDescriptionCs, new ColumnMetadata()
                    {
                        Name = TargetVariableMetadataJson.ColDefinitionVariantDescriptionCs,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = TargetVariableMetadataJson.ColDefinitionVariantDescriptionCs.ToUpper(),
                        HeaderTextEn = TargetVariableMetadataJson.ColDefinitionVariantDescriptionCs.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 35
                    }
                    },
                    { TargetVariableMetadataJson.ColDefinitionVariantLabelEn, new ColumnMetadata()
                    {
                        Name = TargetVariableMetadataJson.ColDefinitionVariantLabelEn,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = TargetVariableMetadataJson.ColDefinitionVariantLabelEn.ToUpper(),
                        HeaderTextEn = TargetVariableMetadataJson.ColDefinitionVariantLabelEn.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 36
                    }
                    },
                    { TargetVariableMetadataJson.ColDefinitionVariantDescriptionEn, new ColumnMetadata()
                    {
                        Name = TargetVariableMetadataJson.ColDefinitionVariantDescriptionEn,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = TargetVariableMetadataJson.ColDefinitionVariantDescriptionEn.ToUpper(),
                        HeaderTextEn = TargetVariableMetadataJson.ColDefinitionVariantDescriptionEn.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 37
                    }
                    },

                    { TargetVariableMetadataJson.ColAreaDomainLabelCs, new ColumnMetadata()
                    {
                        Name = TargetVariableMetadataJson.ColAreaDomainLabelCs,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = TargetVariableMetadataJson.ColAreaDomainLabelCs.ToUpper(),
                        HeaderTextEn = TargetVariableMetadataJson.ColAreaDomainLabelCs.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 38
                    }
                    },
                    { TargetVariableMetadataJson.ColAreaDomainDescriptionCs, new ColumnMetadata()
                    {
                        Name = TargetVariableMetadataJson.ColAreaDomainDescriptionCs,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = TargetVariableMetadataJson.ColAreaDomainDescriptionCs.ToUpper(),
                        HeaderTextEn = TargetVariableMetadataJson.ColAreaDomainDescriptionCs.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 39
                    }
                    },
                    { TargetVariableMetadataJson.ColAreaDomainLabelEn, new ColumnMetadata()
                    {
                        Name = TargetVariableMetadataJson.ColAreaDomainLabelEn,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = TargetVariableMetadataJson.ColAreaDomainLabelEn.ToUpper(),
                        HeaderTextEn = TargetVariableMetadataJson.ColAreaDomainLabelEn.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 40
                    }
                    },
                    { TargetVariableMetadataJson.ColAreaDomainDescriptionEn, new ColumnMetadata()
                    {
                        Name = TargetVariableMetadataJson.ColAreaDomainDescriptionEn,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = TargetVariableMetadataJson.ColAreaDomainDescriptionEn.ToUpper(),
                        HeaderTextEn = TargetVariableMetadataJson.ColAreaDomainDescriptionEn.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 41
                    }
                    },

                    { TargetVariableMetadataJson.ColPopulationLabelCs, new ColumnMetadata()
                    {
                        Name = TargetVariableMetadataJson.ColPopulationLabelCs,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = TargetVariableMetadataJson.ColPopulationLabelCs.ToUpper(),
                        HeaderTextEn = TargetVariableMetadataJson.ColPopulationLabelCs.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 42
                    }
                    },
                    { TargetVariableMetadataJson.ColPopulationDescriptionCs, new ColumnMetadata()
                    {
                        Name = TargetVariableMetadataJson.ColPopulationDescriptionCs,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = TargetVariableMetadataJson.ColPopulationDescriptionCs.ToUpper(),
                        HeaderTextEn = TargetVariableMetadataJson.ColPopulationDescriptionCs.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 43
                    }
                    },
                    { TargetVariableMetadataJson.ColPopulationLabelEn, new ColumnMetadata()
                    {
                        Name = TargetVariableMetadataJson.ColPopulationLabelEn,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = TargetVariableMetadataJson.ColPopulationLabelEn.ToUpper(),
                        HeaderTextEn = TargetVariableMetadataJson.ColPopulationLabelEn.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 44
                    }
                    },
                    { TargetVariableMetadataJson.ColPopulationDescriptionEn, new ColumnMetadata()
                    {
                        Name = TargetVariableMetadataJson.ColPopulationDescriptionEn,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = TargetVariableMetadataJson.ColPopulationDescriptionEn.ToUpper(),
                        HeaderTextEn = TargetVariableMetadataJson.ColPopulationDescriptionEn.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 45
                    }
                    }

                };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols[TargetVariableMetadataJson.ColId];

                /// <summary>
                /// Column target_variable_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColTargetVariableId = Cols[TargetVariableMetadataJson.ColTargetVariableId];

                /// <summary>
                /// Column ldsity_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColLocalDensityId = Cols[TargetVariableMetadataJson.ColLocalDensityId];

                /// <summary>
                /// Column json_validity metadata
                /// </summary>
                public static readonly ColumnMetadata ColJsonValidity = Cols[TargetVariableMetadataJson.ColJsonValidity];

                /// <summary>
                /// Column indicator_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColIndicatorLabelCs = Cols[TargetVariableMetadataJson.ColIndicatorLabelCs];

                /// <summary>
                /// Column indicator_description_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColIndicatorDescriptionCs = Cols[TargetVariableMetadataJson.ColIndicatorDescriptionCs];

                /// <summary>
                /// Column indicator_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColIndicatorLabelEn = Cols[TargetVariableMetadataJson.ColIndicatorLabelEn];

                /// <summary>
                /// Column indicator_description_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColIndicatorDescriptionEn = Cols[TargetVariableMetadataJson.ColIndicatorDescriptionEn];


                /// <summary>
                /// Column state_or_change_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColStateOrChangeLabelCs = Cols[TargetVariableMetadataJson.ColStateOrChangeLabelCs];

                /// <summary>
                /// Column state_or_change_description_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColStateOrChangeDescriptionCs = Cols[TargetVariableMetadataJson.ColStateOrChangeDescriptionCs];

                /// <summary>
                /// Column state_or_change_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColStateOrChangeLabelEn = Cols[TargetVariableMetadataJson.ColStateOrChangeLabelEn];

                /// <summary>
                /// Column state_or_change_description_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColStateOrChangeDescriptionEn = Cols[TargetVariableMetadataJson.ColStateOrChangeDescriptionEn];


                /// <summary>
                /// Column unit_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColUnitLabelCs = Cols[TargetVariableMetadataJson.ColUnitLabelCs];

                /// <summary>
                /// Column unit_description_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColUnitDescriptionCs = Cols[TargetVariableMetadataJson.ColUnitDescriptionCs];

                /// <summary>
                /// Column unit_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColUnitLabelEn = Cols[TargetVariableMetadataJson.ColUnitLabelEn];

                /// <summary>
                /// Column unit_description_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColUnitDescriptionEn = Cols[TargetVariableMetadataJson.ColUnitDescriptionEn];


                /// <summary>
                /// Column ldsity_object_type_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColLocalDensityObjectTypeLabelCs = Cols[TargetVariableMetadataJson.ColLocalDensityObjectTypeLabelCs];

                /// <summary>
                /// Column ldsity_object_type_description_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColLocalDensityObjectTypeDescriptionCs = Cols[TargetVariableMetadataJson.ColLocalDensityObjectTypeDescriptionCs];

                /// <summary>
                /// Column ldsity_object_type_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColLocalDensityObjectTypeLabelEn = Cols[TargetVariableMetadataJson.ColLocalDensityObjectTypeLabelEn];

                /// <summary>
                /// Column ldsity_object_type_description_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColLocalDensityObjectTypeDescriptionEn = Cols[TargetVariableMetadataJson.ColLocalDensityObjectTypeDescriptionEn];


                /// <summary>
                /// Column ldsity_object_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColLocalDensityObjectLabelCs = Cols[TargetVariableMetadataJson.ColLocalDensityObjectLabelCs];

                /// <summary>
                /// Column ldsity_object_description_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColLocalDensityObjectDescriptionCs = Cols[TargetVariableMetadataJson.ColLocalDensityObjectDescriptionCs];

                /// <summary>
                /// Column ldsity_object_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColLocalDensityObjectLabelEn = Cols[TargetVariableMetadataJson.ColLocalDensityObjectLabelEn];

                /// <summary>
                /// Column ldsity_object_description_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColLocalDensityObjectDescriptionEn = Cols[TargetVariableMetadataJson.ColLocalDensityObjectDescriptionEn];


                /// <summary>
                /// Column ldsity_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColLocalDensityLabelCs = Cols[TargetVariableMetadataJson.ColLocalDensityLabelCs];

                /// <summary>
                /// Column ldsity_description_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColLocalDensityDescriptionCs = Cols[TargetVariableMetadataJson.ColLocalDensityDescriptionCs];

                /// <summary>
                /// Column ldsity_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColLocalDensityLabelEn = Cols[TargetVariableMetadataJson.ColLocalDensityLabelEn];

                /// <summary>
                /// Column ldsity_description_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColLocalDensityDescriptionEn = Cols[TargetVariableMetadataJson.ColLocalDensityDescriptionEn];


                /// <summary>
                /// Column use_negative_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColUseNegativeCs = Cols[TargetVariableMetadataJson.ColUseNegativeCs];

                /// <summary>
                /// Column use_negative_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColUseNegativeEn = Cols[TargetVariableMetadataJson.ColUseNegativeEn];


                /// <summary>
                /// Column version_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColVersionLabelCs = Cols[TargetVariableMetadataJson.ColVersionLabelCs];

                /// <summary>
                /// Column version_description_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColVersionDescriptionCs = Cols[TargetVariableMetadataJson.ColVersionDescriptionCs];

                /// <summary>
                /// Column version_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColVersionLabelEn = Cols[TargetVariableMetadataJson.ColVersionLabelEn];

                /// <summary>
                /// Column version_description_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColVersionDescriptionEn = Cols[TargetVariableMetadataJson.ColVersionDescriptionEn];


                /// <summary>
                /// Column definition_variant_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColDefinitionVariantLabelCs = Cols[TargetVariableMetadataJson.ColDefinitionVariantLabelCs];

                /// <summary>
                /// Column definition_variant_description_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColDefinitionVariantDescriptionCs = Cols[TargetVariableMetadataJson.ColDefinitionVariantDescriptionCs];

                /// <summary>
                /// Column definition_variant_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColDefinitionVariantLabelEn = Cols[TargetVariableMetadataJson.ColDefinitionVariantLabelEn];

                /// <summary>
                /// Column definition_variant_description_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColDefinitionVariantDescriptionEn = Cols[TargetVariableMetadataJson.ColDefinitionVariantDescriptionEn];


                /// <summary>
                /// Column area_domain_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColAreaDomainLabelCs = Cols[TargetVariableMetadataJson.ColAreaDomainLabelCs];

                /// <summary>
                /// Column area_domain_description_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColAreaDomainDescriptionCs = Cols[TargetVariableMetadataJson.ColAreaDomainDescriptionCs];

                /// <summary>
                /// Column area_domain_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColAreaDomainLabelEn = Cols[TargetVariableMetadataJson.ColAreaDomainLabelEn];

                /// <summary>
                /// Column area_domain_description_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColAreaDomainDescriptionEn = Cols[TargetVariableMetadataJson.ColAreaDomainDescriptionEn];


                /// <summary>
                /// Column population_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColPopulationLabelCs = Cols[TargetVariableMetadataJson.ColPopulationLabelCs];

                /// <summary>
                /// Column population_description_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColPopulationDescriptionCs = Cols[TargetVariableMetadataJson.ColPopulationDescriptionCs];

                /// <summary>
                /// Column population_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColPopulationLabelEn = Cols[TargetVariableMetadataJson.ColPopulationLabelEn];

                /// <summary>
                /// Column population_description_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColPopulationDescriptionEn = Cols[TargetVariableMetadataJson.ColPopulationDescriptionEn];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public FnEtlGetTargetVariableMetadataList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public FnEtlGetTargetVariableMetadataList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// List of target variable for ETL with metadata
                /// (in the NfiEsta schema)
                /// (read-only)
                /// </summary>
                public List<ITargetVariableMetadata> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new FnEtlGetTargetVariableMetadata(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// Target variable for ETL with metadata from list by identifier (read-only)
                /// </summary>
                /// <param name="id">Target variable for ETL with metadata identifier</param>
                /// <returns>Target variable for ETL with metadata from list by identifier (null if not found)</returns>
                public ITargetVariableMetadata this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new FnEtlGetTargetVariableMetadata(composite: this, data: a))
                                .FirstOrDefault<ITargetVariableMetadata>();
                    }
                }

                #endregion Indexer


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public FnEtlGetTargetVariableMetadataList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new FnEtlGetTargetVariableMetadataList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new FnEtlGetTargetVariableMetadataList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                /// <summary>
                /// List of target variable for ETL with metadata
                /// (in the NfiEsta schema)
                /// for selected target variables
                /// </summary>
                /// <param name="targetVariableId">Selected target variables identifiers</param>
                /// <returns>
                /// List of target variable for ETL with metadata
                /// (in the NfiEsta schema)
                /// for selected target variables
                /// </returns>
                public FnEtlGetTargetVariableMetadataList Reduce(
                    List<int> targetVariableId = null)
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();

                    if ((targetVariableId != null) && targetVariableId.Count != 0)
                    {
                        rows = rows.Where(a => targetVariableId.Contains<int>(
                                value: (int)(a.Field<Nullable<int>>(columnName: ColTargetVariableId.Name) ?? 0)));
                    }

                    return
                        rows.Any() ?
                        new FnEtlGetTargetVariableMetadataList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new FnEtlGetTargetVariableMetadataList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                /// <summary>
                /// List of target variable for ETL with metadata
                /// (in the NfiEsta schema)
                /// for selected metadata element
                /// </summary>
                /// <param name="elementType">Selected metadata element type</param>
                /// <param name="element">Selected metadata element value</param>
                /// <returns>
                /// List of target variable for ETL with metadata
                /// (in the NfiEsta schema)
                /// for selected metadata element
                /// </returns>
                public ITargetVariableMetadataList Reduce(
                    MetadataElementType elementType,
                    MetadataElement element)
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();

                    if (element != null)
                    {
                        rows = Items
                                .Where(a => a.MetadataElements[elementType].Equals(obj: element))
                                .Select(a => a.Data);
                    }

                    return
                        rows.Any() ?
                        new FnEtlGetTargetVariableMetadataList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new FnEtlGetTargetVariableMetadataList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                /// <summary>
                /// Create new empty target variable metadata table
                /// </summary>
                /// <returns>New empty target variable metadata table</returns>
                public ITargetVariableMetadataList Empty()
                {
                    return new FnEtlGetTargetVariableMetadataList(database: ((NfiEstaDB)Database));
                }

                /// <summary>
                /// List of metadata elements for selected element type
                /// </summary>
                /// <param name="elementType">Selected metadata element type</param>
                /// <returns>List of metadata elements for selected element type</returns>
                public List<MetadataElement> GetMetadataElementsOfType(
                    MetadataElementType elementType)
                {
                    return LanguageVersion switch
                    {
                        LanguageVersion.International =>
                            [.. Items
                                .Select(a => a.MetadataElements[elementType])
                                .Distinct<MetadataElement>()
                                .OrderBy(a => (a.TargetVariableId == 0) ? 0 : 1)
                                .ThenBy(a => a.LabelEn)],
                        LanguageVersion.National =>
                            [.. Items
                                .Select(a => a.MetadataElements[elementType])
                                .Distinct<MetadataElement>()
                                .OrderBy(a => (a.TargetVariableId == 0) ? 0 : 1)
                                .ThenBy(a => a.LabelCs)],
                        _ =>
                            [.. Items
                                .Select(a => a.MetadataElements[elementType])
                                .Distinct<MetadataElement>()
                                .OrderBy(a => a.TargetVariableId)],
                    };
                }

                /// <summary>
                /// Loads object data
                /// (if it was not done before)
                /// </summary>
                /// <param name="fnEtlGetTargetVariable">Result of the function fn_etl_get_target_variable</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                public void Load(
                    FnEtlGetTargetVariableTypeList fnEtlGetTargetVariable,
                    string condition = null,
                    Nullable<int> limit = null)
                {
                    if (!Loaded)
                    {
                        ReLoad(
                            fnEtlGetTargetVariable: fnEtlGetTargetVariable,
                            condition: condition,
                            limit: limit);
                    }
                }

                /// <summary>
                /// Loads object data
                /// </summary>
                /// <param name="fnEtlGetTargetVariable">Result of the function fn_etl_get_target_variable</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                public void ReLoad(
                    FnEtlGetTargetVariableTypeList fnEtlGetTargetVariable,
                    string condition = null,
                    Nullable<int> limit = null)
                {
                    Data = EmptyDataTable(
                                tableName: TableName,
                                columns: Columns);
                    Condition = condition;
                    Limit = limit;

                    System.Diagnostics.Stopwatch stopWatch = new();
                    stopWatch.Start();

                    int i = 0;
                    foreach (ITargetVariable item in fnEtlGetTargetVariable.Items)
                    {
                        // FnEtlGetTargetVariableType
                        TargetVariableMetadataJson json = new();
                        json.LoadFromText(text: item.MetadataText);

                        List<DataRow> jsonDataRows =
                            [.. json.Data.AsEnumerable()
                            .OrderBy(a => a.Field<int>(columnName: TargetVariableMetadataJson.ColId))];

                        foreach (DataRow row in jsonDataRows)
                        {
                            DataRow newRow = Data.NewRow();

                            Functions.SetIntArg(
                                row: newRow,
                                name: ColId.Name,
                                val: ++i);

                            Functions.SetIntArg(
                                row: newRow,
                                name: ColTargetVariableId.Name,
                                val: item.Id);

                            Functions.SetIntArg(
                                row: newRow,
                                name: ColLocalDensityId.Name,
                                val: Functions.GetIntArg(
                                    row: row,
                                    name: TargetVariableMetadataJson.ColLocalDensityId,
                                    defaultValue: 0));

                            Functions.SetBoolArg(
                                row: newRow,
                                name: ColJsonValidity.Name,
                                val: Functions.GetBoolArg(
                                    row: row,
                                    name: TargetVariableMetadataJson.ColJsonValidity,
                                    defaultValue: false));

                            #region Indicator

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColIndicatorLabelCs.Name,
                                val: Functions.GetStringArg(
                                    row: row,
                                    name: TargetVariableMetadataJson.ColIndicatorLabelCs,
                                    defaultValue: String.Empty));

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColIndicatorDescriptionCs.Name,
                                val: Functions.GetStringArg(
                                    row: row,
                                    name: TargetVariableMetadataJson.ColIndicatorDescriptionCs,
                                    defaultValue: String.Empty));

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColIndicatorLabelEn.Name,
                                val: Functions.GetStringArg(
                                    row: row,
                                    name: TargetVariableMetadataJson.ColIndicatorLabelEn,
                                    defaultValue: String.Empty));

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColIndicatorDescriptionEn.Name,
                                val: Functions.GetStringArg(
                                    row: row,
                                    name: TargetVariableMetadataJson.ColIndicatorDescriptionEn,
                                    defaultValue: String.Empty));

                            #endregion Indicator

                            #region StateOrChange

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColStateOrChangeLabelCs.Name,
                                val: Functions.GetStringArg(
                                    row: row,
                                    name: TargetVariableMetadataJson.ColStateOrChangeLabelCs,
                                    defaultValue: String.Empty));

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColStateOrChangeDescriptionCs.Name,
                                val: Functions.GetStringArg(
                                    row: row,
                                    name: TargetVariableMetadataJson.ColStateOrChangeDescriptionCs,
                                    defaultValue: String.Empty));

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColStateOrChangeLabelEn.Name,
                                val: Functions.GetStringArg(
                                    row: row,
                                    name: TargetVariableMetadataJson.ColStateOrChangeLabelEn,
                                    defaultValue: String.Empty));

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColStateOrChangeDescriptionEn.Name,
                                val: Functions.GetStringArg(
                                    row: row,
                                    name: TargetVariableMetadataJson.ColStateOrChangeDescriptionEn,
                                    defaultValue: String.Empty));

                            #endregion StateOrChange

                            #region Unit

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColUnitLabelCs.Name,
                                val: Functions.GetStringArg(
                                    row: row,
                                    name: TargetVariableMetadataJson.ColUnitLabelCs,
                                    defaultValue: String.Empty));

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColUnitDescriptionCs.Name,
                                val: Functions.GetStringArg(
                                    row: row,
                                    name: TargetVariableMetadataJson.ColUnitDescriptionCs,
                                    defaultValue: String.Empty));

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColUnitLabelEn.Name,
                                val: Functions.GetStringArg(
                                    row: row,
                                    name: TargetVariableMetadataJson.ColUnitLabelEn,
                                    defaultValue: String.Empty));

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColUnitDescriptionEn.Name,
                                val: Functions.GetStringArg(
                                    row: row,
                                    name: TargetVariableMetadataJson.ColUnitDescriptionEn,
                                    defaultValue: String.Empty));

                            #endregion Unit

                            #region LocalDensityObjectType

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColLocalDensityObjectTypeLabelCs.Name,
                                val: Functions.GetStringArg(
                                   row: row,
                                   name: TargetVariableMetadataJson.ColLocalDensityObjectTypeLabelCs,
                                   defaultValue: String.Empty));

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColLocalDensityObjectTypeDescriptionCs.Name,
                                val: Functions.GetStringArg(
                                   row: row,
                                   name: TargetVariableMetadataJson.ColLocalDensityObjectTypeDescriptionCs,
                                   defaultValue: String.Empty));

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColLocalDensityObjectTypeLabelEn.Name,
                                val: Functions.GetStringArg(
                                   row: row,
                                   name: TargetVariableMetadataJson.ColLocalDensityObjectTypeLabelEn,
                                   defaultValue: String.Empty));

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColLocalDensityObjectTypeDescriptionEn.Name,
                                val: Functions.GetStringArg(
                                   row: row,
                                   name: TargetVariableMetadataJson.ColLocalDensityObjectTypeDescriptionEn,
                                   defaultValue: String.Empty));

                            #endregion LocalDensityObjectType

                            #region LocalDensityObject

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColLocalDensityObjectLabelCs.Name,
                                val: Functions.GetStringArg(
                                    row: row,
                                    name: TargetVariableMetadataJson.ColLocalDensityObjectLabelCs,
                                    defaultValue: String.Empty));

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColLocalDensityObjectDescriptionCs.Name,
                                val: Functions.GetStringArg(
                                   row: row,
                                   name: TargetVariableMetadataJson.ColLocalDensityObjectDescriptionCs,
                                   defaultValue: String.Empty));

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColLocalDensityObjectLabelEn.Name,
                                val: Functions.GetStringArg(
                                   row: row,
                                   name: TargetVariableMetadataJson.ColLocalDensityObjectLabelEn,
                                   defaultValue: String.Empty));

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColLocalDensityObjectDescriptionEn.Name,
                                val: Functions.GetStringArg(
                                   row: row,
                                   name: TargetVariableMetadataJson.ColLocalDensityObjectDescriptionEn,
                                   defaultValue: String.Empty));

                            #endregion LocalDensityObject

                            #region LocalDensity

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColLocalDensityLabelCs.Name,
                                val: Functions.GetStringArg(
                                   row: row,
                                   name: TargetVariableMetadataJson.ColLocalDensityLabelCs,
                                   defaultValue: String.Empty));

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColLocalDensityDescriptionCs.Name,
                                val: Functions.GetStringArg(
                                   row: row,
                                   name: TargetVariableMetadataJson.ColLocalDensityDescriptionCs,
                                   defaultValue: String.Empty));

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColLocalDensityLabelEn.Name,
                                val: Functions.GetStringArg(
                                   row: row,
                                   name: TargetVariableMetadataJson.ColLocalDensityLabelEn,
                                   defaultValue: String.Empty));

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColLocalDensityDescriptionEn.Name,
                                val: Functions.GetStringArg(
                                   row: row,
                                   name: TargetVariableMetadataJson.ColLocalDensityDescriptionEn,
                                   defaultValue: String.Empty));

                            #endregion LocalDensity

                            #region UseNegative

                            Functions.SetNBoolArg(
                                row: newRow,
                                name: ColUseNegativeCs.Name,
                                val: Functions.GetNBoolArg(
                                    row: row,
                                    name: TargetVariableMetadataJson.ColUseNegativeCs,
                                    defaultValue: null));

                            Functions.SetNBoolArg(
                                row: newRow,
                                name: ColUseNegativeEn.Name,
                                val: Functions.GetNBoolArg(
                                   row: row,
                                   name: TargetVariableMetadataJson.ColUseNegativeEn,
                                   defaultValue: null));

                            #endregion UseNegative

                            #region Version

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColVersionLabelCs.Name,
                                val: Functions.GetStringArg(
                                  row: row,
                                  name: TargetVariableMetadataJson.ColVersionLabelCs,
                                  defaultValue: String.Empty));

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColVersionDescriptionCs.Name,
                                val: Functions.GetStringArg(
                                   row: row,
                                   name: TargetVariableMetadataJson.ColVersionDescriptionCs,
                                   defaultValue: String.Empty));

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColVersionLabelEn.Name,
                                val: Functions.GetStringArg(
                                   row: row,
                                   name: TargetVariableMetadataJson.ColVersionLabelEn,
                                   defaultValue: String.Empty));

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColVersionDescriptionEn.Name,
                                val: Functions.GetStringArg(
                                   row: row,
                                   name: TargetVariableMetadataJson.ColVersionDescriptionEn,
                                   defaultValue: String.Empty));

                            #endregion Version

                            #region DefinitionVariant

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColDefinitionVariantLabelCs.Name,
                                val: Functions.GetStringArg(
                                  row: row,
                                  name: TargetVariableMetadataJson.ColDefinitionVariantLabelCs,
                                  defaultValue: String.Empty));

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColDefinitionVariantDescriptionCs.Name,
                                val: Functions.GetStringArg(
                                   row: row,
                                   name: TargetVariableMetadataJson.ColDefinitionVariantDescriptionCs,
                                   defaultValue: String.Empty));

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColDefinitionVariantLabelEn.Name,
                                val: Functions.GetStringArg(
                                   row: row,
                                   name: TargetVariableMetadataJson.ColDefinitionVariantLabelEn,
                                   defaultValue: String.Empty));

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColDefinitionVariantDescriptionEn.Name,
                                val: Functions.GetStringArg(
                                   row: row,
                                   name: TargetVariableMetadataJson.ColDefinitionVariantDescriptionEn,
                                   defaultValue: String.Empty));

                            #endregion DefinitionVariant

                            #region AreaDomain

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColAreaDomainLabelCs.Name,
                                val: Functions.GetStringArg(
                                  row: row,
                                  name: TargetVariableMetadataJson.ColAreaDomainLabelCs,
                                  defaultValue: String.Empty));

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColAreaDomainDescriptionCs.Name,
                                val: Functions.GetStringArg(
                                   row: row,
                                   name: TargetVariableMetadataJson.ColAreaDomainDescriptionCs,
                                   defaultValue: String.Empty));

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColAreaDomainLabelEn.Name,
                                val: Functions.GetStringArg(
                                   row: row,
                                   name: TargetVariableMetadataJson.ColAreaDomainLabelEn,
                                   defaultValue: String.Empty));

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColAreaDomainDescriptionEn.Name,
                                val: Functions.GetStringArg(
                                   row: row,
                                   name: TargetVariableMetadataJson.ColAreaDomainDescriptionEn,
                                   defaultValue: String.Empty));

                            #endregion AreaDomain

                            #region Population

                            Functions.SetStringArg(
                               row: newRow,
                               name: ColPopulationLabelCs.Name,
                               val: Functions.GetStringArg(
                                  row: row,
                                  name: TargetVariableMetadataJson.ColPopulationLabelCs,
                                  defaultValue: String.Empty));

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColPopulationDescriptionCs.Name,
                                val: Functions.GetStringArg(
                                   row: row,
                                   name: TargetVariableMetadataJson.ColPopulationDescriptionCs,
                                   defaultValue: String.Empty));

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColPopulationLabelEn.Name,
                                val: Functions.GetStringArg(
                                   row: row,
                                   name: TargetVariableMetadataJson.ColPopulationLabelEn,
                                   defaultValue: String.Empty));

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColPopulationDescriptionEn.Name,
                                val: Functions.GetStringArg(
                                   row: row,
                                   name: TargetVariableMetadataJson.ColPopulationDescriptionEn,
                                   defaultValue: String.Empty));

                            #endregion Population

                            Data.Rows.Add(row: newRow);
                        }
                    }

                    if (!String.IsNullOrEmpty(value: Condition))
                    {
                        DataView view = new(table: Data);
                        try
                        {
                            view.RowFilter = Condition;
                        }
                        catch
                        {
                            view.RowFilter = String.Empty;
                            Condition = String.Empty;
                        }
                        Data = view.ToTable();
                    }

                    stopWatch.Stop();
                    LoadingTime = 0.001 * stopWatch.ElapsedMilliseconds;
                    Data.TableName = TableName;
                }

                /// <summary>
                /// List of target variable for ETL with metadata aggregated by target variable
                /// </summary>
                /// <returns>List of target variable for ETL with metadata aggregated by target variable</returns>
                [SupportedOSPlatform("windows")]
                public FnEtlGetTargetVariableMetadataList Aggregated()
                {
                    const string strCore = "core";
                    const string strDivision = "division";

                    if (Items == null)
                    {
                        return new FnEtlGetTargetVariableMetadataList(
                            database: (NfiEstaDB)Database);
                    }

                    if (Items.Count == 0)
                    {
                        return new FnEtlGetTargetVariableMetadataList(
                            database: (NfiEstaDB)Database);
                    }

                    if (Items
                            .Where(a => (a.LocalDensityObjectTypeLabelEn != strCore) &&
                                         (a.LocalDensityObjectTypeLabelEn != strDivision))
                            .Any())
                    {
                        System.Windows.Forms.MessageBox.Show(
                           text: $"Only local density object types {strCore} or {strDivision} are allowed.",
                           caption: "Invalid json file format:",
                           buttons: System.Windows.Forms.MessageBoxButtons.OK,
                           icon: System.Windows.Forms.MessageBoxIcon.Information);

                        return new FnEtlGetTargetVariableMetadataList(
                            database: (NfiEstaDB)Database);
                    }

                    return new FnEtlGetTargetVariableMetadataList
                    (
                        database: (NfiEstaDB)Database,
                        rows:
                            Items
                                .GroupBy(a => a.TargetVariableId)
                                .Select(g => new FnEtlGetTargetVariableMetadata()
                                {
                                    Id = g.Key,
                                    TargetVariableId = g.Key,
                                    LocalDensityId = 0,
                                    JsonValidity = g.Select(a => a.JsonValidity).Aggregate((aa, ab) => aa && ab),

                                    IndicatorLabelCs = g.Select(a => a.IndicatorLabelCs).FirstOrDefault(),
                                    IndicatorDescriptionCs = g.Select(a => a.IndicatorDescriptionCs).FirstOrDefault(),
                                    IndicatorLabelEn = g.Select(a => a.IndicatorLabelEn).FirstOrDefault(),
                                    IndicatorDescriptionEn = g.Select(a => a.IndicatorDescriptionEn).FirstOrDefault(),

                                    StateOrChangeLabelCs = g.Select(a => a.StateOrChangeLabelCs).FirstOrDefault(),
                                    StateOrChangeDescriptionCs = g.Select(a => a.StateOrChangeDescriptionCs).FirstOrDefault(),
                                    StateOrChangeLabelEn = g.Select(a => a.StateOrChangeLabelEn).FirstOrDefault(),
                                    StateOrChangeDescriptionEn = g.Select(a => a.StateOrChangeDescriptionEn).FirstOrDefault(),

                                    UnitLabelCs = g.Select(a => a.UnitLabelCs).FirstOrDefault(),
                                    UnitDescriptionCs = g.Select(a => a.UnitDescriptionCs).FirstOrDefault(),
                                    UnitLabelEn = g.Select(a => a.UnitLabelEn).FirstOrDefault(),
                                    UnitDescriptionEn = g.Select(a => a.UnitDescriptionEn).FirstOrDefault(),

                                    LocalDensityObjectTypeLabelCs =
                                        (g.Where(a => a.LocalDensityObjectTypeLabelEn == strCore).Any() &&
                                         g.Where(a => a.LocalDensityObjectTypeLabelEn == strDivision).Any()) ?
                                        String.Join(
                                            separator: " | ",
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strCore)
                                                .Select(a => a.LocalDensityObjectTypeLabelCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strDivision)
                                                .Select(a => a.LocalDensityObjectTypeLabelCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}")) :
                                            g
                                                .Select(a => a.LocalDensityObjectTypeLabelCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),

                                    LocalDensityObjectTypeDescriptionCs =
                                        (g.Where(a => a.LocalDensityObjectTypeLabelEn == strCore).Any() &&
                                         g.Where(a => a.LocalDensityObjectTypeLabelEn == strDivision).Any()) ?
                                        String.Join(
                                            separator: " | ",
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strCore)
                                                .Select(a => a.LocalDensityObjectTypeDescriptionCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strDivision)
                                                .Select(a => a.LocalDensityObjectTypeDescriptionCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}")) :
                                            g
                                                .Select(a => a.LocalDensityObjectTypeDescriptionCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),

                                    LocalDensityObjectTypeLabelEn =
                                        (g.Where(a => a.LocalDensityObjectTypeLabelEn == strCore).Any() &&
                                         g.Where(a => a.LocalDensityObjectTypeLabelEn == strDivision).Any()) ?
                                        String.Join(
                                            separator: " | ",
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strCore)
                                                .Select(a => a.LocalDensityObjectTypeLabelEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strDivision)
                                                .Select(a => a.LocalDensityObjectTypeLabelEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}")) :
                                            g
                                                .Select(a => a.LocalDensityObjectTypeLabelEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),

                                    LocalDensityObjectTypeDescriptionEn =
                                        (g.Where(a => a.LocalDensityObjectTypeLabelEn == strCore).Any() &&
                                         g.Where(a => a.LocalDensityObjectTypeLabelEn == strDivision).Any()) ?
                                        String.Join(
                                            separator: " | ",
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strCore)
                                                .Select(a => a.LocalDensityObjectTypeDescriptionEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strDivision)
                                                .Select(a => a.LocalDensityObjectTypeDescriptionEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}")) :
                                            g
                                                .Select(a => a.LocalDensityObjectTypeDescriptionEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),

                                    LocalDensityObjectLabelCs =
                                        (g.Where(a => a.LocalDensityObjectTypeLabelEn == strCore).Any() &&
                                         g.Where(a => a.LocalDensityObjectTypeLabelEn == strDivision).Any()) ?
                                        String.Join(
                                            separator: " | ",
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strCore)
                                                .Select(a => a.LocalDensityObjectLabelCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strDivision)
                                                .Select(a => a.LocalDensityObjectLabelCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}")) :
                                            g
                                                .Select(a => a.LocalDensityObjectLabelCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),

                                    LocalDensityObjectDescriptionCs =
                                        (g.Where(a => a.LocalDensityObjectTypeLabelEn == strCore).Any() &&
                                         g.Where(a => a.LocalDensityObjectTypeLabelEn == strDivision).Any()) ?
                                        String.Join(
                                            separator: " | ",
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strCore)
                                                .Select(a => a.LocalDensityObjectDescriptionCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strDivision)
                                                .Select(a => a.LocalDensityObjectDescriptionCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}")) :
                                            g
                                                .Select(a => a.LocalDensityObjectDescriptionCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),

                                    LocalDensityObjectLabelEn =
                                        (g.Where(a => a.LocalDensityObjectTypeLabelEn == strCore).Any() &&
                                         g.Where(a => a.LocalDensityObjectTypeLabelEn == strDivision).Any()) ?
                                        String.Join(
                                            separator: " | ",
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strCore)
                                                .Select(a => a.LocalDensityObjectLabelEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strDivision)
                                                .Select(a => a.LocalDensityObjectLabelEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}")) :
                                            g
                                                .Select(a => a.LocalDensityObjectLabelEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),

                                    LocalDensityObjectDescriptionEn =
                                        (g.Where(a => a.LocalDensityObjectTypeLabelEn == strCore).Any() &&
                                         g.Where(a => a.LocalDensityObjectTypeLabelEn == strDivision).Any()) ?
                                        String.Join(
                                            separator: " | ",
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strCore)
                                                .Select(a => a.LocalDensityObjectDescriptionEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strDivision)
                                                .Select(a => a.LocalDensityObjectDescriptionEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}")) :
                                            g
                                                .Select(a => a.LocalDensityObjectDescriptionEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),

                                    LocalDensityLabelCs =
                                        (g.Where(a => a.LocalDensityObjectTypeLabelEn == strCore).Any() &&
                                         g.Where(a => a.LocalDensityObjectTypeLabelEn == strDivision).Any()) ?
                                        String.Join(
                                            separator: " | ",
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strCore)
                                                .Select(a => a.LocalDensityLabelCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strDivision)
                                                .Select(a => a.LocalDensityLabelCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}")) :
                                            g
                                                .Select(a => a.LocalDensityLabelCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),

                                    LocalDensityDescriptionCs =
                                        (g.Where(a => a.LocalDensityObjectTypeLabelEn == strCore).Any() &&
                                         g.Where(a => a.LocalDensityObjectTypeLabelEn == strDivision).Any()) ?
                                        String.Join(
                                            separator: " | ",
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strCore)
                                                .Select(a => a.LocalDensityDescriptionCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strDivision)
                                                .Select(a => a.LocalDensityDescriptionCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}")) :
                                            g
                                                .Select(a => a.LocalDensityDescriptionCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),

                                    LocalDensityLabelEn =
                                        (g.Where(a => a.LocalDensityObjectTypeLabelEn == strCore).Any() &&
                                         g.Where(a => a.LocalDensityObjectTypeLabelEn == strDivision).Any()) ?
                                        String.Join(
                                            separator: " | ",
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strCore)
                                                .Select(a => a.LocalDensityLabelEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strDivision)
                                                .Select(a => a.LocalDensityLabelEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}")) :
                                            g
                                                .Select(a => a.LocalDensityLabelEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),

                                    LocalDensityDescriptionEn =
                                        (g.Where(a => a.LocalDensityObjectTypeLabelEn == strCore).Any() &&
                                         g.Where(a => a.LocalDensityObjectTypeLabelEn == strDivision).Any()) ?
                                        String.Join(
                                            separator: " | ",
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strCore)
                                                .Select(a => a.LocalDensityDescriptionEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strDivision)
                                                .Select(a => a.LocalDensityDescriptionEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}")) :
                                            g
                                                .Select(a => a.LocalDensityDescriptionEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),

                                    VersionLabelCs =
                                        (g.Where(a => a.LocalDensityObjectTypeLabelEn == strCore).Any() &&
                                         g.Where(a => a.LocalDensityObjectTypeLabelEn == strDivision).Any()) ?
                                        String.Join(
                                            separator: " | ",
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strCore)
                                                .Select(a => a.VersionLabelCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strDivision)
                                                .Select(a => a.VersionLabelCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}")) :
                                            g
                                                .Select(a => a.VersionLabelCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),

                                    VersionDescriptionCs =
                                        (g.Where(a => a.LocalDensityObjectTypeLabelEn == strCore).Any() &&
                                         g.Where(a => a.LocalDensityObjectTypeLabelEn == strDivision).Any()) ?
                                        String.Join(
                                            separator: " | ",
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strCore)
                                                .Select(a => a.VersionDescriptionCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strDivision)
                                                .Select(a => a.VersionDescriptionCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}")) :
                                            g
                                                .Select(a => a.VersionDescriptionCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),

                                    VersionLabelEn =
                                        (g.Where(a => a.LocalDensityObjectTypeLabelEn == strCore).Any() &&
                                         g.Where(a => a.LocalDensityObjectTypeLabelEn == strDivision).Any()) ?
                                        String.Join(
                                            separator: " | ",
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strCore)
                                                .Select(a => a.VersionLabelEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strDivision)
                                                .Select(a => a.VersionLabelEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}")) :
                                            g
                                                .Select(a => a.VersionLabelEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),

                                    VersionDescriptionEn =
                                        (g.Where(a => a.LocalDensityObjectTypeLabelEn == strCore).Any() &&
                                         g.Where(a => a.LocalDensityObjectTypeLabelEn == strDivision).Any()) ?
                                        String.Join(
                                            separator: " | ",
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strCore)
                                                .Select(a => a.VersionDescriptionEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strDivision)
                                                .Select(a => a.VersionDescriptionEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}")) :
                                            g
                                                .Select(a => a.VersionDescriptionEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),

                                    DefinitionVariantLabelCs =
                                        (g.Where(a => a.LocalDensityObjectTypeLabelEn == strCore).Any() &&
                                         g.Where(a => a.LocalDensityObjectTypeLabelEn == strDivision).Any()) ?
                                        String.Join(
                                            separator: " | ",
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strCore)
                                                .Select(a => a.DefinitionVariantLabelCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strDivision)
                                                .Select(a => a.DefinitionVariantLabelCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}")) :
                                            g
                                                .Select(a => a.DefinitionVariantLabelCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),

                                    DefinitionVariantDescriptionCs =
                                        (g.Where(a => a.LocalDensityObjectTypeLabelEn == strCore).Any() &&
                                         g.Where(a => a.LocalDensityObjectTypeLabelEn == strDivision).Any()) ?
                                        String.Join(
                                            separator: " | ",
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strCore)
                                                .Select(a => a.DefinitionVariantDescriptionCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strDivision)
                                                .Select(a => a.DefinitionVariantDescriptionCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}")) :
                                            g
                                                .Select(a => a.DefinitionVariantDescriptionCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),

                                    DefinitionVariantLabelEn =
                                        (g.Where(a => a.LocalDensityObjectTypeLabelEn == strCore).Any() &&
                                         g.Where(a => a.LocalDensityObjectTypeLabelEn == strDivision).Any()) ?
                                        String.Join(
                                            separator: " | ",
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strCore)
                                                .Select(a => a.DefinitionVariantLabelEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strDivision)
                                                .Select(a => a.DefinitionVariantLabelEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}")) :
                                            g
                                                .Select(a => a.DefinitionVariantLabelEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),

                                    DefinitionVariantDescriptionEn =
                                        (g.Where(a => a.LocalDensityObjectTypeLabelEn == strCore).Any() &&
                                         g.Where(a => a.LocalDensityObjectTypeLabelEn == strDivision).Any()) ?
                                        String.Join(
                                            separator: " | ",
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strCore)
                                                .Select(a => a.DefinitionVariantDescriptionEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strDivision)
                                                .Select(a => a.DefinitionVariantDescriptionEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}")) :
                                            g
                                                .Select(a => a.DefinitionVariantDescriptionEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),

                                    AreaDomainLabelCs =
                                        (g.Where(a => a.LocalDensityObjectTypeLabelEn == strCore).Any() &&
                                         g.Where(a => a.LocalDensityObjectTypeLabelEn == strDivision).Any()) ?
                                        String.Join(
                                            separator: " | ",
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strCore)
                                                .Select(a => a.AreaDomainLabelCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strDivision)
                                                .Select(a => a.AreaDomainLabelCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}")) :
                                            g
                                                .Select(a => a.AreaDomainLabelCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),

                                    AreaDomainDescriptionCs =
                                        (g.Where(a => a.LocalDensityObjectTypeLabelEn == strCore).Any() &&
                                         g.Where(a => a.LocalDensityObjectTypeLabelEn == strDivision).Any()) ?
                                        String.Join(
                                            separator: " | ",
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strCore)
                                                .Select(a => a.AreaDomainDescriptionCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strDivision)
                                                .Select(a => a.AreaDomainDescriptionCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}")) :
                                            g
                                                .Select(a => a.AreaDomainDescriptionCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),

                                    AreaDomainLabelEn =
                                        (g.Where(a => a.LocalDensityObjectTypeLabelEn == strCore).Any() &&
                                         g.Where(a => a.LocalDensityObjectTypeLabelEn == strDivision).Any()) ?
                                        String.Join(
                                            separator: " | ",
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strCore)
                                                .Select(a => a.AreaDomainLabelEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strDivision)
                                                .Select(a => a.AreaDomainLabelEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}")) :
                                            g
                                                .Select(a => a.AreaDomainLabelEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),

                                    AreaDomainDescriptionEn =
                                        (g.Where(a => a.LocalDensityObjectTypeLabelEn == strCore).Any() &&
                                         g.Where(a => a.LocalDensityObjectTypeLabelEn == strDivision).Any()) ?
                                        String.Join(
                                            separator: " | ",
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strCore)
                                                .Select(a => a.AreaDomainDescriptionEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strDivision)
                                                .Select(a => a.AreaDomainDescriptionEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}")) :
                                            g
                                                .Select(a => a.AreaDomainDescriptionEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),

                                    PopulationLabelCs =
                                        (g.Where(a => a.LocalDensityObjectTypeLabelEn == strCore).Any() &&
                                         g.Where(a => a.LocalDensityObjectTypeLabelEn == strDivision).Any()) ?
                                        String.Join(
                                            separator: " | ",
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strCore)
                                                .Select(a => a.PopulationLabelCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strDivision)
                                                .Select(a => a.PopulationLabelCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}")) :
                                            g
                                                .Select(a => a.PopulationLabelCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),

                                    PopulationDescriptionCs =
                                        (g.Where(a => a.LocalDensityObjectTypeLabelEn == strCore).Any() &&
                                         g.Where(a => a.LocalDensityObjectTypeLabelEn == strDivision).Any()) ?
                                        String.Join(
                                            separator: " | ",
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strCore)
                                                .Select(a => a.PopulationDescriptionCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strDivision)
                                                .Select(a => a.PopulationDescriptionCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}")) :
                                            g
                                                .Select(a => a.PopulationDescriptionCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),

                                    PopulationLabelEn =
                                        (g.Where(a => a.LocalDensityObjectTypeLabelEn == strCore).Any() &&
                                         g.Where(a => a.LocalDensityObjectTypeLabelEn == strDivision).Any()) ?
                                        String.Join(
                                            separator: " | ",
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strCore)
                                                .Select(a => a.PopulationLabelEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strDivision)
                                                .Select(a => a.PopulationLabelEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}")) :
                                            g
                                                .Select(a => a.PopulationLabelEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),

                                    PopulationDescriptionEn =
                                        (g.Where(a => a.LocalDensityObjectTypeLabelEn == strCore).Any() &&
                                         g.Where(a => a.LocalDensityObjectTypeLabelEn == strDivision).Any()) ?
                                        String.Join(
                                            separator: " | ",
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strCore)
                                                .Select(a => a.PopulationLabelEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strDivision)
                                                .Select(a => a.PopulationLabelEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}")) :
                                            g
                                                .Select(a => a.PopulationLabelEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}")
                                })
                                .Select(a => a.Data)
                    );
                }

                #endregion Methods

            }

        }
    }
}