﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {

            /// <summary>
            /// Group of estimation cells
            /// </summary>
            public class EstimationCellGroup
            {

                #region Properties


                /// <summary>
                /// List of estimation cells identifiers as text
                /// </summary>
                public string EstimationCellsIdsText { get; set; }

                /// <summary>
                /// Sorted list of estimation cell identifiers
                /// (read-only)
                /// </summary>
                public List<int> EstimationCellsIdsSortedList
                {
                    get
                    {
                        return
                            [..
                                Functions.StringToNIntList(
                                    text: EstimationCellsIdsText,
                                    separator: (char)59,
                                    defaultValue: null)
                                .Where(a => a != null)
                                .Select(a => (int)a)
                                .OrderBy(a => a) // (sorted list is required from comparer and aggregation)
                            ];
                    }
                }

                /// <summary>
                /// Sorted list of estimation cell identifiers as text
                /// (read-only)
                /// </summary>
                public string EstimationCellsIdsSortedText
                {
                    get
                    {
                        if (EstimationCellsIdsSortedList.Count == 0)
                        {
                            return String.Empty;
                        }

                        return
                            EstimationCellsIdsSortedList
                            .Select(a => a.ToString())
                            .Aggregate((a, b) => $"{a}{(char)59}{b}");
                    }
                }


                /// <summary>
                /// Country identifier
                /// </summary>
                public int CountryId { get; set; }

                /// <summary>
                /// Country
                /// </summary>
                public object Country { get; set; }


                /// <summary>
                /// Strata set identifier
                /// </summary>
                public int StrataSetId { get; set; }

                /// <summary>
                /// Strata set
                /// </summary>
                public object StrataSet { get; set; }


                /// <summary>
                /// Stratum identifier
                /// </summary>
                public int StratumId { get; set; }

                /// <summary>
                /// Stratum
                /// </summary>
                public object Stratum { get; set; }


                /// <summary>
                /// Panel reference year set group object
                /// </summary>
                public PanelRefYearSetGroup PanelRefYearSetGroup { get; set; }

                #endregion Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true|false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not EstimationCellGroup Type, return False
                    if (obj is not EstimationCellGroup)
                    {
                        return false;
                    }

                    return
                         (((EstimationCellGroup)obj).EstimationCellsIdsSortedText == EstimationCellsIdsSortedText) &&
                         (((EstimationCellGroup)obj).CountryId == CountryId) &&
                         (((EstimationCellGroup)obj).StrataSetId == StrataSetId) &&
                         (((EstimationCellGroup)obj).StratumId == StratumId);
                }

                /// <summary>
                /// Returns a hash code for the current object
                /// </summary>
                /// <returns>Hash code for the current object</returns>
                public override int GetHashCode()
                {
                    return
                        EstimationCellsIdsSortedText.GetHashCode() ^
                        CountryId.GetHashCode() ^
                        StrataSetId.GetHashCode() ^
                        StratumId.GetHashCode();
                }

                /// <summary>
                /// Returns a string that represents the current object
                /// </summary>
                /// <returns>String that represents the current object</returns>
                public override string ToString()
                {
                    return String.Concat(
                        $"{nameof(EstimationCellGroup)}: {{",
                        $"{nameof(EstimationCellsIdsSortedList)}: ",
                        $"[{Functions.PrepStringArg(arg: EstimationCellsIdsSortedText).Replace(oldValue: ((char)39).ToString(), newValue: String.Empty)}]; ",
                        $"{nameof(CountryId)}: {Functions.PrepIntArg(arg: CountryId)}; ",
                        $"{nameof(StrataSetId)}: {Functions.PrepIntArg(arg: StrataSetId)}; ",
                        $"{nameof(StratumId)}: {Functions.PrepIntArg(arg: StratumId)}; ",
                        $"{nameof(PanelRefYearSetGroup)}: {Functions.PrepNIntArg(arg: PanelRefYearSetGroup?.Id)}}}");
                }

                #endregion Methods

            }


            /// <summary>
            /// Aggregated group of estimation cells
            /// </summary>
            public class AggregatedEstimationCellGroup
            {

                #region Properties

                /// <summary>
                /// Sorted list of estimation cell identifiers in group (as list)
                /// (read-only)
                /// </summary>
                public List<int> EstimationCellsIdsSortedList
                {
                    get
                    {
                        return
                            [..
                                Functions.StringToNIntList(
                                    text: EstimationCellsIdsSortedText,
                                    separator: (char)59,
                                    defaultValue: null)
                                .Where(a => a != null)
                                .Select(a => (int)a)
                                .OrderBy(a => a)
                            ];
                    }
                }

                /// <summary>
                /// Sorted list of estimation cell identifiers in group (as text)
                /// </summary>
                public string EstimationCellsIdsSortedText { get; set; }


                /// <summary>
                /// List of country identifiers
                /// </summary>
                public List<int> CountryIds { get; set; }

                /// <summary>
                /// List of countries
                /// </summary>
                public List<object> Countries { get; set; }

                /// <summary>
                /// List of country identifiers as text
                /// (read-only)
                /// </summary>
                private string CountryIdsText
                {
                    get
                    {
                        if (CountryIds.Count == 0)
                        {
                            return String.Empty;
                        }

                        return
                            CountryIds
                            .Select(a => a.ToString())
                            .Aggregate((a, b) => $"{a}{(char)59}{b}");
                    }
                }


                /// <summary>
                /// List of strata set identifiers
                /// </summary>
                public List<int> StrataSetIds { get; set; }

                /// <summary>
                /// List of strata sets
                /// </summary>
                public List<object> StrataSets { get; set; }

                /// <summary>
                /// List of strata sets identifiers as text
                /// (read-only)
                /// </summary>
                private string StrataSetIdsText
                {
                    get
                    {
                        if (StrataSetIds.Count == 0)
                        {
                            return String.Empty;
                        }

                        return
                            StrataSetIds
                            .Select(a => a.ToString())
                            .Aggregate((a, b) => $"{a}{(char)59}{b}");
                    }
                }


                /// <summary>
                /// List of strata identifiers
                /// </summary>
                public List<int> StrataIds { get; set; }

                /// <summary>
                /// List of strata
                /// </summary>
                public List<object> Strata { get; set; }

                /// <summary>
                /// List of strata identifiers as text
                /// (read-only)
                /// </summary>
                private string StrataIdsText
                {
                    get
                    {
                        if (StrataIds.Count == 0)
                        {
                            return String.Empty;
                        }

                        return
                            StrataIds
                            .Select(a => a.ToString())
                            .Aggregate((a, b) => $"{a}{(char)59}{b}");
                    }
                }


                /// <summary>
                /// Partial panel reference year set groups
                /// </summary>
                public List<PartialPanelRefYearSetGroup> PartialPanelReferenceYearSetGroups { get; set; }

                /// <summary>
                /// List of partial panel reference year set groups
                /// (read-only)
                /// </summary>
                private string PartialPanelReferenceYearSetGroupsIdsText
                {
                    get
                    {
                        if ((PartialPanelReferenceYearSetGroups == null) ||
                            (PartialPanelReferenceYearSetGroups.Count == 0))
                        {
                            return String.Empty;
                        }

                        return
                            PartialPanelReferenceYearSetGroups
                            .Select(a => a.Id.ToString())
                            .Aggregate((a, b) => $"{a}{(char)59}{b}");
                    }
                }


                /// <summary>
                /// List of panel identifiers
                /// </summary>
                public List<Nullable<int>> PanelsIds { get; set; }

                /// <summary>
                /// List of panel identifiers as text
                /// </summary>
                private string PanelsIdsText
                {
                    get
                    {
                        if ((PanelsIds == null) || (PanelsIds.Count == 0))
                        {
                            return String.Empty;
                        }

                        return
                            PanelsIds
                            .Select(a => (a != null) ? a.ToString() : Functions.StrNull)
                            .Aggregate((a, b) => $"{a}{(char)59}{b}");
                    }
                }


                /// <summary>
                /// List of reference year set identifiers
                /// </summary>
                public List<Nullable<int>> ReferenceYearSetsIds { get; set; }

                /// <summary>
                /// List of panel identifiers as text
                /// </summary>
                private string ReferenceYearSetsIdsText
                {
                    get
                    {
                        if ((ReferenceYearSetsIds == null) || (ReferenceYearSetsIds.Count == 0))
                        {
                            return String.Empty;
                        }

                        return
                            ReferenceYearSetsIds
                            .Select(a => (a != null) ? a.ToString() : Functions.StrNull)
                            .Aggregate((a, b) => $"{a}{(char)59}{b}");
                    }
                }


                /// <summary>
                /// Global panel reference year set group for given lists of panels and referenece year sets identifiers
                /// </summary>
                public TFnApiGetGroupForPanelRefYearSetCombinations GlobalPanelReferenceYearSetGroup { get; set; }

                #endregion Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true|false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not AggregatedEstimationCellGroup Type, return False
                    if (obj is not AggregatedEstimationCellGroup)
                    {
                        return false;
                    }

                    return
                         (((AggregatedEstimationCellGroup)obj).EstimationCellsIdsSortedText == EstimationCellsIdsSortedText);
                }

                /// <summary>
                /// Returns a hash code for the current object
                /// </summary>
                /// <returns>Hash code for the current object</returns>
                public override int GetHashCode()
                {
                    return
                        EstimationCellsIdsSortedText.GetHashCode();
                }

                /// <summary>
                /// Returns a string that represents the current object
                /// </summary>
                /// <returns>String that represents the current object</returns>
                public override string ToString()
                {
                    return String.Concat(
                        $"{nameof(AggregatedEstimationCellGroup)}: {{",
                        $"{nameof(EstimationCellsIdsSortedList)}: ",
                        $"[{Functions.PrepStringArg(arg: EstimationCellsIdsSortedText)
                        .Replace(oldValue: ((char)39).ToString(), newValue: String.Empty)}]; ",
                        $"{nameof(CountryIds)}: ",
                        $"[{Functions.PrepStringArg(arg: CountryIdsText)
                        .Replace(oldValue: ((char)39).ToString(), newValue: String.Empty)}]; ",
                        $"{nameof(StrataSetIds)}: ",
                        $"[{Functions.PrepStringArg(arg: StrataSetIdsText)
                        .Replace(oldValue: ((char)39).ToString(), newValue: String.Empty)}]; ",
                        $"{nameof(StrataIds)}: ",
                        $"[{Functions.PrepStringArg(arg: StrataIdsText)
                        .Replace(oldValue: ((char)39).ToString(), newValue: String.Empty)}]; ",
                        $"{nameof(PartialPanelReferenceYearSetGroups)}: ",
                        $"[{Functions.PrepStringArg(arg: PartialPanelReferenceYearSetGroupsIdsText)
                        .Replace(oldValue: ((char)39).ToString(), newValue: String.Empty)}]; ",
                        $"{nameof(PanelsIds)}: ",
                        $"[{Functions.PrepStringArg(arg: PanelsIdsText)
                        .Replace(oldValue: ((char)39).ToString(), newValue: String.Empty)}]; ",
                        $"{nameof(ReferenceYearSetsIds)}: ",
                        $"[{Functions.PrepStringArg(arg: ReferenceYearSetsIdsText)
                        .Replace(oldValue: ((char)39).ToString(), newValue: String.Empty)}]; ",
                        $"{nameof(GlobalPanelReferenceYearSetGroup)}: {Functions.PrepNIntArg(arg: GlobalPanelReferenceYearSetGroup?.Id)}}}");
                }

                #endregion Methods

            }


            /// <summary>
            /// Partial panel reference year set group for stratum (country-strataset-stratum)
            /// </summary>
            public class PartialPanelRefYearSetGroup
            {

                #region Properties

                /// <summary>
                /// Panel reference year set group identifier
                /// </summary>
                public int Id
                {
                    get
                    {
                        return PanelRefYearSetGroup?.Id ?? 0;
                    }
                }

                /// <summary>
                /// Panel reference year set group
                /// </summary>
                public PanelRefYearSetGroup PanelRefYearSetGroup { get; set; }

                /// <summary>
                /// Country
                /// </summary>
                public object Country { get; set; }

                /// <summary>
                /// Strata set
                /// </summary>
                public object StrataSet { get; set; }

                /// <summary>
                /// Stratum
                /// </summary>
                public object Stratum { get; set; }

                #endregion Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true|false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not PartialPanelRefYearSetGroup Type, return False
                    if (obj is not PartialPanelRefYearSetGroup)
                    {
                        return false;
                    }

                    return ((PartialPanelRefYearSetGroup)obj).Id == Id;
                }

                /// <summary>
                /// Returns a hash code for the current object
                /// </summary>
                /// <returns>Hash code for the current object</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                /// <summary>
                /// Returns a string that represents the current object
                /// </summary>
                /// <returns>String that represents the current object</returns>
                public override string ToString()
                {
                    return String.Concat(
                        $"{nameof(PartialPanelRefYearSetGroup)}: {{",
                        $"{nameof(Id)}: {Functions.PrepIntArg(arg: Id)}}}");
                }

                #endregion Methods

            }

        }
    }
}