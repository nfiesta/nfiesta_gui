﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {
            // fn_api_do_labels_and_descriptions_of_estimation_period_exist

            /// <summary>
            /// Estimation period checks of label and description values
            /// (return type of the stored procedure fn_api_do_labels_and_descriptions_of_estimation_period_exist)
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class TFnApiDoLabelsAndDescriptionsOfEstimationPeriodExist(
                TFnApiDoLabelsAndDescriptionsOfEstimationPeriodExistList composite = null,
                DataRow data = null)
                    : AParametrizedViewEntry<TFnApiDoLabelsAndDescriptionsOfEstimationPeriodExistList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TFnApiDoLabelsAndDescriptionsOfEstimationPeriodExistList.ColId.Name,
                            defaultValue: Int32.Parse(s: TFnApiDoLabelsAndDescriptionsOfEstimationPeriodExistList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TFnApiDoLabelsAndDescriptionsOfEstimationPeriodExistList.ColId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Estimation period with same label in national language has been found in the database table
                /// </summary>
                public Nullable<bool> LabelCsFound
                {
                    get
                    {
                        return Functions.GetNBoolArg(
                            row: Data,
                            name: TFnApiDoLabelsAndDescriptionsOfEstimationPeriodExistList.ColLabelCsFound.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnApiDoLabelsAndDescriptionsOfEstimationPeriodExistList.ColLabelCsFound.DefaultValue) ?
                                (Nullable<bool>)null :
                                Boolean.Parse(value: TFnApiDoLabelsAndDescriptionsOfEstimationPeriodExistList.ColLabelCsFound.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNBoolArg(
                            row: Data,
                            name: TFnApiDoLabelsAndDescriptionsOfEstimationPeriodExistList.ColLabelCsFound.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Estimation period with same label in English has been found in the database table
                /// </summary>
                public Nullable<bool> LabelEnFound
                {
                    get
                    {
                        return Functions.GetNBoolArg(
                            row: Data,
                            name: TFnApiDoLabelsAndDescriptionsOfEstimationPeriodExistList.ColLabelEnFound.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnApiDoLabelsAndDescriptionsOfEstimationPeriodExistList.ColLabelEnFound.DefaultValue) ?
                                (Nullable<bool>)null :
                                Boolean.Parse(value: TFnApiDoLabelsAndDescriptionsOfEstimationPeriodExistList.ColLabelEnFound.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNBoolArg(
                            row: Data,
                            name: TFnApiDoLabelsAndDescriptionsOfEstimationPeriodExistList.ColLabelEnFound.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Estimation period with same description in national language has been found in the database table
                /// </summary>
                public Nullable<bool> DescriptionCsFound
                {
                    get
                    {
                        return Functions.GetNBoolArg(
                            row: Data,
                            name: TFnApiDoLabelsAndDescriptionsOfEstimationPeriodExistList.ColDescriptionCsFound.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnApiDoLabelsAndDescriptionsOfEstimationPeriodExistList.ColDescriptionCsFound.DefaultValue) ?
                                (Nullable<bool>)null :
                                Boolean.Parse(value: TFnApiDoLabelsAndDescriptionsOfEstimationPeriodExistList.ColDescriptionCsFound.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNBoolArg(
                            row: Data,
                            name: TFnApiDoLabelsAndDescriptionsOfEstimationPeriodExistList.ColDescriptionCsFound.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Estimation period with same description in English has been found in the database table
                /// </summary>
                public Nullable<bool> DescriptionEnFound
                {
                    get
                    {
                        return Functions.GetNBoolArg(
                            row: Data,
                            name: TFnApiDoLabelsAndDescriptionsOfEstimationPeriodExistList.ColDescriptionEnFound.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnApiDoLabelsAndDescriptionsOfEstimationPeriodExistList.ColDescriptionEnFound.DefaultValue) ?
                                (Nullable<bool>)null :
                                Boolean.Parse(value: TFnApiDoLabelsAndDescriptionsOfEstimationPeriodExistList.ColDescriptionEnFound.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNBoolArg(
                            row: Data,
                            name: TFnApiDoLabelsAndDescriptionsOfEstimationPeriodExistList.ColDescriptionEnFound.Name,
                            val: value);
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Specified object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not TFnApiDoLabelsAndDescriptionsOfEstimationPeriodExist, return False
                    if (obj is not TFnApiDoLabelsAndDescriptionsOfEstimationPeriodExist)
                    {
                        return false;
                    }

                    return
                        Id == ((TFnApiDoLabelsAndDescriptionsOfEstimationPeriodExist)obj).Id;
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                /// <summary>
                /// Text description of the object
                /// </summary>
                /// <returns>Text description of the object</returns>
                public override string ToString()
                {
                    string labelCsFound =
                        (LabelCsFound == null)
                            ? Functions.StrNull
                            : ((bool)LabelCsFound).ToString();

                    string labelEnFound =
                        (LabelEnFound == null)
                            ? Functions.StrNull
                            : ((bool)LabelEnFound).ToString();

                    string descriptionCsFound =
                        (DescriptionCsFound == null)
                            ? Functions.StrNull
                            : ((bool)DescriptionCsFound).ToString();

                    string descriptionEnFound =
                        (DescriptionEnFound == null)
                            ? Functions.StrNull
                            : ((bool)DescriptionEnFound).ToString();

                    return String.Concat(
                        $"{nameof(Id)}: {Id}, ",
                        $"{nameof(LabelCsFound)}: {labelCsFound}, ",
                        $"{nameof(LabelEnFound)}: {labelEnFound}, ",
                        $"{nameof(DescriptionCsFound)}: {descriptionCsFound}, ",
                        $"{nameof(DescriptionEnFound)}: {descriptionEnFound}");
                }

                #endregion Methods

            }


            /// <summary>
            /// List of estimation period checks of label and description values
            /// (return type of the stored procedure fn_api_do_labels_and_descriptions_of_estimation_period_exist)
            /// </summary>
            public class TFnApiDoLabelsAndDescriptionsOfEstimationPeriodExistList
                 : AParametrizedView
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema =
                    NfiEstaFunctions.FnApiDoLabelsAndDescriptionsOfEstimationPeriodExist.SchemaName;

                /// <summary>
                /// Stored procedure name
                /// </summary>
                public static readonly string Name =
                    NfiEstaFunctions.FnApiDoLabelsAndDescriptionsOfEstimationPeriodExist.Name;

                /// <summary>
                /// Stored procedure alias
                /// </summary>
                public static readonly string Alias =
                    NfiEstaFunctions.FnApiDoLabelsAndDescriptionsOfEstimationPeriodExist.Name;

                /// <summary>
                /// Description in national language
                /// </summary>
                public static readonly string CaptionCs = String.Concat(
                    "Seznam kontrol popisků a popisů výpočetního období ",
                    "(návratový typ uložené procedury fn_api_do_labels_and_descriptions_of_estimation_period_exist)");

                /// <summary>
                /// Description in English
                /// </summary>
                public static readonly string CaptionEn = String.Concat(
                    "List of estimation period checks of label and description values ",
                    "(return type of the fn_api_do_labels_and_descriptions_of_estimation_period_exist stored procedure )");

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
                {
                    { "id", new ColumnMetadata()
                    {
                        Name = "id",
                        DbName = "id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = true,
                        NotNull = true,
                        DefaultValue = "0",
                        FuncCall = "row_number() over()",
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ID",
                        HeaderTextEn = "ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 0
                    }
                    },
                    { "label_cs_found", new ColumnMetadata()
                    {
                        Name = "label_cs_found",
                        DbName = "_label_exists",
                        DataType = "System.Boolean",
                        DbDataType = "bool",
                        NewDataType = "bool",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "LABEL_CS_FOUND",
                        HeaderTextEn = "LABEL_CS_FOUND",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 1
                    }
                    },
                    { "label_en_found", new ColumnMetadata()
                    {
                        Name = "label_en_found",
                        DbName = "_label_en_exists",
                        DataType = "System.Boolean",
                        DbDataType = "bool",
                        NewDataType = "bool",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "LABEL_EN_FOUND",
                        HeaderTextEn = "LABEL_EN_FOUND",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 2
                    }
                    },
                    { "description_cs_found", new ColumnMetadata()
                    {
                        Name = "description_cs_found",
                        DbName = "_description_exists",
                        DataType = "System.Boolean",
                        DbDataType = "bool",
                        NewDataType = "bool",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "DESCRIPTION_CS_FOUND",
                        HeaderTextEn = "DESCRIPTION_CS_FOUND",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 3
                    }
                    },
                    { "description_en_found", new ColumnMetadata()
                    {
                        Name = "description_en_found",
                        DbName = "_description_en_exists",
                        DataType = "System.Boolean",
                        DbDataType = "bool",
                        NewDataType = "bool",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "DESCRIPTION_EN_FOUND",
                        HeaderTextEn = "DESCRIPTION_EN_FOUND",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 4
                    }
                    }
                };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column label_cs_found metadata
                /// </summary>
                public static readonly ColumnMetadata ColLabelCsFound = Cols["label_cs_found"];

                /// <summary>
                /// Column label_en_found metadata
                /// </summary>
                public static readonly ColumnMetadata ColLabelEnFound = Cols["label_en_found"];

                /// <summary>
                /// Column description_cs_found metadata
                /// </summary>
                public static readonly ColumnMetadata ColDescriptionCsFound = Cols["description_cs_found"];

                /// <summary>
                /// Column description_en_found metadata
                /// </summary>
                public static readonly ColumnMetadata ColDescriptionEnFound = Cols["description_en_found"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Private Fields

                /// <summary>
                /// 1st parameter: Label of estimation period in national language
                /// </summary>
                private string labelCs;

                /// <summary>
                /// 2nd parameter: Label of estimation period in English
                /// </summary>
                private string labelEn;

                /// <summary>
                /// 3rd parameter: Description of estimation period in national language
                /// </summary>
                private string descriptionCs;

                /// <summary>
                /// 4th parameter: Description of estimation period in English
                /// </summary>
                private string descriptionEn;

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TFnApiDoLabelsAndDescriptionsOfEstimationPeriodExistList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                {
                    LabelCs = null;
                    LabelEn = null;
                    DescriptionCs = null;
                    DescriptionEn = null;
                }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TFnApiDoLabelsAndDescriptionsOfEstimationPeriodExistList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                {
                    LabelCs = null;
                    LabelEn = null;
                    DescriptionCs = null;
                    DescriptionEn = null;
                }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// 1st parameter: Label of estimation period in national language
                /// </summary>
                public string LabelCs
                {
                    get
                    {
                        return labelCs;
                    }
                    set
                    {
                        labelCs = value;
                    }
                }

                /// <summary>
                /// 2nd parameter: Label of estimation period in English
                /// </summary>
                public string LabelEn
                {
                    get
                    {
                        return labelEn;
                    }
                    set
                    {
                        labelEn = value;
                    }
                }

                /// <summary>
                /// 3rd parameter: Description of estimation period in national language
                /// </summary>
                public string DescriptionCs
                {
                    get
                    {
                        return descriptionCs;
                    }
                    set
                    {
                        descriptionCs = value;
                    }
                }

                /// <summary>
                /// 4th parameter: Description of estimation period in English
                /// </summary>
                public string DescriptionEn
                {
                    get
                    {
                        return descriptionEn;
                    }
                    set
                    {
                        descriptionEn = value;
                    }
                }

                /// <summary>
                /// List of items (read-only)
                /// </summary>
                public List<TFnApiDoLabelsAndDescriptionsOfEstimationPeriodExist> Items
                {
                    get
                    {
                        return
                            [..
                                Data.AsEnumerable()
                                .Select(a => new TFnApiDoLabelsAndDescriptionsOfEstimationPeriodExist(composite: this, data: a))
                            ];
                    }
                }

                #endregion Properties


                #region Indexers

                /// <summary>
                /// Row from list of rows by identifier (read-only)
                /// </summary>
                /// <param name="id">Row identifier</param>
                /// <returns>Row from list of rows by identifier (null if not found)</returns>
                public TFnApiDoLabelsAndDescriptionsOfEstimationPeriodExist this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new TFnApiDoLabelsAndDescriptionsOfEstimationPeriodExist(composite: this, data: a))
                                .FirstOrDefault<TFnApiDoLabelsAndDescriptionsOfEstimationPeriodExist>();
                    }
                }

                #endregion Indexers


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public TFnApiDoLabelsAndDescriptionsOfEstimationPeriodExistList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any()
                        ? new TFnApiDoLabelsAndDescriptionsOfEstimationPeriodExistList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime)
                        {
                            LabelCs = LabelCs,
                            LabelEn = LabelEn,
                            DescriptionCs = DescriptionCs,
                            DescriptionEn = DescriptionEn
                        }
                        : new TFnApiDoLabelsAndDescriptionsOfEstimationPeriodExistList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null)
                        {
                            LabelCs = LabelCs,
                            LabelEn = LabelEn,
                            DescriptionCs = DescriptionCs,
                            DescriptionEn = DescriptionEn
                        };
                }

                #endregion Methods

            }

        }
    }
}
