﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {
            // fn_api_get_data_options4single_phase_config

            /// <summary>
            /// Combination of countries, strata sets, strata, panels, reference year sets that intersect within any estimation cell
            /// and for which all required variables are available
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class TFnApiGetDataOptionsForSinglePhaseConfig(
                TFnApiGetDataOptionsForSinglePhaseConfigList composite = null,
                DataRow data = null)
                    : AParametrizedViewEntry<TFnApiGetDataOptionsForSinglePhaseConfigList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TFnApiGetDataOptionsForSinglePhaseConfigList.ColId.Name,
                            defaultValue: Int32.Parse(s: TFnApiGetDataOptionsForSinglePhaseConfigList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TFnApiGetDataOptionsForSinglePhaseConfigList.ColId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Estimation cells identifiers as text
                /// </summary>
                public string EstimationCellsIdsText
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetDataOptionsForSinglePhaseConfigList.ColEstimationCells.Name,
                            defaultValue: TFnApiGetDataOptionsForSinglePhaseConfigList.ColEstimationCells.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetDataOptionsForSinglePhaseConfigList.ColEstimationCells.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Estimation cells identifiers as list (read-only)
                /// </summary>
                public List<Nullable<int>> EstimationCellsIdsList
                {
                    get
                    {
                        return Functions.StringToNIntList(
                            text: EstimationCellsIdsText,
                            separator: (char)59,
                            defaultValue: null);
                    }
                }


                /// <summary>
                /// Country identifier
                /// </summary>
                public Nullable<int> CountryId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TFnApiGetDataOptionsForSinglePhaseConfigList.ColCountryId.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnApiGetDataOptionsForSinglePhaseConfigList.ColCountryId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TFnApiGetDataOptionsForSinglePhaseConfigList.ColCountryId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TFnApiGetDataOptionsForSinglePhaseConfigList.ColCountryId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Country label in national language
                /// </summary>
                public string CountryLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetDataOptionsForSinglePhaseConfigList.ColCountryLabelCs.Name,
                            defaultValue: TFnApiGetDataOptionsForSinglePhaseConfigList.ColCountryLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetDataOptionsForSinglePhaseConfigList.ColCountryLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Country description in national language
                /// </summary>
                public string CountryDescriptionCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetDataOptionsForSinglePhaseConfigList.ColCountryDescriptionCs.Name,
                            defaultValue: TFnApiGetDataOptionsForSinglePhaseConfigList.ColCountryDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetDataOptionsForSinglePhaseConfigList.ColCountryDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Country extended label in national language (read-only)
                /// </summary>
                public string CountryExtendedLabelCs
                {
                    get
                    {
                        return
                            (CountryId == null)
                                ? String.IsNullOrEmpty(value: CountryLabelCs)
                                    ? String.Empty
                                    : CountryLabelCs
                                : String.IsNullOrEmpty(value: CountryLabelCs)
                                    ? $"{CountryId}"
                                    : $"{CountryId} - {CountryLabelCs}";
                    }
                }

                /// <summary>
                /// Country label in English
                /// </summary>
                public string CountryLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetDataOptionsForSinglePhaseConfigList.ColCountryLabelEn.Name,
                            defaultValue: TFnApiGetDataOptionsForSinglePhaseConfigList.ColCountryLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetDataOptionsForSinglePhaseConfigList.ColCountryLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Country description in English
                /// </summary>
                public string CountryDescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetDataOptionsForSinglePhaseConfigList.ColCountryDescriptionEn.Name,
                            defaultValue: TFnApiGetDataOptionsForSinglePhaseConfigList.ColCountryDescriptionEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetDataOptionsForSinglePhaseConfigList.ColCountryDescriptionEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Country extended label in English (read-only)
                /// </summary>
                public string CountryExtendedLabelEn
                {
                    get
                    {
                        return
                            (CountryId == null)
                                ? String.IsNullOrEmpty(value: CountryLabelEn)
                                    ? String.Empty
                                    : CountryLabelEn
                                : String.IsNullOrEmpty(value: CountryLabelEn)
                                    ? $"{CountryId}"
                                    : $"{CountryId} - {CountryLabelEn}";
                    }
                }


                /// <summary>
                /// Strata set identifier
                /// </summary>
                public Nullable<int> StrataSetId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TFnApiGetDataOptionsForSinglePhaseConfigList.ColStrataSetId.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnApiGetDataOptionsForSinglePhaseConfigList.ColStrataSetId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TFnApiGetDataOptionsForSinglePhaseConfigList.ColStrataSetId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TFnApiGetDataOptionsForSinglePhaseConfigList.ColStrataSetId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Strata set label in national language
                /// </summary>
                public string StrataSetLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetDataOptionsForSinglePhaseConfigList.ColStrataSetLabelCs.Name,
                            defaultValue: TFnApiGetDataOptionsForSinglePhaseConfigList.ColStrataSetLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetDataOptionsForSinglePhaseConfigList.ColStrataSetLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Strata set description in national language
                /// </summary>
                public string StrataSetDescriptionCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetDataOptionsForSinglePhaseConfigList.ColStrataSetDescriptionCs.Name,
                            defaultValue: TFnApiGetDataOptionsForSinglePhaseConfigList.ColStrataSetDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetDataOptionsForSinglePhaseConfigList.ColStrataSetDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Strata set extended label in national language (read-only)
                /// </summary>
                public string StrataSetExtendedLabelCs
                {
                    get
                    {
                        return
                            (StrataSetId == null)
                                ? String.IsNullOrEmpty(value: StrataSetLabelCs)
                                    ? String.Empty
                                    : StrataSetLabelCs
                                : String.IsNullOrEmpty(value: StrataSetLabelCs)
                                    ? $"{StrataSetId}"
                                    : $"{StrataSetId} - {StrataSetLabelCs}";
                    }
                }

                /// <summary>
                /// Strata set label in English
                /// </summary>
                public string StrataSetLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetDataOptionsForSinglePhaseConfigList.ColStrataSetLabelEn.Name,
                            defaultValue: TFnApiGetDataOptionsForSinglePhaseConfigList.ColStrataSetLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetDataOptionsForSinglePhaseConfigList.ColStrataSetLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Strata set description in English
                /// </summary>
                public string StrataSetDescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetDataOptionsForSinglePhaseConfigList.ColStrataSetDescriptionEn.Name,
                            defaultValue: TFnApiGetDataOptionsForSinglePhaseConfigList.ColStrataSetDescriptionEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetDataOptionsForSinglePhaseConfigList.ColStrataSetDescriptionEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Strata set extended label in English (read-only)
                /// </summary>
                public string StrataSetExtendedLabelEn
                {
                    get
                    {
                        return
                            (StrataSetId == null)
                                ? String.IsNullOrEmpty(value: StrataSetLabelEn)
                                    ? String.Empty
                                    : StrataSetLabelEn
                                : String.IsNullOrEmpty(value: StrataSetLabelEn)
                                    ? $"{StrataSetId}"
                                    : $"{StrataSetId} - {StrataSetLabelEn}";
                    }
                }


                /// <summary>
                /// Stratum identifier
                /// </summary>
                public Nullable<int> StratumId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TFnApiGetDataOptionsForSinglePhaseConfigList.ColStratumId.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnApiGetDataOptionsForSinglePhaseConfigList.ColStratumId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TFnApiGetDataOptionsForSinglePhaseConfigList.ColStratumId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TFnApiGetDataOptionsForSinglePhaseConfigList.ColStratumId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Stratum label in national language
                /// </summary>
                public string StratumLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetDataOptionsForSinglePhaseConfigList.ColStratumLabelCs.Name,
                            defaultValue: TFnApiGetDataOptionsForSinglePhaseConfigList.ColStratumLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetDataOptionsForSinglePhaseConfigList.ColStratumLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Stratum description in national language
                /// </summary>
                public string StratumDescriptionCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetDataOptionsForSinglePhaseConfigList.ColStratumDescriptionCs.Name,
                            defaultValue: TFnApiGetDataOptionsForSinglePhaseConfigList.ColStratumDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetDataOptionsForSinglePhaseConfigList.ColStratumDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Stratum extended label in national language (read-only)
                /// </summary>
                public string StratumExtendedLabelCs
                {
                    get
                    {
                        return
                            (StratumId == null)
                                ? String.IsNullOrEmpty(value: StratumLabelCs)
                                    ? String.Empty
                                    : StratumLabelCs
                                : String.IsNullOrEmpty(value: StratumLabelCs)
                                    ? $"{StratumId}"
                                    : $"{StratumId} - {StratumLabelCs}";
                    }
                }

                /// <summary>
                /// Stratum label in English
                /// </summary>
                public string StratumLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetDataOptionsForSinglePhaseConfigList.ColStratumLabelEn.Name,
                            defaultValue: TFnApiGetDataOptionsForSinglePhaseConfigList.ColStratumLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetDataOptionsForSinglePhaseConfigList.ColStratumLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Stratum description in English
                /// </summary>
                public string StratumDescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetDataOptionsForSinglePhaseConfigList.ColStratumDescriptionEn.Name,
                            defaultValue: TFnApiGetDataOptionsForSinglePhaseConfigList.ColStratumDescriptionEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetDataOptionsForSinglePhaseConfigList.ColStratumDescriptionEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Stratum extended label in English (read-only)
                /// </summary>
                public string StratumExtendedLabelEn
                {
                    get
                    {
                        return
                            (StratumId == null)
                                ? String.IsNullOrEmpty(value: StratumLabelEn)
                                    ? String.Empty
                                    : StratumLabelEn
                                : String.IsNullOrEmpty(value: StratumLabelEn)
                                    ? $"{StratumId}"
                                    : $"{StratumId} - {StratumLabelEn}";
                    }
                }


                /// <summary>
                /// Panel identifier
                /// </summary>
                public Nullable<int> PanelId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TFnApiGetDataOptionsForSinglePhaseConfigList.ColPanelId.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnApiGetDataOptionsForSinglePhaseConfigList.ColPanelId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TFnApiGetDataOptionsForSinglePhaseConfigList.ColPanelId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TFnApiGetDataOptionsForSinglePhaseConfigList.ColPanelId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Panel label in national language
                /// </summary>
                public string PanelLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetDataOptionsForSinglePhaseConfigList.ColPanelLabelCs.Name,
                            defaultValue: TFnApiGetDataOptionsForSinglePhaseConfigList.ColPanelLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetDataOptionsForSinglePhaseConfigList.ColPanelLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Panel description in national language
                /// </summary>
                public string PanelDescriptionCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetDataOptionsForSinglePhaseConfigList.ColPanelDescriptionCs.Name,
                            defaultValue: TFnApiGetDataOptionsForSinglePhaseConfigList.ColPanelDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetDataOptionsForSinglePhaseConfigList.ColPanelDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Panel extended label in national language (read-only)
                /// </summary>
                public string PanelExtendedLabelCs
                {
                    get
                    {
                        return
                            (PanelId == null)
                                ? String.IsNullOrEmpty(value: PanelLabelCs)
                                    ? String.Empty
                                    : PanelLabelCs
                                : String.IsNullOrEmpty(value: PanelLabelCs)
                                    ? $"{PanelId}"
                                    : $"{PanelId} - {PanelLabelCs}";
                    }
                }

                /// <summary>
                /// Panel label in English
                /// </summary>
                public string PanelLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetDataOptionsForSinglePhaseConfigList.ColPanelLabelEn.Name,
                            defaultValue: TFnApiGetDataOptionsForSinglePhaseConfigList.ColPanelLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetDataOptionsForSinglePhaseConfigList.ColPanelLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Panel description in English
                /// </summary>
                public string PanelDescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetDataOptionsForSinglePhaseConfigList.ColPanelDescriptionEn.Name,
                            defaultValue: TFnApiGetDataOptionsForSinglePhaseConfigList.ColPanelDescriptionEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetDataOptionsForSinglePhaseConfigList.ColPanelDescriptionEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Panel extended label in English (read-only)
                /// </summary>
                public string PanelExtendedLabelEn
                {
                    get
                    {
                        return
                            (PanelId == null)
                                ? String.IsNullOrEmpty(value: PanelLabelEn)
                                    ? String.Empty
                                    : PanelLabelEn
                                : String.IsNullOrEmpty(value: PanelLabelEn)
                                    ? $"{PanelId}"
                                    : $"{PanelId} - {PanelLabelEn}";
                    }
                }


                /// <summary>
                /// Reference year set identifier
                /// </summary>
                public Nullable<int> ReferenceYearSetId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TFnApiGetDataOptionsForSinglePhaseConfigList.ColReferenceYearSetId.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnApiGetDataOptionsForSinglePhaseConfigList.ColReferenceYearSetId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TFnApiGetDataOptionsForSinglePhaseConfigList.ColReferenceYearSetId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TFnApiGetDataOptionsForSinglePhaseConfigList.ColReferenceYearSetId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Reference year set label in national language
                /// </summary>
                public string ReferenceYearSetLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetDataOptionsForSinglePhaseConfigList.ColReferenceYearSetLabelCs.Name,
                            defaultValue: TFnApiGetDataOptionsForSinglePhaseConfigList.ColReferenceYearSetLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetDataOptionsForSinglePhaseConfigList.ColReferenceYearSetLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Reference year set description in national language
                /// </summary>
                public string ReferenceYearSetDescriptionCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetDataOptionsForSinglePhaseConfigList.ColReferenceYearSetDescriptionCs.Name,
                            defaultValue: TFnApiGetDataOptionsForSinglePhaseConfigList.ColReferenceYearSetDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetDataOptionsForSinglePhaseConfigList.ColReferenceYearSetDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Reference year set extended label in national language (read-only)
                /// </summary>
                public string ReferenceYearSetExtendedLabelCs
                {
                    get
                    {
                        return
                            (ReferenceYearSetId == null)
                                ? String.IsNullOrEmpty(value: ReferenceYearSetLabelCs)
                                    ? String.Empty
                                    : ReferenceYearSetLabelCs
                                : String.IsNullOrEmpty(value: ReferenceYearSetLabelCs)
                                    ? $"{ReferenceYearSetId}"
                                    : $"{ReferenceYearSetId} - {ReferenceYearSetLabelCs}";
                    }
                }

                /// <summary>
                /// Reference year set label in English
                /// </summary>
                public string ReferenceYearSetLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetDataOptionsForSinglePhaseConfigList.ColReferenceYearSetLabelEn.Name,
                            defaultValue: TFnApiGetDataOptionsForSinglePhaseConfigList.ColReferenceYearSetLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetDataOptionsForSinglePhaseConfigList.ColReferenceYearSetLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Reference year set description in English
                /// </summary>
                public string ReferenceYearSetDescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetDataOptionsForSinglePhaseConfigList.ColReferenceYearSetDescriptionEn.Name,
                            defaultValue: TFnApiGetDataOptionsForSinglePhaseConfigList.ColReferenceYearSetDescriptionEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetDataOptionsForSinglePhaseConfigList.ColReferenceYearSetDescriptionEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Reference year set extended label in English (read-only)
                /// </summary>
                public string ReferenceYearSetExtendedLabelEn
                {
                    get
                    {
                        return
                            (ReferenceYearSetId == null)
                                ? String.IsNullOrEmpty(value: ReferenceYearSetLabelEn)
                                    ? String.Empty
                                    : ReferenceYearSetLabelEn
                                : String.IsNullOrEmpty(value: ReferenceYearSetLabelEn)
                                    ? $"{ReferenceYearSetId}"
                                    : $"{ReferenceYearSetId} - {ReferenceYearSetLabelEn}";
                    }
                }


                /// <summary>
                /// Reference date begin
                /// </summary>
                public Nullable<DateTime> ReferenceDateBegin
                {
                    get
                    {
                        return Functions.GetNDateTimeArg(
                            row: Data,
                            name: TFnApiGetDataOptionsForSinglePhaseConfigList.ColReferenceDateBegin.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnApiGetDataOptionsForSinglePhaseConfigList.ColReferenceDateBegin.DefaultValue) ?
                                            (Nullable<DateTime>)null :
                                            DateTime.Parse(s: TFnApiGetDataOptionsForSinglePhaseConfigList.ColReferenceDateBegin.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNDateTimeArg(
                            row: Data,
                            name: TFnApiGetDataOptionsForSinglePhaseConfigList.ColReferenceDateBegin.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Reference date begin as text
                /// </summary>
                public string ReferenceDateBeginText
                {
                    get
                    {
                        return
                            (ReferenceDateBegin == null) ?
                                Functions.StrNull :
                                ((DateTime)ReferenceDateBegin).ToString(
                                    format: TFnApiGetDataOptionsForSinglePhaseConfigList.ColReferenceDateBegin.NumericFormat);
                    }
                }


                /// <summary>
                /// Reference date end
                /// </summary>
                public Nullable<DateTime> ReferenceDateEnd
                {
                    get
                    {
                        return Functions.GetNDateTimeArg(
                            row: Data,
                            name: TFnApiGetDataOptionsForSinglePhaseConfigList.ColReferenceDateEnd.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnApiGetDataOptionsForSinglePhaseConfigList.ColReferenceDateEnd.DefaultValue) ?
                                            (Nullable<DateTime>)null :
                                            DateTime.Parse(s: TFnApiGetDataOptionsForSinglePhaseConfigList.ColReferenceDateEnd.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNDateTimeArg(
                            row: Data,
                            name: TFnApiGetDataOptionsForSinglePhaseConfigList.ColReferenceDateEnd.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Reference date end as text
                /// </summary>
                public string ReferenceDateEndText
                {
                    get
                    {
                        return
                            (ReferenceDateEnd == null) ?
                                Functions.StrNull :
                                ((DateTime)ReferenceDateEnd).ToString(
                                    format: TFnApiGetDataOptionsForSinglePhaseConfigList.ColReferenceDateEnd.NumericFormat);
                    }
                }


                /// <summary>
                /// Is reference year set fully within estimation period?
                /// </summary>
                public Nullable<bool> RefYearSetFullyWithinEstimationPeriod
                {
                    get
                    {
                        return Functions.GetNBoolArg(
                            row: Data,
                            name: TFnApiGetDataOptionsForSinglePhaseConfigList.ColRefYearSetFullyWithinEstimationPeriod.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnApiGetDataOptionsForSinglePhaseConfigList.ColRefYearSetFullyWithinEstimationPeriod.DefaultValue) ?
                                (Nullable<bool>)null :
                                Boolean.Parse(value: TFnApiGetDataOptionsForSinglePhaseConfigList.ColRefYearSetFullyWithinEstimationPeriod.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNBoolArg(
                            row: Data,
                            name: TFnApiGetDataOptionsForSinglePhaseConfigList.ColRefYearSetFullyWithinEstimationPeriod.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Share of reference year set intersected by estimation period
                /// </summary>
                public Nullable<double> ShareOfRefYearSetIntersectedByEstimationPeriod
                {
                    get
                    {
                        return Functions.GetNDoubleArg(
                            row: Data,
                            name: TFnApiGetDataOptionsForSinglePhaseConfigList.ColShareOfRefYearSetIntersectedByEstimationPeriod.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnApiGetDataOptionsForSinglePhaseConfigList.ColShareOfRefYearSetIntersectedByEstimationPeriod.DefaultValue) ?
                                (Nullable<double>)null :
                                Double.Parse(s: TFnApiGetDataOptionsForSinglePhaseConfigList.ColShareOfRefYearSetIntersectedByEstimationPeriod.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNDoubleArg(
                            row: Data,
                            name: TFnApiGetDataOptionsForSinglePhaseConfigList.ColShareOfRefYearSetIntersectedByEstimationPeriod.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Share of estimation period intersected by reference year set
                /// </summary>
                public Nullable<double> ShareOfEstimationPeriodIntersectedByRefYearSet
                {
                    get
                    {
                        return Functions.GetNDoubleArg(
                            row: Data,
                            name: TFnApiGetDataOptionsForSinglePhaseConfigList.ColShareOfEstimationPeriodIntersectedByRefYearSet.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnApiGetDataOptionsForSinglePhaseConfigList.ColShareOfEstimationPeriodIntersectedByRefYearSet.DefaultValue) ?
                                (Nullable<double>)null :
                                Double.Parse(s: TFnApiGetDataOptionsForSinglePhaseConfigList.ColShareOfEstimationPeriodIntersectedByRefYearSet.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNDoubleArg(
                            row: Data,
                            name: TFnApiGetDataOptionsForSinglePhaseConfigList.ColShareOfEstimationPeriodIntersectedByRefYearSet.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Number of points in panel
                /// </summary>
                public Nullable<int> SSize
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TFnApiGetDataOptionsForSinglePhaseConfigList.ColSSize.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnApiGetDataOptionsForSinglePhaseConfigList.ColSSize.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TFnApiGetDataOptionsForSinglePhaseConfigList.ColSSize.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TFnApiGetDataOptionsForSinglePhaseConfigList.ColSSize.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Maximum number of point of panel within estimation period and reference year set?
                /// </summary>
                public Nullable<bool> MaxSSizeWithinEstimationPeriodAndRefYearSet
                {
                    get
                    {
                        return Functions.GetNBoolArg(
                            row: Data,
                            name: TFnApiGetDataOptionsForSinglePhaseConfigList.ColMaxSSizeWithinEstimationPeriodAndRefYearSet.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnApiGetDataOptionsForSinglePhaseConfigList.ColMaxSSizeWithinEstimationPeriodAndRefYearSet.DefaultValue) ?
                                (Nullable<bool>)null :
                                Boolean.Parse(value: TFnApiGetDataOptionsForSinglePhaseConfigList.ColMaxSSizeWithinEstimationPeriodAndRefYearSet.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNBoolArg(
                            row: Data,
                            name: TFnApiGetDataOptionsForSinglePhaseConfigList.ColMaxSSizeWithinEstimationPeriodAndRefYearSet.Name,
                            val: value);
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not TFnApiGetDataOptionsForSinglePhaseConfig Type, return False
                    if (obj is not TFnApiGetDataOptionsForSinglePhaseConfig)
                    {
                        return false;
                    }

                    return
                        ((CountryId ?? 0) == (((TFnApiGetDataOptionsForSinglePhaseConfig)obj).CountryId ?? 0)) &&
                        ((StrataSetId ?? 0) == (((TFnApiGetDataOptionsForSinglePhaseConfig)obj).StrataSetId ?? 0)) &&
                        ((StratumId ?? 0) == (((TFnApiGetDataOptionsForSinglePhaseConfig)obj).StratumId ?? 0)) &&
                        ((PanelId ?? 0) == (((TFnApiGetDataOptionsForSinglePhaseConfig)obj).PanelId ?? 0)) &&
                        ((ReferenceYearSetId ?? 0) == (((TFnApiGetDataOptionsForSinglePhaseConfig)obj).ReferenceYearSetId ?? 0));
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        (CountryId ?? 0).GetHashCode() ^
                        (StrataSetId ?? 0).GetHashCode() ^
                        (StratumId ?? 0).GetHashCode() ^
                        (PanelId ?? 0).GetHashCode() ^
                        (ReferenceYearSetId ?? 0).GetHashCode();
                }

                /// <summary>
                /// Text description of panel in estimation cell object
                /// </summary>
                /// <returns>Text description of panel in estimation cell object</returns>
                public override string ToString()
                {
                    return ((NfiEstaDB)Composite.Database).Postgres.Setting.LanguageVersion switch
                    {
                        LanguageVersion.International => String.Concat(
                            $"Country: {CountryExtendedLabelEn},{Environment.NewLine}",
                            $"Strata set: {StratumExtendedLabelEn},{Environment.NewLine}",
                            $"Stratum: {StratumExtendedLabelEn},{Environment.NewLine}",
                            $"Panel: {PanelExtendedLabelEn},{Environment.NewLine}",
                            $"Reference year set: {ReferenceYearSetExtendedLabelEn}{Environment.NewLine}"
                            ),

                        LanguageVersion.National => String.Concat(
                            $"Země: {CountryExtendedLabelCs},{Environment.NewLine}",
                            $"Strata set: {StratumExtendedLabelCs},{Environment.NewLine}",
                            $"Stratum: {StratumExtendedLabelCs},{Environment.NewLine}",
                            $"Panel: {PanelExtendedLabelCs},{Environment.NewLine}",
                            $"Referenční období: {ReferenceYearSetExtendedLabelCs}{Environment.NewLine}"
                            ),

                        _ => String.Concat(
                            $"Country: {CountryExtendedLabelEn},{Environment.NewLine}",
                            $"Strata set: {StratumExtendedLabelEn},{Environment.NewLine}",
                            $"Stratum: {StratumExtendedLabelEn},{Environment.NewLine}",
                            $"Panel: {PanelExtendedLabelEn},{Environment.NewLine}",
                            $"Reference year set: {ReferenceYearSetExtendedLabelEn}{Environment.NewLine}"
                            )
                    };
                }

                #endregion Methods

            }


            /// <summary>
            /// List of countries, strata sets, strata, panels, reference year sets that intersect within any estimation cell
            /// and for which all required variables are available
            /// </summary>
            public class TFnApiGetDataOptionsForSinglePhaseConfigList
                : AParametrizedView
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema =
                    NfiEstaFunctions.FnApiGetDataOptionsForSinglePhaseConfig.SchemaName;

                /// <summary>
                /// Stored procedure name
                /// </summary>
                public static readonly string Name =
                    NfiEstaFunctions.FnApiGetDataOptionsForSinglePhaseConfig.Name;

                /// <summary>
                /// Stored procedure alias
                /// </summary>
                public static readonly string Alias =
                    NfiEstaFunctions.FnApiGetDataOptionsForSinglePhaseConfig.Name;

                /// <summary>
                /// Description in national language
                /// </summary>
                public static readonly string CaptionCs = String.Concat(
                    "Seznam zemí, skupin strat, strat, panelů, referenčních období, které se protínají s výpočetní buňkou,",
                    "a pro které jsou dostupné všechna požadovaná atributová členění");

                /// <summary>
                /// Description in English
                /// </summary>
                public static readonly string CaptionEn = String.Concat(
                    "List of countries, strata sets, strata, panels, reference year sets that intersect within any estimation cell",
                    "and for which all required variables are available");

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
                {
                    { "id", new ColumnMetadata()
                    {
                        Name = "id",
                        DbName = "id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = true,
                        NotNull = true,
                        DefaultValue = "0",
                        FuncCall = "row_number() over(order by country_id, strata_set_id, stratum_id, panel_id, reference_year_set_id)",
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ID",
                        HeaderTextEn = "ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 0
                    }
                    },
                    { "estimation_cells", new ColumnMetadata()
                    {
                        Name = "estimation_cells",
                        DbName = "estimation_cells",
                        DataType = "System.String",
                        DbDataType = "_int4",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = "array_to_string(estimation_cells, ';', 'NULL')",
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATION_CELLS",
                        HeaderTextEn = "ESTIMATION_CELLS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 1
                    }
                    },
                    { "country_id", new ColumnMetadata()
                    {
                        Name = "country_id",
                        DbName = "country_id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "COUNTRY_ID",
                        HeaderTextEn = "COUNTRY_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 2
                    }
                    },
                    { "country_label_cs", new ColumnMetadata()
                    {
                        Name = "country_label_cs",
                        DbName = "country_label",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "COUNTRY_LABEL_CS",
                        HeaderTextEn = "COUNTRY_LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 3
                    }
                    },
                    { "country_description_cs", new ColumnMetadata()
                    {
                        Name = "country_description_cs",
                        DbName = "country_description",
                        DataType = "System.String",
                        DbDataType = "text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "COUNTRY_DESCRIPTION_CS",
                        HeaderTextEn = "COUNTRY_DESCRIPTION_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 4
                    }
                    },
                    { "country_label_en", new ColumnMetadata()
                    {
                        Name = "country_label_en",
                        DbName = "country_label_en",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "COUNTRY_LABEL_EN",
                        HeaderTextEn = "COUNTRY_LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 5
                    }
                    },
                    { "country_description_en", new ColumnMetadata()
                    {
                        Name = "country_description_en",
                        DbName = "country_description_en",
                        DataType = "System.String",
                        DbDataType = "text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "COUNTRY_DESCRIPTION_EN",
                        HeaderTextEn = "COUNTRY_DESCRIPTION_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 6
                    }
                    },
                    { "strata_set_id", new ColumnMetadata()
                    {
                        Name = "strata_set_id",
                        DbName = "strata_set_id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "STRATA_SET_ID",
                        HeaderTextEn = "STRATA_SET_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 7
                    }
                    },
                    { "strata_set_label_cs", new ColumnMetadata()
                    {
                        Name = "strata_set_label_cs",
                        DbName = "strata_set_label",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "STRATA_SET_LABEL_CS",
                        HeaderTextEn = "STRATA_SET_LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 8
                    }
                    },
                    { "strata_set_description_cs", new ColumnMetadata()
                    {
                        Name = "strata_set_description_cs",
                        DbName = "strata_set_description",
                        DataType = "System.String",
                        DbDataType = "text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "STRATA_SET_DESCRIPTION_CS",
                        HeaderTextEn = "STRATA_SET_DESCRIPTION_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 9
                    }
                    },
                    { "strata_set_label_en", new ColumnMetadata()
                    {
                        Name = "strata_set_label_en",
                        DbName = "strata_set_label_en",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "STRATA_SET_LABEL_EN",
                        HeaderTextEn = "STRATA_SET_LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 10
                    }
                    },
                    { "strata_set_description_en", new ColumnMetadata()
                    {
                        Name = "strata_set_description_en",
                        DbName = "strata_set_description_en",
                        DataType = "System.String",
                        DbDataType = "text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "STRATA_SET_DESCRIPTION_EN",
                        HeaderTextEn = "STRATA_SET_DESCRIPTION_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 11
                    }
                    },
                    { "stratum_id", new ColumnMetadata()
                    {
                        Name = "stratum_id",
                        DbName = "stratum_id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "STRATUM_ID",
                        HeaderTextEn = "STRATUM_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 12
                    }
                    },
                    { "stratum_label_cs", new ColumnMetadata()
                    {
                        Name = "stratum_label_cs",
                        DbName = "stratum_label",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "STRATUM_LABEL_CS",
                        HeaderTextEn = "STRATUM_LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 13
                    }
                    },
                    { "stratum_description_cs", new ColumnMetadata()
                    {
                        Name = "stratum_description_cs",
                        DbName = "stratum_description",
                        DataType = "System.String",
                        DbDataType = "text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "STRATUM_DESCRIPTION_CS",
                        HeaderTextEn = "STRATUM_DESCRIPTION_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 14
                    }
                    },
                    { "stratum_label_en", new ColumnMetadata()
                    {
                        Name = "stratum_label_en",
                        DbName = "stratum_label_en",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "STRATUM_LABEL_EN",
                        HeaderTextEn = "STRATUM_LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 15
                    }
                    },
                    { "stratum_description_en", new ColumnMetadata()
                    {
                        Name = "stratum_description_en",
                        DbName = "stratum_description_en",
                        DataType = "System.String",
                        DbDataType = "text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "STRATUM_DESCRIPTION_EN",
                        HeaderTextEn = "STRATUM_DESCRIPTION_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 16
                    }
                    },
                    { "panel_id", new ColumnMetadata()
                    {
                        Name = "panel_id",
                        DbName = "panel_id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PANEL_ID",
                        HeaderTextEn = "PANEL_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 17
                    }
                    },
                    { "panel_label_cs", new ColumnMetadata()
                    {
                        Name = "panel_label_cs",
                        DbName = "panel_label",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PANEL_LABEL_CS",
                        HeaderTextEn = "PANEL_LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 18
                    }
                    },
                    { "panel_description_cs", new ColumnMetadata()
                    {
                        Name = "panel_description_cs",
                        DbName = "panel_description",
                        DataType = "System.String",
                        DbDataType = "text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PANEL_DESCRIPTION_CS",
                        HeaderTextEn = "PANEL_DESCRIPTION_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 19
                    }
                    },
                    { "panel_label_en", new ColumnMetadata()
                    {
                        Name = "panel_label_en",
                        DbName = "panel_label_en",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PANEL_LABEL_EN",
                        HeaderTextEn = "PANEL_LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 20
                    }
                    },
                    { "panel_description_en", new ColumnMetadata()
                    {
                        Name = "panel_description_en",
                        DbName = "panel_description_en",
                        DataType = "System.String",
                        DbDataType = "text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PANEL_DESCRIPTION_EN",
                        HeaderTextEn = "PANEL_DESCRIPTION_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 21
                    }
                    },
                    { "reference_year_set_id", new ColumnMetadata()
                    {
                        Name = "reference_year_set_id",
                        DbName = "reference_year_set_id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "REFERENCE_YEAR_SET_ID",
                        HeaderTextEn = "REFERENCE_YEAR_SET_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 22
                    }
                    },
                    { "reference_year_set_label_cs", new ColumnMetadata()
                    {
                        Name = "reference_year_set_label_cs",
                        DbName = "reference_year_set_label",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "REFERENCE_YEAR_SET_LABEL_CS",
                        HeaderTextEn = "REFERENCE_YEAR_SET_LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 23
                    }
                    },
                    { "reference_year_set_description_cs", new ColumnMetadata()
                    {
                        Name = "reference_year_set_description_cs",
                        DbName = "reference_year_set_description",
                        DataType = "System.String",
                        DbDataType = "text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "REFERENCE_YEAR_SET_DESCRIPTION_CS",
                        HeaderTextEn = "REFERENCE_YEAR_SET_DESCRIPTION_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 24
                    }
                    },
                    { "reference_year_set_label_en", new ColumnMetadata()
                    {
                        Name = "reference_year_set_label_en",
                        DbName = "reference_year_set_label_en",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "REFERENCE_YEAR_SET_LABEL_EN",
                        HeaderTextEn = "REFERENCE_YEAR_SET_LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 25
                    }
                    },
                    { "reference_year_set_description_en", new ColumnMetadata()
                    {
                        Name = "reference_year_set_description_en",
                        DbName = "reference_year_set_description_en",
                        DataType = "System.String",
                        DbDataType = "text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "REFERENCE_YEAR_SET_DESCRIPTION_EN",
                        HeaderTextEn = "REFERENCE_YEAR_SET_DESCRIPTION_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 26
                    }
                    },
                    { "reference_date_begin", new ColumnMetadata()
                    {
                        Name = "reference_date_begin",
                        DbName = "reference_date_begin",
                        DataType = "System.DateTime",
                        DbDataType = "date",
                        NewDataType = "date",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "yyyy-MM-dd",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "REFERENCE_DATE_BEGIN",
                        HeaderTextEn = "REFERENCE_DATE_BEGIN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 27
                    }
                    },
                    { "reference_date_end", new ColumnMetadata()
                    {
                        Name = "reference_date_end",
                        DbName = "reference_date_end",
                        DataType = "System.DateTime",
                        DbDataType = "date",
                        NewDataType = "date",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "yyyy-MM-dd",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "REFERENCE_DATE_END",
                        HeaderTextEn = "REFERENCE_DATE_END",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 28
                    }
                    },
                    { "refyearset_fully_within_estimation_period", new ColumnMetadata()
                    {
                        Name = "refyearset_fully_within_estimation_period",
                        DbName = "refyearset_fully_within_estimation_period",
                        DataType = "System.Boolean",
                        DbDataType = "bool",
                        NewDataType = "bool",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "REFYEARSET_FULLY_WITHIN_ESTIMATION_PERIOD",
                        HeaderTextEn = "REFYEARSET_FULLY_WITHIN_ESTIMATION_PERIOD",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 29
                    }
                    },
                    { "share_of_refyearset_intersected_by_estimation_period", new ColumnMetadata()
                    {
                        Name = "share_of_refyearset_intersected_by_estimation_period",
                        DbName = "share_of_refyearset_intersected_by_estimation_period",
                        DataType = "System.Double",
                        DbDataType = "float8",
                        NewDataType = "float8",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "P2",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "SHARE_OF_REFYEARSET_INTERSECTED_BY_ESTIMATION_PERIOD",
                        HeaderTextEn = "SHARE_OF_REFYEARSET_INTERSECTED_BY_ESTIMATION_PERIOD",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 30
                    }
                    },
                    { "share_of_estimation_period_intersected_by_refyearset", new ColumnMetadata()
                    {
                        Name = "share_of_estimation_period_intersected_by_refyearset",
                        DbName = "share_of_estimation_period_intersected_by_refyearset",
                        DataType = "System.Double",
                        DbDataType = "float8",
                        NewDataType = "float8",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "P2",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "SHARE_OF_ESTIMATION_PERIOD_INTERSECTED_BY_REFYEARSET",
                        HeaderTextEn = "SHARE_OF_ESTIMATION_PERIOD_INTERSECTED_BY_REFYEARSET",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 31
                    }
                    },
                    { "ssize", new ColumnMetadata()
                    {
                        Name = "ssize",
                        DbName = "ssize",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "SSIZE",
                        HeaderTextEn = "SSIZE",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 32
                    }
                    },
                    { "max_ssize_within_estimation_period_and_refyearset", new ColumnMetadata()
                    {
                        Name = "max_ssize_within_estimation_period_and_refyearset",
                        DbName = "max_ssize_within_estimation_period_and_refyearset",
                        DataType = "System.Boolean",
                        DbDataType = "bool",
                        NewDataType = "bool",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "MAX_SSIZE_WITHIN_ESTIMATION_PERIOD_AND_REFYEARSET",
                        HeaderTextEn = "MAX_SSIZE_WITHIN_ESTIMATION_PERIOD_AND_REFYEARSET",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 33
                    }
                    }
                };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column estimation_cells metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationCells = Cols["estimation_cells"];

                /// <summary>
                /// Column country_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColCountryId = Cols["country_id"];

                /// <summary>
                /// Column country_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColCountryLabelCs = Cols["country_label_cs"];

                /// <summary>
                /// Column country_description_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColCountryDescriptionCs = Cols["country_description_cs"];

                /// <summary>
                /// Column country_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColCountryLabelEn = Cols["country_label_en"];

                /// <summary>
                /// Column country_description_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColCountryDescriptionEn = Cols["country_description_en"];

                /// <summary>
                /// Column strata_set_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColStrataSetId = Cols["strata_set_id"];

                /// <summary>
                /// Column strata_set_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColStrataSetLabelCs = Cols["strata_set_label_cs"];

                /// <summary>
                /// Column strata_set_description_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColStrataSetDescriptionCs = Cols["strata_set_description_cs"];

                /// <summary>
                /// Column strata_set_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColStrataSetLabelEn = Cols["strata_set_label_en"];

                /// <summary>
                /// Column strata_set_description_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColStrataSetDescriptionEn = Cols["strata_set_description_en"];

                /// <summary>
                /// Column stratum_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColStratumId = Cols["stratum_id"];

                /// <summary>
                /// Column stratum_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColStratumLabelCs = Cols["stratum_label_cs"];

                /// <summary>
                /// Column stratum_description_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColStratumDescriptionCs = Cols["stratum_description_cs"];

                /// <summary>
                /// Column stratum_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColStratumLabelEn = Cols["stratum_label_en"];

                /// <summary>
                /// Column stratum_description_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColStratumDescriptionEn = Cols["stratum_description_en"];

                /// <summary>
                /// Column panel_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColPanelId = Cols["panel_id"];

                /// <summary>
                /// Column panel_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColPanelLabelCs = Cols["panel_label_cs"];

                /// <summary>
                /// Column panel_description_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColPanelDescriptionCs = Cols["panel_description_cs"];

                /// <summary>
                /// Column panel_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColPanelLabelEn = Cols["panel_label_en"];

                /// <summary>
                /// Column panel_description_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColPanelDescriptionEn = Cols["panel_description_en"];

                /// <summary>
                /// Column reference_year_set_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColReferenceYearSetId = Cols["reference_year_set_id"];

                /// <summary>
                /// Column reference_year_set_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColReferenceYearSetLabelCs = Cols["reference_year_set_label_cs"];

                /// <summary>
                /// Column reference_year_set_description_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColReferenceYearSetDescriptionCs = Cols["reference_year_set_description_cs"];

                /// <summary>
                /// Column reference_year_set_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColReferenceYearSetLabelEn = Cols["reference_year_set_label_en"];

                /// <summary>
                /// Column reference_year_set_description_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColReferenceYearSetDescriptionEn = Cols["reference_year_set_description_en"];

                /// <summary>
                /// Column reference_date_begin metadata
                /// </summary>
                public static readonly ColumnMetadata ColReferenceDateBegin = Cols["reference_date_begin"];

                /// <summary>
                /// Column reference_date_end metadata
                /// </summary>
                public static readonly ColumnMetadata ColReferenceDateEnd = Cols["reference_date_end"];

                /// <summary>
                /// Column refyearset_fully_within_estimation_period metadata
                /// </summary>
                public static readonly ColumnMetadata ColRefYearSetFullyWithinEstimationPeriod = Cols["refyearset_fully_within_estimation_period"];

                /// <summary>
                /// Column share_of_refyearset_intersected_by_estimation_period metadata
                /// </summary>
                public static readonly ColumnMetadata ColShareOfRefYearSetIntersectedByEstimationPeriod = Cols["share_of_refyearset_intersected_by_estimation_period"];

                /// <summary>
                /// Column share_of_estimation_period_intersected_by_refyearset metadata
                /// </summary>
                public static readonly ColumnMetadata ColShareOfEstimationPeriodIntersectedByRefYearSet = Cols["share_of_estimation_period_intersected_by_refyearset"];

                /// <summary>
                /// Column ssize metadata
                /// </summary>
                public static readonly ColumnMetadata ColSSize = Cols["ssize"];

                /// <summary>
                /// Column max_ssize_within_estimation_period metadata
                /// </summary>
                public static readonly ColumnMetadata ColMaxSSizeWithinEstimationPeriodAndRefYearSet = Cols["max_ssize_within_estimation_period_and_refyearset"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Private Fields

                /// <summary>
                /// First parameter - List of estimation cells identifiers
                /// </summary>
                private List<Nullable<int>> estimationCellsIds;

                /// <summary>
                /// Second parameter - List of attribute categories identifiers
                /// </summary>
                private List<Nullable<int>> variablesIds;

                /// <summary>
                /// Third parameter - The beginning of the time to witch the estimate is calculated
                /// </summary>
                private Nullable<DateTime> estimateDateBegin;

                /// <summary>
                /// Fourth parameter - The end of the time to witch the estimate is calculated
                /// </summary>
                private Nullable<DateTime> estimateDateEnd;

                /// <summary>
                /// Fifth parameter - The end of the time to witch the estimate is calculated
                /// </summary>
                private Nullable<double> cellCoverageTolerance;

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TFnApiGetDataOptionsForSinglePhaseConfigList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                {
                    EstimationCellsIds = [];
                    VariablesIds = [];
                    EstimateDateBegin = null;
                    EstimateDateEnd = null;
                    CellCoverageTolerance = null;
                }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TFnApiGetDataOptionsForSinglePhaseConfigList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                {
                    EstimationCellsIds = [];
                    VariablesIds = [];
                    EstimateDateBegin = null;
                    EstimateDateEnd = null;
                    CellCoverageTolerance = null;
                }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// First parameter - List of estimation cells identifiers
                /// </summary>
                public List<Nullable<int>> EstimationCellsIds
                {
                    get
                    {
                        return estimationCellsIds ?? [];
                    }
                    set
                    {
                        estimationCellsIds = value ?? [];
                    }
                }

                /// <summary>
                /// Second parameter - List of attribute categories identifiers
                /// </summary>
                public List<Nullable<int>> VariablesIds
                {
                    get
                    {
                        return variablesIds ?? [];
                    }
                    set
                    {
                        variablesIds = value ?? [];
                    }
                }

                /// <summary>
                /// Third parameter - The beginning of the time to witch the estimate is calculated
                /// </summary>
                public Nullable<DateTime> EstimateDateBegin
                {
                    get
                    {
                        return estimateDateBegin;
                    }
                    set
                    {
                        estimateDateBegin = value;
                    }
                }

                /// <summary>
                /// Fourth parameter - The end of the time to witch the estimate is calculated
                /// </summary>
                public Nullable<DateTime> EstimateDateEnd
                {
                    get
                    {
                        return estimateDateEnd;
                    }
                    set
                    {
                        estimateDateEnd = value;
                    }
                }

                /// <summary>
                /// Fifth parameter - The end of the time to witch the estimate is calculated
                /// </summary>
                public Nullable<double> CellCoverageTolerance
                {
                    get
                    {
                        return cellCoverageTolerance;
                    }
                    set
                    {
                        cellCoverageTolerance = value;
                    }
                }


                /// <summary>
                /// List of countries, strata sets, strata, panels, reference year sets that intersect within any estimation cell
                /// and for which all required variables are available (read-only)
                /// </summary>
                public List<TFnApiGetDataOptionsForSinglePhaseConfig> Items
                {
                    get
                    {
                        return
                        [..
                            Data.AsEnumerable()
                            .Select(a => new TFnApiGetDataOptionsForSinglePhaseConfig(composite: this, data: a))
                        ];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// Combination of countries, strata sets, strata, panels, reference year sets
                /// that intersect within any estimation cell (read-only)
                /// </summary>
                /// <param name="id">Identifier</param>
                /// <returns>Combination of countries, strata sets, strata, panels, reference year sets
                /// that intersect within any estimation cell (null if not found)</returns>
                public TFnApiGetDataOptionsForSinglePhaseConfig this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new TFnApiGetDataOptionsForSinglePhaseConfig(composite: this, data: a))
                                .FirstOrDefault<TFnApiGetDataOptionsForSinglePhaseConfig>();
                    }
                }

                #endregion Indexer


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public TFnApiGetDataOptionsForSinglePhaseConfigList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new TFnApiGetDataOptionsForSinglePhaseConfigList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime)
                        {
                            EstimationCellsIds = [.. EstimationCellsIds],
                            VariablesIds = [.. VariablesIds],
                            EstimateDateBegin = EstimateDateBegin,
                            EstimateDateEnd = EstimateDateEnd,
                            CellCoverageTolerance = CellCoverageTolerance
                        } :
                        new TFnApiGetDataOptionsForSinglePhaseConfigList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null)
                        {
                            EstimationCellsIds = [.. EstimationCellsIds],
                            VariablesIds = [.. VariablesIds],
                            EstimateDateBegin = EstimateDateBegin,
                            EstimateDateEnd = EstimateDateEnd,
                            CellCoverageTolerance = CellCoverageTolerance
                        };
                }

                /// <summary>
                /// Data table with distinct list of countries
                /// </summary>
                /// <returns>Data table with distinct list of countries</returns>
                public DataTable GetCountries()
                {
                    return new DataView()
                    {
                        Table = Data
                    }
                    .ToTable(
                        distinct: true,
                        ColCountryId.Name,
                        ColCountryLabelCs.Name,
                        ColCountryDescriptionCs.Name,
                        ColCountryLabelEn.Name,
                        ColCountryDescriptionEn.Name);
                }

                /// <summary>
                /// Data table with distinct list of strata sets for given country
                /// </summary>
                /// <param name="countryId">Country identifier</param>
                /// <returns>Data table with distinct list of strata sets for given country</returns>
                public DataTable GetStrataSets(int countryId)
                {
                    return new DataView()
                    {
                        Table = Data,
                        RowFilter = $"{ColCountryId.Name} = {countryId}"
                    }
                   .ToTable(
                       distinct: true,
                       ColStrataSetId.Name,
                       ColStrataSetLabelCs.Name,
                       ColStrataSetDescriptionCs.Name,
                       ColStrataSetLabelEn.Name,
                       ColStrataSetDescriptionEn.Name);
                }

                /// <summary>
                /// Data table with distinct list of strata for given country and strata set
                /// </summary>
                /// <param name="countryId">Country identifier</param>
                /// <param name="strataSetId">Strata set identifier</param>
                /// <returns>Data table with distinct list of strata for given country and strata set</returns>
                public DataTable GetStrata(int countryId, int strataSetId)
                {
                    return new DataView()
                    {
                        Table = Data,
                        RowFilter = $"({ColCountryId.Name} = {countryId}) AND ({ColStrataSetId.Name} = {strataSetId})"
                    }
                   .ToTable(
                       distinct: true,
                       ColStratumId.Name,
                       ColStratumLabelCs.Name,
                       ColStratumDescriptionCs.Name,
                       ColStratumLabelEn.Name,
                       ColStratumDescriptionEn.Name);
                }

                /// <summary>
                /// Data table with distinct list of strata
                /// </summary>
                /// <returns>Data table with distinct list of strata</returns>
                public DataTable GetStrata()
                {
                    return new DataView()
                    {
                        Table = Data
                    }
                    .ToTable(
                       distinct: true,
                       ColStratumId.Name,
                       ColStratumLabelCs.Name,
                       ColStratumDescriptionCs.Name,
                       ColStratumLabelEn.Name,
                       ColStratumDescriptionEn.Name);
                }

                /// <summary>
                /// Data table with distinct list of estimation cells
                /// </summary>
                /// <returns>Data table with distinct list of estimation cells</returns>
                public List<EstimationCellGroup> GetEstimationCellGroups()
                {
                    return
                    [..
                        new DataView() { Table = Data }
                        .ToTable(
                            distinct: true,
                            ColCountryId.Name,
                            ColStrataSetId.Name,
                            ColStratumId.Name,
                            ColEstimationCells.Name)
                        .AsEnumerable()
                        .Where(a => a.Field<Nullable<int>>(columnName: ColCountryId.Name) != null)
                        .Where(a => a.Field<Nullable<int>>(columnName: ColStrataSetId.Name) != null)
                        .Where(a => a.Field<Nullable<int>>(columnName: ColStratumId.Name) != null)
                        .Where(a => !String.IsNullOrEmpty(value: a.Field<string>(columnName: ColEstimationCells.Name)))
                        .Select(a => new EstimationCellGroup()
                        {
                            CountryId = a.Field<int>(columnName: ColCountryId.Name),
                            Country = null,

                            StrataSetId = a.Field<int>(columnName: ColStrataSetId.Name),
                            StrataSet = null,

                            StratumId = a.Field<int>(columnName: ColStratumId.Name),
                            Stratum = null,

                            EstimationCellsIdsText = a.Field<string>(columnName: ColEstimationCells.Name),

                            PanelRefYearSetGroup = null
                        })
                        .Distinct<EstimationCellGroup>()
                    ];
                }

                #endregion Methods

            }

        }
    }
}