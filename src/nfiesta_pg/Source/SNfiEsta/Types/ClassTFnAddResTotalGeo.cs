﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {
            // fn_add_res_total_geo

            /// <summary>
            /// Total estimates geographic additivity
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class TFnAddResTotalGeo(
                TFnAddResTotalGeoList composite = null,
                DataRow data = null)
                    : AParametrizedViewEntry<TFnAddResTotalGeoList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Total estimates geographic additivity identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TFnAddResTotalGeoList.ColId.Name,
                            defaultValue: Int32.Parse(s: TFnAddResTotalGeoList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TFnAddResTotalGeoList.ColId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Combination of target variable and sub-population category and area domain category identifier
                /// </summary>
                public Nullable<int> VariableId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TFnAddResTotalGeoList.ColVariableId.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnAddResTotalGeoList.ColVariableId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TFnAddResTotalGeoList.ColVariableId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TFnAddResTotalGeoList.ColVariableId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Combination of target variable and sub-population category and area domain category object (read-only)
                /// </summary>
                public Variable Variable
                {
                    get
                    {
                        return (VariableId != null) ?
                             ((NfiEstaDB)Composite.Database).SNfiEsta.TVariable[(int)VariableId] : null;
                    }
                }

                /// <summary>
                /// Combination of target variable and sub-population category and area domain category (extended-version) object (read-only)
                /// </summary>
                public VwVariable VwVariable
                {
                    get
                    {
                        return (VariableId != null) ?
                             ((NfiEstaDB)Composite.Database).SNfiEsta.VVariable[(int)VariableId] : null;
                    }
                }


                /// <summary>
                /// Parametrization configuration identifier
                /// </summary>
                public Nullable<int> AuxConfId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TFnAddResTotalGeoList.ColAuxConfId.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnAddResTotalGeoList.ColAuxConfId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TFnAddResTotalGeoList.ColAuxConfId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TFnAddResTotalGeoList.ColAuxConfId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Parametrization configuration object (read-only)
                /// </summary>
                public AuxConf AuxConf
                {
                    get
                    {
                        return (AuxConfId != null) ?
                             ((NfiEstaDB)Composite.Database).SNfiEsta.TAuxConf[(int)AuxConfId] : null;
                    }
                }


                /// <summary>
                /// Force synthetic estimate
                /// </summary>
                public Nullable<bool> ForceSynthetic
                {
                    get
                    {
                        return Functions.GetNBoolArg(
                            row: Data,
                            name: TFnAddResTotalGeoList.ColForceSynthetic.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnAddResTotalGeoList.ColForceSynthetic.DefaultValue) ?
                                (Nullable<bool>)null :
                                Boolean.Parse(value: TFnAddResTotalGeoList.ColForceSynthetic.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNBoolArg(
                            row: Data,
                            name: TFnAddResTotalGeoList.ColForceSynthetic.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Estimate configuration identifier
                /// </summary>
                public Nullable<int> EstimateConfId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TFnAddResTotalGeoList.ColEstimateConfId.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnAddResTotalGeoList.ColEstimateConfId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TFnAddResTotalGeoList.ColEstimateConfId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TFnAddResTotalGeoList.ColEstimateConfId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Estimate configuration object (read-only)
                /// </summary>
                public EstimateConf EstimateConf
                {
                    get
                    {
                        return (EstimateConfId != null) ?
                             ((NfiEstaDB)Composite.Database).SNfiEsta.TEstimateConf[(int)EstimateConfId] : null;
                    }
                }


                /// <summary>
                /// Estimation cell identifier
                /// </summary>
                public Nullable<int> EstimationCellId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TFnAddResTotalGeoList.ColEstimationCellId.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnAddResTotalGeoList.ColEstimationCellId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TFnAddResTotalGeoList.ColEstimationCellId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TFnAddResTotalGeoList.ColEstimationCellId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Estimation cell object (read-only)
                /// </summary>
                public EstimationCell EstimationCell
                {
                    get
                    {
                        return (EstimationCellId != null) ?
                             ((NfiEstaDB)Composite.Database).SNfiEsta.CEstimationCell[(int)EstimationCellId] : null;
                    }
                }


                /// <summary>
                /// Aggregated class point estimate
                /// </summary>
                public Nullable<double> PointEst
                {
                    get
                    {
                        return Functions.GetNDoubleArg(
                            row: Data,
                            name: TFnAddResTotalGeoList.ColPointEst.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnAddResTotalGeoList.ColPointEst.DefaultValue) ?
                                (Nullable<double>)null :
                                Double.Parse(s: TFnAddResTotalGeoList.ColPointEst.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNDoubleArg(
                            row: Data,
                            name: TFnAddResTotalGeoList.ColPointEst.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Sum of sub-classes point estimates (belonging to aggregated class)
                /// </summary>
                public Nullable<double> PointEstSum
                {
                    get
                    {
                        return Functions.GetNDoubleArg(
                            row: Data,
                            name: TFnAddResTotalGeoList.ColPointEstSum.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnAddResTotalGeoList.ColPointEstSum.DefaultValue) ?
                                (Nullable<double>)null :
                                Double.Parse(s: TFnAddResTotalGeoList.ColPointEstSum.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNDoubleArg(
                            row: Data,
                            name: TFnAddResTotalGeoList.ColPointEstSum.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Relative difference between aggregated class estimate and sum of sub-classes estimates
                /// </summary>
                public Nullable<double> Diff
                {
                    get
                    {
                        return Functions.GetNDoubleArg(
                            row: Data,
                            name: TFnAddResTotalGeoList.ColDiff.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnAddResTotalGeoList.ColDiff.DefaultValue) ?
                                (Nullable<double>)null :
                                Double.Parse(s: TFnAddResTotalGeoList.ColDiff.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNDoubleArg(
                            row: Data,
                            name: TFnAddResTotalGeoList.ColDiff.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Estimation cells defined in hierarchy (v_estimation_cell_hierarchy)
                /// </summary>
                public string EstimationCellsDef
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnAddResTotalGeoList.ColEstimationCellsDef.Name,
                            defaultValue: TFnAddResTotalGeoList.ColEstimationCellsDef.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnAddResTotalGeoList.ColEstimationCellsDef.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// List of estimation cells -- estimation cells defined in hierarchy (v_estimation_cell_hierarchy)
                /// </summary>
                public List<int> EstimationCellsDefList
                {
                    get
                    {
                        return Functions.StringToIntList(
                            text: EstimationCellsDef,
                            separator: (char)59,
                            defaultValue: null);
                    }
                }


                /// <summary>
                /// Estimation cells found in data (t_result)
                /// </summary>
                public string EstimationCellsFound
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnAddResTotalGeoList.ColEstimationCellsFound.Name,
                            defaultValue: TFnAddResTotalGeoList.ColEstimationCellsFound.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnAddResTotalGeoList.ColEstimationCellsFound.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// List of estimation cells -- estimation cells found in data (t_result)
                /// </summary>
                public List<int> EstimationCellsFoundList
                {
                    get
                    {
                        return Functions.StringToIntList(
                            text: EstimationCellsFound,
                            separator: (char)59,
                            defaultValue: null);
                    }
                }


                /// <summary>
                /// Estimate configurations found in data (t_result)
                /// </summary>
                public string EstimateConfsFound
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnAddResTotalGeoList.ColEstimateConfsFound.Name,
                            defaultValue: TFnAddResTotalGeoList.ColEstimateConfsFound.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnAddResTotalGeoList.ColEstimateConfsFound.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// List of estimate configurations -- estimate configurations found in data (t_result)
                /// </summary>
                public List<int> EstimateConfsFoundList
                {
                    get
                    {
                        return Functions.StringToIntList(
                            text: EstimateConfsFound,
                            separator: (char)59,
                            defaultValue: null);
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not TFnAddResTotalGeo Type, return False
                    if (obj is not TFnAddResTotalGeo)
                    {
                        return false;
                    }

                    return
                        Id == ((TFnAddResTotalGeo)obj).Id;
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                /// <summary>
                /// Text description of total estimates geographic additivity object
                /// </summary>
                /// <returns>Text description of total estimates geographic additivity object</returns>
                public override string ToString()
                {
                    return ((NfiEstaDB)Composite.Database).Postgres.Setting.LanguageVersion switch
                    {
                        LanguageVersion.National =>
                            String.Concat(
                                $"{"ID"} = {Functions.PrepNIntArg(arg: Id)}.{Environment.NewLine}"
                                ),
                        LanguageVersion.International =>
                            String.Concat(
                                $"{"ID"} = {Functions.PrepNIntArg(arg: Id)}.{Environment.NewLine}"
                                ),
                        _ =>
                            String.Concat(
                                $"{"ID"} = {Functions.PrepNIntArg(arg: Id)}.{Environment.NewLine}"
                                ),
                    };
                }

                #endregion Methods

            }


            /// <summary>
            /// List of total estimates geographic additivity
            /// </summary>
            public class TFnAddResTotalGeoList
                : AParametrizedView
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema =
                    NfiEstaFunctions.FnAddResTotalGeo.SchemaName;

                /// <summary>
                /// Stored procedure name
                /// </summary>
                public static readonly string Name =
                    NfiEstaFunctions.FnAddResTotalGeo.Name;

                /// <summary>
                /// Stored procedure alias
                /// </summary>
                public static readonly string Alias =
                    NfiEstaFunctions.FnAddResTotalGeo.Name;

                /// <summary>
                /// Description in national language
                /// </summary>
                public static readonly string CaptionCs =
                    "Uložená procedura zobrazující geografickou aditivitu odhadů úhrnů";

                /// <summary>
                /// Description in English
                /// </summary>
                public static readonly string CaptionEn =
                    "Stored procedure showing total estimates geographic additivity";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
                {
                    { "id", new ColumnMetadata()
                    {
                        Name = "id",
                        DbName = "id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = true,
                        NotNull = true,
                        DefaultValue = "0",
                        FuncCall = "row_number() over()",
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ID",
                        HeaderTextEn = "ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 0
                    }
                    },
                    { "variable", new ColumnMetadata()
                    {
                        Name = "variable",
                        DbName = "variable",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "VARIABLE",
                        HeaderTextEn = "VARIABLE",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 1
                    }
                    },
                    { "aux_conf", new ColumnMetadata()
                    {
                        Name = "aux_conf",
                        DbName = "aux_conf",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "AUX_CONF",
                        HeaderTextEn = "AUX_CONF",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 2
                    }
                    },
                    { "force_synthetic", new ColumnMetadata()
                    {
                        Name = "force_synthetic",
                        DbName = "force_synthetic",
                        DataType = "System.Boolean",
                        DbDataType = "bool",
                        NewDataType = "bool",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "FORCE_SYNTHETIC",
                        HeaderTextEn = "FORCE_SYNTHETIC",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 3
                    }
                    },
                    { "estimate_conf", new ColumnMetadata()
                    {
                        Name = "estimate_conf",
                        DbName = "estimate_conf",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATE_CONF",
                        HeaderTextEn = "ESTIMATE_CONF",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 4
                    }
                    },
                    { "estimation_cell", new ColumnMetadata()
                    {
                        Name = "estimation_cell",
                        DbName = "estimation_cell",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATION_CELL",
                        HeaderTextEn = "ESTIMATION_CELL",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 5
                    }
                    },
                    { "point_est", new ColumnMetadata()
                    {
                        Name = "point_est",
                        DbName = "point_est",
                        DataType = "System.Double",
                        DbDataType = "float8",
                        NewDataType = "float8",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "N3",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "POINT_EST",
                        HeaderTextEn = "POINT_EST",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 6
                    }
                    },
                    { "point_est_sum", new ColumnMetadata()
                    {
                        Name = "point_est_sum",
                        DbName = "point_est_sum",
                        DataType = "System.Double",
                        DbDataType = "float8",
                        NewDataType = "float8",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "N3",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "POINT_EST_SUM",
                        HeaderTextEn = "POINT_EST_SUM",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 7
                    }
                    },
                    { "diff", new ColumnMetadata()
                    {
                        Name = "diff",
                        DbName = "diff",
                        DataType = "System.Double",
                        DbDataType = "float8",
                        NewDataType = "float8",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "N3",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "DIFF",
                        HeaderTextEn = "DIFF",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 8
                    }
                    },
                    { "estimation_cells_def", new ColumnMetadata()
                    {
                        Name = "estimation_cells_def",
                        DbName = "estimation_cells_def",
                        DataType = "System.String",
                        DbDataType = "_int4",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = "array_to_string({0}, ';', 'NULL')",
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATION_CELLS_DEF",
                        HeaderTextEn = "ESTIMATION_CELLS_DEF",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 9
                    }
                    },
                    { "estimation_cells_found", new ColumnMetadata()
                    {
                        Name = "estimation_cells_found",
                        DbName = "estimation_cells_found",
                        DataType = "System.String",
                        DbDataType = "_int4",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = "array_to_string({0}, ';', 'NULL')",
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATION_CELLS_FOUND",
                        HeaderTextEn = "ESTIMATION_CELLS_FOUND",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 10
                    }
                    },
                    { "estimate_confs_found", new ColumnMetadata()
                    {
                        Name = "estimate_confs_found",
                        DbName = "estimate_confs_found",
                        DataType = "System.String",
                        DbDataType = "_int4",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = "array_to_string({0}, ';', 'NULL')",
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATE_CONFS_FOUND",
                        HeaderTextEn = "ESTIMATE_CONFS_FOUND",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 11
                    }
                    }
                };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column variable metadata
                /// </summary>
                public static readonly ColumnMetadata ColVariableId = Cols["variable"];

                /// <summary>
                /// Column aux_conf metadata
                /// </summary>
                public static readonly ColumnMetadata ColAuxConfId = Cols["aux_conf"];

                /// <summary>
                /// Column force_synthetic metadata
                /// </summary>
                public static readonly ColumnMetadata ColForceSynthetic = Cols["force_synthetic"];

                /// <summary>
                /// Column estimate_conf metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimateConfId = Cols["estimate_conf"];

                /// <summary>
                /// Column estimation_cell metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationCellId = Cols["estimation_cell"];

                /// <summary>
                /// Column point_est metadata
                /// </summary>
                public static readonly ColumnMetadata ColPointEst = Cols["point_est"];

                /// <summary>
                /// Column point_est_sum metadata
                /// </summary>
                public static readonly ColumnMetadata ColPointEstSum = Cols["point_est_sum"];

                /// <summary>
                /// Column diff metadata
                /// </summary>
                public static readonly ColumnMetadata ColDiff = Cols["diff"];

                /// <summary>
                /// Column estimation_cells_def metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationCellsDef = Cols["estimation_cells_def"];

                /// <summary>
                /// Column estimation_cells_found metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationCellsFound = Cols["estimation_cells_found"];

                /// <summary>
                /// Column estimate_confs_found metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimateConfsFound = Cols["estimate_confs_found"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Private Fields

                /// <summary>
                /// First parameter - List of estimate configurations
                /// </summary>
                private List<Nullable<int>> estConfs;

                /// <summary>
                /// Second parameter - Minimal amount of difference [%].
                /// Default value is 0 when parameter is NULL.
                /// </summary>
                private Nullable<double> minDiff;

                /// <summary>
                /// Third parameter - Whether include NULL difference -- indicating defined sub-classes missing in data
                /// Default value is true when parameter is NULL.
                /// </summary>
                private Nullable<bool> includeNullDiff;

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TFnAddResTotalGeoList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                {
                    EstConfs = [];
                    MinDiff = null;
                    IncludeNullDiff = null;
                }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TFnAddResTotalGeoList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                {
                    EstConfs = [];
                    MinDiff = null;
                    IncludeNullDiff = null;
                }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// First parameter - List of estimate configurations
                /// </summary>
                public List<Nullable<int>> EstConfs
                {
                    get
                    {
                        return estConfs ?? [];
                    }
                    set
                    {
                        estConfs = value ?? [];
                    }
                }

                /// <summary>
                /// Second parameter - Minimal amount of difference [%].
                /// Default value is 0 when parameter is NULL.
                /// </summary>
                public Nullable<double> MinDiff
                {
                    get
                    {
                        return minDiff;
                    }
                    set
                    {
                        minDiff = value;
                    }
                }

                /// <summary>
                /// Third parameter - Whether include NULL difference -- indicating defined sub-classes missing in data
                /// Default value is true when parameter is NULL.
                /// </summary>
                public Nullable<bool> IncludeNullDiff
                {
                    get
                    {
                        return includeNullDiff;
                    }
                    set
                    {
                        includeNullDiff = value;
                    }
                }

                /// <summary>
                /// List of total estimates geographic additivity (read-only)
                /// </summary>
                public List<TFnAddResTotalGeo> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new TFnAddResTotalGeo(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// Total estimates geographic additivity from list by identifier (read-only)
                /// </summary>
                /// <param name="id">Total estimates geographic additivity identifier</param>
                /// <returns>Total estimates geographic additivity from list by identifier (null if not found)</returns>
                public TFnAddResTotalGeo this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new TFnAddResTotalGeo(composite: this, data: a))
                                .FirstOrDefault<TFnAddResTotalGeo>();
                    }
                }

                #endregion Indexer


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public TFnAddResTotalGeoList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new TFnAddResTotalGeoList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime)
                        {
                            EstConfs = [.. EstConfs],
                            MinDiff = MinDiff,
                            IncludeNullDiff = IncludeNullDiff
                        } :
                        new TFnAddResTotalGeoList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null)
                        {
                            EstConfs = [.. EstConfs],
                            MinDiff = MinDiff,
                            IncludeNullDiff = IncludeNullDiff
                        };
                }

                #endregion Methods

            }

        }
    }
}
