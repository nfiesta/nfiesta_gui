﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {
            // fn_api_before_delete_panel_refyearset_group

            /// <summary>
            /// Controls if there is a row in tables nfiesta.t_aux_conf and nfiesta.t_total_estimate_conf
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class TFnApiBeforeDeletePanelRefYearSetGroup(
                TFnApiBeforeDeletePanelRefYearSetGroupList composite = null,
                DataRow data = null)
                    : AParametrizedViewEntry<TFnApiBeforeDeletePanelRefYearSetGroupList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TFnApiBeforeDeletePanelRefYearSetGroupList.ColId.Name,
                            defaultValue: Int32.Parse(s: TFnApiBeforeDeletePanelRefYearSetGroupList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TFnApiBeforeDeletePanelRefYearSetGroupList.ColId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Row in table nfiesta.t_aux_conf exists
                /// </summary>
                public Nullable<bool> AuxConfExist
                {
                    get
                    {
                        return Functions.GetNBoolArg(
                            row: Data,
                            name: TFnApiBeforeDeletePanelRefYearSetGroupList.ColAuxConfExist.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnApiBeforeDeletePanelRefYearSetGroupList.ColAuxConfExist.DefaultValue) ?
                                (Nullable<bool>)null :
                                Boolean.Parse(value: TFnApiBeforeDeletePanelRefYearSetGroupList.ColAuxConfExist.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNBoolArg(
                            row: Data,
                            name: TFnApiBeforeDeletePanelRefYearSetGroupList.ColAuxConfExist.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Row in table nfiesta.t_total_estimate_conf exists
                /// </summary>
                public Nullable<bool> TotalEstimateConfExist
                {
                    get
                    {
                        return Functions.GetNBoolArg(
                            row: Data,
                            name: TFnApiBeforeDeletePanelRefYearSetGroupList.ColTotalEstimateConfExist.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnApiBeforeDeletePanelRefYearSetGroupList.ColTotalEstimateConfExist.DefaultValue) ?
                                (Nullable<bool>)null :
                                Boolean.Parse(value: TFnApiBeforeDeletePanelRefYearSetGroupList.ColTotalEstimateConfExist.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNBoolArg(
                            row: Data,
                            name: TFnApiBeforeDeletePanelRefYearSetGroupList.ColTotalEstimateConfExist.Name,
                            val: value);
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not TFnApiBeforeDeletePanelRefYearSetGroup, return False
                    if (obj is not TFnApiBeforeDeletePanelRefYearSetGroup)
                    {
                        return false;
                    }

                    return
                        Id == ((TFnApiBeforeDeletePanelRefYearSetGroup)obj).Id;
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                /// <summary>
                /// Text description of the object
                /// </summary>
                /// <returns>Text description of the object</returns>
                public override string ToString()
                {
                    string auxConfExist =
                        (AuxConfExist == null)
                            ? Functions.StrNull
                            : ((bool)AuxConfExist).ToString();

                    string totalEstimateConfExist =
                        (TotalEstimateConfExist == null)
                            ? Functions.StrNull
                            : ((bool)TotalEstimateConfExist).ToString();

                    return String.Concat(
                        $"{nameof(Id)}: {Id}, ",
                        $"{nameof(AuxConfExist)}: {auxConfExist}, ",
                        $"{nameof(TotalEstimateConfExist)}: {totalEstimateConfExist}");
                }

                #endregion Methods

            }


            /// <summary>
            /// List of controls if there is a row in tables nfiesta.t_aux_conf and nfiesta.t_total_estimate_conf
            /// </summary>
            public class TFnApiBeforeDeletePanelRefYearSetGroupList
                : AParametrizedView
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema =
                    NfiEstaFunctions.FnApiBeforeDeletePanelRefYearSetGroup.SchemaName;

                /// <summary>
                /// Stored procedure name
                /// </summary>
                public static readonly string Name =
                    NfiEstaFunctions.FnApiBeforeDeletePanelRefYearSetGroup.Name;

                /// <summary>
                /// Stored procedure alias
                /// </summary>
                public static readonly string Alias =
                    NfiEstaFunctions.FnApiBeforeDeletePanelRefYearSetGroup.Name;

                /// <summary>
                /// Description in national language
                /// </summary>
                public static readonly string CaptionCs =
                    "Kontrola existence záznamů v tabulkách nfiesta.t_aux_conf a nfiesta.t_total_estimate_conf";

                /// <summary>
                /// Description in English
                /// </summary>
                public static readonly string CaptionEn =
                    "List of controls if there is a row in tables nfiesta.t_aux_conf and nfiesta.t_total_estimate_conf";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
                {
                    { "id", new ColumnMetadata()
                    {
                        Name = "id",
                        DbName = "id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = true,
                        NotNull = true,
                        DefaultValue = "0",
                        FuncCall = "row_number() over()",
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ID",
                        HeaderTextEn = "ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 0
                    }
                    },
                    { "aux_conf_exist", new ColumnMetadata()
                    {
                        Name = "aux_conf_exist",
                        DbName = "_t_aux_conf_exi",
                        DataType = "System.Boolean",
                        DbDataType = "bool",
                        NewDataType = "bool",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "AUX_CONF_EXIST",
                        HeaderTextEn = "AUX_CONF_EXIST",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 1
                    }
                    },
                    { "total_estimate_conf_exist", new ColumnMetadata()
                    {
                        Name = "total_estimate_conf_exist",
                        DbName = "_t_total_estimate_conf_exi",
                        DataType = "System.Boolean",
                        DbDataType = "bool",
                        NewDataType = "bool",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "TOTAL_ESTIMATE_CONF_EXIST",
                        HeaderTextEn = "TOTAL_ESTIMATE_CONF_EXIST",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 2
                    }
                    }
                };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column aux_conf_exist metadata
                /// </summary>
                public static readonly ColumnMetadata ColAuxConfExist = Cols["aux_conf_exist"];

                /// <summary>
                /// Column total_estimate_conf_exist metadata
                /// </summary>
                public static readonly ColumnMetadata ColTotalEstimateConfExist = Cols["total_estimate_conf_exist"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Private Fields

                /// <summary>
                /// First parameter - Panel reference year set group identifier
                /// </summary>
                private Nullable<int> panelRefYearSetGroupId;

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TFnApiBeforeDeletePanelRefYearSetGroupList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                {
                    PanelRefYearSetGroupId = null;
                }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TFnApiBeforeDeletePanelRefYearSetGroupList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                {
                    PanelRefYearSetGroupId = null;
                }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// First parameter - Panel reference year set group identifier
                /// </summary>
                public Nullable<int> PanelRefYearSetGroupId
                {
                    get
                    {
                        return panelRefYearSetGroupId;
                    }
                    set
                    {
                        panelRefYearSetGroupId = value;
                    }
                }

                /// <summary>
                /// List of controls if there is a row in tables nfiesta.t_aux_conf and nfiesta.t_total_estimate_conf (read-only)
                /// </summary>
                public List<TFnApiBeforeDeletePanelRefYearSetGroup> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new TFnApiBeforeDeletePanelRefYearSetGroup(composite: this, data: a))];
                    }
                }

                /// <summary>
                /// Row in table nfiesta.t_aux_conf exists (read-only)
                /// </summary>
                public Nullable<bool> AuxConfExist
                {
                    get
                    {
                        return
                            Items
                                .FirstOrDefault<TFnApiBeforeDeletePanelRefYearSetGroup>()
                                ?.AuxConfExist;
                    }
                }

                /// <summary>
                /// Row in table nfiesta.t_total_estimate_conf exists (read-only)
                /// </summary>
                public Nullable<bool> TotalEstimateConfExist
                {
                    get
                    {
                        return
                            Items
                                .FirstOrDefault<TFnApiBeforeDeletePanelRefYearSetGroup>()
                                ?.TotalEstimateConfExist;
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// Control if there is a row in tables nfiesta.t_aux_conf and nfiesta.t_total_estimate_conf from list by identifier (read-only)
                /// </summary>
                /// <param name="id">Control if there is a row in tables nfiesta.t_aux_conf and nfiesta.t_total_estimate_conf identifier</param>
                /// <returns>Control if there is a row in tables nfiesta.t_aux_conf and nfiesta.t_total_estimate_conf from list by identifier (null if not found)</returns>
                public TFnApiBeforeDeletePanelRefYearSetGroup this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new TFnApiBeforeDeletePanelRefYearSetGroup(composite: this, data: a))
                                .FirstOrDefault<TFnApiBeforeDeletePanelRefYearSetGroup>();
                    }
                }

                #endregion Indexer


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public TFnApiBeforeDeletePanelRefYearSetGroupList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any()
                        ? new TFnApiBeforeDeletePanelRefYearSetGroupList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime)
                        {
                            PanelRefYearSetGroupId = PanelRefYearSetGroupId
                        }
                        : new TFnApiBeforeDeletePanelRefYearSetGroupList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null)
                        {
                            PanelRefYearSetGroupId = PanelRefYearSetGroupId
                        };
                }

                #endregion Methods

            }

        }
    }
}