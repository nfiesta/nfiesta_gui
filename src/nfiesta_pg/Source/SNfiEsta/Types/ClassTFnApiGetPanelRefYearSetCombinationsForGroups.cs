﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {
            // fn_api_get_panel_refyearset_combinations4groups

            /// <summary>
            /// Equally ordered arrays of panels and reference-year sets belonging
            /// to any group of panels and reference year combinations
            /// (return type of the stored procedure fn_api_get_panel_refyearset_combinations4groups)
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class TFnApiGetPanelRefYearSetCombinationsForGroups(
                TFnApiGetPanelRefYearSetCombinationsForGroupsList composite = null,
                DataRow data = null)
                    : AParametrizedViewEntry<TFnApiGetPanelRefYearSetCombinationsForGroupsList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColId.Name,
                            defaultValue: Int32.Parse(s: TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Panel identifier
                /// </summary>
                public Nullable<int> PanelId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColPanelId.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColPanelId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColPanelId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColPanelId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Panel label in national language
                /// </summary>
                public string PanelLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColPanelLabelCs.Name,
                            defaultValue: TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColPanelLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColPanelLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Panel description in national language
                /// </summary>
                public string PanelDescriptionCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColPanelDescriptionCs.Name,
                            defaultValue: TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColPanelDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColPanelDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Panel extended label in national language (read-only)
                /// </summary>
                public string PanelExtendedLabelCs
                {
                    get
                    {
                        return $"{Id} - {PanelLabelCs}";
                    }
                }

                /// <summary>
                /// Panel label in English
                /// </summary>
                public string PanelLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColPanelLabelEn.Name,
                            defaultValue: TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColPanelLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColPanelLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Panel description in English
                /// </summary>
                public string PanelDescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColPanelDescriptionEn.Name,
                            defaultValue: TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColPanelDescriptionEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColPanelDescriptionEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Panel extended label in English (read-only)
                /// </summary>
                public string PanelExtendedLabelEn
                {
                    get
                    {
                        return $"{Id} - {PanelLabelEn}";
                    }
                }

                /// <summary>
                /// Reference year set identifier
                /// </summary>
                public Nullable<int> ReferenceYearSetId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColReferenceYearSetId.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColReferenceYearSetId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColReferenceYearSetId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColReferenceYearSetId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Reference year set label in national language
                /// </summary>
                public string ReferenceYearSetLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColReferenceYearSetLabelCs.Name,
                            defaultValue: TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColReferenceYearSetLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColReferenceYearSetLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Reference year set description in national language
                /// </summary>
                public string ReferenceYearSetDescriptionCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColReferenceYearSetDescriptionCs.Name,
                            defaultValue: TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColReferenceYearSetDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColReferenceYearSetDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Reference year set extended label in national language (read-only)
                /// </summary>
                public string ReferenceYearSetExtendedLabelCs
                {
                    get
                    {
                        return $"{Id} - {ReferenceYearSetLabelCs}";
                    }
                }

                /// <summary>
                /// Reference year set label in English
                /// </summary>
                public string ReferenceYearSetLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColReferenceYearSetLabelEn.Name,
                            defaultValue: TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColReferenceYearSetLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColReferenceYearSetLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Reference year set description in English
                /// </summary>
                public string ReferenceYearSetDescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColReferenceYearSetDescriptionEn.Name,
                            defaultValue: TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColReferenceYearSetDescriptionEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColReferenceYearSetDescriptionEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Reference year set extended label in English (read-only)
                /// </summary>
                public string ReferenceYearSetExtendedLabelEn
                {
                    get
                    {
                        return $"{Id} - {ReferenceYearSetLabelEn}";
                    }
                }

                /// <summary>
                /// Number of clusters
                /// </summary>
                public Nullable<int> ClusterCount
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColClusterCount.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColClusterCount.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColClusterCount.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColClusterCount.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Number of plots
                /// </summary>
                public Nullable<int> PlotCount
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColPlotCount.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColPlotCount.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColPlotCount.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TFnApiGetPanelRefYearSetCombinationsForGroupsList.ColPlotCount.Name,
                            val: value);
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not TFnApiGetPanelRefYearSetCombinationsForGroups, return False
                    if (obj is not TFnApiGetPanelRefYearSetCombinationsForGroups)
                    {
                        return false;
                    }

                    return
                        Id == ((TFnApiGetPanelRefYearSetCombinationsForGroups)obj).Id;
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                /// <summary>
                /// Text description of the object
                /// </summary>
                /// <returns>Text description of the object</returns>
                public override string ToString()
                {
                    return String.Concat(
                        $"{nameof(Id)}: {Id}, ",
                        $"{nameof(PanelId)}: {PanelId}, ",
                        $"{nameof(ReferenceYearSetId)}: {ReferenceYearSetId}");
                }

                #endregion Methods

            }


            /// <summary>
            /// List of equally ordered arrays of panels and reference-year sets belonging
            /// to any group of panels and reference year combinations
            /// (return type of the stored procedure fn_api_get_panel_refyearset_combinations4groups)
            /// </summary>
            public class TFnApiGetPanelRefYearSetCombinationsForGroupsList
                 : AParametrizedView
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema =
                    NfiEstaFunctions.FnApiGetPanelRefYearSetCombinationsForGroups.SchemaName;

                /// <summary>
                /// Stored procedure name
                /// </summary>
                public static readonly string Name =
                    NfiEstaFunctions.FnApiGetPanelRefYearSetCombinationsForGroups.Name;

                /// <summary>
                /// Stored procedure alias
                /// </summary>
                public static readonly string Alias =
                    NfiEstaFunctions.FnApiGetPanelRefYearSetCombinationsForGroups.Name;

                /// <summary>
                /// Description in national language
                /// </summary>
                public static readonly string CaptionCs = String.Concat(
                    "Seznam stejně uspořádaných polí panelů a sad referenčních roků, které k nim patří ",
                    "k libovolné skupině kombinací panelů a referenčních roků ",
                    "(návratový typ uložené procedury fn_api_get_panel_refyearset_combinations4groups)");

                /// <summary>
                /// Description in English
                /// </summary>
                public static readonly string CaptionEn = String.Concat(
                    "List of equally ordered arrays of panels and reference-year sets belonging ",
                    "to any group of panels and reference year combinations ",
                    "(return type of the stored procedure fn_api_get_panel_refyearset_combinations4groups)");

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
                {
                    { "id", new ColumnMetadata()
                    {
                        Name = "id",
                        DbName = "id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = true,
                        NotNull = true,
                        DefaultValue = "0",
                        FuncCall = "row_number() over()",
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ID",
                        HeaderTextEn = "ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 0
                    }
                    },
                    { "panel_id", new ColumnMetadata()
                    {
                        Name = "panel_id",
                        DbName = "panel",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PANEL_ID",
                        HeaderTextEn = "PANEL_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 1
                    }
                    },
                    { "panel_label_cs", new ColumnMetadata()
                    {
                        Name = "panel_label_cs",
                        DbName = "panel_label",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PANEL_LABEL_CS",
                        HeaderTextEn = "PANEL_LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 2
                    }
                    },
                    { "panel_description_cs", new ColumnMetadata()
                    {
                        Name = "panel_description_cs",
                        DbName = "panel_description",
                        DataType = "System.String",
                        DbDataType = "text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PANEL_DESCRIPTION_CS",
                        HeaderTextEn = "PANEL_DESCRIPTION_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 3
                    }
                    },
                    { "panel_label_en", new ColumnMetadata()
                    {
                        Name = "panel_label_en",
                        DbName = "panel_label_en",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PANEL_LABEL_EN",
                        HeaderTextEn = "PANEL_LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 4
                    }
                    },
                    { "panel_description_en", new ColumnMetadata()
                    {
                        Name = "panel_description_en",
                        DbName = "panel_description_en",
                        DataType = "System.String",
                        DbDataType = "text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PANEL_DESCRIPTION_EN",
                        HeaderTextEn = "PANEL_DESCRIPTION_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 5
                    }
                    },
                    { "reference_year_set_id", new ColumnMetadata()
                    {
                        Name = "reference_year_set_id",
                        DbName = "reference_year_set",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "REFERENCE_YEAR_SET_ID",
                        HeaderTextEn = "REFERENCE_YEAR_SET_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 6
                    }
                    },
                    { "reference_year_set_label_cs", new ColumnMetadata()
                    {
                        Name = "reference_year_set_label_cs",
                        DbName = "refyearset_label",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "REFERENCE_YEAR_SET_LABEL_CS",
                        HeaderTextEn = "REFERENCE_YEAR_SET_LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 7
                    }
                    },
                    { "reference_year_set_description_cs", new ColumnMetadata()
                    {
                        Name = "reference_year_set_description_cs",
                        DbName = "refyearset_description",
                        DataType = "System.String",
                        DbDataType = "text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "REFERENCE_YEAR_SET_DESCRIPTION_CS",
                        HeaderTextEn = "REFERENCE_YEAR_SET_DESCRIPTION_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 8
                    }
                    },
                    { "reference_year_set_label_en", new ColumnMetadata()
                    {
                        Name = "reference_year_set_label_en",
                        DbName = "refyearset_label_en",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "REFERENCE_YEAR_SET_LABEL_EN",
                        HeaderTextEn = "REFERENCE_YEAR_SET_LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 9
                    }
                    },
                    { "reference_year_set_description_en", new ColumnMetadata()
                    {
                        Name = "reference_year_set_description_en",
                        DbName = "refyearset_description_en",
                        DataType = "System.String",
                        DbDataType = "text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "REFERENCE_YEAR_SET_DESCRIPTION_EN",
                        HeaderTextEn = "REFERENCE_YEAR_SET_DESCRIPTION_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 10
                    }
                    },
                    { "cluster_count", new ColumnMetadata()
                    {
                        Name = "cluster_count",
                        DbName = "cluster_count",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "CLUSTER_COUNT",
                        HeaderTextEn = "CLUSTER_COUNT",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 11
                    }
                    },
                    { "plot_count", new ColumnMetadata()
                    {
                        Name = "plot_count",
                        DbName = "plot_count",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PLOT_COUNT",
                        HeaderTextEn = "PLOT_COUNT",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 12
                    }
                    },
                };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column panel_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColPanelId = Cols["panel_id"];

                /// <summary>
                /// Column panel_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColPanelLabelCs = Cols["panel_label_cs"];

                /// <summary>
                /// Column panel_description_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColPanelDescriptionCs = Cols["panel_description_cs"];

                /// <summary>
                /// Column panel_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColPanelLabelEn = Cols["panel_label_en"];

                /// <summary>
                /// Column panel_description_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColPanelDescriptionEn = Cols["panel_description_en"];

                /// <summary>
                /// Column reference_year_set_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColReferenceYearSetId = Cols["reference_year_set_id"];

                /// <summary>
                /// Column reference_year_set_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColReferenceYearSetLabelCs = Cols["reference_year_set_label_cs"];

                /// <summary>
                /// Column reference_year_set_description_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColReferenceYearSetDescriptionCs = Cols["reference_year_set_description_cs"];

                /// <summary>
                /// Column reference_year_set_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColReferenceYearSetLabelEn = Cols["reference_year_set_label_en"];

                /// <summary>
                /// Column reference_year_set_description_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColReferenceYearSetDescriptionEn = Cols["reference_year_set_description_en"];

                /// <summary>
                /// Column cluster_count metadata
                /// </summary>
                public static readonly ColumnMetadata ColClusterCount = Cols["cluster_count"];

                /// <summary>
                /// Column plot_count metadata
                /// </summary>
                public static readonly ColumnMetadata ColPlotCount = Cols["plot_count"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Private Fields

                /// <summary>
                /// 1st parameter: List of panel reference year set group identifiers
                /// </summary>
                private List<Nullable<int>> panelRefYearSetGroupsIds;

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TFnApiGetPanelRefYearSetCombinationsForGroupsList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                {
                    PanelRefYearSetGroupsIds = [];
                }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TFnApiGetPanelRefYearSetCombinationsForGroupsList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                {
                    PanelRefYearSetGroupsIds = [];
                }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// 1st parameter: List of panel reference year set group identifiers
                /// </summary>
                public List<Nullable<int>> PanelRefYearSetGroupsIds
                {
                    get
                    {
                        return panelRefYearSetGroupsIds ?? [];
                    }
                    set
                    {
                        panelRefYearSetGroupsIds = value ?? [];
                    }
                }

                /// <summary>
                /// List of items (read-only)
                /// </summary>
                public List<TFnApiGetPanelRefYearSetCombinationsForGroups> Items
                {
                    get
                    {
                        return
                            [..
                                Data.AsEnumerable()
                                .Select(a => new TFnApiGetPanelRefYearSetCombinationsForGroups(composite: this, data: a))
                            ];
                    }
                }

                /// <summary>
                /// List of panels identitiers
                /// </summary>
                public List<Nullable<int>> PanelsIds
                {
                    get
                    {
                        return
                            [..
                                Data.AsEnumerable()
                                .Select(a => a.Field<Nullable<int>>(
                                    columnName: ColPanelId.Name))
                            ];
                    }
                }

                /// <summary>
                /// List of reference year sets identifiers
                /// </summary>
                public List<Nullable<int>> ReferenceYearSetsIds
                {
                    get
                    {
                        return
                            [..
                                Data.AsEnumerable()
                                .Select(a => a.Field<Nullable<int>>(
                                    columnName: ColReferenceYearSetId.Name))
                            ];
                    }
                }

                #endregion Properties


                #region Indexers

                /// <summary>
                /// Row from list of rows by identifier (read-only)
                /// </summary>
                /// <param name="id">Row identifier</param>
                /// <returns>Row from list of rows by identifier (null if not found)</returns>
                public TFnApiGetPanelRefYearSetCombinationsForGroups this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new TFnApiGetPanelRefYearSetCombinationsForGroups(composite: this, data: a))
                                .FirstOrDefault<TFnApiGetPanelRefYearSetCombinationsForGroups>();
                    }
                }

                #endregion Indexers


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public TFnApiGetPanelRefYearSetCombinationsForGroupsList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any()
                        ? new TFnApiGetPanelRefYearSetCombinationsForGroupsList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime)
                        {
                            PanelRefYearSetGroupsIds = [.. PanelRefYearSetGroupsIds]
                        }
                        : new TFnApiGetPanelRefYearSetCombinationsForGroupsList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null)
                        {
                            PanelRefYearSetGroupsIds = [.. PanelRefYearSetGroupsIds]
                        };
                }

                #endregion Methods

            }

        }
    }
}