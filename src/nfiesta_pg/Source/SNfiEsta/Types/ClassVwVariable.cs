﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {
            // v_variable

            /// <summary>
            /// Combination of target variable and sub-population category and area domain category (extended version)
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class VwVariable(
                VwVariableList composite = null,
                DataRow data = null)
                    : ALinqViewEntry<VwVariableList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Combination of target variable and sub-population category and area domain category identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return VariableId;
                    }
                    set
                    {
                        VariableId = value;
                    }
                }


                /// <summary>
                /// Combination of target variable and sub-population category and area domain category identifier
                /// </summary>
                public int VariableId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: VwVariableList.ColVariableId.Name,
                            defaultValue: Int32.Parse(s: VwVariableList.ColVariableId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: VwVariableList.ColVariableId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// National label for combination of target variable and sub-population category and area domain category
                /// </summary>
                public string VariableLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: VwVariableList.ColVariableLabelCs.Name,
                            defaultValue: VwVariableList.ColVariableLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: VwVariableList.ColVariableLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// English label for combination of target variable and sub-population category and area domain category
                /// </summary>
                public string VariableLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: VwVariableList.ColVariableLabelEn.Name,
                            defaultValue: VwVariableList.ColVariableLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: VwVariableList.ColVariableLabelEn.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Target variable identifier
                /// </summary>
                public int TargetVariableId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: VwVariableList.ColTargetVariableId.Name,
                            defaultValue: Int32.Parse(s: VwVariableList.ColTargetVariableId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: VwVariableList.ColTargetVariableId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Target variable object (read-only)
                /// </summary>
                public TargetVariable TargetVariable
                {
                    get
                    {
                        return
                             (TargetVariable)((NfiEstaDB)Composite.Database)
                             .SNfiEsta.CTargetVariable[TargetVariableId];
                    }
                }


                /// <summary>
                /// Sub-population category identifier
                /// </summary>
                public int SubPopulationCategoryId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: VwVariableList.ColSubPopulationCategoryId.Name,
                            defaultValue: Int32.Parse(s: VwVariableList.ColSubPopulationCategoryId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: VwVariableList.ColSubPopulationCategoryId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Label of sub-population category in national language
                /// </summary>
                public string SubPopulationCategoryLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: VwVariableList.ColSubPopulationCategoryLabelCs.Name,
                            defaultValue: VwVariableList.ColSubPopulationCategoryLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: VwVariableList.ColSubPopulationCategoryLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Label of sub-population category in English
                /// </summary>
                public string SubPopulationCategoryLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: VwVariableList.ColSubPopulationCategoryLabelEn.Name,
                            defaultValue: VwVariableList.ColSubPopulationCategoryLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: VwVariableList.ColSubPopulationCategoryLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Sub-population category object (read-only)
                /// </summary>
                public SubPopulationCategory SubPopulationCategory
                {
                    get
                    {
                        return
                             ((NfiEstaDB)Composite.Database)
                             .SNfiEsta.CSubPopulationCategory[SubPopulationCategoryId];
                    }
                }


                /// <summary>
                /// Sub-population identifier
                /// </summary>
                public int SubPopulationId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: VwVariableList.ColSubPopulationId.Name,
                            defaultValue: Int32.Parse(s: VwVariableList.ColSubPopulationId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: VwVariableList.ColSubPopulationId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Label of sub-population in national language
                /// </summary>
                public string SubPopulationLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: VwVariableList.ColSubPopulationLabelCs.Name,
                            defaultValue: VwVariableList.ColSubPopulationLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: VwVariableList.ColSubPopulationLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Label of sub-population in English
                /// </summary>
                public string SubPopulationLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: VwVariableList.ColSubPopulationLabelEn.Name,
                            defaultValue: VwVariableList.ColSubPopulationLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: VwVariableList.ColSubPopulationLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Sub-population object (read-only)
                /// </summary>
                public SubPopulation SubPopulation
                {
                    get
                    {
                        return
                             ((NfiEstaDB)Composite.Database)
                             .SNfiEsta.CSubPopulation[SubPopulationId];
                    }
                }


                /// <summary>
                /// Area domain category identifier
                /// </summary>
                public int AreaDomainCategoryId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: VwVariableList.ColAreaDomainCategoryId.Name,
                            defaultValue: Int32.Parse(s: VwVariableList.ColAreaDomainCategoryId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: VwVariableList.ColAreaDomainCategoryId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Label of area domain category in national language
                /// </summary>
                public string AreaDomainCategoryLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: VwVariableList.ColAreaDomainCategoryLabelCs.Name,
                            defaultValue: VwVariableList.ColAreaDomainCategoryLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: VwVariableList.ColAreaDomainCategoryLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Label of area domain category in English
                /// </summary>
                public string AreaDomainCategoryLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: VwVariableList.ColAreaDomainCategoryLabelEn.Name,
                            defaultValue: VwVariableList.ColAreaDomainCategoryLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: VwVariableList.ColAreaDomainCategoryLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Area domain category object (read-only)
                /// </summary>
                public AreaDomainCategory AreaDomainCategory
                {
                    get
                    {
                        return
                             ((NfiEstaDB)Composite.Database)
                             .SNfiEsta.CAreaDomainCategory[AreaDomainCategoryId];
                    }
                }


                /// <summary>
                /// Area domain identifier
                /// </summary>
                public int AreaDomainId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: VwVariableList.ColAreaDomainId.Name,
                            defaultValue: Int32.Parse(s: VwVariableList.ColAreaDomainId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: VwVariableList.ColAreaDomainId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Label of area domain in national language
                /// </summary>
                public string AreaDomainLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: VwVariableList.ColAreaDomainLabelCs.Name,
                            defaultValue: VwVariableList.ColAreaDomainLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: VwVariableList.ColAreaDomainLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Label of area domain in English
                /// </summary>
                public string AreaDomainLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: VwVariableList.ColAreaDomainLabelEn.Name,
                            defaultValue: VwVariableList.ColAreaDomainLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: VwVariableList.ColAreaDomainLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Area domain object (read-only)
                /// </summary>
                public AreaDomain AreaDomain
                {
                    get
                    {
                        return
                             ((NfiEstaDB)Composite.Database)
                             .SNfiEsta.CAreaDomain[AreaDomainId];
                    }
                }


                /// <summary>
                /// Auxiliary variable category identifier
                /// </summary>
                public int AuxiliaryVariableCategoryId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: VwVariableList.ColAuxiliaryVariableCategoryId.Name,
                            defaultValue: Int32.Parse(s: VwVariableList.ColAuxiliaryVariableCategoryId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: VwVariableList.ColAuxiliaryVariableCategoryId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Label of auxiliary variable category in national language
                /// </summary>
                public string AuxiliaryVariableCategoryLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: VwVariableList.ColAuxiliaryVariableCategoryLabelCs.Name,
                            defaultValue: VwVariableList.ColAuxiliaryVariableCategoryLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: VwVariableList.ColAuxiliaryVariableCategoryLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Label of auxiliary variable category in English
                /// </summary>
                public string AuxiliaryVariableCategoryLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: VwVariableList.ColAuxiliaryVariableCategoryLabelEn.Name,
                            defaultValue: VwVariableList.ColAuxiliaryVariableCategoryLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: VwVariableList.ColAuxiliaryVariableCategoryLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Auxiliary variable category object (read-only)
                /// </summary>
                public AuxiliaryVariableCategory AuxiliaryVariableCategory
                {
                    get
                    {
                        return
                             ((NfiEstaDB)Composite.Database)
                             .SNfiEsta.CAuxiliaryVariableCategory[AuxiliaryVariableCategoryId];
                    }
                }


                /// <summary>
                /// Auxiliary variable identifier
                /// </summary>
                public int AuxiliaryVariableId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: VwVariableList.ColAuxiliaryVariableId.Name,
                            defaultValue: Int32.Parse(s: VwVariableList.ColAuxiliaryVariableId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: VwVariableList.ColAuxiliaryVariableId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Label of auxiliary variable in national language
                /// </summary>
                public string AuxiliaryVariableLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: VwVariableList.ColAuxiliaryVariableLabelCs.Name,
                            defaultValue: VwVariableList.ColAuxiliaryVariableLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: VwVariableList.ColAuxiliaryVariableLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Label of auxiliary variable in English
                /// </summary>
                public string AuxiliaryVariableLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: VwVariableList.ColAuxiliaryVariableLabelEn.Name,
                            defaultValue: VwVariableList.ColAuxiliaryVariableLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: VwVariableList.ColAuxiliaryVariableLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Auxiliary variable object (read-only)
                /// </summary>
                public AuxiliaryVariable AuxiliaryVariable
                {
                    get
                    {
                        return
                             ((NfiEstaDB)Composite.Database)
                             .SNfiEsta.CAuxiliaryVariable[AuxiliaryVariableId];
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not VwVariable Type, return False
                    if (obj is not VwVariable)
                    {
                        return false;
                    }

                    return
                        Id == ((VwVariable)obj).Id;
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                #endregion Methods

            }


            /// <summary>
            /// List of combinations of target variable and sub-population category and area domain category (extended version)
            /// </summary>
            public class VwVariableList
                : ALinqView
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema =
                    NfiEstaSchema.Name;

                /// <summary>
                /// Table name
                /// </summary>
                public static readonly string Name =
                    "v_variable";

                /// <summary>
                /// Table alias
                /// </summary>
                public static readonly string Alias =
                    "v_variable";

                /// <summary>
                /// Description in national language
                /// </summary>
                public static readonly string CaptionCs =
                    "Seznam kombinací cílové proměnné a kategorie sub-populace a kategorie plošné domény (rozšířená verze)";

                /// <summary>
                /// Description in English
                /// </summary>
                public static readonly string CaptionEn =
                    "List of combinations of target variable and sub-population category and area domain category (extended version)";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
                {
                    { "variable_id", new ColumnMetadata()
                    {
                        Name = "variable_id",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = true,
                        NotNull = true,
                        DefaultValue = "0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "VARIABLE_ID",
                        HeaderTextEn = "VARIABLE_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 0
                    }
                    },
                    { "variable_label_cs", new ColumnMetadata()
                    {
                        Name = "variable_label_cs",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "VARIABLE_LABEL_CS",
                        HeaderTextEn = "VARIABLE_LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 1
                    }
                    },
                    { "variable_label_en", new ColumnMetadata()
                    {
                        Name = "variable_label_en",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "VARIABLE_LABEL_EN",
                        HeaderTextEn = "VARIABLE_LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 2
                    }
                    },
                    { "target_variable_id", new ColumnMetadata()
                    {
                        Name = "target_variable_id",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue = "0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "TARGET_VARIABLE_ID",
                        HeaderTextEn = "TARGET_VARIABLE_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 3
                    }
                    },
                    { "sub_population_category_id", new ColumnMetadata()
                    {
                        Name = "sub_population_category_id",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue = "0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "SUB_POPULATION_CATEGORY_ID",
                        HeaderTextEn = "SUB_POPULATION_CATEGORY_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 4
                    }
                    },
                    { "sub_population_category_label_cs", new ColumnMetadata()
                    {
                        Name = "sub_population_category_label_cs",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "SUB_POPULATION_CATEGORY_LABEL_CS",
                        HeaderTextEn = "SUB_POPULATION_CATEGORY_LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 5
                    }
                    },
                    { "sub_population_category_label_en", new ColumnMetadata()
                    {
                        Name = "sub_population_category_label_en",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "SUB_POPULATION_CATEGORY_LABEL_EN",
                        HeaderTextEn = "SUB_POPULATION_CATEGORY_LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 6
                    }
                    },
                    { "sub_population_id", new ColumnMetadata()
                    {
                        Name = "sub_population_id",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue = "0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "SUB_POPULATION_ID",
                        HeaderTextEn = "SUB_POPULATION_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 7
                    }
                    },
                    { "sub_population_label_cs", new ColumnMetadata()
                    {
                        Name = "sub_population_label_cs",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "SUB_POPULATION_LABEL_CS",
                        HeaderTextEn = "SUB_POPULATION_LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 8
                    }
                    },
                    { "sub_population_label_en", new ColumnMetadata()
                    {
                        Name = "sub_population_label_en",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "SUB_POPULATION_LABEL_EN",
                        HeaderTextEn = "SUB_POPULATION_LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 9
                    }
                    },
                    { "area_domain_category_id", new ColumnMetadata()
                    {
                        Name = "area_domain_category_id",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue = "0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "AREA_DOMAIN_CATEGORY_ID",
                        HeaderTextEn = "AREA_DOMAIN_CATEGORY_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 10
                    }
                    },
                    { "area_domain_category_label_cs", new ColumnMetadata()
                    {
                        Name = "area_domain_category_label_cs",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "AREA_DOMAIN_CATEGORY_LABEL_CS",
                        HeaderTextEn = "AREA_DOMAIN_CATEGORY_LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 11
                    }
                    },
                    { "area_domain_category_label_en", new ColumnMetadata()
                    {
                        Name = "area_domain_category_label_en",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "AREA_DOMAIN_CATEGORY_LABEL_EN",
                        HeaderTextEn = "AREA_DOMAIN_CATEGORY_LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 12
                    }
                    },
                    { "area_domain_id", new ColumnMetadata()
                    {
                        Name = "area_domain_id",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue = "0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "AREA_DOMAIN_ID",
                        HeaderTextEn = "AREA_DOMAIN_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 13
                    }
                    },
                    { "area_domain_label_cs", new ColumnMetadata()
                    {
                        Name = "area_domain_label_cs",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "AREA_DOMAIN_LABEL_CS",
                        HeaderTextEn = "AREA_DOMAIN_LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 14
                    }
                    },
                    { "area_domain_label_en", new ColumnMetadata()
                    {
                        Name = "area_domain_label_en",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "AREA_DOMAIN_LABEL_EN",
                        HeaderTextEn = "AREA_DOMAIN_LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 15
                    }
                    },
                    { "auxiliary_variable_category_id", new ColumnMetadata()
                    {
                        Name = "auxiliary_variable_category_id",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue = "0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "AUXILIARY_VARIABLE_CATEGORY_ID",
                        HeaderTextEn = "AUXILIARY_VARIABLE_CATEGORY_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 16
                    }
                    },
                    { "auxiliary_variable_category_label_cs", new ColumnMetadata()
                    {
                        Name = "auxiliary_variable_category_label_cs",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "AUXILIARY_VARIABLE_CATEGORY_LABEL_CS",
                        HeaderTextEn = "AUXILIARY_VARIABLE_CATEGORY_LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 17
                    }
                    },
                    { "auxiliary_variable_category_label_en", new ColumnMetadata()
                    {
                        Name = "auxiliary_variable_category_label_en",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "AUXILIARY_VARIABLE_CATEGORY_LABEL_EN",
                        HeaderTextEn = "AUXILIARY_VARIABLE_CATEGORY_LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 18
                    }
                    },
                     { "auxiliary_variable_id", new ColumnMetadata()
                    {
                        Name = "auxiliary_variable_id",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue = "0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "AUXILIARY_VARIABLE_ID",
                        HeaderTextEn = "AUXILIARY_VARIABLE_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 19
                    }
                    },
                    { "auxiliary_variable_label_cs", new ColumnMetadata()
                    {
                        Name = "auxiliary_variable_label_cs",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "AUXILIARY_VARIABLE_LABEL_CS",
                        HeaderTextEn = "AUXILIARY_VARIABLE_LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 20
                    }
                    },
                    { "auxiliary_variable_label_en", new ColumnMetadata()
                    {
                        Name = "auxiliary_variable_label_en",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "AUXILIARY_VARIABLE_LABEL_EN",
                        HeaderTextEn = "AUXILIARY_VARIABLE_LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 21
                    }
                    }
                };

                /// <summary>
                /// Column variable_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColVariableId = Cols["variable_id"];

                /// <summary>
                /// Column variable_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColVariableLabelCs = Cols["variable_label_cs"];

                /// <summary>
                /// Column variable_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColVariableLabelEn = Cols["variable_label_en"];

                /// <summary>
                /// Column target_variable_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColTargetVariableId = Cols["target_variable_id"];

                /// <summary>
                /// Column sub_population_category_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColSubPopulationCategoryId = Cols["sub_population_category_id"];

                /// <summary>
                /// Column sub_population_category_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColSubPopulationCategoryLabelCs = Cols["sub_population_category_label_cs"];

                /// <summary>
                /// Column sub_population_category_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColSubPopulationCategoryLabelEn = Cols["sub_population_category_label_en"];

                /// <summary>
                /// Column sub_population_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColSubPopulationId = Cols["sub_population_id"];

                /// <summary>
                /// Column sub_population_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColSubPopulationLabelCs = Cols["sub_population_label_cs"];

                /// <summary>
                /// Column sub_population_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColSubPopulationLabelEn = Cols["sub_population_label_en"];

                /// <summary>
                /// Column area_domain_category_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColAreaDomainCategoryId = Cols["area_domain_category_id"];

                /// <summary>
                /// Column area_domain_category_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColAreaDomainCategoryLabelCs = Cols["area_domain_category_label_cs"];

                /// <summary>
                /// Column area_domain_category_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColAreaDomainCategoryLabelEn = Cols["area_domain_category_label_en"];

                /// <summary>
                /// Column area_domain_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColAreaDomainId = Cols["area_domain_id"];

                /// <summary>
                /// Column area_domain_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColAreaDomainLabelCs = Cols["area_domain_label_cs"];

                /// <summary>
                /// Column area_domain_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColAreaDomainLabelEn = Cols["area_domain_label_en"];

                /// <summary>
                /// Column auxiliary_variable_category_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColAuxiliaryVariableCategoryId = Cols["auxiliary_variable_category_id"];

                /// <summary>
                /// Column auxiliary_variable_category_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColAuxiliaryVariableCategoryLabelCs = Cols["auxiliary_variable_category_label_cs"];

                /// <summary>
                /// Column auxiliary_variable_category_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColAuxiliaryVariableCategoryLabelEn = Cols["auxiliary_variable_category_label_en"];

                /// <summary>
                /// Column auxiliary_variable_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColAuxiliaryVariableId = Cols["auxiliary_variable_id"];

                /// <summary>
                /// Column auxiliary_variable_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColAuxiliaryVariableLabelCs = Cols["auxiliary_variable_label_cs"];

                /// <summary>
                /// Column auxiliary_variable_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColAuxiliaryVariableLabelEn = Cols["auxiliary_variable_label_en"];

                private const string altogetherCs = "bez rozlišení";
                private const string altogetherEn = "altogether";

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public VwVariableList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public VwVariableList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// List of combinations of target variable and sub-population category and area domain category (extended version)
                /// </summary>
                public List<VwVariable> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new VwVariable(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// Combination of target variable and sub-population category and area domain category (extended version) from list by identifier (read-only)
                /// </summary>
                /// <param name="id">Combination of target variable and sub-population category and area domain category identifier</param>
                /// <returns>Combination of target variable and sub-population category and area domain category (extended version) from list by identifier (null if not found)</returns>
                public VwVariable this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColVariableId.Name) == id)
                                .Select(a => new VwVariable(composite: this, data: a))
                                .FirstOrDefault<VwVariable>();
                    }
                }

                #endregion Indexer


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public VwVariableList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new VwVariableList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new VwVariableList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                /// <summary>
                /// List of combinations of target variable and sub-population category and area domain category
                /// for selected target variables, area domain categories,
                /// subpopulation categories and auxiliary variable categories
                /// </summary>
                /// <param name="targetVariableId">Selected target variables identifiers</param>
                /// <param name="areaDomainCategoryId">Selected area domain categories identifiers</param>
                /// <param name="subPopulationCategoryId">Selected subpopulation categories identifiers</param>
                /// <param name="auxiliaryVariableCategoryId">Selected auxiliary variable categories identifiers</param>
                /// <returns>
                /// List of combinations of target variable and sub-population category and area domain category
                /// for selected target variables, area domain categories,
                /// subpopulation categories and auxiliary variable categories
                /// </returns>
                public VwVariableList Reduce(
                    List<int> targetVariableId = null,
                    List<int> areaDomainCategoryId = null,
                    List<int> subPopulationCategoryId = null,
                    List<int> auxiliaryVariableCategoryId = null)
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();

                    if ((targetVariableId != null) && targetVariableId.Count != 0)
                    {
                        rows = rows.Where(a => targetVariableId.Contains<int>(
                                value: (int)(a.Field<Nullable<int>>(columnName: ColTargetVariableId.Name) ?? 0)));
                    }

                    if ((areaDomainCategoryId != null) && areaDomainCategoryId.Count != 0)
                    {
                        rows = rows.Where(a => areaDomainCategoryId.Contains<int>(
                                value: (int)(a.Field<Nullable<int>>(columnName: ColAreaDomainCategoryId.Name) ?? 0)));
                    }

                    if ((subPopulationCategoryId != null) && subPopulationCategoryId.Count != 0)
                    {
                        rows = rows.Where(a => subPopulationCategoryId.Contains<int>(
                                value: (int)(a.Field<Nullable<int>>(columnName: ColSubPopulationCategoryId.Name) ?? 0)));
                    }

                    if ((auxiliaryVariableCategoryId != null) && auxiliaryVariableCategoryId.Count != 0)
                    {
                        rows = rows.Where(a => auxiliaryVariableCategoryId.Contains<int>(
                                value: (int)(a.Field<Nullable<int>>(columnName: ColAuxiliaryVariableCategoryId.Name) ?? 0)));
                    }

                    return
                        rows.Any() ?
                        new VwVariableList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new VwVariableList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                /// <summary>
                /// Loads object data
                /// (if it was not done before)
                /// </summary>
                /// <param name="tVariable">List of combinations of target variable and sub-population category and area domain category</param>
                /// <param name="cSubPopulationCategory">List of sub-population categories</param>
                /// <param name="cSubPopulation">List of sub-populations</param>
                /// <param name="cAreaDomainCategory">List of area domain categories</param>
                /// <param name="cAreaDomain">List of area domains</param>
                /// <param name="cAuxiliaryVariableCategory">List of auxiliary variable categories</param>
                /// <param name="cAuxiliaryVariable">List of auxiliary variables</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                public void Load(
                    VariableList tVariable,
                    SubPopulationCategoryList cSubPopulationCategory,
                    SubPopulationList cSubPopulation,
                    AreaDomainCategoryList cAreaDomainCategory,
                    AreaDomainList cAreaDomain,
                    AuxiliaryVariableCategoryList cAuxiliaryVariableCategory,
                    AuxiliaryVariableList cAuxiliaryVariable,
                    string condition = null,
                    Nullable<int> limit = null)
                {
                    if (!Loaded)
                    {
                        ReLoad(
                            tVariable: tVariable,
                            cSubPopulationCategory: cSubPopulationCategory,
                            cSubPopulation: cSubPopulation,
                            cAreaDomainCategory: cAreaDomainCategory,
                            cAreaDomain: cAreaDomain,
                            cAuxiliaryVariableCategory: cAuxiliaryVariableCategory,
                            cAuxiliaryVariable: cAuxiliaryVariable,
                            condition: condition,
                            limit: limit);
                    }
                }

                /// <summary>
                /// Loads object data
                /// </summary>
                /// <param name="tVariable">List of combinations of target variable and sub-population category and area domain category</param>
                /// <param name="cSubPopulationCategory">List of sub-population categories</param>
                /// <param name="cSubPopulation">List of sub-populations</param>
                /// <param name="cAreaDomainCategory">List of area domain categories</param>
                /// <param name="cAreaDomain">List of area domains</param>
                /// <param name="cAuxiliaryVariableCategory">List of auxiliary variable categories</param>
                /// <param name="cAuxiliaryVariable">List of auxiliary variables</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                public void ReLoad(
                    VariableList tVariable,
                    SubPopulationCategoryList cSubPopulationCategory,
                    SubPopulationList cSubPopulation,
                    AreaDomainCategoryList cAreaDomainCategory,
                    AreaDomainList cAreaDomain,
                    AuxiliaryVariableCategoryList cAuxiliaryVariableCategory,
                    AuxiliaryVariableList cAuxiliaryVariable,
                    string condition = null,
                    Nullable<int> limit = null)
                {
                    Data = EmptyDataTable(
                                tableName: TableName,
                                columns: Columns);
                    Condition = condition;
                    Limit = limit;

                    System.Diagnostics.Stopwatch stopWatch = new();
                    stopWatch.Start();

                    // Atributové kategorie
                    var cteVariable =
                        tVariable.Data.AsEnumerable()
                        .Select(a => new
                        {
                            VariableId = a.Field<int>(VariableList.ColId.Name),
                            TargetVariableId = a.Field<Nullable<int>>(VariableList.ColTargetVariableId.Name) ?? 0,
                            SubPopulationCategoryId = a.Field<Nullable<int>>(VariableList.ColSubPopulationCategoryId.Name) ?? 0,
                            AreaDomainCategoryId = a.Field<Nullable<int>>(VariableList.ColAreaDomainCategoryId.Name) ?? 0,
                            AuxiliaryVariableCategoryId = a.Field<Nullable<int>>(VariableList.ColAuxiliaryVariableCategoryId.Name) ?? 0
                        });

                    // Kategorie subpopulace
                    var cteSubPopulationCategory =
                        cSubPopulationCategory.Data.AsEnumerable()
                        .Select(a => new
                        {
                            SubPopulationCategoryId = a.Field<int>(SubPopulationCategoryList.ColId.Name),
                            SubPopulationId = a.Field<int>(SubPopulationCategoryList.ColSubPopulationId.Name),
                            SubPopulationCategoryLabelCs = a.Field<string>(SubPopulationCategoryList.ColLabelCs.Name),
                            SubPopulationCategoryLabelEn = a.Field<string>(SubPopulationCategoryList.ColLabelEn.Name)
                        });

                    // Subpopulace
                    var cteSubPopulation =
                       cSubPopulation.Data.AsEnumerable()
                       .Select(a => new
                       {
                           SubPopulationId = a.Field<int>(SubPopulationList.ColId.Name),
                           SubPopulationLabelCs = a.Field<string>(SubPopulationList.ColLabelCs.Name),
                           SubPopulationLabelEn = a.Field<string>(SubPopulationList.ColLabelEn.Name)
                       });

                    // Kategorie plošné domény
                    var cteAreaDomainCategory =
                       cAreaDomainCategory.Data.AsEnumerable()
                       .Select(a => new
                       {
                           AreaDomainCategoryId = a.Field<int>(AreaDomainCategoryList.ColId.Name),
                           AreaDomainId = a.Field<int>(AreaDomainCategoryList.ColAreaDomainId.Name),
                           AreaDomainCategoryLabelCs = a.Field<string>(AreaDomainCategoryList.ColLabelCs.Name),
                           AreaDomainCategoryLabelEn = a.Field<string>(AreaDomainCategoryList.ColLabelEn.Name)
                       });

                    // Plošná doména
                    var cteAreaDomain =
                      cAreaDomain.Data.AsEnumerable()
                      .Select(a => new
                      {
                          AreaDomainId = a.Field<int>(AreaDomainList.ColId.Name),
                          AreaDomainLabelCs = a.Field<string>(AreaDomainList.ColLabelCs.Name),
                          AreaDomainLabelEn = a.Field<string>(AreaDomainList.ColLabelEn.Name)
                      });

                    // Kategorie pomocné proměnné
                    var cteAuxiliaryVariableCategory =
                       cAuxiliaryVariableCategory.Data.AsEnumerable()
                       .Select(a => new
                       {
                           AuxiliaryVariableCategoryId = a.Field<int>(AuxiliaryVariableCategoryList.ColId.Name),
                           AuxiliaryVariableId = a.Field<int>(AuxiliaryVariableCategoryList.ColAuxiliaryVariableId.Name),
                           AuxiliaryVariableCategoryLabelCs = a.Field<string>(AuxiliaryVariableCategoryList.ColLabelCs.Name),
                           AuxiliaryVariableCategoryLabelEn = a.Field<string>(AuxiliaryVariableCategoryList.ColLabelEn.Name)
                       });

                    // Pomocná proměnná
                    var cteAuxiliaryVariable =
                      cAuxiliaryVariable.Data.AsEnumerable()
                      .Select(a => new
                      {
                          AuxiliaryVariableId = a.Field<int>(AuxiliaryVariableList.ColId.Name),
                          AuxiliaryVariableLabelCs = a.Field<string>(AuxiliaryVariableList.ColLabelCs.Name),
                          AuxiliaryVariableLabelEn = a.Field<string>(AuxiliaryVariableList.ColLabelEn.Name)
                      });

                    // LEFT JOIN t_variable + c_subpopulation_category
                    var cteVariableSubPopulationCategory =
                        from a in cteVariable
                        join b in cteSubPopulationCategory
                        on a.SubPopulationCategoryId equals b.SubPopulationCategoryId into lj
                        from b in lj.DefaultIfEmpty()
                        select new
                        {
                            a.VariableId,
                            a.TargetVariableId,
                            a.SubPopulationCategoryId,
                            b?.SubPopulationCategoryLabelCs,
                            b?.SubPopulationCategoryLabelEn,
                            SubPopulationId = (b != null) ? b.SubPopulationId : 0,
                            a.AreaDomainCategoryId,
                            a.AuxiliaryVariableCategoryId
                        };

                    // LEFT JOIN t_variable + c_subpopulation
                    var cteVariableSubPopulation =
                        from a in cteVariableSubPopulationCategory
                        join b in cteSubPopulation
                        on a.SubPopulationId equals b.SubPopulationId into lj
                        from b in lj.DefaultIfEmpty()
                        select new
                        {
                            a.VariableId,
                            a.TargetVariableId,
                            a.SubPopulationCategoryId,
                            a.SubPopulationCategoryLabelCs,
                            a.SubPopulationCategoryLabelEn,
                            a.SubPopulationId,
                            b?.SubPopulationLabelCs,
                            b?.SubPopulationLabelEn,
                            a.AreaDomainCategoryId,
                            a.AuxiliaryVariableCategoryId
                        };

                    // LEFT JOIN t_variable + c_area_domain_category
                    var cteVariableAreaDomainCategory =
                        from a in cteVariableSubPopulation
                        join b in cteAreaDomainCategory
                        on a.AreaDomainCategoryId equals b.AreaDomainCategoryId into lj
                        from b in lj.DefaultIfEmpty()
                        select new
                        {
                            a.VariableId,
                            a.TargetVariableId,
                            a.SubPopulationCategoryId,
                            a.SubPopulationCategoryLabelCs,
                            a.SubPopulationCategoryLabelEn,
                            a.SubPopulationId,
                            a.SubPopulationLabelCs,
                            a.SubPopulationLabelEn,
                            a.AreaDomainCategoryId,
                            b?.AreaDomainCategoryLabelCs,
                            b?.AreaDomainCategoryLabelEn,
                            AreaDomainId = (b != null) ? b.AreaDomainId : 0,
                            a.AuxiliaryVariableCategoryId
                        };

                    // LEFT JOIN t_variable + c_area_domain
                    var cteVariableAreaDomain =
                        from a in cteVariableAreaDomainCategory
                        join b in cteAreaDomain
                        on a.AreaDomainId equals b.AreaDomainId into lj
                        from b in lj.DefaultIfEmpty()
                        select new
                        {
                            a.VariableId,
                            a.TargetVariableId,
                            a.SubPopulationCategoryId,
                            a.SubPopulationCategoryLabelCs,
                            a.SubPopulationCategoryLabelEn,
                            a.SubPopulationId,
                            a.SubPopulationLabelCs,
                            a.SubPopulationLabelEn,
                            a.AreaDomainCategoryId,
                            a.AreaDomainCategoryLabelCs,
                            a.AreaDomainCategoryLabelEn,
                            a.AreaDomainId,
                            b?.AreaDomainLabelCs,
                            b?.AreaDomainLabelEn,
                            a.AuxiliaryVariableCategoryId
                        };

                    // LEFT JOIN t_variable + c_auxiliary_variable_category
                    var cteVariableAuxiliaryVariableCategory =
                        from a in cteVariableAreaDomain
                        join b in cteAuxiliaryVariableCategory
                        on a.AuxiliaryVariableCategoryId equals b.AuxiliaryVariableCategoryId into lj
                        from b in lj.DefaultIfEmpty()
                        select new
                        {
                            a.VariableId,
                            a.TargetVariableId,
                            a.SubPopulationCategoryId,
                            a.SubPopulationCategoryLabelCs,
                            a.SubPopulationCategoryLabelEn,
                            a.SubPopulationId,
                            a.SubPopulationLabelCs,
                            a.SubPopulationLabelEn,
                            a.AreaDomainCategoryId,
                            a.AreaDomainCategoryLabelCs,
                            a.AreaDomainCategoryLabelEn,
                            a.AreaDomainId,
                            a.AreaDomainLabelCs,
                            a.AreaDomainLabelEn,
                            a.AuxiliaryVariableCategoryId,
                            b?.AuxiliaryVariableCategoryLabelCs,
                            b?.AuxiliaryVariableCategoryLabelEn,
                            AuxiliaryVariableId = (b != null) ? b.AuxiliaryVariableId : 0
                        };

                    // LEFT JOIN t_variable + c_auxiliary_variable
                    var cteVariableAuxiliaryVariable =
                        from a in cteVariableAuxiliaryVariableCategory
                        join b in cteAuxiliaryVariable
                        on a.AuxiliaryVariableId equals b.AuxiliaryVariableId into lj
                        from b in lj.DefaultIfEmpty()
                        select new
                        {
                            a.VariableId,
                            a.TargetVariableId,
                            a.SubPopulationCategoryId,
                            a.SubPopulationCategoryLabelCs,
                            a.SubPopulationCategoryLabelEn,
                            a.SubPopulationId,
                            a.SubPopulationLabelCs,
                            a.SubPopulationLabelEn,
                            a.AreaDomainCategoryId,
                            a.AreaDomainCategoryLabelCs,
                            a.AreaDomainCategoryLabelEn,
                            a.AreaDomainId,
                            a.AreaDomainLabelCs,
                            a.AreaDomainLabelEn,
                            a.AuxiliaryVariableCategoryId,
                            a.AuxiliaryVariableCategoryLabelCs,
                            a.AuxiliaryVariableCategoryLabelEn,
                            a.AuxiliaryVariableId,
                            b?.AuxiliaryVariableLabelCs,
                            b?.AuxiliaryVariableLabelEn
                        };

                    // Atributové kategorie
                    var cteAttributeCategory =
                        cteVariableAuxiliaryVariable
                        .Select(a => new
                        {
                            a.VariableId,
                            VariableLabelCs =
                                ((a.AreaDomainCategoryId != 0) && (a.SubPopulationCategoryId != 0)) ?
                                    $"{a.AreaDomainCategoryLabelCs} - {a.SubPopulationCategoryLabelCs}" :
                                ((a.AreaDomainCategoryId != 0) && (a.SubPopulationCategoryId == 0)) ?
                                    $"{a.AreaDomainCategoryLabelCs}" :
                                ((a.AreaDomainCategoryId == 0) && (a.SubPopulationCategoryId != 0)) ?
                                    $"{a.SubPopulationCategoryLabelCs}" :
                                    altogetherCs,
                            VariableLabelEn =
                                ((a.AreaDomainCategoryId != 0) && (a.SubPopulationCategoryId != 0)) ?
                                    $"{a.AreaDomainCategoryLabelEn} - {a.SubPopulationCategoryLabelEn}" :
                                ((a.AreaDomainCategoryId != 0) && (a.SubPopulationCategoryId == 0)) ?
                                    $"{a.AreaDomainCategoryLabelEn}" :
                                ((a.AreaDomainCategoryId == 0) && (a.SubPopulationCategoryId != 0)) ?
                                    $"{a.SubPopulationCategoryLabelEn}" :
                                    altogetherEn,
                            a.TargetVariableId,
                            a.SubPopulationCategoryId,
                            a.SubPopulationCategoryLabelCs,
                            a.SubPopulationCategoryLabelEn,
                            a.SubPopulationId,
                            a.SubPopulationLabelCs,
                            a.SubPopulationLabelEn,
                            a.AreaDomainCategoryId,
                            a.AreaDomainCategoryLabelCs,
                            a.AreaDomainCategoryLabelEn,
                            a.AreaDomainId,
                            a.AreaDomainLabelCs,
                            a.AreaDomainLabelEn,
                            a.AuxiliaryVariableCategoryId,
                            a.AuxiliaryVariableCategoryLabelCs,
                            a.AuxiliaryVariableCategoryLabelEn,
                            a.AuxiliaryVariableId,
                            a.AuxiliaryVariableLabelCs,
                            a.AuxiliaryVariableLabelEn
                        });

                    if (cteAttributeCategory.Any())
                    {
                        Data = cteAttributeCategory
                            .OrderBy(a => a.VariableId)
                            .Select(a =>
                                Data.LoadDataRow(
                                    values:
                                    [
                                        a.VariableId,
                                        a.VariableLabelCs,
                                        a.VariableLabelEn,
                                        a.TargetVariableId,
                                        a.SubPopulationCategoryId,
                                        a.SubPopulationCategoryLabelCs,
                                        a.SubPopulationCategoryLabelEn,
                                        a.SubPopulationId,
                                        a.SubPopulationLabelCs,
                                        a.SubPopulationLabelEn,
                                        a.AreaDomainCategoryId,
                                        a.AreaDomainCategoryLabelCs,
                                        a.AreaDomainCategoryLabelEn,
                                        a.AreaDomainId,
                                        a.AreaDomainLabelCs,
                                        a.AreaDomainLabelEn,
                                        a.AuxiliaryVariableCategoryId,
                                        a.AuxiliaryVariableCategoryLabelCs,
                                        a.AuxiliaryVariableCategoryLabelEn,
                                        a.AuxiliaryVariableId,
                                        a.AuxiliaryVariableLabelCs,
                                        a.AuxiliaryVariableLabelEn
                                    ],
                                    fAcceptChanges: false))
                             .CopyToDataTable();
                        Data.TableName = Name;
                    }
                    else
                    {
                        Data = EmptyDataTable(
                                tableName: TableName,
                                columns: Columns);
                    }

                    // Omezení podmínkou
                    if (!String.IsNullOrEmpty(value: Condition))
                    {
                        DataView view = new(table: Data);
                        try
                        {
                            view.RowFilter = Condition;
                        }
                        catch
                        {
                            view.RowFilter = String.Empty;
                            Condition = String.Empty;
                        }
                        Data = view.ToTable();
                    }

                    // Omezeni počtem
                    if (Limit != null)
                    {
                        Data = Data.AsEnumerable()
                            .Take(count: (int)Limit)
                            .CopyToDataTable();
                    }

                    stopWatch.Stop();
                    LoadingTime = 0.001 * stopWatch.ElapsedMilliseconds;
                    Data.TableName = TableName;
                }

                /// <summary>
                /// List of area domains
                /// for selected target variable
                /// </summary>
                /// <param name="cAreaDomain">List of all area domains,
                /// extended by domain 0 - altogether</param>
                /// <param name="targetVariable">Selected target variable</param>
                /// <returns>
                /// List of area domains
                /// for selected target variable
                /// </returns>
                public AreaDomainList AreaDomains(
                    AreaDomainList cAreaDomain,
                    TargetVariable targetVariable)
                {
                    List<int> areaDomainIds =
                        Data.AsEnumerable()
                        .Where(a => a.Field<int>(ColTargetVariableId.Name) == targetVariable.Id)
                        .Select(a => a.Field<int>(ColAreaDomainId.Name))
                        .Distinct()
                        .ToList();

                    return
                        new AreaDomainList(
                            database: (NfiEstaDB)Database,
                            rows:
                                cAreaDomain.Data.AsEnumerable()
                                .Where(a => areaDomainIds
                                .Contains(a.Field<int>(AreaDomainList.ColId.Name)))
                                .OrderBy(a => a.Field<int>(AreaDomainList.ColId.Name)));
                }

                /// <summary>
                /// List of subpopulations
                /// for selected target variable
                /// </summary>
                /// <param name="cSubPopulation">List of all subpopulations,
                /// extended by domain 0 - altogether</param>
                /// <param name="targetVariable">Selected target variable</param>
                /// <returns>
                /// List of subpopulations
                /// for selected target variable
                /// </returns>
                public SubPopulationList SubPopulations(
                    SubPopulationList cSubPopulation,
                    TargetVariable targetVariable)
                {
                    List<int> subPopulationIds =
                        Data.AsEnumerable()
                        .Where(a => a.Field<int>(ColTargetVariableId.Name) == targetVariable.Id)
                        .Select(a => a.Field<int>(ColSubPopulationId.Name))
                        .Distinct()
                        .ToList();

                    return
                        new SubPopulationList(
                            database: (NfiEstaDB)Database,
                            rows:
                                cSubPopulation.Data.AsEnumerable()
                                .Where(a => subPopulationIds
                                .Contains(a.Field<int>(SubPopulationList.ColId.Name)))
                                .OrderBy(a => a.Field<int>(SubPopulationList.ColId.Name)));
                }

                /// <summary>
                /// List of attribute categories
                /// for selected target variable
                /// and selected area domain and subpopulation categories
                /// </summary>
                /// <param name="targetVariable">Selected target variable</param>
                /// <param name="areaDomainCategories">Selected area domain categories</param>
                /// <param name="subPopulationCategories">Selected subpopulation categories</param>
                /// <returns>
                /// List of attribute categories
                /// for selected target variable
                /// and selected area domain and subpopulation categories
                /// </returns>
                public VariablePairList AttributeCategories(
                    TargetVariable targetVariable,
                    AreaDomainCategoryList areaDomainCategories,
                    SubPopulationCategoryList subPopulationCategories)
                {
                    List<int> areaDomainCategoryIds =
                        areaDomainCategories.Items.Select(a => a.Id).ToList();

                    List<int> subPopulationCategoryIds =
                        subPopulationCategories.Items.Select(a => a.Id).ToList();

                    VwVariableList attributeCategories =
                        new(
                            database: (NfiEstaDB)Database,
                            rows: Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColTargetVariableId.Name) == targetVariable.Id)
                                .Where(a => areaDomainCategoryIds
                                    .Contains(a.Field<int>(VwVariableList.ColAreaDomainCategoryId.Name)))
                                .Where(a => subPopulationCategoryIds
                                    .Contains(a.Field<int>(VwVariableList.ColSubPopulationCategoryId.Name)))
                                .Where(a => a.Field<int>(VwVariableList.ColAuxiliaryVariableCategoryId.Name) == 0)
                                .OrderBy(a => a.Field<int>(VwVariableList.ColVariableId.Name)));

                    if (attributeCategories.Items.Count != 0)
                    {
                        return
                            new VariablePairList(
                                database: (NfiEstaDB)Database,
                                variablePairs: attributeCategories.Items
                                    .Select(a => new VariablePair(numerator: a)));
                    }
                    else
                    {
                        return
                            new VariablePairList(
                                database: (NfiEstaDB)Database);
                    }
                }

                #endregion Methods

            }

        }
    }
}