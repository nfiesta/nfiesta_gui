﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using MathNet.Numerics.Distributions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {
            // v_olap_ratio_estimate_value

            /// <summary>
            /// OLAP Cube Value - Ratio Estimate
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class OLAPRatioEstimateValue(
                OLAPRatioEstimateValueList composite = null,
                DataRow data = null)
                    : ALinqViewEntry<OLAPRatioEstimateValueList>(composite: composite, data: data)
            {

                #region Derived Properties

                #region Dimensions

                /// <summary>
                /// OLAP Cube Dimension - Ratio Estimate - composite identifier
                /// </summary>
                public new string Id
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateValueList.ColId.Name,
                            defaultValue: OLAPRatioEstimateValueList.ColId.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateValueList.ColId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// OLAP Cube Dimension - Ratio Estimate - composite identifier for numerator
                /// </summary>
                public string NumeratorId
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateValueList.ColNumeratorId.Name,
                            defaultValue: OLAPRatioEstimateValueList.ColNumeratorId.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateValueList.ColNumeratorId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// OLAP Cube Dimension - Ratio Estimate - composite identifier for denominator
                /// </summary>
                public string DenominatorId
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateValueList.ColDenominatorId.Name,
                            defaultValue: OLAPRatioEstimateValueList.ColDenominatorId.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateValueList.ColDenominatorId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Estimate phase identifier
                /// </summary>
                public int PhaseEstimateTypeId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: OLAPRatioEstimateValueList.ColPhaseEstimateTypeId.Name,
                            defaultValue: Int32.Parse(s: OLAPRatioEstimateValueList.ColPhaseEstimateTypeId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: OLAPRatioEstimateValueList.ColPhaseEstimateTypeId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Estimate phase object (read-only)
                /// </summary>
                public PhaseEstimateType PhaseEstimateType
                {
                    get
                    {
                        return
                             ((NfiEstaDB)Composite.Database)
                             .SNfiEsta.CPhaseEstimateType[(int)PhaseEstimateTypeId];
                    }
                }


                /// <summary>
                /// Estimation period identifier
                /// </summary>
                public Nullable<int> EstimationPeriodId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: OLAPRatioEstimateValueList.ColEstimationPeriodId.Name,
                            defaultValue:
                                String.IsNullOrEmpty(value: OLAPRatioEstimateValueList.ColEstimationPeriodId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: OLAPRatioEstimateValueList.ColEstimationPeriodId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: OLAPRatioEstimateValueList.ColEstimationPeriodId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Estimation period object (read-only)
                /// </summary>
                public EstimationPeriod EstimationPeriod
                {
                    get
                    {
                        return (EstimationPeriodId != null) ?
                             ((NfiEstaDB)Composite.Database)
                             .SNfiEsta.CEstimationPeriod[(int)EstimationPeriodId] : null;
                    }
                }


                /// <summary>
                /// Numerator - Combination of target variable and sub-population category and area domain category identifier
                /// </summary>
                public int NumeratorVariableId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: OLAPRatioEstimateValueList.ColNumeratorVariableId.Name,
                            defaultValue: Int32.Parse(s: OLAPRatioEstimateValueList.ColNumeratorVariableId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: OLAPRatioEstimateValueList.ColNumeratorVariableId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Numerator - Combination of target variable and sub-population category and area domain category object (read-only)
                /// </summary>
                public Variable NumeratorVariable
                {
                    get
                    {
                        return
                             ((NfiEstaDB)Composite.Database)
                             .SNfiEsta.TVariable[(int)NumeratorVariableId];
                    }
                }

                /// <summary>
                /// Numerator - Combination of target variable and sub-population category and area domain category (extended-version) object (read-only)
                /// </summary>
                public VwVariable NumeratorVwVariable
                {
                    get
                    {
                        return
                             ((NfiEstaDB)Composite.Database)
                             .SNfiEsta.VVariable[(int)NumeratorVariableId];
                    }
                }


                /// <summary>
                /// Denominator - Combination of target variable and sub-population category and area domain category identifier
                /// </summary>
                public int DenominatorVariableId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: OLAPRatioEstimateValueList.ColDenominatorVariableId.Name,
                            defaultValue: Int32.Parse(s: OLAPRatioEstimateValueList.ColDenominatorVariableId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: OLAPRatioEstimateValueList.ColDenominatorVariableId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Denominator - Combination of target variable and sub-population category and area domain category object (read-only)
                /// </summary>
                public Variable DenominatorVariable
                {
                    get
                    {
                        return
                             ((NfiEstaDB)Composite.Database)
                             .SNfiEsta.TVariable[(int)DenominatorVariableId];
                    }
                }

                /// <summary>
                /// Denominator - Combination of target variable and sub-population category and area domain category (extended-version) object (read-only)
                /// </summary>
                public VwVariable DenominatorVwVariable
                {
                    get
                    {
                        return
                             ((NfiEstaDB)Composite.Database).SNfiEsta.VVariable[(int)DenominatorVariableId];
                    }
                }


                /// <summary>
                /// Estimation cell identifier
                /// </summary>
                public int EstimationCellId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: OLAPRatioEstimateValueList.ColEstimationCellId.Name,
                            defaultValue: Int32.Parse(s: OLAPRatioEstimateValueList.ColEstimationCellId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: OLAPRatioEstimateValueList.ColEstimationCellId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Estimation cell object (read-only)
                /// </summary>
                public EstimationCell EstimationCell
                {
                    get
                    {
                        return
                             ((NfiEstaDB)Composite.Database).SNfiEsta.CEstimationCell[(int)EstimationCellId];
                    }
                }

                #endregion Dimensions

                #region Values

                /// <summary>
                /// Aggregated sets of panels and corresponding reference year sets - identifier
                /// </summary>
                public Nullable<int> PanelRefYearSetGroupId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: OLAPRatioEstimateValueList.ColPanelRefYearSetGroupId.Name,
                            defaultValue:
                                String.IsNullOrEmpty(value: OLAPRatioEstimateValueList.ColPanelRefYearSetGroupId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: OLAPRatioEstimateValueList.ColPanelRefYearSetGroupId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: OLAPRatioEstimateValueList.ColPanelRefYearSetGroupId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Aggregated sets of panels and corresponding reference year sets - label (in national language)
                /// </summary>
                public string PanelRefYearSetGroupLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateValueList.ColPanelRefYearSetGroupLabelCs.Name,
                            defaultValue: OLAPRatioEstimateValueList.ColPanelRefYearSetGroupLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateValueList.ColPanelRefYearSetGroupLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Aggregated sets of panels and corresponding reference year sets - label (in English)
                /// </summary>
                public string PanelRefYearSetGroupLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateValueList.ColPanelRefYearSetGroupLabelEn.Name,
                            defaultValue: OLAPRatioEstimateValueList.ColPanelRefYearSetGroupLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateValueList.ColPanelRefYearSetGroupLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Aggregated sets of panels and corresponding reference year sets - object (read-only)
                /// </summary>
                public PanelRefYearSetGroup PanelRefYearSetGroup
                {
                    get
                    {
                        return (PanelRefYearSetGroupId != null) ?
                             ((NfiEstaDB)Composite.Database).SNfiEsta.CPanelRefYearSetGroup[(int)PanelRefYearSetGroupId] : null;
                    }
                }


                /// <summary>
                /// Estimate configuration identifier
                /// </summary>
                public int EstimateConfId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: OLAPRatioEstimateValueList.ColEstimateConfId.Name,
                            defaultValue: Int32.Parse(s: OLAPRatioEstimateValueList.ColEstimateConfId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: OLAPRatioEstimateValueList.ColEstimateConfId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Estimate configuration object (read-only)
                /// </summary>
                public EstimateConf EstimateConf
                {
                    get
                    {
                        return
                             ((NfiEstaDB)Composite.Database).SNfiEsta.TEstimateConf[(int)EstimateConfId];
                    }
                }


                /// <summary>
                /// Estimate type (total or ratio) identifier
                /// </summary>
                public int EstimateTypeId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: OLAPRatioEstimateValueList.ColEstimateTypeId.Name,
                            defaultValue: Int32.Parse(s: OLAPRatioEstimateValueList.ColEstimateTypeId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: OLAPRatioEstimateValueList.ColEstimateTypeId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Estimate type (total or ratio) object (read-only)
                /// </summary>
                public EstimateType EstimateType
                {
                    get
                    {
                        return
                             ((NfiEstaDB)Composite.Database).SNfiEsta.CEstimateType[(int)EstimateTypeId];
                    }
                }


                /// <summary>
                /// Numerator - Total estimate configuration identifier
                /// </summary>
                public int NumeratorTotalEstimateConfId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: OLAPRatioEstimateValueList.ColNumeratorTotalEstimateConfId.Name,
                            defaultValue: Int32.Parse(s: OLAPRatioEstimateValueList.ColNumeratorTotalEstimateConfId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: OLAPRatioEstimateValueList.ColNumeratorTotalEstimateConfId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Numerator - Total estimate configuration object (read-only)
                /// </summary>
                public TotalEstimateConf NumeratorTotalEstimateConf
                {
                    get
                    {
                        return
                             ((NfiEstaDB)Composite.Database).SNfiEsta.TTotalEstimateConf[(int)NumeratorTotalEstimateConfId];
                    }
                }


                /// <summary>
                /// Denominator - Total estimate configuration identifier
                /// </summary>
                public int DenominatorTotalEstimateConfId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: OLAPRatioEstimateValueList.ColDenominatorTotalEstimateConfId.Name,
                            defaultValue: Int32.Parse(s: OLAPRatioEstimateValueList.ColDenominatorTotalEstimateConfId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: OLAPRatioEstimateValueList.ColDenominatorTotalEstimateConfId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Denominator - Total estimate configuration object (read-only)
                /// </summary>
                public TotalEstimateConf DenominatorTotalEstimateConf
                {
                    get
                    {
                        return
                             ((NfiEstaDB)Composite.Database).SNfiEsta.TTotalEstimateConf[(int)DenominatorTotalEstimateConfId];
                    }
                }


                /// <summary>
                /// User, who configured estimate
                /// </summary>
                public string EstimateExecutor
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateValueList.ColEstimateExecutor.Name,
                            defaultValue: OLAPRatioEstimateValueList.ColEstimateExecutor.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateValueList.ColEstimateExecutor.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Numerator - Total estimate configuration name (Short description of the estimate)
                /// </summary>
                public string NumeratorTotalEstimateConfName
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateValueList.ColNumeratorTotalEstimateConfName.Name,
                            defaultValue: OLAPRatioEstimateValueList.ColNumeratorTotalEstimateConfName.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateValueList.ColNumeratorTotalEstimateConfName.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Denominator - Total estimate configuration name (Short description of the estimate)
                /// </summary>
                public string DenominatorTotalEstimateConfName
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateValueList.ColDenominatorTotalEstimateConfName.Name,
                            defaultValue: OLAPRatioEstimateValueList.ColDenominatorTotalEstimateConfName.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateValueList.ColDenominatorTotalEstimateConfName.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Final estimate identifier
                /// </summary>
                public Nullable<int> ResultId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: OLAPRatioEstimateValueList.ColResultId.Name,
                            defaultValue:
                                String.IsNullOrEmpty(value: OLAPRatioEstimateValueList.ColResultId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: OLAPRatioEstimateValueList.ColResultId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: OLAPRatioEstimateValueList.ColResultId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Final estimate object (read-only)
                /// </summary>
                public Result Result
                {
                    get
                    {
                        return (ResultId != null) ?
                             ((NfiEstaDB)Composite.Database).SNfiEsta.TResult[(int)ResultId] : null;
                    }
                }


                /// <summary>
                /// Point estimate
                /// </summary>
                public Nullable<double> PointEstimate
                {
                    get
                    {
                        return Functions.GetNDoubleArg(
                            row: Data,
                            name: OLAPRatioEstimateValueList.ColPointEstimate.Name,
                            defaultValue:
                                String.IsNullOrEmpty(value: OLAPRatioEstimateValueList.ColPointEstimate.DefaultValue) ?
                                (Nullable<double>)null :
                                Double.Parse(s: OLAPRatioEstimateValueList.ColPointEstimate.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNDoubleArg(
                            row: Data,
                            name: OLAPRatioEstimateValueList.ColPointEstimate.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Variance estimate
                /// </summary>
                public Nullable<double> VarianceEstimate
                {
                    get
                    {
                        return Functions.GetNDoubleArg(
                            row: Data,
                            name: OLAPRatioEstimateValueList.ColVarianceEstimate.Name,
                            defaultValue:
                                String.IsNullOrEmpty(value: OLAPRatioEstimateValueList.ColVarianceEstimate.DefaultValue) ?
                                (Nullable<double>)null :
                                Double.Parse(s: OLAPRatioEstimateValueList.ColVarianceEstimate.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNDoubleArg(
                            row: Data,
                            name: OLAPRatioEstimateValueList.ColVarianceEstimate.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Time when calculation started
                /// </summary>
                public Nullable<DateTime> CalcStarted
                {
                    get
                    {
                        return Functions.GetNDateTimeArg(
                            row: Data,
                            name: OLAPRatioEstimateValueList.ColCalcStarted.Name,
                            defaultValue:
                                String.IsNullOrEmpty(value: OLAPRatioEstimateValueList.ColCalcStarted.DefaultValue) ?
                                (Nullable<DateTime>)null :
                                DateTime.Parse(s: OLAPRatioEstimateValueList.ColCalcStarted.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNDateTimeArg(
                            row: Data,
                            name: OLAPRatioEstimateValueList.ColCalcStarted.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Time when calculation started as text
                /// </summary>
                public string CalcStartedText
                {
                    get
                    {
                        return
                            (CalcStarted == null) ? Functions.StrNull :
                                ((DateTime)CalcStarted).ToString(
                                format: OLAPRatioEstimateValueList.ColCalcStarted.NumericFormat);
                    }
                }

                /// <summary>
                /// Version of the extension with which the results were calculated
                /// </summary>
                public string ExtensionVersion
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateValueList.ColExtensionVersion.Name,
                            defaultValue: OLAPRatioEstimateValueList.ColExtensionVersion.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateValueList.ColExtensionVersion.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Time necessary for calculation
                /// </summary>
                public Nullable<TimeSpan> CalcDuration
                {
                    get
                    {
                        return Functions.GetNTimeSpanArg(
                            row: Data,
                            name: OLAPRatioEstimateValueList.ColCalcDuration.Name,
                            defaultValue:
                                String.IsNullOrEmpty(value: OLAPRatioEstimateValueList.ColCalcDuration.DefaultValue) ?
                                (Nullable<TimeSpan>)null :
                                TimeSpan.Parse(s: OLAPRatioEstimateValueList.ColCalcDuration.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNTimeSpanArg(
                            row: Data,
                            name: OLAPRatioEstimateValueList.ColCalcDuration.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Time necessary for calculation as text
                /// </summary>
                public string CalcDurationText
                {
                    get
                    {
                        return
                            (CalcDuration == null) ? Functions.StrNull :
                                ((TimeSpan)CalcDuration).ToString(
                                format: OLAPRatioEstimateValueList.ColCalcDuration.NumericFormat);
                    }
                }

                /// <summary>
                /// Minimal sample size
                /// </summary>
                public Nullable<long> MinSSize
                {
                    get
                    {
                        return Functions.GetNLongArg(
                            row: Data,
                            name: OLAPRatioEstimateValueList.ColMinSSize.Name,
                            defaultValue:
                                String.IsNullOrEmpty(value: OLAPRatioEstimateValueList.ColMinSSize.DefaultValue) ?
                                (Nullable<long>)null :
                                Int64.Parse(s: OLAPRatioEstimateValueList.ColMinSSize.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNLongArg(
                            row: Data,
                            name: OLAPRatioEstimateValueList.ColMinSSize.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Actual sample size
                /// </summary>
                public Nullable<long> ActSSize
                {
                    get
                    {
                        return Functions.GetNLongArg(
                            row: Data,
                            name: OLAPRatioEstimateValueList.ColActSSize.Name,
                            defaultValue:
                                String.IsNullOrEmpty(value: OLAPRatioEstimateValueList.ColActSSize.DefaultValue) ?
                                (Nullable<long>)null :
                                Int64.Parse(s: OLAPRatioEstimateValueList.ColActSSize.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNLongArg(
                            row: Data,
                            name: OLAPRatioEstimateValueList.ColActSSize.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Flag for identification actual value of final estimate
                /// </summary>
                public Nullable<bool> IsLatest
                {
                    get
                    {
                        return Functions.GetNBoolArg(
                            row: Data,
                            name: OLAPRatioEstimateValueList.ColIsLatest.Name,
                            defaultValue:
                                String.IsNullOrEmpty(value: OLAPRatioEstimateValueList.ColIsLatest.DefaultValue) ?
                                (Nullable<bool>)null :
                                Boolean.Parse(value: OLAPRatioEstimateValueList.ColIsLatest.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNBoolArg(
                            row: Data,
                            name: OLAPRatioEstimateValueList.ColIsLatest.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// User, who calculated estimate
                /// </summary>
                public string ResultExecutor
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateValueList.ColResultExecutor.Name,
                            defaultValue: OLAPRatioEstimateValueList.ColResultExecutor.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateValueList.ColResultExecutor.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Standard error
                /// </summary>
                public Nullable<double> StandardError
                {
                    get
                    {
                        return Functions.GetNDoubleArg(
                            row: Data,
                            name: OLAPRatioEstimateValueList.ColStandardError.Name,
                            defaultValue:
                                String.IsNullOrEmpty(value: OLAPRatioEstimateValueList.ColStandardError.DefaultValue) ?
                                (Nullable<double>)null :
                                Double.Parse(s: OLAPRatioEstimateValueList.ColStandardError.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNDoubleArg(
                            row: Data,
                            name: OLAPRatioEstimateValueList.ColStandardError.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Confidence interval
                /// </summary>
                public Nullable<double> ConfidenceInterval
                {
                    get
                    {
                        return Functions.GetNDoubleArg(
                            row: Data,
                            name: OLAPRatioEstimateValueList.ColConfidenceInterval.Name,
                            defaultValue:
                                String.IsNullOrEmpty(value: OLAPRatioEstimateValueList.ColConfidenceInterval.DefaultValue) ?
                                (Nullable<double>)null :
                                Double.Parse(s: OLAPRatioEstimateValueList.ColConfidenceInterval.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNDoubleArg(
                            row: Data,
                            name: OLAPRatioEstimateValueList.ColConfidenceInterval.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Confidence interval - lower bound
                /// </summary>
                public Nullable<double> LowerBound
                {
                    get
                    {
                        return Functions.GetNDoubleArg(
                            row: Data,
                            name: OLAPRatioEstimateValueList.ColLowerBound.Name,
                            defaultValue:
                                String.IsNullOrEmpty(value: OLAPRatioEstimateValueList.ColLowerBound.DefaultValue) ?
                                (Nullable<double>)null :
                                Double.Parse(s: OLAPRatioEstimateValueList.ColLowerBound.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNDoubleArg(
                            row: Data,
                            name: OLAPRatioEstimateValueList.ColLowerBound.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Confidence interval - upper bound
                /// </summary>
                public Nullable<double> UpperBound
                {
                    get
                    {
                        return Functions.GetNDoubleArg(
                            row: Data,
                            name: OLAPRatioEstimateValueList.ColUpperBound.Name,
                            defaultValue:
                                String.IsNullOrEmpty(value: OLAPRatioEstimateValueList.ColUpperBound.DefaultValue) ?
                                (Nullable<double>)null :
                                Double.Parse(s: OLAPRatioEstimateValueList.ColUpperBound.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNDoubleArg(
                            row: Data,
                            name: OLAPRatioEstimateValueList.ColUpperBound.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Attribute additivity difference -  sum of lower categories
                /// </summary>
                public Nullable<double> AttrDiffDown
                {
                    get
                    {
                        return Functions.GetNDoubleArg(
                            row: Data,
                            name: OLAPRatioEstimateValueList.ColAttrDiffDown.Name,
                            defaultValue:
                                String.IsNullOrEmpty(value: OLAPRatioEstimateValueList.ColAttrDiffDown.DefaultValue) ?
                                (Nullable<double>)null :
                                Double.Parse(s: OLAPRatioEstimateValueList.ColAttrDiffDown.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNDoubleArg(
                            row: Data,
                            name: OLAPRatioEstimateValueList.ColAttrDiffDown.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Attribute additivity difference -  superior category
                /// </summary>
                public Nullable<double> AttrDiffUp
                {
                    get
                    {
                        return Functions.GetNDoubleArg(
                            row: Data,
                            name: OLAPRatioEstimateValueList.ColAttrDiffUp.Name,
                            defaultValue:
                                String.IsNullOrEmpty(value: OLAPRatioEstimateValueList.ColAttrDiffUp.DefaultValue) ?
                                (Nullable<double>)null :
                                Double.Parse(s: OLAPRatioEstimateValueList.ColAttrDiffUp.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNDoubleArg(
                            row: Data,
                            name: OLAPRatioEstimateValueList.ColAttrDiffUp.Name,
                            val: value);
                    }
                }

                #endregion Values

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not OLAPRatioEstimateValue Type, return False
                    if (obj is not OLAPRatioEstimateValue)
                    {
                        return false;
                    }

                    return
                        (PhaseEstimateTypeId == ((OLAPRatioEstimateValue)obj).PhaseEstimateTypeId) &&
                        (EstimationPeriodId == ((OLAPRatioEstimateValue)obj).EstimationPeriodId) &&
                        (NumeratorVariableId == ((OLAPRatioEstimateValue)obj).NumeratorVariableId) &&
                        (DenominatorVariableId == ((OLAPRatioEstimateValue)obj).DenominatorVariableId) &&
                        (EstimationCellId == ((OLAPRatioEstimateValue)obj).EstimationCellId) &&
                        (PanelRefYearSetGroupId == ((OLAPRatioEstimateValue)obj).PanelRefYearSetGroupId) &&
                        (EstimateConfId == ((OLAPRatioEstimateValue)obj).EstimateConfId) &&
                        (NumeratorTotalEstimateConfId == ((OLAPRatioEstimateValue)obj).NumeratorTotalEstimateConfId) &&
                        (DenominatorTotalEstimateConfId == ((OLAPRatioEstimateValue)obj).DenominatorTotalEstimateConfId) &&
                        (ResultId == ((OLAPRatioEstimateValue)obj).ResultId);
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                       PhaseEstimateTypeId.GetHashCode() ^
                       EstimationPeriodId.GetHashCode() ^
                       NumeratorVariableId.GetHashCode() ^
                       DenominatorVariableId.GetHashCode() ^
                       EstimationCellId.GetHashCode() ^
                       PanelRefYearSetGroupId.GetHashCode() ^
                       EstimateConfId.GetHashCode() ^
                       NumeratorTotalEstimateConfId.GetHashCode() ^
                       DenominatorTotalEstimateConfId.GetHashCode() ^
                       ResultId.GetHashCode();
                }

                /// <summary>
                /// Text description of the object
                /// </summary>
                /// <returns>Text description of the object</returns>
                public override string ToString()
                {
                    return String.Concat(
                        $"{"ID"} = {Functions.PrepStringArg(arg: Id)}{Environment.NewLine}");
                }

                #endregion Methods

            }


            /// <summary>
            /// List of OLAP Cube Values - Ratio Estimate
            /// </summary>
            public class OLAPRatioEstimateValueList
                : ALinqView
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema =
                    NfiEstaSchema.Name;

                /// <summary>
                /// Table name
                /// </summary>
                public static readonly string Name =
                    "v_olap_ratio_estimate_value";

                /// <summary>
                /// Table alias
                /// </summary>
                public static readonly string Alias =
                    "v_olap_ratio_estimate_value";

                /// <summary>
                /// Description in national language
                /// </summary>
                public static readonly string CaptionCs =
                    "Seznam hodnot OLAP kostky s daty odhadů podílů";

                /// <summary>
                /// Description in English
                /// </summary>
                public static readonly string CaptionEn =
                    "List of OLAP cube values with ratio estimates data";

                /// <summary>
                /// Significance level for confidence intervals
                /// </summary>
                public const double Alfa = 0.05;

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
                {
                    { "id", new ColumnMetadata()
                    {
                        Name = "id",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = true,
                        NotNull = true,
                        DefaultValue =  String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = String.Empty,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ID",
                        HeaderTextEn = "ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 0
                    }
                    },
                    { "numerator_id", new ColumnMetadata()
                    {
                        Name = "numerator_id",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue =  String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = String.Empty,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "NUMERATOR_ID",
                        HeaderTextEn = "NUMERATOR_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 1
                    }
                    },
                    { "denominator_id", new ColumnMetadata()
                    {
                        Name = "denominator_id",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue =  String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = String.Empty,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "DENOMINATOR_ID",
                        HeaderTextEn = "DENOMINATOR_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 2
                    }
                    },
                    { "phase_estimate_type_id", new ColumnMetadata()
                    {
                        Name = "phase_estimate_type_id",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue =  "0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PHASE_ESTIMATE_TYPE_ID",
                        HeaderTextEn = "PHASE_ESTIMATE_TYPE_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 3
                    }
                    },
                    { "estimation_period_id", new ColumnMetadata()
                    {
                        Name = "estimation_period_id",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATION_PERIOD_ID",
                        HeaderTextEn = "ESTIMATION_PERIOD_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 4
                    }
                    },
                    { "numerator_variable_id", new ColumnMetadata()
                    {
                        Name = "numerator_variable_id",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue =  "0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "NUMERATOR_VARIABLE_ID",
                        HeaderTextEn = "NUMERATOR_VARIABLE_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 5
                    }
                    },
                    { "denominator_variable_id", new ColumnMetadata()
                    {
                        Name = "denominator_variable_id",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue =  "0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "DENOMINATOR_VARIABLE_ID",
                        HeaderTextEn = "DENOMINATOR_VARIABLE_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 6
                    }
                    },
                    { "estimation_cell_id", new ColumnMetadata()
                    {
                        Name = "estimation_cell_id",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue =  "0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATION_CELL_ID",
                        HeaderTextEn = "ESTIMATION_CELL_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 7
                    }
                    },
                    { "panel_refyearset_group_id", new ColumnMetadata()
                    {
                        Name = "panel_refyearset_group_id",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PANEL_REFYEARSET_GROUP_ID",
                        HeaderTextEn = "PANEL_REFYEARSET_GROUP_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 8
                    }
                    },
                    { "panel_refyearset_group_label_cs", new ColumnMetadata()
                    {
                        Name = "panel_refyearset_group_label_cs",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  String.Empty,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PANEL_REFYEARSET_GROUP_LABEL_CS",
                        HeaderTextEn = "PANEL_REFYEARSET_GROUP_LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 9
                    }
                    },
                    { "panel_refyearset_group_label_en", new ColumnMetadata()
                    {
                        Name = "panel_refyearset_group_label_en",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  String.Empty,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PANEL_REFYEARSET_GROUP_LABEL_EN",
                        HeaderTextEn = "PANEL_REFYEARSET_GROUP_LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 10
                    }
                    },
                    { "estimate_conf_id", new ColumnMetadata()
                    {
                        Name = "estimate_conf_id",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue =  "0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATE_CONF_ID",
                        HeaderTextEn = "ESTIMATE_CONF_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 11
                    }
                    },
                    { "estimate_type_id", new ColumnMetadata()
                    {
                        Name = "estimate_type_id",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue =  "0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATE_TYPE_ID",
                        HeaderTextEn = "ESTIMATE_TYPE_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 12
                    }
                    },
                    { "numerator_total_estimate_conf_id", new ColumnMetadata()
                    {
                        Name = "numerator_total_estimate_conf_id",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue =  "0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "NUMERATOR_TOTAL_ESTIMATE_CONF_ID",
                        HeaderTextEn = "NUMERATOR_TOTAL_ESTIMATE_CONF_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 13
                    }
                    },
                    { "denominator_total_estimate_conf_id", new ColumnMetadata()
                    {
                        Name = "denominator_total_estimate_conf_id",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue =  "0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "DENOMINATOR_TOTAL_ESTIMATE_CONF_ID",
                        HeaderTextEn = "DENOMINATOR_TOTAL_ESTIMATE_CONF_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 14
                    }
                    },
                    { "estimate_executor", new ColumnMetadata()
                    {
                        Name = "estimate_executor",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATE_EXECUTOR",
                        HeaderTextEn = "ESTIMATE_EXECUTOR",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 15
                    }
                    },
                    { "numerator_total_estimate_conf_name", new ColumnMetadata()
                    {
                        Name = "numerator_total_estimate_conf_name",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  String.Empty,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "NUMERATOR_TOTAL_ESTIMATE_CONF_NAME",
                        HeaderTextEn = "NUMERATOR_TOTAL_ESTIMATE_CONF_NAME",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 16
                    }
                    },
                    { "denominator_total_estimate_conf_name", new ColumnMetadata()
                    {
                        Name = "denominator_total_estimate_conf_name",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  String.Empty,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "DENOMINATOR_TOTAL_ESTIMATE_CONF_NAME",
                        HeaderTextEn = "DENOMINATOR_TOTAL_ESTIMATE_CONF_NAME",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 17
                    }
                    },
                    { "result_id", new ColumnMetadata()
                    {
                        Name = "result_id",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "RESULT_ID",
                        HeaderTextEn = "RESULT_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 18
                    }
                    },
                    { "point_estimate", new ColumnMetadata()
                    {
                        Name = "point_estimate",
                        DbName = null,
                        DataType = "System.Double",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  "N7",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "POINT_ESTIMATE",
                        HeaderTextEn = "POINT_ESTIMATE",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 19
                    }
                    },
                    { "variance_estimate", new ColumnMetadata()
                    {
                        Name = "variance_estimate",
                        DbName = null,
                        DataType = "System.Double",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  "N7",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "VARIANCE_ESTIMATE",
                        HeaderTextEn = "VARIANCE_ESTIMATE",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 20
                    }
                    },
                    { "calc_started", new ColumnMetadata()
                    {
                        Name = "calc_started",
                        DbName = null,
                        DataType = "System.DateTime",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "yyyy-MM-dd HH:mm:ss",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "CALC_STARTED",
                        HeaderTextEn = "CALC_STARTED",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 21
                    }
                    },
                    { "extension_version", new ColumnMetadata()
                    {
                        Name = "extension_version",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "EXTENSION_VERSION",
                        HeaderTextEn = "EXTENSION_VERSION",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 22
                    }
                    },
                    { "calc_duration", new ColumnMetadata()
                    {
                        Name = "calc_duration",
                        DbName = null,
                        DataType = "System.TimeSpan",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "g",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "CALC_DURATION",
                        HeaderTextEn = "CALC_DURATION",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 23
                    }
                    },
                    { "min_ssize", new ColumnMetadata()
                    {
                        Name = "min_ssize",
                        DbName = null,
                        DataType = "System.Int64",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "MIN_SSIZE",
                        HeaderTextEn = "MIN_SSIZE",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 25
                    }
                    },
                    { "act_ssize", new ColumnMetadata()
                    {
                        Name = "act_ssize",
                        DbName = null,
                        DataType = "System.Int64",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ACT_SSIZE",
                        HeaderTextEn = "ACT_SSIZE",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 26
                    }
                    },
                    { "is_latest", new ColumnMetadata()
                    {
                        Name = "is_latest",
                        DbName = null,
                        DataType = "System.Boolean",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "IS_LATEST",
                        HeaderTextEn = "IS_LATEST",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 27
                    }
                    },
                    { "result_executor", new ColumnMetadata()
                    {
                        Name = "result_executor",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "RESULT_EXECUTOR",
                        HeaderTextEn = "RESULT_EXECUTOR",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 28
                    }
                    },
                    { "standard_error", new ColumnMetadata()
                    {
                        Name = "standard_error",
                        DbName = null,
                        DataType = "System.Double",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  "N7",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "STANDARD_ERROR",
                        HeaderTextEn = "STANDARD_ERROR",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 29
                    }
                    },
                    { "confidence_interval", new ColumnMetadata()
                    {
                        Name = "confidence_interval",
                        DbName = null,
                        DataType = "System.Double",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  "N7",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "CONFIDENCE_INTERVAL",
                        HeaderTextEn = "CONFIDENCE_INTERVAL",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 30
                    }
                    },
                    { "lower_bound", new ColumnMetadata()
                    {
                        Name = "lower_bound",
                        DbName = null,
                        DataType = "System.Double",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  "N7",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "LOWER_BOUND",
                        HeaderTextEn = "LOWER_BOUND",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 31
                    }
                    },
                    { "upper_bound", new ColumnMetadata()
                    {
                        Name = "upper_bound",
                        DbName = null,
                        DataType = "System.Double",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  "N7",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "UPPER_BOUND",
                        HeaderTextEn = "UPPER_BOUND",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 32
                    }
                    },
                    { "attr_diff_down", new ColumnMetadata()
                    {
                        Name = "attr_diff_down",
                        DbName = null,
                        DataType = "System.Double",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  "N10",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ATTR_DIFF_DOWN",
                        HeaderTextEn = "ATTR_DIFF_DOWN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 33
                    }
                    },
                    { "attr_diff_up", new ColumnMetadata()
                    {
                        Name = "attr_diff_up",
                        DbName = null,
                        DataType = "System.Double",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  "N10",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ATTR_DIFF_UP",
                        HeaderTextEn = "ATTR_DIFF_UP",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 34
                    }
                    }
                };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column numerator_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColNumeratorId = Cols["numerator_id"];

                /// <summary>
                /// Column denominator_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColDenominatorId = Cols["denominator_id"];

                /// <summary>
                /// Column phase_estimate_type_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColPhaseEstimateTypeId = Cols["phase_estimate_type_id"];

                /// <summary>
                /// Column estimation_period_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationPeriodId = Cols["estimation_period_id"];

                /// <summary>
                /// Column numerator_variable_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColNumeratorVariableId = Cols["numerator_variable_id"];

                /// <summary>
                /// Column denominator_variable_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColDenominatorVariableId = Cols["denominator_variable_id"];

                /// <summary>
                /// Column estimation_cell_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationCellId = Cols["estimation_cell_id"];

                /// <summary>
                /// Column panel_refyearset_group_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColPanelRefYearSetGroupId = Cols["panel_refyearset_group_id"];

                /// <summary>
                /// Column panel_refyearset_group_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColPanelRefYearSetGroupLabelCs = Cols["panel_refyearset_group_label_cs"];

                /// <summary>
                /// Column panel_refyearset_group_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColPanelRefYearSetGroupLabelEn = Cols["panel_refyearset_group_label_en"];

                /// <summary>
                /// Column estimate_conf_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimateConfId = Cols["estimate_conf_id"];

                /// <summary>
                /// Column estimate_type_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimateTypeId = Cols["estimate_type_id"];

                /// <summary>
                /// Column numerator_total_estimate_conf_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColNumeratorTotalEstimateConfId = Cols["numerator_total_estimate_conf_id"];

                /// <summary>
                /// Column denominator_total_estimate_conf_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColDenominatorTotalEstimateConfId = Cols["denominator_total_estimate_conf_id"];

                /// <summary>
                /// Column estimate_executor metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimateExecutor = Cols["estimate_executor"];

                /// <summary>
                /// Column numerator_total_estimate_conf_name metadata
                /// </summary>
                public static readonly ColumnMetadata ColNumeratorTotalEstimateConfName = Cols["numerator_total_estimate_conf_name"];

                /// <summary>
                /// Column denominator_total_estimate_conf_name metadata
                /// </summary>
                public static readonly ColumnMetadata ColDenominatorTotalEstimateConfName = Cols["denominator_total_estimate_conf_name"];

                /// <summary>
                /// Column result_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColResultId = Cols["result_id"];

                /// <summary>
                /// Column point_estimate metadata
                /// </summary>
                public static readonly ColumnMetadata ColPointEstimate = Cols["point_estimate"];

                /// <summary>
                /// Column variance_estimate metadata
                /// </summary>
                public static readonly ColumnMetadata ColVarianceEstimate = Cols["variance_estimate"];

                /// <summary>
                /// Column calc_started metadata
                /// </summary>
                public static readonly ColumnMetadata ColCalcStarted = Cols["calc_started"];

                /// <summary>
                /// Column extension_version metadata
                /// </summary>
                public static readonly ColumnMetadata ColExtensionVersion = Cols["extension_version"];

                /// <summary>
                /// Column calc_duration metadata
                /// </summary>
                public static readonly ColumnMetadata ColCalcDuration = Cols["calc_duration"];

                /// <summary>
                /// Column min_ssize metadata
                /// </summary>
                public static readonly ColumnMetadata ColMinSSize = Cols["min_ssize"];

                /// <summary>
                /// Column act_ssize metadata
                /// </summary>
                public static readonly ColumnMetadata ColActSSize = Cols["act_ssize"];

                /// <summary>
                /// Column is_latest metadata
                /// </summary>
                public static readonly ColumnMetadata ColIsLatest = Cols["is_latest"];

                /// <summary>
                /// Column result_executor metadata
                /// </summary>
                public static readonly ColumnMetadata ColResultExecutor = Cols["result_executor"];

                /// <summary>
                /// Column standard_error metadata
                /// </summary>
                public static readonly ColumnMetadata ColStandardError = Cols["standard_error"];

                /// <summary>
                /// Column confidence_interval metadata
                /// </summary>
                public static readonly ColumnMetadata ColConfidenceInterval = Cols["confidence_interval"];

                /// <summary>
                /// Column lower_bound metadata
                /// </summary>
                public static readonly ColumnMetadata ColLowerBound = Cols["lower_bound"];

                /// <summary>
                /// Column upper_bound metadata
                /// </summary>
                public static readonly ColumnMetadata ColUpperBound = Cols["upper_bound"];

                /// <summary>
                /// Column attr_diff_down metadata
                /// </summary>
                public static readonly ColumnMetadata ColAttrDiffDown = Cols["attr_diff_down"];

                /// <summary>
                /// Column attr_diff_up metadata
                /// </summary>
                public static readonly ColumnMetadata ColAttrDiffUp = Cols["attr_diff_up"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public OLAPRatioEstimateValueList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public OLAPRatioEstimateValueList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// List of OLAP Cube Values - Ratio Estimate (read-only)
                /// </summary>
                public List<OLAPRatioEstimateValue> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new OLAPRatioEstimateValue(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// OLAP Cube Value - Ratio Estimate from list by identifier (read-only)
                /// </summary>
                /// <param name="id"> OLAP Cube Dimension - Ratio Estimate - composite identifier</param>
                /// <returns> OLAP Cube Value - Ratio Estimate from list by identifier (null if not found)</returns>
                public OLAPRatioEstimateValue this[string id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<string>(ColId.Name) == id)
                                .Select(a => new OLAPRatioEstimateValue(composite: this, data: a))
                                .FirstOrDefault<OLAPRatioEstimateValue>();
                    }
                }

                #endregion Indexer


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public OLAPRatioEstimateValueList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new OLAPRatioEstimateValueList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new OLAPRatioEstimateValueList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                /// <summary>
                /// Loads data into OLAP Cube Values - Ratio Estimate
                /// (if it was not done before)
                /// </summary>
                /// <param name="cPhaseEstimateTypeNumerator">List of phase estimate types for numerator</param>
                /// <param name="cPhaseEstimateTypeDenominator">List of phase estimate types for denominator</param>
                /// <param name="cEstimationPeriod">List of estimation periods</param>
                /// <param name="variablePairs">List of attribute categories pairs for numerator and denominator</param>
                /// <param name="cEstimationCell">List of estimation cells</param>
                /// <param name="cPanelRefYearSetGroup">List of aggregated sets of panels and corresponding reference year sets</param>
                /// <param name="tEstimateConf">List of estimate configurations</param>
                /// <param name="tTotalEstimateConf">List of total estimate configurations</param>
                /// <param name="tResult">List of final estimates</param>
                /// <param name="fnAddResRatioAttr">List of ratio estimates attribute additivity</param>
                /// <param name="additivityLimit">Additivity limit</param>
                /// <param name="isLatest">Only latest estimates</param>
                /// <param name="condition">Condition for selection data</param>
                /// <param name="limit">Rows count limit</param>
                public void Load(
                    PhaseEstimateTypeList cPhaseEstimateTypeNumerator,
                    PhaseEstimateTypeList cPhaseEstimateTypeDenominator,
                    EstimationPeriodList cEstimationPeriod,
                    VariablePairList variablePairs,
                    EstimationCellList cEstimationCell,
                    PanelRefYearSetGroupList cPanelRefYearSetGroup,
                    EstimateConfList tEstimateConf,
                    TotalEstimateConfList tTotalEstimateConf,
                    ResultList tResult,
                    TFnAddResRatioAttrList fnAddResRatioAttr,
                    double additivityLimit,
                    bool isLatest,
                    string condition = null,
                    Nullable<int> limit = null)
                {
                    if (!Loaded)
                    {
                        ReLoad(
                            cPhaseEstimateTypeNumerator: cPhaseEstimateTypeNumerator,
                            cPhaseEstimateTypeDenominator: cPhaseEstimateTypeDenominator,
                            cEstimationPeriod: cEstimationPeriod,
                            variablePairs: variablePairs,
                            cEstimationCell: cEstimationCell,
                            cPanelRefYearSetGroup: cPanelRefYearSetGroup,
                            tEstimateConf: tEstimateConf,
                            tTotalEstimateConf: tTotalEstimateConf,
                            tResult: tResult,
                            fnAddResRatioAttr: fnAddResRatioAttr,
                            additivityLimit: additivityLimit,
                            isLatest: isLatest,
                            condition: condition,
                            limit: limit);
                    }
                }

                /// <summary>
                /// Loads data into OLAP Cube Values - Ratio Estimate
                /// </summary>
                /// <param name="cPhaseEstimateTypeNumerator">List of phase estimate types for numerator</param>
                /// <param name="cPhaseEstimateTypeDenominator">List of phase estimate types for denominator</param>
                /// <param name="cEstimationPeriod">List of estimation periods</param>
                /// <param name="variablePairs">List of attribute categories pairs for numerator and denominator</param>
                /// <param name="cEstimationCell">List of estimation cells</param>
                /// <param name="cPanelRefYearSetGroup">List of aggregated sets of panels and corresponding reference year sets</param>
                /// <param name="tEstimateConf">List of estimate configurations</param>
                /// <param name="tTotalEstimateConf">List of total estimate configurations</param>
                /// <param name="tResult">List of final estimates</param>
                /// <param name="fnAddResRatioAttr">List of ratio estimates attribute additivity</param>
                /// <param name="additivityLimit">Additivity limit</param>
                /// <param name="isLatest">Only latest estimates</param>
                /// <param name="condition">Condition for selection data</param>
                /// <param name="limit">Rows count limit</param>
                public void ReLoad(
                    PhaseEstimateTypeList cPhaseEstimateTypeNumerator,
                    PhaseEstimateTypeList cPhaseEstimateTypeDenominator,
                    EstimationPeriodList cEstimationPeriod,
                    VariablePairList variablePairs,
                    EstimationCellList cEstimationCell,
                    PanelRefYearSetGroupList cPanelRefYearSetGroup,
                    EstimateConfList tEstimateConf,
                    TotalEstimateConfList tTotalEstimateConf,
                    ResultList tResult,
                    TFnAddResRatioAttrList fnAddResRatioAttr,
                    double additivityLimit,
                    bool isLatest,
                    string condition = null,
                    Nullable<int> limit = null)
                {
                    Data = EmptyDataTable(
                                tableName: TableName,
                                columns: Columns);
                    Condition = condition;
                    Limit = limit;

                    System.Diagnostics.Stopwatch stopWatch = new();
                    stopWatch.Start();

                    #region ctePhaseEstimateType: Fáze odhadu (1.dimenze)
                    var ctePhaseEstimateTypeNumerator =
                        cPhaseEstimateTypeNumerator.Data.AsEnumerable()
                        .Select(a => new
                        {
                            NumeratorPhaseEstimateTypeId = a.Field<int>(columnName: PhaseEstimateTypeList.ColId.Name),
                            NumeratorPhaseEstimateTypeLabelCs = a.Field<string>(columnName: PhaseEstimateTypeList.ColLabelCs.Name),
                            NumeratorPhaseEstimateTypeLabelEn = a.Field<string>(columnName: PhaseEstimateTypeList.ColLabelEn.Name),
                            NumeratorPhaseEstimateTypeDescriptionCs = a.Field<string>(columnName: PhaseEstimateTypeList.ColDescriptionCs.Name),
                            NumeratorPhaseEstimateTypeDescriptionEn = a.Field<string>(columnName: PhaseEstimateTypeList.ColDescriptionEn.Name)
                        });
                    var ctePhaseEstimateTypeDenominator =
                        cPhaseEstimateTypeDenominator.Data.AsEnumerable()
                        .Select(a => new
                        {
                            DenominatorPhaseEstimateTypeId = a.Field<int>(columnName: PhaseEstimateTypeList.ColId.Name),
                            DenominatorPhaseEstimateTypeLabelCs = a.Field<string>(columnName: PhaseEstimateTypeList.ColLabelCs.Name),
                            DenominatorPhaseEstimateTypeLabelEn = a.Field<string>(columnName: PhaseEstimateTypeList.ColLabelEn.Name),
                            DenominatorPhaseEstimateTypeDescriptionCs = a.Field<string>(columnName: PhaseEstimateTypeList.ColDescriptionCs.Name),
                            DenominatorPhaseEstimateTypeDescriptionEn = a.Field<string>(columnName: PhaseEstimateTypeList.ColDescriptionEn.Name)
                        });
                    #endregion ctePhaseEstimateType: Fáze odhadu (1.dimenze)

                    #region cteEstimationPeriod: Období odhadu (2.dimenze)
                    var cteEstimationPeriod =
                        cEstimationPeriod.Data.AsEnumerable()
                        .Select(a => new
                        {
                            EstimationPeriodId = a.Field<int>(columnName: EstimationPeriodList.ColId.Name),
                            EstimateDateBegin = a.Field<DateTime>(columnName: EstimationPeriodList.ColEstimateDateBegin.Name),
                            EstimateDateEnd = a.Field<DateTime>(columnName: EstimationPeriodList.ColEstimateDateEnd.Name),
                            EstimationPeriodLabelCs = a.Field<string>(columnName: EstimationPeriodList.ColLabelCs.Name),
                            EstimationPeriodLabelEn = a.Field<string>(columnName: EstimationPeriodList.ColLabelEn.Name),
                            EstimationPeriodDescriptionCs = a.Field<string>(columnName: EstimationPeriodList.ColDescriptionCs.Name),
                            EstimationPeriodDescriptionEn = a.Field<string>(columnName: EstimationPeriodList.ColDescriptionEn.Name)
                        });
                    #endregion cteEstimationPeriod: Období odhadu (2.dimenze)

                    #region cteAttributeCategory: Atributové kategorie čitatele a jmenovatele (3.dimenze)
                    var cteAttributeCategory =
                        variablePairs.VariablePairs
                        .Select(a => new
                        {
                            NumeratorVariableId = a.Numerator.VariableId,
                            NumeratorVariableLabelCs = a.Numerator.VariableLabelCs,
                            NumeratorVariableLabelEn = a.Numerator.VariableLabelEn,
                            DenominatorVariableId = a.Denominator.VariableId,
                            DenominatorVariableLabelCs = a.Denominator.VariableLabelCs,
                            DenominatorVariableLabelEn = a.Denominator.VariableLabelEn
                        });
                    #endregion cteAttributeCategory: Atributové kategorie čitatele a jmenovatele (3.dimenze)

                    #region cteEstimationCell: Výpočetní buňky (4.dimenze)
                    var cteEstimationCell =
                        cEstimationCell.Data.AsEnumerable()
                        .Select(a => new
                        {
                            EstimationCellId = a.Field<int>(columnName: EstimationCellList.ColId.Name),
                            EstimationCellCollectionId = a.Field<int>(columnName: EstimationCellList.ColEstimationCellCollectionId.Name),
                            EstimationCellLabelCs = a.Field<string>(columnName: EstimationCellList.ColLabelCs.Name),
                            EstimationCellLabelEn = a.Field<string>(columnName: EstimationCellList.ColLabelEn.Name),
                            EstimationCellDescriptionCs = a.Field<string>(columnName: EstimationCellList.ColDescriptionCs.Name),
                            EstimationCellDescriptionEn = a.Field<string>(columnName: EstimationCellList.ColDescriptionEn.Name)
                        });
                    #endregion cteEstimationCell: Výpočetní buňky (4.dimenze)

                    #region ctePanelRefYearSetGroup: Skupiny panelů pro období odhadu
                    var ctePanelRefYearSetGroup =
                        cPanelRefYearSetGroup.Data.AsEnumerable()
                        .Select(a => new
                        {
                            PanelRefYearSetGroupId = a.Field<int>(columnName: PanelRefYearSetGroupList.ColId.Name),
                            PanelRefYearSetGroupLabelCs = a.Field<string>(columnName: PanelRefYearSetGroupList.ColLabelCs.Name),
                            PanelRefYearSetGroupLabelEn = a.Field<string>(columnName: PanelRefYearSetGroupList.ColLabelEn.Name),
                            PanelRefYearSetGroupDescriptionCs = a.Field<string>(columnName: PanelRefYearSetGroupList.ColDescriptionCs.Name),
                            PanelRefYearSetGroupDescriptionEn = a.Field<string>(columnName: PanelRefYearSetGroupList.ColDescriptionEn.Name)
                        });
                    #endregion ctePanelRefYearSetGroup: Skupiny panelů pro období odhadu

                    #region cteEstimateConf: Konfigurace odhadů (estimate_type_id = 2)
                    var cteEstimateConf =
                        tEstimateConf.Data.AsEnumerable()
                        .Where(a => a.Field<int>(columnName: EstimateConfList.ColEstimateTypeId.Name) == 2)
                        .Select(a => new
                        {
                            EstimateConfId = a.Field<int>(columnName: EstimateConfList.ColId.Name),
                            EstimateTypeId = a.Field<int>(columnName: EstimateConfList.ColEstimateTypeId.Name),
                            NumeratorTotalEstimateConfId = a.Field<int>(columnName: EstimateConfList.ColTotalEstimateConfId.Name),
                            DenominatorTotalEstimateConfId = a.Field<Nullable<int>>(columnName: EstimateConfList.ColDenominatorId.Name),
                            EstimateExecutor = a.Field<string>(columnName: EstimateConfList.ColExecutor.Name)
                        });
                    #endregion cteEstimateConf: Konfigurace odhadů (estimate_type_id = 2)

                    #region cteNumeratorTotalEstimateConf: Konfigurace odhadů úhrnu čitatele (omezené na zvolené dimenze)
                    var cteNumeratorTotalEstimateConf =
                        tTotalEstimateConf.Data.AsEnumerable()
                        // Omezení první dimenzí (fáze odhadu)
                        .Where(a => ctePhaseEstimateTypeNumerator.Select(f => f.NumeratorPhaseEstimateTypeId)
                                    .Contains(value: a.Field<int>(columnName: TotalEstimateConfList.ColPhaseEstimateTypeId.Name)))
                        // Omezení druhou dimenzí (období odhadu)
                        .Where(a => cteEstimationPeriod.Select(p => p.EstimationPeriodId)
                                    .Contains(value: (int)(a.Field<Nullable<int>>(columnName: TotalEstimateConfList.ColEstimationPeriodId.Name) ?? 0)))
                        // Omezení třetí dimenzí (atributové kategorie)
                        .Where(a => cteAttributeCategory.Select(v => v.NumeratorVariableId)
                                    .Contains(value: a.Field<int>(columnName: TotalEstimateConfList.ColVariableId.Name)))
                        // Omezení čtvrtou dimenzí (výpočetní buňky)
                        .Where(a => cteEstimationCell.Select(c => c.EstimationCellId)
                                    .Contains(value: a.Field<int>(columnName: TotalEstimateConfList.ColEstimationCellId.Name)))
                        .Select(a => new
                        {
                            NumeratorTotalEstimateConfId = a.Field<int>(columnName: TotalEstimateConfList.ColId.Name),
                            NumeratorEstimationCellId = a.Field<int>(columnName: TotalEstimateConfList.ColEstimationCellId.Name),
                            NumeratorTotalEstimateConfName = a.Field<string>(columnName: TotalEstimateConfList.ColName.Name),
                            NumeratorVariableId = a.Field<int>(columnName: TotalEstimateConfList.ColVariableId.Name),
                            NumeratorPhaseEstimateTypeId = a.Field<int>(columnName: TotalEstimateConfList.ColPhaseEstimateTypeId.Name),
                            NumeratorForceSynthetic = a.Field<Nullable<bool>>(columnName: TotalEstimateConfList.ColForceSynthetic.Name),
                            NumeratorAuxConfId = a.Field<Nullable<int>>(columnName: TotalEstimateConfList.ColAuxConfId.Name),
                            NumeratorEstimationPeriodId = a.Field<Nullable<int>>(columnName: TotalEstimateConfList.ColEstimationPeriodId.Name),
                            NumeratorPanelRefYearSetGroupId = a.Field<Nullable<int>>(columnName: TotalEstimateConfList.ColPanelRefYearSetGroupId.Name)
                        });
                    #endregion cteNumeratorTotalEstimateConf: Konfigurace odhadů úhrnu čitatele (omezené na zvolené dimenze)

                    #region cteDenominatorTotalEstimateConf: Konfigurace odhadů úhrnu jmenovatele (omezené na zvolené dimenze)
                    var cteDenominatorTotalEstimateConf =
                        tTotalEstimateConf.Data.AsEnumerable()
                        // Omezení první dimenzí (fáze odhadu)
                        .Where(a => ctePhaseEstimateTypeDenominator.Select(f => f.DenominatorPhaseEstimateTypeId)
                                    .Contains(value: a.Field<int>(columnName: TotalEstimateConfList.ColPhaseEstimateTypeId.Name)))
                        // Omezení druhou dimenzí (období odhadu)
                        .Where(a => cteEstimationPeriod.Select(p => p.EstimationPeriodId)
                                    .Contains(value: (int)(a.Field<Nullable<int>>(columnName: TotalEstimateConfList.ColEstimationPeriodId.Name) ?? 0)))
                        // Omezení třetí dimenzí (atributové kategorie)
                        .Where(a => cteAttributeCategory.Select(v => v.DenominatorVariableId)
                                    .Contains(value: a.Field<int>(columnName: TotalEstimateConfList.ColVariableId.Name)))
                        // Omezení čtvrtou dimenzí (výpočetní buňky)
                        .Where(a => cteEstimationCell.Select(c => c.EstimationCellId)
                                    .Contains(value: a.Field<int>(columnName: TotalEstimateConfList.ColEstimationCellId.Name)))
                        .Select(a => new
                        {
                            DenominatorTotalEstimateConfId = a.Field<int>(columnName: TotalEstimateConfList.ColId.Name),
                            DenominatorEstimationCellId = a.Field<int>(columnName: TotalEstimateConfList.ColEstimationCellId.Name),
                            DenominatorTotalEstimateConfName = a.Field<string>(columnName: TotalEstimateConfList.ColName.Name),
                            DenominatorVariableId = a.Field<int>(columnName: TotalEstimateConfList.ColVariableId.Name),
                            DenominatorPhaseEstimateTypeId = a.Field<int>(columnName: TotalEstimateConfList.ColPhaseEstimateTypeId.Name),
                            DenominatorForceSynthetic = a.Field<Nullable<bool>>(columnName: TotalEstimateConfList.ColForceSynthetic.Name),
                            DenominatorAuxConfId = a.Field<Nullable<int>>(columnName: TotalEstimateConfList.ColAuxConfId.Name),
                            DenominatorEstimationPeriodId = a.Field<Nullable<int>>(columnName: TotalEstimateConfList.ColEstimationPeriodId.Name),
                            DenominatorPanelRefYearSetGroupId = a.Field<Nullable<int>>(columnName: TotalEstimateConfList.ColPanelRefYearSetGroupId.Name)
                        });
                    #endregion cteDenominatorTotalEstimateConf: Konfigurace odhadů úhrnu jmenovatele (omezené na zvolené dimenze)

                    #region cteResult: Výsledky odhadu podílu
                    var cteResult = isLatest ?
                        // Výsledky odhadu podílu (pouze aktuální výsledky)
                        tResult.Data.AsEnumerable()
                        .Where(a => a.Field<bool>(ResultList.ColIsLatest.Name))
                        .Select(a => new
                        {
                            ResultId = a.Field<int>(columnName: ResultList.ColId.Name),
                            EstimateConfId = a.Field<Nullable<int>>(columnName: ResultList.ColEstimateConfId.Name),
                            PointEstimate = a.Field<Nullable<double>>(columnName: ResultList.ColPointEstimate.Name),
                            VarEstimate = a.Field<Nullable<double>>(columnName: ResultList.ColVarianceEstimate.Name),
                            CalcStarted = a.Field<Nullable<DateTime>>(columnName: ResultList.ColCalcStarted.Name),
                            ExtensionVersion = a.Field<string>(columnName: ResultList.ColExtensionVersion.Name),
                            CalcDuration = a.Field<Nullable<TimeSpan>>(columnName: ResultList.ColCalcDuration.Name),
                            SamplingUnits = a.Field<string>(columnName: ResultList.ColSamplingUnits.Name),
                            MinSSize = a.Field<Nullable<double>>(columnName: ResultList.ColMinSSize.Name),
                            ActSSize = a.Field<Nullable<long>>(columnName: ResultList.ColActSSize.Name),
                            IsLatest = a.Field<bool>(columnName: ResultList.ColIsLatest.Name),
                            ResultExecutor = a.Field<string>(columnName: ResultList.ColExecutor.Name)
                        }) :
                        // Výsledky odhadu podílu (všechny včetně historie)
                        tResult.Data.AsEnumerable()
                        .Select(a => new
                        {
                            ResultId = a.Field<int>(columnName: ResultList.ColId.Name),
                            EstimateConfId = a.Field<Nullable<int>>(columnName: ResultList.ColEstimateConfId.Name),
                            PointEstimate = a.Field<Nullable<double>>(columnName: ResultList.ColPointEstimate.Name),
                            VarEstimate = a.Field<Nullable<double>>(columnName: ResultList.ColVarianceEstimate.Name),
                            CalcStarted = a.Field<Nullable<DateTime>>(columnName: ResultList.ColCalcStarted.Name),
                            ExtensionVersion = a.Field<string>(columnName: ResultList.ColExtensionVersion.Name),
                            CalcDuration = a.Field<Nullable<TimeSpan>>(columnName: ResultList.ColCalcDuration.Name),
                            SamplingUnits = a.Field<string>(columnName: ResultList.ColSamplingUnits.Name),
                            MinSSize = a.Field<Nullable<double>>(columnName: ResultList.ColMinSSize.Name),
                            ActSSize = a.Field<Nullable<long>>(columnName: ResultList.ColActSSize.Name),
                            IsLatest = a.Field<bool>(columnName: ResultList.ColIsLatest.Name),
                            ResultExecutor = a.Field<string>(columnName: ResultList.ColExecutor.Name)
                        });
                    #endregion cteResult: Výsledky odhadu podílu

                    #region cteResultStandardError: Výsledky odhadu podílu doplněné o směrodatnou chybu
                    var cteResultStandardError = cteResult
                        .Select(a => new
                        {
                            a.ResultId,
                            a.EstimateConfId,
                            a.PointEstimate,
                            a.VarEstimate,
                            a.CalcStarted,
                            a.ExtensionVersion,
                            a.CalcDuration,
                            a.SamplingUnits,
                            MinSSize =
                                (a.MinSSize != null) ?
                                    (Nullable<long>)Math.Ceiling((decimal)a.MinSSize) :
                                    (Nullable<long>)null,
                            a.ActSSize,
                            a.IsLatest,
                            a.ResultExecutor,
                            StandardError = (Nullable<double>)(
                                (a.VarEstimate != null) ?
                                    (a.VarEstimate >= 0) ?
                                        Math.Sqrt((double)a.VarEstimate) :
                                    (Nullable<double>)null :
                                (Nullable<double>)null)
                        });
                    #endregion cteResultStandardError: Výsledky odhadu podílu doplněné o směrodatnou chybu

                    #region cteResultStudent: Výsledky odhadu podílu doplněné o kvantil Studentova t-rozdělení
                    var cteResultStudent =
                        cteResultStandardError
                        .Select(a => new
                        {
                            a.ResultId,
                            a.EstimateConfId,
                            a.PointEstimate,
                            a.VarEstimate,
                            a.CalcStarted,
                            a.ExtensionVersion,
                            a.CalcDuration,
                            a.SamplingUnits,
                            a.MinSSize,
                            a.ActSSize,
                            a.IsLatest,
                            a.ResultExecutor,
                            a.StandardError,
                            Student =
                                ((a.StandardError != null) && (a.MinSSize != null) && (a.ActSSize != null)) ?
                                    ((a.ActSSize >= a.MinSSize) && (a.ActSSize > 1) && (a.MinSSize > 0)) ?
                                        new StudentT(
                                            location: 0,
                                            scale: 1,
                                            freedom: (double)(a.ActSSize - 1)) :
                                     (StudentT)null :
                                 (StudentT)null
                        });
                    #endregion cteResultStudent: Výsledky odhadu podílu doplněné o kvantil Studentova t-rozdělení

                    #region cteResultConfidenceInterval: Výsledky odhadu podílu doplněné o interval spolehlivosti
                    var cteResultConfidenceInterval =
                        cteResultStudent
                        .Select(a => new
                        {
                            a.ResultId,
                            a.EstimateConfId,
                            a.PointEstimate,
                            a.VarEstimate,
                            a.CalcStarted,
                            a.ExtensionVersion,
                            a.CalcDuration,
                            a.SamplingUnits,
                            a.MinSSize,
                            a.ActSSize,
                            a.IsLatest,
                            a.ResultExecutor,
                            a.StandardError,
                            a.Student,
                            ConfidenceInterval =
                                (a.Student != null) ?
                                    a.Student.InverseCumulativeDistribution(p: 1.0 - (Alfa / 2.0)) * a.StandardError :
                                null
                        });
                    #endregion cteResultConfidenceInterval: Výsledky odhadu podílu doplněné o interval spolehlivosti

                    #region cteResultExtended: Výsledky odhadu úhrnu doplněné o horní a spodní hranici intervalu spolehlivosti
                    var cteResultExtended =
                        cteResultConfidenceInterval
                        .Select(a => new
                        {
                            a.ResultId,
                            a.EstimateConfId,
                            a.PointEstimate,
                            a.VarEstimate,
                            a.CalcStarted,
                            a.ExtensionVersion,
                            a.CalcDuration,
                            a.SamplingUnits,
                            a.MinSSize,
                            a.ActSSize,
                            a.IsLatest,
                            a.ResultExecutor,
                            a.StandardError,
                            a.Student,
                            a.ConfidenceInterval,
                            LowerBound =
                                ((a.PointEstimate != null) && (a.ConfidenceInterval != null)) ?
                                (a.PointEstimate - a.ConfidenceInterval) : null,
                            UpperBound =
                                ((a.PointEstimate != null) && (a.ConfidenceInterval != null)) ?
                                (a.PointEstimate + a.ConfidenceInterval) : null
                        });
                    #endregion cteResultExtended: Výsledky odhadu úhrnu doplněné o horní a spodní hranici intervalu spolehlivosti

                    #region cteAddResRatioAttrGroups: Atributová aditivita
                    var cteAddResRatioAttr =
                        fnAddResRatioAttr.Data.AsEnumerable()
                        // Rozdíl je větší než je stanovený limit
                        .Where(a => (double)(a.Field<Nullable<double>>(TFnAddResRatioAttrList.ColDiff.Name) ?? 0.0) >= additivityLimit)
                        .Select(a => new
                        {
                            AddResRatioAttrId = a.Field<int>(TFnAddResRatioAttrList.ColId.Name),
                            EstimationCellId = a.Field<Nullable<int>>(TFnAddResRatioAttrList.ColEstimationCellId.Name),
                            AuxConfId = a.Field<Nullable<int>>(TFnAddResRatioAttrList.ColAuxConfId.Name),
                            ForceSynthetic = a.Field<Nullable<bool>>(TFnAddResRatioAttrList.ColForceSynthetic.Name),
                            EstimateConfId = a.Field<Nullable<int>>(TFnAddResRatioAttrList.ColEstimateConfId.Name),
                            DenominatorId = a.Field<Nullable<int>>(TFnAddResRatioAttrList.ColDenominatorId.Name),
                            VariableId = a.Field<Nullable<int>>(TFnAddResRatioAttrList.ColVariableId.Name),
                            PointEst = a.Field<Nullable<double>>(TFnAddResRatioAttrList.ColPointEst.Name),
                            PointEstSum = a.Field<Nullable<double>>(TFnAddResRatioAttrList.ColPointEstSum.Name),
                            Diff = a.Field<Nullable<double>>(TFnAddResRatioAttrList.ColDiff.Name),
                            VariablesDef = a.Field<string>(TFnAddResRatioAttrList.ColVariablesDef.Name),
                            VariablesFound = a.Field<string>(TFnAddResRatioAttrList.ColVariablesFound.Name),
                            EstimateConfsFound = a.Field<string>(TFnAddResRatioAttrList.ColEstimateConfsFound.Name),
                            EstimateConfsFoundList = Functions.StringToIntList(
                                text: a.Field<string>(TFnAddResRatioAttrList.ColEstimateConfsFound.Name),
                                separator: (char)59,
                                defaultValue: null)
                        });

                    var cteAddResRatioAttrGroups =
                        from a in cteAddResRatioAttr
                        group a by a.EstimateConfId into g
                        select new
                        {
                            EstimateConfId = g.Key,
                            Diff = g.Max(b => b.Diff)
                        };
                    #endregion cteAddResRatioAttrGroups: Atributová aditivita

                    #region cteNumeratorTotalEstimateConfPanel: cteNumeratorTotalEstimateConf + ctePanelRefYearSetGroup (LEFT JOIN)
                    var cteNumeratorTotalEstimateConfPanel =
                        from a in cteNumeratorTotalEstimateConf
                        join b in ctePanelRefYearSetGroup
                        on a.NumeratorPanelRefYearSetGroupId equals b.PanelRefYearSetGroupId into lj
                        from b in lj.DefaultIfEmpty()
                        select new
                        {
                            a.NumeratorTotalEstimateConfId,
                            a.NumeratorEstimationCellId,
                            a.NumeratorTotalEstimateConfName,
                            a.NumeratorVariableId,
                            a.NumeratorPhaseEstimateTypeId,
                            a.NumeratorForceSynthetic,
                            a.NumeratorAuxConfId,
                            a.NumeratorEstimationPeriodId,
                            a.NumeratorPanelRefYearSetGroupId,
                            NumeratorPanelRefYearSetGroupLabelCs = b?.PanelRefYearSetGroupLabelCs,
                            NumeratorPanelRefYearSetGroupLabelEn = b?.PanelRefYearSetGroupLabelEn,
                            NumeratorPanelRefYearSetGroupDescriptionCs = b?.PanelRefYearSetGroupDescriptionCs,
                            NumeratorPanelRefYearSetGroupDescriptionEn = b?.PanelRefYearSetGroupDescriptionEn
                        };
                    #endregion cteNumeratorTotalEstimateConfPanel: cteNumeratorTotalEstimateConf + ctePanelRefYearSetGroup (LEFT JOIN)

                    #region cteDenominatorTotalEstimateConfPanel: cteDenominatorTotalEstimateConf + ctePanelRefYearSetGroup (LEFT JOIN)
                    var cteDenominatorTotalEstimateConfPanel =
                        from a in cteDenominatorTotalEstimateConf
                        join b in ctePanelRefYearSetGroup
                        on a.DenominatorPanelRefYearSetGroupId equals b.PanelRefYearSetGroupId into lj
                        from b in lj.DefaultIfEmpty()
                        select new
                        {
                            a.DenominatorTotalEstimateConfId,
                            a.DenominatorEstimationCellId,
                            a.DenominatorTotalEstimateConfName,
                            a.DenominatorVariableId,
                            a.DenominatorPhaseEstimateTypeId,
                            a.DenominatorForceSynthetic,
                            a.DenominatorAuxConfId,
                            a.DenominatorEstimationPeriodId,
                            a.DenominatorPanelRefYearSetGroupId,
                            DenominatorPanelRefYearSetGroupLabelCs = b?.PanelRefYearSetGroupLabelCs,
                            DenominatorPanelRefYearSetGroupLabelEn = b?.PanelRefYearSetGroupLabelEn,
                            DenominatorPanelRefYearSetGroupDescriptionCs = b?.PanelRefYearSetGroupDescriptionCs,
                            DenominatorPanelRefYearSetGroupDescriptionEn = b?.PanelRefYearSetGroupDescriptionEn
                        };
                    #endregion cteDenominatorTotalEstimateConfPanel: cteDenominatorTotalEstimateConf + ctePanelRefYearSetGroup (LEFT JOIN)

                    #region cteEstimateConfNumeratorTotalEstimateConf: cteEstimateConf + cteNumeratorTotalEstimateConfPanel (INNER JOIN)
                    var cteEstimateConfNumeratorTotalEstimateConf =
                        from a in cteEstimateConf
                        join b in cteNumeratorTotalEstimateConfPanel
                        on a.NumeratorTotalEstimateConfId equals b.NumeratorTotalEstimateConfId
                        select new
                        {
                            a.EstimateConfId,
                            a.EstimateTypeId,
                            a.NumeratorTotalEstimateConfId,
                            a.DenominatorTotalEstimateConfId,
                            a.EstimateExecutor,
                            b.NumeratorEstimationCellId,
                            b.NumeratorTotalEstimateConfName,
                            b.NumeratorVariableId,
                            b.NumeratorPhaseEstimateTypeId,
                            b.NumeratorForceSynthetic,
                            b.NumeratorAuxConfId,
                            b.NumeratorEstimationPeriodId,
                            b.NumeratorPanelRefYearSetGroupId,
                            b.NumeratorPanelRefYearSetGroupLabelCs,
                            b.NumeratorPanelRefYearSetGroupLabelEn,
                            b.NumeratorPanelRefYearSetGroupDescriptionCs,
                            b.NumeratorPanelRefYearSetGroupDescriptionEn
                        };
                    #endregion cteEstimateConfNumeratorTotalEstimateConf: cteEstimateConf + cteNumeratorTotalEstimateConfPanel (INNER JOIN)

                    #region cteEstimateConfTotalEstimateConf: cteEstimateConfNumeratorTotalEstimateConf + cteDenominatorTotalEstimateConfPanel (INNER JOIN)
                    var cteEstimateConfTotalEstimateConf =
                        from a in cteEstimateConfNumeratorTotalEstimateConf
                        join b in cteDenominatorTotalEstimateConfPanel
                        on a.DenominatorTotalEstimateConfId equals b.DenominatorTotalEstimateConfId
                        select new
                        {
                            Id = String.Format(
                                "{0}:{1}-{2}-{3}:{4}-{5}",
                                a.NumeratorPhaseEstimateTypeId.ToString(),
                                b.DenominatorPhaseEstimateTypeId.ToString(),
                                (a.NumeratorEstimationPeriodId ?? 0).ToString(),
                                a.NumeratorVariableId.ToString(),
                                b.DenominatorVariableId.ToString(),
                                a.NumeratorEstimationCellId.ToString()
                            ),
                            NumeratorId = String.Format(
                                "{0}-{1}-{2}-{3}",
                                a.NumeratorPhaseEstimateTypeId.ToString(),
                                (a.NumeratorEstimationPeriodId ?? 0).ToString(),
                                a.NumeratorVariableId.ToString(),
                                a.NumeratorEstimationCellId.ToString()
                            ),
                            DenominatorId = String.Format(
                                "{0}-{1}-{2}-{3}",
                                b.DenominatorPhaseEstimateTypeId.ToString(),
                                (a.NumeratorEstimationPeriodId ?? 0).ToString(),
                                b.DenominatorVariableId.ToString(),
                                a.NumeratorEstimationCellId.ToString()
                            ),
                            a.EstimateConfId,
                            a.EstimateTypeId,
                            a.NumeratorTotalEstimateConfId,
                            a.DenominatorTotalEstimateConfId,
                            a.EstimateExecutor,
                            a.NumeratorEstimationCellId,
                            a.NumeratorTotalEstimateConfName,
                            a.NumeratorVariableId,
                            a.NumeratorPhaseEstimateTypeId,
                            a.NumeratorForceSynthetic,
                            a.NumeratorAuxConfId,
                            a.NumeratorEstimationPeriodId,
                            a.NumeratorPanelRefYearSetGroupId,
                            a.NumeratorPanelRefYearSetGroupLabelCs,
                            a.NumeratorPanelRefYearSetGroupLabelEn,
                            a.NumeratorPanelRefYearSetGroupDescriptionCs,
                            a.NumeratorPanelRefYearSetGroupDescriptionEn,
                            b.DenominatorEstimationCellId,
                            b.DenominatorTotalEstimateConfName,
                            b.DenominatorVariableId,
                            b.DenominatorPhaseEstimateTypeId,
                            b.DenominatorForceSynthetic,
                            b.DenominatorAuxConfId,
                            b.DenominatorEstimationPeriodId,
                            b.DenominatorPanelRefYearSetGroupId,
                            b.DenominatorPanelRefYearSetGroupLabelCs,
                            b.DenominatorPanelRefYearSetGroupLabelEn,
                            b.DenominatorPanelRefYearSetGroupDescriptionCs,
                            b.DenominatorPanelRefYearSetGroupDescriptionEn
                        };
                    #endregion cteEstimateConfTotalEstimateConf: cteEstimateConfNumeratorTotalEstimateConf + cteDenominatorTotalEstimateConfPanel (INNER JOIN)

                    #region cteEstimateConfResult: cteEstimateConfTotalEstimateConf + cteResultExtended (LEFT JOIN)
                    var cteEstimateConfResult =
                        from a in cteEstimateConfTotalEstimateConf
                        join b in cteResultExtended
                        on a.EstimateConfId equals b.EstimateConfId into lj
                        from b in lj.DefaultIfEmpty()
                        select new
                        {
                            a.Id,
                            a.NumeratorId,
                            a.DenominatorId,
                            a.EstimateConfId,
                            a.EstimateTypeId,
                            a.NumeratorTotalEstimateConfId,
                            a.DenominatorTotalEstimateConfId,
                            a.EstimateExecutor,
                            a.NumeratorEstimationCellId,
                            a.NumeratorTotalEstimateConfName,
                            a.NumeratorVariableId,
                            a.NumeratorPhaseEstimateTypeId,
                            a.NumeratorForceSynthetic,
                            a.NumeratorAuxConfId,
                            a.NumeratorEstimationPeriodId,
                            a.NumeratorPanelRefYearSetGroupId,
                            a.NumeratorPanelRefYearSetGroupLabelCs,
                            a.NumeratorPanelRefYearSetGroupLabelEn,
                            a.NumeratorPanelRefYearSetGroupDescriptionCs,
                            a.NumeratorPanelRefYearSetGroupDescriptionEn,
                            a.DenominatorEstimationCellId,
                            a.DenominatorTotalEstimateConfName,
                            a.DenominatorVariableId,
                            a.DenominatorPhaseEstimateTypeId,
                            a.DenominatorForceSynthetic,
                            a.DenominatorAuxConfId,
                            a.DenominatorEstimationPeriodId,
                            a.DenominatorPanelRefYearSetGroupId,
                            a.DenominatorPanelRefYearSetGroupLabelCs,
                            a.DenominatorPanelRefYearSetGroupLabelEn,
                            a.DenominatorPanelRefYearSetGroupDescriptionCs,
                            a.DenominatorPanelRefYearSetGroupDescriptionEn,
                            b?.ResultId,
                            b?.PointEstimate,
                            b?.VarEstimate,
                            b?.CalcStarted,
                            b?.ExtensionVersion,
                            b?.CalcDuration,
                            b?.SamplingUnits,
                            b?.MinSSize,
                            b?.ActSSize,
                            b?.IsLatest,
                            b?.ResultExecutor,
                            b?.StandardError,
                            b?.Student,
                            b?.ConfidenceInterval,
                            b?.LowerBound,
                            b?.UpperBound
                        };
                    #endregion cteEstimateConfResult: cteEstimateConfTotalEstimateConf + cteResultExtended (LEFT JOIN)

                    #region cteEstimateConfResultAttrAdditivity: cteEstimateConfResult + cteAddResRatioAttrGroups (LEFT JOIN)
                    var cteEstimateConfResultAttrAdditivity =
                        from a in cteEstimateConfResult
                        join b in cteAddResRatioAttrGroups
                        on a.EstimateConfId equals b.EstimateConfId into lj
                        from b in lj.DefaultIfEmpty()
                        select new
                        {
                            a.Id,
                            a.NumeratorId,
                            a.DenominatorId,
                            a.EstimateConfId,
                            a.EstimateTypeId,
                            a.NumeratorTotalEstimateConfId,
                            a.DenominatorTotalEstimateConfId,
                            a.EstimateExecutor,
                            a.NumeratorEstimationCellId,
                            a.NumeratorTotalEstimateConfName,
                            a.NumeratorVariableId,
                            a.NumeratorPhaseEstimateTypeId,
                            a.NumeratorForceSynthetic,
                            a.NumeratorAuxConfId,
                            a.NumeratorEstimationPeriodId,
                            a.NumeratorPanelRefYearSetGroupId,
                            a.NumeratorPanelRefYearSetGroupLabelCs,
                            a.NumeratorPanelRefYearSetGroupLabelEn,
                            a.NumeratorPanelRefYearSetGroupDescriptionCs,
                            a.NumeratorPanelRefYearSetGroupDescriptionEn,
                            a.DenominatorEstimationCellId,
                            a.DenominatorTotalEstimateConfName,
                            a.DenominatorVariableId,
                            a.DenominatorPhaseEstimateTypeId,
                            a.DenominatorForceSynthetic,
                            a.DenominatorAuxConfId,
                            a.DenominatorEstimationPeriodId,
                            a.DenominatorPanelRefYearSetGroupId,
                            a.DenominatorPanelRefYearSetGroupLabelCs,
                            a.DenominatorPanelRefYearSetGroupLabelEn,
                            a.DenominatorPanelRefYearSetGroupDescriptionCs,
                            a.DenominatorPanelRefYearSetGroupDescriptionEn,
                            a.ResultId,
                            a.PointEstimate,
                            a.VarEstimate,
                            a.CalcStarted,
                            a.ExtensionVersion,
                            a.CalcDuration,
                            a.SamplingUnits,
                            a.MinSSize,
                            a.ActSSize,
                            a.IsLatest,
                            a.ResultExecutor,
                            a.StandardError,
                            a.Student,
                            a.ConfidenceInterval,
                            a.LowerBound,
                            a.UpperBound,
                            AttrDiffDown = (b?.Diff) ?? 0.0,
                            AttrDiffUp = 0.0,
                        };
                    #endregion cteEstimateConfResultAttrAdditivity: cteEstimateConfResult + cteAddResRatioAttrGroups (LEFT JOIN)

                    #region Zápis výsledku do DataTable
                    if (cteEstimateConfResultAttrAdditivity.Any())
                    {
                        Data =
                            cteEstimateConfResultAttrAdditivity
                            .OrderBy(a => a.NumeratorPhaseEstimateTypeId)
                            .ThenBy(a => a.NumeratorEstimationPeriodId)
                            .ThenBy(a => a.NumeratorVariableId)
                            .ThenBy(a => a.DenominatorVariableId)
                            .ThenBy(a => a.NumeratorEstimationCellId)
                            .Select(a =>
                                Data.LoadDataRow(
                                    values:
                                    [
                                        a.Id,
                                        a.NumeratorId,
                                        a.DenominatorId,
                                        a.NumeratorPhaseEstimateTypeId,
                                        a.NumeratorEstimationPeriodId,
                                        a.NumeratorVariableId,
                                        a.DenominatorVariableId,
                                        a.NumeratorEstimationCellId,
                                        a.NumeratorPanelRefYearSetGroupId,
                                        a.NumeratorPanelRefYearSetGroupLabelCs,
                                        a.NumeratorPanelRefYearSetGroupLabelEn,
                                        a.EstimateConfId,
                                        a.EstimateTypeId,
                                        a.NumeratorTotalEstimateConfId,
                                        a.DenominatorTotalEstimateConfId,
                                        a.EstimateExecutor,
                                        a.NumeratorTotalEstimateConfName,
                                        a.DenominatorTotalEstimateConfName,
                                        a.ResultId,
                                        a.PointEstimate,
                                        a.VarEstimate,
                                        a.CalcStarted,
                                        a.ExtensionVersion,
                                        a.CalcDuration,
                                        a.MinSSize,
                                        a.ActSSize,
                                        a.IsLatest,
                                        a.ResultExecutor,
                                        a.StandardError,
                                        a.ConfidenceInterval,
                                        a.LowerBound,
                                        a.UpperBound,
                                        a.AttrDiffDown,
                                        a.AttrDiffUp
                                    ],
                                    fAcceptChanges: false))
                            .CopyToDataTable();
                        Data.TableName = Name;
                    }
                    #endregion Zápis výsledku do DataTable

                    #region Doplnění AttrDiffUp
                    foreach (var a in cteEstimateConfResultAttrAdditivity)
                    {
                        foreach (var b in cteAddResRatioAttr)
                        {
                            double diff = b.Diff ?? 0.0;
                            if (b.EstimateConfsFoundList.Contains(item: a.EstimateConfId))
                            {
                                string whereCondition = $"{ColEstimateConfId.Name} = {a.EstimateConfId}";
                                DataRow[] rows = Data.Select(filterExpression: whereCondition);
                                foreach (DataRow row in rows)
                                {
                                    double val = Functions.GetDoubleArg(row: row, name: ColAttrDiffUp.Name, defaultValue: 0);
                                    if (val < diff)
                                    {
                                        Functions.SetNDoubleArg(row: row, name: ColAttrDiffUp.Name, val: diff);
                                    }
                                }
                            }
                        }
                    }
                    #endregion Doplnění AttrDiffUp

                    #region Omezení podmínkou
                    if (!String.IsNullOrEmpty(value: Condition))
                    {
                        DataView view = new(table: Data);
                        try
                        {
                            view.RowFilter = Condition;
                        }
                        catch
                        {
                            view.RowFilter = String.Empty;
                            Condition = String.Empty;
                        }
                        Data = view.ToTable();
                    }
                    #endregion Omezení podmínkou

                    #region Omezeni počtem
                    if (Limit != null)
                    {
                        Data = Data.AsEnumerable()
                            .Take(count: (int)Limit)
                            .CopyToDataTable();
                    }
                    #endregion Omezeni počtem

                    stopWatch.Stop();
                    LoadingTime = 0.001 * stopWatch.ElapsedMilliseconds;
                    Data.TableName = TableName;
                }

                #endregion Methods

            }

        }
    }
}
