﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {
            // v_olap_total_estimate_dimension

            /// <summary>
            /// OLAP Cube Dimension - Total Estimate
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class OLAPTotalEstimateDimension(
                OLAPTotalEstimateDimensionList composite = null,
                DataRow data = null)
                    : ALinqViewEntry<OLAPTotalEstimateDimensionList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// OLAP Cube Dimension - Total Estimate - composite identifier
                /// </summary>
                public new string Id
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateDimensionList.ColId.Name,
                            defaultValue: OLAPTotalEstimateDimensionList.ColId.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateDimensionList.ColId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Estimate phase identifier
                /// </summary>
                public int PhaseEstimateTypeId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: OLAPTotalEstimateDimensionList.ColPhaseEstimateTypeId.Name,
                            defaultValue: Int32.Parse(s: OLAPTotalEstimateDimensionList.ColPhaseEstimateTypeId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: OLAPTotalEstimateDimensionList.ColPhaseEstimateTypeId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Label of estimate phase (in national language)
                /// </summary>
                public string PhaseEstimateTypeLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateDimensionList.ColPhaseEstimateTypeLabelCs.Name,
                            defaultValue: OLAPTotalEstimateDimensionList.ColPhaseEstimateTypeLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateDimensionList.ColPhaseEstimateTypeLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Label of estimate phase (in English)
                /// </summary>
                public string PhaseEstimateTypeLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateDimensionList.ColPhaseEstimateTypeLabelEn.Name,
                            defaultValue: OLAPTotalEstimateDimensionList.ColPhaseEstimateTypeLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateDimensionList.ColPhaseEstimateTypeLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Estimate phase object
                /// </summary>s
                public PhaseEstimateType PhaseEstimateType
                {
                    get
                    {
                        return
                             ((NfiEstaDB)Composite.Database)
                             .SNfiEsta.CPhaseEstimateType[(int)PhaseEstimateTypeId];
                    }
                }


                /// <summary>
                /// Estimation period identifier
                /// </summary>
                public int EstimationPeriodId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: OLAPTotalEstimateDimensionList.ColEstimationPeriodId.Name,
                            defaultValue: Int32.Parse(s: OLAPTotalEstimateDimensionList.ColEstimationPeriodId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: OLAPTotalEstimateDimensionList.ColEstimationPeriodId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// The beginning of the time to witch the estimate is calculated
                /// </summary>
                public DateTime EstimateDateBegin
                {
                    get
                    {
                        return Functions.GetDateTimeArg(
                            row: Data,
                            name: OLAPTotalEstimateDimensionList.ColEstimateDateBegin.Name,
                            defaultValue: DateTime.Parse(s: OLAPTotalEstimateDimensionList.ColEstimateDateBegin.DefaultValue));
                    }
                    set
                    {
                        Functions.SetDateTimeArg(
                            row: Data,
                            name: OLAPTotalEstimateDimensionList.ColEstimateDateBegin.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// The beginning of the time to witch the estimate is calculated as text
                /// </summary>
                public string EstimateDateBeginText
                {
                    get
                    {
                        return
                            EstimateDateBegin.ToString(
                                format: OLAPTotalEstimateDimensionList.ColEstimateDateBegin.NumericFormat);
                    }
                }

                /// <summary>
                /// The end of the time to witch the estimate is calculated
                /// </summary>
                public DateTime EstimateDateEnd
                {
                    get
                    {
                        return Functions.GetDateTimeArg(
                            row: Data,
                            name: OLAPTotalEstimateDimensionList.ColEstimateDateEnd.Name,
                            defaultValue: DateTime.Parse(s: OLAPTotalEstimateDimensionList.ColEstimateDateEnd.DefaultValue));
                    }
                    set
                    {
                        Functions.SetDateTimeArg(
                            row: Data,
                            name: OLAPTotalEstimateDimensionList.ColEstimateDateEnd.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// The end of the time to witch the estimate is calculated as text
                /// </summary>
                public string EstimateDateEndText
                {
                    get
                    {
                        return
                            EstimateDateEnd.ToString(
                                format: OLAPTotalEstimateDimensionList.ColEstimateDateEnd.NumericFormat);
                    }
                }

                /// <summary>
                /// Estimation period label (in national language)
                /// </summary>
                public string EstimationPeriodLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateDimensionList.ColEstimationPeriodLabelCs.Name,
                            defaultValue: OLAPTotalEstimateDimensionList.ColEstimationPeriodLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateDimensionList.ColEstimationPeriodLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Estimation period label (in English)
                /// </summary>
                public string EstimationPeriodLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateDimensionList.ColEstimationPeriodLabelEn.Name,
                            defaultValue: OLAPTotalEstimateDimensionList.ColEstimationPeriodLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateDimensionList.ColEstimationPeriodLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Estimation period object (read-only)
                /// </summary>
                public EstimationPeriod EstimationPeriod
                {
                    get
                    {
                        return
                             ((NfiEstaDB)Composite.Database)
                             .SNfiEsta.CEstimationPeriod[(int)EstimationPeriodId];
                    }
                }


                /// <summary>
                /// Combination of target variable and sub-population category and area domain category identifier
                /// </summary>
                public int VariableId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: OLAPTotalEstimateDimensionList.ColVariableId.Name,
                            defaultValue: Int32.Parse(s: OLAPTotalEstimateDimensionList.ColVariableId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: OLAPTotalEstimateDimensionList.ColVariableId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Label of variable (in national language)
                /// </summary>
                public string VariableLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateDimensionList.ColVariableLabelCs.Name,
                            defaultValue: OLAPTotalEstimateDimensionList.ColVariableLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateDimensionList.ColVariableLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Label of variable (in English)
                /// </summary>
                public string VariableLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateDimensionList.ColVariableLabelEn.Name,
                            defaultValue: OLAPTotalEstimateDimensionList.ColVariableLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateDimensionList.ColVariableLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Combination of target variable and sub-population category and area domain category object
                /// </summary>
                public Variable Variable
                {
                    get
                    {
                        return
                             ((NfiEstaDB)Composite.Database)
                             .SNfiEsta.TVariable[(int)VariableId];
                    }
                }

                /// <summary>
                /// Combination of target variable and sub-population category and area domain category (extended-version) object
                /// </summary>
                public VwVariable VwVariable
                {
                    get
                    {
                        return
                             ((NfiEstaDB)Composite.Database)
                             .SNfiEsta.VVariable[(int)VariableId];
                    }
                }


                /// <summary>
                /// Estimation cell identifier
                /// </summary>
                public int EstimationCellId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: OLAPTotalEstimateDimensionList.ColEstimationCellId.Name,
                            defaultValue: Int32.Parse(s: OLAPTotalEstimateDimensionList.ColEstimationCellId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: OLAPTotalEstimateDimensionList.ColEstimationCellId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Description of estimation cell (in national language)
                /// </summary>
                public string EstimationCellDescriptionCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateDimensionList.ColEstimationCellDescriptionCs.Name,
                            defaultValue: OLAPTotalEstimateDimensionList.ColEstimationCellDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateDimensionList.ColEstimationCellDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Description of estimation cell (in English)
                /// </summary>
                public string EstimationCellDescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateDimensionList.ColEstimationCellDescriptionEn.Name,
                            defaultValue: OLAPTotalEstimateDimensionList.ColEstimationCellDescriptionEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateDimensionList.ColEstimationCellDescriptionEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Estimation cell object
                /// </summary>
                public EstimationCell EstimationCell
                {
                    get
                    {
                        return
                             ((NfiEstaDB)Composite.Database)
                             .SNfiEsta.CEstimationCell[(int)EstimationCellId];
                    }
                }


                /// <summary>
                /// Estimation cell collection identifier
                /// </summary>
                public int EstimationCellCollectionId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: OLAPTotalEstimateDimensionList.ColEstimationCellCollectionId.Name,
                            defaultValue: Int32.Parse(s: OLAPTotalEstimateDimensionList.ColEstimationCellCollectionId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: OLAPTotalEstimateDimensionList.ColEstimationCellCollectionId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Label of estimation cell collection (in national language)
                /// </summary>
                public string EstimationCellCollectionLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateDimensionList.ColEstimationCellCollectionLabelCs.Name,
                            defaultValue: OLAPTotalEstimateDimensionList.ColEstimationCellCollectionLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateDimensionList.ColEstimationCellCollectionLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Label of estimation cell collection (in English)
                /// </summary>
                public string EstimationCellCollectionLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateDimensionList.ColEstimationCellCollectionLabelEn.Name,
                            defaultValue: OLAPTotalEstimateDimensionList.ColEstimationCellCollectionLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateDimensionList.ColEstimationCellCollectionLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Estimation cell collection object
                /// </summary>
                public EstimationCellCollection EstimationCellCollection
                {
                    get
                    {
                        return
                             ((NfiEstaDB)Composite.Database)
                             .SNfiEsta.CEstimationCellCollection[(int)EstimationCellCollectionId];
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not OLAPEstimateDimension Type, return False
                    if (obj is not OLAPTotalEstimateDimension)
                    {
                        return false;
                    }

                    return
                        (PhaseEstimateTypeId == ((OLAPTotalEstimateDimension)obj).PhaseEstimateTypeId) &&
                        (EstimationPeriodId == ((OLAPTotalEstimateDimension)obj).EstimationPeriodId) &&
                        (VariableId == ((OLAPTotalEstimateDimension)obj).VariableId) &&
                        (EstimationCellId == ((OLAPTotalEstimateDimension)obj).EstimationCellId);
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        PhaseEstimateTypeId.GetHashCode() ^
                        EstimationPeriodId.GetHashCode() ^
                        VariableId.GetHashCode() ^
                        EstimationCellId.GetHashCode();
                }

                /// <summary>
                /// Text description of the object
                /// </summary>
                /// <returns>Text description of the object</returns>
                public override string ToString()
                {
                    return String.Concat(
                        $"{"ID"} = {Functions.PrepStringArg(arg: Id)}{Environment.NewLine}");
                }

                #endregion Methods

            }


            /// <summary>
            /// List of OLAP Cube Dimensions - Total Estimate
            /// </summary>
            public class OLAPTotalEstimateDimensionList
                : ALinqView
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema =
                    NfiEstaSchema.Name;

                /// <summary>
                /// Table name
                /// </summary>
                public static readonly string Name =
                    "v_olap_total_estimate_dimension";

                /// <summary>
                /// Table alias
                /// </summary>
                public static readonly string Alias =
                    "v_olap_total_estimate_dimension";

                /// <summary>
                /// Description in national language
                /// </summary>
                public static readonly string CaptionCs =
                    "Seznam dimenzí OLAP kostky s daty odhadů úhrnů";

                /// <summary>
                /// Description in English
                /// </summary>
                public static readonly string CaptionEn =
                    "List of OLAP cube dimensions with total estimates data";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
                {
                    { "id", new ColumnMetadata()
                    {
                        Name = "id",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = true,
                        NotNull = true,
                        DefaultValue =  String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = String.Empty,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ID",
                        HeaderTextEn = "ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 0
                    }
                    },
                    { "phase_estimate_type_id", new ColumnMetadata()
                    {
                        Name = "phase_estimate_type_id",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue =  "0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PHASE_ESTIMATE_TYPE_ID",
                        HeaderTextEn = "PHASE_ESTIMATE_TYPE_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 1
                    }
                    },
                    { "phase_estimate_type_label_cs", new ColumnMetadata()
                    {
                        Name = "phase_estimate_type_label_cs",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue =  String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = String.Empty,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs =  "PHASE_ESTIMATE_TYPE_LABEL_CS",
                        HeaderTextEn = "PHASE_ESTIMATE_TYPE_LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 2
                    }
                    },
                    { "phase_estimate_type_label_en", new ColumnMetadata()
                    {
                        Name = "phase_estimate_type_label_en",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue =  String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = String.Empty,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs =  "PHASE_ESTIMATE_TYPE_LABEL_EN",
                        HeaderTextEn = "PHASE_ESTIMATE_TYPE_LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 3
                    }
                    },
                    { "estimation_period_id", new ColumnMetadata()
                    {
                        Name = "estimation_period_id",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue =  "0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATION_PERIOD_ID",
                        HeaderTextEn = "ESTIMATION_PERIOD_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 4
                    }
                    },
                    { "estimate_date_begin", new ColumnMetadata()
                    {
                        Name = "estimate_date_begin",
                        DbName = null,
                        DataType = "System.DateTime",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue = new DateTime().ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "yyyy-MM-dd",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATE_DATE_BEGIN",
                        HeaderTextEn = "ESTIMATE_DATE_BEGIN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 5
                    }
                    },
                    { "estimate_date_end", new ColumnMetadata()
                    {
                        Name = "estimate_date_end",
                        DbName = null,
                        DataType = "System.DateTime",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue = new DateTime().ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "yyyy-MM-dd",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATE_DATE_END",
                        HeaderTextEn = "ESTIMATE_DATE_END",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 6
                    }
                    },
                    { "estimation_period_label_cs", new ColumnMetadata()
                    {
                        Name = "estimation_period_label_cs",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue =  String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = String.Empty,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs =  "ESTIMATION_PERIOD_LABEL_CS",
                        HeaderTextEn = "ESTIMATION_PERIOD_LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 7
                    }
                    },
                    { "estimation_period_label_en", new ColumnMetadata()
                    {
                        Name = "estimation_period_label_en",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue =  String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = String.Empty,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs =  "ESTIMATION_PERIOD_LABEL_EN",
                        HeaderTextEn = "ESTIMATION_PERIOD_LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 8
                    }
                    },
                    { "variable_id", new ColumnMetadata()
                    {
                        Name = "variable_id",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue =  "0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "VARIABLE_ID",
                        HeaderTextEn = "VARIABLE_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 9
                    }
                    },
                    { "variable_label_cs", new ColumnMetadata()
                    {
                        Name = "variable_label_cs",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue =  String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = String.Empty,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs =  "VARIABLE_LABEL_CS",
                        HeaderTextEn = "VARIABLE_LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 10
                    }
                    },
                    { "variable_label_en", new ColumnMetadata()
                    {
                        Name = "variable_label_en",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue =  String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = String.Empty,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs =  "VARIABLE_LABEL_EN",
                        HeaderTextEn = "VARIABLE_LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 11
                    }
                    },
                    { "estimation_cell_id", new ColumnMetadata()
                    {
                        Name = "estimation_cell_id",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue =  "0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATION_CELL_ID",
                        HeaderTextEn = "ESTIMATION_CELL_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 12
                    }
                    },
                    { "estimation_cell_description_cs", new ColumnMetadata()
                    {
                        Name = "estimation_cell_description_cs",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue =  String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = String.Empty,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs =  "ESTIMATION_CELL_DESCRIPTION_CS",
                        HeaderTextEn = "ESTIMATION_CELL_DESCRIPTION_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 13
                    }
                    },
                    { "estimation_cell_description_en", new ColumnMetadata()
                    {
                        Name = "estimation_cell_description_en",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue =  String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = String.Empty,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs =  "ESTIMATION_CELL_DESCRIPTION_EN",
                        HeaderTextEn = "ESTIMATION_CELL_DESCRIPTION_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 14
                    }
                    },
                    { "estimation_cell_collection_id", new ColumnMetadata()
                    {
                        Name = "estimation_cell_collection_id",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue =  "0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATION_CELL_COLLECTION_ID",
                        HeaderTextEn = "ESTIMATION_CELL_COLLECTION_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 15
                    }
                    },
                    { "estimation_cell_collection_label_cs", new ColumnMetadata()
                    {
                        Name = "estimation_cell_collection_label_cs",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue =  String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = String.Empty,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs =  "ESTIMATION_CELL_COLLECTION_LABEL_CS",
                        HeaderTextEn = "ESTIMATION_CELL_COLLECTION_LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 16
                    }
                    },
                    { "estimation_cell_collection_label_en", new ColumnMetadata()
                    {
                        Name = "estimation_cell_collection_label_en",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue =  String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = String.Empty,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs =  "ESTIMATION_CELL_COLLECTION_DESCRIPTION_EN",
                        HeaderTextEn = "ESTIMATION_CELL_COLLECTION_DESCRIPTION_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 17
                    }
                    }
                };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column phase_estimate_type_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColPhaseEstimateTypeId = Cols["phase_estimate_type_id"];

                /// <summary>
                /// Column phase_estimate_type_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColPhaseEstimateTypeLabelCs = Cols["phase_estimate_type_label_cs"];

                /// <summary>
                /// Column phase_estimate_type_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColPhaseEstimateTypeLabelEn = Cols["phase_estimate_type_label_en"];

                /// <summary>
                /// Column estimation_period_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationPeriodId = Cols["estimation_period_id"];

                /// <summary>
                /// Column estimate_date_begin metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimateDateBegin = Cols["estimate_date_begin"];

                /// <summary>
                /// Column estimate_date_end metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimateDateEnd = Cols["estimate_date_end"];

                /// <summary>
                /// Column estimation_period_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationPeriodLabelCs = Cols["estimation_period_label_cs"];

                /// <summary>
                /// Column estimation_period_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationPeriodLabelEn = Cols["estimation_period_label_en"];

                /// <summary>
                /// Column variable_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColVariableId = Cols["variable_id"];

                /// <summary>
                /// Column variable_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColVariableLabelCs = Cols["variable_label_cs"];

                /// <summary>
                /// Column variable_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColVariableLabelEn = Cols["variable_label_en"];

                /// <summary>
                /// Column estimation_cell_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationCellId = Cols["estimation_cell_id"];

                /// <summary>
                /// Column estimation_cell_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationCellDescriptionCs = Cols["estimation_cell_description_cs"];

                /// <summary>
                /// Column estimation_cell_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationCellDescriptionEn = Cols["estimation_cell_description_en"];

                /// <summary>
                /// Column estimation_cell_collection_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationCellCollectionId = Cols["estimation_cell_collection_id"];

                /// <summary>
                /// Column estimation_cell_collection_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationCellCollectionLabelCs = Cols["estimation_cell_collection_label_cs"];

                /// <summary>
                /// Column estimation_cell_collection_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationCellCollectionLabelEn = Cols["estimation_cell_collection_label_en"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public OLAPTotalEstimateDimensionList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public OLAPTotalEstimateDimensionList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// List of OLAP Cube Dimensions - Total Estimate (read-only)
                /// </summary>
                public List<OLAPTotalEstimateDimension> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new OLAPTotalEstimateDimension(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// OLAP Cube Dimension - Total Estimate from list by identifier (read-only)
                /// </summary>
                /// <param name="id">OLAP Cube Dimension - Total Estimate identifier</param>
                /// <returns>OLAP Cube Dimension - Total Estimate from list by identifier (null if not found)</returns>
                public OLAPTotalEstimateDimension this[string id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<string>(ColId.Name) == id)
                                .Select(a => new OLAPTotalEstimateDimension(composite: this, data: a))
                                .FirstOrDefault<OLAPTotalEstimateDimension>();
                    }
                }

                #endregion Indexer


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public OLAPTotalEstimateDimensionList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new OLAPTotalEstimateDimensionList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new OLAPTotalEstimateDimensionList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                /// <summary>
                /// Loads data into OLAP Cube Dimensions - Total Estimate (if it was not done before)
                /// </summary>
                /// <param name="cPhaseEstimateType">List of phase estimate types</param>
                /// <param name="cEstimationPeriod">List of estimation periods</param>
                /// <param name="variables">List of attribute categories</param>
                /// <param name="cEstimationCell">List of estimation cells</param>
                /// <param name="cEstimationCellCollection">List of estimation cell collections</param>
                /// <param name="condition">Condition for selection data</param>
                /// <param name="limit">Rows count limit</param>
                public void Load(
                    PhaseEstimateTypeList cPhaseEstimateType,
                    EstimationPeriodList cEstimationPeriod,
                    List<VwVariable> variables,
                    EstimationCellList cEstimationCell,
                    EstimationCellCollectionList cEstimationCellCollection,
                    string condition = null,
                    Nullable<int> limit = null)
                {
                    if (!Loaded)
                    {
                        ReLoad(
                            cPhaseEstimateType: cPhaseEstimateType,
                            cEstimationPeriod: cEstimationPeriod,
                            variables: variables,
                            cEstimationCell: cEstimationCell,
                            cEstimationCellCollection: cEstimationCellCollection,
                            condition: condition,
                            limit: limit);
                    }
                }

                /// <summary>
                /// Loads data into OLAP Cube Dimensions - Total Estimate
                /// </summary>
                /// <param name="cPhaseEstimateType">List of phase estimate types</param>
                /// <param name="cEstimationPeriod">List of estimation periods</param>
                /// <param name="variables">List of attribute categories</param>
                /// <param name="cEstimationCell">List of estimation cells</param>
                /// <param name="cEstimationCellCollection">List of estimation cell collections</param>
                /// <param name="condition">Condition for selection data</param>
                /// <param name="limit">Rows count limit</param>
                public void ReLoad(
                    PhaseEstimateTypeList cPhaseEstimateType,
                    EstimationPeriodList cEstimationPeriod,
                    List<VwVariable> variables,
                    EstimationCellList cEstimationCell,
                    EstimationCellCollectionList cEstimationCellCollection,
                    string condition = null,
                    Nullable<int> limit = null)
                {
                    Data = EmptyDataTable(
                                 tableName: TableName,
                                 columns: Columns);
                    Condition = condition;
                    Limit = limit;

                    System.Diagnostics.Stopwatch stopWatch = new();
                    stopWatch.Start();

                    #region ctePhaseEstimateType: Fáze odhadu (1.dimenze)
                    var ctePhaseEstimateType =
                        cPhaseEstimateType.Data.AsEnumerable()
                        .Select(a => new
                        {
                            PhaseEstimateTypeId = a.Field<int>(columnName: PhaseEstimateTypeList.ColId.Name),
                            PhaseEstimateTypeLabelCs = a.Field<string>(columnName: PhaseEstimateTypeList.ColLabelCs.Name),
                            PhaseEstimateTypeLabelEn = a.Field<string>(columnName: PhaseEstimateTypeList.ColLabelEn.Name),
                            PhaseEstimateTypeDescriptionCs = a.Field<string>(columnName: PhaseEstimateTypeList.ColDescriptionCs.Name),
                            PhaseEstimateTypeDescriptionEn = a.Field<string>(columnName: PhaseEstimateTypeList.ColDescriptionEn.Name)
                        });
                    #endregion ctePhaseEstimateType: Fáze odhadu (1.dimenze)

                    #region cteEstimationPeriod: Období odhadu (2.dimenze)
                    var cteEstimationPeriod =
                        cEstimationPeriod.Data.AsEnumerable()
                        .Select(a => new
                        {
                            EstimationPeriodId = a.Field<int>(columnName: EstimationPeriodList.ColId.Name),
                            EstimateDateBegin = a.Field<DateTime>(columnName: EstimationPeriodList.ColEstimateDateBegin.Name),
                            EstimateDateEnd = a.Field<DateTime>(columnName: EstimationPeriodList.ColEstimateDateEnd.Name),
                            EstimationPeriodLabelCs = a.Field<string>(columnName: EstimationPeriodList.ColLabelCs.Name),
                            EstimationPeriodLabelEn = a.Field<string>(columnName: EstimationPeriodList.ColLabelEn.Name),
                            EstimationPeriodDescriptionCs = a.Field<string>(columnName: EstimationPeriodList.ColDescriptionCs.Name),
                            EstimationPeriodDescriptionEn = a.Field<string>(columnName: EstimationPeriodList.ColDescriptionEn.Name)
                        });
                    #endregion cteEstimationPeriod: Období odhadu (2.dimenze)

                    #region cteAttributeCategory: Atributové kategorie (3.dimenze)
                    var cteAttributeCategory =
                        variables
                        .Select(a => new
                        {
                            a.VariableId,
                            a.VariableLabelCs,
                            a.VariableLabelEn
                        });
                    #endregion cteAttributeCategory: Atributové kategorie (3.dimenze)

                    #region cteEstimationCell: Výpočetní buňky (4.dimenze)
                    var cteEstimationCell =
                        cEstimationCell.Data.AsEnumerable()
                        .Select(a => new
                        {
                            EstimationCellId = a.Field<int>(columnName: EstimationCellList.ColId.Name),
                            EstimationCellCollectionId = a.Field<int>(columnName: EstimationCellList.ColEstimationCellCollectionId.Name),
                            EstimationCellLabelCs = a.Field<string>(columnName: EstimationCellList.ColLabelCs.Name),
                            EstimationCellLabelEn = a.Field<string>(columnName: EstimationCellList.ColLabelEn.Name),
                            EstimationCellDescriptionCs = a.Field<string>(columnName: EstimationCellList.ColDescriptionCs.Name),
                            EstimationCellDescriptionEn = a.Field<string>(columnName: EstimationCellList.ColDescriptionEn.Name)
                        });
                    #endregion cteEstimationCell: Výpočetní buňky (4.dimenze)

                    #region cteEstimationCellCollection: Skupiny výpočetních buněk
                    var cteEstimationCellCollection =
                        cEstimationCellCollection.Data.AsEnumerable()
                        .Select(a => new
                        {
                            EstimationCellCollectionId = a.Field<int>(columnName: EstimationCellCollectionList.ColId.Name),
                            EstimationCellCollectionLabelCs = a.Field<string>(columnName: EstimationCellCollectionList.ColLabelCs.Name),
                            EstimationCellCollectionLabelEn = a.Field<string>(columnName: EstimationCellCollectionList.ColLabelEn.Name),
                            EstimationCellCollectionDescriptionCs = a.Field<string>(columnName: EstimationCellCollectionList.ColDescriptionCs.Name),
                            EstimationCellCollectionDescriptionEn = a.Field<string>(columnName: EstimationCellCollectionList.ColDescriptionEn.Name)
                        });
                    #endregion cteEstimationCellCollection: Skupiny výpočetních buněk

                    #region cteEstimationCellExtended: Výpočetní buňky doplněné o příslušnost ke skupině výpočetních buněk
                    var cteEstimationCellExtended =
                        from a in cteEstimationCell
                        join b in cteEstimationCellCollection
                        on a.EstimationCellCollectionId equals b.EstimationCellCollectionId into lj
                        from b in lj.DefaultIfEmpty()
                        select new
                        {
                            a.EstimationCellId,
                            a.EstimationCellLabelCs,
                            a.EstimationCellLabelEn,
                            a.EstimationCellDescriptionCs,
                            a.EstimationCellDescriptionEn,
                            b?.EstimationCellCollectionId,
                            b?.EstimationCellCollectionLabelCs,
                            b?.EstimationCellCollectionLabelEn,
                            b?.EstimationCellCollectionDescriptionCs,
                            b?.EstimationCellCollectionDescriptionEn
                        };
                    #endregion cteEstimationCellExtended: Výpočetní buňky doplněné o příslušnost ke skupině výpočetních buněk

                    #region cteCartesianProduct: Kartézský součin dimenzí
                    var cteCartesianProduct =
                        from a in ctePhaseEstimateType
                        from b in cteEstimationPeriod
                        from c in cteAttributeCategory
                        from d in cteEstimationCellExtended
                        select new
                        {
                            Id = String.Format(
                                "{0}-{1}-{2}-{3}",
                                a.PhaseEstimateTypeId.ToString(),
                                b.EstimationPeriodId.ToString(),
                                c.VariableId.ToString(),
                                d.EstimationCellId.ToString()),
                            a.PhaseEstimateTypeId,
                            a.PhaseEstimateTypeLabelCs,
                            a.PhaseEstimateTypeLabelEn,
                            a.PhaseEstimateTypeDescriptionCs,
                            a.PhaseEstimateTypeDescriptionEn,
                            b.EstimationPeriodId,
                            b.EstimateDateBegin,
                            b.EstimateDateEnd,
                            b.EstimationPeriodLabelCs,
                            b.EstimationPeriodLabelEn,
                            b.EstimationPeriodDescriptionCs,
                            b.EstimationPeriodDescriptionEn,
                            c.VariableId,
                            c.VariableLabelCs,
                            c.VariableLabelEn,
                            d.EstimationCellId,
                            d.EstimationCellLabelCs,
                            d.EstimationCellLabelEn,
                            d.EstimationCellDescriptionCs,
                            d.EstimationCellDescriptionEn,
                            d.EstimationCellCollectionId,
                            d.EstimationCellCollectionLabelCs,
                            d.EstimationCellCollectionLabelEn,
                            d.EstimationCellCollectionDescriptionCs,
                            d.EstimationCellCollectionDescriptionEn
                        };
                    #endregion cteCartesianProduct: Kartézský součin dimenzí

                    #region Zápis výsledku do DataTable
                    if (cteCartesianProduct.Any())
                    {
                        Data =
                            cteCartesianProduct
                            .OrderBy(a => a.PhaseEstimateTypeId)
                            .ThenBy(a => a.EstimationPeriodId)
                            .ThenBy(a => a.VariableId)
                            .ThenBy(a => a.EstimationCellId)
                            .Select(a =>
                                Data.LoadDataRow(
                                    values:
                                    [
                                        a.Id,
                                        a.PhaseEstimateTypeId,
                                        a.PhaseEstimateTypeLabelCs,
                                        a.PhaseEstimateTypeLabelEn,
                                        a.EstimationPeriodId,
                                        a.EstimateDateBegin,
                                        a.EstimateDateEnd,
                                        a.EstimationPeriodLabelCs,
                                        a.EstimationPeriodLabelEn,
                                        a.VariableId,
                                        a.VariableLabelCs,
                                        a.VariableLabelEn,
                                        a.EstimationCellId,
                                        a.EstimationCellDescriptionCs,
                                        a.EstimationCellDescriptionEn,
                                        a.EstimationCellCollectionId,
                                        a.EstimationCellCollectionLabelCs,
                                        a.EstimationCellCollectionLabelEn
                                    ],
                                    fAcceptChanges: false))
                            .CopyToDataTable();
                        Data.TableName = Name;
                    }
                    #endregion Zápis výsledku do DataTable

                    #region Omezení podmínkou
                    if (!String.IsNullOrEmpty(value: Condition))
                    {
                        DataView view = new(table: Data);
                        try
                        {
                            view.RowFilter = Condition;
                        }
                        catch
                        {
                            view.RowFilter = String.Empty;
                            Condition = String.Empty;
                        }
                        Data = view.ToTable();
                    }
                    #endregion Omezení podmínkou

                    #region Omezení počtem
                    if (Limit != null)
                    {
                        Data = Data.AsEnumerable()
                            .Take(count: (int)Limit)
                            .CopyToDataTable();
                    }
                    #endregion  Omezení počtem

                    stopWatch.Stop();
                    LoadingTime = 0.001 * stopWatch.ElapsedMilliseconds;
                    Data.TableName = TableName;
                }

                #endregion Methods

            }

        }
    }
}


