﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {
            // fn_etl_get_target_variable

            /// <summary>
            /// Target variable for ETL
            /// (in the NfiEsta schema)
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class FnEtlGetTargetVariableType(
                FnEtlGetTargetVariableTypeList composite = null,
                DataRow data = null)
                    : AParametrizedViewEntry<FnEtlGetTargetVariableTypeList>(composite: composite, data: data),
                      ITargetVariable<FnEtlGetTargetVariableTypeList>
            {

                #region Derived Properties

                /// <summary>
                /// Target variable identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: FnEtlGetTargetVariableTypeList.ColId.Name,
                            defaultValue: Int32.Parse(s: FnEtlGetTargetVariableTypeList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: FnEtlGetTargetVariableTypeList.ColId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Metadata of the target variable (json format as text)
                /// </summary>
                public string MetadataText
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableTypeList.ColMetadata.Name,
                            defaultValue: FnEtlGetTargetVariableTypeList.ColMetadata.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: FnEtlGetTargetVariableTypeList.ColMetadata.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Metadata of the target variable (json format as object)
                /// </summary>
                public TargetVariableMetadataJson MetadataObject
                {
                    get
                    {
                        TargetVariableMetadataJson json = new();
                        json.LoadFromText(text: MetadataText);
                        return json;
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not FnEtlGetTargetVariableType Type, return False
                    if (obj is not FnEtlGetTargetVariableType)
                    {
                        return false;
                    }

                    return
                        Id == ((FnEtlGetTargetVariableType)obj).Id;
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                /// <summary>
                /// Text description of the object
                /// </summary>
                /// <returns>Text description of the objectt</returns>
                public override string ToString()
                {
                    return ((NfiEstaDB)Composite.Database).Postgres.Setting.LanguageVersion switch
                    {
                        LanguageVersion.National =>
                            String.Concat(
                                $"{"ID"} = {Functions.PrepIntArg(arg: Id)}.{Environment.NewLine}"
                                ),
                        LanguageVersion.International =>
                            String.Concat(
                                $"{"ID"} = {Functions.PrepIntArg(arg: Id)}.{Environment.NewLine}"
                                ),
                        _ =>
                            String.Concat(
                                $"{"ID"} = {Functions.PrepIntArg(arg: Id)}.{Environment.NewLine}"
                                ),
                    };
                }

                #endregion Methods

            }


            /// <summary>
            /// List of target variables for ETL
            /// (in the NfiEsta schema)
            /// </summary>
            public class FnEtlGetTargetVariableTypeList
                : AParametrizedView,
                  ITargetVariableList
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema =
                    NfiEstaFunctions.FnEtlGetTargetVariable.SchemaName;

                /// <summary>
                /// Table name
                /// </summary>
                public static readonly string Name =
                    NfiEstaFunctions.FnEtlGetTargetVariable.Name;

                /// <summary>
                /// Table alias
                /// </summary>
                public static readonly string Alias =
                    NfiEstaFunctions.FnEtlGetTargetVariable.Name;

                /// <summary>
                /// Description in national language
                /// </summary>
                public static readonly string CaptionCs =
                    "Cílová proměnná pro ETL ve schématu NfiEsta";

                /// <summary>
                /// Description in English
                /// </summary>
                public static readonly string CaptionEn =
                    "Target variable for ETL in the NfiEsta schema";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
                {
                    { "id", new ColumnMetadata()
                    {
                        Name = "id",
                        DbName = "id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = true,
                        NotNull = true,
                        DefaultValue = "0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ID",
                        HeaderTextEn = "ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 0
                    }
                    },
                    { "metadata", new ColumnMetadata()
                    {
                        Name = "metadata",
                        DbName = "metadata",
                        DataType = "System.String",
                        DbDataType = "json",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "METADATA",
                        HeaderTextEn = "METADATA",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 1
                    }
                    }
                };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column metadata
                /// </summary>
                public static readonly ColumnMetadata ColMetadata = Cols["metadata"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Private Fields

                /// <summary>
                /// First parameter - National language
                /// </summary>
                private Language nationalLanguage;

                /// <summary>
                /// Second parameter - List of etl identifiers
                /// </summary>
                private List<Nullable<int>> etlId;

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public FnEtlGetTargetVariableTypeList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                {
                    NationalLanguage = Language.CS;
                    EtlId = [];
                }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public FnEtlGetTargetVariableTypeList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                {
                    NationalLanguage = Language.CS;
                    EtlId = [];
                }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// First parameter - National language
                /// </summary>
                public Language NationalLanguage
                {
                    get
                    {
                        return nationalLanguage;
                    }
                    set
                    {
                        nationalLanguage = value;
                    }
                }

                /// <summary>
                /// Second parameter - List of etl identifiers
                /// </summary>
                public List<Nullable<int>> EtlId
                {
                    get
                    {
                        return etlId;
                    }
                    set
                    {
                        etlId = value;
                    }
                }


                /// <summary>
                /// List of target variables for ETL
                /// (in the NfiEsta schema)
                /// </summary>
                public List<ITargetVariable> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new FnEtlGetTargetVariableType(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// Target variable for ETL from list by identifier (read-only)
                /// </summary>
                /// <param name="id">Target variable for ETL identifier</param>
                /// <returns>Target variable for ETL from list by identifier (null if not found)</returns>
                public ITargetVariable this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new FnEtlGetTargetVariableType(composite: this, data: a))
                                .FirstOrDefault<ITargetVariable>();
                    }
                }

                #endregion Indexer


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public FnEtlGetTargetVariableTypeList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new FnEtlGetTargetVariableTypeList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime)
                        {
                            NationalLanguage = NationalLanguage,
                            EtlId = [.. EtlId]
                        } :
                        new FnEtlGetTargetVariableTypeList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null)
                        {
                            NationalLanguage = NationalLanguage,
                            EtlId = [.. EtlId]
                        };
                }

                /// <summary>
                /// Create new empty target variable table
                /// </summary>
                /// <returns>New empty target variable table</returns>
                public ITargetVariableList Empty()
                {
                    return new FnEtlGetTargetVariableTypeList(database: ((NfiEstaDB)Database));
                }

                #endregion Methods

            }

        }
    }
}
