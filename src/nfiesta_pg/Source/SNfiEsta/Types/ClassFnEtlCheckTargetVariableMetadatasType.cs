﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {
            // fn_etl_check_target_variable_metadatas

            /// <summary>
            /// Class for fn_etl_check_target_variable_metadatas.
            /// Function returns records of target variables for given input arguments.
            /// If input argument _metadata_diff = true
            /// then function returns records of target variables that their metadatas are not different.
            /// If input argument _metadata_diff = false
            /// then function returns records of target variables that their metadatas are different.
            /// If input argument _metadata_diff is null (_metadata is true or false)
            /// then function returns both cases.
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class FnEtlCheckTargetVariableMetadatasType(
                FnEtlCheckTargetVariableMetadatasTypeList composite = null,
                DataRow data = null)
                    : AParametrizedViewEntry<FnEtlCheckTargetVariableMetadatasTypeList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Identifier
                /// </summary>
                public new string Id
                {
                    get
                    {
                        return $"{TargetVariableSourceId}-{TargetVariableTargetId}";
                    }
                }

                /// <summary>
                /// Id of target variable in the source database
                /// </summary>
                public int TargetVariableSourceId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: FnEtlCheckTargetVariableMetadatasTypeList.ColTargetVariableSourceId.Name,
                            defaultValue: Int32.Parse(s: FnEtlCheckTargetVariableMetadatasTypeList.ColTargetVariableSourceId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: FnEtlCheckTargetVariableMetadatasTypeList.ColTargetVariableSourceId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Id of target variable in the target database
                /// </summary>
                public int TargetVariableTargetId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: FnEtlCheckTargetVariableMetadatasTypeList.ColTargetVariableTargetId.Name,
                            defaultValue: Int32.Parse(FnEtlCheckTargetVariableMetadatasTypeList.ColTargetVariableTargetId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: FnEtlCheckTargetVariableMetadatasTypeList.ColTargetVariableTargetId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Target variable metadata in the source database
                /// </summary>
                public string MetadataSource
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: FnEtlCheckTargetVariableMetadatasTypeList.ColMetadataSource.Name,
                            defaultValue: FnEtlCheckTargetVariableMetadatasTypeList.ColMetadataSource.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: FnEtlCheckTargetVariableMetadatasTypeList.ColMetadataSource.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Target variable metadata in the target database
                /// </summary>
                public string MetadataTarget
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: FnEtlCheckTargetVariableMetadatasTypeList.ColMetadataTarget.Name,
                            defaultValue: FnEtlCheckTargetVariableMetadatasTypeList.ColMetadataTarget.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: FnEtlCheckTargetVariableMetadatasTypeList.ColMetadataTarget.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Does the metadata between the source and the target database differ?
                /// </summary>
                public Nullable<bool> MetadataDiff
                {
                    get
                    {
                        return Functions.GetNBoolArg(
                            row: Data,
                            name: FnEtlCheckTargetVariableMetadatasTypeList.ColMetadataDiff.Name,
                            defaultValue:
                                String.IsNullOrEmpty(value: FnEtlCheckTargetVariableMetadatasTypeList.ColMetadataDiff.DefaultValue) ?
                                    (Nullable<bool>)null :
                                    Boolean.Parse(value: FnEtlCheckTargetVariableMetadatasTypeList.ColMetadataDiff.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNBoolArg(
                            row: Data,
                            name: FnEtlCheckTargetVariableMetadatasTypeList.ColMetadataDiff.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Does the national language metadata between the source and the target database differ?
                /// </summary>
                public Nullable<bool> MetadataDiffNationalLanguage
                {
                    get
                    {
                        return Functions.GetNBoolArg(
                            row: Data,
                            name: FnEtlCheckTargetVariableMetadatasTypeList.ColMetadataDiffNationalLanguage.Name,
                            defaultValue:
                                String.IsNullOrEmpty(value: FnEtlCheckTargetVariableMetadatasTypeList.ColMetadataDiffNationalLanguage.DefaultValue) ?
                                    (Nullable<bool>)null :
                                    Boolean.Parse(value: FnEtlCheckTargetVariableMetadatasTypeList.ColMetadataDiffNationalLanguage.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNBoolArg(
                            row: Data,
                            name: FnEtlCheckTargetVariableMetadatasTypeList.ColMetadataDiffNationalLanguage.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Does the english language metadata between the source and the target database differ?
                /// </summary>
                public Nullable<bool> MetadataDiffEnglishLanguage
                {
                    get
                    {
                        return Functions.GetNBoolArg(
                            row: Data,
                            name: FnEtlCheckTargetVariableMetadatasTypeList.ColMetadataDiffEnglishLanguage.Name,
                            defaultValue:
                                String.IsNullOrEmpty(value: FnEtlCheckTargetVariableMetadatasTypeList.ColMetadataDiffEnglishLanguage.DefaultValue) ?
                                    (Nullable<bool>)null :
                                    Boolean.Parse(value: FnEtlCheckTargetVariableMetadatasTypeList.ColMetadataDiffEnglishLanguage.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNBoolArg(
                            row: Data,
                            name: FnEtlCheckTargetVariableMetadatasTypeList.ColMetadataDiffEnglishLanguage.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Is the national language metadata missing in the target database?
                /// </summary>
                public Nullable<bool> MissingMetadataNationalLanguage
                {
                    get
                    {
                        return Functions.GetNBoolArg(
                            row: Data,
                            name: FnEtlCheckTargetVariableMetadatasTypeList.ColMissingMetadataNationalLanguage.Name,
                            defaultValue:
                                String.IsNullOrEmpty(value: FnEtlCheckTargetVariableMetadatasTypeList.ColMissingMetadataNationalLanguage.DefaultValue) ?
                                    (Nullable<bool>)null :
                                    Boolean.Parse(value: FnEtlCheckTargetVariableMetadatasTypeList.ColMissingMetadataNationalLanguage.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNBoolArg(
                            row: Data,
                            name: FnEtlCheckTargetVariableMetadatasTypeList.ColMissingMetadataNationalLanguage.Name,
                            val: value);
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not FnEtlCheckTargetVariableMetadatasType Type, return False
                    if (obj is not FnEtlCheckTargetVariableMetadatasType)
                    {
                        return false;
                    }

                    return
                        Id == ((FnEtlCheckTargetVariableMetadatasType)obj).Id;
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                /// <summary>
                /// Text description of the object
                /// </summary>
                /// <returns>Text description of the objectt</returns>
                public override string ToString()
                {
                    return ((NfiEstaDB)Composite.Database).Postgres.Setting.LanguageVersion switch
                    {
                        LanguageVersion.National =>
                            String.Concat(
                                $"{"ID"} = {Functions.PrepStringArg(arg: Id)}.{Environment.NewLine}"
                                ),
                        LanguageVersion.International =>
                            String.Concat(
                                $"{"ID"} = {Functions.PrepStringArg(arg: Id)}.{Environment.NewLine}"
                                ),
                        _ =>
                            String.Concat(
                                $"{"ID"} = {Functions.PrepStringArg(arg: Id)}.{Environment.NewLine}"
                                ),
                    };
                }

                #endregion Methods

            }


            /// <summary>
            /// List with records of target variables
            /// </summary>
            public class FnEtlCheckTargetVariableMetadatasTypeList
                : AParametrizedView
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema =
                    NfiEstaFunctions.FnEtlCheckTargetVariableMetadatas.SchemaName;

                /// <summary>
                /// Table name
                /// </summary>
                public static readonly string Name =
                    NfiEstaFunctions.FnEtlCheckTargetVariableMetadatas.Name;

                /// <summary>
                /// Table alias
                /// </summary>
                public static readonly string Alias =
                    NfiEstaFunctions.FnEtlCheckTargetVariableMetadatas.Name;

                /// <summary>
                /// Description in national language
                /// </summary>
                public static readonly string CaptionCs =
                    "Záznam cílové proměnné pro dané vstupní argumenty";

                /// <summary>
                /// Description in English
                /// </summary>
                public static readonly string CaptionEn =
                    "Target variable record for given input arguments";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
                {
                    { "target_variable_source", new ColumnMetadata()
                    {
                        Name = "target_variable_source",
                        DbName = "target_variable_source",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = "0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "TARGET_VARIABLE_SOURCE",
                        HeaderTextEn = "TARGET_VARIABLE_SOURCE",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 0
                    }
                    },
                    { "target_variable_target", new ColumnMetadata()
                    {
                        Name = "target_variable_target",
                        DbName = "target_variable_target",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = "0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "TARGET_VARIABLE_TARGET",
                        HeaderTextEn = "TARGET_VARIABLE_TARGET",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 1
                    }
                    },
                    { "metadata_source", new ColumnMetadata()
                    {
                        Name = "metadata_source",
                        DbName = "metadata_source",
                        DataType = "System.String",
                        DbDataType = "json",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "Cílová proměnná",
                        HeaderTextEn = "Target variable",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 2
                    }
                    },
                    { "metadata_target", new ColumnMetadata()
                    {
                        Name = "metadata_target",
                        DbName = "metadata_target",
                        DataType = "System.String",
                        DbDataType = "json",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "METADATA_TARGET",
                        HeaderTextEn = "METADATA_TARGET",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 3
                    }
                    },
                    { "metadata_diff", new ColumnMetadata()
                    {
                        Name = "metadata_diff",
                        DbName = "metadata_diff",
                        DataType = "System.Boolean",
                        DbDataType = "boolean",
                        NewDataType = "boolean",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "METADATA_DIFF",
                        HeaderTextEn = "METADATA_DIFF",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 4
                    }
                    },
                    { "metadata_diff_national_language", new ColumnMetadata()
                    {
                        Name = "metadata_diff_national_language",
                        DbName = "metadata_diff_national_language",
                        DataType = "System.Boolean",
                        DbDataType = "boolean",
                        NewDataType = "boolean",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "Liší se v národní verzi?",
                        HeaderTextEn = "Does the national version differ?",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 5
                    }
                    },
                    { "metadata_diff_english_language", new ColumnMetadata()
                    {
                        Name = "metadata_diff_english_language",
                        DbName = "metadata_diff_english_language",
                        DataType = "System.Boolean",
                        DbDataType = "boolean",
                        NewDataType = "boolean",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "Liší se v anglické verzi?",
                        HeaderTextEn = "Does the English version differ?",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 6
                    }
                    },
                    { "missing_metadata_national_language", new ColumnMetadata()
                    {
                        Name = "missing_metadata_national_language",
                        DbName = "missing_metadata_national_language",
                        DataType = "System.Boolean",
                        DbDataType = "boolean",
                        NewDataType = "boolean",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "Chybí národní verze v modulu Odhady?",
                        HeaderTextEn = "Is the national version missing in the Estimates module?",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 7
                    }
                    }
                };

                /// <summary>
                /// Column target variable id in the source database
                /// </summary>
                public static readonly ColumnMetadata ColTargetVariableSourceId = Cols["target_variable_source"];

                /// <summary>
                /// Column target variable id in the target database
                /// </summary>
                public static readonly ColumnMetadata ColTargetVariableTargetId = Cols["target_variable_target"];

                /// <summary>
                /// Column target variable metadata in the source database
                /// </summary>
                public static readonly ColumnMetadata ColMetadataSource = Cols["metadata_source"];

                /// <summary>
                /// Column target variable metadata in the target database
                /// </summary>
                public static readonly ColumnMetadata ColMetadataTarget = Cols["metadata_target"];

                /// <summary>
                /// Column Does the metadata differ?
                /// </summary>
                public static readonly ColumnMetadata ColMetadataDiff = Cols["metadata_diff"];

                /// <summary>
                /// Column Does the metadata differ in the national language?
                /// </summary>
                public static readonly ColumnMetadata ColMetadataDiffNationalLanguage = Cols["metadata_diff_national_language"];

                /// <summary>
                /// Column Does the metadata differ in the English language?
                /// </summary>
                public static readonly ColumnMetadata ColMetadataDiffEnglishLanguage = Cols["metadata_diff_english_language"];

                /// <summary>
                /// Column Is the metadata missing in the national language?
                /// </summary>
                public static readonly ColumnMetadata ColMissingMetadataNationalLanguage = Cols["missing_metadata_national_language"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Private Fields

                /// <summary>
                /// First parameter
                /// </summary>
                private string metadatas;

                /// <summary>
                /// Second parameter
                /// </summary>
                private Language nationalLanguage;

                /// <summary>
                /// Third parameter
                /// </summary>
                private Nullable<bool> metadataDiff;

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public FnEtlCheckTargetVariableMetadatasTypeList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                {
                    Metadatas = String.Empty;
                    NationalLanguage = Language.CS;
                    MetadataDiff = null;
                }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public FnEtlCheckTargetVariableMetadatasTypeList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                {
                    Metadatas = String.Empty;
                    NationalLanguage = Language.CS;
                    MetadataDiff = null;
                }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// First parameter
                /// </summary>
                public string Metadatas
                {
                    get
                    {
                        return metadatas;
                    }
                    set
                    {
                        metadatas = value;
                    }
                }

                /// <summary>
                /// Second parameter
                /// </summary>
                public Language NationalLanguage
                {
                    get
                    {
                        return nationalLanguage;
                    }
                    set
                    {
                        nationalLanguage = value;
                    }
                }

                /// <summary>
                /// Third parameter
                /// </summary>
                public Nullable<bool> MetadataDiff
                {
                    get
                    {
                        return metadataDiff;
                    }
                    set
                    {
                        metadataDiff = value;
                    }
                }

                /// <summary>
                /// List with records of target variables (read-only)
                /// </summary>
                public List<FnEtlCheckTargetVariableMetadatasType> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new FnEtlCheckTargetVariableMetadatasType(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// Target variable from list by identifier (read-only)
                /// </summary>
                /// <param name="id">Target variable identifier</param>
                /// <returns>Target variablefrom list by identifier (null if not found)</returns>
                public FnEtlCheckTargetVariableMetadatasType this[string id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => $"{a.Field<int>(ColTargetVariableSourceId.Name)}-{a.Field<int>(ColTargetVariableTargetId.Name)}" == id)
                                .Select(a => new FnEtlCheckTargetVariableMetadatasType(composite: this, data: a))
                                .FirstOrDefault<FnEtlCheckTargetVariableMetadatasType>();
                    }
                }

                #endregion Indexer


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public FnEtlCheckTargetVariableMetadatasTypeList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new FnEtlCheckTargetVariableMetadatasTypeList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime)
                        {
                            Metadatas = Metadatas,
                            NationalLanguage = NationalLanguage,
                            MetadataDiff = MetadataDiff
                        } :
                        new FnEtlCheckTargetVariableMetadatasTypeList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null)
                        {
                            Metadatas = Metadatas,
                            NationalLanguage = NationalLanguage,
                            MetadataDiff = MetadataDiff
                        };
                }

                #endregion Methods

            }

        }
    }
}