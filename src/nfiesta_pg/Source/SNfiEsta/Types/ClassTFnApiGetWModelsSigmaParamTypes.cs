﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {
            // fn_api_get_wmodels_sigma_paramtypes

            /// <summary>
            /// Alternative combinations of working model, sigma and parametrization area type
            /// (return type of the stored procedure fn_api_get_wmodels_sigma_paramtypes)
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class TFnApiGetWModelsSigmaParamTypes(
                TFnApiGetWModelsSigmaParamTypesList composite = null,
                DataRow data = null)
                    : AParametrizedViewEntry<TFnApiGetWModelsSigmaParamTypesList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TFnApiGetWModelsSigmaParamTypesList.ColId.Name,
                            defaultValue: Int32.Parse(s: TFnApiGetWModelsSigmaParamTypesList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TFnApiGetWModelsSigmaParamTypesList.ColId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Working model identifier
                /// </summary>
                public Nullable<int> WorkingModelId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TFnApiGetWModelsSigmaParamTypesList.ColWorkingModelId.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnApiGetWModelsSigmaParamTypesList.ColWorkingModelId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TFnApiGetWModelsSigmaParamTypesList.ColWorkingModelId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TFnApiGetWModelsSigmaParamTypesList.ColWorkingModelId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Working model label in national language
                /// </summary>
                public string WorkingModelLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetWModelsSigmaParamTypesList.ColWorkingModelLabelCs.Name,
                            defaultValue: TFnApiGetWModelsSigmaParamTypesList.ColWorkingModelLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetWModelsSigmaParamTypesList.ColWorkingModelLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Working model description in national language
                /// </summary>
                public string WorkingModelDescriptionCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetWModelsSigmaParamTypesList.ColWorkingModelDescriptionCs.Name,
                            defaultValue: TFnApiGetWModelsSigmaParamTypesList.ColWorkingModelDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetWModelsSigmaParamTypesList.ColWorkingModelDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Working model extended label in national language (read-only)
                /// </summary>
                public string WorkingModelExtendedLabelCs
                {
                    get
                    {
                        return $"{Id} - {WorkingModelLabelCs}";
                    }
                }

                /// <summary>
                /// Working model label in English
                /// </summary>
                public string WorkingModelLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetWModelsSigmaParamTypesList.ColWorkingModelLabelEn.Name,
                            defaultValue: TFnApiGetWModelsSigmaParamTypesList.ColWorkingModelLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetWModelsSigmaParamTypesList.ColWorkingModelLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Working model description in English
                /// </summary>
                public string WorkingModelDescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetWModelsSigmaParamTypesList.ColWorkingModelDescriptionEn.Name,
                            defaultValue: TFnApiGetWModelsSigmaParamTypesList.ColWorkingModelDescriptionEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetWModelsSigmaParamTypesList.ColWorkingModelDescriptionEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Working model extended label in English (read-only)
                /// </summary>
                public string WorkingModelExtendedLabelEn
                {
                    get
                    {
                        return $"{Id} - {WorkingModelLabelEn}";
                    }
                }

                /// <summary>
                /// Sigma
                /// </summary>
                public Nullable<bool> Sigma
                {
                    get
                    {
                        return Functions.GetNBoolArg(
                            row: Data,
                            name: TFnApiGetWModelsSigmaParamTypesList.ColSigma.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnApiGetWModelsSigmaParamTypesList.ColSigma.DefaultValue) ?
                                (Nullable<bool>)null :
                                Boolean.Parse(value: TFnApiGetWModelsSigmaParamTypesList.ColSigma.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNBoolArg(
                            row: Data,
                            name: TFnApiGetWModelsSigmaParamTypesList.ColSigma.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Parametrization area type identifier
                /// </summary>
                public Nullable<int> ParamAreaTypeId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TFnApiGetWModelsSigmaParamTypesList.ColParamAreaTypeId.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnApiGetWModelsSigmaParamTypesList.ColParamAreaTypeId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TFnApiGetWModelsSigmaParamTypesList.ColParamAreaTypeId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TFnApiGetWModelsSigmaParamTypesList.ColParamAreaTypeId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Parametrization area type label in national language
                /// </summary>
                public string ParamAreaTypeLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetWModelsSigmaParamTypesList.ColParamAreaTypeLabelCs.Name,
                            defaultValue: TFnApiGetWModelsSigmaParamTypesList.ColParamAreaTypeLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetWModelsSigmaParamTypesList.ColParamAreaTypeLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Parametrization area type description in national language
                /// </summary>
                public string ParamAreaTypeDescriptionCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetWModelsSigmaParamTypesList.ColParamAreaTypeDescriptionCs.Name,
                            defaultValue: TFnApiGetWModelsSigmaParamTypesList.ColParamAreaTypeDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetWModelsSigmaParamTypesList.ColParamAreaTypeDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Parametrization area type extended label in national language (read-only)
                /// </summary>
                public string ParamAreaTypeExtendedLabelCs
                {
                    get
                    {
                        return $"{Id} - {ParamAreaTypeLabelCs}";
                    }
                }

                /// <summary>
                /// Parametrization area type label in English
                /// </summary>
                public string ParamAreaTypeLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetWModelsSigmaParamTypesList.ColParamAreaTypeLabelEn.Name,
                            defaultValue: TFnApiGetWModelsSigmaParamTypesList.ColParamAreaTypeLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetWModelsSigmaParamTypesList.ColParamAreaTypeLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Parametrization area type description in English
                /// </summary>
                public string ParamAreaTypeDescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetWModelsSigmaParamTypesList.ColParamAreaTypeDescriptionEn.Name,
                            defaultValue: TFnApiGetWModelsSigmaParamTypesList.ColParamAreaTypeDescriptionEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetWModelsSigmaParamTypesList.ColParamAreaTypeDescriptionEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Parametrization area type extended label in English (read-only)
                /// </summary>
                public string ParamAreaTypeExtendedLabelEn
                {
                    get
                    {
                        return $"{Id} - {ParamAreaTypeLabelEn}";
                    }
                }


                /// <summary>
                /// Number of estimation cells covered
                /// </summary>
                public Nullable<long> NoOfCellsCovered
                {
                    get
                    {
                        return Functions.GetNLongArg(
                            row: Data,
                            name: TFnApiGetWModelsSigmaParamTypesList.ColNoOfCellsCovered.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnApiGetWModelsSigmaParamTypesList.ColNoOfCellsCovered.DefaultValue) ?
                                (Nullable<long>)null :
                                Int64.Parse(s: TFnApiGetWModelsSigmaParamTypesList.ColNoOfCellsCovered.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNLongArg(
                            row: Data,
                            name: TFnApiGetWModelsSigmaParamTypesList.ColNoOfCellsCovered.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// All cells included
                /// </summary>
                public Nullable<bool> AllCellsIncluded
                {
                    get
                    {
                        return Functions.GetNBoolArg(
                            row: Data,
                            name: TFnApiGetWModelsSigmaParamTypesList.ColAllCellsIncluded.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnApiGetWModelsSigmaParamTypesList.ColAllCellsIncluded.DefaultValue) ?
                                (Nullable<bool>)null :
                                Boolean.Parse(value: TFnApiGetWModelsSigmaParamTypesList.ColAllCellsIncluded.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNBoolArg(
                            row: Data,
                            name: TFnApiGetWModelsSigmaParamTypesList.ColAllCellsIncluded.Name,
                            val: value);
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not TFnApiGetWModelsSigmaParamTypes, return False
                    if (obj is not TFnApiGetWModelsSigmaParamTypes)
                    {
                        return false;
                    }

                    return
                        Id == ((TFnApiGetWModelsSigmaParamTypes)obj).Id;
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                /// <summary>
                /// Text description of the object
                /// </summary>
                /// <returns>Text description of the object</returns>
                public override string ToString()
                {
                    return ((NfiEstaDB)Composite.Database).Postgres.Setting.LanguageVersion switch
                    {
                        LanguageVersion.International =>
                            String.Concat($"{"ID"} = {Functions.PrepIntArg(arg: Id)}.{Environment.NewLine}"),
                        LanguageVersion.National =>
                            String.Concat($"{"ID"} = {Functions.PrepIntArg(arg: Id)}.{Environment.NewLine}"),
                        _ =>
                            String.Concat($"{"ID"} = {Functions.PrepIntArg(arg: Id)}.{Environment.NewLine}"),
                    };
                }

                #endregion Methods

            }


            /// <summary>
            /// List of alternative combinations of working model, sigma and parametrization area type
            /// (return type of the stored procedure fn_api_get_wmodels_sigma_paramtypes)
            /// </summary>
            public class TFnApiGetWModelsSigmaParamTypesList
                 : AParametrizedView
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema =
                    NfiEstaFunctions.FnApiGetWModelsSigmaParamTypes.SchemaName;

                /// <summary>
                /// Stored procedure name
                /// </summary>
                public static readonly string Name =
                    NfiEstaFunctions.FnApiGetWModelsSigmaParamTypes.Name;

                /// <summary>
                /// Stored procedure alias
                /// </summary>
                public static readonly string Alias =
                    NfiEstaFunctions.FnApiGetWModelsSigmaParamTypes.Name;

                /// <summary>
                /// Description in national language
                /// </summary>
                public static readonly string CaptionCs = String.Concat(
                    "Seznam dostupných kombinací pracovního modelu, sigma a typu parametrizační oblasti ",
                    "(návratový typ uložené procedury fn_api_get_wmodels_sigma_paramtypes)");

                /// <summary>
                /// Description in English
                /// </summary>
                public static readonly string CaptionEn = String.Concat(
                    "List of alternative combinations of working model, sigma and parametrization area type ",
                    "(return type of the stored procedure fn_api_get_wmodels_sigma_paramtypes)");

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
                {
                    { "id", new ColumnMetadata()
                    {
                        Name = "id",
                        DbName = "id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = true,
                        NotNull = true,
                        DefaultValue = "0",
                        FuncCall = "row_number() over()",
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ID",
                        HeaderTextEn = "ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 0
                    }
                    },
                    { "working_model_id", new ColumnMetadata()
                    {
                        Name = "working_model_id",
                        DbName = "working_model_id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "WORKING_MODEL_ID",
                        HeaderTextEn = "WORKING_MODEL_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 1
                    }
                    },
                    { "working_model_label_cs", new ColumnMetadata()
                    {
                        Name = "working_model_label_cs",
                        DbName = "working_model_label",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "WORKING_MODEL_LABEL_CS",
                        HeaderTextEn = "WORKING_MODEL_LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 2
                    }
                    },
                    { "working_model_description_cs", new ColumnMetadata()
                    {
                        Name = "working_model_description_cs",
                        DbName = "working_model_description",
                        DataType = "System.String",
                        DbDataType = "text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "WORKING_MODEL_DESCRIPTION_CS",
                        HeaderTextEn = "WORKING_MODEL_DESCRIPTION_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 3
                    }
                    },
                    { "working_model_label_en", new ColumnMetadata()
                    {
                        Name = "working_model_label_en",
                        DbName = "working_model_label_en",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "WORKING_MODEL_LABEL_EN",
                        HeaderTextEn = "WORKING_MODEL_LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 4
                    }
                    },
                    { "working_model_description_en", new ColumnMetadata()
                    {
                        Name = "working_model_description_en",
                        DbName = "working_model_description_en",
                        DataType = "System.String",
                        DbDataType = "text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "WORKING_MODEL_DESCRIPTION_EN",
                        HeaderTextEn = "WORKING_MODEL_DESCRIPTION_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 5
                    }
                    },
                    { "sigma", new ColumnMetadata()
                    {
                        Name = "sigma",
                        DbName = "sigma",
                        DataType = "System.Boolean",
                        DbDataType = "bool",
                        NewDataType = "bool",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "SIGMA",
                        HeaderTextEn = "SIGMA",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 6
                    }
                    },
                    { "param_area_type_id", new ColumnMetadata()
                    {
                        Name = "param_area_type_id",
                        DbName = "param_area_type_id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PARAM_AREA_TYPE_ID",
                        HeaderTextEn = "PARAM_AREA_TYPE_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 7
                    }
                    },
                    { "param_area_type_label_cs", new ColumnMetadata()
                    {
                        Name = "param_area_type_label_cs",
                        DbName = "param_area_type_label",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PARAM_AREA_TYPE_LABEL_CS",
                        HeaderTextEn = "PARAM_AREA_TYPE_LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 8
                    }
                    },
                    { "param_area_type_description_cs", new ColumnMetadata()
                    {
                        Name = "param_area_type_description_cs",
                        DbName = "param_area_type_description",
                        DataType = "System.String",
                        DbDataType = "text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PARAM_AREA_TYPE_DESCRIPTION_CS",
                        HeaderTextEn = "PARAM_AREA_TYPE_DESCRIPTION_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 9
                    }
                    },
                    { "param_area_type_label_en", new ColumnMetadata()
                    {
                        Name = "param_area_type_label_en",
                        DbName = "param_area_type_label_en",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PARAM_AREA_TYPE_LABEL_EN",
                        HeaderTextEn = "PARAM_AREA_TYPE_LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 10
                    }
                    },
                    { "param_area_type_description_en", new ColumnMetadata()
                    {
                        Name = "param_area_type_description_en",
                        DbName = "param_area_type_descriptipn_en",
                        DataType = "System.String",
                        DbDataType = "text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PARAM_AREA_TYPE_DESCRIPTION_EN",
                        HeaderTextEn = "PARAM_AREA_TYPE_DESCRIPTION_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 11
                    }
                    },
                    { "no_of_cells_covered", new ColumnMetadata()
                    {
                        Name = "no_of_cells_covered",
                        DbName = "no_of_cells_covered",
                        DataType = "System.Int64",
                        DbDataType = "int8",
                        NewDataType = "int8",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "NO_OF_CELLS_COVERED",
                        HeaderTextEn = "NO_OF_CELLS_COVERED",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 12
                    }
                    },
                    { "all_cells_included", new ColumnMetadata()
                    {
                        Name = "all_cells_included",
                        DbName = "all_cells_included",
                        DataType = "System.Boolean",
                        DbDataType = "bool",
                        NewDataType = "bool",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ALL_CELLS_INCLUDED",
                        HeaderTextEn = "ALL_CELLS_INCLUDED",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 13
                    }
                    }
                };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column working_model_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColWorkingModelId = Cols["working_model_id"];

                /// <summary>
                /// Column working_model_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColWorkingModelLabelCs = Cols["working_model_label_cs"];

                /// <summary>
                /// Column working_model_description_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColWorkingModelDescriptionCs = Cols["working_model_description_cs"];

                /// <summary>
                /// Column working_model_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColWorkingModelLabelEn = Cols["working_model_label_en"];

                /// <summary>
                /// Column working_model_description_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColWorkingModelDescriptionEn = Cols["working_model_description_en"];

                /// <summary>
                /// Column sigma metadata
                /// </summary>
                public static readonly ColumnMetadata ColSigma = Cols["sigma"];

                /// <summary>
                /// Column param_area_type_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColParamAreaTypeId = Cols["param_area_type_id"];

                /// <summary>
                /// Column param_area_type_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColParamAreaTypeLabelCs = Cols["param_area_type_label_cs"];

                /// <summary>
                /// Column param_area_type_description_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColParamAreaTypeDescriptionCs = Cols["param_area_type_description_cs"];

                /// <summary>
                /// Column param_area_type_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColParamAreaTypeLabelEn = Cols["param_area_type_label_en"];

                /// <summary>
                /// Column param_area_type_description_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColParamAreaTypeDescriptionEn = Cols["param_area_type_description_en"];

                /// <summary>
                /// Column no_of_cells_covered metadata
                /// </summary>
                public static readonly ColumnMetadata ColNoOfCellsCovered = Cols["no_of_cells_covered"];

                /// <summary>
                /// Column all_cells_included metadata
                /// </summary>
                public static readonly ColumnMetadata ColAllCellsIncluded = Cols["all_cells_included"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Private Fields

                /// <summary>
                /// 1st parameter: List of panels identifiers
                /// </summary>
                private List<Nullable<int>> panelsIds;

                /// <summary>
                /// 2nd parameter: List of estimation cell identifiers
                /// </summary>
                private List<Nullable<int>> estimationCellsIds;

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TFnApiGetWModelsSigmaParamTypesList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                {
                    PanelsIds = [];
                    EstimationCellsIds = [];
                }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TFnApiGetWModelsSigmaParamTypesList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                {
                    PanelsIds = [];
                    EstimationCellsIds = [];
                }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// 1st parameter: List of panels identifiers
                /// </summary>
                public List<Nullable<int>> PanelsIds
                {
                    get
                    {
                        return panelsIds ?? [];
                    }
                    set
                    {
                        panelsIds = value ?? [];
                    }
                }

                /// <summary>
                /// 2nd parameter: List of estimation cell identifiers
                /// </summary>
                public List<Nullable<int>> EstimationCellsIds
                {
                    get
                    {
                        return estimationCellsIds ?? [];
                    }
                    set
                    {
                        estimationCellsIds = value ?? [];
                    }
                }

                /// <summary>
                /// List of items (read-only)
                /// </summary>
                public List<TFnApiGetWModelsSigmaParamTypes> Items
                {
                    get
                    {
                        return
                            [..
                                Data.AsEnumerable()
                                .Select(a => new TFnApiGetWModelsSigmaParamTypes(composite: this, data: a))
                            ];
                    }
                }

                #endregion Properties


                #region Indexers

                /// <summary>
                /// Row from list of rows by identifier (read-only)
                /// </summary>
                /// <param name="id">Row identifier</param>
                /// <returns>Row from list of rows by identifier (null if not found)</returns>
                public TFnApiGetWModelsSigmaParamTypes this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new TFnApiGetWModelsSigmaParamTypes(composite: this, data: a))
                                .FirstOrDefault<TFnApiGetWModelsSigmaParamTypes>();
                    }
                }

                #endregion Indexers


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public TFnApiGetWModelsSigmaParamTypesList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any()
                        ? new TFnApiGetWModelsSigmaParamTypesList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime)
                        {
                            PanelsIds = [.. PanelsIds],
                            EstimationCellsIds = [.. EstimationCellsIds]
                        }
                        : new TFnApiGetWModelsSigmaParamTypesList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null)
                        {
                            PanelsIds = [.. PanelsIds],
                            EstimationCellsIds = [.. EstimationCellsIds]
                        };
                }

                #endregion Methods

            }

        }
    }
}