﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {
            // fn_get_estimation_cells_number_in_collection

            /// <summary>
            /// Number of estimation cells for each estimation cell collection
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class TFnGetEstimationCellsNumberInCollection(
                TFnGetEstimationCellsNumberInCollectionList composite = null,
                DataRow data = null)
                    : ADataTableEntry<TFnGetEstimationCellsNumberInCollectionList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TFnGetEstimationCellsNumberInCollectionList.ColId.Name,
                            defaultValue: Int32.Parse(s: TFnGetEstimationCellsNumberInCollectionList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TFnGetEstimationCellsNumberInCollectionList.ColId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Estimation cell collection identifier
                /// </summary>
                public int EstimationCellCollectionId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TFnGetEstimationCellsNumberInCollectionList.ColEstimationCellCollectionId.Name,
                            defaultValue: Int32.Parse(s: TFnGetEstimationCellsNumberInCollectionList.ColEstimationCellCollectionId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TFnGetEstimationCellsNumberInCollectionList.ColEstimationCellCollectionId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Estimation cell collection object (read-only)
                /// </summary>
                public EstimationCellCollection EstimationCellCollection
                {
                    get
                    {
                        return
                            ((NfiEstaDB)Composite.Database).SNfiEsta
                            .CEstimationCellCollection[EstimationCellCollectionId];
                    }
                }

                /// <summary>
                /// Number of estimation cells in estimation cell collection
                /// </summary>
                public long EstimationCellsNumber
                {
                    get
                    {
                        return Functions.GetLongArg(
                            row: Data,
                            name: TFnGetEstimationCellsNumberInCollectionList.ColEstimationCellsNumber.Name,
                            defaultValue: Int64.Parse(s: TFnGetEstimationCellsNumberInCollectionList.ColEstimationCellsNumber.DefaultValue));
                    }
                    set
                    {
                        Functions.SetLongArg(
                            row: Data,
                            name: TFnGetEstimationCellsNumberInCollectionList.ColEstimationCellsNumber.Name,
                            val: value);
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not VwEstimationCellsCount Type, return False
                    if (obj is not TFnGetEstimationCellsNumberInCollection)
                    {
                        return false;
                    }

                    return
                        Id == ((TFnGetEstimationCellsNumberInCollection)obj).Id;
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                /// <summary>
                /// Object text representation
                /// </summary>
                /// <returns>Object text representation</returns>
                public override string ToString()
                {
                    return ((NfiEstaDB)Composite.Database).Postgres.Setting.LanguageVersion switch
                    {
                        LanguageVersion.International =>
                            String.Concat(
                                $"{nameof(Id)} = {Functions.PrepNIntArg(arg: Id)}.{Environment.NewLine}"),

                        LanguageVersion.National =>
                            String.Concat(
                                $"{nameof(Id)} = {Functions.PrepNIntArg(arg: Id)}.{Environment.NewLine}"),

                        _ =>
                            String.Concat(
                                $"{nameof(Id)} = {Functions.PrepNIntArg(arg: Id)}.{Environment.NewLine}"),
                    };
                }

                #endregion Methods

            }


            /// <summary>
            /// List of numbers of estimation cells for each estimation cell collection
            /// </summary>
            public class TFnGetEstimationCellsNumberInCollectionList
                : ADataTable
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema =
                    NfiEstaFunctions.FnGetEstimationCellsNumberInCollection.SchemaName;

                /// <summary>
                /// Data view name
                /// </summary>
                public static readonly string Name =
                    NfiEstaFunctions.FnGetEstimationCellsNumberInCollection.Name;

                /// <summary>
                /// Data view alias
                /// </summary>
                public static readonly string Alias =
                    NfiEstaFunctions.FnGetEstimationCellsNumberInCollection.Name;

                /// <summary>
                /// Description in national language
                /// </summary>
                public static readonly string CaptionCs =
                    "Počet výpočetních buněk uvnitř každé kolekce výpočetních buněk";

                /// <summary>
                /// Description in English
                /// </summary>
                public static readonly string CaptionEn =
                    "Number of estimation cells for each estimation cell collection";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
                {
                    { "id", new ColumnMetadata()
                    {
                        Name = "id",
                        DbName = "id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = true,
                        NotNull = true,
                        DefaultValue = "0",
                        FuncCall = "row_number() over(order by estimation_cell_collection)",
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ID",
                        HeaderTextEn = "ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 0
                    }
                    },
                    { "estimation_cell_collection_id", new ColumnMetadata()
                    {
                        Name = "estimation_cell_collection_id",
                        DbName = "estimation_cell_collection",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = "0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATION_CELL_COLLECTION_ID",
                        HeaderTextEn = "ESTIMATION_CELL_COLLECTION_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 1
                    }
                    },
                    { "estimation_cells_number", new ColumnMetadata()
                    {
                        Name = "estimation_cells_number",
                        DbName = "estimation_cells_number",
                        DataType = "System.Int64",
                        DbDataType = "int8",
                        NewDataType = "int8",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = "0",
                        FuncCall = "count(*)",
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATION_CELLS_NUMBER",
                        HeaderTextEn = "ESTIMATION_CELLS_NUMBER",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 2
                    }
                    }
                };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column estimation_cell_collection_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationCellCollectionId = Cols["estimation_cell_collection_id"];

                /// <summary>
                /// Column estimation_cells_count metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationCellsNumber = Cols["estimation_cells_number"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TFnGetEstimationCellsNumberInCollectionList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TFnGetEstimationCellsNumberInCollectionList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// List of items (read-only)
                /// </summary>
                public List<TFnGetEstimationCellsNumberInCollection> Items
                {
                    get
                    {
                        return
                        [..
                            Data.AsEnumerable()
                            .Select(a => new TFnGetEstimationCellsNumberInCollection(composite: this, data: a))
                        ];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// Item from list by identifier (read-only)
                /// </summary>
                /// <param name="id">Item identifier</param>
                /// <returns>Item from list by identifier (null if not found)</returns>
                public TFnGetEstimationCellsNumberInCollection this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new TFnGetEstimationCellsNumberInCollection(composite: this, data: a))
                                .FirstOrDefault<TFnGetEstimationCellsNumberInCollection>();
                    }
                }

                #endregion Indexer


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public TFnGetEstimationCellsNumberInCollectionList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new TFnGetEstimationCellsNumberInCollectionList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new TFnGetEstimationCellsNumberInCollectionList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                #endregion Methods

            }

        }
    }
}