﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {
            // v_olap_total_estimate

            /// <summary>
            /// OLAP Cube - Total Estimate
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class OLAPTotalEstimate(
                OLAPTotalEstimateList composite = null,
                DataRow data = null)
                    : ALinqViewEntry<OLAPTotalEstimateList>(composite: composite, data: data)
            {

                #region Derived Properties

                #region Dimensions

                /// <summary>
                /// OLAP Cube Dimension - Total Estimate - composite identifier
                /// </summary>
                public new string Id
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColId.Name,
                            defaultValue: OLAPTotalEstimateList.ColId.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Phase estimate type identifier
                /// </summary>
                public int PhaseEstimateTypeId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColPhaseEstimateTypeId.Name,
                            defaultValue: Int32.Parse(s: OLAPTotalEstimateList.ColPhaseEstimateTypeId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColPhaseEstimateTypeId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Phase estimate type label (in national language)
                /// </summary>
                public string PhaseEstimateTypeLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColPhaseEstimateTypeLabelCs.Name,
                            defaultValue: OLAPTotalEstimateList.ColPhaseEstimateTypeLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColPhaseEstimateTypeLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Phase estimate type label (in English)
                /// </summary>
                public string PhaseEstimateTypeLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColPhaseEstimateTypeLabelEn.Name,
                            defaultValue: OLAPTotalEstimateList.ColPhaseEstimateTypeLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColPhaseEstimateTypeLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Estimate phase object (read-only)
                /// </summary>
                public PhaseEstimateType PhaseEstimateType
                {
                    get
                    {
                        return
                             ((NfiEstaDB)Composite.Database).SNfiEsta.CPhaseEstimateType[(int)PhaseEstimateTypeId];
                    }
                }


                /// <summary>
                /// Estimation period identifier
                /// </summary>
                public int EstimationPeriodId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColEstimationPeriodId.Name,
                            defaultValue: Int32.Parse(s: OLAPTotalEstimateList.ColEstimationPeriodId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColEstimationPeriodId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// The beginning of the time to witch the estimate is calculated
                /// </summary>
                public DateTime EstimateDateBegin
                {
                    get
                    {
                        return Functions.GetDateTimeArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColEstimateDateBegin.Name,
                            defaultValue: DateTime.Parse(s: OLAPTotalEstimateList.ColEstimateDateBegin.DefaultValue));
                    }
                    set
                    {
                        Functions.SetDateTimeArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColEstimateDateBegin.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// The beginning of the time to witch the estimate is calculated as text
                /// </summary>
                public string EstimateDateBeginText
                {
                    get
                    {
                        return
                            EstimateDateBegin.ToString(
                                format: OLAPTotalEstimateList.ColEstimateDateBegin.NumericFormat);
                    }
                }

                /// <summary>
                /// The end of the time to witch the estimate is calculated
                /// </summary>
                public DateTime EstimateDateEnd
                {
                    get
                    {
                        return Functions.GetDateTimeArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColEstimateDateEnd.Name,
                            defaultValue: DateTime.Parse(s: OLAPTotalEstimateList.ColEstimateDateEnd.DefaultValue));
                    }
                    set
                    {
                        Functions.SetDateTimeArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColEstimateDateEnd.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// The end of the time to witch the estimate is calculated as text
                /// </summary>
                public string EstimateDateEndText
                {
                    get
                    {
                        return
                            EstimateDateEnd.ToString(
                                format: OLAPTotalEstimateList.ColEstimateDateEnd.NumericFormat);
                    }
                }


                /// <summary>
                /// Estimation period label (in national language)
                /// </summary>
                public string EstimationPeriodLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColEstimationPeriodLabelCs.Name,
                            defaultValue: OLAPTotalEstimateList.ColEstimationPeriodLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColEstimationPeriodLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Estimation period label (in English)
                /// </summary>
                public string EstimationPeriodLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColEstimationPeriodLabelEn.Name,
                            defaultValue: OLAPTotalEstimateList.ColEstimationPeriodLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColEstimationPeriodLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Estimation period object (read-only)
                /// </summary>
                public EstimationPeriod EstimationPeriod
                {
                    get
                    {
                        return
                             ((NfiEstaDB)Composite.Database).SNfiEsta.CEstimationPeriod[(int)EstimationPeriodId];
                    }
                }


                /// <summary>
                /// Combination of target variable and sub-population category and area domain category - identifier
                /// </summary>
                public int VariableId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColVariableId.Name,
                            defaultValue: Int32.Parse(s: OLAPTotalEstimateList.ColVariableId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColVariableId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Combination of target variable and sub-population category and area domain category - label (in national language)
                /// </summary>
                public string VariableLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColVariableLabelCs.Name,
                            defaultValue: OLAPTotalEstimateList.ColVariableLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColVariableLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Combination of target variable and sub-population category and area domain category - label (in English)
                /// </summary>
                public string VariableLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColVariableLabelEn.Name,
                            defaultValue: OLAPTotalEstimateList.ColVariableLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColVariableLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Combination of target variable and sub-population category and area domain category - object (read-only)
                /// </summary>
                public Variable Variable
                {
                    get
                    {
                        return
                             ((NfiEstaDB)Composite.Database).SNfiEsta.TVariable[(int)VariableId];
                    }
                }

                /// <summary>
                /// Combination of target variable and sub-population category and area domain category (extended-version) - object (read-only)
                /// </summary>
                public VwVariable VwVariable
                {
                    get
                    {
                        return
                             ((NfiEstaDB)Composite.Database).SNfiEsta.VVariable[(int)VariableId];
                    }
                }


                /// <summary>
                /// Estimation cell identifier
                /// </summary>
                public int EstimationCellId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColEstimationCellId.Name,
                            defaultValue: Int32.Parse(s: OLAPTotalEstimateList.ColEstimationCellId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColEstimationCellId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Estimation cell description (in national language)
                /// </summary>
                public string EstimationCellDescriptionCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColEstimationCellDescriptionCs.Name,
                            defaultValue: OLAPTotalEstimateList.ColEstimationCellDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColEstimationCellDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Estimation cell description (in English)
                /// </summary>
                public string EstimationCellDescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColEstimationCellDescriptionEn.Name,
                            defaultValue: OLAPTotalEstimateList.ColEstimationCellDescriptionEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColEstimationCellDescriptionEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Estimation cell object (read-only)
                /// </summary>
                public EstimationCell EstimationCell
                {
                    get
                    {
                        return
                             ((NfiEstaDB)Composite.Database).SNfiEsta.CEstimationCell[(int)EstimationCellId];
                    }
                }



                /// <summary>
                /// Estimation cell collection identifier
                /// </summary>
                public int EstimationCellCollectionId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColEstimationCellCollectionId.Name,
                            defaultValue: Int32.Parse(s: OLAPTotalEstimateList.ColEstimationCellCollectionId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColEstimationCellCollectionId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Estimation cell collection label (in national language)
                /// </summary>
                public string EstimationCellCollectionLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColEstimationCellCollectionLabelCs.Name,
                            defaultValue: OLAPTotalEstimateList.ColEstimationCellCollectionLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColEstimationCellCollectionLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Estimation cell label (in English)
                /// </summary>
                public string EstimationCellCollectionLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColEstimationCellCollectionLabelEn.Name,
                            defaultValue: OLAPTotalEstimateList.ColEstimationCellCollectionLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColEstimationCellCollectionLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Estimation cell object (read-only)
                /// </summary>
                public EstimationCellCollection EstimationCellCollection
                {
                    get
                    {
                        return
                             ((NfiEstaDB)Composite.Database).SNfiEsta.CEstimationCellCollection[(int)EstimationCellCollectionId];
                    }
                }

                #endregion Dimensions

                #region Values

                /// <summary>
                /// Aggregated sets of panels and corresponding reference year sets - identifier
                /// </summary>
                public Nullable<int> PanelRefYearSetGroupId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColPanelRefYearSetGroupId.Name,
                            defaultValue:
                                String.IsNullOrEmpty(value: OLAPTotalEstimateList.ColPanelRefYearSetGroupId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: OLAPTotalEstimateList.ColPanelRefYearSetGroupId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColPanelRefYearSetGroupId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Aggregated sets of panels and corresponding reference year sets - label (in national language)
                /// </summary>
                public string PanelRefYearSetGroupLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColPanelRefYearSetGroupLabelCs.Name,
                            defaultValue: OLAPTotalEstimateList.ColPanelRefYearSetGroupLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColPanelRefYearSetGroupLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Aggregated sets of panels and corresponding reference year sets - label (in English)
                /// </summary>
                public string PanelRefYearSetGroupLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColPanelRefYearSetGroupLabelEn.Name,
                            defaultValue: OLAPTotalEstimateList.ColPanelRefYearSetGroupLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColPanelRefYearSetGroupLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Aggregated sets of panels and corresponding reference year sets - object (read-only)
                /// </summary>
                public PanelRefYearSetGroup PanelRefYearSetGroup
                {
                    get
                    {
                        return (PanelRefYearSetGroupId != null) ?
                             ((NfiEstaDB)Composite.Database).SNfiEsta.CPanelRefYearSetGroup[(int)PanelRefYearSetGroupId] : null;
                    }
                }


                /// <summary>
                /// Estimate configuration identifier
                /// </summary>
                public Nullable<int> EstimateConfId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColEstimateConfId.Name,
                            defaultValue:
                                String.IsNullOrEmpty(value: OLAPTotalEstimateList.ColEstimateConfId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: OLAPTotalEstimateList.ColEstimateConfId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColEstimateConfId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Estimate configuration object (read-only)
                /// </summary>
                public EstimateConf EstimateConf
                {
                    get
                    {
                        return (EstimateConfId != null) ?
                             ((NfiEstaDB)Composite.Database).SNfiEsta.TEstimateConf[(int)EstimateConfId] : null;
                    }
                }


                /// <summary>
                /// Estimate type (total or ratio) identifier
                /// </summary>
                public Nullable<int> EstimateTypeId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColEstimateTypeId.Name,
                            defaultValue:
                                String.IsNullOrEmpty(value: OLAPTotalEstimateList.ColEstimateTypeId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: OLAPTotalEstimateList.ColEstimateTypeId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColEstimateTypeId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Estimate type (total or ratio) object (read-only)
                /// </summary>
                public EstimateType EstimateType
                {
                    get
                    {
                        return (EstimateTypeId != null) ?
                             ((NfiEstaDB)Composite.Database).SNfiEsta.CEstimateType[(int)EstimateTypeId] : null;
                    }
                }


                /// <summary>
                /// Total estimate configuration identifier
                /// </summary>
                public Nullable<int> TotalEstimateConfId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColTotalEstimateConfId.Name,
                            defaultValue:
                                String.IsNullOrEmpty(value: OLAPTotalEstimateList.ColTotalEstimateConfId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: OLAPTotalEstimateList.ColTotalEstimateConfId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColTotalEstimateConfId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Total estimate configuration object (read-only)
                /// </summary>
                public TotalEstimateConf TotalEstimateConf
                {
                    get
                    {
                        return (TotalEstimateConfId != null) ?
                             ((NfiEstaDB)Composite.Database).SNfiEsta.TTotalEstimateConf[(int)TotalEstimateConfId] : null;
                    }
                }


                /// <summary>
                /// User, who configured estimate
                /// </summary>
                public string EstimateExecutor
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColEstimateExecutor.Name,
                            defaultValue: OLAPTotalEstimateList.ColEstimateExecutor.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColEstimateExecutor.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Total estimate configuration name (Short description of the estimate)
                /// </summary>
                public string TotalEstimateConfName
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColTotalEstimateConfName.Name,
                            defaultValue: OLAPTotalEstimateList.ColTotalEstimateConfName.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColTotalEstimateConfName.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Final estimate identifier
                /// </summary>
                public Nullable<int> ResultId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColResultId.Name,
                            defaultValue:
                                String.IsNullOrEmpty(value: OLAPTotalEstimateList.ColResultId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: OLAPTotalEstimateList.ColResultId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColResultId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Final estimate object (read-only)
                /// </summary>
                public Result Result
                {
                    get
                    {
                        return (ResultId != null) ?
                             ((NfiEstaDB)Composite.Database).SNfiEsta.TResult[(int)ResultId] : null;
                    }
                }


                /// <summary>
                /// Point estimate
                /// </summary>
                public Nullable<double> PointEstimate
                {
                    get
                    {
                        return Functions.GetNDoubleArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColPointEstimate.Name,
                            defaultValue:
                                String.IsNullOrEmpty(value: OLAPTotalEstimateList.ColPointEstimate.DefaultValue) ?
                                (Nullable<double>)null :
                                Double.Parse(s: OLAPTotalEstimateList.ColPointEstimate.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNDoubleArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColPointEstimate.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Variance estimate
                /// </summary>
                public Nullable<double> VarianceEstimate
                {
                    get
                    {
                        return Functions.GetNDoubleArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColVarianceEstimate.Name,
                            defaultValue:
                                String.IsNullOrEmpty(value: OLAPTotalEstimateList.ColVarianceEstimate.DefaultValue) ?
                                (Nullable<double>)null :
                                Double.Parse(s: OLAPTotalEstimateList.ColVarianceEstimate.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNDoubleArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColVarianceEstimate.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Time when calculation started
                /// </summary>
                public Nullable<DateTime> CalcStarted
                {
                    get
                    {
                        return Functions.GetNDateTimeArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColCalcStarted.Name,
                            defaultValue:
                                String.IsNullOrEmpty(value: OLAPTotalEstimateList.ColCalcStarted.DefaultValue) ?
                                (Nullable<DateTime>)null :
                                DateTime.Parse(s: OLAPTotalEstimateList.ColCalcStarted.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNDateTimeArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColCalcStarted.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Time when calculation started as text
                /// </summary>
                public string CalcStartedText
                {
                    get
                    {
                        return
                            (CalcStarted == null) ? Functions.StrNull :
                                ((DateTime)CalcStarted).ToString(
                                format: OLAPTotalEstimateList.ColCalcStarted.NumericFormat);
                    }
                }

                /// <summary>
                /// Version of the extension with which the results were calculated
                /// </summary>
                public string ExtensionVersion
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColExtensionVersion.Name,
                            defaultValue: OLAPTotalEstimateList.ColExtensionVersion.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColExtensionVersion.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Time necessary for calculation
                /// </summary>
                public Nullable<TimeSpan> CalcDuration
                {
                    get
                    {
                        return Functions.GetNTimeSpanArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColCalcDuration.Name,
                            defaultValue:
                                String.IsNullOrEmpty(value: OLAPTotalEstimateList.ColCalcDuration.DefaultValue) ?
                                (Nullable<TimeSpan>)null :
                                TimeSpan.Parse(s: OLAPTotalEstimateList.ColCalcDuration.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNTimeSpanArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColCalcDuration.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Time necessary for calculation as text
                /// </summary>
                public string CalcDurationText
                {
                    get
                    {
                        return
                            (CalcDuration == null) ? Functions.StrNull :
                                ((TimeSpan)CalcDuration).ToString(
                                format: OLAPTotalEstimateList.ColCalcDuration.NumericFormat);
                    }
                }

                /// <summary>
                /// Minimal sample size
                /// </summary>
                public Nullable<long> MinSSize
                {
                    get
                    {
                        return Functions.GetNLongArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColMinSSize.Name,
                            defaultValue:
                                String.IsNullOrEmpty(value: OLAPTotalEstimateList.ColMinSSize.DefaultValue) ?
                                (Nullable<long>)null :
                                Int64.Parse(s: OLAPTotalEstimateList.ColMinSSize.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNLongArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColMinSSize.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Actual sample size
                /// </summary>
                public Nullable<long> ActSSize
                {
                    get
                    {
                        return Functions.GetNLongArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColActSSize.Name,
                            defaultValue:
                                String.IsNullOrEmpty(value: OLAPTotalEstimateList.ColActSSize.DefaultValue) ?
                                (Nullable<long>)null :
                                Int64.Parse(s: OLAPTotalEstimateList.ColActSSize.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNLongArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColActSSize.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Flag for identification actual value of final estimate
                /// </summary>
                public Nullable<bool> IsLatest
                {
                    get
                    {
                        return Functions.GetNBoolArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColIsLatest.Name,
                            defaultValue:
                                String.IsNullOrEmpty(value: OLAPTotalEstimateList.ColIsLatest.DefaultValue) ?
                                (Nullable<bool>)null :
                                Boolean.Parse(value: OLAPTotalEstimateList.ColIsLatest.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNBoolArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColIsLatest.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// User, who calculated estimate
                /// </summary>
                public string ResultExecutor
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColResultExecutor.Name,
                            defaultValue: OLAPTotalEstimateList.ColResultExecutor.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColResultExecutor.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Standard error
                /// </summary>
                public Nullable<double> StandardError
                {
                    get
                    {
                        return Functions.GetNDoubleArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColStandardError.Name,
                            defaultValue:
                                String.IsNullOrEmpty(value: OLAPTotalEstimateList.ColStandardError.DefaultValue) ?
                                (Nullable<double>)null :
                                Double.Parse(s: OLAPTotalEstimateList.ColStandardError.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNDoubleArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColStandardError.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Confidence interval
                /// </summary>
                public Nullable<double> ConfidenceInterval
                {
                    get
                    {
                        return Functions.GetNDoubleArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColConfidenceInterval.Name,
                            defaultValue:
                                String.IsNullOrEmpty(value: OLAPTotalEstimateList.ColConfidenceInterval.DefaultValue) ?
                                (Nullable<double>)null :
                                Double.Parse(s: OLAPTotalEstimateList.ColConfidenceInterval.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNDoubleArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColConfidenceInterval.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Confidence interval - lower bound
                /// </summary>
                public Nullable<double> LowerBound
                {
                    get
                    {
                        return Functions.GetNDoubleArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColLowerBound.Name,
                            defaultValue:
                                String.IsNullOrEmpty(value: OLAPTotalEstimateList.ColLowerBound.DefaultValue) ?
                                (Nullable<double>)null :
                                Double.Parse(s: OLAPTotalEstimateList.ColLowerBound.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNDoubleArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColLowerBound.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Confidence interval - upper bound
                /// </summary>
                public Nullable<double> UpperBound
                {
                    get
                    {
                        return Functions.GetNDoubleArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColUpperBound.Name,
                            defaultValue:
                                String.IsNullOrEmpty(value: OLAPTotalEstimateList.ColUpperBound.DefaultValue) ?
                                (Nullable<double>)null :
                                Double.Parse(s: OLAPTotalEstimateList.ColUpperBound.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNDoubleArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColUpperBound.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Attribute additivity difference in sum of lower categories
                /// </summary>
                public Nullable<double> AttrDiffDown
                {
                    get
                    {
                        return Functions.GetNDoubleArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColAttrDiffDown.Name,
                            defaultValue:
                                String.IsNullOrEmpty(value: OLAPTotalEstimateList.ColAttrDiffDown.DefaultValue) ?
                                (Nullable<double>)null :
                                Double.Parse(s: OLAPTotalEstimateList.ColAttrDiffDown.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNDoubleArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColAttrDiffDown.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Attribute additivity difference in value of superior category
                /// </summary>
                public Nullable<double> AttrDiffUp
                {
                    get
                    {
                        return Functions.GetNDoubleArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColAttrDiffUp.Name,
                            defaultValue:
                                String.IsNullOrEmpty(value: OLAPTotalEstimateList.ColAttrDiffUp.DefaultValue) ?
                                (Nullable<double>)null :
                                Double.Parse(s: OLAPTotalEstimateList.ColAttrDiffUp.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNDoubleArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColAttrDiffUp.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Geographical additivity difference in sum of lower categories
                /// </summary>
                public Nullable<double> GeoDiffDown
                {
                    get
                    {
                        return Functions.GetNDoubleArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColGeoDiffDown.Name,
                            defaultValue:
                                String.IsNullOrEmpty(value: OLAPTotalEstimateList.ColGeoDiffDown.DefaultValue) ?
                                (Nullable<double>)null :
                                Double.Parse(s: OLAPTotalEstimateList.ColGeoDiffDown.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNDoubleArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColGeoDiffDown.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Geographical additivity difference in value of superior category
                /// </summary>
                public Nullable<double> GeoDiffUp
                {
                    get
                    {
                        return Functions.GetNDoubleArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColGeoDiffUp.Name,
                            defaultValue:
                                String.IsNullOrEmpty(value: OLAPTotalEstimateList.ColGeoDiffUp.DefaultValue) ?
                                (Nullable<double>)null :
                                Double.Parse(s: OLAPTotalEstimateList.ColGeoDiffUp.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNDoubleArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColGeoDiffUp.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Estimate is additive in attribute categories
                /// </summary>
                public Nullable<bool> IsAttrAdditive
                {
                    get
                    {
                        return Functions.GetNBoolArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColIsAttrAdditive.Name,
                            defaultValue:
                                String.IsNullOrEmpty(value: OLAPTotalEstimateList.ColIsAttrAdditive.DefaultValue) ?
                                (Nullable<bool>)null :
                                Boolean.Parse(value: OLAPTotalEstimateList.ColIsAttrAdditive.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNBoolArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColIsAttrAdditive.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Estimate is additive in geographical regions
                /// </summary>
                public Nullable<bool> IsGeoAdditive
                {
                    get
                    {
                        return Functions.GetNBoolArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColIsGeoAdditive.Name,
                            defaultValue:
                                String.IsNullOrEmpty(value: OLAPTotalEstimateList.ColIsGeoAdditive.DefaultValue) ?
                                (Nullable<bool>)null :
                                Boolean.Parse(value: OLAPTotalEstimateList.ColIsGeoAdditive.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNBoolArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColIsGeoAdditive.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Estimate is additive
                /// </summary>
                public Nullable<bool> IsAdditive
                {
                    get
                    {
                        return Functions.GetNBoolArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColIsAdditive.Name,
                            defaultValue:
                                String.IsNullOrEmpty(value: OLAPTotalEstimateList.ColIsAdditive.DefaultValue) ?
                                (Nullable<bool>)null :
                                Boolean.Parse(value: OLAPTotalEstimateList.ColIsAdditive.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNBoolArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColIsAdditive.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Model identifier
                /// </summary>
                public Nullable<int> ModelId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColModelId.Name,
                            defaultValue:
                                String.IsNullOrEmpty(value: OLAPTotalEstimateList.ColModelId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: OLAPTotalEstimateList.ColModelId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColModelId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Model label in national language
                /// </summary>
                public string ModelLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColModelLabelCs.Name,
                            defaultValue: OLAPTotalEstimateList.ColModelLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColModelLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Model description in national language
                /// </summary>
                public string ModelDescriptionCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColModelDescriptionCs.Name,
                            defaultValue: OLAPTotalEstimateList.ColModelDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColModelDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Model label in English
                /// </summary>
                public string ModelLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColModelLabelEn.Name,
                            defaultValue: OLAPTotalEstimateList.ColModelLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColModelLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Model description in English
                /// </summary>
                public string ModelDescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColModelLabelEn.Name,
                            defaultValue: OLAPTotalEstimateList.ColModelLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColModelLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Sigma
                /// </summary>
                public Nullable<bool> Sigma
                {
                    get
                    {
                        return Functions.GetNBoolArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColSigma.Name,
                            defaultValue:
                                String.IsNullOrEmpty(value: OLAPTotalEstimateList.ColSigma.DefaultValue) ?
                                (Nullable<bool>)null :
                                Boolean.Parse(value: OLAPTotalEstimateList.ColSigma.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNBoolArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColSigma.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Parametrization area identifier
                /// </summary>
                public Nullable<int> ParamAreaId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColParamAreaId.Name,
                            defaultValue:
                                String.IsNullOrEmpty(value: OLAPTotalEstimateList.ColParamAreaId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: OLAPTotalEstimateList.ColParamAreaId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColParamAreaId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Parametrization area label in national language
                /// </summary>
                public string ParamAreaLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColParamAreaLabelCs.Name,
                            defaultValue: OLAPTotalEstimateList.ColParamAreaLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColParamAreaLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Parametrization area description in national language
                /// </summary>
                public string ParamAreaDescriptionCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColParamAreaDescriptionCs.Name,
                            defaultValue: OLAPTotalEstimateList.ColParamAreaDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColParamAreaDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Parametrization area label in English
                /// </summary>
                public string ParamAreaLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColParamAreaLabelEn.Name,
                            defaultValue: OLAPTotalEstimateList.ColParamAreaLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColParamAreaLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Parametrization area description in English
                /// </summary>
                public string ParamAreaDescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColParamAreaDescriptionEn.Name,
                            defaultValue: OLAPTotalEstimateList.ColParamAreaDescriptionEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColParamAreaDescriptionEn.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Parametrization area type identifier
                /// </summary>
                public Nullable<int> ParamAreaTypeId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColParamAreaTypeId.Name,
                            defaultValue:
                                String.IsNullOrEmpty(value: OLAPTotalEstimateList.ColParamAreaTypeId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: OLAPTotalEstimateList.ColParamAreaTypeId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColParamAreaTypeId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Parametrization area type label in national language
                /// </summary>
                public string ParamAreaTypeLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColParamAreaTypeLabelCs.Name,
                            defaultValue: OLAPTotalEstimateList.ColParamAreaTypeLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColParamAreaTypeLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Parametrization area type description in national language
                /// </summary>
                public string ParamAreaTypeDescriptionCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColParamAreaTypeDescriptionCs.Name,
                            defaultValue: OLAPTotalEstimateList.ColParamAreaTypeDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColParamAreaTypeDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Parametrization area type label in English
                /// </summary>
                public string ParamAreaTypeLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColParamAreaTypeLabelEn.Name,
                            defaultValue: OLAPTotalEstimateList.ColParamAreaTypeLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColParamAreaTypeLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Parametrization area type description in English
                /// </summary>
                public string ParamAreaTypeDescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColParamAreaTypeDescriptionEn.Name,
                            defaultValue: OLAPTotalEstimateList.ColParamAreaTypeDescriptionEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColParamAreaTypeDescriptionEn.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Estimate configuration status identifier
                /// </summary>
                public int EstimateConfStatusId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColEstimateConfStatusId.Name,
                            defaultValue: Int32.Parse(s: OLAPTotalEstimateList.ColEstimateConfStatusId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColEstimateConfStatusId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Estimate configuration status label (in national language)
                /// </summary>
                public string EstimateConfStatusLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColEstimateConfStatusLabelCs.Name,
                            defaultValue: OLAPTotalEstimateList.ColEstimateConfStatusLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColEstimateConfStatusLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Estimate configuration status label (in English)
                /// </summary>
                public string EstimateConfStatusLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColEstimateConfStatusLabelEn.Name,
                            defaultValue: OLAPTotalEstimateList.ColEstimateConfStatusLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPTotalEstimateList.ColEstimateConfStatusLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Status object (read-only)
                /// </summary>
                public EstimateConfStatus EstimateConfStatus
                {
                    get
                    {
                        return
                             ((NfiEstaDB)Composite.Database).SNfiEsta.CEstimateConfStatus[(int)EstimateConfStatusId];
                    }
                }

                #endregion Values

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not OLAPEstimate Type, return False
                    if (obj is not OLAPTotalEstimate)
                    {
                        return false;
                    }

                    return
                        (PhaseEstimateTypeId == ((OLAPTotalEstimate)obj).PhaseEstimateTypeId) &&
                        (EstimationPeriodId == ((OLAPTotalEstimate)obj).EstimationPeriodId) &&
                        (VariableId == ((OLAPTotalEstimate)obj).VariableId) &&
                        (EstimationCellId == ((OLAPTotalEstimate)obj).EstimationCellId) &&
                        (PanelRefYearSetGroupId == ((OLAPTotalEstimate)obj).PanelRefYearSetGroupId) &&
                        (EstimateConfId == ((OLAPTotalEstimate)obj).EstimateConfId) &&
                        (TotalEstimateConfId == ((OLAPTotalEstimate)obj).TotalEstimateConfId) &&
                        (ResultId == ((OLAPTotalEstimate)obj).ResultId);
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        PhaseEstimateTypeId.GetHashCode() ^
                        EstimationPeriodId.GetHashCode() ^
                        VariableId.GetHashCode() ^
                        EstimationCellId.GetHashCode() ^
                        PanelRefYearSetGroupId.GetHashCode() ^
                        EstimateConfId.GetHashCode() ^
                        TotalEstimateConfId.GetHashCode() ^
                        ResultId.GetHashCode();
                }

                /// <summary>
                /// Text description of the object
                /// </summary>
                /// <returns>Text description of the object</returns>
                public override string ToString()
                {
                    return String.Concat(
                        $"{"ID"} = {Functions.PrepStringArg(arg: Id)}{Environment.NewLine}");
                }

                #endregion Methods

            }


            /// <summary>
            /// OLAP Cube - List of Total Estimates
            /// </summary>
            public class OLAPTotalEstimateList
                : ALinqView
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema =
                    NfiEstaSchema.Name;

                /// <summary>
                /// Table name
                /// </summary>
                public static readonly string Name =
                    "v_olap_total_estimate";

                /// <summary>
                /// Table alias
                /// </summary>
                public static readonly string Alias =
                    "v_olap_total_estimate";

                /// <summary>
                /// Description in national language
                /// </summary>
                public static readonly string CaptionCs =
                    "OLAP kostka s daty odhadů úhrnů";

                /// <summary>
                /// Description in English
                /// </summary>
                public static readonly string CaptionEn =
                    "OLAP cube with total estimates data";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
                {
                    { "id", new ColumnMetadata()
                    {
                        Name = "id",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = true,
                        NotNull = true,
                        DefaultValue =  String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = String.Empty,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ID",
                        HeaderTextEn = "ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 0
                    }
                    },
                    { "phase_estimate_type_id", new ColumnMetadata()
                    {
                        Name = "phase_estimate_type_id",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue =  "0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PHASE_ESTIMATE_TYPE_ID",
                        HeaderTextEn = "PHASE_ESTIMATE_TYPE_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 1
                    }
                    },
                    { "phase_estimate_type_label_cs", new ColumnMetadata()
                    {
                        Name = "phase_estimate_type_label_cs",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue =  String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = String.Empty,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PHASE_ESTIMATE_TYPE_LABEL_CS",
                        HeaderTextEn = "PHASE_ESTIMATE_TYPE_LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 2
                    }
                    },
                    { "phase_estimate_type_label_en", new ColumnMetadata()
                    {
                        Name = "phase_estimate_type_label_en",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue =  String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = String.Empty,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PHASE_ESTIMATE_TYPE_LABEL_EN",
                        HeaderTextEn = "PHASE_ESTIMATE_TYPE_LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 3
                    }
                    },
                    { "estimation_period_id", new ColumnMetadata()
                    {
                        Name = "estimation_period_id",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue =  "0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATION_PERIOD_ID",
                        HeaderTextEn = "ESTIMATION_PERIOD_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 4
                    }
                    },
                    { "estimate_date_begin", new ColumnMetadata()
                    {
                        Name = "estimate_date_begin",
                        DbName = null,
                        DataType = "System.DateTime",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue = new DateTime().ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "yyyy-MM-dd",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATE_DATE_BEGIN",
                        HeaderTextEn = "ESTIMATE_DATE_BEGIN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 5
                    }
                    },
                    { "estimate_date_end", new ColumnMetadata()
                    {
                        Name = "estimate_date_end",
                        DbName = null,
                        DataType = "System.DateTime",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue = new DateTime().ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "yyyy-MM-dd",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATE_DATE_END",
                        HeaderTextEn = "ESTIMATE_DATE_END",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 6
                    }
                    },
                    { "estimation_period_label_cs", new ColumnMetadata()
                    {
                        Name = "estimation_period_label_cs",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue =  String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = String.Empty,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATION_PERIOD_LABEL_CS",
                        HeaderTextEn = "ESTIMATION_PERIOD_LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 7
                    }
                    },
                    { "estimation_period_label_en", new ColumnMetadata()
                    {
                        Name = "estimation_period_label_en",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue =  String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = String.Empty,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATION_PERIOD_LABEL_EN",
                        HeaderTextEn = "ESTIMATION_PERIOD_LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 8
                    }
                    },
                    { "variable_id", new ColumnMetadata()
                    {
                        Name = "variable_id",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue =  "0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "VARIABLE_ID",
                        HeaderTextEn = "VARIABLE_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 9
                    }
                    },
                    { "variable_label_cs", new ColumnMetadata()
                    {
                        Name = "variable_label_cs",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue =  String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = String.Empty,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "VARIABLE_LABEL_CS",
                        HeaderTextEn = "VARIABLE_LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 10
                    }
                    },
                    { "variable_label_en", new ColumnMetadata()
                    {
                        Name = "variable_label_en",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue =  String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = String.Empty,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "VARIABLE_LABEL_EN",
                        HeaderTextEn = "VARIABLE_LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 11
                    }
                    },
                    { "estimation_cell_id", new ColumnMetadata()
                    {
                        Name = "estimation_cell_id",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue =  "0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATION_CELL_ID",
                        HeaderTextEn = "ESTIMATION_CELL_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 12
                    }
                    },
                    { "estimation_cell_description_cs", new ColumnMetadata()
                    {
                        Name = "estimation_cell_description_cs",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue =  String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = String.Empty,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATION_CELL_DESCRIPTION_CS",
                        HeaderTextEn = "ESTIMATION_CELL_DESCRIPTION_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 13
                    }
                    },
                    { "estimation_cell_description_en", new ColumnMetadata()
                    {
                        Name = "estimation_cell_description_en",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue =  String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = String.Empty,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATION_CELL_DESCRIPTION_EN",
                        HeaderTextEn = "ESTIMATION_CELL_DESCRIPTION_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 14
                    }
                    },
                    { "estimation_cell_collection_id", new ColumnMetadata()
                    {
                        Name = "estimation_cell_collection_id",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue =  "0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATION_CELL_COLLECTION_ID",
                        HeaderTextEn = "ESTIMATION_CELL_COLLECTION_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 15
                    }
                    },
                    { "estimation_cell_collection_label_cs", new ColumnMetadata()
                    {
                        Name = "estimation_cell_collection_label_cs",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue =  String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = String.Empty,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATION_CELL_COLLECTION_LABEL_CS",
                        HeaderTextEn = "ESTIMATION_CELL_COLLECTION_LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 16
                    }
                    },
                    { "estimation_cell_collection_label_en", new ColumnMetadata()
                    {
                        Name = "estimation_cell_collection_label_en",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue =  String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = String.Empty,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATION_CELL_COLLECTION_LABEL_EN",
                        HeaderTextEn = "ESTIMATION_CELL_COLLECTION_LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 17
                    }
                    },
                    { "panel_refyearset_group_id", new ColumnMetadata()
                    {
                        Name = "panel_refyearset_group_id",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PANEL_REFYEARSET_GROUP_ID",
                        HeaderTextEn = "PANEL_REFYEARSET_GROUP_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 18
                    }
                    },
                    { "panel_refyearset_group_label_cs", new ColumnMetadata()
                    {
                        Name = "panel_refyearset_group_label_cs",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  String.Empty,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PANEL_REFYEARSET_GROUP_LABEL_CS",
                        HeaderTextEn = "PANEL_REFYEARSET_GROUP_LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 19
                    }
                    },
                    { "panel_refyearset_group_label_en", new ColumnMetadata()
                    {
                        Name = "panel_refyearset_group_label_en",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  String.Empty,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PANEL_REFYEARSET_GROUP_LABEL_EN",
                        HeaderTextEn = "PANEL_REFYEARSET_GROUP_LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 20
                    }
                    },
                    { "estimate_conf_id", new ColumnMetadata()
                    {
                        Name = "estimate_conf_id",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATE_CONF_ID",
                        HeaderTextEn = "ESTIMATE_CONF_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 21
                    }
                    },
                    { "estimate_type_id", new ColumnMetadata()
                    {
                        Name = "estimate_type_id",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATE_TYPE_ID",
                        HeaderTextEn = "ESTIMATE_TYPE_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 22
                    }
                    },
                    { "total_estimate_conf_id", new ColumnMetadata()
                    {
                        Name = "total_estimate_conf_id",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "TOTAL_ESTIMATE_CONF_ID",
                        HeaderTextEn = "TOTAL_ESTIMATE_CONF_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 23
                    }
                    },
                    { "estimate_executor", new ColumnMetadata()
                    {
                        Name = "estimate_executor",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATE_EXECUTOR",
                        HeaderTextEn = "ESTIMATE_EXECUTOR",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 24
                    }
                    },
                    { "total_estimate_conf_name", new ColumnMetadata()
                    {
                        Name = "total_estimate_conf_name",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  String.Empty,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "TOTAL_ESTIMATE_CONF_NAME",
                        HeaderTextEn = "TOTAL_ESTIMATE_CONF_NAME",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 25
                    }
                    },
                    { "result_id", new ColumnMetadata()
                    {
                        Name = "result_id",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "RESULT_ID",
                        HeaderTextEn = "RESULT_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 26
                    }
                    },
                    { "point_estimate", new ColumnMetadata()
                    {
                        Name = "point_estimate",
                        DbName = null,
                        DataType = "System.Double",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  "N3",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "POINT_ESTIMATE",
                        HeaderTextEn = "POINT_ESTIMATE",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 27
                    }
                    },
                    { "variance_estimate", new ColumnMetadata()
                    {
                        Name = "variance_estimate",
                        DbName = null,
                        DataType = "System.Double",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  "N3",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "VARIANCE_ESTIMATE",
                        HeaderTextEn = "VARIANCE_ESTIMATE",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 28
                    }
                    },
                    { "calc_started", new ColumnMetadata()
                    {
                        Name = "calc_started",
                        DbName = null,
                        DataType = "System.DateTime",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "yyyy-MM-dd HH:mm:ss",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "CALC_STARTED",
                        HeaderTextEn = "CALC_STARTED",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 29
                    }
                    },
                    { "extension_version", new ColumnMetadata()
                    {
                        Name = "extension_version",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "EXTENSION_VERSION",
                        HeaderTextEn = "EXTENSION_VERSION",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 30
                    }
                    },
                    { "calc_duration", new ColumnMetadata()
                    {
                        Name = "calc_duration",
                        DbName = null,
                        DataType = "System.TimeSpan",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "g",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "CALC_DURATION",
                        HeaderTextEn = "CALC_DURATION",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 31
                    }
                    },
                    { "min_ssize", new ColumnMetadata()
                    {
                        Name = "min_ssize",
                        DbName = null,
                        DataType = "System.Int64",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "MIN_SSIZE",
                        HeaderTextEn = "MIN_SSIZE",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 32
                    }
                    },
                    { "act_ssize", new ColumnMetadata()
                    {
                        Name = "act_ssize",
                        DbName = null,
                        DataType = "System.Int64",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ACT_SSIZE",
                        HeaderTextEn = "ACT_SSIZE",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 33
                    }
                    },
                    { "is_latest", new ColumnMetadata()
                    {
                        Name = "is_latest",
                        DbName = null,
                        DataType = "System.Boolean",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "IS_LATEST",
                        HeaderTextEn = "IS_LATEST",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 34
                    }
                    },
                    { "result_executor", new ColumnMetadata()
                    {
                        Name = "result_executor",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "RESULT_EXECUTOR",
                        HeaderTextEn = "RESULT_EXECUTOR",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 35
                    }
                    },
                    { "standard_error", new ColumnMetadata()
                    {
                        Name = "standard_error",
                        DbName = null,
                        DataType = "System.Double",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  "N3",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "STANDARD_ERROR",
                        HeaderTextEn = "STANDARD_ERROR",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 36
                    }
                    },
                    { "confidence_interval", new ColumnMetadata()
                    {
                        Name = "confidence_interval",
                        DbName = null,
                        DataType = "System.Double",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  "N3",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "CONFIDENCE_INTERVAL",
                        HeaderTextEn = "CONFIDENCE_INTERVAL",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 37
                    }
                    },
                    { "lower_bound", new ColumnMetadata()
                    {
                        Name = "lower_bound",
                        DbName = null,
                        DataType = "System.Double",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  "N3",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "LOWER_BOUND",
                        HeaderTextEn = "LOWER_BOUND",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 38
                    }
                    },
                    { "upper_bound", new ColumnMetadata()
                    {
                        Name = "upper_bound",
                        DbName = null,
                        DataType = "System.Double",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  "N3",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "UPPER_BOUND",
                        HeaderTextEn = "UPPER_BOUND",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 39
                    }
                    },
                    { "attr_diff_down", new ColumnMetadata()
                    {
                        Name = "attr_diff_down",
                        DbName = null,
                        DataType = "System.Double",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  "N10",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ATTR_DIFF_DOWN",
                        HeaderTextEn = "ATTR_DIFF_DOWN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 40
                    }
                    },
                    { "attr_diff_up", new ColumnMetadata()
                    {
                        Name = "attr_diff_up",
                        DbName = null,
                        DataType = "System.Double",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  "N10",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ATTR_DIFF_UP",
                        HeaderTextEn = "ATTR_DIFF_UP",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 41
                    }
                    },
                    { "geo_diff_down", new ColumnMetadata()
                    {
                        Name = "geo_diff_down",
                        DbName = null,
                        DataType = "System.Double",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  "N10",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "GEO_DIFF_DOWN",
                        HeaderTextEn = "GEO_DIFF_DOWN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 42
                    }
                    },
                    { "geo_diff_up", new ColumnMetadata()
                    {
                        Name = "geo_diff_up",
                        DbName = null,
                        DataType = "System.Double",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  "N10",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "GEO_DIFF_UP",
                        HeaderTextEn = "GEO_DIFF_UP",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 43
                    }
                    },
                    { "is_attr_additive", new ColumnMetadata()
                    {
                        Name = "is_attr_additive",
                        DbName = null,
                        DataType = "System.Boolean",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "IS_ATTR_ADDITIVE",
                        HeaderTextEn = "IS_ATTR_ADDITIVE",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 44
                    }
                    },
                    { "is_geo_additive", new ColumnMetadata()
                    {
                        Name = "is_geo_additive",
                        DbName = null,
                        DataType = "System.Boolean",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "IS_GEO_ADDITIVE",
                        HeaderTextEn = "IS_GEO_ADDITIVE",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 45
                    }
                    },
                    { "is_additive", new ColumnMetadata()
                    {
                        Name = "is_additive",
                        DbName = null,
                        DataType = "System.Boolean",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "IS_ADDITIVE",
                        HeaderTextEn = "IS_ADDITIVE",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 46
                    }
                    },
                    { "model_id", new ColumnMetadata()
                    {
                        Name = "model_id",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "MODEL_ID",
                        HeaderTextEn = "MODEL_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 47
                    }
                    },
                    { "model_label_cs", new ColumnMetadata()
                    {
                        Name = "model_label_cs",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "MODEL_LABEL_CS",
                        HeaderTextEn = "MODEL_LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 48
                    }
                    },
                    { "model_description_cs", new ColumnMetadata()
                    {
                        Name = "model_description_cs",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "MODEL_DESCRIPTION_CS",
                        HeaderTextEn = "MODEL_DESCRIPTION_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 49
                    }
                    },
                    { "model_label_en", new ColumnMetadata()
                    {
                        Name = "model_label_en",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "MODEL_LABEL_EN",
                        HeaderTextEn = "MODEL_LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 50
                    }
                    },
                    { "model_description_en", new ColumnMetadata()
                    {
                        Name = "model_description_en",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "MODEL_DESCRIPTION_EN",
                        HeaderTextEn = "MODEL_DESCRIPTION_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 51
                    }
                    },
                    { "sigma", new ColumnMetadata()
                    {
                        Name = "sigma",
                        DbName = null,
                        DataType = "System.Boolean",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "SIGMA",
                        HeaderTextEn = "SIGMA",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 52
                    }
                    },
                    { "param_area_id", new ColumnMetadata()
                    {
                        Name = "param_area_id",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PARAM_AREA_ID",
                        HeaderTextEn = "PARAM_AREA_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 53
                    }
                    },
                    { "param_area_label_cs", new ColumnMetadata()
                    {
                        Name = "param_area_label_cs",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PARAM_AREA_LABEL_CS",
                        HeaderTextEn = "PARAM_AREA_LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 54
                    }
                    },
                    { "param_area_description_cs", new ColumnMetadata()
                    {
                        Name = "param_area_description_cs",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PARAM_AREA_DESCRIPTION_CS",
                        HeaderTextEn = "PARAM_AREA_DESCRIPTION_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 55
                    }
                    },
                    { "param_area_label_en", new ColumnMetadata()
                    {
                        Name = "param_area_label_en",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PARAM_AREA_LABEL_EN",
                        HeaderTextEn = "PARAM_AREA_LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 56
                    }
                    },
                    { "param_area_description_en", new ColumnMetadata()
                    {
                        Name = "param_area_description_en",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PARAM_AREA_DESCRIPTION_EN",
                        HeaderTextEn = "PARAM_AREA_DESCRIPTION_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 57
                    }
                    },
                    { "param_area_type_id", new ColumnMetadata()
                    {
                        Name = "param_area_type_id",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PARAM_AREA_TYPE_ID",
                        HeaderTextEn = "PARAM_AREA_TYPE_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 58
                    }
                    },
                    { "param_area_type_label_cs", new ColumnMetadata()
                    {
                        Name = "param_area_type_label_cs",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PARAM_AREA_TYPE_LABEL_CS",
                        HeaderTextEn = "PARAM_AREA_TYPE_LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 59
                    }
                    },
                    { "param_area_type_description_cs", new ColumnMetadata()
                    {
                        Name = "param_area_type_description_cs",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PARAM_AREA_TYPE_DESCRIPTION_CS",
                        HeaderTextEn = "PARAM_AREA_TYPE_DESCRIPTION_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 60
                    }
                    },
                    { "param_area_type_label_en", new ColumnMetadata()
                    {
                        Name = "param_area_type_label_en",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PARAM_AREA_TYPE_LABEL_EN",
                        HeaderTextEn = "PARAM_AREA_TYPE_LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 61
                    }
                    },
                    { "param_area_type_description_en", new ColumnMetadata()
                    {
                        Name = "param_area_type_description_en",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PARAM_AREA_TYPE_DESCRIPTION_EN",
                        HeaderTextEn = "PARAM_AREA_TYPE_DESCRIPTION_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 62
                    }
                    },
                    { "estimate_conf_status_id", new ColumnMetadata()
                    {
                        Name = "estimate_conf_status_id",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue =  "0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATE_CONF_STATUS_ID",
                        HeaderTextEn = "ESTIMATE_CONF_STATUS_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 63
                    }
                    },
                    { "estimate_conf_status_label_cs", new ColumnMetadata()
                    {
                        Name = "estimate_conf_status_label_cs",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATE_CONF_STATUS_LABEL_CS",
                        HeaderTextEn = "ESTIMATE_CONF_STATUS_LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 64
                    }
                    },
                    { "estimate_conf_status_label_en", new ColumnMetadata()
                    {
                        Name = "estimate_conf_status_label_en",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATE_CONF_STATUS_LABEL_EN",
                        HeaderTextEn = "ESTIMATE_CONF_STATUS_LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 65
                    }
                    }
                };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column phase_estimate_type_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColPhaseEstimateTypeId = Cols["phase_estimate_type_id"];

                /// <summary>
                /// Column phase_estimate_type_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColPhaseEstimateTypeLabelCs = Cols["phase_estimate_type_label_cs"];

                /// <summary>
                /// Column phase_estimate_type_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColPhaseEstimateTypeLabelEn = Cols["phase_estimate_type_label_en"];

                /// <summary>
                /// Column estimation_period_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationPeriodId = Cols["estimation_period_id"];

                /// <summary>
                /// Column estimate_date_begin metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimateDateBegin = Cols["estimate_date_begin"];

                /// <summary>
                /// Column estimate_date_end metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimateDateEnd = Cols["estimate_date_end"];

                /// <summary>
                /// Column estimation_period_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationPeriodLabelCs = Cols["estimation_period_label_cs"];

                /// <summary>
                /// Column estimation_period_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationPeriodLabelEn = Cols["estimation_period_label_en"];

                /// <summary>
                /// Column variable_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColVariableId = Cols["variable_id"];

                /// <summary>
                /// Column variable_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColVariableLabelCs = Cols["variable_label_cs"];

                /// <summary>
                /// Column variable_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColVariableLabelEn = Cols["variable_label_en"];

                /// <summary>
                /// Column estimation_cell_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationCellId = Cols["estimation_cell_id"];

                /// <summary>
                /// Column estimation_cell_description_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationCellDescriptionCs = Cols["estimation_cell_description_cs"];

                /// <summary>
                /// Column estimation_cell_description_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationCellDescriptionEn = Cols["estimation_cell_description_en"];

                /// <summary>
                /// Column estimation_cell_collection_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationCellCollectionId = Cols["estimation_cell_collection_id"];

                /// <summary>
                /// Column estimation_cell_collection_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationCellCollectionLabelCs = Cols["estimation_cell_collection_label_cs"];

                /// <summary>
                /// Column estimation_cell_collection_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationCellCollectionLabelEn = Cols["estimation_cell_collection_label_en"];

                /// <summary>
                /// Column panel_refyearset_group_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColPanelRefYearSetGroupId = Cols["panel_refyearset_group_id"];

                /// <summary>
                /// Column panel_refyearset_group_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColPanelRefYearSetGroupLabelCs = Cols["panel_refyearset_group_label_cs"];

                /// <summary>
                /// Column panel_refyearset_group_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColPanelRefYearSetGroupLabelEn = Cols["panel_refyearset_group_label_en"];

                /// <summary>
                /// Column estimate_conf_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimateConfId = Cols["estimate_conf_id"];

                /// <summary>
                /// Column estimate_type_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimateTypeId = Cols["estimate_type_id"];

                /// <summary>
                /// Column total_estimate_conf_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColTotalEstimateConfId = Cols["total_estimate_conf_id"];

                /// <summary>
                /// Column estimate_executor metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimateExecutor = Cols["estimate_executor"];

                /// <summary>
                /// Column total_estimate_conf_name metadata
                /// </summary>
                public static readonly ColumnMetadata ColTotalEstimateConfName = Cols["total_estimate_conf_name"];

                /// <summary>
                /// Column result_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColResultId = Cols["result_id"];

                /// <summary>
                /// Column point_estimate metadata
                /// </summary>
                public static readonly ColumnMetadata ColPointEstimate = Cols["point_estimate"];

                /// <summary>
                /// Column variance_estimate metadata
                /// </summary>
                public static readonly ColumnMetadata ColVarianceEstimate = Cols["variance_estimate"];

                /// <summary>
                /// Column calc_started metadata
                /// </summary>
                public static readonly ColumnMetadata ColCalcStarted = Cols["calc_started"];

                /// <summary>
                /// Column extension_version metadata
                /// </summary>
                public static readonly ColumnMetadata ColExtensionVersion = Cols["extension_version"];

                /// <summary>
                /// Column calc_duration metadata
                /// </summary>
                public static readonly ColumnMetadata ColCalcDuration = Cols["calc_duration"];

                /// <summary>
                /// Column min_ssize metadata
                /// </summary>
                public static readonly ColumnMetadata ColMinSSize = Cols["min_ssize"];

                /// <summary>
                /// Column act_ssize metadata
                /// </summary>
                public static readonly ColumnMetadata ColActSSize = Cols["act_ssize"];

                /// <summary>
                /// Column is_latest metadata
                /// </summary>
                public static readonly ColumnMetadata ColIsLatest = Cols["is_latest"];

                /// <summary>
                /// Column result_executor metadata
                /// </summary>
                public static readonly ColumnMetadata ColResultExecutor = Cols["result_executor"];

                /// <summary>
                /// Column standard_error metadata
                /// </summary>
                public static readonly ColumnMetadata ColStandardError = Cols["standard_error"];

                /// <summary>
                /// Column confidence_interval metadata
                /// </summary>
                public static readonly ColumnMetadata ColConfidenceInterval = Cols["confidence_interval"];

                /// <summary>
                /// Column lower_bound metadata
                /// </summary>
                public static readonly ColumnMetadata ColLowerBound = Cols["lower_bound"];

                /// <summary>
                /// Column upper_bound metadata
                /// </summary>
                public static readonly ColumnMetadata ColUpperBound = Cols["upper_bound"];


                /// <summary>
                /// Column attr_diff_down metadata
                /// </summary>
                public static readonly ColumnMetadata ColAttrDiffDown = Cols["attr_diff_down"];

                /// <summary>
                /// Column attr_diff_up metadata
                /// </summary>
                public static readonly ColumnMetadata ColAttrDiffUp = Cols["attr_diff_up"];

                /// <summary>
                /// Column geo_diff_down metadata
                /// </summary>
                public static readonly ColumnMetadata ColGeoDiffDown = Cols["geo_diff_down"];

                /// <summary>
                /// Column geo_diff_up metadata
                /// </summary>
                public static readonly ColumnMetadata ColGeoDiffUp = Cols["geo_diff_up"];

                /// <summary>
                /// Column is_attr_additive metadata
                /// </summary>
                public static readonly ColumnMetadata ColIsAttrAdditive = Cols["is_attr_additive"];

                /// <summary>
                /// Column is_geo_additive metadata
                /// </summary>
                public static readonly ColumnMetadata ColIsGeoAdditive = Cols["is_geo_additive"];

                /// <summary>
                /// Column is_additive metadata
                /// </summary>
                public static readonly ColumnMetadata ColIsAdditive = Cols["is_additive"];

                /// <summary>
                /// Column model_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColModelId = Cols["model_id"];

                /// <summary>
                /// Column model_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColModelLabelCs = Cols["model_label_cs"];

                /// <summary>
                /// Column model_description_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColModelDescriptionCs = Cols["model_description_cs"];

                /// <summary>
                /// Column model_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColModelLabelEn = Cols["model_label_en"];

                /// <summary>
                /// Column model_description_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColModelDescriptionEn = Cols["model_description_en"];

                /// <summary>
                /// Column sigma metadata
                /// </summary>
                public static readonly ColumnMetadata ColSigma = Cols["sigma"];

                /// <summary>
                /// Column param_area_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColParamAreaId = Cols["param_area_id"];

                /// <summary>
                /// Column param_area_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColParamAreaLabelCs = Cols["param_area_label_cs"];

                /// <summary>
                /// Column param_area_description_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColParamAreaDescriptionCs = Cols["param_area_description_cs"];

                /// <summary>
                /// Column param_area_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColParamAreaLabelEn = Cols["param_area_label_en"];

                /// <summary>
                /// Column param_area_description_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColParamAreaDescriptionEn = Cols["param_area_description_en"];

                /// <summary>
                /// Column param_area_type_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColParamAreaTypeId = Cols["param_area_type_id"];

                /// <summary>
                /// Column param_area_type_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColParamAreaTypeLabelCs = Cols["param_area_type_label_cs"];

                /// <summary>
                /// Column param_area_type_description_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColParamAreaTypeDescriptionCs = Cols["param_area_type_description_cs"];

                /// <summary>
                /// Column param_area_type_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColParamAreaTypeLabelEn = Cols["param_area_type_label_en"];

                /// <summary>
                /// Column param_area_type_description_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColParamAreaTypeDescriptionEn = Cols["param_area_type_description_en"];

                /// <summary>
                /// Column estimate_conf_status_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimateConfStatusId = Cols["estimate_conf_status_id"];

                /// <summary>
                /// Column estimate_conf_status_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimateConfStatusLabelCs = Cols["estimate_conf_status_label_cs"];

                /// <summary>
                /// Column estimate_conf_status_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimateConfStatusLabelEn = Cols["estimate_conf_status_label_en"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public OLAPTotalEstimateList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public OLAPTotalEstimateList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// List of OLAP Cube Total Estimates (read-only)
                /// </summary>
                public List<OLAPTotalEstimate> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new OLAPTotalEstimate(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// OLAP Cube - Total Estimate from list by identifier (read-only)
                /// </summary>
                /// <param name="id">OLAP Cube - Total Estimate from list by identifier</param>
                /// <returns>OLAP Cube - Total Estimate from list by identifier (null if not found)</returns>
                public OLAPTotalEstimate this[string id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<string>(ColId.Name) == id)
                                .Select(a => new OLAPTotalEstimate(composite: this, data: a))
                                .FirstOrDefault<OLAPTotalEstimate>();
                    }
                }

                #endregion Indexer


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public OLAPTotalEstimateList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new OLAPTotalEstimateList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new OLAPTotalEstimateList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                /// <summary>
                /// Loads data into OLAP Cube - List of Total Estimates (if it was not done before)
                /// </summary>
                /// <param name="olapDimensions">List of OLAP Cube Dimensions</param>
                /// <param name="olapValues">List of OLAP Cube Values</param>
                /// <param name="cEstimateConfStatus">List of estimate configuration status</param>
                /// <param name="additivityLimit">Additivity limit</param>
                /// <param name="condition">Condition for selection data</param>
                /// <param name="limit">Rows count limit</param>
                public void Load(
                    OLAPTotalEstimateDimensionList olapDimensions,
                    OLAPTotalEstimateValueList olapValues,
                    EstimateConfStatusList cEstimateConfStatus,
                    double additivityLimit,
                    string condition = null,
                    Nullable<int> limit = null)
                {
                    if (!Loaded)
                    {
                        ReLoad(
                            olapDimensions: olapDimensions,
                            olapValues: olapValues,
                            cEstimateConfStatus: cEstimateConfStatus,
                            additivityLimit: additivityLimit,
                            condition: condition,
                            limit: limit);
                    }
                }

                /// <summary>
                /// Loads data into OLAP Cube - List of Total Estimates
                /// </summary>
                /// <param name="olapDimensions">List of OLAP Cube Dimensions</param>
                /// <param name="olapValues">List of OLAP Cube Values</param>
                /// <param name="cEstimateConfStatus">List of estimate configuration status</param>
                /// <param name="additivityLimit">Additivity limit</param>
                /// <param name="condition">Condition for selection data</param>
                /// <param name="limit">Rows count limit</param>
                public void ReLoad(
                    OLAPTotalEstimateDimensionList olapDimensions,
                    OLAPTotalEstimateValueList olapValues,
                    EstimateConfStatusList cEstimateConfStatus,
                    double additivityLimit,
                    string condition = null,
                    Nullable<int> limit = null)
                {
                    Data = EmptyDataTable(
                                tableName: TableName,
                                columns: Columns);
                    Condition = condition;
                    Limit = limit;

                    System.Diagnostics.Stopwatch stopWatch = new();
                    stopWatch.Start();

                    #region cteOlapDimensions: Dimenze OLAP
                    var cteOlapDimensions =
                        olapDimensions.Data.AsEnumerable()
                        .Select(a => new
                        {
                            Id = a.Field<string>(columnName: OLAPTotalEstimateDimensionList.ColId.Name),
                            PhaseEstimateTypeId = a.Field<int>(columnName: OLAPTotalEstimateDimensionList.ColPhaseEstimateTypeId.Name),
                            PhaseEstimateTypeLabelCs = a.Field<string>(columnName: OLAPTotalEstimateDimensionList.ColPhaseEstimateTypeLabelCs.Name),
                            PhaseEstimateTypeLabelEn = a.Field<string>(columnName: OLAPTotalEstimateDimensionList.ColPhaseEstimateTypeLabelEn.Name),
                            EstimationPeriodId = a.Field<int>(columnName: OLAPTotalEstimateDimensionList.ColEstimationPeriodId.Name),
                            EstimateDateBegin = a.Field<DateTime>(columnName: OLAPTotalEstimateDimensionList.ColEstimateDateBegin.Name),
                            EstimateDateEnd = a.Field<DateTime>(columnName: OLAPTotalEstimateDimensionList.ColEstimateDateEnd.Name),
                            EstimationPeriodLabelCs = a.Field<string>(columnName: OLAPTotalEstimateDimensionList.ColEstimationPeriodLabelCs.Name),
                            EstimationPeriodLabelEn = a.Field<string>(columnName: OLAPTotalEstimateDimensionList.ColEstimationPeriodLabelEn.Name),
                            VariableId = a.Field<int>(columnName: OLAPTotalEstimateDimensionList.ColVariableId.Name),
                            VariableLabelCs = a.Field<string>(columnName: OLAPTotalEstimateDimensionList.ColVariableLabelCs.Name),
                            VariableLabelEn = a.Field<string>(columnName: OLAPTotalEstimateDimensionList.ColVariableLabelEn.Name),
                            EstimationCellId = a.Field<int>(columnName: OLAPTotalEstimateDimensionList.ColEstimationCellId.Name),
                            EstimationCellDescriptionCs = a.Field<string>(columnName: OLAPTotalEstimateDimensionList.ColEstimationCellDescriptionCs.Name),
                            EstimationCellDescriptionEn = a.Field<string>(columnName: OLAPTotalEstimateDimensionList.ColEstimationCellDescriptionEn.Name),
                            EstimationCellCollectionId = a.Field<int>(columnName: OLAPTotalEstimateDimensionList.ColEstimationCellCollectionId.Name),
                            EstimationCellCollectionLabelCs = a.Field<string>(columnName: OLAPTotalEstimateDimensionList.ColEstimationCellCollectionLabelCs.Name),
                            EstimationCellCollectionLabelEn = a.Field<string>(columnName: OLAPTotalEstimateDimensionList.ColEstimationCellCollectionLabelEn.Name)
                        });
                    #endregion cteOlapDimensions: Dimenze OLAP

                    #region cteOlapValues: Hodnoty OLAP
                    var cteOlapValues =
                        olapValues.Data.AsEnumerable()
                        .Select(a => new
                        {
                            Id = a.Field<string>(columnName: OLAPTotalEstimateValueList.ColId.Name),
                            PhaseEstimateTypeId = a.Field<int>(columnName: OLAPTotalEstimateValueList.ColPhaseEstimateTypeId.Name),
                            EstimationPeriodId = a.Field<int>(columnName: OLAPTotalEstimateValueList.ColEstimationPeriodId.Name),
                            VariableId = a.Field<int>(columnName: OLAPTotalEstimateValueList.ColVariableId.Name),
                            EstimationCellId = a.Field<int>(columnName: OLAPTotalEstimateValueList.ColEstimationCellId.Name),
                            PanelRefYearSetGroupId = a.Field<Nullable<int>>(columnName: OLAPTotalEstimateValueList.ColPanelRefYearSetGroupId.Name),
                            PanelRefYearSetGroupLabelCs = a.Field<string>(columnName: OLAPTotalEstimateValueList.ColPanelRefYearSetGroupLabelCs.Name),
                            PanelRefYearSetGroupLabelEn = a.Field<string>(columnName: OLAPTotalEstimateValueList.ColPanelRefYearSetGroupLabelEn.Name),
                            EstimateConfId = a.Field<Nullable<int>>(columnName: OLAPTotalEstimateValueList.ColEstimateConfId.Name),
                            EstimateTypeId = a.Field<Nullable<int>>(columnName: OLAPTotalEstimateValueList.ColEstimateTypeId.Name),
                            TotalEstimateConfId = a.Field<Nullable<int>>(columnName: OLAPTotalEstimateValueList.ColTotalEstimateConfId.Name),
                            EstimateExecutor = a.Field<string>(columnName: OLAPTotalEstimateValueList.ColEstimateExecutor.Name),
                            TotalEstimateConfName = a.Field<string>(columnName: OLAPTotalEstimateValueList.ColTotalEstimateConfName.Name),
                            ResultId = a.Field<Nullable<int>>(columnName: OLAPTotalEstimateValueList.ColResultId.Name),
                            PointEstimate = a.Field<Nullable<double>>(columnName: OLAPTotalEstimateValueList.ColPointEstimate.Name),
                            VarianceEstimate = a.Field<Nullable<double>>(columnName: OLAPTotalEstimateValueList.ColVarianceEstimate.Name),
                            CalcStarted = a.Field<Nullable<DateTime>>(columnName: OLAPTotalEstimateValueList.ColCalcStarted.Name),
                            ExtensionVersion = a.Field<string>(columnName: OLAPTotalEstimateValueList.ColExtensionVersion.Name),
                            CalcDuration = a.Field<Nullable<TimeSpan>>(columnName: OLAPTotalEstimateValueList.ColCalcDuration.Name),
                            MinSSize = a.Field<Nullable<long>>(columnName: OLAPTotalEstimateValueList.ColMinSSize.Name),
                            ActSSize = a.Field<Nullable<long>>(columnName: OLAPTotalEstimateValueList.ColActSSize.Name),
                            IsLatest = a.Field<Nullable<bool>>(columnName: OLAPTotalEstimateValueList.ColIsLatest.Name),
                            ResultExecutor = a.Field<string>(columnName: OLAPTotalEstimateValueList.ColResultExecutor.Name),
                            StandardError = a.Field<Nullable<double>>(columnName: OLAPTotalEstimateValueList.ColStandardError.Name),
                            ConfidenceInterval = a.Field<Nullable<double>>(columnName: OLAPTotalEstimateValueList.ColConfidenceInterval.Name),
                            LowerBound = a.Field<Nullable<double>>(columnName: OLAPTotalEstimateValueList.ColLowerBound.Name),
                            UpperBound = a.Field<Nullable<double>>(columnName: OLAPTotalEstimateValueList.ColUpperBound.Name),
                            AttrDiffDown = a.Field<Nullable<double>>(columnName: OLAPTotalEstimateValueList.ColAttrDiffDown.Name),
                            AttrDiffUp = a.Field<Nullable<double>>(columnName: OLAPTotalEstimateValueList.ColAttrDiffUp.Name),
                            GeoDiffDown = a.Field<Nullable<double>>(columnName: OLAPTotalEstimateValueList.ColGeoDiffDown.Name),
                            GeoDiffUp = a.Field<Nullable<double>>(columnName: OLAPTotalEstimateValueList.ColGeoDiffUp.Name),
                            ModelId = a.Field<Nullable<int>>(columnName: OLAPTotalEstimateValueList.ColModelId.Name),
                            ModelLabelCs = a.Field<string>(columnName: OLAPTotalEstimateValueList.ColModelLabelCs.Name),
                            ModelDescriptionCs = a.Field<string>(columnName: OLAPTotalEstimateValueList.ColModelDescriptionCs.Name),
                            ModelLabelEn = a.Field<string>(columnName: OLAPTotalEstimateValueList.ColModelLabelEn.Name),
                            ModelDescriptionEn = a.Field<string>(columnName: OLAPTotalEstimateValueList.ColModelDescriptionEn.Name),
                            Sigma = a.Field<Nullable<bool>>(columnName: OLAPTotalEstimateValueList.ColSigma.Name),
                            ParamAreaId = a.Field<Nullable<int>>(columnName: OLAPTotalEstimateValueList.ColParamAreaId.Name),
                            ParamAreaLabelCs = a.Field<string>(columnName: OLAPTotalEstimateValueList.ColParamAreaLabelCs.Name),
                            ParamAreaDescriptionCs = a.Field<string>(columnName: OLAPTotalEstimateValueList.ColParamAreaDescriptionCs.Name),
                            ParamAreaLabelEn = a.Field<string>(columnName: OLAPTotalEstimateValueList.ColParamAreaLabelEn.Name),
                            ParamAreaDescriptionEn = a.Field<string>(columnName: OLAPTotalEstimateValueList.ColParamAreaDescriptionEn.Name),
                            ParamAreaTypeId = a.Field<Nullable<int>>(columnName: OLAPTotalEstimateValueList.ColParamAreaTypeId.Name),
                            ParamAreaTypeLabelCs = a.Field<string>(columnName: OLAPTotalEstimateValueList.ColParamAreaTypeLabelCs.Name),
                            ParamAreaTypeDescriptionCs = a.Field<string>(columnName: OLAPTotalEstimateValueList.ColParamAreaTypeDescriptionCs.Name),
                            ParamAreaTypeLabelEn = a.Field<string>(columnName: OLAPTotalEstimateValueList.ColParamAreaTypeLabelEn.Name),
                            ParamAreaTypeDescriptionEn = a.Field<string>(columnName: OLAPTotalEstimateValueList.ColParamAreaTypeDescriptionEn.Name)
                        });
                    #endregion cteOlapValues: Hodnoty OLAP

                    #region cteEstimateConfStatus: Status odhadu
                    var cteEstimateConfStatus =
                        cEstimateConfStatus.Data.AsEnumerable()
                        .Select(a => new
                        {
                            EstimateConfStatusId = a.Field<int>(columnName: EstimateConfStatusList.ColId.Name),
                            EstimateConfStatusLabelCs = a.Field<string>(columnName: EstimateConfStatusList.ColLabelCs.Name),
                            EstimateConfStatusLabelEn = a.Field<string>(columnName: EstimateConfStatusList.ColLabelEn.Name),
                            EstimateConfStatusDescriptionCs = a.Field<string>(columnName: EstimateConfStatusList.ColDescriptionCs.Name),
                            EstimateConfStatusDescriptionEn = a.Field<string>(columnName: EstimateConfStatusList.ColDescriptionEn.Name)
                        });
                    #endregion cteEstimateConfStatus: Status odhadu

                    #region cteOlapCubeJoin: cteOlapDimensions + cteOlapValues (LEFT JOIN)
                    var cteOlapCubeJoin =
                        from a in cteOlapDimensions
                        join b in cteOlapValues
                        on a.Id equals b.Id into lj
                        from b in lj.DefaultIfEmpty()
                        select new
                        {
                            a.Id,
                            a.PhaseEstimateTypeId,
                            a.PhaseEstimateTypeLabelCs,
                            a.PhaseEstimateTypeLabelEn,
                            a.EstimationPeriodId,
                            a.EstimateDateBegin,
                            a.EstimateDateEnd,
                            a.EstimationPeriodLabelCs,
                            a.EstimationPeriodLabelEn,
                            a.VariableId,
                            a.VariableLabelCs,
                            a.VariableLabelEn,
                            a.EstimationCellId,
                            a.EstimationCellDescriptionCs,
                            a.EstimationCellDescriptionEn,
                            a.EstimationCellCollectionId,
                            a.EstimationCellCollectionLabelCs,
                            a.EstimationCellCollectionLabelEn,
                            b?.PanelRefYearSetGroupId,
                            b?.PanelRefYearSetGroupLabelCs,
                            b?.PanelRefYearSetGroupLabelEn,
                            b?.EstimateConfId,
                            b?.EstimateTypeId,
                            b?.TotalEstimateConfId,
                            b?.EstimateExecutor,
                            b?.TotalEstimateConfName,
                            b?.ResultId,
                            b?.PointEstimate,
                            b?.VarianceEstimate,
                            b?.CalcStarted,
                            b?.ExtensionVersion,
                            b?.CalcDuration,
                            b?.MinSSize,
                            b?.ActSSize,
                            b?.IsLatest,
                            b?.ResultExecutor,
                            b?.StandardError,
                            b?.ConfidenceInterval,
                            b?.LowerBound,
                            b?.UpperBound,
                            b?.AttrDiffDown,
                            b?.AttrDiffUp,
                            b?.GeoDiffDown,
                            b?.GeoDiffUp,
                            b?.ModelId,
                            b?.ModelLabelCs,
                            b?.ModelDescriptionCs,
                            b?.ModelLabelEn,
                            b?.ModelDescriptionEn,
                            b?.Sigma,
                            b?.ParamAreaId,
                            b?.ParamAreaLabelCs,
                            b?.ParamAreaDescriptionCs,
                            b?.ParamAreaLabelEn,
                            b?.ParamAreaDescriptionEn,
                            b?.ParamAreaTypeId,
                            b?.ParamAreaTypeLabelCs,
                            b?.ParamAreaTypeDescriptionCs,
                            b?.ParamAreaTypeLabelEn,
                            b?.ParamAreaTypeDescriptionEn,
                            EstimateConfStatusId =
                                (b == null) ? 100 :                     // není konfigurace
                                (b.ResultId == null) ? 200 :            // není odhad
                                300                                     // odhad je vypočten
                        };
                    #endregion cteOlapCubeJoin: cteOlapDimensions + cteOlapValues (LEFT JOIN)

                    #region cteOlapCube: cteOlapCubeJoin + cteEstimateConfStatus (LEFT JOIN)
                    var cteOlapCube =
                        from a in cteOlapCubeJoin
                        join b in cteEstimateConfStatus
                        on a.EstimateConfStatusId equals b.EstimateConfStatusId into lj
                        from b in lj.DefaultIfEmpty()
                        select new
                        {
                            a.Id,
                            a.PhaseEstimateTypeId,
                            a.PhaseEstimateTypeLabelCs,
                            a.PhaseEstimateTypeLabelEn,
                            a.EstimationPeriodId,
                            a.EstimateDateBegin,
                            a.EstimateDateEnd,
                            a.EstimationPeriodLabelCs,
                            a.EstimationPeriodLabelEn,
                            a.VariableId,
                            a.VariableLabelCs,
                            a.VariableLabelEn,
                            a.EstimationCellId,
                            a.EstimationCellDescriptionCs,
                            a.EstimationCellDescriptionEn,
                            a.EstimationCellCollectionId,
                            a.EstimationCellCollectionLabelCs,
                            a.EstimationCellCollectionLabelEn,
                            a.PanelRefYearSetGroupId,
                            a.PanelRefYearSetGroupLabelCs,
                            a.PanelRefYearSetGroupLabelEn,
                            a.EstimateConfId,
                            a.EstimateTypeId,
                            a.TotalEstimateConfId,
                            a.EstimateExecutor,
                            a.TotalEstimateConfName,
                            a.ResultId,
                            a.PointEstimate,
                            a.VarianceEstimate,
                            a.CalcStarted,
                            a.ExtensionVersion,
                            a.CalcDuration,
                            a.MinSSize,
                            a.ActSSize,
                            a.IsLatest,
                            a.ResultExecutor,
                            a.StandardError,
                            a.ConfidenceInterval,
                            a.LowerBound,
                            a.UpperBound,
                            a.AttrDiffDown,
                            a.AttrDiffUp,
                            a.GeoDiffDown,
                            a.GeoDiffUp,
                            IsAttrAdditive =
                                (a.AttrDiffDown == null) ? (Nullable<bool>)null :
                                (a.AttrDiffUp == null) ? (Nullable<bool>)null :
                                (a.AttrDiffDown < additivityLimit) &&
                                (a.AttrDiffUp < additivityLimit),
                            IsGeoAdditive =
                                (a.GeoDiffDown == null) ? (Nullable<bool>)null :
                                (a.GeoDiffUp == null) ? (Nullable<bool>)null :
                                (a.GeoDiffDown < additivityLimit) &&
                                (a.GeoDiffUp < additivityLimit),
                            IsAdditive =
                                (a.AttrDiffDown == null) ? (Nullable<bool>)null :
                                (a.AttrDiffUp == null) ? (Nullable<bool>)null :
                                (a.GeoDiffDown == null) ? (Nullable<bool>)null :
                                (a.GeoDiffUp == null) ? (Nullable<bool>)null :
                                (a.AttrDiffDown < additivityLimit) &&
                                (a.AttrDiffUp < additivityLimit) &&
                                (a.GeoDiffDown < additivityLimit) &&
                                (a.GeoDiffUp < additivityLimit),
                            a.ModelId,
                            a.ModelLabelCs,
                            a.ModelDescriptionCs,
                            a.ModelLabelEn,
                            a.ModelDescriptionEn,
                            a.Sigma,
                            a.ParamAreaId,
                            a.ParamAreaLabelCs,
                            a.ParamAreaDescriptionCs,
                            a.ParamAreaLabelEn,
                            a.ParamAreaDescriptionEn,
                            a.ParamAreaTypeId,
                            a.ParamAreaTypeLabelCs,
                            a.ParamAreaTypeDescriptionCs,
                            a.ParamAreaTypeLabelEn,
                            a.ParamAreaTypeDescriptionEn,
                            b?.EstimateConfStatusId,
                            b?.EstimateConfStatusLabelCs,
                            b?.EstimateConfStatusLabelEn
                        };
                    #endregion cteOlapCube: cteOlapCubeJoin + cteEstimateConfStatus (LEFT JOIN)

                    #region Zápis výsledku do DataTable
                    if (cteOlapCube.Any())
                    {
                        Data =
                            cteOlapCube
                            .OrderBy(a => a.PhaseEstimateTypeId)
                            .ThenBy(a => a.EstimationPeriodId)
                            .ThenBy(a => a.VariableId)
                            .ThenBy(a => a.EstimationCellId)
                            .ThenBy(a => a.PanelRefYearSetGroupId)
                            .ThenByDescending(a => a.CalcStarted)
                            .Select(a =>
                                Data.LoadDataRow(
                                    values:
                                    [
                                        a.Id,
                                        a.PhaseEstimateTypeId,
                                        a.PhaseEstimateTypeLabelCs,
                                        a.PhaseEstimateTypeLabelEn,
                                        a.EstimationPeriodId,
                                        a.EstimateDateBegin,
                                        a.EstimateDateEnd,
                                        a.EstimationPeriodLabelCs,
                                        a.EstimationPeriodLabelEn,
                                        a.VariableId,
                                        a.VariableLabelCs,
                                        a.VariableLabelEn,
                                        a.EstimationCellId,
                                        a.EstimationCellDescriptionCs,
                                        a.EstimationCellDescriptionEn,
                                        a.EstimationCellCollectionId,
                                        a.EstimationCellCollectionLabelCs,
                                        a.EstimationCellCollectionLabelEn,
                                        a.PanelRefYearSetGroupId,
                                        a.PanelRefYearSetGroupLabelCs,
                                        a.PanelRefYearSetGroupLabelEn,
                                        a.EstimateConfId,
                                        a.EstimateTypeId,
                                        a.TotalEstimateConfId,
                                        a.EstimateExecutor,
                                        a.TotalEstimateConfName,
                                        a.ResultId,
                                        a.PointEstimate,
                                        a.VarianceEstimate,
                                        a.CalcStarted,
                                        a.ExtensionVersion,
                                        a.CalcDuration,
                                        a.MinSSize,
                                        a.ActSSize,
                                        a.IsLatest,
                                        a.ResultExecutor,
                                        a.StandardError,
                                        a.ConfidenceInterval,
                                        a.LowerBound,
                                        a.UpperBound,
                                        a.AttrDiffDown,
                                        a.AttrDiffUp,
                                        a.GeoDiffDown,
                                        a.GeoDiffUp,
                                        a.IsAttrAdditive,
                                        a.IsGeoAdditive,
                                        a.IsAdditive,
                                        a.ModelId,
                                        a.ModelLabelCs,
                                        a.ModelDescriptionCs,
                                        a.ModelLabelEn,
                                        a.ModelDescriptionEn,
                                        a.Sigma,
                                        a.ParamAreaId,
                                        a.ParamAreaLabelCs,
                                        a.ParamAreaDescriptionCs,
                                        a.ParamAreaLabelEn,
                                        a.ParamAreaDescriptionEn,
                                        a.ParamAreaTypeId,
                                        a.ParamAreaTypeLabelCs,
                                        a.ParamAreaTypeDescriptionCs,
                                        a.ParamAreaTypeLabelEn,
                                        a.ParamAreaTypeDescriptionEn,
                                        a.EstimateConfStatusId,
                                        a.EstimateConfStatusLabelCs,
                                        a.EstimateConfStatusLabelEn
                                    ],
                                    fAcceptChanges: false))
                            .CopyToDataTable();
                        Data.TableName = Name;
                    }
                    #endregion Zápis výsledku do DataTable

                    #region Omezení podmínkou
                    if (!String.IsNullOrEmpty(value: Condition))
                    {
                        DataView view = new(table: Data);
                        try
                        {
                            view.RowFilter = Condition;
                        }
                        catch
                        {
                            view.RowFilter = String.Empty;
                            Condition = String.Empty;
                        }
                        Data = view.ToTable();
                    }
                    #endregion Omezení podmínkou

                    #region Omezení počtem
                    if (Limit != null)
                    {
                        Data = Data.AsEnumerable()
                            .Take(count: (int)Limit)
                            .CopyToDataTable();
                    }
                    #endregion Omezení počtem

                    stopWatch.Stop();
                    LoadingTime = 0.001 * stopWatch.ElapsedMilliseconds;
                    Data.TableName = TableName;
                }

                #endregion Methods

            }

        }
    }
}
