﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {
            // v_panel_refyearset_group

            /// <summary>
            /// Pair of panel with corresponding reference year set (extended version)
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class VwPanelRefYearSetPair(
                VwPanelRefYearSetPairList composite = null,
                DataRow data = null)
                    : ALinqViewEntry<VwPanelRefYearSetPairList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Pair of panel with corresponding reference year set identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: VwPanelRefYearSetPairList.ColId.Name,
                            defaultValue: Int32.Parse(s: VwPanelRefYearSetPairList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: VwPanelRefYearSetPairList.ColId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Aggregated sets of panels and corresponding reference year sets identifier
                /// </summary>
                public int PanelRefYearSetGroupId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: VwPanelRefYearSetPairList.ColPanelRefYearSetGroupId.Name,
                            defaultValue: Int32.Parse(s: VwPanelRefYearSetPairList.ColPanelRefYearSetGroupId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: VwPanelRefYearSetPairList.ColPanelRefYearSetGroupId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Aggregated sets of panels and corresponding reference year sets object (read-only)
                /// </summary>
                public PanelRefYearSetGroup PanelRefYearSetGroup
                {
                    get
                    {
                        return ((NfiEstaDB)Composite.Database).SNfiEsta.CPanelRefYearSetGroup[PanelRefYearSetGroupId];
                    }
                }


                /// <summary>
                /// Sampling panel identifier
                /// </summary>
                public int PanelId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: VwPanelRefYearSetPairList.ColPanelId.Name,
                            defaultValue: Int32.Parse(s: VwPanelRefYearSetPairList.ColPanelId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: VwPanelRefYearSetPairList.ColPanelId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Sampling panel name
                /// </summary>
                public string PanelName
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: VwPanelRefYearSetPairList.ColPanelName.Name,
                            defaultValue: VwPanelRefYearSetPairList.ColPanelName.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: VwPanelRefYearSetPairList.ColPanelName.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Sampling panel label
                /// </summary>
                public string PanelLabel
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: VwPanelRefYearSetPairList.ColPanelLabel.Name,
                            defaultValue: VwPanelRefYearSetPairList.ColPanelLabel.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: VwPanelRefYearSetPairList.ColPanelLabel.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Total number of clusters on panel
                /// </summary>
                public Nullable<int> PanelClusterCount
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: VwPanelRefYearSetPairList.ColPanelClusterCount.Name,
                            defaultValue: String.IsNullOrEmpty(value: VwPanelRefYearSetPairList.ColPanelClusterCount.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: VwPanelRefYearSetPairList.ColPanelClusterCount.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: VwPanelRefYearSetPairList.ColPanelClusterCount.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Total number of plots on panel
                /// </summary>
                public Nullable<int> PanelPlotCount
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: VwPanelRefYearSetPairList.ColPanelPlotCount.Name,
                            defaultValue: String.IsNullOrEmpty(value: VwPanelRefYearSetPairList.ColPanelPlotCount.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: VwPanelRefYearSetPairList.ColPanelPlotCount.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: VwPanelRefYearSetPairList.ColPanelPlotCount.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Sampling panel object (read-only)
                /// </summary>
                public SDesign.Panel Panel
                {
                    get
                    {
                        return ((NfiEstaDB)Composite.Database).SDesign.TPanel[PanelId];
                    }
                }


                /// <summary>
                /// Reference year or season set identifier
                /// </summary>
                public int ReferenceYearSetId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: VwPanelRefYearSetPairList.ColReferenceYearSetId.Name,
                            defaultValue: Int32.Parse(s: VwPanelRefYearSetPairList.ColReferenceYearSetId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: VwPanelRefYearSetPairList.ColReferenceYearSetId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Reference year or season set name
                /// </summary>
                public string ReferenceYearSetName
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: VwPanelRefYearSetPairList.ColReferenceYearSetName.Name,
                            defaultValue: VwPanelRefYearSetPairList.ColReferenceYearSetName.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: VwPanelRefYearSetPairList.ColReferenceYearSetName.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Reference year or season set label
                /// </summary>
                public string ReferenceYearSetLabel
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: VwPanelRefYearSetPairList.ColReferenceYearSetLabel.Name,
                            defaultValue: VwPanelRefYearSetPairList.ColReferenceYearSetLabel.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: VwPanelRefYearSetPairList.ColReferenceYearSetLabel.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Date from which the period set begins
                /// </summary>
                public Nullable<DateTime> ReferenceDateBegin
                {
                    get
                    {
                        return Functions.GetNDateTimeArg(
                            row: Data,
                            name: VwPanelRefYearSetPairList.ColReferenceDateBegin.Name,
                            defaultValue: String.IsNullOrEmpty(value: VwPanelRefYearSetPairList.ColReferenceDateBegin.DefaultValue) ?
                                            (Nullable<DateTime>)null :
                                            DateTime.Parse(s: VwPanelRefYearSetPairList.ColReferenceDateBegin.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNDateTimeArg(
                            row: Data,
                            name: VwPanelRefYearSetPairList.ColReferenceDateBegin.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Date from which the period set begins as text
                /// </summary>
                public string ReferenceDateBeginText
                {
                    get
                    {
                        return
                            (ReferenceDateBegin == null) ?
                                Functions.StrNull :
                                ((DateTime)ReferenceDateBegin).ToString(
                                    format: VwPanelRefYearSetPairList.ColReferenceDateBegin.NumericFormat);
                    }
                }

                /// <summary>
                /// Date to which the period set ends
                /// </summary>
                public Nullable<DateTime> ReferenceDateEnd
                {
                    get
                    {
                        return Functions.GetNDateTimeArg(
                            row: Data,
                            name: VwPanelRefYearSetPairList.ColReferenceDateEnd.Name,
                            defaultValue: String.IsNullOrEmpty(value: VwPanelRefYearSetPairList.ColReferenceDateEnd.DefaultValue) ?
                                            (Nullable<DateTime>)null :
                                            DateTime.Parse(s: VwPanelRefYearSetPairList.ColReferenceDateEnd.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNDateTimeArg(
                            row: Data,
                            name: VwPanelRefYearSetPairList.ColReferenceDateEnd.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Date to which the period set ends as text
                /// </summary>
                public string ReferenceDateEndText
                {
                    get
                    {
                        return
                            (ReferenceDateEnd == null) ?
                                Functions.StrNull :
                                ((DateTime)ReferenceDateEnd).ToString(
                                    format: VwPanelRefYearSetPairList.ColReferenceDateEnd.NumericFormat);
                    }
                }

                /// <summary>
                /// Reference year or season set object (read-only)
                /// </summary>
                public SDesign.ReferenceYearSet ReferenceYearSet
                {
                    get
                    {
                        return ((NfiEstaDB)Composite.Database).SDesign.TReferenceYearSet[ReferenceYearSetId];
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not VwPanelRefYearSetPair Type, return False
                    if (obj is not VwPanelRefYearSetPair)
                    {
                        return false;
                    }

                    return
                        Id == ((VwPanelRefYearSetPair)obj).Id;
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                #endregion Methods

            }


            /// <summary>
            /// List of pairs of panel with corresponding reference year set (extended version)
            /// </summary>
            public class VwPanelRefYearSetPairList
                : ALinqView
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema =
                    NfiEstaSchema.Name;

                /// <summary>
                /// Table name
                /// </summary>
                public static readonly string Name =
                    "v_panel_refyearset_group";

                /// <summary>
                /// Table alias
                /// </summary>
                public static readonly string Alias =
                    "v_panel_refyearset_group";

                /// <summary>
                /// Description in national language
                /// </summary>
                public static readonly string CaptionCs =
                    "Seznam párů panelu s odpovídajícím rokem měření (rozšířená verze)";

                /// <summary>
                /// Description in English
                /// </summary>
                public static readonly string CaptionEn =
                    "List of pairs of panel with corresponding reference year set (extended version)";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
                {
                    { "id", new ColumnMetadata()
                    {
                        Name = "id",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = true,
                        NotNull = true,
                        DefaultValue =  "0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ID",
                        HeaderTextEn = "ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 0
                    }
                    },
                    { "panel_refyearset_group_id", new ColumnMetadata()
                    {
                        Name = "panel_refyearset_group_id",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue =  "0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PANEL_REFYERSET_GROUP_ID",
                        HeaderTextEn = "PANEL_REFYERSET_GROUP_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 1
                    }
                    },
                    { "panel_id", new ColumnMetadata()
                    {
                        Name = "panel_id",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue =  "0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PANEL_ID",
                        HeaderTextEn = "PANEL_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 2
                    }
                    },
                    { "panel_name", new ColumnMetadata()
                    {
                        Name = "panel_name",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PANEL_NAME",
                        HeaderTextEn = "PANEL_NAME",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 3
                    }
                    },
                    { "panel_label", new ColumnMetadata()
                    {
                        Name = "panel_label",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PANEL_LABEL",
                        HeaderTextEn = "PANEL_LABEL",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 4
                    }
                    },
                    { "panel_cluster_count", new ColumnMetadata()
                    {
                        Name = "panel_cluster_count",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PANEL_CLUSTER_COUNT",
                        HeaderTextEn = "PANEL_CLUSTER_COUNT",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 5
                    }
                    },
                    { "panel_plot_count", new ColumnMetadata()
                    {
                        Name = "panel_plot_count",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PANEL_PLOT_COUNT",
                        HeaderTextEn = "PANEL_PLOT_COUNT",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 6
                    }
                    },
                    { "reference_year_set_id", new ColumnMetadata()
                    {
                        Name = "reference_year_set_id",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue = "0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "REFERENCE_YEAR_SET_ID",
                        HeaderTextEn = "REFERENCE_YEAR_SET_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 7
                    }
                    },
                    { "reference_year_set_name", new ColumnMetadata()
                    {
                        Name = "reference_year_set_name",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "REFERENCE_YEAR_SET_NAME",
                        HeaderTextEn = "REFERENCE_YEAR_SET_NAME",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 8
                    }
                    },
                    { "reference_year_set_label", new ColumnMetadata()
                    {
                        Name = "reference_year_set_label",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "REFERENCE_YEAR_SET_LABEL",
                        HeaderTextEn = "REFERENCE_YEAR_SET_LABEL",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 9
                    }
                    },
                    { "reference_date_begin", new ColumnMetadata()
                    {
                        Name = "reference_date_begin",
                        DbName = null,
                        DataType = "System.DateTime",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "yyyy-MM-dd",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "REFERENCE_DATE_BEGIN",
                        HeaderTextEn = "REFERENCE_DATE_BEGIN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 10
                    }
                    },
                    { "reference_date_end", new ColumnMetadata()
                    {
                        Name = "reference_date_end",
                        DbName = null,
                        DataType = "System.DateTime",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "yyyy-MM-dd",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "REFERENCE_DATE_END",
                        HeaderTextEn = "REFERENCE_DATE_END",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 11
                    }
                    }
                };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column panel_refyearset_group_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColPanelRefYearSetGroupId = Cols["panel_refyearset_group_id"];

                /// <summary>
                /// Column panel_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColPanelId = Cols["panel_id"];

                /// <summary>
                /// Column panel_name metadata
                /// </summary>
                public static readonly ColumnMetadata ColPanelName = Cols["panel_name"];

                /// <summary>
                /// Column panel_label metadata
                /// </summary>
                public static readonly ColumnMetadata ColPanelLabel = Cols["panel_label"];

                /// <summary>
                /// Column panel_cluster_count metadata
                /// </summary>
                public static readonly ColumnMetadata ColPanelClusterCount = Cols["panel_cluster_count"];

                /// <summary>
                /// Column panel_plot_count metadata
                /// </summary>
                public static readonly ColumnMetadata ColPanelPlotCount = Cols["panel_plot_count"];

                /// <summary>
                /// Column reference_year_set_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColReferenceYearSetId = Cols["reference_year_set_id"];

                /// <summary>
                /// Column reference_year_set_name metadata
                /// </summary>
                public static readonly ColumnMetadata ColReferenceYearSetName = Cols["reference_year_set_name"];

                /// <summary>
                /// Column reference_year_set_label metadata
                /// </summary>
                public static readonly ColumnMetadata ColReferenceYearSetLabel = Cols["reference_year_set_label"];

                /// <summary>
                /// Column reference_date_begin metadata
                /// </summary>
                public static readonly ColumnMetadata ColReferenceDateBegin = Cols["reference_date_begin"];

                /// <summary>
                /// Column reference_date_end metadata
                /// </summary>
                public static readonly ColumnMetadata ColReferenceDateEnd = Cols["reference_date_end"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public VwPanelRefYearSetPairList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public VwPanelRefYearSetPairList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// List of pairs of panel with corresponding reference year set (extended version) (read-only)
                /// </summary>
                public List<VwPanelRefYearSetPair> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new VwPanelRefYearSetPair(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// Pair of panel with corresponding reference year set (extended version) from list by identifier (read-only)
                /// </summary>
                /// <param name="id">Pair of panel with corresponding reference year set identifier</param>
                /// <returns>Pair of panel with corresponding reference year set (extended version) from list by identifier (null if not found)</returns>
                public VwPanelRefYearSetPair this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new VwPanelRefYearSetPair(composite: this, data: a))
                                .FirstOrDefault<VwPanelRefYearSetPair>();
                    }
                }

                #endregion Indexer


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public VwPanelRefYearSetPairList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new VwPanelRefYearSetPairList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new VwPanelRefYearSetPairList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                /// <summary>
                /// List of pairs of panel with corresponding reference year set
                /// for selected panel reference year set groups, sampling panels
                /// and reference year or season sets
                /// </summary>
                /// <param name="panelRefYearSetGroupId">Selected panel reference year set groups identifiers</param>
                /// <param name="panelId">Selected sampling panels identifiers</param>
                /// <param name="referenceYearSetId">Selected reference year or season sets identifiers</param>
                /// <returns>
                /// List of pairs of panel with corresponding reference year set
                /// for selected panel reference year set groups, sampling panels
                /// and reference year or season sets
                /// </returns>
                public VwPanelRefYearSetPairList Reduce(
                    List<int> panelRefYearSetGroupId = null,
                    List<int> panelId = null,
                    List<int> referenceYearSetId = null)
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();

                    if ((panelRefYearSetGroupId != null) && panelRefYearSetGroupId.Count != 0)
                    {
                        rows = rows.Where(a => panelRefYearSetGroupId.Contains<int>(
                                value: (int)(a.Field<Nullable<int>>(columnName: ColPanelRefYearSetGroupId.Name) ?? 0)));
                    }

                    if ((panelId != null) && panelId.Count != 0)
                    {
                        rows = rows.Where(a => panelId.Contains<int>(
                                value: (int)(a.Field<Nullable<int>>(columnName: ColPanelId.Name) ?? 0)));
                    }

                    if ((referenceYearSetId != null) && referenceYearSetId.Count != 0)
                    {
                        rows = rows.Where(a => referenceYearSetId.Contains<int>(
                                value: (int)(a.Field<Nullable<int>>(columnName: ColReferenceYearSetId.Name) ?? 0)));
                    }

                    return
                        rows.Any() ?
                        new VwPanelRefYearSetPairList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new VwPanelRefYearSetPairList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                /// <summary>
                /// Loads object data
                /// (if it was not done before)
                /// </summary>
                /// <param name="tPanelRefYearSetGroup">List of pairs of panel with corresponding reference year set</param>
                /// <param name="tPanel">List of sampling panels</param>
                /// <param name="tReferenceYearSet">List of reference year or season sets</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                public void Load(
                    PanelRefYearSetPairList tPanelRefYearSetGroup,
                    SDesign.PanelList tPanel,
                    SDesign.ReferenceYearSetList tReferenceYearSet,
                    string condition = null,
                    Nullable<int> limit = null)
                {
                    if (!Loaded)
                    {
                        ReLoad(
                            tPanelRefYearSetGroup: tPanelRefYearSetGroup,
                            tPanel: tPanel,
                            tReferenceYearSet: tReferenceYearSet,
                            condition: condition,
                            limit: limit);
                    }
                }

                /// <summary>
                /// Loads object data
                /// </summary>
                /// <param name="tPanelRefYearSetGroup">List of pairs of panel with corresponding reference year set</param>
                /// <param name="tPanel">List of sampling panels</param>
                /// <param name="tReferenceYearSet">List of reference year or season sets</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                public void ReLoad(
                    PanelRefYearSetPairList tPanelRefYearSetGroup,
                    SDesign.PanelList tPanel,
                    SDesign.ReferenceYearSetList tReferenceYearSet,
                    string condition = null,
                    Nullable<int> limit = null)
                {
                    Data = EmptyDataTable(
                                tableName: TableName,
                                columns: Columns);
                    Condition = condition;
                    Limit = limit;

                    System.Diagnostics.Stopwatch stopWatch = new();
                    stopWatch.Start();

                    // Skupina panelů a referenčních období
                    var ctePanelRefYearSetGroup =
                        tPanelRefYearSetGroup.Data.AsEnumerable()
                        .Select(a => new
                        {
                            Id = a.Field<int>(PanelRefYearSetPairList.ColId.Name),
                            PanelRefYearSetGroupId = a.Field<int>(PanelRefYearSetPairList.ColPanelRefYearSetGroupId.Name),
                            PanelId = a.Field<int>(PanelRefYearSetPairList.ColPanelId.Name),
                            ReferenceYearSetId = a.Field<Nullable<int>>(PanelRefYearSetPairList.ColReferenceYearSetId.Name) ?? 0
                        });

                    // Panely
                    var ctePanel =
                         tPanel.Data.AsEnumerable()
                         .Select(a => new
                         {
                             PanelId = a.Field<int>(SDesign.PanelList.ColId.Name),
                             PanelName = a.Field<string>(SDesign.PanelList.ColName.Name),
                             PanelLabel = a.Field<string>(SDesign.PanelList.ColLabel.Name),
                             PanelClusterCount = a.Field<int>(SDesign.PanelList.ColClusterCount.Name),
                             PanelPlotCount = a.Field<int>(SDesign.PanelList.ColPlotCount.Name)
                         });

                    // Referenční období
                    var cteReferenceYearSet =
                         tReferenceYearSet.Data.AsEnumerable()
                         .Select(a => new
                         {
                             ReferenceYearSetId = a.Field<int>(SDesign.ReferenceYearSetList.ColId.Name),
                             ReferenceYearSetName = a.Field<string>(SDesign.ReferenceYearSetList.ColName.Name),
                             ReferenceYearSetLabel = a.Field<string>(SDesign.ReferenceYearSetList.ColLabel.Name),
                             ReferenceDateBegin = a.Field<Nullable<DateTime>>(SDesign.ReferenceYearSetList.ColReferenceDateBegin.Name),
                             ReferenceDateEnd = a.Field<Nullable<DateTime>>(SDesign.ReferenceYearSetList.ColReferenceDateEnd.Name)
                         });

                    // LEFT JOIN t_panel_refyearset_group + t_panel
                    var ctePanelRefYearSetGroupPanel =
                        from a in ctePanelRefYearSetGroup
                        join b in ctePanel
                        on a.PanelId equals b.PanelId into lj
                        from b in lj.DefaultIfEmpty()
                        select new
                        {
                            a.Id,
                            a.PanelRefYearSetGroupId,
                            a.PanelId,
                            b?.PanelName,
                            b?.PanelLabel,
                            b?.PanelClusterCount,
                            b?.PanelPlotCount,
                            a.ReferenceYearSetId
                        };

                    // LEFT JOIN t_panel_refyearset_group + t_reference_year_set
                    var ctePanelRefYearSetGroupReferenceYearSet =
                        from a in ctePanelRefYearSetGroupPanel
                        join b in cteReferenceYearSet
                        on a.ReferenceYearSetId equals b.ReferenceYearSetId into lj
                        from b in lj.DefaultIfEmpty()
                        select new
                        {
                            a.Id,
                            a.PanelRefYearSetGroupId,
                            a.PanelId,
                            a.PanelName,
                            a.PanelLabel,
                            a.PanelClusterCount,
                            a.PanelPlotCount,
                            a.ReferenceYearSetId,
                            b?.ReferenceYearSetName,
                            b?.ReferenceYearSetLabel,
                            b?.ReferenceDateBegin,
                            b?.ReferenceDateEnd
                        };

                    if (ctePanelRefYearSetGroupReferenceYearSet.Any())
                    {
                        Data = ctePanelRefYearSetGroupReferenceYearSet
                            .OrderBy(a => a.PanelRefYearSetGroupId)
                            .Select(a =>
                                Data.LoadDataRow(
                                    values:
                                    [
                                        a.Id,
                                        a.PanelRefYearSetGroupId,
                                        a.PanelId,
                                        a.PanelName,
                                        a.PanelLabel,
                                        a.PanelClusterCount,
                                        a.PanelPlotCount,
                                        a.ReferenceYearSetId,
                                        a.ReferenceYearSetName,
                                        a.ReferenceYearSetLabel,
                                        a.ReferenceDateBegin,
                                        a.ReferenceDateEnd
                                    ],
                                    fAcceptChanges: false))
                            .CopyToDataTable();
                        Data.TableName = Name;
                    }
                    else
                    {
                        Data = EmptyDataTable(
                               tableName: TableName,
                               columns: Columns);
                    }

                    // Omezení podmínkou
                    if (!String.IsNullOrEmpty(value: Condition))
                    {
                        DataView view = new(table: Data);
                        try
                        {
                            view.RowFilter = Condition;
                        }
                        catch
                        {
                            view.RowFilter = String.Empty;
                            Condition = String.Empty;
                        }
                        Data = view.ToTable();
                    }

                    // Omezeni počtem
                    if (Limit != null)
                    {
                        Data = Data.AsEnumerable()
                            .Take(count: (int)Limit)
                            .CopyToDataTable();
                    }

                    stopWatch.Stop();
                    LoadingTime = 0.001 * stopWatch.ElapsedMilliseconds;
                    Data.TableName = TableName;
                }

                #endregion Methods

            }

        }
    }
}