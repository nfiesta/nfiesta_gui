﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {
            // fn_api_get_1pgroups4gregmap

            /// <summary>
            /// Distinct strata combinations and panel refyearset groups
            /// for each GREG-map total estimate in the set of desired estimates
            /// (return type of the stored procedure fn_api_get_1pgroups4gregmap)
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class TFnApiGetOnePGroupsForGRegMap(
                TFnApiGetOnePGroupsForGRegMapList composite = null,
                DataRow data = null)
                    : AParametrizedViewEntry<TFnApiGetOnePGroupsForGRegMapList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TFnApiGetOnePGroupsForGRegMapList.ColId.Name,
                            defaultValue: Int32.Parse(s: TFnApiGetOnePGroupsForGRegMapList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TFnApiGetOnePGroupsForGRegMapList.ColId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Estimation period identifier
                /// </summary>
                public Nullable<int> EstimationPeriodId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TFnApiGetOnePGroupsForGRegMapList.ColEstimationPeriodId.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnApiGetOnePGroupsForGRegMapList.ColEstimationPeriodId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TFnApiGetOnePGroupsForGRegMapList.ColEstimationPeriodId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TFnApiGetOnePGroupsForGRegMapList.ColEstimationPeriodId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Estimation period label in national language
                /// </summary>
                public string EstimationPeriodLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetOnePGroupsForGRegMapList.ColEstimationPeriodLabelCs.Name,
                            defaultValue: TFnApiGetOnePGroupsForGRegMapList.ColEstimationPeriodLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetOnePGroupsForGRegMapList.ColEstimationPeriodLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Estimation period description in national language
                /// </summary>
                public string EstimationPeriodDescriptionCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetOnePGroupsForGRegMapList.ColEstimationPeriodDescriptionCs.Name,
                            defaultValue: TFnApiGetOnePGroupsForGRegMapList.ColEstimationPeriodDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetOnePGroupsForGRegMapList.ColEstimationPeriodDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Estimation period extended label in national language (read-only)
                /// </summary>
                public string EstimationPeriodExtendedLabelCs
                {
                    get
                    {
                        return $"{Id} - {EstimationPeriodLabelCs}";
                    }
                }

                /// <summary>
                /// Estimation period label in English
                /// </summary>
                public string EstimationPeriodLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetOnePGroupsForGRegMapList.ColEstimationPeriodLabelEn.Name,
                            defaultValue: TFnApiGetOnePGroupsForGRegMapList.ColEstimationPeriodLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetOnePGroupsForGRegMapList.ColEstimationPeriodLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Estimation period description in English
                /// </summary>
                public string EstimationPeriodDescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetOnePGroupsForGRegMapList.ColEstimationPeriodDescriptionEn.Name,
                            defaultValue: TFnApiGetOnePGroupsForGRegMapList.ColEstimationPeriodDescriptionEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetOnePGroupsForGRegMapList.ColEstimationPeriodDescriptionEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Estimation period extended label in English (read-only)
                /// </summary>
                public string EstimationPeriodExtendedLabelEn
                {
                    get
                    {
                        return $"{Id} - {EstimationPeriodLabelEn}";
                    }
                }


                /// <summary>
                /// Country identifiers (as text)
                /// </summary>
                public string CountryId
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetOnePGroupsForGRegMapList.ColCountryId.Name,
                            defaultValue: TFnApiGetOnePGroupsForGRegMapList.ColCountryId.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetOnePGroupsForGRegMapList.ColCountryId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Country identifiers (as list)
                /// </summary>
                public List<Nullable<int>> CountryIdList
                {
                    get
                    {
                        return Functions.StringToNIntList(
                            text: CountryId,
                            separator: (char)59,
                            defaultValue: null);
                    }
                }

                /// <summary>
                /// Country labels in national language (as text)
                /// </summary>
                public string CountryLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetOnePGroupsForGRegMapList.ColCountryLabelCs.Name,
                            defaultValue: TFnApiGetOnePGroupsForGRegMapList.ColCountryLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetOnePGroupsForGRegMapList.ColCountryLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Country labels in national language (as list)
                /// </summary>
                public List<string> CountryLabelCsList
                {
                    get
                    {
                        return Functions.StringToStringList(
                            text: CountryLabelCs,
                            separator: (char)59);
                    }
                }

                /// <summary>
                /// Country descriptions in national language (as text)
                /// </summary>
                public string CountryDescriptionCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetOnePGroupsForGRegMapList.ColCountryDescriptionCs.Name,
                            defaultValue: TFnApiGetOnePGroupsForGRegMapList.ColCountryDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetOnePGroupsForGRegMapList.ColCountryDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Country descriptions in national language (as list)
                /// </summary>
                public List<string> CountryDescriptionCsList
                {
                    get
                    {
                        return Functions.StringToStringList(
                            text: CountryDescriptionCs,
                            separator: (char)59);

                    }
                }

                /// <summary>
                /// Country labels in English (as text)
                /// </summary>
                public string CountryLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetOnePGroupsForGRegMapList.ColCountryLabelEn.Name,
                            defaultValue: TFnApiGetOnePGroupsForGRegMapList.ColCountryLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetOnePGroupsForGRegMapList.ColCountryLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Country labels in English (as list)
                /// </summary>
                public List<string> CountryLabelEnList
                {
                    get
                    {
                        return Functions.StringToStringList(
                            text: CountryLabelEn,
                            separator: (char)59);

                    }
                }

                /// <summary>
                /// Country descriptions in English (as text)
                /// </summary>
                public string CountryDescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetOnePGroupsForGRegMapList.ColCountryDescriptionCs.Name,
                            defaultValue: TFnApiGetOnePGroupsForGRegMapList.ColCountryDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetOnePGroupsForGRegMapList.ColCountryDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Country descriptions in English (as list)
                /// </summary>
                public List<string> CountryDescriptionEnList
                {
                    get
                    {
                        return Functions.StringToStringList(
                            text: CountryDescriptionEn,
                            separator: (char)59);

                    }
                }


                /// <summary>
                /// Strata set identifiers (as text)
                /// </summary>
                public string StrataSetId
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetOnePGroupsForGRegMapList.ColStrataSetId.Name,
                            defaultValue: TFnApiGetOnePGroupsForGRegMapList.ColStrataSetId.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetOnePGroupsForGRegMapList.ColStrataSetId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Strata set identifiers (as list)
                /// </summary>
                public List<Nullable<int>> StrataSetIdList
                {
                    get
                    {
                        return Functions.StringToNIntList(
                            text: StrataSetId,
                            separator: (char)59,
                            defaultValue: null);
                    }
                }

                /// <summary>
                /// Strata set labels in national language (as text)
                /// </summary>
                public string StrataSetLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetOnePGroupsForGRegMapList.ColStrataSetLabelCs.Name,
                            defaultValue: TFnApiGetOnePGroupsForGRegMapList.ColStrataSetLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetOnePGroupsForGRegMapList.ColStrataSetLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Strata set labels in national language (as list)
                /// </summary>
                public List<string> StrataSetLabelCsList
                {
                    get
                    {
                        return Functions.StringToStringList(
                            text: StrataSetLabelCs,
                            separator: (char)59);

                    }
                }

                /// <summary>
                /// Strata set descriptions in national language (as text)
                /// </summary>
                public string StrataSetDescriptionCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetOnePGroupsForGRegMapList.ColStrataSetDescriptionCs.Name,
                            defaultValue: TFnApiGetOnePGroupsForGRegMapList.ColStrataSetDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetOnePGroupsForGRegMapList.ColStrataSetDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Strata set descriptions in national language (as list)
                /// </summary>
                public List<string> StrataSetDescriptionCsList
                {
                    get
                    {
                        return Functions.StringToStringList(
                            text: StrataSetDescriptionCs,
                            separator: (char)59);

                    }
                }

                /// <summary>
                /// Strata set labels in English (as text)
                /// </summary>
                public string StrataSetLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetOnePGroupsForGRegMapList.ColStrataSetLabelEn.Name,
                            defaultValue: TFnApiGetOnePGroupsForGRegMapList.ColStrataSetLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetOnePGroupsForGRegMapList.ColStrataSetLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Strata set labels in English (as list)
                /// </summary>
                public List<string> StrataSetLabelEnList
                {
                    get
                    {
                        return Functions.StringToStringList(
                            text: StrataSetLabelEn,
                            separator: (char)59);

                    }
                }

                /// <summary>
                /// Strata set descriptions in English (as text)
                /// </summary>
                public string StrataSetDescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetOnePGroupsForGRegMapList.ColStrataSetDescriptionEn.Name,
                            defaultValue: TFnApiGetOnePGroupsForGRegMapList.ColStrataSetDescriptionEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetOnePGroupsForGRegMapList.ColStrataSetDescriptionEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Strata set descriptions in English (as list)
                /// </summary>
                public List<string> StrataSetDescriptionEnList
                {
                    get
                    {
                        return Functions.StringToStringList(
                            text: StrataSetDescriptionEn,
                            separator: (char)59);

                    }
                }


                /// <summary>
                /// Stratum identifiers (as text)
                /// </summary>
                public string StratumId
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetOnePGroupsForGRegMapList.ColStratumId.Name,
                            defaultValue: TFnApiGetOnePGroupsForGRegMapList.ColStratumId.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetOnePGroupsForGRegMapList.ColStratumId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Stratum identifiers (as list)
                /// </summary>
                public List<Nullable<int>> StratumIdList
                {
                    get
                    {
                        return Functions.StringToNIntList(
                            text: StratumId,
                            separator: (char)59,
                            defaultValue: null);
                    }
                }

                /// <summary>
                /// Stratum labels in national language (as text)
                /// </summary>
                public string StratumLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetOnePGroupsForGRegMapList.ColStratumLabelCs.Name,
                            defaultValue: TFnApiGetOnePGroupsForGRegMapList.ColStratumLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetOnePGroupsForGRegMapList.ColStratumLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Stratum labels in national language (as list)
                /// </summary>
                public List<string> StratumLabelCsList
                {
                    get
                    {
                        return Functions.StringToStringList(
                            text: StratumLabelCs,
                            separator: (char)59);

                    }
                }

                /// <summary>
                /// Stratum descriptions in national language (as text)
                /// </summary>
                public string StratumDescriptionCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetOnePGroupsForGRegMapList.ColStratumDescriptionCs.Name,
                            defaultValue: TFnApiGetOnePGroupsForGRegMapList.ColStratumDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetOnePGroupsForGRegMapList.ColStratumDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Stratum descriptions in national language (as list)
                /// </summary>
                public List<string> StratumDescriptionCsList
                {
                    get
                    {
                        return Functions.StringToStringList(
                            text: StratumDescriptionCs,
                            separator: (char)59);

                    }
                }

                /// <summary>
                /// Stratum labels in English (as text)
                /// </summary>
                public string StratumLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetOnePGroupsForGRegMapList.ColStratumLabelEn.Name,
                            defaultValue: TFnApiGetOnePGroupsForGRegMapList.ColStratumLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetOnePGroupsForGRegMapList.ColStratumLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Stratum labels in English (as list)
                /// </summary>
                public List<string> StratumLabelEnList
                {
                    get
                    {
                        return Functions.StringToStringList(
                            text: StratumLabelEn,
                            separator: (char)59);

                    }
                }

                /// <summary>
                /// Stratum descriptions in English (as text)
                /// </summary>
                public string StratumDescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetOnePGroupsForGRegMapList.ColStratumDescriptionCs.Name,
                            defaultValue: TFnApiGetOnePGroupsForGRegMapList.ColStratumDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetOnePGroupsForGRegMapList.ColStratumDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Stratum descriptions in English (as list)
                /// </summary>
                public List<string> StratumDescriptionEnList
                {
                    get
                    {
                        return Functions.StringToStringList(
                            text: StratumDescriptionEn,
                            separator: (char)59);

                    }
                }


                /// <summary>
                /// Panel reference year set group identifier
                /// </summary>
                public Nullable<int> PanelRefYearSetGroupId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TFnApiGetOnePGroupsForGRegMapList.ColPanelRefYearSetGroupId.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnApiGetOnePGroupsForGRegMapList.ColPanelRefYearSetGroupId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TFnApiGetOnePGroupsForGRegMapList.ColPanelRefYearSetGroupId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TFnApiGetOnePGroupsForGRegMapList.ColPanelRefYearSetGroupId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Panel reference year set group label in national language
                /// </summary>
                public string PanelRefYearSetGroupLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetOnePGroupsForGRegMapList.ColPanelRefYearSetGroupLabelCs.Name,
                            defaultValue: TFnApiGetOnePGroupsForGRegMapList.ColPanelRefYearSetGroupLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetOnePGroupsForGRegMapList.ColPanelRefYearSetGroupLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Panel reference year set group description in national language
                /// </summary>
                public string PanelRefYearSetGroupDescriptionCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetOnePGroupsForGRegMapList.ColPanelRefYearSetGroupDescriptionCs.Name,
                            defaultValue: TFnApiGetOnePGroupsForGRegMapList.ColPanelRefYearSetGroupDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetOnePGroupsForGRegMapList.ColPanelRefYearSetGroupDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Panel reference year set group extended label in national language (read-only)
                /// </summary>
                public string PanelRefYearSetGroupExtendedLabelCs
                {
                    get
                    {
                        return $"{Id} - {PanelRefYearSetGroupLabelCs}";
                    }
                }

                /// <summary>
                /// Panel reference year set group label in English
                /// </summary>
                public string PanelRefYearSetGroupLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetOnePGroupsForGRegMapList.ColPanelRefYearSetGroupLabelEn.Name,
                            defaultValue: TFnApiGetOnePGroupsForGRegMapList.ColPanelRefYearSetGroupLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetOnePGroupsForGRegMapList.ColPanelRefYearSetGroupLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Panel reference year set group description in English
                /// </summary>
                public string PanelRefYearSetGroupDescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetOnePGroupsForGRegMapList.ColPanelRefYearSetGroupDescriptionEn.Name,
                            defaultValue: TFnApiGetOnePGroupsForGRegMapList.ColPanelRefYearSetGroupDescriptionEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetOnePGroupsForGRegMapList.ColPanelRefYearSetGroupDescriptionEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Panel reference year set group extended label in English (read-only)
                /// </summary>
                public string PanelRefYearSetGroupExtendedLabelEn
                {
                    get
                    {
                        return $"{Id} - {PanelRefYearSetGroupLabelEn}";
                    }
                }


                /// <summary>
                /// Complete group
                /// </summary>
                public Nullable<bool> CompleteGroup
                {
                    get
                    {
                        return Functions.GetNBoolArg(
                            row: Data,
                            name: TFnApiGetOnePGroupsForGRegMapList.ColCompleteGroup.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnApiGetOnePGroupsForGRegMapList.ColCompleteGroup.DefaultValue) ?
                                (Nullable<bool>)null :
                                Boolean.Parse(value: TFnApiGetOnePGroupsForGRegMapList.ColCompleteGroup.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNBoolArg(
                            row: Data,
                            name: TFnApiGetOnePGroupsForGRegMapList.ColCompleteGroup.Name,
                            val: value);
                    }
                }


                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not TFnApiGetOnePGroupsForGRegMap, return False
                    if (obj is not TFnApiGetOnePGroupsForGRegMap)
                    {
                        return false;
                    }

                    return
                        Id == ((TFnApiGetOnePGroupsForGRegMap)obj).Id;
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                /// <summary>
                /// Text description of the object
                /// </summary>
                /// <returns>Text description of the object</returns>
                public override string ToString()
                {
                    return ((NfiEstaDB)Composite.Database).Postgres.Setting.LanguageVersion switch
                    {
                        LanguageVersion.International =>
                            String.Concat($"{"ID"} = {Functions.PrepIntArg(arg: Id)}.{Environment.NewLine}"),
                        LanguageVersion.National =>
                            String.Concat($"{"ID"} = {Functions.PrepIntArg(arg: Id)}.{Environment.NewLine}"),
                        _ =>
                            String.Concat($"{"ID"} = {Functions.PrepIntArg(arg: Id)}.{Environment.NewLine}"),
                    };
                }

                #endregion Methods

            }


            /// <summary>
            /// List of distinct strata combinations and panel refyearset groups
            /// for each GREG-map total estimate in the set of desired estimates
            /// (return type of the stored procedure fn_api_get_1pgroups4gregmap)
            /// </summary>
            public class TFnApiGetOnePGroupsForGRegMapList
                 : AParametrizedView
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema =
                    NfiEstaFunctions.FnApiGetOnePGroupsForGRegMap.SchemaName;

                /// <summary>
                /// Stored procedure name
                /// </summary>
                public static readonly string Name =
                    NfiEstaFunctions.FnApiGetOnePGroupsForGRegMap.Name;

                /// <summary>
                /// Stored procedure alias
                /// </summary>
                public static readonly string Alias =
                    NfiEstaFunctions.FnApiGetOnePGroupsForGRegMap.Name;

                /// <summary>
                /// Description in national language
                /// </summary>
                public static readonly string CaptionCs = String.Concat(
                    "Seznam kombinací strat a skupin panelů pro každý 2p-reg odhad úhrnu ",
                    "(návratový typ uložené procedury fn_api_get_1pgroups4gregmap)");

                /// <summary>
                /// Description in English
                /// </summary>
                public static readonly string CaptionEn = String.Concat(
                    "List of distinct strata combinations and panel refyearset groups ",
                    "for each GREG-map total estimate in the set of desired estimates ",
                    "(return type of the stored procedure fn_api_get_1pgroups4gregmap)");

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
                {
                    { "id", new ColumnMetadata()
                    {
                        Name = "id",
                        DbName = "id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = true,
                        NotNull = true,
                        DefaultValue = "0",
                        FuncCall = "row_number() over()",
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ID",
                        HeaderTextEn = "ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 0
                    }
                    },

                    { "estimation_period_id", new ColumnMetadata()
                    {
                        Name = "estimation_period_id",
                        DbName = "estimation_period_id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATION_PERIOD_ID",
                        HeaderTextEn = "ESTIMATION_PERIOD_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 1
                    }
                    },
                    { "estimation_period_label_cs", new ColumnMetadata()
                    {
                        Name = "estimation_period_label_cs",
                        DbName = "estimation_period_label",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATION_PERIOD_LABEL_CS",
                        HeaderTextEn = "ESTIMATION_PERIOD_LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 2
                    }
                    },
                    { "estimation_period_description_cs", new ColumnMetadata()
                    {
                        Name = "estimation_period_description_cs",
                        DbName = "estimation_period_description",
                        DataType = "System.String",
                        DbDataType = "text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATION_PERIOD_DESCRIPTION_CS",
                        HeaderTextEn = "ESTIMATION_PERIOD_DESCRIPTION_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 3
                    }
                    },
                    { "estimation_period_label_en", new ColumnMetadata()
                    {
                        Name = "estimation_period_label_en",
                        DbName = "estimation_period_label_en",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATION_PERIOD_LABEL_EN",
                        HeaderTextEn = "ESTIMATION_PERIOD_LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 4
                    }
                    },
                    { "estimation_period_description_en", new ColumnMetadata()
                    {
                        Name = "estimation_period_description_en",
                        DbName = "estimation_period_description_en",
                        DataType = "System.String",
                        DbDataType = "text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATION_PERIOD_DESCRIPTION_EN",
                        HeaderTextEn = "ESTIMATION_PERIOD_DESCRIPTION_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 5
                    }
                    },

                    { "country_id", new ColumnMetadata()
                    {
                        Name = "country_id",
                        DbName = "country_id",
                        DataType = "System.String",
                        DbDataType = "_int4",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = "array_to_string(country_id, ';', 'NULL')",
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "COUNTRY_ID",
                        HeaderTextEn = "COUNTRY_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 6
                    }
                    },
                    { "country_label_cs", new ColumnMetadata()
                    {
                        Name = "country_label_cs",
                        DbName = "country_label",
                        DataType = "System.String",
                        DbDataType = "_varchar",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = "array_to_string(country_label, ';', 'NULL')",
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "COUNTRY_LABEL_CS",
                        HeaderTextEn = "COUNTRY_LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 7
                    }
                    },
                    { "country_description_cs", new ColumnMetadata()
                    {
                        Name = "country_description_cs",
                        DbName = "country_description",
                        DataType = "System.String",
                        DbDataType = "_text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = "array_to_string(country_description, ';', 'NULL')",
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "COUNTRY_DESCRIPTION_CS",
                        HeaderTextEn = "COUNTRY_DESCRIPTION_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 8
                    }
                    },
                    { "country_label_en", new ColumnMetadata()
                    {
                        Name = "country_label_en",
                        DbName = "country_label_en",
                        DataType = "System.String",
                        DbDataType = "_varchar",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = "array_to_string(country_label_en, ';', 'NULL')",
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "COUNTRY_LABEL_EN",
                        HeaderTextEn = "COUNTRY_LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 9
                    }
                    },
                    { "country_description_en", new ColumnMetadata()
                    {
                        Name = "country_description_en",
                        DbName = "country_description_en",
                        DataType = "System.String",
                        DbDataType = "_text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = "array_to_string(country_description_en, ';', 'NULL')",
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "COUNTRY_DESCRIPTION_EN",
                        HeaderTextEn = "COUNTRY_DESCRIPTION_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 10
                    }
                    },

                    { "strata_set_id", new ColumnMetadata()
                    {
                        Name = "strata_set_id",
                        DbName = "strata_set_id",
                        DataType = "System.String",
                        DbDataType = "_int4",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = "array_to_string(strata_set_id, ';', 'NULL')",
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "STRATA_SET_ID",
                        HeaderTextEn = "STRATA_SET_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 11
                    }
                    },
                    { "strata_set_label_cs", new ColumnMetadata()
                    {
                        Name = "strata_set_label_cs",
                        DbName = "strata_set_label",
                        DataType = "System.String",
                        DbDataType = "_varchar",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = "array_to_string(strata_set_label, ';', 'NULL')",
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "STRATA_SET_LABEL_CS",
                        HeaderTextEn = "STRATA_SET_LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 12
                    }
                    },
                    { "strata_set_description_cs", new ColumnMetadata()
                    {
                        Name = "strata_set_description_cs",
                        DbName = "strata_set_description",
                        DataType = "System.String",
                        DbDataType = "_text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = "array_to_string(strata_set_description, ';', 'NULL')",
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "STRATA_SET_DESCRIPTION_CS",
                        HeaderTextEn = "STRATA_SET_DESCRIPTION_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 13
                    }
                    },
                    { "strata_set_label_en", new ColumnMetadata()
                    {
                        Name = "strata_set_label_en",
                        DbName = "strata_set_label_en",
                        DataType = "System.String",
                        DbDataType = "_varchar",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = "array_to_string(strata_set_label_en, ';', 'NULL')",
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "STRATA_SET_LABEL_EN",
                        HeaderTextEn = "STRATA_SET_LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 14
                    }
                    },
                    { "strata_set_description_en", new ColumnMetadata()
                    {
                        Name = "strata_set_description_en",
                        DbName = "strata_set_description_en",
                        DataType = "System.String",
                        DbDataType = "_text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = "array_to_string(strata_set_description_en, ';', 'NULL')",
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "STRATA_SET_DESCRIPTION_EN",
                        HeaderTextEn = "STRATA_SET_DESCRIPTION_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 15
                    }
                    },

                    { "stratum_id", new ColumnMetadata()
                    {
                        Name = "stratum_id",
                        DbName = "stratum_id",
                        DataType = "System.String",
                        DbDataType = "_int4",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = "array_to_string(stratum_id, ';', 'NULL')",
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "STRATUM_ID",
                        HeaderTextEn = "STRATUM_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 16
                    }
                    },
                    { "stratum_label_cs", new ColumnMetadata()
                    {
                        Name = "stratum_label_cs",
                        DbName = "stratum_label",
                        DataType = "System.String",
                        DbDataType = "_varchar",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = "array_to_string(stratum_label, ';', 'NULL')",
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "STRATUM_LABEL_CS",
                        HeaderTextEn = "STRATUM_LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 17
                    }
                    },
                    { "stratum_description_cs", new ColumnMetadata()
                    {
                        Name = "stratum_description_cs",
                        DbName = "stratum_description",
                        DataType = "System.String",
                        DbDataType = "_text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = "array_to_string(stratum_description, ';', 'NULL')",
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "STRATUM_DESCRIPTION_CS",
                        HeaderTextEn = "STRATUM_DESCRIPTION_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 18
                    }
                    },
                    { "stratum_label_en", new ColumnMetadata()
                    {
                        Name = "stratum_label_en",
                        DbName = "stratum_label_en",
                        DataType = "System.String",
                        DbDataType = "_varchar",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = "array_to_string(stratum_label_en, ';', 'NULL')",
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "STRATUM_LABEL_EN",
                        HeaderTextEn = "STRATUM_LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 19
                    }
                    },
                    { "stratum_description_en", new ColumnMetadata()
                    {
                        Name = "stratum_description_en",
                        DbName = "stratum_description_en",
                        DataType = "System.String",
                        DbDataType = "_text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = "array_to_string(stratum_description_en, ';', 'NULL')",
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "STRATUM_DESCRIPTION_EN",
                        HeaderTextEn = "STRATUM_DESCRIPTION_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 20
                    }
                    },

                    { "panel_refyearset_group_id", new ColumnMetadata()
                    {
                        Name = "panel_refyearset_group_id",
                        DbName = "panel_refyearset_group_id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PANEL_REFYEARSET_GROUP_ID",
                        HeaderTextEn = "PANEL_REFYEARSET_GROUP_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 21
                    }
                    },
                    { "panel_refyearset_group_label_cs", new ColumnMetadata()
                    {
                        Name = "panel_refyearset_group_label_cs",
                        DbName = "panel_refyearset_group_label",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PANEL_REFYEARSET_GROUP_LABEL_CS",
                        HeaderTextEn = "PANEL_REFYEARSET_GROUP_LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 22
                    }
                    },
                    { "panel_refyearset_group_description_cs", new ColumnMetadata()
                    {
                        Name = "panel_refyearset_group_description_cs",
                        DbName = "panel_refyearset_group_description",
                        DataType = "System.String",
                        DbDataType = "text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PANEL_REFYEARSETGROUP_DESCRIPTION_CS",
                        HeaderTextEn = "PANEL_REFYEARSETGROUP_DESCRIPTION_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 23
                    }
                    },
                    { "panel_refyearset_group_label_en", new ColumnMetadata()
                    {
                        Name = "panel_refyearset_group_label_en",
                        DbName = "panel_refyearset_group_label_en",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PANEL_REFYEARSET_GROUP_LABEL_EN",
                        HeaderTextEn = "PANEL_REFYEARSET_GROUP_LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 24
                    }
                    },
                    { "panel_refyearset_group_description_en", new ColumnMetadata()
                    {
                        Name = "panel_refyearset_group_description_en",
                        DbName = "panel_refyearset_group_description_en",
                        DataType = "System.String",
                        DbDataType = "text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PANEL_REFYEARSET_GROUP_DESCRIPTION_EN",
                        HeaderTextEn = "PANEL_REFYEARSET_GROUP_DESCRIPTION_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 25
                    }
                    },

                    { "complete_group", new ColumnMetadata()
                    {
                        Name = "complete_group",
                        DbName = "complete_group",
                        DataType = "System.Boolean",
                        DbDataType = "bool",
                        NewDataType = "bool",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "COMPLETE_GROUP",
                        HeaderTextEn = "COMPLETE_GROUP",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 26
                    }
                    }
                };


                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];


                /// <summary>
                /// Column estimation_period_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationPeriodId = Cols["estimation_period_id"];

                /// <summary>
                /// Column estimation_period_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationPeriodLabelCs = Cols["estimation_period_label_cs"];

                /// <summary>
                /// Column estimation_period_description_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationPeriodDescriptionCs = Cols["estimation_period_description_cs"];

                /// <summary>
                /// Column estimation_period_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationPeriodLabelEn = Cols["estimation_period_label_en"];

                /// <summary>
                /// Column estimation_period_description_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationPeriodDescriptionEn = Cols["estimation_period_description_en"];


                /// <summary>
                /// Column country_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColCountryId = Cols["country_id"];

                /// <summary>
                /// Column country_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColCountryLabelCs = Cols["country_label_cs"];

                /// <summary>
                /// Column country_description_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColCountryDescriptionCs = Cols["country_description_cs"];

                /// <summary>
                /// Column country_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColCountryLabelEn = Cols["country_label_en"];

                /// <summary>
                /// Column country_description_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColCountryDescriptionEn = Cols["country_description_en"];


                /// <summary>
                /// Column strata_set_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColStrataSetId = Cols["strata_set_id"];

                /// <summary>
                /// Column strata_set_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColStrataSetLabelCs = Cols["strata_set_label_cs"];

                /// <summary>
                /// Column strata_set_description_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColStrataSetDescriptionCs = Cols["strata_set_description_cs"];

                /// <summary>
                /// Column strata_set_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColStrataSetLabelEn = Cols["strata_set_label_en"];

                /// <summary>
                /// Column strata_set_description_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColStrataSetDescriptionEn = Cols["strata_set_description_en"];


                /// <summary>
                /// Column stratum_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColStratumId = Cols["stratum_id"];

                /// <summary>
                /// Column stratum_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColStratumLabelCs = Cols["stratum_label_cs"];

                /// <summary>
                /// Column stratum_description_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColStratumDescriptionCs = Cols["stratum_description_cs"];

                /// <summary>
                /// Column stratum_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColStratumLabelEn = Cols["stratum_label_en"];

                /// <summary>
                /// Column stratum_description_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColStratumDescriptionEn = Cols["stratum_description_en"];


                /// <summary>
                /// Column panel_refyearset_group_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColPanelRefYearSetGroupId = Cols["panel_refyearset_group_id"];

                /// <summary>
                /// Column panel_refyearset_group_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColPanelRefYearSetGroupLabelCs = Cols["panel_refyearset_group_label_cs"];

                /// <summary>
                /// Column panel_refyearset_group_description_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColPanelRefYearSetGroupDescriptionCs = Cols["panel_refyearset_group_description_cs"];

                /// <summary>
                /// Column panel_refyearset_group_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColPanelRefYearSetGroupLabelEn = Cols["panel_refyearset_group_label_en"];

                /// <summary>
                /// Column panel_refyearset_group_description_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColPanelRefYearSetGroupDescriptionEn = Cols["panel_refyearset_group_description_en"];


                /// <summary>
                /// Column complete_group metadata
                /// </summary>
                public static readonly ColumnMetadata ColCompleteGroup = Cols["complete_group"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Private Fields

                /// <summary>
                /// 1st parameter: Estimation period identifier
                /// </summary>
                private Nullable<int> estimationPeriodId;

                /// <summary>
                /// 2nd parameter: List of estimation cells identifiers
                /// </summary>
                private List<Nullable<int>> estimationCellIds;

                /// <summary>
                /// 3rd parameter:List of attribute categories identifiers
                /// </summary>
                private List<Nullable<int>> variableIds;

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TFnApiGetOnePGroupsForGRegMapList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                {
                    EstimationPeriodId = null;
                    EstimationCellIds = [];
                    VariableIds = [];
                }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TFnApiGetOnePGroupsForGRegMapList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                {
                    EstimationPeriodId = null;
                    EstimationCellIds = [];
                    VariableIds = [];
                }

                #endregion Constructor


                #region Properties


                /// <summary>
                /// 1st parameter: Estimation period identifier
                /// </summary>
                public Nullable<int> EstimationPeriodId
                {
                    get
                    {
                        return estimationPeriodId;
                    }
                    set
                    {
                        estimationPeriodId = value;
                    }
                }

                /// <summary>
                /// 2nd parameter: List of estimation cells identifiers
                /// </summary>
                public List<Nullable<int>> EstimationCellIds
                {
                    get
                    {
                        return estimationCellIds ?? [];
                    }
                    set
                    {
                        estimationCellIds = value ?? [];
                    }
                }

                /// <summary>
                /// 3rd parameter:List of attribute categories identifiers
                /// </summary>
                public List<Nullable<int>> VariableIds
                {
                    get
                    {
                        return variableIds ?? [];
                    }
                    set
                    {
                        variableIds = value ?? [];
                    }
                }

                /// <summary>
                /// List of items (read-only)
                /// </summary>
                public List<TFnApiGetOnePGroupsForGRegMap> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new TFnApiGetOnePGroupsForGRegMap(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexers

                /// <summary>
                /// Row from list of rows by identifier (read-only)
                /// </summary>
                /// <param name="id">Row identifier</param>
                /// <returns>Row from list of rows by identifier (null if not found)</returns>
                public TFnApiGetOnePGroupsForGRegMap this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new TFnApiGetOnePGroupsForGRegMap(composite: this, data: a))
                                .FirstOrDefault<TFnApiGetOnePGroupsForGRegMap>();
                    }
                }

                #endregion Indexers


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public TFnApiGetOnePGroupsForGRegMapList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any()
                        ? new TFnApiGetOnePGroupsForGRegMapList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime)
                        {
                            EstimationPeriodId = EstimationPeriodId,
                            EstimationCellIds = [.. EstimationCellIds],
                            VariableIds = [.. VariableIds],
                        }
                        : new TFnApiGetOnePGroupsForGRegMapList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null)
                        {
                            EstimationPeriodId = EstimationPeriodId,
                            EstimationCellIds = [.. EstimationCellIds],
                            VariableIds = [.. VariableIds],
                        };
                }

                #endregion Methods

            }

        }
    }
}