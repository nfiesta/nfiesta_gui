﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {
            // fn_api_get_gregmap_configs4ratio

            /// <summary>
            /// Valid combination of panel-refyearset group, param. area type,
            /// force synthetic, model and sigma of already configured GREG-map totals
            /// (return type of the stored procedure fn_api_get_gregmap_configs4ratio)
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class TFnApiGetGRegMapConfigsForRatio(
                TFnApiGetGRegMapConfigsForRatioList composite = null,
                DataRow data = null)
                    : AParametrizedViewEntry<TFnApiGetGRegMapConfigsForRatioList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TFnApiGetGRegMapConfigsForRatioList.ColId.Name,
                            defaultValue: Int32.Parse(s: TFnApiGetGRegMapConfigsForRatioList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TFnApiGetGRegMapConfigsForRatioList.ColId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Panel reference year set group identifier
                /// </summary>
                public Nullable<int> PanelRefYearSetGroupId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TFnApiGetGRegMapConfigsForRatioList.ColPanelRefYearSetGroupId.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnApiGetGRegMapConfigsForRatioList.ColPanelRefYearSetGroupId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TFnApiGetGRegMapConfigsForRatioList.ColPanelRefYearSetGroupId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TFnApiGetGRegMapConfigsForRatioList.ColPanelRefYearSetGroupId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Panel reference year set group label in national language
                /// </summary>
                public string PanelRefYearSetGroupLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetGRegMapConfigsForRatioList.ColPanelRefYearSetGroupLabelCs.Name,
                            defaultValue: TFnApiGetGRegMapConfigsForRatioList.ColPanelRefYearSetGroupLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetGRegMapConfigsForRatioList.ColPanelRefYearSetGroupLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Panel reference year set group description in national language
                /// </summary>
                public string PanelRefYearSetGroupDescriptionCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetGRegMapConfigsForRatioList.ColPanelRefYearSetGroupDescriptionCs.Name,
                            defaultValue: TFnApiGetGRegMapConfigsForRatioList.ColPanelRefYearSetGroupDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetGRegMapConfigsForRatioList.ColPanelRefYearSetGroupDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Panel reference year set group extended label in national language (read-only)
                /// </summary>
                public string PanelRefYearSetGroupExtendedLabelCs
                {
                    get
                    {
                        return
                            (PanelRefYearSetGroupId == null)
                                ? String.IsNullOrEmpty(value: PanelRefYearSetGroupLabelCs)
                                    ? String.Empty
                                    : PanelRefYearSetGroupLabelCs
                                : String.IsNullOrEmpty(value: PanelRefYearSetGroupLabelCs)
                                    ? $"{PanelRefYearSetGroupId}"
                                    : $"{PanelRefYearSetGroupId} - {PanelRefYearSetGroupLabelCs}";
                    }
                }

                /// <summary>
                /// Panel reference year set group label in English
                /// </summary>
                public string PanelRefYearSetGroupLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetGRegMapConfigsForRatioList.ColPanelRefYearSetGroupLabelEn.Name,
                            defaultValue: TFnApiGetGRegMapConfigsForRatioList.ColPanelRefYearSetGroupLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetGRegMapConfigsForRatioList.ColPanelRefYearSetGroupLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Panel reference year set group description in English
                /// </summary>
                public string PanelRefYearSetGroupDescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetGRegMapConfigsForRatioList.ColPanelRefYearSetGroupDescriptionEn.Name,
                            defaultValue: TFnApiGetGRegMapConfigsForRatioList.ColPanelRefYearSetGroupDescriptionEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetGRegMapConfigsForRatioList.ColPanelRefYearSetGroupDescriptionEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Panel reference year set group extended label in English (read-only)
                /// </summary>
                public string PanelRefYearSetGroupExtendedLabelEn
                {
                    get
                    {
                        return
                            (PanelRefYearSetGroupId == null)
                                ? String.IsNullOrEmpty(value: PanelRefYearSetGroupLabelEn)
                                    ? String.Empty
                                    : PanelRefYearSetGroupLabelEn
                                : String.IsNullOrEmpty(value: PanelRefYearSetGroupLabelEn)
                                    ? $"{PanelRefYearSetGroupId}"
                                    : $"{PanelRefYearSetGroupId} - {PanelRefYearSetGroupLabelEn}";
                    }
                }


                /// <summary>
                /// Parametrization area type identifier
                /// </summary>
                public Nullable<int> ParamAreaTypeId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TFnApiGetGRegMapConfigsForRatioList.ColParamAreaTypeId.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnApiGetGRegMapConfigsForRatioList.ColParamAreaTypeId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TFnApiGetGRegMapConfigsForRatioList.ColParamAreaTypeId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TFnApiGetGRegMapConfigsForRatioList.ColParamAreaTypeId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Parametrization area type label in national language
                /// </summary>
                public string ParamAreaTypeLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetGRegMapConfigsForRatioList.ColParamAreaTypeLabelCs.Name,
                            defaultValue: TFnApiGetGRegMapConfigsForRatioList.ColParamAreaTypeLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetGRegMapConfigsForRatioList.ColParamAreaTypeLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Parametrization area type description in national language
                /// </summary>
                public string ParamAreaTypeDescriptionCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetGRegMapConfigsForRatioList.ColParamAreaTypeDescriptionCs.Name,
                            defaultValue: TFnApiGetGRegMapConfigsForRatioList.ColParamAreaTypeDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetGRegMapConfigsForRatioList.ColParamAreaTypeDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Parametrization area type extended label in national language (read-only)
                /// </summary>
                public string ParamAreaTypeExtendedLabelCs
                {
                    get
                    {
                        return
                            (ParamAreaTypeId == null)
                                ? String.IsNullOrEmpty(value: ParamAreaTypeLabelCs)
                                    ? String.Empty
                                    : ParamAreaTypeLabelCs
                                : String.IsNullOrEmpty(value: ParamAreaTypeLabelCs)
                                    ? $"{ParamAreaTypeId}"
                                    : $"{ParamAreaTypeId} - {ParamAreaTypeLabelCs}";
                    }
                }

                /// <summary>
                /// Parametrization area type label in English
                /// </summary>
                public string ParamAreaTypeLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetGRegMapConfigsForRatioList.ColParamAreaTypeLabelEn.Name,
                            defaultValue: TFnApiGetGRegMapConfigsForRatioList.ColParamAreaTypeLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetGRegMapConfigsForRatioList.ColParamAreaTypeLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Parametrization area type description in English
                /// </summary>
                public string ParamAreaTypeDescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetGRegMapConfigsForRatioList.ColParamAreaTypeDescriptionEn.Name,
                            defaultValue: TFnApiGetGRegMapConfigsForRatioList.ColParamAreaTypeDescriptionEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetGRegMapConfigsForRatioList.ColParamAreaTypeDescriptionEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Parametrization area type extended label in English (read-only)
                /// </summary>
                public string ParamAreaTypeExtendedLabelEn
                {
                    get
                    {
                        return
                            (ParamAreaTypeId == null)
                                ? String.IsNullOrEmpty(value: ParamAreaTypeLabelEn)
                                    ? String.Empty
                                    : ParamAreaTypeLabelEn
                                : String.IsNullOrEmpty(value: ParamAreaTypeLabelEn)
                                    ? $"{ParamAreaTypeId}"
                                    : $"{ParamAreaTypeId} - {ParamAreaTypeLabelEn}";
                    }
                }

                /// <summary>
                /// Force synthetic estimate
                /// </summary>
                public Nullable<bool> ForceSynthetic
                {
                    get
                    {
                        return Functions.GetNBoolArg(
                            row: Data,
                            name: TFnApiGetGRegMapConfigsForRatioList.ColForceSynthetic.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnApiGetGRegMapConfigsForRatioList.ColForceSynthetic.DefaultValue) ?
                                (Nullable<bool>)null :
                                Boolean.Parse(value: TFnApiGetGRegMapConfigsForRatioList.ColForceSynthetic.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNBoolArg(
                            row: Data,
                            name: TFnApiGetGRegMapConfigsForRatioList.ColForceSynthetic.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Numerator model identifier
                /// </summary>
                public Nullable<int> NumeratorModelId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TFnApiGetGRegMapConfigsForRatioList.ColNumeratorModelId.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnApiGetGRegMapConfigsForRatioList.ColNumeratorModelId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TFnApiGetGRegMapConfigsForRatioList.ColNumeratorModelId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TFnApiGetGRegMapConfigsForRatioList.ColNumeratorModelId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Numerator model label in national language
                /// </summary>
                public string NumeratorModelLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetGRegMapConfigsForRatioList.ColNumeratorModelLabelCs.Name,
                            defaultValue: TFnApiGetGRegMapConfigsForRatioList.ColNumeratorModelLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetGRegMapConfigsForRatioList.ColNumeratorModelLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Numerator model description in national language
                /// </summary>
                public string NumeratorModelDescriptionCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetGRegMapConfigsForRatioList.ColNumeratorModelDescriptionCs.Name,
                            defaultValue: TFnApiGetGRegMapConfigsForRatioList.ColNumeratorModelDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetGRegMapConfigsForRatioList.ColNumeratorModelDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Numerator model extended label in national language (read-only)
                /// </summary>
                public string NumeratorModelExtendedLabelCs
                {
                    get
                    {
                        return
                            (NumeratorModelId == null)
                                ? String.IsNullOrEmpty(value: NumeratorModelLabelCs)
                                    ? String.Empty
                                    : NumeratorModelLabelCs
                                : String.IsNullOrEmpty(value: NumeratorModelLabelCs)
                                    ? $"{NumeratorModelId}"
                                    : $"{NumeratorModelId} - {NumeratorModelLabelCs}";
                    }
                }

                /// <summary>
                /// Numerator model label in English
                /// </summary>
                public string NumeratorModelLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetGRegMapConfigsForRatioList.ColNumeratorModelLabelEn.Name,
                            defaultValue: TFnApiGetGRegMapConfigsForRatioList.ColNumeratorModelLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetGRegMapConfigsForRatioList.ColNumeratorModelLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Numerator model description in English
                /// </summary>
                public string NumeratorModelDescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetGRegMapConfigsForRatioList.ColNumeratorModelDescriptionEn.Name,
                            defaultValue: TFnApiGetGRegMapConfigsForRatioList.ColNumeratorModelDescriptionEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetGRegMapConfigsForRatioList.ColNumeratorModelDescriptionEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Numerator model extended label in English (read-only)
                /// </summary>
                public string NumeratorModelExtendedLabelEn
                {
                    get
                    {
                        return
                            (NumeratorModelId == null)
                                ? String.IsNullOrEmpty(value: NumeratorModelLabelEn)
                                    ? String.Empty
                                    : NumeratorModelLabelEn
                                : String.IsNullOrEmpty(value: NumeratorModelLabelEn)
                                    ? $"{NumeratorModelId}"
                                    : $"{NumeratorModelId} - {NumeratorModelLabelEn}";
                    }
                }

                /// <summary>
                /// Sigma for numerator model
                /// </summary>
                public Nullable<bool> NumeratorModelSigma
                {
                    get
                    {
                        return Functions.GetNBoolArg(
                            row: Data,
                            name: TFnApiGetGRegMapConfigsForRatioList.ColNumeratorModelSigma.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnApiGetGRegMapConfigsForRatioList.ColNumeratorModelSigma.DefaultValue) ?
                                (Nullable<bool>)null :
                                Boolean.Parse(value: TFnApiGetGRegMapConfigsForRatioList.ColNumeratorModelSigma.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNBoolArg(
                            row: Data,
                            name: TFnApiGetGRegMapConfigsForRatioList.ColNumeratorModelSigma.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Denominator model identifier
                /// </summary>
                public Nullable<int> DenominatorModelId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TFnApiGetGRegMapConfigsForRatioList.ColDenominatorModelId.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnApiGetGRegMapConfigsForRatioList.ColDenominatorModelId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TFnApiGetGRegMapConfigsForRatioList.ColDenominatorModelId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TFnApiGetGRegMapConfigsForRatioList.ColDenominatorModelId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Denominator model label in national language
                /// </summary>
                public string DenominatorModelLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetGRegMapConfigsForRatioList.ColDenominatorModelLabelCs.Name,
                            defaultValue: TFnApiGetGRegMapConfigsForRatioList.ColDenominatorModelLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetGRegMapConfigsForRatioList.ColDenominatorModelLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Denominator model description in national language
                /// </summary>
                public string DenominatorModelDescriptionCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetGRegMapConfigsForRatioList.ColDenominatorModelDescriptionCs.Name,
                            defaultValue: TFnApiGetGRegMapConfigsForRatioList.ColDenominatorModelDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetGRegMapConfigsForRatioList.ColDenominatorModelDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Denominator model extended label in national language (read-only)
                /// </summary>
                public string DenominatorModelExtendedLabelCs
                {
                    get
                    {
                        return
                            (DenominatorModelId == null)
                                ? String.IsNullOrEmpty(value: DenominatorModelLabelCs)
                                    ? String.Empty
                                    : DenominatorModelLabelCs
                                : String.IsNullOrEmpty(value: DenominatorModelLabelCs)
                                    ? $"{DenominatorModelId}"
                                    : $"{DenominatorModelId} - {DenominatorModelLabelCs}";
                    }
                }

                /// <summary>
                /// Denominator model label in English
                /// </summary>
                public string DenominatorModelLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetGRegMapConfigsForRatioList.ColDenominatorModelLabelEn.Name,
                            defaultValue: TFnApiGetGRegMapConfigsForRatioList.ColDenominatorModelLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetGRegMapConfigsForRatioList.ColDenominatorModelLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Denominator model description in English
                /// </summary>
                public string DenominatorModelDescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetGRegMapConfigsForRatioList.ColDenominatorModelDescriptionEn.Name,
                            defaultValue: TFnApiGetGRegMapConfigsForRatioList.ColDenominatorModelDescriptionEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetGRegMapConfigsForRatioList.ColDenominatorModelDescriptionEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Denominator model extended label in English (read-only)
                /// </summary>
                public string DenominatorModelExtendedLabelEn
                {
                    get
                    {
                        return
                            (DenominatorModelId == null)
                                ? String.IsNullOrEmpty(value: DenominatorModelLabelEn)
                                    ? String.Empty
                                    : DenominatorModelLabelEn
                                : String.IsNullOrEmpty(value: DenominatorModelLabelEn)
                                    ? $"{DenominatorModelId}"
                                    : $"{DenominatorModelId} - {DenominatorModelLabelEn}";
                    }
                }

                /// <summary>
                /// Sigma for denominator model
                /// </summary>
                public Nullable<bool> DenominatorModelSigma
                {
                    get
                    {
                        return Functions.GetNBoolArg(
                            row: Data,
                            name: TFnApiGetGRegMapConfigsForRatioList.ColDenominatorModelSigma.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnApiGetGRegMapConfigsForRatioList.ColDenominatorModelSigma.DefaultValue) ?
                                (Nullable<bool>)null :
                                Boolean.Parse(value: TFnApiGetGRegMapConfigsForRatioList.ColDenominatorModelSigma.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNBoolArg(
                            row: Data,
                            name: TFnApiGetGRegMapConfigsForRatioList.ColDenominatorModelSigma.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// All estimates configurable
                /// </summary>
                public Nullable<bool> AllEstimatesConfigurable
                {
                    get
                    {
                        return Functions.GetNBoolArg(
                            row: Data,
                            name: TFnApiGetGRegMapConfigsForRatioList.ColAllEstimatesConfigurable.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnApiGetGRegMapConfigsForRatioList.ColAllEstimatesConfigurable.DefaultValue) ?
                                (Nullable<bool>)null :
                                Boolean.Parse(value: TFnApiGetGRegMapConfigsForRatioList.ColAllEstimatesConfigurable.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNBoolArg(
                            row: Data,
                            name: TFnApiGetGRegMapConfigsForRatioList.ColAllEstimatesConfigurable.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Total estimate configurations for numerator (as text)
                /// </summary>
                public string TotalEstimateConfNumeratorText
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetGRegMapConfigsForRatioList.ColTotalEstimateConfNumerator.Name,
                            defaultValue: TFnApiGetGRegMapConfigsForRatioList.ColTotalEstimateConfNumerator.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetGRegMapConfigsForRatioList.ColTotalEstimateConfNumerator.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Total estimate configurations for numerator (as list)
                /// </summary>
                public List<Nullable<int>> TotalEstimateConfNumeratorList
                {
                    get
                    {
                        return Functions.StringToNIntList(
                            text: TotalEstimateConfNumeratorText,
                            separator: (char)59,
                            defaultValue: null);
                    }
                }


                /// <summary>
                /// Total estimate configurations for denominator (as text)
                /// </summary>
                public string TotalEstimateConfDenominatorText
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnApiGetGRegMapConfigsForRatioList.ColTotalEstimateConfDenominator.Name,
                            defaultValue: TFnApiGetGRegMapConfigsForRatioList.ColTotalEstimateConfDenominator.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnApiGetGRegMapConfigsForRatioList.ColTotalEstimateConfDenominator.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Total estimate configurations for denominator (as list)
                /// </summary>
                public List<Nullable<int>> TotalEstimateConfDenominatorList
                {
                    get
                    {
                        return Functions.StringToNIntList(
                            text: TotalEstimateConfDenominatorText,
                            separator: (char)59,
                            defaultValue: null);
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not TFnApiGetGRegMapConfigsForRatio, return False
                    if (obj is not TFnApiGetGRegMapConfigsForRatio)
                    {
                        return false;
                    }

                    return
                        Id == ((TFnApiGetGRegMapConfigsForRatio)obj).Id;
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                /// <summary>
                /// Text description of the object
                /// </summary>
                /// <returns>Text description of the object</returns>
                public override string ToString()
                {
                    return ((NfiEstaDB)Composite.Database).Postgres.Setting.LanguageVersion switch
                    {
                        LanguageVersion.International =>
                            String.Concat($"{"ID"} = {Functions.PrepIntArg(arg: Id)}.{Environment.NewLine}"),
                        LanguageVersion.National =>
                            String.Concat($"{"ID"} = {Functions.PrepIntArg(arg: Id)}.{Environment.NewLine}"),
                        _ =>
                            String.Concat($"{"ID"} = {Functions.PrepIntArg(arg: Id)}.{Environment.NewLine}"),
                    };
                }

                #endregion Methods

            }


            /// <summary>
            /// List of valid combinations of panel-refyearset group, param. area type,
            /// force synthetic, model and sigma of already configured GREG-map totals
            /// (return type of the stored procedure fn_api_get_gregmap_configs4ratio)
            /// </summary>
            public class TFnApiGetGRegMapConfigsForRatioList
                 : AParametrizedView
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema =
                    NfiEstaFunctions.FnApiGetGRegMapConfigsForRatio.SchemaName;

                /// <summary>
                /// Stored procedure name
                /// </summary>
                public static readonly string Name =
                    NfiEstaFunctions.FnApiGetGRegMapConfigsForRatio.Name;

                /// <summary>
                /// Stored procedure alias
                /// </summary>
                public static readonly string Alias =
                    NfiEstaFunctions.FnApiGetGRegMapConfigsForRatio.Name;

                /// <summary>
                /// Description in national language
                /// </summary>
                public static readonly string CaptionCs = String.Concat(
                    "Seznam platných kombinací skupin panelů a referenčních období, typu parametrizační oblasti ",
                    "syntetického odhadu, modelu a sigma pro již nakonfigurované regresní odhady úhrnů ",
                    "(návratový typ uložené procedury fn_api_get_gregmap_configs4ratio)");

                /// <summary>
                /// Description in English
                /// </summary>
                public static readonly string CaptionEn = String.Concat(
                    "List of valid combinations of panel-refyearset group, param. area type, ",
                    "force synthetic, model and sigma of already configured GREG-map totals ",
                    "(return type of the stored procedure fn_api_get_gregmap_configs4ratio)");

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
                {
                    { "id", new ColumnMetadata()
                    {
                        Name = "id",
                        DbName = "id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = true,
                        NotNull = true,
                        DefaultValue = "0",
                        FuncCall = "row_number() over()",
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ID",
                        HeaderTextEn = "ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 0
                    }
                    },
                    { "panel_refyearset_group_id", new ColumnMetadata()
                    {
                        Name = "panel_refyearset_group_id",
                        DbName = "panel_refyearset_group_id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PANEL_REFYEARSET_GROUP_ID",
                        HeaderTextEn = "PANEL_REFYEARSET_GROUP_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 1
                    }
                    },
                    { "panel_refyearset_group_label_cs", new ColumnMetadata()
                    {
                        Name = "panel_refyearset_group_label_cs",
                        DbName = "panel_refyearset_group_label",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PANEL_REFYEARSET_GROUP_LABEL_CS",
                        HeaderTextEn = "PANEL_REFYEARSET_GROUP_LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 2
                    }
                    },
                    { "panel_refyearset_group_description_cs", new ColumnMetadata()
                    {
                        Name = "panel_refyearset_group_description_cs",
                        DbName = "panel_refyearset_group_description",
                        DataType = "System.String",
                        DbDataType = "text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PANEL_REFYEARSET_GROUP_DESCRIPTION_CS",
                        HeaderTextEn = "PANEL_REFYEARSET_GROUP_DESCRIPTION_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 3
                    }
                    },
                    { "panel_refyearset_group_label_en", new ColumnMetadata()
                    {
                        Name = "panel_refyearset_group_label_en",
                        DbName = "panel_refyearset_group_label_en",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PANEL_REFYEARSET_GROUP_LABEL_EN",
                        HeaderTextEn = "PANEL_REFYEARSET_GROUP_LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 4
                    }
                    },
                    { "panel_refyearset_group_description_en", new ColumnMetadata()
                    {
                        Name = "panel_refyearset_group_description_en",
                        DbName = "panel_refyearset_group_description_en",
                        DataType = "System.String",
                        DbDataType = "text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PANEL_REFYEARSET_GROUP_DESCRIPTION_EN",
                        HeaderTextEn = "PANEL_REFYEARSET_GROUP_DESCRIPTION_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 5
                    }
                    },
                    { "param_area_type_id", new ColumnMetadata()
                    {
                        Name = "param_area_type_id",
                        DbName = "param_area_type_id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PARAM_AREA_TYPE_ID",
                        HeaderTextEn = "PARAM_AREA_TYPE_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 6
                    }
                    },
                    { "param_area_type_label_cs", new ColumnMetadata()
                    {
                        Name = "param_area_type_label_cs",
                        DbName = "param_area_type_label",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PARAM_AREA_TYPE_LABEL_CS",
                        HeaderTextEn = "PARAM_AREA_TYPE_LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 7
                    }
                    },
                    { "param_area_type_description_cs", new ColumnMetadata()
                    {
                        Name = "param_area_type_description_cs",
                        DbName = "param_area_type_description",
                        DataType = "System.String",
                        DbDataType = "text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PARAM_AREA_TYPE_DESCRIPTION_CS",
                        HeaderTextEn = "PARAM_AREA_TYPE_DESCRIPTION_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 8
                    }
                    },
                    { "param_area_type_label_en", new ColumnMetadata()
                    {
                        Name = "param_area_type_label_en",
                        DbName = "param_area_type_label_en",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PARAM_AREA_TYPE_LABEL_EN",
                        HeaderTextEn = "PARAM_AREA_TYPE_LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 9
                    }
                    },
                    { "param_area_type_description_en", new ColumnMetadata()
                    {
                        Name = "param_area_type_description_en",
                        DbName = "param_area_type_description_en",
                        DataType = "System.String",
                        DbDataType = "text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PARAM_AREA_TYPE_DESCRIPTION_EN",
                        HeaderTextEn = "PARAM_AREA_TYPE_DESCRIPTION_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 10
                    }
                    },
                    { "force_synthetic", new ColumnMetadata()
                    {
                        Name = "force_synthetic",
                        DbName = "force_synthetic",
                        DataType = "System.Boolean",
                        DbDataType = "bool",
                        NewDataType = "bool",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "FORCE_SYNTHETIC",
                        HeaderTextEn = "FORCE_SYNTHETIC",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 11
                    }
                    },
                    { "numerator_model_id", new ColumnMetadata()
                    {
                        Name = "numerator_model_id",
                        DbName = "nominator_model_id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "NUMERATOR_MODEL_ID",
                        HeaderTextEn = "NUMERATOR_MODEL_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 12
                    }
                    },
                    { "numerator_model_label_cs", new ColumnMetadata()
                    {
                        Name = "numerator_model_label_cs",
                        DbName = "nominator_model_label",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "NUMERATOR_MODEL_LABEL_CS",
                        HeaderTextEn = "NUMERATOR_MODEL_LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 13
                    }
                    },
                    { "numerator_model_description_cs", new ColumnMetadata()
                    {
                        Name = "numerator_model_description_cs",
                        DbName = "nominator_model_description",
                        DataType = "System.String",
                        DbDataType = "text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "NUMERATOR_MODEL_DESCRIPTION_CS",
                        HeaderTextEn = "NUMERATOR_MODEL_DESCRIPTION_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 14
                    }
                    },
                    { "numerator_model_label_en", new ColumnMetadata()
                    {
                        Name = "numerator_model_label_en",
                        DbName = "nominator_model_label_en",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "NUMERATOR_MODEL_LABEL_EN",
                        HeaderTextEn = "NUMERATOR_MODEL_LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 15
                    }
                    },
                    { "numerator_model_description_en", new ColumnMetadata()
                    {
                        Name = "numerator_model_description_en",
                        DbName = "nominator_model_description_en",
                        DataType = "System.String",
                        DbDataType = "text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "NUMERATOR_MODEL_DESCRIPTION_EN",
                        HeaderTextEn = "NUMERATOR_MODEL_DESCRIPTION_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 16
                    }
                    },
                    { "numerator_model_sigma", new ColumnMetadata()
                    {
                        Name = "numerator_model_sigma",
                        DbName = "nominator_model_sigma",
                        DataType = "System.Boolean",
                        DbDataType = "bool",
                        NewDataType = "bool",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "NUMERATOR_MODEL_SIGMA",
                        HeaderTextEn = "NUMERATOR_MODEL_SIGMA",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 17
                    }
                    },
                    { "denominator_model_id", new ColumnMetadata()
                    {
                        Name = "denominator_model_id",
                        DbName = "denominator_model_id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "DENOMINATOR_MODEL_ID",
                        HeaderTextEn = "DENOMINATOR_MODEL_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 18
                    }
                    },
                    { "denominator_model_label_cs", new ColumnMetadata()
                    {
                        Name = "denominator_model_label_cs",
                        DbName = "denominator_model_label",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "DENOMINATOR_MODEL_LABEL_CS",
                        HeaderTextEn = "DENOMINATOR_MODEL_LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 19
                    }
                    },
                    { "denominator_model_description_cs", new ColumnMetadata()
                    {
                        Name = "denominator_model_description_cs",
                        DbName = "denominator_model_description",
                        DataType = "System.String",
                        DbDataType = "text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "DENOMINATOR_MODEL_DESCRIPTION_CS",
                        HeaderTextEn = "DENOMINATOR_MODEL_DESCRIPTION_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 20
                    }
                    },
                    { "denominator_model_label_en", new ColumnMetadata()
                    {
                        Name = "denominator_model_label_en",
                        DbName = "denominator_model_label_en",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "DENOMINATOR_MODEL_LABEL_EN",
                        HeaderTextEn = "DENOMINATOR_MODEL_LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 21
                    }
                    },
                    { "denominator_model_description_en", new ColumnMetadata()
                    {
                        Name = "denominator_model_description_en",
                        DbName = "denominator_model_description_en",
                        DataType = "System.String",
                        DbDataType = "text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "DENOMINATOR_MODEL_DESCRIPTION_EN",
                        HeaderTextEn = "DENOMINATOR_MODEL_DESCRIPTION_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 22
                    }
                    },
                    { "denominator_model_sigma", new ColumnMetadata()
                    {
                        Name = "denominator_model_sigma",
                        DbName = "denominator_model_sigma",
                        DataType = "System.Boolean",
                        DbDataType = "bool",
                        NewDataType = "bool",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "DENOMINATOR_MODEL_SIGMA",
                        HeaderTextEn = "DENOMINATOR_MODEL_SIGMA",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 23
                    }
                    },
                    { "all_estimates_configurable", new ColumnMetadata()
                    {
                        Name = "all_estimates_configurable",
                        DbName = "all_estimates_configurable",
                        DataType = "System.Boolean",
                        DbDataType = "bool",
                        NewDataType = "bool",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ALL_ESTIMATES_CONFIGURABLE",
                        HeaderTextEn = "ALL_ESTIMATES_CONFIGURABLE",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 24
                    }
                    },
                    { "total_estimate_conf_numerator", new ColumnMetadata()
                    {
                        Name = "total_estimate_conf_numerator",
                        DbName = "total_estimate_conf_nominator",
                        DataType = "System.String",
                        DbDataType = "_int4",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = "array_to_string(total_estimate_conf_nominator, ';', 'NULL')",
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "TOTAL_ESTIMATE_CONF_NUMERATOR",
                        HeaderTextEn = "TOTAL_ESTIMATE_CONF_NUMERATOR",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 25
                    }
                    },
                    { "total_estimate_conf_denominator", new ColumnMetadata()
                    {
                        Name = "total_estimate_conf_denominator",
                        DbName = "total_estimate_conf_denominator",
                        DataType = "System.String",
                        DbDataType = "_int4",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = "array_to_string(total_estimate_conf_denominator, ';', 'NULL')",
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "TOTAL_ESTIMATE_CONF_DENOMINATOR",
                        HeaderTextEn = "TOTAL_ESTIMATE_CONF_DENOMINATOR",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 26
                    }
                    }
                };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column panel_refyearset_group_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColPanelRefYearSetGroupId = Cols["panel_refyearset_group_id"];

                /// <summary>
                /// Column panel_refyearset_group_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColPanelRefYearSetGroupLabelCs = Cols["panel_refyearset_group_label_cs"];

                /// <summary>
                /// Column panel_refyearset_group_description_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColPanelRefYearSetGroupDescriptionCs = Cols["panel_refyearset_group_description_cs"];

                /// <summary>
                /// Column panel_refyearset_group_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColPanelRefYearSetGroupLabelEn = Cols["panel_refyearset_group_label_en"];

                /// <summary>
                /// Column panel_refyearset_group_description_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColPanelRefYearSetGroupDescriptionEn = Cols["panel_refyearset_group_description_en"];

                /// <summary>
                /// Column param_area_type_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColParamAreaTypeId = Cols["param_area_type_id"];

                /// <summary>
                /// Column param_area_type_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColParamAreaTypeLabelCs = Cols["param_area_type_label_cs"];

                /// <summary>
                /// Column param_area_type_description_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColParamAreaTypeDescriptionCs = Cols["param_area_type_description_cs"];

                /// <summary>
                /// Column param_area_type_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColParamAreaTypeLabelEn = Cols["param_area_type_label_en"];

                /// <summary>
                /// Column param_area_type_description_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColParamAreaTypeDescriptionEn = Cols["param_area_type_description_en"];

                /// <summary>
                /// Column force_synthetic metadata
                /// </summary>
                public static readonly ColumnMetadata ColForceSynthetic = Cols["force_synthetic"];

                /// <summary>
                /// Column numerator_model_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColNumeratorModelId = Cols["numerator_model_id"];

                /// <summary>
                /// Column numerator_model_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColNumeratorModelLabelCs = Cols["numerator_model_label_cs"];

                /// <summary>
                /// Column numerator_model_description_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColNumeratorModelDescriptionCs = Cols["numerator_model_description_cs"];

                /// <summary>
                /// Column numerator_model_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColNumeratorModelLabelEn = Cols["numerator_model_label_en"];

                /// <summary>
                /// Column numerator_model_description_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColNumeratorModelDescriptionEn = Cols["numerator_model_description_en"];

                /// <summary>
                /// Column numerator_model_sigma metadata
                /// </summary>
                public static readonly ColumnMetadata ColNumeratorModelSigma = Cols["numerator_model_sigma"];

                /// <summary>
                /// Column denominator_model_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColDenominatorModelId = Cols["denominator_model_id"];

                /// <summary>
                /// Column denominator_model_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColDenominatorModelLabelCs = Cols["denominator_model_label_cs"];

                /// <summary>
                /// Column denominator_model_description_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColDenominatorModelDescriptionCs = Cols["denominator_model_description_cs"];

                /// <summary>
                /// Column denominator_model_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColDenominatorModelLabelEn = Cols["denominator_model_label_en"];

                /// <summary>
                /// Column denominator_model_description_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColDenominatorModelDescriptionEn = Cols["denominator_model_description_en"];

                /// <summary>
                /// Column denominator_model_sigma metadata
                /// </summary>
                public static readonly ColumnMetadata ColDenominatorModelSigma = Cols["denominator_model_sigma"];

                /// <summary>
                /// Column all_estimates_configurable metadata
                /// </summary>
                public static readonly ColumnMetadata ColAllEstimatesConfigurable = Cols["all_estimates_configurable"];

                /// <summary>
                /// Column total_estimate_conf_numerator metadata
                /// </summary>
                public static readonly ColumnMetadata ColTotalEstimateConfNumerator = Cols["total_estimate_conf_numerator"];

                /// <summary>
                /// Column total_estimate_conf_denominator metadata
                /// </summary>
                public static readonly ColumnMetadata ColTotalEstimateConfDenominator = Cols["total_estimate_conf_denominator"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Private Fields

                /// <summary>
                /// 1st parameter: Estimation period identifier
                /// </summary>
                private Nullable<int> estimationPeriodId;

                /// <summary>
                /// 2nd parameter: List of estimation cells identifiers
                /// </summary>
                private List<Nullable<int>> estimationCellsIds;

                /// <summary>
                /// 3rd parameter: List of attribute categories identifiers for numerator
                /// </summary>
                private List<Nullable<int>> variablesNumeratorIds;

                /// <summary>
                /// 4th parameter: List of attribute categories identifiers for denominator
                /// </summary>
                private List<Nullable<int>> variablesDenominatorIds;

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TFnApiGetGRegMapConfigsForRatioList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                {
                    EstimationPeriodId = null;
                    EstimationCellsIds = [];
                    VariablesNumeratorIds = [];
                    VariablesDenominatorIds = [];
                }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TFnApiGetGRegMapConfigsForRatioList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                {
                    EstimationPeriodId = null;
                    EstimationCellsIds = [];
                    VariablesNumeratorIds = [];
                    VariablesDenominatorIds = [];
                }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// 1st parameter: Estimation period identifier
                /// </summary>
                public Nullable<int> EstimationPeriodId
                {
                    get
                    {
                        return estimationPeriodId;
                    }
                    set
                    {
                        estimationPeriodId = value;
                    }
                }

                /// <summary>
                /// 2nd parameter: List of estimation cells identifiers
                /// </summary>
                public List<Nullable<int>> EstimationCellsIds
                {
                    get
                    {
                        return estimationCellsIds ?? [];
                    }
                    set
                    {
                        estimationCellsIds = value ?? [];
                    }
                }

                /// <summary>
                /// 3rd parameter: List of attribute categories identifiers for numerator
                /// </summary>
                public List<Nullable<int>> VariablesNumeratorIds
                {
                    get
                    {
                        return variablesNumeratorIds ?? [];
                    }
                    set
                    {
                        variablesNumeratorIds = value ?? [];
                    }
                }

                /// <summary>
                /// 4th parameter: List of attribute categories identifiers for denominator
                /// </summary>
                public List<Nullable<int>> VariablesDenominatorIds
                {
                    get
                    {
                        return variablesDenominatorIds ?? [];
                    }
                    set
                    {
                        variablesDenominatorIds = value ?? [];
                    }
                }

                /// <summary>
                /// List of items (read-only)
                /// </summary>
                public List<TFnApiGetGRegMapConfigsForRatio> Items
                {
                    get
                    {
                        return
                            [..
                                Data.AsEnumerable()
                                .Select(a => new TFnApiGetGRegMapConfigsForRatio(composite: this, data: a))
                            ];
                    }
                }

                #endregion Properties


                #region Indexers

                /// <summary>
                /// Row from list of rows by identifier (read-only)
                /// </summary>
                /// <param name="id">Row identifier</param>
                /// <returns>Row from list of rows by identifier (null if not found)</returns>
                public TFnApiGetGRegMapConfigsForRatio this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new TFnApiGetGRegMapConfigsForRatio(composite: this, data: a))
                                .FirstOrDefault<TFnApiGetGRegMapConfigsForRatio>();
                    }
                }

                #endregion Indexers


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public TFnApiGetGRegMapConfigsForRatioList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any()
                        ? new TFnApiGetGRegMapConfigsForRatioList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime)
                        {
                            EstimationPeriodId = EstimationPeriodId,
                            EstimationCellsIds = [.. EstimationCellsIds],
                            VariablesNumeratorIds = [.. VariablesNumeratorIds],
                            VariablesDenominatorIds = [.. VariablesDenominatorIds]
                        }
                        : new TFnApiGetGRegMapConfigsForRatioList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null)
                        {
                            EstimationPeriodId = EstimationPeriodId,
                            EstimationCellsIds = [.. EstimationCellsIds],
                            VariablesNumeratorIds = [.. VariablesNumeratorIds],
                            VariablesDenominatorIds = [.. VariablesDenominatorIds]
                        };
                }

                #endregion Methods

            }

        }
    }
}