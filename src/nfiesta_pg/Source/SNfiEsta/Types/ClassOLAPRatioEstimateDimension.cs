﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {
            // v_olap_ratio_estimate_dimension

            /// <summary>
            /// OLAP Cube Dimension - Ratio Estimate
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class OLAPRatioEstimateDimension(
                OLAPRatioEstimateDimensionList composite = null,
                DataRow data = null)
                    : ALinqViewEntry<OLAPRatioEstimateDimensionList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// OLAP Cube Dimension - Ratio Estimate - composite identifier
                /// </summary>
                public new string Id
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateDimensionList.ColId.Name,
                            defaultValue: OLAPRatioEstimateDimensionList.ColId.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateDimensionList.ColId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// OLAP Cube Dimension - Ratio Estimate - composite identifier for numerator
                /// </summary>
                public string NumeratorId
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateDimensionList.ColNumeratorId.Name,
                            defaultValue: OLAPRatioEstimateDimensionList.ColNumeratorId.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateDimensionList.ColNumeratorId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// OLAP Cube Dimension - Ratio Estimate - composite identifier for denominator
                /// </summary>
                public string DenominatorId
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateDimensionList.ColDenominatorId.Name,
                            defaultValue: OLAPRatioEstimateDimensionList.ColDenominatorId.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateDimensionList.ColDenominatorId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Numerator estimate phase identifier
                /// </summary>
                public int NumeratorPhaseEstimateTypeId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: OLAPRatioEstimateDimensionList.ColNumeratorPhaseEstimateTypeId.Name,
                            defaultValue: Int32.Parse(s: OLAPRatioEstimateDimensionList.ColNumeratorPhaseEstimateTypeId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: OLAPRatioEstimateDimensionList.ColNumeratorPhaseEstimateTypeId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Label of numerator estimate phase (in national language)
                /// </summary>
                public string NumeratorPhaseEstimateTypeLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateDimensionList.ColNumeratorPhaseEstimateTypeLabelCs.Name,
                            defaultValue: OLAPRatioEstimateDimensionList.ColNumeratorPhaseEstimateTypeLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateDimensionList.ColNumeratorPhaseEstimateTypeLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Label of numerator estimate phase (in English)
                /// </summary>
                public string NumeratorPhaseEstimateTypeLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateDimensionList.ColNumeratorPhaseEstimateTypeLabelEn.Name,
                            defaultValue: OLAPRatioEstimateDimensionList.ColNumeratorPhaseEstimateTypeLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateDimensionList.ColNumeratorPhaseEstimateTypeLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Numerator estimate phase object (read-only)
                /// </summary>
                public PhaseEstimateType NumeratorPhaseEstimateType
                {
                    get
                    {
                        return
                             ((NfiEstaDB)Composite.Database).SNfiEsta.CPhaseEstimateType[(int)NumeratorPhaseEstimateTypeId];
                    }
                }


                /// <summary>
                /// Denominator estimate phase identifier
                /// </summary>
                public int DenominatorPhaseEstimateTypeId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: OLAPRatioEstimateDimensionList.ColDenominatorPhaseEstimateTypeId.Name,
                            defaultValue: Int32.Parse(s: OLAPRatioEstimateDimensionList.ColDenominatorPhaseEstimateTypeId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: OLAPRatioEstimateDimensionList.ColDenominatorPhaseEstimateTypeId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Label of denominator estimate phase (in national language)
                /// </summary>
                public string DenominatorPhaseEstimateTypeLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateDimensionList.ColDenominatorPhaseEstimateTypeLabelCs.Name,
                            defaultValue: OLAPRatioEstimateDimensionList.ColDenominatorPhaseEstimateTypeLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateDimensionList.ColDenominatorPhaseEstimateTypeLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Label of denominator estimate phase (in English)
                /// </summary>
                public string DenominatorPhaseEstimateTypeLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateDimensionList.ColDenominatorPhaseEstimateTypeLabelEn.Name,
                            defaultValue: OLAPRatioEstimateDimensionList.ColDenominatorPhaseEstimateTypeLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateDimensionList.ColDenominatorPhaseEstimateTypeLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Denominator estimate phase object (read-only)
                /// </summary>
                public PhaseEstimateType DenominatorPhaseEstimateType
                {
                    get
                    {
                        return
                             ((NfiEstaDB)Composite.Database).SNfiEsta.CPhaseEstimateType[(int)DenominatorPhaseEstimateTypeId];
                    }
                }


                /// <summary>
                /// Estimation period identifier
                /// </summary>
                public int EstimationPeriodId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: OLAPRatioEstimateDimensionList.ColEstimationPeriodId.Name,
                            defaultValue: Int32.Parse(s: OLAPRatioEstimateDimensionList.ColEstimationPeriodId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: OLAPRatioEstimateDimensionList.ColEstimationPeriodId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// The beginning of the time to witch the estimate is calculated
                /// </summary>
                public DateTime EstimateDateBegin
                {
                    get
                    {
                        return Functions.GetDateTimeArg(
                            row: Data,
                            name: OLAPRatioEstimateDimensionList.ColEstimateDateBegin.Name,
                            defaultValue: DateTime.Parse(s: OLAPRatioEstimateDimensionList.ColEstimateDateBegin.DefaultValue));
                    }
                    set
                    {
                        Functions.SetDateTimeArg(
                            row: Data,
                            name: OLAPRatioEstimateDimensionList.ColEstimateDateBegin.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// The beginning of the time to witch the estimate is calculated as text
                /// </summary>
                public string EstimateDateBeginText
                {
                    get
                    {
                        return
                            EstimateDateBegin.ToString(
                                format: OLAPRatioEstimateDimensionList.ColEstimateDateBegin.NumericFormat);
                    }
                }

                /// <summary>
                /// The end of the time to witch the estimate is calculated
                /// </summary>
                public DateTime EstimateDateEnd
                {
                    get
                    {
                        return Functions.GetDateTimeArg(
                            row: Data,
                            name: OLAPRatioEstimateDimensionList.ColEstimateDateEnd.Name,
                            defaultValue: DateTime.Parse(s: OLAPRatioEstimateDimensionList.ColEstimateDateEnd.DefaultValue));
                    }
                    set
                    {
                        Functions.SetDateTimeArg(
                            row: Data,
                            name: OLAPRatioEstimateDimensionList.ColEstimateDateEnd.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// The end of the time to witch the estimate is calculated as text
                /// </summary>
                public string EstimateDateEndText
                {
                    get
                    {
                        return
                            EstimateDateEnd.ToString(
                                format: OLAPRatioEstimateDimensionList.ColEstimateDateEnd.NumericFormat);
                    }
                }

                /// <summary>
                /// Estimation period label (in national language)
                /// </summary>
                public string EstimationPeriodLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateDimensionList.ColEstimationPeriodLabelCs.Name,
                            defaultValue: OLAPRatioEstimateDimensionList.ColEstimationPeriodLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateDimensionList.ColEstimationPeriodLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Estimation period label (in English)
                /// </summary>
                public string EstimationPeriodLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateDimensionList.ColEstimationPeriodLabelEn.Name,
                            defaultValue: OLAPRatioEstimateDimensionList.ColEstimationPeriodLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateDimensionList.ColEstimationPeriodLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Period object (read-only)
                /// </summary>
                public EstimationPeriod EstimationPeriod
                {
                    get
                    {
                        return
                             ((NfiEstaDB)Composite.Database).SNfiEsta.CEstimationPeriod[(int)EstimationPeriodId];
                    }
                }


                /// <summary>
                /// Numerator - Combination of target variable and sub-population category and area domain category identifier
                /// </summary>
                public int NumeratorVariableId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: OLAPRatioEstimateDimensionList.ColNumeratorVariableId.Name,
                            defaultValue: Int32.Parse(s: OLAPRatioEstimateDimensionList.ColNumeratorVariableId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: OLAPRatioEstimateDimensionList.ColNumeratorVariableId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Numerator - Label of variable (in national language)
                /// </summary>
                public string NumeratorVariableLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateDimensionList.ColNumeratorVariableLabelCs.Name,
                            defaultValue: OLAPRatioEstimateDimensionList.ColNumeratorVariableLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateDimensionList.ColNumeratorVariableLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Numerator - Label of variable (in English)
                /// </summary>
                public string NumeratorVariableLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateDimensionList.ColNumeratorVariableLabelEn.Name,
                            defaultValue: OLAPRatioEstimateDimensionList.ColNumeratorVariableLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateDimensionList.ColNumeratorVariableLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Numerator - Combination of target variable and sub-population category and area domain category object (read-only)
                /// </summary>
                public Variable NumeratorVariable
                {
                    get
                    {
                        return
                             ((NfiEstaDB)Composite.Database).SNfiEsta.TVariable[(int)NumeratorVariableId];
                    }
                }

                /// <summary>
                /// Numerator - Combination of target variable and sub-population category and area domain category (extended-version) object (read-only)
                /// </summary>
                public VwVariable NumeratorVwVariable
                {
                    get
                    {
                        return
                             ((NfiEstaDB)Composite.Database).SNfiEsta.VVariable[(int)NumeratorVariableId];
                    }
                }


                /// <summary>
                /// Denominator - Combination of target variable and sub-population category and area domain category identifier
                /// </summary>
                public int DenominatorVariableId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: OLAPRatioEstimateDimensionList.ColDenominatorVariableId.Name,
                            defaultValue: Int32.Parse(s: OLAPRatioEstimateDimensionList.ColDenominatorVariableId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: OLAPRatioEstimateDimensionList.ColDenominatorVariableId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Denominator - Label of variable (in national language)
                /// </summary>
                public string DenominatorVariableLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateDimensionList.ColDenominatorVariableLabelCs.Name,
                            defaultValue: OLAPRatioEstimateDimensionList.ColDenominatorVariableLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateDimensionList.ColDenominatorVariableLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Denominator - Label of variable (in English)
                /// </summary>
                public string DenominatorVariableLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateDimensionList.ColDenominatorVariableLabelEn.Name,
                            defaultValue: OLAPRatioEstimateDimensionList.ColDenominatorVariableLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateDimensionList.ColDenominatorVariableLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Denominator - Combination of target variable and sub-population category and area domain category object (read-only)
                /// </summary>
                public Variable DenominatorVariable
                {
                    get
                    {
                        return
                             ((NfiEstaDB)Composite.Database).SNfiEsta.TVariable[(int)DenominatorVariableId];
                    }
                }

                /// <summary>
                /// Denominator - Combination of target variable and sub-population category and area domain category (extended-version) object (read-only)
                /// </summary>
                public VwVariable DenominatorVwVariable
                {
                    get
                    {
                        return
                             ((NfiEstaDB)Composite.Database).SNfiEsta.VVariable[(int)DenominatorVariableId];
                    }
                }


                /// <summary>
                /// Estimation cell identifier
                /// </summary>
                public int EstimationCellId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: OLAPRatioEstimateDimensionList.ColEstimationCellId.Name,
                            defaultValue: Int32.Parse(s: OLAPRatioEstimateDimensionList.ColEstimationCellId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: OLAPRatioEstimateDimensionList.ColEstimationCellId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Label of estimation cell (in national language)
                /// </summary>
                public string EstimationCellDescriptionCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateDimensionList.ColEstimationCellDescriptionCs.Name,
                            defaultValue: OLAPRatioEstimateDimensionList.ColEstimationCellDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateDimensionList.ColEstimationCellDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Label of estimation cell (in English)
                /// </summary>
                public string EstimationCellDescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateDimensionList.ColEstimationCellDescriptionEn.Name,
                            defaultValue: OLAPRatioEstimateDimensionList.ColEstimationCellDescriptionEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateDimensionList.ColEstimationCellDescriptionEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Estimation cell object (read-only)
                /// </summary>
                public EstimationCell EstimationCell
                {
                    get
                    {
                        return
                             ((NfiEstaDB)Composite.Database).SNfiEsta.CEstimationCell[(int)EstimationCellId];
                    }
                }


                /// <summary>
                /// Estimation cell collection identifier
                /// </summary>
                public int EstimationCellCollectionId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: OLAPRatioEstimateDimensionList.ColEstimationCellCollectionId.Name,
                            defaultValue: Int32.Parse(s: OLAPRatioEstimateDimensionList.ColEstimationCellCollectionId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: OLAPRatioEstimateDimensionList.ColEstimationCellCollectionId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Label of estimation cell collection (in national language)
                /// </summary>
                public string EstimationCellCollectionLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateDimensionList.ColEstimationCellCollectionLabelCs.Name,
                            defaultValue: OLAPRatioEstimateDimensionList.ColEstimationCellCollectionLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateDimensionList.ColEstimationCellCollectionLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Label of estimation cell collection (in English)
                /// </summary>
                public string EstimationCellCollectionLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateDimensionList.ColEstimationCellCollectionLabelEn.Name,
                            defaultValue: OLAPRatioEstimateDimensionList.ColEstimationCellCollectionLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateDimensionList.ColEstimationCellCollectionLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Estimation cell collection object (read-only)
                /// </summary>
                public EstimationCellCollection EstimationCellCollection
                {
                    get
                    {
                        return
                             ((NfiEstaDB)Composite.Database).SNfiEsta.CEstimationCellCollection[(int)EstimationCellCollectionId];
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not OLAPRatioDimension Type, return False
                    if (obj is not OLAPRatioEstimateDimension)
                    {
                        return false;
                    }

                    return
                        (NumeratorPhaseEstimateTypeId == ((OLAPRatioEstimateDimension)obj).NumeratorPhaseEstimateTypeId) &&
                        (DenominatorPhaseEstimateTypeId == ((OLAPRatioEstimateDimension)obj).DenominatorPhaseEstimateTypeId) &&
                        (EstimationPeriod == ((OLAPRatioEstimateDimension)obj).EstimationPeriod) &&
                        (NumeratorVariableId == ((OLAPRatioEstimateDimension)obj).NumeratorVariableId) &&
                        (DenominatorVariableId == ((OLAPRatioEstimateDimension)obj).DenominatorVariableId) &&
                        (EstimationCellId == ((OLAPRatioEstimateDimension)obj).EstimationCellId);
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        NumeratorPhaseEstimateTypeId.GetHashCode() ^
                        DenominatorPhaseEstimateTypeId.GetHashCode() ^
                        EstimationPeriod.GetHashCode() ^
                        NumeratorVariableId.GetHashCode() ^
                        DenominatorVariableId.GetHashCode() ^
                        EstimationCellId.GetHashCode();
                }

                /// <summary>
                /// Text description of the object
                /// </summary>
                /// <returns>Text description of the object</returns>
                public override string ToString()
                {
                    return String.Concat(
                        $"{"ID"} = {Functions.PrepStringArg(arg: Id)}{Environment.NewLine}");
                }

                #endregion Methods

            }


            /// <summary>
            /// List of OLAP Cube Dimensions - Ratio Estimate
            /// </summary>
            public class OLAPRatioEstimateDimensionList
                : ALinqView
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema =
                    NfiEstaSchema.Name;

                /// <summary>
                /// Table name
                /// </summary>
                public static readonly string Name =
                    "v_olap_ratio_estimate_dimension";

                /// <summary>
                /// Table alias
                /// </summary>
                public static readonly string Alias =
                    "v_olap_ratio_estimate_dimension";

                /// <summary>
                /// Description in national language
                /// </summary>
                public static readonly string CaptionCs =
                    "Seznam dimenzí OLAP kostky s daty odhadů podílů";

                /// <summary>
                /// Description in English
                /// </summary>
                public static readonly string CaptionEn =
                    "List of OLAP cube dimensions with ratio estimates data";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
                {
                    { "id", new ColumnMetadata()
                    {
                        Name = "id",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = true,
                        NotNull = true,
                        DefaultValue =  String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = String.Empty,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ID",
                        HeaderTextEn = "ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 0
                    }
                    },
                    { "numerator_id", new ColumnMetadata()
                    {
                        Name = "numerator_id",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue =  String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = String.Empty,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "NUMERATOR_ID",
                        HeaderTextEn = "NUMERATOR_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 1
                    }
                    },
                    { "denominator_id", new ColumnMetadata()
                    {
                        Name = "denominator_id",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue =  String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = String.Empty,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "DENOMINATOR_ID",
                        HeaderTextEn = "DENOMINATOR_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 2
                    }
                    },
                    { "numerator_phase_estimate_type_id", new ColumnMetadata()
                    {
                        Name = "numerator_phase_estimate_type_id",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue =  "0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "NUMERATOR_PHASE_ESTIMATE_TYPE_ID",
                        HeaderTextEn = "NUMERATOR_PHASE_ESTIMATE_TYPE_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 3
                    }
                    },
                    { "numerator_phase_estimate_type_label_cs", new ColumnMetadata()
                    {
                        Name = "numerator_phase_estimate_type_label_cs",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = String.Empty,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "NUMERATOR_PHASE_ESTIMATE_TYPE_LABEL_CS",
                        HeaderTextEn = "NUMERATOR_PHASE_ESTIMATE_TYPE_LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 4
                    }
                    },
                    { "numerator_phase_estimate_type_label_en", new ColumnMetadata()
                    {
                        Name = "numerator_phase_estimate_type_label_en",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = String.Empty,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "NUMERATOR_PHASE_ESTIMATE_TYPE_LABEL_EN",
                        HeaderTextEn = "NUMERATOR_PHASE_ESTIMATE_TYPE_LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 5
                    }
                    },
                    { "denominator_phase_estimate_type_id", new ColumnMetadata()
                    {
                        Name = "denominator_phase_estimate_type_id",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue =  "0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "DENOMINATOR_PHASE_ESTIMATE_TYPE_ID",
                        HeaderTextEn = "DENOMINATOR_PHASE_ESTIMATE_TYPE_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 6
                    }
                    },
                    { "denominator_phase_estimate_type_label_cs", new ColumnMetadata()
                    {
                        Name = "denominator_phase_estimate_type_label_cs",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = String.Empty,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "DENOMINATOR_PHASE_ESTIMATE_TYPE_LABEL_CS",
                        HeaderTextEn = "DENOMINATOR_PHASE_ESTIMATE_TYPE_LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 7
                    }
                    },
                    { "denominator_phase_estimate_type_label_en", new ColumnMetadata()
                    {
                        Name = "denominator_phase_estimate_type_label_en",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = String.Empty,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "DENOMINATOR_PHASE_ESTIMATE_TYPE_LABEL_EN",
                        HeaderTextEn = "DENOMINATOR_PHASE_ESTIMATE_TYPE_LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 8
                    }
                    },
                    { "estimation_period_id", new ColumnMetadata()
                    {
                        Name = "estimation_period_id",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue =  "0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATION_PERIOD_ID",
                        HeaderTextEn = "ESTIMATION_PERIOD_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 9
                    }
                    },
                    { "estimate_date_begin", new ColumnMetadata()
                    {
                        Name = "estimate_date_begin",
                        DbName = null,
                        DataType = "System.DateTime",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue = new DateTime().ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  "yyyy-MM-dd",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATE_DATE_BEGIN",
                        HeaderTextEn = "ESTIMATE_DATE_BEGIN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 10
                    }
                    },
                    { "estimate_date_end", new ColumnMetadata()
                    {
                        Name = "estimate_date_end",
                        DbName = null,
                        DataType = "System.DateTime",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue = new DateTime().ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  "yyyy-MM-dd",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATE_DATE_END",
                        HeaderTextEn = "ESTIMATE_DATE_END",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 11
                    }
                    },
                    { "estimation_period_label_cs", new ColumnMetadata()
                    {
                        Name = "estimation_period_label_cs",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = String.Empty,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATION_PERIOD_LABEL_CS",
                        HeaderTextEn = "ESTIMATION_PERIOD_LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 12
                    }
                    },
                    { "estimation_period_label_en", new ColumnMetadata()
                    {
                        Name = "estimation_period_label_en",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = String.Empty,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATION_PERIOD_LABEL_EN",
                        HeaderTextEn = "ESTIMATION_PERIOD_LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 13
                    }
                    },
                    { "numerator_variable_id", new ColumnMetadata()
                    {
                        Name = "numerator_variable_id",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue =  "0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "NUMERATOR_VARIABLE_ID",
                        HeaderTextEn = "NUMERATOR_VARIABLE_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 14
                    }
                    },
                    { "numerator_variable_label_cs", new ColumnMetadata()
                    {
                        Name = "numerator_variable_label_cs",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = String.Empty,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "NUMERATOR_VARIABLE_LABEL_CS",
                        HeaderTextEn = "NUMERATOR_VARIABLE_LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 15
                    }
                    },
                    { "numerator_variable_label_en", new ColumnMetadata()
                    {
                        Name = "numerator_variable_label_en",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = String.Empty,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "NUMERATOR_VARIABLE_LABEL_EN",
                        HeaderTextEn = "NUMERATOR_VARIABLE_LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 16
                    }
                    },
                    { "denominator_variable_id", new ColumnMetadata()
                    {
                        Name = "denominator_variable_id",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue =  "0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "DENOMINATOR_VARIABLE_ID",
                        HeaderTextEn = "DENOMINATOR_VARIABLE_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 17
                    }
                    },
                    { "denominator_variable_label_cs", new ColumnMetadata()
                    {
                        Name = "denominator_variable_label_cs",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = String.Empty,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "DENOMINATOR_VARIABLE_LABEL_CS",
                        HeaderTextEn = "DENOMINATOR_VARIABLE_LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 18
                    }
                    },
                    { "denominator_variable_label_en", new ColumnMetadata()
                    {
                        Name = "denominator_variable_label_en",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = String.Empty,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "DENOMINATOR_VARIABLE_LABEL_EN",
                        HeaderTextEn = "DENOMINATOR_VARIABLE_LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 19
                    }
                    },
                    { "estimation_cell_id", new ColumnMetadata()
                    {
                        Name = "estimation_cell_id",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue =  "0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATION_CELL_ID",
                        HeaderTextEn = "ESTIMATION_CELL_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 20
                    }
                    },
                    { "estimation_cell_description_cs", new ColumnMetadata()
                    {
                        Name = "estimation_cell_description_cs",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = String.Empty,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATION_CELL_DESCRIPTION_CS",
                        HeaderTextEn = "ESTIMATION_CELL_DESCRIPTION_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 21
                    }
                    },
                    { "estimation_cell_description_en", new ColumnMetadata()
                    {
                        Name = "estimation_cell_description_en",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = String.Empty,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATION_CELL_DESCRIPTION_EN",
                        HeaderTextEn = "ESTIMATION_CELL_DESCRIPTION_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 22
                    }
                    },
                    { "estimation_cell_collection_id", new ColumnMetadata()
                    {
                        Name = "estimation_cell_collection_id",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue =  "0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATION_CELL_COLLECTION_ID",
                        HeaderTextEn = "ESTIMATION_CELL_COLLECTION_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 23
                    }
                    },
                    { "estimation_cell_collection_label_cs", new ColumnMetadata()
                    {
                        Name = "estimation_cell_collection_label_cs",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = String.Empty,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATION_CELL_COLLECTION_LABEL_CS",
                        HeaderTextEn = "ESTIMATION_CELL_COLLECTION_LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 24
                    }
                    },
                    { "estimation_cell_collection_label_en", new ColumnMetadata()
                    {
                        Name = "estimation_cell_collection_label_en",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = String.Empty,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATION_CELL_COLLECTION_LABEL_EN",
                        HeaderTextEn = "ESTIMATION_CELL_COLLECTION_LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 25
                    }
                    }
                };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column numerator_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColNumeratorId = Cols["numerator_id"];

                /// <summary>
                /// Column denominator_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColDenominatorId = Cols["denominator_id"];

                /// <summary>
                /// Column numerator_phase_estimate_type_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColNumeratorPhaseEstimateTypeId = Cols["numerator_phase_estimate_type_id"];

                /// <summary>
                /// Column numerator_phase_estimate_type_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColNumeratorPhaseEstimateTypeLabelCs = Cols["numerator_phase_estimate_type_label_cs"];

                /// <summary>
                /// Column numerator_phase_estimate_type_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColNumeratorPhaseEstimateTypeLabelEn = Cols["numerator_phase_estimate_type_label_en"];

                /// <summary>
                /// Column denominator_phase_estimate_type_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColDenominatorPhaseEstimateTypeId = Cols["denominator_phase_estimate_type_id"];

                /// <summary>
                /// Column denominator_phase_estimate_type_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColDenominatorPhaseEstimateTypeLabelCs = Cols["denominator_phase_estimate_type_label_cs"];

                /// <summary>
                /// Column denominator_phase_estimate_type_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColDenominatorPhaseEstimateTypeLabelEn = Cols["denominator_phase_estimate_type_label_en"];

                /// <summary>
                /// Column estimation_period_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationPeriodId = Cols["estimation_period_id"];

                /// <summary>
                /// Column estimate_date_begin metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimateDateBegin = Cols["estimate_date_begin"];

                /// <summary>
                /// Column estimate_date_end metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimateDateEnd = Cols["estimate_date_end"];

                /// <summary>
                /// Column estimation_period_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationPeriodLabelCs = Cols["estimation_period_label_cs"];

                /// <summary>
                /// Column estimation_period_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationPeriodLabelEn = Cols["estimation_period_label_en"];

                /// <summary>
                /// Column numerator_variable_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColNumeratorVariableId = Cols["numerator_variable_id"];

                /// <summary>
                /// Column numerator_variable_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColNumeratorVariableLabelCs = Cols["numerator_variable_label_cs"];

                /// <summary>
                /// Column numerator_variable_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColNumeratorVariableLabelEn = Cols["numerator_variable_label_en"];

                /// <summary>
                /// Column denominator_variable_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColDenominatorVariableId = Cols["denominator_variable_id"];

                /// <summary>
                /// Column denominator_variable_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColDenominatorVariableLabelCs = Cols["denominator_variable_label_cs"];

                /// <summary>
                /// Column denominator_variable_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColDenominatorVariableLabelEn = Cols["denominator_variable_label_en"];

                /// <summary>
                /// Column estimation_cell_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationCellId = Cols["estimation_cell_id"];

                /// <summary>
                /// Column estimation_cell_description_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationCellDescriptionCs = Cols["estimation_cell_description_cs"];

                /// <summary>
                /// Column estimation_cell_description_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationCellDescriptionEn = Cols["estimation_cell_description_en"];

                /// <summary>
                /// Column estimation_cell_collection_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationCellCollectionId = Cols["estimation_cell_collection_id"];

                /// <summary>
                /// Column estimation_cell_collection_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationCellCollectionLabelCs = Cols["estimation_cell_collection_label_cs"];

                /// <summary>
                /// Column estimation_cell_collection_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationCellCollectionLabelEn = Cols["estimation_cell_collection_label_en"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public OLAPRatioEstimateDimensionList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public OLAPRatioEstimateDimensionList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// List of OLAP Cube Dimensions - Ratio Estimate (read-only)
                /// </summary>
                public List<OLAPRatioEstimateDimension> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new OLAPRatioEstimateDimension(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// OLAP Cube Dimension - Ratio Estimate from list by identifier (read-only)
                /// </summary>
                /// <param name="id">OLAP Cube Dimension - Ratio Estimate identifier</param>
                /// <returns>OLAP Cube Dimension - Ratio Estimate from list by identifier (null if not found)</returns>
                public OLAPRatioEstimateDimension this[string id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<string>(ColId.Name) == id)
                                .Select(a => new OLAPRatioEstimateDimension(composite: this, data: a))
                                .FirstOrDefault<OLAPRatioEstimateDimension>();
                    }
                }

                #endregion Indexer


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public OLAPRatioEstimateDimensionList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new OLAPRatioEstimateDimensionList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new OLAPRatioEstimateDimensionList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                /// <summary>
                /// Loads data into OLAP Cube Dimensions - Ratio Estimate (if it was not done before)
                /// </summary>
                /// <param name="cPhaseEstimateTypeNumerator">List of phase estimate types for numerator</param>
                /// <param name="cPhaseEstimateTypeDenominator">List of phase estimate types for denominator</param>
                /// <param name="cEstimationPeriod">List of estimation periods</param>
                /// <param name="variablePairs">List of attribute categories pairs for numerator and denominator</param>
                /// <param name="cEstimationCell">List of estimation cells</param>
                /// <param name="cEstimationCellCollection">List of estimation cell collections</param>
                /// <param name="condition">Condition for selection data</param>
                /// <param name="limit">Rows count limit</param>
                public void Load(
                    PhaseEstimateTypeList cPhaseEstimateTypeNumerator,
                    PhaseEstimateTypeList cPhaseEstimateTypeDenominator,
                    EstimationPeriodList cEstimationPeriod,
                    VariablePairList variablePairs,
                    EstimationCellList cEstimationCell,
                    EstimationCellCollectionList cEstimationCellCollection,
                    string condition = null,
                    Nullable<int> limit = null)
                {
                    if (!Loaded)
                    {
                        ReLoad(
                            cPhaseEstimateTypeNumerator: cPhaseEstimateTypeNumerator,
                            cPhaseEstimateTypeDenominator: cPhaseEstimateTypeDenominator,
                            cEstimationPeriod: cEstimationPeriod,
                            variablePairs: variablePairs,
                            cEstimationCell: cEstimationCell,
                            cEstimationCellCollection: cEstimationCellCollection,
                            condition: condition,
                            limit: limit);
                    }
                }

                /// <summary>
                /// Loads data into OLAP Cube Dimensions - Ratio Estimate
                /// </summary>
                /// <param name="cPhaseEstimateTypeNumerator">List of phase estimate types for numerator</param>
                /// <param name="cPhaseEstimateTypeDenominator">List of phase estimate types for denominator</param>
                /// <param name="cEstimationPeriod">List of estimation periods</param>
                /// <param name="variablePairs">List of attribute categories pairs for numerator and denominator</param>
                /// <param name="cEstimationCell">List of estimation cells</param>
                /// <param name="cEstimationCellCollection">List of estimation cell collections</param>
                /// <param name="condition">Condition for selection data</param>
                /// <param name="limit">Rows count limit</param>
                public void ReLoad(
                    PhaseEstimateTypeList cPhaseEstimateTypeNumerator,
                    PhaseEstimateTypeList cPhaseEstimateTypeDenominator,
                    EstimationPeriodList cEstimationPeriod,
                    VariablePairList variablePairs,
                    EstimationCellList cEstimationCell,
                    EstimationCellCollectionList cEstimationCellCollection,
                    string condition = null,
                    Nullable<int> limit = null)
                {
                    Data = EmptyDataTable(
                                 tableName: TableName,
                                 columns: Columns);
                    Condition = condition;
                    Limit = limit;

                    System.Diagnostics.Stopwatch stopWatch = new();
                    stopWatch.Start();

                    #region ctePhaseEstimateType: Fáze odhadu (1.dimenze)
                    var ctePhaseEstimateTypeNumerator =
                        cPhaseEstimateTypeNumerator.Data.AsEnumerable()
                        .Select(a => new
                        {
                            NumeratorPhaseEstimateTypeId = a.Field<int>(columnName: PhaseEstimateTypeList.ColId.Name),
                            NumeratorPhaseEstimateTypeLabelCs = a.Field<string>(columnName: PhaseEstimateTypeList.ColLabelCs.Name),
                            NumeratorPhaseEstimateTypeLabelEn = a.Field<string>(columnName: PhaseEstimateTypeList.ColLabelEn.Name),
                            NumeratorPhaseEstimateTypeDescriptionCs = a.Field<string>(columnName: PhaseEstimateTypeList.ColDescriptionCs.Name),
                            NumeratorPhaseEstimateTypeDescriptionEn = a.Field<string>(columnName: PhaseEstimateTypeList.ColDescriptionEn.Name)
                        });
                    var ctePhaseEstimateTypeDenominator =
                        cPhaseEstimateTypeDenominator.Data.AsEnumerable()
                        .Select(a => new
                        {
                            DenominatorPhaseEstimateTypeId = a.Field<int>(columnName: PhaseEstimateTypeList.ColId.Name),
                            DenominatorPhaseEstimateTypeLabelCs = a.Field<string>(columnName: PhaseEstimateTypeList.ColLabelCs.Name),
                            DenominatorPhaseEstimateTypeLabelEn = a.Field<string>(columnName: PhaseEstimateTypeList.ColLabelEn.Name),
                            DenominatorPhaseEstimateTypeDescriptionCs = a.Field<string>(columnName: PhaseEstimateTypeList.ColDescriptionCs.Name),
                            DenominatorPhaseEstimateTypeDescriptionEn = a.Field<string>(columnName: PhaseEstimateTypeList.ColDescriptionEn.Name)
                        });
                    #endregion ctePhaseEstimateType: Fáze odhadu (1.dimenze)

                    #region cteEstimationPeriod: Období odhadu (2.dimenze)
                    var cteEstimationPeriod =
                        cEstimationPeriod.Data.AsEnumerable()
                        .Select(a => new
                        {
                            EstimationPeriodId = a.Field<int>(columnName: EstimationPeriodList.ColId.Name),
                            EstimateDateBegin = a.Field<DateTime>(columnName: EstimationPeriodList.ColEstimateDateBegin.Name),
                            EstimateDateEnd = a.Field<DateTime>(columnName: EstimationPeriodList.ColEstimateDateEnd.Name),
                            EstimationPeriodLabelCs = a.Field<string>(columnName: EstimationPeriodList.ColLabelCs.Name),
                            EstimationPeriodLabelEn = a.Field<string>(columnName: EstimationPeriodList.ColLabelEn.Name),
                            EstimationPeriodDescriptionCs = a.Field<string>(columnName: EstimationPeriodList.ColDescriptionCs.Name),
                            EstimationPeriodDescriptionEn = a.Field<string>(columnName: EstimationPeriodList.ColDescriptionEn.Name)
                        });
                    #endregion cteEstimationPeriod: Období odhadu (2.dimenze)

                    #region cteAttributeCategory: Atributové kategorie čitatele a jmenovatele (3.dimenze)
                    var cteAttributeCategory =
                        variablePairs.VariablePairs
                        .Select(a => new
                        {
                            NumeratorVariableId = a.Numerator.VariableId,
                            NumeratorVariableLabelCs = a.Numerator.VariableLabelCs,
                            NumeratorVariableLabelEn = a.Numerator.VariableLabelEn,
                            DenominatorVariableId = a.Denominator.VariableId,
                            DenominatorVariableLabelCs = a.Denominator.VariableLabelCs,
                            DenominatorVariableLabelEn = a.Denominator.VariableLabelEn
                        });
                    #endregion cteAttributeCategory: Atributové kategorie čitatele a jmenovatele (3.dimenze)

                    #region cteEstimationCell: Výpočetní buňky (4.dimenze)
                    var cteEstimationCell =
                        cEstimationCell.Data.AsEnumerable()
                        .Select(a => new
                        {
                            EstimationCellId = a.Field<int>(columnName: EstimationCellList.ColId.Name),
                            EstimationCellCollectionId = a.Field<int>(columnName: EstimationCellList.ColEstimationCellCollectionId.Name),
                            EstimationCellLabelCs = a.Field<string>(columnName: EstimationCellList.ColLabelCs.Name),
                            EstimationCellLabelEn = a.Field<string>(columnName: EstimationCellList.ColLabelEn.Name),
                            EstimationCellDescriptionCs = a.Field<string>(columnName: EstimationCellList.ColDescriptionCs.Name),
                            EstimationCellDescriptionEn = a.Field<string>(columnName: EstimationCellList.ColDescriptionEn.Name)
                        });
                    #endregion cteEstimationCell: Výpočetní buňky (4.dimenze)

                    #region cteEstimationCellCollection: Skupiny výpočetních buněk
                    var cteEstimationCellCollection =
                        cEstimationCellCollection.Data.AsEnumerable()
                        .Select(a => new
                        {
                            EstimationCellCollectionId = a.Field<int>(columnName: EstimationCellCollectionList.ColId.Name),
                            EstimationCellCollectionLabelCs = a.Field<string>(columnName: EstimationCellCollectionList.ColLabelCs.Name),
                            EstimationCellCollectionLabelEn = a.Field<string>(columnName: EstimationCellCollectionList.ColLabelEn.Name),
                            EstimationCellCollectionDescriptionCs = a.Field<string>(columnName: EstimationCellCollectionList.ColDescriptionCs.Name),
                            EstimationCellCollectionDescriptionEn = a.Field<string>(columnName: EstimationCellCollectionList.ColDescriptionEn.Name)
                        });
                    #endregion cteEstimationCellCollection: Skupiny výpočetních buněk

                    #region cteEstimationCellExtended: Výpočetní buňky doplněné o příslušnost ke skupině výpočetních buněk
                    var cteEstimationCellExtended =
                        from a in cteEstimationCell
                        join b in cteEstimationCellCollection
                        on a.EstimationCellCollectionId equals b.EstimationCellCollectionId into lj
                        from b in lj.DefaultIfEmpty()
                        select new
                        {
                            a.EstimationCellId,
                            a.EstimationCellLabelCs,
                            a.EstimationCellLabelEn,
                            a.EstimationCellDescriptionCs,
                            a.EstimationCellDescriptionEn,
                            b?.EstimationCellCollectionId,
                            b?.EstimationCellCollectionLabelCs,
                            b?.EstimationCellCollectionLabelEn,
                            b?.EstimationCellCollectionDescriptionCs,
                            b?.EstimationCellCollectionDescriptionEn
                        };
                    #endregion cteEstimationCellExtended: Výpočetní buňky doplněné o příslušnost ke skupině výpočetních buněk

                    #region cteCartesianProduct: Kartézský součin dimenzí
                    var cteCartesianProduct =
                        from a in ctePhaseEstimateTypeNumerator
                        from b in ctePhaseEstimateTypeDenominator
                        from c in cteEstimationPeriod
                        from d in cteAttributeCategory
                        from e in cteEstimationCellExtended
                        select new
                        {
                            Id = String.Format("{0}:{1}-{2}-{3}:{4}-{5}",
                                a.NumeratorPhaseEstimateTypeId.ToString(),
                                b.DenominatorPhaseEstimateTypeId.ToString(),
                                c.EstimationPeriodId.ToString(),
                                d.NumeratorVariableId.ToString(),
                                d.DenominatorVariableId.ToString(),
                                e.EstimationCellId.ToString()),
                            NumeratorId = String.Format("{0}-{1}-{2}-{3}",
                                a.NumeratorPhaseEstimateTypeId.ToString(),
                                c.EstimationPeriodId.ToString(),
                                d.NumeratorVariableId.ToString(),
                                e.EstimationCellId.ToString()),
                            DenominatorId = String.Format("{0}-{1}-{2}-{3}",
                                b.DenominatorPhaseEstimateTypeId.ToString(),
                                c.EstimationPeriodId.ToString(),
                                d.DenominatorVariableId.ToString(),
                                e.EstimationCellId.ToString()),
                            a.NumeratorPhaseEstimateTypeId,
                            a.NumeratorPhaseEstimateTypeLabelCs,
                            a.NumeratorPhaseEstimateTypeLabelEn,
                            a.NumeratorPhaseEstimateTypeDescriptionCs,
                            a.NumeratorPhaseEstimateTypeDescriptionEn,
                            b.DenominatorPhaseEstimateTypeId,
                            b.DenominatorPhaseEstimateTypeLabelCs,
                            b.DenominatorPhaseEstimateTypeLabelEn,
                            b.DenominatorPhaseEstimateTypeDescriptionCs,
                            b.DenominatorPhaseEstimateTypeDescriptionEn,
                            c.EstimationPeriodId,
                            c.EstimateDateBegin,
                            c.EstimateDateEnd,
                            c.EstimationPeriodLabelCs,
                            c.EstimationPeriodLabelEn,
                            c.EstimationPeriodDescriptionCs,
                            c.EstimationPeriodDescriptionEn,
                            d.NumeratorVariableId,
                            d.NumeratorVariableLabelCs,
                            d.NumeratorVariableLabelEn,
                            d.DenominatorVariableId,
                            d.DenominatorVariableLabelCs,
                            d.DenominatorVariableLabelEn,
                            e.EstimationCellId,
                            e.EstimationCellLabelCs,
                            e.EstimationCellLabelEn,
                            e.EstimationCellDescriptionCs,
                            e.EstimationCellDescriptionEn,
                            e.EstimationCellCollectionId,
                            e.EstimationCellCollectionLabelCs,
                            e.EstimationCellCollectionLabelEn,
                            e.EstimationCellCollectionDescriptionCs,
                            e.EstimationCellCollectionDescriptionEn
                        };
                    #endregion cteCartesianProduct: Kartézský součin dimenzí

                    #region Zápis výsledku do DataTable
                    if (cteCartesianProduct.Any())
                    {
                        Data =
                            cteCartesianProduct
                            .OrderBy(a => a.NumeratorPhaseEstimateTypeId)
                            .ThenBy(a => a.DenominatorPhaseEstimateTypeId)
                            .ThenBy(a => a.EstimationPeriodId)
                            .ThenBy(a => a.NumeratorVariableId)
                            .ThenBy(a => a.DenominatorVariableId)
                            .ThenBy(a => a.EstimationCellId)
                            .Select(a =>
                                Data.LoadDataRow(
                                    values: [
                                        a.Id,
                                        a.NumeratorId,
                                        a.DenominatorId,
                                        a.NumeratorPhaseEstimateTypeId,
                                        a.NumeratorPhaseEstimateTypeLabelCs,
                                        a.NumeratorPhaseEstimateTypeLabelEn,
                                        a.DenominatorPhaseEstimateTypeId,
                                        a.DenominatorPhaseEstimateTypeLabelCs,
                                        a.DenominatorPhaseEstimateTypeLabelEn,
                                        a.EstimationPeriodId,
                                        a.EstimateDateBegin,
                                        a.EstimateDateEnd,
                                        a.EstimationPeriodLabelCs,
                                        a.EstimationPeriodLabelEn,
                                        a.NumeratorVariableId,
                                        a.NumeratorVariableLabelCs,
                                        a.NumeratorVariableLabelEn,
                                        a.DenominatorVariableId,
                                        a.DenominatorVariableLabelCs,
                                        a.DenominatorVariableLabelEn,
                                        a.EstimationCellId,
                                        a.EstimationCellDescriptionCs,
                                        a.EstimationCellDescriptionEn,
                                        a.EstimationCellCollectionId,
                                        a.EstimationCellCollectionLabelCs,
                                        a.EstimationCellCollectionLabelEn
                                    ],
                                fAcceptChanges: false))
                            .CopyToDataTable();
                        Data.TableName = Name;
                    }
                    #endregion Zápis výsledku do DataTable

                    #region Omezení podmínkou
                    if (!String.IsNullOrEmpty(value: Condition))
                    {
                        DataView view = new(table: Data);
                        try
                        {
                            view.RowFilter = Condition;
                        }
                        catch
                        {
                            view.RowFilter = String.Empty;
                            Condition = String.Empty;
                        }
                        Data = view.ToTable();
                    }
                    #endregion Omezení podmínkou

                    #region Omezení počtem
                    if (Limit != null)
                    {
                        Data = Data.AsEnumerable()
                            .Take(count: (int)Limit)
                            .CopyToDataTable();
                    }
                    #endregion Omezení počtem

                    stopWatch.Stop();
                    LoadingTime = 0.001 * stopWatch.ElapsedMilliseconds;
                    Data.TableName = TableName;
                }

                #endregion Methods

            }

        }
    }
}


