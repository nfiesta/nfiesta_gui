﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {
            // v_olap_ratio_estimate

            /// <summary>
            /// OLAP Cube - Ratio Estimate
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class OLAPRatioEstimate(
                OLAPRatioEstimateList composite = null,
                DataRow data = null)
                    : ALinqViewEntry<OLAPRatioEstimateList>(composite: composite, data: data)
            {

                #region Derived Properties

                #region Dimensions

                /// <summary>
                /// OLAP Cube Dimension - Ratio Estimate - composite identifier
                /// </summary>
                public new string Id
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColId.Name,
                            defaultValue: OLAPRatioEstimateList.ColId.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// OLAP Cube Dimension - Ratio Estimate - composite identifier for numerator
                /// </summary>
                public string NumeratorId
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColNumeratorId.Name,
                            defaultValue: OLAPRatioEstimateList.ColNumeratorId.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColNumeratorId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// OLAP Cube Dimension - Ratio Estimate - composite identifier for denominator
                /// </summary>
                public string DenominatorId
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColDenominatorId.Name,
                            defaultValue: OLAPRatioEstimateList.ColDenominatorId.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColDenominatorId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Numerator estimate phase identifier
                /// </summary>
                public int NumeratorPhaseEstimateTypeId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColNumeratorPhaseEstimateTypeId.Name,
                            defaultValue: Int32.Parse(s: OLAPRatioEstimateList.ColNumeratorPhaseEstimateTypeId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColNumeratorPhaseEstimateTypeId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Label of numerator estimate phase (in national language)
                /// </summary>
                public string NumeratorPhaseEstimateTypeLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColNumeratorPhaseEstimateTypeLabelCs.Name,
                            defaultValue: OLAPRatioEstimateList.ColNumeratorPhaseEstimateTypeLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColNumeratorPhaseEstimateTypeLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Label of numerator estimate phase (in English)
                /// </summary>
                public string NumeratorPhaseEstimateTypeLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColNumeratorPhaseEstimateTypeLabelEn.Name,
                            defaultValue: OLAPRatioEstimateList.ColNumeratorPhaseEstimateTypeLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColNumeratorPhaseEstimateTypeLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Numerator estimate phase object (read-only)
                /// </summary>
                public PhaseEstimateType NumeratorPhaseEstimateType
                {
                    get
                    {
                        return
                             ((NfiEstaDB)Composite.Database).SNfiEsta.CPhaseEstimateType[(int)NumeratorPhaseEstimateTypeId];
                    }
                }


                /// <summary>
                /// Denominator estimate phase identifier
                /// </summary>
                public int DenominatorPhaseEstimateTypeId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColDenominatorPhaseEstimateTypeId.Name,
                            defaultValue: Int32.Parse(s: OLAPRatioEstimateList.ColDenominatorPhaseEstimateTypeId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColDenominatorPhaseEstimateTypeId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Label of numerator estimate phase (in national language)
                /// </summary>
                public string DenominatorPhaseEstimateTypeLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColDenominatorPhaseEstimateTypeLabelCs.Name,
                            defaultValue: OLAPRatioEstimateList.ColDenominatorPhaseEstimateTypeLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColDenominatorPhaseEstimateTypeLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Label of numerator estimate phase (in English)
                /// </summary>
                public string DenominatorPhaseEstimateTypeLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColDenominatorPhaseEstimateTypeLabelEn.Name,
                            defaultValue: OLAPRatioEstimateList.ColDenominatorPhaseEstimateTypeLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColDenominatorPhaseEstimateTypeLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Denominator estimate phase object (read-only)
                /// </summary>
                public PhaseEstimateType DenominatorPhaseEstimateType
                {
                    get
                    {
                        return
                             ((NfiEstaDB)Composite.Database).SNfiEsta.CPhaseEstimateType[(int)DenominatorPhaseEstimateTypeId];
                    }
                }


                /// <summary>
                /// Estimation period identifier
                /// </summary>
                public int EstimationPeriodId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColEstimationPeriodId.Name,
                            defaultValue: Int32.Parse(s: OLAPRatioEstimateList.ColEstimationPeriodId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColEstimationPeriodId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// The beginning of the time to witch the estimate is calculated
                /// </summary>
                public DateTime EstimateDateBegin
                {
                    get
                    {
                        return Functions.GetDateTimeArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColEstimateDateBegin.Name,
                            defaultValue: DateTime.Parse(s: OLAPRatioEstimateList.ColEstimateDateBegin.DefaultValue));
                    }
                    set
                    {
                        Functions.SetDateTimeArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColEstimateDateBegin.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// The beginning of the time to witch the estimate is calculated as text
                /// </summary>
                public string EstimateDateBeginText
                {
                    get
                    {
                        return
                            EstimateDateBegin.ToString(
                                format: OLAPRatioEstimateList.ColEstimateDateBegin.NumericFormat);
                    }
                }

                /// <summary>
                /// The end of the time to witch the estimate is calculated
                /// </summary>
                public DateTime EstimateDateEnd
                {
                    get
                    {
                        return Functions.GetDateTimeArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColEstimateDateEnd.Name,
                            defaultValue: DateTime.Parse(s: OLAPRatioEstimateList.ColEstimateDateEnd.DefaultValue));
                    }
                    set
                    {
                        Functions.SetDateTimeArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColEstimateDateEnd.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// The end of the time to witch the estimate is calculated as text
                /// </summary>
                public string EstimateDateEndText
                {
                    get
                    {
                        return
                            EstimateDateEnd.ToString(
                                format: OLAPRatioEstimateList.ColEstimateDateEnd.NumericFormat);
                    }
                }
                /// <summary>
                /// Estimation period label (in national language)
                /// </summary>
                public string EstimationPeriodLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColEstimationPeriodLabelCs.Name,
                            defaultValue: OLAPRatioEstimateList.ColEstimationPeriodLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColEstimationPeriodLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Estimation period label (in English)
                /// </summary>
                public string EstimationPeriodLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColEstimationPeriodLabelEn.Name,
                            defaultValue: OLAPRatioEstimateList.ColEstimationPeriodLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColEstimationPeriodLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Period object (read-only)
                /// </summary>
                public EstimationPeriod EstimationPeriod
                {
                    get
                    {
                        return
                             ((NfiEstaDB)Composite.Database).SNfiEsta.CEstimationPeriod[(int)EstimationPeriodId];
                    }
                }


                /// <summary>
                /// Numerator - Combination of target variable and sub-population category and area domain category identifier
                /// </summary>
                public int NumeratorVariableId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColNumeratorVariableId.Name,
                            defaultValue: Int32.Parse(s: OLAPRatioEstimateList.ColNumeratorVariableId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColNumeratorVariableId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Numerator - Label of variable (in national language)
                /// </summary>
                public string NumeratorVariableLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColNumeratorVariableLabelCs.Name,
                            defaultValue: OLAPRatioEstimateList.ColNumeratorVariableLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColNumeratorVariableLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Numerator - Label of variable (in English)
                /// </summary>
                public string NumeratorVariableLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColNumeratorVariableLabelEn.Name,
                            defaultValue: OLAPRatioEstimateList.ColNumeratorVariableLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColNumeratorVariableLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Numerator - Combination of target variable and sub-population category and area domain category object (read-only)
                /// </summary>
                public Variable NumeratorVariable
                {
                    get
                    {
                        return
                             ((NfiEstaDB)Composite.Database).SNfiEsta.TVariable[(int)NumeratorVariableId];
                    }
                }

                /// <summary>
                /// Numerator - Combination of target variable and sub-population category and area domain category (extended-version) object (read-only)
                /// </summary>
                public VwVariable NumeratorVwVariable
                {
                    get
                    {
                        return
                             ((NfiEstaDB)Composite.Database).SNfiEsta.VVariable[(int)NumeratorVariableId];
                    }
                }


                /// <summary>
                /// Denominator - Combination of target variable and sub-population category and area domain category identifier
                /// </summary>
                public int DenominatorVariableId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColDenominatorVariableId.Name,
                            defaultValue: Int32.Parse(s: OLAPRatioEstimateList.ColDenominatorVariableId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColDenominatorVariableId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Denominator - Label of variable (in national language)
                /// </summary>
                public string DenominatorVariableLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColDenominatorVariableLabelCs.Name,
                            defaultValue: OLAPRatioEstimateList.ColDenominatorVariableLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColDenominatorVariableLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Denominator - Label of variable (in English)
                /// </summary>
                public string DenominatorVariableLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColDenominatorVariableLabelEn.Name,
                            defaultValue: OLAPRatioEstimateList.ColDenominatorVariableLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColDenominatorVariableLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Denominator - Combination of target variable and sub-population category and area domain category object (read-only)
                /// </summary>
                public Variable DenominatorVariable
                {
                    get
                    {
                        return
                             ((NfiEstaDB)Composite.Database).SNfiEsta.TVariable[(int)DenominatorVariableId];
                    }
                }

                /// <summary>
                /// Denominator - Combination of target variable and sub-population category and area domain category (extended-version) object (read-only)
                /// </summary>
                public VwVariable DenominatorVwVariable
                {
                    get
                    {
                        return
                             ((NfiEstaDB)Composite.Database).SNfiEsta.VVariable[(int)DenominatorVariableId];
                    }
                }


                /// <summary>
                /// Estimation cell identifier
                /// </summary>
                public int EstimationCellId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColEstimationCellId.Name,
                            defaultValue: Int32.Parse(s: OLAPRatioEstimateList.ColEstimationCellId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColEstimationCellId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Label of estimation cell (in national language)
                /// </summary>
                public string EstimationCellDescriptionCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColEstimationCellDescriptionCs.Name,
                            defaultValue: OLAPRatioEstimateList.ColEstimationCellDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColEstimationCellDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Label of estimation cell (in English)
                /// </summary>
                public string EstimationCellDescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColEstimationCellDescriptionEn.Name,
                            defaultValue: OLAPRatioEstimateList.ColEstimationCellDescriptionEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColEstimationCellDescriptionEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Estimation cell object (read-only)
                /// </summary>
                public EstimationCell EstimationCell
                {
                    get
                    {
                        return
                             ((NfiEstaDB)Composite.Database).SNfiEsta.CEstimationCell[(int)EstimationCellId];
                    }
                }


                /// <summary>
                /// Estimation cell collection identifier
                /// </summary>
                public int EstimationCellCollectionId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColEstimationCellCollectionId.Name,
                            defaultValue: Int32.Parse(s: OLAPRatioEstimateList.ColEstimationCellCollectionId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColEstimationCellCollectionId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Label of estimation cell collection (in national language)
                /// </summary>
                public string EstimationCellCollectionLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColEstimationCellCollectionLabelCs.Name,
                            defaultValue: OLAPRatioEstimateList.ColEstimationCellCollectionLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColEstimationCellCollectionLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Label of estimation cell collection (in English)
                /// </summary>
                public string EstimationCellCollectionLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColEstimationCellCollectionLabelEn.Name,
                            defaultValue: OLAPRatioEstimateList.ColEstimationCellCollectionLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColEstimationCellCollectionLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Estimation cell collection object (read-only)
                /// </summary>
                public EstimationCellCollection EstimationCellCollection
                {
                    get
                    {
                        return
                             ((NfiEstaDB)Composite.Database).SNfiEsta.CEstimationCellCollection[(int)EstimationCellCollectionId];
                    }
                }

                #endregion Dimensions


                #region Values

                /// <summary>
                /// Aggregated sets of panels and corresponding reference year sets - identifier
                /// </summary>
                public Nullable<int> PanelRefYearSetGroupId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColPanelRefYearSetGroupId.Name,
                            defaultValue: String.IsNullOrEmpty(value: OLAPRatioEstimateList.ColPanelRefYearSetGroupId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: OLAPRatioEstimateList.ColPanelRefYearSetGroupId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColPanelRefYearSetGroupId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Aggregated sets of panels and corresponding reference year sets - label (in national language)
                /// </summary>
                public string PanelRefYearSetGroupLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColPanelRefYearSetGroupLabelCs.Name,
                            defaultValue: OLAPRatioEstimateList.ColPanelRefYearSetGroupLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColPanelRefYearSetGroupLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Aggregated sets of panels and corresponding reference year sets - label (in English)
                /// </summary>
                public string PanelRefYearSetGroupLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColPanelRefYearSetGroupLabelEn.Name,
                            defaultValue: OLAPRatioEstimateList.ColPanelRefYearSetGroupLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColPanelRefYearSetGroupLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Aggregated sets of panels and corresponding reference year sets - object (read-only)
                /// </summary>
                public PanelRefYearSetGroup PanelRefYearSetGroup
                {
                    get
                    {
                        return (PanelRefYearSetGroupId != null) ?
                             ((NfiEstaDB)Composite.Database).SNfiEsta.CPanelRefYearSetGroup[(int)PanelRefYearSetGroupId] : null;
                    }
                }


                /// <summary>
                /// Estimate configuration identifier
                /// </summary>
                public Nullable<int> EstimateConfId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColEstimateConfId.Name,
                            defaultValue: String.IsNullOrEmpty(value: OLAPRatioEstimateList.ColEstimateConfId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: OLAPRatioEstimateList.ColEstimateConfId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColEstimateConfId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Estimate configuration object (read-only)
                /// </summary>
                public EstimateConf EstimateConf
                {
                    get
                    {
                        return (EstimateConfId != null) ?
                             ((NfiEstaDB)Composite.Database).SNfiEsta.TEstimateConf[(int)EstimateConfId] : null;
                    }
                }


                /// <summary>
                /// Estimate type (total or ratio) identifier
                /// </summary>
                public Nullable<int> EstimateTypeId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColEstimateTypeId.Name,
                            defaultValue: String.IsNullOrEmpty(value: OLAPRatioEstimateList.ColEstimateTypeId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: OLAPRatioEstimateList.ColEstimateTypeId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColEstimateTypeId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Estimate type (total or ratio) object (read-only)
                /// </summary>
                public EstimateType EstimateType
                {
                    get
                    {
                        return (EstimateTypeId != null) ?
                             ((NfiEstaDB)Composite.Database).SNfiEsta.CEstimateType[(int)EstimateTypeId] : null;
                    }
                }


                /// <summary>
                /// Numerator - Total estimate configuration identifier
                /// </summary>
                public Nullable<int> NumeratorTotalEstimateConfId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColNumeratorTotalEstimateConfId.Name,
                            defaultValue: String.IsNullOrEmpty(value: OLAPRatioEstimateList.ColNumeratorTotalEstimateConfId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: OLAPRatioEstimateList.ColNumeratorTotalEstimateConfId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColNumeratorTotalEstimateConfId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Numerator - Total estimate configuration object (read-only)
                /// </summary>
                public TotalEstimateConf NumeratorTotalEstimateConf
                {
                    get
                    {
                        return (NumeratorTotalEstimateConfId != null) ?
                             ((NfiEstaDB)Composite.Database).SNfiEsta.TTotalEstimateConf[(int)NumeratorTotalEstimateConfId] : null;
                    }
                }


                /// <summary>
                /// Denominator - Total estimate configuration identifier
                /// </summary>
                public Nullable<int> DenominatorTotalEstimateConfId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColDenominatorTotalEstimateConfId.Name,
                            defaultValue: String.IsNullOrEmpty(value: OLAPRatioEstimateList.ColDenominatorTotalEstimateConfId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: OLAPRatioEstimateList.ColDenominatorTotalEstimateConfId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColDenominatorTotalEstimateConfId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Denominator - Total estimate configuration object (read-only)
                /// </summary>
                public TotalEstimateConf DenominatorTotalEstimateConf
                {
                    get
                    {
                        return (DenominatorTotalEstimateConfId != null) ?
                             ((NfiEstaDB)Composite.Database).SNfiEsta.TTotalEstimateConf[(int)DenominatorTotalEstimateConfId] : null;
                    }
                }


                /// <summary>
                /// User, who configured estimate
                /// </summary>
                public string EstimateExecutor
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColEstimateExecutor.Name,
                            defaultValue: OLAPRatioEstimateList.ColEstimateExecutor.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColEstimateExecutor.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Numerator - Total estimate configuration name (Short description of the estimate)
                /// </summary>
                public string NumeratorTotalEstimateConfName
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColNumeratorTotalEstimateConfName.Name,
                            defaultValue: OLAPRatioEstimateList.ColNumeratorTotalEstimateConfName.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColNumeratorTotalEstimateConfName.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Denominator - Total estimate configuration name (Short description of the estimate)
                /// </summary>
                public string DenominatorTotalEstimateConfName
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColDenominatorTotalEstimateConfName.Name,
                            defaultValue: OLAPRatioEstimateList.ColDenominatorTotalEstimateConfName.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColDenominatorTotalEstimateConfName.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Final estimate identifier
                /// </summary>
                public Nullable<int> ResultId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColResultId.Name,
                            defaultValue: String.IsNullOrEmpty(value: OLAPRatioEstimateList.ColResultId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: OLAPRatioEstimateList.ColResultId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColResultId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Final estimate object (read-only)
                /// </summary>
                public Result Result
                {
                    get
                    {
                        return (ResultId != null) ?
                             ((NfiEstaDB)Composite.Database).SNfiEsta.TResult[(int)ResultId] : null;
                    }
                }


                /// <summary>
                /// Point estimate
                /// </summary>
                public Nullable<double> PointEstimate
                {
                    get
                    {
                        return Functions.GetNDoubleArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColPointEstimate.Name,
                            defaultValue: String.IsNullOrEmpty(value: OLAPRatioEstimateList.ColPointEstimate.DefaultValue) ?
                                (Nullable<double>)null :
                                Double.Parse(s: OLAPRatioEstimateList.ColPointEstimate.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNDoubleArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColPointEstimate.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Variance estimate
                /// </summary>
                public Nullable<double> VarianceEstimate
                {
                    get
                    {
                        return Functions.GetNDoubleArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColVarianceEstimate.Name,
                            defaultValue: String.IsNullOrEmpty(value: OLAPRatioEstimateList.ColVarianceEstimate.DefaultValue) ?
                                (Nullable<double>)null :
                                Double.Parse(s: OLAPRatioEstimateList.ColVarianceEstimate.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNDoubleArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColVarianceEstimate.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Time when calculation started
                /// </summary>
                public Nullable<DateTime> CalcStarted
                {
                    get
                    {
                        return Functions.GetNDateTimeArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColCalcStarted.Name,
                            defaultValue: String.IsNullOrEmpty(value: OLAPRatioEstimateList.ColCalcStarted.DefaultValue) ?
                                            (Nullable<DateTime>)null :
                                            DateTime.Parse(s: OLAPRatioEstimateList.ColCalcStarted.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNDateTimeArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColCalcStarted.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Time when calculation started as text
                /// </summary>
                public string CalcStartedText
                {
                    get
                    {
                        return
                            (CalcStarted == null) ? Functions.StrNull :
                                ((DateTime)CalcStarted).ToString(
                                format: OLAPRatioEstimateList.ColCalcStarted.NumericFormat);
                    }
                }

                /// <summary>
                /// Version of the extension with which the results were calculated
                /// </summary>
                public string ExtensionVersion
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColExtensionVersion.Name,
                            defaultValue: OLAPRatioEstimateList.ColExtensionVersion.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColExtensionVersion.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Time necessary for calculation
                /// </summary>
                public Nullable<TimeSpan> CalcDuration
                {
                    get
                    {
                        return Functions.GetNTimeSpanArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColCalcDuration.Name,
                            defaultValue: String.IsNullOrEmpty(value: OLAPRatioEstimateList.ColCalcDuration.DefaultValue) ?
                                            (Nullable<TimeSpan>)null :
                                            TimeSpan.Parse(s: OLAPRatioEstimateList.ColCalcDuration.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNTimeSpanArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColCalcDuration.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Time necessary for calculation as text
                /// </summary>
                public string CalcDurationText
                {
                    get
                    {
                        return
                            (CalcDuration == null) ? Functions.StrNull :
                                ((TimeSpan)CalcDuration).ToString(
                                format: OLAPRatioEstimateList.ColCalcDuration.NumericFormat);
                    }
                }

                /// <summary>
                /// Minimal sample size
                /// </summary>
                public Nullable<long> MinSSize
                {
                    get
                    {
                        return Functions.GetNLongArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColMinSSize.Name,
                            defaultValue: String.IsNullOrEmpty(value: OLAPRatioEstimateList.ColMinSSize.DefaultValue) ?
                                (Nullable<long>)null :
                                Int64.Parse(s: OLAPRatioEstimateList.ColMinSSize.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNLongArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColMinSSize.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Actual sample size
                /// </summary>
                public Nullable<long> ActSSize
                {
                    get
                    {
                        return Functions.GetNLongArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColActSSize.Name,
                            defaultValue: String.IsNullOrEmpty(value: OLAPRatioEstimateList.ColActSSize.DefaultValue) ?
                                (Nullable<long>)null :
                                Int64.Parse(s: OLAPRatioEstimateList.ColActSSize.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNLongArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColActSSize.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Flag for identification actual value of final estimate
                /// </summary>
                public Nullable<bool> IsLatest
                {
                    get
                    {
                        return Functions.GetNBoolArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColIsLatest.Name,
                            defaultValue: String.IsNullOrEmpty(value: OLAPRatioEstimateList.ColIsLatest.DefaultValue) ?
                                (Nullable<bool>)null :
                                Boolean.Parse(value: OLAPRatioEstimateList.ColIsLatest.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNBoolArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColIsLatest.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// User, who calculated estimate
                /// </summary>
                public string ResultExecutor
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColResultExecutor.Name,
                            defaultValue: OLAPRatioEstimateList.ColResultExecutor.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColResultExecutor.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Standard error
                /// </summary>
                public Nullable<double> StandardError
                {
                    get
                    {
                        return Functions.GetNDoubleArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColStandardError.Name,
                            defaultValue: String.IsNullOrEmpty(value: OLAPRatioEstimateList.ColStandardError.DefaultValue) ?
                                (Nullable<double>)null :
                                Double.Parse(s: OLAPRatioEstimateList.ColStandardError.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNDoubleArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColStandardError.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Confidence interval
                /// </summary>
                public Nullable<double> ConfidenceInterval
                {
                    get
                    {
                        return Functions.GetNDoubleArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColConfidenceInterval.Name,
                            defaultValue: String.IsNullOrEmpty(value: OLAPRatioEstimateList.ColConfidenceInterval.DefaultValue) ?
                                (Nullable<double>)null :
                                Double.Parse(s: OLAPRatioEstimateList.ColConfidenceInterval.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNDoubleArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColConfidenceInterval.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Confidence interval - lower bound
                /// </summary>
                public Nullable<double> LowerBound
                {
                    get
                    {
                        return Functions.GetNDoubleArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColLowerBound.Name,
                            defaultValue: String.IsNullOrEmpty(value: OLAPRatioEstimateList.ColLowerBound.DefaultValue) ?
                                (Nullable<double>)null :
                                Double.Parse(s: OLAPRatioEstimateList.ColLowerBound.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNDoubleArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColLowerBound.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Confidence interval - upper bound
                /// </summary>
                public Nullable<double> UpperBound
                {
                    get
                    {
                        return Functions.GetNDoubleArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColUpperBound.Name,
                            defaultValue: String.IsNullOrEmpty(value: OLAPRatioEstimateList.ColUpperBound.DefaultValue) ?
                                (Nullable<double>)null :
                                Double.Parse(s: OLAPRatioEstimateList.ColUpperBound.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNDoubleArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColUpperBound.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Attribute additivity difference -  sum of lower categories
                /// </summary>
                public Nullable<double> AttrDiffDown
                {
                    get
                    {
                        return Functions.GetNDoubleArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColAttrDiffDown.Name,
                            defaultValue: String.IsNullOrEmpty(value: OLAPRatioEstimateList.ColAttrDiffDown.DefaultValue) ?
                                (Nullable<double>)null :
                                Double.Parse(s: OLAPRatioEstimateList.ColAttrDiffDown.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNDoubleArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColAttrDiffDown.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Attribute additivity difference -  superior category
                /// </summary>
                public Nullable<double> AttrDiffUp
                {
                    get
                    {
                        return Functions.GetNDoubleArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColAttrDiffUp.Name,
                            defaultValue: String.IsNullOrEmpty(value: OLAPRatioEstimateList.ColAttrDiffUp.DefaultValue) ?
                                (Nullable<double>)null :
                                Double.Parse(s: OLAPRatioEstimateList.ColAttrDiffUp.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNDoubleArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColAttrDiffUp.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Estimate is additive
                /// </summary>
                public Nullable<bool> IsAdditive
                {
                    get
                    {
                        return Functions.GetNBoolArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColIsAdditive.Name,
                            defaultValue: String.IsNullOrEmpty(value: OLAPRatioEstimateList.ColIsAdditive.DefaultValue) ?
                                (Nullable<bool>)null :
                                Boolean.Parse(value: OLAPRatioEstimateList.ColIsAdditive.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNBoolArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColIsAdditive.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Estimate configuration status identifier
                /// </summary>
                public int EstimateConfStatusId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColEstimateConfStatusId.Name,
                            defaultValue: Int32.Parse(s: OLAPRatioEstimateList.ColEstimateConfStatusId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColEstimateConfStatusId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Estimate configuration status label (in national language)
                /// </summary>
                public string EstimateConfStatusLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColEstimateConfStatusLabelCs.Name,
                            defaultValue: OLAPRatioEstimateList.ColEstimateConfStatusLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColEstimateConfStatusLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Estimate configuration status label (in English)
                /// </summary>
                public string EstimateConfStatusLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColEstimateConfStatusLabelEn.Name,
                            defaultValue: OLAPRatioEstimateList.ColEstimateConfStatusLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: OLAPRatioEstimateList.ColEstimateConfStatusLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Status object (read-only)
                /// </summary>
                public EstimateConfStatus EstimateConfStatus
                {
                    get
                    {
                        return
                             ((NfiEstaDB)Composite.Database).SNfiEsta.CEstimateConfStatus[(int)EstimateConfStatusId];
                    }
                }

                #endregion Values

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not OLAPRatio Type, return False
                    if (obj is not OLAPRatioEstimate)
                    {
                        return false;
                    }

                    return
                         (NumeratorPhaseEstimateTypeId == ((OLAPRatioEstimate)obj).NumeratorPhaseEstimateTypeId) &&
                         (DenominatorPhaseEstimateTypeId == ((OLAPRatioEstimate)obj).DenominatorPhaseEstimateTypeId) &&
                         (EstimationPeriodId == ((OLAPRatioEstimate)obj).EstimationPeriodId) &&
                         (NumeratorVariableId == ((OLAPRatioEstimate)obj).NumeratorVariableId) &&
                         (DenominatorVariableId == ((OLAPRatioEstimate)obj).DenominatorVariableId) &&
                         (EstimationCellId == ((OLAPRatioEstimate)obj).EstimationCellId) &&
                         (PanelRefYearSetGroupId == ((OLAPRatioEstimate)obj).PanelRefYearSetGroupId) &&
                         (EstimateConfId == ((OLAPRatioEstimate)obj).EstimateConfId) &&
                         (NumeratorTotalEstimateConfId == ((OLAPRatioEstimate)obj).NumeratorTotalEstimateConfId) &&
                         (DenominatorTotalEstimateConfId == ((OLAPRatioEstimate)obj).DenominatorTotalEstimateConfId) &&
                         (ResultId == ((OLAPRatioEstimate)obj).ResultId);
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                       NumeratorPhaseEstimateTypeId.GetHashCode() ^
                       DenominatorPhaseEstimateTypeId.GetHashCode() ^
                       EstimationPeriodId.GetHashCode() ^
                       NumeratorVariableId.GetHashCode() ^
                       DenominatorVariableId.GetHashCode() ^
                       EstimationCellId.GetHashCode() ^
                       PanelRefYearSetGroupId.GetHashCode() ^
                       EstimateConfId.GetHashCode() ^
                       NumeratorTotalEstimateConfId.GetHashCode() ^
                       DenominatorTotalEstimateConfId.GetHashCode() ^
                       ResultId.GetHashCode();
                }

                /// <summary>
                /// Text description of the object
                /// </summary>
                /// <returns>Text description of the object</returns>
                public override string ToString()
                {
                    return String.Concat(
                        $"{"ID"} = {Functions.PrepStringArg(arg: Id)}{Environment.NewLine}");
                }

                #endregion Methods

            }


            /// <summary>
            /// OLAP Cube - List of Ratio Estimates
            /// </summary>
            public class OLAPRatioEstimateList
                : ALinqView
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema =
                    NfiEstaSchema.Name;

                /// <summary>
                /// Table name
                /// </summary>
                public static readonly string Name =
                    "v_olap_ratio_estimate";

                /// <summary>
                /// Table alias
                /// </summary>
                public static readonly string Alias =
                    "v_olap_ratio_estimate";

                /// <summary>
                /// Description in national language
                /// </summary>
                public static readonly string CaptionCs =
                    "OLAP kostka s daty odhadů podílů";

                /// <summary>
                /// Description in English
                /// </summary>
                public static readonly string CaptionEn =
                    "OLAP cube with ratio estimates data";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
                {
                    { "id", new ColumnMetadata()
                    {
                        Name = "id",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = true,
                        NotNull = true,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = String.Empty,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ID",
                        HeaderTextEn = "ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 0
                    }
                    },
                    { "numerator_id", new ColumnMetadata()
                    {
                        Name = "numerator_id",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = String.Empty,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "NUMERATOR_ID",
                        HeaderTextEn = "NUMERATOR_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 1
                    }
                    },
                    { "denominator_id", new ColumnMetadata()
                    {
                        Name = "denominator_id",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = String.Empty,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "DENOMINATOR_ID",
                        HeaderTextEn = "DENOMINATOR_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 2
                    }
                    },

                    { "numerator_phase_estimate_type_id", new ColumnMetadata()
                    {
                        Name = "numerator_phase_estimate_type_id",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue = "0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "NUMERATOR_PHASE_ESTIMATE_TYPE_ID",
                        HeaderTextEn = "NUMERATOR_PHASE_ESTIMATE_TYPE_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 3
                    }
                    },
                    { "numerator_phase_estimate_type_label_cs", new ColumnMetadata()
                    {
                        Name = "numerator_phase_estimate_type_label_cs",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = String.Empty,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "NUMERATOR_PHASE_ESTIMATE_TYPE_LABEL_CS",
                        HeaderTextEn = "NUMERATOR_PHASE_ESTIMATE_TYPE_LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 4
                    }
                    },
                    { "numerator_phase_estimate_type_label_en", new ColumnMetadata()
                    {
                        Name = "numerator_phase_estimate_type_label_en",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = String.Empty,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "NUMERATOR_PHASE_ESTIMATE_TYPE_LABEL_EN",
                        HeaderTextEn = "NUMERATOR_PHASE_ESTIMATE_TYPE_LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 5
                    }
                    },

                    { "denominator_phase_estimate_type_id", new ColumnMetadata()
                    {
                        Name = "denominator_phase_estimate_type_id",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue = "0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "DENOMINATOR_PHASE_ESTIMATE_TYPE_ID",
                        HeaderTextEn = "DENOMINATOR_PHASE_ESTIMATE_TYPE_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 6
                    }
                    },
                    { "denominator_phase_estimate_type_label_cs", new ColumnMetadata()
                    {
                        Name = "denominator_phase_estimate_type_label_cs",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = String.Empty,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "DENOMINATOR_PHASE_ESTIMATE_TYPE_LABEL_CS",
                        HeaderTextEn = "DENOMINATOR_PHASE_ESTIMATE_TYPE_LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 7
                    }
                    },
                    { "denominator_phase_estimate_type_label_en", new ColumnMetadata()
                    {
                        Name = "denominator_phase_estimate_type_label_en",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = String.Empty,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "DENOMINATOR_PHASE_ESTIMATE_TYPE_LABEL_EN",
                        HeaderTextEn = "DENOMINATOR_PHASE_ESTIMATE_TYPE_LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 8
                    }
                    },

                    { "estimation_period_id", new ColumnMetadata()
                    {
                        Name = "estimation_period_id",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue = "0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATION_PERIOD_ID",
                        HeaderTextEn = "ESTIMATION_PERIOD_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 9
                    }
                    },
                    { "estimate_date_begin", new ColumnMetadata()
                    {
                        Name = "estimate_date_begin",
                        DbName = null,
                        DataType = "System.DateTime",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue = new DateTime().ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "yyyy-MM-dd",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATE_DATE_BEGIN",
                        HeaderTextEn = "ESTIMATE_DATE_BEGIN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 10
                    }
                    },
                    { "estimate_date_end", new ColumnMetadata()
                    {
                        Name = "estimate_date_end",
                        DbName = null,
                        DataType = "System.DateTime",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue = new DateTime().ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "yyyy-MM-dd",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATE_DATE_END",
                        HeaderTextEn = "ESTIMATE_DATE_END",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 11
                    }
                    },
                    { "estimation_period_label_cs", new ColumnMetadata()
                    {
                        Name = "estimation_period_label_cs",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = String.Empty,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATION_PERIOD_LABEL_CS",
                        HeaderTextEn = "ESTIMATION_PERIOD_LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 12
                    }
                    },
                    { "estimation_period_label_en", new ColumnMetadata()
                    {
                        Name = "estimation_period_label_en",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = String.Empty,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATION_PERIOD_LABEL_EN",
                        HeaderTextEn = "ESTIMATION_PERIOD_LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 13
                    }
                    },

                    { "numerator_variable_id", new ColumnMetadata()
                    {
                        Name = "numerator_variable_id",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue = "0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "NUMERATOR_VARIABLE_ID",
                        HeaderTextEn = "NUMERATOR_VARIABLE_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 14
                    }
                    },
                    { "numerator_variable_label_cs", new ColumnMetadata()
                    {
                        Name = "numerator_variable_label_cs",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = String.Empty,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "NUMERATOR_VARIABLE_LABEL_CS",
                        HeaderTextEn = "NUMERATOR_VARIABLE_LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 15
                    }
                    },
                    { "numerator_variable_label_en", new ColumnMetadata()
                    {
                        Name = "numerator_variable_label_en",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = String.Empty,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "NUMERATOR_VARIABLE_LABEL_EN",
                        HeaderTextEn = "NUMERATOR_VARIABLE_LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 16
                    }
                    },
                    { "denominator_variable_id", new ColumnMetadata()
                    {
                        Name = "denominator_variable_id",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue = "0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "DENOMINATOR_VARIABLE_ID",
                        HeaderTextEn = "DENOMINATOR_VARIABLE_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 17
                    }
                    },
                    { "denominator_variable_label_cs", new ColumnMetadata()
                    {
                        Name = "denominator_variable_label_cs",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = String.Empty,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "DENOMINATOR_VARIABLE_LABEL_CS",
                        HeaderTextEn = "DENOMINATOR_VARIABLE_LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 18
                    }
                    },
                    { "denominator_variable_label_en", new ColumnMetadata()
                    {
                        Name = "denominator_variable_label_en",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = String.Empty,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "DENOMINATOR_VARIABLE_LABEL_EN",
                        HeaderTextEn = "DENOMINATOR_VARIABLE_LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 19
                    }
                    },

                    { "estimation_cell_id", new ColumnMetadata()
                    {
                        Name = "estimation_cell_id",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue = "0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATION_CELL_ID",
                        HeaderTextEn = "ESTIMATION_CELL_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 20
                    }
                    },
                    { "estimation_cell_description_cs", new ColumnMetadata()
                    {
                        Name = "estimation_cell_description_cs",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = String.Empty,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATION_CELL_DESCRIPTION_CS",
                        HeaderTextEn = "ESTIMATION_CELL_DESCRIPTION_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 21
                    }
                    },
                    { "estimation_cell_description_en", new ColumnMetadata()
                    {
                        Name = "estimation_cell_description_en",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = String.Empty,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATION_CELL_DESCRIPTION_EN",
                        HeaderTextEn = "ESTIMATION_CELL_DESCRIPTION_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 22
                    }
                    },
                    { "estimation_cell_collection_id", new ColumnMetadata()
                    {
                        Name = "estimation_cell_collection_id",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue = "0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATION_CELL_COLLECTION_ID",
                        HeaderTextEn = "ESTIMATION_CELL_COLLECTION_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 23
                    }
                    },
                    { "estimation_cell_collection_label_cs", new ColumnMetadata()
                    {
                        Name = "estimation_cell_collection_label_cs",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = String.Empty,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATION_CELL_COLLECTION_LABEL_CS",
                        HeaderTextEn = "ESTIMATION_CELL_COLLECTION_LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 24
                    }
                    },
                    { "estimation_cell_collection_label_en", new ColumnMetadata()
                    {
                        Name = "estimation_cell_collection_label_en",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = String.Empty,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATION_CELL_COLLECTION_LABEL_EN",
                        HeaderTextEn = "ESTIMATION_CELL_COLLECTION_LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 25
                    }
                    },

                    { "panel_refyearset_group_id", new ColumnMetadata()
                    {
                        Name = "panel_refyearset_group_id",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PANEL_REFYEARSET_GROUP_ID",
                        HeaderTextEn = "PANEL_REFYEARSET_GROUP_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 26
                    }
                    },
                    { "panel_refyearset_group_label_cs", new ColumnMetadata()
                    {
                        Name = "panel_refyearset_group_label_cs",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = String.Empty,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PANEL_REFYEARSET_GROUP_LABEL_CS",
                        HeaderTextEn = "PANEL_REFYEARSET_GROUP_LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 27
                    }
                    },
                    { "panel_refyearset_group_label_en", new ColumnMetadata()
                    {
                        Name = "panel_refyearset_group_label_en",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = String.Empty,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PANEL_REFYEARSET_GROUP_LABEL_EN",
                        HeaderTextEn = "PANEL_REFYEARSET_GROUP_LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 28
                    }
                    },

                    { "estimate_conf_id", new ColumnMetadata()
                    {
                        Name = "estimate_conf_id",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATE_CONF_ID",
                        HeaderTextEn = "ESTIMATE_CONF_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 29
                    }
                    },
                    { "estimate_type_id", new ColumnMetadata()
                    {
                        Name = "estimate_type_id",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATE_TYPE_ID",
                        HeaderTextEn = "ESTIMATE_TYPE_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 30
                    }
                    },
                    { "numerator_total_estimate_conf_id", new ColumnMetadata()
                    {
                        Name = "numerator_total_estimate_conf_id",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "NUMERATOR_TOTAL_ESTIMATE_CONF_ID",
                        HeaderTextEn = "NUMERATOR_TOTAL_ESTIMATE_CONF_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 31
                    }
                    },
                    { "denominator_total_estimate_conf_id", new ColumnMetadata()
                    {
                        Name = "denominator_total_estimate_conf_id",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "DENOMINATOR_TOTAL_ESTIMATE_CONF_ID",
                        HeaderTextEn = "DENOMINATOR_TOTAL_ESTIMATE_CONF_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 32
                    }
                    },
                    { "estimate_executor", new ColumnMetadata()
                    {
                        Name = "estimate_executor",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATE_EXECUTOR",
                        HeaderTextEn = "ESTIMATE_EXECUTOR",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 33
                    }
                    },
                    { "numerator_total_estimate_conf_name", new ColumnMetadata()
                    {
                        Name = "numerator_total_estimate_conf_name",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = String.Empty,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "NUMERATOR_TOTAL_ESTIMATE_CONF_NAME",
                        HeaderTextEn = "NUMERATOR_TOTAL_ESTIMATE_CONF_NAME",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 34
                    }
                    },
                    { "denominator_total_estimate_conf_name", new ColumnMetadata()
                    {
                        Name = "denominator_total_estimate_conf_name",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = String.Empty,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "DENOMINATOR_TOTAL_ESTIMATE_CONF_NAME",
                        HeaderTextEn = "DENOMINATOR_TOTAL_ESTIMATE_CONF_NAME",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 35
                    }
                    },

                    { "result_id", new ColumnMetadata()
                    {
                        Name = "result_id",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "RESULT_ID",
                        HeaderTextEn = "RESULT_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 36
                    }
                    },
                    { "point_estimate", new ColumnMetadata()
                    {
                        Name = "point_estimate",
                        DbName = null,
                        DataType = "System.Double",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "N7",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "POINT_ESTIMATE",
                        HeaderTextEn = "POINT_ESTIMATE",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 37
                    }
                    },
                    { "variance_estimate", new ColumnMetadata()
                    {
                        Name = "variance_estimate",
                        DbName = null,
                        DataType = "System.Double",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "N7",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "VARIANCE_ESTIMATE",
                        HeaderTextEn = "VARIANCE_ESTIMATE",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 38
                    }
                    },
                    { "calc_started", new ColumnMetadata()
                    {
                        Name = "calc_started",
                        DbName = null,
                        DataType = "System.DateTime",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "yyyy-MM-dd HH:mm:ss",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "CALC_STARTED",
                        HeaderTextEn = "CALC_STARTED",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 39
                    }
                    },
                    { "extension_version", new ColumnMetadata()
                    {
                        Name = "extension_version",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "EXTENSION_VERSION",
                        HeaderTextEn = "EXTENSION_VERSION",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 40
                    }
                    },
                    { "calc_duration", new ColumnMetadata()
                    {
                        Name = "calc_duration",
                        DbName = null,
                        DataType = "System.TimeSpan",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "g",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "CALC_DURATION",
                        HeaderTextEn = "CALC_DURATION",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 41
                    }
                    },
                    { "min_ssize", new ColumnMetadata()
                    {
                        Name = "min_ssize",
                        DbName = null,
                        DataType = "System.Int64",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "MIN_SSIZE",
                        HeaderTextEn = "MIN_SSIZE",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 42
                    }
                    },
                    { "act_ssize", new ColumnMetadata()
                    {
                        Name = "act_ssize",
                        DbName = null,
                        DataType = "System.Int64",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ACT_SSIZE",
                        HeaderTextEn = "ACT_SSIZE",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 43
                    }
                    },
                    { "is_latest", new ColumnMetadata()
                    {
                        Name = "is_latest",
                        DbName = null,
                        DataType = "System.Boolean",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "IS_LATEST",
                        HeaderTextEn = "IS_LATEST",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 44
                    }
                    },
                    { "result_executor", new ColumnMetadata()
                    {
                        Name = "result_executor",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "RESULT_EXECUTOR",
                        HeaderTextEn = "RESULT_EXECUTOR",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 45
                    }
                    },
                    { "standard_error", new ColumnMetadata()
                    {
                        Name = "standard_error",
                        DbName = null,
                        DataType = "System.Double",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "N7",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "STANDARD_ERROR",
                        HeaderTextEn = "STANDARD_ERROR",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 46
                    }
                    },
                    { "confidence_interval", new ColumnMetadata()
                    {
                        Name = "confidence_interval",
                        DbName = null,
                        DataType = "System.Double",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "N7",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "CONFIDENCE_INTERVAL",
                        HeaderTextEn = "CONFIDENCE_INTERVAL",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 47
                    }
                    },
                    { "lower_bound", new ColumnMetadata()
                    {
                        Name = "lower_bound",
                        DbName = null,
                        DataType = "System.Double",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "N7",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "LOWER_BOUND",
                        HeaderTextEn = "LOWER_BOUND",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 48
                    }
                    },
                    { "upper_bound", new ColumnMetadata()
                    {
                        Name = "upper_bound",
                        DbName = null,
                        DataType = "System.Double",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "N7",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "UPPER_BOUND",
                        HeaderTextEn = "UPPER_BOUND",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 49
                    }
                    },
                    { "attr_diff_down", new ColumnMetadata()
                    {
                        Name = "attr_diff_down",
                        DbName = null,
                        DataType = "System.Double",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "N10",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ATTR_DIFF_DOWN",
                        HeaderTextEn = "ATTR_DIFF_DOWN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 50
                    }
                    },
                    { "attr_diff_up", new ColumnMetadata()
                    {
                        Name = "attr_diff_up",
                        DbName = null,
                        DataType = "System.Double",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "N10",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ATTR_DIFF_UP",
                        HeaderTextEn = "ATTR_DIFF_UP",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 51
                    }
                    },
                    { "is_additive", new ColumnMetadata()
                    {
                        Name = "is_additive",
                        DbName = null,
                        DataType = "System.Boolean",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue =  null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat =  null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "IS_ADDITIVE",
                        HeaderTextEn = "IS_ADDITIVE",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 52
                    }
                    },
                    { "estimate_conf_status_id", new ColumnMetadata()
                    {
                        Name = "estimate_conf_status_id",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATE_CONF_STATUS_ID",
                        HeaderTextEn = "ESTIMATE_CONF_STATUS_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 53
                    }
                    },
                    { "estimate_conf_status_label_cs", new ColumnMetadata()
                    {
                        Name = "estimate_conf_status_label_cs",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = String.Empty,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATE_CONF_STATUS_LABEL_CS",
                        HeaderTextEn = "ESTIMATE_CONF_STATUS_LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 54
                    }
                    },
                    { "estimate_conf_status_label_en", new ColumnMetadata()
                    {
                        Name = "estimate_conf_status_label_en",
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = String.Empty,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATE_CONF_STATUS_LABEL_EN",
                        HeaderTextEn = "ESTIMATE_CONF_STATUS_LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 55
                    }
                    }
                };


                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column numerator_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColNumeratorId = Cols["numerator_id"];

                /// <summary>
                /// Column denominator_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColDenominatorId = Cols["denominator_id"];


                /// <summary>
                /// Column numerator_phase_estimate_type_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColNumeratorPhaseEstimateTypeId = Cols["numerator_phase_estimate_type_id"];

                /// <summary>
                /// Column numerator_phase_estimate_type_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColNumeratorPhaseEstimateTypeLabelCs = Cols["numerator_phase_estimate_type_label_cs"];

                /// <summary>
                /// Column numerator_phase_estimate_type_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColNumeratorPhaseEstimateTypeLabelEn = Cols["numerator_phase_estimate_type_label_en"];


                /// <summary>
                /// Column denominator_phase_estimate_type_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColDenominatorPhaseEstimateTypeId = Cols["denominator_phase_estimate_type_id"];

                /// <summary>
                /// Column denominator_phase_estimate_type_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColDenominatorPhaseEstimateTypeLabelCs = Cols["denominator_phase_estimate_type_label_cs"];

                /// <summary>
                /// Column denominator_phase_estimate_type_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColDenominatorPhaseEstimateTypeLabelEn = Cols["denominator_phase_estimate_type_label_en"];


                /// <summary>
                /// Column estimation_period_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationPeriodId = Cols["estimation_period_id"];

                /// <summary>
                /// Column estimate_date_begin metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimateDateBegin = Cols["estimate_date_begin"];

                /// <summary>
                /// Column estimate_date_end metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimateDateEnd = Cols["estimate_date_end"];

                /// <summary>
                /// Column estimation_period_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationPeriodLabelCs = Cols["estimation_period_label_cs"];

                /// <summary>
                /// Column estimation_period_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationPeriodLabelEn = Cols["estimation_period_label_en"];


                /// <summary>
                /// Column numerator_variable_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColNumeratorVariableId = Cols["numerator_variable_id"];

                /// <summary>
                /// Column numerator_variable_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColNumeratorVariableLabelCs = Cols["numerator_variable_label_cs"];

                /// <summary>
                /// Column numerator_variable_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColNumeratorVariableLabelEn = Cols["numerator_variable_label_en"];

                /// <summary>
                /// Column denominator_variable_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColDenominatorVariableId = Cols["denominator_variable_id"];

                /// <summary>
                /// Column denominator_variable_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColDenominatorVariableLabelCs = Cols["denominator_variable_label_cs"];

                /// <summary>
                /// Column denominator_variable_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColDenominatorVariableLabelEn = Cols["denominator_variable_label_en"];


                /// <summary>
                /// Column estimation_cell_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationCellId = Cols["estimation_cell_id"];

                /// <summary>
                /// Column estimation_cell_description_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationCellDescriptionCs = Cols["estimation_cell_description_cs"];

                /// <summary>
                /// Column estimation_cell_description_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationCellDescriptionEn = Cols["estimation_cell_description_en"];

                /// <summary>
                /// Column estimation_cell_collection_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationCellCollectionId = Cols["estimation_cell_collection_id"];

                /// <summary>
                /// Column estimation_cell_collection_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationCellCollectionLabelCs = Cols["estimation_cell_collection_label_cs"];

                /// <summary>
                /// Column estimation_cell_collection_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationCellCollectionLabelEn = Cols["estimation_cell_collection_label_en"];


                /// <summary>
                /// Column panel_refyearset_group_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColPanelRefYearSetGroupId = Cols["panel_refyearset_group_id"];

                /// <summary>
                /// Column panel_refyearset_group_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColPanelRefYearSetGroupLabelCs = Cols["panel_refyearset_group_label_cs"];

                /// <summary>
                /// Column panel_refyearset_group_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColPanelRefYearSetGroupLabelEn = Cols["panel_refyearset_group_label_en"];


                /// <summary>
                /// Column estimate_conf_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimateConfId = Cols["estimate_conf_id"];

                /// <summary>
                /// Column estimate_type_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimateTypeId = Cols["estimate_type_id"];

                /// <summary>
                /// Column numerator_total_estimate_conf_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColNumeratorTotalEstimateConfId = Cols["numerator_total_estimate_conf_id"];

                /// <summary>
                /// Column denominator_total_estimate_conf_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColDenominatorTotalEstimateConfId = Cols["denominator_total_estimate_conf_id"];

                /// <summary>
                /// Column estimate_executor metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimateExecutor = Cols["estimate_executor"];

                /// <summary>
                /// Column numerator_total_estimate_conf_name metadata
                /// </summary>
                public static readonly ColumnMetadata ColNumeratorTotalEstimateConfName = Cols["numerator_total_estimate_conf_name"];

                /// <summary>
                /// Column denominator_total_estimate_conf_name metadata
                /// </summary>
                public static readonly ColumnMetadata ColDenominatorTotalEstimateConfName = Cols["denominator_total_estimate_conf_name"];


                /// <summary>
                /// Column result_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColResultId = Cols["result_id"];

                /// <summary>
                /// Column point_estimate metadata
                /// </summary>
                public static readonly ColumnMetadata ColPointEstimate = Cols["point_estimate"];

                /// <summary>
                /// Column variance_estimate metadata
                /// </summary>
                public static readonly ColumnMetadata ColVarianceEstimate = Cols["variance_estimate"];

                /// <summary>
                /// Column calc_started metadata
                /// </summary>
                public static readonly ColumnMetadata ColCalcStarted = Cols["calc_started"];

                /// <summary>
                /// Column extension_version metadata
                /// </summary>
                public static readonly ColumnMetadata ColExtensionVersion = Cols["extension_version"];

                /// <summary>
                /// Column calc_duration metadata
                /// </summary>
                public static readonly ColumnMetadata ColCalcDuration = Cols["calc_duration"];

                /// <summary>
                /// Column min_ssize metadata
                /// </summary>
                public static readonly ColumnMetadata ColMinSSize = Cols["min_ssize"];

                /// <summary>
                /// Column act_ssize metadata
                /// </summary>
                public static readonly ColumnMetadata ColActSSize = Cols["act_ssize"];

                /// <summary>
                /// Column is_latest metadata
                /// </summary>
                public static readonly ColumnMetadata ColIsLatest = Cols["is_latest"];

                /// <summary>
                /// Column result_executor metadata
                /// </summary>
                public static readonly ColumnMetadata ColResultExecutor = Cols["result_executor"];


                /// <summary>
                /// Column standard_error metadata
                /// </summary>
                public static readonly ColumnMetadata ColStandardError = Cols["standard_error"];

                /// <summary>
                /// Column confidence_interval metadata
                /// </summary>
                public static readonly ColumnMetadata ColConfidenceInterval = Cols["confidence_interval"];

                /// <summary>
                /// Column lower_bound metadata
                /// </summary>
                public static readonly ColumnMetadata ColLowerBound = Cols["lower_bound"];

                /// <summary>
                /// Column upper_bound metadata
                /// </summary>
                public static readonly ColumnMetadata ColUpperBound = Cols["upper_bound"];


                /// <summary>
                /// Column attr_diff_down metadata
                /// </summary>
                public static readonly ColumnMetadata ColAttrDiffDown = Cols["attr_diff_down"];

                /// <summary>
                /// Column attr_diff_up metadata
                /// </summary>
                public static readonly ColumnMetadata ColAttrDiffUp = Cols["attr_diff_up"];

                /// <summary>
                /// Column is_additive metadata
                /// </summary>
                public static readonly ColumnMetadata ColIsAdditive = Cols["is_additive"];


                /// <summary>
                /// Column estimate_conf_status_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimateConfStatusId = Cols["estimate_conf_status_id"];

                /// <summary>
                /// Column estimate_conf_status_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimateConfStatusLabelCs = Cols["estimate_conf_status_label_cs"];

                /// <summary>
                /// Column estimate_conf_status_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimateConfStatusLabelEn = Cols["estimate_conf_status_label_en"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public OLAPRatioEstimateList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public OLAPRatioEstimateList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// List of OLAP Cube Ratio Estimates (read-only)
                /// </summary>
                public List<OLAPRatioEstimate> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new OLAPRatioEstimate(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// OLAP Cube - Ratio Estimate from list by identifier (read-only)
                /// </summary>
                /// <param name="id">OLAP Cube - Ratio Estimate identifier</param>
                /// <returns>OLAP Cube - Ratio Estimate from list by identifier (null if not found)</returns>
                public OLAPRatioEstimate this[string id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<string>(ColId.Name) == id)
                                .Select(a => new OLAPRatioEstimate(composite: this, data: a))
                                .FirstOrDefault<OLAPRatioEstimate>();
                    }
                }

                #endregion Indexer


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public OLAPRatioEstimateList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new OLAPRatioEstimateList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new OLAPRatioEstimateList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                /// <summary>
                /// Loads data into OLAP Cube - List of Ratio Estimates (if it was not done before)
                /// </summary>
                /// <param name="olapDimensions">List of OLAP Cube Dimensions</param>
                /// <param name="olapValues">List of OLAP Cube Values</param>
                /// <param name="cEstimateConfStatus">List of estimate configuration status</param>
                /// <param name="additivityLimit">Additivity limit</param>
                /// <param name="condition">Condition for selection data</param>
                /// <param name="limit">Rows count limit</param>
                public void Load(
                    OLAPRatioEstimateDimensionList olapDimensions,
                    OLAPRatioEstimateValueList olapValues,
                    EstimateConfStatusList cEstimateConfStatus,
                    double additivityLimit,
                    string condition = null,
                    Nullable<int> limit = null)
                {
                    if (!Loaded)
                    {
                        ReLoad(
                           olapDimensions: olapDimensions,
                           olapValues: olapValues,
                           cEstimateConfStatus: cEstimateConfStatus,
                           additivityLimit: additivityLimit,
                           condition: condition,
                           limit: limit);
                    }
                }

                /// <summary>
                /// Loads data into OLAP Cube - List of Ratio Estimates
                /// </summary>
                /// <param name="olapDimensions">List of OLAP Cube Dimensions</param>
                /// <param name="olapValues">List of OLAP Cube Values</param>
                /// <param name="cEstimateConfStatus">List of estimate configuration status</param>
                /// <param name="additivityLimit">Additivity limit</param>
                /// <param name="condition">Condition for selection data</param>
                /// <param name="limit">Rows count limit</param>
                public void ReLoad(
                    OLAPRatioEstimateDimensionList olapDimensions,
                    OLAPRatioEstimateValueList olapValues,
                    EstimateConfStatusList cEstimateConfStatus,
                    double additivityLimit,
                    string condition = null,
                    Nullable<int> limit = null)
                {
                    Data = EmptyDataTable(
                                tableName: TableName,
                                columns: Columns);
                    Condition = condition;
                    Limit = limit;

                    System.Diagnostics.Stopwatch stopWatch = new();
                    stopWatch.Start();

                    #region cteOlapDimensions: Dimenze OLAP
                    var cteOlapDimensions =
                        olapDimensions.Data.AsEnumerable()
                        .Select(a => new
                        {
                            Id = a.Field<string>(OLAPRatioEstimateDimensionList.ColId.Name),
                            NumeratorId = a.Field<string>(OLAPRatioEstimateDimensionList.ColNumeratorId.Name),
                            DenominatorId = a.Field<string>(OLAPRatioEstimateDimensionList.ColDenominatorId.Name),
                            NumeratorPhaseEstimateTypeId = a.Field<int>(OLAPRatioEstimateDimensionList.ColNumeratorPhaseEstimateTypeId.Name),
                            NumeratorPhaseEstimateTypeLabelCs = a.Field<string>(OLAPRatioEstimateDimensionList.ColNumeratorPhaseEstimateTypeLabelCs.Name),
                            NumeratorPhaseEstimateTypeLabelEn = a.Field<string>(OLAPRatioEstimateDimensionList.ColNumeratorPhaseEstimateTypeLabelEn.Name),
                            DenominatorPhaseEstimateTypeId = a.Field<int>(OLAPRatioEstimateDimensionList.ColDenominatorPhaseEstimateTypeId.Name),
                            DenominatorPhaseEstimateTypeLabelCs = a.Field<string>(OLAPRatioEstimateDimensionList.ColDenominatorPhaseEstimateTypeLabelCs.Name),
                            DenominatorPhaseEstimateTypeLabelEn = a.Field<string>(OLAPRatioEstimateDimensionList.ColDenominatorPhaseEstimateTypeLabelEn.Name),
                            EstimationPeriodId = a.Field<int>(OLAPRatioEstimateDimensionList.ColEstimationPeriodId.Name),
                            EstimateDateBegin = a.Field<DateTime>(OLAPRatioEstimateDimensionList.ColEstimateDateBegin.Name),
                            EstimateDateEnd = a.Field<DateTime>(OLAPRatioEstimateDimensionList.ColEstimateDateEnd.Name),
                            EstimationPeriodLabelCs = a.Field<string>(OLAPRatioEstimateDimensionList.ColEstimationPeriodLabelCs.Name),
                            EstimationPeriodLabelEn = a.Field<string>(OLAPRatioEstimateDimensionList.ColEstimationPeriodLabelEn.Name),
                            NumeratorVariableId = a.Field<int>(OLAPRatioEstimateDimensionList.ColNumeratorVariableId.Name),
                            NumeratorVariableLabelCs = a.Field<string>(OLAPRatioEstimateDimensionList.ColNumeratorVariableLabelCs.Name),
                            NumeratorVariableLabelEn = a.Field<string>(OLAPRatioEstimateDimensionList.ColNumeratorVariableLabelEn.Name),
                            DenominatorVariableId = a.Field<int>(OLAPRatioEstimateDimensionList.ColDenominatorVariableId.Name),
                            DenominatorVariableLabelCs = a.Field<string>(OLAPRatioEstimateDimensionList.ColDenominatorVariableLabelCs.Name),
                            DenominatorVariableLabelEn = a.Field<string>(OLAPRatioEstimateDimensionList.ColDenominatorVariableLabelEn.Name),
                            EstimationCellId = a.Field<int>(OLAPRatioEstimateDimensionList.ColEstimationCellId.Name),
                            EstimationCellDescriptionCs = a.Field<string>(OLAPRatioEstimateDimensionList.ColEstimationCellDescriptionCs.Name),
                            EstimationCellDescriptionEn = a.Field<string>(OLAPRatioEstimateDimensionList.ColEstimationCellDescriptionEn.Name),
                            EstimationCellCollectionId = a.Field<int>(OLAPRatioEstimateDimensionList.ColEstimationCellCollectionId.Name),
                            EstimationCellCollectionLabelCs = a.Field<string>(OLAPRatioEstimateDimensionList.ColEstimationCellCollectionLabelCs.Name),
                            EstimationCellCollectionLabelEn = a.Field<string>(OLAPRatioEstimateDimensionList.ColEstimationCellCollectionLabelEn.Name)
                        });
                    #endregion cteOlapDimensions: Dimenze OLAP

                    #region cteOlapValues: Hodnoty OLAP
                    var cteOlapValues =
                        olapValues.Data.AsEnumerable()
                        .Select(a => new
                        {
                            Id = a.Field<string>(OLAPRatioEstimateValueList.ColId.Name),
                            NumeratorId = a.Field<string>(OLAPRatioEstimateValueList.ColNumeratorId.Name),
                            DenominatorId = a.Field<string>(OLAPRatioEstimateValueList.ColDenominatorId.Name),
                            PhaseEstimateTypeId = a.Field<int>(OLAPRatioEstimateValueList.ColPhaseEstimateTypeId.Name),
                            EstimationPeriodId = a.Field<int>(OLAPRatioEstimateValueList.ColEstimationPeriodId.Name),
                            NumeratorVariableId = a.Field<int>(OLAPRatioEstimateValueList.ColNumeratorVariableId.Name),
                            DenominatorVariableId = a.Field<int>(OLAPRatioEstimateValueList.ColDenominatorVariableId.Name),
                            EstimationCellId = a.Field<int>(OLAPRatioEstimateValueList.ColEstimationCellId.Name),
                            PanelRefYearSetGroupId = a.Field<Nullable<int>>(OLAPRatioEstimateValueList.ColPanelRefYearSetGroupId.Name),
                            PanelRefYearSetGroupLabelCs = a.Field<string>(OLAPRatioEstimateValueList.ColPanelRefYearSetGroupLabelCs.Name),
                            PanelRefYearSetGroupLabelEn = a.Field<string>(OLAPRatioEstimateValueList.ColPanelRefYearSetGroupLabelEn.Name),
                            EstimateConfId = a.Field<Nullable<int>>(OLAPRatioEstimateValueList.ColEstimateConfId.Name),
                            EstimateTypeId = a.Field<Nullable<int>>(OLAPRatioEstimateValueList.ColEstimateTypeId.Name),
                            NumeratorTotalEstimateConfId = a.Field<Nullable<int>>(OLAPRatioEstimateValueList.ColNumeratorTotalEstimateConfId.Name),
                            DenominatorTotalEstimateConfId = a.Field<Nullable<int>>(OLAPRatioEstimateValueList.ColDenominatorTotalEstimateConfId.Name),
                            EstimateExecutor = a.Field<string>(OLAPRatioEstimateValueList.ColEstimateExecutor.Name),
                            NumeratorTotalEstimateConfName = a.Field<string>(OLAPRatioEstimateValueList.ColNumeratorTotalEstimateConfName.Name),
                            DenominatorTotalEstimateConfName = a.Field<string>(OLAPRatioEstimateValueList.ColDenominatorTotalEstimateConfName.Name),
                            ResultId = a.Field<Nullable<int>>(OLAPRatioEstimateValueList.ColResultId.Name),
                            PointEstimate = a.Field<Nullable<double>>(OLAPRatioEstimateValueList.ColPointEstimate.Name),
                            VarianceEstimate = a.Field<Nullable<double>>(OLAPRatioEstimateValueList.ColVarianceEstimate.Name),
                            CalcStarted = a.Field<Nullable<DateTime>>(OLAPRatioEstimateValueList.ColCalcStarted.Name),
                            ExtensionVersion = a.Field<string>(OLAPRatioEstimateValueList.ColExtensionVersion.Name),
                            CalcDuration = a.Field<Nullable<TimeSpan>>(OLAPRatioEstimateValueList.ColCalcDuration.Name),
                            MinSSize = a.Field<Nullable<long>>(OLAPRatioEstimateValueList.ColMinSSize.Name),
                            ActSSize = a.Field<Nullable<long>>(OLAPRatioEstimateValueList.ColActSSize.Name),
                            IsLatest = a.Field<Nullable<bool>>(OLAPRatioEstimateValueList.ColIsLatest.Name),
                            ResultExecutor = a.Field<string>(OLAPRatioEstimateValueList.ColResultExecutor.Name),
                            StandardError = a.Field<Nullable<double>>(OLAPRatioEstimateValueList.ColStandardError.Name),
                            ConfidenceInterval = a.Field<Nullable<double>>(OLAPRatioEstimateValueList.ColConfidenceInterval.Name),
                            LowerBound = a.Field<Nullable<double>>(OLAPRatioEstimateValueList.ColLowerBound.Name),
                            UpperBound = a.Field<Nullable<double>>(OLAPRatioEstimateValueList.ColUpperBound.Name),
                            AttrDiffDown = a.Field<Nullable<double>>(OLAPRatioEstimateValueList.ColAttrDiffDown.Name),
                            AttrDiffUp = a.Field<Nullable<double>>(OLAPRatioEstimateValueList.ColAttrDiffUp.Name)
                        });
                    #endregion cteOlapValues: Hodnoty OLAP

                    #region cteEstimateConfStatus: Status odhadu
                    var cteEstimateConfStatus =
                        cEstimateConfStatus.Data.AsEnumerable()
                        .Select(a => new
                        {
                            EstimateConfStatusId = a.Field<int>(columnName: EstimateConfStatusList.ColId.Name),
                            EstimateConfStatusLabelCs = a.Field<string>(columnName: EstimateConfStatusList.ColLabelCs.Name),
                            EstimateConfStatusLabelEn = a.Field<string>(columnName: EstimateConfStatusList.ColLabelEn.Name),
                            EstimateConfStatusDescriptionCs = a.Field<string>(columnName: EstimateConfStatusList.ColDescriptionCs.Name),
                            EstimateConfStatusDescriptionEn = a.Field<string>(columnName: EstimateConfStatusList.ColDescriptionEn.Name)
                        });
                    #endregion cteEstimateConfStatus: Status odhadu

                    #region cteOlapCubeJoin: cteOlapDimensions + cteOlapValues (LEFT JOIN)
                    var cteOlapCubeJoin =
                        from a in cteOlapDimensions
                        join b in cteOlapValues
                        on a.Id equals b.Id into lj
                        from b in lj.DefaultIfEmpty()
                        select new
                        {
                            a.Id,
                            a.NumeratorId,
                            a.DenominatorId,
                            a.NumeratorPhaseEstimateTypeId,
                            a.NumeratorPhaseEstimateTypeLabelCs,
                            a.NumeratorPhaseEstimateTypeLabelEn,
                            a.DenominatorPhaseEstimateTypeId,
                            a.DenominatorPhaseEstimateTypeLabelCs,
                            a.DenominatorPhaseEstimateTypeLabelEn,
                            a.EstimationPeriodId,
                            a.EstimateDateBegin,
                            a.EstimateDateEnd,
                            a.EstimationPeriodLabelCs,
                            a.EstimationPeriodLabelEn,
                            a.NumeratorVariableId,
                            a.NumeratorVariableLabelCs,
                            a.NumeratorVariableLabelEn,
                            a.DenominatorVariableId,
                            a.DenominatorVariableLabelCs,
                            a.DenominatorVariableLabelEn,
                            a.EstimationCellId,
                            a.EstimationCellDescriptionCs,
                            a.EstimationCellDescriptionEn,
                            a.EstimationCellCollectionId,
                            a.EstimationCellCollectionLabelCs,
                            a.EstimationCellCollectionLabelEn,
                            b?.PanelRefYearSetGroupId,
                            b?.PanelRefYearSetGroupLabelCs,
                            b?.PanelRefYearSetGroupLabelEn,
                            b?.EstimateConfId,
                            b?.EstimateTypeId,
                            b?.NumeratorTotalEstimateConfId,
                            b?.DenominatorTotalEstimateConfId,
                            b?.EstimateExecutor,
                            b?.NumeratorTotalEstimateConfName,
                            b?.DenominatorTotalEstimateConfName,
                            b?.ResultId,
                            b?.PointEstimate,
                            b?.VarianceEstimate,
                            b?.CalcStarted,
                            b?.ExtensionVersion,
                            b?.CalcDuration,
                            b?.MinSSize,
                            b?.ActSSize,
                            b?.IsLatest,
                            b?.ResultExecutor,
                            b?.StandardError,
                            b?.ConfidenceInterval,
                            b?.LowerBound,
                            b?.UpperBound,
                            b?.AttrDiffDown,
                            b?.AttrDiffUp,
                            EstimateConfStatusId =
                                (b == null) ? 100 :                     // není konfigurace
                                (b.ResultId == null) ? 200 :            // není odhad
                                300                                     // odhad je vypočten
                        };
                    #endregion cteOlapCubeJoin: cteOlapDimensions + cteOlapValues (LEFT JOIN)

                    #region cteOlapCube: cteOlapCubeJoin + cteEstimateConfStatus (LEFT JOIN)
                    var cteOlapCube =
                        from a in cteOlapCubeJoin
                        join b in cteEstimateConfStatus
                        on a.EstimateConfStatusId equals b.EstimateConfStatusId into lj
                        from b in lj.DefaultIfEmpty()
                        select new
                        {
                            a.Id,
                            a.NumeratorId,
                            a.DenominatorId,
                            a.NumeratorPhaseEstimateTypeId,
                            a.NumeratorPhaseEstimateTypeLabelCs,
                            a.NumeratorPhaseEstimateTypeLabelEn,
                            a.DenominatorPhaseEstimateTypeId,
                            a.DenominatorPhaseEstimateTypeLabelCs,
                            a.DenominatorPhaseEstimateTypeLabelEn,
                            a.EstimationPeriodId,
                            a.EstimateDateBegin,
                            a.EstimateDateEnd,
                            a.EstimationPeriodLabelCs,
                            a.EstimationPeriodLabelEn,
                            a.NumeratorVariableId,
                            a.NumeratorVariableLabelCs,
                            a.NumeratorVariableLabelEn,
                            a.DenominatorVariableId,
                            a.DenominatorVariableLabelCs,
                            a.DenominatorVariableLabelEn,
                            a.EstimationCellId,
                            a.EstimationCellDescriptionCs,
                            a.EstimationCellDescriptionEn,
                            a.EstimationCellCollectionId,
                            a.EstimationCellCollectionLabelCs,
                            a.EstimationCellCollectionLabelEn,
                            a.PanelRefYearSetGroupId,
                            a.PanelRefYearSetGroupLabelCs,
                            a.PanelRefYearSetGroupLabelEn,
                            a.EstimateConfId,
                            a.EstimateTypeId,
                            a.NumeratorTotalEstimateConfId,
                            a.DenominatorTotalEstimateConfId,
                            a.EstimateExecutor,
                            a.NumeratorTotalEstimateConfName,
                            a.DenominatorTotalEstimateConfName,
                            a.ResultId,
                            a.PointEstimate,
                            a.VarianceEstimate,
                            a.CalcStarted,
                            a.ExtensionVersion,
                            a.CalcDuration,
                            a.MinSSize,
                            a.ActSSize,
                            a.IsLatest,
                            a.ResultExecutor,
                            a.StandardError,
                            a.ConfidenceInterval,
                            a.LowerBound,
                            a.UpperBound,
                            a.AttrDiffDown,
                            a.AttrDiffUp,
                            IsAdditive =
                                (a.AttrDiffDown == null) ? (Nullable<bool>)null :
                                (a.AttrDiffUp == null) ? (Nullable<bool>)null :
                                (a.AttrDiffDown < additivityLimit) &&
                                (a.AttrDiffUp < additivityLimit),
                            b?.EstimateConfStatusId,
                            b?.EstimateConfStatusLabelCs,
                            b?.EstimateConfStatusLabelEn
                        };
                    #endregion cteOlapCube: cteOlapCubeJoin + cteEstimateConfStatus (LEFT JOIN)

                    #region Zápis výsledku do DataTable
                    if (cteOlapCube.Any())
                    {
                        Data =
                           cteOlapCube
                           .OrderBy(a => a.NumeratorPhaseEstimateTypeId)
                            .ThenBy(a => a.DenominatorPhaseEstimateTypeId)
                           .ThenBy(a => a.EstimationPeriodId)
                           .ThenBy(a => a.NumeratorVariableId)
                           .ThenBy(a => a.DenominatorVariableId)
                           .ThenBy(a => a.EstimationCellId)
                           .ThenBy(a => a.PanelRefYearSetGroupId)
                           .ThenByDescending(a => a.CalcStarted)
                           .Select(a =>
                               Data.LoadDataRow(
                                    values:
                                    [
                                        a.Id,
                                        a.NumeratorId,
                                        a.DenominatorId,
                                        a.NumeratorPhaseEstimateTypeId,
                                        a.NumeratorPhaseEstimateTypeLabelCs,
                                        a.NumeratorPhaseEstimateTypeLabelEn,
                                        a.DenominatorPhaseEstimateTypeId,
                                        a.DenominatorPhaseEstimateTypeLabelCs,
                                        a.DenominatorPhaseEstimateTypeLabelEn,
                                        a.EstimationPeriodId,
                                        a.EstimateDateBegin,
                                        a.EstimateDateEnd,
                                        a.EstimationPeriodLabelCs,
                                        a.EstimationPeriodLabelEn,
                                        a.NumeratorVariableId,
                                        a.NumeratorVariableLabelCs,
                                        a.NumeratorVariableLabelEn,
                                        a.DenominatorVariableId,
                                        a.DenominatorVariableLabelCs,
                                        a.DenominatorVariableLabelEn,
                                        a.EstimationCellId,
                                        a.EstimationCellDescriptionCs,
                                        a.EstimationCellDescriptionEn,
                                        a.EstimationCellCollectionId,
                                        a.EstimationCellCollectionLabelCs,
                                        a.EstimationCellCollectionLabelEn,
                                        a.PanelRefYearSetGroupId,
                                        a.PanelRefYearSetGroupLabelCs,
                                        a.PanelRefYearSetGroupLabelEn,
                                        a.EstimateConfId,
                                        a.EstimateTypeId,
                                        a.NumeratorTotalEstimateConfId,
                                        a.DenominatorTotalEstimateConfId,
                                        a.EstimateExecutor,
                                        a.NumeratorTotalEstimateConfName,
                                        a.DenominatorTotalEstimateConfName,
                                        a.ResultId,
                                        a.PointEstimate,
                                        a.VarianceEstimate,
                                        a.CalcStarted,
                                        a.ExtensionVersion,
                                        a.CalcDuration,
                                        a.MinSSize,
                                        a.ActSSize,
                                        a.IsLatest,
                                        a.ResultExecutor,
                                        a.StandardError,
                                        a.ConfidenceInterval,
                                        a.LowerBound,
                                        a.UpperBound,
                                        a.AttrDiffDown,
                                        a.AttrDiffUp,
                                        a.IsAdditive,
                                        a.EstimateConfStatusId,
                                        a.EstimateConfStatusLabelCs,
                                        a.EstimateConfStatusLabelEn
                                    ],
                                    fAcceptChanges: false))
                            .CopyToDataTable();
                        Data.TableName = Name;
                    }
                    #endregion Zápis výsledku do DataTable

                    #region Omezení podmínkou
                    if (!String.IsNullOrEmpty(value: Condition))
                    {
                        DataView view = new(table: Data);
                        try
                        {
                            view.RowFilter = Condition;
                        }
                        catch
                        {
                            view.RowFilter = String.Empty;
                            Condition = String.Empty;
                        }
                        Data = view.ToTable();
                    }
                    #endregion Omezení podmínkou

                    #region Omezení počtem
                    if (Limit != null)
                    {
                        Data = Data.AsEnumerable()
                            .Take(count: (int)Limit)
                            .CopyToDataTable();
                    }
                    #endregion Omezení počtem

                    stopWatch.Stop();
                    LoadingTime = 0.001 * stopWatch.ElapsedMilliseconds;
                    Data.TableName = TableName;
                }

                #endregion Methods

            }

        }
    }
}
