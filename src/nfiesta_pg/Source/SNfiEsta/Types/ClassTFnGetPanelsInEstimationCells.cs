﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {
            // fn_get_panels_in_estimation_cells

            /// <summary>
            /// Panel in estimation cell
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class TFnGetPanelsInEstimationCells(
                TFnGetPanelsInEstimationCellsList composite = null,
                DataRow data = null)
                    : AParametrizedViewEntry<TFnGetPanelsInEstimationCellsList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Panel in estimation cell identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TFnGetPanelsInEstimationCellsList.ColId.Name,
                            defaultValue: Int32.Parse(s: TFnGetPanelsInEstimationCellsList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TFnGetPanelsInEstimationCellsList.ColId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Stratum identifier
                /// </summary>
                public Nullable<int> StratumId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TFnGetPanelsInEstimationCellsList.ColStratumId.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnGetPanelsInEstimationCellsList.ColStratumId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TFnGetPanelsInEstimationCellsList.ColStratumId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TFnGetPanelsInEstimationCellsList.ColStratumId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Stratum label
                /// </summary>
                public string StratumLabel
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnGetPanelsInEstimationCellsList.ColStratumLabel.Name,
                            defaultValue: TFnGetPanelsInEstimationCellsList.ColStratumLabel.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnGetPanelsInEstimationCellsList.ColStratumLabel.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Stratum object (read-only)
                /// </summary>
                public SDesign.Stratum Stratum
                {
                    get
                    {
                        return
                            (StratumId != null) ?
                             ((NfiEstaDB)Composite.Database).SDesign.TStratum[(int)StratumId] :
                            null;
                    }
                }


                /// <summary>
                /// Panel identifier
                /// </summary>
                public Nullable<int> PanelId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TFnGetPanelsInEstimationCellsList.ColPanelId.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnGetPanelsInEstimationCellsList.ColPanelId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TFnGetPanelsInEstimationCellsList.ColPanelId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TFnGetPanelsInEstimationCellsList.ColPanelId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Panel label
                /// </summary>
                public string PanelLabel
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnGetPanelsInEstimationCellsList.ColPanelLabel.Name,
                            defaultValue: TFnGetPanelsInEstimationCellsList.ColPanelLabel.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnGetPanelsInEstimationCellsList.ColPanelLabel.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Panel object (read-only)
                /// </summary>
                public SDesign.Panel Panel
                {
                    get
                    {
                        return
                            (PanelId != null) ?
                             ((NfiEstaDB)Composite.Database).SDesign.TPanel[(int)PanelId] :
                            null;
                    }
                }


                /// <summary>
                /// Superior panel identifier
                /// </summary>
                public Nullable<int> PanelSubsetId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TFnGetPanelsInEstimationCellsList.ColPanelSubsetId.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnGetPanelsInEstimationCellsList.ColPanelSubsetId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TFnGetPanelsInEstimationCellsList.ColPanelSubsetId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TFnGetPanelsInEstimationCellsList.ColPanelSubsetId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Superior panel label (read-only)
                /// </summary>
                public string PanelSubsetLabel
                {
                    get
                    {
                        return
                            (PanelSubset != null) ?
                                PanelSubset.Label :
                                String.Empty;
                    }
                }

                /// <summary>
                /// Superior panel object (read-only)
                /// </summary>
                public SDesign.Panel PanelSubset
                {
                    get
                    {
                        return
                            (PanelSubsetId != null) ?
                             ((NfiEstaDB)Composite.Database).SDesign.TPanel[(int)PanelSubsetId] :
                            null;
                    }
                }


                /// <summary>
                /// Reference year set identifier
                /// </summary>
                public Nullable<int> ReferenceYearSetId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TFnGetPanelsInEstimationCellsList.ColReferenceYearSetId.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnGetPanelsInEstimationCellsList.ColReferenceYearSetId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TFnGetPanelsInEstimationCellsList.ColReferenceYearSetId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TFnGetPanelsInEstimationCellsList.ColReferenceYearSetId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Reference year set label
                /// </summary>
                public string ReferenceYearSetLabel
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnGetPanelsInEstimationCellsList.ColReferenceYearSetLabel.Name,
                            defaultValue: TFnGetPanelsInEstimationCellsList.ColReferenceYearSetLabel.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnGetPanelsInEstimationCellsList.ColReferenceYearSetLabel.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Reference year set object (read-only)
                /// </summary>
                public SDesign.ReferenceYearSet ReferenceYearSet
                {
                    get
                    {
                        return
                            (ReferenceYearSetId != null) ?
                             ((NfiEstaDB)Composite.Database).SDesign.TReferenceYearSet[(int)ReferenceYearSetId] :
                            null;
                    }
                }


                /// <summary>
                /// Reference date begin
                /// </summary>
                public Nullable<DateTime> ReferenceDateBegin
                {
                    get
                    {
                        return Functions.GetNDateTimeArg(
                            row: Data,
                            name: TFnGetPanelsInEstimationCellsList.ColReferenceDateBegin.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnGetPanelsInEstimationCellsList.ColReferenceDateBegin.DefaultValue) ?
                                            (Nullable<DateTime>)null :
                                            DateTime.Parse(s: TFnGetPanelsInEstimationCellsList.ColReferenceDateBegin.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNDateTimeArg(
                            row: Data,
                            name: TFnGetPanelsInEstimationCellsList.ColReferenceDateBegin.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Reference date begin as text
                /// </summary>
                public string ReferenceDateBeginText
                {
                    get
                    {
                        return
                            (ReferenceDateBegin == null) ?
                                Functions.StrNull :
                                ((DateTime)ReferenceDateBegin).ToString(
                                    format: TFnGetPanelsInEstimationCellsList.ColReferenceDateBegin.NumericFormat);
                    }
                }


                /// <summary>
                /// Reference date end
                /// </summary>
                public Nullable<DateTime> ReferenceDateEnd
                {
                    get
                    {
                        return Functions.GetNDateTimeArg(
                            row: Data,
                            name: TFnGetPanelsInEstimationCellsList.ColReferenceDateEnd.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnGetPanelsInEstimationCellsList.ColReferenceDateEnd.DefaultValue) ?
                                            (Nullable<DateTime>)null :
                                            DateTime.Parse(s: TFnGetPanelsInEstimationCellsList.ColReferenceDateEnd.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNDateTimeArg(
                            row: Data,
                            name: TFnGetPanelsInEstimationCellsList.ColReferenceDateEnd.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Reference date end as text
                /// </summary>
                public string ReferenceDateEndText
                {
                    get
                    {
                        return
                            (ReferenceDateEnd == null) ?
                                Functions.StrNull :
                                ((DateTime)ReferenceDateEnd).ToString(
                                    format: TFnGetPanelsInEstimationCellsList.ColReferenceDateEnd.NumericFormat);
                    }
                }


                /// <summary>
                /// If panel falls within estimate dates
                /// </summary>
                public Nullable<bool> ReferenceYearSetFit
                {
                    get
                    {
                        return Functions.GetNBoolArg(
                            row: Data,
                            name: TFnGetPanelsInEstimationCellsList.ColReferenceYearSetFit.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnGetPanelsInEstimationCellsList.ColReferenceYearSetFit.DefaultValue) ?
                                (Nullable<bool>)null :
                                Boolean.Parse(value: TFnGetPanelsInEstimationCellsList.ColReferenceYearSetFit.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNBoolArg(
                            row: Data,
                            name: TFnGetPanelsInEstimationCellsList.ColReferenceYearSetFit.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Portion of panel inside
                /// </summary>
                public Nullable<double> PortionOfPanelInside
                {
                    get
                    {
                        return Functions.GetNDoubleArg(
                            row: Data,
                            name: TFnGetPanelsInEstimationCellsList.ColPortionOfPanelInside.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnGetPanelsInEstimationCellsList.ColPortionOfPanelInside.DefaultValue) ?
                                (Nullable<double>)null :
                                Double.Parse(s: TFnGetPanelsInEstimationCellsList.ColPortionOfPanelInside.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNDoubleArg(
                            row: Data,
                            name: TFnGetPanelsInEstimationCellsList.ColPortionOfPanelInside.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Portion of period covered by panel
                /// </summary>
                public Nullable<double> PortionOfPeriodCovered
                {
                    get
                    {
                        return Functions.GetNDoubleArg(
                            row: Data,
                            name: TFnGetPanelsInEstimationCellsList.ColPortionOfPeriodCovered.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnGetPanelsInEstimationCellsList.ColPortionOfPeriodCovered.DefaultValue) ?
                                (Nullable<double>)null :
                                Double.Parse(s: TFnGetPanelsInEstimationCellsList.ColPortionOfPeriodCovered.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNDoubleArg(
                            row: Data,
                            name: TFnGetPanelsInEstimationCellsList.ColPortionOfPeriodCovered.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Number of points in panel
                /// </summary>
                public Nullable<int> Total
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TFnGetPanelsInEstimationCellsList.ColTotal.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnGetPanelsInEstimationCellsList.ColTotal.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TFnGetPanelsInEstimationCellsList.ColTotal.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TFnGetPanelsInEstimationCellsList.ColTotal.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Panel with max number of points
                /// </summary>
                public Nullable<bool> IsMax
                {
                    get
                    {
                        return Functions.GetNBoolArg(
                            row: Data,
                            name: TFnGetPanelsInEstimationCellsList.ColIsMax.Name,
                            defaultValue: String.IsNullOrEmpty(value: TFnGetPanelsInEstimationCellsList.ColIsMax.DefaultValue) ?
                                (Nullable<bool>)null :
                                Boolean.Parse(value: TFnGetPanelsInEstimationCellsList.ColIsMax.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNBoolArg(
                            row: Data,
                            name: TFnGetPanelsInEstimationCellsList.ColIsMax.Name,
                            val: value);
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not TFnGetPanelsInEstimationCells Type, return False
                    if (obj is not TFnGetPanelsInEstimationCells)
                    {
                        return false;
                    }

                    return
                        (StratumId == ((TFnGetPanelsInEstimationCells)obj).StratumId) &&
                        (PanelId == ((TFnGetPanelsInEstimationCells)obj).PanelId) &&
                        (ReferenceYearSetId == ((TFnGetPanelsInEstimationCells)obj).ReferenceYearSetId);
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        StratumId.GetHashCode() ^
                        PanelId.GetHashCode() ^
                        ReferenceYearSetId.GetHashCode();
                }

                /// <summary>
                /// Text description of panel in estimation cell object
                /// </summary>
                /// <returns>Text description of panel in estimation cell object</returns>
                public override string ToString()
                {
                    return ((NfiEstaDB)Composite.Database).Postgres.Setting.LanguageVersion switch
                    {
                        LanguageVersion.National => String.Concat(
                            $"{"Stratum-id"} = {Functions.PrepNIntArg(arg: StratumId)},{Environment.NewLine}",
                            $"{"Stratum-label"} = {Functions.PrepStringArg(arg: StratumLabel)},{Environment.NewLine}",
                            $"{"Panel-id"} = {Functions.PrepNIntArg(arg: PanelId)},{Environment.NewLine}",
                            $"{"Panel-label"} = {Functions.PrepStringArg(arg: PanelLabel)},{Environment.NewLine}",
                            $"{"ReferenceYearSet-id"} = {Functions.PrepNIntArg(arg: ReferenceYearSetId)},{Environment.NewLine}",
                            $"{"ReferenceYearSet-label"} = {Functions.PrepStringArg(arg: ReferenceYearSetLabel)},{Environment.NewLine}"
                            ),
                        LanguageVersion.International => String.Concat(
                            $"{"Stratum-id"} = {Functions.PrepNIntArg(arg: StratumId)},{Environment.NewLine}",
                            $"{"Stratum-label"} = {Functions.PrepStringArg(arg: StratumLabel)},{Environment.NewLine}",
                            $"{"Panel-id"} = {Functions.PrepNIntArg(arg: PanelId)},{Environment.NewLine}",
                            $"{"Panel-label"} = {Functions.PrepStringArg(arg: PanelLabel)},{Environment.NewLine}",
                            $"{"ReferenceYearSet-id"} = {Functions.PrepNIntArg(arg: ReferenceYearSetId)},{Environment.NewLine}",
                            $"{"ReferenceYearSet-label"} = {Functions.PrepStringArg(arg: ReferenceYearSetLabel)},{Environment.NewLine}"
                            ),
                        _ => String.Concat(
                            $"{"Stratum-id"} = {Functions.PrepNIntArg(arg: StratumId)},{Environment.NewLine}",
                            $"{"Stratum-label"} = {Functions.PrepStringArg(arg: StratumLabel)},{Environment.NewLine}",
                            $"{"Panel-id"} = {Functions.PrepNIntArg(arg: PanelId)},{Environment.NewLine}",
                            $"{"Panel-label"} = {Functions.PrepStringArg(arg: PanelLabel)},{Environment.NewLine}",
                            $"{"ReferenceYearSet-id"} = {Functions.PrepNIntArg(arg: ReferenceYearSetId)},{Environment.NewLine}",
                            $"{"ReferenceYearSet-label"} = {Functions.PrepStringArg(arg: ReferenceYearSetLabel)},{Environment.NewLine}"
                            ),
                    };
                }

                #endregion Methods

            }


            /// <summary>
            /// List of panels in estimation cell
            /// </summary>
            public class TFnGetPanelsInEstimationCellsList
                : AParametrizedView
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema =
                    NfiEstaFunctions.FnGetPanelsInEstimationCells.SchemaName;

                /// <summary>
                /// Stored procedure name
                /// </summary>
                public static readonly string Name =
                    NfiEstaFunctions.FnGetPanelsInEstimationCells.Name;

                /// <summary>
                /// Stored procedure alias
                /// </summary>
                public static readonly string Alias =
                    NfiEstaFunctions.FnGetPanelsInEstimationCells.Name;

                /// <summary>
                /// Description in national language
                /// </summary>
                public static readonly string CaptionCs =
                    "Uložená procedura, která vrací seznam panelů pro výpočetní buňky";

                /// <summary>
                /// Description in English
                /// </summary>
                public static readonly string CaptionEn =
                    "Stored procedure that returns list of panels for list of estimation cells";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
                {
                    { "id", new ColumnMetadata()
                    {
                        Name = "id",
                        DbName = "id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = true,
                        NotNull = true,
                        DefaultValue = "0",
                        FuncCall = "row_number() over()",
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ID",
                        HeaderTextEn = "ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 0
                    }
                    },
                    { "stratum", new ColumnMetadata()
                    {
                        Name = "stratum",
                        DbName = "stratum",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "STRATUM",
                        HeaderTextEn = "STRATUM",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 1
                    }
                    },
                    { "stratum_label", new ColumnMetadata()
                    {
                        Name = "stratum_label",
                        DbName = "stratum_label",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "STRATUM_LABEL",
                        HeaderTextEn = "STRATUM_LABEL",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 2
                    }
                    },
                    { "panel", new ColumnMetadata()
                    {
                        Name = "panel",
                        DbName = "panel",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PANEL",
                        HeaderTextEn = "PANEL",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 3
                    }
                    },
                    { "panel_label", new ColumnMetadata()
                    {
                        Name = "panel_label",
                        DbName = "panel_label",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PANEL_LABEL",
                        HeaderTextEn = "PANEL_LABEL",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 4
                    }
                    },
                    { "panel_subset", new ColumnMetadata()
                    {
                        Name = "panel_subset",
                        DbName = "panel_subset",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PANEL_SUBSET",
                        HeaderTextEn = "PANEL_SUBSET",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 5
                    }
                    },
                    { "reference_year_set", new ColumnMetadata()
                    {
                        Name = "reference_year_set",
                        DbName = "reference_year_set",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "REFERENCE_YEAR_SET",
                        HeaderTextEn = "REFERENCE_YEAR_SET",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 6
                    }
                    },
                    { "reference_year_set_label", new ColumnMetadata()
                    {
                        Name = "reference_year_set_label",
                        DbName = "reference_year_set_label",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "REFERENCE_YEAR_SET_LABEL",
                        HeaderTextEn = "REFERENCE_YEAR_SET_LABEL",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 7
                    }
                    },
                    { "reference_date_begin", new ColumnMetadata()
                    {
                        Name = "reference_date_begin",
                        DbName = "reference_date_begin",
                        DataType = "System.DateTime",
                        DbDataType = "date",
                        NewDataType = "date",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "yyyy-MM-dd",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "REFERENCE_DATE_BEGIN",
                        HeaderTextEn = "REFERENCE_DATE_BEGIN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 8
                    }
                    },
                    { "reference_date_end", new ColumnMetadata()
                    {
                        Name = "reference_date_end",
                        DbName = "reference_date_end",
                        DataType = "System.DateTime",
                        DbDataType = "date",
                        NewDataType = "date",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "yyyy-MM-dd",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "REFERENCE_DATE_END",
                        HeaderTextEn = "REFERENCE_DATE_END",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 9
                    }
                    },
                    { "reference_year_set_fit", new ColumnMetadata()
                    {
                        Name = "reference_year_set_fit",
                        DbName = "reference_year_set_fit",
                        DataType = "System.Boolean",
                        DbDataType = "bool",
                        NewDataType = "bool",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "REFERENCE_YEAR_SET_FIT",
                        HeaderTextEn = "REFERENCE_YEAR_SET_FIT",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 10
                    }
                    },
                    { "portion_of_panel_inside", new ColumnMetadata()
                    {
                        Name = "portion_of_panel_inside",
                        DbName = "portion_of_panel_inside",
                        DataType = "System.Double",
                        DbDataType = "float8",
                        NewDataType = "float8",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "N3",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PORTION_OF_PANEL_INSIDE",
                        HeaderTextEn = "PORTION_OF_PANEL_INSIDE",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 11
                    }
                    },
                    { "portion_of_period_covered", new ColumnMetadata()
                    {
                        Name = "portion_of_period_covered",
                        DbName = "portion_of_period_covered",
                        DataType = "System.Double",
                        DbDataType = "float8",
                        NewDataType = "float8",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "N3",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PORTION_OF_PERIOD_COVERED",
                        HeaderTextEn = "PORTION_OF_PERIOD_COVERED",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 12
                    }
                    },
                    { "total", new ColumnMetadata()
                    {
                        Name = "total",
                        DbName = "total",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "TOTAL",
                        HeaderTextEn = "TOTAL",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 13
                    }
                    },
                    { "is_max", new ColumnMetadata()
                    {
                        Name = "is_max",
                        DbName = "is_max",
                        DataType = "System.Boolean",
                        DbDataType = "bool",
                        NewDataType = "bool",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "IS_MAX",
                        HeaderTextEn = "IS_MAX",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 14
                    }
                    }
                };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column stratum metadata
                /// </summary>
                public static readonly ColumnMetadata ColStratumId = Cols["stratum"];

                /// <summary>
                /// Column stratum_label metadata
                /// </summary>
                public static readonly ColumnMetadata ColStratumLabel = Cols["stratum_label"];

                /// <summary>
                /// Column panel metadata
                /// </summary>
                public static readonly ColumnMetadata ColPanelId = Cols["panel"];

                /// <summary>
                /// Column panel_label metadata
                /// </summary>
                public static readonly ColumnMetadata ColPanelLabel = Cols["panel_label"];

                /// <summary>
                /// Column panel_subset metadata
                /// </summary>
                public static readonly ColumnMetadata ColPanelSubsetId = Cols["panel_subset"];

                /// <summary>
                /// Column reference_year_set metadata
                /// </summary>
                public static readonly ColumnMetadata ColReferenceYearSetId = Cols["reference_year_set"];

                /// <summary>
                /// Column reference_year_set_label metadata
                /// </summary>
                public static readonly ColumnMetadata ColReferenceYearSetLabel = Cols["reference_year_set_label"];

                /// <summary>
                /// Column reference_date_begin metadata
                /// </summary>
                public static readonly ColumnMetadata ColReferenceDateBegin = Cols["reference_date_begin"];

                /// <summary>
                /// Column reference_date_end metadata
                /// </summary>
                public static readonly ColumnMetadata ColReferenceDateEnd = Cols["reference_date_end"];

                /// <summary>
                /// Column reference_year_set_fit metadata
                /// </summary>
                public static readonly ColumnMetadata ColReferenceYearSetFit = Cols["reference_year_set_fit"];

                /// <summary>
                /// Column portion_of_panel_inside metadata
                /// </summary>
                public static readonly ColumnMetadata ColPortionOfPanelInside = Cols["portion_of_panel_inside"];

                /// <summary>
                /// Column portion_of_period_covered metadata
                /// </summary>
                public static readonly ColumnMetadata ColPortionOfPeriodCovered = Cols["portion_of_period_covered"];

                /// <summary>
                /// Column total metadata
                /// </summary>
                public static readonly ColumnMetadata ColTotal = Cols["total"];

                /// <summary>
                /// Column is_max metadata
                /// </summary>
                public static readonly ColumnMetadata ColIsMax = Cols["is_max"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                /// <summary>
                /// List of common panels in estimation cell between numerator and denominator,
                /// or null in case when numerator and denominator don't have common panels
                /// </summary>
                /// <param name="numerator">List of panels in estimation cell for numerator</param>
                /// <param name="denominator">List of panels in estimation cell for denominator</param>
                /// <returns>List of common panels in estimation cell between numerator and denominator
                /// or null in case when numerator and denominator don't have common panels </returns>
                public static TFnGetPanelsInEstimationCellsList CommonPanels(
                    TFnGetPanelsInEstimationCellsList numerator,
                    TFnGetPanelsInEstimationCellsList denominator)
                {
                    if (numerator == null)
                    {
                        return null;
                    }

                    if (denominator == null)
                    {
                        return null;
                    }

                    var cteNumerator = numerator.Data.AsEnumerable()
                        .Select(a => new
                        {
                            StratumId = a.Field<Nullable<int>>(columnName: ColStratumId.Name),
                            StratumLabel = a.Field<string>(columnName: ColStratumLabel.Name),
                            PanelId = a.Field<Nullable<int>>(columnName: ColPanelId.Name),
                            PanelLabel = a.Field<string>(columnName: ColPanelLabel.Name),
                            PanelSubsetId = a.Field<Nullable<int>>(columnName: ColPanelSubsetId.Name),
                            ReferenceYearSetId = a.Field<Nullable<int>>(columnName: ColReferenceYearSetId.Name),
                            ReferenceYearSetLabel = a.Field<string>(columnName: ColReferenceYearSetLabel.Name),
                            ReferenceDateBegin = a.Field<Nullable<DateTime>>(columnName: ColReferenceDateBegin.Name),
                            ReferenceDateEnd = a.Field<Nullable<DateTime>>(columnName: ColReferenceDateEnd.Name),
                            ReferenceYearSetFit = a.Field<Nullable<bool>>(columnName: ColReferenceYearSetFit.Name),
                            PortionOfPanelInside = a.Field<Nullable<double>>(columnName: ColPortionOfPanelInside.Name),
                            PortionOfPeriodCovered = a.Field<Nullable<double>>(columnName: ColPortionOfPeriodCovered.Name),
                            Total = a.Field<Nullable<int>>(columnName: ColTotal.Name),
                            IsMax = a.Field<Nullable<bool>>(columnName: ColIsMax.Name)
                        });

                    var cteDenominator = denominator.Data.AsEnumerable()
                        .Select(a => new
                        {
                            StratumId = a.Field<Nullable<int>>(columnName: ColStratumId.Name),
                            StratumLabel = a.Field<string>(columnName: ColStratumLabel.Name),
                            PanelId = a.Field<Nullable<int>>(columnName: ColPanelId.Name),
                            PanelLabel = a.Field<string>(columnName: ColPanelLabel.Name),
                            PanelSubsetId = a.Field<Nullable<int>>(columnName: ColPanelSubsetId.Name),
                            ReferenceYearSetId = a.Field<Nullable<int>>(columnName: ColReferenceYearSetId.Name),
                            ReferenceYearSetLabel = a.Field<string>(columnName: ColReferenceYearSetLabel.Name),
                            ReferenceDateBegin = a.Field<Nullable<DateTime>>(columnName: ColReferenceDateBegin.Name),
                            ReferenceDateEnd = a.Field<Nullable<DateTime>>(columnName: ColReferenceDateEnd.Name),
                            ReferenceYearSetFit = a.Field<Nullable<bool>>(columnName: ColReferenceYearSetFit.Name),
                            PortionOfPanelInside = a.Field<Nullable<double>>(columnName: ColPortionOfPanelInside.Name),
                            PortionOfPeriodCovered = a.Field<Nullable<double>>(columnName: ColPortionOfPeriodCovered.Name),
                            Total = a.Field<Nullable<int>>(columnName: ColTotal.Name),
                            IsMax = a.Field<Nullable<bool>>(columnName: ColIsMax.Name)
                        });

                    var cteCommonPanels =
                        from a in cteNumerator
                        join b in cteDenominator
                        on
                        $"{a.StratumId}-{a.PanelId}-{a.ReferenceYearSetId}" equals
                        $"{b.StratumId}-{b.PanelId}-{b.ReferenceYearSetId}"
                        select a;

                    if (cteCommonPanels.Any())
                    {
                        DataTable dtCommonPanels = EmptyDataTable();

                        IEnumerable<DataRow> rowsCommonPanels =
                            cteCommonPanels
                            .Select(a => dtCommonPanels.LoadDataRow(
                                values:
                                [
                                    0,              //Id
                                    a.StratumId,
                                    a.StratumLabel,
                                    a.PanelId,
                                    a.PanelLabel,
                                    a.PanelSubsetId,
                                    a.ReferenceYearSetId,
                                    a.ReferenceYearSetLabel,
                                    a.ReferenceDateBegin,
                                    a.ReferenceDateEnd,
                                    a.ReferenceYearSetFit,
                                    a.PortionOfPanelInside,
                                    a.PortionOfPeriodCovered,
                                    a.Total,
                                    a.IsMax
                                ],
                            fAcceptChanges: false));

                        if (rowsCommonPanels.Any())
                        {
                            dtCommonPanels = rowsCommonPanels.CopyToDataTable();

                            return
                                new TFnGetPanelsInEstimationCellsList(
                                    database: (NfiEstaDB)numerator.Database,
                                    data: dtCommonPanels)
                                {
                                    EstimationCellIdList = numerator.EstimationCellIdList,
                                    EstimateDateBegin = numerator.EstimateDateBegin,
                                    EstimateDateEnd = numerator.EstimateDateEnd,
                                    VariableId = numerator.VariableId
                                };
                        }
                        else
                        {
                            // Common panels don't exist
                            return null;
                        }
                    }
                    else
                    {
                        // Common panels don't exist
                        return null;
                    }
                }

                #endregion Static Methods


                #region Private Fields

                /// <summary>
                /// First parameter - List of estimation cells identifiers
                /// </summary>
                private List<Nullable<int>> estimationCellIdList;

                /// <summary>
                /// Second parameter - The beginning of the time to witch the estimate is calculated
                /// </summary>
                private Nullable<DateTime> estimateDateBegin;

                /// <summary>
                /// Third parameter - The end of the time to witch the estimate is calculated
                /// </summary>
                private Nullable<DateTime> estimateDateEnd;

                /// <summary>
                /// Fourth parameter - Attribute category identifier
                /// </summary>
                private Nullable<int> variableId;

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TFnGetPanelsInEstimationCellsList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                {
                    EstimationCellIdList = [];
                    EstimateDateBegin = null;
                    EstimateDateEnd = null;
                    VariableId = null;
                }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TFnGetPanelsInEstimationCellsList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                {
                    EstimationCellIdList = [];
                    EstimateDateBegin = null;
                    EstimateDateEnd = null;
                    VariableId = null;
                }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// First parameter - List of estimation cells identifiers
                /// </summary>
                public List<Nullable<int>> EstimationCellIdList
                {
                    get
                    {
                        return estimationCellIdList ?? [];
                    }
                    set
                    {
                        estimationCellIdList = value ?? [];
                    }
                }

                /// <summary>
                /// Second parameter - The beginning of the time to witch the estimate is calculated
                /// </summary>
                public Nullable<DateTime> EstimateDateBegin
                {
                    get
                    {
                        return estimateDateBegin;
                    }
                    set
                    {
                        estimateDateBegin = value;
                    }
                }

                /// <summary>
                /// Third parameter - The end of the time to witch the estimate is calculated
                /// </summary>
                public Nullable<DateTime> EstimateDateEnd
                {
                    get
                    {
                        return estimateDateEnd;
                    }
                    set
                    {
                        estimateDateEnd = value;
                    }
                }

                /// <summary>
                /// Fourth parameter - Attribute category identifier (read-only)
                /// </summary>
                public Nullable<int> VariableId
                {
                    get
                    {
                        return variableId;
                    }
                    set
                    {
                        variableId = value;
                    }
                }

                /// <summary>
                /// List of panels in estimation cell (read-only)
                /// </summary>
                public List<TFnGetPanelsInEstimationCells> Items
                {
                    get
                    {
                        return
                        [..
                            Data.AsEnumerable()
                            .Select(a => new TFnGetPanelsInEstimationCells(composite: this, data: a))
                        ];
                    }
                }

                /// <summary>
                /// List of stata for panels in estimation cell (read-only)
                /// </summary>
                public List<SDesign.Stratum> Strata
                {
                    get
                    {
                        IEnumerable<int> cteStatumIdList =
                            Data.AsEnumerable()
                            .Select(a => (int)(a.Field<Nullable<int>>(ColStratumId.Name) ?? 0))
                            .Distinct<int>();
                        return
                            ((NfiEstaDB)Database).SDesign.TStratum.Items
                            .Where(a => cteStatumIdList.Contains(value: a.Id))
                            .ToList<SDesign.Stratum>();
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// Panel in estimation cell from list by identifier (read-only)
                /// </summary>
                /// <param name="id">Panel in estimation cell identifier</param>
                /// <returns>Panel in estimation cell from list by identifier (null if not found)</returns>
                public TFnGetPanelsInEstimationCells this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new TFnGetPanelsInEstimationCells(composite: this, data: a))
                                .FirstOrDefault<TFnGetPanelsInEstimationCells>();
                    }
                }

                #endregion Indexer


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public TFnGetPanelsInEstimationCellsList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new TFnGetPanelsInEstimationCellsList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime)
                        {
                            EstimationCellIdList = [.. EstimationCellIdList],
                            EstimateDateBegin = EstimateDateBegin,
                            EstimateDateEnd = EstimateDateEnd,
                            VariableId = VariableId
                        } :
                        new TFnGetPanelsInEstimationCellsList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null)
                        {
                            EstimationCellIdList = [.. EstimationCellIdList],
                            EstimateDateBegin = EstimateDateBegin,
                            EstimateDateEnd = EstimateDateEnd,
                            VariableId = VariableId
                        };
                }

                #endregion Methods

            }

        }
    }
}