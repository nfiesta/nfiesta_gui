﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {
            // cm_plot2param_area_mapping

            /// <summary>
            /// Mapping between inventory plot and parametrization area
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class PlotToParametrizationAreaMapping(
                PlotToParametrizationAreaMappingList composite = null,
                DataRow data = null)
                    : AMappingTableEntry<PlotToParametrizationAreaMappingList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Mapping between inventory plot and parametrization area identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: PlotToParametrizationAreaMappingList.ColId.Name,
                            defaultValue: Int32.Parse(s: PlotToParametrizationAreaMappingList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: PlotToParametrizationAreaMappingList.ColId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Inventory plot identifier
                /// </summary>
                public Nullable<int> PlotId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: PlotToParametrizationAreaMappingList.ColPlotId.Name,
                            defaultValue: String.IsNullOrEmpty(PlotToParametrizationAreaMappingList.ColPlotId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: PlotToParametrizationAreaMappingList.ColPlotId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: PlotToParametrizationAreaMappingList.ColPlotId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Inventory plot object (read-only)
                /// </summary>
                public SDesign.Plot Plot
                {
                    get
                    {
                        return (PlotId != null) ?
                             ((NfiEstaDB)Composite.Database).SDesign.FpPlot[(int)PlotId] : null;
                    }
                }


                /// <summary>
                /// Parametrization area identifier
                /// </summary>
                public Nullable<int> ParamAreaId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: PlotToParametrizationAreaMappingList.ColParamAreaId.Name,
                            defaultValue: String.IsNullOrEmpty(value: PlotToParametrizationAreaMappingList.ColParamAreaId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: PlotToParametrizationAreaMappingList.ColParamAreaId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: PlotToParametrizationAreaMappingList.ColParamAreaId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Parametrization area object (read-only)
                /// </summary>
                public ParamArea ParamArea
                {
                    get
                    {
                        return (ParamAreaId != null) ?
                             ((NfiEstaDB)Composite.Database).SNfiEsta.FaParamArea[(int)ParamAreaId] : null;
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not PlotToParametrizationAreaMapping Type, return False
                    if (obj is not PlotToParametrizationAreaMapping)
                    {
                        return false;
                    }

                    return
                        Id == ((PlotToParametrizationAreaMapping)obj).Id;
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                #endregion Methods

            }


            /// <summary>
            /// List of mappings between inventory plot and parametrization area
            /// </summary>
            public class PlotToParametrizationAreaMappingList
                : AMappingTable
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema = NfiEstaSchema.Name;

                /// <summary>
                /// Table name
                /// </summary>
                public const string Name = "cm_plot2param_area_mapping";

                /// <summary>
                /// Table alias
                /// </summary>
                public const string Alias = "cm_plot2param_area_mapping";

                /// <summary>
                /// Description in national language
                /// </summary>
                public const string CaptionCs = "Seznam mapování inventarizačních ploch do parametrizačních oblastí";

                /// <summary>
                /// Description in English
                /// </summary>
                public const string CaptionEn = "List of mappings between inventory plot and parametrization area";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
                {
                    { "id", new ColumnMetadata()
                    {
                        Name = "id",
                        DbName = "id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = true,
                        NotNull = true,
                        DefaultValue = default(int).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ID",
                        HeaderTextEn = "ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 0
                    }
                    },
                    { "plot", new ColumnMetadata()
                    {
                        Name = "plot",
                        DbName = "plot",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PLOT",
                        HeaderTextEn = "PLOT",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 1
                    }
                    },
                    { "param_area", new ColumnMetadata()
                    {
                        Name = "param_area",
                        DbName = "param_area",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PARAM_AREA",
                        HeaderTextEn = "PARAM_AREA",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 2
                    }
                    }
                };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column plot metadata
                /// </summary>
                public static readonly ColumnMetadata ColPlotId = Cols["plot"];

                /// <summary>
                /// Column param_area metadata
                /// </summary>
                public static readonly ColumnMetadata ColParamAreaId = Cols["param_area"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public PlotToParametrizationAreaMappingList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public PlotToParametrizationAreaMappingList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// List of mappings between inventory plot and parametrization area(read-only)
                /// </summary>
                public List<PlotToParametrizationAreaMapping> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new PlotToParametrizationAreaMapping(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// Mapping between inventory plot and parametrization area from list by identifier (read-only)
                /// </summary>
                /// <param name="id">Mapping between inventory plot and parametrization area identifier</param>
                /// <returns>Mapping between inventory plot and parametrization area from list by identifier (null if not found)</returns>
                public PlotToParametrizationAreaMapping this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new PlotToParametrizationAreaMapping(composite: this, data: a))
                                .FirstOrDefault<PlotToParametrizationAreaMapping>();
                    }
                }

                #endregion Indexer


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public PlotToParametrizationAreaMappingList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new PlotToParametrizationAreaMappingList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new PlotToParametrizationAreaMappingList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                /// <summary>
                /// List of plots
                /// for selected parametrization areas
                /// </summary>
                /// <param name="paramAreaId">Selected parametrization areas identifiers</param>
                /// <returns>
                /// List of plots
                /// for selected parametrization areas
                /// </returns>
                public PlotToParametrizationAreaMappingList Reduce(
                    List<int> paramAreaId = null)
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();

                    if ((paramAreaId != null) && paramAreaId.Count != 0)
                    {
                        rows = rows.Where(a => paramAreaId.Contains<int>(
                                value: (int)(a.Field<Nullable<int>>(columnName: ColParamAreaId.Name) ?? 0)));
                    }

                    return
                        rows.Any() ?
                        new PlotToParametrizationAreaMappingList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new PlotToParametrizationAreaMappingList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                #endregion Methods

            }

        }
    }
}
