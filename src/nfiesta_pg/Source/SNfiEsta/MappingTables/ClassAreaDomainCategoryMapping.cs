﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {
            // cm_area_domain_category

            /// <summary>
            /// Mapping area domain categories
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class AreaDomainCategoryMapping(
                AreaDomainCategoryMappingList composite = null,
                DataRow data = null)
                    : AMappingTableEntry<AreaDomainCategoryMappingList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Mapping area domain categories identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: AreaDomainCategoryMappingList.ColId.Name,
                            defaultValue: Int32.Parse(AreaDomainCategoryMappingList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: AreaDomainCategoryMappingList.ColId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Identifier of area domain category, foreign key to table c_area_domain_category.
                /// </summary>
                public int AreaDomainCategoryId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: AreaDomainCategoryMappingList.ColAreaDomainCategoryId.Name,
                            defaultValue: Int32.Parse(s: AreaDomainCategoryMappingList.ColAreaDomainCategoryId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: AreaDomainCategoryMappingList.ColAreaDomainCategoryId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Area domain category object
                /// </summary>
                public AreaDomainCategory AreaDomainCategory
                {
                    get
                    {
                        return ((NfiEstaDB)Composite.Database).SNfiEsta.CAreaDomainCategory[AreaDomainCategoryId];
                    }
                }


                /// <summary>
                /// Identifier of atomic area domain category,
                /// foreign key to table c_area_domain_category.
                /// </summary>
                public int AtomicAreaDomainCategoryId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: AreaDomainCategoryMappingList.ColAtomicAreaDomainCategoryId.Name,
                            defaultValue: Int32.Parse(s: AreaDomainCategoryMappingList.ColAtomicAreaDomainCategoryId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: AreaDomainCategoryMappingList.ColAtomicAreaDomainCategoryId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Atomic area domain category object
                /// </summary>
                public AreaDomainCategory AtomicAreaDomainCategory
                {
                    get
                    {
                        return ((NfiEstaDB)Composite.Database).SNfiEsta.CAreaDomainCategory[AtomicAreaDomainCategoryId];
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not AreaDomainCategoryMapping Type, return False
                    if (obj is not AreaDomainCategoryMapping)
                    {
                        return false;
                    }

                    return
                        Id == ((AreaDomainCategoryMapping)obj).Id;
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                #endregion Methods

            }


            /// <summary>
            /// List of mappings area domain categories
            /// </summary>
            public class AreaDomainCategoryMappingList
                : AMappingTable
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema = NfiEstaSchema.Name;

                /// <summary>
                /// Table name
                /// </summary>
                public const string Name = "cm_area_domain_category";

                /// <summary>
                /// Table alias
                /// </summary>
                public const string Alias = "cm_area_domain_category";

                /// <summary>
                /// Description in national language
                /// </summary>
                public const string CaptionCs = "Seznam mapování kategorií plošných domén";

                /// <summary>
                /// Description in English
                /// </summary>
                public const string CaptionEn = "List of mappings area domain categories";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
                {
                    { "id", new ColumnMetadata()
                    {
                        Name = "id",
                        DbName = "id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = true,
                        NotNull = true,
                        DefaultValue = default(int).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ID",
                        HeaderTextEn = "ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 0
                    }
                    },
                    { "area_domain_category", new ColumnMetadata()
                    {
                        Name = "area_domain_category",
                        DbName = "area_domain_category",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = default(int).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "AREA_DOMAIN_CATEGORY",
                        HeaderTextEn = "AREA_DOMAIN_CATEGORY",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 1
                    }
                    },
                    { "atomic_category", new ColumnMetadata()
                    {
                        Name = "atomic_category",
                        DbName = "atomic_category",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = true,
                        DefaultValue= default(int).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ATOMIC_CATEGORY",
                        HeaderTextEn = "ATOMIC_CATEGORY",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 2
                    }
                    }
                };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column area_domain_category metadata
                /// </summary>
                public static readonly ColumnMetadata ColAreaDomainCategoryId = Cols["area_domain_category"];

                /// <summary>
                /// Column atomic_category
                /// </summary>
                public static readonly ColumnMetadata ColAtomicAreaDomainCategoryId = Cols["atomic_category"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public AreaDomainCategoryMappingList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public AreaDomainCategoryMappingList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// List of mappings area domain categories (read-only)
                /// </summary>
                public List<AreaDomainCategoryMapping> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new AreaDomainCategoryMapping(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// Mapping area domain categories from list by identifier (read-only)
                /// </summary>
                /// <param name="id">Mapping area domain categories identifier</param>
                /// <returns>Mapping area domain categories from list by identifier (null if not found)</returns>
                public AreaDomainCategoryMapping this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new AreaDomainCategoryMapping(composite: this, data: a))
                                .FirstOrDefault<AreaDomainCategoryMapping>();
                    }
                }

                #endregion Indexer


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public AreaDomainCategoryMappingList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new AreaDomainCategoryMappingList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new AreaDomainCategoryMappingList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                /// <summary>
                /// List of mappings area domain categories
                /// for selected area domain categories and atomic area domain categories
                /// </summary>
                /// <param name="areaDomainCategoryId">Selected area domain categories identifiers</param>
                /// <param name="atomicAreaDomainCategoryId">Selected atomic area domain categories identifiers</param>
                /// <returns>
                /// List of mappings area domain categories
                /// for selected area domain categories and atomic area domain categories
                /// </returns>
                public AreaDomainCategoryMappingList Reduce(
                    List<int> areaDomainCategoryId = null,
                    List<int> atomicAreaDomainCategoryId = null)
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();

                    if ((areaDomainCategoryId != null) && areaDomainCategoryId.Count != 0)
                    {
                        rows = rows.Where(a => areaDomainCategoryId.Contains<int>(
                                value: (int)(a.Field<Nullable<int>>(columnName: ColAreaDomainCategoryId.Name) ?? 0)));
                    }

                    if ((atomicAreaDomainCategoryId != null) && atomicAreaDomainCategoryId.Count != 0)
                    {
                        rows = rows.Where(a => atomicAreaDomainCategoryId.Contains<int>(
                                value: (int)(a.Field<Nullable<int>>(columnName: ColAtomicAreaDomainCategoryId.Name) ?? 0)));
                    }

                    return
                        rows.Any() ?
                        new AreaDomainCategoryMappingList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new AreaDomainCategoryMappingList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                #endregion Methods

            }

        }
    }
}
