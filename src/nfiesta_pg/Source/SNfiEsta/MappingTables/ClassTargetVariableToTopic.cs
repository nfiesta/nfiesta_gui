﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {
            // cm_tvariable2topic

            /// <summary>
            /// Mapping between target variable and topic
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class TargetVariableToTopic(
                TargetVariableToTopicList composite = null,
                DataRow data = null)
                    : AMappingTableEntry<TargetVariableToTopicList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Mapping between target variable and topic identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TargetVariableToTopicList.ColId.Name,
                            defaultValue: Int32.Parse(s: TargetVariableToTopicList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TargetVariableToTopicList.ColId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Target variable identifier
                /// </summary>
                public int TargetVariableId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TargetVariableToTopicList.ColTargetVariableId.Name,
                            defaultValue: Int32.Parse(s: TargetVariableToTopicList.ColTargetVariableId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TargetVariableToTopicList.ColTargetVariableId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Target variable object
                /// </summary>
                public TargetVariable TargetVariable
                {
                    get
                    {
                        return (TargetVariable)((NfiEstaDB)Composite.Database).SNfiEsta.CTargetVariable[TargetVariableId];
                    }
                }


                /// <summary>
                /// Topic identifier
                /// </summary>
                public int TopicId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TargetVariableToTopicList.ColTopicId.Name,
                            defaultValue: Int32.Parse(s: TargetVariableToTopicList.ColTopicId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TargetVariableToTopicList.ColTopicId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Topic object
                /// </summary>
                public Topic Topic
                {
                    get
                    {
                        return ((NfiEstaDB)Composite.Database).SNfiEsta.CTopic[TopicId];
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not TargetVariableToTopic Type, return False
                    if (obj is not TargetVariableToTopic)
                    {
                        return false;
                    }

                    return
                        Id == ((TargetVariableToTopic)obj).Id;
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                #endregion Methods

            }


            /// <summary>
            /// List of mappings between target variable and topic
            /// </summary>
            public class TargetVariableToTopicList
                : AMappingTable
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema = NfiEstaSchema.Name;

                /// <summary>
                /// Table name
                /// </summary>
                public const string Name = "cm_tvariable2topic";

                /// <summary>
                /// Table alias
                /// </summary>
                public const string Alias = "cm_tvariable2topic";

                /// <summary>
                /// Description in national language
                /// </summary>
                public const string CaptionCs = "Seznam mapování mezi cílovou proměnnou a tématickým okruhem";

                /// <summary>
                /// Description in English
                /// </summary>
                public const string CaptionEn = "List of mappings between target variable and topic";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
                {
                    { "id", new ColumnMetadata()
                    {
                        Name = "id",
                        DbName = "id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = true,
                        NotNull = true,
                        DefaultValue = default(int).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ID",
                        HeaderTextEn = "ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 0
                    }
                    },
                    { "target_variable", new ColumnMetadata()
                    {
                        Name = "target_variable",
                        DbName = "target_variable",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = default(int).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "TARGET_VARIABLE",
                        HeaderTextEn = "TARGET_VARIABLE",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 1
                    }
                    },
                    { "topic", new ColumnMetadata()
                    {
                        Name = "topic",
                        DbName = "topic",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = default(int).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "TOPIC",
                        HeaderTextEn = "TOPIC",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 2
                    }
                    }
                };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column target_variable metadata
                /// </summary>
                public static readonly ColumnMetadata ColTargetVariableId = Cols["target_variable"];

                /// <summary>
                /// Column topic metadata
                /// </summary>
                public static readonly ColumnMetadata ColTopicId = Cols["topic"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TargetVariableToTopicList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TargetVariableToTopicList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// List of mappings between target variable and topic (read-only)
                /// </summary>
                public List<TargetVariableToTopic> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new TargetVariableToTopic(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// Mapping between target variable and topic from list by identifier (read-only)
                /// </summary>
                /// <param name="id">Mapping between target variable and topic identifier</param>
                /// <returns>Mapping between target variable and topic from list by identifier (null if not found)</returns>
                public TargetVariableToTopic this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new TargetVariableToTopic(composite: this, data: a))
                                .FirstOrDefault<TargetVariableToTopic>();
                    }
                }

                #endregion Indexer


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public TargetVariableToTopicList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new TargetVariableToTopicList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new TargetVariableToTopicList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                /// <summary>
                /// List of mappings between target variable and topic
                /// for selected target variables and topics
                /// </summary>
                /// <param name="targetVariableId">Selected target variables identifiers</param>
                /// <param name="topicId">Selected topics identifiers</param>
                /// <returns>
                /// List of mappings between target variable and topic
                /// for selected target variables and topics
                /// </returns>
                public TargetVariableToTopicList Reduce(
                    List<int> targetVariableId = null,
                    List<int> topicId = null)
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();

                    if ((targetVariableId != null) && targetVariableId.Count != 0)
                    {
                        rows = rows.Where(a => targetVariableId.Contains<int>(
                                value: (int)(a.Field<Nullable<int>>(columnName: ColTargetVariableId.Name) ?? 0)));
                    }

                    if ((topicId != null) && topicId.Count != 0)
                    {
                        rows = rows.Where(a => topicId.Contains<int>(
                                value: (int)(a.Field<Nullable<int>>(columnName: ColTopicId.Name) ?? 0)));
                    }

                    return
                        rows.Any() ?
                        new TargetVariableToTopicList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new TargetVariableToTopicList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                #endregion Methods

            }

        }
    }
}