﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace Core
        {
            // cm_plot2cell_mapping

            /// <summary>
            /// Mapping between inventory plot and estimation cell
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class PlotToEstimationCellMapping(
                PlotToEstimationCellMappingList composite = null,
                DataRow data = null)
                    : AMappingTableEntry<PlotToEstimationCellMappingList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Mapping between inventory plot and estimation cell identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: PlotToEstimationCellMappingList.ColId.Name,
                            defaultValue: Int32.Parse(s: PlotToEstimationCellMappingList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: PlotToEstimationCellMappingList.ColId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Inventory plot identifier
                /// </summary>
                public Nullable<int> PlotId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: PlotToEstimationCellMappingList.ColPlotId.Name,
                            defaultValue: String.IsNullOrEmpty(PlotToEstimationCellMappingList.ColPlotId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: PlotToEstimationCellMappingList.ColPlotId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: PlotToEstimationCellMappingList.ColPlotId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Inventory plot object (read-only)
                /// </summary>
                public SDesign.Plot Plot
                {
                    get
                    {
                        return (PlotId != null) ?
                             ((NfiEstaDB)Composite.Database).SDesign.FpPlot[(int)PlotId] : null;
                    }
                }


                /// <summary>
                /// Estimation cell identifier
                /// </summary>
                public Nullable<int> EstimationCellId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: PlotToEstimationCellMappingList.ColEstimationCellId.Name,
                            defaultValue: String.IsNullOrEmpty(PlotToEstimationCellMappingList.ColEstimationCellId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: PlotToEstimationCellMappingList.ColEstimationCellId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: PlotToEstimationCellMappingList.ColEstimationCellId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Estimation cell object (read-only)
                /// </summary>
                public EstimationCell EstimationCell
                {
                    get
                    {
                        return (EstimationCellId != null) ?
                             ((NfiEstaDB)Composite.Database).SNfiEsta.CEstimationCell[(int)EstimationCellId] : null;
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not PlotToEstimationCellMapping Type, return False
                    if (obj is not PlotToEstimationCellMapping)
                    {
                        return false;
                    }

                    return
                        Id == ((PlotToEstimationCellMapping)obj).Id;
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                #endregion Methods

            }


            /// <summary>
            /// List of mappings between inventory plot and estimation cell
            /// </summary>
            public class PlotToEstimationCellMappingList
                : AMappingTable
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema = NfiEstaSchema.Name;

                /// <summary>
                /// Table name
                /// </summary>
                public const string Name = "cm_plot2cell_mapping";

                /// <summary>
                /// Table alias
                /// </summary>
                public const string Alias = "cm_plot2cell_mapping";

                /// <summary>
                /// Description in national language
                /// </summary>
                public const string CaptionCs = "Seznam mapování inventarizačních ploch do výpočetních buněk";

                /// <summary>
                /// Description in English
                /// </summary>
                public const string CaptionEn = "List of mappings between inventory plot and estimation cell";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
                {
                    { "id", new ColumnMetadata()
                    {
                        Name = "id",
                        DbName = "id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = true,
                        NotNull = true,
                        DefaultValue = default(int).ToString(),
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ID",
                        HeaderTextEn = "ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 0
                    }
                    },
                    { "plot", new ColumnMetadata()
                    {
                        Name = "plot",
                        DbName = "plot",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PLOT",
                        HeaderTextEn = "PLOT",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 1
                    }
                    },
                    { "estimation_cell", new ColumnMetadata()
                    {
                        Name = "estimation_cell",
                        DbName = "estimation_cell",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ESTIMATION_CELL",
                        HeaderTextEn = "ESTIMATION_CELL",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 2
                    }
                    }
                };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column plot metadata
                /// </summary>
                public static readonly ColumnMetadata ColPlotId = Cols["plot"];

                /// <summary>
                /// Column estimation_cell metadata
                /// </summary>
                public static readonly ColumnMetadata ColEstimationCellId = Cols["estimation_cell"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public PlotToEstimationCellMappingList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public PlotToEstimationCellMappingList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// List of mappings between inventory plot and estimation cell(read-only)
                /// </summary>
                public List<PlotToEstimationCellMapping> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new PlotToEstimationCellMapping(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// Mapping between inventory plot and estimation cell from list by identifier (read-only)
                /// </summary>
                /// <param name="id">Mapping between inventory plot and estimation cell identifier</param>
                /// <returns>Mapping between inventory plot and estimation cell from list by identifier (null if not found)</returns>
                public PlotToEstimationCellMapping this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new PlotToEstimationCellMapping(composite: this, data: a))
                                .FirstOrDefault<PlotToEstimationCellMapping>();
                    }
                }

                #endregion Indexer


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public PlotToEstimationCellMappingList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new PlotToEstimationCellMappingList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new PlotToEstimationCellMappingList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                /// <summary>
                /// List of plots
                /// for selected estimation cells
                /// </summary>
                /// <param name="estimationCellId">Selected estimation cells identifiers</param>
                /// <returns>
                /// List of plots
                /// for selected estimation cells
                /// </returns>
                public PlotToEstimationCellMappingList Reduce(
                    List<int> estimationCellId = null)
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();

                    if ((estimationCellId != null) && estimationCellId.Count != 0)
                    {
                        rows = rows.Where(a => estimationCellId.Contains<int>(
                                value: (int)(a.Field<Nullable<int>>(columnName: ColEstimationCellId.Name) ?? 0)));
                    }

                    return
                        rows.Any() ?
                        new PlotToEstimationCellMappingList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new PlotToEstimationCellMappingList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                #endregion Methods

            }

        }
    }
}
