﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {
            // c_ldsity_object

            /// <summary>
            /// Local density object
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class TDLDsityObject(
                TDLDsityObjectList composite = null,
                DataRow data = null)
                    : ALookupTableEntry<TDLDsityObjectList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Local density object identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TDLDsityObjectList.ColId.Name,
                            defaultValue: Int32.Parse(TDLDsityObjectList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TDLDsityObjectList.ColId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Label of local density object in national language
                /// </summary>
                public override string LabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDLDsityObjectList.ColLabelCs.Name,
                            defaultValue: TDLDsityObjectList.ColLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDLDsityObjectList.ColLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Description of local density object in national language
                /// </summary>
                public override string DescriptionCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDLDsityObjectList.ColDescriptionCs.Name,
                            defaultValue: TDLDsityObjectList.ColDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDLDsityObjectList.ColDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Extended label of local density object in national language (read-only)
                /// </summary>
                public override string ExtendedLabelCs
                {
                    get
                    {
                        return $"{Id} - {LabelCs}";
                    }
                }


                /// <summary>
                /// Table name
                /// </summary>
                public string TableName
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDLDsityObjectList.ColTableName.Name,
                            defaultValue: TDLDsityObjectList.ColTableName.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDLDsityObjectList.ColTableName.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Superior local density object identifier
                /// </summary>
                public Nullable<int> UpperObjectId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TDLDsityObjectList.ColUpperObjectId.Name,
                            defaultValue: String.IsNullOrEmpty(TDLDsityObjectList.ColUpperObjectId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(TDLDsityObjectList.ColUpperObjectId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TDLDsityObjectList.ColUpperObjectId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Superior local density object (read-only)
                /// </summary>
                public TDLDsityObject UpperObject
                {
                    get
                    {
                        return (UpperObjectId != null) ?
                            ((NfiEstaDB)Composite.Database).STargetData.CLDsityObject[(int)UpperObjectId] : null;
                    }
                }


                /// <summary>
                /// Areal or population category identifier
                /// </summary>
                public Nullable<int> ArealOrPopulationId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TDLDsityObjectList.ColArealOrPopulationId.Name,
                            defaultValue: String.IsNullOrEmpty(TDLDsityObjectList.ColArealOrPopulationId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(TDLDsityObjectList.ColArealOrPopulationId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TDLDsityObjectList.ColArealOrPopulationId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Areal of population category object (read-only)
                /// </summary>
                public TDArealOrPopulation ArealOrPopulation
                {
                    get
                    {
                        return (ArealOrPopulationId != null) ?
                            ((NfiEstaDB)Composite.Database).STargetData.CArealOrPopulation[(int)ArealOrPopulationId] : null;
                    }
                }


                /// <summary>
                /// Column for upper object
                /// </summary>
                public string ColumnForUpperObject
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDLDsityObjectList.ColColumnForUpperObject.Name,
                            defaultValue: TDLDsityObjectList.ColColumnForUpperObject.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDLDsityObjectList.ColColumnForUpperObject.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Label of local density object in English
                /// </summary>
                public override string LabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDLDsityObjectList.ColLabelEn.Name,
                            defaultValue: TDLDsityObjectList.ColLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDLDsityObjectList.ColLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Description of local density object in English
                /// </summary>
                public override string DescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDLDsityObjectList.ColDescriptionEn.Name,
                            defaultValue: TDLDsityObjectList.ColDescriptionEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDLDsityObjectList.ColDescriptionEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Extended label of local density object in English (read-only)
                /// </summary>
                public override string ExtendedLabelEn
                {
                    get
                    {
                        return $"{Id} - {LabelEn}";
                    }
                }


                /// <summary>
                /// Filter
                /// </summary>
                public string Filter
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDLDsityObjectList.ColFilter.Name,
                            defaultValue: TDLDsityObjectList.ColFilter.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDLDsityObjectList.ColFilter.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Superior local density object identifier
                /// </summary>
                public Nullable<int> SuperiorLDsityObjectId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TDLDsityObjectList.ColSuperiorLDsityObjectId.Name,
                            defaultValue: String.IsNullOrEmpty(TDLDsityObjectList.ColSuperiorLDsityObjectId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(TDLDsityObjectList.ColSuperiorLDsityObjectId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TDLDsityObjectList.ColSuperiorLDsityObjectId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Superior local density object (read-only)
                /// </summary>
                public TDLDsityObject SuperiorLDsityObject
                {
                    get
                    {
                        return (SuperiorLDsityObjectId != null) ?
                            ((NfiEstaDB)Composite.Database).STargetData.CLDsityObject[(int)SuperiorLDsityObjectId] : null;
                    }
                }


                /// <summary>
                /// Classification rule
                /// </summary>
                public Nullable<bool> ClassificationRule
                {
                    get
                    {
                        return Functions.GetNBoolArg(
                            row: Data,
                            name: TDLDsityObjectList.ColClassificationRule.Name,
                            defaultValue: String.IsNullOrEmpty(TDLDsityObjectList.ColClassificationRule.DefaultValue) ?
                                (Nullable<bool>)null :
                                Boolean.Parse(TDLDsityObjectList.ColClassificationRule.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNBoolArg(
                            row: Data,
                            name: TDLDsityObjectList.ColClassificationRule.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Use negative local density contribution
                /// </summary>
                public Nullable<bool> UseNegative
                {
                    get
                    {
                        return Functions.GetNBoolArg(
                            row: Data,
                            name: TDLDsityObjectList.ColUseNegative.Name,
                            defaultValue: String.IsNullOrEmpty(TDLDsityObjectList.ColUseNegative.DefaultValue) ?
                                (Nullable<bool>)null :
                                Boolean.Parse(TDLDsityObjectList.ColUseNegative.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNBoolArg(
                            row: Data,
                            name: TDLDsityObjectList.ColUseNegative.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Group of local density objects identifier
                /// </summary>
                public Nullable<int> LDsityObjectGroupId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TDLDsityObjectList.ColLDsityObjectGroupId.Name,
                            defaultValue: String.IsNullOrEmpty(TDLDsityObjectList.ColLDsityObjectGroupId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(TDLDsityObjectList.ColLDsityObjectGroupId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TDLDsityObjectList.ColLDsityObjectGroupId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Group of local density objects (read-only)
                /// </summary>
                public TDLDsityObjectGroup LDsityObjectGroup
                {
                    get
                    {
                        return (LDsityObjectGroupId != null) ?
                            ((NfiEstaDB)Composite.Database).STargetData.CLDsityObjectGroup[(int)LDsityObjectGroupId] : null;
                    }
                }


                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not TDLDsityObject Type, return False
                    if (obj is not TDLDsityObject)
                    {
                        return false;
                    }

                    return
                        Id == ((TDLDsityObject)obj).Id;
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                #endregion Methods

            }


            /// <summary>
            /// List of local density objects
            /// </summary>
            public class TDLDsityObjectList
                : ALookupTable
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema = TDSchema.Name;

                /// <summary>
                /// Table name
                /// </summary>
                public const string Name = "c_ldsity_object";

                /// <summary>
                /// Table alias
                /// </summary>
                public const string Alias = "c_ldsity_object";

                /// <summary>
                /// Description in national language
                /// </summary>
                public const string CaptionCs = "Seznam objektů lokálních hustot ";

                /// <summary>
                /// Description in English
                /// </summary>
                public const string CaptionEn = "List of local density objects ";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
            {
                { "id", new ColumnMetadata()
                {
                    Name = "id",
                    DbName = "id",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = true,
                    NotNull = true,
                    DefaultValue = "0",
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "ID",
                    HeaderTextEn = "ID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 0
                }
                },
                { "label_cs", new ColumnMetadata()
                {
                    Name = "label_cs",
                    DbName = "label",
                    DataType = "System.String",
                    DbDataType = "varchar",
                    NewDataType = "varchar",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = String.Empty,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "LABEL_CS",
                    HeaderTextEn = "LABEL_CS",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 1
                }
                },
                { "description_cs", new ColumnMetadata()
                {
                    Name = "description_cs",
                    DbName = "description",
                    DataType = "System.String",
                    DbDataType = "text",
                    NewDataType = "text",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = String.Empty,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "DESCRIPTION_CS",
                    HeaderTextEn = "DESCRIPTION_CS",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 2
                }
                },
                { "table_name", new ColumnMetadata()
                {
                    Name = "table_name",
                    DbName = "table_name",
                    DataType = "System.String",
                    DbDataType = "varchar",
                    NewDataType = "varchar",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = String.Empty,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "TABLE_NAME",
                    HeaderTextEn = "TABLE_NAME",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 3
                }
                },
                { "upper_object_id", new ColumnMetadata()
                {
                    Name = "upper_object_id",
                    DbName = "upper_object",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "UPPER_OBJECT",
                    HeaderTextEn = "UPPER_OBJECT",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 4
                }
                },
                { "areal_or_population_id", new ColumnMetadata()
                {
                    Name = "areal_or_population_id",
                    DbName = "areal_or_population",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = "0",
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "AREAL_OR_POPULATION_ID",
                    HeaderTextEn = "AREAL_OR_POPULATION_ID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 5
                }
                },
                { "column4upper_object", new ColumnMetadata()
                {
                    Name = "column4upper_object",
                    DbName = "column4upper_object",
                    DataType = "System.String",
                    DbDataType = "varchar",
                    NewDataType = "varchar",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "COLUMN4UPPER_OBJECT",
                    HeaderTextEn = "COLUMN4UPPER_OBJECT",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 6
                }
                },
                { "label_en", new ColumnMetadata()
                {
                    Name = "label_en",
                    DbName = "label_en",
                    DataType = "System.String",
                    DbDataType = "varchar",
                    NewDataType = "varchar",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = String.Empty,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "LABEL_EN",
                    HeaderTextEn = "LABEL_EN",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 7
                }
                },
                { "description_en", new ColumnMetadata()
                {
                    Name = "description_en",
                    DbName = "description_en",
                    DataType = "System.String",
                    DbDataType = "text",
                    NewDataType = "text",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = String.Empty,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "DESCRIPTION_EN",
                    HeaderTextEn = "DESCRIPTION_EN",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 8
                }
                },
                { "filter", new ColumnMetadata()
                {
                    Name = "filter",
                    DbName = "filter",
                    DataType = "System.String",
                    DbDataType = "text",
                    NewDataType = "text",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "FILTER",
                    HeaderTextEn = "FILTER",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 9
                }
                },
                { "superior_ldsity_object_id", new ColumnMetadata()
                {
                    Name = "superior_ldsity_object_id",
                    DbName = "ldsity_object",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "SUPERIOR_LDSITY_OBJECT_ID",
                    HeaderTextEn = "SUPERIOR_LDSITY_OBJECT_ID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = false,
                    DisplayIndex = 10
                }
                },
                { "classification_rule", new ColumnMetadata()
                {
                    Name = "classification_rule",
                    DbName = "classification_rule",
                    DataType = "System.Boolean",
                    DbDataType = "bool",
                    NewDataType = "bool",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "CLASSIFICATION_RULE",
                    HeaderTextEn = "CLASSIFICATION_RULE",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = false,
                    DisplayIndex = 11
                }
                },
                { "use_negative", new ColumnMetadata()
                {
                    Name = "use_negative",
                    DbName = "use_negative",
                    DataType = "System.Boolean",
                    DbDataType = "bool",
                    NewDataType = "bool",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "USE_NEGATIVE",
                    HeaderTextEn = "USE_NEGATIVE",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = false,
                    DisplayIndex = 12
                }
                },
                { "ldsity_object_group_id", new ColumnMetadata()
                {
                    Name = "ldsity_object_group_id",
                    DbName = "ldsity_object_group",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = "0",
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "LDSITY_OBJECT_GROUP_ID",
                    HeaderTextEn = "LDSITY_OBJECT_GROUP_ID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = false,
                    DisplayIndex = 13
                }
                }
            };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColLabelCs = Cols["label_cs"];

                /// <summary>
                /// Column description_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColDescriptionCs = Cols["description_cs"];

                /// <summary>
                /// Column table_name metadata
                /// </summary>
                public static readonly ColumnMetadata ColTableName = Cols["table_name"];

                /// <summary>
                /// Column upper_object metadata
                /// </summary>
                public static readonly ColumnMetadata ColUpperObjectId = Cols["upper_object_id"];

                /// <summary>
                /// Column areal_or_population_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColArealOrPopulationId = Cols["areal_or_population_id"];

                /// <summary>
                ///
                /// Column column4upper_object metadata
                /// </summary>
                public static readonly ColumnMetadata ColColumnForUpperObject = Cols["column4upper_object"];

                /// <summary>
                /// Column label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColLabelEn = Cols["label_en"];

                /// <summary>
                /// Column description_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColDescriptionEn = Cols["description_en"];

                /// <summary>
                /// Column filter metadata
                /// </summary>
                public static readonly ColumnMetadata ColFilter = Cols["filter"];

                /// <summary>
                /// Column superior_ldsity_object_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColSuperiorLDsityObjectId = Cols["superior_ldsity_object_id"];

                /// <summary>
                /// Column classification_rule metadata
                /// </summary>
                public static readonly ColumnMetadata ColClassificationRule = Cols["classification_rule"];

                /// <summary>
                /// Column use_negative metadata
                /// </summary>
                public static readonly ColumnMetadata ColUseNegative = Cols["use_negative"];

                /// <summary>
                /// Column ldsity_object_group_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColLDsityObjectGroupId = Cols["ldsity_object_group_id"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="extended">Altogether category is included - true|false</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TDLDsityObjectList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    bool extended = false,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            extended: extended,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="extended">Altogether category is included - true|false</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TDLDsityObjectList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    bool extended = false,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            extended: extended,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// Last command text
                /// </summary>
                public string CommandText = String.Empty;

                /// <summary>
                /// List of local density objects (read-only)
                /// </summary>
                public List<TDLDsityObject> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new TDLDsityObject(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexers

                /// <summary>
                /// Local density object from list by identifier (read-only)
                /// </summary>
                /// <param name="id"> Local density object identifier</param>
                /// <returns> Local density object from list by identifier (null if not found)</returns>
                public TDLDsityObject this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new TDLDsityObject(composite: this, data: a))
                                .FirstOrDefault<TDLDsityObject>();
                    }
                }

                #endregion Indexers


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public TDLDsityObjectList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new TDLDsityObjectList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            extended: Extended,
                            loadingTime: LoadingTime) :
                        new TDLDsityObjectList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            extended: Extended,
                            loadingTime: null);
                }

                /// <summary>
                /// Update local density object in database
                /// </summary>
                /// <param name="item">Local density object</param>
                public void Update(TDLDsityObject item)
                {
                    if (item != null)
                    {
                        int id = item.Id;
                        string labelCs = item.LabelCs;
                        string descriptionCs = item.DescriptionCs;
                        string labelEn = item.LabelEn;
                        string descriptionEn = item.DescriptionEn;

                        if (Items.Where(a => a.Id == id).Any())
                        {
                            TDLDsityObject ldsityObject = Items.Where(a => a.Id == id).First();
                            ldsityObject.Id = id;
                            ldsityObject.LabelCs = labelCs;
                            ldsityObject.DescriptionCs = descriptionCs;
                            ldsityObject.LabelEn = labelEn;
                            ldsityObject.DescriptionEn = descriptionEn;
                            Data.AcceptChanges();

                            this.CommandText = String.Concat(
                            $"UPDATE {TDSchema.Name}.{Name}{Environment.NewLine}",
                            $"SET{Environment.NewLine}",
                            $"    {ColLabelCs.DbName} = {Functions.PrepStringArg(labelCs)},{Environment.NewLine}",
                            $"    {ColDescriptionCs.DbName} = {Functions.PrepStringArg(descriptionCs)},{Environment.NewLine}",
                            $"    {ColLabelEn.DbName} = {Functions.PrepStringArg(labelEn)},{Environment.NewLine}",
                            $"    {ColDescriptionEn.DbName} = {Functions.PrepStringArg(descriptionEn)}{Environment.NewLine}",
                            $"WHERE {ColId.DbName} = {Functions.PrepIntArg(id)};{Environment.NewLine}");
                            Database.Postgres.ExecuteNonQuery(this.CommandText);
                        }
                        else
                        {
                            throw new ArgumentException(
                                $"Local density object id = {id} doesn't exist.");
                        }
                    }
                    else
                    {
                        throw new ArgumentException(
                                $"Local density object is null.");
                    }
                }

                #endregion Methods

            }

        }
    }
}