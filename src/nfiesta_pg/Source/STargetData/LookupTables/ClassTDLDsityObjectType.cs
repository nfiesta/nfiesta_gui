﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {
            // c_ldsity_object_type

            /// <summary>
            /// Local density object type
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class TDLDsityObjectType(
                TDLDsityObjectTypeList composite = null,
                DataRow data = null)
                    : ALookupTableEntry<TDLDsityObjectTypeList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Local density object type identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TDLDsityObjectTypeList.ColId.Name,
                            defaultValue: Int32.Parse(s: TDLDsityObjectTypeList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TDLDsityObjectTypeList.ColId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Core or division local density object type (read-only)
                /// </summary>
                public TDLDsityObjectTypeEnum Value
                {
                    get
                    {
                        return (TDLDsityObjectTypeEnum)Id;
                    }
                }


                /// <summary>
                /// Label of local density object type in English
                /// </summary>
                public override string LabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDLDsityObjectTypeList.ColLabelEn.Name,
                            defaultValue: TDLDsityObjectTypeList.ColLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDLDsityObjectTypeList.ColLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Description of local density object type in English
                /// </summary>
                public override string DescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDLDsityObjectTypeList.ColDescriptionEn.Name,
                            defaultValue: TDLDsityObjectTypeList.ColDescriptionEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDLDsityObjectTypeList.ColDescriptionEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Extended label of local density object type in English (read-only)
                /// </summary>
                public override string ExtendedLabelEn
                {
                    get
                    {
                        return $"{Id} - {LabelEn}";
                    }
                }


                /// <summary>
                /// Label of local density object type in national language
                /// </summary>
                public override string LabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDLDsityObjectTypeList.ColLabelCs.Name,
                            defaultValue: TDLDsityObjectTypeList.ColLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDLDsityObjectTypeList.ColLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Description of local density object type in national language
                /// </summary>
                public override string DescriptionCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDLDsityObjectTypeList.ColDescriptionCs.Name,
                            defaultValue: TDLDsityObjectTypeList.ColDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDLDsityObjectTypeList.ColDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Extended label of local density object type in national language (read-only)
                /// </summary>
                public override string ExtendedLabelCs
                {
                    get
                    {
                        return $"{Id} - {LabelCs}";
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not TDLDsityObjectType Type, return False
                    if (obj is not TDLDsityObjectType)
                    {
                        return false;
                    }

                    return
                        Id == ((TDLDsityObjectType)obj).Id;
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                #endregion Methods

            }


            /// <summary>
            /// List of local density object types
            /// </summary>
            public class TDLDsityObjectTypeList
                : ALookupTable
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema = TDSchema.Name;

                /// <summary>
                /// Table name
                /// </summary>
                public const string Name = "c_ldsity_object_type";

                /// <summary>
                /// Table alias
                /// </summary>
                public const string Alias = "c_ldsity_object_type";

                /// <summary>
                /// Description in national language
                /// </summary>
                public const string CaptionCs = "Seznam typů objektů lokálních hustot ";

                /// <summary>
                /// Description in English
                /// </summary>
                public const string CaptionEn = "List of local density object types ";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
            {
                { "id", new ColumnMetadata()
                {
                    Name = "id",
                    DbName = "id",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = true,
                    NotNull = true,
                    DefaultValue = "0",
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "ID",
                    HeaderTextEn = "ID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 0
                }
                },
                { "label_en", new ColumnMetadata()
                {
                    Name = "label_en",
                    DbName = "label",
                    DataType = "System.String",
                    DbDataType = "varchar",
                    NewDataType = "varchar",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = String.Empty,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "LABEL_EN",
                    HeaderTextEn = "LABEL_EN",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 1
                }
                },
                { "description_en", new ColumnMetadata()
                {
                    Name = "description_en",
                    DbName = "description",
                    DataType = "System.String",
                    DbDataType = "text",
                    NewDataType = "text",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = String.Empty,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "DESCRIPTION_EN",
                    HeaderTextEn = "DESCRIPTION_EN",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 2
                }
                },
                { "label_cs", new ColumnMetadata()
                {
                    Name = "label_cs",
                    DbName = String.Empty,
                    DataType = "System.String",
                    DbDataType = "varchar",
                    NewDataType = "varchar",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = String.Empty,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "LABEL_CS",
                    HeaderTextEn = "LABEL_CS",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = false,
                    DisplayIndex = 3
                }
                },
                { "description_cs", new ColumnMetadata()
                {
                    Name = "description_cs",
                    DbName = String.Empty,
                    DataType = "System.String",
                    DbDataType = "text",
                    NewDataType = "text",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = String.Empty,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "DESCRIPTION_CS",
                    HeaderTextEn = "DESCRIPTION_CS",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = false,
                    DisplayIndex = 4
                }
                }
            };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColLabelEn = Cols["label_en"];

                /// <summary>
                /// Column description_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColDescriptionEn = Cols["description_en"];

                /// <summary>
                /// Column label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColLabelCs = Cols["label_cs"];

                /// <summary>
                /// Column description_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColDescriptionCs = Cols["description_cs"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                /// <summary>
                /// Set national label and description in static lookup table c_ldsity_object_type
                /// </summary>
                /// <param name="dt">Data table object for lookup table c_ldsity_object_type</param>
                public static void FixNationalLabels(DataTable dt)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        Functions.SetStringArg(
                            row: row,
                            name: ColLabelCs.Name,
                            val: Functions.GetStringArg(
                                row: row,
                                name: ColLabelEn.Name,
                                defaultValue: ColLabelEn.DefaultValue));

                        Functions.SetStringArg(
                            row: row,
                            name: ColDescriptionCs.Name,
                            val: Functions.GetStringArg(
                                row: row,
                                name: ColDescriptionEn.Name,
                                defaultValue: ColDescriptionEn.DefaultValue));
                    }
                }

                #endregion Static Methods


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="extended">Altogether category is included - true|false</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TDLDsityObjectTypeList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    bool extended = false,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            extended: extended,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="extended">Altogether category is included - true|false</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TDLDsityObjectTypeList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    bool extended = false,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            extended: extended,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// Last command text
                /// </summary>
                public string CommandText = String.Empty;

                /// <summary>
                /// List of local density object types (read-only)
                /// </summary>
                public List<TDLDsityObjectType> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new TDLDsityObjectType(composite: this, data: a))];
                    }
                }

                /// <summary>
                /// Local density object type: Core (ID = 100) (read-only)
                /// </summary>
                public TDLDsityObjectType Core
                {
                    get
                    {
                        return this[(int)TDLDsityObjectTypeEnum.Core];
                    }
                }

                /// <summary>
                /// Local density object type: Core (ID = 200) (read-only)
                /// </summary>
                public TDLDsityObjectType Division
                {
                    get
                    {
                        return this[(int)TDLDsityObjectTypeEnum.Division];
                    }
                }

                #endregion Properties


                #region Indexers

                /// <summary>
                /// Local density object type from list by identifier (read-only)
                /// </summary>
                /// <param name="id"> Local density object type identifier</param>
                /// <returns> Local density object type from list by identifier (null if not found)</returns>
                public TDLDsityObjectType this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new TDLDsityObjectType(composite: this, data: a))
                                .FirstOrDefault<TDLDsityObjectType>();
                    }
                }

                #endregion Indexers


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public TDLDsityObjectTypeList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new TDLDsityObjectTypeList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            extended: Extended,
                            loadingTime: LoadingTime) :
                        new TDLDsityObjectTypeList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            extended: Extended,
                            loadingTime: null);
                }

                /// <summary>
                /// Loads data from database table
                /// (if it was not done before)
                /// </summary>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="extended">Altogether category is included - true|false</param>
                public new void Load(
                    string condition = null,
                    Nullable<int> limit = null,
                    bool extended = false)
                {
                    if (!Loaded)
                    {
                        ReLoad(
                            condition: condition,
                            limit: limit,
                            extended: extended);
                    }
                }

                /// <summary>
                /// Loads data from database table
                /// </summary>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="extended">Altogether category is included - true|false</param>
                public new void ReLoad(
                    string condition = null,
                    Nullable<int> limit = null,
                    bool extended = false)
                {
                    base.ReLoad(
                        condition: condition,
                        limit: limit,
                        extended: extended);
                    FixNationalLabels(dt: Data);
                }

                /// <summary>
                /// Update local density object type in database
                /// </summary>
                /// <param name="item">Local density object type</param>
                public void Update(TDLDsityObjectType item)
                {
                    if (item != null)
                    {
                        int id = item.Id;
                        string labelCs = item.LabelCs;
                        string descriptionCs = item.DescriptionCs;
                        string labelEn = item.LabelEn;
                        string descriptionEn = item.DescriptionEn;

                        if (Items.Where(a => a.Id == id).Any())
                        {
                            TDLDsityObjectType ldsityObjectType = Items.Where(a => a.Id == id).First();
                            ldsityObjectType.Id = id;
                            ldsityObjectType.LabelCs = labelCs;
                            ldsityObjectType.DescriptionCs = descriptionCs;
                            ldsityObjectType.LabelEn = labelEn;
                            ldsityObjectType.DescriptionEn = descriptionEn;
                            Data.AcceptChanges();

                            this.CommandText = String.Concat(
                            $"UPDATE {TDSchema.Name}.{Name}{Environment.NewLine}",
                            $"SET{Environment.NewLine}",
                            $"    {ColLabelEn.DbName} = {Functions.PrepStringArg(labelEn)},{Environment.NewLine}",
                            $"    {ColDescriptionEn.DbName} = {Functions.PrepStringArg(descriptionEn)},{Environment.NewLine}",
                            $"WHERE {ColId.DbName} = {Functions.PrepIntArg(item.Id)};{Environment.NewLine}");
                            Database.Postgres.ExecuteNonQuery(this.CommandText);
                        }
                        else
                        {
                            throw new ArgumentException(
                                $"Local density object type id = {id} doesn't exist.");
                        }
                    }
                    else
                    {
                        throw new ArgumentException(
                                $"Local density object type is null.");
                    }
                }

                #endregion Methods

            }


            /// <summary>
            /// Core or division local density object type
            /// </summary>
            public enum TDLDsityObjectTypeEnum
            {

                /// <summary>
                /// Local density object type is not specified
                /// </summary>
                Unknown = 0,

                /// <summary>
                /// Core local density object type
                /// </summary>
                Core = 100,

                /// <summary>
                /// Division local density object type
                /// </summary>
                Division = 200

            }

        }
    }
}

