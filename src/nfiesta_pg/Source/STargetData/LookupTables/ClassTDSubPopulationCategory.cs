﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {
            // c_sub_population_category

            /// <summary>
            /// Subpopulation category
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class TDSubPopulationCategory(
                TDSubPopulationCategoryList composite = null,
                DataRow data = null)
                    : ALookupTableEntry<TDSubPopulationCategoryList>(composite: composite, data: data),
                      IArealOrPopulationCategory<TDSubPopulationCategoryList>
            {

                #region Derived Properties

                /// <summary>
                /// Subpopulation category identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TDSubPopulationCategoryList.ColId.Name,
                            defaultValue: Int32.Parse(s: TDSubPopulationCategoryList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TDSubPopulationCategoryList.ColId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Label of subpopulation category in national language
                /// </summary>
                public override string LabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDSubPopulationCategoryList.ColLabelCs.Name,
                            defaultValue: TDSubPopulationCategoryList.ColLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDSubPopulationCategoryList.ColLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Description of subpopulation category in national language
                /// </summary>
                public override string DescriptionCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDSubPopulationCategoryList.ColDescriptionCs.Name,
                            defaultValue: TDSubPopulationCategoryList.ColDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDSubPopulationCategoryList.ColDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Extended label of subpopulation category in national language (read-only)
                /// </summary>
                public override string ExtendedLabelCs
                {
                    get
                    {
                        return $"{LabelCs} [{Id}]";
                    }
                }


                /// <summary>
                /// Subpopulation identifier
                /// </summary>
                public Nullable<int> SubPopulationId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TDSubPopulationCategoryList.ColSubPopulationId.Name,
                            defaultValue: String.IsNullOrEmpty(value: TDSubPopulationCategoryList.ColSubPopulationId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TDSubPopulationCategoryList.ColSubPopulationId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TDSubPopulationCategoryList.ColSubPopulationId.Name,
                            val: value);
                    }
                }

                /// <summary>
                ///  Subpopulation object (read-only)
                /// </summary>
                public TDSubPopulation SubPopulation
                {
                    get
                    {
                        return (SubPopulationId != null) ?
                            ((NfiEstaDB)Composite.Database).STargetData.CSubPopulation[(int)SubPopulationId] : null;
                    }
                }


                /// <summary>
                /// Label of subpopulation category in English
                /// </summary>
                public override string LabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDSubPopulationCategoryList.ColLabelEn.Name,
                            defaultValue: TDSubPopulationCategoryList.ColLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDSubPopulationCategoryList.ColLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Description of subpopulation category in English
                /// </summary>
                public override string DescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDSubPopulationCategoryList.ColDescriptionEn.Name,
                            defaultValue: TDSubPopulationCategoryList.ColDescriptionEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDSubPopulationCategoryList.ColDescriptionEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Extended label of subpopulation category in English (read-only)
                /// </summary>
                public override string ExtendedLabelEn
                {
                    get
                    {
                        return $"{LabelEn} [{Id}]";
                    }
                }


                /// <summary>
                /// Target variable identifier
                /// </summary>
                public Nullable<int> TargetVariableId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TDSubPopulationCategoryList.ColTargetVariableId.Name,
                            defaultValue: String.IsNullOrEmpty(value: TDSubPopulationCategoryList.ColTargetVariableId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TDSubPopulationCategoryList.ColTargetVariableId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TDSubPopulationCategoryList.ColTargetVariableId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Target variable (read-only)
                /// </summary>
                public TDTargetVariable TargetVariable
                {
                    get
                    {
                        return (TargetVariableId != null) ?
                            ((NfiEstaDB)Composite.Database).STargetData.CTargetVariable[(int)TargetVariableId] : null;
                    }
                }


                /// <summary>
                /// Local density identifier
                /// </summary>
                public Nullable<int> LDsityId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TDSubPopulationCategoryList.ColLDsityId.Name,
                            defaultValue: String.IsNullOrEmpty(value: TDSubPopulationCategoryList.ColLDsityId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TDSubPopulationCategoryList.ColLDsityId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TDSubPopulationCategoryList.ColLDsityId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Local density (read-only)
                /// </summary>
                public TDLDsity LDsity
                {
                    get
                    {
                        return (LDsityId != null) ?
                            ((NfiEstaDB)Composite.Database).STargetData.CLDsity[(int)LDsityId] : null;
                    }
                }



                /// <summary>
                /// Local density object identifier
                /// </summary>
                public Nullable<int> LDsityObjectId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TDSubPopulationCategoryList.ColLDsityObjectId.Name,
                            defaultValue: String.IsNullOrEmpty(value: TDSubPopulationCategoryList.ColLDsityObjectId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TDSubPopulationCategoryList.ColLDsityObjectId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TDSubPopulationCategoryList.ColLDsityObjectId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Local density object (read-only)
                /// </summary>
                public TDLDsityObject LDsityObject
                {
                    get
                    {
                        return (LDsityObjectId != null) ?
                            ((NfiEstaDB)Composite.Database).STargetData.CLDsityObject[(int)LDsityObjectId] : null;
                    }
                }

                /// <summary>
                /// Label of local density object in national language
                /// </summary>
                public string LDsityObjectLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDSubPopulationCategoryList.ColLDsityObjectLabelCs.Name,
                            defaultValue: TDSubPopulationCategoryList.ColLDsityObjectLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDSubPopulationCategoryList.ColLDsityObjectLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Description of local density object in national language
                /// </summary>
                public string LDsityObjectDescriptionCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDSubPopulationCategoryList.ColLDsityObjectDescriptionCs.Name,
                            defaultValue: TDSubPopulationCategoryList.ColLDsityObjectDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDSubPopulationCategoryList.ColLDsityObjectDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Extended label of local density object in national language (read-only)
                /// </summary>
                public string ExtendedLDsityObjectLabelCs
                {
                    get
                    {
                        return $"{LDsityObjectId} - {LDsityObjectLabelCs}";
                    }
                }

                /// <summary>
                /// Label of local density object in English
                /// </summary>
                public string LDsityObjectLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDSubPopulationCategoryList.ColLDsityObjectLabelEn.Name,
                            defaultValue: TDSubPopulationCategoryList.ColLDsityObjectLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDSubPopulationCategoryList.ColLDsityObjectLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Description of local density object in English
                /// </summary>
                public string LDsityObjectDescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDSubPopulationCategoryList.ColLDsityObjectDescriptionEn.Name,
                            defaultValue: TDSubPopulationCategoryList.ColLDsityObjectDescriptionEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDSubPopulationCategoryList.ColLDsityObjectDescriptionEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Extended label of local density object in English (read-only)
                /// </summary>
                public string ExtendedLDsityObjectLabelEn
                {
                    get
                    {
                        return $"{LDsityObjectId} - {LDsityObjectLabelEn}";
                    }
                }


                /// <summary>
                /// Negative local density contribution
                /// </summary>
                public Nullable<bool> UseNegative
                {
                    get
                    {
                        return Functions.GetNBoolArg(
                            row: Data,
                            name: TDSubPopulationCategoryList.ColUseNegative.Name,
                            defaultValue: String.IsNullOrEmpty(value: TDSubPopulationCategoryList.ColUseNegative.DefaultValue) ?
                                (Nullable<bool>)null :
                                Boolean.Parse(value: TDSubPopulationCategoryList.ColUseNegative.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNBoolArg(
                            row: Data,
                            name: TDSubPopulationCategoryList.ColUseNegative.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Restriction
                /// </summary>
                public Nullable<bool> Restriction
                {
                    get
                    {
                        return Functions.GetNBoolArg(
                            row: Data,
                            name: TDSubPopulationCategoryList.ColRestriction.Name,
                            defaultValue: String.IsNullOrEmpty(value: TDSubPopulationCategoryList.ColRestriction.DefaultValue) ?
                                (Nullable<bool>)null :
                                Boolean.Parse(value: TDSubPopulationCategoryList.ColRestriction.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNBoolArg(
                            row: Data,
                            name: TDSubPopulationCategoryList.ColRestriction.Name,
                            val: value);
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not TDSubPopulationCategory Type, return False
                    if (obj is not TDSubPopulationCategory)
                    {
                        return false;
                    }

                    return
                        Id == ((TDSubPopulationCategory)obj).Id;
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                #endregion Methods

            }


            /// <summary>
            /// List of subpopulation categories
            /// </summary>
            public class TDSubPopulationCategoryList
                : ALookupTable, IArealOrPopulationCategoryTable
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema = TDSchema.Name;

                /// <summary>
                /// Table name
                /// </summary>
                public const string Name = "c_sub_population_category";

                /// <summary>
                /// Table alias
                /// </summary>
                public const string Alias = "c_sub_population_category";

                /// <summary>
                /// Description in national language
                /// </summary>
                public const string CaptionCs = "Seznam kategorií subpopulací";

                /// <summary>
                /// Description in English
                /// </summary>
                public const string CaptionEn = "List of subpopulation categories";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
            {
                { "id", new ColumnMetadata()
                {
                    Name = "id",
                    DbName = "id",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = true,
                    NotNull = true,
                    DefaultValue = "0",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "ID",
                    HeaderTextEn = "ID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 0
                }
                },
                { "label_cs", new ColumnMetadata()
                {
                    Name = "label_cs",
                    DbName = "label",
                    DataType = "System.String",
                    DbDataType = "varchar",
                    NewDataType = "varchar",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = String.Empty,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "LABEL_CS",
                    HeaderTextEn = "LABEL_CS",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 1
                }
                },
                { "description_cs", new ColumnMetadata()
                {
                    Name = "description_cs",
                    DbName = "description",
                    DataType = "System.String",
                    DbDataType = "text",
                    NewDataType = "text",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = String.Empty,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "DESCRIPTION_CS",
                    HeaderTextEn = "DESCRIPTION_CS",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 2
                }
                },
                { "sub_population_id", new ColumnMetadata()
                {
                    Name = "sub_population_id",
                    DbName = "sub_population",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = "0",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "SUB_POPULATION_ID",
                    HeaderTextEn = "SUB_POPULATION_ID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 3
                }
                },
                { "label_en", new ColumnMetadata()
                {
                    Name = "label_en",
                    DbName = "label_en",
                    DataType = "System.String",
                    DbDataType = "varchar",
                    NewDataType = "varchar",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = String.Empty,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "LABEL_EN",
                    HeaderTextEn = "LABEL_EN",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 4
                }
                },
                { "description_en", new ColumnMetadata()
                {
                    Name = "description_en",
                    DbName = "description_en",
                    DataType = "System.String",
                    DbDataType = "text",
                    NewDataType = "text",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = String.Empty,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "DESCRIPTION_EN",
                    HeaderTextEn = "DESCRIPTION_EN",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 5
                }
                },
                { "target_variable_id", new ColumnMetadata()
                {
                    Name = "target_variable_id",
                    DbName = "target_variable",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = null,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "TARGET_VARIABLE_ID",
                    HeaderTextEn = "TARGET_VARIABLE_ID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = false,
                    DisplayIndex = 6
                }
                },
                { "ldsity_id", new ColumnMetadata()
                {
                    Name = "ldsity_id",
                    DbName = "ldsity",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = null,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "LDSITY_ID",
                    HeaderTextEn = "LDSITY_ID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = false,
                    DisplayIndex = 7
                }
                },
                { "ldsity_object_id", new ColumnMetadata()
                {
                    Name = "ldsity_object_id",
                    DbName = "ldsity_object",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = null,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "LDSITY_OBJECT_ID",
                    HeaderTextEn = "LDSITY_OBJECT_ID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = false,
                    DisplayIndex = 8
                }
                },
                { "ldsity_object_label_cs", new ColumnMetadata()
                {
                    Name = "ldsity_object_label_cs",
                    DbName = "object_label",
                    DataType = "System.String",
                    DbDataType = "varchar",
                    NewDataType = "varchar",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = String.Empty,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "LDSITY_OBJECT_LABEL_CS",
                    HeaderTextEn = "LDSITY_OBJECT_LABEL_CS",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = false,
                    DisplayIndex = 9
                }
                },
                { "ldsity_object_description_cs", new ColumnMetadata()
                {
                    Name = "ldsity_object_description_cs",
                    DbName = "object_description",
                    DataType = "System.String",
                    DbDataType = "text",
                    NewDataType = "text",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = String.Empty,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "LDSITY_OBJECT_DESCRIPTION_CS",
                    HeaderTextEn = "LDSITY_OBJECT_DESCRIPTION_CS",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = false,
                    DisplayIndex = 10
                }
                },
                { "ldsity_object_label_en", new ColumnMetadata()
                {
                    Name = "ldsity_object_label_en",
                    DbName = "object_label_en",
                    DataType = "System.String",
                    DbDataType = "varchar",
                    NewDataType = "varchar",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = String.Empty,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "LDSITY_OBJECT_LABEL_EN",
                    HeaderTextEn = "LDSITY_OBJECT_LABEL_EN",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = false,
                    DisplayIndex = 11
                }
                },
                { "ldsity_object_description_en", new ColumnMetadata()
                {
                    Name = "ldsity_object_description_en",
                    DbName = "object_description_en",
                    DataType = "System.String",
                    DbDataType = "text",
                    NewDataType = "text",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = String.Empty,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "LDSITY_OBJECT_DESCRIPTION_EN",
                    HeaderTextEn = "LDSITY_OBJECT_DESCRIPTION_EN",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = false,
                    DisplayIndex = 12
                }
                },
                { "use_negative", new ColumnMetadata()
                {
                    Name = "use_negative",
                    DbName = "use_negative",
                    DataType = "System.Boolean",
                    DbDataType = "bool",
                    NewDataType = "bool",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = null,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "USE_NEGATIVE",
                    HeaderTextEn = "USE_NEGATIVE",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = false,
                    DisplayIndex = 13
                }
                },
                { "restriction", new ColumnMetadata()
                {
                    Name = "restriction",
                    DbName = "restriction",
                    DataType = "System.Boolean",
                    DbDataType = "bool",
                    NewDataType = "bool",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = null,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "RESTRICTION",
                    HeaderTextEn = "RESTRICTION",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = false,
                    DisplayIndex = 14
                }
                }
            };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColLabelCs = Cols["label_cs"];

                /// <summary>
                /// Column description_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColDescriptionCs = Cols["description_cs"];

                /// <summary>
                /// Column sub_population_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColSubPopulationId = Cols["sub_population_id"];

                /// <summary>
                /// Column label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColLabelEn = Cols["label_en"];

                /// <summary>
                /// Column description_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColDescriptionEn = Cols["description_en"];

                /// <summary>
                /// Column target_variable_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColTargetVariableId = Cols["target_variable_id"];

                /// <summary>
                /// Column ldsity_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColLDsityId = Cols["ldsity_id"];

                /// <summary>
                /// Column ldsity_object_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColLDsityObjectId = Cols["ldsity_object_id"];

                /// <summary>
                /// Column ldsity_object_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColLDsityObjectLabelCs = Cols["ldsity_object_label_cs"];

                /// <summary>
                /// Column ldsity_object_description_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColLDsityObjectDescriptionCs = Cols["ldsity_object_description_cs"];

                /// <summary>
                /// Column ldsity_object_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColLDsityObjectLabelEn = Cols["ldsity_object_label_en"];

                /// <summary>
                /// Column ldsity_object_description_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColLDsityObjectDescriptionEn = Cols["ldsity_object_description_en"];

                /// <summary>
                /// Column use_negative metadata
                /// </summary>
                public static readonly ColumnMetadata ColUseNegative = Cols["use_negative"];

                /// <summary>
                /// Column restriction metadata
                /// </summary>
                public static readonly ColumnMetadata ColRestriction = Cols["restriction"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="extended">Altogether category is included - true|false</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TDSubPopulationCategoryList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    bool extended = false,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            extended: extended,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="extended">Altogether category is included - true|false</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TDSubPopulationCategoryList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    bool extended = false,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            extended: extended,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// Last command text
                /// </summary>
                public string CommandText = String.Empty;

                /// <summary>
                /// List of subpopulation categories (read-only)
                /// </summary>
                public List<TDSubPopulationCategory> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new TDSubPopulationCategory(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexers

                /// <summary>
                /// Subpopulation category from list by identifier (read-only)
                /// </summary>
                /// <param name="id">Subpopulation category identifier</param>
                /// <returns>Subpopulation category from list by identifier (null if not found)</returns>
                public TDSubPopulationCategory this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new TDSubPopulationCategory(composite: this, data: a))
                                .FirstOrDefault<TDSubPopulationCategory>();
                    }
                }

                #endregion Indexers


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public TDSubPopulationCategoryList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new TDSubPopulationCategoryList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            extended: Extended,
                            loadingTime: LoadingTime) :
                        new TDSubPopulationCategoryList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            extended: Extended,
                            loadingTime: null);
                }

                /// <summary>
                /// Update subpopulation category in database
                /// </summary>
                /// <param name="item">Subpopulation category</param>
                public void Update(TDSubPopulationCategory item)
                {
                    if (item != null)
                    {
                        int id = item.Id;
                        string labelCs = item.LabelCs;
                        string descriptionCs = item.DescriptionCs;
                        string labelEn = item.LabelEn;
                        string descriptionEn = item.DescriptionEn;

                        if (Items.Where(a => a.Id == id).Any())
                        {
                            TDSubPopulationCategory subpopulationCategory = Items.Where(a => a.Id == id).First();
                            subpopulationCategory.Id = id;
                            subpopulationCategory.LabelCs = labelCs;
                            subpopulationCategory.DescriptionCs = descriptionCs;
                            subpopulationCategory.LabelEn = labelEn;
                            subpopulationCategory.DescriptionEn = descriptionEn;
                            Data.AcceptChanges();

                            this.CommandText = String.Concat(
                            $"UPDATE {TDSchema.Name}.{Name}{Environment.NewLine}",
                            $"SET{Environment.NewLine}",
                            $"    {ColLabelCs.DbName} = {Functions.PrepStringArg(labelCs)},{Environment.NewLine}",
                            $"    {ColDescriptionCs.DbName} = {Functions.PrepStringArg(descriptionCs)},{Environment.NewLine}",
                            $"    {ColLabelEn.DbName} = {Functions.PrepStringArg(labelEn)},{Environment.NewLine}",
                            $"    {ColDescriptionEn.DbName} = {Functions.PrepStringArg(descriptionEn)}{Environment.NewLine}",
                            $"WHERE {ColId.DbName} = {Functions.PrepIntArg(id)};{Environment.NewLine}");
                            Database.Postgres.ExecuteNonQuery(this.CommandText);
                        }
                        else
                        {
                            throw new ArgumentException(
                                $"Subpopulation category id = {id} doesn't exist.");
                        }
                    }
                    else
                    {
                        throw new ArgumentException(
                                $"Subpopulation category is null.");
                    }
                }

                #endregion Methods

            }

        }
    }
}