﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {
            // c_target_variable

            /// <summary>
            /// Target variable
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class TDTargetVariable(
                TDTargetVariableList composite = null,
                DataRow data = null)
                    : ALookupTableEntry<TDTargetVariableList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Target variable identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TDTargetVariableList.ColId.Name,
                            defaultValue: Int32.Parse(s: TDTargetVariableList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TDTargetVariableList.ColId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Label of target variable in national language
                /// </summary>
                public override string LabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDTargetVariableList.ColLabelCs.Name,
                            defaultValue: TDTargetVariableList.ColLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDTargetVariableList.ColLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Description of target variable in national language
                /// </summary>
                public override string DescriptionCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDTargetVariableList.ColDescriptionCs.Name,
                            defaultValue: TDTargetVariableList.ColDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDTargetVariableList.ColDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Extended label of target variable in national language (read-only)
                /// </summary>
                public override string ExtendedLabelCs
                {
                    get
                    {
                        return $"{Id} - {LabelCs}";
                    }
                }


                /// <summary>
                /// State or change category identifier
                /// </summary>
                public int StateOrChangeId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TDTargetVariableList.ColStateOrChangeId.Name,
                            defaultValue: Int32.Parse(s: TDTargetVariableList.ColStateOrChangeId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TDTargetVariableList.ColStateOrChangeId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// State or change category object (read-only)
                /// </summary>
                public TDStateOrChange StateOrChange
                {
                    get
                    {
                        return ((NfiEstaDB)Composite.Database).STargetData.CStateOrChange[StateOrChangeId];
                    }
                }

                /// <summary>
                /// State or change category object (read-only)
                /// </summary>
                public TDStateOrChangeEnum StateOrChangeValue
                {
                    get
                    {
                        return (TDStateOrChangeEnum)StateOrChangeId;
                    }
                }


                /// <summary>
                /// Label of target variable in English
                /// </summary>
                public override string LabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDTargetVariableList.ColLabelEn.Name,
                            defaultValue: TDTargetVariableList.ColLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDTargetVariableList.ColLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Description of target variable in English
                /// </summary>
                public override string DescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDTargetVariableList.ColDescriptionEn.Name,
                            defaultValue: TDTargetVariableList.ColDescriptionEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDTargetVariableList.ColDescriptionEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Extended label of target variable in English (read-only)
                /// </summary>
                public override string ExtendedLabelEn
                {
                    get
                    {
                        return $"{Id} - {LabelEn}";
                    }
                }


                /// <summary>
                /// Group of local density objects identifer
                /// </summary>
                public Nullable<int> LDsityObjectGroupId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TDTargetVariableList.ColLDsityObjectGroupId.Name,
                            defaultValue: String.IsNullOrEmpty(value: TDTargetVariableList.ColLDsityObjectGroupId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TDTargetVariableList.ColLDsityObjectGroupId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TDTargetVariableList.ColLDsityObjectGroupId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Group of local density objects (read-only)
                /// </summary>
                public TDLDsityObjectGroup LDsityObjectGroup
                {
                    get
                    {
                        return (LDsityObjectGroupId != null) ?
                            ((NfiEstaDB)Composite.Database).STargetData.CLDsityObjectGroup[(int)LDsityObjectGroupId] : null;
                    }
                }


                /// <summary>
                /// Areal or population identifier
                /// </summary>
                public Nullable<int> ArealOrPopulationId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TDTargetVariableList.ColArealOrPopulationId.Name,
                            defaultValue: String.IsNullOrEmpty(value: TDTargetVariableList.ColArealOrPopulationId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TDTargetVariableList.ColArealOrPopulationId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TDTargetVariableList.ColArealOrPopulationId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Areal or population value (read-only)
                /// </summary>
                public TDArealOrPopulationEnum ArealOrPopulationValue
                {
                    get
                    {
                        return (TDArealOrPopulationEnum)(ArealOrPopulationId ?? 0);
                    }
                }

                /// <summary>
                /// Areal or population object (read-only)
                /// </summary>
                public TDArealOrPopulation ArealOrPopulation
                {
                    get
                    {
                        return (ArealOrPopulationId != null) ?
                            ((NfiEstaDB)Composite.Database).STargetData.CArealOrPopulation[(int)ArealOrPopulationId] : null;
                    }
                }

                /// <summary>
                /// Core target variable identifier
                /// </summary>
                public Nullable<bool> Core
                {
                    get
                    {
                        return Functions.GetNBoolArg(
                            row: Data,
                            name: TDTargetVariableList.ColCore.Name,
                            defaultValue: String.IsNullOrEmpty(value: TDTargetVariableList.ColCore.DefaultValue) ?
                                (Nullable<bool>)null :
                                Boolean.Parse(value: TDTargetVariableList.ColCore.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNBoolArg(
                            row: Data,
                            name: TDTargetVariableList.ColCore.Name,
                            val: value);
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not TDTargetVariable Type, return False
                    if (obj is not TDTargetVariable)
                    {
                        return false;
                    }

                    return
                        Id == ((TDTargetVariable)obj).Id;
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                #endregion Methods

            }


            /// <summary>
            /// List of target variables
            /// </summary>
            public class TDTargetVariableList
                : ALookupTable
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema = TDSchema.Name;

                /// <summary>
                /// Table name
                /// </summary>
                public const string Name = "c_target_variable";

                /// <summary>
                /// Table alias
                /// </summary>
                public const string Alias = "c_target_variable";

                /// <summary>
                /// Description in national language
                /// </summary>
                public const string CaptionCs = "Seznam cílových proměnných";

                /// <summary>
                /// Description in English
                /// </summary>
                public const string CaptionEn = "List of target variables";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
            {
                { "id", new ColumnMetadata()
                {
                    Name = "id",
                    DbName = "id",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = true,
                    NotNull = true,
                    DefaultValue = "0",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "ID",
                    HeaderTextEn = "ID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 0
                }
                },
                { "label_cs", new ColumnMetadata()
                {
                    Name = "label_cs",
                    DbName = "label",
                    DataType = "System.String",
                    DbDataType = "varchar",
                    NewDataType = "varchar",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = String.Empty,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "LABEL_CS",
                    HeaderTextEn = "LABEL_CS",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 1
                }
                },
                { "description_cs", new ColumnMetadata()
                {
                    Name = "description_cs",
                    DbName = "description",
                    DataType = "System.String",
                    DbDataType = "text",
                    NewDataType = "text",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = String.Empty,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "DESCRIPTION_CS",
                    HeaderTextEn = "DESCRIPTION_CS",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 2
                }
                },
                { "state_or_change_id", new ColumnMetadata()
                {
                    Name = "state_or_change_id",
                    DbName = "state_or_change",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = "0",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "STATE_OR_CHANGE_ID",
                    HeaderTextEn = "STATE_OR_CHANGE_ID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 3
                }
                },
                { "label_en", new ColumnMetadata()
                {
                    Name = "label_en",
                    DbName = "label_en",
                    DataType = "System.String",
                    DbDataType = "varchar",
                    NewDataType = "varchar",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = String.Empty,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "LABEL_EN",
                    HeaderTextEn = "LABEL_EN",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 4
                }
                },
                { "description_en", new ColumnMetadata()
                {
                    Name = "description_en",
                    DbName = "description_en",
                    DataType = "System.String",
                    DbDataType = "text",
                    NewDataType = "text",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = String.Empty,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "DESCRIPTION_EN",
                    HeaderTextEn = "DESCRIPTION_EN",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 5
                }
                },
                { "ldsity_object_group_id", new ColumnMetadata()
                {
                    Name = "ldsity_object_group_id",
                    DbName = "ldsity_object_group",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = null,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "LDSITY_OBJECT_GROUP_ID",
                    HeaderTextEn = "LDSITY_OBJECT_GROUP_ID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = false,
                    DisplayIndex = 6
                }
                },
                { "areal_or_population_id", new ColumnMetadata()
                {
                    Name = "areal_or_population_id",
                    DbName = "areal_or_population",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = null,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "AREAL_OR_POPULATION_ID",
                    HeaderTextEn = "AREAL_OR_POPULATION_ID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = false,
                    DisplayIndex = 7
                }
                },
                { "core", new ColumnMetadata()
                {
                    Name = "core",
                    DbName = "core",
                    DataType = "System.Boolean",
                    DbDataType = "bool",
                    NewDataType = "bool",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = null,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "CORE",
                    HeaderTextEn = "CORE",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = false,
                    DisplayIndex = 8
                }
                }
            };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColLabelCs = Cols["label_cs"];

                /// <summary>
                /// Column description_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColDescriptionCs = Cols["description_cs"];

                /// <summary>
                /// Column state_or_change_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColStateOrChangeId = Cols["state_or_change_id"];

                /// <summary>
                /// Column label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColLabelEn = Cols["label_en"];

                /// <summary>
                /// Column description_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColDescriptionEn = Cols["description_en"];

                /// <summary>
                /// Column ldsity_object_group_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColLDsityObjectGroupId = Cols["ldsity_object_group_id"];

                /// <summary>
                /// Column areal_or_population_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColArealOrPopulationId = Cols["areal_or_population_id"];

                /// <summary>
                /// Column core metadata
                /// </summary>
                public static readonly ColumnMetadata ColCore = Cols["core"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="extended">Altogether category is included - true|false</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TDTargetVariableList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    bool extended = false,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            extended: extended,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="extended">Altogether category is included - true|false</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TDTargetVariableList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    bool extended = false,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            extended: extended,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// Last command text
                /// </summary>
                public string CommandText = String.Empty;

                /// <summary>
                /// List of target variables (read-only)
                /// </summary>
                public List<TDTargetVariable> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new TDTargetVariable(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexers

                /// <summary>
                /// Target variable from list by identifier (read-only)
                /// </summary>
                /// <param name="id">Target variable identifier</param>
                /// <returns>Target variable from list by identifier (null if not found)</returns>
                public TDTargetVariable this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new TDTargetVariable(composite: this, data: a))
                                .FirstOrDefault<TDTargetVariable>();
                    }
                }

                #endregion Indexers


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public TDTargetVariableList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new TDTargetVariableList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            extended: Extended,
                            loadingTime: LoadingTime) :
                        new TDTargetVariableList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            extended: Extended,
                            loadingTime: null);
                }

                /// <summary>
                /// Update target variable in database
                /// </summary>
                /// <param name="item">Target variable</param>
                public void Update(TDTargetVariable item)
                {
                    if (item != null)
                    {
                        int id = item.Id;
                        string labelCs = item.LabelCs;
                        string descriptionCs = item.DescriptionCs;
                        string labelEn = item.LabelEn;
                        string descriptionEn = item.DescriptionEn;

                        if (Items.Where(a => a.Id == id).Any())
                        {
                            TDTargetVariable targetVariable = Items.Where(a => a.Id == id).First();
                            targetVariable.Id = id;
                            targetVariable.LabelCs = labelCs;
                            targetVariable.DescriptionCs = descriptionCs;
                            targetVariable.LabelEn = labelEn;
                            targetVariable.DescriptionEn = descriptionEn;
                            Data.AcceptChanges();

                            this.CommandText = String.Concat(
                            $"UPDATE {TDSchema.Name}.{Name}{Environment.NewLine}",
                            $"SET{Environment.NewLine}",
                            $"    {ColLabelCs.DbName} = {Functions.PrepStringArg(labelCs)},{Environment.NewLine}",
                            $"    {ColDescriptionCs.DbName} = {Functions.PrepStringArg(descriptionCs)},{Environment.NewLine}",
                            $"    {ColLabelEn.DbName} = {Functions.PrepStringArg(labelEn)},{Environment.NewLine}",
                            $"    {ColDescriptionEn.DbName} = {Functions.PrepStringArg(descriptionEn)}{Environment.NewLine}",
                            $"WHERE {ColId.DbName} = {Functions.PrepIntArg(id)};{Environment.NewLine}");
                            Database.Postgres.ExecuteNonQuery(this.CommandText);
                        }
                        else
                        {
                            throw new ArgumentException(
                                $"Target variable id = {id} doesn't exist.");
                        }
                    }
                    else
                    {
                        throw new ArgumentException(
                                $"Target variable is null.");
                    }
                }

                #endregion Methods

            }

        }
    }
}