﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {
            // c_ldsity

            /// <summary>
            /// Local density contribution
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class TDLDsity(
                TDLDsityList composite = null,
                DataRow data = null)
                    : ALookupTableEntry<TDLDsityList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Local density contribution identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TDLDsityList.ColId.Name,
                            defaultValue: Int32.Parse(s: TDLDsityList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TDLDsityList.ColId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Label of local density contribution in national language
                /// </summary>
                public override string LabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDLDsityList.ColLabelCs.Name,
                            defaultValue: TDLDsityList.ColLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDLDsityList.ColLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Description of local density contribution in national language
                /// </summary>
                public override string DescriptionCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDLDsityList.ColDescriptionCs.Name,
                            defaultValue: TDLDsityList.ColDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDLDsityList.ColDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Extended label of local density contribution in national language (read-only)
                /// </summary>
                public override string ExtendedLabelCs
                {
                    get
                    {
                        if (UseNegative == null)
                        {
                            return $"{Id} - {LabelCs}";
                        }
                        else if (UseNegative == false)
                        {
                            return $"{Id} - {LabelCs} (kladný příspěvek)";
                        }
                        else
                        {
                            return $"{Id} - {LabelCs} (záporný příspěvek)";
                        }
                    }
                }


                /// <summary>
                /// Label of local density contribution in English
                /// </summary>
                public override string LabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDLDsityList.ColLabelEn.Name,
                            defaultValue: TDLDsityList.ColLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDLDsityList.ColLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Description of local density contribution in English
                /// </summary>
                public override string DescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDLDsityList.ColDescriptionEn.Name,
                            defaultValue: TDLDsityList.ColDescriptionEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDLDsityList.ColDescriptionEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Extended label of local density contribution in English (read-only)
                /// </summary>
                public override string ExtendedLabelEn
                {
                    get
                    {
                        if (UseNegative == null)
                        {
                            return $"{Id} - {LabelEn}";
                        }
                        else if (UseNegative == false)
                        {
                            return $"{Id} - {LabelEn} (positive contribution)";
                        }
                        else
                        {
                            return $"{Id} - {LabelEn} (negative contribution)";
                        }
                    }
                }


                /// <summary>
                /// Local density object identifier
                /// </summary>
                public int LDsityObjectId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TDLDsityList.ColLDsityObjectId.Name,
                            defaultValue: Int32.Parse(s: TDLDsityList.ColLDsityObjectId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TDLDsityList.ColLDsityObjectId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Local density object (read-only)
                /// </summary>
                public TDLDsityObject LDsityObject
                {
                    get
                    {
                        return ((NfiEstaDB)Composite.Database).STargetData.CLDsityObject[LDsityObjectId];
                    }
                }


                /// <summary>
                /// Label of local density object in national language
                /// </summary>
                public string LDsityObjectLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDLDsityList.ColLDsityObjectLabelCs.Name,
                            defaultValue: TDLDsityList.ColLDsityObjectLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDLDsityList.ColLDsityObjectLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Description of local density object in national language
                /// </summary>
                public string LDsityObjectDescriptionCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDLDsityList.ColLDsityObjectDescriptionCs.Name,
                            defaultValue: TDLDsityList.ColLDsityObjectDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDLDsityList.ColLDsityObjectDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Extended label of local density object in national language (read-only)
                /// </summary>
                public string ExtendedLDsityObjectLabelCs
                {
                    get
                    {
                        return $"{LDsityObjectId} - {LDsityObjectLabelCs}";
                    }
                }


                /// <summary>
                /// Label of local density object in English
                /// </summary>
                public string LDsityObjectLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDLDsityList.ColLDsityObjectLabelEn.Name,
                            defaultValue: TDLDsityList.ColLDsityObjectLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDLDsityList.ColLDsityObjectLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Description of local density object in English
                /// </summary>
                public string LDsityObjectDescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDLDsityList.ColLDsityObjectDescriptionEn.Name,
                            defaultValue: TDLDsityList.ColLDsityObjectDescriptionEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDLDsityList.ColLDsityObjectDescriptionEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Extended label of local density object in English (read-only)
                /// </summary>
                public string ExtendedLDsityObjectLabelEn
                {
                    get
                    {
                        return $"{LDsityObjectId} - {LDsityObjectLabelEn}";
                    }
                }


                /// <summary>
                /// Column expression
                /// </summary>
                public string ColumnExpression
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDLDsityList.ColColumnExpression.Name,
                            defaultValue: TDLDsityList.ColColumnExpression.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDLDsityList.ColColumnExpression.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Unit of measure
                /// </summary>
                public int UnitOfMeasureId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TDLDsityList.ColUnitOfMeasureId.Name,
                            defaultValue: Int32.Parse(s: TDLDsityList.ColUnitOfMeasureId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TDLDsityList.ColUnitOfMeasureId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Area domain category identifiers as text
                /// </summary>
                public string AreaDomainCategory
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDLDsityList.ColAreaDomainCategory.Name,
                            defaultValue: TDLDsityList.ColAreaDomainCategory.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDLDsityList.ColAreaDomainCategory.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// List of area domain category identifiers
                /// </summary>
                public List<int> AreaDomainCategoryList
                {
                    get
                    {
                        return Functions.StringToIntList(
                            text: AreaDomainCategory,
                            separator: (char)59,
                            defaultValue: null);
                    }
                }


                /// <summary>
                /// Subpopulation category identifiers as text
                /// </summary>
                public string SubPopulationCategory
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDLDsityList.ColSubPopulationCategory.Name,
                            defaultValue: TDLDsityList.ColSubPopulationCategory.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDLDsityList.ColSubPopulationCategory.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// List of subpopulation category identifiers
                /// </summary>
                public List<int> SubPopulationCategoryList
                {
                    get
                    {
                        return Functions.StringToIntList(
                            text: SubPopulationCategory,
                            separator: (char)59,
                            defaultValue: null);
                    }
                }


                /// <summary>
                /// Definition variant identifiers as text
                /// </summary>
                public string DefinitionVariant
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDLDsityList.ColDefinitionVariant.Name,
                            defaultValue: TDLDsityList.ColDefinitionVariant.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDLDsityList.ColDefinitionVariant.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// List of definition variant identifiers
                /// </summary>
                public List<int> DefinitionVariantList
                {
                    get
                    {
                        return Functions.StringToIntList(
                            text: DefinitionVariant,
                            separator: (char)59,
                            defaultValue: null);
                    }
                }


                /// <summary>
                /// Target variable identifer
                /// </summary>
                public Nullable<int> TargetVariableId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TDLDsityList.ColTargetVariableId.Name,
                            defaultValue: String.IsNullOrEmpty(value: TDLDsityList.ColTargetVariableId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TDLDsityList.ColTargetVariableId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TDLDsityList.ColTargetVariableId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Target variable object (read-only)
                /// </summary>
                public TDTargetVariable TargetVariable
                {
                    get
                    {
                        return (TargetVariableId != null) ?
                            ((NfiEstaDB)Composite.Database).STargetData.CTargetVariable[(int)TargetVariableId] : null;
                    }
                }


                /// <summary>
                /// Local density object type identifier
                /// </summary>
                public Nullable<int> LDsityObjectTypeId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TDLDsityList.ColLDsityObjectTypeId.Name,
                            defaultValue: String.IsNullOrEmpty(value: TDLDsityList.ColLDsityObjectTypeId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TDLDsityList.ColLDsityObjectTypeId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TDLDsityList.ColLDsityObjectTypeId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Local density object type (read-only)
                /// </summary>
                public TDLDsityObjectType LDsityObjectType
                {
                    get
                    {
                        return (LDsityObjectTypeId != null) ?
                            ((NfiEstaDB)Composite.Database).STargetData.CLDsityObjectType[(int)LDsityObjectTypeId] : null;
                    }
                }


                /// <summary>
                /// Negative local density contribution
                /// </summary>
                public Nullable<bool> UseNegative
                {
                    get
                    {
                        return Functions.GetNBoolArg(
                            row: Data,
                            name: TDLDsityList.ColUseNegative.Name,
                            defaultValue: String.IsNullOrEmpty(value: TDLDsityList.ColUseNegative.DefaultValue) ?
                                (Nullable<bool>)null :
                                Boolean.Parse(value: TDLDsityList.ColUseNegative.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNBoolArg(
                            row: Data,
                            name: TDLDsityList.ColUseNegative.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Version identifier
                /// </summary>
                public Nullable<int> VersionId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TDLDsityList.ColVersionId.Name,
                            defaultValue: String.IsNullOrEmpty(value: TDLDsityList.ColVersionId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TDLDsityList.ColVersionId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TDLDsityList.ColVersionId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Version object (read-only)
                /// </summary>
                public TDVersion Version
                {
                    get
                    {
                        return (VersionId != null) ?
                            ((NfiEstaDB)Composite.Database).STargetData.CVersion[(int)VersionId] : null;
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not TDLDsity Type, return False
                    if (obj is not TDLDsity)
                    {
                        return false;
                    }

                    return
                        Id == ((TDLDsity)obj).Id;
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                #endregion Methods

            }


            /// <summary>
            /// List of local density contributions
            /// </summary>
            public class TDLDsityList
                : ALookupTable
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema = TDSchema.Name;

                /// <summary>
                /// Table name
                /// </summary>
                public const string Name = "c_ldsity";

                /// <summary>
                /// Table alias
                /// </summary>
                public const string Alias = "c_ldsity";

                /// <summary>
                /// Description in national language
                /// </summary>
                public const string CaptionCs = "Seznam příspěvků lokálních hustot";

                /// <summary>
                /// Description in English
                /// </summary>
                public const string CaptionEn = "List of local density contributions";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
            {
                { "id", new ColumnMetadata()
                {
                    Name = "id",
                    DbName = "id",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = true,
                    NotNull = true,
                    DefaultValue = "0",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "ID",
                    HeaderTextEn = "ID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 0
                }
                },
                { "label_cs", new ColumnMetadata()
                {
                    Name = "label_cs",
                    DbName = "label",
                    DataType = "System.String",
                    DbDataType = "varchar",
                    NewDataType = "varchar",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = String.Empty,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "LABEL_CS",
                    HeaderTextEn = "LABEL_CS",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 1
                }
                },
                { "description_cs", new ColumnMetadata()
                {
                    Name = "description_cs",
                    DbName = "description",
                    DataType = "System.String",
                    DbDataType = "text",
                    NewDataType = "text",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = String.Empty,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "DESCRIPTION_CS",
                    HeaderTextEn = "DESCRIPTION_CS",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 2
                }
                },
                { "label_en", new ColumnMetadata()
                {
                    Name = "label_en",
                    DbName = "label_en",
                    DataType = "System.String",
                    DbDataType = "varchar",
                    NewDataType = "varchar",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = String.Empty,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "LABEL_EN",
                    HeaderTextEn = "LABEL_EN",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 3
                }
                },
                { "description_en", new ColumnMetadata()
                {
                    Name = "description_en",
                    DbName = "description_en",
                    DataType = "System.String",
                    DbDataType = "text",
                    NewDataType = "text",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = String.Empty,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "DESCRIPTION_EN",
                    HeaderTextEn = "DESCRIPTION_EN",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 4
                }
                },
                { "ldsity_object_id", new ColumnMetadata()
                {
                    Name = "ldsity_object_id",
                    DbName = "ldsity_object",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = "0",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "LDSITY_OBJECT_ID",
                    HeaderTextEn = "LDSITY_OBJECT_ID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 5
                }
                },
                { "ldsity_object_label_cs", new ColumnMetadata()
                {
                    Name = "ldsity_object_label_cs",
                    DbName = "object_label",
                    DataType = "System.String",
                    DbDataType = "varchar",
                    NewDataType = "varchar",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = null,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "LDSITY_OBJECT_LABEL_CS",
                    HeaderTextEn = "LDSITY_OBJECT_LABEL_CS",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = false,
                    DisplayIndex = 6
                }
                },
                { "ldsity_object_description_cs", new ColumnMetadata()
                {
                    Name = "ldsity_object_description_cs",
                    DbName = "object_description",
                    DataType = "System.String",
                    DbDataType = "text",
                    NewDataType = "text",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = null,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "LDSITY_OBJECT_DESCRIPTION_CS",
                    HeaderTextEn = "LDSITY_OBJECT_DESCRIPTION_CS",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = false,
                    DisplayIndex = 7
                }
                },
                { "ldsity_object_label_en", new ColumnMetadata()
                {
                    Name = "ldsity_object_label_en",
                    DbName = "object_label_en",
                    DataType = "System.String",
                    DbDataType = "varchar",
                    NewDataType = "varchar",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = null,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "LDSITY_OBJECT_LABEL_EN",
                    HeaderTextEn = "LDSITY_OBJECT_LABEL_EN",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = false,
                    DisplayIndex = 8
                }
                },
                { "ldsity_object_description_en", new ColumnMetadata()
                {
                    Name = "ldsity_object_description_en",
                    DbName = "object_description_en",
                    DataType = "System.String",
                    DbDataType = "text",
                    NewDataType = "text",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = null,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "LDSITY_OBJECT_DESCRIPTION_EN",
                    HeaderTextEn = "LDSITY_OBJECT_DESCRIPTION_EN",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = false,
                    DisplayIndex = 9
                }
                },
                { "column_expression", new ColumnMetadata()
                {
                    Name = "column_expression",
                    DbName = "column_expression",
                    DataType = "System.String",
                    DbDataType = "text",
                    NewDataType = "text",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = String.Empty,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "COLUMN_EXPRESSION",
                    HeaderTextEn = "COLUMN_EXPRESSION",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 10
                }
                },
                { "unit_of_measure_id", new ColumnMetadata()
                {
                    Name = "unit_of_measure_id",
                    DbName = "unit_of_measure",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = "0",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "UNIT_OF_MEASURE_ID",
                    HeaderTextEn = "UNIT_OF_MEASURE_ID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 11
                }
                },
                { "area_domain_category", new ColumnMetadata()
                {
                    Name = "area_domain_category",
                    DbName = "area_domain_category",
                    DataType = "System.String",
                    DbDataType = "_int4",
                    NewDataType = "text",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = null,
                    FuncCall = "array_to_string({0}, ';', 'NULL')",
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "AREA_DOMAIN_CATEGORY",
                    HeaderTextEn = "AREA_DOMAIN_CATEGORY",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 12
                }
                },
                { "sub_population_category", new ColumnMetadata()
                {
                    Name = "sub_population_category",
                    DbName = "sub_population_category",
                    DataType = "System.String",
                    DbDataType = "_int4",
                    NewDataType = "text",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = null,
                    FuncCall = "array_to_string({0}, ';', 'NULL')",
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "SUB_POPULATION_CATEGORY",
                    HeaderTextEn = "SUB_POPULATION_CATEGORY",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 13
                }
                },
                { "definition_variant", new ColumnMetadata()
                {
                    Name = "definition_variant",
                    DbName = "definition_variant",
                    DataType = "System.String",
                    DbDataType = "_int4",
                    NewDataType = "text",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = null,
                    FuncCall = "array_to_string({0}, ';', 'NULL')",
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "DEFINITION_VARIANT",
                    HeaderTextEn = "DEFINITION_VARIANT",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 14
                }
                },
                { "target_variable_id", new ColumnMetadata()
                {
                    Name = "target_variable_id",
                    DbName = "target_variable",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = null,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "TARGET_VARIABLE_ID",
                    HeaderTextEn = "TARGET_VARIABLE_ID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = false,
                    DisplayIndex = 15
                }
                },
                { "ldsity_object_type_id", new ColumnMetadata()
                {
                    Name = "ldsity_object_type_id",
                    DbName = "ldsity_object_type",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = null,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "LDSITY_OBJECT_TYPE_ID",
                    HeaderTextEn = "LDSITY_OBJECT_TYPE_ID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = false,
                    DisplayIndex = 16
                }
                },
                { "use_negative", new ColumnMetadata()
                {
                    Name = "use_negative",
                    DbName = "use_negative",
                    DataType = "System.Boolean",
                    DbDataType = "bool",
                    NewDataType = "bool",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = null,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "USE_NEGATIVE",
                    HeaderTextEn = "USE_NEGATIVE",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = false,
                    DisplayIndex = 17
                }
                },
                { "version_id", new ColumnMetadata()
                {
                    Name = "version_id",
                    DbName = "version",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = null,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "VERSION_ID",
                    HeaderTextEn = "VERSION_ID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = false,
                    DisplayIndex = 18
                }
                }
            };


                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColLabelCs = Cols["label_cs"];

                /// <summary>
                /// Column description_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColDescriptionCs = Cols["description_cs"];

                /// <summary>
                /// Column label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColLabelEn = Cols["label_en"];

                /// <summary>
                /// Column description_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColDescriptionEn = Cols["description_en"];

                /// <summary>
                /// Column ldsity_object_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColLDsityObjectId = Cols["ldsity_object_id"];

                /// <summary>
                /// Column ldsity_object_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColLDsityObjectLabelCs = Cols["ldsity_object_label_cs"];

                /// <summary>
                /// Column ldsity_object_description_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColLDsityObjectDescriptionCs = Cols["ldsity_object_description_cs"];

                /// <summary>
                /// Column ldsity_object_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColLDsityObjectLabelEn = Cols["ldsity_object_label_en"];

                /// <summary>
                /// Column ldsity_object_description_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColLDsityObjectDescriptionEn = Cols["ldsity_object_description_en"];

                /// <summary>
                /// Column column_expression metadata
                /// </summary>
                public static readonly ColumnMetadata ColColumnExpression = Cols["column_expression"];

                /// <summary>
                /// Column unit_of_measure_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColUnitOfMeasureId = Cols["unit_of_measure_id"];

                /// <summary>
                /// Column area_domain_category metadata
                /// </summary>
                public static readonly ColumnMetadata ColAreaDomainCategory = Cols["area_domain_category"];

                /// <summary>
                /// Column sub_population_category metadata
                /// </summary>
                public static readonly ColumnMetadata ColSubPopulationCategory = Cols["sub_population_category"];

                /// <summary>
                /// Column definition_variant metadata
                /// </summary>
                public static readonly ColumnMetadata ColDefinitionVariant = Cols["definition_variant"];

                /// <summary>
                /// Column target_variable_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColTargetVariableId = Cols["target_variable_id"];

                /// <summary>
                /// Column ldsity_object_type_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColLDsityObjectTypeId = Cols["ldsity_object_type_id"];

                /// <summary>
                /// Column use_negative metadata
                /// </summary>
                public static readonly ColumnMetadata ColUseNegative = Cols["use_negative"];

                /// <summary>
                /// Column version_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColVersionId = Cols["version_id"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="extended">Altogether category is included - true|false</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TDLDsityList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    bool extended = false,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            extended: extended,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="extended">Altogether category is included - true|false</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TDLDsityList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    bool extended = false,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            extended: extended,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// Last command text
                /// </summary>
                public string CommandText = String.Empty;

                /// <summary>
                /// List of local density contributions (read-only)
                /// </summary>
                public List<TDLDsity> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new TDLDsity(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexers

                /// <summary>
                /// Local density contribution from list by identifier (read-only)
                /// </summary>
                /// <param name="id"> Local density contribution identifier</param>
                /// <returns> Local density contribution from list by identifier (null if not found)</returns>
                public TDLDsity this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new TDLDsity(composite: this, data: a))
                                .FirstOrDefault<TDLDsity>();
                    }
                }

                #endregion Indexers


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public TDLDsityList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new TDLDsityList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            extended: Extended,
                            loadingTime: LoadingTime) :
                        new TDLDsityList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            extended: Extended,
                            loadingTime: null);
                }

                /// <summary>
                /// Update local density contribution in database
                /// </summary>
                /// <param name="item">Local density contribution</param>
                public void Update(TDLDsity item)
                {
                    if (item != null)
                    {
                        int id = item.Id;
                        string labelCs = item.LabelCs;
                        string descriptionCs = item.DescriptionCs;
                        string labelEn = item.LabelEn;
                        string descriptionEn = item.DescriptionEn;

                        if (Items.Where(a => a.Id == id).Any())
                        {
                            TDLDsity ldsity = Items.Where(a => a.Id == id).First();
                            ldsity.Id = id;
                            ldsity.LabelCs = labelCs;
                            ldsity.DescriptionCs = descriptionCs;
                            ldsity.LabelEn = labelEn;
                            ldsity.DescriptionEn = descriptionEn;
                            Data.AcceptChanges();

                            this.CommandText = String.Concat(
                             $"UPDATE {TDSchema.Name}.{Name}{Environment.NewLine}",
                             $"SET{Environment.NewLine}",
                             $"    {ColLabelCs.DbName} = {Functions.PrepStringArg(labelCs)},{Environment.NewLine}",
                             $"    {ColDescriptionCs.DbName} = {Functions.PrepStringArg(descriptionCs)},{Environment.NewLine}",
                             $"    {ColLabelEn.DbName} = {Functions.PrepStringArg(labelEn)},{Environment.NewLine}",
                             $"    {ColDescriptionEn.DbName} = {Functions.PrepStringArg(descriptionEn)}{Environment.NewLine}",
                             $"WHERE {ColId.DbName} = {Functions.PrepIntArg(id)};{Environment.NewLine}");
                            Database.Postgres.ExecuteNonQuery(this.CommandText);
                        }
                        else
                        {
                            throw new ArgumentException(
                                $"Local density contribution id = {id} doesn't exist.");
                        }
                    }
                    else
                    {
                        throw new ArgumentException(
                                $"Local density contribution is null.");
                    }
                }

                #endregion Methods

            }

        }
    }
}