﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi.NfiEstaPg.TargetData
{

    partial class FormTargetData
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.msMain = new System.Windows.Forms.MenuStrip();
            this.tsmiLookupTables = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiCAreaDomain = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiCAreaDomainCategory = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiCArealOrPopulation = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiCClassificationType = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiCDefinitionVariant = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiCLDsity = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiCLDsityObject = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiCLDsityObjectGroup = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiCLDsityObjectType = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiCPanelRefYearSetGroup = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiCStateOrChange = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiCSubPopulation = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiCSubPopulationCategory = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiCTargetVariable = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiCUnitOfMeasure = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiCVersion = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiMappingTables = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiCMADCToClassificationRule = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiCMADCToClassRuleToPanelRefYearSet = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiCMLDsityObjectToLDsityObjectGroup = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiCMLDsityToPanelRefYearSetVersion = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiCMLDsityToTargetToCategorizationSetup = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiCMLDsityToTargetVariable = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiCMSPCToClassificationRule = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiCMSPCToClassRuleToPanelRefYearSet = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiDataTables = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiTADCHierarchy = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiTAvailableDataSets = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiTCategorizationSetup = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiTETLAreaDomain = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiTETLAreaDomainCategory = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiTETLLog = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiTETLSubPopulation = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiTETLSubPopulationCategory = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiTETLTargetVariable = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiTExportConnection = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiTLDsityValues = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiTPanelRefYearSetGroup = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiTSPCHierarchy = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiCheckFunctions = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFnCheckAdcRuleHasPanelRef = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFnCheckClassificationRule = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFnCheckClassificationRuleSyntax = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFnCheckClassificationRules = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFnCheckSpcRuleHasPanelRef = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiGetFunctions = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiOtherGetFunctions = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFnAddPlotTargetAttr = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFnGetAttributeDomainAD = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFnGetAttributeDomainSP = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFnGetAvailableRefYearSetToPanelForClassificationRuleIdAdc = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFnGetAvailableRefYearSetToPanelForClassificationRuleIdSpc = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFnGetCaseAdForCategorizationSetups = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFnGetCategoryForClassificationRuleIdAdc = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFnGetCategoryForClassificationRuleIdSpc = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFnGetCategoryTypeForClassificationRuleIdAdc = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFnGetCategoryTypeForClassificationRuleIdSpc = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFnGetClassificationRuleForAdc = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFnGetClassificationRuleForSpc = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFnGetClassificationRuleForClassificationRuleIdAdc = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFnGetClassificationRuleForClassificationRuleIdSpc = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFnGetClassificationRuleId4Category = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFnGetClassificationRules4CategorizationSetups = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFnGetClassificationRulesId4Categories = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFnGetEligiblePanelRefYearSetCombinations = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFnGetEligiblePanelRefyearsetGroups = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFnGetLDsityObjects = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFnGetPlots = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFnGetRefYearSetToPanelMappingForGroup = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiETLFunctions = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFnEtlGetExportConnections = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFnEtlGetTargetVariable = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFnEtlGetTargetVariableExtended = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFnEtlGetTargetVariableAggregated = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiControlTargetVariableSelector = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFnGetAreaDomain = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFnGetAreaDomainCategory = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFnGetAreaDomainCategoryForDomain = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFnGetArealOrPopulation = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFnGetDefinitionVariant = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFnGetDefinitionVariantForLDsity = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFnGetHierarchyAdc = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFnGetHierarchySpc = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFnGetLDsity = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFnGetLDsityForObject = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFnGetLDsityObject = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFnGetLDsityObjectForADSP = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFnGetLDsityObjectForLDsityObjectADSP = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFnGetLDsityObjectForLDsityObjectGroup = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFnGetLDsityObjectGroup = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFnGetLDsityObjectType = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFnGetLDsitySubPopForObject = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFnGetPanelRefYearSet = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFnGetPanelRefYearSetGroup = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFnGetStateOrChange = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFnGetSupPopulation = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFnGetSupPopulationCategory = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFnGetSubPopulationCategoryForPopulation = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFnGetTargetVariableAgg = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFnGetTargetVariable = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFnGetUnitOfMeasure = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFnGetUnitOfMeasureForLDsityObjectGroup = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFnGetVersion = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiETL = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiExtractData = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiGetDifferences = new System.Windows.Forms.ToolStripMenuItem();
            this.tlpMain = new System.Windows.Forms.TableLayoutPanel();
            this.tlpButtons = new System.Windows.Forms.TableLayoutPanel();
            this.pnlClose = new System.Windows.Forms.Panel();
            this.btnClose = new System.Windows.Forms.Button();
            this.grpMain = new System.Windows.Forms.GroupBox();
            this.splitMain = new System.Windows.Forms.SplitContainer();
            this.pnlData = new System.Windows.Forms.Panel();
            this.txtSQL = new System.Windows.Forms.RichTextBox();
            this.msMain.SuspendLayout();
            this.tlpMain.SuspendLayout();
            this.tlpButtons.SuspendLayout();
            this.pnlClose.SuspendLayout();
            this.grpMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitMain)).BeginInit();
            this.splitMain.Panel1.SuspendLayout();
            this.splitMain.Panel2.SuspendLayout();
            this.splitMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // msMain
            // 
            this.msMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiLookupTables,
            this.tsmiMappingTables,
            this.tsmiDataTables,
            this.tsmiCheckFunctions,
            this.tsmiGetFunctions,
            this.tsmiETL});
            this.msMain.Location = new System.Drawing.Point(0, 0);
            this.msMain.Name = "msMain";
            this.msMain.Size = new System.Drawing.Size(944, 24);
            this.msMain.TabIndex = 0;
            this.msMain.Text = "menuStrip1";
            // 
            // tsmiLookupTables
            // 
            this.tsmiLookupTables.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiCAreaDomain,
            this.tsmiCAreaDomainCategory,
            this.tsmiCArealOrPopulation,
            this.tsmiCClassificationType,
            this.tsmiCDefinitionVariant,
            this.tsmiCLDsity,
            this.tsmiCLDsityObject,
            this.tsmiCLDsityObjectGroup,
            this.tsmiCLDsityObjectType,
            this.tsmiCPanelRefYearSetGroup,
            this.tsmiCStateOrChange,
            this.tsmiCSubPopulation,
            this.tsmiCSubPopulationCategory,
            this.tsmiCTargetVariable,
            this.tsmiCUnitOfMeasure,
            this.tsmiCVersion});
            this.tsmiLookupTables.Name = "tsmiLookupTables";
            this.tsmiLookupTables.Size = new System.Drawing.Size(114, 20);
            this.tsmiLookupTables.Text = "tsmiLookupTables";
            // 
            // tsmiCAreaDomain
            // 
            this.tsmiCAreaDomain.Name = "tsmiCAreaDomain";
            this.tsmiCAreaDomain.Size = new System.Drawing.Size(231, 22);
            this.tsmiCAreaDomain.Text = "tsmiCAreaDomain";
            // 
            // tsmiCAreaDomainCategory
            // 
            this.tsmiCAreaDomainCategory.Name = "tsmiCAreaDomainCategory";
            this.tsmiCAreaDomainCategory.Size = new System.Drawing.Size(231, 22);
            this.tsmiCAreaDomainCategory.Text = "tsmiCAreaDomainCategory";
            // 
            // tsmiCArealOrPopulation
            // 
            this.tsmiCArealOrPopulation.Name = "tsmiCArealOrPopulation";
            this.tsmiCArealOrPopulation.Size = new System.Drawing.Size(231, 22);
            this.tsmiCArealOrPopulation.Text = "tsmiCArealOrPopulation";
            // 
            // tsmiCClassificationType
            // 
            this.tsmiCClassificationType.Name = "tsmiCClassificationType";
            this.tsmiCClassificationType.Size = new System.Drawing.Size(231, 22);
            this.tsmiCClassificationType.Text = "tsmiCClassificationType";
            // 
            // tsmiCDefinitionVariant
            // 
            this.tsmiCDefinitionVariant.Name = "tsmiCDefinitionVariant";
            this.tsmiCDefinitionVariant.Size = new System.Drawing.Size(231, 22);
            this.tsmiCDefinitionVariant.Text = "tsmiCDefinitionVariant";
            // 
            // tsmiCLDsity
            // 
            this.tsmiCLDsity.Name = "tsmiCLDsity";
            this.tsmiCLDsity.Size = new System.Drawing.Size(231, 22);
            this.tsmiCLDsity.Text = "tsmiCLDsity";
            // 
            // tsmiCLDsityObject
            // 
            this.tsmiCLDsityObject.Name = "tsmiCLDsityObject";
            this.tsmiCLDsityObject.Size = new System.Drawing.Size(231, 22);
            this.tsmiCLDsityObject.Text = "tsmiCLDsityObject";
            // 
            // tsmiCLDsityObjectGroup
            // 
            this.tsmiCLDsityObjectGroup.Name = "tsmiCLDsityObjectGroup";
            this.tsmiCLDsityObjectGroup.Size = new System.Drawing.Size(231, 22);
            this.tsmiCLDsityObjectGroup.Text = "tsmiCLDsityObjectGroup";
            // 
            // tsmiCLDsityObjectType
            // 
            this.tsmiCLDsityObjectType.Name = "tsmiCLDsityObjectType";
            this.tsmiCLDsityObjectType.Size = new System.Drawing.Size(231, 22);
            this.tsmiCLDsityObjectType.Text = "tsmiCLDsityObjectType";
            // 
            // tsmiCPanelRefYearSetGroup
            // 
            this.tsmiCPanelRefYearSetGroup.Name = "tsmiCPanelRefYearSetGroup";
            this.tsmiCPanelRefYearSetGroup.Size = new System.Drawing.Size(231, 22);
            this.tsmiCPanelRefYearSetGroup.Text = "tsmiCPanelRefYearSetGroup";
            // 
            // tsmiCStateOrChange
            // 
            this.tsmiCStateOrChange.Name = "tsmiCStateOrChange";
            this.tsmiCStateOrChange.Size = new System.Drawing.Size(231, 22);
            this.tsmiCStateOrChange.Text = "tsmiCStateOrChange";
            // 
            // tsmiCSubPopulation
            // 
            this.tsmiCSubPopulation.Name = "tsmiCSubPopulation";
            this.tsmiCSubPopulation.Size = new System.Drawing.Size(231, 22);
            this.tsmiCSubPopulation.Text = "tsmiCSubPopulation";
            // 
            // tsmiCSubPopulationCategory
            // 
            this.tsmiCSubPopulationCategory.Name = "tsmiCSubPopulationCategory";
            this.tsmiCSubPopulationCategory.Size = new System.Drawing.Size(231, 22);
            this.tsmiCSubPopulationCategory.Text = "tsmiCSubPopulationCategory";
            // 
            // tsmiCTargetVariable
            // 
            this.tsmiCTargetVariable.Name = "tsmiCTargetVariable";
            this.tsmiCTargetVariable.Size = new System.Drawing.Size(231, 22);
            this.tsmiCTargetVariable.Text = "tsmiCTargetVariable";
            // 
            // tsmiCUnitOfMeasure
            // 
            this.tsmiCUnitOfMeasure.Name = "tsmiCUnitOfMeasure";
            this.tsmiCUnitOfMeasure.Size = new System.Drawing.Size(231, 22);
            this.tsmiCUnitOfMeasure.Text = "tsmiCUnitOfMeasure";
            // 
            // tsmiCVersion
            // 
            this.tsmiCVersion.Name = "tsmiCVersion";
            this.tsmiCVersion.Size = new System.Drawing.Size(231, 22);
            this.tsmiCVersion.Text = "tsmiCVersion";
            // 
            // tsmiMappingTables
            // 
            this.tsmiMappingTables.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiCMADCToClassificationRule,
            this.tsmiCMADCToClassRuleToPanelRefYearSet,
            this.tsmiCMLDsityObjectToLDsityObjectGroup,
            this.tsmiCMLDsityToPanelRefYearSetVersion,
            this.tsmiCMLDsityToTargetToCategorizationSetup,
            this.tsmiCMLDsityToTargetVariable,
            this.tsmiCMSPCToClassificationRule,
            this.tsmiCMSPCToClassRuleToPanelRefYearSet});
            this.tsmiMappingTables.Name = "tsmiMappingTables";
            this.tsmiMappingTables.Size = new System.Drawing.Size(122, 20);
            this.tsmiMappingTables.Text = "tsmiMappingTables";
            // 
            // tsmiCMADCToClassificationRule
            // 
            this.tsmiCMADCToClassificationRule.Name = "tsmiCMADCToClassificationRule";
            this.tsmiCMADCToClassificationRule.Size = new System.Drawing.Size(311, 22);
            this.tsmiCMADCToClassificationRule.Text = "tsmiCMADCToClassificationRule";
            // 
            // tsmiCMADCToClassRuleToPanelRefYearSet
            // 
            this.tsmiCMADCToClassRuleToPanelRefYearSet.Name = "tsmiCMADCToClassRuleToPanelRefYearSet";
            this.tsmiCMADCToClassRuleToPanelRefYearSet.Size = new System.Drawing.Size(311, 22);
            this.tsmiCMADCToClassRuleToPanelRefYearSet.Text = "tsmiCMADCToClassRuleToPanelRefYearSet";
            // 
            // tsmiCMLDsityObjectToLDsityObjectGroup
            // 
            this.tsmiCMLDsityObjectToLDsityObjectGroup.Name = "tsmiCMLDsityObjectToLDsityObjectGroup";
            this.tsmiCMLDsityObjectToLDsityObjectGroup.Size = new System.Drawing.Size(311, 22);
            this.tsmiCMLDsityObjectToLDsityObjectGroup.Text = "tsmiCMLDsityObjectToLDsityObjectGroup";
            // 
            // tsmiCMLDsityToPanelRefYearSetVersion
            // 
            this.tsmiCMLDsityToPanelRefYearSetVersion.Name = "tsmiCMLDsityToPanelRefYearSetVersion";
            this.tsmiCMLDsityToPanelRefYearSetVersion.Size = new System.Drawing.Size(311, 22);
            this.tsmiCMLDsityToPanelRefYearSetVersion.Text = "tsmiCMLDsityToPanelRefYearSetVersion";
            // 
            // tsmiCMLDsityToTargetToCategorizationSetup
            // 
            this.tsmiCMLDsityToTargetToCategorizationSetup.Name = "tsmiCMLDsityToTargetToCategorizationSetup";
            this.tsmiCMLDsityToTargetToCategorizationSetup.Size = new System.Drawing.Size(311, 22);
            this.tsmiCMLDsityToTargetToCategorizationSetup.Text = "tsmiCMLDsityToTargetToCategorizationSetup";
            // 
            // tsmiCMLDsityToTargetVariable
            // 
            this.tsmiCMLDsityToTargetVariable.Name = "tsmiCMLDsityToTargetVariable";
            this.tsmiCMLDsityToTargetVariable.Size = new System.Drawing.Size(311, 22);
            this.tsmiCMLDsityToTargetVariable.Text = "tsmiCMLDsityToTargetVariable";
            // 
            // tsmiCMSPCToClassificationRule
            // 
            this.tsmiCMSPCToClassificationRule.Name = "tsmiCMSPCToClassificationRule";
            this.tsmiCMSPCToClassificationRule.Size = new System.Drawing.Size(311, 22);
            this.tsmiCMSPCToClassificationRule.Text = "tsmiCMSPCToClassificationRule";
            // 
            // tsmiCMSPCToClassRuleToPanelRefYearSet
            // 
            this.tsmiCMSPCToClassRuleToPanelRefYearSet.Name = "tsmiCMSPCToClassRuleToPanelRefYearSet";
            this.tsmiCMSPCToClassRuleToPanelRefYearSet.Size = new System.Drawing.Size(311, 22);
            this.tsmiCMSPCToClassRuleToPanelRefYearSet.Text = "tsmiCMSPCToClassRuleToPanelRefYearSet";
            // 
            // tsmiDataTables
            // 
            this.tsmiDataTables.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiTADCHierarchy,
            this.tsmiTAvailableDataSets,
            this.tsmiTCategorizationSetup,
            this.tsmiTETLAreaDomain,
            this.tsmiTETLAreaDomainCategory,
            this.tsmiTETLLog,
            this.tsmiTETLSubPopulation,
            this.tsmiTETLSubPopulationCategory,
            this.tsmiTETLTargetVariable,
            this.tsmiTExportConnection,
            this.tsmiTLDsityValues,
            this.tsmiTPanelRefYearSetGroup,
            this.tsmiTSPCHierarchy});
            this.tsmiDataTables.Name = "tsmiDataTables";
            this.tsmiDataTables.Size = new System.Drawing.Size(98, 20);
            this.tsmiDataTables.Text = "tsmiDataTables";
            // 
            // tsmiTADCHierarchy
            // 
            this.tsmiTADCHierarchy.Name = "tsmiTADCHierarchy";
            this.tsmiTADCHierarchy.Size = new System.Drawing.Size(247, 22);
            this.tsmiTADCHierarchy.Text = "tsmiTADCHierarchy";
            // 
            // tsmiTAvailableDataSets
            // 
            this.tsmiTAvailableDataSets.Name = "tsmiTAvailableDataSets";
            this.tsmiTAvailableDataSets.Size = new System.Drawing.Size(247, 22);
            this.tsmiTAvailableDataSets.Text = "tsmiTAvailableDataSets";
            // 
            // tsmiTCategorizationSetup
            // 
            this.tsmiTCategorizationSetup.Name = "tsmiTCategorizationSetup";
            this.tsmiTCategorizationSetup.Size = new System.Drawing.Size(247, 22);
            this.tsmiTCategorizationSetup.Text = "tsmiTCategorizationSetup";
            // 
            // tsmiTETLAreaDomain
            // 
            this.tsmiTETLAreaDomain.Name = "tsmiTETLAreaDomain";
            this.tsmiTETLAreaDomain.Size = new System.Drawing.Size(247, 22);
            this.tsmiTETLAreaDomain.Text = "tsmiTETLAreaDomain";
            // 
            // tsmiTETLAreaDomainCategory
            // 
            this.tsmiTETLAreaDomainCategory.Name = "tsmiTETLAreaDomainCategory";
            this.tsmiTETLAreaDomainCategory.Size = new System.Drawing.Size(247, 22);
            this.tsmiTETLAreaDomainCategory.Text = "tsmiTETLAreaDomainCategory";
            // 
            // tsmiTETLLog
            // 
            this.tsmiTETLLog.Name = "tsmiTETLLog";
            this.tsmiTETLLog.Size = new System.Drawing.Size(247, 22);
            this.tsmiTETLLog.Text = "tsmiTETLLog";
            // 
            // tsmiTETLSubPopulation
            // 
            this.tsmiTETLSubPopulation.Name = "tsmiTETLSubPopulation";
            this.tsmiTETLSubPopulation.Size = new System.Drawing.Size(247, 22);
            this.tsmiTETLSubPopulation.Text = "tsmiTETLSubPopulation";
            // 
            // tsmiTETLSubPopulationCategory
            // 
            this.tsmiTETLSubPopulationCategory.Name = "tsmiTETLSubPopulationCategory";
            this.tsmiTETLSubPopulationCategory.Size = new System.Drawing.Size(247, 22);
            this.tsmiTETLSubPopulationCategory.Text = "tsmiTETLSubPopulationCategory";
            // 
            // tsmiTETLTargetVariable
            // 
            this.tsmiTETLTargetVariable.Name = "tsmiTETLTargetVariable";
            this.tsmiTETLTargetVariable.Size = new System.Drawing.Size(247, 22);
            this.tsmiTETLTargetVariable.Text = "tsmiTETLTargetVariable";
            // 
            // tsmiTExportConnection
            // 
            this.tsmiTExportConnection.Name = "tsmiTExportConnection";
            this.tsmiTExportConnection.Size = new System.Drawing.Size(247, 22);
            this.tsmiTExportConnection.Text = "tsmiTExportConnection";
            // 
            // tsmiTLDsityValues
            // 
            this.tsmiTLDsityValues.Name = "tsmiTLDsityValues";
            this.tsmiTLDsityValues.Size = new System.Drawing.Size(247, 22);
            this.tsmiTLDsityValues.Text = "tsmiTLDsityValues";
            // 
            // tsmiTPanelRefYearSetGroup
            // 
            this.tsmiTPanelRefYearSetGroup.Name = "tsmiTPanelRefYearSetGroup";
            this.tsmiTPanelRefYearSetGroup.Size = new System.Drawing.Size(247, 22);
            this.tsmiTPanelRefYearSetGroup.Text = "tsmiTPanelRefYearSetGroup";
            // 
            // tsmiTSPCHierarchy
            // 
            this.tsmiTSPCHierarchy.Name = "tsmiTSPCHierarchy";
            this.tsmiTSPCHierarchy.Size = new System.Drawing.Size(247, 22);
            this.tsmiTSPCHierarchy.Text = "tsmiTSPCHierarchy";
            // 
            // tsmiCheckFunctions
            // 
            this.tsmiCheckFunctions.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiFnCheckAdcRuleHasPanelRef,
            this.tsmiFnCheckClassificationRule,
            this.tsmiFnCheckClassificationRuleSyntax,
            this.tsmiFnCheckClassificationRules,
            this.tsmiFnCheckSpcRuleHasPanelRef});
            this.tsmiCheckFunctions.Name = "tsmiCheckFunctions";
            this.tsmiCheckFunctions.Size = new System.Drawing.Size(127, 20);
            this.tsmiCheckFunctions.Text = "tsmiCheckFunctions";
            // 
            // tsmiFnCheckAdcRuleHasPanelRef
            // 
            this.tsmiFnCheckAdcRuleHasPanelRef.Name = "tsmiFnCheckAdcRuleHasPanelRef";
            this.tsmiFnCheckAdcRuleHasPanelRef.Size = new System.Drawing.Size(271, 22);
            this.tsmiFnCheckAdcRuleHasPanelRef.Text = "tsmiFnCheckAdcRuleHasPanelRef";
            this.tsmiFnCheckAdcRuleHasPanelRef.Click += new System.EventHandler(this.ItemFnCheckAdcRuleHasPanelRef_Click);
            // 
            // tsmiFnCheckClassificationRule
            // 
            this.tsmiFnCheckClassificationRule.Name = "tsmiFnCheckClassificationRule";
            this.tsmiFnCheckClassificationRule.Size = new System.Drawing.Size(271, 22);
            this.tsmiFnCheckClassificationRule.Text = "tsmiFnCheckClassificationRule";
            this.tsmiFnCheckClassificationRule.Click += new System.EventHandler(this.ItemFnCheckClassificationRule_Click);
            // 
            // tsmiFnCheckClassificationRuleSyntax
            // 
            this.tsmiFnCheckClassificationRuleSyntax.Name = "tsmiFnCheckClassificationRuleSyntax";
            this.tsmiFnCheckClassificationRuleSyntax.Size = new System.Drawing.Size(271, 22);
            this.tsmiFnCheckClassificationRuleSyntax.Text = "tsmiFnCheckClassificationRuleSyntax";
            this.tsmiFnCheckClassificationRuleSyntax.Click += new System.EventHandler(this.ItemFnCheckClassificationRuleSyntax_Click);
            // 
            // tsmiFnCheckClassificationRules
            // 
            this.tsmiFnCheckClassificationRules.Name = "tsmiFnCheckClassificationRules";
            this.tsmiFnCheckClassificationRules.Size = new System.Drawing.Size(271, 22);
            this.tsmiFnCheckClassificationRules.Text = "tsmiFnCheckClassificationRules";
            this.tsmiFnCheckClassificationRules.Click += new System.EventHandler(this.ItemFnCheckClassificationRules_Click);
            // 
            // tsmiFnCheckSpcRuleHasPanelRef
            // 
            this.tsmiFnCheckSpcRuleHasPanelRef.Name = "tsmiFnCheckSpcRuleHasPanelRef";
            this.tsmiFnCheckSpcRuleHasPanelRef.Size = new System.Drawing.Size(271, 22);
            this.tsmiFnCheckSpcRuleHasPanelRef.Text = "tsmiFnCheckSpcRuleHasPanelRef";
            this.tsmiFnCheckSpcRuleHasPanelRef.Click += new System.EventHandler(this.ItemFnCheckSpcRuleHasPanelRef_Click);
            // 
            // tsmiGetFunctions
            // 
            this.tsmiGetFunctions.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiOtherGetFunctions,
            this.tsmiETLFunctions,
            this.tsmiFnGetAreaDomain,
            this.tsmiFnGetAreaDomainCategory,
            this.tsmiFnGetAreaDomainCategoryForDomain,
            this.tsmiFnGetArealOrPopulation,
            this.tsmiFnGetDefinitionVariant,
            this.tsmiFnGetDefinitionVariantForLDsity,
            this.tsmiFnGetHierarchyAdc,
            this.tsmiFnGetHierarchySpc,
            this.tsmiFnGetLDsity,
            this.tsmiFnGetLDsityForObject,
            this.tsmiFnGetLDsityObject,
            this.tsmiFnGetLDsityObjectForADSP,
            this.tsmiFnGetLDsityObjectForLDsityObjectADSP,
            this.tsmiFnGetLDsityObjectForLDsityObjectGroup,
            this.tsmiFnGetLDsityObjectGroup,
            this.tsmiFnGetLDsityObjectType,
            this.tsmiFnGetLDsitySubPopForObject,
            this.tsmiFnGetPanelRefYearSet,
            this.tsmiFnGetPanelRefYearSetGroup,
            this.tsmiFnGetStateOrChange,
            this.tsmiFnGetSupPopulation,
            this.tsmiFnGetSupPopulationCategory,
            this.tsmiFnGetSubPopulationCategoryForPopulation,
            this.tsmiFnGetTargetVariableAgg,
            this.tsmiFnGetTargetVariable,
            this.tsmiFnGetUnitOfMeasure,
            this.tsmiFnGetUnitOfMeasureForLDsityObjectGroup,
            this.tsmiFnGetVersion});
            this.tsmiGetFunctions.Name = "tsmiGetFunctions";
            this.tsmiGetFunctions.Size = new System.Drawing.Size(112, 20);
            this.tsmiGetFunctions.Text = "tsmiGetFunctions";
            // 
            // tsmiOtherGetFunctions
            // 
            this.tsmiOtherGetFunctions.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiFnAddPlotTargetAttr,
            this.tsmiFnGetAttributeDomainAD,
            this.tsmiFnGetAttributeDomainSP,
            this.tsmiFnGetAvailableRefYearSetToPanelForClassificationRuleIdAdc,
            this.tsmiFnGetAvailableRefYearSetToPanelForClassificationRuleIdSpc,
            this.tsmiFnGetCaseAdForCategorizationSetups,
            this.tsmiFnGetCategoryForClassificationRuleIdAdc,
            this.tsmiFnGetCategoryForClassificationRuleIdSpc,
            this.tsmiFnGetCategoryTypeForClassificationRuleIdAdc,
            this.tsmiFnGetCategoryTypeForClassificationRuleIdSpc,
            this.tsmiFnGetClassificationRuleForAdc,
            this.tsmiFnGetClassificationRuleForSpc,
            this.tsmiFnGetClassificationRuleForClassificationRuleIdAdc,
            this.tsmiFnGetClassificationRuleForClassificationRuleIdSpc,
            this.tsmiFnGetClassificationRuleId4Category,
            this.tsmiFnGetClassificationRules4CategorizationSetups,
            this.tsmiFnGetClassificationRulesId4Categories,
            this.tsmiFnGetEligiblePanelRefYearSetCombinations,
            this.tsmiFnGetEligiblePanelRefyearsetGroups,
            this.tsmiFnGetLDsityObjects,
            this.tsmiFnGetPlots,
            this.tsmiFnGetRefYearSetToPanelMappingForGroup});
            this.tsmiOtherGetFunctions.Name = "tsmiOtherGetFunctions";
            this.tsmiOtherGetFunctions.Size = new System.Drawing.Size(329, 22);
            this.tsmiOtherGetFunctions.Text = "tsmiOtherGetFunctions";
            // 
            // tsmiFnAddPlotTargetAttr
            // 
            this.tsmiFnAddPlotTargetAttr.Name = "tsmiFnAddPlotTargetAttr";
            this.tsmiFnAddPlotTargetAttr.Size = new System.Drawing.Size(413, 22);
            this.tsmiFnAddPlotTargetAttr.Text = "tsmiFnAddPlotTargetAttr";
            this.tsmiFnAddPlotTargetAttr.Click += new System.EventHandler(this.ItemFnAddPlotTargetAttr_Click);
            // 
            // tsmiFnGetAttributeDomainAD
            // 
            this.tsmiFnGetAttributeDomainAD.Name = "tsmiFnGetAttributeDomainAD";
            this.tsmiFnGetAttributeDomainAD.Size = new System.Drawing.Size(413, 22);
            this.tsmiFnGetAttributeDomainAD.Text = "tsmiFnGetAttributeDomainAD";
            this.tsmiFnGetAttributeDomainAD.Click += new System.EventHandler(this.ItemFnGetAttributeDomainAD_Click);
            // 
            // tsmiFnGetAttributeDomainSP
            // 
            this.tsmiFnGetAttributeDomainSP.Name = "tsmiFnGetAttributeDomainSP";
            this.tsmiFnGetAttributeDomainSP.Size = new System.Drawing.Size(413, 22);
            this.tsmiFnGetAttributeDomainSP.Text = "tsmiFnGetAttributeDomainSP";
            this.tsmiFnGetAttributeDomainSP.Click += new System.EventHandler(this.ItemFnGetAttributeDomainSP_Click);
            // 
            // tsmiFnGetAvailableRefYearSetToPanelForClassificationRuleIdAdc
            // 
            this.tsmiFnGetAvailableRefYearSetToPanelForClassificationRuleIdAdc.Name = "tsmiFnGetAvailableRefYearSetToPanelForClassificationRuleIdAdc";
            this.tsmiFnGetAvailableRefYearSetToPanelForClassificationRuleIdAdc.Size = new System.Drawing.Size(413, 22);
            this.tsmiFnGetAvailableRefYearSetToPanelForClassificationRuleIdAdc.Text = "tsmiFnGetAvailableRefYearSetToPanelForClassificationRuleIdAdc";
            this.tsmiFnGetAvailableRefYearSetToPanelForClassificationRuleIdAdc.Click += new System.EventHandler(this.ItemFnGetAvailableRefYearSetToPanelForClassificationRuleIdAdc_Click);
            // 
            // tsmiFnGetAvailableRefYearSetToPanelForClassificationRuleIdSpc
            // 
            this.tsmiFnGetAvailableRefYearSetToPanelForClassificationRuleIdSpc.Name = "tsmiFnGetAvailableRefYearSetToPanelForClassificationRuleIdSpc";
            this.tsmiFnGetAvailableRefYearSetToPanelForClassificationRuleIdSpc.Size = new System.Drawing.Size(413, 22);
            this.tsmiFnGetAvailableRefYearSetToPanelForClassificationRuleIdSpc.Text = "tsmiFnGetAvailableRefYearSetToPanelForClassificationRuleIdSpc";
            this.tsmiFnGetAvailableRefYearSetToPanelForClassificationRuleIdSpc.Click += new System.EventHandler(this.ItemFnGetAvailableRefYearSetToPanelForClassificationRuleIdSpc_Click);
            // 
            // tsmiFnGetCaseAdForCategorizationSetups
            // 
            this.tsmiFnGetCaseAdForCategorizationSetups.Name = "tsmiFnGetCaseAdForCategorizationSetups";
            this.tsmiFnGetCaseAdForCategorizationSetups.Size = new System.Drawing.Size(413, 22);
            this.tsmiFnGetCaseAdForCategorizationSetups.Text = "tsmiFnGetCaseAdForCategorizationSetups";
            this.tsmiFnGetCaseAdForCategorizationSetups.Click += new System.EventHandler(this.ItemFnGetCaseAdForCategorizationSetups_Click);
            // 
            // tsmiFnGetCategoryForClassificationRuleIdAdc
            // 
            this.tsmiFnGetCategoryForClassificationRuleIdAdc.Name = "tsmiFnGetCategoryForClassificationRuleIdAdc";
            this.tsmiFnGetCategoryForClassificationRuleIdAdc.Size = new System.Drawing.Size(413, 22);
            this.tsmiFnGetCategoryForClassificationRuleIdAdc.Text = "tsmiFnGetCategoryForClassificationRuleIdAdc";
            this.tsmiFnGetCategoryForClassificationRuleIdAdc.Click += new System.EventHandler(this.ItemFnGetCategoryForClassificationRuleIdAdc_Click);
            // 
            // tsmiFnGetCategoryForClassificationRuleIdSpc
            // 
            this.tsmiFnGetCategoryForClassificationRuleIdSpc.Name = "tsmiFnGetCategoryForClassificationRuleIdSpc";
            this.tsmiFnGetCategoryForClassificationRuleIdSpc.Size = new System.Drawing.Size(413, 22);
            this.tsmiFnGetCategoryForClassificationRuleIdSpc.Text = "tsmiFnGetCategoryForClassificationRuleIdSpc";
            this.tsmiFnGetCategoryForClassificationRuleIdSpc.Click += new System.EventHandler(this.ItemFnGetCategoryForClassificationRuleIdSpc_Click);
            // 
            // tsmiFnGetCategoryTypeForClassificationRuleIdAdc
            // 
            this.tsmiFnGetCategoryTypeForClassificationRuleIdAdc.Name = "tsmiFnGetCategoryTypeForClassificationRuleIdAdc";
            this.tsmiFnGetCategoryTypeForClassificationRuleIdAdc.Size = new System.Drawing.Size(413, 22);
            this.tsmiFnGetCategoryTypeForClassificationRuleIdAdc.Text = "tsmiFnGetCategoryTypeForClassificationRuleIdAdc";
            this.tsmiFnGetCategoryTypeForClassificationRuleIdAdc.Click += new System.EventHandler(this.ItemFnGetCategoryTypeForClassificationRuleIdAdc_Click);
            // 
            // tsmiFnGetCategoryTypeForClassificationRuleIdSpc
            // 
            this.tsmiFnGetCategoryTypeForClassificationRuleIdSpc.Name = "tsmiFnGetCategoryTypeForClassificationRuleIdSpc";
            this.tsmiFnGetCategoryTypeForClassificationRuleIdSpc.Size = new System.Drawing.Size(413, 22);
            this.tsmiFnGetCategoryTypeForClassificationRuleIdSpc.Text = "tsmiFnGetCategoryTypeForClassificationRuleIdSpc";
            this.tsmiFnGetCategoryTypeForClassificationRuleIdSpc.Click += new System.EventHandler(this.ItemFnGetCategoryTypeForClassificationRuleIdSpc_Click);
            // 
            // tsmiFnGetClassificationRuleForAdc
            // 
            this.tsmiFnGetClassificationRuleForAdc.Name = "tsmiFnGetClassificationRuleForAdc";
            this.tsmiFnGetClassificationRuleForAdc.Size = new System.Drawing.Size(413, 22);
            this.tsmiFnGetClassificationRuleForAdc.Text = "tsmiFnGetClassificationRuleForAdc";
            this.tsmiFnGetClassificationRuleForAdc.Click += new System.EventHandler(this.ItemFnGetClassificationRuleForAdc_Click);
            // 
            // tsmiFnGetClassificationRuleForSpc
            // 
            this.tsmiFnGetClassificationRuleForSpc.Name = "tsmiFnGetClassificationRuleForSpc";
            this.tsmiFnGetClassificationRuleForSpc.Size = new System.Drawing.Size(413, 22);
            this.tsmiFnGetClassificationRuleForSpc.Text = "tsmiFnGetClassificationRuleForSpc";
            this.tsmiFnGetClassificationRuleForSpc.Click += new System.EventHandler(this.ItemFnGetClassificationRuleForSpc_Click);
            // 
            // tsmiFnGetClassificationRuleForClassificationRuleIdAdc
            // 
            this.tsmiFnGetClassificationRuleForClassificationRuleIdAdc.Name = "tsmiFnGetClassificationRuleForClassificationRuleIdAdc";
            this.tsmiFnGetClassificationRuleForClassificationRuleIdAdc.Size = new System.Drawing.Size(413, 22);
            this.tsmiFnGetClassificationRuleForClassificationRuleIdAdc.Text = "tsmiFnGetClassificationRuleForClassificationRuleIdAdc";
            this.tsmiFnGetClassificationRuleForClassificationRuleIdAdc.Click += new System.EventHandler(this.ItemFnGetClassificationRuleForClassificationRuleIdAdc_Click);
            // 
            // tsmiFnGetClassificationRuleForClassificationRuleIdSpc
            // 
            this.tsmiFnGetClassificationRuleForClassificationRuleIdSpc.Name = "tsmiFnGetClassificationRuleForClassificationRuleIdSpc";
            this.tsmiFnGetClassificationRuleForClassificationRuleIdSpc.Size = new System.Drawing.Size(413, 22);
            this.tsmiFnGetClassificationRuleForClassificationRuleIdSpc.Text = "tsmiFnGetClassificationRuleForClassificationRuleIdSpc";
            this.tsmiFnGetClassificationRuleForClassificationRuleIdSpc.Click += new System.EventHandler(this.ItemFnGetClassificationRuleForClassificationRuleIdSpc_Click);
            // 
            // tsmiFnGetClassificationRuleId4Category
            // 
            this.tsmiFnGetClassificationRuleId4Category.Name = "tsmiFnGetClassificationRuleId4Category";
            this.tsmiFnGetClassificationRuleId4Category.Size = new System.Drawing.Size(413, 22);
            this.tsmiFnGetClassificationRuleId4Category.Text = "tsmiFnGetClassificationRuleId4Category";
            this.tsmiFnGetClassificationRuleId4Category.Click += new System.EventHandler(this.ItemFnGetClassificationRuleId4Category_Click);
            // 
            // tsmiFnGetClassificationRules4CategorizationSetups
            // 
            this.tsmiFnGetClassificationRules4CategorizationSetups.Name = "tsmiFnGetClassificationRules4CategorizationSetups";
            this.tsmiFnGetClassificationRules4CategorizationSetups.Size = new System.Drawing.Size(413, 22);
            this.tsmiFnGetClassificationRules4CategorizationSetups.Text = "tsmiFnGetClassificationRules4CategorizationSetups";
            this.tsmiFnGetClassificationRules4CategorizationSetups.Click += new System.EventHandler(this.ItemFnGetClassificationRules4CategorizationSetups_Click);
            // 
            // tsmiFnGetClassificationRulesId4Categories
            // 
            this.tsmiFnGetClassificationRulesId4Categories.Name = "tsmiFnGetClassificationRulesId4Categories";
            this.tsmiFnGetClassificationRulesId4Categories.Size = new System.Drawing.Size(413, 22);
            this.tsmiFnGetClassificationRulesId4Categories.Text = "tsmiFnGetClassificationRulesId4Categories";
            this.tsmiFnGetClassificationRulesId4Categories.Click += new System.EventHandler(this.ItemFnGetClassificationRulesId4Categories_Click);
            // 
            // tsmiFnGetEligiblePanelRefYearSetCombinations
            // 
            this.tsmiFnGetEligiblePanelRefYearSetCombinations.Name = "tsmiFnGetEligiblePanelRefYearSetCombinations";
            this.tsmiFnGetEligiblePanelRefYearSetCombinations.Size = new System.Drawing.Size(413, 22);
            this.tsmiFnGetEligiblePanelRefYearSetCombinations.Text = "tsmiFnGetEligiblePanelRefYearSetCombinations";
            this.tsmiFnGetEligiblePanelRefYearSetCombinations.Click += new System.EventHandler(this.ItemFnGetEligiblePanelRefYearSetCombinations_Click);
            // 
            // tsmiFnGetEligiblePanelRefyearsetGroups
            // 
            this.tsmiFnGetEligiblePanelRefyearsetGroups.Name = "tsmiFnGetEligiblePanelRefyearsetGroups";
            this.tsmiFnGetEligiblePanelRefyearsetGroups.Size = new System.Drawing.Size(413, 22);
            this.tsmiFnGetEligiblePanelRefyearsetGroups.Text = "tsmiFnGetEligiblePanelRefyearsetGroups";
            this.tsmiFnGetEligiblePanelRefyearsetGroups.Click += new System.EventHandler(this.ItemFnGetEligiblePanelRefyearsetGroups_Click);
            // 
            // tsmiFnGetLDsityObjects
            // 
            this.tsmiFnGetLDsityObjects.Name = "tsmiFnGetLDsityObjects";
            this.tsmiFnGetLDsityObjects.Size = new System.Drawing.Size(413, 22);
            this.tsmiFnGetLDsityObjects.Text = "tsmiFnGetLDsityObjects";
            this.tsmiFnGetLDsityObjects.Click += new System.EventHandler(this.ItemFnGetLDsityObjects_Click);
            // 
            // tsmiFnGetPlots
            // 
            this.tsmiFnGetPlots.Name = "tsmiFnGetPlots";
            this.tsmiFnGetPlots.Size = new System.Drawing.Size(413, 22);
            this.tsmiFnGetPlots.Text = "tsmiFnGetPlots";
            this.tsmiFnGetPlots.Click += new System.EventHandler(this.ItemFnGetPlots_Click);
            // 
            // tsmiFnGetRefYearSetToPanelMappingForGroup
            // 
            this.tsmiFnGetRefYearSetToPanelMappingForGroup.Name = "tsmiFnGetRefYearSetToPanelMappingForGroup";
            this.tsmiFnGetRefYearSetToPanelMappingForGroup.Size = new System.Drawing.Size(413, 22);
            this.tsmiFnGetRefYearSetToPanelMappingForGroup.Text = "tsmiFnGetRefYearSetToPanelMappingForGroup";
            this.tsmiFnGetRefYearSetToPanelMappingForGroup.Click += new System.EventHandler(this.ItemFnGetRefYearSetToPanelMappingForGroup_Click);
            // 
            // tsmiETLFunctions
            // 
            this.tsmiETLFunctions.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiFnEtlGetExportConnections,
            this.tsmiFnEtlGetTargetVariable,
            this.tsmiFnEtlGetTargetVariableExtended,
            this.tsmiFnEtlGetTargetVariableAggregated,
            this.tsmiControlTargetVariableSelector});
            this.tsmiETLFunctions.Name = "tsmiETLFunctions";
            this.tsmiETLFunctions.Size = new System.Drawing.Size(329, 22);
            this.tsmiETLFunctions.Text = "tsmiETLFunctions";
            // 
            // tsmiFnEtlGetExportConnections
            // 
            this.tsmiFnEtlGetExportConnections.Name = "tsmiFnEtlGetExportConnections";
            this.tsmiFnEtlGetExportConnections.Size = new System.Drawing.Size(276, 22);
            this.tsmiFnEtlGetExportConnections.Text = "tsmiFnEtlGetExportConnections";
            this.tsmiFnEtlGetExportConnections.Click += new System.EventHandler(this.ItemFnEtlGetExportConnections_Click);
            // 
            // tsmiFnEtlGetTargetVariable
            // 
            this.tsmiFnEtlGetTargetVariable.Name = "tsmiFnEtlGetTargetVariable";
            this.tsmiFnEtlGetTargetVariable.Size = new System.Drawing.Size(276, 22);
            this.tsmiFnEtlGetTargetVariable.Text = "tsmiFnEtlGetTargetVariable";
            this.tsmiFnEtlGetTargetVariable.Click += new System.EventHandler(this.ItemFnEtlGetTargetVariable_Click);
            // 
            // tsmiFnEtlGetTargetVariableExtended
            // 
            this.tsmiFnEtlGetTargetVariableExtended.Name = "tsmiFnEtlGetTargetVariableExtended";
            this.tsmiFnEtlGetTargetVariableExtended.Size = new System.Drawing.Size(276, 22);
            this.tsmiFnEtlGetTargetVariableExtended.Text = "tsmiFnEtlGetTargetVariableExtended";
            this.tsmiFnEtlGetTargetVariableExtended.Click += new System.EventHandler(this.ItemFnEtlGetTargetVariableExtended_Click);
            // 
            // tsmiFnEtlGetTargetVariableAggregated
            // 
            this.tsmiFnEtlGetTargetVariableAggregated.Name = "tsmiFnEtlGetTargetVariableAggregated";
            this.tsmiFnEtlGetTargetVariableAggregated.Size = new System.Drawing.Size(276, 22);
            this.tsmiFnEtlGetTargetVariableAggregated.Text = "tsmiFnEtlGetTargetVariableAggregated";
            this.tsmiFnEtlGetTargetVariableAggregated.Click += new System.EventHandler(this.ItemFnEtlGetTargetVariableAggregated_Click);
            // 
            // tsmiControlTargetVariableSelector
            // 
            this.tsmiControlTargetVariableSelector.Name = "tsmiControlTargetVariableSelector";
            this.tsmiControlTargetVariableSelector.Size = new System.Drawing.Size(276, 22);
            this.tsmiControlTargetVariableSelector.Text = "tsmiControlTargetVariableSelector";
            this.tsmiControlTargetVariableSelector.Click += new System.EventHandler(this.ItemControlTargetVariableSelector_Click);
            // 
            // tsmiFnGetAreaDomain
            // 
            this.tsmiFnGetAreaDomain.Name = "tsmiFnGetAreaDomain";
            this.tsmiFnGetAreaDomain.Size = new System.Drawing.Size(329, 22);
            this.tsmiFnGetAreaDomain.Text = "tsmiFnGetAreaDomain";
            this.tsmiFnGetAreaDomain.Click += new System.EventHandler(this.ItemFnGetAreaDomain_Click);
            // 
            // tsmiFnGetAreaDomainCategory
            // 
            this.tsmiFnGetAreaDomainCategory.BackColor = System.Drawing.SystemColors.Control;
            this.tsmiFnGetAreaDomainCategory.Name = "tsmiFnGetAreaDomainCategory";
            this.tsmiFnGetAreaDomainCategory.Size = new System.Drawing.Size(329, 22);
            this.tsmiFnGetAreaDomainCategory.Text = "tsmiFnGetAreaDomainCategory";
            this.tsmiFnGetAreaDomainCategory.Click += new System.EventHandler(this.ItemFnGetAreaDomainCategory_Click);
            // 
            // tsmiFnGetAreaDomainCategoryForDomain
            // 
            this.tsmiFnGetAreaDomainCategoryForDomain.Name = "tsmiFnGetAreaDomainCategoryForDomain";
            this.tsmiFnGetAreaDomainCategoryForDomain.Size = new System.Drawing.Size(329, 22);
            this.tsmiFnGetAreaDomainCategoryForDomain.Text = "tsmiFnGetAreaDomainCategoryForDomain";
            this.tsmiFnGetAreaDomainCategoryForDomain.Click += new System.EventHandler(this.ItemFnGetAreaDomainCategoryForDomain_Click);
            // 
            // tsmiFnGetArealOrPopulation
            // 
            this.tsmiFnGetArealOrPopulation.Name = "tsmiFnGetArealOrPopulation";
            this.tsmiFnGetArealOrPopulation.Size = new System.Drawing.Size(329, 22);
            this.tsmiFnGetArealOrPopulation.Text = "tsmiFnGetArealOrPopulation";
            this.tsmiFnGetArealOrPopulation.Click += new System.EventHandler(this.ItemFnGetArealOrPopulation_Click);
            // 
            // tsmiFnGetDefinitionVariant
            // 
            this.tsmiFnGetDefinitionVariant.Name = "tsmiFnGetDefinitionVariant";
            this.tsmiFnGetDefinitionVariant.Size = new System.Drawing.Size(329, 22);
            this.tsmiFnGetDefinitionVariant.Text = "tsmiFnGetDefinitionVariant";
            this.tsmiFnGetDefinitionVariant.Click += new System.EventHandler(this.ItemFnGetDefinitionVariant_Click);
            // 
            // tsmiFnGetDefinitionVariantForLDsity
            // 
            this.tsmiFnGetDefinitionVariantForLDsity.Name = "tsmiFnGetDefinitionVariantForLDsity";
            this.tsmiFnGetDefinitionVariantForLDsity.Size = new System.Drawing.Size(329, 22);
            this.tsmiFnGetDefinitionVariantForLDsity.Text = "tsmiFnGetDefinitionVariantForLDsity";
            this.tsmiFnGetDefinitionVariantForLDsity.Click += new System.EventHandler(this.ItemFnGetDefinitionVariantForLDsity);
            // 
            // tsmiFnGetHierarchyAdc
            // 
            this.tsmiFnGetHierarchyAdc.Name = "tsmiFnGetHierarchyAdc";
            this.tsmiFnGetHierarchyAdc.Size = new System.Drawing.Size(329, 22);
            this.tsmiFnGetHierarchyAdc.Text = "tsmiFnGetHierarchyAdc";
            this.tsmiFnGetHierarchyAdc.Click += new System.EventHandler(this.ItemFnGetHierarchyAdc_Click);
            // 
            // tsmiFnGetHierarchySpc
            // 
            this.tsmiFnGetHierarchySpc.Name = "tsmiFnGetHierarchySpc";
            this.tsmiFnGetHierarchySpc.Size = new System.Drawing.Size(329, 22);
            this.tsmiFnGetHierarchySpc.Text = "tsmiFnGetHierarchySpc";
            this.tsmiFnGetHierarchySpc.Click += new System.EventHandler(this.ItemFnGetHierarchySpc_Click);
            // 
            // tsmiFnGetLDsity
            // 
            this.tsmiFnGetLDsity.Name = "tsmiFnGetLDsity";
            this.tsmiFnGetLDsity.Size = new System.Drawing.Size(329, 22);
            this.tsmiFnGetLDsity.Text = "tsmiFnGetLDsity";
            this.tsmiFnGetLDsity.Click += new System.EventHandler(this.ItemFnGetLDsity_Click);
            // 
            // tsmiFnGetLDsityForObject
            // 
            this.tsmiFnGetLDsityForObject.Name = "tsmiFnGetLDsityForObject";
            this.tsmiFnGetLDsityForObject.Size = new System.Drawing.Size(329, 22);
            this.tsmiFnGetLDsityForObject.Text = "tsmiFnGetLDsityForObject";
            this.tsmiFnGetLDsityForObject.Click += new System.EventHandler(this.ItemFnGetLDsityForObject_Click);
            // 
            // tsmiFnGetLDsityObject
            // 
            this.tsmiFnGetLDsityObject.Name = "tsmiFnGetLDsityObject";
            this.tsmiFnGetLDsityObject.Size = new System.Drawing.Size(329, 22);
            this.tsmiFnGetLDsityObject.Text = "tsmiFnGetLDsityObject";
            this.tsmiFnGetLDsityObject.Click += new System.EventHandler(this.ItemFnGetLDsityObject_Click);
            // 
            // tsmiFnGetLDsityObjectForADSP
            // 
            this.tsmiFnGetLDsityObjectForADSP.Name = "tsmiFnGetLDsityObjectForADSP";
            this.tsmiFnGetLDsityObjectForADSP.Size = new System.Drawing.Size(329, 22);
            this.tsmiFnGetLDsityObjectForADSP.Text = "tsmiFnGetLDsityObjectForADSP";
            this.tsmiFnGetLDsityObjectForADSP.Click += new System.EventHandler(this.ItemFnGetLDsityObjectForADSP_Click);
            // 
            // tsmiFnGetLDsityObjectForLDsityObjectADSP
            // 
            this.tsmiFnGetLDsityObjectForLDsityObjectADSP.Name = "tsmiFnGetLDsityObjectForLDsityObjectADSP";
            this.tsmiFnGetLDsityObjectForLDsityObjectADSP.Size = new System.Drawing.Size(329, 22);
            this.tsmiFnGetLDsityObjectForLDsityObjectADSP.Text = "tsmiFnGetLDsityObjectForLDsityObjectADSP";
            this.tsmiFnGetLDsityObjectForLDsityObjectADSP.Click += new System.EventHandler(this.ItemFnGetLDsityObjectForLDsityObjectADSP_Click);
            // 
            // tsmiFnGetLDsityObjectForLDsityObjectGroup
            // 
            this.tsmiFnGetLDsityObjectForLDsityObjectGroup.Name = "tsmiFnGetLDsityObjectForLDsityObjectGroup";
            this.tsmiFnGetLDsityObjectForLDsityObjectGroup.Size = new System.Drawing.Size(329, 22);
            this.tsmiFnGetLDsityObjectForLDsityObjectGroup.Text = "tsmiFnGetLDsityObjectForLDsityObjectGroup";
            this.tsmiFnGetLDsityObjectForLDsityObjectGroup.Click += new System.EventHandler(this.ItemFnGetLDsityObjectForLDsityObjectGroup_Click);
            // 
            // tsmiFnGetLDsityObjectGroup
            // 
            this.tsmiFnGetLDsityObjectGroup.Name = "tsmiFnGetLDsityObjectGroup";
            this.tsmiFnGetLDsityObjectGroup.Size = new System.Drawing.Size(329, 22);
            this.tsmiFnGetLDsityObjectGroup.Text = "tsmiFnGetLDsityObjectGroup";
            this.tsmiFnGetLDsityObjectGroup.Click += new System.EventHandler(this.ItemFnGetLDsityObjectGroup_Click);
            // 
            // tsmiFnGetLDsityObjectType
            // 
            this.tsmiFnGetLDsityObjectType.Name = "tsmiFnGetLDsityObjectType";
            this.tsmiFnGetLDsityObjectType.Size = new System.Drawing.Size(329, 22);
            this.tsmiFnGetLDsityObjectType.Text = "tsmiFnGetLDsityObjectType";
            this.tsmiFnGetLDsityObjectType.Click += new System.EventHandler(this.ItemFnGetLDsityObjectType_Click);
            // 
            // tsmiFnGetLDsitySubPopForObject
            // 
            this.tsmiFnGetLDsitySubPopForObject.Name = "tsmiFnGetLDsitySubPopForObject";
            this.tsmiFnGetLDsitySubPopForObject.Size = new System.Drawing.Size(329, 22);
            this.tsmiFnGetLDsitySubPopForObject.Text = "tsmiFnGetLDsitySubPopForObject";
            this.tsmiFnGetLDsitySubPopForObject.Click += new System.EventHandler(this.ItemFnGetLDsitySubPopForObject_Click);
            // 
            // tsmiFnGetPanelRefYearSet
            // 
            this.tsmiFnGetPanelRefYearSet.Name = "tsmiFnGetPanelRefYearSet";
            this.tsmiFnGetPanelRefYearSet.Size = new System.Drawing.Size(329, 22);
            this.tsmiFnGetPanelRefYearSet.Text = "tsmiFnGetPanelRefYearSet";
            this.tsmiFnGetPanelRefYearSet.Click += new System.EventHandler(this.ItemFnGetPanelRefYearSet_Click);
            // 
            // tsmiFnGetPanelRefYearSetGroup
            // 
            this.tsmiFnGetPanelRefYearSetGroup.Name = "tsmiFnGetPanelRefYearSetGroup";
            this.tsmiFnGetPanelRefYearSetGroup.Size = new System.Drawing.Size(329, 22);
            this.tsmiFnGetPanelRefYearSetGroup.Text = "tsmiFnGetPanelRefYearSetGroup";
            this.tsmiFnGetPanelRefYearSetGroup.Click += new System.EventHandler(this.ItemFnGetPanelRefYearSetGroup_Click);
            // 
            // tsmiFnGetStateOrChange
            // 
            this.tsmiFnGetStateOrChange.Name = "tsmiFnGetStateOrChange";
            this.tsmiFnGetStateOrChange.Size = new System.Drawing.Size(329, 22);
            this.tsmiFnGetStateOrChange.Text = "tsmiFnGetStateOrChange";
            this.tsmiFnGetStateOrChange.Click += new System.EventHandler(this.ItemFnGetStateOrChange_Click);
            // 
            // tsmiFnGetSupPopulation
            // 
            this.tsmiFnGetSupPopulation.Name = "tsmiFnGetSupPopulation";
            this.tsmiFnGetSupPopulation.Size = new System.Drawing.Size(329, 22);
            this.tsmiFnGetSupPopulation.Text = "tsmiFnGetSupPopulation";
            this.tsmiFnGetSupPopulation.Click += new System.EventHandler(this.ItemFnGetSupPopulation_Click);
            // 
            // tsmiFnGetSupPopulationCategory
            // 
            this.tsmiFnGetSupPopulationCategory.Name = "tsmiFnGetSupPopulationCategory";
            this.tsmiFnGetSupPopulationCategory.Size = new System.Drawing.Size(329, 22);
            this.tsmiFnGetSupPopulationCategory.Text = "tsmiFnGetSupPopulationCategory";
            this.tsmiFnGetSupPopulationCategory.Click += new System.EventHandler(this.ItemFnGetSupPopulationCategory_Click);
            // 
            // tsmiFnGetSubPopulationCategoryForPopulation
            // 
            this.tsmiFnGetSubPopulationCategoryForPopulation.Name = "tsmiFnGetSubPopulationCategoryForPopulation";
            this.tsmiFnGetSubPopulationCategoryForPopulation.Size = new System.Drawing.Size(329, 22);
            this.tsmiFnGetSubPopulationCategoryForPopulation.Text = "tsmiFnGetSubPopulationCategoryForPopulation";
            this.tsmiFnGetSubPopulationCategoryForPopulation.Click += new System.EventHandler(this.ItemFnGetSubPopulationCategoryForPopulation_Click);
            // 
            // tsmiFnGetTargetVariableAgg
            // 
            this.tsmiFnGetTargetVariableAgg.Name = "tsmiFnGetTargetVariableAgg";
            this.tsmiFnGetTargetVariableAgg.Size = new System.Drawing.Size(329, 22);
            this.tsmiFnGetTargetVariableAgg.Text = "tsmiFnGetTargetVariableAgg";
            this.tsmiFnGetTargetVariableAgg.Click += new System.EventHandler(this.ItemFnGetTargetVariableAgg_Click);
            // 
            // tsmiFnGetTargetVariable
            // 
            this.tsmiFnGetTargetVariable.Name = "tsmiFnGetTargetVariable";
            this.tsmiFnGetTargetVariable.Size = new System.Drawing.Size(329, 22);
            this.tsmiFnGetTargetVariable.Text = "tsmiFnGetTargetVariable";
            this.tsmiFnGetTargetVariable.Click += new System.EventHandler(this.ItemFnGetTargetVariable_Click);
            // 
            // tsmiFnGetUnitOfMeasure
            // 
            this.tsmiFnGetUnitOfMeasure.Name = "tsmiFnGetUnitOfMeasure";
            this.tsmiFnGetUnitOfMeasure.Size = new System.Drawing.Size(329, 22);
            this.tsmiFnGetUnitOfMeasure.Text = "tsmiFnGetUnitOfMeasure";
            this.tsmiFnGetUnitOfMeasure.Click += new System.EventHandler(this.ItemFnGetUnitOfMeasure_Click);
            // 
            // tsmiFnGetUnitOfMeasureForLDsityObjectGroup
            // 
            this.tsmiFnGetUnitOfMeasureForLDsityObjectGroup.Name = "tsmiFnGetUnitOfMeasureForLDsityObjectGroup";
            this.tsmiFnGetUnitOfMeasureForLDsityObjectGroup.Size = new System.Drawing.Size(329, 22);
            this.tsmiFnGetUnitOfMeasureForLDsityObjectGroup.Text = "tsmiFnGetUnitOfMeasureForLDsityObjectGroup";
            this.tsmiFnGetUnitOfMeasureForLDsityObjectGroup.Click += new System.EventHandler(this.ItemFnGetUnitOfMeasureForLDsityObjectGroup_Click);
            // 
            // tsmiFnGetVersion
            // 
            this.tsmiFnGetVersion.Name = "tsmiFnGetVersion";
            this.tsmiFnGetVersion.Size = new System.Drawing.Size(329, 22);
            this.tsmiFnGetVersion.Text = "tsmiFnGetVersion";
            this.tsmiFnGetVersion.Click += new System.EventHandler(this.ItemFnGetVersion_Click);
            // 
            // tsmiETL
            // 
            this.tsmiETL.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiExtractData,
            this.tsmiGetDifferences});
            this.tsmiETL.Name = "tsmiETL";
            this.tsmiETL.Size = new System.Drawing.Size(60, 20);
            this.tsmiETL.Text = "tsmiETL";
            // 
            // tsmiExtractData
            // 
            this.tsmiExtractData.Name = "tsmiExtractData";
            this.tsmiExtractData.Size = new System.Drawing.Size(174, 22);
            this.tsmiExtractData.Text = "tsmiExtractData";
            // 
            // tsmiGetDifferences
            // 
            this.tsmiGetDifferences.Name = "tsmiGetDifferences";
            this.tsmiGetDifferences.Size = new System.Drawing.Size(174, 22);
            this.tsmiGetDifferences.Text = "tsmiGetDifferences";
            // 
            // tlpMain
            // 
            this.tlpMain.ColumnCount = 1;
            this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpMain.Controls.Add(this.tlpButtons, 0, 1);
            this.tlpMain.Controls.Add(this.grpMain, 0, 0);
            this.tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpMain.Location = new System.Drawing.Point(0, 24);
            this.tlpMain.Name = "tlpMain";
            this.tlpMain.RowCount = 2;
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlpMain.Size = new System.Drawing.Size(944, 477);
            this.tlpMain.TabIndex = 5;
            // 
            // tlpButtons
            // 
            this.tlpButtons.BackColor = System.Drawing.SystemColors.Control;
            this.tlpButtons.ColumnCount = 2;
            this.tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            this.tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpButtons.Controls.Add(this.pnlClose, 1, 0);
            this.tlpButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpButtons.Location = new System.Drawing.Point(0, 437);
            this.tlpButtons.Margin = new System.Windows.Forms.Padding(0);
            this.tlpButtons.Name = "tlpButtons";
            this.tlpButtons.RowCount = 1;
            this.tlpButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpButtons.Size = new System.Drawing.Size(944, 40);
            this.tlpButtons.TabIndex = 30;
            // 
            // pnlClose
            // 
            this.pnlClose.Controls.Add(this.btnClose);
            this.pnlClose.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlClose.Location = new System.Drawing.Point(784, 0);
            this.pnlClose.Margin = new System.Windows.Forms.Padding(0);
            this.pnlClose.Name = "pnlClose";
            this.pnlClose.Padding = new System.Windows.Forms.Padding(5);
            this.pnlClose.Size = new System.Drawing.Size(160, 40);
            this.pnlClose.TabIndex = 12;
            // 
            // btnClose
            // 
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.btnClose.Location = new System.Drawing.Point(5, 5);
            this.btnClose.Margin = new System.Windows.Forms.Padding(0);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(150, 30);
            this.btnClose.TabIndex = 13;
            this.btnClose.Text = "btnClose";
            this.btnClose.UseVisualStyleBackColor = true;
            // 
            // grpMain
            // 
            this.grpMain.Controls.Add(this.splitMain);
            this.grpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpMain.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.grpMain.ForeColor = System.Drawing.SystemColors.ControlText;
            this.grpMain.Location = new System.Drawing.Point(3, 3);
            this.grpMain.Name = "grpMain";
            this.grpMain.Size = new System.Drawing.Size(938, 431);
            this.grpMain.TabIndex = 5;
            this.grpMain.TabStop = false;
            // 
            // splitMain
            // 
            this.splitMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitMain.Location = new System.Drawing.Point(3, 18);
            this.splitMain.Name = "splitMain";
            this.splitMain.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitMain.Panel1
            // 
            this.splitMain.Panel1.Controls.Add(this.pnlData);
            // 
            // splitMain.Panel2
            // 
            this.splitMain.Panel2.Controls.Add(this.txtSQL);
            this.splitMain.Size = new System.Drawing.Size(932, 410);
            this.splitMain.SplitterDistance = 204;
            this.splitMain.SplitterWidth = 1;
            this.splitMain.TabIndex = 11;
            // 
            // pnlData
            // 
            this.pnlData.AutoScroll = true;
            this.pnlData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlData.Location = new System.Drawing.Point(0, 0);
            this.pnlData.Name = "pnlData";
            this.pnlData.Size = new System.Drawing.Size(932, 204);
            this.pnlData.TabIndex = 3;
            // 
            // txtSQL
            // 
            this.txtSQL.BackColor = System.Drawing.SystemColors.Window;
            this.txtSQL.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtSQL.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtSQL.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.txtSQL.Location = new System.Drawing.Point(0, 0);
            this.txtSQL.Name = "txtSQL";
            this.txtSQL.ReadOnly = true;
            this.txtSQL.Size = new System.Drawing.Size(932, 205);
            this.txtSQL.TabIndex = 0;
            this.txtSQL.Text = "";
            // 
            // FormTargetData
            // 
            this.AcceptButton = this.btnClose;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(944, 501);
            this.Controls.Add(this.tlpMain);
            this.Controls.Add(this.msMain);
            this.MainMenuStrip = this.msMain;
            this.Name = "FormTargetData";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Data from schema target_data";
            this.msMain.ResumeLayout(false);
            this.msMain.PerformLayout();
            this.tlpMain.ResumeLayout(false);
            this.tlpButtons.ResumeLayout(false);
            this.pnlClose.ResumeLayout(false);
            this.grpMain.ResumeLayout(false);
            this.splitMain.Panel1.ResumeLayout(false);
            this.splitMain.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitMain)).EndInit();
            this.splitMain.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip msMain;
        private System.Windows.Forms.ToolStripMenuItem tsmiDataTables;
        private System.Windows.Forms.ToolStripMenuItem tsmiTADCHierarchy;
        private System.Windows.Forms.ToolStripMenuItem tsmiTAvailableDataSets;
        private System.Windows.Forms.ToolStripMenuItem tsmiTLDsityValues;
        private System.Windows.Forms.ToolStripMenuItem tsmiTPanelRefYearSetGroup;
        private System.Windows.Forms.ToolStripMenuItem tsmiTSPCHierarchy;
        private System.Windows.Forms.ToolStripMenuItem tsmiGetFunctions;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnGetAreaDomainCategory;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnGetDefinitionVariant;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnGetStateOrChange;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnGetSupPopulationCategory;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnGetTargetVariableAgg;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnGetUnitOfMeasure;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnGetLDsity;
        private System.Windows.Forms.ToolStripMenuItem tsmiETL;
        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.GroupBox grpMain;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnGetArealOrPopulation;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnGetLDsityForObject;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnGetLDsityObject;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnGetLDsityObjectGroup;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnGetLDsityObjectType;
        private System.Windows.Forms.SplitContainer splitMain;
        private System.Windows.Forms.Panel pnlData;
        private System.Windows.Forms.RichTextBox txtSQL;
        private System.Windows.Forms.ToolStripMenuItem tsmiGetDifferences;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnGetLDsityObjectForLDsityObjectGroup;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnGetAreaDomain;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnGetSupPopulation;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnGetLDsityObjectForADSP;
        private System.Windows.Forms.ToolStripMenuItem tsmiTCategorizationSetup;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnGetUnitOfMeasureForLDsityObjectGroup;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnGetVersion;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnGetDefinitionVariantForLDsity;
        private System.Windows.Forms.ToolStripMenuItem tsmiCheckFunctions;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnCheckClassificationRuleSyntax;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnCheckClassificationRule;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnCheckClassificationRules;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnGetPanelRefYearSet;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnGetLDsityObjectForLDsityObjectADSP;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnGetHierarchyAdc;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnGetHierarchySpc;
        private System.Windows.Forms.ToolStripMenuItem tsmiLookupTables;
        private System.Windows.Forms.ToolStripMenuItem tsmiCAreaDomain;
        private System.Windows.Forms.ToolStripMenuItem tsmiCAreaDomainCategory;
        private System.Windows.Forms.ToolStripMenuItem tsmiCArealOrPopulation;
        private System.Windows.Forms.ToolStripMenuItem tsmiCDefinitionVariant;
        private System.Windows.Forms.ToolStripMenuItem tsmiCLDsity;
        private System.Windows.Forms.ToolStripMenuItem tsmiCLDsityObject;
        private System.Windows.Forms.ToolStripMenuItem tsmiCLDsityObjectGroup;
        private System.Windows.Forms.ToolStripMenuItem tsmiCLDsityObjectType;
        private System.Windows.Forms.ToolStripMenuItem tsmiCPanelRefYearSetGroup;
        private System.Windows.Forms.ToolStripMenuItem tsmiCStateOrChange;
        private System.Windows.Forms.ToolStripMenuItem tsmiCSubPopulation;
        private System.Windows.Forms.ToolStripMenuItem tsmiCSubPopulationCategory;
        private System.Windows.Forms.ToolStripMenuItem tsmiCTargetVariable;
        private System.Windows.Forms.ToolStripMenuItem tsmiCUnitOfMeasure;
        private System.Windows.Forms.ToolStripMenuItem tsmiCVersion;
        private System.Windows.Forms.ToolStripMenuItem tsmiMappingTables;
        private System.Windows.Forms.ToolStripMenuItem tsmiCMADCToClassificationRule;
        private System.Windows.Forms.ToolStripMenuItem tsmiCMADCToClassRuleToPanelRefYearSet;
        private System.Windows.Forms.ToolStripMenuItem tsmiCMLDsityObjectToLDsityObjectGroup;
        private System.Windows.Forms.ToolStripMenuItem tsmiCMLDsityToPanelRefYearSetVersion;
        private System.Windows.Forms.ToolStripMenuItem tsmiCMLDsityToTargetToCategorizationSetup;
        private System.Windows.Forms.ToolStripMenuItem tsmiCMLDsityToTargetVariable;
        private System.Windows.Forms.ToolStripMenuItem tsmiCMSPCToClassificationRule;
        private System.Windows.Forms.ToolStripMenuItem tsmiCMSPCToClassRuleToPanelRefYearSet;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnGetLDsitySubPopForObject;
        private System.Windows.Forms.ToolStripMenuItem tsmiOtherGetFunctions;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnAddPlotTargetAttr;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnGetAttributeDomainAD;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnGetAttributeDomainSP;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnGetCaseAdForCategorizationSetups;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnGetCategoryForClassificationRuleIdAdc;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnGetCategoryForClassificationRuleIdSpc;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnGetClassificationRuleForAdc;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnGetClassificationRuleForSpc;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnGetClassificationRuleForClassificationRuleIdAdc;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnGetClassificationRuleForClassificationRuleIdSpc;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnGetClassificationRuleId4Category;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnGetClassificationRules4CategorizationSetups;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnGetClassificationRulesId4Categories;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnGetLDsityObjects;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnGetPlots;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnGetCategoryTypeForClassificationRuleIdAdc;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnGetCategoryTypeForClassificationRuleIdSpc;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnGetPanelRefYearSetGroup;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnGetAvailableRefYearSetToPanelForClassificationRuleIdAdc;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnGetAvailableRefYearSetToPanelForClassificationRuleIdSpc;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnGetEligiblePanelRefYearSetCombinations;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnGetEligiblePanelRefyearsetGroups;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnGetRefYearSetToPanelMappingForGroup;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnCheckAdcRuleHasPanelRef;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnCheckSpcRuleHasPanelRef;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnGetAreaDomainCategoryForDomain;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnGetSubPopulationCategoryForPopulation;
        private System.Windows.Forms.ToolStripMenuItem tsmiTETLAreaDomain;
        private System.Windows.Forms.ToolStripMenuItem tsmiTETLAreaDomainCategory;
        private System.Windows.Forms.ToolStripMenuItem tsmiTETLLog;
        private System.Windows.Forms.ToolStripMenuItem tsmiTETLSubPopulation;
        private System.Windows.Forms.ToolStripMenuItem tsmiTETLSubPopulationCategory;
        private System.Windows.Forms.ToolStripMenuItem tsmiTETLTargetVariable;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnGetTargetVariable;
        private System.Windows.Forms.ToolStripMenuItem tsmiTExportConnection;
        private System.Windows.Forms.ToolStripMenuItem tsmiETLFunctions;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnEtlGetExportConnections;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnEtlGetTargetVariable;
        private System.Windows.Forms.ToolStripMenuItem tsmiCClassificationType;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnEtlGetTargetVariableExtended;
        private System.Windows.Forms.ToolStripMenuItem tsmiFnEtlGetTargetVariableAggregated;
        private System.Windows.Forms.ToolStripMenuItem tsmiExtractData;
        private System.Windows.Forms.TableLayoutPanel tlpButtons;
        private System.Windows.Forms.Panel pnlClose;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.ToolStripMenuItem tsmiControlTargetVariableSelector;
    }

}