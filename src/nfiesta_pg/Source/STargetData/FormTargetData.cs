﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi.NfiEstaPg.TargetData
{

    /// <summary>
    /// <para lang="cs">Formulář zobrazení dat extenze nfiesta_target_data</para>
    /// <para lang="en">Form for displaying nfiesta_target_data extension data</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    public partial class FormTargetData
        : Form, INfiEstaControl
    {

        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Omezení počtu řádků</para>
        /// <para lang="en">Row count limit</para>
        /// </summary>
        private Nullable<int> limit;

        private string msgDataExportComplete = String.Empty;
        private string msgNoDifferencesFound = String.Empty;

        #endregion Private Fields


        #region Contructor

        /// <summary>
        /// <para lang="cs">Konstruktor formuláře</para>
        /// <para lang="en">Form constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        public FormTargetData(Control controlOwner)
        {
            InitializeComponent();
            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                if (controlOwner is not INfiEstaControl)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(INfiEstaControl)}.",
                        paramName: nameof(ControlOwner));
                }

                return controlOwner;
            }
            set
            {
                if (value is not INfiEstaControl)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(INfiEstaControl)}.",
                        paramName: nameof(ControlOwner));
                }

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables (read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        #endregion Common Properties


        #region Properties

        /// <summary>
        /// <para lang="cs">Omezení počtu řádků</para>
        /// <para lang="en">Row count limit</para>
        /// </summary>
        public Nullable<int> Limit
        {
            get
            {
                return limit;
            }
            set
            {
                limit = value;
            }
        }

        #endregion Properties


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace formuláře</para>
        /// <para lang="en">Initializing the form</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník formuláře</para>
        /// <para lang="en">Form owner</para>
        /// </param>
        private void Initialize(Control controlOwner)
        {
            ControlOwner = controlOwner;
            Limit = null;

            InitializeLabels();

            btnClose.Click += new EventHandler(
                (sender, e) =>
                {
                    DialogResult = DialogResult.Cancel;
                    Close();
                });

            tsmiCAreaDomain.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.TargetData.TDAreaDomainList(database: Database),
                        columns: ZaJi.NfiEstaPg.TargetData.TDAreaDomainList.Cols);
                });

            tsmiCAreaDomainCategory.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.TargetData.TDAreaDomainCategoryList(database: Database),
                        columns: ZaJi.NfiEstaPg.TargetData.TDAreaDomainCategoryList.Cols);
                });

            tsmiCArealOrPopulation.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.TargetData.TDArealOrPopulationList(database: Database),
                        columns: ZaJi.NfiEstaPg.TargetData.TDArealOrPopulationList.Cols);
                });

            tsmiCClassificationType.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.TargetData.TDClassificationTypeList(database: Database),
                        columns: ZaJi.NfiEstaPg.TargetData.TDClassificationTypeList.Cols);
                });

            tsmiCDefinitionVariant.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.TargetData.TDDefinitionVariantList(database: Database),
                        columns: ZaJi.NfiEstaPg.TargetData.TDDefinitionVariantList.Cols);
                });

            tsmiCLDsity.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.TargetData.TDLDsityList(database: Database),
                        columns: ZaJi.NfiEstaPg.TargetData.TDLDsityList.Cols);
                });

            tsmiCLDsityObject.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.TargetData.TDLDsityObjectList(database: Database),
                        columns: ZaJi.NfiEstaPg.TargetData.TDLDsityObjectList.Cols);
                });

            tsmiCLDsityObjectGroup.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.TargetData.TDLDsityObjectGroupList(database: Database),
                        columns: ZaJi.NfiEstaPg.TargetData.TDLDsityObjectGroupList.Cols);
                });

            tsmiCLDsityObjectType.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.TargetData.TDLDsityObjectTypeList(database: Database),
                        columns: ZaJi.NfiEstaPg.TargetData.TDLDsityObjectTypeList.Cols);
                });

            tsmiCPanelRefYearSetGroup.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.TargetData.TDPanelRefYearSetGroupList(database: Database),
                        columns: ZaJi.NfiEstaPg.TargetData.TDPanelRefYearSetGroupList.Cols);
                });

            tsmiCStateOrChange.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.TargetData.TDStateOrChangeList(database: Database),
                        columns: ZaJi.NfiEstaPg.TargetData.TDStateOrChangeList.Cols);
                });

            tsmiCSubPopulation.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.TargetData.TDSubPopulationList(database: Database),
                        columns: ZaJi.NfiEstaPg.TargetData.TDSubPopulationList.Cols);
                });

            tsmiCSubPopulationCategory.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.TargetData.TDSubPopulationCategoryList(database: Database),
                        columns: ZaJi.NfiEstaPg.TargetData.TDSubPopulationCategoryList.Cols);
                });

            tsmiCTargetVariable.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.TargetData.TDTargetVariableList(database: Database),
                        columns: ZaJi.NfiEstaPg.TargetData.TDTargetVariableList.Cols);
                });

            tsmiCUnitOfMeasure.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.TargetData.TDUnitOfMeasureList(database: Database),
                        columns: ZaJi.NfiEstaPg.TargetData.TDUnitOfMeasureList.Cols);
                });

            tsmiCVersion.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.TargetData.TDVersionList(database: Database),
                        columns: ZaJi.NfiEstaPg.TargetData.TDVersionList.Cols);
                });

            tsmiCMADCToClassificationRule.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.TargetData.TDADCToClassificationRuleList(database: Database),
                        columns: ZaJi.NfiEstaPg.TargetData.TDADCToClassificationRuleList.Cols);
                });

            tsmiCMADCToClassRuleToPanelRefYearSet.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.TargetData.TDADCToClassRuleToPanelRefYearSetList(database: Database),
                        columns: ZaJi.NfiEstaPg.TargetData.TDADCToClassRuleToPanelRefYearSetList.Cols);
                });

            tsmiCMLDsityObjectToLDsityObjectGroup.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.TargetData.TDLDObjectToLDObjectGroupList(database: Database),
                        columns: ZaJi.NfiEstaPg.TargetData.TDLDObjectToLDObjectGroupList.Cols);
                });

            tsmiCMLDsityToPanelRefYearSetVersion.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.TargetData.TDLDsityToPanelRefYearSetVersionList(database: Database),
                        columns: ZaJi.NfiEstaPg.TargetData.TDLDsityToPanelRefYearSetVersionList.Cols);
                });

            tsmiCMLDsityToTargetToCategorizationSetup.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.TargetData.TDLDsityToTargetToCategorizationSetupList(database: Database),
                        columns: ZaJi.NfiEstaPg.TargetData.TDLDsityToTargetToCategorizationSetupList.Cols);
                });

            tsmiCMLDsityToTargetVariable.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.TargetData.TDLDsityToTargetVariableList(database: Database),
                        columns: ZaJi.NfiEstaPg.TargetData.TDLDsityToTargetVariableList.Cols);
                });

            tsmiCMSPCToClassificationRule.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.TargetData.TDSPCToClassificationRuleList(database: Database),
                        columns: ZaJi.NfiEstaPg.TargetData.TDSPCToClassificationRuleList.Cols);
                });

            tsmiCMSPCToClassRuleToPanelRefYearSet.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.TargetData.TDSPCToClassRuleToPanelRefYearSetList(database: Database),
                        columns: ZaJi.NfiEstaPg.TargetData.TDSPCToClassRuleToPanelRefYearSetList.Cols);
                });

            tsmiTADCHierarchy.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.TargetData.TDADCHierarchyList(database: Database),
                        columns: ZaJi.NfiEstaPg.TargetData.TDADCHierarchyList.Cols);
                });

            tsmiTAvailableDataSets.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.TargetData.TDAvailableDataSetList(database: Database),
                        columns: ZaJi.NfiEstaPg.TargetData.TDAvailableDataSetList.Cols);
                });

            tsmiTCategorizationSetup.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.TargetData.TDCategorizationSetupList(database: Database),
                        columns: ZaJi.NfiEstaPg.TargetData.TDCategorizationSetupList.Cols);
                });

            tsmiTETLAreaDomain.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.TargetData.TDEtlAreaDomainList(database: Database),
                        columns: ZaJi.NfiEstaPg.TargetData.TDEtlAreaDomainList.Cols);
                });

            tsmiTETLAreaDomainCategory.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.TargetData.TDEtlAreaDomainCategoryList(database: Database),
                        columns: ZaJi.NfiEstaPg.TargetData.TDEtlAreaDomainCategoryList.Cols);
                });

            tsmiTETLLog.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.TargetData.TDEtlLogList(database: Database),
                        columns: ZaJi.NfiEstaPg.TargetData.TDEtlLogList.Cols);
                });

            tsmiTETLSubPopulation.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.TargetData.TDEtlSubPopulationList(database: Database),
                        columns: ZaJi.NfiEstaPg.TargetData.TDEtlSubPopulationList.Cols);
                });

            tsmiTETLSubPopulationCategory.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.TargetData.TDSubPopulationCategoryList(database: Database),
                        columns: ZaJi.NfiEstaPg.TargetData.TDSubPopulationCategoryList.Cols);
                });

            tsmiTETLTargetVariable.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.TargetData.TDEtlTargetVariableList(database: Database),
                        columns: ZaJi.NfiEstaPg.TargetData.TDEtlTargetVariableList.Cols);
                });

            tsmiTExportConnection.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.TargetData.TDExportConnectionList(database: Database),
                        columns: ZaJi.NfiEstaPg.TargetData.TDExportConnectionList.Cols);
                });

            tsmiTLDsityValues.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.TargetData.TDLDsityList(database: Database),
                        columns: ZaJi.NfiEstaPg.TargetData.TDLDsityList.Cols);
                });

            tsmiTPanelRefYearSetGroup.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.TargetData.TDPanelRefYearSetGroupList(database: Database),
                        columns: ZaJi.NfiEstaPg.TargetData.TDPanelRefYearSetGroupList.Cols);
                });

            tsmiTSPCHierarchy.Click += new EventHandler(
                (sender, e) =>
                {
                    DisplayTable(
                        table: new ZaJi.NfiEstaPg.TargetData.TDSPCHierarchyList(database: Database),
                        columns: ZaJi.NfiEstaPg.TargetData.TDSPCHierarchyList.Cols);
                });

            tsmiExtractData.Click += new EventHandler(
              (sender, e) => { ExportData(); });

            tsmiGetDifferences.Click += new EventHandler(
              (sender, e) => { DisplayDifferences(); });
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků formuláře</para>
        /// <para lang="en">Initializing form labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            switch (LanguageVersion)
            {
                case LanguageVersion.National:
                    Text = $"{TDSchema.ExtensionName} ({TDSchema.ExtensionVersion})";

                    tsmiLookupTables.Text = "Číselníky";
                    tsmiCAreaDomain.Text = ZaJi.NfiEstaPg.TargetData.TDAreaDomainList.Name;
                    tsmiCAreaDomainCategory.Text = ZaJi.NfiEstaPg.TargetData.TDAreaDomainCategoryList.Name;
                    tsmiCArealOrPopulation.Text = ZaJi.NfiEstaPg.TargetData.TDArealOrPopulationList.Name;
                    tsmiCClassificationType.Text = ZaJi.NfiEstaPg.TargetData.TDClassificationTypeList.Name;
                    tsmiCDefinitionVariant.Text = ZaJi.NfiEstaPg.TargetData.TDDefinitionVariantList.Name;
                    tsmiCLDsity.Text = ZaJi.NfiEstaPg.TargetData.TDLDsityList.Name;
                    tsmiCLDsityObject.Text = ZaJi.NfiEstaPg.TargetData.TDLDsityObjectList.Name;
                    tsmiCLDsityObjectGroup.Text = ZaJi.NfiEstaPg.TargetData.TDLDsityObjectGroupList.Name;
                    tsmiCLDsityObjectType.Text = ZaJi.NfiEstaPg.TargetData.TDLDsityObjectTypeList.Name;
                    tsmiCPanelRefYearSetGroup.Text = ZaJi.NfiEstaPg.TargetData.TDPanelRefYearSetGroupList.Name;
                    tsmiCStateOrChange.Text = ZaJi.NfiEstaPg.TargetData.TDStateOrChangeList.Name;
                    tsmiCSubPopulation.Text = ZaJi.NfiEstaPg.TargetData.TDSubPopulationList.Name;
                    tsmiCSubPopulationCategory.Text = ZaJi.NfiEstaPg.TargetData.TDSubPopulationCategoryList.Name;
                    tsmiCTargetVariable.Text = ZaJi.NfiEstaPg.TargetData.TDTargetVariableList.Name;
                    tsmiCUnitOfMeasure.Text = ZaJi.NfiEstaPg.TargetData.TDUnitOfMeasureList.Name;
                    tsmiCVersion.Text = ZaJi.NfiEstaPg.TargetData.TDVersionList.Name;

                    tsmiMappingTables.Text = "Mapovací tabulky";
                    tsmiCMADCToClassificationRule.Text = ZaJi.NfiEstaPg.TargetData.TDADCToClassificationRuleList.Name;
                    tsmiCMADCToClassRuleToPanelRefYearSet.Text = ZaJi.NfiEstaPg.TargetData.TDADCToClassRuleToPanelRefYearSetList.Name;
                    tsmiCMLDsityObjectToLDsityObjectGroup.Text = ZaJi.NfiEstaPg.TargetData.TDLDObjectToLDObjectGroupList.Name;
                    tsmiCMLDsityToPanelRefYearSetVersion.Text = ZaJi.NfiEstaPg.TargetData.TDLDsityToPanelRefYearSetVersionList.Name;
                    tsmiCMLDsityToTargetToCategorizationSetup.Text = ZaJi.NfiEstaPg.TargetData.TDLDsityToTargetToCategorizationSetupList.Name;
                    tsmiCMLDsityToTargetVariable.Text = ZaJi.NfiEstaPg.TargetData.TDLDsityToTargetVariableList.Name;
                    tsmiCMSPCToClassificationRule.Text = ZaJi.NfiEstaPg.TargetData.TDSPCToClassificationRuleList.Name;
                    tsmiCMSPCToClassRuleToPanelRefYearSet.Text = ZaJi.NfiEstaPg.TargetData.TDSPCToClassRuleToPanelRefYearSetList.Name;

                    tsmiDataTables.Text = "Datové tabulky";
                    tsmiTADCHierarchy.Text = ZaJi.NfiEstaPg.TargetData.TDADCHierarchyList.Name;
                    tsmiTAvailableDataSets.Text = ZaJi.NfiEstaPg.TargetData.TDAvailableDataSetList.Name;
                    tsmiTCategorizationSetup.Text = ZaJi.NfiEstaPg.TargetData.TDCategorizationSetupList.Name;
                    tsmiTETLAreaDomain.Text = ZaJi.NfiEstaPg.TargetData.TDEtlAreaDomainList.Name;
                    tsmiTETLAreaDomainCategory.Text = ZaJi.NfiEstaPg.TargetData.TDEtlAreaDomainCategoryList.Name;
                    tsmiTETLLog.Text = ZaJi.NfiEstaPg.TargetData.TDEtlLogList.Name;
                    tsmiTETLSubPopulation.Text = ZaJi.NfiEstaPg.TargetData.TDEtlSubPopulationList.Name;
                    tsmiTETLSubPopulationCategory.Text = ZaJi.NfiEstaPg.TargetData.TDEtlSubPopulationCategoryList.Name;
                    tsmiTETLTargetVariable.Text = ZaJi.NfiEstaPg.TargetData.TDEtlTargetVariableList.Name;
                    tsmiTExportConnection.Text = ZaJi.NfiEstaPg.TargetData.TDExportConnectionList.Name;
                    tsmiTLDsityValues.Text = ZaJi.NfiEstaPg.TargetData.TDLDsityList.Name;
                    tsmiTPanelRefYearSetGroup.Text = ZaJi.NfiEstaPg.TargetData.TDPanelRefYearSetGroupList.Name;
                    tsmiTSPCHierarchy.Text = ZaJi.NfiEstaPg.TargetData.TDSPCHierarchyList.Name;

                    tsmiCheckFunctions.Text = "Uložené procedury fn_check_*";
                    tsmiFnCheckAdcRuleHasPanelRef.Text = "fn_check_adc_rule_has_panelref(2)";
                    tsmiFnCheckClassificationRule.Text = "fn_check_classification_rule(2, 200, 'species IN (1,20)')";
                    tsmiFnCheckClassificationRuleSyntax.Text = "fn_check_classification_rule_syntax(200, 'species IN (1,20)')";
                    tsmiFnCheckClassificationRules.Text = "fn_check_classification_rules(2, 200, array['species IN (1,20)','species IN (50)'], 1)";
                    tsmiFnCheckSpcRuleHasPanelRef.Text = "fn_check_adc_rule_has_panelref(5)";

                    tsmiGetFunctions.Text = "Uložené procedury fn_get_*";
                    tsmiFnGetAreaDomain.Text = "fn_get_area_domain(200)";
                    tsmiFnGetAreaDomainCategory.Text = "fn_get_area_domain_category()";
                    tsmiFnGetAreaDomainCategoryForDomain.Text = "fn_get_area_domain_category4domain(200)";
                    tsmiFnGetArealOrPopulation.Text = "fn_get_areal_or_population()";
                    tsmiFnGetDefinitionVariant.Text = "fn_get_definition_variant(2)";
                    tsmiFnGetDefinitionVariantForLDsity.Text = "fn_get_definition_variant4ldsity(2)";

                    tsmiFnGetHierarchyAdc.Text = "fn_get_hierarchy(100,100,200,false,true)";
                    tsmiFnGetHierarchySpc.Text = "fn_get_hierarchy(200,100,200,false,true)";
                    tsmiFnGetLDsity.Text = "fn_get_ldsity(array[1],100)";
                    tsmiFnGetLDsityForObject.Text = "fn_get_ldsity4object(200,200)";
                    tsmiFnGetLDsityObject.Text = "fn_get_ldsity_object()";
                    tsmiFnGetLDsityObjectForADSP.Text = "fn_get_ldsity_object4adsp(200, 100)";
                    tsmiFnGetLDsityObjectForLDsityObjectADSP.Text = "fn_get_ldsity_object4ld_object_adsp(200,200,300,false)";
                    tsmiFnGetLDsityObjectForLDsityObjectGroup.Text = "fn_get_ldsity_object4ld_object_group(3)";
                    tsmiFnGetLDsityObjectGroup.Text = "fn_get_ldsity_object_group(3)";
                    tsmiFnGetLDsityObjectType.Text = "fn_get_ldsity_object_type()";
                    tsmiFnGetLDsitySubPopForObject.Text = "fn_get_ldsity_subpop4object(100)";
                    tsmiFnGetPanelRefYearSet.Text = "fn_get_panel_refyearset()";
                    tsmiFnGetPanelRefYearSetGroup.Text = "fn_get_panel_refyearset_group(1)";
                    tsmiFnGetStateOrChange.Text = "fn_get_state_or_change()";
                    tsmiFnGetSupPopulation.Text = "fn_get_sub_population(300)";
                    tsmiFnGetSupPopulationCategory.Text = "fn_get_sub_population_category()";
                    tsmiFnGetSubPopulationCategoryForPopulation.Text = "fn_get_sub_population_category4population(300)";
                    tsmiFnGetTargetVariableAgg.Text = "fn_get_target_variable_agg(null)";
                    tsmiFnGetTargetVariable.Text = "fn_get_target_variable(null)";
                    tsmiFnGetUnitOfMeasure.Text = "fn_get_unit_of_measure(2)";
                    tsmiFnGetUnitOfMeasureForLDsityObjectGroup.Text = "fn_get_unit_of_measure4ld_object_group(3)";
                    tsmiFnGetVersion.Text = "fn_get_version()";

                    tsmiOtherGetFunctions.Text = "Ostatní uložené procedury fn_get_*";
                    tsmiFnAddPlotTargetAttr.Text = "fn_add_plot_target_attr(array[1,2,3], array[1,2,3], array[1,2,3], 0.0, true)";
                    tsmiFnGetAttributeDomainAD.Text = "fn_get_attribute_domain('ad', array[100,200])";
                    tsmiFnGetAttributeDomainSP.Text = "fn_get_attribute_domain('sp', array[1,300])";
                    tsmiFnGetAvailableRefYearSetToPanelForClassificationRuleIdAdc.Text = "fn_get_available_refyearset2panel4classifiaction_rule_id('adc', 1)";
                    tsmiFnGetAvailableRefYearSetToPanelForClassificationRuleIdSpc.Text = "fn_get_available_refyearset2panel4classifiaction_rule_id('spc', 1)";
                    tsmiFnGetCaseAdForCategorizationSetups.Text = "fn_get_case_ad4categorization_setups(array[1,2])";
                    tsmiFnGetCategoryForClassificationRuleIdAdc.Text = "fn_get_category4classification_rule_id('adc', array[2,3])";
                    tsmiFnGetCategoryForClassificationRuleIdSpc.Text = "fn_get_category4classification_rule_id('spc', array[5,6])";
                    tsmiFnGetCategoryTypeForClassificationRuleIdAdc.Text = "fn_get_category_type4classification_rule_id ('adc', array[2,3])";
                    tsmiFnGetCategoryTypeForClassificationRuleIdSpc.Text = "fn_get_category_type4classification_rule_id ('spc', array[5,6])";
                    tsmiFnGetClassificationRuleForAdc.Text = "fn_get_classification_rule4adc(201,100,false)";
                    tsmiFnGetClassificationRuleForSpc.Text = "fn_get_classification_rule4spc(301,200,false)";
                    tsmiFnGetClassificationRuleForClassificationRuleIdAdc.Text = "fn_get_classification_rule4classification_rule_id('adc', array[1,2], array[1,2])";
                    tsmiFnGetClassificationRuleForClassificationRuleIdSpc.Text = "fn_get_classification_rule4classification_rule_id('spc', array[1,2], array[1,2])";
                    tsmiFnGetClassificationRuleId4Category.Text = "fn_get_classification_rule_id4category(array[201, 202], array[100, 100], array[1,2], array[200, 200])";
                    tsmiFnGetClassificationRules4CategorizationSetups.Text = "fn_get_classification_rules4categorization_setups(array[1,2],1)";
                    tsmiFnGetClassificationRulesId4Categories.Text = "fn_get_classification_rules_id4categories(array[200], array[100], array[300], array[200])";
                    tsmiFnGetEligiblePanelRefYearSetCombinations.Text = "fn_get_eligible_panel_refyearset_combinations(1)";
                    tsmiFnGetEligiblePanelRefyearsetGroups.Text = "fn_get_eligible_panel_refyearset_groups(array[1])";
                    tsmiFnGetLDsityObjects.Text = "fn_get_ldsity_objects(array[400])";
                    tsmiFnGetPlots.Text = "fn_get_plots(array[1,2], 100, false)";
                    tsmiFnGetRefYearSetToPanelMappingForGroup.Text = "fn_get_refyearset2panel_mapping4group(1);";

                    tsmiETLFunctions.Text = "Ostatní uložené procedury fn_etl_get_*";
                    tsmiFnEtlGetExportConnections.Text = "fn_etl_get_export_connections()";
                    tsmiFnEtlGetTargetVariable.Text = "fn_etl_get_target_variable(1)";
                    tsmiFnEtlGetTargetVariableExtended.Text = "fn_etl_get_target_variable_extended(1)";
                    tsmiFnEtlGetTargetVariableAggregated.Text = "fn_etl_get_target_variable_aggregated(1)";
                    tsmiControlTargetVariableSelector.Text = "target_variable_selector_test";

                    tsmiETL.Text = "ETL";
                    tsmiExtractData.Text = "Export dat do vybrané složky";
                    tsmiGetDifferences.Text = "Rozdíly mezi dll a databázovou extenzí";

                    btnClose.Text = "Zavřít";

                    msgDataExportComplete = "Export dat byl dokončen.";
                    msgNoDifferencesFound = "Nebyly nalezeny žádné rozdíly.";

                    break;

                case LanguageVersion.International:
                    Text = $"{TDSchema.ExtensionName} ({TDSchema.ExtensionVersion})";

                    tsmiLookupTables.Text = "Lookup tables";
                    tsmiCAreaDomain.Text = ZaJi.NfiEstaPg.TargetData.TDAreaDomainList.Name;
                    tsmiCAreaDomainCategory.Text = ZaJi.NfiEstaPg.TargetData.TDAreaDomainCategoryList.Name;
                    tsmiCArealOrPopulation.Text = ZaJi.NfiEstaPg.TargetData.TDArealOrPopulationList.Name;
                    tsmiCClassificationType.Text = ZaJi.NfiEstaPg.TargetData.TDClassificationTypeList.Name;
                    tsmiCDefinitionVariant.Text = ZaJi.NfiEstaPg.TargetData.TDDefinitionVariantList.Name;
                    tsmiCLDsity.Text = ZaJi.NfiEstaPg.TargetData.TDLDsityList.Name;
                    tsmiCLDsityObject.Text = ZaJi.NfiEstaPg.TargetData.TDLDsityObjectList.Name;
                    tsmiCLDsityObjectGroup.Text = ZaJi.NfiEstaPg.TargetData.TDLDsityObjectGroupList.Name;
                    tsmiCLDsityObjectType.Text = ZaJi.NfiEstaPg.TargetData.TDLDsityObjectTypeList.Name;
                    tsmiCPanelRefYearSetGroup.Text = ZaJi.NfiEstaPg.TargetData.TDPanelRefYearSetGroupList.Name;
                    tsmiCStateOrChange.Text = ZaJi.NfiEstaPg.TargetData.TDStateOrChangeList.Name;
                    tsmiCSubPopulation.Text = ZaJi.NfiEstaPg.TargetData.TDSubPopulationList.Name;
                    tsmiCSubPopulationCategory.Text = ZaJi.NfiEstaPg.TargetData.TDSubPopulationCategoryList.Name;
                    tsmiCTargetVariable.Text = ZaJi.NfiEstaPg.TargetData.TDTargetVariableList.Name;
                    tsmiCUnitOfMeasure.Text = ZaJi.NfiEstaPg.TargetData.TDUnitOfMeasureList.Name;
                    tsmiCVersion.Text = ZaJi.NfiEstaPg.TargetData.TDVersionList.Name;

                    tsmiMappingTables.Text = "Mapping tables";
                    tsmiCMADCToClassificationRule.Text = ZaJi.NfiEstaPg.TargetData.TDADCToClassificationRuleList.Name;
                    tsmiCMADCToClassRuleToPanelRefYearSet.Text = ZaJi.NfiEstaPg.TargetData.TDADCToClassRuleToPanelRefYearSetList.Name;
                    tsmiCMLDsityObjectToLDsityObjectGroup.Text = ZaJi.NfiEstaPg.TargetData.TDLDObjectToLDObjectGroupList.Name;
                    tsmiCMLDsityToPanelRefYearSetVersion.Text = ZaJi.NfiEstaPg.TargetData.TDLDsityToPanelRefYearSetVersionList.Name;
                    tsmiCMLDsityToTargetToCategorizationSetup.Text = ZaJi.NfiEstaPg.TargetData.TDLDsityToTargetToCategorizationSetupList.Name;
                    tsmiCMLDsityToTargetVariable.Text = ZaJi.NfiEstaPg.TargetData.TDLDsityToTargetVariableList.Name;
                    tsmiCMSPCToClassificationRule.Text = ZaJi.NfiEstaPg.TargetData.TDSPCToClassificationRuleList.Name;
                    tsmiCMSPCToClassRuleToPanelRefYearSet.Text = ZaJi.NfiEstaPg.TargetData.TDSPCToClassRuleToPanelRefYearSetList.Name;

                    tsmiDataTables.Text = "Data tables";
                    tsmiTADCHierarchy.Text = ZaJi.NfiEstaPg.TargetData.TDADCHierarchyList.Name;
                    tsmiTAvailableDataSets.Text = ZaJi.NfiEstaPg.TargetData.TDAvailableDataSetList.Name;
                    tsmiTCategorizationSetup.Text = ZaJi.NfiEstaPg.TargetData.TDCategorizationSetupList.Name;
                    tsmiTETLAreaDomain.Text = ZaJi.NfiEstaPg.TargetData.TDEtlAreaDomainList.Name;
                    tsmiTETLAreaDomainCategory.Text = ZaJi.NfiEstaPg.TargetData.TDEtlAreaDomainCategoryList.Name;
                    tsmiTETLLog.Text = ZaJi.NfiEstaPg.TargetData.TDEtlLogList.Name;
                    tsmiTETLSubPopulation.Text = ZaJi.NfiEstaPg.TargetData.TDEtlSubPopulationList.Name;
                    tsmiTETLSubPopulationCategory.Text = ZaJi.NfiEstaPg.TargetData.TDEtlSubPopulationCategoryList.Name;
                    tsmiTETLTargetVariable.Text = ZaJi.NfiEstaPg.TargetData.TDEtlTargetVariableList.Name;
                    tsmiTExportConnection.Text = ZaJi.NfiEstaPg.TargetData.TDExportConnectionList.Name;
                    tsmiTLDsityValues.Text = ZaJi.NfiEstaPg.TargetData.TDLDsityList.Name;
                    tsmiTPanelRefYearSetGroup.Text = ZaJi.NfiEstaPg.TargetData.TDPanelRefYearSetGroupList.Name;
                    tsmiTSPCHierarchy.Text = ZaJi.NfiEstaPg.TargetData.TDSPCHierarchyList.Name;

                    tsmiCheckFunctions.Text = "Stored procedures fn_check_*";
                    tsmiFnCheckAdcRuleHasPanelRef.Text = "fn_check_adc_rule_has_panelref(2)";
                    tsmiFnCheckClassificationRule.Text = "fn_check_classification_rule(2, 200, 'species IN (1,20)')";
                    tsmiFnCheckClassificationRuleSyntax.Text = "fn_check_classification_rule_syntax(200, 'species IN (1,20)')";
                    tsmiFnCheckClassificationRules.Text = "fn_check_classification_rules(2, 200, array['species IN (1,20)','species IN (50)'], 1)";
                    tsmiFnCheckSpcRuleHasPanelRef.Text = "fn_check_adc_rule_has_panelref(5)";

                    tsmiGetFunctions.Text = "Stored procedures fn_get_*";
                    tsmiFnGetAreaDomain.Text = "fn_get_area_domain(200)";
                    tsmiFnGetAreaDomainCategory.Text = "fn_get_area_domain_category()";
                    tsmiFnGetAreaDomainCategoryForDomain.Text = "fn_get_area_domain_category4domain(200)";
                    tsmiFnGetArealOrPopulation.Text = "fn_get_areal_or_population()";
                    tsmiFnGetDefinitionVariant.Text = "fn_get_definition_variant(2)";
                    tsmiFnGetDefinitionVariantForLDsity.Text = "fn_get_definition_variant4ldsity(2)";
                    tsmiFnEtlGetExportConnections.Text = "fn_etl_get_export_connections()";
                    tsmiFnGetHierarchyAdc.Text = "fn_get_hierarchy(100,100,200,false,true)";
                    tsmiFnGetHierarchySpc.Text = "fn_get_hierarchy(200,100,200,false,true)";
                    tsmiFnGetLDsity.Text = "fn_get_ldsity(array[1],100)";
                    tsmiFnGetLDsityForObject.Text = "fn_get_ldsity4object(200,200)";
                    tsmiFnGetLDsityObject.Text = "fn_get_ldsity_object()";
                    tsmiFnGetLDsityObjectForADSP.Text = "fn_get_ldsity_object4adsp(200, 100)";
                    tsmiFnGetLDsityObjectForLDsityObjectADSP.Text = "fn_get_ldsity_object4ld_object_adsp(200,200,300,false)";
                    tsmiFnGetLDsityObjectForLDsityObjectGroup.Text = "fn_get_ldsity_object4ld_object_group(3)";
                    tsmiFnGetLDsityObjectGroup.Text = "fn_get_ldsity_object_group(3)";
                    tsmiFnGetLDsityObjectType.Text = "fn_get_ldsity_object_type()";
                    tsmiFnGetLDsitySubPopForObject.Text = "fn_get_ldsity_subpop4object(100)";
                    tsmiFnGetPanelRefYearSet.Text = "fn_get_panel_refyearset()";
                    tsmiFnGetPanelRefYearSetGroup.Text = "fn_get_panel_refyearset_group(1)";
                    tsmiFnGetStateOrChange.Text = "fn_get_state_or_change()";
                    tsmiFnGetSupPopulation.Text = "fn_get_sub_population(300)";
                    tsmiFnGetSupPopulationCategory.Text = "fn_get_sub_population_category()";
                    tsmiFnGetSubPopulationCategoryForPopulation.Text = "fn_get_sub_population_category4population(300)";
                    tsmiFnGetTargetVariableAgg.Text = "fn_get_target_variable_agg(null)";
                    tsmiFnGetTargetVariable.Text = "fn_get_target_variable(null)";
                    tsmiFnGetUnitOfMeasure.Text = "fn_get_unit_of_measure(2)";
                    tsmiFnGetUnitOfMeasureForLDsityObjectGroup.Text = "fn_get_unit_of_measure4ld_object_group(3)";
                    tsmiFnGetVersion.Text = "fn_get_version()";

                    tsmiOtherGetFunctions.Text = "Other stored procedures fn_get_*";
                    tsmiFnAddPlotTargetAttr.Text = "fn_add_plot_target_attr(array[1,2,3], array[1,2,3], array[1,2,3], 0.0, true)";
                    tsmiFnGetAttributeDomainAD.Text = "fn_get_attribute_domain('ad', array[100,200])";
                    tsmiFnGetAttributeDomainSP.Text = "fn_get_attribute_domain('sp', array[1,300])";
                    tsmiFnGetAvailableRefYearSetToPanelForClassificationRuleIdAdc.Text = "fn_get_available_refyearset2panel4classifiaction_rule_id('adc', 1)";
                    tsmiFnGetAvailableRefYearSetToPanelForClassificationRuleIdSpc.Text = "fn_get_available_refyearset2panel4classifiaction_rule_id('spc', 1)";
                    tsmiFnGetCaseAdForCategorizationSetups.Text = "fn_get_case_ad4categorization_setups(array[1,2])";
                    tsmiFnGetCategoryForClassificationRuleIdAdc.Text = "fn_get_category4classification_rule_id('adc', array[2,3])";
                    tsmiFnGetCategoryForClassificationRuleIdSpc.Text = "fn_get_category4classification_rule_id('spc', array[5,6])";
                    tsmiFnGetCategoryTypeForClassificationRuleIdAdc.Text = "fn_get_category_type4classification_rule_id ('adc', array[2,3])";
                    tsmiFnGetCategoryTypeForClassificationRuleIdSpc.Text = "fn_get_category_type4classification_rule_id ('spc', array[5,6])";
                    tsmiFnGetClassificationRuleForAdc.Text = "fn_get_classification_rule4adc(201,100,false)";
                    tsmiFnGetClassificationRuleForSpc.Text = "fn_get_classification_rule4spc(301,200,false)";
                    tsmiFnGetClassificationRuleForClassificationRuleIdAdc.Text = "fn_get_classification_rule4classification_rule_id('adc', array[1,2], array[1,2])";
                    tsmiFnGetClassificationRuleForClassificationRuleIdSpc.Text = "fn_get_classification_rule4classification_rule_id('spc', array[1,2], array[1,2])";
                    tsmiFnGetClassificationRuleId4Category.Text = "fn_get_classification_rule_id4category(array[201, 202], array[100, 100], array[1, 2], array[200, 200])";
                    tsmiFnGetClassificationRules4CategorizationSetups.Text = "fn_get_classification_rules4categorization_setups(array[1,2],1)";
                    tsmiFnGetClassificationRulesId4Categories.Text = "fn_get_classification_rules_id4categories(array[200], array[100], array[300], array[200])";
                    tsmiFnGetEligiblePanelRefYearSetCombinations.Text = "fn_get_eligible_panel_refyearset_combinations(1)";
                    tsmiFnGetEligiblePanelRefyearsetGroups.Text = "fn_get_eligible_panel_refyearset_groups(array[1])";
                    tsmiFnGetLDsityObjects.Text = "fn_get_ldsity_objects(array[400])";
                    tsmiFnGetPlots.Text = "fn_get_plots(array[1,2], 100, false)";
                    tsmiFnGetRefYearSetToPanelMappingForGroup.Text = "fn_get_refyearset2panel_mapping4group(1);";

                    tsmiETLFunctions.Text = "Other stored procedures fn_etl_get_*";
                    tsmiFnEtlGetExportConnections.Text = "fn_etl_get_export_connections()";
                    tsmiFnEtlGetTargetVariable.Text = "fn_etl_get_target_variable(1)";
                    tsmiFnEtlGetTargetVariableExtended.Text = "fn_etl_get_target_variable_extended(1)";
                    tsmiFnEtlGetTargetVariableAggregated.Text = "fn_etl_get_target_variable_aggregated(1)";
                    tsmiControlTargetVariableSelector.Text = "target_variable_selector_test";

                    tsmiETL.Text = "ETL";
                    tsmiExtractData.Text = "Export data to folder";
                    tsmiGetDifferences.Text = "Differences between dll database extension";

                    btnClose.Text = "Close";

                    msgDataExportComplete = "Data export has been completed.";
                    msgNoDifferencesFound = "No differences have been found.";

                    break;

                default:
                    Text = nameof(FormTargetData);

                    tsmiLookupTables.Text = nameof(tsmiLookupTables);
                    tsmiCAreaDomain.Text = nameof(tsmiCAreaDomain);
                    tsmiCAreaDomainCategory.Text = nameof(tsmiCAreaDomainCategory);
                    tsmiCArealOrPopulation.Text = nameof(tsmiCArealOrPopulation);
                    tsmiCClassificationType.Text = nameof(tsmiCClassificationType);
                    tsmiCDefinitionVariant.Text = nameof(tsmiCDefinitionVariant);
                    tsmiCLDsity.Text = nameof(tsmiCLDsity);
                    tsmiCLDsityObject.Text = nameof(tsmiCLDsityObject);
                    tsmiCLDsityObjectGroup.Text = nameof(tsmiCLDsityObjectGroup);
                    tsmiCLDsityObjectType.Text = nameof(tsmiCLDsityObjectType);
                    tsmiCPanelRefYearSetGroup.Text = nameof(tsmiCPanelRefYearSetGroup);
                    tsmiCStateOrChange.Text = nameof(tsmiCStateOrChange);
                    tsmiCSubPopulation.Text = nameof(tsmiCSubPopulation);
                    tsmiCSubPopulationCategory.Text = nameof(tsmiCSubPopulationCategory);
                    tsmiCTargetVariable.Text = nameof(tsmiCTargetVariable);
                    tsmiCUnitOfMeasure.Text = nameof(tsmiCUnitOfMeasure);
                    tsmiCVersion.Text = nameof(tsmiCVersion);

                    tsmiMappingTables.Text = nameof(tsmiMappingTables);
                    tsmiCMADCToClassificationRule.Text = nameof(tsmiCMADCToClassificationRule);
                    tsmiCMADCToClassRuleToPanelRefYearSet.Text = nameof(tsmiCMADCToClassRuleToPanelRefYearSet);
                    tsmiCMLDsityObjectToLDsityObjectGroup.Text = nameof(tsmiCMLDsityObjectToLDsityObjectGroup);
                    tsmiCMLDsityToPanelRefYearSetVersion.Text = nameof(tsmiCMLDsityToPanelRefYearSetVersion);
                    tsmiCMLDsityToTargetToCategorizationSetup.Text = nameof(tsmiCMLDsityToTargetToCategorizationSetup);
                    tsmiCMLDsityToTargetVariable.Text = nameof(tsmiCMLDsityToTargetVariable);
                    tsmiCMSPCToClassificationRule.Text = nameof(tsmiCMSPCToClassificationRule);
                    tsmiCMSPCToClassRuleToPanelRefYearSet.Text = nameof(tsmiCMSPCToClassRuleToPanelRefYearSet);

                    tsmiDataTables.Text = nameof(tsmiDataTables);
                    tsmiTADCHierarchy.Text = nameof(tsmiTADCHierarchy);
                    tsmiTAvailableDataSets.Text = nameof(tsmiTAvailableDataSets);
                    tsmiTCategorizationSetup.Text = nameof(tsmiTCategorizationSetup);
                    tsmiTETLAreaDomain.Text = nameof(tsmiTETLAreaDomain);
                    tsmiTETLAreaDomainCategory.Text = nameof(tsmiTETLAreaDomainCategory);
                    tsmiTETLLog.Text = nameof(tsmiTETLLog);
                    tsmiTETLSubPopulation.Text = nameof(tsmiTETLSubPopulation);
                    tsmiTETLSubPopulationCategory.Text = nameof(tsmiTETLSubPopulationCategory);
                    tsmiTETLTargetVariable.Text = nameof(tsmiTETLTargetVariable);
                    tsmiTExportConnection.Text = nameof(tsmiTExportConnection);
                    tsmiTLDsityValues.Text = nameof(tsmiTLDsityValues);
                    tsmiTPanelRefYearSetGroup.Text = nameof(tsmiTPanelRefYearSetGroup);
                    tsmiTSPCHierarchy.Text = nameof(tsmiTSPCHierarchy);

                    tsmiCheckFunctions.Text = nameof(tsmiCheckFunctions);
                    tsmiFnCheckAdcRuleHasPanelRef.Text = nameof(tsmiFnCheckAdcRuleHasPanelRef);
                    tsmiFnCheckClassificationRule.Text = nameof(tsmiFnCheckClassificationRule);
                    tsmiFnCheckClassificationRuleSyntax.Text = nameof(tsmiFnCheckClassificationRuleSyntax);
                    tsmiFnCheckClassificationRules.Text = nameof(tsmiFnCheckClassificationRules);
                    tsmiFnCheckSpcRuleHasPanelRef.Text = nameof(tsmiFnCheckSpcRuleHasPanelRef);

                    tsmiGetFunctions.Text = nameof(tsmiGetFunctions);
                    tsmiFnGetAreaDomain.Text = nameof(tsmiFnGetAreaDomain);
                    tsmiFnGetAreaDomainCategory.Text = nameof(tsmiFnGetAreaDomainCategory);
                    tsmiFnGetAreaDomainCategoryForDomain.Text = nameof(tsmiFnGetAreaDomainCategoryForDomain);
                    tsmiFnGetArealOrPopulation.Text = nameof(tsmiFnGetArealOrPopulation);
                    tsmiFnGetDefinitionVariant.Text = nameof(tsmiFnGetDefinitionVariant);
                    tsmiFnGetDefinitionVariantForLDsity.Text = nameof(tsmiFnGetDefinitionVariantForLDsity);
                    tsmiFnEtlGetExportConnections.Text = nameof(tsmiFnEtlGetExportConnections);
                    tsmiFnGetHierarchyAdc.Text = nameof(tsmiFnGetHierarchyAdc);
                    tsmiFnGetHierarchySpc.Text = nameof(tsmiFnGetHierarchySpc);
                    tsmiFnGetLDsity.Text = nameof(tsmiFnGetLDsity);
                    tsmiFnGetLDsityForObject.Text = nameof(tsmiFnGetLDsityForObject);
                    tsmiFnGetLDsityObject.Text = nameof(tsmiFnGetLDsityObject);
                    tsmiFnGetLDsityObjectForADSP.Text = nameof(tsmiFnGetLDsityObjectForADSP);
                    tsmiFnGetLDsityObjectForLDsityObjectADSP.Text = nameof(tsmiFnGetLDsityObjectForLDsityObjectADSP);
                    tsmiFnGetLDsityObjectForLDsityObjectGroup.Text = nameof(tsmiFnGetLDsityObjectForLDsityObjectGroup);
                    tsmiFnGetLDsityObjectGroup.Text = nameof(tsmiFnGetLDsityObjectGroup);
                    tsmiFnGetLDsityObjectType.Text = nameof(tsmiFnGetLDsityObjectType);
                    tsmiFnGetLDsitySubPopForObject.Text = nameof(tsmiFnGetLDsitySubPopForObject);
                    tsmiFnGetPanelRefYearSet.Text = nameof(tsmiFnGetPanelRefYearSet);
                    tsmiFnGetPanelRefYearSetGroup.Text = nameof(tsmiFnGetPanelRefYearSetGroup);
                    tsmiFnGetStateOrChange.Text = nameof(tsmiFnGetStateOrChange);
                    tsmiFnGetSupPopulation.Text = nameof(tsmiFnGetSupPopulation);
                    tsmiFnGetSupPopulationCategory.Text = nameof(tsmiFnGetSupPopulationCategory);
                    tsmiFnGetSubPopulationCategoryForPopulation.Text = nameof(tsmiFnGetSubPopulationCategoryForPopulation);
                    tsmiFnGetTargetVariableAgg.Text = nameof(tsmiFnGetTargetVariableAgg);
                    tsmiFnGetTargetVariable.Text = nameof(tsmiFnGetTargetVariable);
                    tsmiFnGetUnitOfMeasure.Text = nameof(tsmiFnGetUnitOfMeasure);
                    tsmiFnGetUnitOfMeasureForLDsityObjectGroup.Text = nameof(tsmiFnGetUnitOfMeasureForLDsityObjectGroup);
                    tsmiFnGetVersion.Text = nameof(tsmiFnGetVersion);

                    tsmiOtherGetFunctions.Text = nameof(tsmiOtherGetFunctions);
                    tsmiFnAddPlotTargetAttr.Text = nameof(tsmiFnAddPlotTargetAttr);
                    tsmiFnGetAttributeDomainAD.Text = nameof(tsmiFnGetAttributeDomainAD);
                    tsmiFnGetAttributeDomainSP.Text = nameof(tsmiFnGetAttributeDomainSP);
                    tsmiFnGetAvailableRefYearSetToPanelForClassificationRuleIdAdc.Text = nameof(tsmiFnGetAvailableRefYearSetToPanelForClassificationRuleIdAdc);
                    tsmiFnGetAvailableRefYearSetToPanelForClassificationRuleIdSpc.Text = nameof(tsmiFnGetAvailableRefYearSetToPanelForClassificationRuleIdSpc);
                    tsmiFnGetCaseAdForCategorizationSetups.Text = nameof(tsmiFnGetCaseAdForCategorizationSetups);
                    tsmiFnGetCategoryForClassificationRuleIdAdc.Text = nameof(tsmiFnGetCategoryForClassificationRuleIdAdc);
                    tsmiFnGetCategoryForClassificationRuleIdSpc.Text = nameof(tsmiFnGetCategoryForClassificationRuleIdSpc);
                    tsmiFnGetCategoryTypeForClassificationRuleIdAdc.Text = nameof(tsmiFnGetCategoryTypeForClassificationRuleIdAdc);
                    tsmiFnGetCategoryTypeForClassificationRuleIdSpc.Text = nameof(tsmiFnGetCategoryTypeForClassificationRuleIdSpc);
                    tsmiFnGetClassificationRuleForAdc.Text = nameof(tsmiFnGetClassificationRuleForAdc);
                    tsmiFnGetClassificationRuleForSpc.Text = nameof(tsmiFnGetClassificationRuleForSpc);
                    tsmiFnGetClassificationRuleForClassificationRuleIdAdc.Text = nameof(tsmiFnGetClassificationRuleForClassificationRuleIdAdc);
                    tsmiFnGetClassificationRuleForClassificationRuleIdSpc.Text = nameof(tsmiFnGetClassificationRuleForClassificationRuleIdSpc);
                    tsmiFnGetClassificationRuleId4Category.Text = nameof(tsmiFnGetClassificationRuleId4Category);
                    tsmiFnGetClassificationRules4CategorizationSetups.Text = nameof(tsmiFnGetClassificationRules4CategorizationSetups);
                    tsmiFnGetClassificationRulesId4Categories.Text = nameof(tsmiFnGetClassificationRulesId4Categories);
                    tsmiFnGetEligiblePanelRefYearSetCombinations.Text = nameof(tsmiFnGetEligiblePanelRefYearSetCombinations);
                    tsmiFnGetEligiblePanelRefyearsetGroups.Text = nameof(tsmiFnGetEligiblePanelRefyearsetGroups);
                    tsmiFnGetLDsityObjects.Text = nameof(tsmiFnGetLDsityObjects);
                    tsmiFnGetPlots.Text = nameof(tsmiFnGetPlots);
                    tsmiFnGetRefYearSetToPanelMappingForGroup.Text = nameof(tsmiFnGetRefYearSetToPanelMappingForGroup);

                    tsmiETLFunctions.Text = nameof(tsmiETLFunctions);
                    tsmiFnEtlGetExportConnections.Text = nameof(tsmiFnEtlGetExportConnections);
                    tsmiFnEtlGetTargetVariable.Text = nameof(tsmiFnEtlGetTargetVariable);
                    tsmiFnEtlGetTargetVariableExtended.Text = nameof(tsmiFnEtlGetTargetVariableExtended);
                    tsmiFnEtlGetTargetVariableAggregated.Text = nameof(tsmiFnEtlGetTargetVariableAggregated);
                    tsmiControlTargetVariableSelector.Text = nameof(tsmiControlTargetVariableSelector);

                    tsmiETL.Text = nameof(tsmiETL);
                    tsmiExtractData.Text = nameof(tsmiExtractData);
                    tsmiGetDifferences.Text = nameof(tsmiGetDifferences);

                    btnClose.Text = nameof(btnClose);

                    msgDataExportComplete = nameof(msgDataExportComplete);
                    msgNoDifferencesFound = nameof(msgNoDifferencesFound);

                    break;
            }
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek</para>
        /// <para lang="en">Uploading database table data</para>
        /// </summary>
        public void LoadContent() { }

        /// <summary>
        /// <para lang="cs">Zobrazí data číselníku</para>
        /// <para lang="en">Display lookup table data</para>
        /// </summary>
        /// <param name="table">
        /// <para lang="cs">Tabulka</para>
        /// <para lang="en">Table</para>
        /// </param>
        /// <param name="columns">
        /// <para lang="cs">Sloupce tabulky</para>
        /// <para lang="en">Table columns</para>
        /// </param>
        private void DisplayTable(
            ALookupTable table,
            Dictionary<string, ColumnMetadata> columns)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(value: dgvData);
            table.ReLoad(limit: Limit, extended: false);
            ALookupTable.SetColumnsVisibility(columns: columns);
            table.SetDataGridViewDataSource(dataGridView: dgvData);
            table.FormatDataGridView(dataGridView: dgvData);
            grpMain.Text = table.Caption;
            txtSQL.Text = $"{Environment.NewLine}{table.SQL}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(box: txtSQL);
        }

        /// <summary>
        /// <para lang="cs">Zobrazí data mapovací tabulky</para>
        /// <para lang="en">Display mapping table data</para>
        /// </summary>
        /// <param name="table">
        /// <para lang="cs">Tabulka</para>
        /// <para lang="en">Table</para>
        /// </param>
        /// <param name="columns">
        /// <para lang="cs">Sloupce tabulky</para>
        /// <para lang="en">Table columns</para>
        /// </param>
        private void DisplayTable(
            AMappingTable table,
            Dictionary<string, ColumnMetadata> columns)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(value: dgvData);
            table.ReLoad(limit: Limit);
            AMappingTable.SetColumnsVisibility(columns: columns);
            table.SetDataGridViewDataSource(dataGridView: dgvData);
            table.FormatDataGridView(dataGridView: dgvData);
            grpMain.Text = table.Caption;
            txtSQL.Text = $"{Environment.NewLine}{table.SQL}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(box: txtSQL);
        }

        /// <summary>
        /// <para lang="cs">Zobrazí data tabulky s prostorovými daty</para>
        /// <para lang="en">Display data table with spatial data</para>
        /// </summary>
        /// <param name="table">
        /// <para lang="cs">Tabulka</para>
        /// <para lang="en">Table</para>
        /// </param>
        /// <param name="columns">
        /// <para lang="cs">Sloupce tabulky</para>
        /// <para lang="en">Table columns</para>
        /// </param>
        private void DisplayTable(
            ASpatialTable table,
            Dictionary<string, ColumnMetadata> columns)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(value: dgvData);
            table.ReLoad(limit: Limit, withGeom: false);
            ASpatialTable.SetColumnsVisibility(columns: columns);
            table.SetDataGridViewDataSource(dataGridView: dgvData);
            table.FormatDataGridView(dataGridView: dgvData);
            grpMain.Text = table.Caption;
            txtSQL.Text = $"{Environment.NewLine}{table.SQL}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(box: txtSQL);
        }

        /// <summary>
        /// <para lang="cs">Zobrazí data tabulky</para>
        /// <para lang="en">Display table data</para>
        /// </summary>
        /// <param name="table">
        /// <para lang="cs">Tabulka</para>
        /// <para lang="en">Table</para>
        /// </param>
        /// <param name="columns">
        /// <para lang="cs">Sloupce tabulky</para>
        /// <para lang="en">Table columns</para>
        /// </param>
        private void DisplayTable(
            ADataTable table,
            Dictionary<string, ColumnMetadata> columns)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(value: dgvData);
            table.ReLoad(limit: Limit);
            ADataTable.SetColumnsVisibility(columns: columns);
            table.SetDataGridViewDataSource(dataGridView: dgvData);
            table.FormatDataGridView(dataGridView: dgvData);
            grpMain.Text = table.Caption;
            txtSQL.Text = $"{Environment.NewLine}{table.SQL}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(box: txtSQL);
        }

        /// <summary>
        /// <para lang="cs">Zobrazí data vrácená uloženou procedurou</para>
        /// <para lang="en">Display data returned from stored procedure</para>
        /// </summary>
        /// <param name="table">
        /// <para lang="cs">Tabulka s daty vrácenými uloženou procedurou</para>
        /// <para lang="en">Table with data returned from stored procedure</para>
        /// </param>
        /// <param name="columns">
        /// <para lang="cs">Sloupce tabulky</para>
        /// <para lang="en">Table columns</para>
        /// </param>
        private void DisplayTable(
            AParametrizedView table,
            Dictionary<string, ColumnMetadata> columns)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(value: dgvData);
            ADataTable.SetColumnsVisibility(columns: columns);
            table.SetDataGridViewDataSource(dataGridView: dgvData);
            table.FormatDataGridView(dataGridView: dgvData);
            grpMain.Text = table.Caption;
            txtSQL.Text = $"{Environment.NewLine}{table.SQL}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(box: txtSQL);
        }

        /// <summary>
        /// <para lang="cs">Export dat</para>
        /// <para lang="en">Data export</para>
        /// </summary>
        private void ExportData()
        {
            TDSchema.ExportData(
               database: Database,
               fileFormat: ExportFileFormat.Xml);

            MessageBox.Show(
                caption: String.Empty,
                text: msgDataExportComplete,
                buttons: MessageBoxButtons.OK,
                icon: MessageBoxIcon.Information);
        }

        /// <summary>
        /// <para lang="cs">Zobrazí provedené změny v databázi</para>
        /// <para lang="en">Displays changes made to the database</para>
        /// </summary>
        private void DisplayDifferences()
        {
            grpMain.Text = String.Empty;
            pnlData.Controls.Clear();
            txtSQL.Text = String.Empty;

            List<string> differences = Database.STargetData.Differences;
            System.Text.StringBuilder stringBuilder = new();
            if ((differences == null) || (differences.Count == 0))
            {
                stringBuilder.AppendLine(value: msgNoDifferencesFound);
            }
            else
            {
                foreach (string difference in differences)
                {
                    stringBuilder.AppendLine(value: $"* {difference}");
                }
            }

            Label lblReport = new()
            {
                Dock = DockStyle.Fill,
                Text = stringBuilder.ToString()
            };
            pnlData.Controls.Add(value: lblReport);
        }

        #endregion Methods


        #region Event Handlers

        #region Stored procedures fn_check_*

        /// <summary>
        /// Test stored procedure: fn_check_adc_rule_has_panelref
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemFnCheckAdcRuleHasPanelRef_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            TextBox txtData = new()
            {
                Dock = DockStyle.Fill,
                Font = new Font("Courier New", 10.0f, FontStyle.Regular),
                BackColor = SystemColors.Window,
                ForeColor = SystemColors.WindowText,
                ReadOnly = true,
                BorderStyle = BorderStyle.None,
                Multiline = true,
                WordWrap = true
            };
            pnlData.Controls.Add(txtData);

            Nullable<bool> result = TDFunctions.FnCheckAdcRuleHasPanelRef.Execute(
                database: this.Database,
                adcToClassificationRuleId: 2);
            txtData.Text = Functions.PrepNBoolArg(result);
            grpMain.Text = TDFunctions.FnCheckAdcRuleHasPanelRef.Name;
            txtSQL.Text = $"{Environment.NewLine}{TDFunctions.FnCheckAdcRuleHasPanelRef.CommandText}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }

        /// <summary>
        /// Test stored procedure: fn_check_classification_rule
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemFnCheckClassificationRule_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(dgvData);

            TDFnCheckClassificationRuleTypeList result = TDFunctions.FnCheckClassificationRule.Execute(
                database: this.Database,
                ldsityId: 2,
                ldsityObjectId: 200,
                rule: "(species IN (1,20))",
                panelRefYearSetId: null,
                adc: null,
                spc: null,
                useNegative: false);
            TDFnCheckClassificationRuleTypeList.SetColumnsVisibility(
                (from a in TDFnCheckClassificationRuleTypeList.Cols
                 orderby a.Value.DisplayIndex
                 select a.Value.Name).ToArray<string>());
            result.SetDataGridViewDataSource(dgvData);
            result.FormatDataGridView(dgvData);
            grpMain.Text = TDFunctions.FnCheckClassificationRule.Name;
            txtSQL.Text = $"{Environment.NewLine}{TDFunctions.FnCheckClassificationRule.CommandText}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }

        /// <summary>
        /// Test stored procedure: fn_check_classification_rule_syntax
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemFnCheckClassificationRuleSyntax_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            TextBox txtData = new()
            {
                Dock = DockStyle.Fill,
                Font = new Font("Courier New", 10.0f, FontStyle.Regular),
                BackColor = SystemColors.Window,
                ForeColor = SystemColors.WindowText,
                ReadOnly = true,
                BorderStyle = BorderStyle.None,
                Multiline = true,
                WordWrap = true
            };
            pnlData.Controls.Add(txtData);

            Nullable<bool> result = TDFunctions.FnCheckClassificationRuleSyntax.Execute(
                database: Database,
                ldsityObjectId: 200,
                rule: "species IN (1,20)");
            txtData.Text = Functions.PrepNBoolArg(result);
            grpMain.Text = TDFunctions.FnCheckClassificationRuleSyntax.Name;
            txtSQL.Text = $"{Environment.NewLine}{TDFunctions.FnCheckClassificationRuleSyntax.CommandText}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }

        /// <summary>
        /// Test stored procedure: fn_check_classification_rules
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemFnCheckClassificationRules_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(dgvData);

            TDFnCheckClassificationRulesTypeList result = TDFunctions.FnCheckClassificationRules.Execute(
                database: Database,
                ldsityId: 2,
                ldsityObjectId: 200,
                rules: ["species IN (1,20)", "species IN (50)"],
                panelRefYearSetId: 1,
                adc: null,
                spc: null,
                useNegative: false);
            TDFnCheckClassificationRulesTypeList.SetColumnsVisibility(
                (from a in TDFnCheckClassificationRulesTypeList.Cols
                 orderby a.Value.DisplayIndex
                 select a.Value.Name).ToArray<string>());
            result.SetDataGridViewDataSource(dgvData);
            result.FormatDataGridView(dgvData);
            grpMain.Text = TDFunctions.FnCheckClassificationRules.Name;
            txtSQL.Text = $"{Environment.NewLine}{TDFunctions.FnCheckClassificationRules.CommandText}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }


        /// <summary>
        /// Test stored procedure: fn_check_spc_rule_has_panelref
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemFnCheckSpcRuleHasPanelRef_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            TextBox txtData = new()
            {
                Dock = DockStyle.Fill,
                Font = new Font("Courier New", 10.0f, FontStyle.Regular),
                BackColor = SystemColors.Window,
                ForeColor = SystemColors.WindowText,
                ReadOnly = true,
                BorderStyle = BorderStyle.None,
                Multiline = true,
                WordWrap = true
            };
            pnlData.Controls.Add(txtData);

            Nullable<bool> result = TDFunctions.FnCheckSpcRuleHasPanelRef.Execute(
                database: Database,
                spcToClassificationRuleId: 5);
            txtData.Text = Functions.PrepNBoolArg(result);
            grpMain.Text = TDFunctions.FnCheckSpcRuleHasPanelRef.Name;
            txtSQL.Text = $"{Environment.NewLine}{TDFunctions.FnCheckSpcRuleHasPanelRef.CommandText}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }

        #endregion Stored procedures fn_check_*


        #region Stored procedures fn_get_*

        /// <summary>
        /// Test stored procedure: fn_get_area_domain
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemFnGetAreaDomain_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(dgvData);

            TDAreaDomainList result = TDFunctions.FnGetAreaDomain.Execute(
                database: Database,
                areaDomainId: 200);
            TDAreaDomainList.SetColumnsVisibility(
                (from a in TDAreaDomainList.Cols
                 orderby a.Value.DisplayIndex
                 select a.Value.Name).ToArray<string>());
            result.SetDataGridViewDataSource(dgvData);
            result.FormatDataGridView(dgvData);
            grpMain.Text = TDFunctions.FnGetAreaDomain.Name;
            txtSQL.Text = $"{Environment.NewLine}{TDFunctions.FnGetAreaDomain.CommandText}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }

        /// <summary>
        /// Test stored procedure: fn_get_area_domain_category
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemFnGetAreaDomainCategory_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(dgvData);

            TDAreaDomainCategoryList result = TDFunctions.FnGetAreaDomainCategory.Execute(
                database: Database,
                targetVariableId: null,
                ldsityId: null);
            TDAreaDomainCategoryList.SetColumnsVisibility(
                (from a in TDAreaDomainCategoryList.Cols
                 orderby a.Value.DisplayIndex
                 select a.Value.Name).ToArray<string>());
            result.SetDataGridViewDataSource(dgvData);
            result.FormatDataGridView(dgvData);
            grpMain.Text = TDFunctions.FnGetAreaDomainCategory.Name;
            txtSQL.Text = $"{Environment.NewLine}{TDFunctions.FnGetAreaDomainCategory.CommandText}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }

        /// <summary>
        /// Test stored procedure: fn_get_area_domain_category4domain
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemFnGetAreaDomainCategoryForDomain_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(dgvData);

            TDAreaDomainCategoryList result = TDFunctions.FnGetAreaDomainCategoryForDomain.Execute(
                database: Database,
                areaDomainId: 200);
            TDAreaDomainCategoryList.SetColumnsVisibility(
                (from a in TDAreaDomainCategoryList.Cols
                 orderby a.Value.DisplayIndex
                 select a.Value.Name).ToArray<string>());
            result.SetDataGridViewDataSource(dgvData);
            result.FormatDataGridView(dgvData);
            grpMain.Text = TDFunctions.FnGetAreaDomainCategoryForDomain.Name;
            txtSQL.Text = $"{Environment.NewLine}{TDFunctions.FnGetAreaDomainCategoryForDomain.CommandText}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }

        /// <summary>
        /// Test stored procedure: fn_get_areal_or_population
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemFnGetArealOrPopulation_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(dgvData);

            TDArealOrPopulationList result = TDFunctions.FnGetArealOrPopulation.Execute(
                database: Database);
            TDArealOrPopulationList.SetColumnsVisibility(
                (from a in TDArealOrPopulationList.Cols
                 orderby a.Value.DisplayIndex
                 select a.Value.Name).ToArray<string>());
            result.SetDataGridViewDataSource(dgvData);
            result.FormatDataGridView(dgvData);
            grpMain.Text = TDFunctions.FnGetArealOrPopulation.Name;
            txtSQL.Text = $"{Environment.NewLine}{TDFunctions.FnGetArealOrPopulation.CommandText}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }

        /// <summary>
        /// Test stored procedure: fn_get_definition_variant
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemFnGetDefinitionVariant_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(dgvData);

            TDDefinitionVariantList result = TDFunctions.FnGetDefinitionVariant.Execute(
                database: Database,
                definitionVariantId: 2);
            TDDefinitionVariantList.SetColumnsVisibility(
                (from a in TDDefinitionVariantList.Cols
                 orderby a.Value.DisplayIndex
                 select a.Value.Name).ToArray<string>());
            result.SetDataGridViewDataSource(dgvData);
            result.FormatDataGridView(dgvData);
            grpMain.Text = TDFunctions.FnGetDefinitionVariant.Name;
            txtSQL.Text = $"{Environment.NewLine}{TDFunctions.FnGetDefinitionVariant.CommandText}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }

        /// <summary>
        /// Test stored procedure: fn_get_definition_variant4ldsity
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemFnGetDefinitionVariantForLDsity(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(dgvData);

            TDDefinitionVariantList result = TDFunctions.FnGetDefinitionVariantForLDsity.Execute(
                database: Database,
                ldsityId: 2);
            TDDefinitionVariantList.SetColumnsVisibility(
                (from a in TDDefinitionVariantList.Cols
                 orderby a.Value.DisplayIndex
                 select a.Value.Name).ToArray<string>());
            result.SetDataGridViewDataSource(dgvData);
            result.FormatDataGridView(dgvData);
            grpMain.Text = TDFunctions.FnGetDefinitionVariantForLDsity.Name;
            txtSQL.Text = $"{Environment.NewLine}{TDFunctions.FnGetDefinitionVariantForLDsity.CommandText}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }

        /// <summary>
        /// Test stored procedure: fn_get_hierarchy_adc
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemFnGetHierarchyAdc_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(dgvData);

            TDADCHierarchyList result = TDFunctions.FnGetHierarchyAdc.Execute(
                database: Database,
                areaDomainSuperiorId: 100,
                areaDomainInferiorId: 200,
                dependent: false,
                nullInferior: true);
            TDADCHierarchyList.SetColumnsVisibility(
                (from a in TDADCHierarchyList.Cols
                 orderby a.Value.DisplayIndex
                 select a.Value.Name).ToArray<string>());
            result.SetDataGridViewDataSource(dgvData);
            result.FormatDataGridView(dgvData);
            grpMain.Text = TDFunctions.FnGetHierarchyAdc.Name;
            txtSQL.Text = $"{Environment.NewLine}{TDFunctions.FnGetHierarchyAdc.CommandText}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }

        /// <summary>
        /// Test stored procedure: fn_get_hierarchy_spc
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemFnGetHierarchySpc_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(dgvData);

            TDSPCHierarchyList result = TDFunctions.FnGetHierarchySpc.Execute(
                database: Database,
                subPopulationSuperiorId: 100,
                subPopulationInferiorId: 200,
                dependent: false,
                nullInferior: true);
            TDSPCHierarchyList.SetColumnsVisibility(
                (from a in TDSPCHierarchyList.Cols
                 orderby a.Value.DisplayIndex
                 select a.Value.Name).ToArray<string>());
            result.SetDataGridViewDataSource(dgvData);
            result.FormatDataGridView(dgvData);
            grpMain.Text = TDFunctions.FnGetHierarchySpc.Name;
            txtSQL.Text = $"{Environment.NewLine}{TDFunctions.FnGetHierarchySpc.CommandText}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }

        /// <summary>
        /// Test stored procedure: fn_get_ldsity
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemFnGetLDsity_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(dgvData);

            TDLDsityList result = TDFunctions.FnGetLDsity.Execute(
                database: Database,
                targetVariableIds: [1],
                ldsityObjectTypeId: 100
                );
            TDLDsityList.SetColumnsVisibility(
                (from a in TDLDsityList.Cols
                 orderby a.Value.DisplayIndex
                 select a.Value.Name).ToArray<string>());
            result.SetDataGridViewDataSource(dgvData);
            result.FormatDataGridView(dgvData);
            grpMain.Text = TDFunctions.FnGetLDsity.Name;
            txtSQL.Text = $"{Environment.NewLine}{TDFunctions.FnGetLDsity.CommandText}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }

        /// <summary>
        /// Test stored procedure: fn_get_ldsity4object
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemFnGetLDsityForObject_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(dgvData);

            TDLDsityList result = TDFunctions.FnGetLDsityForObject.Execute(
                database: Database,
                ldsityObjectId: 200,
                unitOfMeasureId: 200);
            TDLDsityList.SetColumnsVisibility(
                (from a in TDLDsityList.Cols
                 orderby a.Value.DisplayIndex
                 select a.Value.Name).ToArray<string>());
            result.SetDataGridViewDataSource(dgvData);
            result.FormatDataGridView(dgvData);
            grpMain.Text = TDFunctions.FnGetLDsityForObject.Name;
            txtSQL.Text = $"{Environment.NewLine}{TDFunctions.FnGetLDsityForObject.CommandText}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }

        /// <summary>
        /// Test stored procedure: fn_get_ldsity_object
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemFnGetLDsityObject_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(dgvData);

            TDLDsityObjectList result = TDFunctions.FnGetLDsityObject.Execute(
                database: Database);
            TDLDsityObjectList.SetColumnsVisibility(
                (from a in TDLDsityObjectList.Cols
                 orderby a.Value.DisplayIndex
                 select a.Value.Name).ToArray<string>());
            result.SetDataGridViewDataSource(dgvData);
            result.FormatDataGridView(dgvData);
            grpMain.Text = TDFunctions.FnGetLDsityObject.Name;
            txtSQL.Text = $"{Environment.NewLine}{TDFunctions.FnGetLDsityObject.CommandText}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }

        /// <summary>
        /// Test stored procedure: fn_get_ldsity_object4adsp
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemFnGetLDsityObjectForADSP_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(dgvData);

            TDLDsityObjectList result = TDFunctions.FnGetLDsityObjectForADSP.Execute(
                database: Database,
                ldsityObjectId: 200,
                arealOrPopulationId: 100);
            TDLDsityObjectList.SetColumnsVisibility(
                (from a in TDLDsityObjectList.Cols
                 orderby a.Value.DisplayIndex
                 select a.Value.Name).ToArray<string>());
            result.SetDataGridViewDataSource(dgvData);
            result.FormatDataGridView(dgvData);
            grpMain.Text = TDFunctions.FnGetLDsityObjectForADSP.Name;
            txtSQL.Text = $"{Environment.NewLine}{TDFunctions.FnGetLDsityObjectForADSP.CommandText}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }

        /// <summary>
        /// Test stored procedure: fn_get_ldsity_object4ld_object_adsp
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemFnGetLDsityObjectForLDsityObjectADSP_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(dgvData);

            TDLDsityObjectList result = TDFunctions.FnGetLDsityObjectForLDsityObjectADSP.Execute(
                database: Database,
                ldsityObjectId: 200,
                arealOrPopulationId: 200,
                domainId: 300,
                useNegative: false);
            TDLDsityObjectList.SetColumnsVisibility(
                (from a in TDLDsityObjectList.Cols
                 orderby a.Value.DisplayIndex
                 select a.Value.Name).ToArray<string>());
            result.SetDataGridViewDataSource(dgvData);
            result.FormatDataGridView(dgvData);
            grpMain.Text = TDFunctions.FnGetLDsityObjectForLDsityObjectADSP.Name;
            txtSQL.Text = $"{Environment.NewLine}{TDFunctions.FnGetLDsityObjectForLDsityObjectADSP.CommandText}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }

        /// <summary>
        /// Test stored procedure: fn_get_ldsity_object4ld_object_group
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemFnGetLDsityObjectForLDsityObjectGroup_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(dgvData);

            TDLDsityObjectList result = TDFunctions.FnGetLDsityObjectForLDsityObjectGroup.Execute(
                database: Database,
                ldsityObjectGroupId: 3);
            TDLDsityObjectList.SetColumnsVisibility(
                (from a in TDLDsityObjectList.Cols
                 orderby a.Value.DisplayIndex
                 select a.Value.Name).ToArray<string>());
            result.SetDataGridViewDataSource(dgvData);
            result.FormatDataGridView(dgvData);
            grpMain.Text = TDFunctions.FnGetLDsityObjectForLDsityObjectGroup.Name;
            txtSQL.Text = $"{Environment.NewLine}{TDFunctions.FnGetLDsityObjectForLDsityObjectGroup.CommandText}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }

        /// <summary>
        /// Test stored procedure: fn_get_ldsity_object_group
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemFnGetLDsityObjectGroup_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(dgvData);

            TDLDsityObjectGroupList result = TDFunctions.FnGetLDsityObjectGroup.Execute(
                database: Database,
                ldsityObjectGroupId: 3);
            TDLDsityObjectGroupList.SetColumnsVisibility(
                (from a in TDLDsityObjectGroupList.Cols
                 orderby a.Value.DisplayIndex
                 select a.Value.Name).ToArray<string>());
            result.SetDataGridViewDataSource(dgvData);
            result.FormatDataGridView(dgvData);
            grpMain.Text = TDFunctions.FnGetLDsityObjectGroup.Name;
            txtSQL.Text = $"{Environment.NewLine}{TDFunctions.FnGetLDsityObjectGroup.CommandText}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }

        /// <summary>
        /// Test stored procedure: fn_get_ldsity_object_type
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemFnGetLDsityObjectType_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(dgvData);

            TDLDsityObjectTypeList result = TDFunctions.FnGetLDsityObjectType.Execute(
                database: Database);
            TDLDsityObjectTypeList.SetColumnsVisibility(
                (from a in TDLDsityObjectTypeList.Cols
                 orderby a.Value.DisplayIndex
                 select a.Value.Name).ToArray<string>());
            result.SetDataGridViewDataSource(dgvData);
            result.FormatDataGridView(dgvData);
            grpMain.Text = TDFunctions.FnGetLDsityObjectType.Name;
            txtSQL.Text = $"{Environment.NewLine}{TDFunctions.FnGetLDsityObjectType.CommandText}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }

        /// <summary>
        /// Test stored procedure: fn_get_ldsity_subpop4object
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemFnGetLDsitySubPopForObject_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(dgvData);

            TDLDsityList result = TDFunctions.FnGetLDsitySubPopForObject.Execute(
                database: Database,
                ldsityObjectId: 100);
            TDLDsityList.SetColumnsVisibility(
                (from a in TDLDsityList.Cols
                 orderby a.Value.DisplayIndex
                 select a.Value.Name).ToArray<string>());
            result.SetDataGridViewDataSource(dgvData);
            result.FormatDataGridView(dgvData);
            grpMain.Text = TDFunctions.FnGetLDsitySubPopForObject.Name;
            txtSQL.Text = $"{Environment.NewLine}{TDFunctions.FnGetLDsitySubPopForObject.CommandText}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }

        /// <summary>
        /// Test stored procedure: fn_get_panel_refyearset
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemFnGetPanelRefYearSet_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(dgvData);

            TDFnGetPanelRefYearSetTypeList result = TDFunctions.FnGetPanelRefYearSet.Execute(
                database: Database,
                panelRefYearSetId: null);
            TDFnGetPanelRefYearSetTypeList.SetColumnsVisibility(
                (from a in TDFnGetPanelRefYearSetTypeList.Cols
                 orderby a.Value.DisplayIndex
                 select a.Value.Name).ToArray<string>());
            result.SetDataGridViewDataSource(dgvData);
            result.FormatDataGridView(dgvData);
            grpMain.Text = TDFunctions.FnGetPanelRefYearSet.Name;
            txtSQL.Text = $"{Environment.NewLine}{TDFunctions.FnGetPanelRefYearSet.CommandText}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }

        /// <summary>
        /// Test stored procedure: fn_get_panel_refyearset_group
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemFnGetPanelRefYearSetGroup_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(dgvData);

            TDPanelRefYearSetGroupList result = TDFunctions.FnGetPanelRefYearSetGroup.Execute(
                database: Database,
                panelRefYearSetGroupId: 1);
            TDPanelRefYearSetGroupList.SetColumnsVisibility(
                (from a in TDPanelRefYearSetGroupList.Cols
                 orderby a.Value.DisplayIndex
                 select a.Value.Name).ToArray<string>());
            result.SetDataGridViewDataSource(dgvData);
            result.FormatDataGridView(dgvData);
            grpMain.Text = TDFunctions.FnGetPanelRefYearSetGroup.Name;
            txtSQL.Text = $"{Environment.NewLine}{TDFunctions.FnGetPanelRefYearSetGroup.CommandText}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }

        /// <summary>
        /// Test stored procedure: fn_get_state_or_change
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemFnGetStateOrChange_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(dgvData);

            TDStateOrChangeList result = TDFunctions.FnGetStateOrChange.Execute(
                database: Database);
            TDStateOrChangeList.SetColumnsVisibility(
                (from a in TDStateOrChangeList.Cols
                 orderby a.Value.DisplayIndex
                 select a.Value.Name).ToArray<string>());
            result.SetDataGridViewDataSource(dgvData);
            result.FormatDataGridView(dgvData);
            grpMain.Text = TDFunctions.FnGetStateOrChange.Name;
            txtSQL.Text = $"{Environment.NewLine}{TDFunctions.FnGetStateOrChange.CommandText}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }

        /// <summary>
        /// Test stored procedure: fn_get_sub_population
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemFnGetSupPopulation_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(dgvData);

            TDSubPopulationList result = TDFunctions.FnGetSubPopulation.Execute(
                database: Database,
                subPopulationId: 300);
            TDSubPopulationList.SetColumnsVisibility(
                (from a in TDSubPopulationList.Cols
                 orderby a.Value.DisplayIndex
                 select a.Value.Name).ToArray<string>());
            result.SetDataGridViewDataSource(dgvData);
            result.FormatDataGridView(dgvData);
            grpMain.Text = TDFunctions.FnGetSubPopulation.Name;
            txtSQL.Text = $"{Environment.NewLine}{TDFunctions.FnGetSubPopulation.CommandText}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }

        /// <summary>
        /// Test stored procedure: fn_get_sub_population_category
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemFnGetSupPopulationCategory_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(dgvData);

            TDSubPopulationCategoryList result = TDFunctions.FnGetSubPopulationCategory.Execute(
                database: Database,
                targetVariableId: null,
                ldsityId: null);
            TDSubPopulationCategoryList.SetColumnsVisibility(
                (from a in TDSubPopulationCategoryList.Cols
                 orderby a.Value.DisplayIndex
                 select a.Value.Name).ToArray<string>());
            result.SetDataGridViewDataSource(dgvData);
            result.FormatDataGridView(dgvData);
            grpMain.Text = TDFunctions.FnGetSubPopulationCategory.Name;
            txtSQL.Text = $"{Environment.NewLine}{TDFunctions.FnGetSubPopulationCategory.CommandText}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }

        /// <summary>
        /// Test stored procedure: fn_get_sub_population_category4population
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemFnGetSubPopulationCategoryForPopulation_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(dgvData);

            TDSubPopulationCategoryList result = TDFunctions.FnGetSubPopulationCategoryForPopulation.Execute(
                database: Database,
                subPopulationId: 300);
            TDSubPopulationCategoryList.SetColumnsVisibility(
                (from a in TDSubPopulationCategoryList.Cols
                 orderby a.Value.DisplayIndex
                 select a.Value.Name).ToArray<string>());
            result.SetDataGridViewDataSource(dgvData);
            result.FormatDataGridView(dgvData);
            grpMain.Text = TDFunctions.FnGetSubPopulationCategoryForPopulation.Name;
            txtSQL.Text = $"{Environment.NewLine}{TDFunctions.FnGetSubPopulationCategoryForPopulation.CommandText}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }

        /// <summary>
        /// Test stored procedure: fn_get_target_variable
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemFnGetTargetVariableAgg_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(dgvData);

            TDFnGetTargetVariableTypeList result =
                TDFunctions.FnGetTargetVariable.Execute(
                    database: Database,
                    ldsityObjectGroupId: null);
            TDFnGetTargetVariableTypeList.SetColumnsVisibility(
                (from a in TDFnGetTargetVariableTypeList.Cols
                 orderby a.Value.DisplayIndex
                 select a.Value.Name).ToArray<string>());
            result.SetDataGridViewDataSource(dgvData);
            result.FormatDataGridView(dgvData);
            grpMain.Text = TDFunctions.FnGetTargetVariable.Name;
            txtSQL.Text = $"{Environment.NewLine}{TDFunctions.FnGetTargetVariable.CommandText}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }

        /// <summary>
        /// Test stored procedure: fn_get_target_variable
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemFnGetTargetVariable_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(dgvData);

            TDFnGetTargetVariableTypeList fnResult =
                TDFunctions.FnGetTargetVariable.Execute(
                    database: Database,
                    ldsityObjectGroupId: null);
            TDTargetVariableList result = fnResult.ConvertToTargetVariableList();

            TDTargetVariableList.SetColumnsVisibility(
                (from a in TDTargetVariableList.Cols
                 orderby a.Value.DisplayIndex
                 select a.Value.Name).ToArray<string>());
            result.SetDataGridViewDataSource(dgvData);
            result.FormatDataGridView(dgvData);
            grpMain.Text = TDFunctions.FnGetTargetVariable.Name;
            txtSQL.Text = $"{Environment.NewLine}{TDFunctions.FnGetTargetVariable.CommandText}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }

        /// <summary>
        /// Test stored procedure: fn_get_unit_of_measure
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemFnGetUnitOfMeasure_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(dgvData);

            TDUnitOfMeasureList result = TDFunctions.FnGetUnitOfMeasure.Execute(
                database: Database,
                ldsityId: 2);
            TDUnitOfMeasureList.SetColumnsVisibility(
                (from a in TDUnitOfMeasureList.Cols
                 orderby a.Value.DisplayIndex
                 select a.Value.Name).ToArray<string>());
            result.SetDataGridViewDataSource(dgvData);
            result.FormatDataGridView(dgvData);
            grpMain.Text = TDFunctions.FnGetUnitOfMeasure.Name;
            txtSQL.Text = $"{Environment.NewLine}{TDFunctions.FnGetUnitOfMeasure.CommandText}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }

        /// <summary>
        /// Test stored procedure:fn_get_unit_of_measure4ld_object_group
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemFnGetUnitOfMeasureForLDsityObjectGroup_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(dgvData);

            TDUnitOfMeasureList result = TDFunctions.FnGetUnitOfMeasureForLDsityObjectGroup.Execute(
                database: Database,
                ldsityObjectGroupId: 3);
            TDUnitOfMeasureList.SetColumnsVisibility(
                (from a in TDUnitOfMeasureList.Cols
                 orderby a.Value.DisplayIndex
                 select a.Value.Name).ToArray<string>());
            result.SetDataGridViewDataSource(dgvData);
            result.FormatDataGridView(dgvData);
            grpMain.Text = TDFunctions.FnGetUnitOfMeasureForLDsityObjectGroup.Name;
            txtSQL.Text = $"{Environment.NewLine}{TDFunctions.FnGetUnitOfMeasureForLDsityObjectGroup.CommandText}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }

        /// <summary>
        /// Test stored procedure:fn_get_version
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemFnGetVersion_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(dgvData);

            TDVersionList result = TDFunctions.FnGetVersion.Execute(
                database: Database,
                targetVariableId: null,
                ldsityId: null);
            TDVersionList.SetColumnsVisibility(
                (from a in TDVersionList.Cols
                 orderby a.Value.DisplayIndex
                 select a.Value.Name).ToArray<string>());
            result.SetDataGridViewDataSource(dgvData);
            result.FormatDataGridView(dgvData);
            grpMain.Text = TDFunctions.FnGetVersion.Name;
            txtSQL.Text = $"{Environment.NewLine}{TDFunctions.FnGetVersion.CommandText}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }

        #endregion Stored procedures fn_get_*


        #region Other Stored procedures fn_get_ *

        /// <summary>
        /// Test stored procedure: fn_add_plot_target_attr
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemFnAddPlotTargetAttr_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill, ReadOnly = true };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(dgvData);

            DataTable result = TDFunctions.FnAddPlotTargetAttr.ExecuteQuery(
                database: Database,
                variables: [1, 2, 3],
                plots: [1, 2, 3],
                refyearsets: [1, 2, 3],
                minDiff: 0.0,
                includeNullDiff: true);
            dgvData.DataSource = result;
            grpMain.Text = TDFunctions.FnAddPlotTargetAttr.Name;
            txtSQL.Text = $"{Environment.NewLine}{TDFunctions.FnAddPlotTargetAttr.CommandText}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }

        /// <summary>
        /// Test stored procedure: fn_get_attribute_domain_ad
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemFnGetAttributeDomainAD_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            ListBox lstData = new()
            {
                Dock = DockStyle.Fill,
                SelectionMode = SelectionMode.None
            };
            pnlData.Controls.Add(lstData);

            TDDomainCategoryCombinationList<TDAreaDomainCategory> result =
                TDFunctions.FnGetAttributeDomainAD.Execute(
                    database: Database,
                    areaDomains:
                    [
                            new() {Id = 100},
                            new() {Id = 200}
                    ]);
            lstData.DataSource = result.Items;
            lstData.DisplayMember = "ExtendedLabelCs";
            lstData.ValueMember = "Id";
            grpMain.Text = TDFunctions.FnGetAttributeDomainAD.Name;
            txtSQL.Text = $"{Environment.NewLine}{TDFunctions.FnGetAttributeDomainAD.CommandText}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }

        /// <summary>
        /// Test stored procedure: fn_get_attribute_domain_sp
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemFnGetAttributeDomainSP_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            ListBox lstData = new()
            {
                Dock = DockStyle.Fill,
                SelectionMode = SelectionMode.None
            };
            pnlData.Controls.Add(lstData);

            TDDomainCategoryCombinationList<TDSubPopulationCategory> result =
                TDFunctions.FnGetAttributeDomainSP.Execute(
                    database: Database,
                    subPopulations:
                    [
                            new () {Id = 1},
                            new () {Id = 300}
                    ]);
            lstData.DataSource = result.Items;
            lstData.DisplayMember = "ExtendedLabelCs";
            lstData.ValueMember = "Id";
            grpMain.Text = TDFunctions.FnGetAttributeDomainSP.Name;
            txtSQL.Text = $"{Environment.NewLine}{TDFunctions.FnGetAttributeDomainSP.CommandText}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }

        /// <summary>
        /// Test stored procedure: fn_get_available_refyearset2panel4classifiaction_rule_id
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemFnGetAvailableRefYearSetToPanelForClassificationRuleIdAdc_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill, ReadOnly = true };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(dgvData);

            DataTable result = TDFunctions.FnGetAvailableRefYearSetToPanelForClassificationRuleIdAdc.ExecuteQuery(
                database: Database,
                classificationRuleId: 1);
            dgvData.DataSource = result;
            grpMain.Text = TDFunctions.FnGetAvailableRefYearSetToPanelForClassificationRuleIdAdc.Name;
            txtSQL.Text = $"{Environment.NewLine}{TDFunctions.FnGetAvailableRefYearSetToPanelForClassificationRuleIdAdc.CommandText}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }

        /// <summary>
        /// Test stored procedure: fn_get_available_refyearset2panel4classifiaction_rule_id
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemFnGetAvailableRefYearSetToPanelForClassificationRuleIdSpc_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill, ReadOnly = true };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(dgvData);

            DataTable result = TDFunctions.FnGetAvailableRefYearSetToPanelForClassificationRuleIdSpc.ExecuteQuery(
                database: Database,
                classificationRuleId: 1);
            dgvData.DataSource = result;
            grpMain.Text = TDFunctions.FnGetAvailableRefYearSetToPanelForClassificationRuleIdSpc.Name;
            txtSQL.Text = $"{Environment.NewLine}{TDFunctions.FnGetAvailableRefYearSetToPanelForClassificationRuleIdSpc.CommandText}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }

        /// <summary>
        /// Test stored procedure: fn_get_case_ad4categorization_setups
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemFnGetCaseAdForCategorizationSetups_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            TextBox txtData = new()
            {
                Dock = DockStyle.Fill,
                Font = new Font("Courier New", 10.0f, FontStyle.Regular),
                BackColor = SystemColors.Window,
                ForeColor = SystemColors.WindowText,
                ReadOnly = true,
                BorderStyle = BorderStyle.None,
                Multiline = true,
                WordWrap = true
            };
            pnlData.Controls.Add(txtData);

            string result = TDFunctions.FnGetCaseAdForCategorizationSetups.Execute(
                database: Database,
                categorizationSetupIds: [1, 2]);
            txtData.Text = result;
            grpMain.Text = TDFunctions.FnGetCaseAdForCategorizationSetups.Name;
            txtSQL.Text = $"{Environment.NewLine}{TDFunctions.FnGetCaseAdForCategorizationSetups.CommandText}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }

        /// <summary>
        /// Test stored procedure: fn_get_category4classification_rule_id
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemFnGetCategoryForClassificationRuleIdAdc_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill, ReadOnly = true };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(dgvData);

            DataTable result = TDFunctions.FnGetCategoryForClassificationRuleIdAdc.ExecuteQuery(
                database: Database,
                classificationRuleIds: [2, 3]);
            dgvData.DataSource = result;
            grpMain.Text = TDFunctions.FnGetCategoryForClassificationRuleIdAdc.Name;
            txtSQL.Text = $"{Environment.NewLine}{TDFunctions.FnGetCategoryForClassificationRuleIdAdc.CommandText}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }

        /// <summary>
        /// Test stored procedure: fn_get_category4classification_rule_id
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemFnGetCategoryForClassificationRuleIdSpc_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill, ReadOnly = true };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(dgvData);

            DataTable result = TDFunctions.FnGetCategoryForClassificationRuleIdSpc.ExecuteQuery(
                database: Database,
                classificationRuleIds: [5, 6]);
            dgvData.DataSource = result;
            grpMain.Text = TDFunctions.FnGetCategoryForClassificationRuleIdSpc.Name;
            txtSQL.Text = $"{Environment.NewLine}{TDFunctions.FnGetCategoryForClassificationRuleIdSpc.CommandText}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }

        /// <summary>
        /// Test stored procedure: fn_get_category_type4classification_rule_id
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemFnGetCategoryTypeForClassificationRuleIdAdc_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill, ReadOnly = true };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(dgvData);

            DataTable result = TDFunctions.FnGetCategoryTypeForClassificationRuleIdAdc.ExecuteQuery(
                database: Database,
                classificationRuleIds: [2, 3]);
            dgvData.DataSource = result;
            grpMain.Text = TDFunctions.FnGetCategoryTypeForClassificationRuleIdAdc.Name;
            txtSQL.Text = $"{Environment.NewLine}{TDFunctions.FnGetCategoryTypeForClassificationRuleIdAdc.CommandText}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }

        /// <summary>
        /// Test stored procedure: fn_get_category_type4classification_rule_id
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemFnGetCategoryTypeForClassificationRuleIdSpc_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill, ReadOnly = true };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(dgvData);

            DataTable result = TDFunctions.FnGetCategoryTypeForClassificationRuleIdSpc.ExecuteQuery(
                database: Database,
                classificationRuleIds: [5, 6]);
            dgvData.DataSource = result;
            grpMain.Text = TDFunctions.FnGetCategoryTypeForClassificationRuleIdSpc.Name;
            txtSQL.Text = $"{Environment.NewLine}{TDFunctions.FnGetCategoryTypeForClassificationRuleIdSpc.CommandText}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }

        /// <summary>
        /// Test stored procedure: fn_get_classification_rule4adc
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemFnGetClassificationRuleForAdc_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(dgvData);

            TDADCToClassificationRuleList result = TDFunctions.FnGetClassificationRuleForAdc.Execute(
                database: Database,
                areaDomainCategoryId: 201,
                ldsityObjectId: 100,
                useNegative: false);
            TDADCToClassificationRuleList.SetColumnsVisibility(
                (from a in TDADCToClassificationRuleList.Cols
                 orderby a.Value.DisplayIndex
                 select a.Value.Name).ToArray<string>());
            result.SetDataGridViewDataSource(dgvData);
            result.FormatDataGridView(dgvData);
            grpMain.Text = TDFunctions.FnGetClassificationRuleForAdc.Name;
            txtSQL.Text = $"{Environment.NewLine}{TDFunctions.FnGetClassificationRuleForAdc.CommandText}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }

        /// <summary>
        /// Test stored procedure: fn_get_classification_rule4spc
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemFnGetClassificationRuleForSpc_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(dgvData);

            TDSPCToClassificationRuleList result = TDFunctions.FnGetClassificationRuleForSpc.Execute(
                database: Database,
                subPopulationCategoryId: 301,
                ldsityObjectId: 200,
                useNegative: false);
            TDSPCToClassificationRuleList.SetColumnsVisibility(
                (from a in TDSPCToClassificationRuleList.Cols
                 orderby a.Value.DisplayIndex
                 select a.Value.Name).ToArray<string>());
            result.SetDataGridViewDataSource(dgvData);
            result.FormatDataGridView(dgvData);
            grpMain.Text = TDFunctions.FnGetClassificationRuleForSpc.Name;
            txtSQL.Text = $"{Environment.NewLine}{TDFunctions.FnGetClassificationRuleForSpc.CommandText}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }

        /// <summary>
        /// Test stored procedure: fn_get_classification_rule4classification_rule_id
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemFnGetClassificationRuleForClassificationRuleIdAdc_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            TextBox txtData = new()
            {
                Dock = DockStyle.Fill,
                Font = new Font("Courier New", 10.0f, FontStyle.Regular),
                BackColor = SystemColors.Window,
                ForeColor = SystemColors.WindowText,
                ReadOnly = true,
                BorderStyle = BorderStyle.None,
                Multiline = true,
                WordWrap = true
            };
            pnlData.Controls.Add(txtData);

            string result = TDFunctions.FnGetClassificationRuleForClassificationRuleIdAdc.Execute(
                database: Database,
                classificationRuleIds: [1, 2],
                ldsityObjectIds: [1, 2]);
            txtData.Text = result;
            grpMain.Text = TDFunctions.FnGetClassificationRuleForClassificationRuleIdAdc.Name;
            txtSQL.Text = $"{Environment.NewLine}{TDFunctions.FnGetClassificationRuleForClassificationRuleIdAdc.CommandText}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }

        /// <summary>
        /// Test stored procedure: fn_get_classification_rule4classification_rule_id
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemFnGetClassificationRuleForClassificationRuleIdSpc_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            TextBox txtData = new()
            {
                Dock = DockStyle.Fill,
                Font = new Font("Courier New", 10.0f, FontStyle.Regular),
                BackColor = SystemColors.Window,
                ForeColor = SystemColors.WindowText,
                ReadOnly = true,
                BorderStyle = BorderStyle.None,
                Multiline = true,
                WordWrap = true
            };
            pnlData.Controls.Add(txtData);

            string result = TDFunctions.FnGetClassificationRuleForClassificationRuleIdSpc.Execute(
                database: Database,
                classificationRuleIds: [1, 2],
                ldsityObjectIds: [1, 2]);
            txtData.Text = result;
            grpMain.Text = TDFunctions.FnGetClassificationRuleForClassificationRuleIdSpc.Name;
            txtSQL.Text = $"{Environment.NewLine}{TDFunctions.FnGetClassificationRuleForClassificationRuleIdSpc.CommandText}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }

        /// <summary>
        /// Test stored procedure: fn_get_classification_rule_id4category
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemFnGetClassificationRuleId4Category_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill, ReadOnly = true };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(dgvData);

            DataTable result = TDFunctions.FnGetClassificationRuleId4Category.ExecuteQuery(
                database: Database,
                areaDomainCategoryIds: [201, 202],
                areaDomainLDsityObjectIds: [100, 100],
                subPopulationCategoryIds: [1, 2],
                subPopulationLDsityObjectIds: [200, 200],
                useNegativeAd: [false, false],
                useNegativeSp: [false, false]
                );
            dgvData.DataSource = result;
            grpMain.Text = TDFunctions.FnGetClassificationRuleId4Category.Name;
            txtSQL.Text = $"{Environment.NewLine}{TDFunctions.FnGetClassificationRuleId4Category.CommandText}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }

        /// <summary>
        /// Test stored procedure: fn_get_classification_rules4categorization_setups
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemFnGetClassificationRules4CategorizationSetups_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            TextBox txtData = new()
            {
                Dock = DockStyle.Fill,
                Font = new Font("Courier New", 10.0f, FontStyle.Regular),
                BackColor = SystemColors.Window,
                ForeColor = SystemColors.WindowText,
                ReadOnly = true,
                BorderStyle = BorderStyle.None,
                Multiline = true,
                WordWrap = true
            };
            pnlData.Controls.Add(txtData);

            string result = TDFunctions.FnGetClassificationRules4CategorizationSetups.Execute(
                database: Database,
                categorizationSetupIds: [1, 2],
                ldsityToTargetVariableId: 1);
            txtData.Text = result;
            grpMain.Text = TDFunctions.FnGetClassificationRules4CategorizationSetups.Name;
            txtSQL.Text = $"{Environment.NewLine}{TDFunctions.FnGetClassificationRules4CategorizationSetups.CommandText}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }

        /// <summary>
        /// Test stored procedure: fn_get_classification_rules_id4categories
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemFnGetClassificationRulesId4Categories_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill, ReadOnly = true };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(dgvData);

            DataTable result = TDFunctions.FnGetClassificationRulesIdForCategories.ExecuteQuery(
                database: Database,
                areaDomainIds: [200],
                areaDomainLDsityObjectIds: [100],
                subPopulationIds: [300],
                subPopulationLDsityObjectIds: [200],
                useNegativeAd: [false],
                useNegativeSp: [false]);
            dgvData.DataSource = result;
            grpMain.Text = TDFunctions.FnGetClassificationRulesIdForCategories.Name;
            txtSQL.Text = $"{Environment.NewLine}{TDFunctions.FnGetClassificationRulesIdForCategories.CommandText}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }

        /// <summary>
        /// Test stored procedure: fn_get_eligible_panel_refyearset_combinations
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemFnGetEligiblePanelRefYearSetCombinations_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(dgvData);

            TDFnGetEligiblePanelRefYearSetCombinationsTypeList result = TDFunctions.FnGetEligiblePanelRefYearSetCombinationsA.Execute(
                database: Database,
                targetVariableId: 1,
                categorizationSetupIds: [4]);
            TDFnGetEligiblePanelRefYearSetCombinationsTypeList.SetColumnsVisibility(
                (from a in TDFnGetEligiblePanelRefYearSetCombinationsTypeList.Cols
                 orderby a.Value.DisplayIndex
                 select a.Value.Name).ToArray<string>());
            result.SetDataGridViewDataSource(dgvData);
            result.FormatDataGridView(dgvData);
            grpMain.Text = TDFunctions.FnGetEligiblePanelRefYearSetCombinationsA.Name;
            txtSQL.Text = $"{Environment.NewLine}{TDFunctions.FnGetEligiblePanelRefYearSetCombinationsA.CommandText}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }

        /// <summary>
        /// Test stored procedure: fn_get_eligible_panel_refyearset_groups
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemFnGetEligiblePanelRefyearsetGroups_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(dgvData);

            TDPanelRefYearSetGroupList result = TDFunctions.FnGetEligiblePanelRefYearSetGroups.Execute(
                database: Database,
                eligibleRefYearSetToPanelMappingIds: [1]);
            TDPanelRefYearSetGroupList.SetColumnsVisibility(
                (from a in TDPanelRefYearSetGroupList.Cols
                 orderby a.Value.DisplayIndex
                 select a.Value.Name).ToArray<string>());
            result.SetDataGridViewDataSource(dgvData);
            result.FormatDataGridView(dgvData);
            grpMain.Text = TDFunctions.FnGetEligiblePanelRefYearSetGroups.Name;
            txtSQL.Text = $"{Environment.NewLine}{TDFunctions.FnGetEligiblePanelRefYearSetGroups.CommandText}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }

        /// <summary>
        /// Test stored procedure: fn_get_ldsity_objects
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemFnGetLDsityObjects_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill, ReadOnly = true };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(dgvData);

            DataTable result = TDFunctions.FnGetLDsityObjects.ExecuteQuery(
                database: Database,
                ldsityObjectIds: [400]);
            dgvData.DataSource = result;
            grpMain.Text = TDFunctions.FnGetLDsityObjects.Name;
            txtSQL.Text = $"{Environment.NewLine}{TDFunctions.FnGetLDsityObjects.CommandText}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }

        /// <summary>
        /// Test stored procedure: fn_get_plots
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemFnGetPlots_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill, ReadOnly = true };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(dgvData);

            DataTable result = TDFunctions.FnGetPlots.ExecuteQuery(
                database: Database,
                refYearSetToPanelMappingIds: [1, 2],
                stateOrChangeId: 100,
                useNegative: false);

            dgvData.DataSource = result;
            grpMain.Text = TDFunctions.FnGetPlots.Name;
            txtSQL.Text = $"{Environment.NewLine}{TDFunctions.FnGetPlots.CommandText}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }

        /// <summary>
        /// Test stored procedure: fn_get_refyearset2panel_mapping4group
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemFnGetRefYearSetToPanelMappingForGroup_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill, ReadOnly = true };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(dgvData);

            DataTable result = TDFunctions.FnGetRefYearSetToPanelMappingForGroup.ExecuteQuery(
                database: Database,
                panelRefYearSetGroupId: 1);

            dgvData.DataSource = result;
            grpMain.Text = TDFunctions.FnGetPlots.Name;
            txtSQL.Text = $"{Environment.NewLine}{TDFunctions.FnGetPlots.CommandText}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }

        #endregion Other Stored procedures fn_get_ *


        #region Get ETL

        /// <summary>
        /// Test stored procedure: fn_etl_get_export_connections
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemFnEtlGetExportConnections_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(dgvData);

            TDExportConnectionList result = TDFunctions.FnEtlGetExportConnections.Execute(
                database: Database);
            TDExportConnectionList.SetColumnsVisibility(
                (from a in TDExportConnectionList.Cols
                 orderby a.Value.DisplayIndex
                 select a.Value.Name).ToArray<string>());
            result.SetDataGridViewDataSource(dgvData);
            result.FormatDataGridView(dgvData);
            grpMain.Text = TDFunctions.FnEtlGetExportConnections.Name;
            txtSQL.Text = $"{Environment.NewLine}{TDFunctions.FnEtlGetExportConnections.CommandText}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }

        /// <summary>
        /// Test stored procedure: fn_etl_get_target_variable
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemFnEtlGetTargetVariable_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(dgvData);

            TDFnEtlGetTargetVariableTypeList result = TDFunctions.FnEtlGetTargetVariable.Execute(
                database: Database,
                exportConnection: 1);
            TDFnEtlGetTargetVariableTypeList.SetColumnsVisibility(
                (from a in TDFnEtlGetTargetVariableTypeList.Cols
                 orderby a.Value.DisplayIndex
                 select a.Value.Name).ToArray<string>());
            result.SetDataGridViewDataSource(dgvData);
            result.FormatDataGridView(dgvData);
            grpMain.Text = TDFunctions.FnEtlGetTargetVariable.Name;
            txtSQL.Text = $"{Environment.NewLine}{TDFunctions.FnEtlGetTargetVariable.CommandText}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }

        /// <summary>
        /// Test stored procedure: fn_etl_get_target_variable (this one splits the metadata json into data table columns)
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemFnEtlGetTargetVariableExtended_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(dgvData);

            TDFnEtlGetTargetVariableTypeList fnEtlGetTargetVariables = TDFunctions.FnEtlGetTargetVariable.Execute(
                 database: Database,
                 exportConnection: 1);
            TDFnEtlGetTargetVariableMetadataList fnEtlGetTargetVariablesExtended = new(
                database: Database);
            fnEtlGetTargetVariablesExtended.ReLoad(
                fnEtlGetTargetVariable: fnEtlGetTargetVariables);

            TDFnEtlGetTargetVariableMetadataList.SetColumnsVisibility(
                (from a in TDFnEtlGetTargetVariableMetadataList.Cols
                 orderby a.Value.DisplayIndex
                 select a.Value.Name).ToArray<string>());
            fnEtlGetTargetVariablesExtended.SetDataGridViewDataSource(dgvData);
            fnEtlGetTargetVariablesExtended.FormatDataGridView(dgvData);
            grpMain.Text = TDFunctions.FnEtlGetTargetVariable.Name;
            txtSQL.Text = $"{Environment.NewLine}{TDFunctions.FnEtlGetTargetVariable.CommandText}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }

        /// <summary>
        /// Test stored procedure: fn_etl_get_target_variable (this one aggregates rows by target variable id)
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemFnEtlGetTargetVariableAggregated_Click(object sender, EventArgs e)
        {
            pnlData.Controls.Clear();
            DataGridView dgvData = new() { Dock = DockStyle.Fill };
            dgvData.SelectionChanged += (a, b) => { dgvData.ClearSelection(); };
            pnlData.Controls.Add(dgvData);

            TDFnEtlGetTargetVariableTypeList fnEtlGetTargetVariables = TDFunctions.FnEtlGetTargetVariable.Execute(
                 database: Database,
                 exportConnection: 1);

            TDFnEtlGetTargetVariableMetadataList fnEtlGetTargetVariablesExtended = new(
                database: Database);
            fnEtlGetTargetVariablesExtended.ReLoad(
                fnEtlGetTargetVariable: fnEtlGetTargetVariables);

            TDFnEtlGetTargetVariableMetadataList fnEtlGetTargetVariablesAggregated =
                fnEtlGetTargetVariablesExtended.Aggregated();

            TDFnEtlGetTargetVariableMetadataList.SetColumnsVisibility(
                (from a in TDFnEtlGetTargetVariableMetadataList.Cols
                 orderby a.Value.DisplayIndex
                 select a.Value.Name).ToArray<string>());
            fnEtlGetTargetVariablesAggregated.SetDataGridViewDataSource(dgvData);
            fnEtlGetTargetVariablesAggregated.FormatDataGridView(dgvData);
            grpMain.Text = TDFunctions.FnEtlGetTargetVariable.Name;
            txtSQL.Text = $"{Environment.NewLine}{TDFunctions.FnEtlGetTargetVariable.CommandText}{Environment.NewLine}";
            PostgreSQL.Functions.HighlightSQLSyntax(txtSQL);
        }

        /// <summary>
        /// Stored procedure fn_etl_get_target_variable_metadata aggregated
        /// displayed in target variable selector
        /// </summary>
        /// <param name="sender">Reference to the object that raised the event (ToolStripMenuItem)</param>
        /// <param name="e">Event data</param>
        private void ItemControlTargetVariableSelector_Click(object sender, EventArgs e)
        {
            TDFnEtlGetTargetVariableTypeList fnEtlGetTargetVariable =
              TDFunctions.FnEtlGetTargetVariable.Execute(
                  database: Database,
                  exportConnection: 1,
                  nationalLanguage: Language.CS);

            pnlData.Controls.Clear();
            Controls.ControlTargetVariableSelector ctrTargetVariableSelector
                = new(controlOwner: this)
                {
                    Design = TargetVariableSelectorDesign.Simple,
                    Dock = DockStyle.Fill,
                    MetadataElementsText = "Metadata",
                    MetadataIndicatorText = "Indicator",
                    NotAssignedCategory = true,
                    Spacing = [5]
                };
            ctrTargetVariableSelector.CreateControl();
            pnlData.Controls.Add(value: ctrTargetVariableSelector);

            ctrTargetVariableSelector.Data = fnEtlGetTargetVariable;
            ctrTargetVariableSelector.LoadContent();
        }

        #endregion Get ETL

        #endregion Event Handlers

    }

}