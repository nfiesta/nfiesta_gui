﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {
                // fn_etl_get_sub_population

                #region FnEtlGetSubPopulation

                /// <summary>
                /// Wrapper for stored procedure fn_etl_get_sub_population.
                /// Function returns records for ETL c_sub_population table.
                /// </summary>
                public static class FnEtlGetSubPopulation
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_etl_get_sub_population";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_refyearset2panel_mapping integer[], ",
                            $"_id_t_etl_target_variable integer, ",
                            $"_auto_information json, ",
                            $"_etl boolean DEFAULT NULL::boolean; ",
                            $"returns: ",
                            $"TABLE(",
                            $"id integer, ",
                            $"export_connection integer, ",
                            $"sub_population integer[], ",
                            $"label character varying, ",
                            $"description text, ",
                            $"label_en character varying, ",
                            $"description_en text, ",
                            $"etl_id integer, ",
                            $"id_t_etl_sp integer, ",
                            $"atomic boolean, ",
                            $"auto_transfer boolean, ",
                            $"auto_pair boolean, ",
                            $"auto boolean)");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    {Name}.id                                                     AS id,{Environment.NewLine}",
                            $"    {Name}.export_connection                                      AS export_connection,{Environment.NewLine}",
                            $"    array_to_string({Name}.sub_population, ';', 'NULL')           AS sub_population,{Environment.NewLine}",
                            $"    {Name}.label                                                  AS label,{Environment.NewLine}",
                            $"    {Name}.description                                            AS description,{Environment.NewLine}",
                            $"    {Name}.label_en                                               AS label_en,{Environment.NewLine}",
                            $"    {Name}.description_en                                         AS description_en,{Environment.NewLine}",
                            $"    {Name}.etl_id                                                 AS etl_id,{Environment.NewLine}",
                            $"    {Name}.id_t_etl_sp                                            AS id_t_etl_sp,{Environment.NewLine}",
                            $"    {Name}.atomic                                                 AS atomic,{Environment.NewLine}",
                            $"    {Name}.auto_transfer                                          AS auto_transfer,{Environment.NewLine}",
                            $"    {Name}.auto_pair                                              AS auto_pair,{Environment.NewLine}",
                            $"    {Name}.auto                                                   AS auto{Environment.NewLine}",
                            $"FROM{Environment.NewLine}",
                            $"    $SchemaName.$Name(@refYearSetToPanelMapping, @idTEtlTargetVariable, @autoInformation, @etl) AS {Name}{Environment.NewLine}",
                            $"ORDER BY{Environment.NewLine}",
                            $"    {Name}.id;{Environment.NewLine}"
                            );

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="refYearSetToPanelMapping"></param>
                    /// <param name="idTEtlTargetVariable"></param>
                    /// <param name="autoInformation">Json</param>
                    /// <param name="etl"></param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        List<Nullable<int>> refYearSetToPanelMapping,
                        Nullable<int> idTEtlTargetVariable,
                        string autoInformation,
                        Nullable<bool> etl)
                    {
                        string result = SQL;

                        result = result.Replace(
                            oldValue: "@refYearSetToPanelMapping",
                            newValue: Functions.PrepNIntArrayArg(args: refYearSetToPanelMapping, dbType: "int4"));

                        result = result.Replace(
                            oldValue: "@idTEtlTargetVariable",
                            newValue: Functions.PrepNIntArg(arg: idTEtlTargetVariable));

                        result = result.Replace(
                            oldValue: "@autoInformation",
                            newValue: Functions.PrepStringArg(arg: autoInformation));

                        result = result.Replace(
                            oldValue: "@etl",
                            newValue: Functions.PrepNBoolArg(arg: etl));

                        return result;
                    }

                    /// <summary>
                    ///  Execute stored procedure fn_etl_get_sub_population
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="refYearSetToPanelMapping"></param>
                    /// <param name="idTEtlTargetVariable"></param>
                    /// <param name="autoInformation">Json</param>
                    /// <param name="etl"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        List<Nullable<int>> refYearSetToPanelMapping,
                        Nullable<int> idTEtlTargetVariable,
                        string autoInformation,
                        Nullable<bool> etl,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(
                            refYearSetToPanelMapping: refYearSetToPanelMapping,
                            idTEtlTargetVariable: idTEtlTargetVariable,
                            autoInformation: autoInformation,
                            etl: etl);

                        NpgsqlParameter pRefYearSetToPanelMapping = new(
                            parameterName: "refYearSetToPanelMapping",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        NpgsqlParameter pIdTEtlTargetVariable = new(
                            parameterName: "idTEtlTargetVariable",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pAutoInformation = new(
                            parameterName: "autoInformation",
                            parameterType: NpgsqlDbType.Json);

                        NpgsqlParameter pEtl = new(
                          parameterName: "etl",
                          parameterType: NpgsqlDbType.Boolean);

                        if (refYearSetToPanelMapping != null)
                        {
                            pRefYearSetToPanelMapping.Value = refYearSetToPanelMapping.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pRefYearSetToPanelMapping.Value = DBNull.Value;
                        }

                        if (idTEtlTargetVariable != null)
                        {
                            pIdTEtlTargetVariable.Value = (int)idTEtlTargetVariable;
                        }
                        else
                        {
                            pIdTEtlTargetVariable.Value = DBNull.Value;
                        }

                        if (autoInformation != null)
                        {
                            pAutoInformation.Value = (string)autoInformation;
                        }
                        else
                        {
                            pAutoInformation.Value = DBNull.Value;
                        }

                        if (etl != null)
                        {
                            pEtl.Value = (bool)etl;
                        }
                        else
                        {
                            pEtl.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteQuery(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pRefYearSetToPanelMapping, pIdTEtlTargetVariable, pAutoInformation, pEtl);
                    }

                    #endregion Methods

                }

                #endregion FnEtlGetSubPopulation

            }

        }
    }
}