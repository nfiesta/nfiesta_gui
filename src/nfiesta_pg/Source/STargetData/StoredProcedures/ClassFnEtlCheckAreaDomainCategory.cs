﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {
                // fn_etl_check_area_domain_category

                #region FnEtlCheckAreaDomainCategory

                /// <summary>
                /// Wrapper for stored procedure fn_etl_check_area_domain_category.
                /// Function returns records for ETL c_area_domain_category table.
                /// </summary>
                public static class FnEtlCheckAreaDomainCategory
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_etl_check_area_domain_category";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_refyearset2panel_mapping integer[], ",
                            $"_id_t_etl_target_variable integer, ",
                            $"_id_t_etl_ad integer DEFAULT NULL::integer; ",
                            $"returns: ",
                            $"TABLE(",
                            $"id integer, ",
                            $"export_connection integer, ",
                            $"etl_id_area_domain integer, ",
                            $"label_area_domain character varying, ",
                            $"label_en_area_domain character varying, ",
                            $"area_domain_category integer[], ",
                            $"label character varying, ",
                            $"description text, ",
                            $"label_en character varying, ",
                            $"description_en text, ",
                            $"etl_id integer, ",
                            $"id_t_etl_adc integer, ",
                            $"id_t_etl_ad integer)");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    {Name}.id                                                   AS id,{Environment.NewLine}",
                            $"    {Name}.export_connection                                    AS export_connection,{Environment.NewLine}",
                            $"    {Name}.etl_id_area_domain                                   AS etl_id_area_domain,{Environment.NewLine}",
                            $"    {Name}.label_area_domain                                    AS label_area_domain,{Environment.NewLine}",
                            $"    {Name}.label_en_area_domain                                 AS label_en_area_domain,{Environment.NewLine}",
                            $"    array_to_string({Name}.area_domain_category, ';', 'NULL')   AS area_domain_category,{Environment.NewLine}",
                            $"    {Name}.label                                                AS label,{Environment.NewLine}",
                            $"    {Name}.description                                          AS description,{Environment.NewLine}",
                            $"    {Name}.label_en                                             AS label_en,{Environment.NewLine}",
                            $"    {Name}.description_en                                       AS description_en,{Environment.NewLine}",
                            $"    {Name}.etl_id                                               AS etl_id,{Environment.NewLine}",
                            $"    {Name}.id_t_etl_adc                                         AS id_t_etl_adc,{Environment.NewLine}",
                            $"    {Name}.id_t_etl_ad                                          AS id_t_etl_ad{Environment.NewLine}",
                            $"FROM{Environment.NewLine}",
                            $"    $SchemaName.$Name(@refYearSetToPanelMapping, @idTEtlTargetVariable, @idTEtlAd) AS {Name}{Environment.NewLine}",
                            $"ORDER BY{Environment.NewLine}",
                            $"    {Name}.id;{Environment.NewLine}"
                            );

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="refYearSetToPanelMapping"></param>
                    /// <param name="idTEtlTargetVariable"></param>
                    /// <param name="idTEtlAd"></param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        List<Nullable<int>> refYearSetToPanelMapping,
                        Nullable<int> idTEtlTargetVariable,
                        Nullable<int> idTEtlAd = null)
                    {
                        string result = SQL;

                        result = result.Replace(
                            oldValue: "@refYearSetToPanelMapping",
                            newValue: Functions.PrepNIntArrayArg(args: refYearSetToPanelMapping, dbType: "int4"));

                        result = result.Replace(
                            oldValue: "@idTEtlTargetVariable",
                            newValue: Functions.PrepNIntArg(arg: idTEtlTargetVariable));

                        result = result.Replace(
                            oldValue: "@idTEtlAd",
                            newValue: Functions.PrepNIntArg(arg: idTEtlAd));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_etl_check_area_domain_category
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="refYearSetToPanelMapping"></param>
                    /// <param name="idTEtlTargetVariable"></param>
                    /// <param name="idTEtlAd"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        List<Nullable<int>> refYearSetToPanelMapping,
                        Nullable<int> idTEtlTargetVariable,
                        Nullable<int> idTEtlAd = null,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(
                            refYearSetToPanelMapping: refYearSetToPanelMapping,
                            idTEtlTargetVariable: idTEtlTargetVariable,
                            idTEtlAd: idTEtlAd);

                        NpgsqlParameter pRefYearSetToPanelMapping = new(
                            parameterName: "refYearSetToPanelMapping",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        NpgsqlParameter pIdTEtlTargetVariable = new(
                            parameterName: "idTEtlTargetVariable",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pIdTEtlAd = new(
                            parameterName: "idTEtlAd",
                            parameterType: NpgsqlDbType.Integer);

                        if (refYearSetToPanelMapping != null)
                        {
                            pRefYearSetToPanelMapping.Value = refYearSetToPanelMapping.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pRefYearSetToPanelMapping.Value = DBNull.Value;
                        }

                        if (idTEtlTargetVariable != null)
                        {
                            pIdTEtlTargetVariable.Value = (int)idTEtlTargetVariable;
                        }
                        else
                        {
                            pIdTEtlTargetVariable.Value = DBNull.Value;
                        }

                        if (idTEtlAd != null)
                        {
                            pIdTEtlAd.Value = (int)idTEtlAd;
                        }
                        else
                        {
                            pIdTEtlAd.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteQuery(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pRefYearSetToPanelMapping, pIdTEtlTargetVariable, pIdTEtlAd);
                    }

                    #endregion Methods

                }

                #endregion FnEtlCheckAreaDomainCategory

            }

        }
    }
}