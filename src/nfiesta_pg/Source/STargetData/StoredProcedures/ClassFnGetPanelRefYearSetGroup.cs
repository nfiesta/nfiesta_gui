﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {

                #region FnGetPanelRefYearSetGroup

                /// <summary>
                /// Wrapper for stored procedure fn_get_panel_refyearset_group.
                /// Function returns records from c_panel_refyearset_group table.
                /// </summary>
                public static class FnGetPanelRefYearSetGroup
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_get_panel_refyearset_group";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_id integer DEFAULT NULL::integer; ",
                            $"returns: ",
                            $"TABLE(",
                            $"id integer, ",
                            $"label character varying, ",
                            $"description text, ",
                            $"label_en character varying, ",
                            $"description_en text)");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    {TDPanelRefYearSetGroupList.ColId.SQL(TDPanelRefYearSetGroupList.Cols, Name)}",
                            $"    {TDPanelRefYearSetGroupList.ColLabelCs.SQL(TDPanelRefYearSetGroupList.Cols, Name)}",
                            $"    {TDPanelRefYearSetGroupList.ColDescriptionCs.SQL(TDPanelRefYearSetGroupList.Cols, Name)}",
                            $"    {TDPanelRefYearSetGroupList.ColLabelEn.SQL(TDPanelRefYearSetGroupList.Cols, Name)}",
                            $"    {TDPanelRefYearSetGroupList.ColDescriptionEn.SQL(TDPanelRefYearSetGroupList.Cols, Name, isLastOne: true)}",
                            $"FROM{Environment.NewLine}",
                            $"    $SchemaName.$Name(",
                            $"@panelRefYearSetGroupId) ",
                            $"AS $Name{Environment.NewLine}",
                            $"ORDER BY{Environment.NewLine}",
                            $"    $Name.{TDPanelRefYearSetGroupList.ColId.DbName};{Environment.NewLine}"
                            );

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Execute stored procedure fn_get_panel_refyearset_group
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="panelRefYearSetGroupId">Identifier of aggregated sets of panels and corresponding reference year sets
                    /// (primary key from table c_panel_refyearset_group)</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table with aggregated sets of panels and corresponding reference year sets</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> panelRefYearSetGroupId = null,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = SQL;

                        CommandText = CommandText.Replace(
                            oldValue: "@panelRefYearSetGroupId",
                            newValue: Functions.PrepNIntArg(arg: panelRefYearSetGroupId));

                        NpgsqlParameter pPanelRefYearSetGroupId = new(
                            parameterName: "panelRefYearSetGroupId",
                            parameterType: NpgsqlDbType.Integer);

                        if (panelRefYearSetGroupId != null)
                        {
                            pPanelRefYearSetGroupId.Value = (int)panelRefYearSetGroupId;
                        }
                        else
                        {
                            pPanelRefYearSetGroupId.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteQuery(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pPanelRefYearSetGroupId);
                    }

                    /// <summary>
                    /// Execute stored procedure fn_get_panel_refyearset_group
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="panelRefYearSetGroupId">Identifier of aggregated sets of panels and corresponding reference year sets
                    /// (primary key from table c_panel_refyearset_group)</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>List of aggregated sets of panels and corresponding reference year sets</returns>
                    public static TDPanelRefYearSetGroupList Execute(
                        NfiEstaDB database,
                        Nullable<int> panelRefYearSetGroupId = null,
                        NpgsqlTransaction transaction = null)
                    {
                        return new TDPanelRefYearSetGroupList(
                            database: database,
                            data: ExecuteQuery(
                                database: database,
                                panelRefYearSetGroupId: panelRefYearSetGroupId,
                                transaction: transaction))
                        { StoredProcedure = CommandText };
                    }

                    #endregion Methods

                }

                #endregion FnGetPanelRefYearSetGroup

            }

        }
    }
}