﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {

                #region FnGetCategoryTypeForClassificationRuleIdSpc

                /// <summary>
                /// Wrapper for stored procedure fn_get_category_type4classification_rule_id.
                /// The function gets category type for input classification rules.
                /// </summary>
                public static class FnGetCategoryTypeForClassificationRuleIdSpc
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_get_category_type4classification_rule_id";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_classification_rule_type character varying, ",
                            $"_classification_rule_id integer[]; ",
                            $"returns: ",
                            $"TABLE(",
                            $"id_type integer[], ",
                            $"label character varying, ",
                            $"description text, ",
                            $"label_en character varying, ",
                            $"description_en text)");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    array_to_string($Name.id_type, ';', 'NULL') AS id_type,{Environment.NewLine}",
                            $"    $Name.label                                 AS label,{Environment.NewLine}",
                            $"    $Name.description                           AS description,{Environment.NewLine}",
                            $"    $Name.label_en                              AS label_en,{Environment.NewLine}",
                            $"    $Name.description_en                        AS description_en{Environment.NewLine}",
                            $"FROM{Environment.NewLine}",
                            $"    $SchemaName.$Name(@classificationRuleType, @classificationRuleIds) AS $Name{Environment.NewLine}",
                            $"ORDER BY{Environment.NewLine}",
                            $"    array_to_string($Name.id_type, ';', 'NULL');{Environment.NewLine}"
                            );

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Execute stored procedure fn_get_category_type4classification_rule_id
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="classificationRuleIds">Classification rules identifiers</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table with columns: id_type - classification rule type identifier,
                    /// label - classification rule type label in national language, description - classification rule type description in national language
                    /// label_en - classification rule type label in English, description_en - classification rule type description in English
                    /// </returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        List<Nullable<int>> classificationRuleIds,
                        NpgsqlTransaction transaction = null)
                    {
                        string classificationRuleType = "spc";

                        CommandText = SQL;

                        CommandText = CommandText.Replace(
                            oldValue: "@classificationRuleType",
                            newValue: Functions.PrepStringArg(arg: classificationRuleType));

                        CommandText = CommandText.Replace(
                            oldValue: "@classificationRuleIds",
                            newValue: Functions.PrepNIntArrayArg(args: classificationRuleIds, dbType: "int4"));

                        NpgsqlParameter pClassificationRuleType = new(
                            parameterName: "classificationRuleType",
                            parameterType: NpgsqlDbType.Varchar);

                        NpgsqlParameter pClassificationRuleIds = new(
                            parameterName: "classificationRuleIds",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        if (classificationRuleType != null)
                        {
                            pClassificationRuleType.Value = (string)classificationRuleType;
                        }
                        else
                        {
                            pClassificationRuleType.Value = DBNull.Value;
                        }

                        if (classificationRuleIds != null)
                        {
                            pClassificationRuleIds.Value = classificationRuleIds.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pClassificationRuleIds.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteQuery(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pClassificationRuleType, pClassificationRuleIds);
                    }

                    #endregion Methods

                }

                #endregion FnGetCategoryTypeForClassificationRuleIdSpc

            }
        }
    }
}