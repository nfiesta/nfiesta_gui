﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {

                #region FnImportSaveAdcToClassRuleToPanelRefYearSet

                /// <summary>
                /// Wrapper for stored procedure fn_import_save_adc2classrule2panel_refyearset.
                /// </summary>
                public static class FnImportSaveAdcToClassRuleToPanelRefYearSet
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_import_save_adc2classrule2panel_refyearset";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_id integer, ",
                            $"_adc2classification_rule integer, ",
                            $"_refyearset2panel integer; ",
                            $"returns: ",
                            $"TABLE(",
                            $"id integer, ",
                            $"etl_id integer)");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    id,{Environment.NewLine}",
                            $"    etl_id{Environment.NewLine}",
                            $"FROM{Environment.NewLine}",
                            $"    $SchemaName.$Name(",
                            $"@id, ",
                            $"@adcToClassificationRule, ",
                            $"@refYearSetToPanel);{Environment.NewLine}");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Execute stored procedure fn_import_save_adc2classrule2panel_refyearset
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="id"></param>
                    /// <param name="adcToClassificationRule"></param>
                    /// <param name="refYearSetToPanel"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> id,
                        Nullable<int> adcToClassificationRule,
                        Nullable<int> refYearSetToPanel,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = SQL;

                        CommandText = CommandText.Replace(
                            oldValue: "@id",
                            newValue: Functions.PrepNIntArg(arg: id));

                        CommandText = CommandText.Replace(
                            oldValue: "@adcToClassificationRule",
                            newValue: Functions.PrepNIntArg(arg: adcToClassificationRule));

                        CommandText = CommandText.Replace(
                           oldValue: "@refYearSetToPanel",
                           newValue: Functions.PrepNIntArg(arg: refYearSetToPanel));

                        NpgsqlParameter pId = new(
                            parameterName: "id",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pAdcToClassificationRule = new(
                            parameterName: "adcToClassificationRule",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pRefYearSetToPanel = new(
                            parameterName: "refYearSetToPanel",
                            parameterType: NpgsqlDbType.Integer);

                        if (id != null)
                        {
                            pId.Value = (int)id;
                        }
                        else
                        {
                            pId.Value = DBNull.Value;
                        }

                        if (adcToClassificationRule != null)
                        {
                            pAdcToClassificationRule.Value = (int)adcToClassificationRule;
                        }
                        else
                        {
                            pAdcToClassificationRule.Value = DBNull.Value;
                        }

                        if (refYearSetToPanel != null)
                        {
                            pRefYearSetToPanel.Value = (int)refYearSetToPanel;
                        }
                        else
                        {
                            pRefYearSetToPanel.Value = DBNull.Value;
                        }

                        return
                            database.Postgres.ExecuteQuery(
                                sqlCommand: SQL,
                                transaction: transaction,
                                pId, pAdcToClassificationRule, pRefYearSetToPanel);
                    }

                    #endregion Methods

                }

                #endregion FnImportSaveAdcToClassRuleToPanelRefYearSet

            }

        }
    }
}