﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using System;
using System.Data;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {

                #region FnGetLDsityObjectType

                /// <summary>
                /// Wrapper for stored procedure fn_get_ldsity_object_type.
                /// Function returns records from c_ldsity_object_type table.
                /// </summary>
                public static class FnGetLDsityObjectType
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_get_ldsity_object_type";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ; ",
                            $"returns: ",
                            $"TABLE(",
                            $"id integer, ",
                            $"label character varying, ",
                            $"description text)");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    {TDLDsityObjectTypeList.ColId.SQL(TDLDsityObjectTypeList.Cols, Name)}",
                            $"    {TDLDsityObjectTypeList.ColLabelEn.SQL(TDLDsityObjectTypeList.Cols, Name)}",
                            $"    {TDLDsityObjectTypeList.ColDescriptionEn.SQL(TDLDsityObjectTypeList.Cols, Name)}",
                            $"    {TDLDsityObjectTypeList.ColLabelCs.SQL(TDLDsityObjectTypeList.Cols, Name, setToNull: true)}",
                            $"    {TDLDsityObjectTypeList.ColDescriptionCs.SQL(TDLDsityObjectTypeList.Cols, Name, isLastOne: true, setToNull: true)}",
                            $"FROM{Environment.NewLine}",
                            $"    $SchemaName.$Name() AS {Name}{Environment.NewLine}",
                            $"ORDER BY{Environment.NewLine}",
                            $"    {Name}.{TDLDsityObjectTypeList.ColId.DbName};{Environment.NewLine}"
                            );

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Execute stored procedure fn_get_ldsity_object_type
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table with local density object types</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = SQL;

                        DataTable data = database.Postgres.ExecuteQuery(
                            sqlCommand: SQL,
                            transaction: transaction);

                        TDLDsityObjectTypeList.FixNationalLabels(dt: data);

                        return data;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_get_ldsity_object_type
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>List of local density object types</returns>
                    public static TDLDsityObjectTypeList Execute(
                        NfiEstaDB database,
                        NpgsqlTransaction transaction = null)
                    {
                        return new TDLDsityObjectTypeList(
                            database: database,
                            data: ExecuteQuery(
                                database: database,
                                transaction: transaction))
                        { StoredProcedure = CommandText };
                    }

                    #endregion Methods

                }

                #endregion FnGetLDsityObjectType

            }

        }
    }
}