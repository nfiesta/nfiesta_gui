﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {
                // fn_etl_get_area_domain_categories4update

                #region FnEtlGetAreaDomainCategoriesForUpdate

                /// <summary>
                /// Wrapper for stored procedure fn_etl_get_area_domain_categories4update.
                /// The function returns list of area domain categories that had already been ETLed for given area domain.
                /// </summary>
                public static class FnEtlGetAreaDomainCategoriesForUpdate
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_etl_get_area_domain_categories4update";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_etl_area_domain integer, ",
                            $"_area_domain_target integer; ",
                            $"returns: json");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    $SchemaName.$Name(@etlAreaDomain, @areaDomainTarget)::text AS $Name;{Environment.NewLine}");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="etlAreaDomain"></param>
                    /// <param name="areaDomainTarget"></param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        Nullable<int> etlAreaDomain,
                        Nullable<int> areaDomainTarget)
                    {
                        string result = SQL;

                        result = result.Replace(
                            oldValue: "@etlAreaDomain",
                            newValue: Functions.PrepNIntArg(arg: etlAreaDomain));

                        result = result.Replace(
                            oldValue: "@areaDomainTarget",
                            newValue: Functions.PrepNIntArg(arg: areaDomainTarget));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_etl_get_area_domain_categories4update
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="etlAreaDomain"></param>
                    /// <param name="areaDomainTarget"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> etlAreaDomain,
                        Nullable<int> areaDomainTarget,
                        NpgsqlTransaction transaction = null)
                    {
                        string val = Execute(
                            database: database,
                            etlAreaDomain: etlAreaDomain,
                            areaDomainTarget: areaDomainTarget,
                            transaction: transaction);

                        DataTable dt = new();

                        dt.Columns.Add(column: new DataColumn()
                        {
                            ColumnName = Name,
                            DataType = Type.GetType(typeName: "System.String")
                        });

                        DataRow row = dt.NewRow();

                        Functions.SetStringArg(
                            row: row,
                            name: Name,
                            val: val);

                        dt.Rows.Add(row: row);

                        return dt;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_etl_get_area_domain_categories4update
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="etlAreaDomain"></param>
                    /// <param name="areaDomainTarget"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table</returns>
                    public static string Execute(
                        NfiEstaDB database,
                        Nullable<int> etlAreaDomain,
                        Nullable<int> areaDomainTarget,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(
                            etlAreaDomain: etlAreaDomain,
                            areaDomainTarget: areaDomainTarget);

                        NpgsqlParameter pEtlAreaDomain = new(
                            parameterName: "etlAreaDomain",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pAreaDomainTarget = new(
                            parameterName: "areaDomainTarget",
                            parameterType: NpgsqlDbType.Integer);

                        if (etlAreaDomain != null)
                        {
                            pEtlAreaDomain.Value = (int)etlAreaDomain;
                        }
                        else
                        {
                            pEtlAreaDomain.Value = DBNull.Value;
                        }

                        if (areaDomainTarget != null)
                        {
                            pAreaDomainTarget.Value = (int)areaDomainTarget;
                        }
                        else
                        {
                            pAreaDomainTarget.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteScalar(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pEtlAreaDomain,
                            pAreaDomainTarget);
                    }

                    #endregion Methods

                }

                #endregion FnEtlGetAreaDomainCategoriesForUpdate

            }

        }
    }
}