﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {
                // fn_delete_hierarchy

                #region FnDeleteHierarchy

                /// <summary>
                /// Wrapper for stored procedure fn_delete_hierarchy.
                /// Function deletes records from
                /// t_adc_hierarchy/t_spc_hierarchy table.
                /// </summary>
                public static class FnDeleteHierarchy
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_delete_hierarchy";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_areal_or_pop integer, ",
                            $"_superior_cat integer[], ",
                            $"_inferior_cat integer[]; ",
                            $"returns: ",
                            $"TABLE(",
                            $"superior_cat integer, ",
                            $"inferior_cat integer)");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    superior_cat,{Environment.NewLine}",
                            $"    inferior_cat {Environment.NewLine}",
                            $"FROM{Environment.NewLine}",
                            $"    $SchemaName.$Name",
                            $"(@arealOrPopulationId, @superiorCategoriesId, @inferiorCategoriesId);{Environment.NewLine}");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="arealOrPopulation">Areal or population</param>
                    /// <param name="superiorCategoriesId">List of superior categories identifiers</param>
                    /// <param name="inferiorCategoriesId">List of inferior categories identifiers</param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        TDArealOrPopulationEnum arealOrPopulation,
                        List<Nullable<int>> superiorCategoriesId,
                        List<Nullable<int>> inferiorCategoriesId)
                    {
                        string result = SQL;

                        result = result.Replace(
                             oldValue: "@arealOrPopulationId",
                             newValue: Functions.PrepIntArg(arg: (int)arealOrPopulation));

                        result = result.Replace(
                            oldValue: "@superiorCategoriesId",
                            newValue: Functions.PrepNIntArrayArg(args: superiorCategoriesId, dbType: "int4"));

                        result = result.Replace(
                            oldValue: "@inferiorCategoriesId",
                            newValue: Functions.PrepNIntArrayArg(args: inferiorCategoriesId, dbType: "int4"));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_delete_hierarchy
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="arealOrPopulation">Areal or population</param>
                    /// <param name="superiorCategoriesId">List of superior categories identifiers</param>
                    /// <param name="inferiorCategoriesId">List of inferior categories identifiers</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        TDArealOrPopulationEnum arealOrPopulation,
                        List<Nullable<int>> superiorCategoriesId,
                        List<Nullable<int>> inferiorCategoriesId,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(
                            arealOrPopulation: arealOrPopulation,
                            superiorCategoriesId: superiorCategoriesId,
                            inferiorCategoriesId: inferiorCategoriesId);

                        NpgsqlParameter pArealOrPopulation = new(
                            parameterName: "arealOrPopulationId",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pSuperiorCategoriesId = new(
                            parameterName: "superiorCategoriesId",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        NpgsqlParameter pInferiorCategoriesId = new(
                            parameterName: "inferiorCategoriesId",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        pArealOrPopulation.Value = (int)arealOrPopulation;

                        if (superiorCategoriesId != null)
                        {
                            pSuperiorCategoriesId.Value = superiorCategoriesId.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pSuperiorCategoriesId.Value = DBNull.Value;
                        }

                        if (inferiorCategoriesId != null)
                        {
                            pInferiorCategoriesId.Value = inferiorCategoriesId.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pInferiorCategoriesId.Value = DBNull.Value;
                        }

                        return
                            database.Postgres.ExecuteQuery(
                                sqlCommand: SQL,
                                transaction: transaction,
                                pArealOrPopulation, pSuperiorCategoriesId, pInferiorCategoriesId);
                    }

                    /// <summary>
                    /// Execute stored procedure fn_delete_hierarchy
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="arealOrPopulation">Areal or population</param>
                    /// <param name="superiorCategoriesId">List of superior categories identifiers</param>
                    /// <param name="inferiorCategoriesId">List of inferior categories identifiers</param>
                    /// <param name="transaction">Transaction</param>
                    public static void Execute(
                        NfiEstaDB database,
                        TDArealOrPopulationEnum arealOrPopulation,
                        List<Nullable<int>> superiorCategoriesId,
                        List<Nullable<int>> inferiorCategoriesId,
                        NpgsqlTransaction transaction = null)
                    {
                        ExecuteQuery(
                            database: database,
                            arealOrPopulation: arealOrPopulation,
                            superiorCategoriesId: superiorCategoriesId,
                            inferiorCategoriesId: inferiorCategoriesId,
                            transaction: transaction
                            );

                        return;
                    }

                    #endregion Methods

                }

                #endregion FnDeleteHierarchy

            }

        }
    }
}