﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using System;
using System.Data;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {

                #region FnGetStateOrChange

                /// <summary>
                /// Wrapper for stored procedure fn_get_state_or_change.
                /// Function returns records from c_state_or_change table.
                /// </summary>
                public static class FnGetStateOrChange
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_get_state_or_change";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ; ",
                            $"returns: ",
                            $"TABLE(",
                            $"id integer, ",
                            $"label character varying, ",
                            $"description text)");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                                 $"SELECT{Environment.NewLine}",
                                 $"    {TDStateOrChangeList.ColId.SQL(TDStateOrChangeList.Cols, Name)}",
                                 $"    {TDStateOrChangeList.ColLabelEn.SQL(TDStateOrChangeList.Cols, Name)}",
                                 $"    {TDStateOrChangeList.ColDescriptionEn.SQL(TDStateOrChangeList.Cols, Name)}",
                                 $"    {TDStateOrChangeList.ColLabelCs.SQL(TDStateOrChangeList.Cols, Name, setToNull: true)}",
                                 $"    {TDStateOrChangeList.ColDescriptionCs.SQL(TDStateOrChangeList.Cols, Name, isLastOne: true, setToNull: true)}",
                                 $"FROM{Environment.NewLine}",
                                 $"    $SchemaName.$Name() AS {Name}{Environment.NewLine}",
                                 $"ORDER BY{Environment.NewLine}",
                                 $"    {Name}.{TDStateOrChangeList.ColId.DbName};{Environment.NewLine}"
                                 );

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Execute stored procedure fn_get_state_or_change
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table with state or change categories</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = SQL;

                        DataTable data = database.Postgres.ExecuteQuery(
                            sqlCommand: SQL,
                            transaction: transaction);

                        TDStateOrChangeList.FixNationalLabels(dt: data);

                        return data;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_get_state_or_change
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>List of state or change categories</returns>
                    public static TDStateOrChangeList Execute(
                        NfiEstaDB database,
                        NpgsqlTransaction transaction = null)
                    {
                        return new TDStateOrChangeList(
                            database: database,
                            data: ExecuteQuery(
                                database: database,
                                transaction: transaction))
                        { StoredProcedure = CommandText };
                    }

                    #endregion Methods

                }

                #endregion FnGetStateOrChange

            }

        }
    }
}