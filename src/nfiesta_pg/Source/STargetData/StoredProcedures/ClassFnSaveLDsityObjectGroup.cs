﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {

                #region FnSaveLDsityObjectGroup

                /// <summary>
                /// Wrapper for stored procedure fn_save_ldsity_object_group.
                /// Function provides insert into/update
                /// in c_ldsity_object_group table.
                /// </summary>
                public static class FnSaveLDsityObjectGroup
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_save_ldsity_object_group";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_label character varying, ",
                            $"_description text, ",
                            $"_label_en character varying DEFAULT NULL::character varying, ",
                            $"_description_en text DEFAULT NULL::text, ",
                            $"_ldsity_objects integer[] DEFAULT NULL::integer[], ",
                            $"_id integer DEFAULT NULL::integer; ",
                            $"returns: integer");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    $SchemaName.$Name(",
                            $"@labelCs, ",
                            $"@descriptionCs, ",
                            $"@labelEn, ",
                            $"@descriptionEn, ",
                            $"@ldsityObjectIds, ",
                            $"@ldsityObjectGroupId)::integer AS $Name;{Environment.NewLine}");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Execute stored procedure fn_save_ldsity_object_group
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="labelCs">Label of group of local density objects in national language</param>
                    /// <param name="descriptionCs">Description of group of local density objects in national language</param>
                    /// <param name="labelEn">Label of group of local density objects in English</param>
                    /// <param name="descriptionEn">Description of group of local density objects in English</param>
                    /// <param name="ldsityObjectIds">List with selected local density object identifiers in the group</param>
                    /// <param name="ldsityObjectGroupId">Group of local density objects identifier</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table with local density object group identifier</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        string labelCs,
                        string descriptionCs,
                        string labelEn,
                        string descriptionEn,
                        List<Nullable<int>> ldsityObjectIds = null,
                        Nullable<int> ldsityObjectGroupId = null,
                        NpgsqlTransaction transaction = null)
                    {
                        Nullable<int> val = Execute(
                            database: database,
                            labelCs: labelCs,
                            descriptionCs: descriptionCs,
                            labelEn: labelEn,
                            descriptionEn: descriptionEn,
                            ldsityObjectIds: ldsityObjectIds,
                            ldsityObjectGroupId: ldsityObjectGroupId,
                            transaction: transaction);

                        DataTable dt = new();

                        dt.Columns.Add(column: new DataColumn()
                        {
                            ColumnName = Name,
                            DataType = Type.GetType(typeName: "System.Int32")
                        });

                        DataRow row = dt.NewRow();

                        Functions.SetNIntArg(
                            row: row,
                            name: Name,
                            val: val);

                        dt.Rows.Add(row: row);

                        return dt;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_save_ldsity_object_group
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="labelCs">Label of group of local density objects in national language</param>
                    /// <param name="descriptionCs">Description of group of local density objects in national language</param>
                    /// <param name="labelEn">Label of group of local density objects in English</param>
                    /// <param name="descriptionEn">Description of group of local density objects in English</param>
                    /// <param name="ldsityObjects">List with selected local density objects in the group</param>
                    /// <param name="ldsityObjectGroupId">Group of local density objects identifier</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table with local density object group identifier</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        string labelCs,
                        string descriptionCs,
                        string labelEn,
                        string descriptionEn,
                        List<TDLDsityObject> ldsityObjects = null,
                        Nullable<int> ldsityObjectGroupId = null,
                        NpgsqlTransaction transaction = null)
                    {
                        if (ldsityObjects != null)
                        {
                            return ExecuteQuery(
                                database: database,
                                labelCs: labelCs,
                                descriptionCs: descriptionCs,
                                labelEn: labelEn,
                                descriptionEn: descriptionEn,
                                ldsityObjectIds: ldsityObjects
                                    .Select(a => (Nullable<int>)a.Id)
                                    .ToList<Nullable<int>>(),
                                ldsityObjectGroupId: ldsityObjectGroupId,
                                transaction: transaction); ;
                        }
                        else
                        {
                            return ExecuteQuery(
                                database: database,
                                labelCs: labelCs,
                                descriptionCs: descriptionCs,
                                labelEn: labelEn,
                                descriptionEn: descriptionEn,
                                ldsityObjectIds: (List<Nullable<int>>)null,
                                ldsityObjectGroupId: ldsityObjectGroupId,
                                transaction: transaction);
                        }
                    }

                    /// <summary>
                    /// Execute stored procedure fn_save_ldsity_object_group
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="labelCs">Label of group of local density objects in national language</param>
                    /// <param name="descriptionCs">Description of group of local density objects in national language</param>
                    /// <param name="labelEn">Label of group of local density objects in English</param>
                    /// <param name="descriptionEn">Description of group of local density objects in English</param>
                    /// <param name="ldsityObjectIds">List with selected local density object identifiers in the group</param>
                    /// <param name="ldsityObjectGroupId">Group of local density objects identifier</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Local density object group identifier</returns>
                    public static Nullable<int> Execute(
                        NfiEstaDB database,
                        string labelCs,
                        string descriptionCs,
                        string labelEn,
                        string descriptionEn,
                        List<Nullable<int>> ldsityObjectIds = null,
                        Nullable<int> ldsityObjectGroupId = null,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = SQL;

                        CommandText = CommandText.Replace(
                            oldValue: "@labelCs",
                            newValue: Functions.PrepStringArg(arg: labelCs));

                        CommandText = CommandText.Replace(
                            oldValue: "@descriptionCs",
                            newValue: Functions.PrepStringArg(arg: descriptionCs));

                        CommandText = CommandText.Replace(
                            oldValue: "@labelEn",
                            newValue: Functions.PrepStringArg(arg: labelEn));

                        CommandText = CommandText.Replace(
                            oldValue: "@descriptionEn",
                            newValue: Functions.PrepStringArg(arg: descriptionEn));

                        CommandText = CommandText.Replace(
                            oldValue: "@ldsityObjectIds",
                            newValue: Functions.PrepNIntArrayArg(args: ldsityObjectIds, dbType: "int4"));

                        CommandText = CommandText.Replace(
                            oldValue: "@ldsityObjectGroupId",
                            newValue: Functions.PrepNIntArg(arg: ldsityObjectGroupId));

                        NpgsqlParameter pLabelCs = new(
                            parameterName: "labelCs",
                            parameterType: NpgsqlDbType.Varchar);

                        NpgsqlParameter pDescriptionCs = new(
                            parameterName: "descriptionCs",
                            parameterType: NpgsqlDbType.Text);

                        NpgsqlParameter pLabelEn = new(
                            parameterName: "labelEn",
                            parameterType: NpgsqlDbType.Varchar);

                        NpgsqlParameter pDescriptionEn = new(
                            parameterName: "descriptionEn",
                            parameterType: NpgsqlDbType.Text);

                        NpgsqlParameter pLDsityObjectIds = new(
                            parameterName: "ldsityObjectIds",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        NpgsqlParameter pLDsityObjectGroupId = new(
                            parameterName: "ldsityObjectGroupId",
                            parameterType: NpgsqlDbType.Integer);

                        if (labelCs != null)
                        {
                            pLabelCs.Value = (string)labelCs;
                        }
                        else
                        {
                            pLabelCs.Value = DBNull.Value;
                        }

                        if (descriptionCs != null)
                        {
                            pDescriptionCs.Value = (string)descriptionCs;
                        }
                        else
                        {
                            pDescriptionCs.Value = DBNull.Value;
                        }

                        if (labelEn != null)
                        {
                            pLabelEn.Value = (string)labelEn;
                        }
                        else
                        {
                            pLabelEn.Value = DBNull.Value;
                        }

                        if (descriptionEn != null)
                        {
                            pDescriptionEn.Value = (string)descriptionEn;
                        }
                        else
                        {
                            pDescriptionEn.Value = DBNull.Value;
                        }

                        // UPDATE record in c_ldsity_object_group
                        if (ldsityObjectGroupId != null)
                        {
                            if (ldsityObjectIds != null)
                            {
                                throw new ArgumentException(
                                    message: "Argument ldsityObjectIds must be null.");
                            }

                            pLDsityObjectIds.Value = DBNull.Value;

                            pLDsityObjectGroupId.Value = (int)ldsityObjectGroupId;
                        }

                        // INSERT new record into c_ldsity_object_group
                        else
                        {
                            if (ldsityObjectIds == null)
                            {
                                throw new ArgumentException(
                                    message: "Arguments ldsityObjectIds must not be null.");
                            }

                            if (ldsityObjectIds.Count == 0)
                            {
                                throw new ArgumentException(
                                    message: "Arguments ldsityObjectIds must not be empty list.");
                            }

                            pLDsityObjectIds.Value = ldsityObjectIds.ToArray<Nullable<int>>();

                            pLDsityObjectGroupId.Value = DBNull.Value;
                        }

                        string strResult = database.Postgres.ExecuteScalar(
                                sqlCommand: SQL,
                                transaction: transaction,
                                pLabelCs, pDescriptionCs, pLabelEn, pDescriptionEn,
                                pLDsityObjectIds, pLDsityObjectGroupId);

                        if (String.IsNullOrEmpty(value: strResult))
                        {
                            return null;
                        }
                        else
                        {
                            if (Int32.TryParse(s: strResult, result: out int intResult))
                            {
                                return intResult;
                            }
                            else
                            {
                                return null;
                            }
                        }
                    }

                    /// <summary>
                    /// Execute stored procedure fn_save_ldsity_object_group
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="labelCs">Label of group of local density objects in national language</param>
                    /// <param name="descriptionCs">Description of group of local density objects in national language</param>
                    /// <param name="labelEn">Label of group of local density objects in English</param>
                    /// <param name="descriptionEn">Description of group of local density objects in English</param>
                    /// <param name="ldsityObjects">List with selected local density objects in the group</param>
                    /// <param name="ldsityObjectGroupId">Group of local density objects identifier</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Local density object group identifier</returns>
                    public static Nullable<int> Execute(
                        NfiEstaDB database,
                        string labelCs,
                        string descriptionCs,
                        string labelEn,
                        string descriptionEn,
                        List<TDLDsityObject> ldsityObjects = null,
                        Nullable<int> ldsityObjectGroupId = null,
                        NpgsqlTransaction transaction = null)
                    {
                        if (ldsityObjects != null)
                        {
                            return Execute(
                                database: database,
                                labelCs: labelCs,
                                descriptionCs: descriptionCs,
                                labelEn: labelEn,
                                descriptionEn: descriptionEn,
                                ldsityObjectIds: ldsityObjects
                                    .Select(a => (Nullable<int>)a.Id)
                                    .ToList<Nullable<int>>(),
                                ldsityObjectGroupId: ldsityObjectGroupId,
                                transaction: transaction); ;
                        }
                        else
                        {
                            return Execute(
                                database: database,
                                labelCs: labelCs,
                                descriptionCs: descriptionCs,
                                labelEn: labelEn,
                                descriptionEn: descriptionEn,
                                ldsityObjectIds: (List<Nullable<int>>)null,
                                ldsityObjectGroupId: ldsityObjectGroupId,
                                transaction: transaction);
                        }
                    }

                    #endregion Methods

                }

                #endregion FnSaveLDsityObjectGroup

            }

        }
    }
}