﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Versioning;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {

                #region FnGetAttributeDomainSP

                /// <summary>
                /// Wrapper for stored procedure fn_get_attribute_domain.
                /// The function returns the attribute domain field table
                /// for the specified attribute type combination.
                /// </summary>
                [SupportedOSPlatform("windows")]
                public static class FnGetAttributeDomainSP
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_get_attribute_domain";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_attr_type character varying, ",
                            $"_attr_types integer[]; ",
                            $"returns: ",
                            $"TABLE(",
                            $"attribute_domain integer[], ",
                            $"category integer[], ",
                            $"label character varying[], ",
                            $"description text[])");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    array_to_string($Name.attribute_domain, ';', 'NULL')  AS attribute_domain,{Environment.NewLine}",
                            $"    array_to_string($Name.category, ';', 'NULL')          AS category,{Environment.NewLine}",
                            $"    array_to_string($Name.label, ';', 'NULL')             AS label,{Environment.NewLine}",
                            $"    array_to_string($Name.description, ';', 'NULL')       AS description{Environment.NewLine}",
                            $"FROM{Environment.NewLine}",
                            $"    $SchemaName.$Name(",
                            $"@attrType, ",
                            $"@subPopulations",
                            $") AS $Name;{Environment.NewLine}");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Execute stored procedure fn_get_attribute_domain
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="subPopulations">List of subpopulations</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table with groups of subpopulation categories</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        List<TDSubPopulation> subPopulations,
                        NpgsqlTransaction transaction = null)
                    {
                        string attrType = "sp";

                        CommandText = SQL;

                        CommandText = CommandText.Replace(
                           oldValue: "@attrType",
                           newValue: Functions.PrepStringArg(arg: attrType));

                        CommandText = CommandText.Replace(
                            oldValue: "@subPopulations",
                            newValue: Functions.PrepIntArrayArg(
                                args: subPopulations.Select(a => a.Id).ToArray<int>(),
                                dbType: "int4"));

                        NpgsqlParameter pAttrType = new(
                            parameterName: "attrType",
                            parameterType: NpgsqlDbType.Varchar);

                        NpgsqlParameter pAttrTypes = new(
                            parameterName: "subPopulations",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        if (attrType != null)
                        {
                            pAttrType.Value = (string)attrType;
                        }
                        else
                        {
                            pAttrType.Value = DBNull.Value;
                        }

                        if (subPopulations != null)
                        {
                            pAttrTypes.Value = subPopulations.Select(a => a.Id).ToArray<int>();
                        }
                        else
                        {
                            pAttrTypes.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteQuery(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pAttrType, pAttrTypes);
                    }

                    /// <summary>
                    /// Execute stored procedure fn_get_attribute_domain
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="subPopulations">List of subpopulations</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>List of combinations of subpopulation categories</returns>
                    public static TDDomainCategoryCombinationList<TDSubPopulationCategory> Execute(
                        NfiEstaDB database,
                        List<TDSubPopulation> subPopulations,
                        NpgsqlTransaction transaction = null)
                    {
                        DataTable data = ExecuteQuery(
                            database: database,
                            subPopulations: subPopulations,
                            transaction: transaction);

                        TDSubPopulationCategoryList cSubPopulationCategory =
                            new(database: database);
                        cSubPopulationCategory.ReLoad(extended: true);

                        List<TDDomainCategoryCombination<TDSubPopulationCategory>> items = [];
                        foreach (DataRow row in data.Rows)
                        {
                            TDDomainCategoryCombination<TDSubPopulationCategory> combination
                                    = new(database: database);

                            foreach (int id in
                                Functions.StringToIntList(
                                    text: Functions.GetStringArg(row: row, name: "category", defaultValue: null),
                                    separator: (char)59,
                                    defaultValue: null))
                            {
                                combination.Add(category: cSubPopulationCategory[id]);
                            }

                            items.Add(item: combination);
                        }

                        return new TDDomainCategoryCombinationList<TDSubPopulationCategory>(
                            database: database,
                            items: items);
                    }

                    #endregion Methods

                }

                #endregion FnGetAttributeDomainSP

            }
        }
    }
}