﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {

                #region FnGetEligiblePanelRefYearSetCombinationsInternal

                /// <summary>
                /// Wrapper for stored procedure fn_get_eligible_panel_refyearset_combinations_internal.
                /// Function returns combinations of panel and reference-yearsets (ids of cm_refyearset2panel_mapping),
                /// for which positive or negative contributions to local density of the given target variable
                /// and for the given set of its categorizations can be calculated.
                /// </summary>
                public static class FnGetEligiblePanelRefYearSetCombinationsInternal
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_get_eligible_panel_refyearset_combinations_internal";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_target_variable integer, ",
                            $"_categorization_setup integer[], ",
                            $"_use_negative boolean; ",
                            $"returns: integer[]");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    array_to_string($SchemaName.$Name(",
                            $"@targetVariableId,",
                            $"@categorizationSetupIds,",
                            $"@useNegative), '{(char)59}', '{Functions.StrNull}') ",
                            $"AS {Name};{Environment.NewLine}");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Execute stored procedure fn_get_eligible_panel_refyearset_combinations_internal
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="targetVariableId">Target variable identifier</param>
                    /// <param name="categorizationSetupIds">List of categorization setup identifiers</param>
                    /// <param name="useNegative">Positive or negative contribution to local density</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>List of combinations of panel and reference-yearsets identifiers
                    /// for which positive or negative contributions to local density of the given target variable
                    /// and for the given set of its categorizations can be calculated.</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> targetVariableId,
                        List<Nullable<int>> categorizationSetupIds,
                        Nullable<bool> useNegative,
                        NpgsqlTransaction transaction = null)
                    {
                        List<Nullable<int>> list = Execute(
                            database: database,
                            targetVariableId: targetVariableId,
                            categorizationSetupIds: categorizationSetupIds,
                            useNegative: useNegative,
                            transaction: transaction);

                        DataTable dt = new();

                        dt.Columns.Add(column: new DataColumn()
                        {
                            ColumnName = Name,
                            DataType = Type.GetType(typeName: "System.Int32")
                        });

                        if (list == null)
                        {
                            return dt;
                        }

                        foreach (Nullable<int> val in list)
                        {
                            DataRow row = dt.NewRow();

                            Functions.SetNIntArg(
                                row: row,
                                name: Name,
                                val: val);

                            dt.Rows.Add(row: row);
                        }

                        return dt;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_get_eligible_panel_refyearset_combinations_internal
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="targetVariableId">Target variable identifier</param>
                    /// <param name="categorizationSetupIds">List of categorization setup identifiers</param>
                    /// <param name="useNegative">Positive or negative contribution to local density</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>List of combinations of panel and reference-yearsets identifiers
                    /// for which positive or negative contributions to local density of the given target variable
                    /// and for the given set of its categorizations can be calculated.</returns>
                    public static List<Nullable<int>> Execute(
                        NfiEstaDB database,
                        Nullable<int> targetVariableId,
                        List<Nullable<int>> categorizationSetupIds,
                        Nullable<bool> useNegative,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = SQL;

                        CommandText = CommandText.Replace(
                            oldValue: "@targetVariableId",
                            newValue: Functions.PrepNIntArg(arg: targetVariableId));

                        CommandText = CommandText.Replace(
                            oldValue: "@categorizationSetupIds",
                            newValue: Functions.PrepNIntArrayArg(args: categorizationSetupIds, dbType: "int4"));

                        CommandText = CommandText.Replace(
                            oldValue: "@useNegative",
                            newValue: Functions.PrepNBoolArg(arg: useNegative));

                        NpgsqlParameter pTargetVariableId = new(
                            parameterName: "targetVariableId",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pCategorizationSetupIds = new(
                            parameterName: "categorizationSetupIds",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        NpgsqlParameter pUseNegative = new(
                            parameterName: "useNegative",
                            parameterType: NpgsqlDbType.Boolean);

                        if (targetVariableId != null)
                        {
                            pTargetVariableId.Value = (int)targetVariableId;
                        }
                        else
                        {
                            pTargetVariableId.Value = DBNull.Value;
                        }

                        if (categorizationSetupIds != null)
                        {
                            pCategorizationSetupIds.Value = categorizationSetupIds.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pCategorizationSetupIds.Value = DBNull.Value;
                        }

                        if (useNegative != null)
                        {
                            pUseNegative.Value = (bool)useNegative;
                        }
                        else
                        {
                            pUseNegative.Value = DBNull.Value;
                        }

                        return
                            Functions.StringToNIntList(
                                text: database.Postgres.ExecuteScalar(
                                        sqlCommand: SQL,
                                        transaction: transaction,
                                        pTargetVariableId, pCategorizationSetupIds, pUseNegative),
                                separator: (char)59);
                    }

                    #endregion Methods

                }

                #endregion FnGetEligiblePanelRefYearSetCombinationsInternal

            }

        }
    }
}