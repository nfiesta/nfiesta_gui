﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {

                #region FnSaveClassificationRule

                /// <summary>
                /// Wrapper for stored procedure fn_save_classification_rule.
                /// Functions inserts a record into table
                /// c_adc2conversion_string or c_spc2conversion_string
                /// based on given parameters.
                /// </summary>
                public static class FnSaveClassificationRule
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_save_classification_rule";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_parent integer, ",
                            $"_areal_or_population integer, ",
                            $"_ldsity_object integer, ",
                            $"_classification_rule text, ",
                            $"_use_negative boolean DEFAULT false; ",
                            $"returns: integer");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    $SchemaName.$Name(",
                            $"@parentId, ",
                            $"@arealOrPopulationId, ",
                            $"@ldsityObjectId, ",
                            $"@classificationRule, ",
                            $"@useNegative)::integer AS $Name;");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Execute stored procedure fn_save_classification_rule
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="parentId">Parent idenfier (id from c_area_domain_category/c_sub_population_category)</param>
                    /// <param name="arealOrPopulationId">Areal or population identifier</param>
                    /// <param name="ldsityObjectId">Local density object identifier</param>
                    /// <param name="classificationRule">Classification rule</param>
                    /// <param name="useNegative">Use negative local density contribution</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table with classification rule identifier
                    /// (id from cm_adc2classification_rule/cm_spc2classification_rule)</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> parentId,
                        Nullable<int> arealOrPopulationId,
                        Nullable<int> ldsityObjectId,
                        string classificationRule,
                        Nullable<bool> useNegative,
                        NpgsqlTransaction transaction = null
                       )
                    {
                        Nullable<int> val = Execute(
                            database: database,
                            parentId: parentId,
                            arealOrPopulationId: arealOrPopulationId,
                            ldsityObjectId: ldsityObjectId,
                            classificationRule: classificationRule,
                            useNegative: useNegative,
                            transaction: transaction);

                        DataTable dt = new();

                        dt.Columns.Add(column: new DataColumn()
                        {
                            ColumnName = Name,
                            DataType = Type.GetType(typeName: "System.Int32")
                        });

                        DataRow row = dt.NewRow();

                        Functions.SetNIntArg(
                            row: row,
                            name: Name,
                            val: val);

                        dt.Rows.Add(row: row);

                        return dt;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_save_classification_rule
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="parentId">Parent idenfier (id from c_area_domain_category/c_sub_population_category)</param>
                    /// <param name="arealOrPopulationId">Areal or population identifier</param>
                    /// <param name="ldsityObjectId">Local density object identifier</param>
                    /// <param name="classificationRule">Classification rule</param>
                    /// <param name="useNegative">Use negative local density contribution</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Classification rule identifier
                    /// (id from cm_adc2classification_rule/cm_spc2classification_rule)</returns>
                    public static Nullable<int> Execute(
                        NfiEstaDB database,
                        Nullable<int> parentId,
                        Nullable<int> arealOrPopulationId,
                        Nullable<int> ldsityObjectId,
                        string classificationRule,
                        Nullable<bool> useNegative,
                        NpgsqlTransaction transaction = null
                       )
                    {
                        CommandText = SQL;

                        CommandText = CommandText.Replace(
                            oldValue: "@parentId",
                            newValue: Functions.PrepNIntArg(arg: parentId));

                        CommandText = CommandText.Replace(
                            oldValue: "@arealOrPopulationId",
                            newValue: Functions.PrepNIntArg(arg: arealOrPopulationId));

                        CommandText = CommandText.Replace(
                            oldValue: "@ldsityObjectId",
                            newValue: Functions.PrepNIntArg(arg: ldsityObjectId));

                        CommandText = CommandText.Replace(
                            oldValue: "@classificationRule",
                            newValue: Functions.PrepStringArg(arg: classificationRule));

                        CommandText = CommandText.Replace(
                            oldValue: "@useNegative",
                            newValue: Functions.PrepNBoolArg(arg: useNegative));

                        NpgsqlParameter pParentId = new(
                            parameterName: "parentId",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pArealOrPopulationId = new(
                            parameterName: "arealOrPopulationId",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pLDsityObjectId = new(
                            parameterName: "ldsityObjectId",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pClassificationRule = new(
                            parameterName: "classificationRule",
                            parameterType: NpgsqlDbType.Varchar);

                        NpgsqlParameter pUseNegative = new(
                            parameterName: "useNegative",
                            parameterType: NpgsqlDbType.Boolean);

                        if (parentId != null)
                        {
                            pParentId.Value = (int)parentId;
                        }
                        else
                        {
                            pParentId.Value = DBNull.Value;
                        }

                        if (arealOrPopulationId != null)
                        {
                            pArealOrPopulationId.Value = (int)arealOrPopulationId;
                        }
                        else
                        {
                            pArealOrPopulationId.Value = DBNull.Value;
                        }

                        if (ldsityObjectId != null)
                        {
                            pLDsityObjectId.Value = (int)ldsityObjectId;
                        }
                        else
                        {
                            pLDsityObjectId.Value = DBNull.Value;
                        }

                        if (classificationRule != null)
                        {
                            pClassificationRule.Value = (string)classificationRule;
                        }
                        else
                        {
                            pClassificationRule.Value = DBNull.Value;
                        }

                        if (useNegative != null)
                        {
                            pUseNegative.Value = (bool)useNegative;
                        }
                        else
                        {
                            pUseNegative.Value = DBNull.Value;
                        }

                        string strResult = database.Postgres.ExecuteScalar(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pParentId, pArealOrPopulationId, pLDsityObjectId,
                            pClassificationRule, pUseNegative);

                        if (String.IsNullOrEmpty(value: strResult))
                        {
                            return null;
                        }
                        else
                        {
                            if (Int32.TryParse(s: strResult, result: out int intResult))
                            {
                                return intResult;
                            }
                            else
                            {
                                return null;
                            }
                        }
                    }

                    #endregion Methods

                }

                #endregion FnSaveClassificationRule

            }

        }
    }
}