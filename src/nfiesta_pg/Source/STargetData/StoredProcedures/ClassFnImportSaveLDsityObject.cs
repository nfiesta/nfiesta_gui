﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {

                #region FnImportSaveLDsityObject

                /// <summary>
                /// Wrapper for stored procedure fn_import_save_ldsity_object.
                /// </summary>
                public static class FnImportSaveLDsityObject
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_import_save_ldsity_object";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_ldsity_object integer, ",
                            $"_label character varying, ",
                            $"_description text, ",
                            $"_label_en character varying, ",
                            $"_description_en text, ",
                            $"_table_name character varying, ",
                            $"_upper_object integer, ",
                            $"_areal_or_population integer, ",
                            $"_column4upper_object character varying, ",
                            $"_filter text; ",
                            $"returns: integer");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                         = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    $SchemaName.$Name(",
                            $"@ldsityObject, ",
                            $"@label, ",
                            $"@description, ",
                            $"@labelEn, ",
                            $"@descriptionEn, ",
                            $"@tableName, ",
                            $"@upperObject, ",
                            $"@arealOrPopulation, ",
                            $"@columnForUpperObject, ",
                            $"@filter",
                            $")::integer AS $Name;");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Execute stored procedure fn_import_save_ldsity_object
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="ldsityObject"></param>
                    /// <param name="label"></param>
                    /// <param name="description"></param>
                    /// <param name="labelEn"></param>
                    /// <param name="descriptionEn"></param>
                    /// <param name="tableName"></param>
                    /// <param name="upperObject"></param>
                    /// <param name="arealOrPopulation"></param>
                    /// <param name="columnForUpperObject"></param>
                    /// <param name="filter"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> ldsityObject,
                        string label,
                        string description,
                        string labelEn,
                        string descriptionEn,
                        string tableName,
                        Nullable<int> upperObject,
                        Nullable<int> arealOrPopulation,
                        string columnForUpperObject,
                        string filter,
                        NpgsqlTransaction transaction = null)
                    {
                        Nullable<int> val = Execute(
                            database: database,
                            ldsityObject: ldsityObject,
                            label: label,
                            description: description,
                            labelEn: labelEn,
                            descriptionEn: descriptionEn,
                            tableName: tableName,
                            upperObject: upperObject,
                            arealOrPopulation: arealOrPopulation,
                            columnForUpperObject: columnForUpperObject,
                            filter: filter,
                            transaction: transaction);

                        DataTable dt = new();

                        dt.Columns.Add(column: new DataColumn()
                        {
                            ColumnName = Name,
                            DataType = Type.GetType(typeName: "System.Int32")
                        });

                        DataRow row = dt.NewRow();

                        Functions.SetNIntArg(
                            row: row,
                            name: Name,
                            val: val);

                        dt.Rows.Add(row: row);

                        return dt;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_import_save_ldsity_object
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="ldsityObject"></param>
                    /// <param name="label"></param>
                    /// <param name="description"></param>
                    /// <param name="labelEn"></param>
                    /// <param name="descriptionEn"></param>
                    /// <param name="tableName"></param>
                    /// <param name="upperObject"></param>
                    /// <param name="arealOrPopulation"></param>
                    /// <param name="columnForUpperObject"></param>
                    /// <param name="filter"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Integer</returns>
                    public static Nullable<int> Execute(
                        NfiEstaDB database,
                        Nullable<int> ldsityObject,
                        string label,
                        string description,
                        string labelEn,
                        string descriptionEn,
                        string tableName,
                        Nullable<int> upperObject,
                        Nullable<int> arealOrPopulation,
                        string columnForUpperObject,
                        string filter,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = SQL;

                        CommandText = CommandText.Replace(
                            oldValue: "@ldsityObject",
                            newValue: Functions.PrepNIntArg(arg: ldsityObject));

                        CommandText = CommandText.Replace(
                            oldValue: "@label",
                            newValue: Functions.PrepStringArg(arg: label));

                        CommandText = CommandText.Replace(
                            oldValue: "@description",
                            newValue: Functions.PrepStringArg(arg: description));

                        CommandText = CommandText.Replace(
                            oldValue: "@labelEn",
                            newValue: Functions.PrepStringArg(arg: labelEn));

                        CommandText = CommandText.Replace(
                            oldValue: "@descriptionEn",
                            newValue: Functions.PrepStringArg(arg: descriptionEn));

                        CommandText = CommandText.Replace(
                            oldValue: "@tableName",
                            newValue: Functions.PrepStringArg(arg: tableName));

                        CommandText = CommandText.Replace(
                            oldValue: "@upperObject",
                            newValue: Functions.PrepNIntArg(arg: upperObject));

                        CommandText = CommandText.Replace(
                            oldValue: "@arealOrPopulation",
                            newValue: Functions.PrepNIntArg(arg: arealOrPopulation));

                        CommandText = CommandText.Replace(
                            oldValue: "@columnForUpperObject",
                            newValue: Functions.PrepStringArg(arg: columnForUpperObject));

                        CommandText = CommandText.Replace(
                            oldValue: "@filter",
                            newValue: Functions.PrepStringArg(arg: filter));

                        NpgsqlParameter pLDsityObject = new(
                            parameterName: "ldsityObject",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pLabel = new(
                            parameterName: "label",
                            parameterType: NpgsqlDbType.Varchar);

                        NpgsqlParameter pDescription = new(
                            parameterName: "description",
                            parameterType: NpgsqlDbType.Text);

                        NpgsqlParameter pLabelEn = new(
                            parameterName: "labelEn",
                            parameterType: NpgsqlDbType.Varchar);

                        NpgsqlParameter pDescriptionEn = new(
                            parameterName: "descriptionEn",
                            parameterType: NpgsqlDbType.Text);

                        NpgsqlParameter pTableName = new(
                            parameterName: "tableName",
                            parameterType: NpgsqlDbType.Varchar);

                        NpgsqlParameter pUpperObject = new(
                            parameterName: "upperObject",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pArealOrPopulation = new(
                            parameterName: "arealOrPopulation",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pColumnForUpperObject = new(
                           parameterName: "columnForUpperObject",
                           parameterType: NpgsqlDbType.Varchar);

                        NpgsqlParameter pFilter = new(
                           parameterName: "filter",
                           parameterType: NpgsqlDbType.Text);

                        if (ldsityObject != null)
                        {
                            pLDsityObject.Value = (int)ldsityObject;
                        }
                        else
                        {
                            pLDsityObject.Value = DBNull.Value;
                        }

                        if (label != null)
                        {
                            pLabel.Value = (string)label;
                        }
                        else
                        {
                            pLabel.Value = DBNull.Value;
                        }

                        if (description != null)
                        {
                            pDescription.Value = (string)description;
                        }
                        else
                        {
                            pDescription.Value = DBNull.Value;
                        }

                        if (labelEn != null)
                        {
                            pLabelEn.Value = (string)labelEn;
                        }
                        else
                        {
                            pLabelEn.Value = DBNull.Value;
                        }

                        if (descriptionEn != null)
                        {
                            pDescriptionEn.Value = (string)descriptionEn;
                        }
                        else
                        {
                            pDescriptionEn.Value = DBNull.Value;
                        }

                        if (tableName != null)
                        {
                            pTableName.Value = (string)tableName;
                        }
                        else
                        {
                            pTableName.Value = DBNull.Value;
                        }

                        if (upperObject != null)
                        {
                            pUpperObject.Value = (int)upperObject;
                        }
                        else
                        {
                            pUpperObject.Value = DBNull.Value;
                        }

                        if (arealOrPopulation != null)
                        {
                            pArealOrPopulation.Value = (int)arealOrPopulation;
                        }
                        else
                        {
                            pArealOrPopulation.Value = DBNull.Value;
                        }

                        if (columnForUpperObject != null)
                        {
                            pColumnForUpperObject.Value = (string)columnForUpperObject;
                        }
                        else
                        {
                            pColumnForUpperObject.Value = DBNull.Value;
                        }

                        if (filter != null)
                        {
                            pFilter.Value = (string)filter;
                        }
                        else
                        {
                            pFilter.Value = DBNull.Value;
                        }

                        string strResult = database.Postgres.ExecuteScalar(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pLDsityObject, pLabel, pDescription, pLabelEn, pDescriptionEn,
                            pTableName, pUpperObject, pArealOrPopulation, pColumnForUpperObject, pFilter);

                        if (String.IsNullOrEmpty(value: strResult))
                        {
                            return null;
                        }
                        else
                        {
                            if (Int32.TryParse(s: strResult, result: out int intResult))
                            {
                                return intResult;
                            }
                            else
                            {
                                return null;
                            }
                        }
                    }

                    #endregion Methods

                }

                #endregion FnImportSaveLDsityObject

            }

        }
    }
}