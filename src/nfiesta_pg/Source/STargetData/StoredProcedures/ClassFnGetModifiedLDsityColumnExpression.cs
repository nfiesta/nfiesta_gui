﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {

                #region FnGetModifiedLDsityColumnExpression

                /// <summary>
                /// Wrapper for stored procedure fn_get_modified_ldsity_column_expression.
                /// The function modified input ldsity column exression for given ldsity object and their position in ldsity objects.
                /// </summary>
                public static class FnGetModifiedLDsityColumnExpression
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_get_modified_ldsity_column_expression";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_ldsity_column_expression text, ",
                            $"_position4ldsity integer, ",
                            $"_ldsity_ldsity_object integer; ",
                            $"returns: text");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    $SchemaName.$Name(",
                            $"@ldsityColumnExpression, ",
                            $"@positionForLDsity, ",
                            $"@ldsityLDsityObject",
                            $")::text AS $Name;{Environment.NewLine}");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Execute stored procedure fn_get_modified_ldsity_column_expression
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="ldsityColumnExpression"></param>
                    /// <param name="positionForLDsity"></param>
                    /// <param name="ldsityLDsityObject"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> ldsityColumnExpression,
                        List<Nullable<int>> positionForLDsity,
                        Nullable<int> ldsityLDsityObject,
                        NpgsqlTransaction transaction = null)
                    {
                        string val = Execute(
                            database: database,
                            ldsityColumnExpression: ldsityColumnExpression,
                            positionForLDsity: positionForLDsity,
                            ldsityLDsityObject: ldsityLDsityObject,
                            transaction: transaction);

                        DataTable dt = new();

                        dt.Columns.Add(column: new DataColumn()
                        {
                            ColumnName = Name,
                            DataType = Type.GetType(typeName: "System.String")
                        });

                        DataRow row = dt.NewRow();

                        Functions.SetStringArg(
                            row: row,
                            name: Name,
                            val: val);

                        dt.Rows.Add(row: row);

                        return dt;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_get_modified_ldsity_column_expression
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="ldsityColumnExpression"></param>
                    /// <param name="positionForLDsity"></param>
                    /// <param name="ldsityLDsityObject"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Text</returns>
                    public static string Execute(
                        NfiEstaDB database,
                        Nullable<int> ldsityColumnExpression,
                        List<Nullable<int>> positionForLDsity,
                        Nullable<int> ldsityLDsityObject,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = SQL;

                        CommandText = CommandText.Replace(
                            oldValue: "@ldsityColumnExpression",
                            newValue: Functions.PrepNIntArg(arg: ldsityColumnExpression));

                        CommandText = CommandText.Replace(
                            oldValue: "@positionForLDsity",
                            newValue: Functions.PrepNIntArrayArg(args: positionForLDsity, dbType: "int4"));

                        CommandText = CommandText.Replace(
                            oldValue: "@ldsityLDsityObject",
                            newValue: Functions.PrepNIntArg(arg: ldsityLDsityObject));

                        NpgsqlParameter pLDsityColumnExpression = new(
                            parameterName: "ldsityColumnExpression",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pPositionForLDsity = new(
                            parameterName: "positionForLDsity",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        NpgsqlParameter pLDsityLDsityObject = new(
                            parameterName: "ldsityLDsityObject",
                            parameterType: NpgsqlDbType.Integer);

                        if (ldsityColumnExpression != null)
                        {
                            pLDsityColumnExpression.Value = (int)ldsityColumnExpression;
                        }
                        else
                        {
                            pLDsityColumnExpression.Value = DBNull.Value;
                        }

                        if (positionForLDsity != null)
                        {
                            pPositionForLDsity.Value = positionForLDsity.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pPositionForLDsity.Value = DBNull.Value;
                        }

                        if (ldsityLDsityObject != null)
                        {
                            pLDsityLDsityObject.Value = (int)ldsityLDsityObject;
                        }
                        else
                        {
                            pLDsityLDsityObject.Value = DBNull.Value;
                        }

                        return
                            database.Postgres.ExecuteScalar(
                                sqlCommand: SQL,
                                transaction: transaction,
                                pLDsityColumnExpression, pPositionForLDsity, pLDsityLDsityObject);
                    }

                    #endregion Methods

                }

                #endregion FnGetModifiedLDsityColumnExpression

            }

        }
    }
}