﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {
                // fn_etl_get_target_variable

                #region FnEtlGetTargetVariable

                /// <summary>
                /// Wrapper for stored procedure fn_etl_get_target_variable.
                /// The function returs available target variables for ETL.
                /// </summary>
                public static class FnEtlGetTargetVariable
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_etl_get_target_variable";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_export_connection integer, ",
                            $"_national_language character varying DEFAULT 'en'::character varying(2), ",
                            $"_etl boolean DEFAULT NULL::boolean, ",
                            $"_target_variable integer DEFAULT NULL::integer; ",
                            $"returns: ",
                            $"TABLE(",
                            $"id integer, ",
                            $"label character varying, ",
                            $"description text, ",
                            $"label_en character varying, ",
                            $"description_en text, ",
                            $"id_etl_target_variable integer, ",
                            $"check_target_variable boolean, ",
                            $"refyearset2panel_mapping integer[], ",
                            $"check_atomic_area_domains boolean, ",
                            $"check_atomic_sub_populations boolean, ",
                            $"metadata json)");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    {TDFnEtlGetTargetVariableTypeList.ColId.SQL(TDFnEtlGetTargetVariableTypeList.Cols, Name)}",
                            $"    {TDFnEtlGetTargetVariableTypeList.ColLabelCs.SQL(TDFnEtlGetTargetVariableTypeList.Cols, Name)}",
                            $"    {TDFnEtlGetTargetVariableTypeList.ColDescriptionCs.SQL(TDFnEtlGetTargetVariableTypeList.Cols, Name)}",
                            $"    {TDFnEtlGetTargetVariableTypeList.ColLabelEn.SQL(TDFnEtlGetTargetVariableTypeList.Cols, Name)}",
                            $"    {TDFnEtlGetTargetVariableTypeList.ColDescriptionEn.SQL(TDFnEtlGetTargetVariableTypeList.Cols, Name)}",
                            $"    {TDFnEtlGetTargetVariableTypeList.ColIdEtlTargetVariableMetadata.SQL(TDFnEtlGetTargetVariableTypeList.Cols, Name)}",
                            $"    {TDFnEtlGetTargetVariableTypeList.ColCheckTargetVariable.SQL(TDFnEtlGetTargetVariableTypeList.Cols, Name)}",
                            $"    {TDFnEtlGetTargetVariableTypeList.ColRefYearSetToPanelMapping.SQL(TDFnEtlGetTargetVariableTypeList.Cols, Name)}",
                            $"    {TDFnEtlGetTargetVariableTypeList.ColCheckAtomicAreaDomains.SQL(TDFnEtlGetTargetVariableTypeList.Cols, Name)}",
                            $"    {TDFnEtlGetTargetVariableTypeList.ColCheckAtomicSubPopulations.SQL(TDFnEtlGetTargetVariableTypeList.Cols, Name)}",
                            $"    {TDFnEtlGetTargetVariableTypeList.ColMetadata.SQL(TDFnEtlGetTargetVariableTypeList.Cols, Name, isLastOne: true)}",
                            $"FROM{Environment.NewLine}",
                            $"    $SchemaName.$Name(@exportConnection, @nationalLanguage, @etl, @targetVariable) AS {Name}{Environment.NewLine}",
                            $"ORDER BY{Environment.NewLine}",
                            $"    {Name}.id;{Environment.NewLine}"
                            );

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="exportConnection"></param>
                    /// <param name="nationalLanguage"></param>
                    /// <param name="etl"></param>
                    /// <param name="targetVariable"></param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        Nullable<int> exportConnection,
                        Language nationalLanguage = PostgreSQL.Language.CS,
                        Nullable<bool> etl = false,
                        Nullable<int> targetVariable = null)
                    {
                        string result = SQL;

                        result = result.Replace(
                            oldValue: "@exportConnection",
                            newValue: Functions.PrepNIntArg(arg: exportConnection));

                        result = result.Replace(
                            oldValue: "@nationalLanguage",
                            newValue: Functions.PrepStringArg(
                                arg: LanguageList.ISO_639_1(language: nationalLanguage)));

                        result = result.Replace(
                            oldValue: "@etl",
                            newValue: Functions.PrepNBoolArg(arg: etl));

                        result = result.Replace(
                            oldValue: "@targetVariable",
                            newValue: Functions.PrepNIntArg(arg: targetVariable));

                        return result;
                    }

                    /// <summary>
                    ///  Execute stored procedure fn_etl_get_target_variable
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="exportConnection"></param>
                    /// <param name="nationalLanguage"></param>
                    /// <param name="etl"></param>
                    /// <param name="targetVariable"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> exportConnection,
                        Language nationalLanguage = PostgreSQL.Language.CS,
                        Nullable<bool> etl = false,
                        Nullable<int> targetVariable = null,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(
                            exportConnection: exportConnection,
                            nationalLanguage: nationalLanguage,
                            etl: etl,
                            targetVariable: targetVariable);

                        NpgsqlParameter pExportConnection = new(
                            parameterName: "exportConnection",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pNationalLanguage = new(
                            parameterName: "nationalLanguage",
                            parameterType: NpgsqlDbType.Varchar);

                        NpgsqlParameter pEtl = new(
                            parameterName: "etl",
                            parameterType: NpgsqlDbType.Boolean);

                        NpgsqlParameter pTargetVariable = new(
                            parameterName: "targetVariable",
                            parameterType: NpgsqlDbType.Integer);

                        if (exportConnection != null)
                        {
                            pExportConnection.Value = (int)exportConnection;
                        }
                        else
                        {
                            pExportConnection.Value = DBNull.Value;
                        }

                        pNationalLanguage.Value
                            = LanguageList.ISO_639_1(language: nationalLanguage);

                        if (etl != null)
                        {
                            pEtl.Value = (bool)etl;
                        }
                        else
                        {
                            pEtl.Value = DBNull.Value;
                        }

                        if (targetVariable != null)
                        {
                            pTargetVariable.Value = (int)targetVariable;
                        }
                        else
                        {
                            pTargetVariable.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteQuery(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pExportConnection, pNationalLanguage, pEtl, pTargetVariable);
                    }

                    /// <summary>
                    /// Execute stored procedure fn_etl_get_target_variable
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="exportConnection"></param>
                    /// <param name="nationalLanguage"></param>
                    /// <param name="etl"></param>
                    /// <param name="targetVariable"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>List of target variables for ETL</returns>
                    public static TDFnEtlGetTargetVariableTypeList Execute(
                        NfiEstaDB database,
                        Nullable<int> exportConnection,
                        Language nationalLanguage = PostgreSQL.Language.CS,
                        Nullable<bool> etl = false,
                        Nullable<int> targetVariable = null,
                        NpgsqlTransaction transaction = null)
                    {
                        System.Diagnostics.Stopwatch stopWatch = new();
                        stopWatch.Start();

                        DataTable data = ExecuteQuery(
                            database: database,
                            exportConnection: exportConnection,
                            nationalLanguage: nationalLanguage,
                            etl: etl,
                            targetVariable: targetVariable,
                            transaction: transaction);
                        data.TableName = Name;

                        stopWatch.Stop();

                        return new TDFnEtlGetTargetVariableTypeList(
                            database: database,
                            data: data,
                            loadingTime: 0.001 * stopWatch.ElapsedMilliseconds,
                            storedProcedure: CommandText)
                        {
                            ExportConnection = exportConnection,
                            NationalLanguage = nationalLanguage,
                            Etl = etl,
                            TargetVariable = targetVariable
                        };
                    }

                    #endregion Methods

                }

                #endregion FnEtlGetTargetVariable

            }

        }
    }
}