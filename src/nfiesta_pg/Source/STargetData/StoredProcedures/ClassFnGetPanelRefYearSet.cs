﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {

                #region FnGetPanelRefYearSet

                /// <summary>
                /// Wrapper for stored procedure fn_get_panel_refyearset.
                /// Function returns records from cm_refyearset2panel_mappping table, optionally for given panel_refyearset.
                /// </summary>
                public static class FnGetPanelRefYearSet
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_get_panel_refyearset";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_panel_refyearset integer DEFAULT NULL::integer; ",
                            $"returns: ",
                            $"TABLE(",
                            $"id integer, ",
                            $"panel_id integer, ",
                            $"panel character varying, ",
                            $"panel_label character varying, ",
                            $"refyearset_id integer, ",
                            $"reference_year_set character varying, ",
                            $"refyearset_label character varying)");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    {TDFnGetPanelRefYearSetTypeList.ColId.SQL(TDFnGetPanelRefYearSetTypeList.Cols, Name)}",
                            $"    {TDFnGetPanelRefYearSetTypeList.ColPanelId.SQL(TDFnGetPanelRefYearSetTypeList.Cols, Name)}",
                            $"    {TDFnGetPanelRefYearSetTypeList.ColPanelCode.SQL(TDFnGetPanelRefYearSetTypeList.Cols, Name)}",
                            $"    {TDFnGetPanelRefYearSetTypeList.ColPanelLabel.SQL(TDFnGetPanelRefYearSetTypeList.Cols, Name)}",
                            $"    {TDFnGetPanelRefYearSetTypeList.ColRefYearSetId.SQL(TDFnGetPanelRefYearSetTypeList.Cols, Name)}",
                            $"    {TDFnGetPanelRefYearSetTypeList.ColRefYearSetCode.SQL(TDFnGetPanelRefYearSetTypeList.Cols, Name)}",
                            $"    {TDFnGetPanelRefYearSetTypeList.ColRefYearSetLabel.SQL(TDFnGetPanelRefYearSetTypeList.Cols, Name, isLastOne: true)}",
                            $"FROM{Environment.NewLine}",
                            $"    $SchemaName.$Name(@panelRefYearSetId) AS {Name}{Environment.NewLine}",
                            $"ORDER BY{Environment.NewLine}",
                            $"    {Name}.{TDFnGetPanelRefYearSetTypeList.ColId.DbName};{Environment.NewLine}"
                            );

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Execute stored procedure fn_get_panel_refyearset
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="panelRefYearSetId">Panel-Reference year set mapping identifier</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table with panel reference year set mappings</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> panelRefYearSetId = null,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = SQL;

                        CommandText = CommandText.Replace(
                            oldValue: "@panelRefYearSetId",
                            newValue: Functions.PrepNIntArg(arg: panelRefYearSetId));

                        NpgsqlParameter pPanelRefYearSetId = new(
                            parameterName: "panelRefYearSetId",
                            parameterType: NpgsqlDbType.Integer);

                        if (panelRefYearSetId != null)
                        {
                            pPanelRefYearSetId.Value = (int)panelRefYearSetId;
                        }
                        else
                        {
                            pPanelRefYearSetId.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteQuery(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pPanelRefYearSetId);
                    }

                    /// <summary>
                    /// Execute stored procedure fn_get_panel_refyearset
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="panelRefYearSetId">Panel-Reference year set mapping identifier</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>List of records from sdesign.cm_refyearset2panel_mapping table</returns>
                    public static TDFnGetPanelRefYearSetTypeList Execute(
                        NfiEstaDB database,
                        Nullable<int> panelRefYearSetId = null,
                        NpgsqlTransaction transaction = null)
                    {
                        System.Diagnostics.Stopwatch stopWatch = new();
                        stopWatch.Start();

                        DataTable data = ExecuteQuery(
                                database: database,
                                panelRefYearSetId: panelRefYearSetId,
                                transaction: transaction);
                        data.TableName = Name;

                        stopWatch.Stop();

                        return new TDFnGetPanelRefYearSetTypeList(
                            database: database,
                            data: data,
                            loadingTime: 0.001 * stopWatch.ElapsedMilliseconds,
                            storedProcedure: CommandText)
                        {
                            PanelRefYearSetId = panelRefYearSetId
                        };
                    }

                    #endregion Methods

                }

                #endregion FnGetPanelRefYearSet

            }

        }
    }
}