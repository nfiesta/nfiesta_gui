﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {

                #region FnImportGetLDsityToPanelRefYearSetVersions

                /// <summary>
                /// Wrapper for stored procedure fn_import_get_ldsity2panel_refyearset_versions.
                /// </summary>
                public static class FnImportGetLDsityToPanelRefYearSetVersions
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_import_get_ldsity2panel_refyearset_versions";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_id integer, ",
                            $"_etl_ldsity__etl_id integer, ",
                            $"_refyearset2panel integer, ",
                            $"_etl_version__etl_id integer, ",
                            $"_etl_id integer[] DEFAULT NULL::integer[]; ",
                            $"returns: ",
                            $"TABLE(",
                            $"id integer, ",
                            $"etl_id integer, ",
                            $"ldsity integer, ",
                            $"refyearset2panel integer, ",
                            $"version integer)");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    id,{Environment.NewLine}",
                            $"    etl_id,{Environment.NewLine}",
                            $"    ldsity,{Environment.NewLine}",
                            $"    refyearset2panel,{Environment.NewLine}",
                            $"    version{Environment.NewLine}",
                            $"FROM{Environment.NewLine}",
                            $"    $SchemaName.$Name(",
                            $"@id, ",
                            $"@etlLDsityEtlId, ",
                            $"@refYearSetToPanel, ",
                            $"@etlVersionEtlId, ",
                            $"@etlId);{Environment.NewLine}");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Execute stored procedure fn_import_get_ldsity2panel_refyearset_versions
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="id"></param>
                    /// <param name="etlLDsityEtlId"></param>
                    /// <param name="refYearSetToPanel"></param>
                    /// <param name="etlVersionEtlId"></param>
                    /// <param name="etlId"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> id,
                        Nullable<int> etlLDsityEtlId,
                        Nullable<int> refYearSetToPanel,
                        Nullable<int> etlVersionEtlId,
                        List<Nullable<int>> etlId,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = SQL;

                        CommandText = CommandText.Replace(
                            oldValue: "@id",
                            newValue: Functions.PrepNIntArg(arg: id));

                        CommandText = CommandText.Replace(
                            oldValue: "@etlLDsityEtlId",
                            newValue: Functions.PrepNIntArg(arg: etlLDsityEtlId));

                        CommandText = CommandText.Replace(
                            oldValue: "@refYearSetToPanel",
                            newValue: Functions.PrepNIntArg(arg: refYearSetToPanel));

                        CommandText = CommandText.Replace(
                            oldValue: "@etlVersionEtlId",
                            newValue: Functions.PrepNIntArg(arg: etlVersionEtlId));

                        CommandText = CommandText.Replace(
                            oldValue: "@etlId",
                            newValue: Functions.PrepNIntArrayArg(args: etlId, dbType: "int4"));

                        NpgsqlParameter pId = new(
                            parameterName: "id",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pEtlLDsityEtlId = new(
                           parameterName: "etlLDsityEtlId",
                           parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pRefYearSetToPanel = new(
                           parameterName: "refYearSetToPanel",
                           parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pEtlVersionEtlId = new(
                           parameterName: "etlVersionEtlId",
                           parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pEtlId = new(
                            parameterName: "etlId",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        if (id != null)
                        {
                            pId.Value = (int)id;
                        }
                        else
                        {
                            pId.Value = DBNull.Value;
                        }

                        if (etlLDsityEtlId != null)
                        {
                            pEtlLDsityEtlId.Value = (int)etlLDsityEtlId;
                        }
                        else
                        {
                            pEtlLDsityEtlId.Value = DBNull.Value;
                        }

                        if (refYearSetToPanel != null)
                        {
                            pRefYearSetToPanel.Value = (int)refYearSetToPanel;
                        }
                        else
                        {
                            pRefYearSetToPanel.Value = DBNull.Value;
                        }

                        if (etlVersionEtlId != null)
                        {
                            pEtlVersionEtlId.Value = (int)etlVersionEtlId;
                        }
                        else
                        {
                            pEtlVersionEtlId.Value = DBNull.Value;
                        }

                        if (etlId != null)
                        {
                            pEtlId.Value = etlId.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pEtlId.Value = DBNull.Value;
                        }

                        return
                            database.Postgres.ExecuteQuery(
                                sqlCommand: SQL,
                                transaction: transaction,
                                pId, pEtlLDsityEtlId, pRefYearSetToPanel, pEtlVersionEtlId, pEtlId);
                    }

                    #endregion Methods

                }

                #endregion FnImportGetLDsityToPanelRefYearSetVersions

            }

        }
    }
}