﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {

                #region FnImportGetAdcToClassificationRules

                /// <summary>
                /// Wrapper for stored procedure fn_import_get_adc2classification_rules.
                /// </summary>
                public static class FnImportGetAdcToClassificationRules
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_import_get_adc2classification_rules";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_adc2classification_rule integer, ",
                            $"_area_domain_category__etl_id integer, ",
                            $"_ldsity_object__etl_id integer, ",
                            $"_etl_id integer[] DEFAULT NULL::integer[]; ",
                            $"returns: ",
                            $"TABLE(",
                            $"adc2classification_rule integer, ",
                            $"ldsity_object__id integer, ",
                            $"ldsity_object__label character varying, ",
                            $"ldsity_object__description text, ",
                            $"ldsity_object__label_en character varying, ",
                            $"ldsity_object__description_en text, ",
                            $"area_domain_category__id integer, ",
                            $"area_domain_category__label character varying, ",
                            $"area_domain_category__description text, ",
                            $"area_domain_category__label_en character varying, ",
                            $"area_domain_category__description_en text, ",
                            $"etl_id integer, ",
                            $"classification_rule text)");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    adc2classification_rule,{Environment.NewLine}",
                            $"    ldsity_object__id,{Environment.NewLine}",
                            $"    ldsity_object__label,{Environment.NewLine}",
                            $"    ldsity_object__description,{Environment.NewLine}",
                            $"    ldsity_object__label_en,{Environment.NewLine}",
                            $"    ldsity_object__description_en,{Environment.NewLine}",
                            $"    area_domain_category__id,{Environment.NewLine}",
                            $"    area_domain_category__label,{Environment.NewLine}",
                            $"    area_domain_category__description,{Environment.NewLine}",
                            $"    area_domain_category__label_en,{Environment.NewLine}",
                            $"    area_domain_category__description_en,{Environment.NewLine}",
                            $"    etl_id,{Environment.NewLine}",
                            $"    classification_rule{Environment.NewLine}",
                            $"FROM{Environment.NewLine}",
                            $"    $SchemaName.$Name(",
                            $"@adcToClassificationRule, ",
                            $"@areaDomainCategoryEtlId, ",
                            $"@ldsityObjectEtlId, ",
                            $"@etlId);{Environment.NewLine}");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Execute stored procedure fn_import_get_adc2classification_rules
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="adcToClassificationRule"></param>
                    /// <param name="areaDomainCategoryEtlId"></param>
                    /// <param name="ldsityObjectEtlId"></param>
                    /// <param name="etlId"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> adcToClassificationRule,
                        Nullable<int> areaDomainCategoryEtlId,
                        Nullable<int> ldsityObjectEtlId,
                        List<Nullable<int>> etlId,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = SQL;

                        CommandText = CommandText.Replace(
                            oldValue: "@adcToClassificationRule",
                            newValue: Functions.PrepNIntArg(arg: adcToClassificationRule));

                        CommandText = CommandText.Replace(
                            oldValue: "@areaDomainCategoryEtlId",
                            newValue: Functions.PrepNIntArg(arg: areaDomainCategoryEtlId));

                        CommandText = CommandText.Replace(
                            oldValue: "@ldsityObjectEtlId",
                            newValue: Functions.PrepNIntArg(arg: ldsityObjectEtlId));

                        CommandText = CommandText.Replace(
                            oldValue: "@etlId",
                            newValue: Functions.PrepNIntArrayArg(args: etlId, dbType: "int4"));

                        NpgsqlParameter pAdcToClassificationRule = new(
                            parameterName: "adcToClassificationRule",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pAreaDomainCategoryEtlId = new(
                            parameterName: "areaDomainCategoryEtlId",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pLDsityObjectEtlId = new(
                            parameterName: "ldsityObjectEtlId",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pEtlId = new(
                            parameterName: "etlId",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        if (adcToClassificationRule != null)
                        {
                            pAdcToClassificationRule.Value = (int)adcToClassificationRule;
                        }
                        else
                        {
                            pAdcToClassificationRule.Value = DBNull.Value;
                        }

                        if (areaDomainCategoryEtlId != null)
                        {
                            pAreaDomainCategoryEtlId.Value = (int)areaDomainCategoryEtlId;
                        }
                        else
                        {
                            pAreaDomainCategoryEtlId.Value = DBNull.Value;
                        }

                        if (ldsityObjectEtlId != null)
                        {
                            pLDsityObjectEtlId.Value = (int)ldsityObjectEtlId;
                        }
                        else
                        {
                            pLDsityObjectEtlId.Value = DBNull.Value;
                        }

                        if (etlId != null)
                        {
                            pEtlId.Value = etlId.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pEtlId.Value = DBNull.Value;
                        }

                        return
                            database.Postgres.ExecuteQuery(
                                sqlCommand: SQL,
                                transaction: transaction,
                                pAdcToClassificationRule, pAreaDomainCategoryEtlId, pLDsityObjectEtlId, pEtlId);
                    }

                    #endregion Methods

                }

                #endregion FnImportGetAdcToClassificationRules

            }

        }
    }
}