﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {

                #region FnGetClassificationRuleId4Category

                /// <summary>
                /// Wrapper for stored procedure fn_get_classification_rule_id4category.
                /// Function returns records from cm_spc2classification_rule table,
                /// optionally for given sub_population_category and/or ldsity object.
                /// </summary>
                public static class FnGetClassificationRuleId4Category
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_get_classification_rule_id4category";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_area_domain_category integer[], ",
                            $"_area_domain_object integer[], ",
                            $"_sub_population_category integer[], ",
                            $"_sub_population_object integer[], ",
                            $"_use_negative_ad boolean[], ",
                            $"_use_negative_sp boolean[]; ",
                            $"returns: ",
                            $"TABLE(",
                            $"adc2classification_rule integer[], ",
                            $"spc2classification_rule integer[])");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    array_to_string(adc2classification_rule, ';', 'NULL')  AS adc2classification_rule,{Environment.NewLine}",
                            $"    array_to_string(spc2classification_rule, ';', 'NULL')  AS spc2classification_rule{Environment.NewLine}",
                            $"FROM{Environment.NewLine}",
                            $"    $SchemaName.$Name(",
                            $"@areaDomainCategoryIds, @areaDomainLDsityObjectIds, ",
                            $"@subPopulationCategoryIds, @subPopulationLDsityObjectIds, ",
                            $"@useNegativeAd, @useNegativeSp);{Environment.NewLine}"
                            );

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Execute stored procedure fn_get_classification_rule_id4category
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="areaDomainCategoryIds">List of area domain category identifiers</param>
                    /// <param name="areaDomainLDsityObjectIds">List of local density object identifiers for area domain category</param>
                    /// <param name="subPopulationCategoryIds">List of subpopulation category identifiers</param>
                    /// <param name="subPopulationLDsityObjectIds">List of local density object identifiers for subpopulation category</param>
                    /// <param name="useNegativeAd"></param>
                    /// <param name="useNegativeSp"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table with list of adc2classification_rule and list of spc2_classification_rule</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        List<Nullable<int>> areaDomainCategoryIds,
                        List<Nullable<int>> areaDomainLDsityObjectIds,
                        List<Nullable<int>> subPopulationCategoryIds,
                        List<Nullable<int>> subPopulationLDsityObjectIds,
                        List<Nullable<bool>> useNegativeAd,
                        List<Nullable<bool>> useNegativeSp,
                        NpgsqlTransaction transaction = null
                        )
                    {
                        CommandText = SQL;

                        CommandText = CommandText.Replace(
                            oldValue: "@areaDomainCategoryIds",
                            newValue: Functions.PrepNIntArrayArg(args: areaDomainCategoryIds, dbType: "int4"));

                        CommandText = CommandText.Replace(
                            oldValue: "@areaDomainLDsityObjectIds",
                            newValue: Functions.PrepNIntArrayArg(args: areaDomainLDsityObjectIds, dbType: "int4"));

                        CommandText = CommandText.Replace(
                            oldValue: "@subPopulationCategoryIds",
                            newValue: Functions.PrepNIntArrayArg(args: subPopulationCategoryIds, dbType: "int4"));

                        CommandText = CommandText.Replace(
                            oldValue: "@subPopulationLDsityObjectIds",
                            newValue: Functions.PrepNIntArrayArg(args: subPopulationLDsityObjectIds, dbType: "int4"));

                        CommandText = CommandText.Replace(
                            oldValue: "@useNegativeAd",
                            newValue: Functions.PrepNBoolArrayArg(args: useNegativeAd, dbType: "bool"));

                        CommandText = CommandText.Replace(
                            oldValue: "@useNegativeSp",
                            newValue: Functions.PrepNBoolArrayArg(args: useNegativeSp, dbType: "bool"));

                        NpgsqlParameter pAreaDomainCategoryIds = new(
                            parameterName: "areaDomainCategoryIds",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        NpgsqlParameter pAreaDomainLDsityObjectIds = new(
                            parameterName: "areaDomainLDsityObjectIds",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        NpgsqlParameter pSubPopulationCategoryIds = new(
                            parameterName: "subPopulationCategoryIds",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        NpgsqlParameter pSubPopulationLDsityObjectIds = new(
                            parameterName: "subPopulationLDsityObjectIds",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        NpgsqlParameter pUseNegativeAd = new(
                           parameterName: "useNegativeAd",
                           parameterType: NpgsqlDbType.Array | NpgsqlDbType.Boolean);

                        NpgsqlParameter pUseNegativeSp = new(
                           parameterName: "useNegativeSp",
                           parameterType: NpgsqlDbType.Array | NpgsqlDbType.Boolean);

                        if (areaDomainCategoryIds != null)
                        {
                            pAreaDomainCategoryIds.Value = areaDomainCategoryIds.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pAreaDomainCategoryIds.Value = DBNull.Value;
                        }

                        if (areaDomainLDsityObjectIds != null)
                        {
                            pAreaDomainLDsityObjectIds.Value = areaDomainLDsityObjectIds.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pAreaDomainLDsityObjectIds.Value = DBNull.Value;
                        }

                        if (subPopulationCategoryIds != null)
                        {
                            pSubPopulationCategoryIds.Value = subPopulationCategoryIds.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pSubPopulationCategoryIds.Value = DBNull.Value;
                        }

                        if (subPopulationLDsityObjectIds != null)
                        {
                            pSubPopulationLDsityObjectIds.Value = subPopulationLDsityObjectIds.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pSubPopulationLDsityObjectIds.Value = DBNull.Value;
                        }

                        if (useNegativeAd != null)
                        {
                            pUseNegativeAd.Value = useNegativeAd.ToArray<Nullable<bool>>();
                        }
                        else
                        {
                            pUseNegativeAd.Value = DBNull.Value;
                        }

                        if (useNegativeSp != null)
                        {
                            pUseNegativeSp.Value = useNegativeSp.ToArray<Nullable<bool>>();
                        }
                        else
                        {
                            pUseNegativeSp.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteQuery(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pAreaDomainCategoryIds, pAreaDomainLDsityObjectIds,
                            pSubPopulationCategoryIds, pSubPopulationLDsityObjectIds,
                            pUseNegativeAd, pUseNegativeSp);
                    }

                    #endregion Methods

                }

                #endregion FnGetClassificationRuleId4Category

            }

        }
    }
}