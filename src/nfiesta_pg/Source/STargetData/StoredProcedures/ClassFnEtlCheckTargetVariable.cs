﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {
                // fn_etl_check_target_variable

                #region FnEtlCheckTargetVariable

                /// <summary>
                /// Wrapper for stored procedure fn_etl_check_target_variable.
                /// Function returns record ID from table t_etl_target_variable based on given parameters.
                /// </summary>
                public static class FnEtlCheckTargetVariable
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_etl_check_target_variable";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_export_connection integer, ",
                            $"_target_variable integer; ",
                            $"returns: ",
                            $"TABLE(",
                            $"id_etl_target_variable integer, ",
                            $"check_target_variable boolean, ",
                            $"refyearset2panel_mapping integer[])");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT {Environment.NewLine}",
                            $"    {Name}.id_etl_target_variable                                    AS id_etl_target_variable,{Environment.NewLine}",
                            $"    {Name}.check_target_variable                                     AS check_target_variable,{Environment.NewLine}",
                            $"    array_to_string({Name}.refyearset2panel_mapping, ';', 'NULL')    AS refyearset2panel_mapping{Environment.NewLine}",
                            $"FROM {Environment.NewLine}",
                            $"    $SchemaName.$Name AS {Name}",
                            $"(@exportConnection, @targetVariable) AS {Name}{Environment.NewLine}",
                            $"ORDER BY {Environment.NewLine}",
                            $"    {Name}.id_etl_target_variable;{Environment.NewLine}");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="exportConnection"></param>
                    /// <param name="targetVariable"></param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        Nullable<int> exportConnection,
                        Nullable<int> targetVariable)
                    {
                        string result = SQL;

                        result = result.Replace(
                            oldValue: "@exportConnection",
                            newValue: Functions.PrepNIntArg(arg: exportConnection));

                        result = result.Replace(
                            oldValue: "@targetVariable",
                            newValue: Functions.PrepNIntArg(arg: targetVariable));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_etl_check_target_variable
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="exportConnection"></param>
                    /// <param name="targetVariable"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>DataTable</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> exportConnection,
                        Nullable<int> targetVariable,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(exportConnection: exportConnection, targetVariable: targetVariable);

                        NpgsqlParameter pExportConnection = new(
                            parameterName: "exportConnection",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pTargetVariable = new(
                            parameterName: "targetVariable",
                            parameterType: NpgsqlDbType.Integer);

                        if (exportConnection != null)
                        {
                            pExportConnection.Value = (int)exportConnection;
                        }
                        else
                        {
                            pExportConnection.Value = DBNull.Value;
                        }

                        if (targetVariable != null)
                        {
                            pTargetVariable.Value = (int)targetVariable;
                        }
                        else
                        {
                            pTargetVariable.Value = DBNull.Value;
                        }

                        return
                            database.Postgres.ExecuteQuery(
                                sqlCommand: SQL,
                                transaction: transaction,
                                pExportConnection, pTargetVariable);
                    }

                    #endregion Methods

                }

                #endregion FnEtlCheckTargetVariable

            }

        }
    }
}