﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {

                #region FnImportCheckLDsity

                /// <summary>
                /// Wrapper for stored procedure fn_import_check_ldsity.
                /// </summary>
                public static class FnImportCheckLDsity
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_import_check_ldsity";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_ldsity integer, ",
                            $"_ldsity_object__etl_id integer, ",
                            $"_label_en character varying; ",
                            $"returns: ",
                            $"TABLE(",
                            $"ldsity integer, ",
                            $"etl_id integer, ",
                            $"label character varying, ",
                            $"description text, ",
                            $"label_en character varying, ",
                            $"description_en text, ",
                            $"ldsity_object__id integer, ",
                            $"ldsity_object__label character varying, ",
                            $"ldsity_object__description text, ",
                            $"ldsity_object__label_en character varying, ",
                            $"ldsity_object__description_en text, ",
                            $"column_expression text, ",
                            $"unit_of_measure__id integer, ",
                            $"unit_of_measure__label character varying, ",
                            $"unit_of_measure__description text, ",
                            $"unit_of_measure__label_en character varying, ",
                            $"unit_of_measure__description_en text, ",
                            $"area_domain_category__id integer[], ",
                            $"area_domain_category__classification_rule text[], ",
                            $"sub_population_category__id integer[], ",
                            $"sub_population_category__classification_rule text[], ",
                            $"definition_variant__id integer[], ",
                            $"definition_variant__label character varying[], ",
                            $"definition_variant__description text[], ",
                            $"definition_variant__label_en character varying[], ",
                            $"definition_variant__description_en text[])");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    ldsity,{Environment.NewLine}",
                            $"    etl_id,{Environment.NewLine}",
                            $"    label,{Environment.NewLine}",
                            $"    description,{Environment.NewLine}",
                            $"    label_en,{Environment.NewLine}",
                            $"    description_en,{Environment.NewLine}",
                            $"    ldsity_object__id,{Environment.NewLine}",
                            $"    ldsity_object__label,{Environment.NewLine}",
                            $"    ldsity_object__description,{Environment.NewLine}",
                            $"    ldsity_object__label_en,{Environment.NewLine}",
                            $"    ldsity_object__description_en,{Environment.NewLine}",
                            $"    column_expression,{Environment.NewLine}",
                            $"    unit_of_measure__id,{Environment.NewLine}",
                            $"    unit_of_measure__label,{Environment.NewLine}",
                            $"    unit_of_measure__description,{Environment.NewLine}",
                            $"    unit_of_measure__label_en,{Environment.NewLine}",
                            $"    unit_of_measure__description_en,{Environment.NewLine}",
                            $"    array_to_string(area_domain_category__id, ';', 'NULL'),{Environment.NewLine}",
                            $"    array_to_string(area_domain_category__classification_rule, ';', 'NULL'),{Environment.NewLine}",
                            $"    array_to_string(sub_population_category__id, ';', 'NULL'),{Environment.NewLine}",
                            $"    array_to_string(sub_population_category__classification_rule, ';', 'NULL'),{Environment.NewLine}",
                            $"    array_to_string(definition_variant__id, ';', 'NULL'),{Environment.NewLine}",
                            $"    array_to_string(definition_variant__label, ';', 'NULL'),{Environment.NewLine}",
                            $"    array_to_string(definition_variant__description, ';', 'NULL'),{Environment.NewLine}",
                            $"    array_to_string(definition_variant__label_en, ';', 'NULL'),{Environment.NewLine}",
                            $"    array_to_string(definition_variant__description_en, ';', 'NULL'){Environment.NewLine}",
                            $"FROM{Environment.NewLine}",
                            $"    $SchemaName.$Name(",
                            $"@ldsity, ",
                            $"@ldsityObjectEtlId, ",
                            $"@labelEn);{Environment.NewLine}");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Execute stored procedure fn_import_check_ldsity
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="ldsity"></param>
                    /// <param name="ldsityObjectEtlId"></param>
                    /// <param name="labelEn"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> ldsity,
                        Nullable<int> ldsityObjectEtlId,
                        string labelEn,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = SQL;

                        CommandText = CommandText.Replace(
                            oldValue: "@ldsity",
                            newValue: Functions.PrepNIntArg(arg: ldsity));

                        CommandText = CommandText.Replace(
                           oldValue: "@ldsityObjectEtlId",
                           newValue: Functions.PrepNIntArg(arg: ldsityObjectEtlId));

                        CommandText = CommandText.Replace(
                            oldValue: "@labelEn",
                            newValue: Functions.PrepStringArg(arg: labelEn));

                        NpgsqlParameter pLDsity = new(
                            parameterName: "ldsity",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pLDsityObjectEtlId = new(
                           parameterName: "ldsityObjectEtlId",
                           parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pLabelEn = new(
                            parameterName: "labelEn",
                            parameterType: NpgsqlDbType.Varchar);

                        if (ldsity != null)
                        {
                            pLDsity.Value = (int)ldsity;
                        }
                        else
                        {
                            pLDsity.Value = DBNull.Value;
                        }

                        if (ldsityObjectEtlId != null)
                        {
                            pLDsityObjectEtlId.Value = (int)ldsityObjectEtlId;
                        }
                        else
                        {
                            pLDsityObjectEtlId.Value = DBNull.Value;
                        }

                        if (labelEn != null)
                        {
                            pLabelEn.Value = (string)labelEn;
                        }
                        else
                        {
                            pLabelEn.Value = DBNull.Value;
                        }

                        return
                            database.Postgres.ExecuteQuery(
                                sqlCommand: SQL,
                                transaction: transaction,
                                pLDsity, pLDsityObjectEtlId, pLabelEn);
                    }

                    #endregion Methods

                }

                #endregion FnImportCheckLDsity

            }

        }
    }
}