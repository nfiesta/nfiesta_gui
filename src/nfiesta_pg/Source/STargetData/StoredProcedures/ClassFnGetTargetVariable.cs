﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {

                #region FnGetTargetVariable

                /// <summary>
                /// Wrapper for stored procedure fn_get_target_variable.
                /// Function returns records from c_target_variable table,
                /// optionally for given ldsity_object_group.
                /// </summary>
                public static class FnGetTargetVariable
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_get_target_variable";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: _ldsity_object_group integer DEFAULT NULL::integer; ",
                            $"returns: ",
                            $"TABLE(",
                            $"id integer[], ",
                            $"core boolean[], ",
                            $"ldsity_object_group integer, ",
                            $"label character varying, ",
                            $"description text, ",
                            $"label_en character varying, ",
                            $"description_en text, ",
                            $"state_or_change integer, ",
                            $"soc_label character varying, ",
                            $"soc_description text, ",
                            $"areal_or_population integer)");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    {TDFnGetTargetVariableTypeList.ColId.SQL(TDFnGetTargetVariableTypeList.Cols, Name)}",
                            $"    {TDFnGetTargetVariableTypeList.ColCore.SQL(TDFnGetTargetVariableTypeList.Cols, Name)}",
                            $"    {TDFnGetTargetVariableTypeList.ColTargetVariableIds.SQL(TDFnGetTargetVariableTypeList.Cols, Name)}",
                            $"    {TDFnGetTargetVariableTypeList.ColLDsityObjectGroupId.SQL(TDFnGetTargetVariableTypeList.Cols, Name)}",
                            $"    {TDFnGetTargetVariableTypeList.ColLabelCs.SQL(TDFnGetTargetVariableTypeList.Cols, Name)}",
                            $"    {TDFnGetTargetVariableTypeList.ColDescriptionCs.SQL(TDFnGetTargetVariableTypeList.Cols, Name)}",
                            $"    {TDFnGetTargetVariableTypeList.ColLabelEn.SQL(TDFnGetTargetVariableTypeList.Cols, Name)}",
                            $"    {TDFnGetTargetVariableTypeList.ColDescriptionEn.SQL(TDFnGetTargetVariableTypeList.Cols, Name)}",
                            $"    {TDFnGetTargetVariableTypeList.ColStateOrChangeId.SQL(TDFnGetTargetVariableTypeList.Cols, Name)}",
                            $"    {TDFnGetTargetVariableTypeList.ColSocLabelEn.SQL(TDFnGetTargetVariableTypeList.Cols, Name)}",
                            $"    {TDFnGetTargetVariableTypeList.ColSocDescriptionEn.SQL(TDFnGetTargetVariableTypeList.Cols, Name)}",
                            $"    {TDFnGetTargetVariableTypeList.ColArealOrPopulationId.SQL(TDFnGetTargetVariableTypeList.Cols, Name, isLastOne: true)}",
                            $"FROM{Environment.NewLine}",
                            $"    $SchemaName.$Name(@ldsityObjectGroupId) AS {Name}{Environment.NewLine}",
                            $"ORDER BY{Environment.NewLine}",
                            $"    {Name}.{TDFnGetTargetVariableTypeList.ColLDsityObjectGroupId.DbName};{Environment.NewLine}"
                            );

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Execute stored procedure fn_get_target_variable
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="ldsityObjectGroupId">Group of local density objects identifier</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table with target variables</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> ldsityObjectGroupId = null,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = SQL;

                        CommandText = CommandText.Replace(
                            oldValue: "@ldsityObjectGroupId",
                            newValue: Functions.PrepNIntArg(arg: ldsityObjectGroupId));

                        NpgsqlParameter pLDsityObjectGroupId = new(
                            parameterName: "ldsityObjectGroupId",
                            parameterType: NpgsqlDbType.Integer);

                        if (ldsityObjectGroupId != null)
                        {
                            pLDsityObjectGroupId.Value = (int)ldsityObjectGroupId;
                        }
                        else
                        {
                            pLDsityObjectGroupId.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteQuery(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pLDsityObjectGroupId);
                    }

                    /// <summary>
                    /// Execute stored procedure fn_get_target_variable
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="ldsityObjectGroupId">Group of local density objects identifier</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>List of target variables</returns>
                    public static TDFnGetTargetVariableTypeList Execute(
                        NfiEstaDB database,
                        Nullable<int> ldsityObjectGroupId = null,
                        NpgsqlTransaction transaction = null)
                    {
                        System.Diagnostics.Stopwatch stopWatch = new();
                        stopWatch.Start();

                        DataTable data = ExecuteQuery(
                                database: database,
                                ldsityObjectGroupId: ldsityObjectGroupId,
                                transaction: transaction);
                        data.TableName = Name;

                        stopWatch.Stop();

                        return new TDFnGetTargetVariableTypeList(
                            database: database,
                            data: data,
                            loadingTime: 0.001 * stopWatch.ElapsedMilliseconds,
                            storedProcedure: CommandText)
                        {
                            LDsityObjectGroupId = ldsityObjectGroupId
                        };
                    }

                    #endregion Methods

                }

                #endregion FnGetTargetVariable

            }

        }
    }
}