﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {

                #region FnSaveClassRuleMapping

                /// <summary>
                /// Wrapper for stored procedure fn_save_class_rule_mapping.
                /// Function checks syntax in text field with classification rules.
                /// </summary>
                public static class FnSaveClassRuleMapping
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_save_class_rule_mapping";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_areal_or_pop integer, ",
                            $"_rules integer[], ",
                            $"_use_negative boolean[], ",
                            $"_panel_refyearset integer[]; ",
                            $"returns: ",
                            $"TABLE(",
                            $"classification_rule integer, ",
                            $"refyearset2panel integer)");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    classification_rule,{Environment.NewLine}",
                            $"    refyearset2panel{Environment.NewLine}",
                            $"FROM{Environment.NewLine}",
                            $"    $SchemaName.$Name(",
                            $"@arealOrPopulationId, ",
                            $"@rulesIds, ",
                            $"@useNegative, ",
                            $"@panelRefYearSetIds);{Environment.NewLine}");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Execute stored procedure fn_save_class_rule_mapping
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="arealOrPopulationId">Areal or population identifier</param>
                    /// <param name="rulesIds">List of classification rule identifiers</param>
                    /// <param name="useNegative">Use negative local density contributions</param>
                    /// <param name="panelRefYearSetIds">List of panel reference year set identifiers</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table with classification rule identifier and panel reference year set identifier</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> arealOrPopulationId,
                        List<Nullable<int>> rulesIds,
                        List<Nullable<bool>> useNegative,
                        List<Nullable<int>> panelRefYearSetIds,
                        NpgsqlTransaction transaction = null
                       )
                    {
                        CommandText = SQL;

                        CommandText = CommandText.Replace(
                            oldValue: "@arealOrPopulationId",
                            newValue: Functions.PrepNIntArg(arg: arealOrPopulationId));

                        CommandText = CommandText.Replace(
                            oldValue: "@rulesIds",
                            newValue: Functions.PrepNIntArrayArg(args: rulesIds, dbType: "int4"));

                        CommandText = CommandText.Replace(
                            oldValue: "@useNegative",
                            newValue: Functions.PrepNBoolArrayArg(args: useNegative, dbType: "bool"));

                        CommandText = CommandText.Replace(
                            oldValue: "@panelRefYearSetIds",
                            newValue: Functions.PrepNIntArrayArg(args: panelRefYearSetIds, dbType: "int4"));

                        NpgsqlParameter pArealOrPopulationId = new(
                            parameterName: "arealOrPopulationId",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pRulesIds = new(
                            parameterName: "rulesIds",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        NpgsqlParameter pUseNegative = new(
                            parameterName: "useNegative",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Boolean);

                        NpgsqlParameter pPanelRefYearSetIds = new(
                            parameterName: "panelRefYearSetIds",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        if (arealOrPopulationId != null)
                        {
                            pArealOrPopulationId.Value = (int)arealOrPopulationId;
                        }
                        else
                        {
                            pArealOrPopulationId.Value = DBNull.Value;
                        }

                        if (rulesIds != null)
                        {
                            pRulesIds.Value = rulesIds.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pRulesIds.Value = DBNull.Value;
                        }

                        if (useNegative != null)
                        {
                            pUseNegative.Value = useNegative.ToArray<Nullable<bool>>();
                        }
                        else
                        {
                            pUseNegative.Value = DBNull.Value;
                        }

                        if (panelRefYearSetIds != null)
                        {
                            pPanelRefYearSetIds.Value = panelRefYearSetIds.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pPanelRefYearSetIds.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteQuery(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pArealOrPopulationId, pRulesIds, pUseNegative, pPanelRefYearSetIds);
                    }

                    /// <summary>
                    /// Execute stored procedure fn_save_class_rule_mapping
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="arealOrPopulationId">Areal or population identifier</param>
                    /// <param name="rulesIds">List of classification rule identifiers</param>
                    /// <param name="useNegative">Use negative local density contributions</param>
                    /// <param name="panelRefYearSetIds">List of panel reference year set identifiers</param>
                    /// <param name="transaction">Transaction</param>
                    public static void Execute(
                        NfiEstaDB database,
                        Nullable<int> arealOrPopulationId,
                        List<Nullable<int>> rulesIds,
                        List<Nullable<bool>> useNegative,
                        List<Nullable<int>> panelRefYearSetIds,
                        NpgsqlTransaction transaction = null
                       )
                    {
                        ExecuteQuery(
                            database: database,
                            arealOrPopulationId: arealOrPopulationId,
                            rulesIds: rulesIds,
                            useNegative: useNegative,
                            panelRefYearSetIds: panelRefYearSetIds,
                            transaction: transaction);
                        return;
                    }

                    #endregion Methods

                }

                #endregion FnSaveClassRuleMapping

            }
        }
    }
}