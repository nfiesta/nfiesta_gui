﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {

                #region FnCheckStoredClassificationRules

                /// <summary>
                /// Wrapper for stored procedure fn_check_stored_classification_rules.
                /// Function checks already stored classification rules by passing them into the fn_check_classification_rules.
                /// </summary>
                public static class FnCheckStoredClassificationRules
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name = "fn_check_stored_classification_rules";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_ldsity integer, ",
                            $"_ldsity_object integer, ",
                            $"_area_domain integer DEFAULT NULL::integer, ",
                            $"_sub_population integer DEFAULT NULL::integer, ",
                            $"_panel_refyearset integer DEFAULT NULL::integer, ",
                            $"_use_negative boolean DEFAULT false; ",
                            $"returns: ",
                            $"TABLE(",
                            $"result boolean, ",
                            $"message_id integer, ",
                            $"message text)");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                    = String.Concat(
                    $"SELECT{Environment.NewLine}",
                    $"    $Name.result       AS result,{Environment.NewLine}",
                    $"    $Name.message_id   AS message_id,{Environment.NewLine}",
                    $"    $Name.message      AS message{Environment.NewLine}",
                    $"FROM{Environment.NewLine}",
                    $"    $SchemaName.$Name(@ldsityId, @ldsityObjectId, @areaDomainId, @subPopulationId, @panelRefYearSetId, @useNegative) ",
                    $"AS $Name;{Environment.NewLine}");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Execute stored procedure fn_check_stored_classification_rules
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="ldsityId">Local density identifier</param>
                    /// <param name="ldsityObjectId">Local density object identifier</param>
                    /// <param name="areaDomainId">Area domain identifier</param>
                    /// <param name="subPopulationId">Subpopulation identifier</param>
                    /// <param name="panelRefYearSetId">Panel reference year set identifier</param>
                    /// <param name="useNegative">Positive (false) or negative (true) classification rule</param>
                    /// <param name="transaction">Transaction</param>
                    public static void Execute(
                        NfiEstaDB database,
                        Nullable<int> ldsityId,
                        Nullable<int> ldsityObjectId,
                        Nullable<int> areaDomainId,
                        Nullable<int> subPopulationId,
                        Nullable<int> panelRefYearSetId,
                        Nullable<bool> useNegative,
                        NpgsqlTransaction transaction = null)
                    {
                        ExecuteQuery(
                            database: database,
                            ldsityId: ldsityId,
                            ldsityObjectId: ldsityObjectId,
                            areaDomainId: areaDomainId,
                            subPopulationId: subPopulationId,
                            panelRefYearSetId: panelRefYearSetId,
                            useNegative: useNegative,
                            transaction: transaction);
                    }

                    /// <summary>
                    /// Execute stored procedure fn_check_stored_classification_rules
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="ldsityId">Local density identifier</param>
                    /// <param name="ldsityObjectId">Local density object identifier</param>
                    /// <param name="areaDomainId">Area domain identifier</param>
                    /// <param name="subPopulationId">Subpopulation identifier</param>
                    /// <param name="panelRefYearSetId">Panel reference year set identifier</param>
                    /// <param name="useNegative">Positive (false) or negative (true) classification rule</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> ldsityId,
                        Nullable<int> ldsityObjectId,
                        Nullable<int> areaDomainId,
                        Nullable<int> subPopulationId,
                        Nullable<int> panelRefYearSetId,
                        Nullable<bool> useNegative,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = SQL;

                        CommandText = CommandText.Replace(
                            oldValue: "@ldsityId",
                            newValue: Functions.PrepNIntArg(arg: ldsityId));

                        CommandText = CommandText.Replace(
                           oldValue: "@ldsityObjectId",
                           newValue: Functions.PrepNIntArg(arg: ldsityObjectId));

                        CommandText = CommandText.Replace(
                           oldValue: "@areaDomainId",
                           newValue: Functions.PrepNIntArg(arg: areaDomainId));

                        CommandText = CommandText.Replace(
                           oldValue: "@subPopulationId",
                           newValue: Functions.PrepNIntArg(arg: subPopulationId));

                        CommandText = CommandText.Replace(
                           oldValue: "@panelRefYearSetId",
                           newValue: Functions.PrepNIntArg(arg: panelRefYearSetId));

                        CommandText = CommandText.Replace(
                            oldValue: "@useNegative",
                            newValue: Functions.PrepNBoolArg(arg: useNegative));

                        NpgsqlParameter pLDsityId = new(
                            parameterName: "ldsityId",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pLDsityObjectId = new(
                            parameterName: "ldsityObjectId",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pAreaDomainId = new(
                            parameterName: "areaDomainId",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pSubPopulationId = new(
                            parameterName: "subPopulationId",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pPanelRefYearSetId = new(
                            parameterName: "panelRefYearSetId",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pUseNegative = new(
                            parameterName: "useNegative",
                            parameterType: NpgsqlDbType.Boolean);

                        if (ldsityId != null)
                        {
                            pLDsityId.Value = (int)ldsityId;
                        }
                        else
                        {
                            pLDsityId.Value = DBNull.Value;
                        }

                        if (ldsityObjectId != null)
                        {
                            pLDsityObjectId.Value = (int)ldsityObjectId;
                        }
                        else
                        {
                            pLDsityObjectId.Value = DBNull.Value;
                        }

                        if (areaDomainId != null)
                        {
                            pAreaDomainId.Value = (int)areaDomainId;
                        }
                        else
                        {
                            pAreaDomainId.Value = DBNull.Value;
                        }

                        if (subPopulationId != null)
                        {
                            pSubPopulationId.Value = (int)subPopulationId;
                        }
                        else
                        {
                            pSubPopulationId.Value = DBNull.Value;
                        }

                        if (panelRefYearSetId != null)
                        {
                            pPanelRefYearSetId.Value = (int)panelRefYearSetId;
                        }
                        else
                        {
                            pPanelRefYearSetId.Value = DBNull.Value;
                        }

                        if (useNegative != null)
                        {
                            pUseNegative.Value = (bool)useNegative;
                        }
                        else
                        {
                            pUseNegative.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteQuery(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pLDsityId, pLDsityObjectId,
                            pAreaDomainId, pSubPopulationId,
                            pPanelRefYearSetId, pUseNegative);
                    }

                    #endregion Methods

                }

                #endregion FnCheckStoredClassificationRules

            }

        }
    }
}