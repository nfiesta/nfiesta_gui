﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {

                #region FnCheckClassificationRules

                /// <summary>
                /// Wrapper for stored procedure fn_check_classification_rules.
                /// Function checks syntax in text field with classification rules
                /// for given ldsity contribution and ldsity object within its hierarchy.
                /// Optionally also for given panel x reference year set combination and/or
                /// area domain/sub population category/ies.
                /// </summary>
                public static class FnCheckClassificationRules
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_check_classification_rules";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_ldsity integer, ",
                            $"_ldsity_object integer, ",
                            $"_rules text[], ",
                            $"_panel_refyearset integer DEFAULT NULL::integer, ",
                            $"_adc integer[] DEFAULT NULL::integer[], ",
                            $"_spc integer[] DEFAULT NULL::integer[], ",
                            $"_use_negative boolean DEFAULT false; ",
                            $"returns: ",
                            $"TABLE(",
                            $"result boolean, ",
                            $"message_id integer, ",
                            $"message text)");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                    = String.Concat(
                    $"SELECT{Environment.NewLine}",
                            $"    {TDFnCheckClassificationRulesTypeList.ColId.SQL(TDFnCheckClassificationRulesTypeList.Cols, Name)}",
                            $"    @panelRefYearSetId AS {TDFnCheckClassificationRulesTypeList.ColPanelRefYearSetId.Name},{Environment.NewLine}",
                            $"    {TDFnCheckClassificationRulesTypeList.ColResult.SQL(TDFnCheckClassificationRulesTypeList.Cols, Name)}",
                            $"    {TDFnCheckClassificationRulesTypeList.ColMessageId.SQL(TDFnCheckClassificationRulesTypeList.Cols, Name)}",
                            $"    {TDFnCheckClassificationRulesTypeList.ColMessage.SQL(TDFnCheckClassificationRulesTypeList.Cols, Name, isLastOne: true)}",
                            $"FROM {Environment.NewLine}",
                            $"    $SchemaName.$Name{Environment.NewLine}",
                            $"({Environment.NewLine}",
                            $"@ldsityId,            -- ldsityId{Environment.NewLine}",
                            $"@ldsityObjectId,      -- ldsityObjectId{Environment.NewLine}",
                            $"@rules,               -- rules{Environment.NewLine}",
                            $"@panelRefYearSetId,   -- panelRefYearSetId{Environment.NewLine}",
                            $"@adc,                 -- adc{Environment.NewLine}",
                            $"@spc,                 -- spc{Environment.NewLine}",
                            $"@useNegative          -- useNegative{Environment.NewLine}",
                            $") AS {Name};{Environment.NewLine}");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Execute stored procedure fn_check_classification_rules
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="ldsityId">Local density contribution identifier</param>
                    /// <param name="ldsityObjectId">Local density object identifier</param>
                    /// <param name="rules">List of the classification rule texts</param>
                    /// <param name="panelRefYearSetId">Pair of panel with corresponding reference year set identifier</param>
                    /// <param name="adc">Area domain categories</param>
                    /// <param name="spc">Subpopulation categories</param>
                    /// <param name="useNegative">Positive (false) or negative (true) classification rule</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table with classification rule check results</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> ldsityId,
                        Nullable<int> ldsityObjectId,
                        List<string> rules,
                        Nullable<int> panelRefYearSetId,
                        List<List<Nullable<int>>> adc,
                        List<List<Nullable<int>>> spc,
                        Nullable<bool> useNegative,
                        NpgsqlTransaction transaction = null)
                    {
                        string sql = SQL;

                        sql = sql.Replace(
                            oldValue: "@adc",
                            newValue: Functions.PrepNIntArrayOfArrayArg(args: adc, dbType: "int4"));

                        sql = sql.Replace(
                            oldValue: "@spc",
                            newValue: Functions.PrepNIntArrayOfArrayArg(args: spc, dbType: "int4"));

                        CommandText = sql;

                        CommandText = CommandText.Replace(
                            oldValue: "@ldsityId",
                            newValue: Functions.PrepNIntArg(arg: ldsityId));

                        CommandText = CommandText.Replace(
                            oldValue: "@ldsityObjectId",
                            newValue: Functions.PrepNIntArg(arg: ldsityObjectId));

                        CommandText = CommandText.Replace(
                            oldValue: "@rules",
                            newValue: Functions.PrepStringArrayArg(args: rules, dbType: "text"));

                        CommandText = CommandText.Replace(
                            oldValue: "@panelRefYearSetId",
                            newValue: Functions.PrepNIntArg(arg: panelRefYearSetId));

                        CommandText = CommandText.Replace(
                            oldValue: "@useNegative",
                            newValue: Functions.PrepNBoolArg(arg: useNegative));

                        NpgsqlParameter pLDsityId = new(
                            parameterName: "ldsityId",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pLDsityObjectId = new(
                            parameterName: "ldsityObjectId",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pRules = new(
                            parameterName: "rules",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Text);

                        NpgsqlParameter pPanelRefYearSetId = new(
                            parameterName: "panelRefYearSetId",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pUseNegative = new(
                            parameterName: "useNegative",
                            parameterType: NpgsqlDbType.Boolean);

                        if (ldsityId != null)
                        {
                            pLDsityId.Value = (int)ldsityId;
                        }
                        else
                        {
                            pLDsityId.Value = DBNull.Value;
                        }

                        if (ldsityObjectId != null)
                        {
                            pLDsityObjectId.Value = (int)ldsityObjectId;
                        }
                        else
                        {
                            pLDsityObjectId.Value = DBNull.Value;
                        }

                        if (rules != null)
                        {
                            pRules.Value = rules.ToArray<string>();
                        }
                        else
                        {
                            pRules.Value = DBNull.Value;
                        }

                        if (panelRefYearSetId != null)
                        {
                            pPanelRefYearSetId.Value = (int)panelRefYearSetId;
                        }
                        else
                        {
                            pPanelRefYearSetId.Value = DBNull.Value;
                        }

                        if (useNegative != null)
                        {
                            pUseNegative.Value = (bool)useNegative;
                        }
                        else
                        {
                            pUseNegative.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteQuery(
                            sqlCommand: sql,
                            transaction: transaction,
                            pLDsityId, pLDsityObjectId, pRules,
                            pPanelRefYearSetId, pUseNegative);
                    }

                    /// <summary>
                    /// Execute stored procedure fn_check_classification_rules
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="ldsityId">Local density contribution identifier</param>
                    /// <param name="ldsityObjectId">Local density object identifier</param>
                    /// <param name="rules">List of the classification rule texts</param>
                    /// <param name="panelRefYearSetId">Pair of panel with corresponding reference year set identifier</param>
                    /// <param name="adc">Area domain categories</param>
                    /// <param name="spc">Subpopulation categories</param>
                    /// <param name="useNegative">Positive (false) or negative (true) classification rule</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>List of classification rule check results</returns>
                    public static TDFnCheckClassificationRulesTypeList Execute(
                        NfiEstaDB database,
                        Nullable<int> ldsityId,
                        Nullable<int> ldsityObjectId,
                        List<string> rules,
                        Nullable<int> panelRefYearSetId,
                        List<List<Nullable<int>>> adc,
                        List<List<Nullable<int>>> spc,
                        Nullable<bool> useNegative,
                        NpgsqlTransaction transaction = null)
                    {
                        System.Diagnostics.Stopwatch stopWatch = new();
                        stopWatch.Start();

                        DataTable data = ExecuteQuery(
                                database: database,
                                ldsityId: ldsityId,
                                ldsityObjectId: ldsityObjectId,
                                rules: rules,
                                panelRefYearSetId: panelRefYearSetId,
                                adc: adc,
                                spc: spc,
                                useNegative: useNegative,
                                transaction: transaction);
                        data.TableName = Name;

                        stopWatch.Stop();

                        return new TDFnCheckClassificationRulesTypeList(
                            database: database,
                            data: data,
                            loadingTime: 0.001 * stopWatch.ElapsedMilliseconds,
                            storedProcedure: CommandText)
                        {
                            LDsityId = ldsityId,
                            LDsityObjectId = ldsityObjectId,
                            Rules = rules,
                            PanelRefYearSetId = panelRefYearSetId,
                            Adc = adc,
                            Spc = spc,
                            UseNegative = useNegative
                        };
                    }

                    #endregion Methods

                }

                #endregion FnCheckClassificationRules

            }

        }
    }
}