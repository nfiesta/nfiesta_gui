﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {

                #region FnGetAvailableRefYearSetToPanelForClassificationRuleIdAdc

                /// <summary>
                /// Wrapper for stored procedure fn_get_available_refyearset2panel4classification_rule_id.
                /// The function gets list of available IDs of refyear2panel for given input arguments.
                /// </summary>
                public static class FnGetAvailableRefYearSetToPanelForClassificationRuleIdAdc
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_get_available_refyearset2panel4classification_rule_id";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_classification_rule_type character varying, ",
                            $"_classification_rule_id integer; ",
                            $"returns: ",
                            $"TABLE(",
                            $"classification_rule_id integer, ",
                            $"refyearset2panel integer)");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    $Name.classification_rule_id    AS classification_rule_id,{Environment.NewLine}",
                            $"    $Name.refyearset2panel          AS refyearset2panel{Environment.NewLine}",
                            $"FROM{Environment.NewLine}",
                            $"    $SchemaName.$Name(",
                            $"@classificationRuleType, ",
                            $"@classificationRuleId) ",
                            $"AS $Name {Environment.NewLine}",
                            $"ORDER BY{Environment.NewLine}",
                            $"    $Name.classification_rule_id;{Environment.NewLine}"
                            );


                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Execute stored procedure fn_get_available_refyearset2panel4classification_rule_id
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="classificationRuleId">Classification rule identifier
                    /// (primary key from table cm_adc2classification_rule or cm_spc2classification_rule)</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table with available IDs of refyearset2panel</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> classificationRuleId,
                        NpgsqlTransaction transaction = null)
                    {
                        string classificationRuleType = "adc";

                        CommandText = SQL;

                        CommandText = CommandText.Replace(
                            oldValue: "@classificationRuleType",
                            newValue: Functions.PrepStringArg(arg: classificationRuleType));

                        CommandText = CommandText.Replace(
                            oldValue: "@classificationRuleId",
                            newValue: Functions.PrepNIntArg(arg: classificationRuleId));

                        NpgsqlParameter pClassificationRuleType = new(
                            parameterName: "classificationRuleType",
                            parameterType: NpgsqlDbType.Varchar);

                        NpgsqlParameter pClassificationRuleId = new(
                            parameterName: "classificationRuleId",
                            parameterType: NpgsqlDbType.Integer);

                        if (classificationRuleType != null)
                        {
                            pClassificationRuleType.Value = (string)classificationRuleType;
                        }
                        else
                        {
                            pClassificationRuleType.Value = DBNull.Value;
                        }

                        if (classificationRuleId != null)
                        {
                            pClassificationRuleId.Value = (int)classificationRuleId;
                        }
                        else
                        {
                            pClassificationRuleId.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteQuery(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pClassificationRuleType, pClassificationRuleId);
                    }

                    #endregion Methods

                }

                #endregion FnGetAvailableRefYearSetToPanelForClassificationRuleIdAdc

            }
        }
    }
}