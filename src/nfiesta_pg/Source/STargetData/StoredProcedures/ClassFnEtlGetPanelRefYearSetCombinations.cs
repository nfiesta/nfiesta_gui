﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {
                // fn_etl_get_panel_refyearset_combinations

                #region FnEtlGetPanelRefYearSetCombinations

                /// <summary>
                /// Wrapper for stored procedure fn_etl_get_panel_refyearset_combinations.
                /// Function returns list of combinations of panels and reference year sets for given target variable that are available for ETL.
                /// </summary>
                public static class FnEtlGetPanelRefYearSetCombinations
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_etl_get_panel_refyearset_combinations";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_id_t_etl_target_variable integer, ",
                            $"_refyearset2panel_mapping integer[], ",
                            $"_refyearset2panel_mapping_module integer[] DEFAULT NULL::integer[]; ",
                            $"returns: ",
                            $"TABLE(",
                            $"refyearset2panel integer, ",
                            $"panel character varying, ",
                            $"reference_year_set character varying, ",
                            $"passed_by_module boolean)");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    {TDFnEtlGetPanelRefYearSetCombinationsTypeList.ColRefYearSetToPanel.SQL(cols: TDFnEtlGetPanelRefYearSetCombinationsTypeList.Cols, alias: Name)}",
                            $"    {TDFnEtlGetPanelRefYearSetCombinationsTypeList.ColPanel.SQL(cols: TDFnEtlGetPanelRefYearSetCombinationsTypeList.Cols, alias: Name)}",
                            $"    {TDFnEtlGetPanelRefYearSetCombinationsTypeList.ColReferenceYearSet.SQL(cols: TDFnEtlGetPanelRefYearSetCombinationsTypeList.Cols, alias: Name)}",
                            $"    {TDFnEtlGetPanelRefYearSetCombinationsTypeList.ColPassedByModule.SQL(cols: TDFnEtlGetPanelRefYearSetCombinationsTypeList.Cols, alias: Name, isLastOne: true)}",
                            $"FROM{Environment.NewLine}",
                            $"    $SchemaName.$Name(@idTEtlTargetVariable, @refYearSetToPanelMapping, @refYearSetToPanelMappingModule) AS {Name}{Environment.NewLine}",
                            $"ORDER BY{Environment.NewLine}",
                            $"    {Name}.{TDFnEtlGetPanelRefYearSetCombinationsTypeList.ColRefYearSetToPanel.Name};{Environment.NewLine}"
                            );

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="idTEtlTargetVariable">Id from t_etl_target_variable table</param>
                    /// <param name="refYearSetToPanelMapping">Ids of combinations of panels and reference-year sets</param>
                    /// <param name="refYearSetToPanelMappingModule">Ids of combinations of panels and reference-year sets
                    /// - whether they come directly from the module for configuration of ldsity values</param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        Nullable<int> idTEtlTargetVariable,
                        List<Nullable<int>> refYearSetToPanelMapping,
                        List<Nullable<int>> refYearSetToPanelMappingModule)
                    {
                        string result = SQL;

                        result = result.Replace(
                            oldValue: "@idTEtlTargetVariable",
                            newValue: Functions.PrepNIntArg(arg: idTEtlTargetVariable));

                        result = result.Replace(
                            oldValue: "@refYearSetToPanelMapping",
                            newValue: Functions.PrepNIntArrayArg(args: refYearSetToPanelMapping, dbType: "int4"));

                        result = result.Replace(
                            oldValue: "@refYearSetToPanelMappingModule",
                            newValue: Functions.PrepNIntArrayArg(args: refYearSetToPanelMappingModule, dbType: "int4"));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_etl_get_panel_refyearset_combinations
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="idTEtlTargetVariable">Id from t_etl_target_variable table</param>
                    /// <param name="refYearSetToPanelMapping">Ids of combinations of panels and reference-year sets</param>
                    /// <param name="refYearSetToPanelMappingModule">Ids of combinations of panels and reference-year sets
                    /// - whether they come directly from the module for configuration of ldsity values</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> idTEtlTargetVariable,
                        List<Nullable<int>> refYearSetToPanelMapping,
                        List<Nullable<int>> refYearSetToPanelMappingModule,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(
                            idTEtlTargetVariable: idTEtlTargetVariable,
                            refYearSetToPanelMapping: refYearSetToPanelMapping,
                            refYearSetToPanelMappingModule: refYearSetToPanelMappingModule);

                        NpgsqlParameter pIdTEtlTargetVariable = new(
                            parameterName: "idTEtlTargetVariable",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pRefYearSetToPanelMapping = new(
                            parameterName: "refYearSetToPanelMapping",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        NpgsqlParameter pRefYearSetToPanelMappingModule = new(
                            parameterName: "refYearSetToPanelMappingModule",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        if (idTEtlTargetVariable != null)
                        {
                            pIdTEtlTargetVariable.Value = (int)idTEtlTargetVariable;
                        }
                        else
                        {
                            pIdTEtlTargetVariable.Value = DBNull.Value;
                        }

                        if (refYearSetToPanelMapping != null)
                        {
                            pRefYearSetToPanelMapping.Value = refYearSetToPanelMapping.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pRefYearSetToPanelMapping.Value = DBNull.Value;
                        }

                        if (refYearSetToPanelMappingModule != null)
                        {
                            pRefYearSetToPanelMappingModule.Value = refYearSetToPanelMappingModule.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pRefYearSetToPanelMappingModule.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteQuery(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pIdTEtlTargetVariable,
                            pRefYearSetToPanelMapping,
                            pRefYearSetToPanelMappingModule);
                    }

                    /// <summary>
                    /// Execute stored procedure fn_etl_get_panel_refyearset_combinations
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="idTEtlTargetVariable">Id of the selected target variable</param>
                    /// <param name="refYearSetToPanelMapping">Ids of the panel and refyearset combinations</param>
                    /// <param name="refYearSetToPanelMappingModule">Ids of combinations of panels and reference-year sets
                    /// - whether they come directly from the module for configuration of ldsity values</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>List of panel and refyearset combinations</returns>
                    public static TDFnEtlGetPanelRefYearSetCombinationsTypeList Execute(
                        NfiEstaDB database,
                        Nullable<int> idTEtlTargetVariable,
                        List<Nullable<int>> refYearSetToPanelMapping,
                        List<Nullable<int>> refYearSetToPanelMappingModule,
                        NpgsqlTransaction transaction = null)
                    {
                        System.Diagnostics.Stopwatch stopWatch = new();
                        stopWatch.Start();

                        DataTable data = ExecuteQuery(
                                database: database,
                                idTEtlTargetVariable: idTEtlTargetVariable,
                                refYearSetToPanelMapping: refYearSetToPanelMapping,
                                refYearSetToPanelMappingModule: refYearSetToPanelMappingModule,
                                transaction: transaction);
                        data.TableName = Name;

                        stopWatch.Stop();

                        return new TDFnEtlGetPanelRefYearSetCombinationsTypeList(
                            database: database,
                            data: data,
                            loadingTime: 0.001 * stopWatch.ElapsedMilliseconds,
                            storedProcedure: CommandText)
                        {
                            IdTEtlTargetVariable = idTEtlTargetVariable,
                            RefYearSetToPanelMapping = refYearSetToPanelMapping,
                            RefYearSetToPanelMappingModule = refYearSetToPanelMappingModule
                        };
                    }

                    #endregion Methods

                }

                #endregion FnEtlGetPanelRefYearSetCombinations

            }

        }
    }
}