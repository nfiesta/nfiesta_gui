﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {

                #region FnGetSubPopulation

                /// <summary>
                /// Wrapper for stored procedure fn_get_sub_population.
                /// Function returns records from c_sub_population table.
                /// </summary>
                public static class FnGetSubPopulation
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_get_sub_population";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_id integer DEFAULT NULL::integer, ",
                            $"_target_variable integer DEFAULT NULL::integer; ",
                            $"returns: ",
                            $"TABLE(",
                            $"id integer, ",
                            $"label character varying, ",
                            $"description text, ",
                            $"label_en character varying, ",
                            $"description_en text, ",
                            $"classification_type integer)");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                    = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    {TDSubPopulationList.ColId.SQL(TDSubPopulationList.Cols, Name)}",
                            $"    {TDSubPopulationList.ColLabelCs.SQL(TDSubPopulationList.Cols, Name)}",
                            $"    {TDSubPopulationList.ColDescriptionCs.SQL(TDSubPopulationList.Cols, Name)}",
                            $"    {TDSubPopulationList.ColLabelEn.SQL(TDSubPopulationList.Cols, Name)}",
                            $"    {TDSubPopulationList.ColDescriptionEn.SQL(TDSubPopulationList.Cols, Name)}",
                            $"    {TDSubPopulationList.ColClassificationType.SQL(TDSubPopulationList.Cols, Name, isLastOne: true)}",
                            $"FROM{Environment.NewLine}",
                            $"    $SchemaName.$Name(@subPopulationId, @targetVariableId) AS {Name}{Environment.NewLine}",
                            $"ORDER BY{Environment.NewLine}",
                            $"    {Name}.{TDSubPopulationList.ColId.DbName};{Environment.NewLine}"
                            );

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Execute stored procedure fn_get_sub_population
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="subPopulationId">Subpopulation identifier</param>
                    /// <param name="targetVariableId">Target variable identifier</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table with subpopulations</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> subPopulationId = null,
                        Nullable<int> targetVariableId = null,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = SQL;

                        CommandText = CommandText.Replace(
                            oldValue: "@subPopulationId",
                            newValue: Functions.PrepNIntArg(arg: subPopulationId));

                        CommandText = CommandText.Replace(
                            oldValue: "@targetVariableId",
                            newValue: Functions.PrepNIntArg(arg: targetVariableId));

                        NpgsqlParameter pSubPopulationId = new(
                            parameterName: "subPopulationId",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pTargetVariableId = new(
                           parameterName: "targetVariableId",
                           parameterType: NpgsqlDbType.Integer);

                        if (subPopulationId != null)
                        {
                            pSubPopulationId.Value = (int)subPopulationId;
                        }
                        else
                        {
                            pSubPopulationId.Value = DBNull.Value;
                        }

                        if (targetVariableId != null)
                        {
                            pTargetVariableId.Value = (int)targetVariableId;
                        }
                        else
                        {
                            pTargetVariableId.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteQuery(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pSubPopulationId, pTargetVariableId);
                    }

                    /// <summary>
                    /// Execute stored procedure fn_get_sub_population
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="subPopulationId">Subpopulation identifier</param>
                    /// <param name="targetVariableId">Target variable identifier</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>List of subpopulations</returns>
                    public static TDSubPopulationList Execute(
                        NfiEstaDB database,
                        Nullable<int> subPopulationId = null,
                        Nullable<int> targetVariableId = null,
                        NpgsqlTransaction transaction = null)
                    {
                        return new TDSubPopulationList(
                            database: database,
                            data: ExecuteQuery(
                                database: database,
                                subPopulationId: subPopulationId,
                                targetVariableId: targetVariableId,
                                transaction: transaction))
                        { StoredProcedure = CommandText };
                    }

                    #endregion Methods

                }

                #endregion FnGetSubPopulation

            }

        }
    }
}