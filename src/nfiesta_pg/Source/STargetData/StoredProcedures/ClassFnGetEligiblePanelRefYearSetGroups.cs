﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {

                #region FnGetEligiblePanelRefYearSetGroups

                /// <summary>
                /// Wrapper for stored procedure fn_get_eligible_panel_refyearset_groups.
                /// Function returns groups of panels and reference-yearsets,
                /// for which local densities can be calculated.
                /// </summary>
                public static class FnGetEligiblePanelRefYearSetGroups
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_get_eligible_panel_refyearset_groups";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_eligible_refyearset2panel_mappings integer[]; ",
                            $"returns: ",
                            $"TABLE(",
                            $"panel_refyearset_group integer, ",
                            $"label character varying, ",
                            $"description text, ",
                            $"label_en character varying, ",
                            $"description_en text)");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    {Name}.panel_refyearset_group AS {TDPanelRefYearSetGroupList.ColId.Name},{Environment.NewLine}",
                            $"    {TDPanelRefYearSetGroupList.ColLabelCs.SQL(TDPanelRefYearSetGroupList.Cols, Name)}",
                            $"    {TDPanelRefYearSetGroupList.ColDescriptionCs.SQL(TDPanelRefYearSetGroupList.Cols, Name)}",
                            $"    {TDPanelRefYearSetGroupList.ColLabelEn.SQL(TDPanelRefYearSetGroupList.Cols, Name)}",
                            $"    {TDPanelRefYearSetGroupList.ColDescriptionEn.SQL(TDPanelRefYearSetGroupList.Cols, Name, isLastOne: true)}",
                            $"FROM{Environment.NewLine}",
                            $"    $SchemaName.$Name(@eligibleRefYearSetToPanelMappingIds) AS {Name}{Environment.NewLine}",
                            $"ORDER BY{Environment.NewLine}",
                            $"    {Name}.panel_refyearset_group;{Environment.NewLine}"
                            );

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Execute stored procedure fn_get_eligible_panel_refyearset_groups
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="eligibleRefYearSetToPanelMappingIds">
                    /// <param name="transaction">Transaction</param>
                    /// List of identifiers of the combination of panel and reference year set,
                    /// (primary key from table sdesign.cm_refyearset2panel_mapping)</param>
                    /// <returns>Data table with aggregated sets of panels and corresponding reference year sets</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        List<Nullable<int>> eligibleRefYearSetToPanelMappingIds,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = SQL;

                        CommandText = CommandText.Replace(
                           oldValue: "@eligibleRefYearSetToPanelMappingIds",
                           newValue: Functions.PrepNIntArrayArg(
                               args: eligibleRefYearSetToPanelMappingIds,
                               dbType: "int4"));

                        NpgsqlParameter pEligibleRefYearSetToPanelMappingIds = new(
                            parameterName: "eligibleRefYearSetToPanelMappingIds",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        if (eligibleRefYearSetToPanelMappingIds != null)
                        {
                            pEligibleRefYearSetToPanelMappingIds.Value
                                = eligibleRefYearSetToPanelMappingIds.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pEligibleRefYearSetToPanelMappingIds.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteQuery(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pEligibleRefYearSetToPanelMappingIds);
                    }

                    /// <summary>
                    /// Execute stored procedure fn_get_eligible_panel_refyearset_groups
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="eligibleRefYearSetToPanelMappingIds">List of identifiers of the combination of panel and reference year set,
                    /// (primary key from table sdesign.cm_refyearset2panel_mapping)</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>List of aggregated sets of panels and corresponding reference year sets</returns>
                    public static TDPanelRefYearSetGroupList Execute(
                        NfiEstaDB database,
                        List<Nullable<int>> eligibleRefYearSetToPanelMappingIds,
                        NpgsqlTransaction transaction = null)
                    {
                        return
                            new TDPanelRefYearSetGroupList(
                                database: database,
                                data: ExecuteQuery(
                                    database: database,
                                    eligibleRefYearSetToPanelMappingIds: eligibleRefYearSetToPanelMappingIds,
                                    transaction: transaction))
                            { StoredProcedure = CommandText };
                    }

                    #endregion Methods

                }

                #endregion FnGetEligiblePanelRefYearSetGroups

            }

        }
    }
}