﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {

                #region FnCheckClassificationRuleSyntax

                /// <summary>
                /// Wrapper for stored procedure fn_check_classification_rule_syntax.
                /// Function checks syntax in text field with classification rule.
                /// </summary>
                public static class FnCheckClassificationRuleSyntax
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_check_classification_rule_syntax";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_ldsity_object integer, ",
                            $"_rule text; ",
                            $"returns: boolean");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    $SchemaName.$Name({Environment.NewLine}",
                            $"@ldsityObjectId,      -- ldsityObjectId{Environment.NewLine}",
                            $"@rule                 -- rule{Environment.NewLine}",
                            $")::boolean AS {Name};{Environment.NewLine}");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Execute stored procedure fn_check_classification_rule_syntax
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="ldsityObjectId">Local density object identifier</param>
                    /// <param name="rule">Classification rule</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>DataTable with boolean column: true/false -
                    /// Syntax in text field with classification rule is correct</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> ldsityObjectId,
                        string rule,
                        NpgsqlTransaction transaction = null)
                    {
                        Nullable<bool> val = Execute(
                            database: database,
                            ldsityObjectId: ldsityObjectId,
                            rule: rule,
                            transaction: transaction);

                        DataTable dt = new();

                        dt.Columns.Add(column: new DataColumn()
                        {
                            ColumnName = Name,
                            DataType = Type.GetType(typeName: "System.Boolean")
                        });

                        DataRow row = dt.NewRow();

                        Functions.SetNBoolArg(
                            row: row,
                            name: Name,
                            val: val);

                        dt.Rows.Add(row: row);

                        return dt;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_check_classification_rule_syntax
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="ldsityObjectId">Local density object identifier</param>
                    /// <param name="rule">Classification rule</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>true/false - Syntax in text field with classification rule is correct</returns>
                    public static Nullable<bool> Execute(
                        NfiEstaDB database,
                        Nullable<int> ldsityObjectId,
                        string rule,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = SQL;

                        CommandText = CommandText.Replace(
                            oldValue: "@ldsityObjectId",
                            newValue: Functions.PrepNIntArg(arg: ldsityObjectId));

                        CommandText = CommandText.Replace(
                            oldValue: "@rule",
                            newValue: Functions.PrepStringArg(arg: rule));

                        NpgsqlParameter pLDsityObjectId = new(
                            parameterName: "ldsityObjectId",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pRule = new(
                            parameterName: "rule",
                            parameterType: NpgsqlDbType.Text);

                        if (ldsityObjectId != null)
                        {
                            pLDsityObjectId.Value = (int)ldsityObjectId;
                        }
                        else
                        {
                            pLDsityObjectId.Value = DBNull.Value;
                        }

                        if (rule != null)
                        {
                            pRule.Value = (string)rule;
                        }
                        else
                        {
                            pRule.Value = DBNull.Value;
                        }

                        string strResult = database.Postgres.ExecuteScalar(
                            sqlCommand: CommandText,
                            transaction: transaction);

                        if (String.IsNullOrEmpty(value: strResult))
                        {
                            return null;
                        }
                        else
                        {
                            if (Boolean.TryParse(value: strResult, result: out bool boolResult))
                            {
                                return boolResult;
                            }
                            else
                            {
                                return null;
                            }
                        }
                    }

                    #endregion Methods

                }

                #endregion FnCheckClassificationRuleSyntax

            }

        }
    }
}