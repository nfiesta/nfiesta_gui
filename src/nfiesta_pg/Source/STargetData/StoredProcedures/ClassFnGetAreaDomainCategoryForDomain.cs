﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {

                #region FnGetAreaDomainCategoryForDomain

                /// <summary>
                /// Wrapper for stored procedure fn_get_area_domain_category4domain.
                /// Function returns records from c_area_domain_category table,
                /// optionally for given area domain.
                /// </summary>
                public static class FnGetAreaDomainCategoryForDomain
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_get_area_domain_category4domain";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_area_domain integer DEFAULT NULL::integer; ",
                            $"returns: ",
                            $"TABLE(",
                            $"id integer, ",
                            $"area_domain integer, ",
                            $"label character varying, ",
                            $"description text, ",
                            $"label_en character varying, ",
                            $"description_en text)");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    {TDAreaDomainCategoryList.ColId.SQL(TDAreaDomainCategoryList.Cols, Name)}",
                            $"    {TDAreaDomainCategoryList.ColLabelCs.SQL(TDAreaDomainCategoryList.Cols, Name)}",
                            $"    {TDAreaDomainCategoryList.ColDescriptionCs.SQL(TDAreaDomainCategoryList.Cols, Name)}",
                            $"    {TDAreaDomainCategoryList.ColAreaDomainId.SQL(TDAreaDomainCategoryList.Cols, Name)}",
                            $"    {TDAreaDomainCategoryList.ColLabelEn.SQL(TDAreaDomainCategoryList.Cols, Name)}",
                            $"    {TDAreaDomainCategoryList.ColDescriptionEn.SQL(TDAreaDomainCategoryList.Cols, Name)}",
                            $"    {TDAreaDomainCategoryList.ColTargetVariableId.SQL(TDAreaDomainCategoryList.Cols, Name, setToNull: true)}",
                            $"    {TDAreaDomainCategoryList.ColLDsityId.SQL(TDAreaDomainCategoryList.Cols, Name, setToNull: true)}",
                            $"    {TDAreaDomainCategoryList.ColLDsityObjectId.SQL(TDAreaDomainCategoryList.Cols, Name, setToNull: true)}",
                            $"    {TDAreaDomainCategoryList.ColLDsityObjectLabelCs.SQL(TDAreaDomainCategoryList.Cols, Name, setToNull: true)}",
                            $"    {TDAreaDomainCategoryList.ColLDsityObjectDescriptionCs.SQL(TDAreaDomainCategoryList.Cols, Name, setToNull: true)}",
                            $"    {TDAreaDomainCategoryList.ColLDsityObjectLabelEn.SQL(TDAreaDomainCategoryList.Cols, Name, setToNull: true)}",
                            $"    {TDAreaDomainCategoryList.ColLDsityObjectDescriptionEn.SQL(TDAreaDomainCategoryList.Cols, Name, setToNull: true)}",
                            $"    {TDAreaDomainCategoryList.ColUseNegative.SQL(TDAreaDomainCategoryList.Cols, Name, setToNull: true)}",
                            $"    {TDAreaDomainCategoryList.ColRestriction.SQL(TDAreaDomainCategoryList.Cols, Name, isLastOne: true, setToNull: true)}",
                            $"FROM{Environment.NewLine}",
                            $"    $SchemaName.$Name(@areaDomainId) AS {Name}{Environment.NewLine}",
                            $"ORDER BY{Environment.NewLine}",
                            $"    {Name}.{TDAreaDomainCategoryList.ColId.DbName};{Environment.NewLine}"
                            );

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Execute stored procedure fn_get_area_domain_category4domain
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="areaDomainId">Area domain identifier</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table with area domain categories</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> areaDomainId = null,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = SQL;

                        CommandText = CommandText.Replace(
                            oldValue: "@areaDomainId",
                            newValue: Functions.PrepNIntArg(arg: areaDomainId));

                        NpgsqlParameter pAreaDomainId = new(
                            parameterName: "areaDomainId",
                            parameterType: NpgsqlDbType.Integer);

                        if (areaDomainId != null)
                        {
                            pAreaDomainId.Value = (int)areaDomainId;
                        }
                        else
                        {
                            pAreaDomainId.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteQuery(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pAreaDomainId);
                    }

                    /// <summary>
                    /// Execute stored procedure fn_get_area_domain_category4domain
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="areaDomainId">Area domain identifier</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>List of area domain categories</returns>
                    public static TDAreaDomainCategoryList Execute(
                        NfiEstaDB database,
                        Nullable<int> areaDomainId = null,
                        NpgsqlTransaction transaction = null)
                    {
                        return new TDAreaDomainCategoryList(
                            database: database,
                            data: ExecuteQuery(
                                database: database,
                                areaDomainId: areaDomainId,
                                transaction: transaction))
                        { StoredProcedure = CommandText };
                    }

                    #endregion Methods

                }

                #endregion FnGetAreaDomainCategoryForDomain

            }

        }
    }
}