﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {

                #region FnSaveCategorizationSetup

                /// <summary>
                /// Wrapper for stored procedure fn_save_categorization_setup.
                /// The function sets attribute configurations.
                /// </summary>
                public static class FnSaveCategorizationSetup
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_save_categorization_setup";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_target_variable integer, ",
                            $"_target_variable_cm integer[], ",
                            $"_area_domain integer[], ",
                            $"_area_domain_object integer[], ",
                            $"_sub_population integer[], ",
                            $"_sub_population_object integer[]; ",
                            $"returns: void");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                                $"    $SchemaName.$Name(",
                                $"@targetVariableId, ",
                                $"@cmTargetVariableIds, ",
                                $"@areaDomainIds, ",
                                $"@areaDomainObjectIds, ",
                                $"@subPopulationIds, ",
                                $"@subPopulationObjectIds);");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Execute stored procedure fn_save_categorization_setup
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="targetVariableId">Target variable identifier</param>
                    /// <param name="cmTargetVariableIds">List of identifiers from mapping between local density and target variable</param>
                    /// <param name="areaDomainIds">List of area domain identifiers</param>
                    /// <param name="areaDomainObjectIds">List of list of local density object identifiers for area domains</param>
                    /// <param name="subPopulationIds">List of subpopulation identifiers</param>
                    /// <param name="subPopulationObjectIds">List of list local density object identifiers for subpopulations</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Empty data table</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> targetVariableId,
                        List<Nullable<int>> cmTargetVariableIds,
                        List<Nullable<int>> areaDomainIds = null,
                        List<List<Nullable<int>>> areaDomainObjectIds = null,
                        List<Nullable<int>> subPopulationIds = null,
                        List<List<Nullable<int>>> subPopulationObjectIds = null,
                        NpgsqlTransaction transaction = null)
                    {
                        Execute(
                            database: database,
                            targetVariableId: targetVariableId,
                            cmTargetVariableIds: cmTargetVariableIds,
                            areaDomainIds: areaDomainIds,
                            areaDomainObjectIds: areaDomainObjectIds,
                            subPopulationIds: subPopulationIds,
                            subPopulationObjectIds: subPopulationObjectIds,
                            transaction: transaction);

                        DataTable dt = new();

                        dt.Columns.Add(column: new DataColumn()
                        {
                            ColumnName = Name,
                            DataType = Type.GetType(typeName: "System.Int32")
                        });

                        return dt;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_save_categorization_setup
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="targetVariableId">Target variable identifier</param>
                    /// <param name="cmTargetVariableIds">List of identifiers from mapping between local density and target variable</param>
                    /// <param name="areaDomainIds">List of area domain identifiers</param>
                    /// <param name="areaDomainObjectIds">List of list of local density object identifiers for area domains</param>
                    /// <param name="subPopulationIds">List of subpopulation identifiers</param>
                    /// <param name="subPopulationObjectIds">List of list local density object identifiers for subpopulations</param>
                    /// <param name="transaction">Transaction</param>
                    public static void Execute(
                        NfiEstaDB database,
                        Nullable<int> targetVariableId,
                        List<Nullable<int>> cmTargetVariableIds,
                        List<Nullable<int>> areaDomainIds = null,
                        List<List<Nullable<int>>> areaDomainObjectIds = null,
                        List<Nullable<int>> subPopulationIds = null,
                        List<List<Nullable<int>>> subPopulationObjectIds = null,
                        NpgsqlTransaction transaction = null)
                    {
                        string sql = SQL;

                        sql = sql.Replace(
                            oldValue: "@areaDomainObjectIds",
                            newValue: Functions.PrepNIntArrayOfArrayArg(args: areaDomainObjectIds, dbType: "int4"));

                        sql = sql.Replace(
                            oldValue: "@subPopulationObjectIds",
                            newValue: Functions.PrepNIntArrayOfArrayArg(args: subPopulationObjectIds, dbType: "int4"));

                        CommandText = sql;

                        CommandText = CommandText.Replace(
                            oldValue: "@targetVariableId",
                            newValue: Functions.PrepNIntArg(arg: targetVariableId));

                        CommandText = CommandText.Replace(
                            oldValue: "@cmTargetVariableIds",
                            newValue: Functions.PrepNIntArrayArg(args: cmTargetVariableIds, dbType: "int4"));

                        CommandText = CommandText.Replace(
                            oldValue: "@areaDomainIds",
                            newValue: Functions.PrepNIntArrayArg(args: areaDomainIds, dbType: "int4"));

                        CommandText = CommandText.Replace(
                            oldValue: "@subPopulationIds",
                            newValue: Functions.PrepNIntArrayArg(args: subPopulationIds, dbType: "int4"));

                        NpgsqlParameter pTargetVariableId = new(
                            parameterName: "targetVariableId",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pCmTargetVariableIds = new(
                            parameterName: "cmTargetVariableIds",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        NpgsqlParameter pAreaDomainIds = new(
                            parameterName: "areaDomainIds",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        NpgsqlParameter pSubPopulationIds = new(
                            parameterName: "subPopulationIds",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        if (targetVariableId != null)
                        {
                            pTargetVariableId.Value = (int)targetVariableId;
                        }
                        else
                        {
                            pTargetVariableId.Value = DBNull.Value;
                        }

                        if (cmTargetVariableIds != null)
                        {
                            pCmTargetVariableIds.Value = cmTargetVariableIds.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pCmTargetVariableIds.Value = DBNull.Value;
                        }

                        if (areaDomainIds != null)
                        {
                            pAreaDomainIds.Value = areaDomainIds.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pAreaDomainIds.Value = DBNull.Value;
                        }

                        if (subPopulationIds != null)
                        {
                            pSubPopulationIds.Value = subPopulationIds.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pSubPopulationIds.Value = DBNull.Value;
                        }

                        database.Postgres.ExecuteNonQuery(
                            sqlCommand: sql,
                            transaction: transaction,
                            pTargetVariableId, pCmTargetVariableIds,
                            pAreaDomainIds, pSubPopulationIds);
                    }

                    #endregion Methods

                }

                #endregion FnSaveCategorizationSetup

            }
        }
    }
}