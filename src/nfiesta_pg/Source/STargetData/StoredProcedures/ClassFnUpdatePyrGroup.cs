﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {
                // fn_update_pyrgroup

                #region FnUpdatePyrGroup

                /// <summary>
                /// Wrapper for stored procedure fn_update_pyrgroup.
                /// Function provides update in c_panel_refyearset_group table.
                /// </summary>
                public static class FnUpdatePyrGroup
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_update_pyrgroup";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_label character varying, ",
                            $"_description text, ",
                            $"_label_en character varying, ",
                            $"_description_en text, ",
                            $"_id integer; ",
                            $"returns: void");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    $SchemaName.$Name(",
                            $"@labelCs, ",
                            $"@descriptionCs, ",
                            $"@labelEn, ",
                            $"@descriptionEn, ",
                            $"@id);");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="labelCs">Label of aggregated sets of panels and corresponding reference year sets in national language</param>
                    /// <param name="descriptionCs">Description of aggregated sets of panels and corresponding reference year sets in national language</param>
                    /// <param name="labelEn">Label of aggregated sets of panels and corresponding reference year sets in English</param>
                    /// <param name="descriptionEn">Description of aggregated sets of panels and corresponding reference year sets in English</param>
                    /// <param name="id">Identifier of aggregated sets of panels and corresponding reference year sets
                    /// (primary key from table c_panel_refyearset_group)</param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        string labelCs,
                        string descriptionCs,
                        string labelEn,
                        string descriptionEn,
                        Nullable<int> id)
                    {
                        string result = SQL;

                        result = result.Replace(
                            oldValue: "@labelCs",
                            newValue: Functions.PrepStringArg(arg: labelCs));

                        result = result.Replace(
                            oldValue: "@descriptionCs",
                            newValue: Functions.PrepStringArg(arg: descriptionCs));

                        result = result.Replace(
                            oldValue: "@labelEn",
                            newValue: Functions.PrepStringArg(arg: labelEn));

                        result = result.Replace(
                            oldValue: "@descriptionEn",
                            newValue: Functions.PrepStringArg(arg: descriptionEn));

                        result = result.Replace(
                            oldValue: "@id",
                            newValue: Functions.PrepNIntArg(arg: id));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_update_pyrgroup
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="labelCs">Label of aggregated sets of panels and corresponding reference year sets in national language</param>
                    /// <param name="descriptionCs">Description of aggregated sets of panels and corresponding reference year sets in national language</param>
                    /// <param name="labelEn">Label of aggregated sets of panels and corresponding reference year sets in English</param>
                    /// <param name="descriptionEn">Description of aggregated sets of panels and corresponding reference year sets in English</param>
                    /// <param name="id">Identifier of aggregated sets of panels and corresponding reference year sets
                    /// (primary key from table c_panel_refyearset_group)</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Empty data table</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        string labelCs,
                        string descriptionCs,
                        string labelEn,
                        string descriptionEn,
                        Nullable<int> id,
                        NpgsqlTransaction transaction = null)
                    {
                        Execute(
                            database: database,
                            labelCs: labelCs,
                            descriptionCs: descriptionCs,
                            labelEn: labelEn,
                            descriptionEn: descriptionEn,
                            id: id,
                            transaction: transaction);

                        DataTable dt = new();

                        dt.Columns.Add(column: new DataColumn()
                        {
                            ColumnName = Name,
                            DataType = Type.GetType(typeName: "System.Int32")
                        });

                        return dt;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_update_pyrgroup
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="labelCs">Label of aggregated sets of panels and corresponding reference year sets in national language</param>
                    /// <param name="descriptionCs">Description of aggregated sets of panels and corresponding reference year sets in national language</param>
                    /// <param name="labelEn">Label of aggregated sets of panels and corresponding reference year sets in English</param>
                    /// <param name="descriptionEn">Description of aggregated sets of panels and corresponding reference year sets in English</param>
                    /// <param name="id">Identifier of aggregated sets of panels and corresponding reference year sets
                    /// (primary key from table c_panel_refyearset_group)</param>
                    /// <param name="transaction">Transaction</param>
                    public static void Execute(
                        NfiEstaDB database,
                        string labelCs,
                        string descriptionCs,
                        string labelEn,
                        string descriptionEn,
                        Nullable<int> id,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(
                            labelCs: labelCs,
                            descriptionCs: descriptionCs,
                            labelEn: labelEn,
                            descriptionEn: descriptionEn,
                            id: id);

                        NpgsqlParameter pLabelCs = new(
                            parameterName: "labelCs",
                            parameterType: NpgsqlDbType.Varchar);

                        NpgsqlParameter pDescriptionCs = new(
                            parameterName: "descriptionCs",
                            parameterType: NpgsqlDbType.Text);

                        NpgsqlParameter pLabelEn = new(
                            parameterName: "labelEn",
                            parameterType: NpgsqlDbType.Varchar);

                        NpgsqlParameter pDescriptionEn = new(
                            parameterName: "descriptionEn",
                            parameterType: NpgsqlDbType.Text);

                        NpgsqlParameter pId = new(
                            parameterName: "id",
                            parameterType: NpgsqlDbType.Integer);

                        if (labelCs != null)
                        {
                            pLabelCs.Value = (string)labelCs;
                        }
                        else
                        {
                            pLabelCs.Value = DBNull.Value;
                        }

                        if (descriptionCs != null)
                        {
                            pDescriptionCs.Value = (string)descriptionCs;
                        }
                        else
                        {
                            pDescriptionCs.Value = DBNull.Value;
                        }

                        if (labelEn != null)
                        {
                            pLabelEn.Value = (string)labelEn;
                        }
                        else
                        {
                            pLabelEn.Value = DBNull.Value;
                        }

                        if (descriptionEn != null)
                        {
                            pDescriptionEn.Value = (string)descriptionEn;
                        }
                        else
                        {
                            pDescriptionEn.Value = DBNull.Value;
                        }

                        if (id != null)
                        {
                            pId.Value = (int)id;
                        }
                        else
                        {
                            pId.Value = DBNull.Value;
                        }

                        database.Postgres.ExecuteNonQuery(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pLabelCs, pDescriptionCs, pLabelEn, pDescriptionEn,
                            pId);
                    }

                    #endregion Methods

                }

                #endregion FnUpdatePyrGroup

            }

        }
    }
}