﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {

                #region FnSaveChangeTargetVariable

                /// <summary>
                /// Wrapper for stored procedure fn_save_change_target_variable.
                /// Functions inserts records into tables
                /// c_target_variable and cm_ldsity2target_variable
                /// for given parameters.
                /// </summary>
                public static class FnSaveChangeTargetVariable
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_save_change_target_variable";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_label character varying, ",
                            $"_description text, ",
                            $"_label_en character varying DEFAULT NULL::character varying, ",
                            $"_description_en text DEFAULT NULL::text, ",
                            $"_ldsity integer[] DEFAULT NULL::integer[], ",
                            $"_ldsity_object_type integer[] DEFAULT NULL::integer[], ",
                            $"_version integer[] DEFAULT NULL::integer[], ",
                            $"_ldsity_negat integer[] DEFAULT NULL::integer[], ",
                            $"_ldsity_object_type_negat integer[] DEFAULT NULL::integer[], ",
                            $"_version_negat integer[] DEFAULT NULL::integer[], ",
                            $"_id integer DEFAULT NULL::integer; ",
                            $"returns: integer[]");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    array_to_string($SchemaName.$Name(",
                            $"@labelCs, ",
                            $"@descriptionCs, ",
                            $"@labelEn, ",
                            $"@descriptionEn, ",
                            $"@ldsityIds, ",
                            $"@ldsityObjectTypeIds, ",
                            $"@versionIds, ",
                            $"@ldsityNegatIds, ",
                            $"@ldsityObjectTypeNegatIds, ",
                            $"@versionNegatIds, ",
                            $"@targetVariableId),';','NULL')::text AS $Name;{Environment.NewLine}");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Execute stored procedure fn_save_change_target_variable
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="labelCs">Target variable label in national language</param>
                    /// <param name="descriptionCs">Target variable description in national language</param>
                    /// <param name="labelEn">Target variable label in English</param>
                    /// <param name="descriptionEn">Target variable description in English</param>
                    /// <param name="ldsityIds">List of local density contributions</param>
                    /// <param name="ldsityObjectTypeIds">List of local density object types</param>
                    /// <param name="versionIds">List of local density versions</param>
                    /// <param name="ldsityNegatIds">List of negative local density contributions</param>
                    /// <param name="ldsityObjectTypeNegatIds">List of negative local density object types</param>
                    /// <param name="versionNegatIds">List of negative local density versions</param>
                    /// <param name="targetVariableId">Target variable identifier</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table with target variable identifiers</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        string labelCs,
                        string descriptionCs,
                        string labelEn,
                        string descriptionEn,
                        List<Nullable<int>> ldsityIds = null,
                        List<Nullable<int>> ldsityObjectTypeIds = null,
                        List<Nullable<int>> versionIds = null,
                        List<Nullable<int>> ldsityNegatIds = null,
                        List<Nullable<int>> ldsityObjectTypeNegatIds = null,
                        List<Nullable<int>> versionNegatIds = null,
                        Nullable<int> targetVariableId = null,
                        NpgsqlTransaction transaction = null)
                    {
                        List<Nullable<int>> list = Execute(
                            database: database,
                            labelCs: labelCs,
                            descriptionCs: descriptionCs,
                            labelEn: labelEn,
                            descriptionEn: descriptionEn,
                            ldsityIds: ldsityIds,
                            ldsityObjectTypeIds: ldsityObjectTypeIds,
                            versionIds: versionIds,
                            ldsityNegatIds: ldsityNegatIds,
                            ldsityObjectTypeNegatIds: ldsityObjectTypeNegatIds,
                            versionNegatIds: versionNegatIds,
                            targetVariableId: targetVariableId,
                            transaction: transaction);

                        DataTable dt = new();

                        dt.Columns.Add(column: new DataColumn()
                        {
                            ColumnName = Name,
                            DataType = Type.GetType(typeName: "System.Int32")
                        });

                        if (list == null)
                        {
                            return dt;
                        }

                        foreach (Nullable<int> val in list)
                        {
                            DataRow row = dt.NewRow();

                            Functions.SetNIntArg(
                                row: row,
                                name: Name,
                                val: val);

                            dt.Rows.Add(row: row);
                        }

                        return dt;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_save_change_target_variable
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="labelCs">Target variable label in national language</param>
                    /// <param name="descriptionCs">Target variable description in national language</param>
                    /// <param name="labelEn">Target variable label in English</param>
                    /// <param name="descriptionEn">Target variable description in English</param>
                    /// <param name="ldsityIds">List of local density contributions</param>
                    /// <param name="ldsityObjectTypeIds">List of local density object types</param>
                    /// <param name="versionIds">List of local density versions</param>
                    /// <param name="ldsityNegatIds">List of negative local density contributions</param>
                    /// <param name="ldsityObjectTypeNegatIds">List of negative local density object types</param>
                    /// <param name="versionNegatIds">List of negative local density versions</param>
                    /// <param name="targetVariableId">Target variable identifier</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>List of target variable identifiers</returns>
                    public static List<Nullable<int>> Execute(
                        NfiEstaDB database,
                        string labelCs,
                        string descriptionCs,
                        string labelEn,
                        string descriptionEn,
                        List<Nullable<int>> ldsityIds = null,
                        List<Nullable<int>> ldsityObjectTypeIds = null,
                        List<Nullable<int>> versionIds = null,
                        List<Nullable<int>> ldsityNegatIds = null,
                        List<Nullable<int>> ldsityObjectTypeNegatIds = null,
                        List<Nullable<int>> versionNegatIds = null,
                        Nullable<int> targetVariableId = null,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = SQL;

                        CommandText = CommandText.Replace(
                            oldValue: "@labelCs",
                            newValue: Functions.PrepStringArg(arg: labelCs));

                        CommandText = CommandText.Replace(
                            oldValue: "@descriptionCs",
                            newValue: Functions.PrepStringArg(arg: descriptionCs));

                        CommandText = CommandText.Replace(
                            oldValue: "@labelEn",
                            newValue: Functions.PrepStringArg(arg: labelEn));

                        CommandText = CommandText.Replace(
                            oldValue: "@descriptionEn",
                            newValue: Functions.PrepStringArg(arg: descriptionEn));

                        CommandText = CommandText.Replace(
                            oldValue: "@ldsityIds",
                            newValue: Functions.PrepNIntArrayArg(args: ldsityIds, dbType: "int4"));

                        CommandText = CommandText.Replace(
                            oldValue: "@ldsityObjectTypeIds",
                            newValue: Functions.PrepNIntArrayArg(args: ldsityObjectTypeIds, dbType: "int4"));

                        CommandText = CommandText.Replace(
                            oldValue: "@versionIds",
                            newValue: Functions.PrepNIntArrayArg(args: versionIds, dbType: "int4"));

                        CommandText = CommandText.Replace(
                            oldValue: "@ldsityNegatIds",
                            newValue: Functions.PrepNIntArrayArg(args: ldsityNegatIds, dbType: "int4"));

                        CommandText = CommandText.Replace(
                            oldValue: "@ldsityObjectTypeNegatIds",
                            newValue: Functions.PrepNIntArrayArg(args: ldsityObjectTypeNegatIds, dbType: "int4"));

                        CommandText = CommandText.Replace(
                            oldValue: "@versionNegatIds",
                            newValue: Functions.PrepNIntArrayArg(args: versionNegatIds, dbType: "int4"));

                        CommandText = CommandText.Replace(
                            oldValue: "@targetVariableId",
                            newValue: Functions.PrepNIntArg(arg: targetVariableId));

                        NpgsqlParameter pLabelCs = new(
                            parameterName: "labelCs",
                            parameterType: NpgsqlDbType.Varchar);

                        NpgsqlParameter pDescriptionCs = new(
                            parameterName: "descriptionCs",
                            parameterType: NpgsqlDbType.Text);

                        NpgsqlParameter pLabelEn = new(
                            parameterName: "labelEn",
                            parameterType: NpgsqlDbType.Varchar);

                        NpgsqlParameter pDescriptionEn = new(
                            parameterName: "descriptionEn",
                            parameterType: NpgsqlDbType.Text);

                        NpgsqlParameter pLDsityIds = new(
                            parameterName: "ldsityIds",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        NpgsqlParameter pLDsityObjectTypeIds = new(
                            parameterName: "ldsityObjectTypeIds",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        NpgsqlParameter pVersionIds = new(
                            parameterName: "versionIds",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        NpgsqlParameter pLDsityNegatIds = new(
                            parameterName: "ldsityNegatIds",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        NpgsqlParameter pLDsityObjectTypeNegatIds = new(
                            parameterName: "ldsityObjectTypeNegatIds",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        NpgsqlParameter pVersionNegatIds = new(
                            parameterName: "versionNegatIds",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        NpgsqlParameter pTargetVariableId = new(
                            parameterName: "targetVariableId",
                            parameterType: NpgsqlDbType.Integer);

                        if (labelCs != null)
                        {
                            pLabelCs.Value = (string)labelCs;
                        }
                        else
                        {
                            pLabelCs.Value = DBNull.Value;
                        }

                        if (descriptionCs != null)
                        {
                            pDescriptionCs.Value = (string)descriptionCs;
                        }
                        else
                        {
                            pDescriptionCs.Value = DBNull.Value;
                        }

                        if (labelEn != null)
                        {
                            pLabelEn.Value = (string)labelEn;
                        }
                        else
                        {
                            pLabelEn.Value = DBNull.Value;
                        }

                        if (descriptionEn != null)
                        {
                            pDescriptionEn.Value = (string)descriptionEn;
                        }
                        else
                        {
                            pDescriptionEn.Value = DBNull.Value;
                        }

                        if (ldsityIds != null)
                        {
                            pLDsityIds.Value = ldsityIds.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pLDsityIds.Value = DBNull.Value;
                        }

                        if (ldsityObjectTypeIds != null)
                        {
                            pLDsityObjectTypeIds.Value = ldsityObjectTypeIds.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pLDsityObjectTypeIds.Value = DBNull.Value;
                        }

                        if (versionIds != null)
                        {
                            pVersionIds.Value = versionIds.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pVersionIds.Value = DBNull.Value;
                        }

                        if (ldsityNegatIds != null)
                        {
                            pLDsityNegatIds.Value = ldsityNegatIds.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pLDsityNegatIds.Value = DBNull.Value;
                        }

                        if (ldsityObjectTypeNegatIds != null)
                        {
                            pLDsityObjectTypeNegatIds.Value = ldsityObjectTypeNegatIds.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pLDsityObjectTypeNegatIds.Value = DBNull.Value;
                        }

                        if (versionNegatIds != null)
                        {
                            pVersionNegatIds.Value = versionNegatIds.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pVersionNegatIds.Value = DBNull.Value;
                        }

                        if (targetVariableId != null)
                        {
                            pTargetVariableId.Value = targetVariableId;
                        }
                        else
                        {
                            pTargetVariableId.Value = DBNull.Value;
                        }

                        string strResult = database.Postgres.ExecuteScalar(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pLabelCs, pDescriptionCs, pLabelEn, pDescriptionEn,
                            pLDsityIds, pLDsityObjectTypeIds, pVersionIds,
                            pLDsityNegatIds, pLDsityObjectTypeNegatIds, pVersionNegatIds,
                            pTargetVariableId);

                        if (String.IsNullOrEmpty(value: strResult))
                        {
                            return null;
                        }
                        else
                        {
                            return
                            Functions.StringToNIntList(
                                text: strResult,
                                separator: (char)59,
                                defaultValue: null);
                        }
                    }

                    #endregion Methods

                }

                #endregion FnSaveChangeTargetVariable

            }

        }
    }
}