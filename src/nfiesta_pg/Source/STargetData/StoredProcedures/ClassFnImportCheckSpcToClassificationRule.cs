﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {

                #region FnImportCheckSpcToClassificationRule

                /// <summary>
                /// Wrapper for stored procedure fn_import_check_spc2classification_rule.
                /// </summary>
                public static class FnImportCheckSpcToClassificationRule
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_import_check_spc2classification_rule";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_spc2classification_rule integer, ",
                            $"_sub_population_category__etl_id integer, ",
                            $"_ldsity_object__etl_id integer, ",
                            $"_classification_rule text; ",
                            $"returns: ",
                            $"TABLE(",
                            $"spc2classification_rule integer, ",
                            $"ldsity_object__id integer, ",
                            $"ldsity_object__label character varying, ",
                            $"ldsity_object__description text, ",
                            $"ldsity_object__label_en character varying, ",
                            $"ldsity_object__description_en text, ",
                            $"sub_population_category__id integer, ",
                            $"sub_population_category__label character varying, ",
                            $"sub_population_category__description text, ",
                            $"sub_population_category__label_en character varying, ",
                            $"sub_population_category__description_en text, ",
                            $"etl_id integer, ",
                            $"classification_rule text)");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                          = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    spc2classification_rule,{Environment.NewLine}",
                            $"    ldsity_object__id,{Environment.NewLine}",
                            $"    ldsity_object__label,{Environment.NewLine}",
                            $"    ldsity_object__description,{Environment.NewLine}",
                            $"    ldsity_object__label_en,{Environment.NewLine}",
                            $"    ldsity_object__description_en,{Environment.NewLine}",
                            $"    sub_population_category__id,{Environment.NewLine}",
                            $"    sub_population_category__label,{Environment.NewLine}",
                            $"    sub_population_category__description,{Environment.NewLine}",
                            $"    sub_population_category__label_en,{Environment.NewLine}",
                            $"    sub_population_category__description_en,{Environment.NewLine}",
                            $"    etl_id,{Environment.NewLine}",
                            $"    classification_rule{Environment.NewLine}",
                            $"FROM{Environment.NewLine}",
                            $"    $SchemaName.$Name(",
                            $"@spcToClassificationRule, ",
                            $"@subPopulationCategoryEtlId, ",
                            $"@ldsityObjectEtlId, ",
                            $"@classificationRule);{Environment.NewLine}");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Execute stored procedure fn_import_check_spc2classification_rule
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="spcToClassificationRule"></param>
                    /// <param name="subPopulationCategoryEtlId"></param>
                    /// <param name="ldsityObjectEtlId"></param>
                    /// <param name="classificationRule"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> spcToClassificationRule,
                        Nullable<int> subPopulationCategoryEtlId,
                        Nullable<int> ldsityObjectEtlId,
                        string classificationRule,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = SQL;

                        CommandText = CommandText.Replace(
                            oldValue: "@spcToClassificationRule",
                            newValue: Functions.PrepNIntArg(arg: spcToClassificationRule));

                        CommandText = CommandText.Replace(
                            oldValue: "@subPopulationCategoryEtlId",
                            newValue: Functions.PrepNIntArg(arg: subPopulationCategoryEtlId));

                        CommandText = CommandText.Replace(
                            oldValue: "@ldsityObjectEtlId",
                            newValue: Functions.PrepNIntArg(arg: ldsityObjectEtlId));

                        CommandText = CommandText.Replace(
                            oldValue: "@classificationRule",
                            newValue: Functions.PrepStringArg(arg: classificationRule));

                        NpgsqlParameter pSpcToClassificationRule = new(
                            parameterName: "spcToClassificationRule",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pSubPopulationCategoryEtlId = new(
                            parameterName: "subPopulationCategoryEtlId",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pLDsityObjectEtlId = new(
                            parameterName: "ldsityObjectEtlId",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pClassificationRule = new(
                            parameterName: "classificationRule",
                            parameterType: NpgsqlDbType.Text);

                        if (spcToClassificationRule != null)
                        {
                            pSpcToClassificationRule.Value = (int)spcToClassificationRule;
                        }
                        else
                        {
                            pSpcToClassificationRule.Value = DBNull.Value;
                        }

                        if (subPopulationCategoryEtlId != null)
                        {
                            pSubPopulationCategoryEtlId.Value = (int)subPopulationCategoryEtlId;
                        }
                        else
                        {
                            pSubPopulationCategoryEtlId.Value = DBNull.Value;
                        }

                        if (ldsityObjectEtlId != null)
                        {
                            pLDsityObjectEtlId.Value = (int)ldsityObjectEtlId;
                        }
                        else
                        {
                            pLDsityObjectEtlId.Value = DBNull.Value;
                        }

                        if (classificationRule != null)
                        {
                            pClassificationRule.Value = (string)classificationRule;
                        }
                        else
                        {
                            pClassificationRule.Value = DBNull.Value;
                        }

                        return
                            database.Postgres.ExecuteQuery(
                                sqlCommand: SQL,
                                transaction: transaction,
                                pSpcToClassificationRule, pSubPopulationCategoryEtlId,
                                pLDsityObjectEtlId, pClassificationRule);
                    }

                    #endregion Methods

                }

                #endregion FnImportCheckSpcToClassificationRule

            }

        }
    }
}