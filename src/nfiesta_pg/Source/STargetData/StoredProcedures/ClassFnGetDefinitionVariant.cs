﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {

                #region FnGetDefinitionVariant

                /// <summary>
                /// Wrapper for stored procedure fn_get_definition_variant.
                /// Function returns records from c_definition_variant table, optionally for given id.
                /// </summary>
                public static class FnGetDefinitionVariant
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_get_definition_variant";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_id integer DEFAULT NULL::integer; ",
                            $"returns: ",
                            $"TABLE(",
                            $"id integer, ",
                            $"label character varying, ",
                            $"description text, ",
                            $"label_en character varying, ",
                            $"description_en text)");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    {TDDefinitionVariantList.ColId.SQL(TDDefinitionVariantList.Cols, Name)}",
                            $"    {TDDefinitionVariantList.ColLabelCs.SQL(TDDefinitionVariantList.Cols, Name)}",
                            $"    {TDDefinitionVariantList.ColDescriptionCs.SQL(TDDefinitionVariantList.Cols, Name)}",
                            $"    {TDDefinitionVariantList.ColLabelEn.SQL(TDDefinitionVariantList.Cols, Name)}",
                            $"    {TDDefinitionVariantList.ColDescriptionEn.SQL(TDDefinitionVariantList.Cols, Name)}",
                            $"    {TDDefinitionVariantList.ColLDsityId.SQL(TDDefinitionVariantList.Cols, Name, isLastOne: true, setToNull: true)}",
                            $"FROM{Environment.NewLine}",
                            $"    $SchemaName.$Name(@definitionVariantId) AS {Name}{Environment.NewLine}",
                            $"ORDER BY{Environment.NewLine}",
                            $"    {Name}.{TDDefinitionVariantList.ColId.DbName};{Environment.NewLine}"
                            );

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Execute stored procedure fn_get_definition_variant
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="definitionVariantId">Definition variant identifier</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table with definition variants</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> definitionVariantId = null,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = SQL;

                        CommandText = CommandText.Replace(
                            oldValue: "@definitionVariantId",
                            newValue: Functions.PrepNIntArg(arg: definitionVariantId));

                        NpgsqlParameter pDefinitionVariantId = new(
                            parameterName: "definitionVariantId",
                            parameterType: NpgsqlDbType.Integer);

                        if (definitionVariantId != null)
                        {
                            pDefinitionVariantId.Value = (int)definitionVariantId;
                        }
                        else
                        {
                            pDefinitionVariantId.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteQuery(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pDefinitionVariantId);
                    }

                    /// <summary>
                    /// Execute stored procedure fn_get_definition_variant
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="definitionVariantId">Definition variant identifier</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>List of definition variants</returns>
                    public static TDDefinitionVariantList Execute(
                        NfiEstaDB database,
                        Nullable<int> definitionVariantId = null,
                        NpgsqlTransaction transaction = null)
                    {
                        return new TDDefinitionVariantList(
                            database: database,
                            data: ExecuteQuery(
                                database: database,
                                definitionVariantId: definitionVariantId,
                                transaction: transaction))
                        { StoredProcedure = CommandText };
                    }

                    #endregion Methods

                }

                #endregion FnGetDefinitionVariant

            }

        }
    }
}