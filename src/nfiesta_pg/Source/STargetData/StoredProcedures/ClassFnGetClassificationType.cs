﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {

                #region FnGetClassificationType

                /// <summary>
                /// Wrapper for stored procedure fn_get_classification_type.
                /// Function returns records from c_classification_type table,
                /// optionally for given target_variable.
                /// </summary>
                public static class FnGetClassificationType
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_get_classification_type";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_target_variable integer DEFAULT NULL::integer; ",
                            $"returns: ",
                            $"TABLE(",
                            $"id integer, ",
                            $"target_variable integer, ",
                            $"label character varying, ",
                            $"description text)");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    {TDClassificationTypeList.ColId.SQL(TDClassificationTypeList.Cols, Name)}",
                            $"    {TDClassificationTypeList.ColLabelEn.SQL(TDClassificationTypeList.Cols, Name)}",
                            $"    {TDClassificationTypeList.ColDescriptionEn.SQL(TDClassificationTypeList.Cols, Name)}",
                            $"    {TDClassificationTypeList.ColLabelCs.SQL(TDClassificationTypeList.Cols, Name, setToNull: true)}",
                            $"    {TDClassificationTypeList.ColDescriptionCs.SQL(TDClassificationTypeList.Cols, Name, isLastOne: true, setToNull: true)}",
                            $"FROM{Environment.NewLine}",
                            $"    $SchemaName.$Name(@targetVariableId) AS {Name}{Environment.NewLine}",
                            $"ORDER BY{Environment.NewLine}",
                            $"    {Name}.{TDClassificationTypeList.ColId.DbName};{Environment.NewLine}"
                            );

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Execute stored procedure fn_get_classification_type
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="targetVariableId">Target variable identifier</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table with classification type categories</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> targetVariableId = null,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = SQL;

                        CommandText = CommandText.Replace(
                            oldValue: "@targetVariableId",
                            newValue: Functions.PrepNIntArg(arg: targetVariableId));

                        NpgsqlParameter pTargetVariableId = new(
                            parameterName: "targetVariableId",
                            parameterType: NpgsqlDbType.Integer);

                        if (targetVariableId != null)
                        {
                            pTargetVariableId.Value = (int)targetVariableId;
                        }
                        else
                        {
                            pTargetVariableId.Value = DBNull.Value;
                        }

                        DataTable data = database.Postgres.ExecuteQuery(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pTargetVariableId);

                        TDClassificationTypeList.FixNationalLabels(dt: data);

                        return data;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_get_classification_type
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="targetVariableId">Target variable identifier</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>List of classification type categories</returns>
                    public static TDClassificationTypeList Execute(
                        NfiEstaDB database,
                        Nullable<int> targetVariableId = null,
                        NpgsqlTransaction transaction = null)
                    {
                        return new TDClassificationTypeList(
                            database: database,
                            data: ExecuteQuery(
                                database: database,
                                targetVariableId: targetVariableId,
                                transaction: transaction))
                        { StoredProcedure = CommandText };
                    }

                    #endregion Methods

                }

                #endregion FnGetClassificationType

            }

        }
    }
}