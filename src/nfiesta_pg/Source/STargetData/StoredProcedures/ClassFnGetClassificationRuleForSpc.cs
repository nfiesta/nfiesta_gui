﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {

                #region FnGetClassificationRuleForSpc

                /// <summary>
                /// Wrapper for stored procedure fn_get_classification_rule4spc.
                /// Function returns records from cm_spc2classification_rule table,
                /// optionally for given sub_population_category and/or ldsity object.
                /// </summary>
                public static class FnGetClassificationRuleForSpc
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_get_classification_rule4spc";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_sub_population_category integer DEFAULT NULL::integer, ",
                            $"_ldsity_object integer DEFAULT NULL::integer, ",
                            $"_use_negative boolean DEFAULT NULL::boolean; ",
                            $"returns: ",
                            $"TABLE(",
                            $"id integer, ",
                            $"sub_population_category integer, ",
                            $"ldsity_object integer, ",
                            $"classification_rule text, ",
                            $"use_negative boolean)");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                                $"SELECT{Environment.NewLine}",
                                $"    $Name.{TDSPCToClassificationRuleList.ColId.DbName} ",
                                $"AS {TDSPCToClassificationRuleList.ColId.Name},{Environment.NewLine}",
                                $"    $Name.{TDSPCToClassificationRuleList.ColSubPopulationCategoryId.DbName} ",
                                $"AS {TDSPCToClassificationRuleList.ColSubPopulationCategoryId.Name},{Environment.NewLine}",
                                $"    $Name.{TDSPCToClassificationRuleList.ColLDsityObjectId.DbName} ",
                                $"AS {TDSPCToClassificationRuleList.ColLDsityObjectId.Name},{Environment.NewLine}",
                                $"    $Name.{TDSPCToClassificationRuleList.ColClassificationRule.DbName} ",
                                $"AS {TDSPCToClassificationRuleList.ColClassificationRule.Name},{Environment.NewLine}",
                                $"    $Name.{TDSPCToClassificationRuleList.ColUseNegative.DbName} ",
                                $"AS {TDSPCToClassificationRuleList.ColUseNegative.Name}{Environment.NewLine}",
                                $"FROM{Environment.NewLine}",
                                $"    {SchemaName}.{Name}(",
                                $"@subPopulationCategoryId, ",
                                $"@ldsityObjectId, ",
                                $"@useNegative",
                                $") AS $Name{Environment.NewLine}",
                                $"ORDER BY{Environment.NewLine}",
                                $"    $Name.{TDSPCToClassificationRuleList.ColId.DbName};{Environment.NewLine}"
                                );

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Execute stored procedure fn_get_classification_rule4spc
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="subPopulationCategoryId">Subpopulation category identifier</param>
                    /// <param name="ldsityObjectId">Local density object identifier</param>
                    /// <param name="useNegative">Use negative local density contribution</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table with records from cm_spc2classification_rule</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> subPopulationCategoryId,
                        Nullable<int> ldsityObjectId,
                        Nullable<bool> useNegative,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = SQL;

                        CommandText = CommandText.Replace(
                            oldValue: "@subPopulationCategoryId",
                            newValue: Functions.PrepNIntArg(arg: subPopulationCategoryId));

                        CommandText = CommandText.Replace(
                            oldValue: "@ldsityObjectId",
                            newValue: Functions.PrepNIntArg(arg: ldsityObjectId));

                        CommandText = CommandText.Replace(
                           oldValue: "@useNegative",
                           newValue: Functions.PrepNBoolArg(arg: useNegative));

                        NpgsqlParameter pSubPopulationCategoryId = new(
                            parameterName: "subPopulationCategoryId",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pLDsityObjectId = new(
                            parameterName: "ldsityObjectId",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pUseNegative = new(
                            parameterName: "useNegative",
                            parameterType: NpgsqlDbType.Boolean);

                        if (subPopulationCategoryId != null)
                        {
                            pSubPopulationCategoryId.Value = (int)subPopulationCategoryId;
                        }
                        else
                        {
                            pSubPopulationCategoryId.Value = DBNull.Value;
                        }

                        if (ldsityObjectId != null)
                        {
                            pLDsityObjectId.Value = (int)ldsityObjectId;
                        }
                        else
                        {
                            pLDsityObjectId.Value = DBNull.Value;
                        }

                        if (useNegative != null)
                        {
                            pUseNegative.Value = (bool)useNegative;
                        }
                        else
                        {
                            pUseNegative.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteQuery(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pSubPopulationCategoryId,
                            pLDsityObjectId,
                            pUseNegative);
                    }

                    /// <summary>
                    /// Execute stored procedure fn_get_classification_rule4spc
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="subPopulationCategoryId">Subpopulation category identifier</param>
                    /// <param name="ldsityObjectId">Local density object identifier</param>
                    /// <param name="useNegative">Use negative local density contribution</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>List of records from cm_spc2classification_rule</returns>
                    public static TDSPCToClassificationRuleList Execute(
                        NfiEstaDB database,
                        Nullable<int> subPopulationCategoryId,
                        Nullable<int> ldsityObjectId,
                        Nullable<bool> useNegative,
                        NpgsqlTransaction transaction = null)
                    {
                        return new TDSPCToClassificationRuleList(
                            database: database,
                            data: ExecuteQuery(
                                database: database,
                                subPopulationCategoryId: subPopulationCategoryId,
                                ldsityObjectId: ldsityObjectId,
                                useNegative: useNegative,
                                transaction: transaction))
                        { StoredProcedure = CommandText };
                    }

                    #endregion Methods

                }

                #endregion FnGetClassificationRuleForSpc

            }

        }
    }
}