﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {

                #region FnGetEligiblePanelRefYearSetCombinationsA

                /// <summary>
                /// Wrapper for stored procedure fn_get_eligible_panel_refyearset_combinations.
                /// Function returns combinations of panel and reference-yearsets(ids of cm_refyearset2panel_mapping),
                /// for which local density of the given target variable
                /// and for the given set of its categorizations can be calculated.
                /// </summary>
                public static class FnGetEligiblePanelRefYearSetCombinationsA
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_get_eligible_panel_refyearset_combinations";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_target_variable integer, ",
                            $"_categorization_setup integer[]; ",
                            $"returns: ",
                            $"TABLE(",
                            $"refyearset2panel_mapping integer, ",
                            $"panel_label character varying, ",
                            $"panel_description character varying, ",
                            $"refyearset_label character varying, ",
                            $"refyearset_description character varying)");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    {TDFnGetEligiblePanelRefYearSetCombinationsTypeList.ColId.SQL(TDFnGetEligiblePanelRefYearSetCombinationsTypeList.Cols, Name)}",
                            $"    {TDFnGetEligiblePanelRefYearSetCombinationsTypeList.ColPanelLabelCs.SQL(TDFnGetEligiblePanelRefYearSetCombinationsTypeList.Cols, Name)}",
                            $"    {TDFnGetEligiblePanelRefYearSetCombinationsTypeList.ColPanelDescriptionCs.SQL(TDFnGetEligiblePanelRefYearSetCombinationsTypeList.Cols, Name)}",
                            $"    {TDFnGetEligiblePanelRefYearSetCombinationsTypeList.ColPanelLabelEn.SQL(TDFnGetEligiblePanelRefYearSetCombinationsTypeList.Cols, Name)}",
                            $"    {TDFnGetEligiblePanelRefYearSetCombinationsTypeList.ColPanelDescriptionEn.SQL(TDFnGetEligiblePanelRefYearSetCombinationsTypeList.Cols, Name)}",
                            $"    {TDFnGetEligiblePanelRefYearSetCombinationsTypeList.ColRefYearSetLabelCs.SQL(TDFnGetEligiblePanelRefYearSetCombinationsTypeList.Cols, Name)}",
                            $"    {TDFnGetEligiblePanelRefYearSetCombinationsTypeList.ColRefYearSetDescriptionCs.SQL(TDFnGetEligiblePanelRefYearSetCombinationsTypeList.Cols, Name)}",
                            $"    {TDFnGetEligiblePanelRefYearSetCombinationsTypeList.ColRefYearSetLabelEn.SQL(TDFnGetEligiblePanelRefYearSetCombinationsTypeList.Cols, Name)}",
                            $"    {TDFnGetEligiblePanelRefYearSetCombinationsTypeList.ColRefYearSetDescriptionEn.SQL(TDFnGetEligiblePanelRefYearSetCombinationsTypeList.Cols, Name, isLastOne: true)}",
                            $"FROM{Environment.NewLine}",
                            $"    $SchemaName.$Name(@targetVariableId, @categorizationSetupIds) AS {Name}{Environment.NewLine}",
                            $"ORDER BY{Environment.NewLine}",
                            $"    {Name}.{TDFnGetEligiblePanelRefYearSetCombinationsTypeList.ColId.DbName};{Environment.NewLine}"
                            );

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Execute stored procedure fn_get_eligible_panel_refyearset_combinations
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="targetVariableId">Target variable identifier</param>
                    /// <param name="categorizationSetupIds">List of categorization setup identifiers</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>
                    /// Function returns combinations of panel and reference-yearsets(ids of cm_refyearset2panel_mapping),
                    /// for which local density of the given target variable
                    /// and for the given set of its categorizations can be calculated.
                    /// </returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> targetVariableId,
                        List<Nullable<int>> categorizationSetupIds,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = SQL;

                        CommandText = CommandText.Replace(
                            oldValue: "@targetVariableId",
                            newValue: Functions.PrepNIntArg(arg: targetVariableId));

                        CommandText = CommandText.Replace(
                            oldValue: "@categorizationSetupIds",
                            newValue: Functions.PrepNIntArrayArg(args: categorizationSetupIds, dbType: "int4"));

                        NpgsqlParameter pTargetVariableId = new(
                            parameterName: "targetVariableId",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pCategorizationSetupIds = new(
                            parameterName: "categorizationSetupIds",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        if (targetVariableId != null)
                        {
                            pTargetVariableId.Value = (int)targetVariableId;
                        }
                        else
                        {
                            pTargetVariableId.Value = DBNull.Value;
                        }

                        if (categorizationSetupIds != null)
                        {
                            pCategorizationSetupIds.Value = categorizationSetupIds.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pCategorizationSetupIds.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteQuery(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pTargetVariableId, pCategorizationSetupIds);
                    }

                    /// <summary>
                    /// Execute stored procedure fn_get_eligible_panel_refyearset_combinations
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="targetVariableId">Target variable identifier</param>
                    /// <param name="categorizationSetupIds">List of categorization setup identifiers</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>
                    /// Function returns combinations of panel and reference-yearsets(ids of cm_refyearset2panel_mapping),
                    /// for which local density of the given target variable
                    /// and for the given set of its categorizations can be calculated.
                    /// </returns>
                    public static TDFnGetEligiblePanelRefYearSetCombinationsTypeList Execute(
                        NfiEstaDB database,
                        Nullable<int> targetVariableId,
                        List<Nullable<int>> categorizationSetupIds,
                        NpgsqlTransaction transaction = null)
                    {
                        System.Diagnostics.Stopwatch stopWatch = new();
                        stopWatch.Start();

                        DataTable data = ExecuteQuery(
                                database: database,
                                targetVariableId: targetVariableId,
                                categorizationSetupIds: categorizationSetupIds,
                                transaction: transaction);
                        data.TableName = Name;

                        stopWatch.Stop();

                        return new TDFnGetEligiblePanelRefYearSetCombinationsTypeList(
                            database: database,
                            data: data,
                            loadingTime: 0.001 * stopWatch.ElapsedMilliseconds,
                            storedProcedure: CommandText)
                        {
                            TargetVariableId = targetVariableId,
                            CategorizationSetupIds = categorizationSetupIds
                        };
                    }

                    #endregion Methods

                }

                #endregion FnGetEligiblePanelRefYearSetCombinationsA

            }

        }
    }
}