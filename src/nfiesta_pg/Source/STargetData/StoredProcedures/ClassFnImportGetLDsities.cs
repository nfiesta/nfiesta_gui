﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {

                #region FnImportGetLDsities

                /// <summary>
                /// Wrapper for stored procedure fn_import_get_ldsities.
                /// </summary>
                public static class FnImportGetLDsities
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_import_get_ldsities";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_ldsity_object integer, ",
                            $"_ldsity integer, ",
                            $"_etl_id integer[] DEFAULT NULL::integer[]; ",
                            $"returns: ",
                            $"TABLE(",
                            $"ldsity integer, ",
                            $"id integer, ",
                            $"label character varying, ",
                            $"description text, ",
                            $"label_en character varying, ",
                            $"description_en text, ",
                            $"ldsity_object__id integer, ",
                            $"ldsity_object__label character varying, ",
                            $"ldsity_object__description text, ",
                            $"ldsity_object__label_en character varying, ",
                            $"ldsity_object__description_en text, ",
                            $"column_expression text, ",
                            $"unit_of_measure__id integer, ",
                            $"unit_of_measure__label character varying, ",
                            $"unit_of_measure__description text, ",
                            $"unit_of_measure__label_en character varying, ",
                            $"unit_of_measure__description_en text, ",
                            $"area_domain_category__id integer[], ",
                            $"area_domain_category__classification_rule text[], ",
                            $"sub_population_category__id integer[], ",
                            $"sub_population_category__classification_rule text[], ",
                            $"definition_variant__id integer[], ",
                            $"definition_variant__label character varying[], ",
                            $"definition_variant__description text[], ",
                            $"definition_variant__label_en character varying[], ",
                            $"definition_variant__description_en text[])");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    ldsity,{Environment.NewLine}",
                            $"    id,{Environment.NewLine}",
                            $"    label,{Environment.NewLine}",
                            $"    description,{Environment.NewLine}",
                            $"    label_en,{Environment.NewLine}",
                            $"    description_en,{Environment.NewLine}",
                            $"    ldsity_object__id,{Environment.NewLine}",
                            $"    ldsity_object__label,{Environment.NewLine}",
                            $"    ldsity_object__description,{Environment.NewLine}",
                            $"    ldsity_object__label_en,{Environment.NewLine}",
                            $"    ldsity_object__description_en,{Environment.NewLine}",
                            $"    column_expression,{Environment.NewLine}",
                            $"    unit_of_measure__id,{Environment.NewLine}",
                            $"    unit_of_measure__label,{Environment.NewLine}",
                            $"    unit_of_measure__description,{Environment.NewLine}",
                            $"    unit_of_measure__label_en,{Environment.NewLine}",
                            $"    unit_of_measure__description_en,{Environment.NewLine}",
                            $"    area_domain_category__id,{Environment.NewLine}",
                            $"    area_domain_category__classification_rule,{Environment.NewLine}",
                            $"    sub_population_category__id,{Environment.NewLine}",
                            $"    sub_population_category__classification_rule,{Environment.NewLine}",
                            $"    definition_variant__id,{Environment.NewLine}",
                            $"    definition_variant__label,{Environment.NewLine}",
                            $"    definition_variant__description,{Environment.NewLine}",
                            $"    definition_variant__label_en,{Environment.NewLine}",
                            $"    definition_variant__description_en{Environment.NewLine}",
                            $"FROM{Environment.NewLine}",
                            $"    $SchemaName.$Name(",
                            $"@ldsityObject, ",
                            $"@ldsity, ",
                            $"@etlId);{Environment.NewLine}");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Execute stored procedure fn_import_get_ldsities
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="ldsityObject"></param>
                    /// <param name="ldsity"></param>
                    /// <param name="etlId"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> ldsityObject,
                        Nullable<int> ldsity,
                        List<Nullable<int>> etlId,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = SQL;

                        CommandText = CommandText.Replace(
                            oldValue: "@ldsityObject",
                            newValue: Functions.PrepNIntArg(arg: ldsityObject));

                        CommandText = CommandText.Replace(
                            oldValue: "@ldsity",
                            newValue: Functions.PrepNIntArg(arg: ldsity));

                        CommandText = CommandText.Replace(
                            oldValue: "@etlId",
                            newValue: Functions.PrepNIntArrayArg(args: etlId, dbType: "int4"));

                        NpgsqlParameter pLDsityObject = new(
                            parameterName: "ldsityObject",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pLDsity = new(
                            parameterName: "ldsity",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pEtlId = new(
                            parameterName: "etlId",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        if (ldsityObject != null)
                        {
                            pLDsityObject.Value = (int)ldsityObject;
                        }
                        else
                        {
                            pLDsityObject.Value = DBNull.Value;
                        }

                        if (ldsity != null)
                        {
                            pLDsity.Value = (int)ldsity;
                        }
                        else
                        {
                            pLDsity.Value = DBNull.Value;
                        }

                        if (etlId != null)
                        {
                            pEtlId.Value = etlId.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pEtlId.Value = DBNull.Value;
                        }

                        return
                            database.Postgres.ExecuteQuery(
                                sqlCommand: SQL,
                                transaction: transaction,
                                pLDsityObject, pLDsity, pEtlId);
                    }

                    #endregion Methods

                }

                #endregion FnImportGetLDsities

            }

        }
    }
}