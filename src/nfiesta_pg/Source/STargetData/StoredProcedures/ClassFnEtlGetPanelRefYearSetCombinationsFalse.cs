﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {
                // fn_etl_get_panel_refyearset_combinations_false

                #region FnEtlGetPanelRefYearSetCombinationsFalse

                /// <summary>
                /// Wrapper for stored procedure fn_etl_get_panel_refyearset_combinations_false.
                /// Function returns list of combinations of panels and reference year sets that user wants to for ETL
                /// and is added information that given combination of panel a reference year set is useable to ETL.
                /// </summary>
                public static class FnEtlGetPanelRefYearSetCombinationsFalse
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_etl_get_panel_refyearset_combinations_false";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_variables json, ",
                            $"_refyearset2panel_mapping integer[]; ",
                            $"returns: ",
                            $"TABLE(",
                            $"refyearset2panel integer, ",
                            $"panel character varying, ",
                            $"reference_year_set character varying, ",
                            $"useable4etl boolean)");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    {TDFnEtlGetPanelRefYearSetCombinationsFalseTypeList.ColRefYearSetToPanel.SQL(cols: TDFnEtlGetPanelRefYearSetCombinationsFalseTypeList.Cols, alias: Name)}",
                            $"    {TDFnEtlGetPanelRefYearSetCombinationsFalseTypeList.ColPanel.SQL(cols: TDFnEtlGetPanelRefYearSetCombinationsFalseTypeList.Cols, alias: Name)}",
                            $"    {TDFnEtlGetPanelRefYearSetCombinationsFalseTypeList.ColReferenceYearSet.SQL(cols: TDFnEtlGetPanelRefYearSetCombinationsFalseTypeList.Cols, alias: Name)}",
                            $"    {TDFnEtlGetPanelRefYearSetCombinationsFalseTypeList.ColUseableForEtl.SQL(cols: TDFnEtlGetPanelRefYearSetCombinationsFalseTypeList.Cols, alias: Name, isLastOne: true)}",
                            $"FROM{Environment.NewLine}",
                            $"    $SchemaName.$Name(@variables, @refYearSetToPanelMapping) AS {Name}{Environment.NewLine}",
                            $"ORDER BY{Environment.NewLine}",
                            $"    {Name}.{TDFnEtlGetPanelRefYearSetCombinationsFalseTypeList.ColRefYearSetToPanel.Name};{Environment.NewLine}"
                            );

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="variables">Variables json</param>
                    /// <param name="refYearSetToPanelMapping">Ids of refyearset to panel mapping</param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        string variables,
                        List<Nullable<int>> refYearSetToPanelMapping)
                    {
                        string result = SQL;

                        result = result.Replace(
                            oldValue: "@variables",
                            newValue: Functions.PrepStringArg(arg: variables));

                        result = result.Replace(
                            oldValue: "@refYearSetToPanelMapping",
                            newValue: Functions.PrepNIntArrayArg(args: refYearSetToPanelMapping, dbType: "int4"));

                        return result;
                    }

                    /// <summary>
                    ///  Execute stored procedure fn_etl_get_panel_refyearset_combinations_false
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="variables">Variables json</param>
                    /// <param name="refYearSetToPanelMapping">Ids of refyearset to panel mapping</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table with panel and refyearset combinations</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        string variables,
                        List<Nullable<int>> refYearSetToPanelMapping,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(
                            variables: variables,
                            refYearSetToPanelMapping: refYearSetToPanelMapping);

                        NpgsqlParameter pVariables = new(
                           parameterName: "variables",
                           parameterType: NpgsqlDbType.Json);

                        NpgsqlParameter pRefYearSetToPanelMapping = new(
                            parameterName: "refYearSetToPanelMapping",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        if (variables != null)
                        {
                            pVariables.Value = (string)variables;
                        }
                        else
                        {
                            pVariables.Value = DBNull.Value;
                        }

                        if (refYearSetToPanelMapping != null)
                        {
                            pRefYearSetToPanelMapping.Value = refYearSetToPanelMapping.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pRefYearSetToPanelMapping.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteQuery(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pVariables,
                            pRefYearSetToPanelMapping);
                    }

                    /// <summary>
                    /// Execute stored procedure fn_etl_get_panel_refyearset_combinations_false
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="variables">Variables json</param>
                    /// <param name="refYearSetToPanelMapping">Ids of the panel and refyearset combinations</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>List of panel and refyearset combinations</returns>
                    public static TDFnEtlGetPanelRefYearSetCombinationsFalseTypeList Execute(
                        NfiEstaDB database,
                        string variables,
                        List<Nullable<int>> refYearSetToPanelMapping,
                        NpgsqlTransaction transaction = null)
                    {
                        System.Diagnostics.Stopwatch stopWatch = new();
                        stopWatch.Start();

                        DataTable data = ExecuteQuery(
                                database: database,
                                variables: variables,
                                refYearSetToPanelMapping: refYearSetToPanelMapping,
                                transaction: transaction);
                        data.TableName = Name;

                        stopWatch.Stop();

                        return new TDFnEtlGetPanelRefYearSetCombinationsFalseTypeList(
                            database: database,
                            data: data,
                            loadingTime: 0.001 * stopWatch.ElapsedMilliseconds,
                            storedProcedure: CommandText)
                        {
                            Variables = variables,
                            RefYearSetToPanelMapping = refYearSetToPanelMapping
                        };
                    }

                    #endregion Methods

                }

                #endregion FnEtlGetPanelRefYearSetCombinationsFalse

            }

        }
    }
}
