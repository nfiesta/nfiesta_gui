﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {
                // fn_etl_get_list_of_area_domains4update

                #region FnEtlGetListOfAreaDomainsForUpdate

                /// <summary>
                /// Wrapper for stored procedure fn_etl_get_list_of_area_domains4update.
                /// The function returns list of area domains that had already been ETLed
                /// and some of their label or description is difference
                /// between source and target DB for given export connection.
                /// </summary>
                public static class FnEtlGetListOfAreaDomainsForUpdate
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_etl_get_list_of_area_domains4update";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_export_connection integer, ",
                            $"_area_domain_target integer[]; ",
                            $"returns: ",
                            $"TABLE(",
                            $"etl_area_domain integer, ",
                            $"area_domain_target integer, ",
                            $"label character varying, ",
                            $"description text, ",
                            $"label_en character varying, ",
                            $"description_en text)");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    etl_area_domain::integer          AS etl_area_domain,{Environment.NewLine}",
                            $"    area_domain_target::integer       AS area_domain_target,{Environment.NewLine}",
                            $"    label::character varying          AS label,{Environment.NewLine}",
                            $"    description::text                 AS description,{Environment.NewLine}",
                            $"    label_en::character varying       AS label_en,{Environment.NewLine}",
                            $"    description_en::text              AS description_en{Environment.NewLine}",
                            $"FROM {Environment.NewLine}",
                            $"    $SchemaName.$Name{Environment.NewLine}",
                            $"({Environment.NewLine}",
                            $"@exportConnection,    -- _export_connection{Environment.NewLine}",
                            $"@areaDomainTarget     -- _area_domain_target{Environment.NewLine}",
                            $") AS $Name;{Environment.NewLine}");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="exportConnection"></param>
                    /// <param name="areaDomainTarget"></param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        Nullable<int> exportConnection,
                        List<Nullable<int>> areaDomainTarget)
                    {
                        string result = SQL;

                        result = result.Replace(
                            oldValue: "@exportConnection",
                            newValue: Functions.PrepNIntArg(arg: exportConnection));

                        result = result.Replace(
                            oldValue: "@areaDomainTarget",
                            newValue: Functions.PrepNIntArrayArg(args: areaDomainTarget, dbType: "int4"));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_etl_get_list_of_area_domains4update
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="exportConnection"></param>
                    /// <param name="areaDomainTarget"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> exportConnection,
                        List<Nullable<int>> areaDomainTarget,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(
                            exportConnection: exportConnection,
                            areaDomainTarget: areaDomainTarget);

                        NpgsqlParameter pExportConnection = new(
                            parameterName: "exportConnection",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pAreaDomainTarget = new(
                            parameterName: "areaDomainTarget",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        if (exportConnection != null)
                        {
                            pExportConnection.Value = (int)exportConnection;
                        }
                        else
                        {
                            pExportConnection.Value = DBNull.Value;
                        }

                        if (areaDomainTarget != null)
                        {
                            pAreaDomainTarget.Value = areaDomainTarget.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pAreaDomainTarget.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteQuery(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pExportConnection, pAreaDomainTarget);
                    }

                    #endregion Methods

                }

                #endregion FnEtlGetListOfAreaDomainsForUpdate

            }

        }
    }
}