﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {
                // fn_delete_ldsity_values

                #region FnDeleteLDsityValues

                /// <summary>
                /// Wrapper for stored procedure fn_delete_ldsity_values.
                /// Function deletes records from t_ldsity_values
                /// and t_available_datasets table.
                /// </summary>
                public static class FnDeleteLDsityValues
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_delete_ldsity_values";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_target_variable integer DEFAULT NULL::integer, ",
                            $"_area_domain integer DEFAULT NULL::integer, ",
                            $"_sub_population integer DEFAULT NULL::integer; ",
                            $"returns: void");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    $SchemaName.$Name(@targetVariableId, @areaDomainId, @subPopulationId);{Environment.NewLine}");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="targetVariableId">Target variable identifier</param>
                    /// <param name="areaDomainId">Area domain identifier</param>
                    /// <param name="subPopulationId">Subpopulation identifier</param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        Nullable<int> targetVariableId = null,
                        Nullable<int> areaDomainId = null,
                        Nullable<int> subPopulationId = null)
                    {
                        string result = SQL;

                        result = result.Replace(
                            oldValue: "@targetVariableId",
                            newValue: Functions.PrepNIntArg(arg: targetVariableId));

                        result = result.Replace(
                            oldValue: "@areaDomainId",
                            newValue: Functions.PrepNIntArg(arg: areaDomainId));

                        result = result.Replace(
                            oldValue: "@subPopulationId",
                            newValue: Functions.PrepNIntArg(arg: subPopulationId));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_delete_ldsity_values
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="targetVariableId">Target variable identifier</param>
                    /// <param name="areaDomainId">Area domain identifier</param>
                    /// <param name="subPopulationId">Subpopulation identifier</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Empty data table</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> targetVariableId = null,
                        Nullable<int> areaDomainId = null,
                        Nullable<int> subPopulationId = null,
                        NpgsqlTransaction transaction = null)
                    {
                        Execute(
                            database: database,
                            targetVariableId: targetVariableId,
                            areaDomainId: areaDomainId,
                            subPopulationId: subPopulationId,
                            transaction: transaction);

                        DataTable dt = new();

                        dt.Columns.Add(column: new DataColumn()
                        {
                            ColumnName = Name,
                            DataType = Type.GetType(typeName: "System.Int32")
                        });

                        return dt;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_delete_ldsity_values
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="targetVariableId">Target variable identifier</param>
                    /// <param name="areaDomainId">Area domain identifier</param>
                    /// <param name="subPopulationId">Subpopulation identifier</param>
                    /// <param name="transaction">Transaction</param>
                    public static void Execute(
                        NfiEstaDB database,
                        Nullable<int> targetVariableId = null,
                        Nullable<int> areaDomainId = null,
                        Nullable<int> subPopulationId = null,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(
                            targetVariableId: targetVariableId,
                            areaDomainId: areaDomainId,
                            subPopulationId: subPopulationId);

                        NpgsqlParameter pTargetVariableId = new(
                            parameterName: "targetVariableId",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pAreaDomainId = new(
                           parameterName: "areaDomainId",
                           parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pSubPopulationId = new(
                           parameterName: "subPopulationId",
                           parameterType: NpgsqlDbType.Integer);

                        if (targetVariableId != null)
                        {
                            pTargetVariableId.Value = (int)targetVariableId;
                        }
                        else
                        {
                            pTargetVariableId.Value = DBNull.Value;
                        }

                        if (areaDomainId != null)
                        {
                            pAreaDomainId.Value = (int)areaDomainId;
                        }
                        else
                        {
                            pAreaDomainId.Value = DBNull.Value;
                        }

                        if (subPopulationId != null)
                        {
                            pSubPopulationId.Value = (int)subPopulationId;
                        }
                        else
                        {
                            pSubPopulationId.Value = DBNull.Value;
                        }

                        database.Postgres.ExecuteNonQuery(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pTargetVariableId,
                            pAreaDomainId,
                            pSubPopulationId);
                    }

                    #endregion Methods

                }

                #endregion FnDeleteLDsityValues

            }

        }
    }
}