﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {
                // fn_etl_save_export_connection

                #region FnEtlSaveExportConnection

                /// <summary>
                /// Wrapper for stored procedure fn_etl_save_export_connection.
                /// Function inserts a record into table c_export_connection based on given parameters.
                /// </summary>
                public static class FnEtlSaveExportConnection
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_etl_save_export_connection";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_host character varying, ",
                            $"_dbname character varying, ",
                            $"_port integer, ",
                            $"_comment text; ",
                            $"returns: integer");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    $SchemaName.$Name(@host, @dbname, @port, @comment)::integer AS {Name};{Environment.NewLine}");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="host">Server name or IP address</param>
                    /// <param name="dbname">Database name</param>
                    /// <param name="port">TCP port</param>
                    /// <param name="comment">Comment</param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        string host,
                        string dbname,
                        Nullable<int> port,
                        string comment)
                    {
                        string result = SQL;

                        result = result.Replace(
                            oldValue: "@host",
                            newValue: Functions.PrepStringArg(arg: host));

                        result = result.Replace(
                            oldValue: "@dbname",
                            newValue: Functions.PrepStringArg(arg: dbname));

                        result = result.Replace(
                            oldValue: "@port",
                            newValue: Functions.PrepNIntArg(arg: port));

                        result = result.Replace(
                            oldValue: "@comment",
                            newValue: Functions.PrepStringArg(arg: comment));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_etl_save_export_connection
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="host">Server name or IP address</param>
                    /// <param name="dbname">Database name</param>
                    /// <param name="port">TCP port</param>
                    /// <param name="comment">Comment</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table with identifier for new export connection</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        string host,
                        string dbname,
                        Nullable<int> port,
                        string comment,
                        NpgsqlTransaction transaction = null)
                    {
                        Nullable<int> val = Execute(
                            database: database,
                            host: host,
                            dbname: dbname,
                            port: port,
                            comment: comment,
                            transaction: transaction);

                        DataTable dt = new();

                        dt.Columns.Add(column: new DataColumn()
                        {
                            ColumnName = Name,
                            DataType = Type.GetType(typeName: "System.Int32")
                        });

                        DataRow row = dt.NewRow();

                        Functions.SetNIntArg(
                            row: row,
                            name: Name,
                            val: val);

                        dt.Rows.Add(row: row);

                        return dt;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_etl_save_export_connection
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="host">Server name or IP address</param>
                    /// <param name="dbname">Database name</param>
                    /// <param name="port">TCP port</param>
                    /// <param name="comment">Comment</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Identifier for new export connection</returns>
                    public static Nullable<int> Execute(
                        NfiEstaDB database,
                        string host,
                        string dbname,
                        Nullable<int> port,
                        string comment,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(host: host, dbname: dbname, port: port, comment: comment);

                        NpgsqlParameter pHost = new(
                            parameterName: "host",
                            parameterType: NpgsqlDbType.Varchar);

                        NpgsqlParameter pDBName = new(
                            parameterName: "dbname",
                            parameterType: NpgsqlDbType.Varchar);

                        NpgsqlParameter pPort = new(
                            parameterName: "port",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pComment = new(
                            parameterName: "comment",
                            parameterType: NpgsqlDbType.Text);

                        if (host != null)
                        {
                            pHost.Value = (string)host;
                        }
                        else
                        {
                            pHost.Value = DBNull.Value;
                        }

                        if (dbname != null)
                        {
                            pDBName.Value = (string)dbname;
                        }
                        else
                        {
                            pDBName.Value = DBNull.Value;
                        }

                        if (port != null)
                        {
                            pPort.Value = (int)port;
                        }
                        else
                        {
                            pPort.Value = DBNull.Value;
                        }

                        if (comment != null)
                        {
                            pComment.Value = (string)comment;
                        }
                        else
                        {
                            pComment.Value = DBNull.Value;
                        }

                        string strResult = database.Postgres.ExecuteScalar(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pHost, pDBName, pPort, pComment);

                        if (String.IsNullOrEmpty(value: strResult))
                        {
                            return null;
                        }
                        else
                        {
                            if (Int32.TryParse(s: strResult, result: out int intResult))
                            {
                                return intResult;
                            }
                            else
                            {
                                return null;
                            }
                        }
                    }

                    #endregion Methods

                }

                #endregion FnEtlSaveExportConnection

            }

        }
    }
}