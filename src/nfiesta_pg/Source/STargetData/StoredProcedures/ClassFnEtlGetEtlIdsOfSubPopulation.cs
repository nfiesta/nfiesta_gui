﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {
                // fn_etl_get_etl_ids_of_sub_population

                #region FnEtlGetEtlIdsOfSubPopulation

                /// <summary>
                /// Wrapper for stored procedure fn_etl_get_etl_ids_of_sub_population.
                /// The function gets an array of etl_ids for subpopulation combinations that was ETLed yet.
                /// </summary>
                public static class FnEtlGetEtlIdsOfSubPopulation
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_etl_get_etl_ids_of_sub_population";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_export_connection integer; ",
                            $"returns: integer[]");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    array_to_string($SchemaName.$Name(",
                            $"@exportConnectionId),';','NULL')::text AS $Name;{Environment.NewLine}");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="exportConnectionId">Export connection identifier</param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(Nullable<int> exportConnectionId)
                    {
                        string result = SQL;

                        result = result.Replace(
                            oldValue: "@exportConnectionId",
                            newValue: Functions.PrepNIntArg(arg: exportConnectionId));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_etl_get_etl_ids_of_sub_population
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="exportConnectionId">Export connection identifier</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table with etl_ids for subpopulation combinations</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> exportConnectionId,
                        NpgsqlTransaction transaction = null)
                    {
                        List<Nullable<int>> list = Execute(
                            database: database,
                            exportConnectionId: exportConnectionId,
                            transaction: transaction);

                        DataTable dt = new();

                        dt.Columns.Add(column: new DataColumn()
                        {
                            ColumnName = Name,
                            DataType = Type.GetType(typeName: "System.Int32")
                        });

                        if (list == null)
                        {
                            return dt;
                        }

                        foreach (Nullable<int> val in list)
                        {
                            DataRow row = dt.NewRow();

                            Functions.SetNIntArg(
                                row: row,
                                name: Name,
                                val: val);

                            dt.Rows.Add(row: row);
                        }

                        return dt;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_etl_get_etl_ids_of_sub_population
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="exportConnectionId">Export connection identifier</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>List with etl_ids for subpopulation combinations</returns>
                    public static List<Nullable<int>> Execute(
                        NfiEstaDB database,
                        Nullable<int> exportConnectionId,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(exportConnectionId: exportConnectionId);

                        NpgsqlParameter pExportConnectionId = new(
                            parameterName: "exportConnectionId",
                            parameterType: NpgsqlDbType.Integer);

                        if (exportConnectionId != null)
                        {
                            pExportConnectionId.Value = (int)exportConnectionId;
                        }
                        else
                        {
                            pExportConnectionId.Value = DBNull.Value;
                        }

                        string strResult = database.Postgres.ExecuteScalar(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pExportConnectionId);

                        if (String.IsNullOrEmpty(value: strResult))
                        {
                            return null;
                        }
                        else
                        {
                            return
                            Functions.StringToNIntList(
                                text: strResult,
                                separator: (char)59,
                                defaultValue: null);
                        }
                    }

                    #endregion Methods

                }

                #endregion FnEtlGetEtlIdsOfSubPopulation

            }

        }
    }
}