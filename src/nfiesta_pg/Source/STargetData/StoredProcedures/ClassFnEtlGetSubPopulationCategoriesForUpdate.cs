﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {
                // fn_etl_get_sub_population_categories4update

                #region FnEtlGetSubPopulationCategoriesForUpdate

                /// <summary>
                /// Wrapper for stored procedure fn_etl_get_sub_population_categories4update.
                /// The function returns list of sub population categories that had already been ETLed for given sub population.
                /// </summary>
                public static class FnEtlGetSubPopulationCategoriesForUpdate
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_etl_get_sub_population_categories4update";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_etl_sub_population integer, ",
                            $"_sub_population_target integer; ",
                            $"returns: json");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    $SchemaName.$Name(@etlSubPopulation, @subPopulationTarget)::text AS $Name;{Environment.NewLine}");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="etlSubPopulation"></param>
                    /// <param name="subPopulationTarget"></param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        Nullable<int> etlSubPopulation,
                        Nullable<int> subPopulationTarget)
                    {
                        string result = SQL;

                        result = result.Replace(
                            oldValue: "@etlSubPopulation",
                            newValue: Functions.PrepNIntArg(arg: etlSubPopulation));

                        result = result.Replace(
                            oldValue: "@subPopulationTarget",
                            newValue: Functions.PrepNIntArg(arg: subPopulationTarget));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_etl_get_sub_population_categories4update
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="etlSubPopulation"></param>
                    /// <param name="subPopulationTarget"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> etlSubPopulation,
                        Nullable<int> subPopulationTarget,
                        NpgsqlTransaction transaction = null)
                    {
                        string val = Execute(
                            database: database,
                            etlSubPopulation: etlSubPopulation,
                            subPopulationTarget: subPopulationTarget,
                            transaction: transaction);

                        DataTable dt = new();

                        dt.Columns.Add(column: new DataColumn()
                        {
                            ColumnName = Name,
                            DataType = Type.GetType(typeName: "System.String")
                        });

                        DataRow row = dt.NewRow();

                        Functions.SetStringArg(
                            row: row,
                            name: Name,
                            val: val);

                        dt.Rows.Add(row: row);

                        return dt;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_etl_get_sub_population_categories4update
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="etlSubPopulation"></param>
                    /// <param name="subPopulationTarget"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table</returns>
                    public static string Execute(
                        NfiEstaDB database,
                        Nullable<int> etlSubPopulation,
                        Nullable<int> subPopulationTarget,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(
                            etlSubPopulation,
                            subPopulationTarget);

                        NpgsqlParameter pEtlSubPopulation = new(
                            parameterName: "etlSubPopulation",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pSubPopulationTarget = new(
                            parameterName: "subPopulationTarget",
                            parameterType: NpgsqlDbType.Integer);

                        if (etlSubPopulation != null)
                        {
                            pEtlSubPopulation.Value = (int)etlSubPopulation;
                        }
                        else
                        {
                            pEtlSubPopulation.Value = DBNull.Value;
                        }

                        if (subPopulationTarget != null)
                        {
                            pSubPopulationTarget.Value = (int)subPopulationTarget;
                        }
                        else
                        {
                            pSubPopulationTarget.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteScalar(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pEtlSubPopulation,
                            pSubPopulationTarget);
                    }

                    #endregion Methods

                }

                #endregion FnEtlGetSubPopulationCategoriesForUpdate

            }

        }
    }
}