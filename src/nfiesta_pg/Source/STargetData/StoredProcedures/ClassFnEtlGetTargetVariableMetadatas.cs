﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {
                // fn_etl_get_target_variable_metadatas

                #region FnEtlGetTargetVariableMetadatas

                /// <summary>
                /// Wrapper for stored procedure fn_etl_get_target_variable_metadatas.
                /// The function returs metadata for input target variables.
                /// </summary>
                public static class FnEtlGetTargetVariableMetadatas
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_etl_get_target_variable_metadatas";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_id_etl_target_variable integer[], ",
                            $"_national_language character varying DEFAULT 'en'::character varying(2); ",
                            $"returns: json");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT ",
                            $"    $SchemaName.$Name(@etlTargetVariableIds, @nationalLanguage)::text AS {Name};{Environment.NewLine}");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="etlTargetVariableIds"></param>
                    /// <param name="nationalLanguage"></param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        List<Nullable<int>> etlTargetVariableIds,
                        Language nationalLanguage = PostgreSQL.Language.CS)
                    {
                        string result = SQL;

                        result = result.Replace(
                            oldValue: "@etlTargetVariableIds",
                            newValue: Functions.PrepNIntArrayArg(args: etlTargetVariableIds, dbType: "int4"));

                        result = result.Replace(
                            oldValue: "@nationalLanguage",
                            newValue: Functions.PrepStringArg(
                                arg: LanguageList.ISO_639_1(language: nationalLanguage)));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_etl_get_target_variable_metadatas
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="etlTargetVariableIds"></param>
                    /// <param name="nationalLanguage"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table</returns>
                    public static DataTable ExecuteQuery(
                         NfiEstaDB database,
                         List<Nullable<int>> etlTargetVariableIds,
                         Language nationalLanguage = PostgreSQL.Language.CS,
                         NpgsqlTransaction transaction = null)
                    {
                        string val = Execute(
                            database: database,
                            etlTargetVariableIds: etlTargetVariableIds,
                            nationalLanguage: nationalLanguage,
                            transaction: transaction);

                        DataTable dt = new();

                        dt.Columns.Add(column: new DataColumn()
                        {
                            ColumnName = Name,
                            DataType = Type.GetType(typeName: "System.String")
                        });

                        DataRow row = dt.NewRow();

                        Functions.SetStringArg(
                            row: row,
                            name: Name,
                            val: val);

                        dt.Rows.Add(row: row);

                        return dt;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_etl_get_target_variable_metadatas
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="etlTargetVariableIds"></param>
                    /// <param name="nationalLanguage"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>json</returns>
                    public static string Execute(
                        NfiEstaDB database,
                        List<Nullable<int>> etlTargetVariableIds,
                        Language nationalLanguage = PostgreSQL.Language.CS,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(
                            etlTargetVariableIds: etlTargetVariableIds,
                            nationalLanguage: nationalLanguage);

                        NpgsqlParameter pEtlTargetVariableIds = new(
                            parameterName: "etlTargetVariableIds",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        NpgsqlParameter pNationalLanguage = new(
                            parameterName: "nationalLanguage",
                            parameterType: NpgsqlDbType.Varchar);

                        if (etlTargetVariableIds != null)
                        {
                            pEtlTargetVariableIds.Value = etlTargetVariableIds.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pEtlTargetVariableIds.Value = DBNull.Value;
                        }

                        pNationalLanguage.Value =
                                LanguageList.ISO_639_1(language: nationalLanguage);

                        return
                            database.Postgres.ExecuteScalar(
                                sqlCommand: SQL,
                                transaction: transaction,
                                pEtlTargetVariableIds, pNationalLanguage);
                    }

                    #endregion Methods

                }

                #endregion FnEtlGetTargetVariableMetadatas

            }

        }
    }
}