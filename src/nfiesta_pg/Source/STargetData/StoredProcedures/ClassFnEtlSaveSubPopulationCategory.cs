﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {
                // fn_etl_save_sub_population_category

                #region FnEtlSaveSubPopulationCategory

                /// <summary>
                /// Wrapper for stored procedure fn_etl_save_sub_population_category.
                /// Function inserts a record into table t_etl_sub_population_category based on given parameters.
                /// </summary>
                public static class FnEtlSaveSubPopulationCategory
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_etl_save_sub_population_category";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_sub_population_category integer[], ",
                            $"_etl_id integer, ",
                            $"_etl_sub_population integer; ",
                            $"returns: integer");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    $SchemaName.$Name(@subPopulationCategory, @etlId, @etlSubPopulation)::integer AS {Name};{Environment.NewLine}");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="subPopulationCategory"></param>
                    /// <param name="etlId"></param>
                    /// <param name="etlSubPopulation"></param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        List<Nullable<int>> subPopulationCategory,
                        Nullable<int> etlId,
                        Nullable<int> etlSubPopulation)
                    {
                        string result = SQL;

                        result = result.Replace(
                            oldValue: "@subPopulationCategory",
                            newValue: Functions.PrepNIntArrayArg(args: subPopulationCategory, dbType: "int4"));

                        result = result.Replace(
                            oldValue: "@etlId",
                            newValue: Functions.PrepNIntArg(arg: etlId));

                        result = result.Replace(
                            oldValue: "@etlSubPopulation",
                            newValue: Functions.PrepNIntArg(arg: etlSubPopulation));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_etl_save_sub_population_category
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="subPopulationCategory"></param>
                    /// <param name="etlId"></param>
                    /// <param name="etlSubPopulation"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        List<Nullable<int>> subPopulationCategory,
                        Nullable<int> etlId,
                        Nullable<int> etlSubPopulation,
                        NpgsqlTransaction transaction = null)
                    {
                        Nullable<int> val = Execute(
                            database: database,
                            subPopulationCategory: subPopulationCategory,
                            etlId: etlId,
                            etlSubPopulation: etlSubPopulation,
                            transaction: transaction);

                        DataTable dt = new();

                        dt.Columns.Add(column: new DataColumn()
                        {
                            ColumnName = Name,
                            DataType = Type.GetType(typeName: "System.Int32")
                        });

                        DataRow row = dt.NewRow();

                        Functions.SetNIntArg(
                            row: row,
                            name: Name,
                            val: val);

                        dt.Rows.Add(row: row);

                        return dt;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_etl_save_sub_population_category
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="subPopulationCategory"></param>
                    /// <param name="etlId"></param>
                    /// <param name="etlSubPopulation"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>integer</returns>
                    public static Nullable<int> Execute(
                        NfiEstaDB database,
                        List<Nullable<int>> subPopulationCategory,
                        Nullable<int> etlId,
                        Nullable<int> etlSubPopulation,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(
                            subPopulationCategory: subPopulationCategory,
                            etlId: etlId,
                            etlSubPopulation: etlSubPopulation);

                        NpgsqlParameter pSubPopulationCategory = new(
                            parameterName: "subPopulationCategory",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        NpgsqlParameter pEtlId = new(
                            parameterName: "etlId",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pEtlSubPopulation = new(
                            parameterName: "etlSubPopulation",
                            parameterType: NpgsqlDbType.Integer);

                        if (subPopulationCategory != null)
                        {
                            pSubPopulationCategory.Value = subPopulationCategory.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pSubPopulationCategory.Value = DBNull.Value;
                        }

                        if (etlId != null)
                        {
                            pEtlId.Value = (int)etlId;
                        }
                        else
                        {
                            pEtlId.Value = DBNull.Value;
                        }

                        if (etlSubPopulation != null)
                        {
                            pEtlSubPopulation.Value = (int)etlSubPopulation;
                        }
                        else
                        {
                            pEtlSubPopulation.Value = DBNull.Value;
                        }

                        string strResult = database.Postgres.ExecuteScalar(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pSubPopulationCategory, pEtlId, pEtlSubPopulation);

                        if (String.IsNullOrEmpty(value: strResult))
                        {
                            return null;
                        }
                        else
                        {
                            if (Int32.TryParse(s: strResult, result: out int intResult))
                            {
                                return intResult;
                            }
                            else
                            {
                                return null;
                            }
                        }
                    }

                    #endregion Methods

                }

                #endregion FnEtlSaveSubPopulationCategory

            }

        }
    }
}