﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {

                #region FnGetAreaDomain

                /// <summary>
                /// Wrapper for stored procedure fn_get_area_domain.
                /// Function returns records from c_area_domain table.
                /// </summary>
                public static class FnGetAreaDomain
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_get_area_domain";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_id integer DEFAULT NULL::integer, ",
                            $"_target_variable integer DEFAULT NULL::integer; ",
                            $"returns: ",
                            $"TABLE(",
                            $"id integer, ",
                            $"label character varying, ",
                            $"description text, ",
                            $"label_en character varying, ",
                            $"description_en text, ",
                            $"classification_type integer)");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                    = String.Concat(
                    $"SELECT{Environment.NewLine}",
                    $"    {TDAreaDomainList.ColId.SQL(TDAreaDomainList.Cols, Name)}",
                    $"    {TDAreaDomainList.ColLabelCs.SQL(TDAreaDomainList.Cols, Name)}",
                            $"    {TDAreaDomainList.ColDescriptionCs.SQL(TDAreaDomainList.Cols, Name)}",
                            $"    {TDAreaDomainList.ColLabelEn.SQL(TDAreaDomainList.Cols, Name)}",
                            $"    {TDAreaDomainList.ColDescriptionEn.SQL(TDAreaDomainList.Cols, Name)}",
                            $"    {TDAreaDomainList.ColClassificationType.SQL(TDAreaDomainList.Cols, Name, isLastOne: true)}",
                            $"FROM{Environment.NewLine}",
                            $"    $SchemaName.$Name(@areaDomainId, @targetVariableId) AS {Name}{Environment.NewLine}",
                            $"ORDER BY{Environment.NewLine}",
                            $"    {Name}.{TDAreaDomainList.ColId.DbName};{Environment.NewLine}"
                            );

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Execute stored procedure fn_get_area_domain
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="areaDomainId">Area domain identifier</param>
                    /// <param name="targetVariableId">Target variable identifier</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table with area domains</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> areaDomainId = null,
                        Nullable<int> targetVariableId = null,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = SQL;

                        CommandText = CommandText.Replace(
                            oldValue: "@areaDomainId",
                            newValue: Functions.PrepNIntArg(arg: areaDomainId));

                        CommandText = CommandText.Replace(
                            oldValue: "@targetVariableId",
                            newValue: Functions.PrepNIntArg(arg: targetVariableId));

                        NpgsqlParameter pAreaDomainId = new(
                            parameterName: "areaDomainId",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pTargetVariableId = new(
                           parameterName: "targetVariableId",
                           parameterType: NpgsqlDbType.Integer);

                        if (areaDomainId != null)
                        {
                            pAreaDomainId.Value = (int)areaDomainId;
                        }
                        else
                        {
                            pAreaDomainId.Value = DBNull.Value;
                        }

                        if (targetVariableId != null)
                        {
                            pTargetVariableId.Value = (int)targetVariableId;
                        }
                        else
                        {
                            pTargetVariableId.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteQuery(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pAreaDomainId, pTargetVariableId);
                    }

                    /// <summary>
                    /// Execute stored procedure fn_get_area_domain
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="areaDomainId">Area domain identifier</param>
                    /// <param name="targetVariableId">Target variable identifier</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>List of area domains</returns>
                    public static TDAreaDomainList Execute(
                        NfiEstaDB database,
                        Nullable<int> areaDomainId = null,
                        Nullable<int> targetVariableId = null,
                        NpgsqlTransaction transaction = null)
                    {
                        return new TDAreaDomainList(
                            database: database,
                            data: ExecuteQuery(
                                database: database,
                                areaDomainId: areaDomainId,
                                targetVariableId: targetVariableId,
                                transaction: transaction))
                        { StoredProcedure = CommandText };
                    }

                    #endregion Methods

                }

                #endregion FnGetAreaDomain

            }

        }
    }
}