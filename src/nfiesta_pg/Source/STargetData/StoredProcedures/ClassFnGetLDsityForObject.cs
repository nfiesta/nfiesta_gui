﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {

                #region FnGetLDsityForObject

                /// <summary>
                /// Wrapper for stored procedure fn_get_ldsity4object.
                /// Function returns records from c_ldsity table,
                /// optionally for given ldsity object and unit_of_measure.
                /// </summary>
                public static class FnGetLDsityForObject
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_get_ldsity4object";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_ldsity_object integer DEFAULT NULL::integer, ",
                            $"_unit_of_measure integer DEFAULT NULL::integer; ",
                            $"returns: ",
                            $"TABLE(",
                            $"id integer, ",
                            $"ldsity_object integer, ",
                            $"label character varying, ",
                            $"description text, ",
                            $"label_en character varying, ",
                            $"description_en text, ",
                            $"column_expression text, ",
                            $"unit_of_measure integer, ",
                            $"definition_variant integer[], ",
                            $"version integer)");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    {TDLDsityList.ColId.SQL(TDLDsityList.Cols, Name)}",
                            $"    {TDLDsityList.ColLabelCs.SQL(TDLDsityList.Cols, Name)}",
                            $"    {TDLDsityList.ColDescriptionCs.SQL(TDLDsityList.Cols, Name)}",
                            $"    {TDLDsityList.ColLabelEn.SQL(TDLDsityList.Cols, Name)}",
                            $"    {TDLDsityList.ColDescriptionEn.SQL(TDLDsityList.Cols, Name)}",
                            $"    {TDLDsityList.ColLDsityObjectId.SQL(TDLDsityList.Cols, Name)}",
                            $"    {TDLDsityList.ColLDsityObjectLabelCs.SQL(TDLDsityList.Cols, Name, setToNull: true)}",
                            $"    {TDLDsityList.ColLDsityObjectDescriptionCs.SQL(TDLDsityList.Cols, Name, setToNull: true)}",
                            $"    {TDLDsityList.ColLDsityObjectLabelEn.SQL(TDLDsityList.Cols, Name, setToNull: true)}",
                            $"    {TDLDsityList.ColLDsityObjectDescriptionEn.SQL(TDLDsityList.Cols, Name, setToNull: true)}",
                            $"    {TDLDsityList.ColColumnExpression.SQL(TDLDsityList.Cols, Name)}",
                            $"    {TDLDsityList.ColUnitOfMeasureId.SQL(TDLDsityList.Cols, Name)}",
                            $"    {TDLDsityList.ColAreaDomainCategory.SQL(TDLDsityList.Cols, Name, setToNull: true)}",
                            $"    {TDLDsityList.ColSubPopulationCategory.SQL(TDLDsityList.Cols, Name, setToNull: true)}",
                            $"    {TDLDsityList.ColDefinitionVariant.SQL(TDLDsityList.Cols, Name)}",
                            $"    {TDLDsityList.ColTargetVariableId.SQL(TDLDsityList.Cols, Name, setToNull: true)}",
                            $"    {TDLDsityList.ColLDsityObjectTypeId.SQL(TDLDsityList.Cols, Name, setToNull: true)}",
                            $"    {TDLDsityList.ColUseNegative.SQL(TDLDsityList.Cols, Name, setToNull: true)}",
                            $"    {TDLDsityList.ColVersionId.SQL(TDLDsityList.Cols, Name, isLastOne: true)}",
                            $"FROM{Environment.NewLine}",
                            $"    $SchemaName.$Name(@ldsityObjectId, @unitOfMeasureId) AS {Name}{Environment.NewLine}",
                            $"ORDER BY{Environment.NewLine}",
                            $"    {Name}.{TDLDsityList.ColId.DbName};{Environment.NewLine}"
                            );

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Execute stored procedure fn_get_ldsity4object
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="ldsityObjectId">Local density object identifier</param>
                    /// <param name="unitOfMeasureId">Unit of measure identifier</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table with local densities</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> ldsityObjectId = null,
                        Nullable<int> unitOfMeasureId = null,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = SQL;

                        CommandText = CommandText.Replace(
                           oldValue: "@ldsityObjectId",
                           newValue: Functions.PrepNIntArg(arg: ldsityObjectId));

                        CommandText = CommandText.Replace(
                            oldValue: "@unitOfMeasureId",
                            newValue: Functions.PrepNIntArg(arg: unitOfMeasureId));

                        NpgsqlParameter pLDsityObjectId = new(
                            parameterName: "ldsityObjectId",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pUnitOfMeasureId = new(
                            parameterName: "unitOfMeasureId",
                            parameterType: NpgsqlDbType.Integer);

                        if (ldsityObjectId != null)
                        {
                            if (unitOfMeasureId != null)
                            {
                                pLDsityObjectId.Value = (int)ldsityObjectId;
                                pUnitOfMeasureId.Value = (int)unitOfMeasureId;
                            }
                            else
                            {
                                pLDsityObjectId.Value = (int)ldsityObjectId;
                                pUnitOfMeasureId.Value = DBNull.Value;
                            }
                        }
                        else
                        {
                            if (unitOfMeasureId != null)
                            {
                                pLDsityObjectId.Value = DBNull.Value;
                                pUnitOfMeasureId.Value = (int)unitOfMeasureId;
                            }
                            else
                            {
                                pLDsityObjectId.Value = DBNull.Value;
                                pUnitOfMeasureId.Value = DBNull.Value;
                            }
                        }

                        return database.Postgres.ExecuteQuery(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pLDsityObjectId, pUnitOfMeasureId);
                    }

                    /// <summary>
                    /// Execute stored procedure fn_get_ldsity4object
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="ldsityObjectId">Local density object identifier</param>
                    /// <param name="unitOfMeasureId">Unit of measure identifier</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>List of local densities</returns>
                    public static TDLDsityList Execute(
                        NfiEstaDB database,
                        Nullable<int> ldsityObjectId = null,
                        Nullable<int> unitOfMeasureId = null,
                        NpgsqlTransaction transaction = null)
                    {
                        return new TDLDsityList(
                            database: database,
                            data: ExecuteQuery(
                               database: database,
                               ldsityObjectId: ldsityObjectId,
                               unitOfMeasureId: unitOfMeasureId,
                               transaction: transaction))
                        { StoredProcedure = CommandText };
                    }

                    #endregion Methods

                }

                #endregion FnGetLDsityForObject

            }

        }
    }
}