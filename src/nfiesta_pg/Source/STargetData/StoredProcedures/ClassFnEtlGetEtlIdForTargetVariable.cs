﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {
                //fn_etl_get_etl_id4target_variable

                #region FnEtlGetEtlIdForTargetVariable

                /// <summary>
                /// Wrapper for stored procedure fn_etl_get_etl_id4target_variable.
                /// Function returns record ETL_ID from table t_etl_target_variable based on given parameters.
                /// </summary>
                public static class FnEtlGetEtlIdForTargetVariable
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_etl_get_etl_id4target_variable";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_id_t_etl_target_variable integer; ",
                            $"returns: integer");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    $SchemaName.$Name(@idTEtlTargetVariable)::integer AS {Name};{Environment.NewLine}");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="idTEtlTargetVariable"></param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(Nullable<int> idTEtlTargetVariable)
                    {
                        string result = SQL;

                        result = result.Replace(
                            oldValue: "@idTEtlTargetVariable",
                            newValue: Functions.PrepNIntArg(arg: idTEtlTargetVariable));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_etl_get_etl_id4target_variable
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="idTEtlTargetVariable"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> idTEtlTargetVariable,
                        NpgsqlTransaction transaction = null)
                    {
                        Nullable<int> val = Execute(
                            database: database,
                            idTEtlTargetVariable: idTEtlTargetVariable,
                            transaction: transaction);

                        DataTable dt = new();

                        dt.Columns.Add(column: new DataColumn()
                        {
                            ColumnName = Name,
                            DataType = Type.GetType(typeName: "System.Int32")
                        });

                        DataRow row = dt.NewRow();

                        Functions.SetNIntArg(
                            row: row,
                            name: Name,
                            val: val);

                        dt.Rows.Add(row: row);

                        return dt;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_etl_get_etl_id4target_variable
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="idTEtlTargetVariable"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>integer</returns>
                    public static Nullable<int> Execute(
                        NfiEstaDB database,
                        Nullable<int> idTEtlTargetVariable,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(idTEtlTargetVariable: idTEtlTargetVariable);

                        NpgsqlParameter pIdTEtlTargetVariable = new(
                            parameterName: "idTEtlTargetVariable",
                            parameterType: NpgsqlDbType.Integer);

                        if (idTEtlTargetVariable != null)
                        {
                            pIdTEtlTargetVariable.Value = (int)idTEtlTargetVariable;
                        }
                        else
                        {
                            pIdTEtlTargetVariable.Value = DBNull.Value;
                        }

                        string strResult =
                            database.Postgres.ExecuteScalar(
                                sqlCommand: SQL,
                                transaction: transaction,
                                pIdTEtlTargetVariable);

                        if (String.IsNullOrEmpty(value: strResult))
                        {
                            return null;
                        }
                        else
                        {
                            if (Int32.TryParse(s: strResult, result: out int intResult))
                            {
                                return intResult;
                            }
                            else
                            {
                                return null;
                            }
                        }
                    }

                    #endregion Methods

                }

                #endregion FnEtlGetEtlIdForTargetVariable

            }

        }
    }
}