﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {

                #region FnImportCheckAreaDomainCategory

                /// <summary>
                /// Wrapper for stored procedure fn_import_check_area_domain_category.
                /// </summary>
                public static class FnImportCheckAreaDomainCategory
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_import_check_area_domain_category";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_area_domain_category integer, ",
                            $"_area_domain__etl_id integer, ",
                            $"_label_en character varying; ",
                            $"returns: ",
                            $"TABLE(",
                            $"area_domain_category integer, ",
                            $"area_domain__etl_id integer, ",
                            $"area_domain__label character varying, ",
                            $"area_domain__description text, ",
                            $"area_domain__label_en character varying, ",
                            $"area_domain__description_en text, ",
                            $"etl_id integer, ",
                            $"label character varying, ",
                            $"description text, ",
                            $"label_en character varying, ",
                            $"description_en text)");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                       = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    area_domain_category,{Environment.NewLine}",
                            $"    area_domain__etl_id,{Environment.NewLine}",
                            $"    area_domain__label,{Environment.NewLine}",
                            $"    area_domain__description,{Environment.NewLine}",
                            $"    area_domain__label_en,{Environment.NewLine}",
                            $"    area_domain__description_en,{Environment.NewLine}",
                            $"    etl_id,{Environment.NewLine}",
                            $"    label,{Environment.NewLine}",
                            $"    description,{Environment.NewLine}",
                            $"    label_en,{Environment.NewLine}",
                            $"    description_en{Environment.NewLine}",
                            $"FROM{Environment.NewLine}",
                            $"    $SchemaName.$Name(",
                            $"@areaDomainCategory, ",
                            $"@areaDomainEtlId, ",
                            $"@labelEn);{Environment.NewLine}");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Execute stored procedure fn_import_check_area_domain_category
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="areaDomainCategory"></param>
                    /// <param name="areaDomainEtlId"></param>
                    /// <param name="labelEn"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> areaDomainCategory,
                        Nullable<int> areaDomainEtlId,
                        string labelEn,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = SQL;

                        CommandText = CommandText.Replace(
                            oldValue: "@areaDomainCategory",
                            newValue: Functions.PrepNIntArg(arg: areaDomainCategory));

                        CommandText = CommandText.Replace(
                            oldValue: "@areaDomainEtlId",
                            newValue: Functions.PrepNIntArg(arg: areaDomainEtlId));

                        CommandText = CommandText.Replace(
                            oldValue: "@labelEn",
                            newValue: Functions.PrepStringArg(arg: labelEn));

                        NpgsqlParameter pAreaDomainCategory = new(
                            parameterName: "areaDomainCategory",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pAreaDomainEtlId = new(
                           parameterName: "areaDomainEtlId",
                           parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pLabelEn = new(
                            parameterName: "labelEn",
                            parameterType: NpgsqlDbType.Varchar);

                        if (areaDomainCategory != null)
                        {
                            pAreaDomainCategory.Value = (int)areaDomainCategory;
                        }
                        else
                        {
                            pAreaDomainCategory.Value = DBNull.Value;
                        }

                        if (areaDomainEtlId != null)
                        {
                            pAreaDomainEtlId.Value = (int)areaDomainEtlId;
                        }
                        else
                        {
                            pAreaDomainEtlId.Value = DBNull.Value;
                        }

                        if (labelEn != null)
                        {
                            pLabelEn.Value = (string)labelEn;
                        }
                        else
                        {
                            pLabelEn.Value = DBNull.Value;
                        }

                        return
                            database.Postgres.ExecuteQuery(
                                sqlCommand: SQL,
                                transaction: transaction,
                                pAreaDomainCategory,
                                pAreaDomainEtlId,
                                pLabelEn);
                    }

                    #endregion Methods

                }

                #endregion FnImportCheckAreaDomainCategory

            }

        }
    }
}