﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {
                // fn_try_pyr_group_refyearset2panel_mapping

                #region FnTryPyrGroupRefYearSetToPanelMapping

                /// <summary>
                /// Wrapper for stored procedure fn_try_pyrgroup_refyearset2panel_mapping.
                /// Function checks if a combination of panels and reference year sets
                /// already exists in t_panel_refyearset_group table.
                /// </summary>
                public static class FnTryPyrGroupRefYearSetToPanelMapping
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_try_pyrgroup_refyearset2panel_mapping";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_refyearset2panels integer[]; ",
                            $"returns: boolean");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    $SchemaName.$Name(@refYearSetToPanelIds);{Environment.NewLine}");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="refYearSetToPanelIds">List of identifiers from table t_panel_refyearset_group</param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(List<Nullable<int>> refYearSetToPanelIds)
                    {
                        string result = SQL;

                        result = result.Replace(
                            oldValue: "@refYearSetToPanelIds",
                            newValue: Functions.PrepNIntArrayArg(args: refYearSetToPanelIds, dbType: "int4"));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_try_pyr_group_refyearset2panel_mapping
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="refYearSetToPanelIds">List of identifiers from table t_panel_refyearset_group</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>DataTable with check result</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        List<Nullable<int>> refYearSetToPanelIds,
                        NpgsqlTransaction transaction = null)
                    {
                        Nullable<bool> val = Execute(
                            database: database,
                            refYearSetToPanelIds: refYearSetToPanelIds,
                            transaction: transaction);

                        DataTable dt = new();

                        dt.Columns.Add(column: new DataColumn()
                        {
                            ColumnName = Name,
                            DataType = Type.GetType(typeName: "System.Boolean")
                        });

                        DataRow row = dt.NewRow();

                        Functions.SetNBoolArg(
                            row: row,
                            name: Name,
                            val: val);

                        dt.Rows.Add(row: row);

                        return dt;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_try_pyr_group_refyearset2panel_mapping
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="refYearSetToPanelIds">List of identifiers from table t_panel_refyearset_group</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Check result</returns>
                    public static Nullable<bool> Execute(
                        NfiEstaDB database,
                        List<Nullable<int>> refYearSetToPanelIds,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(refYearSetToPanelIds: refYearSetToPanelIds);

                        NpgsqlParameter pRefYearSetToPanelIds = new(
                            parameterName: "refYearSetToPanelIds",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        if (refYearSetToPanelIds != null)
                        {
                            pRefYearSetToPanelIds.Value = refYearSetToPanelIds.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pRefYearSetToPanelIds.Value = DBNull.Value;
                        }

                        string strResult = database.Postgres.ExecuteScalar(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pRefYearSetToPanelIds);

                        if (String.IsNullOrEmpty(value: strResult))
                        {
                            return null;
                        }
                        else
                        {
                            if (Boolean.TryParse(value: strResult, result: out bool boolResult))
                            {
                                return boolResult;
                            }
                            else
                            {
                                return null;
                            }
                        }
                    }

                    #endregion Methods

                }

                #endregion FnTryPyrGroupRefYearSetToPanelMapping

            }

        }
    }
}