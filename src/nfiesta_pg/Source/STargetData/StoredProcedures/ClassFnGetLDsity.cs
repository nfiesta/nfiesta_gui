﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {

                #region FnGetLDsity

                /// <summary>
                /// Wrapper for stored procedure fn_get_ldsity.
                /// Function returns records from c_ldsity table,
                /// optionally for given target_variable.
                /// </summary>
                public static class FnGetLDsity
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_get_ldsity";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_target_variable integer[] DEFAULT NULL::integer[], ",
                            $"_ldsity_object_type integer DEFAULT 100; ",
                            $"returns: ",
                            $"TABLE(",
                            $"id integer, ",
                            $"target_variable integer, ",
                            $"label character varying, ",
                            $"description text, ",
                            $"label_en character varying, ",
                            $"description_en text, ",
                            $"column_expression text, ",
                            $"unit_of_measure integer, ",
                            $"ldsity_object integer, ",
                            $"object_label character varying, ",
                            $"object_description text, ",
                            $"object_label_en character varying, ",
                            $"object_description_en text, ",
                            $"ldsity_object_type integer, ",
                            $"object_type_label character varying, ",
                            $"object_type_desc text, ",
                            $"use_negative boolean, ",
                            $"version integer)");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    {TDLDsityList.ColId.SQL(TDLDsityList.Cols, Name)}",
                            $"    {TDLDsityList.ColLabelCs.SQL(TDLDsityList.Cols, Name)}",
                            $"    {TDLDsityList.ColDescriptionCs.SQL(TDLDsityList.Cols, Name)}",
                            $"    {TDLDsityList.ColLabelEn.SQL(TDLDsityList.Cols, Name)}",
                            $"    {TDLDsityList.ColDescriptionEn.SQL(TDLDsityList.Cols, Name)}",
                            $"    {TDLDsityList.ColLDsityObjectId.SQL(TDLDsityList.Cols, Name)}",
                            $"    {TDLDsityList.ColLDsityObjectLabelCs.SQL(TDLDsityList.Cols, Name)}",
                            $"    {TDLDsityList.ColLDsityObjectDescriptionCs.SQL(TDLDsityList.Cols, Name)}",
                            $"    {TDLDsityList.ColLDsityObjectLabelEn.SQL(TDLDsityList.Cols, Name)}",
                            $"    {TDLDsityList.ColLDsityObjectDescriptionEn.SQL(TDLDsityList.Cols, Name)}",
                            $"    {TDLDsityList.ColColumnExpression.SQL(TDLDsityList.Cols, Name)}",
                            $"    {TDLDsityList.ColUnitOfMeasureId.SQL(TDLDsityList.Cols, Name)}",
                            $"    {TDLDsityList.ColAreaDomainCategory.SQL(TDLDsityList.Cols, Name, setToNull: true)}",
                            $"    {TDLDsityList.ColSubPopulationCategory.SQL(TDLDsityList.Cols, Name, setToNull: true)}",
                            $"    {TDLDsityList.ColDefinitionVariant.SQL(TDLDsityList.Cols, Name, setToNull: true)}",
                            $"    {TDLDsityList.ColTargetVariableId.SQL(TDLDsityList.Cols, Name)}",
                            $"    {TDLDsityList.ColLDsityObjectTypeId.SQL(TDLDsityList.Cols, Name)}",
                            $"    {TDLDsityList.ColUseNegative.SQL(TDLDsityList.Cols, Name)}",
                            $"    {TDLDsityList.ColVersionId.SQL(TDLDsityList.Cols, Name, isLastOne: true)}",
                            $"FROM{Environment.NewLine}",
                            $"    $SchemaName.$Name(@targetVariableIds, @ldsityObjectTypeId) AS {Name}{Environment.NewLine}",
                            $"ORDER BY{Environment.NewLine}",
                            $"    {Name}.{TDLDsityList.ColId.DbName};{Environment.NewLine}"
                            );

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Execute stored procedure fn_get_ldsity
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="targetVariableIds">List of target variable identifiers</param>
                    /// <param name="ldsityObjectTypeId">Local density object type identifier</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table with local densities</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        List<Nullable<int>> targetVariableIds = null,
                        Nullable<int> ldsityObjectTypeId = 100,
                        NpgsqlTransaction transaction = null
                        )
                    {
                        CommandText = SQL;

                        CommandText = CommandText.Replace(
                           oldValue: "@targetVariableIds",
                           newValue: Functions.PrepNIntArrayArg(args: targetVariableIds, dbType: "int4"));

                        CommandText = CommandText.Replace(
                            oldValue: "@ldsityObjectTypeId",
                            newValue: Functions.PrepNIntArg(arg: ldsityObjectTypeId));

                        NpgsqlParameter pTargetVariableIds = new(
                            parameterName: "targetVariableIds",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        NpgsqlParameter pLDsityObjectTypeId = new(
                            parameterName: "ldsityObjectTypeId",
                            parameterType: NpgsqlDbType.Integer);

                        if (targetVariableIds != null)
                        {
                            pTargetVariableIds.Value = targetVariableIds.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pTargetVariableIds.Value = DBNull.Value;
                        }

                        if (ldsityObjectTypeId != null)
                        {
                            pLDsityObjectTypeId.Value = (int)ldsityObjectTypeId;
                        }
                        else
                        {
                            pLDsityObjectTypeId.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteQuery(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pTargetVariableIds, pLDsityObjectTypeId);
                    }

                    /// <summary>
                    /// Execute stored procedure fn_get_ldsity
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="targetVariableIds">List of target variable identifiers</param>
                    /// <param name="ldsityObjectTypeId">Local density object type identifier</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>List of local densities</returns>
                    public static TDLDsityList Execute(
                        NfiEstaDB database,
                        List<Nullable<int>> targetVariableIds = null,
                        Nullable<int> ldsityObjectTypeId = 100,
                        NpgsqlTransaction transaction = null
                        )
                    {
                        return new TDLDsityList(
                            database: database,
                            data: ExecuteQuery(
                                database: database,
                                targetVariableIds: targetVariableIds,
                                ldsityObjectTypeId: ldsityObjectTypeId,
                                transaction: transaction))
                        { StoredProcedure = CommandText };
                    }

                    #endregion Methods

                }

                #endregion FnGetLDsity

            }

        }
    }
}