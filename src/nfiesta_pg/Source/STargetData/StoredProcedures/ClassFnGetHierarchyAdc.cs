﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {

                #region FnGetHierarchyAdc

                /// <summary>
                /// Wrapper for stored procedure fn_get_hierarchy.
                /// Function returns records from t_adc_hierarchy table.
                /// </summary>
                public static class FnGetHierarchyAdc
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_get_hierarchy";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_areal_or_pop integer, ",
                            $"_type_superior integer, ",
                            $"_type_inferior integer, ",
                            $"_dependent boolean DEFAULT false, ",
                            $"_null_inferior boolean DEFAULT true; ",
                            $"returns: ",
                            $"TABLE(",
                            $"superior_cat integer, ",
                            $"inferior_cat integer, ",
                            $"dependent boolean)");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    row_number() over()  ::int4    AS {TDADCHierarchyList.ColId.Name},{Environment.NewLine}",
                            $"    {Name}.superior_cat ::integer AS {TDADCHierarchyList.ColVariableSuperiorId.Name},{Environment.NewLine}",
                            $"    {Name}.inferior_cat ::integer AS {TDADCHierarchyList.ColVariableId.Name},{Environment.NewLine}",
                            $"    {Name}.dependent    ::boolean AS {TDADCHierarchyList.ColDependent.Name}{Environment.NewLine}",
                            $"FROM{Environment.NewLine}",
                            $"    $SchemaName.$Name",
                            $"(@arealOrPopulationId, @areaDomainSuperiorId, @areaDomainInferiorId, @dependent, @nullInferior) ",
                            $"AS {Name}{Environment.NewLine}",
                            $"ORDER BY{Environment.NewLine}",
                            $"    {Name}.superior_cat,{Environment.NewLine}",
                            $"    {Name}.inferior_cat;{Environment.NewLine}"
                            );

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Execute stored procedure fn_get_hierarchy
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="areaDomainSuperiorId">Superior area domain identifier</param>
                    /// <param name="areaDomainInferiorId">Inferior area domain identifier</param>
                    /// <param name="dependent"></param>
                    /// <param name="nullInferior"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table with area domain category mappings</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> areaDomainSuperiorId,
                        Nullable<int> areaDomainInferiorId,
                        Nullable<bool> dependent = false,
                        Nullable<bool> nullInferior = true,
                        NpgsqlTransaction transaction = null)
                    {
                        int arealOrPopulationId =
                            (int)TDArealOrPopulationEnum.AreaDomain;

                        CommandText = SQL;

                        CommandText = CommandText.Replace(
                            oldValue: "@arealOrPopulationId",
                            newValue: Functions.PrepIntArg(arg: arealOrPopulationId));

                        CommandText = CommandText.Replace(
                            oldValue: "@areaDomainSuperiorId",
                            newValue: Functions.PrepNIntArg(arg: areaDomainSuperiorId));

                        CommandText = CommandText.Replace(
                            oldValue: "@areaDomainInferiorId",
                            newValue: Functions.PrepNIntArg(arg: areaDomainInferiorId));

                        CommandText = CommandText.Replace(
                            oldValue: "@dependent",
                            newValue: Functions.PrepNBoolArg(arg: dependent));

                        CommandText = CommandText.Replace(
                            oldValue: "@nullInferior",
                            newValue: Functions.PrepNBoolArg(arg: nullInferior));

                        NpgsqlParameter pArealOrPopulationId = new(
                            parameterName: "arealOrPopulationId",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pAreaDomainSuperiorId = new(
                            parameterName: "areaDomainSuperiorId",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pAreaDomainInferiorId = new(
                            parameterName: "areaDomainInferiorId",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pDependent = new(
                            parameterName: "dependent",
                            parameterType: NpgsqlDbType.Boolean);

                        NpgsqlParameter pNullInferior = new(
                            parameterName: "nullInferior",
                            parameterType: NpgsqlDbType.Boolean);

                        pArealOrPopulationId.Value = arealOrPopulationId;

                        if (areaDomainSuperiorId != null)
                        {
                            pAreaDomainSuperiorId.Value = (int)areaDomainSuperiorId;
                        }
                        else
                        {
                            pAreaDomainSuperiorId.Value = DBNull.Value;
                        }

                        if (areaDomainInferiorId != null)
                        {
                            pAreaDomainInferiorId.Value = (int)areaDomainInferiorId;
                        }
                        else
                        {
                            pAreaDomainInferiorId.Value = DBNull.Value;
                        }

                        if (dependent != null)
                        {
                            pDependent.Value = (bool)dependent;
                        }
                        else
                        {
                            pDependent.Value = DBNull.Value;
                        }

                        if (nullInferior != null)
                        {
                            pNullInferior.Value = (bool)nullInferior;
                        }
                        else
                        {
                            pNullInferior.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteQuery(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pArealOrPopulationId, pAreaDomainSuperiorId, pAreaDomainInferiorId,
                            pDependent, pNullInferior);
                    }

                    /// <summary>
                    /// Execute stored procedure fn_get_hierarchy
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="areaDomainSuperiorId">Superior area domain identifier</param>
                    /// <param name="areaDomainInferiorId">Inferior area domain identifier</param>
                    /// <param name="dependent"></param>
                    /// <param name="nullInferior"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>List of area domain category mappings</returns>
                    public static TDADCHierarchyList Execute(
                        NfiEstaDB database,
                        Nullable<int> areaDomainSuperiorId,
                        Nullable<int> areaDomainInferiorId,
                        Nullable<bool> dependent = false,
                        Nullable<bool> nullInferior = true,
                        NpgsqlTransaction transaction = null)
                    {
                        return new TDADCHierarchyList(
                            database: database,
                            data: ExecuteQuery(
                               database: database,
                               areaDomainSuperiorId: areaDomainSuperiorId,
                               areaDomainInferiorId: areaDomainInferiorId,
                               dependent: dependent,
                               nullInferior: nullInferior,
                               transaction: transaction))
                        { StoredProcedure = CommandText };
                    }

                    #endregion Methods

                }

                #endregion FnGetHierarchyAdc

            }

        }
    }
}