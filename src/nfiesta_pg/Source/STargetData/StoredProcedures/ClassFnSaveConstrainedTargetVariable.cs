﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {

                #region FnSaveConstrainedTargetVariable

                /// <summary>
                /// Wrapper for stored procedure fn_save_constrained_target_variable.
                /// Functions inserts records into tables c_target_variable and cm_ldsity2target_variable for given parameters.
                /// It takes already created target variable, mirrors its ldsity contributions
                /// and adds a constraint for given area domain and sub population categories.
                /// </summary>
                public static class FnSaveConstrainedTargetVariable
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_save_constrained_target_variable";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_target_variable integer, ",
                            $"_target_variable_cm integer[], ",
                            $"_area_domain_category integer[], ",
                            $"_adc_object integer[], ",
                            $"_sub_population_category integer[], ",
                            $"_spc_object integer[]; ",
                            $"returns: integer");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    $SchemaName.$Name{Environment.NewLine}",
                            $"({Environment.NewLine}",
                            $"@targetVariableId,          -- _target_variable{Environment.NewLine}",
                            $"@targetVariableCm,          -- _target_variable_cm{Environment.NewLine}",
                            $"@areaDomainCategoryIds,     -- _area_domain_category{Environment.NewLine}",
                            $"@adcObjectIds,              -- _adc_object{Environment.NewLine}",
                            $"@subPopulationCategoryIds,  -- _sub_population_category{Environment.NewLine}",
                            $"@spcObjectIds               -- _spc_object{Environment.NewLine}",
                            $")::int4 AS $Name;{Environment.NewLine}");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Execute stored procedure fn_save_constrained_target_variable
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="targetVariableId"></param>
                    /// <param name="targetVariableCm"></param>
                    /// <param name="areaDomainCategoryIds"></param>
                    /// <param name="adcObjectIds"></param>
                    /// <param name="subPopulationCategoryIds"></param>
                    /// <param name="spcObjectIds"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns></returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> targetVariableId,
                        List<Nullable<int>> targetVariableCm,
                        List<List<Nullable<int>>> areaDomainCategoryIds,
                        List<List<Nullable<int>>> adcObjectIds,
                        List<List<Nullable<int>>> subPopulationCategoryIds,
                        List<List<Nullable<int>>> spcObjectIds,
                        NpgsqlTransaction transaction = null)
                    {
                        Nullable<int> val = Execute(
                            database: database,
                            targetVariableId: targetVariableId,
                            targetVariableCm: targetVariableCm,
                            areaDomainCategoryIds: areaDomainCategoryIds,
                            adcObjectIds: adcObjectIds,
                            subPopulationCategoryIds: subPopulationCategoryIds,
                            spcObjectIds: spcObjectIds,
                            transaction: transaction);

                        DataTable dt = new();

                        dt.Columns.Add(column: new DataColumn()
                        {
                            ColumnName = Name,
                            DataType = Type.GetType(typeName: "System.Int32")
                        });

                        DataRow row = dt.NewRow();

                        Functions.SetNIntArg(
                            row: row,
                            name: Name,
                            val: val);

                        dt.Rows.Add(row: row);

                        return dt;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_save_constrained_target_variable
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="targetVariableId"></param>
                    /// <param name="targetVariableCm"></param>
                    /// <param name="areaDomainCategoryIds"></param>
                    /// <param name="adcObjectIds"></param>
                    /// <param name="subPopulationCategoryIds"></param>
                    /// <param name="spcObjectIds"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns></returns>
                    public static Nullable<int> Execute(
                        NfiEstaDB database,
                        Nullable<int> targetVariableId,
                        List<Nullable<int>> targetVariableCm,
                        List<List<Nullable<int>>> areaDomainCategoryIds,
                        List<List<Nullable<int>>> adcObjectIds,
                        List<List<Nullable<int>>> subPopulationCategoryIds,
                        List<List<Nullable<int>>> spcObjectIds,
                        NpgsqlTransaction transaction = null)
                    {
                        string sql = SQL;

                        sql = sql.Replace(
                            oldValue: "@areaDomainCategoryIds",
                            newValue: Functions.PrepNIntArrayOfArrayArg(args: areaDomainCategoryIds, dbType: "int4"));

                        sql = sql.Replace(
                            oldValue: "@adcObjectIds",
                            newValue: Functions.PrepNIntArrayOfArrayArg(args: adcObjectIds, dbType: "int4"));

                        sql = sql.Replace(
                           oldValue: "@subPopulationCategoryIds",
                           newValue: Functions.PrepNIntArrayOfArrayArg(args: subPopulationCategoryIds, dbType: "int4"));

                        sql = sql.Replace(
                            oldValue: "@spcObjectIds",
                            newValue: Functions.PrepNIntArrayOfArrayArg(args: spcObjectIds, dbType: "int4"));

                        CommandText = sql;

                        CommandText = CommandText.Replace(
                            oldValue: "@targetVariableId",
                            newValue: Functions.PrepNIntArg(arg: targetVariableId));

                        CommandText = CommandText.Replace(
                            oldValue: "@targetVariableCm",
                            newValue: Functions.PrepNIntArrayArg(args: targetVariableCm, dbType: "int4"));

                        NpgsqlParameter pTargetVariableId = new(
                            parameterName: "targetVariableId",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pTargetVariableCm = new(
                            parameterName: "targetVariableCm",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        if (targetVariableId != null)
                        {
                            pTargetVariableId.Value = (int)targetVariableId;
                        }
                        else
                        {
                            pTargetVariableId.Value = DBNull.Value;
                        }

                        if (targetVariableCm != null)
                        {
                            pTargetVariableCm.Value = targetVariableCm.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pTargetVariableCm.Value = DBNull.Value;
                        }

                        string strResult = database.Postgres.ExecuteScalar(
                            sqlCommand: sql,
                            transaction: transaction,
                            pTargetVariableId, pTargetVariableCm);

                        if (String.IsNullOrEmpty(value: strResult))
                        {
                            return null;
                        }
                        else
                        {
                            if (Int32.TryParse(s: strResult, result: out int intResult))
                            {
                                return intResult;
                            }
                            else
                            {
                                return null;
                            }
                        }
                    }

                    #endregion Methods

                }

                #endregion FnSaveConstrainedTargetVariable

            }

        }
    }
}