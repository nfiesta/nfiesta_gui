﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {

                #region FnGetLDsityObjectsForTargetVariable

                /// <summary>
                /// Wrapper for stored procedure fn_get_ldsity_objects4target_variable.
                /// The function for the specified target variable
                /// returns a list of ldsity objects.
                /// </summary>
                public static class FnGetLDsityObjectsForTargetVariable
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_get_ldsity_objects4target_variable";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_target_variable integer; ",
                            $"returns: ",
                            $"TABLE(",
                            $"id_cm_ldsity2target_variable integer, ",
                            $"ldsity_object integer, ",
                            $"use_negative boolean, ",
                            $"ldsity_object_type integer)");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    $Name.id_cm_ldsity2target_variable    AS cm_ldsity2target_variable_id,{Environment.NewLine}",
                            $"    $Name.ldsity_object                   AS ldsity_object_id,{Environment.NewLine}",
                            $"    $Name.use_negative                    AS use_negative,{Environment.NewLine}",
                            $"    $Name.ldsity_object_type              AS ldsity_object_type_id{Environment.NewLine}",
                            $"FROM{Environment.NewLine}",
                            $"    $SchemaName.$Name(",
                            $"@targetVariableId",
                            $") AS $Name{Environment.NewLine}",
                            $"ORDER BY{Environment.NewLine}",
                            $"    $Name.ldsity_object;{Environment.NewLine}"
                            );

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Execute stored procedure fn_get_ldsity_objects4target_variable
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="targetVariableId">Target variable identifier</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>DataTable</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> targetVariableId,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = SQL;

                        CommandText = CommandText.Replace(
                            oldValue: "@targetVariableId",
                            newValue: Functions.PrepNIntArg(targetVariableId));

                        NpgsqlParameter pTargetVariableId = new(
                            parameterName: "targetVariableId",
                            parameterType: NpgsqlDbType.Integer);

                        if (targetVariableId != null)
                        {
                            pTargetVariableId.Value = (int)targetVariableId;
                        }
                        else
                        {
                            pTargetVariableId.Value = DBNull.Value;
                        }

                        return
                            database.Postgres.ExecuteQuery(
                                sqlCommand: SQL,
                                transaction: transaction,
                                pTargetVariableId);
                    }

                    /// <summary>
                    /// Execute stored procedure fn_get_ldsity_objects4target_variable
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="targetVariableId">Target variable identifier</param>
                    /// <param name="code">Error code</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Dictionary</returns>
                    public static Dictionary<DataObjectIdentifier, DataObject> Execute(
                        NfiEstaDB database,
                        Nullable<int> targetVariableId,
                        out ErrorCode code,
                        NpgsqlTransaction transaction = null)
                    {
                        DataTable dt = ExecuteQuery(
                            database: database,
                            targetVariableId: targetVariableId,
                            transaction: transaction);

                        TDLDsityObjectList cLDsityObject
                            = new(database: database);
                        cLDsityObject.ReLoad();

                        TDLDsityObjectTypeList cLDsityObjectType
                            = new(database: database);
                        cLDsityObjectType.ReLoad();

                        Dictionary<DataObjectIdentifier, DataObject> result = [];

                        foreach (DataRow row in dt.Rows)
                        {
                            Nullable<int> cmLDsityToTargetVariableId =
                                Functions.GetNIntArg(
                                        row: row,
                                        name: "cm_ldsity2target_variable_id",
                                        defaultValue: null);

                            Nullable<int> cLDsityObjectId =
                                 Functions.GetNIntArg(
                                        row: row,
                                        name: "ldsity_object_id",
                                        defaultValue: null);

                            Nullable<bool> useNegative =
                                Functions.GetNBoolArg(
                                    row: row,
                                    name: "use_negative",
                                    defaultValue: null);

                            Nullable<int> cLDsityObjectTypeId =
                                 Functions.GetNIntArg(
                                        row: row,
                                        name: "ldsity_object_type_id",
                                        defaultValue: null);

                            DataObjectIdentifier key = new(
                                ldsityObject: (cLDsityObjectId == null) ? null : cLDsityObject[(int)cLDsityObjectId],
                                useNegative: useNegative);

                            DataObject val = new(
                                cmLDsityToTargetVariableId: cmLDsityToTargetVariableId,
                                useNegative: useNegative,
                                ldsityObject: (cLDsityObjectId == null) ? null : cLDsityObject[(int)cLDsityObjectId],
                                ldsityObjectType: (cLDsityObjectTypeId == null) ? null : cLDsityObjectType[(int)cLDsityObjectTypeId]
                                );

                            if (key.ContainsNull)
                            {
                                code = ErrorCode.NullKey;
                                return null;
                            }

                            if (val.ContainsNull)
                            {
                                code = ErrorCode.NullValue;
                                return null;
                            }

                            if (result.ContainsKey(key: key))
                            {
                                code = ErrorCode.DuplicateKey;
                                return null;
                            }

                            if (result.Values
                                .Where(a => a.CmLDsityToTargetVariableId == val.CmLDsityToTargetVariableId)
                                .Any())
                            {
                                code = ErrorCode.DuplicateValue;
                                return null;
                            }

                            result.Add(
                                key: key,
                                value: val);
                        }

                        code = ErrorCode.None;

                        return result
                            .OrderBy(a => a.Key)
                            .ToDictionary(a => a.Key, a => a.Value);
                    }

                    #endregion Methods


                    #region Internal Classes

                    /// <summary>
                    /// Data object identifier
                    /// </summary>
                    /// <remarks>
                    /// Constructor
                    /// </remarks>
                    /// <param name="ldsityObject">Local density object</param>
                    /// <param name="useNegative">Use negative local density contribution</param>
                    public class DataObjectIdentifier(
                        TDLDsityObject ldsityObject,
                        Nullable<bool> useNegative)
                            : IComparable<DataObjectIdentifier>
                    {

                        #region Private Fields

                        /// <summary>
                        /// Local density object
                        /// </summary>
                        private readonly TDLDsityObject ldsityObject = ldsityObject;

                        /// <summary>
                        /// Use negative local density contribution
                        /// </summary>
                        private readonly Nullable<bool> useNegative = useNegative;

                        /// <summary>
                        /// Identifier contains null values
                        /// </summary>
                        private readonly bool containsNull =
                                (ldsityObject == null) ||
                                (useNegative == null);

                        #endregion Private Fields


                        #region Properties

                        /// <summary>
                        /// Local density object identifier (read-only)
                        /// </summary>
                        public int LDsityObjectId
                        {
                            get
                            {
                                return
                                    (ldsityObject == null) ? 0 : ldsityObject.Id;
                            }
                        }

                        /// <summary>
                        /// Local density object (read-only)
                        /// </summary>
                        public TDLDsityObject LDsityObject
                        {
                            get
                            {
                                return ldsityObject;
                            }
                        }

                        /// <summary>
                        /// Use negative local density contribution (read-only)
                        /// </summary>
                        public bool UseNegative
                        {
                            get
                            {
                                return useNegative ?? false;
                            }
                        }

                        /// <summary>
                        /// Identifier contains null values
                        /// </summary>
                        public bool ContainsNull
                        {
                            get
                            {
                                return containsNull;
                            }
                        }

                        #endregion Properties


                        #region Methods

                        /// <summary>
                        /// Compares this instance of data object identifier
                        /// to a specified instance of data object identifier
                        /// and returns an indication of their relative values
                        /// </summary>
                        /// <param name="other">Specified instance of data object identifier</param>
                        /// <returns>Indication of their relative values</returns>
                        public int CompareTo(DataObjectIdentifier other)
                        {
                            if (LDsityObjectId < other.LDsityObjectId) return -1;
                            if (LDsityObjectId > other.LDsityObjectId) return 1;
                            if (!UseNegative && other.UseNegative) return -1;
                            if (UseNegative && !other.UseNegative) return 1;
                            return 0;
                        }

                        /// <summary>
                        /// Returns a string that represents the current object
                        /// </summary>
                        /// <returns>Returns a string that represents the current object</returns>
                        public override string ToString()
                        {
                            char useNegativeIdentifier = (UseNegative) ? (char)49 : (char)48;
                            return $"{LDsityObjectId}-{useNegativeIdentifier}";
                        }

                        /// <summary>
                        /// Determines whether specified object is equal to the current object
                        /// </summary>
                        /// <param name="obj">Speciefied object</param>
                        /// <returns>true/false</returns>
                        public override bool Equals(object obj)
                        {
                            // If the passed object is null, return False
                            if (obj == null)
                            {
                                return false;
                            }

                            // If the passed object is not DataObjectIdentifier Type, return False
                            if (obj is not DataObjectIdentifier)
                            {
                                return false;
                            }

                            return
                                LDsityObjectId == ((DataObjectIdentifier)obj).LDsityObjectId &&
                                UseNegative == ((DataObjectIdentifier)obj).UseNegative;
                        }

                        /// <summary>
                        /// Returns the hash code
                        /// </summary>
                        /// <returns>Hash code</returns>
                        public override int GetHashCode()
                        {
                            return
                                LDsityObjectId.GetHashCode() ^
                                UseNegative.GetHashCode();
                        }

                        #endregion Methods

                    }

                    /// <summary>
                    /// Data object - return type of the function FnGetLDsityObjectsForTargetVariable
                    /// </summary>
                    /// <remarks>
                    /// Constructor
                    /// </remarks>
                    /// <param name="cmLDsityToTargetVariableId">Identifier from database table cm_ldsity2target_variable</param>
                    /// <param name="useNegative">Use negative local density contribution</param>
                    /// <param name="ldsityObject">Local density object</param>
                    /// <param name="ldsityObjectType">Local density object type</param>
                    public class DataObject(
                        Nullable<int> cmLDsityToTargetVariableId,
                        Nullable<bool> useNegative,
                        TDLDsityObject ldsityObject,
                        TDLDsityObjectType ldsityObjectType)
                    {

                        #region Private Fields

                        /// <summary>
                        /// Identifier from database table cm_ldsity2target_variable
                        /// </summary>
                        private readonly int cmLDsityToTargetVariableId = cmLDsityToTargetVariableId ?? 0;

                        /// <summary>
                        /// Use negative local density contribution
                        /// </summary>
                        private readonly bool useNegative = useNegative ?? false;

                        /// <summary>
                        /// Local density object
                        /// </summary>
                        private readonly TDLDsityObject ldsityObject = ldsityObject;

                        /// <summary>
                        /// Local density object type
                        /// </summary>
                        private readonly TDLDsityObjectType ldsityObjectType = ldsityObjectType;

                        /// <summary>
                        /// Identifier contains null values
                        /// </summary>
                        private readonly bool containsNull =
                                (cmLDsityToTargetVariableId == null) ||
                                (useNegative == null) ||
                                (ldsityObject == null) ||
                                (ldsityObjectType == null);

                        #endregion Private Fields


                        #region Properties

                        /// <summary>
                        /// Identifier from database table cm_ldsity2target_variable (read-only)
                        /// </summary>
                        public int CmLDsityToTargetVariableId
                        {
                            get
                            {
                                return cmLDsityToTargetVariableId;
                            }
                        }

                        /// <summary>
                        /// Use negative local density contribution (read-only)
                        /// </summary>
                        public bool UseNegative
                        {
                            get
                            {
                                return useNegative;
                            }
                        }

                        /// <summary>
                        /// Local density object (read-only)
                        /// </summary>
                        public TDLDsityObject LDsityObject
                        {
                            get
                            {
                                return ldsityObject;
                            }
                        }

                        /// <summary>
                        /// Local density object type (read-only)
                        /// </summary>
                        public TDLDsityObjectType LDsityObjectType
                        {
                            get
                            {
                                return ldsityObjectType;
                            }
                        }

                        /// <summary>
                        /// Data object contains null values
                        /// </summary>
                        public bool ContainsNull
                        {
                            get
                            {
                                return containsNull;
                            }
                        }

                        #endregion Properties


                        #region Methods

                        /// <summary>
                        /// Returns a string that represents the current object
                        /// </summary>
                        /// <returns>Returns a string that represents the current object</returns>
                        public override string ToString()
                        {
                            return $"{cmLDsityToTargetVariableId}";
                        }

                        /// <summary>
                        /// Determines whether specified object is equal to the current object
                        /// </summary>
                        /// <param name="obj">Speciefied object</param>
                        /// <returns>true/false</returns>
                        public override bool Equals(object obj)
                        {
                            // If the passed object is null, return False
                            if (obj == null)
                            {
                                return false;
                            }

                            // If the passed object is not DataObjectIdentifier Type, return False
                            if (obj is not DataObject)
                            {
                                return false;
                            }

                            return
                                CmLDsityToTargetVariableId == ((DataObject)obj).CmLDsityToTargetVariableId &&
                                UseNegative == ((DataObject)obj).UseNegative &&
                                LDsityObject.Equals(obj: ((DataObject)obj).LDsityObject) &&
                                LDsityObjectType.Equals(obj: ((DataObject)obj).LDsityObjectType);
                        }

                        /// <summary>
                        /// Returns the hash code
                        /// </summary>
                        /// <returns>Hash code</returns>
                        public override int GetHashCode()
                        {
                            return
                                CmLDsityToTargetVariableId.GetHashCode() ^
                                UseNegative.GetHashCode() ^
                                LDsityObject.GetHashCode() ^
                                LDsityObjectType.GetHashCode();
                        }

                        #endregion Methods

                    }

                    /// <summary>
                    /// Error code
                    /// </summary>
                    public enum ErrorCode
                    {
                        /// <summary>
                        /// None
                        /// </summary>
                        None = 0,

                        /// <summary>
                        /// Local density object identifier must not be null
                        /// </summary>
                        NullKey = 1,

                        /// <summary>
                        /// Corresponding identifier of mapping between local density contribution and target variable must not be null
                        /// </summary>
                        NullValue = 2,

                        /// <summary>
                        /// Only one corresponding identifier of maping between local density contribution and target variable is allowed for local density object
                        /// </summary>
                        DuplicateKey = 3,

                        /// <summary>
                        /// Only local density object is allowed for one corresponding identifier of maping between local density contribution and target variable
                        /// </summary>
                        DuplicateValue = 4
                    }

                    #endregion Internal Classes

                }

                #endregion FnGetLDsityObjectsForTargetVariable

            }

        }
    }
}