﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {

                #region FnGetPlots

                /// <summary>
                /// Wrapper for stored procedure fn_get_plots.
                /// The function for the specified list of panels and reference_year_sets
                /// returns a list of points for local densities.
                /// </summary>
                public static class FnGetPlots
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_get_plots";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_refyearset2panel_mapping integer[], ",
                            $"_state_or_change integer, ",
                            $"_use_negative boolean; ",
                            $"returns: ",
                            $"TABLE(",
                            $"gid integer, ",
                            $"cluster_configuration integer, ",
                            $"reference_year_set4join integer, ",
                            $"reference_year_set integer, ",
                            $"panel integer, ",
                            $"stratum integer)");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    gid,{Environment.NewLine}",
                            $"    cluster_configuration,{Environment.NewLine}",
                            $"    reference_year_set4join,{Environment.NewLine}",
                            $"    reference_year_set,{Environment.NewLine}",
                            $"    panel,{Environment.NewLine}",
                            $"    stratum{Environment.NewLine}",
                            $"FROM{Environment.NewLine}",
                            $"    $SchemaName.$Name(",
                            $"@refYearSetToPanelMappingIds, ",
                            $"@stateOrChangeId, ",
                            $"@useNegative",
                            $"){Environment.NewLine}",
                            $"ORDER BY{Environment.NewLine}",
                            $"    gid{Environment.NewLine}"
                            );

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Execute stored procedure fn_get_plots
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="refYearSetToPanelMappingIds">List of identifiers from mapping table sdesign.cm_refyearset2panel_mapping</param>
                    /// <param name="stateOrChangeId">State or change identifier</param>
                    /// <param name="useNegative">Positive or negative local density contribution</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table with list of points for local densities</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        List<Nullable<int>> refYearSetToPanelMappingIds,
                        Nullable<int> stateOrChangeId,
                        Nullable<bool> useNegative,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = SQL;

                        CommandText = CommandText.Replace(
                            oldValue: "@refYearSetToPanelMappingIds",
                            newValue: Functions.PrepNIntArrayArg(args: refYearSetToPanelMappingIds, dbType: "int4"));

                        CommandText = CommandText.Replace(
                            oldValue: "@stateOrChangeId",
                            newValue: Functions.PrepNIntArg(arg: stateOrChangeId));

                        CommandText = CommandText.Replace(
                            oldValue: "@useNegative",
                            newValue: Functions.PrepNBoolArg(arg: useNegative));

                        NpgsqlParameter pRefYearSetToPanelMappingIds = new(
                            parameterName: "refYearSetToPanelMappingIds",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        NpgsqlParameter pStateOrChangeId = new(
                            parameterName: "stateOrChangeId",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pUseNegative = new(
                            parameterName: "useNegative",
                            parameterType: NpgsqlDbType.Boolean);

                        if (refYearSetToPanelMappingIds != null)
                        {
                            pRefYearSetToPanelMappingIds.Value = refYearSetToPanelMappingIds.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pRefYearSetToPanelMappingIds.Value = DBNull.Value;
                        }

                        if (stateOrChangeId != null)
                        {
                            pStateOrChangeId.Value = (int)stateOrChangeId;
                        }
                        else
                        {
                            pStateOrChangeId.Value = DBNull.Value;
                        }

                        if (useNegative != null)
                        {
                            pUseNegative.Value = (bool)useNegative;
                        }
                        else
                        {
                            pUseNegative.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteQuery(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pRefYearSetToPanelMappingIds, pStateOrChangeId, pUseNegative);
                    }

                    #endregion Methods

                }

                #endregion FnGetPlots

            }

        }
    }
}