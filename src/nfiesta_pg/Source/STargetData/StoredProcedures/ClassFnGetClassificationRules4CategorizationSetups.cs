﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {

                #region FnGetClassificationRules4CategorizationSetups

                /// <summary>
                /// Wrapper for stored procedure fn_get_classification_rules4categorization_setups.
                /// The function gets text of classification rules for input categorization setups.
                /// </summary>
                public static class FnGetClassificationRules4CategorizationSetups
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_get_classification_rules4categorization_setups";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_categorization_setups integer[], ",
                            $"_ldsity2target_variable integer; ",
                            $"returns: text");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    $SchemaName.$Name(",
                            $"@categorizationSetupIds, ",
                            $"@ldsityToTargetVariableId",
                            $")::text AS $Name;");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Execute stored procedure fn_get_classification_rules4categorization_setups
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="categorizationSetupIds">List of categorization setup identifiers</param>
                    /// <param name="ldsityToTargetVariableId">Mapping between local density and target variable identifier</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table with classification rules for input categorization setups</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        List<Nullable<int>> categorizationSetupIds,
                        int ldsityToTargetVariableId,
                        NpgsqlTransaction transaction = null)
                    {
                        string val = Execute(
                            database: database,
                            categorizationSetupIds: categorizationSetupIds,
                            ldsityToTargetVariableId: ldsityToTargetVariableId,
                            transaction: transaction);

                        DataTable dt = new();

                        dt.Columns.Add(column: new DataColumn()
                        {
                            ColumnName = Name,
                            DataType = Type.GetType(typeName: "System.String")
                        });

                        DataRow row = dt.NewRow();

                        Functions.SetStringArg(
                            row: row,
                            name: Name,
                            val: val);

                        dt.Rows.Add(row: row);

                        return dt;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_get_classification_rules4categorization_setups
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="categorizationSetupIds">List of categorization setup identifiers</param>
                    /// <param name="ldsityToTargetVariableId">Mapping between local density and target variable identifier</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Classification rules for input categorization setups</returns>
                    public static string Execute(
                        NfiEstaDB database,
                        List<Nullable<int>> categorizationSetupIds,
                        Nullable<int> ldsityToTargetVariableId,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = SQL;

                        CommandText = CommandText.Replace(
                            oldValue: "@categorizationSetupIds",
                            newValue: Functions.PrepNIntArrayArg(args: categorizationSetupIds, dbType: "int4"));

                        CommandText = CommandText.Replace(
                            oldValue: "@ldsityToTargetVariableId",
                            newValue: Functions.PrepNIntArg(arg: ldsityToTargetVariableId));

                        NpgsqlParameter pCategorizationSetupIds = new(
                            parameterName: "categorizationSetupIds",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        NpgsqlParameter pLDsityToTargetVariableId = new(
                            parameterName: "ldsityToTargetVariableId",
                            parameterType: NpgsqlDbType.Integer);

                        if (categorizationSetupIds != null)
                        {
                            pCategorizationSetupIds.Value = categorizationSetupIds.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pCategorizationSetupIds.Value = DBNull.Value;
                        }

                        if (ldsityToTargetVariableId != null)
                        {
                            pLDsityToTargetVariableId.Value = (int)ldsityToTargetVariableId;
                        }
                        else
                        {
                            pLDsityToTargetVariableId.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteScalar(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pCategorizationSetupIds, pLDsityToTargetVariableId);
                    }

                    #endregion Methods

                }

                #endregion FnGetClassificationRules4CategorizationSetups

            }

        }
    }
}