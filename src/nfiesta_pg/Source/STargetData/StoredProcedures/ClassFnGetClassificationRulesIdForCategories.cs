﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {

                #region FnGetClassificationRulesIdForCategories

                /// <summary>
                /// Wrapper for stored procedure fn_get_classification_rules_id4categories.
                /// The function for the area domain or sub population returns a combination of attribute classifications
                /// and database identifiers of their classifiaction_rules.
                /// </summary>
                public static class FnGetClassificationRulesIdForCategories
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_get_classification_rules_id4categories";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_area_domain integer[], ",
                            $"_area_domain_object integer[], ",
                            $"_sub_population integer[], ",
                            $"_sub_population_object integer[], ",
                            $"_use_negative_ad boolean[], ",
                            $"_use_negative_sp boolean[]; ",
                            $"returns: ",
                            $"TABLE(",
                            $"area_domain_category integer[], ",
                            $"sub_population_category integer[], ",
                            $"adc2classification_rule integer[], ",
                            $"spc2classification_rule integer[])");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    array_to_string(area_domain_category, ';', 'NULL')     AS area_domain_category,{Environment.NewLine}",
                            $"    array_to_string(sub_population_category, ';', 'NULL')  AS sub_population_category,{Environment.NewLine}",
                            $"    array_to_string(adc2classification_rule, ';', 'NULL')  AS adc2classification_rule,{Environment.NewLine}",
                            $"    array_to_string(spc2classification_rule, ';', 'NULL')  AS spc2classification_rule{Environment.NewLine}",
                            $"FROM{Environment.NewLine}",
                            $"    $SchemaName.$Name(",
                            $"@areaDomainIds, @areaDomainLDsityObjectIds, ",
                            $"@subPopulationIds, @subPopulationLDsityObjectIds, ",
                            $"@useNegativeAd, @useNegativeSp);{Environment.NewLine}"
                            );

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Execute stored procedure fn_get_classification_rules_id4categories
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="areaDomainIds">List of area domain identifiers</param>
                    /// <param name="areaDomainLDsityObjectIds">List of local density object identifiers for area domain category</param>
                    /// <param name="subPopulationIds">List of subpopulation identifiers</param>
                    /// <param name="subPopulationLDsityObjectIds">List of local density object identifiers for subpopulation category</param>
                    /// <param name="useNegativeAd"></param>
                    /// <param name="useNegativeSp">y</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table with lists of area_domain_category, sub_population_category,
                    /// adc2classification_rule, spc2classification_rule</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        List<Nullable<int>> areaDomainIds,
                        List<Nullable<int>> areaDomainLDsityObjectIds,
                        List<Nullable<int>> subPopulationIds,
                        List<Nullable<int>> subPopulationLDsityObjectIds,
                        List<Nullable<bool>> useNegativeAd,
                        List<Nullable<bool>> useNegativeSp,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = SQL;

                        CommandText = CommandText.Replace(
                            oldValue: "@areaDomainIds",
                            newValue: Functions.PrepNIntArrayArg(args: areaDomainIds, dbType: "int4"));

                        CommandText = CommandText.Replace(
                            oldValue: "@areaDomainLDsityObjectIds",
                            newValue: Functions.PrepNIntArrayArg(args: areaDomainLDsityObjectIds, dbType: "int4"));

                        CommandText = CommandText.Replace(
                            oldValue: "@subPopulationIds",
                            newValue: Functions.PrepNIntArrayArg(args: subPopulationIds, dbType: "int4"));

                        CommandText = CommandText.Replace(
                            oldValue: "@subPopulationLDsityObjectIds",
                            newValue: Functions.PrepNIntArrayArg(args: subPopulationLDsityObjectIds, dbType: "int4"));

                        CommandText = CommandText.Replace(
                           oldValue: "@useNegativeAd",
                           newValue: Functions.PrepNBoolArrayArg(args: useNegativeAd, dbType: "bool"));

                        CommandText = CommandText.Replace(
                           oldValue: "@useNegativeSp",
                           newValue: Functions.PrepNBoolArrayArg(args: useNegativeSp, dbType: "bool"));

                        NpgsqlParameter pAreaDomainIds = new(
                            parameterName: "areaDomainIds",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        NpgsqlParameter pAreaDomainLDsityObjectIds = new(
                            parameterName: "areaDomainLDsityObjectIds",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        NpgsqlParameter pSubPopulationIds = new(
                            parameterName: "subPopulationIds",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        NpgsqlParameter pSubPopulationLDsityObjectIds = new(
                            parameterName: "subPopulationLDsityObjectIds",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        NpgsqlParameter pUseNegativeAd = new(
                            parameterName: "useNegativeAd",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Boolean);

                        NpgsqlParameter pUseNegativeSp = new(
                            parameterName: "useNegativeSp",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Boolean);

                        if (areaDomainIds != null)
                        {
                            pAreaDomainIds.Value = areaDomainIds.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pAreaDomainIds.Value = DBNull.Value;
                        }

                        if (areaDomainLDsityObjectIds != null)
                        {
                            pAreaDomainLDsityObjectIds.Value = areaDomainLDsityObjectIds.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pAreaDomainLDsityObjectIds.Value = DBNull.Value;
                        }

                        if (subPopulationIds != null)
                        {
                            pSubPopulationIds.Value = subPopulationIds.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pSubPopulationIds.Value = DBNull.Value;
                        }

                        if (subPopulationLDsityObjectIds != null)
                        {
                            pSubPopulationLDsityObjectIds.Value = subPopulationLDsityObjectIds.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pSubPopulationLDsityObjectIds.Value = DBNull.Value;
                        }

                        if (useNegativeAd != null)
                        {
                            pUseNegativeAd.Value = useNegativeAd.ToArray<Nullable<bool>>();
                        }
                        else
                        {
                            pUseNegativeAd.Value = DBNull.Value;
                        }

                        if (useNegativeSp != null)
                        {
                            pUseNegativeSp.Value = useNegativeSp.ToArray<Nullable<bool>>();
                        }
                        else
                        {
                            pUseNegativeSp.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteQuery(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pAreaDomainIds, pAreaDomainLDsityObjectIds,
                            pSubPopulationIds, pSubPopulationLDsityObjectIds,
                            pUseNegativeAd, pUseNegativeSp);
                    }

                    #endregion Methods

                }

                #endregion FnGetClassificationRulesIdForCategories

            }

        }
    }
}