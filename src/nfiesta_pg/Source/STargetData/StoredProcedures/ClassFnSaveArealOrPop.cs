﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {

                #region FnSaveArealOrPop

                /// <summary>
                /// Wrapper for stored procedure fn_save_areal_or_pop.
                /// Functions inserts or updates a record
                /// in table c_area_domain or c_sub_population
                /// based on given parameters.
                /// </summary>
                public static class FnSaveArealOrPop
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_save_areal_or_pop";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_areal_or_population integer, ",
                            $"_label character varying, ",
                            $"_description text, ",
                            $"_label_en character varying, ",
                            $"_description_en text, ",
                            $"_classification_type integer DEFAULT 100, ",
                            $"_id integer DEFAULT NULL::integer; ",
                            $"returns: integer");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    $SchemaName.$Name(",
                            $"@arealOrPopulationId, ",
                            $"@labelCs, ",
                            $"@descriptionCs, ",
                            $"@labelEn, ",
                            $"@descriptionEn, ",
                            $"@classificationType, ",
                            $"@id)::integer AS $Name;");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Execute stored procedure fn_save_areal_or_pop
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="arealOrPopulationId">Areal or population identifier</param>
                    /// <param name="labelCs">Label of area domain or subpopulation in national language</param>
                    /// <param name="descriptionCs">Description of area domain or subpopulation in national language</param>
                    /// <param name="labelEn">Label of area domain or subpopulation in English</param>
                    /// <param name="descriptionEn">Description of area domain or subpopulation in English</param>
                    /// <param name="classificationType">Classification type</param>
                    /// <param name="id">Identifier of area domain or subpopulation</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> arealOrPopulationId,
                        string labelCs,
                        string descriptionCs,
                        string labelEn,
                        string descriptionEn,
                        TDClassificationTypeEnum classificationType,
                        Nullable<int> id = null,
                        NpgsqlTransaction transaction = null)
                    {
                        Nullable<int> val = Execute(
                            database: database,
                            arealOrPopulationId: arealOrPopulationId,
                            labelCs: labelCs,
                            descriptionCs: descriptionCs,
                            labelEn: labelEn,
                            descriptionEn: descriptionEn,
                            classificationType: classificationType,
                            id: id,
                            transaction: transaction);

                        DataTable dt = new();

                        dt.Columns.Add(column: new DataColumn()
                        {
                            ColumnName = Name,
                            DataType = Type.GetType(typeName: "System.Int32")
                        });

                        DataRow row = dt.NewRow();

                        Functions.SetNIntArg(
                            row: row,
                            name: Name,
                            val: val);

                        dt.Rows.Add(row: row);

                        return dt;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_save_areal_or_pop
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="arealOrPopulationId">Areal or population identifier</param>
                    /// <param name="labelCs">Label of area domain or subpopulation in national language</param>
                    /// <param name="descriptionCs">Description of area domain or subpopulation in national language</param>
                    /// <param name="labelEn">Label of area domain or subpopulation in English</param>
                    /// <param name="descriptionEn">Description of area domain or subpopulation in English</param>
                    /// <param name="classificationType">Classification type</param>
                    /// <param name="id">Identifier of area domain or subpopulation</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Area domain or subpopulation identifier</returns>
                    public static Nullable<int> Execute(
                        NfiEstaDB database,
                        Nullable<int> arealOrPopulationId,
                        string labelCs,
                        string descriptionCs,
                        string labelEn,
                        string descriptionEn,
                        TDClassificationTypeEnum classificationType,
                        Nullable<int> id = null,
                        NpgsqlTransaction transaction = null)
                    {
                        if (classificationType == TDClassificationTypeEnum.Unknown)
                        {
                            return null;
                        }

                        Nullable<int> classificationTypeId = (int)classificationType;

                        CommandText = SQL;

                        CommandText = CommandText.Replace(
                            oldValue: "@arealOrPopulationId",
                            newValue: Functions.PrepNIntArg(arg: arealOrPopulationId));

                        CommandText = CommandText.Replace(
                            oldValue: "@labelCs",
                            newValue: Functions.PrepStringArg(arg: labelCs));

                        CommandText = CommandText.Replace(
                            oldValue: "@descriptionCs",
                            newValue: Functions.PrepStringArg(arg: descriptionCs));

                        CommandText = CommandText.Replace(
                            oldValue: "@labelEn",
                            newValue: Functions.PrepStringArg(arg: labelEn));

                        CommandText = CommandText.Replace(
                            oldValue: "@descriptionEn",
                            newValue: Functions.PrepStringArg(arg: descriptionEn));

                        CommandText = CommandText.Replace(
                            oldValue: "@classificationType",
                            newValue: Functions.PrepNIntArg(arg: classificationTypeId));

                        CommandText = CommandText.Replace(
                            oldValue: "@id",
                            newValue: Functions.PrepNIntArg(arg: id));

                        NpgsqlParameter pArealOrPopulationId = new(
                            parameterName: "arealOrPopulationId",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pLabelCs = new(
                            parameterName: "labelCs",
                            parameterType: NpgsqlDbType.Varchar);

                        NpgsqlParameter pDescriptionCs = new(
                            parameterName: "descriptionCs",
                            parameterType: NpgsqlDbType.Text);

                        NpgsqlParameter pLabelEn = new(
                            parameterName: "labelEn",
                            parameterType: NpgsqlDbType.Varchar);

                        NpgsqlParameter pDescriptionEn = new(
                            parameterName: "descriptionEn",
                            parameterType: NpgsqlDbType.Text);

                        NpgsqlParameter pClassificationType = new(
                            parameterName: "classificationType",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pId = new(
                            parameterName: "id",
                            parameterType: NpgsqlDbType.Integer);

                        if (arealOrPopulationId != null)
                        {
                            pArealOrPopulationId.Value = (int)arealOrPopulationId;
                        }
                        else
                        {
                            pArealOrPopulationId.Value = DBNull.Value;
                        }

                        if (labelCs != null)
                        {
                            pLabelCs.Value = (string)labelCs;
                        }
                        else
                        {
                            pLabelCs.Value = DBNull.Value;
                        }

                        if (descriptionCs != null)
                        {
                            pDescriptionCs.Value = (string)descriptionCs;
                        }
                        else
                        {
                            pDescriptionCs.Value = DBNull.Value;
                        }

                        if (labelEn != null)
                        {
                            pLabelEn.Value = (string)labelEn;
                        }
                        else
                        {
                            pLabelEn.Value = DBNull.Value;
                        }

                        if (descriptionEn != null)
                        {
                            pDescriptionEn.Value = (string)descriptionEn;
                        }
                        else
                        {
                            pDescriptionEn.Value = DBNull.Value;
                        }

                        if (classificationTypeId != null)
                        {
                            pClassificationType.Value = (int)classificationTypeId;
                        }
                        else
                        {
                            pClassificationType.Value = DBNull.Value;
                        }

                        if (id != null)
                        {
                            pId.Value = (int)id;
                        }
                        else
                        {
                            pId.Value = DBNull.Value;
                        }

                        string strResult = database.Postgres.ExecuteScalar(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pArealOrPopulationId,
                            pLabelCs, pDescriptionCs, pLabelEn, pDescriptionEn,
                            pClassificationType, pId);

                        if (String.IsNullOrEmpty(value: strResult))
                        {
                            return null;
                        }
                        else
                        {
                            if (Int32.TryParse(s: strResult, result: out int intResult))
                            {
                                return intResult;
                            }
                            else
                            {
                                return null;
                            }
                        }
                    }

                    #endregion Methods

                }

                #endregion FnSaveArealOrPop

            }

        }
    }
}