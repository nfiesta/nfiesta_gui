﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {

                #region FnGetSubPopulationCategoryForPopulation

                /// <summary>
                /// Wrapper for stored procedure fn_get_sub_population_category4population.
                /// Function returns records from c_sub_population_category table,
                /// optionally for given sub population.
                /// </summary>
                public static class FnGetSubPopulationCategoryForPopulation
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_get_sub_population_category4population";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_sub_population integer DEFAULT NULL::integer; ",
                            $"returns: ",
                            $"TABLE(",
                            $"id integer, ",
                            $"sub_population integer, ",
                            $"label character varying, ",
                            $"description text, ",
                            $"label_en character varying, ",
                            $"description_en text)");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    {TDSubPopulationCategoryList.ColId.SQL(TDSubPopulationCategoryList.Cols, Name)}",
                            $"    {TDSubPopulationCategoryList.ColLabelCs.SQL(TDSubPopulationCategoryList.Cols, Name)}",
                            $"    {TDSubPopulationCategoryList.ColDescriptionCs.SQL(TDSubPopulationCategoryList.Cols, Name)}",
                            $"    {TDSubPopulationCategoryList.ColSubPopulationId.SQL(TDSubPopulationCategoryList.Cols, Name)}",
                            $"    {TDSubPopulationCategoryList.ColLabelEn.SQL(TDSubPopulationCategoryList.Cols, Name)}",
                            $"    {TDSubPopulationCategoryList.ColDescriptionEn.SQL(TDSubPopulationCategoryList.Cols, Name)}",
                            $"    {TDSubPopulationCategoryList.ColTargetVariableId.SQL(TDSubPopulationCategoryList.Cols, Name, setToNull: true)}",
                            $"    {TDSubPopulationCategoryList.ColLDsityId.SQL(TDSubPopulationCategoryList.Cols, Name, setToNull: true)}",
                            $"    {TDSubPopulationCategoryList.ColLDsityObjectId.SQL(TDSubPopulationCategoryList.Cols, Name, setToNull: true)}",
                            $"    {TDSubPopulationCategoryList.ColLDsityObjectLabelCs.SQL(TDSubPopulationCategoryList.Cols, Name, setToNull: true)}",
                            $"    {TDSubPopulationCategoryList.ColLDsityObjectDescriptionCs.SQL(TDSubPopulationCategoryList.Cols, Name, setToNull: true)}",
                            $"    {TDSubPopulationCategoryList.ColLDsityObjectLabelEn.SQL(TDSubPopulationCategoryList.Cols, Name, setToNull: true)}",
                            $"    {TDSubPopulationCategoryList.ColLDsityObjectDescriptionEn.SQL(TDSubPopulationCategoryList.Cols, Name, setToNull: true)}",
                            $"    {TDSubPopulationCategoryList.ColUseNegative.SQL(TDSubPopulationCategoryList.Cols, Name, setToNull: true)}",
                            $"    {TDSubPopulationCategoryList.ColRestriction.SQL(TDSubPopulationCategoryList.Cols, Name, isLastOne: true, setToNull: true)}",
                            $"FROM{Environment.NewLine}",
                            $"    $SchemaName.$Name(@subPopulationId) AS {Name}{Environment.NewLine}",
                            $"ORDER BY{Environment.NewLine}",
                            $"    {Name}.{TDSubPopulationCategoryList.ColId.DbName};{Environment.NewLine}"
                            );

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Execute stored procedure fn_get_sub_population_category4population
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="subPopulationId">Subpopulation identifier</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table with subpopulation categories</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> subPopulationId = null,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = SQL;

                        CommandText = CommandText.Replace(
                            oldValue: "@subPopulationId",
                            newValue: Functions.PrepNIntArg(arg: subPopulationId));

                        NpgsqlParameter pSubPopulationId = new(
                            parameterName: "subPopulationId",
                            parameterType: NpgsqlDbType.Integer);

                        if (subPopulationId != null)
                        {
                            pSubPopulationId.Value = (int)subPopulationId;
                        }
                        else
                        {
                            pSubPopulationId.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteQuery(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pSubPopulationId);
                    }

                    /// <summary>
                    /// Execute stored procedure fn_get_sub_population_category4population
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="subPopulationId">Subpopulation identifier</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>List of subpopulation categories</returns>
                    public static TDSubPopulationCategoryList Execute(
                        NfiEstaDB database,
                        Nullable<int> subPopulationId = null,
                        NpgsqlTransaction transaction = null)
                    {
                        return new TDSubPopulationCategoryList(
                            database: database,
                            data: ExecuteQuery(
                                database: database,
                                subPopulationId: subPopulationId,
                                transaction: transaction))
                        { StoredProcedure = CommandText };
                    }

                    #endregion Methods

                }

                #endregion FnGetSubPopulationCategoryForPopulation

            }

        }
    }
}