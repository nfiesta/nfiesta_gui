﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {

                #region FnSaveCategories

                /// <summary>
                /// Wrapper for stored procedure fn_save_categories.
                /// Functions inserts a record into table
                /// c_area_domain_category or c_sub_population_category
                /// based on given parameters.
                /// </summary>
                public static class FnSaveCategories
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_save_categories";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_parent integer, ",
                            $"_areal_or_population integer, ",
                            $"_label character varying[], ",
                            $"_description text[], ",
                            $"_label_en character varying[], ",
                            $"_description_en text[]; ",
                            $"returns: ",
                            $"TABLE(",
                            $"id integer, ",
                            $"label character varying, ",
                            $"description text, ",
                            $"label_en character varying, ",
                            $"description_en text)");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    id,{Environment.NewLine}",
                            $"    label,{Environment.NewLine}",
                            $"    description,{Environment.NewLine}",
                            $"    label_en,{Environment.NewLine}",
                            $"    description_en{Environment.NewLine}",
                            $"FROM{Environment.NewLine}",
                            $"    $SchemaName.$Name(",
                            $"@parentId, ",
                            $"@arealOrPopulationId, ",
                            $"@labelCs, ",
                            $"@descriptionCs, ",
                            $"@labelEn, ",
                            $"@descriptionEn);{Environment.NewLine}");


                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Execute stored procedure fn_save_categories
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="parentId">Parent idenfier (id from c_area_domain/c_sub_population)</param>
                    /// <param name="arealOrPopulationId">Areal or population identifier</param>
                    /// <param name="labelCs">List of labels of area domain categories or subpopulation categories in national language</param>
                    /// <param name="descriptionCs">List of descriptions of area domain categories or subpopulation categories in national language</param>
                    /// <param name="labelEn">List of labels of area domain categories or subpopulation categories in English</param>
                    /// <param name="descriptionEn">List of description of area domain categories or subpopulation categories in English</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table with identifier, label, description, label_en and description_en</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> parentId,
                        Nullable<int> arealOrPopulationId,
                        List<string> labelCs,
                        List<string> descriptionCs,
                        List<string> labelEn,
                        List<string> descriptionEn,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = SQL;

                        CommandText = CommandText.Replace(
                            oldValue: "@parentId",
                            newValue: Functions.PrepNIntArg(arg: parentId));

                        CommandText = CommandText.Replace(
                            oldValue: "@arealOrPopulationId",
                            newValue: Functions.PrepNIntArg(arg: arealOrPopulationId));

                        CommandText = CommandText.Replace(
                            oldValue: "@labelCs",
                            newValue: Functions.PrepStringArrayArg(args: labelCs, dbType: "character varying"));

                        CommandText = CommandText.Replace(
                            oldValue: "@descriptionCs",
                            newValue: Functions.PrepStringArrayArg(args: descriptionCs, dbType: "text"));

                        CommandText = CommandText.Replace(
                            oldValue: "@labelEn",
                            newValue: Functions.PrepStringArrayArg(args: labelEn, dbType: "character varying"));

                        CommandText = CommandText.Replace(
                            oldValue: "@descriptionEn",
                            newValue: Functions.PrepStringArrayArg(args: descriptionEn, dbType: "text"));

                        NpgsqlParameter pParentId = new(
                            parameterName: "parentId",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pArealOrPopulationId = new(
                            parameterName: "arealOrPopulationId",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pLabelCs = new(
                            parameterName: "labelCs",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Varchar);

                        NpgsqlParameter pDescriptionCs = new(
                            parameterName: "descriptionCs",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Text);

                        NpgsqlParameter pLabelEn = new(
                            parameterName: "labelEn",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Varchar);

                        NpgsqlParameter pDescriptionEn = new(
                            parameterName: "descriptionEn",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Text);

                        if (parentId != null)
                        {
                            pParentId.Value = (int)parentId;
                        }
                        else
                        {
                            pParentId.Value = DBNull.Value;
                        }

                        if (arealOrPopulationId != null)
                        {
                            pArealOrPopulationId.Value = (int)arealOrPopulationId;
                        }
                        else
                        {
                            pArealOrPopulationId.Value = DBNull.Value;
                        }

                        if (labelCs != null)
                        {
                            pLabelCs.Value = labelCs.ToArray<string>();
                        }
                        else
                        {
                            pLabelCs.Value = DBNull.Value;
                        }

                        if (descriptionCs != null)
                        {
                            pDescriptionCs.Value = descriptionCs.ToArray<string>();
                        }
                        else
                        {
                            pDescriptionCs.Value = DBNull.Value;
                        }

                        if (labelEn != null)
                        {
                            pLabelEn.Value = labelEn.ToArray<string>();
                        }
                        else
                        {
                            pLabelEn.Value = DBNull.Value;
                        }

                        if (descriptionEn != null)
                        {
                            pDescriptionEn.Value = descriptionEn.ToArray<string>();
                        }
                        else
                        {
                            pDescriptionEn.Value = DBNull.Value;
                        }

                        return
                            database.Postgres.ExecuteQuery(
                                sqlCommand: SQL,
                                transaction: transaction,
                                pParentId, pArealOrPopulationId,
                                pLabelCs, pDescriptionCs, pLabelEn, pDescriptionEn);
                    }

                    /// <summary>
                    /// Execute stored procedure fn_save_categories
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="parentId">Parent idenfier (id from c_area_domain/c_sub_population)</param>
                    /// <param name="arealOrPopulationId">Areal or population identifier</param>
                    /// <param name="labelCs">List of labels of area domain categories or subpopulation categories in national language</param>
                    /// <param name="descriptionCs">List of descriptions of area domain categories or subpopulation categories in national language</param>
                    /// <param name="labelEn">List of labels of area domain categories or subpopulation categories in English</param>
                    /// <param name="descriptionEn">List of description of area domain categories or subpopulation categories in English</param>
                    /// <param name="transaction">Transaction</param>
                    public static void Execute(
                        NfiEstaDB database,
                        Nullable<int> parentId,
                        Nullable<int> arealOrPopulationId,
                        List<string> labelCs,
                        List<string> descriptionCs,
                        List<string> labelEn,
                        List<string> descriptionEn,
                        NpgsqlTransaction transaction = null)
                    {
                        ExecuteQuery(
                            database: database,
                            parentId: parentId,
                            arealOrPopulationId: arealOrPopulationId,
                            labelCs: labelCs,
                            descriptionCs: descriptionCs,
                            labelEn: labelEn,
                            descriptionEn: descriptionEn,
                            transaction: transaction);
                        return;
                    }

                    #endregion Methods

                }

                #endregion FnSaveCategories

            }

        }
    }
}