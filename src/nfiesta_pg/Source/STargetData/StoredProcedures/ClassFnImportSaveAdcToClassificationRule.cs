﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {

                #region FnImportSaveAdcToClassificationRule

                /// <summary>
                /// Wrapper for stored procedure fn_import_save_adc2classification_rule.
                /// </summary>
                public static class FnImportSaveAdcToClassificationRule
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_import_save_adc2classification_rule";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_adc2classification_rule integer, ",
                            $"_area_domain_category__etl_id integer, ",
                            $"_ldsity_object__etl_id integer, ",
                            $"_classification_rule text, ",
                            $"_refyearset2panel integer; ",
                            $"returns: ",
                            $"TABLE(",
                            $"adc2classification_rule integer, ",
                            $"etl_id integer, ",
                            $"cm_adc2classrule2panel_refyearset__etl_id integer)");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                       = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    adc2classification_rule,{Environment.NewLine}",
                            $"    etl_id,{Environment.NewLine}",
                            $"    cm_adc2classrule2panel_refyearset__etl_id{Environment.NewLine}",
                            $"FROM{Environment.NewLine}",
                            $"    $SchemaName.$Name(",
                            $"@adcToClassificationRule, ",
                            $"@areaDomainCategoryEtlId, ",
                            $"@ldsityObjectEtlId, ",
                            $"@classificationRule, ",
                            $"@refYearSetToPanel);{Environment.NewLine}");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Execute stored procedure fn_import_save_adc2classification_rule
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="adcToClassificationRule"></param>
                    /// <param name="areaDomainCategoryEtlId"></param>
                    /// <param name="ldsityObjectEtlId"></param>
                    /// <param name="classificationRule"></param>
                    /// <param name="refYearSetToPanel"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> adcToClassificationRule,
                        Nullable<int> areaDomainCategoryEtlId,
                        Nullable<int> ldsityObjectEtlId,
                        string classificationRule,
                        Nullable<int> refYearSetToPanel,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = SQL;

                        CommandText = CommandText.Replace(
                            oldValue: "@adcToClassificationRule",
                            newValue: Functions.PrepNIntArg(arg: adcToClassificationRule));

                        CommandText = CommandText.Replace(
                            oldValue: "@areaDomainCategoryEtlId",
                            newValue: Functions.PrepNIntArg(arg: areaDomainCategoryEtlId));

                        CommandText = CommandText.Replace(
                           oldValue: "@ldsityObjectEtlId",
                           newValue: Functions.PrepNIntArg(arg: ldsityObjectEtlId));

                        CommandText = CommandText.Replace(
                           oldValue: "@classificationRule",
                           newValue: Functions.PrepStringArg(arg: classificationRule));

                        CommandText = CommandText.Replace(
                           oldValue: "@refYearSetToPanel",
                           newValue: Functions.PrepNIntArg(arg: refYearSetToPanel));

                        NpgsqlParameter pAdcToClassificationRule = new(
                            parameterName: "adcToClassificationRule",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pAreaDomainCategoryEtlId = new(
                            parameterName: "areaDomainCategoryEtlId",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pLDsityObjectEtlId = new(
                            parameterName: "ldsityObjectEtlId",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pClassificationRule = new(
                            parameterName: "classificationRule",
                            parameterType: NpgsqlDbType.Text);

                        NpgsqlParameter pRefYearSetToPanel = new(
                            parameterName: "refYearSetToPanel",
                            parameterType: NpgsqlDbType.Boolean);

                        if (adcToClassificationRule != null)
                        {
                            pAdcToClassificationRule.Value = (int)adcToClassificationRule;
                        }
                        else
                        {
                            pAdcToClassificationRule.Value = DBNull.Value;
                        }

                        if (areaDomainCategoryEtlId != null)
                        {
                            pAreaDomainCategoryEtlId.Value = (int)areaDomainCategoryEtlId;
                        }
                        else
                        {
                            pAreaDomainCategoryEtlId.Value = DBNull.Value;
                        }

                        if (ldsityObjectEtlId != null)
                        {
                            pLDsityObjectEtlId.Value = (int)ldsityObjectEtlId;
                        }
                        else
                        {
                            pLDsityObjectEtlId.Value = DBNull.Value;
                        }

                        if (classificationRule != null)
                        {
                            pClassificationRule.Value = (string)classificationRule;
                        }
                        else
                        {
                            pClassificationRule.Value = DBNull.Value;
                        }

                        if (refYearSetToPanel != null)
                        {
                            pRefYearSetToPanel.Value = (int)refYearSetToPanel;
                        }
                        else
                        {
                            pRefYearSetToPanel.Value = DBNull.Value;
                        }

                        return
                            database.Postgres.ExecuteQuery(
                                sqlCommand: SQL,
                                transaction: transaction,
                                pAdcToClassificationRule, pAreaDomainCategoryEtlId,
                                pLDsityObjectEtlId, pClassificationRule,
                                pRefYearSetToPanel);
                    }

                    #endregion Methods

                }

                #endregion FnImportSaveAdcToClassificationRule

            }

        }
    }
}