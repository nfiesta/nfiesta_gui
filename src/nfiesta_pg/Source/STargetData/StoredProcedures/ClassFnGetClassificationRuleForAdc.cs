﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {

                #region FnGetClassificationRuleForAdc

                /// <summary>
                /// Wrapper for stored procedure fn_get_classification_rule4adc.
                /// Function returns records from cm_adc2classification_rule table,
                /// optionally for given area_domain_category and/or ldsity object.
                /// </summary>
                public static class FnGetClassificationRuleForAdc
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_get_classification_rule4adc";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_area_domain_category integer DEFAULT NULL::integer, ",
                            $"_ldsity_object integer DEFAULT NULL::integer, ",
                            $"_use_negative boolean DEFAULT NULL::boolean; ",
                            $"returns: ",
                            $"TABLE(",
                            $"id integer, ",
                            $"area_domain_category integer, ",
                            $"ldsity_object integer, ",
                            $"classification_rule text, ",
                            $"use_negative boolean)");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    $Name.{TDADCToClassificationRuleList.ColId.DbName} ",
                            $"AS {TDADCToClassificationRuleList.ColId.Name},{Environment.NewLine}",
                            $"    $Name.{TDADCToClassificationRuleList.ColAreaDomainCategoryId.DbName} ",
                            $"AS {TDADCToClassificationRuleList.ColAreaDomainCategoryId.Name},{Environment.NewLine}",
                            $"    $Name.{TDADCToClassificationRuleList.ColLDsityObjectId.DbName} ",
                            $"AS {TDADCToClassificationRuleList.ColLDsityObjectId.Name},{Environment.NewLine}",
                            $"    $Name.{TDADCToClassificationRuleList.ColClassificationRule.DbName} ",
                            $"AS {TDADCToClassificationRuleList.ColClassificationRule.Name},{Environment.NewLine}",
                            $"    $Name.{TDADCToClassificationRuleList.ColUseNegative.DbName} ",
                            $"AS {TDADCToClassificationRuleList.ColUseNegative.Name}{Environment.NewLine}",
                            $"FROM{Environment.NewLine}",
                            $"    {SchemaName}.{Name}(",
                            $"@areaDomainCategoryId, ",
                            $"@ldsityObjectId, ",
                            $"@useNegative",
                            $") AS $Name{Environment.NewLine}",
                            $"ORDER BY{Environment.NewLine}",
                            $"    $Name.{TDADCToClassificationRuleList.ColId.DbName};{Environment.NewLine}"
                            );

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Execute stored procedure fn_get_classification_rule4adc
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="areaDomainCategoryId">Area domain category identifier</param>
                    /// <param name="ldsityObjectId">Local density object identifier</param>
                    /// <param name="useNegative">Use negative local density contribution</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table with records from cm_adc2classification_rule</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> areaDomainCategoryId,
                        Nullable<int> ldsityObjectId,
                        Nullable<bool> useNegative,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = SQL;

                        CommandText = CommandText.Replace(
                            oldValue: "@areaDomainCategoryId",
                            newValue: Functions.PrepNIntArg(arg: areaDomainCategoryId));

                        CommandText = CommandText.Replace(
                            oldValue: "@ldsityObjectId",
                            newValue: Functions.PrepNIntArg(arg: ldsityObjectId));

                        CommandText = CommandText.Replace(
                           oldValue: "@useNegative",
                           newValue: Functions.PrepNBoolArg(arg: useNegative));

                        NpgsqlParameter pAreaDomainCategoryId = new(
                            parameterName: "areaDomainCategoryId",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pLDsityObjectId = new(
                            parameterName: "ldsityObjectId",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pUseNegative = new(
                            parameterName: "useNegative",
                            parameterType: NpgsqlDbType.Boolean);

                        if (areaDomainCategoryId != null)
                        {
                            pAreaDomainCategoryId.Value = (int)areaDomainCategoryId;
                        }
                        else
                        {
                            pAreaDomainCategoryId.Value = DBNull.Value;
                        }

                        if (ldsityObjectId != null)
                        {
                            pLDsityObjectId.Value = (int)ldsityObjectId;
                        }
                        else
                        {
                            pLDsityObjectId.Value = DBNull.Value;
                        }

                        if (useNegative != null)
                        {
                            pUseNegative.Value = (bool)useNegative;
                        }
                        else
                        {
                            pUseNegative.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteQuery(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pAreaDomainCategoryId, pLDsityObjectId, pUseNegative);
                    }

                    /// <summary>
                    /// Execute stored procedure fn_get_classification_rule4adc
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="areaDomainCategoryId">Area domain category identifier</param>
                    /// <param name="ldsityObjectId">Local density object identifier</param>
                    /// <param name="useNegative">Use negative local density contribution</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>List of records from cm_adc2classification_rule</returns>
                    public static TDADCToClassificationRuleList Execute(
                        NfiEstaDB database,
                        Nullable<int> areaDomainCategoryId,
                        Nullable<int> ldsityObjectId,
                        Nullable<bool> useNegative,
                        NpgsqlTransaction transaction = null)
                    {
                        return new TDADCToClassificationRuleList(
                            database: database,
                            data: ExecuteQuery(
                                database: database,
                                areaDomainCategoryId: areaDomainCategoryId,
                                ldsityObjectId: ldsityObjectId,
                                useNegative: useNegative,
                                transaction: transaction))
                        { StoredProcedure = CommandText };
                    }

                    #endregion Methods

                }

                #endregion FnGetClassificationRuleForAdc

            }

        }
    }
}