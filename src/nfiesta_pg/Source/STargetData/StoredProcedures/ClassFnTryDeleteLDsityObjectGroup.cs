﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {
                // fn_try_delete_ldsity_object_group

                #region FnTryDeleteLDsityObjectGroup

                /// <summary>
                /// Wrapper for stored procedure fn_try_delete_ldsity_object_group.
                /// Function provides test if it is possible
                /// to delete records from c_ldsity_object_group table.
                /// </summary>
                public static class FnTryDeleteLDsityObjectGroup
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_try_delete_ldsity_object_group";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_id integer; ",
                            $"returns: boolean");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    $SchemaName.$Name(@ldsityObjectGroupId);{Environment.NewLine}");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="ldsityObjectGroupId">Group of local density objects identifier</param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(Nullable<int> ldsityObjectGroupId)
                    {
                        string result = SQL;

                        result = result.Replace(
                            oldValue: "@ldsityObjectGroupId",
                            newValue: Functions.PrepNIntArg(arg: ldsityObjectGroupId));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_try_delete_ldsity_object_group
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="ldsityObjectGroupId">Group of local density objects identifier</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>DataTable with test result</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> ldsityObjectGroupId,
                        NpgsqlTransaction transaction = null)
                    {
                        bool val = Execute(
                            database: database,
                            ldsityObjectGroupId: ldsityObjectGroupId,
                            transaction: transaction);

                        DataTable dt = new();

                        dt.Columns.Add(column: new DataColumn()
                        {
                            ColumnName = Name,
                            DataType = Type.GetType(typeName: "System.Boolean")
                        });

                        DataRow row = dt.NewRow();

                        Functions.SetBoolArg(
                            row: row,
                            name: Name,
                            val: val);

                        dt.Rows.Add(row: row);

                        return dt;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_try_delete_ldsity_object_group
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="ldsityObjectGroupId">Group of local density objects identifier</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Test result</returns>
                    public static bool Execute(
                        NfiEstaDB database,
                        Nullable<int> ldsityObjectGroupId,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(ldsityObjectGroupId: ldsityObjectGroupId);

                        NpgsqlParameter pLDsityObjectGroupId = new(
                            parameterName: "ldsityObjectGroupId",
                            parameterType: NpgsqlDbType.Integer);

                        if (ldsityObjectGroupId != null)
                        {
                            pLDsityObjectGroupId.Value = (int)ldsityObjectGroupId;
                        }
                        else
                        {
                            pLDsityObjectGroupId.Value = DBNull.Value;
                        }

                        string strResult =
                            database.Postgres.ExecuteScalar(
                                sqlCommand: SQL,
                                transaction: transaction,
                                pLDsityObjectGroupId);

                        if (String.IsNullOrEmpty(value: strResult))
                        {
                            return false;
                        }
                        else
                        {
                            if (Boolean.TryParse(value: strResult, result: out bool boolResult))
                            {
                                return boolResult;
                            }
                            else
                            {
                                return false;
                            }
                        }
                    }

                    #endregion Methods

                }

                #endregion FnTryDeleteLDsityObjectGroup

            }

        }
    }
}