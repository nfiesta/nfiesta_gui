﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {

                #region FnSaveLdsityValuesInternal

                /// <summary>
                /// Wrapper for stored procedure fn_save_ldsity_values_internal.
                /// The function for the specified list of input arguments inserts data into the t_available_datasets table
                /// and inserts data into the t_ldsity_values table(aggregated local density at the plot level).
                /// </summary>
                public static class FnSaveLdsityValuesInternal
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_save_ldsity_values_internal";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_refyearset2panel_mapping integer[], ",
                            $"_categorization_setups integer[], ",
                            $"_threshold double precision; ",
                            $"returns: text");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    $SchemaName.$Name(",
                            $"@refYearSetToPanelMappingIds, ",
                            $"@categorizationSetupIds, ",
                            $"@threshold)::text AS $Name;");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Execute stored procedure fn_save_ldsity_values_internal
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="refYearSetToPanelMappingIds">List of identifiers from mapping table sdesign.cm_refyearset2panel_mapping</param>
                    /// <param name="categorizationSetupIds">List of categorization setup identifiers</param>
                    /// <param name="threshold">Threshold</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        List<Nullable<int>> refYearSetToPanelMappingIds,
                        List<Nullable<int>> categorizationSetupIds,
                        Nullable<double> threshold,
                        NpgsqlTransaction transaction = null)
                    {
                        string val = Execute(
                            database: database,
                            refYearSetToPanelMappingIds: refYearSetToPanelMappingIds,
                            categorizationSetupIds: categorizationSetupIds,
                            threshold: threshold,
                            transaction: transaction);

                        DataTable dt = new();

                        dt.Columns.Add(column: new DataColumn()
                        {
                            ColumnName = Name,
                            DataType = Type.GetType(typeName: "System.String")
                        });

                        DataRow row = dt.NewRow();

                        Functions.SetStringArg(
                            row: row,
                            name: Name,
                            val: val);

                        dt.Rows.Add(row: row);

                        return dt;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_save_ldsity_values_internal
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="refYearSetToPanelMappingIds">List of identifiers from mapping table sdesign.cm_refyearset2panel_mapping</param>
                    /// <param name="categorizationSetupIds">List of categorization setup identifiers</param>
                    /// <param name="threshold">Threshold</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns> Message with result status</returns>
                    public static string Execute(
                        NfiEstaDB database,
                        List<Nullable<int>> refYearSetToPanelMappingIds,
                        List<Nullable<int>> categorizationSetupIds,
                        Nullable<double> threshold,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = SQL;

                        CommandText = CommandText.Replace(
                            oldValue: "@refYearSetToPanelMappingIds",
                            newValue: Functions.PrepNIntArrayArg(args: refYearSetToPanelMappingIds, dbType: "int4"));

                        CommandText = CommandText.Replace(
                            oldValue: "@categorizationSetupIds",
                            newValue: Functions.PrepNIntArrayArg(args: categorizationSetupIds, dbType: "int4"));

                        CommandText = CommandText.Replace(
                            oldValue: "@threshold",
                            newValue: Functions.PrepNDoubleArg(arg: threshold));

                        NpgsqlParameter pRefYearSetToPanelMappingIds = new(
                            parameterName: "refYearSetToPanelMappingIds",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        NpgsqlParameter pCategorizationSetupIds = new(
                            parameterName: "categorizationSetupIds",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        NpgsqlParameter pThreshold = new(
                            parameterName: "threshold",
                            parameterType: NpgsqlDbType.Double);

                        if (refYearSetToPanelMappingIds != null)
                        {
                            pRefYearSetToPanelMappingIds.Value = refYearSetToPanelMappingIds.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pRefYearSetToPanelMappingIds.Value = DBNull.Value;
                        }

                        if (categorizationSetupIds != null)
                        {
                            pCategorizationSetupIds.Value = categorizationSetupIds.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pCategorizationSetupIds.Value = DBNull.Value;
                        }

                        if (threshold != null)
                        {
                            pThreshold.Value = (double)threshold;
                        }
                        else
                        {
                            pThreshold.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteScalar(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pRefYearSetToPanelMappingIds, pCategorizationSetupIds, pThreshold);
                    }

                    #endregion Methods

                }

                #endregion FnSaveLdsityValuesInternal

            }

        }
    }
}