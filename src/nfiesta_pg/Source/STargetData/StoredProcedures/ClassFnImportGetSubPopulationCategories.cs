﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {

                #region FnImportGetSubPopulationCategories

                /// <summary>
                /// Wrapper for stored procedure fn_import_get_sub_population_categories.
                /// </summary>
                public static class FnImportGetSubPopulationCategories
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_import_get_sub_population_categories";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_sub_population_category integer, ",
                            $"_sub_population__etl_id integer, ",
                            $"_etl_id integer[] DEFAULT NULL::integer[]; ",
                            $"returns: ",
                            $"TABLE(",
                            $"sub_population_category integer, ",
                            $"etl_id integer, ",
                            $"label character varying, ",
                            $"description text, ",
                            $"label_en character varying, ",
                            $"description_en text)");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    sub_population_category,{Environment.NewLine}",
                            $"    etl_id,{Environment.NewLine}",
                            $"    label,{Environment.NewLine}",
                            $"    description,{Environment.NewLine}",
                            $"    label_en,{Environment.NewLine}",
                            $"    description_en{Environment.NewLine}",
                            $"FROM{Environment.NewLine}",
                            $"    $SchemaName.$Name(",
                            $"@subPopulationCategory, ",
                            $"@subPopulationEtlId, ",
                            $"@etlId);{Environment.NewLine}");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Execute stored procedure fn_import_get_sub_population_categories
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="subPopulationCategory"></param>
                    /// <param name="subPopulationEtlId"></param>
                    /// <param name="etlId"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> subPopulationCategory,
                        Nullable<int> subPopulationEtlId,
                        List<Nullable<int>> etlId,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = SQL;

                        CommandText = CommandText.Replace(
                            oldValue: "@subPopulationCategory",
                            newValue: Functions.PrepNIntArg(arg: subPopulationCategory));

                        CommandText = CommandText.Replace(
                            oldValue: "@subPopulationEtlId",
                            newValue: Functions.PrepNIntArg(arg: subPopulationEtlId));

                        CommandText = CommandText.Replace(
                            oldValue: "@etlId",
                            newValue: Functions.PrepNIntArrayArg(args: etlId, dbType: "int4"));

                        NpgsqlParameter pSubPopulationCategory = new(
                            parameterName: "subPopulationCategory",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pSubPopulationEtlId = new(
                            parameterName: "subPopulationEtlId",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pEtlId = new(
                            parameterName: "etlId",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        if (subPopulationCategory != null)
                        {
                            pSubPopulationCategory.Value = (int)subPopulationCategory;
                        }
                        else
                        {
                            pSubPopulationCategory.Value = DBNull.Value;
                        }

                        if (subPopulationEtlId != null)
                        {
                            pSubPopulationEtlId.Value = (int)subPopulationEtlId;
                        }
                        else
                        {
                            pSubPopulationEtlId.Value = DBNull.Value;
                        }

                        if (etlId != null)
                        {
                            pEtlId.Value = etlId.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pEtlId.Value = DBNull.Value;
                        }

                        return
                            database.Postgres.ExecuteQuery(
                                sqlCommand: SQL,
                                transaction: transaction,
                                pSubPopulationCategory,
                                pSubPopulationEtlId,
                                pEtlId);
                    }

                    #endregion Methods

                }

                #endregion FnImportGetSubPopulationCategories

            }

        }
    }
}