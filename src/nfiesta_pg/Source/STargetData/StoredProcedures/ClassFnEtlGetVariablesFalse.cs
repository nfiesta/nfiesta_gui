﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {
                // fn_etl_get_panel_variables_false

                #region FnEtlGetVariablesFalse

                /// <summary>
                /// Wrapper for stored procedure fn_etl_get_variables_false.
                /// Function returns list of attribute variables that must be still implemented
                /// for given combination of panel and reference year set for selected target variable.
                /// </summary>
                public static class FnEtlGetVariablesFalse
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_etl_get_variables_false";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_variables json, ",
                            $"_panel character varying, ",
                            $"_reference_year_set character varying, ",
                            $"_national_language character varying DEFAULT 'en'::character varying(2); ",
                            $"returns: ",
                            $"TABLE(",
                            $"sub_population character varying, ",
                            $"sub_population_category character varying, ",
                            $"area_domain character varying, ",
                            $"area_domain_category character varying, ",
                            $"sub_population_en character varying, ",
                            $"sub_population_category_en character varying, ",
                            $"area_domain_en character varying, ",
                            $"area_domain_category_en character varying)");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    {TDFnEtlGetVariablesFalseTypeList.ColId.SQL(TDFnEtlGetVariablesFalseTypeList.Cols, Name)}",
                            $"    {TDFnEtlGetVariablesFalseTypeList.ColSubPopulation.SQL(TDFnEtlGetVariablesFalseTypeList.Cols, Name)}",
                            $"    {TDFnEtlGetVariablesFalseTypeList.ColSubPopulationCategory.SQL(TDFnEtlGetVariablesFalseTypeList.Cols, Name)}",
                            $"    {TDFnEtlGetVariablesFalseTypeList.ColAreaDomain.SQL(TDFnEtlGetVariablesFalseTypeList.Cols, Name)}",
                            $"    {TDFnEtlGetVariablesFalseTypeList.ColAreaDomainCategory.SQL(TDFnEtlGetVariablesFalseTypeList.Cols, Name)}",
                            $"    {TDFnEtlGetVariablesFalseTypeList.ColSubPopulationEn.SQL(TDFnEtlGetVariablesFalseTypeList.Cols, Name)}",
                            $"    {TDFnEtlGetVariablesFalseTypeList.ColSubPopulationCategoryEn.SQL(TDFnEtlGetVariablesFalseTypeList.Cols, Name)}",
                            $"    {TDFnEtlGetVariablesFalseTypeList.ColAreaDomainEn.SQL(TDFnEtlGetVariablesFalseTypeList.Cols, Name)}",
                            $"    {TDFnEtlGetVariablesFalseTypeList.ColAreaDomainCategoryEn.SQL(TDFnEtlGetVariablesFalseTypeList.Cols, Name, isLastOne: true)}",
                            $"FROM{Environment.NewLine}",
                            $"    $SchemaName.$Name(@variables, @panel, @referenceYearSet, @nationalLanguage) AS {Name}{Environment.NewLine}",
                            $"ORDER BY{Environment.NewLine}",
                            $"    {Name}.{TDFnEtlGetVariablesFalseTypeList.ColSubPopulation.Name},{Environment.NewLine}",
                            $"    {Name}.{TDFnEtlGetVariablesFalseTypeList.ColAreaDomain.Name};{Environment.NewLine}"
                            );

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="variables">Variables json</param>
                    /// <param name="panel">Panel</param>
                    /// <param name="referenceYearSet">Reference-year set</param>
                    /// <param name="nationalLanguage">Language</param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        string variables,
                        string panel,
                        string referenceYearSet,
                        Language nationalLanguage = PostgreSQL.Language.CS)
                    {
                        string result = SQL;

                        result = result.Replace(
                            oldValue: "@variables",
                            newValue: Functions.PrepStringArg(arg: variables));

                        result = result.Replace(
                            oldValue: "@panel",
                            newValue: Functions.PrepStringArg(arg: panel));

                        result = result.Replace(
                           oldValue: "@referenceYearSet",
                           newValue: Functions.PrepStringArg(arg: referenceYearSet));

                        result = result.Replace(
                          oldValue: "@nationalLanguage",
                          newValue: Functions.PrepStringArg(
                              arg: LanguageList.ISO_639_1(language: nationalLanguage)));

                        return result;
                    }

                    /// <summary>
                    ///  Execute stored procedure fn_etl_get_panel_variables_false
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="variables">Variables json</param>
                    /// <param name="panel">Panel</param>
                    /// <param name="referenceYearSet">Reference-year set</param>
                    /// <param name="nationalLanguage">Language</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        string variables,
                        string panel,
                        string referenceYearSet,
                        Language nationalLanguage = PostgreSQL.Language.CS,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(
                            variables: variables,
                            panel: panel,
                            referenceYearSet: referenceYearSet,
                            nationalLanguage: nationalLanguage);

                        NpgsqlParameter pVariables = new(
                            parameterName: "variables",
                            parameterType: NpgsqlDbType.Json);

                        NpgsqlParameter pPanel = new(
                            parameterName: "panel",
                            parameterType: NpgsqlDbType.Varchar);

                        NpgsqlParameter pReferenceYearSet = new(
                            parameterName: "referenceYearSet",
                            parameterType: NpgsqlDbType.Varchar);

                        NpgsqlParameter pNationalLanguage = new(
                            parameterName: "nationalLanguage",
                            parameterType: NpgsqlDbType.Varchar);

                        if (variables != null)
                        {
                            pVariables.Value = (string)variables;
                        }
                        else
                        {
                            pVariables.Value = DBNull.Value;
                        }

                        if (panel != null)
                        {
                            pPanel.Value = (string)panel;
                        }
                        else
                        {
                            pPanel.Value = DBNull.Value;
                        }

                        if (referenceYearSet != null)
                        {
                            pReferenceYearSet.Value = (string)referenceYearSet;
                        }
                        else
                        {
                            pReferenceYearSet.Value = DBNull.Value;
                        }

                        pNationalLanguage.Value =
                            LanguageList.ISO_639_1(language: nationalLanguage);

                        return database.Postgres.ExecuteQuery(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pVariables, pPanel, pReferenceYearSet, pNationalLanguage);
                    }

                    /// <summary>
                    ///  Execute stored procedure fn_etl_get_panel_variables_false
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="variables">Variables json</param>
                    /// <param name="panel">Panel</param>
                    /// <param name="referenceYearSet">Reference-year set</param>
                    /// <param name="nationalLanguage">Language</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table</returns>
                    public static TDFnEtlGetVariablesFalseTypeList Execute(
                        NfiEstaDB database,
                        string variables,
                        string panel,
                        string referenceYearSet,
                        Language nationalLanguage = PostgreSQL.Language.CS,
                        NpgsqlTransaction transaction = null)
                    {
                        System.Diagnostics.Stopwatch stopWatch = new();
                        stopWatch.Start();

                        DataTable data = ExecuteQuery(
                                database: database,
                                variables: variables,
                                panel: panel,
                                referenceYearSet: referenceYearSet,
                                nationalLanguage: nationalLanguage,
                                transaction: transaction);
                        data.TableName = Name;

                        stopWatch.Stop();

                        return new TDFnEtlGetVariablesFalseTypeList(
                            database: database,
                            data: data,
                            loadingTime: 0.001 * stopWatch.ElapsedMilliseconds,
                            storedProcedure: CommandText)
                        {
                            Variables = variables,
                            Panel = panel,
                            ReferenceYearSet = referenceYearSet,
                            NationalLanguage = nationalLanguage
                        };
                    }

                    #endregion Methods

                }

                #endregion FnEtlGetVariablesFalse

            }

        }
    }
}