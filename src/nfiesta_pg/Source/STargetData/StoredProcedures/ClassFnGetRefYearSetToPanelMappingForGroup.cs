﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {

                #region FnGetRefYearSetToPanelMappingForGroup

                /// <summary>
                /// Wrapper for stored procedure fn_get_refyearset2panel_mapping4group.
                /// Function returns an array of panel and reference year-set combinations (sdesign.cm_refyearset2panel_mapping)
                /// belonging to a  particular group (sdesign.c_panel_refyearset_group).
                /// </summary>
                public static class FnGetRefYearSetToPanelMappingForGroup
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name = "fn_get_refyearset2panel_mapping4group";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_panel_refyearset_group integer; ",
                            $"returns: ",
                            $"TABLE(",
                            $"refyearset2panel_mapping integer, ",
                            $"panel_label character varying, ",
                            $"panel_description character varying, ",
                            $"refyearset_label character varying, ",
                            $"refyearset_description character varying)");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return String.Concat(
                                $"SELECT{Environment.NewLine}",
                                $"    {TFnGetRefYearSetToPanelMappingForGroupList.ColId.SQL(cols: TFnGetRefYearSetToPanelMappingForGroupList.Cols, alias: Name)}",
                                $"    {TFnGetRefYearSetToPanelMappingForGroupList.ColPanelLabel.SQL(cols: TFnGetRefYearSetToPanelMappingForGroupList.Cols, alias: Name)}",
                                $"    {TFnGetRefYearSetToPanelMappingForGroupList.ColPanelDescription.SQL(cols: TFnGetRefYearSetToPanelMappingForGroupList.Cols, alias: Name)}",
                                $"    {TFnGetRefYearSetToPanelMappingForGroupList.ColRefYearSetLabel.SQL(cols: TFnGetRefYearSetToPanelMappingForGroupList.Cols, alias: Name)}",
                                $"    {TFnGetRefYearSetToPanelMappingForGroupList.ColRefYearSetDescription.SQL(cols: TFnGetRefYearSetToPanelMappingForGroupList.Cols, alias: Name, isLastOne: true)}",
                                $"FROM{Environment.NewLine}",
                                $"    {SchemaName}.{Name}(@panelRefYearSetGroupId) AS {Name}{Environment.NewLine}",
                                $"ORDER BY{Environment.NewLine}",
                                $"    {Name}.{TFnGetRefYearSetToPanelMappingForGroupList.ColId.DbName};{Environment.NewLine}"
                                );
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="panelRefYearSetGroupId">
                    /// Aggregated sets of panels and corresponding reference year sets identifier
                    /// (id from table target_data.c_panel_refyearset_group)
                    /// </param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(Nullable<int> panelRefYearSetGroupId)
                    {
                        string result = SQL;

                        result = result.Replace(
                            oldValue: "@panelRefYearSetGroupId",
                            newValue: Functions.PrepNIntArg(arg: panelRefYearSetGroupId));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_get_refyearset2panel_mapping4group
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="panelRefYearSetGroupId">
                    /// Aggregated sets of panels and corresponding reference year sets identifier
                    /// (id from table target_data.c_panel_refyearset_group)
                    /// </param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table with panel reference year set combinations belonging to a particuliar group</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> panelRefYearSetGroupId,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(panelRefYearSetGroupId: panelRefYearSetGroupId);

                        NpgsqlParameter pPanelRefYearSetGroupId = new(
                            parameterName: "panelRefYearSetGroupId",
                            parameterType: NpgsqlDbType.Integer);

                        if (panelRefYearSetGroupId != null)
                        {
                            pPanelRefYearSetGroupId.Value = (int)panelRefYearSetGroupId;
                        }
                        else
                        {
                            pPanelRefYearSetGroupId.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteQuery(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pPanelRefYearSetGroupId);
                    }

                    /// <summary>
                    /// Execute stored procedure fn_get_refyearset2panel_mapping4group
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="panelRefYearSetGroupId">
                    /// Aggregated sets of panels and corresponding reference year sets identifier
                    /// (id from table target_data.c_panel_refyearset_group)
                    /// </param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>List of panel reference year set combinations belonging to a particuliar group</returns>
                    public static TFnGetRefYearSetToPanelMappingForGroupList Execute(
                        NfiEstaDB database,
                        Nullable<int> panelRefYearSetGroupId,
                        NpgsqlTransaction transaction = null)
                    {
                        System.Diagnostics.Stopwatch stopWatch = new();
                        stopWatch.Start();

                        DataTable data = ExecuteQuery(
                            database: database,
                            panelRefYearSetGroupId: panelRefYearSetGroupId,
                            transaction: transaction);
                        data.TableName = Name;

                        stopWatch.Stop();

                        return new TFnGetRefYearSetToPanelMappingForGroupList(
                            database: database,
                            data: data,
                            loadingTime: 0.001 * stopWatch.ElapsedMilliseconds,
                            storedProcedure: CommandText)
                        {
                            PanelRefYearSetGroupId = panelRefYearSetGroupId
                        };
                    }

                    #endregion Methods

                }

                #endregion FnGetRefYearSetToPanelMappingForGroup

            }

        }
    }
}