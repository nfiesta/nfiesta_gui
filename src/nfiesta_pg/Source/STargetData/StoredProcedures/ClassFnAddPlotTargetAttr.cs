﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {
                // fn_add_plot_target_attr

                #region FnAddPlotTargetAttr

                /// <summary>
                /// Wrapper for stored procedure fn_add_plot_target_attr.
                /// Function showing plot level target local density attribute additivity
                /// </summary>
                public static class FnAddPlotTargetAttr
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_add_plot_target_attr";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature =
                        String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"variables integer[], ",
                            $"plots integer[] DEFAULT NULL::integer[], ",
                            $"refyearsets integer[] DEFAULT NULL::integer[], ",
                            $"min_diff double precision DEFAULT 0.0, ",
                            $"include_null_diff boolean DEFAULT true; ",
                            $"returns: ",
                            $"TABLE(",
                            $"plot integer, ",
                            $"panel integer, ",
                            $"reference_year_set integer, ",
                            $"variable integer, ",
                            $"ldsity double precision, ",
                            $"ldsity_sum double precision, ",
                            $"variables_def integer[], ",
                            $"variables_found integer[], ",
                            $"diff double precision)");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    $Name.plot                                             AS plot,{Environment.NewLine}",
                            $"    $Name.panel                                            AS panel,{Environment.NewLine}",
                            $"    $Name.reference_year_set                               AS reference_year_set,{Environment.NewLine}",
                            $"    $Name.variable                                         AS variable,{Environment.NewLine}",
                            $"    $Name.ldsity                                           AS ldsity,{Environment.NewLine}",
                            $"    $Name.ldsity_sum                                       AS ldsity_sum,{Environment.NewLine}",
                            $"    array_to_string($Name.variables_def, ';', 'NULL')      AS variables_def,{Environment.NewLine}",
                            $"    array_to_string($Name.variables_found, ';', 'NULL')    AS variables_found,{Environment.NewLine}",
                            $"    $Name.diff                                             AS diff{Environment.NewLine}",
                            $"FROM{Environment.NewLine}",
                            $"    $SchemaName.$Name(@variables, @plots, @refyearsets, @minDiff, @includeNullDiff){Environment.NewLine}",
                            $"ORDER BY{Environment.NewLine}",
                            $"    $Name.variable,{Environment.NewLine}",
                            $"    $Name.plot,{Environment.NewLine}",
                            $"    $Name.panel,{Environment.NewLine}",
                            $"    $Name.reference_year_set;{Environment.NewLine}"
                            );

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="variables">Identifiers from table t_variables</param>
                    /// <param name="plots">Identifiers from table f_p_plot</param>
                    /// <param name="refyearsets">Identifiers from table t_reference_year_set</param>
                    /// <param name="minDiff">Minimal amount of difference[%]</param>
                    /// <param name="includeNullDiff">Whether include NULL difference</param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        List<Nullable<int>> variables,
                        List<Nullable<int>> plots = null,
                        List<Nullable<int>> refyearsets = null,
                        Nullable<double> minDiff = 0.0,
                        Nullable<bool> includeNullDiff = true)
                    {
                        string result = SQL;

                        result = result.Replace(
                            oldValue: "@variables",
                            newValue: Functions.PrepNIntArrayArg(args: variables, dbType: "int4"));

                        result = result.Replace(
                            oldValue: "@plots",
                            newValue: Functions.PrepNIntArrayArg(args: plots, dbType: "int4"));

                        result = result.Replace(
                            oldValue: "@refyearsets",
                            newValue: Functions.PrepNIntArrayArg(args: refyearsets, dbType: "int4"));

                        result = result.Replace(
                            oldValue: "@minDiff",
                            newValue: Functions.PrepNDoubleArg(arg: minDiff));

                        result = result.Replace(
                            oldValue: "@includeNullDiff",
                            newValue: Functions.PrepNBoolArg(arg: includeNullDiff));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_add_plot_target_attr
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="variables">Identifiers from table t_variables</param>
                    /// <param name="plots">Identifiers from table f_p_plot</param>
                    /// <param name="refyearsets">Identifiers from table t_reference_year_set</param>
                    /// <param name="minDiff">Minimal amount of difference[%]</param>
                    /// <param name="includeNullDiff">Whether include NULL difference</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table with columns:
                    /// plot - identifier from table f_p_plot, reference_year_set - identifier from talbe t_reference_year_set, variable - identifier from table t_variable
                    /// ldsity_sum - sum of local densities, variables_def - variables defined in hierarchy, variables_found - variables found in data,
                    /// diff - relative difference between local densities and sum of local densities</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        List<Nullable<int>> variables,
                        List<Nullable<int>> plots = null,
                        List<Nullable<int>> refyearsets = null,
                        Nullable<double> minDiff = 0.0,
                        Nullable<bool> includeNullDiff = true,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(
                            variables: variables,
                            plots: plots,
                            refyearsets: refyearsets,
                            minDiff: minDiff,
                            includeNullDiff: includeNullDiff);

                        NpgsqlParameter pVariables = new(
                            parameterName: "variables",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        NpgsqlParameter pPlots = new(
                            parameterName: "plots",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        NpgsqlParameter pRefyearsets = new(
                            parameterName: "refyearsets",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        NpgsqlParameter pMinDiff = new(
                            parameterName: "minDiff",
                            parameterType: NpgsqlDbType.Double);

                        NpgsqlParameter pIncludeNullDiff = new(
                            parameterName: "includeNullDiff",
                            parameterType: NpgsqlDbType.Boolean);

                        if (variables != null)
                        {
                            pVariables.Value = variables.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pVariables.Value = DBNull.Value;
                        }

                        if (plots != null)
                        {
                            pPlots.Value = plots.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pPlots.Value = DBNull.Value;
                        }

                        if (refyearsets != null)
                        {
                            pRefyearsets.Value = refyearsets.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pRefyearsets.Value = DBNull.Value;
                        }

                        if (minDiff != null)
                        {
                            pMinDiff.Value = (double)minDiff;
                        }
                        else
                        {
                            pMinDiff.Value = DBNull.Value;
                        }

                        if (includeNullDiff != null)
                        {
                            pIncludeNullDiff.Value = (bool)includeNullDiff;
                        }
                        else
                        {
                            pIncludeNullDiff.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteQuery(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pVariables, pPlots, pRefyearsets, pMinDiff, pIncludeNullDiff);
                    }

                    #endregion Methods

                }

                #endregion FnAddPlotTargetAttr

            }
        }
    }
}