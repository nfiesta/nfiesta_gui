﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {

                #region FnImportSaveLDsity

                /// <summary>
                /// Wrapper for stored procedure fn_import_save_ldsity.
                /// </summary>
                public static class FnImportSaveLDsity
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_import_save_ldsity";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_ldsity integer, ",
                            $"_label character varying, ",
                            $"_description text, ",
                            $"_label_en character varying, ",
                            $"_description_en text, ",
                            $"_ldsity_object integer, ",
                            $"_column_expression text, ",
                            $"_unit_of_measure integer, ",
                            $"_area_domain_category integer[], ",
                            $"_sub_population_category integer[], ",
                            $"_definition_variant integer[]; ",
                            $"returns: integer");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                       = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    $SchemaName.$Name(",
                            $"@ldsity, ",
                            $"@label, ",
                            $"@description, ",
                            $"@labelEn, ",
                            $"@descriptionEn, ",
                            $"@ldsityObject, ",
                            $"@columnExpression, ",
                            $"@unitOfMeasure, ",
                            $"@areaDomainCategory, ",
                            $"@subPopulationCategory, ",
                            $"@definitionVariant",
                            $")::integer AS $Name;");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Execute stored procedure fn_import_save_ldsity
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="ldsity"></param>
                    /// <param name="label"></param>
                    /// <param name="description"></param>
                    /// <param name="labelEn"></param>
                    /// <param name="descriptionEn"></param>
                    /// <param name="ldsityObject"></param>
                    /// <param name="columnExpression"></param>
                    /// <param name="unitOfMeasure"></param>
                    /// <param name="areaDomainCategory"></param>
                    /// <param name="subPopulationCategory"></param>
                    /// <param name="definitionVariant"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> ldsity,
                        string label,
                        string description,
                        string labelEn,
                        string descriptionEn,
                        Nullable<int> ldsityObject,
                        string columnExpression,
                        Nullable<int> unitOfMeasure,
                        List<Nullable<int>> areaDomainCategory,
                        List<Nullable<int>> subPopulationCategory,
                        List<Nullable<int>> definitionVariant,
                        NpgsqlTransaction transaction = null)
                    {
                        Nullable<int> val = Execute(
                            database: database,
                            ldsity: ldsity,
                            label: label,
                            description: description,
                            labelEn: labelEn,
                            descriptionEn: descriptionEn,
                            ldsityObject: ldsityObject,
                            columnExpression: columnExpression,
                            unitOfMeasure: unitOfMeasure,
                            areaDomainCategory: areaDomainCategory,
                            subPopulationCategory: subPopulationCategory,
                            definitionVariant: definitionVariant,
                            transaction: transaction);

                        DataTable dt = new();

                        dt.Columns.Add(column: new DataColumn()
                        {
                            ColumnName = Name,
                            DataType = Type.GetType(typeName: "System.Int32")
                        });

                        DataRow row = dt.NewRow();

                        Functions.SetNIntArg(
                            row: row,
                            name: Name,
                            val: val);

                        dt.Rows.Add(row: row);

                        return dt;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_import_save_ldsity
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="ldsity"></param>
                    /// <param name="label"></param>
                    /// <param name="description"></param>
                    /// <param name="labelEn"></param>
                    /// <param name="descriptionEn"></param>
                    /// <param name="ldsityObject"></param>
                    /// <param name="columnExpression"></param>
                    /// <param name="unitOfMeasure"></param>
                    /// <param name="areaDomainCategory"></param>
                    /// <param name="subPopulationCategory"></param>
                    /// <param name="definitionVariant"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Integer</returns>
                    public static Nullable<int> Execute(
                        NfiEstaDB database,
                        Nullable<int> ldsity,
                        string label,
                        string description,
                        string labelEn,
                        string descriptionEn,
                        Nullable<int> ldsityObject,
                        string columnExpression,
                        Nullable<int> unitOfMeasure,
                        List<Nullable<int>> areaDomainCategory,
                        List<Nullable<int>> subPopulationCategory,
                        List<Nullable<int>> definitionVariant,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = SQL;

                        CommandText = CommandText.Replace(
                            oldValue: "@ldsity",
                            newValue: Functions.PrepNIntArg(arg: ldsity));

                        CommandText = CommandText.Replace(
                            oldValue: "@label",
                            newValue: Functions.PrepStringArg(arg: label));

                        CommandText = CommandText.Replace(
                            oldValue: "@description",
                            newValue: Functions.PrepStringArg(arg: description));

                        CommandText = CommandText.Replace(
                            oldValue: "@labelEn",
                            newValue: Functions.PrepStringArg(arg: labelEn));

                        CommandText = CommandText.Replace(
                            oldValue: "@descriptionEn",
                            newValue: Functions.PrepStringArg(arg: descriptionEn));

                        CommandText = CommandText.Replace(
                            oldValue: "@ldsityObject",
                            newValue: Functions.PrepNIntArg(arg: ldsityObject));

                        CommandText = CommandText.Replace(
                           oldValue: "@columnExpression",
                           newValue: Functions.PrepStringArg(arg: columnExpression));

                        CommandText = CommandText.Replace(
                            oldValue: "@unitOfMeasure",
                            newValue: Functions.PrepNIntArg(arg: unitOfMeasure));

                        CommandText = CommandText.Replace(
                            oldValue: "@areaDomainCategory",
                            newValue: Functions.PrepNIntArrayArg(args: areaDomainCategory, dbType: "int4"));

                        CommandText = CommandText.Replace(
                            oldValue: "@subPopulationCategory",
                            newValue: Functions.PrepNIntArrayArg(args: subPopulationCategory, dbType: "int4"));

                        CommandText = CommandText.Replace(
                            oldValue: "@definitionVariant",
                            newValue: Functions.PrepNIntArrayArg(args: definitionVariant, dbType: "int4"));

                        NpgsqlParameter pLDsity = new(
                            parameterName: "ldsity",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pLabel = new(
                            parameterName: "label",
                            parameterType: NpgsqlDbType.Varchar);

                        NpgsqlParameter pDescription = new(
                            parameterName: "description",
                            parameterType: NpgsqlDbType.Text);

                        NpgsqlParameter pLabelEn = new(
                            parameterName: "labelEn",
                            parameterType: NpgsqlDbType.Varchar);

                        NpgsqlParameter pDescriptionEn = new(
                            parameterName: "descriptionEn",
                            parameterType: NpgsqlDbType.Text);

                        NpgsqlParameter pLDsityObject = new(
                            parameterName: "ldsityObject",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pColumnExpression = new(
                            parameterName: "columnExpression",
                            parameterType: NpgsqlDbType.Text);

                        NpgsqlParameter pUnitOfMeasure = new(
                           parameterName: "unitOfMeasure",
                           parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pAreaDomainCategory = new(
                          parameterName: "areaDomainCategory",
                          parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        NpgsqlParameter pSubPopulationCategory = new(
                          parameterName: "subPopulationCategory",
                          parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        NpgsqlParameter pDefinitionVariant = new(
                          parameterName: "definitionVariant",
                          parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        if (ldsity != null)
                        {
                            pLDsity.Value = (int)ldsity;
                        }
                        else
                        {
                            pLDsity.Value = DBNull.Value;
                        }

                        if (label != null)
                        {
                            pLabel.Value = (string)label;
                        }
                        else
                        {
                            pLabel.Value = DBNull.Value;
                        }

                        if (description != null)
                        {
                            pDescription.Value = (string)description;
                        }
                        else
                        {
                            pDescription.Value = DBNull.Value;
                        }

                        if (labelEn != null)
                        {
                            pLabelEn.Value = (string)labelEn;
                        }
                        else
                        {
                            pLabelEn.Value = DBNull.Value;
                        }

                        if (descriptionEn != null)
                        {
                            pDescriptionEn.Value = (string)descriptionEn;
                        }
                        else
                        {
                            pDescriptionEn.Value = DBNull.Value;
                        }

                        if (ldsityObject != null)
                        {
                            pLDsityObject.Value = (int)ldsityObject;
                        }
                        else
                        {
                            pLDsityObject.Value = DBNull.Value;
                        }

                        if (columnExpression != null)
                        {
                            pColumnExpression.Value = (string)columnExpression;
                        }
                        else
                        {
                            pColumnExpression.Value = DBNull.Value;
                        }

                        if (unitOfMeasure != null)
                        {
                            pUnitOfMeasure.Value = (int)unitOfMeasure;
                        }
                        else
                        {
                            pUnitOfMeasure.Value = DBNull.Value;
                        }

                        if (areaDomainCategory != null)
                        {
                            pAreaDomainCategory.Value = areaDomainCategory.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pAreaDomainCategory.Value = DBNull.Value;
                        }

                        if (subPopulationCategory != null)
                        {
                            pSubPopulationCategory.Value = subPopulationCategory.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pSubPopulationCategory.Value = DBNull.Value;
                        }

                        if (definitionVariant != null)
                        {
                            pDefinitionVariant.Value = definitionVariant.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pDefinitionVariant.Value = DBNull.Value;
                        }

                        string strResult = database.Postgres.ExecuteScalar(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pLDsity, pLabel, pDescription, pLabelEn, pDescriptionEn,
                            pLDsityObject, pColumnExpression, pUnitOfMeasure,
                            pAreaDomainCategory, pSubPopulationCategory, pDefinitionVariant);

                        if (String.IsNullOrEmpty(value: strResult))
                        {
                            return null;
                        }
                        else
                        {
                            if (Int32.TryParse(s: strResult, result: out int intResult))
                            {
                                return intResult;
                            }
                            else
                            {
                                return null;
                            }
                        }
                    }

                    #endregion Methods

                }

                #endregion FnImportSaveLDsity

            }

        }
    }
}