﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {

                #region FnCheckClassificationRule

                /// <summary>
                /// Wrapper for stored procedure fn_check_classification_rule.
                /// Function returns number of objects classified by the rule
                /// for given ldsity and attribute_type hierarchy.
                /// </summary>
                public static class FnCheckClassificationRule
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_check_classification_rule";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_ldsity integer, ",
                            $"_ldsity_object integer, ",
                            $"_rule text, ",
                            $"_panel_refyearset integer DEFAULT NULL::integer, ",
                            $"_adc integer[] DEFAULT NULL::integer[], ",
                            $"_spc integer[] DEFAULT NULL::integer[], ",
                            $"_use_negative boolean DEFAULT false; ",
                            $"returns: TABLE(",
                            $"compliance_status boolean, ",
                            $"no_of_objects integer)");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    {TDFnCheckClassificationRuleTypeList.ColId.SQL(TDFnCheckClassificationRuleTypeList.Cols, Name)}",
                            $"    {TDFnCheckClassificationRuleTypeList.ColComplianceStatus.SQL(TDFnCheckClassificationRuleTypeList.Cols, Name)}",
                            $"    {TDFnCheckClassificationRuleTypeList.ColNumberOfObjects.SQL(TDFnCheckClassificationRuleTypeList.Cols, Name, isLastOne: true)}",
                            $"FROM {Environment.NewLine}",
                            $"    $SchemaName.$Name{Environment.NewLine}",
                            $"({Environment.NewLine}",
                            $"@ldsityId,            -- ldsityId{Environment.NewLine}",
                            $"@ldsityObjectId,      -- ldsityObjectId{Environment.NewLine}",
                            $"@rule,                -- rule{Environment.NewLine}",
                            $"@panelRefYearSetId,   -- panelRefYearSetId{Environment.NewLine}",
                            $"@adc,                 -- adc{Environment.NewLine}",
                            $"@spc,                 -- spc{Environment.NewLine}",
                            $"@useNegative         -- useNegative{Environment.NewLine}",
                            $") AS {Name}{Environment.NewLine}",
                            $"ORDER BY{Environment.NewLine}",
                            $"    {Name}.{TDFnCheckClassificationRuleTypeList.ColComplianceStatus.DbName};{Environment.NewLine}");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Execute stored procedure fn_check_classification_rule
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="ldsityId">Local density contribution identifier</param>
                    /// <param name="ldsityObjectId">Local density object identifier</param>
                    /// <param name="rule">Text of the classification rule</param>
                    /// <param name="panelRefYearSetId">Pair of panel with corresponding reference year set identifier</param>
                    /// <param name="adc">Area domain categories</param>
                    /// <param name="spc">Subpopulation categories</param>
                    /// <param name="useNegative">Positive (false) or negative (true) classification rule</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Returns number of objects classified by the rule for given ldsity and attribute_type hierarchy.</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> ldsityId,
                        Nullable<int> ldsityObjectId,
                        string rule,
                        Nullable<int> panelRefYearSetId,
                        List<List<Nullable<int>>> adc,
                        List<List<Nullable<int>>> spc,
                        Nullable<bool> useNegative,
                        NpgsqlTransaction transaction = null)
                    {
                        string sql = SQL;

                        sql = sql.Replace(
                            oldValue: "@adc",
                            newValue: Functions.PrepNIntArrayOfArrayArg(args: adc, dbType: "int4"));

                        sql = sql.Replace(
                            oldValue: "@spc",
                            newValue: Functions.PrepNIntArrayOfArrayArg(args: spc, dbType: "int4"));

                        CommandText = sql;

                        CommandText = CommandText.Replace(
                            oldValue: "@ldsityId",
                            newValue: Functions.PrepNIntArg(arg: ldsityId));

                        CommandText = CommandText.Replace(
                            oldValue: "@ldsityObjectId",
                            newValue: Functions.PrepNIntArg(arg: ldsityObjectId));

                        CommandText = CommandText.Replace(
                            oldValue: "@rule",
                            newValue: Functions.PrepStringArg(arg: rule));

                        CommandText = CommandText.Replace(
                            oldValue: "@panelRefYearSetId",
                            newValue: Functions.PrepNIntArg(arg: panelRefYearSetId));

                        CommandText = CommandText.Replace(
                            oldValue: "@useNegative",
                            newValue: Functions.PrepNBoolArg(arg: useNegative));

                        NpgsqlParameter pLDsityId = new(
                            parameterName: "ldsityId",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pLDsityObjectId = new(
                            parameterName: "ldsityObjectId",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pRule = new(
                            parameterName: "rule",
                            parameterType: NpgsqlDbType.Text);

                        NpgsqlParameter pPanelRefYearSetId = new(
                            parameterName: "panelRefYearSetId",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pUseNegative = new(
                            parameterName: "useNegative",
                            parameterType: NpgsqlDbType.Boolean);

                        if (ldsityId != null)
                        {
                            pLDsityId.Value = (int)ldsityId;
                        }
                        else
                        {
                            pLDsityId.Value = DBNull.Value;
                        }

                        if (ldsityObjectId != null)
                        {
                            pLDsityObjectId.Value = (int)ldsityObjectId;
                        }
                        else
                        {
                            pLDsityObjectId.Value = DBNull.Value;
                        }

                        if (rule != null)
                        {
                            pRule.Value = (string)rule;
                        }
                        else
                        {
                            pRule.Value = DBNull.Value;
                        }

                        if (panelRefYearSetId != null)
                        {
                            pPanelRefYearSetId.Value = (int)panelRefYearSetId;
                        }
                        else
                        {
                            pPanelRefYearSetId.Value = DBNull.Value;
                        }

                        if (useNegative != null)
                        {
                            pUseNegative.Value = (bool)useNegative;
                        }
                        else
                        {
                            pUseNegative.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteQuery(
                            sqlCommand: sql,
                            transaction: transaction,
                            pLDsityId, pLDsityObjectId, pRule,
                            pPanelRefYearSetId, pUseNegative);
                    }

                    /// <summary>
                    /// Execute stored procedure fn_check_classification_rule
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="ldsityId">Local density contribution identifier</param>
                    /// <param name="ldsityObjectId">Local density object identifier</param>
                    /// <param name="rule">Text of the classification rule</param>
                    /// <param name="panelRefYearSetId">Pair of panel with corresponding reference year set identifier</param>
                    /// <param name="adc">Area domain categories</param>
                    /// <param name="spc">Subpopulation categories</param>
                    /// <param name="useNegative">Positive (false) or negative (true) classification rule</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>List of number of objects classified by the rule for given ldsity and attribute_type hierarchy.</returns>
                    public static TDFnCheckClassificationRuleTypeList Execute(
                        NfiEstaDB database,
                        Nullable<int> ldsityId,
                        Nullable<int> ldsityObjectId,
                        string rule,
                        Nullable<int> panelRefYearSetId,
                        List<List<Nullable<int>>> adc,
                        List<List<Nullable<int>>> spc,
                        Nullable<bool> useNegative,
                        NpgsqlTransaction transaction = null)
                    {
                        System.Diagnostics.Stopwatch stopWatch = new();
                        stopWatch.Start();

                        DataTable data = ExecuteQuery(
                                database: database,
                                ldsityId: ldsityId,
                                ldsityObjectId: ldsityObjectId,
                                rule: rule,
                                panelRefYearSetId: panelRefYearSetId,
                                adc: adc,
                                spc: spc,
                                useNegative: useNegative,
                                transaction: transaction);
                        data.TableName = Name;

                        stopWatch.Stop();

                        return new TDFnCheckClassificationRuleTypeList(
                            database: database,
                            data: data,
                            loadingTime: 0.001 * stopWatch.ElapsedMilliseconds,
                            storedProcedure: CommandText)
                        {
                            LDsityId = ldsityId,
                            LDsityObjectId = ldsityObjectId,
                            Rule = rule,
                            PanelRefYearSetId = panelRefYearSetId,
                            Adc = adc,
                            Spc = spc,
                            UseNegative = useNegative
                        };
                    }

                    #endregion Methods

                }

                #endregion FnCheckClassificationRule

            }

        }
    }
}