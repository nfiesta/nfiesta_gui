﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {

                #region FnCheckSpcRuleHasPanelRef

                /// <summary>
                /// Wrapper for stored procedure fn_check_spc_rule_has_panelref.
                /// Function provides test if classification rule has some records
                /// in cm_spc2classrule2panel_refyearset table.
                /// </summary>
                public static class FnCheckSpcRuleHasPanelRef
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_check_spc_rule_has_panelref";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_id integer; ",
                            $"returns: boolean");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    $SchemaName.$Name(@spcToClassificationRuleId)::boolean AS {Name};{Environment.NewLine}");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Execute stored procedure fn_check_spc_rule_has_panelref
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="spcToClassificationRuleId">Classification rule for subpopulation category identifier</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>DataTable with boolean column: true/false
                    /// Does classification rule have some records in cm_spc2classrule2panel_refyearset table?</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> spcToClassificationRuleId,
                        NpgsqlTransaction transaction = null)
                    {
                        Nullable<bool> val = Execute(
                            database: database,
                            spcToClassificationRuleId: spcToClassificationRuleId,
                            transaction: transaction);

                        DataTable dt = new();

                        dt.Columns.Add(column: new DataColumn()
                        {
                            ColumnName = Name,
                            DataType = Type.GetType(typeName: "System.Boolean")
                        });

                        DataRow row = dt.NewRow();

                        Functions.SetNBoolArg(
                            row: row,
                            name: Name,
                            val: val);

                        dt.Rows.Add(row: row);

                        return dt;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_check_spc_rule_has_panelref
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="spcToClassificationRuleId">Classification rule for subpopulation category identifier</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns> true/false
                    /// Does classification rule have some records in cm_spc2classrule2panel_refyearset table?</returns>
                    public static Nullable<bool> Execute(
                        NfiEstaDB database,
                        Nullable<int> spcToClassificationRuleId,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = SQL;

                        CommandText = CommandText.Replace(
                            oldValue: "@spcToClassificationRuleId",
                            newValue: Functions.PrepNIntArg(arg: spcToClassificationRuleId));

                        NpgsqlParameter pSpcToClassificationRuleId = new(
                            parameterName: "spcToClassificationRuleId",
                            parameterType: NpgsqlDbType.Integer);

                        if (spcToClassificationRuleId != null)
                        {
                            pSpcToClassificationRuleId.Value = (int)spcToClassificationRuleId;
                        }
                        else
                        {
                            pSpcToClassificationRuleId.Value = DBNull.Value;
                        }

                        string strResult = database.Postgres.ExecuteScalar(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pSpcToClassificationRuleId);

                        if (String.IsNullOrEmpty(value: strResult))
                        {
                            return null;
                        }
                        else
                        {
                            if (Boolean.TryParse(value: strResult, result: out bool boolResult))
                            {
                                return boolResult;
                            }
                            else
                            {
                                return null;
                            }
                        }
                    }

                    #endregion Methods

                }

                #endregion FnCheckSpcRuleHasPanelRef

            }

        }
    }
}