﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {

                #region FnGetLDsityObjectForLDsityObjectADSP

                /// <summary>
                /// Wrapper for stored procedure fn_get_ldsity_object4ld_object_adsp.
                /// Function returns records from c_ldsity_object table, optionally for given topic.
                /// </summary>
                public static class FnGetLDsityObjectForLDsityObjectADSP
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_get_ldsity_object4ld_object_adsp";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_ldsity_object integer, ",
                            $"_areal_or_population integer, ",
                            $"_id integer, ",
                            $"_use_negative boolean DEFAULT false; ",
                            $"returns: ",
                            $"TABLE(",
                            $"id integer, ",
                            $"label character varying, ",
                            $"description text, ",
                            $"label_en character varying, ",
                            $"description_en text, ",
                            $"table_name character varying, ",
                            $"classification_rule boolean, ",
                            $"use_negative boolean)");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                             $"SELECT{Environment.NewLine}",
                             $"    {TDLDsityObjectList.ColId.SQL(TDLDsityObjectList.Cols, Name)}",
                             $"    {TDLDsityObjectList.ColLabelCs.SQL(TDLDsityObjectList.Cols, Name)}",
                             $"    {TDLDsityObjectList.ColDescriptionCs.SQL(TDLDsityObjectList.Cols, Name)}",
                             $"    {TDLDsityObjectList.ColTableName.SQL(TDLDsityObjectList.Cols, Name)}",
                             $"    {TDLDsityObjectList.ColUpperObjectId.SQL(TDLDsityObjectList.Cols, Name, setToNull: true)}",
                             $"    {TDLDsityObjectList.ColArealOrPopulationId.SQL(TDLDsityObjectList.Cols, Name, setToNull: true)}",
                             $"    {TDLDsityObjectList.ColColumnForUpperObject.SQL(TDLDsityObjectList.Cols, Name, setToNull: true)}",
                             $"    {TDLDsityObjectList.ColLabelEn.SQL(TDLDsityObjectList.Cols, Name)}",
                             $"    {TDLDsityObjectList.ColDescriptionEn.SQL(TDLDsityObjectList.Cols, Name)}",
                             $"    {TDLDsityObjectList.ColFilter.SQL(TDLDsityObjectList.Cols, Name, setToNull: true)}",
                             $"    {TDLDsityObjectList.ColSuperiorLDsityObjectId.SQL(TDLDsityObjectList.Cols, Name, setToNull: true)}",
                             $"    {TDLDsityObjectList.ColClassificationRule.SQL(TDLDsityObjectList.Cols, Name)}",
                             $"    {TDLDsityObjectList.ColUseNegative.SQL(TDLDsityObjectList.Cols, Name)}",
                             $"    {TDLDsityObjectList.ColLDsityObjectGroupId.SQL(TDLDsityObjectList.Cols, Name, isLastOne: true, setToNull: true)}",
                             $"FROM{Environment.NewLine}",
                             $"    $SchemaName.$Name(@ldsityObjectId, @arealOrPopulationId, @domainId, @useNegative) AS {Name}{Environment.NewLine}",
                             $"ORDER BY{Environment.NewLine}",
                             $"    {Name}.{TDLDsityObjectList.ColId.DbName};{Environment.NewLine}"
                             );

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Execute stored procedure fn_get_ldsity_object4ld_object_adsp
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="ldsityObjectId">Local density object identifier</param>
                    /// <param name="arealOrPopulationId">Areal or population category identifier</param>
                    /// <param name="domainId">Area domain or subpopulation identifier</param>
                    /// <param name="useNegative">Use negative local density contribution</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table local density objects</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> ldsityObjectId,
                        Nullable<int> arealOrPopulationId,
                        Nullable<int> domainId,
                        Nullable<bool> useNegative,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = SQL;

                        CommandText = CommandText.Replace(
                            oldValue: "@ldsityObjectId",
                            newValue: Functions.PrepNIntArg(arg: ldsityObjectId));

                        CommandText = CommandText.Replace(
                            oldValue: "@arealOrPopulationId",
                            newValue: Functions.PrepNIntArg(arg: arealOrPopulationId));

                        CommandText = CommandText.Replace(
                            oldValue: "@domainId",
                            newValue: Functions.PrepNIntArg(arg: domainId));

                        CommandText = CommandText.Replace(
                            oldValue: "@useNegative",
                            newValue: Functions.PrepNBoolArg(arg: useNegative));

                        NpgsqlParameter pLDsityObjectId = new(
                            parameterName: "ldsityObjectId",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pArealOrPopulationId = new(
                            parameterName: "arealOrPopulationId",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pDomainId = new(
                            parameterName: "domainId",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pUseNegative = new(
                            parameterName: "useNegative",
                            parameterType: NpgsqlDbType.Boolean);

                        if (ldsityObjectId != null)
                        {
                            pLDsityObjectId.Value = (int)ldsityObjectId;
                        }
                        else
                        {
                            pLDsityObjectId.Value = DBNull.Value;
                        }

                        if (arealOrPopulationId != null)
                        {
                            pArealOrPopulationId.Value = (int)arealOrPopulationId;
                        }
                        else
                        {
                            pArealOrPopulationId.Value = DBNull.Value;
                        }

                        if (domainId != null)
                        {
                            pDomainId.Value = (int)domainId;
                        }
                        else
                        {
                            pDomainId.Value = DBNull.Value;
                        }

                        if (useNegative != null)
                        {
                            pUseNegative.Value = (bool)useNegative;
                        }
                        else
                        {
                            pUseNegative.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteQuery(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pLDsityObjectId, pArealOrPopulationId, pDomainId, pUseNegative);
                    }

                    /// <summary>
                    /// Execute stored procedure fn_get_ldsity_object4ld_object_adsp
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="ldsityObjectId">Local density object identifier</param>
                    /// <param name="arealOrPopulationId">Areal or population category identifier</param>
                    /// <param name="domainId">Area domain or subpopulation identifier</param>
                    /// <param name="useNegative">Use negative local density contribution</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>List of local density objects</returns>
                    public static TDLDsityObjectList Execute(
                        NfiEstaDB database,
                        Nullable<int> ldsityObjectId,
                        Nullable<int> arealOrPopulationId,
                        Nullable<int> domainId,
                        Nullable<bool> useNegative,
                        NpgsqlTransaction transaction = null)
                    {
                        return
                            new TDLDsityObjectList(
                                database: database,
                                data: ExecuteQuery(
                                    database: database,
                                    ldsityObjectId: ldsityObjectId,
                                    arealOrPopulationId: arealOrPopulationId,
                                    domainId: domainId,
                                    useNegative: useNegative,
                                    transaction: transaction))
                            { StoredProcedure = CommandText };
                    }

                    #endregion Methods

                }

                #endregion FnGetLDsityObjectForLDsityObjectADSP

            }

        }
    }
}