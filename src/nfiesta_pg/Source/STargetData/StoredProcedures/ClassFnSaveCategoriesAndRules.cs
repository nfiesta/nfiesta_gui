﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {

                #region FnSaveCategoriesAndRules

                /// <summary>
                /// Wrapper for stored procedure fn_save_categories_and_rules.
                /// Function inserts all necessary data into
                /// lookups/tables with area_domain/sub_population categories
                /// and its classification rules.
                /// </summary>
                public static class FnSaveCategoriesAndRules
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_save_categories_and_rules";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_ldsity_object integer, ",
                            $"_areal_or_pop integer, ",
                            $"_classification_type integer, ",
                            $"_label character varying, ",
                            $"_description text, ",
                            $"_label_en character varying, ",
                            $"_description_en text, ",
                            $"_cat_label character varying[], ",
                            $"_cat_description text[], ",
                            $"_cat_label_en character varying[], ",
                            $"_cat_description_en text[], ",
                            $"_rules text[], ",
                            $"_use_negative boolean[], ",
                            $"_panel_refyearset integer[], ",
                            $"_adc integer[] DEFAULT NULL::integer[], ",
                            $"_spc integer[] DEFAULT NULL::integer[]; ",
                            $"returns: ",
                            $"TABLE(",
                            $"areal_or_pop integer, ",
                            $"cat_label character varying, ",
                            $"cat_label_en character varying, ",
                            $"parent_id integer, ",
                            $"category_id integer, ",
                            $"classification_rule integer, ",
                            $"refyearset2panel integer)");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    areal_or_pop,{Environment.NewLine}",
                            $"    cat_label,{Environment.NewLine}",
                            $"    cat_label_en,{Environment.NewLine}",
                            $"    parent_id,{Environment.NewLine}",
                            $"    category_id,{Environment.NewLine}",
                            $"    classification_rule,{Environment.NewLine}",
                            $"    refyearset2panel{Environment.NewLine}",
                            $"FROM{Environment.NewLine}",
                            $"    {SchemaName}.{Name}{Environment.NewLine}",
                            $"({Environment.NewLine}",
                            $"@ldsityObjectId,          -- ldsityObjectId{Environment.NewLine}",
                            $"@arealOrPopulationId,     -- arealOrPopulationId{Environment.NewLine}",
                            $"@classificationTypeId,    -- classificationTypeId{Environment.NewLine}",
                            $"@labelCs,                 -- labelCs{Environment.NewLine}",
                            $"@descriptionCs,           -- descriptionCs{Environment.NewLine}",
                            $"@labelEn,                 -- labelEn{Environment.NewLine}",
                            $"@descriptionEn,           -- descriptionEn{Environment.NewLine}",
                            $"@categoryLabelCs,         -- categoryLabelCs{Environment.NewLine}",
                            $"@categoryDescriptionCs,   -- categoryDescriptionCs{Environment.NewLine}",
                            $"@categoryLabelEn,         -- categoryLabelEn{Environment.NewLine}",
                            $"@categoryDescriptionEn,   -- categoryDescriptionEn{Environment.NewLine}",
                            $"@rules,                   -- rules{Environment.NewLine}",
                            $"@useNegative,             -- useNegative{Environment.NewLine}",
                            $"@panelRefYearSetIds,      -- panelRefYearSetIds{Environment.NewLine}",
                            $"@adc,                     -- adc{Environment.NewLine}",
                            $"@spc                      -- spc{Environment.NewLine}",
                            $");{Environment.NewLine}");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Execute stored procedure fn_save_categories_and_rules
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="ldsityObjectId">Local density object identifier</param>
                    /// <param name="arealOrPopulationId">Areal or population identifier</param>
                    /// <param name="classificationTypeId">Classification type identifier</param>
                    /// <param name="labelCs">Domain label in national language</param>
                    /// <param name="descriptionCs">Domain description in national language</param>
                    /// <param name="labelEn">Domain label in English</param>
                    /// <param name="descriptionEn">Domain description in English</param>
                    /// <param name="categoryLabelCs">List of category labels in national language</param>
                    /// <param name="categoryDescriptionCs">List of category descriptions in national language</param>
                    /// <param name="categoryLabelEn">List of category labels in English</param>
                    /// <param name="categoryDescriptionEn">List of category descriptions in English</param>
                    /// <param name="rules">List of classification rules</param>
                    /// <param name="useNegative">List of use negative local density contributions </param>
                    /// <param name="panelRefYearSetIds">List of panel reference year set identifiers</param>
                    /// <param name="adc">List for each new area domain category of list of superior area domain category identifiers</param>
                    /// <param name="spc">List for each new subpopulation category of list of superior subpopulation category identifiers</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table with areal or population identifier, parent identifier, category identifier,
                    /// classification rule identifier and panel reference year set identifier</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> ldsityObjectId,
                        Nullable<int> arealOrPopulationId,
                        Nullable<int> classificationTypeId,
                        string labelCs,
                        string descriptionCs,
                        string labelEn,
                        string descriptionEn,
                        List<string> categoryLabelCs,
                        List<string> categoryDescriptionCs,
                        List<string> categoryLabelEn,
                        List<string> categoryDescriptionEn,
                        List<string> rules,
                        List<Nullable<bool>> useNegative,
                        List<List<Nullable<int>>> panelRefYearSetIds,
                        List<List<Nullable<int>>> adc = null,
                        List<List<Nullable<int>>> spc = null,
                        NpgsqlTransaction transaction = null
                        )
                    {
                        string sql = SQL;

                        sql = sql.Replace(
                            oldValue: "@panelRefYearSetIds",
                            newValue: Functions.PrepNIntArrayOfArrayArg(args: panelRefYearSetIds, dbType: "int4"));

                        sql = sql.Replace(
                            oldValue: "@adc",
                            newValue: Functions.PrepNIntArrayOfArrayArg(args: adc, dbType: "int4"));

                        sql = sql.Replace(
                            oldValue: "@spc",
                            newValue: Functions.PrepNIntArrayOfArrayArg(args: spc, dbType: "int4"));

                        CommandText = sql;

                        CommandText = CommandText.Replace(
                            oldValue: "@ldsityObjectId",
                            newValue: Functions.PrepNIntArg(arg: ldsityObjectId));

                        CommandText = CommandText.Replace(
                            oldValue: "@arealOrPopulationId",
                            newValue: Functions.PrepNIntArg(arg: arealOrPopulationId));

                        CommandText = CommandText.Replace(
                            oldValue: "@classificationTypeId",
                            newValue: Functions.PrepNIntArg(arg: classificationTypeId));

                        CommandText = CommandText.Replace(
                            oldValue: "@labelCs",
                            newValue: Functions.PrepStringArg(arg: labelCs));

                        CommandText = CommandText.Replace(
                            oldValue: "@descriptionCs",
                            newValue: Functions.PrepStringArg(arg: descriptionCs));

                        CommandText = CommandText.Replace(
                            oldValue: "@labelEn",
                            newValue: Functions.PrepStringArg(arg: labelEn));

                        CommandText = CommandText.Replace(
                            oldValue: "@descriptionEn",
                            newValue: Functions.PrepStringArg(arg: descriptionEn));

                        CommandText = CommandText.Replace(
                            oldValue: "@categoryLabelCs",
                            newValue: Functions.PrepStringArrayArg(args: categoryLabelCs, dbType: "character varying"));

                        CommandText = CommandText.Replace(
                            oldValue: "@categoryDescriptionCs",
                            newValue: Functions.PrepStringArrayArg(args: categoryDescriptionCs, dbType: "text"));

                        CommandText = CommandText.Replace(
                            oldValue: "@categoryLabelEn",
                            newValue: Functions.PrepStringArrayArg(args: categoryLabelEn, dbType: "character varying"));

                        CommandText = CommandText.Replace(
                            oldValue: "@categoryDescriptionEn",
                            newValue: Functions.PrepStringArrayArg(args: categoryDescriptionEn, dbType: "text"));

                        CommandText = CommandText.Replace(
                            oldValue: "@rules",
                            newValue: Functions.PrepStringArrayArg(args: rules, dbType: "text"));

                        CommandText = CommandText.Replace(
                            oldValue: "@useNegative",
                            newValue: Functions.PrepNBoolArrayArg(args: useNegative, dbType: "bool"));

                        NpgsqlParameter pLDsityObjectId = new(
                            parameterName: "ldsityObjectId",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pArealOrPopulationId = new(
                            parameterName: "arealOrPopulationId",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pClassificationTypeId = new(
                            parameterName: "classificationTypeId",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pLabelCs = new(
                            parameterName: "labelCs",
                            parameterType: NpgsqlDbType.Varchar);

                        NpgsqlParameter pDescriptionCs = new(
                            parameterName: "descriptionCs",
                            parameterType: NpgsqlDbType.Text);

                        NpgsqlParameter pLabelEn = new(
                            parameterName: "labelEn",
                            parameterType: NpgsqlDbType.Varchar);

                        NpgsqlParameter pDescriptionEn = new(
                            parameterName: "descriptionEn",
                            parameterType: NpgsqlDbType.Text);

                        NpgsqlParameter pCategoryLabelCs = new(
                            parameterName: "categoryLabelCs",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Varchar);

                        NpgsqlParameter pCategoryDescriptionCs = new(
                            parameterName: "categoryDescriptionCs",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Text);

                        NpgsqlParameter pCategoryLabelEn = new(
                            parameterName: "categoryLabelEn",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Varchar);

                        NpgsqlParameter pCategoryDescriptionEn = new(
                            parameterName: "categoryDescriptionEn",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Text);

                        NpgsqlParameter pRules = new(
                            parameterName: "rules",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Text);

                        NpgsqlParameter pUseNegative = new(
                            parameterName: "useNegative",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Boolean);

                        if (ldsityObjectId != null)
                        {
                            pLDsityObjectId.Value = (int)ldsityObjectId;
                        }
                        else
                        {
                            pLDsityObjectId.Value = DBNull.Value;
                        }

                        if (arealOrPopulationId != null)
                        {
                            pArealOrPopulationId.Value = (int)arealOrPopulationId;
                        }
                        else
                        {
                            pArealOrPopulationId.Value = DBNull.Value;
                        }

                        if (classificationTypeId != null)
                        {
                            pClassificationTypeId.Value = (int)classificationTypeId;
                        }
                        else
                        {
                            pClassificationTypeId.Value = DBNull.Value;
                        }

                        if (labelCs != null)
                        {
                            pLabelCs.Value = (string)labelCs;
                        }
                        else
                        {
                            pLabelCs.Value = DBNull.Value;
                        }

                        if (descriptionCs != null)
                        {
                            pDescriptionCs.Value = (string)descriptionCs;
                        }
                        else
                        {
                            pDescriptionCs.Value = DBNull.Value;
                        }

                        if (labelEn != null)
                        {
                            pLabelEn.Value = (string)labelEn;
                        }
                        else
                        {
                            pLabelEn.Value = DBNull.Value;
                        }

                        if (descriptionEn != null)
                        {
                            pDescriptionEn.Value = (string)descriptionEn;
                        }
                        else
                        {
                            pDescriptionEn.Value = DBNull.Value;
                        }

                        if (categoryLabelCs != null)
                        {
                            pCategoryLabelCs.Value = categoryLabelCs.ToArray<string>();
                        }
                        else
                        {
                            pCategoryLabelCs.Value = DBNull.Value;
                        }

                        if (categoryDescriptionCs != null)
                        {
                            pCategoryDescriptionCs.Value = categoryDescriptionCs.ToArray<string>();
                        }
                        else
                        {
                            pCategoryDescriptionCs.Value = DBNull.Value;
                        }

                        if (categoryLabelEn != null)
                        {
                            pCategoryLabelEn.Value = categoryLabelEn.ToArray<string>();
                        }
                        else
                        {
                            pCategoryLabelEn.Value = DBNull.Value;
                        }

                        if (categoryDescriptionEn != null)
                        {
                            pCategoryDescriptionEn.Value = categoryDescriptionEn.ToArray<string>();
                        }
                        else
                        {
                            pCategoryDescriptionEn.Value = DBNull.Value;
                        }

                        if (rules != null)
                        {
                            pRules.Value = rules.ToArray<string>();
                        }
                        else
                        {
                            pRules.Value = DBNull.Value;
                        }

                        if (useNegative != null)
                        {
                            pUseNegative.Value = useNegative.ToArray<Nullable<bool>>();
                        }
                        else
                        {
                            pUseNegative.Value = DBNull.Value;
                        }

                        return database.Postgres.ExecuteQuery(
                            sqlCommand: sql,
                            transaction: transaction,
                            pLDsityObjectId, pArealOrPopulationId, pClassificationTypeId,
                            pLabelCs, pDescriptionCs, pLabelEn, pDescriptionEn,
                            pCategoryLabelCs, pCategoryDescriptionCs, pCategoryLabelEn, pCategoryDescriptionEn,
                            pRules, pUseNegative);
                    }

                    /// <summary>
                    /// Execute stored procedure fn_save_categories_and_rules
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="ldsityObjectId">Local density object identifier</param>
                    /// <param name="arealOrPopulationId">Areal or population identifier</param>
                    /// <param name="classificationTypeId">Classification type identifier</param>
                    /// <param name="labelCs">Domain label in national language</param>
                    /// <param name="descriptionCs">Domain description in national language</param>
                    /// <param name="labelEn">Domain label in English</param>
                    /// <param name="descriptionEn">Domain description in English</param>
                    /// <param name="categoryLabelCs">List of category labels in national language</param>
                    /// <param name="categoryDescriptionCs">List of category descriptions in national language</param>
                    /// <param name="categoryLabelEn">List of category labels in English</param>
                    /// <param name="categoryDescriptionEn">List of category descriptions in English</param>
                    /// <param name="rules">List of classification rules</param>
                    /// <param name="useNegative">List of use negative local density contributions </param>
                    /// <param name="panelRefYearSetIds">List of panel reference year set identifiers</param>
                    /// <param name="adc">List for each new area domain category of list of superior area domain category identifiers</param>
                    /// <param name="spc">List for each new subpopulation category of list of superior subpopulation category identifiers</param>
                    /// <param name="transaction">Transaction</param>
                    public static void Execute(
                        NfiEstaDB database,
                        Nullable<int> ldsityObjectId,
                        Nullable<int> arealOrPopulationId,
                        Nullable<int> classificationTypeId,
                        string labelCs,
                        string descriptionCs,
                        string labelEn,
                        string descriptionEn,
                        List<string> categoryLabelCs,
                        List<string> categoryDescriptionCs,
                        List<string> categoryLabelEn,
                        List<string> categoryDescriptionEn,
                        List<string> rules,
                        List<Nullable<bool>> useNegative,
                        List<List<Nullable<int>>> panelRefYearSetIds,
                        List<List<Nullable<int>>> adc = null,
                        List<List<Nullable<int>>> spc = null,
                        NpgsqlTransaction transaction = null
                        )
                    {
                        ExecuteQuery(
                            database: database,
                            ldsityObjectId: ldsityObjectId,
                            classificationTypeId: classificationTypeId,
                            arealOrPopulationId: arealOrPopulationId,
                            labelCs: labelCs,
                            descriptionCs: descriptionCs,
                            labelEn: labelEn,
                            descriptionEn: descriptionEn,
                            categoryLabelCs: categoryLabelCs,
                            categoryDescriptionCs: categoryDescriptionCs,
                            categoryLabelEn: categoryLabelEn,
                            categoryDescriptionEn: categoryDescriptionEn,
                            rules: rules,
                            useNegative: useNegative,
                            panelRefYearSetIds: panelRefYearSetIds,
                            adc: adc,
                            spc: spc,
                            transaction: transaction
                            );
                        return;
                    }

                    #endregion Methods

                }

                #endregion FnSaveCategoriesAndRules

            }

        }
    }
}