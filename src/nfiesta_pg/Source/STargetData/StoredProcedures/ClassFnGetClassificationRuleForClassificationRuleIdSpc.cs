﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {

                #region FnGetClassificationRuleForClassificationRuleIdSpc

                /// <summary>
                /// Wrapper for stored procedure fn_get_classification_rule4classification_rule_id.
                /// The function gets text of classification rules
                /// for their classification rule identifiers.
                /// </summary>
                public static class FnGetClassificationRuleForClassificationRuleIdSpc
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_get_classification_rule4classification_rule_id";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_classification_rule_type character varying, ",
                            $"_classification_rule_id integer[], ",
                            $"_ldsity_objects integer[]; ",
                            $"returns: text");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    $SchemaName.$Name(",
                            $"@classificationRuleType, ",
                            $"@classificationRuleIds, ",
                            $"@ldsityObjectIds",
                            $")::text AS $Name;");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Execute stored procedure fn_get_classification_rule4classification_rule_id
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="classificationRuleIds">List of classification rule identifiers</param>
                    /// <param name="ldsityObjectIds">List of local density identifiers</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table with text of classification rules for their classification rule identifiers</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        List<Nullable<int>> classificationRuleIds,
                        List<Nullable<int>> ldsityObjectIds,
                        NpgsqlTransaction transaction = null)
                    {
                        string val = Execute(
                            database: database,
                            classificationRuleIds: classificationRuleIds,
                            ldsityObjectIds: ldsityObjectIds,
                            transaction: transaction);

                        DataTable dt = new();

                        dt.Columns.Add(column: new DataColumn()
                        {
                            ColumnName = Name,
                            DataType = Type.GetType(typeName: "System.String")
                        });

                        DataRow row = dt.NewRow();

                        Functions.SetStringArg(
                            row: row,
                            name: Name,
                            val: val);

                        dt.Rows.Add(row: row);

                        return dt;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_get_classification_rule4classification_rule_id
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="classificationRuleIds">List of classification rule identifiers</param>
                    /// <param name="ldsityObjectIds">List of local density identifiers</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Text of classification rules for their classification rule identifiers</returns>
                    public static string Execute(
                        NfiEstaDB database,
                        List<Nullable<int>> classificationRuleIds,
                        List<Nullable<int>> ldsityObjectIds,
                        NpgsqlTransaction transaction = null)
                    {
                        string classificationRuleType = "spc";

                        CommandText = SQL;

                        CommandText = CommandText.Replace(
                            oldValue: "@classificationRuleType",
                            newValue: Functions.PrepStringArg(arg: classificationRuleType));

                        CommandText = CommandText.Replace(
                            oldValue: "@classificationRuleIds",
                            newValue: Functions.PrepNIntArrayArg(args: classificationRuleIds, dbType: "int4"));

                        CommandText = CommandText.Replace(
                            oldValue: "@ldsityObjectIds",
                            newValue: Functions.PrepNIntArrayArg(args: ldsityObjectIds, dbType: "int4"));

                        NpgsqlParameter pClassificationRuleType = new(
                            parameterName: "classificationRuleType",
                            parameterType: NpgsqlDbType.Varchar);

                        NpgsqlParameter pClassificationRuleIds = new(
                            parameterName: "classificationRuleIds",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        NpgsqlParameter pLDsityObjectIds = new(
                            parameterName: "ldsityObjectIds",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        if (classificationRuleType != null)
                        {
                            pClassificationRuleType.Value = (string)classificationRuleType;
                        }
                        else
                        {
                            pClassificationRuleType.Value = DBNull.Value;
                        }

                        if (classificationRuleIds != null)
                        {
                            pClassificationRuleIds.Value = classificationRuleIds.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pClassificationRuleIds.Value = DBNull.Value;
                        }

                        if (ldsityObjectIds != null)
                        {
                            pLDsityObjectIds.Value = ldsityObjectIds.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pLDsityObjectIds.Value = DBNull.Value;
                        }

                        return
                               database.Postgres.ExecuteScalar(
                                    sqlCommand: SQL,
                                    transaction: transaction,
                                    pClassificationRuleType,
                                    pClassificationRuleIds,
                                    pLDsityObjectIds);
                    }

                    #endregion Methods

                }

                #endregion FnGetClassificationRuleForClassificationRuleIdSpc

            }

        }
    }
}