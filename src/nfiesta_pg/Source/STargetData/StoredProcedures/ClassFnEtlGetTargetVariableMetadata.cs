﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {
                // fn_etl_get_target_variable_metadata

                #region FnEtlGetTargetVariableMetadata

                /// <summary>
                /// Wrapper for stored procedure fn_etl_get_target_variable_metadata.
                /// The function returs metadata for input target variable.
                /// </summary>
                public static class FnEtlGetTargetVariableMetadata
                {

                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_etl_get_target_variable_metadata";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_target_variable integer, ",
                            $"_national_language character varying DEFAULT 'en'::character varying(2); ",
                            $"returns: json");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                             $"SELECT{Environment.NewLine}",
                             $"    $SchemaName.$Name(@targetVariable, @nationalLanguage)::text AS {Name};{Environment.NewLine}");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="targetVariable"></param>
                    /// <param name="nationalLanguage"></param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        Nullable<int> targetVariable,
                        Language nationalLanguage = PostgreSQL.Language.CS)
                    {
                        string result = SQL;

                        result = result.Replace(
                             oldValue: "@targetVariable",
                             newValue: Functions.PrepNIntArg(arg: targetVariable));

                        result = result.Replace(
                            oldValue: "@nationalLanguage",
                            newValue: Functions.PrepStringArg(
                                arg: LanguageList.ISO_639_1(language: nationalLanguage)));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_etl_get_target_variable_metadata
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="targetVariable"></param>
                    /// <param name="nationalLanguage"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>Data table</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        Nullable<int> targetVariable,
                        Language nationalLanguage = PostgreSQL.Language.CS,
                        NpgsqlTransaction transaction = null)
                    {
                        string val = Execute(
                            database: database,
                            targetVariable: targetVariable,
                            nationalLanguage: nationalLanguage,
                            transaction: transaction);

                        DataTable dt = new();

                        dt.Columns.Add(column: new DataColumn()
                        {
                            ColumnName = Name,
                            DataType = Type.GetType(typeName: "System.String")
                        });

                        DataRow row = dt.NewRow();

                        Functions.SetStringArg(
                            row: row,
                            name: Name,
                            val: val);

                        dt.Rows.Add(row: row);

                        return dt;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_etl_get_target_variable_metadata
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="targetVariable"></param>
                    /// <param name="nationalLanguage"></param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns>json</returns>
                    public static string Execute(
                        NfiEstaDB database,
                        Nullable<int> targetVariable,
                        Language nationalLanguage = PostgreSQL.Language.CS,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(targetVariable: targetVariable, nationalLanguage: nationalLanguage);

                        NpgsqlParameter pTargetVariable = new(
                            parameterName: "targetVariable",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pNationalLanguage = new(
                            parameterName: "nationalLanguage",
                            parameterType: NpgsqlDbType.Varchar);

                        if (targetVariable != null)
                        {
                            pTargetVariable.Value = (int)targetVariable;
                        }
                        else
                        {
                            pTargetVariable.Value = DBNull.Value;
                        }

                        pNationalLanguage.Value =
                                LanguageList.ISO_639_1(language: nationalLanguage);

                        return
                            database.Postgres.ExecuteScalar(
                                sqlCommand: SQL,
                                transaction: transaction,
                                pTargetVariable, pNationalLanguage);
                    }

                    #endregion Methods

                }

                #endregion FnEtlGetTargetVariableMetadata

            }

        }
    }
}