﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Stored procedures of schema target_data
            /// </summary>
            public static partial class TDFunctions
            {

                #region FnSaveLDsityValues

                /// <summary>
                /// Wrapper for stored procedure fn_save_ldsity_values.
                /// The function for the specified list of input arguments
                /// inserts data into the t_available_datasets table
                /// and inserts data into the t_ldsity_values table (aggregated local density at the plot level).
                /// </summary>
                public static class FnSaveLDsityValues
                {

                    #region Constants

                    /// <summary>
                    /// Text indicating error-free completion of local density preparation
                    /// </summary>
                    public const string ValidResultText
                        = "The ldsity values were prepared successfully.";

                    #endregion Constants


                    #region Private Fields

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    private static string schemaName
                        = TDSchema.Name;

                    /// <summary>
                    /// Stored procedure name
                    /// </summary>
                    private static readonly string name
                        = "fn_save_ldsity_values";

                    /// <summary>
                    /// Stored procedure signature
                    /// </summary>
                    private static readonly string signature
                        = String.Concat(
                            $"$SchemaName.$Name; ",
                            $"args: ",
                            $"_refyearset2panel_mapping integer[], ",
                            $"_target_variable integer, ",
                            $"_threshold double precision DEFAULT 0.0000000001; ",
                            $"returns: text");

                    /// <summary>
                    /// SQL command text
                    /// </summary>
                    private static readonly string sql
                        = String.Concat(
                            $"SELECT{Environment.NewLine}",
                            $"    $SchemaName.$Name(",
                            $"@refYearSetToPanelMappingIds, ",
                            $"@targetVariableId, ",
                            $"@threshold)::text AS $Name;");

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    private static string commandText
                        = String.Empty;

                    #endregion Private Fields


                    #region Properties

                    /// <summary>
                    /// Schema name
                    /// </summary>
                    public static string SchemaName
                    {
                        get
                        {
                            return schemaName;
                        }
                        set
                        {
                            schemaName = value;
                        }
                    }

                    /// <summary>
                    /// Stored procedure name (read-only)
                    /// </summary>
                    public static string Name
                    {
                        get
                        {
                            return name;
                        }
                    }

                    /// <summary>
                    /// Stored procedure signature (read-only)
                    /// </summary>
                    public static string Signature
                    {
                        get
                        {
                            return
                                signature
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// SQL command text (read-only)
                    /// </summary>
                    public static string SQL
                    {
                        get
                        {
                            return
                                sql
                                    .Replace("$SchemaName", schemaName)
                                    .Replace("$Name", name);
                        }
                    }

                    /// <summary>
                    /// Last command text
                    /// </summary>
                    public static string CommandText
                    {
                        get
                        {
                            return commandText;
                        }
                        set
                        {
                            commandText = value;
                        }
                    }

                    #endregion Properties


                    #region Methods

                    /// <summary>
                    /// Prepare command text
                    /// </summary>
                    /// <param name="refYearSetToPanelMappingIds">List of identifiers from mapping table sdesign.cm_refyearset2panel_mapping</param>
                    /// <param name="targetVariableId">Target variable identifier</param>
                    /// <param name="threshold">Threshold</param>
                    /// <returns>Command text</returns>
                    public static string GetCommandText(
                        List<Nullable<int>> refYearSetToPanelMappingIds,
                        Nullable<int> targetVariableId,
                        Nullable<double> threshold)
                    {
                        string result = SQL;

                        result = result.Replace(
                            oldValue: "@refYearSetToPanelMappingIds",
                            newValue: Functions.PrepNIntArrayArg(args: refYearSetToPanelMappingIds, dbType: "int4"));

                        result = result.Replace(
                            oldValue: "@targetVariableId",
                            newValue: Functions.PrepNIntArg(arg: targetVariableId));

                        result = result.Replace(
                            oldValue: "@threshold",
                            newValue: Functions.PrepNDoubleArg(arg: threshold));

                        return result;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_save_ldsity_values
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="refYearSetToPanelMappingIds">List of identifiers from mapping table sdesign.cm_refyearset2panel_mapping</param>
                    /// <param name="targetVariableId">Target variable identifier</param>
                    /// <param name="threshold">Threshold</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns> Data table</returns>
                    public static DataTable ExecuteQuery(
                        NfiEstaDB database,
                        List<Nullable<int>> refYearSetToPanelMappingIds,
                        Nullable<int> targetVariableId,
                        Nullable<double> threshold,
                        NpgsqlTransaction transaction = null)
                    {
                        return
                            ExecuteQuery(
                                connection: database.Postgres,
                                refYearSetToPanelMappingIds: refYearSetToPanelMappingIds,
                                targetVariableId: targetVariableId,
                                threshold: threshold,
                                transaction: transaction
                            );
                    }

                    /// <summary>
                    /// Execute stored procedure fn_save_ldsity_values
                    /// </summary>
                    /// <param name="connection">Database connection</param>
                    /// <param name="refYearSetToPanelMappingIds">List of identifiers from mapping table sdesign.cm_refyearset2panel_mapping</param>
                    /// <param name="targetVariableId">Target variable identifier</param>
                    /// <param name="threshold">Threshold</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns> Data table</returns>
                    public static DataTable ExecuteQuery(
                    ZaJi.PostgreSQL.PostgreSQLWrapper connection,
                    List<Nullable<int>> refYearSetToPanelMappingIds,
                    Nullable<int> targetVariableId,
                    Nullable<double> threshold,
                    NpgsqlTransaction transaction = null)
                    {
                        string val = Execute(
                            connection: connection,
                            refYearSetToPanelMappingIds: refYearSetToPanelMappingIds,
                            targetVariableId: targetVariableId,
                            threshold: threshold,
                            transaction: transaction);

                        DataTable dt = new();

                        dt.Columns.Add(column: new DataColumn()
                        {
                            ColumnName = Name,
                            DataType = Type.GetType(typeName: "System.String")
                        });

                        DataRow row = dt.NewRow();

                        Functions.SetStringArg(
                            row: row,
                            name: Name,
                            val: val);

                        dt.Rows.Add(row: row);

                        return dt;
                    }

                    /// <summary>
                    /// Execute stored procedure fn_save_ldsity_values
                    /// </summary>
                    /// <param name="database">Database tables</param>
                    /// <param name="refYearSetToPanelMappingIds">List of identifiers from mapping table sdesign.cm_refyearset2panel_mapping</param>
                    /// <param name="targetVariableId">Target variable identifier</param>
                    /// <param name="threshold">Threshold</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns> Message with result status</returns>
                    public static string Execute(
                        NfiEstaDB database,
                        List<Nullable<int>> refYearSetToPanelMappingIds,
                        Nullable<int> targetVariableId,
                        Nullable<double> threshold,
                        NpgsqlTransaction transaction = null)
                    {
                        return
                            Execute(
                                connection: database.Postgres,
                                refYearSetToPanelMappingIds: refYearSetToPanelMappingIds,
                                targetVariableId: targetVariableId,
                                threshold: threshold,
                                transaction: transaction);
                    }

                    /// <summary>
                    /// Execute stored procedure fn_save_ldsity_values
                    /// </summary>
                    /// <param name="connection">Database connection</param>
                    /// <param name="refYearSetToPanelMappingIds">List of identifiers from mapping table sdesign.cm_refyearset2panel_mapping</param>
                    /// <param name="targetVariableId">Target variable identifier</param>
                    /// <param name="threshold">Threshold</param>
                    /// <param name="transaction">Transaction</param>
                    /// <returns> Message with result status</returns>
                    public static string Execute(
                        ZaJi.PostgreSQL.PostgreSQLWrapper connection,
                        List<Nullable<int>> refYearSetToPanelMappingIds,
                        Nullable<int> targetVariableId,
                        Nullable<double> threshold,
                        NpgsqlTransaction transaction = null)
                    {
                        CommandText = GetCommandText(
                            refYearSetToPanelMappingIds: refYearSetToPanelMappingIds,
                            targetVariableId: targetVariableId,
                            threshold: threshold);

                        NpgsqlParameter pRefYearSetToPanelMappingIds = new(
                            parameterName: "refYearSetToPanelMappingIds",
                            parameterType: NpgsqlDbType.Array | NpgsqlDbType.Integer);

                        NpgsqlParameter pTargetVariableId = new(
                            parameterName: "targetVariableId",
                            parameterType: NpgsqlDbType.Integer);

                        NpgsqlParameter pThreshold = new(
                            parameterName: "threshold",
                            parameterType: NpgsqlDbType.Double);

                        if (refYearSetToPanelMappingIds != null)
                        {
                            pRefYearSetToPanelMappingIds.Value = refYearSetToPanelMappingIds.ToArray<Nullable<int>>();
                        }
                        else
                        {
                            pRefYearSetToPanelMappingIds.Value = DBNull.Value;
                        }

                        if (targetVariableId != null)
                        {
                            pTargetVariableId.Value = (int)targetVariableId;
                        }
                        else
                        {
                            pTargetVariableId.Value = DBNull.Value;
                        }

                        if (threshold != null)
                        {
                            pThreshold.Value = (double)threshold;
                        }
                        else
                        {
                            pThreshold.Value = DBNull.Value;
                        }

                        return connection.ExecuteScalar(
                            sqlCommand: SQL,
                            transaction: transaction,
                            pRefYearSetToPanelMappingIds, pTargetVariableId, pThreshold);
                    }

                    #endregion Methods

                }

                #endregion FnSaveLDsityValues

            }

        }
    }
}