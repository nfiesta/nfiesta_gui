﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {
            // t_ldsity_values

            /// <summary>
            /// Local density value
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class TDLDsityValue(
                TDLDsityValueList composite = null,
                DataRow data = null)
                    : ADataTableEntry<TDLDsityValueList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Local density value identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TDLDsityValueList.ColId.Name,
                            defaultValue: Int32.Parse(s: TDLDsityValueList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TDLDsityValueList.ColId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Inventory plot identifier
                /// </summary>
                public int PlotId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TDLDsityValueList.ColPlotId.Name,
                            defaultValue: Int32.Parse(s: TDLDsityValueList.ColPlotId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TDLDsityValueList.ColPlotId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Available data sets identifier
                /// </summary>
                public int AvailableDataSetsId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TDLDsityValueList.ColAvailableDataSetsId.Name,
                            defaultValue: Int32.Parse(s: TDLDsityValueList.ColAvailableDataSetsId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TDLDsityValueList.ColAvailableDataSetsId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Available data sets object (read-only)
                /// </summary>
                public TDAvailableDataSet AvailableDataSets
                {
                    get
                    {
                        return ((NfiEstaDB)Composite.Database).STargetData.TAvailableDataSet[AvailableDataSetsId];
                    }
                }


                /// <summary>
                /// Local density value
                /// </summary>
                public Nullable<double> Value
                {
                    get
                    {
                        return Functions.GetNDoubleArg(
                            row: Data,
                            name: TDLDsityValueList.ColValue.Name,
                            defaultValue: String.IsNullOrEmpty(value: TDLDsityValueList.ColValue.DefaultValue) ?
                                (Nullable<double>)null :
                                Double.Parse(s: TDLDsityValueList.ColValue.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNDoubleArg(
                            row: Data,
                            name: TDLDsityValueList.ColValue.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Creation time
                /// </summary>
                public DateTime CreationTime
                {
                    get
                    {
                        return Functions.GetDateTimeArg(
                            row: Data,
                            name: TDLDsityValueList.ColCreationTime.Name,
                            defaultValue: DateTime.Parse(s: TDLDsityValueList.ColCreationTime.DefaultValue));
                    }
                    set
                    {
                        Functions.SetDateTimeArg(
                            row: Data,
                            name: TDLDsityValueList.ColCreationTime.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Creation time as text
                /// </summary>
                public string CreationTimeText
                {
                    get
                    {
                        return
                            CreationTime.ToString(
                                format: TDLDsityValueList.ColCreationTime.NumericFormat);
                    }
                }


                /// <summary>
                /// Flag for identification actual value of local density
                /// </summary>
                public bool IsLatest
                {
                    get
                    {
                        return Functions.GetBoolArg(
                            row: Data,
                            name: TDLDsityValueList.ColIsLatest.Name,
                            Boolean.Parse(value: TDLDsityValueList.ColIsLatest.DefaultValue));
                    }
                    set
                    {
                        Functions.SetBoolArg(
                            row: Data,
                            name: TDLDsityValueList.ColIsLatest.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Database extension version identifier
                /// </summary>
                public string ExtVersion
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDLDsityValueList.ColExtVersion.Name,
                            defaultValue: TDLDsityValueList.ColExtVersion.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDLDsityValueList.ColExtVersion.Name,
                            val: value);
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not TDLDsityValue Type, return False
                    if (obj is not TDLDsityValue)
                    {
                        return false;
                    }

                    return
                        Id == ((TDLDsityValue)obj).Id;
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                #endregion Methods

            }


            /// <summary>
            /// List of local density values
            /// </summary>
            public class TDLDsityValueList
                : ADataTable
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema = TDSchema.Name;

                /// <summary>
                /// Table name
                /// </summary>
                public const string Name = "t_ldsity_values";

                /// <summary>
                /// Table alias
                /// </summary>
                public const string Alias = "t_ldsity_values";

                /// <summary>
                /// Description in national language
                /// </summary>
                public const string CaptionCs = "Seznam hodnot lokálních hustot ";

                /// <summary>
                /// Description in English
                /// </summary>
                public const string CaptionEn = "List of local density values ";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
            {
                { "id", new ColumnMetadata()
                {
                    Name = "id",
                    DbName = "id",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = true,
                    NotNull = true,
                    DefaultValue = "0",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "ID",
                    HeaderTextEn = "ID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 0
                }
                },
                { "plot_id", new ColumnMetadata()
                {
                    Name = "plot_id",
                    DbName = "plot",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = "0",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "PLOT_ID",
                    HeaderTextEn = "PLOT_ID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 1
                }
                },
                { "available_datasets_id", new ColumnMetadata()
                {
                    Name = "available_datasets_id",
                    DbName = "available_datasets",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = "0",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "AVAILABLE_DATASETS_ID",
                    HeaderTextEn = "AVAILABLE_DATASETS_ID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 2
                }
                },
                { "value", new ColumnMetadata()
                {
                    Name = "value",
                    DbName = "value",
                    DataType = "System.Double",
                    DbDataType = "float8",
                    NewDataType = "float8",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = "0.0",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "N3",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "VALUE",
                    HeaderTextEn = "VALUE",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 3
                }
                },
                { "creation_time", new ColumnMetadata()
                {
                    Name = "creation_time",
                    DbName = "creation_time",
                    DataType = "System.DateTime",
                    DbDataType = "timestamptz",
                    NewDataType = "timestamptz",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = new DateTime().ToString(),
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "yyyy-MM-dd HH:mm:ss",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "CREATION_TIME",
                    HeaderTextEn = "CREATION_TIME",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 4
                }
                },
                { "is_latest", new ColumnMetadata()
                {
                    Name = "is_latest",
                    DbName = "is_latest",
                    DataType = "System.Boolean",
                    DbDataType = "bool",
                    NewDataType = "bool",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = "false",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "IS_LATEST",
                    HeaderTextEn = "IS_LATEST",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 5
                }
                },
                { "ext_version", new ColumnMetadata()
                {
                    Name = "ext_version",
                    DbName = "ext_version",
                    DataType = "System.String",
                    DbDataType = "varchar",
                    NewDataType = "varchar",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = String.Empty,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "EXTENSION_VERSION",
                    HeaderTextEn = "EXTENSION_VERSION",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 6
                }
                }
            };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column plot_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColPlotId = Cols["plot_id"];

                /// <summary>
                /// Column available_datasets_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColAvailableDataSetsId = Cols["available_datasets_id"];

                /// <summary>
                /// Column value metadata
                /// </summary>
                public static readonly ColumnMetadata ColValue = Cols["value"];

                /// <summary>
                /// Column creation_time metadata
                /// </summary>
                public static readonly ColumnMetadata ColCreationTime = Cols["creation_time"];

                /// <summary>
                /// Column is_latest metadata
                /// </summary>
                public static readonly ColumnMetadata ColIsLatest = Cols["is_latest"];

                /// <summary>
                /// Column ext_version metadata
                /// </summary>
                public static readonly ColumnMetadata ColExtVersion = Cols["ext_version"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TDLDsityValueList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TDLDsityValueList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// List of local density values (read-only)
                /// </summary>
                public List<TDLDsityValue> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new TDLDsityValue(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// Local density value from list by identifier (read-only)
                /// </summary>
                /// <param name="id"> Local density identifier</param>
                /// <returns> Local density from list by identifier (null if not found)</returns>
                public TDLDsityValue this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new TDLDsityValue(composite: this, data: a))
                                .FirstOrDefault<TDLDsityValue>();
                    }
                }

                #endregion Indexer


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public TDLDsityValueList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new TDLDsityValueList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new TDLDsityValueList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                #endregion Methods

            }

        }
    }
}
