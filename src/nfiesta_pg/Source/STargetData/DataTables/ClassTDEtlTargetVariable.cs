﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {
            // t_etl_target_variable

            /// <summary>
            /// Record from table of target variables for ETL
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class TDEtlTargetVariable(
                TDEtlTargetVariableList composite = null,
                DataRow data = null)
                    : ADataTableEntry<TDEtlTargetVariableList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Identifier of target variable for ETL, primary key.
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TDEtlTargetVariableList.ColId.Name,
                            defaultValue: Int32.Parse(s: TDEtlTargetVariableList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TDEtlTargetVariableList.ColId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Identifier of export connection, foreign key to c_export_connection.
                /// </summary>
                public int ExportConnectionId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TDEtlTargetVariableList.ColExportConnectionId.Name,
                            defaultValue: Int32.Parse(s: TDEtlTargetVariableList.ColExportConnectionId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TDEtlTargetVariableList.ColExportConnectionId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Export connection object (read-only)
                /// </summary>
                public TDExportConnection ExportConnection
                {
                    get
                    {
                        return ((NfiEstaDB)Composite.Database).STargetData.CExportConnection[ExportConnectionId];
                    }
                }


                /// <summary>
                /// Identifier of target variable, foreign key to c_target_variable.
                /// </summary>
                public int ColTargetVariableId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TDEtlTargetVariableList.ColTargetVariableId.Name,
                            defaultValue: Int32.Parse(s: TDEtlTargetVariableList.ColTargetVariableId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TDEtlTargetVariableList.ColTargetVariableId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Identifier of target variable, foreign key to nfiesta.c_target_variable.
                /// </summary>
                public int EtlId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TDEtlTargetVariableList.ColEtlId.Name,
                            defaultValue: Int32.Parse(s: TDEtlTargetVariableList.ColEtlId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TDEtlTargetVariableList.ColEtlId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Target variable object (read-only)
                /// </summary>
                public Core.TargetVariable TargetVariable
                {
                    get
                    {
                        return (Core.TargetVariable)((NfiEstaDB)Composite.Database).SNfiEsta.CTargetVariable[EtlId];
                    }
                }


                /// <summary>
                /// Identifier of ETL current user for given target variable
                /// </summary>
                public string EtlUser
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDEtlTargetVariableList.ColEtlUser.Name,
                            defaultValue: TDEtlTargetVariableList.ColEtlUser.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                             row: Data,
                             name: TDEtlTargetVariableList.ColEtlUser.Name,
                             val: value);
                    }
                }


                /// <summary>
                /// Identifier of ETL time of given target variable
                /// </summary>
                public Nullable<DateTime> EtlTime
                {
                    get
                    {
                        return Functions.GetNDateTimeArg(
                            row: Data,
                            name: TDEtlTargetVariableList.ColEtlTime.Name,
                            defaultValue: String.IsNullOrEmpty(value: TDEtlTargetVariableList.ColEtlTime.DefaultValue) ?
                                            (Nullable<DateTime>)null :
                                            DateTime.Parse(s: TDEtlTargetVariableList.ColEtlTime.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNDateTimeArg(
                            row: Data,
                            name: TDEtlTargetVariableList.ColEtlTime.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Identifier of ETL time of given target variable as text
                /// </summary>
                public string EtlTimeText
                {
                    get
                    {
                        return
                            (EtlTime == null) ? Functions.StrNull :
                                ((DateTime)EtlTime).ToString(
                                format: TDEtlTargetVariableList.ColEtlTime.NumericFormat);
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not TDEtlTargetVariable Type, return False
                    if (obj is not TDEtlTargetVariable)
                    {
                        return false;
                    }

                    return
                        Id == ((TDEtlTargetVariable)obj).Id;
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                #endregion Methods

            }


            /// <summary>
            /// List of target variables for ETL
            /// </summary>
            public class TDEtlTargetVariableList
                : ADataTable
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema = TDSchema.Name;

                /// <summary>
                /// Table name
                /// </summary>
                public const string Name = "t_etl_target_variable";

                /// <summary>
                /// Table alias
                /// </summary>
                public const string Alias = "t_etl_target_variable";

                /// <summary>
                /// Description in national language
                /// </summary>
                public const string CaptionCs = "Seznam cílových proměnných pro ETL";

                /// <summary>
                /// Description in English
                /// </summary>
                public const string CaptionEn = "List of target variables for ETL";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
                {
                { "id", new ColumnMetadata()
                {
                    Name = "id",
                    DbName = "id",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = true,
                    NotNull = true,
                    DefaultValue = "0",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "ID",
                    HeaderTextEn = "ID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 0
                }
                },
                { "export_connection_id", new ColumnMetadata()
                {
                    Name = "export_connection_id",
                    DbName = "export_connection",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = "0",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "EXPORT_CONNECTION_ID",
                    HeaderTextEn = "EXPORT_CONNECTION_ID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 1
                }
                },
                { "target_variable_id", new ColumnMetadata()
                {
                    Name = "target_variable_id",
                    DbName = "target_variable",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = "0",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "TARGET_VARIABLE_ID",
                    HeaderTextEn = "TARGET_VARIABLE_ID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 2
                }
                },
                { "etl_id", new ColumnMetadata()
                {
                    Name = "etl_id",
                    DbName = "etl_id",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = "0",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "ETL_ID",
                    HeaderTextEn = "ETL_ID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 3
                }
                },
                { "etl_user", new ColumnMetadata()
                {
                    Name = "etl_user",
                    DbName = "etl_user",
                    DataType = "System.String",
                    DbDataType = "name",
                    NewDataType = "text",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = null,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "ETL_USER",
                    HeaderTextEn = "ETL_USER",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 4
                }
                },
                { "etl_time", new ColumnMetadata()
                {
                    Name = "etl_time",
                    DbName = "etl_time",
                    DataType = "System.DateTime",
                    DbDataType = "timestamptz",
                    NewDataType = "timestamptz",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = null,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "yyyy-MM-dd HH:mm:ss",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "ETL_TIME",
                    HeaderTextEn = "ETL_TIME",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 5
                }
                }
                };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column export_connection_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColExportConnectionId = Cols["export_connection_id"];

                /// <summary>
                /// Column target_variable_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColTargetVariableId = Cols["target_variable_id"];

                /// <summary>
                /// Column etl_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColEtlId = Cols["etl_id"];

                /// <summary>
                /// Column etl_user metadata
                /// </summary>
                public static readonly ColumnMetadata ColEtlUser = Cols["etl_user"];

                /// <summary>
                /// Column etl_time metadata
                /// </summary>
                public static readonly ColumnMetadata ColEtlTime = Cols["etl_time"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TDEtlTargetVariableList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TDEtlTargetVariableList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// List of target variables for ETL (read-only)
                /// </summary>
                public List<TDEtlTargetVariable> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new TDEtlTargetVariable(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// Target variable for ETL from list by identifier (read-only)
                /// </summary>
                /// <param name="id">Targe variable for ETL identifier</param>
                /// <returns>Target variable for ETL from list by identifier (null if not found)</returns>
                public TDEtlTargetVariable this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new TDEtlTargetVariable(composite: this, data: a))
                                .FirstOrDefault<TDEtlTargetVariable>();
                    }
                }

                #endregion Indexer


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public TDEtlSubPopulationList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new TDEtlSubPopulationList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new TDEtlSubPopulationList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                #endregion Methods

            }

        }
    }
}