﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {
            // t_available_datasets

            /// <summary>
            /// Available data set
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class TDAvailableDataSet(
                TDAvailableDataSetList composite = null,
                DataRow data = null)
                    : ADataTableEntry<TDAvailableDataSetList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Available data set identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TDAvailableDataSetList.ColId.Name,
                            defaultValue: Int32.Parse(s: TDAvailableDataSetList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TDAvailableDataSetList.ColId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Sampling panel identifier
                /// </summary>
                public int PanelId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TDAvailableDataSetList.ColPanelId.Name,
                            defaultValue: Int32.Parse(s: TDAvailableDataSetList.ColPanelId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TDAvailableDataSetList.ColPanelId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Reference year or season set identifier
                /// </summary>
                public int ReferenceYearSetId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TDAvailableDataSetList.ColReferenceYearSetId.Name,
                            defaultValue: Int32.Parse(s: TDAvailableDataSetList.ColReferenceYearSetId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TDAvailableDataSetList.ColReferenceYearSetId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Categorization setup identifier
                /// </summary>
                public Nullable<int> CategorizationSetupId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TDAvailableDataSetList.ColCategorizationSetupId.Name,
                            defaultValue: String.IsNullOrEmpty(value: TDAvailableDataSetList.ColCategorizationSetupId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TDAvailableDataSetList.ColCategorizationSetupId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TDAvailableDataSetList.ColCategorizationSetupId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Saved threshold for given combination panel, reference_year_set and categorization_setup.
                /// </summary>
                public double LDsityThreshold
                {
                    get
                    {
                        return Functions.GetDoubleArg(
                            row: Data,
                            name: TDAvailableDataSetList.ColLDsityThreshold.Name,
                            defaultValue: Double.Parse(s: TDAvailableDataSetList.ColLDsityThreshold.DefaultValue));
                    }
                    set
                    {
                        Functions.SetDoubleArg(
                            row: Data,
                            name: TDAvailableDataSetList.ColLDsityThreshold.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Value of last change for given panel, reference_year_set and categorization_setup.
                /// This value is change after insert, after update or after check additivity of local densities.
                /// </summary>
                public DateTime LastChange
                {
                    get
                    {
                        return Functions.GetDateTimeArg(
                            row: Data,
                            name: TDAvailableDataSetList.ColLastChange.Name,
                            defaultValue: DateTime.Parse(s: TDAvailableDataSetList.ColLastChange.DefaultValue));
                    }
                    set
                    {
                        Functions.SetDateTimeArg(
                            row: Data,
                            name: TDAvailableDataSetList.ColLastChange.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Value of last change for given panel, reference_year_set and categorization_setup.
                /// This value is change after insert, after update or after check additivity of local densities.
                /// </summary>
                public string LastChangeText
                {
                    get
                    {
                        return
                            LastChange.ToString(
                                format: TDAvailableDataSetList.ColLastChange.NumericFormat);
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not TDAvailableDataSet Type, return False
                    if (obj is not TDAvailableDataSet)
                    {
                        return false;
                    }

                    return
                        Id == ((TDAvailableDataSet)obj).Id;
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                #endregion Methods

            }


            /// <summary>
            /// List of available data sets
            /// </summary>
            public class TDAvailableDataSetList
                : ADataTable
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema = TDSchema.Name;

                /// <summary>
                /// Table name
                /// </summary>
                public const string Name = "t_available_datasets";

                /// <summary>
                /// Table alias
                /// </summary>
                public const string Alias = "t_available_datasets";

                /// <summary>
                /// Description in national language
                /// </summary>
                public const string CaptionCs = "Seznam dostupných datových sad";

                /// <summary>
                /// Description in English
                /// </summary>
                public const string CaptionEn = "List of available data sets";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
            {
                { "id", new ColumnMetadata()
                {
                    Name = "id",
                    DbName = "id",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = true,
                    NotNull = true,
                    DefaultValue = "0",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "ID",
                    HeaderTextEn = "ID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 0
                }
                },
                { "panel_id", new ColumnMetadata()
                {
                    Name = "panel_id",
                    DbName = "panel",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = "0",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "PANEL_ID",
                    HeaderTextEn = "PANEL_ID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 1
                }
                },
                { "reference_year_set_id", new ColumnMetadata()
                {
                    Name = "reference_year_set_id",
                    DbName = "reference_year_set",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = "0",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "REFERENCE_YEAR_SET_ID",
                    HeaderTextEn = "REFERENCE_YEAR_SET_ID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 2
                }
                },
                { "categorization_setup_id", new ColumnMetadata()
                {
                    Name = "categorization_setup_id",
                    DbName = "categorization_setup",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = null,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "CATEGORIZATION_SETUP_ID",
                    HeaderTextEn = "CATEGORIZATION_SETUP_ID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 3
                }
                },
                { "ldsity_threshold", new ColumnMetadata()
                {
                    Name = "ldsity_threshold",
                    DbName = "ldsity_threshold",
                    DataType = "System.Double",
                    DbDataType = "float8",
                    NewDataType = "float8",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = "0.0",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "N3",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "LDSITY_THRESHOLD",
                    HeaderTextEn = "LDSITY_THRESHOLD",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 4
                }
                },
                { "last_change", new ColumnMetadata()
                {
                    Name = "last_change",
                    DbName = "last_change",
                    DataType = "System.DateTime",
                    DbDataType = "timestamptz",
                    NewDataType = "timestamptz",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = new DateTime().ToString(),
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "yyyy-MM-dd HH:mm:ss",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "LAST_CHANGE",
                    HeaderTextEn = "LAST_CHANGE",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 5
                }
                }
            };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column panel_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColPanelId = Cols["panel_id"];

                /// <summary>
                /// Column reference_year_set_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColReferenceYearSetId = Cols["reference_year_set_id"];

                /// <summary>
                /// Column categorization_setup_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColCategorizationSetupId = Cols["categorization_setup_id"];

                /// <summary>
                /// Column ldsity_threshold metadata
                /// </summary>
                public static readonly ColumnMetadata ColLDsityThreshold = Cols["ldsity_threshold"];

                /// <summary>
                /// Column last_change metadata
                /// </summary>
                public static readonly ColumnMetadata ColLastChange = Cols["last_change"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TDAvailableDataSetList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TDAvailableDataSetList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// List of available data sets (read-only)
                /// </summary>
                public List<TDAvailableDataSet> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new TDAvailableDataSet(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// Available data set from list by identifier (read-only)
                /// </summary>
                /// <param name="id"> Available data set identifier</param>
                /// <returns> Available data set from list by identifier (null if not found)</returns>
                public TDAvailableDataSet this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new TDAvailableDataSet(composite: this, data: a))
                                .FirstOrDefault<TDAvailableDataSet>();
                    }
                }

                #endregion Indexer


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public TDAvailableDataSetList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new TDAvailableDataSetList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new TDAvailableDataSetList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                #endregion Methods

            }

        }
    }
}
