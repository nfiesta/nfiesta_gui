﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {
            // t_etl_sub_population_category

            /// <summary>
            /// Record from table of sub-population categories for ETL
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class TDEtlSubPopulationCategory(
                TDEtlSubPopulationCategoryList composite = null,
                DataRow data = null)
                    : ADataTableEntry<TDEtlSubPopulationCategoryList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Identifier of sub-population category for ETL, primary key.
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TDEtlSubPopulationCategoryList.ColId.Name,
                            defaultValue: Int32.Parse(s: TDEtlSubPopulationCategoryList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TDEtlSubPopulationCategoryList.ColId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Identifier of sub-population category, array of foreign keys to c_sub_population_category.
                /// </summary>
                public string SubPopulationCategoryIds
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDEtlSubPopulationCategoryList.ColSubPopulationCategoryIds.Name,
                            defaultValue: TDEtlSubPopulationCategoryList.ColSubPopulationCategoryIds.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDEtlSubPopulationCategoryList.ColSubPopulationCategoryIds.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// List of identifiers of sub-population category, array of foreign keys to c_sub_population_category.
                /// </summary>
                public List<int> SubPopulationCategoryIdsList
                {
                    get
                    {
                        return Functions.StringToIntList(
                            text: SubPopulationCategoryIds,
                            separator: (char)59,
                            defaultValue: null);
                    }
                }


                /// <summary>
                /// Identifier of sub-population category, foreign key to nfiesta.c_sub_population_category.
                /// </summary>
                public int EtlId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TDEtlSubPopulationCategoryList.ColEtlId.Name,
                            defaultValue: Int32.Parse(s: TDEtlSubPopulationCategoryList.ColEtlId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TDEtlSubPopulationCategoryList.ColEtlId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Sub-population category object (read-only)
                /// </summary>
                public Core.SubPopulationCategory SubPopulationCategory
                {
                    get
                    {
                        return ((NfiEstaDB)Composite.Database).SNfiEsta.CSubPopulationCategory[EtlId];
                    }
                }


                /// <summary>
                /// Identifier of sub-population, foreign key to c_sub_population.
                /// </summary>
                public int EtlSubPopulationId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TDEtlSubPopulationCategoryList.ColEtlSubPopulationId.Name,
                            defaultValue: Int32.Parse(s: TDEtlSubPopulationCategoryList.ColEtlSubPopulationId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TDEtlSubPopulationCategoryList.ColEtlSubPopulationId.Name,
                            val: value);
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not TDEtlSubPopulationCategory Type, return False
                    if (obj is not TDEtlSubPopulationCategory)
                    {
                        return false;
                    }

                    return
                        Id == ((TDEtlSubPopulationCategory)obj).Id;
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                #endregion Methods

            }


            /// <summary>
            /// List of sub-population categories for ETL
            /// </summary>
            public class TDEtlSubPopulationCategoryList
                : ADataTable
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema = TDSchema.Name;

                /// <summary>
                /// Table name
                /// </summary>
                public const string Name = "t_etl_sub_population_category";

                /// <summary>
                /// Table alias
                /// </summary>
                public const string Alias = "t_etl_sub_population_category";

                /// <summary>
                /// Description in national language
                /// </summary>
                public const string CaptionCs = "Seznam kategorií subpopulací pro ETL";

                /// <summary>
                /// Description in English
                /// </summary>
                public const string CaptionEn = "List of sub-population categories for ETL";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
                {
                { "id", new ColumnMetadata()
                {
                    Name = "id",
                    DbName = "id",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = true,
                    NotNull = true,
                    DefaultValue = "0",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "ID",
                    HeaderTextEn = "ID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 0
                }
                },
                { "sub_population_category_ids", new ColumnMetadata()
                {
                    Name = "sub_population_category_ids",
                    DbName = "sub_population_category",
                    DataType = "System.String",
                    DbDataType = "_int4",
                    NewDataType = "text",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = String.Empty,
                    FuncCall = "array_to_string({0}, ';', 'NULL')",
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "SUB_POPULATION_CATEGORY_IDS",
                    HeaderTextEn = "SUB_POPULATION_CATEGORY_IDS",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 1
                }
                },
                { "etl_id", new ColumnMetadata()
                {
                    Name = "etl_id",
                    DbName = "etl_id",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = "0",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "ETL_ID",
                    HeaderTextEn = "ETL_ID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 2
                }
                },
                { "etl_sub_population_id", new ColumnMetadata()
                {
                    Name = "etl_sub_population_id",
                    DbName = "etl_sub_population",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = "0",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "ETL_SUB_POPULATION_ID",
                    HeaderTextEn = "ETL_SUB_POPULATION_ID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 3
                }
                }
                };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column sub_population_category_ids metadata
                /// </summary>
                public static readonly ColumnMetadata ColSubPopulationCategoryIds = Cols["sub_population_category_ids"];

                /// <summary>
                /// Column etl_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColEtlId = Cols["etl_id"];

                /// <summary>
                /// Column etl_sub_population_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColEtlSubPopulationId = Cols["etl_sub_population_id"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TDEtlSubPopulationCategoryList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TDEtlSubPopulationCategoryList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// List of sub-population categories for ETL (read-only)
                /// </summary>
                public List<TDEtlSubPopulationCategory> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new TDEtlSubPopulationCategory(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// Sub-population category for ETL from list by identifier (read-only)
                /// </summary>
                /// <param name="id">Sub-population category for ETL identifier</param>
                /// <returns>Sub-population category for ETL from list by identifier (null if not found)</returns>
                public TDEtlSubPopulationCategory this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new TDEtlSubPopulationCategory(composite: this, data: a))
                                .FirstOrDefault<TDEtlSubPopulationCategory>();
                    }
                }

                #endregion Indexer


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public TDEtlSubPopulationCategoryList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new TDEtlSubPopulationCategoryList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new TDEtlSubPopulationCategoryList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                #endregion Methods

            }

        }
    }
}