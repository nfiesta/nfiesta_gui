﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {
            // cm_ldsity2target_variable

            /// <summary>
            /// Mapping between local density and target variable
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class TDLDsityToTargetVariable(
                TDLDsityToTargetVariableList composite = null,
                DataRow data = null)
                    : AMappingTableEntry<TDLDsityToTargetVariableList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Mapping between local density and target variable identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TDLDsityToTargetVariableList.ColId.Name,
                            defaultValue: Int32.Parse(s: TDLDsityToTargetVariableList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TDLDsityToTargetVariableList.ColId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Target variable identifier
                /// </summary>
                public int TargetVariableId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TDLDsityToTargetVariableList.ColTargetVariableId.Name,
                            defaultValue: Int32.Parse(s: TDLDsityToTargetVariableList.ColTargetVariableId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TDLDsityToTargetVariableList.ColTargetVariableId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Target variable object (read-only)
                /// </summary>
                public TDTargetVariable TargetVariable
                {
                    get
                    {
                        return ((NfiEstaDB)Composite.Database).STargetData.CTargetVariable[TargetVariableId];
                    }
                }


                /// <summary>
                /// Local density identifier
                /// </summary>
                public int LDsityId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TDLDsityToTargetVariableList.ColLDsityId.Name,
                            defaultValue: Int32.Parse(s: TDLDsityToTargetVariableList.ColLDsityId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TDLDsityToTargetVariableList.ColLDsityId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Local density object (read-only)
                /// </summary>
                public TDLDsity LDsity
                {
                    get
                    {
                        return ((NfiEstaDB)Composite.Database).STargetData.CLDsity[LDsityId];
                    }
                }


                /// <summary>
                /// Local density object type identifier
                /// </summary>
                public int LDsityObjectTypeId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TDLDsityToTargetVariableList.ColLDsityObjectTypeId.Name,
                            defaultValue: Int32.Parse(s: TDLDsityToTargetVariableList.ColLDsityObjectTypeId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TDLDsityToTargetVariableList.ColLDsityObjectTypeId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Local density object type (read-only)
                /// </summary>
                public TDLDsityObjectType LDsityObjectType
                {
                    get
                    {
                        return ((NfiEstaDB)Composite.Database).STargetData.CLDsityObjectType[LDsityObjectTypeId];
                    }
                }


                /// <summary>
                /// List of area domain category identifiers as text
                /// </summary>
                public string AreaDomainCategory
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDLDsityToTargetVariableList.ColAreaDomainCategory.Name,
                            defaultValue: TDLDsityToTargetVariableList.ColAreaDomainCategory.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDLDsityToTargetVariableList.ColAreaDomainCategory.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// List of area domain category identifiers
                /// </summary>
                public List<int> AreaDomainCategoryList
                {
                    get
                    {
                        return Functions.StringToIntList(
                            text: AreaDomainCategory,
                            separator: (char)59,
                            defaultValue: null);
                    }
                }


                /// <summary>
                /// List of subpopulation category identifiers as text
                /// </summary>
                public string SubPopulationCategory
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDLDsityToTargetVariableList.ColSubPopulationCategory.Name,
                            defaultValue: TDLDsityToTargetVariableList.ColSubPopulationCategory.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDLDsityToTargetVariableList.ColSubPopulationCategory.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// List of subpopulation category identifiers
                /// </summary>
                public List<int> SubPopulationCategoryList
                {
                    get
                    {
                        return Functions.StringToIntList(
                            text: SubPopulationCategory,
                            separator: (char)59,
                            defaultValue: null);
                    }
                }


                /// <summary>
                /// Is local density value negative?
                /// </summary>
                public bool UseNegative
                {
                    get
                    {
                        return Functions.GetBoolArg(
                            row: Data,
                            name: TDLDsityToTargetVariableList.ColUseNegative.Name,
                            defaultValue: Boolean.Parse(value: TDLDsityToTargetVariableList.ColUseNegative.DefaultValue));
                    }
                    set
                    {
                        Functions.SetBoolArg(
                            row: Data,
                            name: TDLDsityToTargetVariableList.ColUseNegative.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Version identifier
                /// </summary>
                public int VersionId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TDLDsityToTargetVariableList.ColVersionId.Name,
                            defaultValue: Int32.Parse(s: TDLDsityToTargetVariableList.ColVersionId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TDLDsityToTargetVariableList.ColVersionId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Version (read-only)
                /// </summary>
                public TDVersion Version
                {
                    get
                    {
                        return
                            ((NfiEstaDB)Composite.Database).STargetData.CVersion[VersionId];
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not TDLDsityToTargetVariable Type, return False
                    if (obj is not TDLDsityToTargetVariable)
                    {
                        return false;
                    }

                    return Id == ((TDLDsityToTargetVariable)obj).Id;
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                #endregion Methods

            }


            /// <summary>
            /// List of mappings between local density and target variable
            /// </summary>
            public class TDLDsityToTargetVariableList
                : AMappingTable
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema = TDSchema.Name;

                /// <summary>
                /// Table name
                /// </summary>
                public const string Name = "cm_ldsity2target_variable";

                /// <summary>
                /// Table alias
                /// </summary>
                public const string Alias = "cm_ldsity2target_variable";

                /// <summary>
                /// Description in national language
                /// </summary>
                public const string CaptionCs = "Seznam mapování mezi lokální hustotou a cílovou proměnnou";

                /// <summary>
                /// Description in English
                /// </summary>
                public const string CaptionEn = "List of mappings between local density and target variable";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
            {
                { "id", new ColumnMetadata()
                {
                    Name = "id",
                    DbName = "id",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = true,
                    NotNull = true,
                    DefaultValue = "0",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "ID",
                    HeaderTextEn = "ID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 0
                }
                },
                { "target_variable_id", new ColumnMetadata()
                {
                    Name = "target_variable_id",
                    DbName = "target_variable",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = "0",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "TARGET_VARIABLE_ID",
                    HeaderTextEn = "TARGET_VARIABLE_ID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 1
                }
                },
                { "ldsity_id", new ColumnMetadata()
                {
                    Name = "ldsity_id",
                    DbName = "ldsity",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = "0",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "LDSITY_ID",
                    HeaderTextEn = "LDSITY_ID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 2
                }
                },
                { "ldsity_object_type_id", new ColumnMetadata()
                {
                    Name = "ldsity_object_type_id",
                    DbName = "ldsity_object_type",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = "0",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "LDSITY_OBJECT_TYPE_ID",
                    HeaderTextEn = "LDSITY_OBJECT_TYPE_ID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 3
                }
                },
                { "area_domain_category", new ColumnMetadata()
                {
                    Name = "area_domain_category",
                    DbName = "area_domain_category",
                    DataType = "System.String",
                    DbDataType = "_int4",
                    NewDataType = "text",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = null,
                    FuncCall = "array_to_string({0}, ';', 'NULL')",
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "AREA_DOMAIN_CATEGORY",
                    HeaderTextEn = "AREA_DOMAIN_CATEGORY",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 4
                }
                },
                { "sub_population_category", new ColumnMetadata()
                {
                    Name = "sub_population_category",
                    DbName = "sub_population_category",
                    DataType = "System.String",
                    DbDataType = "_int4",
                    NewDataType = "text",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = null,
                    FuncCall = "array_to_string({0}, ';', 'NULL')",
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "SUB_POPULATION_CATEGORY",
                    HeaderTextEn = "SUB_POPULATION_CATEGORY",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 5
                }
                },
                { "use_negative", new ColumnMetadata()
                {
                    Name = "use_negative",
                    DbName = "use_negative",
                    DataType = "System.Boolean",
                    DbDataType = "bool",
                    NewDataType = "bool",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = "false",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "USE_NEGATIVE",
                    HeaderTextEn = "USE_NEGATIVE",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 6
                }
                },
                { "version_id", new ColumnMetadata()
                {
                    Name = "version_id",
                    DbName = "version",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = default(int).ToString(),
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "VERSION_ID",
                    HeaderTextEn = "VERSION_ID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 7
                }
                }
            };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column target_variable_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColTargetVariableId = Cols["target_variable_id"];

                /// <summary>
                /// Column ldsity_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColLDsityId = Cols["ldsity_id"];

                /// <summary>
                /// Column ldsity_object_type_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColLDsityObjectTypeId = Cols["ldsity_object_type_id"];

                /// <summary>
                /// Column area_domain_category metadata
                /// </summary>
                public static readonly ColumnMetadata ColAreaDomainCategory = Cols["area_domain_category"];

                /// <summary>
                /// Column sub_population_category metadata
                /// </summary>
                public static readonly ColumnMetadata ColSubPopulationCategory = Cols["sub_population_category"];

                /// <summary>
                /// Column use_negative metadata
                /// </summary>
                public static readonly ColumnMetadata ColUseNegative = Cols["use_negative"];

                /// <summary>
                /// Column version_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColVersionId = Cols["version_id"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TDLDsityToTargetVariableList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TDLDsityToTargetVariableList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// List of mappings between local density and target variable (read-only)
                /// </summary>
                public List<TDLDsityToTargetVariable> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new TDLDsityToTargetVariable(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// Mapping between local density and target variable from list by identifier (read-only)
                /// </summary>
                /// <param name="id">Mapping between local density and target variable identifier</param>
                /// <returns>Mapping between local density and target variable from list by identifier (null if not found)</returns>
                public TDLDsityToTargetVariable this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new TDLDsityToTargetVariable(composite: this, data: a))
                                .FirstOrDefault<TDLDsityToTargetVariable>();
                    }
                }

                #endregion Indexer


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public TDLDsityToTargetVariableList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new TDLDsityToTargetVariableList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new TDLDsityToTargetVariableList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                #endregion Methods

            }

        }
    }
}

