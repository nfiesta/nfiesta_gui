﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {
            // cm_spc2classrule2panel_refyearset

            /// <summary>
            /// Mapping between classification rules and panels/reference year sets
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class TDSPCToClassRuleToPanelRefYearSet(
                TDSPCToClassRuleToPanelRefYearSetList composite = null,
                DataRow data = null)
                    : AMappingTableEntry<TDSPCToClassRuleToPanelRefYearSetList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Mapping between classification rules and panels/reference year sets identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TDSPCToClassRuleToPanelRefYearSetList.ColId.Name,
                            defaultValue: Int32.Parse(s: TDSPCToClassRuleToPanelRefYearSetList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TDSPCToClassRuleToPanelRefYearSetList.ColId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Classification rule for subpopulation category identifier
                /// </summary>
                public int SPCToClassificationRuleId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TDSPCToClassRuleToPanelRefYearSetList.ColSPCToClassificationRuleId.Name,
                            defaultValue: Int32.Parse(s: TDSPCToClassRuleToPanelRefYearSetList.ColSPCToClassificationRuleId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TDSPCToClassRuleToPanelRefYearSetList.ColSPCToClassificationRuleId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Classification rule for subpopulation category object (read-only)
                /// </summary>
                public TDSPCToClassificationRule SPCToClassificationRule
                {
                    get
                    {
                        return
                            ((NfiEstaDB)Composite.Database)
                            .STargetData.CmSPCToClassificationRule[SPCToClassificationRuleId];
                    }
                }


                /// <summary>
                /// Mapping between panel and reference year set identifier
                /// </summary>
                public int RefYearSetToPanelId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TDSPCToClassRuleToPanelRefYearSetList.ColRefYearSetToPanelId.Name,
                            defaultValue: Int32.Parse(s: TDSPCToClassRuleToPanelRefYearSetList.ColRefYearSetToPanelId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TDSPCToClassRuleToPanelRefYearSetList.ColRefYearSetToPanelId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Mapping between panel and reference year set object (read-only)
                /// </summary>
                public SDesign.ReferenceYearSetToPanelMapping RefYearSetToPanel
                {
                    get
                    {
                        return ((NfiEstaDB)Composite.Database).SDesign.CmReferenceYearSetToPanelMapping[RefYearSetToPanelId];
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not TDSPCToClassificationRule Type, return False
                    if (obj is not TDSPCToClassRuleToPanelRefYearSet)
                    {
                        return false;
                    }

                    return
                        Id == ((TDSPCToClassRuleToPanelRefYearSet)obj).Id;
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                #endregion Methods

            }


            /// <summary>
            /// List of mappings between classification rules and panels/reference year sets
            /// </summary>
            public class TDSPCToClassRuleToPanelRefYearSetList
                : AMappingTable
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema = TDSchema.Name;

                /// <summary>
                /// Table name
                /// </summary>
                public const string Name = "cm_spc2classrule2panel_refyearset";

                /// <summary>
                /// Table alias
                /// </summary>
                public const string Alias = "cm_spc2classrule2panel_refyearset";

                /// <summary>
                /// Description in national language
                /// </summary>
                public const string CaptionCs = "Seznam mapování mezi klasifikačními pravidly a panely a roky měření";

                /// <summary>
                /// Description in English
                /// </summary>
                public const string CaptionEn = "List of mappings between classification rules and panels/reference year sets";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
            {
                { "id", new ColumnMetadata()
                {
                    Name = "id",
                    DbName = "id",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = true,
                    NotNull = true,
                    DefaultValue = "0",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "ID",
                    HeaderTextEn = "ID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 0
                }
                },
                { "spc2classification_rule", new ColumnMetadata()
                {
                    Name = "spc2classification_rule",
                    DbName = "spc2classification_rule",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = "0",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "SPC_TO_CLASSIFICATION_RULE",
                    HeaderTextEn = "SPC_TO_CLASSIFICATION_RULE",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 1
                }
                },
                { "refyearset2panel", new ColumnMetadata()
                {
                    Name = "refyearset2panel",
                    DbName = "refyearset2panel",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = "0",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "REF_YEAR_SET_TO_PANEL",
                    HeaderTextEn = "REF_YEAR_SET_TO_PANEL",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 2
                }
                }
            };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column spc2classification_rule metadata
                /// </summary>
                public static readonly ColumnMetadata ColSPCToClassificationRuleId = Cols["spc2classification_rule"];

                /// <summary>
                /// Column refyearset2panel metadata
                /// </summary>
                public static readonly ColumnMetadata ColRefYearSetToPanelId = Cols["refyearset2panel"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TDSPCToClassRuleToPanelRefYearSetList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TDSPCToClassRuleToPanelRefYearSetList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// List of mappings between classification rules and panels/reference year sets (read-only)
                /// </summary>
                public List<TDSPCToClassRuleToPanelRefYearSet> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new TDSPCToClassRuleToPanelRefYearSet(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// Mapping between classification rules and panels/reference year sets from list by identifier (read-only)
                /// </summary>
                /// <param name="id">Mapping between classification rules and panels/reference year sets identifier</param>
                /// <returns>Mapping between classification rules and panels/reference year sets from list by identifier (null if not found)</returns>
                public TDSPCToClassRuleToPanelRefYearSet this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new TDSPCToClassRuleToPanelRefYearSet(composite: this, data: a))
                                .FirstOrDefault<TDSPCToClassRuleToPanelRefYearSet>();
                    }
                }

                #endregion Indexer


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public TDSPCToClassRuleToPanelRefYearSetList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new TDSPCToClassRuleToPanelRefYearSetList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new TDSPCToClassRuleToPanelRefYearSetList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                #endregion Methods

            }

        }
    }
}
