﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {
            // cm_ld_object2ld_object_group

            /// <summary>
            /// Mapping between local density object and group of local density objects
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class TDLDObjectToLDObjectGroup(
                TDLDObjectToLDObjectGroupList composite = null,
                DataRow data = null)
                    : AMappingTableEntry<TDLDObjectToLDObjectGroupList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Mapping between local density object and group of local density objects identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TDLDObjectToLDObjectGroupList.ColId.Name,
                            defaultValue: Int32.Parse(s: TDLDObjectToLDObjectGroupList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TDLDObjectToLDObjectGroupList.ColId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Group of local density objects identifier
                /// </summary>
                public int LDsityObjectGroupId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TDLDObjectToLDObjectGroupList.ColLDsityObjectGroupId.Name,
                            defaultValue: Int32.Parse(s: TDLDObjectToLDObjectGroupList.ColLDsityObjectGroupId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TDLDObjectToLDObjectGroupList.ColLDsityObjectGroupId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Group of local density objects (read-only)
                /// </summary>
                public TDLDsityObjectGroup LDsityObjectGroup
                {
                    get
                    {
                        return ((NfiEstaDB)Composite.Database).STargetData.CLDsityObjectGroup[LDsityObjectGroupId];
                    }
                }


                /// <summary>
                /// Local density object identifier
                /// </summary>
                public int LDsityObjectId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TDLDObjectToLDObjectGroupList.ColLDsityObjectId.Name,
                            defaultValue: Int32.Parse(s: TDLDObjectToLDObjectGroupList.ColLDsityObjectId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TDLDObjectToLDObjectGroupList.ColLDsityObjectId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Local density object (read-only)
                /// </summary>
                public TDLDsityObject LDsityObject
                {
                    get
                    {
                        return ((NfiEstaDB)Composite.Database).STargetData.CLDsityObject[LDsityObjectId];
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not TDLDObjectToLDObjectGroup Type, return False
                    if (obj is not TDLDObjectToLDObjectGroup)
                    {
                        return false;
                    }

                    return
                        Id == ((TDLDObjectToLDObjectGroup)obj).Id;
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                #endregion Methods

            }


            /// <summary>
            /// List of mappings between local density object and group of local density objects
            /// </summary>
            public class TDLDObjectToLDObjectGroupList
                : AMappingTable
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema = TDSchema.Name;

                /// <summary>
                /// Table name
                /// </summary>
                public const string Name = "cm_ld_object2ld_object_group";

                /// <summary>
                /// Table alias
                /// </summary>
                public const string Alias = "cm_ld_object2ld_object_group";

                /// <summary>
                /// Description in national language
                /// </summary>
                public const string CaptionCs = "Seznam mapování objektů lokálních hustot do skupiny objektů lokálních hustot";

                /// <summary>
                /// Description in English
                /// </summary>
                public const string CaptionEn = "List of mappings between local density object and group of local density objects";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
            {
                { "id", new ColumnMetadata()
                {
                    Name = "id",
                    DbName = "id",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = true,
                    NotNull = true,
                    DefaultValue = "0",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "ID",
                    HeaderTextEn = "ID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 0
                }
                },
                { "ldsity_object_group_id", new ColumnMetadata()
                {
                    Name = "ldsity_object_group_id",
                    DbName = "ldsity_object_group",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = "0",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "LDSITY_OBJECT_GROUP_ID",
                    HeaderTextEn = "LDSITY_OBJECT_GROUP_ID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 1
                }
                },
                { "ldsity_object_id", new ColumnMetadata()
                {
                    Name = "ldsity_object_id",
                    DbName = "ldsity_object",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = "0",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "LDSITY_OBJECT_ID",
                    HeaderTextEn = "LDSITY_OBJECT_ID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 2
                }
                }
            };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column ldsity_object_group_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColLDsityObjectGroupId = Cols["ldsity_object_group_id"];

                /// <summary>
                /// Column ldsity_object_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColLDsityObjectId = Cols["ldsity_object_id"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TDLDObjectToLDObjectGroupList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TDLDObjectToLDObjectGroupList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// List of mappings between local density object and group of local density objects (read-only)
                /// </summary>
                public List<TDLDObjectToLDObjectGroup> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new TDLDObjectToLDObjectGroup(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// Mapping between local density object and group of local density objects from list by identifier (read-only)
                /// </summary>
                /// <param name="id">Mapping between local density object and group of local density objects identifier</param>
                /// <returns>Mapping between local density object and group of local density objects from list by identifier (null if not found)</returns>
                public TDLDObjectToLDObjectGroup this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new TDLDObjectToLDObjectGroup(composite: this, data: a))
                                .FirstOrDefault<TDLDObjectToLDObjectGroup>();
                    }
                }

                #endregion Indexer


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public TDLDObjectToLDObjectGroupList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new TDLDObjectToLDObjectGroupList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new TDLDObjectToLDObjectGroupList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                #endregion Methods

            }

        }
    }
}
