﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {
            // cm_spc2classification_rule

            /// <summary>
            /// Classification rule for subpopulation category
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class TDSPCToClassificationRule(
                TDSPCToClassificationRuleList composite = null,
                DataRow data = null)
                    : AMappingTableEntry<TDSPCToClassificationRuleList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Classification rule for subpopulation category identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TDSPCToClassificationRuleList.ColId.Name,
                            defaultValue: Int32.Parse(s: TDSPCToClassificationRuleList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TDSPCToClassificationRuleList.ColId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Subpopulation category identifier
                /// </summary>
                public int SubPopulationCategoryId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TDSPCToClassificationRuleList.ColSubPopulationCategoryId.Name,
                            defaultValue: Int32.Parse(s: TDSPCToClassificationRuleList.ColSubPopulationCategoryId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TDSPCToClassificationRuleList.ColSubPopulationCategoryId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Subpopulation category object (read-only)
                /// </summary>
                public TDSubPopulationCategory SubPopulationCategory
                {
                    get
                    {
                        return ((NfiEstaDB)Composite.Database).STargetData.CSubPopulationCategory[SubPopulationCategoryId];
                    }
                }


                /// <summary>
                /// Local density object identifier
                /// </summary>
                public int LDsityObjectId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TDSPCToClassificationRuleList.ColLDsityObjectId.Name,
                            defaultValue: Int32.Parse(s: TDSPCToClassificationRuleList.ColLDsityObjectId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TDSPCToClassificationRuleList.ColLDsityObjectId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Local density object (read-only)
                /// </summary>
                public TDLDsityObject LDsityObject
                {
                    get
                    {
                        return ((NfiEstaDB)Composite.Database).STargetData.CLDsityObject[LDsityObjectId];
                    }
                }


                /// <summary>
                /// Classification rule for subpopulation category
                /// </summary>
                public string ClassificationRule
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDSPCToClassificationRuleList.ColClassificationRule.Name,
                            defaultValue: TDSPCToClassificationRuleList.ColClassificationRule.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDSPCToClassificationRuleList.ColClassificationRule.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Negative local density contribution
                /// </summary>
                public bool UseNegative
                {
                    get
                    {
                        return Functions.GetBoolArg(
                            row: Data,
                            name: TDSPCToClassificationRuleList.ColUseNegative.Name,
                            defaultValue: Boolean.Parse(value: TDSPCToClassificationRuleList.ColUseNegative.DefaultValue));
                    }
                    set
                    {
                        Functions.SetBoolArg(
                            row: Data,
                            name: TDSPCToClassificationRuleList.ColUseNegative.Name,
                            val: value);
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not TDSPCToClassificationRule Type, return False
                    if (obj is not TDSPCToClassificationRule)
                    {
                        return false;
                    }

                    return
                        Id == ((TDSPCToClassificationRule)obj).Id;
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                #endregion Methods

            }


            /// <summary>
            /// List of classification rules for subpopulation category
            /// </summary>
            public class TDSPCToClassificationRuleList
                : AMappingTable
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema = TDSchema.Name;

                /// <summary>
                /// Table name
                /// </summary>
                public const string Name = "cm_spc2classification_rule";

                /// <summary>
                /// Table alias
                /// </summary>
                public const string Alias = "cm_spc2classification_rule";

                /// <summary>
                /// Description in national language
                /// </summary>
                public const string CaptionCs = "Seznam klasifikačních pravidel pro kategorie subpopulací";

                /// <summary>
                /// Description in English
                /// </summary>
                public const string CaptionEn = "List of classification rules for subpopulation category";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
            {
                { "id", new ColumnMetadata()
                {
                    Name = "id",
                    DbName = "id",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = true,
                    NotNull = true,
                    DefaultValue = "0",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "ID",
                    HeaderTextEn = "ID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 0
                }
                },
                { "sub_population_category_id", new ColumnMetadata()
                {
                    Name = "sub_population_category_id",
                    DbName = "sub_population_category",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = "0",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "SUB_POPULATION_CATEGORY_ID",
                    HeaderTextEn = "SUB_POPULATION_CATEGORY_ID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 1
                }
                },
                { "ldsity_object_id", new ColumnMetadata()
                {
                    Name = "ldsity_object_id",
                    DbName = "ldsity_object",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = "0",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "LDSITY_OBJECT_ID",
                    HeaderTextEn = "LDSITY_OBJECT_ID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 2
                }
                },
                { "classification_rule", new ColumnMetadata()
                {
                    Name = "classification_rule",
                    DbName = "classification_rule",
                    DataType = "System.String",
                    DbDataType = "text",
                    NewDataType = "text",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = null,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "CLASSIFICATION_RULE",
                    HeaderTextEn = "CLASSIFICATION_RULE",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 3
                }
                },
                { "use_negative", new ColumnMetadata()
                {
                    Name = "use_negative",
                    DbName = "use_negative",
                    DataType = "System.Boolean",
                    DbDataType = "bool",
                    NewDataType = "bool",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = "false",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "USE_NEGATIVE",
                    HeaderTextEn = "USE_NEGATIVE",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 4
                }
                }
            };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column sub_population_category_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColSubPopulationCategoryId = Cols["sub_population_category_id"];

                /// <summary>
                /// Column ldsity_object_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColLDsityObjectId = Cols["ldsity_object_id"];

                /// <summary>
                /// Column classification_rule metadata
                /// </summary>
                public static readonly ColumnMetadata ColClassificationRule = Cols["classification_rule"];

                /// <summary>
                /// Column use_negative metadata
                /// </summary>
                public static readonly ColumnMetadata ColUseNegative = Cols["use_negative"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TDSPCToClassificationRuleList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TDSPCToClassificationRuleList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// List of classification rules for subpopulation category (read-only)
                /// </summary>
                public List<TDSPCToClassificationRule> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new TDSPCToClassificationRule(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// Classification rule for subpopulation category from list by identifier (read-only)
                /// </summary>
                /// <param name="id"> Classification rule for subpopulation category identifier</param>
                /// <returns> Classification rule for subpopulation category from list by identifier (null if not found)</returns>
                public TDSPCToClassificationRule this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new TDSPCToClassificationRule(composite: this, data: a))
                                .FirstOrDefault<TDSPCToClassificationRule>();
                    }
                }

                #endregion Indexer


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public TDSPCToClassificationRuleList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new TDSPCToClassificationRuleList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new TDSPCToClassificationRuleList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                #endregion Methods

            }

        }
    }
}
