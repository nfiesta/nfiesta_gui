﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {
            // cm_ldsity2panel_refyearset_version

            /// <summary>
            /// Mapping between local density contribution, panels and reference year sets and also versions
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class TDLDsityToPanelRefYearSetVersion(
                TDLDsityToPanelRefYearSetVersionList composite = null,
                DataRow data = null)
                    : AMappingTableEntry<TDLDsityToPanelRefYearSetVersionList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Mapping between local density contribution, panels and reference year sets and also versions identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TDLDsityToPanelRefYearSetVersionList.ColId.Name,
                            defaultValue: Int32.Parse(s: TDLDsityToPanelRefYearSetVersionList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TDLDsityToPanelRefYearSetVersionList.ColId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Local density contribution identifier
                /// </summary>
                public Nullable<int> LDsityId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TDLDsityToPanelRefYearSetVersionList.ColLDsityId.Name,
                            defaultValue: String.IsNullOrEmpty(value: TDLDsityToPanelRefYearSetVersionList.ColLDsityId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TDLDsityToPanelRefYearSetVersionList.ColLDsityId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TDLDsityToPanelRefYearSetVersionList.ColLDsityId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Local density contribution object (read-only)
                /// </summary>
                public TDLDsity LDsity
                {
                    get
                    {
                        return (LDsityId != null) ?
                            ((NfiEstaDB)Composite.Database).STargetData.CLDsity[(int)LDsityId] : null;
                    }
                }


                /// <summary>
                /// Mapping between panel and reference year set identifier
                /// </summary>
                public Nullable<int> ReferenceYearSetToPanelMappingId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TDLDsityToPanelRefYearSetVersionList.ColReferenceYearSetToPanelMappingId.Name,
                            defaultValue: String.IsNullOrEmpty(value: TDLDsityToPanelRefYearSetVersionList.ColReferenceYearSetToPanelMappingId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TDLDsityToPanelRefYearSetVersionList.ColReferenceYearSetToPanelMappingId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TDLDsityToPanelRefYearSetVersionList.ColReferenceYearSetToPanelMappingId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Mapping between panel and reference year set object (read-only)
                /// </summary>
                public SDesign.ReferenceYearSetToPanelMapping ReferenceYearSetToPanelMapping
                {
                    get
                    {
                        return (ReferenceYearSetToPanelMappingId != null) ?
                            ((NfiEstaDB)Composite.Database).SDesign.CmReferenceYearSetToPanelMapping[(int)ReferenceYearSetToPanelMappingId] : null;
                    }
                }


                /// <summary>
                /// Version identifier
                /// </summary>
                public Nullable<int> VersionId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TDLDsityToPanelRefYearSetVersionList.ColVersionId.Name,
                            defaultValue: String.IsNullOrEmpty(TDLDsityToPanelRefYearSetVersionList.ColVersionId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TDLDsityToPanelRefYearSetVersionList.ColVersionId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TDLDsityToPanelRefYearSetVersionList.ColVersionId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Version object (read-only)
                /// </summary>
                public TDVersion Version
                {
                    get
                    {
                        return (VersionId != null) ?
                            ((NfiEstaDB)Composite.Database).STargetData.CVersion[(int)VersionId] : null;
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not TDLDsityToPanelRefYearSetVersion Type, return False
                    if (obj is not TDLDsityToPanelRefYearSetVersion)
                    {
                        return false;
                    }

                    return
                        Id == ((TDLDsityToPanelRefYearSetVersion)obj).Id;
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                #endregion Methods

            }


            /// <summary>
            /// List of mappings between local density contribution, panels and reference year sets and also versions
            /// </summary>
            public class TDLDsityToPanelRefYearSetVersionList
                : AMappingTable
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema = TDSchema.Name;

                /// <summary>
                /// Table name
                /// </summary>
                public const string Name = "cm_ldsity2panel_refyearset_version";

                /// <summary>
                /// Table alias
                /// </summary>
                public const string Alias = "cm_ldsity2panel_refyearset_version";

                /// <summary>
                /// Description in national language
                /// </summary>
                public const string CaptionCs = "Seznam mapování mezi příspěvky lokálních hustot, panely a referenčními roky a také verzemi";

                /// <summary>
                /// Description in English
                /// </summary>
                public const string CaptionEn = "List of mappings between local density contribution, panels and reference year sets and also versions";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
            {
                { "id", new ColumnMetadata()
                {
                    Name = "id",
                    DbName = "id",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = true,
                    NotNull = true,
                    DefaultValue = "0",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "ID",
                    HeaderTextEn = "ID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 0
                }
                },
                { "ldsity_id", new ColumnMetadata()
                {
                    Name = "ldsity_id",
                    DbName = "ldsity",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = null,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "LDSITY_ID",
                    HeaderTextEn = "LDSITY_ID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 1
                }
                },
                { "refyearset_to_panel_id", new ColumnMetadata()
                {
                    Name = "refyearset_to_panel_id",
                    DbName = "refyearset2panel",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = null,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "REFYEARSET_TO_PANEL_ID",
                    HeaderTextEn = "REFYEARSET_TO_PANEL_ID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 2
                }
                },
                { "version_id", new ColumnMetadata()
                {
                    Name = "version_id",
                    DbName = "version",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = null,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "VERSION_ID",
                    HeaderTextEn = "VERSION_ID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = true,
                    DisplayIndex = 3
                }
                }
            };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column ldsity_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColLDsityId = Cols["ldsity_id"];

                /// <summary>
                /// Column refyearset_to_panel_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColReferenceYearSetToPanelMappingId = Cols["refyearset_to_panel_id"];

                /// <summary>
                /// Column version_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColVersionId = Cols["version_id"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TDLDsityToPanelRefYearSetVersionList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TDLDsityToPanelRefYearSetVersionList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// List of mappings between local density contribution, panels and reference year sets and also versions (read-only)
                /// </summary>
                public List<TDLDsityToPanelRefYearSetVersion> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new TDLDsityToPanelRefYearSetVersion(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// Mapping between local density contribution, panels and reference year sets and also versions from list by identifier (read-only)
                /// </summary>
                /// <param name="id">Mapping between local density contribution, panels and reference year sets and also versions identifier</param>
                /// <returns>Mapping between local density contribution, panels and reference year sets and also versions from list by identifier (null if not found)</returns>
                public TDLDsityToPanelRefYearSetVersion this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new TDLDsityToPanelRefYearSetVersion(composite: this, data: a))
                                .FirstOrDefault<TDLDsityToPanelRefYearSetVersion>();
                    }
                }

                #endregion Indexer


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public TDLDsityToPanelRefYearSetVersionList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new TDLDsityToPanelRefYearSetVersionList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new TDLDsityToPanelRefYearSetVersionList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                #endregion Methods

            }

        }
    }
}

