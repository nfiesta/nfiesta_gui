﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Versioning;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.Catalog;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {

            /// <summary>
            /// Schema target_data
            /// </summary>
            public class TDSchema
                : IDatabaseSchema
            {

                #region Constants

                /// <summary>
                /// Schema name,
                /// contains default value,
                /// value is not constant now,
                /// extension can be created with different schemas
                /// </summary>
                public static string Name = "target_data";

                /// <summary>
                /// Extension name
                /// </summary>
                public const string ExtensionName = "nfiesta_target_data";

                /// <summary>
                /// Extension version
                /// </summary>
                public static readonly ExtensionVersion ExtensionVersion =
                    new(version: "2.24.7");

                #endregion Constantss


                #region Private Fields

                #region General

                /// <summary>
                /// Database
                /// </summary>
                private IDatabase database;

                /// <summary>
                /// Omitted tables
                /// </summary>
                private List<string> omittedTables;

                #endregion General


                #region Lookup Tables

                /// <summary>
                /// List of area domains
                /// </summary>
                private ZaJi.NfiEstaPg.TargetData.TDAreaDomainList cAreaDomain;

                /// <summary>
                /// List of area domain categories
                /// </summary>
                private ZaJi.NfiEstaPg.TargetData.TDAreaDomainCategoryList cAreaDomainCategory;

                /// <summary>
                /// List of areal or population categories
                /// </summary>
                private ZaJi.NfiEstaPg.TargetData.TDArealOrPopulationList cArealOrPopulation;

                /// <summary>
                /// List of classification type categories
                /// </summary>
                private ZaJi.NfiEstaPg.TargetData.TDClassificationTypeList cClassificationType;

                /// <summary>
                /// List of definition variants
                /// </summary>
                private ZaJi.NfiEstaPg.TargetData.TDDefinitionVariantList cDefinitionVariant;

                /// <summary>
                /// List of export connections
                /// </summary>
                private ZaJi.NfiEstaPg.TargetData.TDExportConnectionList cExportConnection;

                /// <summary>
                /// List of local density contributions
                /// </summary>
                private ZaJi.NfiEstaPg.TargetData.TDLDsityList cLDsity;

                /// <summary>
                /// List of local density objects
                /// </summary>
                private ZaJi.NfiEstaPg.TargetData.TDLDsityObjectList cLDsityObject;

                /// <summary>
                /// List of local density object groups
                /// </summary>
                private ZaJi.NfiEstaPg.TargetData.TDLDsityObjectGroupList cLDsityObjectGroup;

                /// <summary>
                /// List of local density object types
                /// </summary>
                private ZaJi.NfiEstaPg.TargetData.TDLDsityObjectTypeList cLDsityObjectType;

                /// <summary>
                /// List of aggregated sets of panels and corresponding reference year sets
                /// </summary>
                private ZaJi.NfiEstaPg.TargetData.TDPanelRefYearSetGroupList cPanelRefYearSetGroup;

                /// <summary>
                /// List of state or change characters
                /// </summary>
                private ZaJi.NfiEstaPg.TargetData.TDStateOrChangeList cStateOrChange;

                /// <summary>
                /// List of subpopulations
                /// </summary>
                private ZaJi.NfiEstaPg.TargetData.TDSubPopulationList cSubPopulation;

                /// <summary>
                /// List of subpopulation categories
                /// </summary>
                private ZaJi.NfiEstaPg.TargetData.TDSubPopulationCategoryList cSubPopulationCategory;

                /// <summary>
                /// List of target variables
                /// </summary>
                private ZaJi.NfiEstaPg.TargetData.TDTargetVariableList cTargetVariable;

                /// <summary>
                /// List of units of measure
                /// </summary>
                private ZaJi.NfiEstaPg.TargetData.TDUnitOfMeasureList cUnitOfMeasure;

                /// <summary>
                /// List of versions
                /// </summary>
                private ZaJi.NfiEstaPg.TargetData.TDVersionList cVersion;

                #endregion Lookup Tables


                #region Mapping Tables

                /// <summary>
                /// List of classification rules for area domain categories
                /// </summary>
                private ZaJi.NfiEstaPg.TargetData.TDADCToClassificationRuleList cmADCToClassificationRule;

                /// <summary>
                /// List of mappings between classification rules and panels/reference year sets
                /// </summary>
                private ZaJi.NfiEstaPg.TargetData.TDADCToClassRuleToPanelRefYearSetList cmADCToClassRuleToPanelRefYearSet;

                /// <summary>
                /// List of mappings between local density object and group of local density objects
                /// </summary>
                private ZaJi.NfiEstaPg.TargetData.TDLDObjectToLDObjectGroupList cmLDObjectToLDObjectGroup;

                /// <summary>
                /// Mapping between local density contribution, panels and reference year sets and also versions
                /// </summary>
                private ZaJi.NfiEstaPg.TargetData.TDLDsityToPanelRefYearSetVersionList cmLDsityToPanelRefYearSetVersion;

                /// <summary>
                /// List of mappings between ldsity2target_variable and categorization_setup
                /// </summary>
                private ZaJi.NfiEstaPg.TargetData.TDLDsityToTargetToCategorizationSetupList cmLDsityToTargetToCategorizationSetup;

                /// <summary>
                /// List of mappings between local density and target variable
                /// </summary>
                private ZaJi.NfiEstaPg.TargetData.TDLDsityToTargetVariableList cmLDsityToTargetVariable;

                /// <summary>
                /// List of classification rules for subpopulation category
                /// </summary>
                private ZaJi.NfiEstaPg.TargetData.TDSPCToClassificationRuleList cmSPCToClassificationRule;

                /// <summary>
                /// List of mappings between classification rules and panels/reference year sets
                /// </summary>
                private ZaJi.NfiEstaPg.TargetData.TDSPCToClassRuleToPanelRefYearSetList cmSPCToClassRuleToPanelRefYearSet;

                /// <summary>
                /// List of area domain category mappings
                /// </summary>
                private ZaJi.NfiEstaPg.TargetData.TDADCHierarchyList tADCHierarchy;

                /// <summary>
                /// List of subpopulation category mappings
                /// </summary>
                private ZaJi.NfiEstaPg.TargetData.TDSPCHierarchyList tSPCHierarchy;

                #endregion Mapping Tables


                #region Spatial Data Tables
                #endregion Spatial Data Tables


                #region Data Tables

                /// <summary>
                /// List of available data sets
                /// </summary>
                private ZaJi.NfiEstaPg.TargetData.TDAvailableDataSetList tAvailableDataSet;

                /// <summary>
                /// List of categorizations
                /// </summary>
                private ZaJi.NfiEstaPg.TargetData.TDCategorizationSetupList tCategorizationSetup;

                /// <summary>
                /// List of area domains for ETL
                /// </summary>
                private ZaJi.NfiEstaPg.TargetData.TDEtlAreaDomainList tEtlAreaDomain;

                /// <summary>
                ///  List of area domain categories for ETL
                /// </summary>
                private ZaJi.NfiEstaPg.TargetData.TDEtlAreaDomainCategoryList tEtlAreaDomainCategory;

                /// <summary>
                /// List of ETL times for ETL target variable
                /// </summary>
                private ZaJi.NfiEstaPg.TargetData.TDEtlLogList tEtlLog;

                /// <summary>
                /// List of sub-populations for ETL
                /// </summary>
                private ZaJi.NfiEstaPg.TargetData.TDEtlSubPopulationList tEtlSubPopulation;

                /// <summary>
                /// List of sub-population categories for ETL
                /// </summary>
                private ZaJi.NfiEstaPg.TargetData.TDEtlSubPopulationCategoryList tEtlSubPopulationCategory;

                /// <summary>
                /// List of target variables for ETL
                /// </summary>
                private ZaJi.NfiEstaPg.TargetData.TDEtlTargetVariableList tEtlTargetVariable;

                /// <summary>
                /// List of local density values
                /// </summary>
                private ZaJi.NfiEstaPg.TargetData.TDLDsityValueList tLDsityValue;

                /// <summary>
                /// List of pairs of panel with corresponding reference year set
                /// </summary>
                private ZaJi.NfiEstaPg.TargetData.TDPanelRefYearSetPairList tPanelRefYearSetGroup;

                #endregion Data Tables

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database</param>
                public TDSchema(NfiEstaDB database)
                {

                    #region General

                    Database = database;

                    OmittedTables = [];

                    #endregion General


                    #region Lookup Tables

                    CAreaDomain =
                        new ZaJi.NfiEstaPg.TargetData.TDAreaDomainList(
                        database: (NfiEstaDB)Database);
                    CAreaDomainCategory =
                        new ZaJi.NfiEstaPg.TargetData.TDAreaDomainCategoryList(
                        database: (NfiEstaDB)Database);
                    CArealOrPopulation =
                        new ZaJi.NfiEstaPg.TargetData.TDArealOrPopulationList(
                        database: (NfiEstaDB)Database);
                    CClassificationType =
                        new ZaJi.NfiEstaPg.TargetData.TDClassificationTypeList(
                        database: (NfiEstaDB)Database);
                    CDefinitionVariant =
                        new ZaJi.NfiEstaPg.TargetData.TDDefinitionVariantList(
                        database: (NfiEstaDB)Database);
                    CLDsity =
                        new ZaJi.NfiEstaPg.TargetData.TDLDsityList(
                        database: (NfiEstaDB)Database);
                    CLDsityObject =
                        new ZaJi.NfiEstaPg.TargetData.TDLDsityObjectList(
                        database: (NfiEstaDB)Database);
                    CLDsityObjectGroup =
                        new ZaJi.NfiEstaPg.TargetData.TDLDsityObjectGroupList(
                        database: (NfiEstaDB)Database);
                    CLDsityObjectType =
                        new ZaJi.NfiEstaPg.TargetData.TDLDsityObjectTypeList(
                        database: (NfiEstaDB)Database);
                    CPanelRefYearSetGroup =
                        new ZaJi.NfiEstaPg.TargetData.TDPanelRefYearSetGroupList(
                        database: (NfiEstaDB)Database);
                    CStateOrChange =
                        new ZaJi.NfiEstaPg.TargetData.TDStateOrChangeList(
                        database: (NfiEstaDB)Database);
                    CSubPopulation =
                        new ZaJi.NfiEstaPg.TargetData.TDSubPopulationList(
                        database: (NfiEstaDB)Database);
                    CSubPopulationCategory =
                        new ZaJi.NfiEstaPg.TargetData.TDSubPopulationCategoryList(
                        database: (NfiEstaDB)Database);
                    CTargetVariable =
                        new ZaJi.NfiEstaPg.TargetData.TDTargetVariableList(
                        database: (NfiEstaDB)Database);
                    CUnitOfMeasure =
                        new ZaJi.NfiEstaPg.TargetData.TDUnitOfMeasureList(
                        database: (NfiEstaDB)Database);
                    CVersion =
                        new ZaJi.NfiEstaPg.TargetData.TDVersionList(
                        database: (NfiEstaDB)Database);

                    #endregion Lookup Tables


                    #region Mapping tables

                    CmADCToClassificationRule =
                        new ZaJi.NfiEstaPg.TargetData.TDADCToClassificationRuleList(
                        database: (NfiEstaDB)Database);
                    CmADCToClassRuleToPanelRefYearSet =
                        new ZaJi.NfiEstaPg.TargetData.TDADCToClassRuleToPanelRefYearSetList(
                        database: (NfiEstaDB)Database);
                    CmLDObjectToLDObjectGroup =
                        new ZaJi.NfiEstaPg.TargetData.TDLDObjectToLDObjectGroupList(
                        database: (NfiEstaDB)Database);
                    CmLDsityToPanelRefYearSetVersion =
                        new ZaJi.NfiEstaPg.TargetData.TDLDsityToPanelRefYearSetVersionList(
                        database: (NfiEstaDB)Database);
                    CmLDsityToTargetToCategorizationSetup =
                        new ZaJi.NfiEstaPg.TargetData.TDLDsityToTargetToCategorizationSetupList(
                        database: (NfiEstaDB)Database);
                    CmLDsityToTargetVariable =
                        new ZaJi.NfiEstaPg.TargetData.TDLDsityToTargetVariableList(
                        database: (NfiEstaDB)Database);
                    CmSPCToClassificationRule =
                        new ZaJi.NfiEstaPg.TargetData.TDSPCToClassificationRuleList(
                        database: (NfiEstaDB)Database);
                    CmSPCToClassRuleToPanelRefYearSet =
                        new ZaJi.NfiEstaPg.TargetData.TDSPCToClassRuleToPanelRefYearSetList(
                        database: (NfiEstaDB)Database);

                    #endregion Mapping tables


                    #region Spatial Data Tables
                    #endregion Spatial Data Tables


                    #region Data tables

                    CExportConnection =
                        new ZaJi.NfiEstaPg.TargetData.TDExportConnectionList(
                        database: (NfiEstaDB)Database);
                    TAvailableDataSet =
                        new ZaJi.NfiEstaPg.TargetData.TDAvailableDataSetList(
                        database: (NfiEstaDB)Database);
                    TCategorizationSetup =
                        new ZaJi.NfiEstaPg.TargetData.TDCategorizationSetupList(
                        database: (NfiEstaDB)Database);
                    TEtlAreaDomain =
                        new ZaJi.NfiEstaPg.TargetData.TDEtlAreaDomainList(
                        database: (NfiEstaDB)Database);
                    TEtlAreaDomainCategory =
                        new ZaJi.NfiEstaPg.TargetData.TDEtlAreaDomainCategoryList(
                        database: (NfiEstaDB)Database);
                    TEtlLog = new ZaJi.NfiEstaPg.TargetData.TDEtlLogList(
                        database: (NfiEstaDB)Database);
                    TEtlSubPopulation =
                        new ZaJi.NfiEstaPg.TargetData.TDEtlSubPopulationList(
                        database: (NfiEstaDB)Database);
                    TEtlSubPopulationCategory =
                        new ZaJi.NfiEstaPg.TargetData.TDEtlSubPopulationCategoryList(
                        database: (NfiEstaDB)Database);
                    TEtlTargetVariable =
                        new ZaJi.NfiEstaPg.TargetData.TDEtlTargetVariableList(
                        database: (NfiEstaDB)Database);
                    TLDsityValue =
                        new ZaJi.NfiEstaPg.TargetData.TDLDsityValueList(
                        database: (NfiEstaDB)Database);
                    TPanelRefYearSetGroup =
                        new ZaJi.NfiEstaPg.TargetData.TDPanelRefYearSetPairList(
                        database: (NfiEstaDB)Database);
                    TADCHierarchy =
                        new ZaJi.NfiEstaPg.TargetData.TDADCHierarchyList(
                        database: (NfiEstaDB)Database);
                    TSPCHierarchy =
                        new ZaJi.NfiEstaPg.TargetData.TDSPCHierarchyList(
                        database: (NfiEstaDB)Database);

                    #endregion Data tables

                }

                #endregion Constructor


                #region Properties

                #region General

                /// <summary>
                /// Schema name (read-only)
                /// </summary>
                public string SchemaName
                {
                    get
                    {
                        return TDSchema.Name;
                    }
                }

                /// <summary>
                /// Database (not-null) (read-only)
                /// </summary>
                public IDatabase Database
                {
                    get
                    {
                        if (database == null)
                        {
                            throw new ArgumentNullException(
                                message: $"Argument {nameof(Database)} must not be null.",
                                paramName: nameof(Database));
                        }

                        if (database is not NfiEstaDB)
                        {
                            throw new ArgumentException(
                                message: $"Argument {nameof(Database)} must be type of {nameof(NfiEstaDB)}.",
                                paramName: nameof(Database));
                        }

                        return database;
                    }
                    private set
                    {
                        if (value == null)
                        {
                            throw new ArgumentNullException(
                                message: $"Argument {nameof(Database)} must not be null.",
                                paramName: nameof(Database));
                        }

                        if (value is not NfiEstaDB)
                        {
                            throw new ArgumentException(
                                message: $"Argument {nameof(Database)} must be type of {nameof(NfiEstaDB)}.",
                                paramName: nameof(Database));
                        }

                        database = value;
                    }
                }

                /// <summary>
                /// List of lookup tables (not-null) (read-only)
                /// </summary>
                public List<ILookupTable> LookupTables
                {
                    get
                    {
                        return
                        [
                            CAreaDomain,
                            CAreaDomainCategory,
                            CArealOrPopulation,
                            CClassificationType,
                            CDefinitionVariant,
                            CLDsity,
                            CLDsityObject,
                            CLDsityObjectGroup,
                            CLDsityObjectType,
                            CPanelRefYearSetGroup,
                            CStateOrChange,
                            CSubPopulation,
                            CSubPopulationCategory,
                            CTargetVariable,
                            CUnitOfMeasure,
                            CVersion
                        ];
                    }
                }

                /// <summary>
                /// List of mapping tables (not-null) (read-only)
                /// </summary>
                public List<IMappingTable> MappingTables
                {
                    get
                    {
                        return
                        [
                            CmADCToClassificationRule,
                            CmADCToClassRuleToPanelRefYearSet,
                            CmLDObjectToLDObjectGroup,
                            CmLDsityToPanelRefYearSetVersion,
                            CmLDsityToTargetToCategorizationSetup,
                            CmLDsityToTargetVariable,
                            CmSPCToClassificationRule,
                            CmSPCToClassRuleToPanelRefYearSet
                        ];
                    }
                }

                /// <summary>
                /// List of spatial data tables (not-null) (read-only)
                /// </summary>
                public List<ISpatialTable> SpatialTables
                {
                    get
                    {
                        return [];
                    }
                }

                /// <summary>
                /// List of data tables (not-null) (read-only)
                /// </summary>
                public List<IDataTable> DataTables
                {
                    get
                    {
                        return
                        [
                            CExportConnection,
                            TAvailableDataSet,
                            TCategorizationSetup,
                            TEtlAreaDomain,
                            TEtlAreaDomainCategory,
                            TEtlLog,
                            TEtlSubPopulation,
                            TEtlSubPopulationCategory,
                            TEtlTargetVariable,
                            TLDsityValue,
                            TPanelRefYearSetGroup,
                            TADCHierarchy,
                            TSPCHierarchy
                        ];
                    }
                }

                /// <summary>
                /// List of all tables (not-null) (read-only)
                /// </summary>
                public List<IDatabaseTable> Tables
                {
                    get
                    {
                        List<IDatabaseTable> tables = [];
                        tables.AddRange(collection: LookupTables);
                        tables.AddRange(collection: MappingTables);
                        tables.AddRange(collection: SpatialTables);
                        tables.AddRange(collection: DataTables);
                        tables.Sort(comparison: (a, b) => a.TableName.CompareTo(strB: b.TableName));
                        return tables;
                    }
                }

                /// <summary>
                /// Omitted tables
                /// </summary>
                public List<string> OmittedTables
                {
                    get
                    {
                        return omittedTables ?? [];
                    }
                    set
                    {
                        omittedTables = value ?? [];
                    }
                }

                #endregion General


                #region Lookup Tables

                /// <summary>
                /// List of area domains (not-null)
                /// </summary>
                public ZaJi.NfiEstaPg.TargetData.TDAreaDomainList CAreaDomain
                {
                    get
                    {
                        return cAreaDomain ??
                            new ZaJi.NfiEstaPg.TargetData.TDAreaDomainList(database: (NfiEstaDB)Database);
                    }
                    set
                    {
                        cAreaDomain = value ??
                            new ZaJi.NfiEstaPg.TargetData.TDAreaDomainList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of area domain categories (not-null)
                /// </summary>
                public ZaJi.NfiEstaPg.TargetData.TDAreaDomainCategoryList CAreaDomainCategory
                {
                    get
                    {
                        return cAreaDomainCategory ??
                            new ZaJi.NfiEstaPg.TargetData.TDAreaDomainCategoryList(database: (NfiEstaDB)Database);
                    }
                    set
                    {
                        cAreaDomainCategory = value ??
                            new ZaJi.NfiEstaPg.TargetData.TDAreaDomainCategoryList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of areal or population categories (not-null)
                /// </summary>
                public ZaJi.NfiEstaPg.TargetData.TDArealOrPopulationList CArealOrPopulation
                {
                    get
                    {
                        return cArealOrPopulation ??
                            new ZaJi.NfiEstaPg.TargetData.TDArealOrPopulationList(database: (NfiEstaDB)Database);
                    }
                    set
                    {
                        cArealOrPopulation = value ??
                            new ZaJi.NfiEstaPg.TargetData.TDArealOrPopulationList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of classification type categories (not-null)
                /// </summary>
                public ZaJi.NfiEstaPg.TargetData.TDClassificationTypeList CClassificationType
                {
                    get
                    {
                        return cClassificationType ??
                            new ZaJi.NfiEstaPg.TargetData.TDClassificationTypeList(database: (NfiEstaDB)Database);
                    }
                    set
                    {
                        cClassificationType = value ??
                            new ZaJi.NfiEstaPg.TargetData.TDClassificationTypeList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of definition variants (not-null)
                /// </summary>
                public ZaJi.NfiEstaPg.TargetData.TDDefinitionVariantList CDefinitionVariant
                {
                    get
                    {
                        return cDefinitionVariant ??
                            new ZaJi.NfiEstaPg.TargetData.TDDefinitionVariantList(database: (NfiEstaDB)Database);
                    }
                    set
                    {
                        cDefinitionVariant = value ??
                            new ZaJi.NfiEstaPg.TargetData.TDDefinitionVariantList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of local density contributions (not-null)
                /// </summary>
                public ZaJi.NfiEstaPg.TargetData.TDLDsityList CLDsity
                {
                    get
                    {
                        return cLDsity ??
                            new ZaJi.NfiEstaPg.TargetData.TDLDsityList(database: (NfiEstaDB)Database);
                    }
                    set
                    {
                        cLDsity = value ??
                            new ZaJi.NfiEstaPg.TargetData.TDLDsityList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of local density objects (not-null)
                /// </summary>
                public ZaJi.NfiEstaPg.TargetData.TDLDsityObjectList CLDsityObject
                {
                    get
                    {
                        return cLDsityObject ??
                            new ZaJi.NfiEstaPg.TargetData.TDLDsityObjectList(database: (NfiEstaDB)Database);
                    }
                    set
                    {
                        cLDsityObject = value ??
                            new ZaJi.NfiEstaPg.TargetData.TDLDsityObjectList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of local density object groups (not-null)
                /// </summary>
                public ZaJi.NfiEstaPg.TargetData.TDLDsityObjectGroupList CLDsityObjectGroup
                {
                    get
                    {
                        return cLDsityObjectGroup ??
                            new ZaJi.NfiEstaPg.TargetData.TDLDsityObjectGroupList(database: (NfiEstaDB)Database);
                    }
                    set
                    {
                        cLDsityObjectGroup = value ??
                            new ZaJi.NfiEstaPg.TargetData.TDLDsityObjectGroupList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of local density object types (not-null)
                /// </summary>
                public ZaJi.NfiEstaPg.TargetData.TDLDsityObjectTypeList CLDsityObjectType
                {
                    get
                    {
                        return cLDsityObjectType ??
                            new ZaJi.NfiEstaPg.TargetData.TDLDsityObjectTypeList(database: (NfiEstaDB)Database);
                    }
                    set
                    {
                        cLDsityObjectType = value ??
                            new ZaJi.NfiEstaPg.TargetData.TDLDsityObjectTypeList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of aggregated sets of panels and corresponding reference year sets
                /// (not-null)
                /// </summary>
                public ZaJi.NfiEstaPg.TargetData.TDPanelRefYearSetGroupList CPanelRefYearSetGroup
                {
                    get
                    {
                        return cPanelRefYearSetGroup ??
                            new ZaJi.NfiEstaPg.TargetData.TDPanelRefYearSetGroupList(database: (NfiEstaDB)Database);
                    }
                    set
                    {
                        cPanelRefYearSetGroup = value ??
                            new ZaJi.NfiEstaPg.TargetData.TDPanelRefYearSetGroupList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of state or change characters (not-null)
                /// </summary>
                public ZaJi.NfiEstaPg.TargetData.TDStateOrChangeList CStateOrChange
                {
                    get
                    {
                        return cStateOrChange ??
                            new ZaJi.NfiEstaPg.TargetData.TDStateOrChangeList(database: (NfiEstaDB)Database);
                    }
                    set
                    {
                        cStateOrChange = value ??
                            new ZaJi.NfiEstaPg.TargetData.TDStateOrChangeList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of subpopulations (not-null)
                /// </summary>
                public ZaJi.NfiEstaPg.TargetData.TDSubPopulationList CSubPopulation
                {
                    get
                    {
                        return cSubPopulation ??
                            new ZaJi.NfiEstaPg.TargetData.TDSubPopulationList(database: (NfiEstaDB)Database);
                    }
                    set
                    {
                        cSubPopulation = value ??
                            new ZaJi.NfiEstaPg.TargetData.TDSubPopulationList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of subpopulation categories (not-null)
                /// </summary>
                public ZaJi.NfiEstaPg.TargetData.TDSubPopulationCategoryList CSubPopulationCategory
                {
                    get
                    {
                        return cSubPopulationCategory ??
                            new ZaJi.NfiEstaPg.TargetData.TDSubPopulationCategoryList(database: (NfiEstaDB)Database);
                    }
                    set
                    {
                        cSubPopulationCategory = value ??
                            new ZaJi.NfiEstaPg.TargetData.TDSubPopulationCategoryList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of target variables (not-null)
                /// </summary>
                public ZaJi.NfiEstaPg.TargetData.TDTargetVariableList CTargetVariable
                {
                    get
                    {
                        return cTargetVariable ??
                            new ZaJi.NfiEstaPg.TargetData.TDTargetVariableList(database: (NfiEstaDB)Database);
                    }
                    set
                    {
                        cTargetVariable = value ??
                            new ZaJi.NfiEstaPg.TargetData.TDTargetVariableList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of units of measure (not-null)
                /// </summary>
                public ZaJi.NfiEstaPg.TargetData.TDUnitOfMeasureList CUnitOfMeasure
                {
                    get
                    {
                        return cUnitOfMeasure ??
                            new ZaJi.NfiEstaPg.TargetData.TDUnitOfMeasureList(database: (NfiEstaDB)Database);
                    }
                    set
                    {
                        cUnitOfMeasure = value ??
                            new ZaJi.NfiEstaPg.TargetData.TDUnitOfMeasureList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of versions (not-null)
                /// </summary>
                public ZaJi.NfiEstaPg.TargetData.TDVersionList CVersion
                {
                    get
                    {
                        return cVersion ??
                            new ZaJi.NfiEstaPg.TargetData.TDVersionList(database: (NfiEstaDB)Database);
                    }
                    set
                    {
                        cVersion = value ??
                            new ZaJi.NfiEstaPg.TargetData.TDVersionList(database: (NfiEstaDB)Database);
                    }
                }

                #endregion Lookup Tables


                #region Mapping Tables

                /// <summary>
                /// List of classification rules for area domain categories (not-null)
                /// </summary>
                public ZaJi.NfiEstaPg.TargetData.TDADCToClassificationRuleList CmADCToClassificationRule
                {
                    get
                    {
                        return cmADCToClassificationRule ??
                            new ZaJi.NfiEstaPg.TargetData.TDADCToClassificationRuleList(database: (NfiEstaDB)Database);
                    }
                    set
                    {
                        cmADCToClassificationRule = value ??
                            new ZaJi.NfiEstaPg.TargetData.TDADCToClassificationRuleList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of mappings between classification rules and panels/reference year sets (not-null)
                /// </summary>
                public ZaJi.NfiEstaPg.TargetData.TDADCToClassRuleToPanelRefYearSetList CmADCToClassRuleToPanelRefYearSet
                {
                    get
                    {
                        return cmADCToClassRuleToPanelRefYearSet ??
                            new ZaJi.NfiEstaPg.TargetData.TDADCToClassRuleToPanelRefYearSetList(database: (NfiEstaDB)Database);
                    }
                    set
                    {
                        cmADCToClassRuleToPanelRefYearSet = value ??
                            new ZaJi.NfiEstaPg.TargetData.TDADCToClassRuleToPanelRefYearSetList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of mappings between local density object and group of local density objects (not-null)
                /// </summary>
                public ZaJi.NfiEstaPg.TargetData.TDLDObjectToLDObjectGroupList CmLDObjectToLDObjectGroup
                {
                    get
                    {
                        return cmLDObjectToLDObjectGroup ??
                            new ZaJi.NfiEstaPg.TargetData.TDLDObjectToLDObjectGroupList(database: (NfiEstaDB)Database);
                    }
                    set
                    {
                        cmLDObjectToLDObjectGroup = value ??
                            new ZaJi.NfiEstaPg.TargetData.TDLDObjectToLDObjectGroupList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// Mapping between local density contribution, panels and reference year sets and also versions (not-null)
                /// </summary>
                public ZaJi.NfiEstaPg.TargetData.TDLDsityToPanelRefYearSetVersionList CmLDsityToPanelRefYearSetVersion
                {
                    get
                    {
                        return cmLDsityToPanelRefYearSetVersion ??
                            new ZaJi.NfiEstaPg.TargetData.TDLDsityToPanelRefYearSetVersionList(database: (NfiEstaDB)Database);
                    }
                    set
                    {
                        cmLDsityToPanelRefYearSetVersion = value ??
                            new ZaJi.NfiEstaPg.TargetData.TDLDsityToPanelRefYearSetVersionList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of mappings between ldsity2target_variable and categorization_setup (not-null)
                /// </summary>
                public ZaJi.NfiEstaPg.TargetData.TDLDsityToTargetToCategorizationSetupList CmLDsityToTargetToCategorizationSetup
                {
                    get
                    {
                        return cmLDsityToTargetToCategorizationSetup ??
                            new ZaJi.NfiEstaPg.TargetData.TDLDsityToTargetToCategorizationSetupList(database: (NfiEstaDB)Database);
                    }
                    set
                    {
                        cmLDsityToTargetToCategorizationSetup = value ??
                            new ZaJi.NfiEstaPg.TargetData.TDLDsityToTargetToCategorizationSetupList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of mappings between local density and target variable (not-null)
                /// </summary>
                public ZaJi.NfiEstaPg.TargetData.TDLDsityToTargetVariableList CmLDsityToTargetVariable
                {
                    get
                    {
                        return cmLDsityToTargetVariable ??
                            new ZaJi.NfiEstaPg.TargetData.TDLDsityToTargetVariableList(database: (NfiEstaDB)Database);
                    }
                    set
                    {
                        cmLDsityToTargetVariable = value ??
                            new ZaJi.NfiEstaPg.TargetData.TDLDsityToTargetVariableList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of classification rules for subpopulation category (not-null)
                /// </summary>
                public ZaJi.NfiEstaPg.TargetData.TDSPCToClassificationRuleList CmSPCToClassificationRule
                {
                    get
                    {
                        return cmSPCToClassificationRule ??
                            new ZaJi.NfiEstaPg.TargetData.TDSPCToClassificationRuleList(database: (NfiEstaDB)Database);
                    }
                    set
                    {
                        cmSPCToClassificationRule = value ??
                            new ZaJi.NfiEstaPg.TargetData.TDSPCToClassificationRuleList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of mappings between classification rules and panels/reference year sets (not-null)
                /// </summary>
                public ZaJi.NfiEstaPg.TargetData.TDSPCToClassRuleToPanelRefYearSetList CmSPCToClassRuleToPanelRefYearSet
                {
                    get
                    {
                        return cmSPCToClassRuleToPanelRefYearSet ??
                            new ZaJi.NfiEstaPg.TargetData.TDSPCToClassRuleToPanelRefYearSetList(database: (NfiEstaDB)Database);
                    }
                    set
                    {
                        cmSPCToClassRuleToPanelRefYearSet = value ??
                            new ZaJi.NfiEstaPg.TargetData.TDSPCToClassRuleToPanelRefYearSetList(database: (NfiEstaDB)Database);
                    }
                }

                #endregion Mapping Tables


                #region Spatial Data Tables
                #endregion Spatial Data Tables


                #region Data Tables

                /// <summary>
                /// List of export connections (not-null)
                /// </summary>
                public ZaJi.NfiEstaPg.TargetData.TDExportConnectionList CExportConnection
                {
                    get
                    {
                        return cExportConnection ??
                            new ZaJi.NfiEstaPg.TargetData.TDExportConnectionList(database: (NfiEstaDB)Database);
                    }
                    set
                    {
                        cExportConnection = value ??
                            new ZaJi.NfiEstaPg.TargetData.TDExportConnectionList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of available datasets (not-null)
                /// </summary>
                public ZaJi.NfiEstaPg.TargetData.TDAvailableDataSetList TAvailableDataSet
                {
                    get
                    {
                        return tAvailableDataSet ??
                            new ZaJi.NfiEstaPg.TargetData.TDAvailableDataSetList(database: (NfiEstaDB)Database);
                    }
                    set
                    {
                        tAvailableDataSet = value ??
                            new ZaJi.NfiEstaPg.TargetData.TDAvailableDataSetList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of categorizations (not-null)
                /// </summary>
                public ZaJi.NfiEstaPg.TargetData.TDCategorizationSetupList TCategorizationSetup
                {
                    get
                    {
                        return tCategorizationSetup ??
                            new ZaJi.NfiEstaPg.TargetData.TDCategorizationSetupList(database: (NfiEstaDB)Database);
                    }
                    set
                    {
                        tCategorizationSetup = value ??
                            new ZaJi.NfiEstaPg.TargetData.TDCategorizationSetupList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of area domains for ETL (not-null)
                /// </summary>
                public ZaJi.NfiEstaPg.TargetData.TDEtlAreaDomainList TEtlAreaDomain
                {
                    get
                    {
                        return tEtlAreaDomain ??
                            new ZaJi.NfiEstaPg.TargetData.TDEtlAreaDomainList(database: (NfiEstaDB)Database);
                    }
                    set
                    {
                        tEtlAreaDomain = value ??
                            new ZaJi.NfiEstaPg.TargetData.TDEtlAreaDomainList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                ///  List of area domain categories for ETL (not-null)
                /// </summary>
                public ZaJi.NfiEstaPg.TargetData.TDEtlAreaDomainCategoryList TEtlAreaDomainCategory
                {
                    get
                    {
                        return tEtlAreaDomainCategory ??
                            new ZaJi.NfiEstaPg.TargetData.TDEtlAreaDomainCategoryList(database: (NfiEstaDB)Database);
                    }
                    set
                    {
                        tEtlAreaDomainCategory = value ??
                            new ZaJi.NfiEstaPg.TargetData.TDEtlAreaDomainCategoryList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of ETL times for ETL target variable (not-null)
                /// </summary>
                public ZaJi.NfiEstaPg.TargetData.TDEtlLogList TEtlLog
                {
                    get
                    {
                        return tEtlLog ??
                            new ZaJi.NfiEstaPg.TargetData.TDEtlLogList(database: (NfiEstaDB)Database);
                    }
                    set
                    {
                        tEtlLog = value ??
                            new ZaJi.NfiEstaPg.TargetData.TDEtlLogList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of sub-populations for ETL (not-null)
                /// </summary>
                public ZaJi.NfiEstaPg.TargetData.TDEtlSubPopulationList TEtlSubPopulation
                {
                    get
                    {
                        return tEtlSubPopulation ??
                            new ZaJi.NfiEstaPg.TargetData.TDEtlSubPopulationList(database: (NfiEstaDB)Database);
                    }
                    set
                    {
                        tEtlSubPopulation = value ??
                            new ZaJi.NfiEstaPg.TargetData.TDEtlSubPopulationList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of sub-population categories for ETL (not-null)
                /// </summary>
                public ZaJi.NfiEstaPg.TargetData.TDEtlSubPopulationCategoryList TEtlSubPopulationCategory
                {
                    get
                    {
                        return tEtlSubPopulationCategory ??
                            new ZaJi.NfiEstaPg.TargetData.TDEtlSubPopulationCategoryList(database: (NfiEstaDB)Database);
                    }
                    set
                    {
                        tEtlSubPopulationCategory = value ??
                            new ZaJi.NfiEstaPg.TargetData.TDEtlSubPopulationCategoryList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of target variables for ETL (not-null)
                /// </summary>
                public ZaJi.NfiEstaPg.TargetData.TDEtlTargetVariableList TEtlTargetVariable
                {
                    get
                    {
                        return tEtlTargetVariable ??
                            new ZaJi.NfiEstaPg.TargetData.TDEtlTargetVariableList(database: (NfiEstaDB)Database);
                    }
                    set
                    {
                        tEtlTargetVariable = value ??
                            new ZaJi.NfiEstaPg.TargetData.TDEtlTargetVariableList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of local density values (not-null)
                /// </summary>
                public ZaJi.NfiEstaPg.TargetData.TDLDsityValueList TLDsityValue
                {
                    get
                    {
                        return tLDsityValue ??
                            new ZaJi.NfiEstaPg.TargetData.TDLDsityValueList(database: (NfiEstaDB)Database);
                    }
                    set
                    {
                        tLDsityValue = value ??
                            new ZaJi.NfiEstaPg.TargetData.TDLDsityValueList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of pairs of panel with corresponding reference year set (not-null)
                /// </summary>
                public ZaJi.NfiEstaPg.TargetData.TDPanelRefYearSetPairList TPanelRefYearSetGroup
                {
                    get
                    {
                        return tPanelRefYearSetGroup ??
                            new ZaJi.NfiEstaPg.TargetData.TDPanelRefYearSetPairList(database: (NfiEstaDB)Database);
                    }
                    set
                    {
                        tPanelRefYearSetGroup = value ??
                            new ZaJi.NfiEstaPg.TargetData.TDPanelRefYearSetPairList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of area domain category mappings (not-null)
                /// </summary>
                public ZaJi.NfiEstaPg.TargetData.TDADCHierarchyList TADCHierarchy
                {
                    get
                    {
                        return tADCHierarchy ??
                            new ZaJi.NfiEstaPg.TargetData.TDADCHierarchyList(database: (NfiEstaDB)Database);
                    }
                    set
                    {
                        tADCHierarchy = value ??
                            new ZaJi.NfiEstaPg.TargetData.TDADCHierarchyList(database: (NfiEstaDB)Database);
                    }
                }

                /// <summary>
                /// List of subpopulation category mappings (not-null)
                /// </summary>
                public ZaJi.NfiEstaPg.TargetData.TDSPCHierarchyList TSPCHierarchy
                {
                    get
                    {
                        return tSPCHierarchy ??
                            new ZaJi.NfiEstaPg.TargetData.TDSPCHierarchyList(database: (NfiEstaDB)Database);
                    }
                    set
                    {
                        tSPCHierarchy = value ??
                            new ZaJi.NfiEstaPg.TargetData.TDSPCHierarchyList(database: (NfiEstaDB)Database);
                    }
                }

                #endregion Data Tables


                #region Differences

                /// <summary>
                /// Differences in database schema (not-null) (read-only)
                /// </summary>
                public List<string> Differences
                {
                    get
                    {
                        List<string> result = [];
                        Database.Postgres.Catalog.Load();

                        DBExtension dbExtension = Database.Postgres.Catalog.Extensions.Items
                            .Where(a => a.Name == TDSchema.ExtensionName)
                            .FirstOrDefault();

                        if (dbExtension == null)
                        {
                            result.Add(item: String.Concat(
                                $"Extension {TDSchema.ExtensionName} does not exist in database."));
                            return result;
                        }
                        else
                        {
                            ExtensionVersion dbExtensionVersion = new(
                                 version: dbExtension.Version);
                            if (!dbExtensionVersion.Equals(obj: TDSchema.ExtensionVersion))
                            {
                                result.Add(item: String.Concat(
                                    $"Database extension {TDSchema.ExtensionName} is in version {dbExtension.Version}. ",
                                    $"But the version {TDSchema.ExtensionVersion} was expected."));
                            }

                            DBSchema dbSchema = Database.Postgres.Catalog.Schemas.Items
                                .Where(a => a.Name == TDSchema.Name)
                                .FirstOrDefault();

                            if (dbSchema == null)
                            {
                                result.Add(item: String.Concat(
                                    $"Schema {TDSchema.Name} does not exist in database."));
                                return result;
                            }
                            else
                            {
                                foreach (DBTable dbTable in dbSchema.Tables.Items.OrderBy(a => a.Name))
                                {
                                    if (OmittedTables.Contains(item: dbTable.Name))
                                    {
                                        continue;
                                    }

                                    IDatabaseTable iTable = ((NfiEstaDB)Database).STargetData.Tables
                                        .Where(a => a.TableName == dbTable.Name)
                                        .FirstOrDefault();

                                    if (iTable == null)
                                    {
                                        result.Add(item: String.Concat(
                                            $"Class for table {dbTable.Name} does not exist in dll library."));
                                    }
                                }

                                foreach (IDatabaseTable iTable in ((NfiEstaDB)Database).STargetData.Tables.OrderBy(a => a.TableName))
                                {
                                    DBTable dbTable = dbSchema.Tables.Items
                                            .Where(a => a.Name == iTable.TableName)
                                            .FirstOrDefault();

                                    if (dbTable == null)
                                    {
                                        if (iTable.Columns.Values.Where(a => a.Elemental).Any())
                                        {
                                            result.Add(item: String.Concat(
                                                $"Table {iTable.TableName} does not exist in database."));
                                        }
                                        else
                                        {
                                            // Table doesn't contain any column that is loaded from database.
                                        }
                                    }
                                    else
                                    {
                                        foreach (DBColumn dbColumn in dbTable.Columns.Items.OrderBy(a => a.Name))
                                        {
                                            ColumnMetadata iColumn = iTable.Columns.Values
                                                .Where(a => a.Elemental)
                                                .Where(a => a.DbName == dbColumn.Name)
                                                .FirstOrDefault();

                                            if (iColumn == null)
                                            {
                                                result.Add(item: String.Concat(
                                                    $"Column {dbTable.Name}.{dbColumn.Name} does not exist in dll library."));
                                            }
                                        }

                                        foreach (ColumnMetadata iColumn in iTable.Columns.Values.Where(a => a.Elemental).OrderBy(a => a.DbName))
                                        {
                                            DBColumn dbColumn = dbTable.Columns.Items
                                                .Where(a => a.Name == iColumn.DbName)
                                                .FirstOrDefault();

                                            if (dbColumn == null)
                                            {
                                                result.Add(item: String.Concat(
                                                    $"Column {iTable.TableName}.{iColumn.DbName} does not exist in database."));
                                            }
                                            else
                                            {
                                                if (dbColumn.TypeName != iColumn.DbDataType)
                                                {
                                                    result.Add(String.Concat(
                                                        $"Column {iTable.TableName}.{iColumn.DbName} is type of {dbColumn.TypeName}. ",
                                                        $"But type of {iColumn.DbDataType} was expected."));
                                                }

                                                if (dbColumn.NotNull != iColumn.NotNull)
                                                {
                                                    string strDbNull = (bool)dbColumn.NotNull ? "has NOT NULL constraint" : "is NULLABLE";
                                                    string strINull = iColumn.NotNull ? "NOT NULL contraint" : "NULLABLE";
                                                    result.Add(String.Concat(
                                                        $"Column {iTable.TableName}.{iColumn.DbName} {strDbNull}. ",
                                                        $"But {strINull} was expected."));
                                                }
                                            }
                                        }
                                    }
                                }

                                Dictionary<string, string> iStoredProcedures = [];
                                foreach (
                                    Type nestedType in typeof(ZaJi.NfiEstaPg.TargetData.TDFunctions)
                                    .GetNestedTypes()
                                    .AsEnumerable()
                                    .OrderBy(a => a.FullName)
                                    )
                                {
                                    FieldInfo fieldSignature = nestedType.GetField(name: "Signature");
                                    FieldInfo fieldName = nestedType.GetField(name: "Name");
                                    if ((fieldSignature != null) && (fieldName != null))
                                    {
                                        string a = fieldSignature.GetValue(obj: null).ToString();
                                        string b = fieldName.GetValue(obj: null).ToString();

                                        iStoredProcedures.TryAdd(key: a, value: b);
                                    }

                                    PropertyInfo pSignature = nestedType.GetProperty(name: "Signature");
                                    PropertyInfo pName = nestedType.GetProperty(name: "Name");
                                    if ((pSignature != null) && (pName != null))
                                    {
                                        // signature = null označeje uložené procedury,
                                        // pro které není požadována implementace v databázi
                                        // nekontrolují se
                                        if (pSignature.GetValue(obj: null) != null)
                                        {
                                            string a = pSignature.GetValue(obj: null).ToString();
                                            string b = pName.GetValue(obj: null).ToString();

                                            iStoredProcedures.TryAdd(key: a, value: b);
                                        }
                                    }
                                }

                                foreach (DBStoredProcedure dbStoredProcedure in dbSchema.StoredProcedures.Items.OrderBy(a => a.Name))
                                {
                                    if (!iStoredProcedures.ContainsKey(dbStoredProcedure.Signature))
                                    {
                                        result.Add(String.Concat(
                                           $"Class for stored procedure {dbStoredProcedure.Name} does not exist in dll library."));
                                    }
                                }

                                foreach (KeyValuePair<string, string> iStoredProcedure in iStoredProcedures)
                                {
                                    DBStoredProcedure dbStoredProcedure = dbSchema.StoredProcedures.Items
                                            .Where(a => a.Signature == iStoredProcedure.Key)
                                            .FirstOrDefault();
                                    if (dbStoredProcedure == null)
                                    {
                                        result.Add(String.Concat(
                                               $"Stored procedure {iStoredProcedure.Value} does not exist in database."));
                                    }
                                }

                                return result;
                            }
                        }
                    }
                }

                #endregion Differences

                #endregion Properties


                #region Methods

                /// <summary>
                /// Returns the string that represents current object
                /// </summary>
                /// <returns>Returns the string that represents current object</returns>
                public override string ToString()
                {
                    return Database.Postgres.Setting.LanguageVersion switch
                    {
                        LanguageVersion.National =>
                            String.Concat($"Data schématu {TDSchema.Name}."),
                        LanguageVersion.International =>
                            String.Concat($"Data from schema {TDSchema.Name}."),
                        _ =>
                            String.Concat($"Data from schema {TDSchema.Name}."),
                    };
                }

                #endregion Methods


                #region Static Methods

                /// <summary>
                /// Exports data of the schema
                /// </summary>
                /// <param name="database">Database tables</param>
                /// <param name="fileFormat">Format of the file with exported data</param>
                /// <param name="tableNames">List of table names to export (if null than all tables)</param>
                [SupportedOSPlatform("windows")]
                public static void ExportData(
                    NfiEstaDB database,
                    ExportFileFormat fileFormat,
                    List<string> tableNames = null)
                {
                    System.Windows.Forms.FolderBrowserDialog dlg = new();

                    if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        ExportData(
                            database: database,
                            fileFormat: fileFormat,
                            folder: dlg.SelectedPath,
                            tableNames: tableNames);
                    }
                }

                /// <summary>
                /// Exports data of the schema
                /// </summary>
                /// <param name="database">Database tables</param>
                /// <param name="fileFormat">Format of the file with exported data</param>
                /// <param name="folder">Folder for writing file with data</param>
                /// <param name="tableNames">List of table names to export (if null than all tables)</param>
                public static void ExportData(
                    NfiEstaDB database,
                    ExportFileFormat fileFormat,
                    string folder,
                    List<string> tableNames = null)
                {
                    List<ADatabaseTable> databaseTables =
                    [
                        // c_area_domain
                        new ZaJi.NfiEstaPg.TargetData.TDAreaDomainList(database: database),

                        // c_area_domain_category
                        new ZaJi.NfiEstaPg.TargetData.TDAreaDomainCategoryList(database: database),

                        // c_areal_or_population
                        new ZaJi.NfiEstaPg.TargetData.TDArealOrPopulationList(database: database),

                        // c_classification_type
                        new ZaJi.NfiEstaPg.TargetData.TDClassificationTypeList(database: database),

                        // c_definition_variant
                        new ZaJi.NfiEstaPg.TargetData.TDDefinitionVariantList(database: database),

                        // c_ldsity
                        new ZaJi.NfiEstaPg.TargetData.TDLDsityList(database: database),

                        // c_ldsity_object
                        new ZaJi.NfiEstaPg.TargetData.TDLDsityObjectList(database: database),

                        // c_ldsity_object_group
                        new ZaJi.NfiEstaPg.TargetData.TDLDsityObjectGroupList(database: database),

                        // c_ldsity_object_type
                        new ZaJi.NfiEstaPg.TargetData.TDLDsityObjectTypeList(database: database),

                        // c_panel_refyearset_group
                        new ZaJi.NfiEstaPg.TargetData.TDPanelRefYearSetGroupList(database: database),

                        // c_state_or_change
                        new ZaJi.NfiEstaPg.TargetData.TDStateOrChangeList(database: database),

                        // c_sub_population
                        new ZaJi.NfiEstaPg.TargetData.TDSubPopulationList(database: database),

                        // c_sub_population_category
                        new ZaJi.NfiEstaPg.TargetData.TDSubPopulationCategoryList(database: database),

                        // c_target_variable
                        new ZaJi.NfiEstaPg.TargetData.TDTargetVariableList(database: database),

                        // c_unit_of_measure
                        new ZaJi.NfiEstaPg.TargetData.TDUnitOfMeasureList(database: database),

                        // c_version
                        new ZaJi.NfiEstaPg.TargetData.TDVersionList(database: database),


                        // cm_adc2classification_rule
                        new ZaJi.NfiEstaPg.TargetData.TDADCToClassificationRuleList(database: database),

                        // cm_adc2classrule2panel_refyearset
                        new ZaJi.NfiEstaPg.TargetData.TDADCToClassRuleToPanelRefYearSetList(database: database),

                        // cm_ld_object2ld_object_group
                        new ZaJi.NfiEstaPg.TargetData.TDLDObjectToLDObjectGroupList(database: database),

                        // cm_ldsity2panel_refyearset_version
                        new ZaJi.NfiEstaPg.TargetData.TDLDsityToPanelRefYearSetVersionList(database: database),

                        // cm_ldsity2target2categorization_setup
                        new ZaJi.NfiEstaPg.TargetData.TDLDsityToTargetToCategorizationSetupList(database: database),

                        // cm_ldsity2target_variable
                        new ZaJi.NfiEstaPg.TargetData.TDLDsityToTargetVariableList(database: database),

                        // cm_spc2classification_rule
                        new ZaJi.NfiEstaPg.TargetData.TDSPCToClassificationRuleList(database: database),

                        // cm_spc2classrule2panel_refyearset
                        new ZaJi.NfiEstaPg.TargetData.TDSPCToClassRuleToPanelRefYearSetList(database: database),


                        // t_adc_hierarchy
                        new ZaJi.NfiEstaPg.TargetData.TDADCHierarchyList(database: database),

                        // t_available_datasets
                        new ZaJi.NfiEstaPg.TargetData.TDAvailableDataSetList(database: database),

                        // t_categorization_setup
                        new ZaJi.NfiEstaPg.TargetData.TDCategorizationSetupList(database: database),

                        // t_etl_area_domain
                        new ZaJi.NfiEstaPg.TargetData.TDEtlAreaDomainList(database: database),

                        // t_etl_area_domain_category
                        new ZaJi.NfiEstaPg.TargetData.TDEtlAreaDomainCategoryList(database: database),

                        // t_etl_log
                        new ZaJi.NfiEstaPg.TargetData.TDEtlLogList(database: database),

                        // t_etl_sub_population
                        new ZaJi.NfiEstaPg.TargetData.TDEtlSubPopulationList(database: database),

                        // t_etl_sub_population_category
                        new ZaJi.NfiEstaPg.TargetData.TDEtlSubPopulationCategoryList(database: database),

                        // t_etl_target_variable
                        new ZaJi.NfiEstaPg.TargetData.TDEtlTargetVariableList(database: database),

                        // t_export_connection
                        new ZaJi.NfiEstaPg.TargetData.TDExportConnectionList(database: database),

                        // t_ldsity_values
                        new ZaJi.NfiEstaPg.TargetData.TDLDsityValueList(database: database),

                        // t_panel_refyearset_group
                        new ZaJi.NfiEstaPg.TargetData.TDPanelRefYearSetPairList(database: database),

                        // t_spc_hierarchy
                        new ZaJi.NfiEstaPg.TargetData.TDSPCHierarchyList(database: database)
                    ];

                    foreach (ADatabaseTable databaseTable in databaseTables)
                    {
                        if ((tableNames == null) ||
                             tableNames.Contains(item: databaseTable.TableName))
                        {
                            if (databaseTable is ALookupTable lookupTable)
                            {
                                lookupTable.ReLoad(extended: false);
                            }
                            else if (databaseTable is AMappingTable mappingTable)
                            {
                                mappingTable.ReLoad();
                            }
                            else if (databaseTable is ASpatialTable spatialTable)
                            {
                                spatialTable.ReLoad(withGeom: false);
                            }
                            else if (databaseTable is ADataTable dataTable)
                            {
                                dataTable.ReLoad();
                            }
                            else
                            {
                                throw new Exception(message: $"Argument {nameof(databaseTable)} unknown type.");
                            }

                            databaseTable.ExportData(
                                fileFormat: fileFormat,
                                folder: folder);
                        }
                    }
                }

                #endregion Static Methods

            }

        }
    }
}
