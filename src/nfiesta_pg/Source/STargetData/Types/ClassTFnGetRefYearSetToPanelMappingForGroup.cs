﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {
            // fn_get_refyearset2panel_mapping4group

            /// <summary>
            /// Panel reference year set combination belonging to a particuliar group
            /// (return type of the stored procedure fn_get_refyearset2panel_mapping4group)
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class TFnGetRefYearSetToPanelMappingForGroup(
                TFnGetRefYearSetToPanelMappingForGroupList composite = null,
                DataRow data = null)
                    : AParametrizedViewEntry<TFnGetRefYearSetToPanelMappingForGroupList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TFnGetRefYearSetToPanelMappingForGroupList.ColId.Name,
                            defaultValue: Int32.Parse(s: TFnGetRefYearSetToPanelMappingForGroupList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TFnGetRefYearSetToPanelMappingForGroupList.ColId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Panel label
                /// </summary>
                public string PanelLabel
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnGetRefYearSetToPanelMappingForGroupList.ColPanelLabel.Name,
                            defaultValue: TFnGetRefYearSetToPanelMappingForGroupList.ColPanelLabel.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnGetRefYearSetToPanelMappingForGroupList.ColPanelLabel.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Panel description
                /// </summary>
                public string PanelDescription
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnGetRefYearSetToPanelMappingForGroupList.ColPanelDescription.Name,
                            defaultValue: TFnGetRefYearSetToPanelMappingForGroupList.ColPanelDescription.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnGetRefYearSetToPanelMappingForGroupList.ColPanelDescription.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Reference year set label
                /// </summary>
                public string RefYearSetLabel
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnGetRefYearSetToPanelMappingForGroupList.ColRefYearSetLabel.Name,
                            defaultValue: TFnGetRefYearSetToPanelMappingForGroupList.ColRefYearSetLabel.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnGetRefYearSetToPanelMappingForGroupList.ColRefYearSetLabel.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Reference year set description
                /// </summary>
                public string RefYearSetDescription
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TFnGetRefYearSetToPanelMappingForGroupList.ColRefYearSetDescription.Name,
                            defaultValue: TFnGetRefYearSetToPanelMappingForGroupList.ColRefYearSetDescription.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TFnGetRefYearSetToPanelMappingForGroupList.ColRefYearSetDescription.Name,
                            val: value);
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true|false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not TFnGetRefYearSetToPanelMappingForGroup Type, return False
                    if (obj is not TFnGetRefYearSetToPanelMappingForGroup)
                    {
                        return false;
                    }

                    return ((TFnGetRefYearSetToPanelMappingForGroup)obj).Id == Id;
                }

                /// <summary>
                /// Returns a hash code for the current object
                /// </summary>
                /// <returns>Hash code for the current object</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                /// <summary>
                /// Returns a string that represents the current object
                /// </summary>
                /// <returns>String that represents the current object</returns>
                public override string ToString()
                {
                    return String.Concat(
                        $"{nameof(TFnGetRefYearSetToPanelMappingForGroup)}: {{",
                        $"{nameof(Id)}: {Functions.PrepIntArg(arg: Id)}; ",
                        $"{nameof(PanelLabel)}: {Functions.PrepStringArg(arg: PanelLabel)}; ",
                        $"{nameof(RefYearSetLabel)}: {Functions.PrepStringArg(arg: RefYearSetLabel)}; ",
                        $"{nameof(PanelDescription)}: {Functions.PrepStringArg(arg: PanelDescription)}; ",
                        $"{nameof(RefYearSetDescription)}: {Functions.PrepStringArg(arg: RefYearSetDescription)}}}");
                }

                #endregion Methods

            }


            /// <summary>
            /// List of panel reference year set combinations belonging to a particuliar group
            /// (return type of the stored procedure fn_get_refyearset2panel_mapping4group)
            /// </summary>
            public class TFnGetRefYearSetToPanelMappingForGroupList
                 : AParametrizedView
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema =
                    TDFunctions.FnGetRefYearSetToPanelMappingForGroup.SchemaName;

                /// <summary>
                /// Stored procedure name
                /// </summary>
                public static readonly string Name =
                    TDFunctions.FnGetRefYearSetToPanelMappingForGroup.Name;

                /// <summary>
                /// Stored procedure alias
                /// </summary>
                public static readonly string Alias =
                    TDFunctions.FnGetRefYearSetToPanelMappingForGroup.Name;

                /// <summary>
                /// Description in national language
                /// </summary>
                public static readonly string CaptionCs = String.Concat(
                    "Seznam kombinací panelů a referenčích období patřících do jedné skupiny ",
                    "(návratový typ uložené procedury fn_get_refyearset2panel_mapping4group)");

                /// <summary>
                /// Description in English
                /// </summary>
                public static readonly string CaptionEn = String.Concat(
                    "List of panel reference year set combinations belonging to a particuliar group ",
                    "(return type of the stored procedure fn_get_refyearset2panel_mapping4group)");

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
                {
                    { "id", new ColumnMetadata()
                    {
                        Name = "id",
                        DbName = "refyearset2panel_mapping",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = true,
                        NotNull = true,
                        DefaultValue = "0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ID",
                        HeaderTextEn = "ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 0
                    }
                    },
                    { "panel_label", new ColumnMetadata()
                    {
                        Name = "panel_label",
                        DbName = "panel_label",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PANEL_LABEL",
                        HeaderTextEn = "PANEL_LABEL",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 1
                    }
                    },
                    { "panel_description", new ColumnMetadata()
                    {
                        Name = "panel_description",
                        DbName = "panel_description",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PANEL_DESCRIPTION",
                        HeaderTextEn = "PANEL_DESCRIPTION",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 2
                    }
                    },
                    { "refyearset_label", new ColumnMetadata()
                    {
                        Name = "refyearset_label",
                        DbName = "refyearset_label",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "REFYEARSET_LABEL",
                        HeaderTextEn = "REFYEARSET_LABEL",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 3
                    }
                    },
                    { "refyearset_description", new ColumnMetadata()
                    {
                        Name = "refyearset_description",
                        DbName = "refyearset_description",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "REFYEARSET_DESCRIPTION",
                        HeaderTextEn = "REFYEARSET_DESCRIPTION",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 4
                    }
                    }
                };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column panel_label metadata
                /// </summary>
                public static readonly ColumnMetadata ColPanelLabel = Cols["panel_label"];

                /// <summary>
                /// Column panel_description metadata
                /// </summary>
                public static readonly ColumnMetadata ColPanelDescription = Cols["panel_description"];

                /// <summary>
                /// Column refyearset_label metadata
                /// </summary>
                public static readonly ColumnMetadata ColRefYearSetLabel = Cols["refyearset_label"];

                /// <summary>
                /// Column refyearset_description metadata
                /// </summary>
                public static readonly ColumnMetadata ColRefYearSetDescription = Cols["refyearset_description"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Private Fields

                /// <summary>
                /// 1st parameter:
                /// Aggregated sets of panels and corresponding reference year sets identifier
                /// (id from table target_data.c_panel_refyearset_group)
                /// </summary>
                private Nullable<int> panelRefYearSetGroupId;

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TFnGetRefYearSetToPanelMappingForGroupList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                {
                    PanelRefYearSetGroupId = null;
                }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TFnGetRefYearSetToPanelMappingForGroupList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                {
                    PanelRefYearSetGroupId = null;
                }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// 1st parameter:
                /// Aggregated sets of panels and corresponding reference year sets identifier
                /// (id from table target_data.c_panel_refyearset_group)
                /// </summary>
                public Nullable<int> PanelRefYearSetGroupId
                {
                    get
                    {
                        return panelRefYearSetGroupId;
                    }
                    set
                    {
                        panelRefYearSetGroupId = value;
                    }
                }

                /// <summary>
                /// List of items (read-only)
                /// </summary>
                public List<TFnGetRefYearSetToPanelMappingForGroup> Items
                {
                    get
                    {
                        return
                            [..
                                Data.AsEnumerable()
                                .Select(a => new TFnGetRefYearSetToPanelMappingForGroup(composite: this, data: a))
                            ];
                    }
                }

                #endregion Properties


                #region Indexers

                /// <summary>
                /// Row from list of rows by identifier (read-only)
                /// </summary>
                /// <param name="id">Row identifier</param>
                /// <returns>Row from list of rows by identifier (null if not found)</returns>
                public TFnGetRefYearSetToPanelMappingForGroup this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new TFnGetRefYearSetToPanelMappingForGroup(composite: this, data: a))
                                .FirstOrDefault<TFnGetRefYearSetToPanelMappingForGroup>();
                    }
                }

                #endregion Indexers


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public TFnGetRefYearSetToPanelMappingForGroupList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any()
                        ? new TFnGetRefYearSetToPanelMappingForGroupList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime)
                        {
                            PanelRefYearSetGroupId = PanelRefYearSetGroupId
                        }
                        : new TFnGetRefYearSetToPanelMappingForGroupList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null)
                        {
                            PanelRefYearSetGroupId = PanelRefYearSetGroupId
                        };
                }

                #endregion Methods

            }

        }
    }
}