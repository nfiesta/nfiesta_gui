﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {
            // fn_get_panel_refyearset

            /// <summary>
            /// Record from sdesign.cm_refyearset2panel_mapping table
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class TDFnGetPanelRefYearSetType(
                TDFnGetPanelRefYearSetTypeList composite = null,
                DataRow data = null)
                    : AParametrizedViewEntry<TDFnGetPanelRefYearSetTypeList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Record from sdesign.cm_refyearset2panel_mapping table identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TDFnGetPanelRefYearSetTypeList.ColId.Name,
                            defaultValue: Int32.Parse(s: TDFnGetPanelRefYearSetTypeList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TDFnGetPanelRefYearSetTypeList.ColId.Name,
                            val: value);
                    }
                }


                /// <summary>
                ///  Sampling panel identifier
                /// </summary>
                public int PanelId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TDFnGetPanelRefYearSetTypeList.ColPanelId.Name,
                            defaultValue: Int32.Parse(s: TDFnGetPanelRefYearSetTypeList.ColPanelId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TDFnGetPanelRefYearSetTypeList.ColPanelId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Sampling panel code
                /// </summary>
                public string PanelCode
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnGetPanelRefYearSetTypeList.ColPanelCode.Name,
                            defaultValue: TDFnGetPanelRefYearSetTypeList.ColPanelCode.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnGetPanelRefYearSetTypeList.ColPanelCode.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Sampling panel label
                /// </summary>
                public string PanelLabel
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnGetPanelRefYearSetTypeList.ColPanelLabel.Name,
                            defaultValue: TDFnGetPanelRefYearSetTypeList.ColPanelLabel.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnGetPanelRefYearSetTypeList.ColPanelLabel.Name,
                            val: value);
                    }
                }


                /// <summary>
                ///  Reference year or season set identifier
                /// </summary>
                public int RefYearSetId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TDFnGetPanelRefYearSetTypeList.ColRefYearSetId.Name,
                            defaultValue: Int32.Parse(s: TDFnGetPanelRefYearSetTypeList.ColRefYearSetId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TDFnGetPanelRefYearSetTypeList.ColRefYearSetId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Reference year or season set code
                /// </summary>
                public string RefYearSetCode
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnGetPanelRefYearSetTypeList.ColRefYearSetCode.Name,
                            defaultValue: TDFnGetPanelRefYearSetTypeList.ColRefYearSetCode.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnGetPanelRefYearSetTypeList.ColRefYearSetCode.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Reference year or season set label
                /// </summary>
                public string RefYearSetLabel
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnGetPanelRefYearSetTypeList.ColRefYearSetLabel.Name,
                            defaultValue: TDFnGetPanelRefYearSetTypeList.ColRefYearSetLabel.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnGetPanelRefYearSetTypeList.ColRefYearSetLabel.Name,
                            val: value);
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not TDFnGetPanelRefYearSetType, return False
                    if (obj is not TDFnGetPanelRefYearSetType)
                    {
                        return false;
                    }

                    return
                        Id == ((TDFnGetPanelRefYearSetType)obj).Id;
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                /// <summary>
                /// Text description of record from sdesign.cm_refyearset2panel_mapping table
                /// </summary>
                /// <returns>Text description of record from sdesign.cm_refyearset2panel_mapping table</returns>
                public override string ToString()
                {
                    return ((NfiEstaDB)Composite.Database).Postgres.Setting.LanguageVersion switch
                    {
                        LanguageVersion.National =>
                            String.Concat(
                                $"{"ID"} = {Functions.PrepIntArg(arg: Id)}.{Environment.NewLine}"
                                ),
                        LanguageVersion.International =>
                            String.Concat(
                                $"{"ID"} = {Functions.PrepIntArg(arg: Id)}.{Environment.NewLine}"
                                ),
                        _ =>
                            String.Concat(
                                $"{"ID"} = {Functions.PrepIntArg(arg: Id)}.{Environment.NewLine}"
                                ),
                    };
                }

                #endregion Methods

            }


            /// <summary>
            /// List of records from sdesign.cm_refyearset2panel_mapping table
            /// </summary>
            public class TDFnGetPanelRefYearSetTypeList
                 : AParametrizedView
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema = TDSchema.Name;

                /// <summary>
                /// Stored procedure name
                /// </summary>
                public const string Name = "fn_get_panel_refyearset";

                /// <summary>
                /// Stored procedure alias
                /// </summary>
                public const string Alias = "fn_get_panel_refyearset";

                /// <summary>
                /// Description in national language
                /// </summary>
                public const string CaptionCs = "Seznam záznamů z mapovací tabulky sdesign.cm_refyearset2panel_mapping.";

                /// <summary>
                /// Description in English
                /// </summary>
                public const string CaptionEn = "List of records from sdesign.cm_refyearset2panel_mapping table.";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
                {
                    { "id", new ColumnMetadata()
                    {
                        Name = "id",
                        DbName = "id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = true,
                        NotNull = true,
                        DefaultValue = "0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ID",
                        HeaderTextEn = "ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 0
                    }
                    },
                    { "panel_id", new ColumnMetadata()
                    {
                        Name = "panel_id",
                        DbName = "panel_id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = "0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PANEL_ID",
                        HeaderTextEn = "PANEL_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 1
                    }
                    },
                    { "panel_code", new ColumnMetadata()
                    {
                        Name = "panel_code",
                        DbName = "panel",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PANEL_CODE",
                        HeaderTextEn = "PANEL_CODE",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 2
                    }
                    },
                    { "panel_label", new ColumnMetadata()
                    {
                        Name = "panel_label",
                        DbName = "panel_label",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PANEL_LABEL",
                        HeaderTextEn = "PANEL_LABEL",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 3
                    }
                    },
                    { "refyearset_id", new ColumnMetadata()
                    {
                        Name = "refyearset_id",
                        DbName = "refyearset_id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = "0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "REFYEARSET_ID",
                        HeaderTextEn = "REFYEARSET_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 4
                    }
                    },
                    { "refyearset_code", new ColumnMetadata()
                    {
                        Name = "refyearset_code",
                        DbName = "reference_year_set",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "REFYEARSET_CODE",
                        HeaderTextEn = "REFYEARSET_CODE",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 5
                    }
                    },
                    { "refyearset_label", new ColumnMetadata()
                    {
                        Name = "refyearset_label",
                        DbName = "refyearset_label",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "REFYEARSET_LABEL",
                        HeaderTextEn = "REFYEARSET_LABEL",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 6
                    }
                    }
                };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column panel_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColPanelId = Cols["panel_id"];

                /// <summary>
                /// Column panel_code metadata
                /// </summary>
                public static readonly ColumnMetadata ColPanelCode = Cols["panel_code"];

                /// <summary>
                /// Column panel_label metadata
                /// </summary>
                public static readonly ColumnMetadata ColPanelLabel = Cols["panel_label"];

                /// <summary>
                /// Column refyearset_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColRefYearSetId = Cols["refyearset_id"];

                /// <summary>
                /// Column refyearset_code metadata
                /// </summary>
                public static readonly ColumnMetadata ColRefYearSetCode = Cols["refyearset_code"];

                /// <summary>
                /// Column refyearset_label metadata
                /// </summary>
                public static readonly ColumnMetadata ColRefYearSetLabel = Cols["refyearset_label"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Private Fields

                /// <summary>
                /// 1st parameter: Panel-Reference year set mapping identifier
                /// </summary>
                private Nullable<int> panelRefYearSetId;

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TDFnGetPanelRefYearSetTypeList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                {
                    PanelRefYearSetId = null;
                }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TDFnGetPanelRefYearSetTypeList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                {
                    PanelRefYearSetId = null;
                }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// 1st parameter: Panel-Reference year set mapping identifier
                /// </summary>
                public Nullable<int> PanelRefYearSetId
                {
                    get
                    {
                        return panelRefYearSetId;
                    }
                    set
                    {
                        panelRefYearSetId = value;
                    }
                }

                /// <summary>
                /// List of records from sdesign.cm_refyearset2panel_mapping table (read-only)
                /// </summary>
                public List<TDFnGetPanelRefYearSetType> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new TDFnGetPanelRefYearSetType(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexers

                /// <summary>
                /// Record from sdesign.cm_refyearset2panel_mapping table from list by identifier (read-only)
                /// </summary>
                /// <param name="id">Record from sdesign.cm_refyearset2panel_mapping table identifier</param>
                /// <returns>Record from sdesign.cm_refyearset2panel_mapping table from list by identifier (null if not found)</returns>
                public TDFnGetPanelRefYearSetType this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new TDFnGetPanelRefYearSetType(composite: this, data: a))
                                .FirstOrDefault<TDFnGetPanelRefYearSetType>();
                    }
                }

                #endregion Indexers


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public TDFnGetPanelRefYearSetTypeList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new TDFnGetPanelRefYearSetTypeList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime)
                        {
                            PanelRefYearSetId = null
                        } :
                        new TDFnGetPanelRefYearSetTypeList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null)
                        {
                            PanelRefYearSetId = null
                        };
                }

                #endregion Methods

            }

        }
    }
}
