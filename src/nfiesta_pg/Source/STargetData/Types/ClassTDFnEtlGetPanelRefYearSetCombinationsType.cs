﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.Versioning;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {
            // fn_etl_get_panel_refyearset_combinations

            /// <summary>
            /// Combination of panels and refyearsets
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class TDFnEtlGetPanelRefYearSetCombinationsType(
                TDFnEtlGetPanelRefYearSetCombinationsTypeList composite = null,
                DataRow data = null)
                    : AParametrizedViewEntry<TDFnEtlGetPanelRefYearSetCombinationsTypeList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// RefYearSetToPanel
                /// </summary>
                public int RefYearSetToPanel
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TDFnEtlGetPanelRefYearSetCombinationsTypeList.ColRefYearSetToPanel.Name,
                            defaultValue: Int32.Parse(s: TDFnEtlGetPanelRefYearSetCombinationsTypeList.ColRefYearSetToPanel.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TDFnEtlGetPanelRefYearSetCombinationsTypeList.ColRefYearSetToPanel.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Panel
                /// </summary>
                public string Panel
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnEtlGetPanelRefYearSetCombinationsTypeList.ColPanel.Name,
                            defaultValue: TDFnEtlGetPanelRefYearSetCombinationsTypeList.ColPanel.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnEtlGetPanelRefYearSetCombinationsTypeList.ColPanel.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Reference year set
                /// </summary>
                public string ReferenceYearSet
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnEtlGetPanelRefYearSetCombinationsTypeList.ColReferenceYearSet.Name,
                            defaultValue: TDFnEtlGetPanelRefYearSetCombinationsTypeList.ColReferenceYearSet.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnEtlGetPanelRefYearSetCombinationsTypeList.ColReferenceYearSet.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Passed by module
                /// </summary>
                public Nullable<bool> PassedByModule
                {
                    get
                    {
                        return Functions.GetNBoolArg(
                            row: Data,
                            TDFnEtlGetPanelRefYearSetCombinationsTypeList.ColPassedByModule.Name,
                            String.IsNullOrEmpty(TDFnEtlGetPanelRefYearSetCombinationsTypeList.ColPassedByModule.DefaultValue) ? (Nullable<bool>)null :
                            Boolean.Parse(TDFnEtlGetPanelRefYearSetCombinationsTypeList.ColPassedByModule.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNBoolArg(
                            row: Data,
                            TDFnEtlGetPanelRefYearSetCombinationsTypeList.ColPassedByModule.Name, value);
                    }
                }

                /// <summary>
                /// Useable for ETL
                /// </summary>
                public Nullable<bool> UseableForETL
                {
                    get
                    {
                        return Functions.GetNBoolArg(
                            row: Data,
                            TDFnEtlGetPanelRefYearSetCombinationsTypeList.ColUseableForETL.Name,
                            String.IsNullOrEmpty(TDFnEtlGetPanelRefYearSetCombinationsTypeList.ColUseableForETL.DefaultValue) ? (Nullable<bool>)null :
                            Boolean.Parse(TDFnEtlGetPanelRefYearSetCombinationsTypeList.ColUseableForETL.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNBoolArg(
                            row: Data,
                            TDFnEtlGetPanelRefYearSetCombinationsTypeList.ColUseableForETL.Name, value);
                    }
                }

                /// <summary>
                /// Label (read-only)
                /// </summary>
                public string Label
                {
                    get
                    {
                        return
                           $"{Panel} / {ReferenceYearSet}";
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Specified object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not TDFnEtlGetPanelRefYearSetCombinationsType, return False
                    if (obj is not TDFnEtlGetPanelRefYearSetCombinationsType)
                    {
                        return false;
                    }

                    return
                        RefYearSetToPanel == ((TDFnEtlGetPanelRefYearSetCombinationsType)obj).RefYearSetToPanel;
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        RefYearSetToPanel.GetHashCode();
                }

                /// <summary>
                /// Text description of object of panel and refyearset combination
                /// </summary>
                /// <returns>Text description of object of panel and refyearset combination</returns>
                public override string ToString()
                {
                    return ((NfiEstaDB)Composite.Database).Postgres.Setting.LanguageVersion switch
                    {
                        LanguageVersion.National =>
                            String.Concat(
                                $"{"RefYearSetToPanel"} = {Functions.PrepIntArg(arg: RefYearSetToPanel)},{Environment.NewLine}",
                                $"{"Name"} = {Functions.PrepStringArg(arg: Label)}.{Environment.NewLine}"
                                ),
                        LanguageVersion.International =>
                            String.Concat(
                                $"{"RefYearSetToPanel"} = {Functions.PrepIntArg(arg: RefYearSetToPanel)},{Environment.NewLine}",
                                $"{"Name"} = {Functions.PrepStringArg(arg: Label)}.{Environment.NewLine}"
                                ),
                        _ =>
                            String.Concat(
                                $"{"RefYearSetToPanel"} = {Functions.PrepIntArg(arg: RefYearSetToPanel)},{Environment.NewLine}",
                                $"{"Name"} = {Functions.PrepStringArg(arg: Label)}.{Environment.NewLine}"
                                ),
                    };
                }

                #endregion Methods

            }


            /// <summary>
            /// List of panel and refyearset combinations
            /// </summary>
            public class TDFnEtlGetPanelRefYearSetCombinationsTypeList
                : AParametrizedView
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema = TDSchema.Name;

                /// <summary>
                /// Table name
                /// </summary>
                public const string Name = "fn_etl_get_panel_refyearset_combinations";

                /// <summary>
                /// Table alias
                /// </summary>
                public const string Alias = "fn_etl_get_panel_refyearset_combinations";

                /// <summary>
                /// Description in national language
                /// </summary>
                public const string CaptionCs = "Kombinace panelů a refyearsetů";

                /// <summary>
                /// Description in English
                /// </summary>
                public const string CaptionEn = "Panel and refyearset combination";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
                {
                    { "refyearset2panel", new ColumnMetadata()
                    {
                        Name = "refyearset2panel",
                        DbName = "refyearset2panel",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = "0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "REFYEARSETTOPANEL",
                        HeaderTextEn = "REFYEARSETTOPANEL",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 0
                    }
                    },
                    { "panel", new ColumnMetadata()
                    {
                        Name = "panel",
                        DbName = "panel",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "Panel",
                        HeaderTextEn = "Panel",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 1
                    }
                    },
                    { "reference_year_set", new ColumnMetadata()
                    {
                        Name = "reference_year_set",
                        DbName = "reference_year_set",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "Roky měření",
                        HeaderTextEn = "Reference-yearset",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 2
                    }
                    },
                    { "passed_by_module", new ColumnMetadata()
                    {
                        Name = "passed_by_module",
                        DbName = "passed_by_module",
                        DataType = "System.Boolean",
                        DbDataType = "boolean",
                        NewDataType = "boolean",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "Předáno modulem Terénní data",
                        HeaderTextEn = "Passed by the Field data module",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 3
                    }
                    },
                    { "useable4etl", new ColumnMetadata()
                    {
                        Name = "useable4etl",
                        DbName = "useable4etl",
                        DataType = "System.Boolean",
                        DbDataType = "boolean",
                        NewDataType = "boolean",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "USEABLE_FOR_ETL",
                        HeaderTextEn = "USEABLE_FOR_ETL",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 4
                    }
                    }
                };

                /// <summary>
                /// Column refyearset2panel metadata
                /// </summary>
                public static readonly ColumnMetadata ColRefYearSetToPanel = Cols["refyearset2panel"];

                /// <summary>
                /// Column panel metadata
                /// </summary>
                public static readonly ColumnMetadata ColPanel = Cols["panel"];

                /// <summary>
                /// Column refyearset metadata
                /// </summary>
                public static readonly ColumnMetadata ColReferenceYearSet = Cols["reference_year_set"];

                /// <summary>
                /// Column passed_by_module metadata
                /// </summary>
                public static readonly ColumnMetadata ColPassedByModule = Cols["passed_by_module"];

                /// <summary>
                /// Column useable4etl metadata
                /// </summary>
                public static readonly ColumnMetadata ColUseableForETL = Cols["useable4etl"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Private Fields

                /// <summary>
                /// First parameter - Id of the selected target variable
                /// </summary>
                private Nullable<int> idTEtlTargetVariable;

                /// <summary>
                /// Second parameter - Ids of the panel and refyearset combinations
                /// </summary>
                private List<Nullable<int>> refYearSetToPanelMapping;

                /// <summary>
                /// Third parameter - Ids of combinations of panels and reference-year sets
                /// - whether they come directly from the module for configuration of ldsity values
                /// </summary>
                private List<Nullable<int>> refYearSetToPanelMappingModule;

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TDFnEtlGetPanelRefYearSetCombinationsTypeList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                {
                    IdTEtlTargetVariable = null;
                    RefYearSetToPanelMapping = [];
                    RefYearSetToPanelMappingModule = [];
                }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TDFnEtlGetPanelRefYearSetCombinationsTypeList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                {
                    IdTEtlTargetVariable = null;
                    RefYearSetToPanelMapping = [];
                    RefYearSetToPanelMappingModule = [];
                }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// First parameter - Id of the selected target variable
                /// </summary>
                public Nullable<int> IdTEtlTargetVariable
                {
                    get
                    {
                        return idTEtlTargetVariable;
                    }
                    set
                    {
                        idTEtlTargetVariable = value;
                    }
                }

                /// <summary>
                /// Second parameter - Ids of the panel and refyearset combinations
                /// </summary>
                public List<Nullable<int>> RefYearSetToPanelMapping
                {
                    get
                    {
                        return refYearSetToPanelMapping ?? [];
                    }
                    set
                    {
                        refYearSetToPanelMapping = value ?? [];
                    }
                }

                /// <summary>
                /// Third parameter - Ids of combinations of panels and reference-year sets
                /// - whether they come directly from the module for configuration of ldsity values
                /// </summary>
                public List<Nullable<int>> RefYearSetToPanelMappingModule
                {
                    get
                    {
                        return refYearSetToPanelMappingModule ?? [];
                    }
                    set
                    {
                        refYearSetToPanelMappingModule = value ?? [];
                    }
                }

                /// <summary>
                /// List of panel and refyearset combinations (read-only)
                /// </summary>
                public List<TDFnEtlGetPanelRefYearSetCombinationsType> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new TDFnEtlGetPanelRefYearSetCombinationsType(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexers

                /// <summary>
                /// Combination of panels and refyearsets from list by identifier (read-only)
                /// </summary>
                /// <param name="refYearSetToPanelId">Identifier of panel and refyearset combination</param>
                /// <returns>Combination of panels and refyearsets from list by identifier (null if not found)</returns>
                public TDFnEtlGetPanelRefYearSetCombinationsType this[int refYearSetToPanelId]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColRefYearSetToPanel.Name) == refYearSetToPanelId)
                                .Select(a => new TDFnEtlGetPanelRefYearSetCombinationsType(composite: this, data: a))
                                .FirstOrDefault<TDFnEtlGetPanelRefYearSetCombinationsType>();
                    }
                }

                #endregion Indexers


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public TDFnEtlGetPanelRefYearSetCombinationsTypeList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new TDFnEtlGetPanelRefYearSetCombinationsTypeList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime)
                        {
                            IdTEtlTargetVariable = IdTEtlTargetVariable,
                            RefYearSetToPanelMapping = [.. RefYearSetToPanelMapping],
                            RefYearSetToPanelMappingModule = [.. RefYearSetToPanelMappingModule]
                        } :
                        new TDFnEtlGetPanelRefYearSetCombinationsTypeList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null)
                        {
                            IdTEtlTargetVariable = IdTEtlTargetVariable,
                            RefYearSetToPanelMapping = [.. RefYearSetToPanelMapping],
                            RefYearSetToPanelMappingModule = [.. RefYearSetToPanelMappingModule]
                        };
                }

                /// <summary>
                /// Sets header, visibility, formats by data type and alignment in columns of given DataGridView
                /// </summary>
                /// <param name="dataGridView">Object DataGridView</param>
                [SupportedOSPlatform("windows")]
                public new void FormatDataGridView(System.Windows.Forms.DataGridView dataGridView)
                {
                    // General:
                    dataGridView.AllowUserToAddRows = false;
                    dataGridView.AllowUserToDeleteRows = false;
                    dataGridView.AllowUserToResizeRows = false;

                    dataGridView.AlternatingRowsDefaultCellStyle = new System.Windows.Forms.DataGridViewCellStyle()
                    {
                        BackColor = Color.LightCyan
                    };

                    dataGridView.AutoGenerateColumns = false;
                    dataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.None;
                    dataGridView.BackgroundColor = SystemColors.Window;
                    dataGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
                    dataGridView.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
                    dataGridView.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;

                    dataGridView.ColumnHeadersDefaultCellStyle = new System.Windows.Forms.DataGridViewCellStyle()
                    {
                        Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft,
                        BackColor = Color.DarkBlue,
                        Font = new Font("Microsoft Sans Serif", 9.75F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(238))),
                        ForeColor = Color.White,
                        SelectionBackColor = Color.DarkBlue,
                        SelectionForeColor = Color.White,
                        WrapMode = System.Windows.Forms.DataGridViewTriState.True
                    };

                    dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;

                    dataGridView.DefaultCellStyle = new System.Windows.Forms.DataGridViewCellStyle()
                    {
                        Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft,
                        BackColor = Color.White,
                        Font = new Font("Microsoft Sans Serif", 9.75F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(238))),
                        ForeColor = SystemColors.ControlText,
                        Format = "N3",
                        NullValue = "NULL",
                        SelectionBackColor = Color.Bisque,
                        SelectionForeColor = SystemColors.ControlText,
                        WrapMode = System.Windows.Forms.DataGridViewTriState.False
                    };

                    dataGridView.EnableHeadersVisualStyles = false;
                    dataGridView.MultiSelect = true;
                    dataGridView.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
                    dataGridView.RowHeadersVisible = false;
                    dataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;

                    // Columns:
                    foreach (ColumnMetadata column in Cols.Values.OrderBy(a => a.DisplayIndex))
                    {
                        if (dataGridView.Columns.Contains(column.Name))
                        {
                            dataGridView.Columns[column.Name].ReadOnly = column.ReadOnly;
                            dataGridView.Columns[column.Name].Visible = column.Visible;
                            dataGridView.Columns[column.Name].HeaderText = (Database.Postgres.Setting.LanguageVersion == LanguageVersion.National) ?
                                (String.IsNullOrEmpty(column.HeaderTextCs) ? column.Name : column.HeaderTextCs) :
                                (String.IsNullOrEmpty(column.HeaderTextEn) ? column.Name : column.HeaderTextEn);
                            dataGridView.Columns[column.Name].ToolTipText = (Database.Postgres.Setting.LanguageVersion == LanguageVersion.National) ?
                                (String.IsNullOrEmpty(column.ToolTipTextCs) ? dataGridView.Columns[column.Name].ToolTipText : column.ToolTipTextCs) :
                                (String.IsNullOrEmpty(column.ToolTipTextEn) ? dataGridView.Columns[column.Name].ToolTipText : column.ToolTipTextEn);
                            dataGridView.Columns[column.Name].DefaultCellStyle.Format =
                                (String.IsNullOrEmpty(column.NumericFormat) ? dataGridView.Columns[column.Name].DefaultCellStyle.Format : column.NumericFormat);
                            dataGridView.Columns[column.Name].DefaultCellStyle.Alignment = (System.Windows.Forms.DataGridViewContentAlignment)(int)column.Alignment;
                            dataGridView.Columns[column.Name].DisplayIndex = column.DisplayIndex;
                        }
                    }
                }

                #endregion Methods

            }
        }
    }
}

