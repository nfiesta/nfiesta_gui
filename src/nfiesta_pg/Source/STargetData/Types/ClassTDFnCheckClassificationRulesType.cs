﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {
            // fn_check_classification_rules

            /// <summary>
            /// Classification rule check result
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class TDFnCheckClassificationRulesType(
                TDFnCheckClassificationRulesTypeList composite = null,
                DataRow data = null)
                    : AParametrizedViewEntry<TDFnCheckClassificationRulesTypeList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Classification rule check result identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TDFnCheckClassificationRulesTypeList.ColId.Name,
                            defaultValue: Int32.Parse(s: TDFnCheckClassificationRulesTypeList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TDFnCheckClassificationRulesTypeList.ColId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Panel reference year set identifier
                /// </summary>
                public Nullable<int> PanelRefYearSetId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TDFnCheckClassificationRulesTypeList.ColPanelRefYearSetId.Name,
                            defaultValue:
                                String.IsNullOrEmpty(value: TDFnCheckClassificationRulesTypeList.ColPanelRefYearSetId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TDFnCheckClassificationRulesTypeList.ColPanelRefYearSetId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TDFnCheckClassificationRulesTypeList.ColPanelRefYearSetId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Classification rule check result
                /// </summary>
                public Nullable<bool> Result
                {
                    get
                    {
                        return Functions.GetNBoolArg(
                            row: Data,
                            name: TDFnCheckClassificationRulesTypeList.ColResult.Name,
                            defaultValue:
                                String.IsNullOrEmpty(value: TDFnCheckClassificationRulesTypeList.ColResult.DefaultValue) ?
                                (Nullable<bool>)null :
                                Boolean.Parse(value: TDFnCheckClassificationRulesTypeList.ColResult.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNBoolArg(
                            row: Data,
                            name: TDFnCheckClassificationRulesTypeList.ColResult.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Message identifier
                /// </summary>
                public Nullable<int> MessageId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TDFnCheckClassificationRulesTypeList.ColMessageId.Name,
                            defaultValue:
                                String.IsNullOrEmpty(TDFnCheckClassificationRulesTypeList.ColMessageId.DefaultValue) ?
                                    (Nullable<int>)null :
                                    Int32.Parse(s: TDFnCheckClassificationRulesTypeList.ColMessageId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TDFnCheckClassificationRulesTypeList.ColMessageId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Message
                /// </summary>
                public string Message
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnCheckClassificationRulesTypeList.ColMessage.Name,
                            defaultValue: TDFnCheckClassificationRulesTypeList.ColMessage.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnCheckClassificationRulesTypeList.ColMessage.Name,
                            val: value);
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not TDFnCheckClassificationRulesType, return False
                    if (obj is not TDFnCheckClassificationRulesType)
                    {
                        return false;
                    }

                    return
                        Id == ((TDFnCheckClassificationRulesType)obj).Id;
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                /// <summary>
                /// Text description of classification rule check result
                /// </summary>
                /// <returns>Text description of classification rule check result</returns>
                public override string ToString()
                {
                    return ((NfiEstaDB)Composite.Database).Postgres.Setting.LanguageVersion switch
                    {
                        LanguageVersion.National =>
                            String.Concat(
                                $"{"ID"} = {Functions.PrepIntArg(arg: Id)}.{Environment.NewLine}"
                                ),
                        LanguageVersion.International =>
                            String.Concat(
                                $"{"ID"} = {Functions.PrepIntArg(arg: Id)}.{Environment.NewLine}"
                                ),
                        _ => String.Concat(
                                $"{"ID"} = {Functions.PrepIntArg(arg: Id)}.{Environment.NewLine}"
                                ),
                    };
                }

                #endregion Methods

            }


            /// <summary>
            /// List of classification rule check results
            /// </summary>
            public class TDFnCheckClassificationRulesTypeList
                : AParametrizedView
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema = TDSchema.Name;

                /// <summary>
                /// Stored procedure name
                /// </summary>
                public const string Name = "fn_check_classification_rules";

                /// <summary>
                /// Stored procedure alias
                /// </summary>
                public const string Alias = "fn_check_classification_rules";

                /// <summary>
                /// Description in national language
                /// </summary>
                public const string CaptionCs = "Seznam výsledků kontroly klasifikačních pravidel.";

                /// <summary>
                /// Description in English
                /// </summary>
                public const string CaptionEn = "List of classification rule check results.";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
                {
                    { "id", new ColumnMetadata()
                    {
                        Name = "id",
                        DbName = "id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = true,
                        NotNull = true,
                        DefaultValue = "0",
                        FuncCall = "row_number() over()",
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ID",
                        HeaderTextEn = "ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 0
                    }
                    },
                    { "panel_refyearset_id", new ColumnMetadata()
                    {
                        Name = "panel_refyearset_id",
                        DbName = "panel_refyearset_id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PANEL_REFYEARSET_ID",
                        HeaderTextEn = "PANEL_REFYEARSET_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 1
                    }
                    },
                    { "result", new ColumnMetadata()
                    {
                        Name = "result",
                        DbName = "result",
                        DataType = "System.Boolean",
                        DbDataType = "bool",
                        NewDataType = "bool",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "RESULT",
                        HeaderTextEn = "RESULT",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 2
                    }
                    },
                    { "message_id", new ColumnMetadata()
                    {
                        Name = "message_id",
                        DbName = "message_id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "MESSAGE_ID",
                        HeaderTextEn = "MESSAGE_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 3
                    }
                    },
                    { "message", new ColumnMetadata()
                    {
                        Name = "message",
                        DbName = "message",
                        DataType = "System.String",
                        DbDataType = "text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "MESSAGE",
                        HeaderTextEn = "MESSAGE",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 4
                    }
                    }
                };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column panel_refyearset_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColPanelRefYearSetId = Cols["panel_refyearset_id"];

                /// <summary>
                /// Column result metadata
                /// </summary>
                public static readonly ColumnMetadata ColResult = Cols["result"];

                /// <summary>
                /// Column message_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColMessageId = Cols["message_id"];

                /// <summary>
                /// Column message metadata
                /// </summary>
                public static readonly ColumnMetadata ColMessage = Cols["message"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Private Fields

                /// <summary>
                /// 1st parameter: Local density contribution identifier
                /// </summary>
                private Nullable<int> ldsityId;

                /// <summary>
                /// 2nd parameter: Local density object identifier
                /// </summary>
                private Nullable<int> ldsityObjectId;

                /// <summary>
                /// 3rd parameter: List of the classification rule texts
                /// </summary>
                private List<string> rules;

                /// <summary>
                /// 4th parameter: Pair of panel with corresponding reference year set identifier
                /// </summary>
                private Nullable<int> panelRefYearSetId;

                /// <summary>
                /// 5th parameter: Area domain categories
                /// </summary>
                private List<List<Nullable<int>>> adc;

                /// <summary>
                /// 6th parameter: Subpopulation categories
                /// </summary>
                private List<List<Nullable<int>>> spc;

                /// <summary>
                /// 7th parameter: Positive (false) or negative (true) classification rule
                /// </summary>
                public Nullable<bool> useNegative;

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TDFnCheckClassificationRulesTypeList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                {
                    LDsityId = null;
                    LDsityObjectId = null;
                    Rules = [];
                    PanelRefYearSetId = null;
                    Adc = [];
                    Spc = [];
                    UseNegative = null;
                }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TDFnCheckClassificationRulesTypeList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                {
                    LDsityId = null;
                    LDsityObjectId = null;
                    Rules = [];
                    PanelRefYearSetId = null;
                    Adc = [];
                    Spc = [];
                    UseNegative = null;
                }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// 1st parameter: Local density contribution identifier
                /// </summary>
                public Nullable<int> LDsityId
                {
                    get
                    {
                        return ldsityId;
                    }
                    set
                    {
                        ldsityId = value;
                    }
                }

                /// <summary>
                /// 2nd parameter: Local density object identifier
                /// </summary>
                public Nullable<int> LDsityObjectId
                {
                    get
                    {
                        return ldsityObjectId;
                    }
                    set
                    {
                        ldsityObjectId = value;
                    }
                }

                /// <summary>
                /// 3rd parameter: List of the classification rule texts
                /// </summary>
                public List<string> Rules
                {
                    get
                    {
                        return rules;
                    }
                    set
                    {
                        rules = value;
                    }
                }

                /// <summary>
                /// 4th parameter: Pair of panel with corresponding reference year set identifier
                /// </summary>
                public Nullable<int> PanelRefYearSetId
                {
                    get
                    {
                        return panelRefYearSetId;
                    }
                    set
                    {
                        panelRefYearSetId = value;
                    }
                }

                /// <summary>
                /// 5th parameter: Area domain categories
                /// </summary>
                public List<List<Nullable<int>>> Adc
                {
                    get
                    {
                        return adc;
                    }
                    set
                    {
                        adc = value;
                    }
                }

                /// <summary>
                /// 6th parameter: Subpopulation categories
                /// </summary>
                public List<List<Nullable<int>>> Spc
                {
                    get
                    {
                        return spc;
                    }
                    set
                    {
                        spc = value;
                    }
                }

                /// <summary>
                /// 7th parameter: Positive (false) or negative (true) classification rule
                /// </summary>
                public Nullable<bool> UseNegative
                {
                    get
                    {
                        return useNegative;
                    }
                    set
                    {
                        useNegative = value;
                    }
                }


                /// <summary>
                /// List of classification rule check results (read-only)
                /// </summary>
                public List<TDFnCheckClassificationRulesType> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new TDFnCheckClassificationRulesType(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexers

                /// <summary>
                /// Classification rule check result from list by identifier (read-only)
                /// </summary>
                /// <param name="id">Classification rule check results identifier</param>
                /// <returns>Classification rule check results from list by identifier (null if not found)</returns>
                public TDFnCheckClassificationRulesType this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new TDFnCheckClassificationRulesType(composite: this, data: a))
                                .FirstOrDefault<TDFnCheckClassificationRulesType>();
                    }
                }

                #endregion Indexers


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public TDFnCheckClassificationRulesTypeList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new TDFnCheckClassificationRulesTypeList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime)
                        {
                            LDsityId = LDsityId,
                            LDsityObjectId = LDsityObjectId,
                            Rules = [.. Rules],
                            PanelRefYearSetId = PanelRefYearSetId,
                            Adc = [.. Adc],
                            Spc = [.. Spc],
                            UseNegative = UseNegative
                        } :
                        new TDFnCheckClassificationRulesTypeList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null)
                        {
                            LDsityId = LDsityId,
                            LDsityObjectId = LDsityObjectId,
                            Rules = [.. Rules],
                            PanelRefYearSetId = PanelRefYearSetId,
                            Adc = [.. Adc],
                            Spc = [.. Spc],
                            UseNegative = UseNegative
                        };
                }

                #endregion Methods

            }

        }
    }
}
