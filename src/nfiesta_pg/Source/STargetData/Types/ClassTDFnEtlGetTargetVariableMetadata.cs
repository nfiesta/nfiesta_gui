﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Versioning;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {
            // fn_etl_get_target_variable_metadata

            /// <summary>
            /// Target variable for ETL with metadata
            /// (in the TargetData schema)
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class TDFnEtlGetTargetVariableMetadata(
                TDFnEtlGetTargetVariableMetadataList composite = null,
                DataRow data = null)
                    : ALinqViewEntry<TDFnEtlGetTargetVariableMetadataList>(composite: composite, data: data),
                      ITargetVariableMetadata<TDFnEtlGetTargetVariableMetadataList>
            {

                #region Properties

                /// <summary>
                /// List of metadata elements
                /// </summary>
                public MetadataElementList MetadataElements
                {
                    get
                    {
                        return MetadataElementList.Standard(
                            metadata: this);
                    }
                }

                #endregion Properties


                #region Derived Properties

                /// <summary>
                /// Identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColId.Name,
                            defaultValue: Int32.Parse(s: TDFnEtlGetTargetVariableMetadataList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Target variable identifier
                /// </summary>
                public int TargetVariableId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColTargetVariableId.Name,
                            defaultValue: Int32.Parse(s: TDFnEtlGetTargetVariableMetadataList.ColTargetVariableId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColTargetVariableId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Generated local density identifier
                /// </summary>
                public int LocalDensityId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColLocalDensityId.Name,
                            defaultValue: Int32.Parse(s: TDFnEtlGetTargetVariableMetadataList.ColLocalDensityId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColLocalDensityId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// JsonValidity
                /// </summary>
                public bool JsonValidity
                {
                    get
                    {
                        return Functions.GetBoolArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColJsonValidity.Name,
                            defaultValue: Boolean.Parse(value: TDFnEtlGetTargetVariableMetadataList.ColJsonValidity.DefaultValue));
                    }
                    set
                    {
                        Functions.SetBoolArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColJsonValidity.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Label in national language
                /// </summary>
                public string LabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColLabelCs.Name,
                            defaultValue: TDFnEtlGetTargetVariableMetadataList.ColLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Description in national language
                /// </summary>
                public string DescriptionCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColDescriptionCs.Name,
                            defaultValue: TDFnEtlGetTargetVariableMetadataList.ColDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Extended label in national language (read-only)
                /// </summary>
                public string ExtendedLabelCs
                {
                    get
                    {
                        return $"{Id} - {LabelCs}";
                    }
                }

                /// <summary>
                /// Label in international language
                /// </summary>
                public string LabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColLabelEn.Name,
                            defaultValue: TDFnEtlGetTargetVariableMetadataList.ColLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Description in international language
                /// </summary>
                public string DescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColDescriptionEn.Name,
                            defaultValue: TDFnEtlGetTargetVariableMetadataList.ColDescriptionEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColDescriptionEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Extended label in international language (read-only)
                /// </summary>
                public string ExtendedLabelEn
                {
                    get
                    {
                        return $"{Id} - {LabelEn}";
                    }
                }

                /// <summary>
                /// IdEtlTargetVariableMetadata
                /// </summary>
                public Nullable<int> IdEtlTargetVariableMetadata
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColIdEtlTargetVariableMetadata.Name,
                            defaultValue: String.IsNullOrEmpty(value: TDFnEtlGetTargetVariableMetadataList.ColIdEtlTargetVariableMetadata.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TDFnEtlGetTargetVariableMetadataList.ColIdEtlTargetVariableMetadata.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColIdEtlTargetVariableMetadata.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// CheckTargetVariable
                /// </summary>
                public Nullable<bool> CheckTargetVariable
                {
                    get
                    {
                        return Functions.GetNBoolArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColCheckTargetVariable.Name,
                            defaultValue: String.IsNullOrEmpty(value: TDFnEtlGetTargetVariableMetadataList.ColCheckTargetVariable.DefaultValue) ?
                                (Nullable<bool>)null :
                                Boolean.Parse(value: TDFnEtlGetTargetVariableMetadataList.ColCheckTargetVariable.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNBoolArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColCheckTargetVariable.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// RefYearSetToPanelMapping
                /// </summary>
                public string RefYearSetToPanelMappingString
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColRefYearSetToPanelMapping.Name,
                            defaultValue: TDFnEtlGetTargetVariableMetadataList.ColRefYearSetToPanelMapping.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColRefYearSetToPanelMapping.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// CheckAtomicAreaDomains
                /// </summary>
                public Nullable<bool> CheckAtomicAreaDomains
                {
                    get
                    {
                        return Functions.GetNBoolArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColCheckAtomicAreaDomains.Name,
                            defaultValue: String.IsNullOrEmpty(value: TDFnEtlGetTargetVariableMetadataList.ColCheckAtomicAreaDomains.DefaultValue) ?
                                (Nullable<bool>)null :
                                Boolean.Parse(value: TDFnEtlGetTargetVariableMetadataList.ColCheckAtomicAreaDomains.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNBoolArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColCheckAtomicAreaDomains.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// CheckAtomicSubPopulations
                /// </summary>
                public Nullable<bool> CheckAtomicSubPopulations
                {
                    get
                    {
                        return Functions.GetNBoolArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColCheckAtomicSubPopulations.Name,
                            defaultValue: String.IsNullOrEmpty(value: TDFnEtlGetTargetVariableMetadataList.ColCheckAtomicSubPopulations.DefaultValue) ?
                                (Nullable<bool>)null :
                                Boolean.Parse(value: TDFnEtlGetTargetVariableMetadataList.ColCheckAtomicSubPopulations.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNBoolArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColCheckAtomicSubPopulations.Name,
                            val: value);
                    }
                }


                #region Indicator

                /// <summary>
                /// Indicator label in national language
                /// </summary>
                public string IndicatorLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColIndicatorLabelCs.Name,
                            defaultValue: TDFnEtlGetTargetVariableMetadataList.ColIndicatorLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColIndicatorLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Indicator description in national language
                /// </summary>
                public string IndicatorDescriptionCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColIndicatorDescriptionCs.Name,
                            defaultValue: TDFnEtlGetTargetVariableMetadataList.ColIndicatorDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColIndicatorDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Indicator label in English
                /// </summary>
                public string IndicatorLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColIndicatorLabelEn.Name,
                            defaultValue: TDFnEtlGetTargetVariableMetadataList.ColIndicatorLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColIndicatorLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Indicator description in English
                /// </summary>
                public string IndicatorDescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColIndicatorDescriptionEn.Name,
                            defaultValue: TDFnEtlGetTargetVariableMetadataList.ColIndicatorDescriptionEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColIndicatorDescriptionEn.Name,
                            val: value);
                    }
                }

                #endregion Indicator


                #region StateOrChange

                /// <summary>
                /// State or change label in national language
                /// </summary>
                public string StateOrChangeLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColStateOrChangeLabelCs.Name,
                            defaultValue: TDFnEtlGetTargetVariableMetadataList.ColStateOrChangeLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColStateOrChangeLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// State or change description in national language
                /// </summary>
                public string StateOrChangeDescriptionCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColStateOrChangeDescriptionCs.Name,
                            defaultValue: TDFnEtlGetTargetVariableMetadataList.ColStateOrChangeDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColStateOrChangeDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// State or change label in English
                /// </summary>
                public string StateOrChangeLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColStateOrChangeLabelEn.Name,
                            defaultValue: TDFnEtlGetTargetVariableMetadataList.ColStateOrChangeLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColStateOrChangeLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// State or change description in English
                /// </summary>
                public string StateOrChangeDescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColStateOrChangeDescriptionEn.Name,
                            defaultValue: TDFnEtlGetTargetVariableMetadataList.ColStateOrChangeDescriptionEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColStateOrChangeDescriptionEn.Name,
                            val: value);
                    }
                }

                #endregion StateOrChange


                #region Unit

                /// <summary>
                /// Unit label in national language
                /// </summary>
                public string UnitLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColUnitLabelCs.Name,
                            defaultValue: TDFnEtlGetTargetVariableMetadataList.ColUnitLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColUnitLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Unit description in national language
                /// </summary>
                public string UnitDescriptionCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColUnitDescriptionCs.Name,
                            defaultValue: TDFnEtlGetTargetVariableMetadataList.ColUnitDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColUnitDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Unit label in English
                /// </summary>
                public string UnitLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColUnitLabelEn.Name,
                            defaultValue: TDFnEtlGetTargetVariableMetadataList.ColUnitLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColUnitLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Unit description in English
                /// </summary>
                public string UnitDescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColUnitDescriptionEn.Name,
                            defaultValue: TDFnEtlGetTargetVariableMetadataList.ColUnitDescriptionEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColUnitDescriptionEn.Name,
                            val: value);
                    }
                }

                #endregion Unit


                #region LocalDensityObjectType

                /// <summary>
                /// Local density object type label in national language
                /// </summary>
                public string LocalDensityObjectTypeLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColLocalDensityObjectTypeLabelCs.Name,
                            defaultValue: TDFnEtlGetTargetVariableMetadataList.ColLocalDensityObjectTypeLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColLocalDensityObjectTypeLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Local density object type description in national language
                /// </summary>
                public string LocalDensityObjectTypeDescriptionCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColLocalDensityObjectTypeDescriptionCs.Name,
                            defaultValue: TDFnEtlGetTargetVariableMetadataList.ColLocalDensityObjectTypeDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColLocalDensityObjectTypeDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Local density object type label in English
                /// </summary>
                public string LocalDensityObjectTypeLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColLocalDensityObjectTypeLabelEn.Name,
                            defaultValue: TDFnEtlGetTargetVariableMetadataList.ColLocalDensityObjectTypeLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColLocalDensityObjectTypeLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Local density object type description in English
                /// </summary>
                public string LocalDensityObjectTypeDescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColLocalDensityObjectTypeDescriptionEn.Name,
                            defaultValue: TDFnEtlGetTargetVariableMetadataList.ColLocalDensityObjectTypeDescriptionEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColLocalDensityObjectTypeDescriptionEn.Name,
                            val: value);
                    }
                }

                #endregion LocalDensityObjectType


                #region LocalDensityObject

                /// <summary>
                /// Local density object label in national language
                /// </summary>
                public string LocalDensityObjectLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColLocalDensityObjectLabelCs.Name,
                            defaultValue: TDFnEtlGetTargetVariableMetadataList.ColLocalDensityObjectLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColLocalDensityObjectLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Local density object description in national language
                /// </summary>
                public string LocalDensityObjectDescriptionCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColLocalDensityObjectDescriptionCs.Name,
                            defaultValue: TDFnEtlGetTargetVariableMetadataList.ColLocalDensityObjectDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColLocalDensityObjectDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Local density object label in English
                /// </summary>
                public string LocalDensityObjectLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColLocalDensityObjectLabelEn.Name,
                            defaultValue: TDFnEtlGetTargetVariableMetadataList.ColLocalDensityObjectLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColLocalDensityObjectLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Local density object description in English
                /// </summary>
                public string LocalDensityObjectDescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColLocalDensityObjectDescriptionEn.Name,
                            defaultValue: TDFnEtlGetTargetVariableMetadataList.ColLocalDensityObjectDescriptionEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColLocalDensityObjectDescriptionEn.Name,
                            val: value);
                    }
                }

                #endregion LocalDensityObject


                #region LocalDensity

                /// <summary>
                /// Local density contribution label in national language
                /// </summary>
                public string LocalDensityLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColLocalDensityLabelCs.Name,
                            defaultValue: TDFnEtlGetTargetVariableMetadataList.ColLocalDensityLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColLocalDensityLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Local density contribution description in national language
                /// </summary>
                public string LocalDensityDescriptionCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColLocalDensityDescriptionCs.Name,
                            defaultValue: TDFnEtlGetTargetVariableMetadataList.ColLocalDensityDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColLocalDensityDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Local density contribution label in English
                /// </summary>
                public string LocalDensityLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColLocalDensityLabelEn.Name,
                            defaultValue: TDFnEtlGetTargetVariableMetadataList.ColLocalDensityLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColLocalDensityLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Local density contribution description in English
                /// </summary>
                public string LocalDensityDescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColLocalDensityDescriptionEn.Name,
                            defaultValue: TDFnEtlGetTargetVariableMetadataList.ColLocalDensityDescriptionEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColLocalDensityDescriptionEn.Name,
                            val: value);
                    }
                }

                #endregion LocalDensity


                #region UseNegative

                /// <summary>
                /// Use negative value in national section of JSON file
                /// </summary>
                public Nullable<bool> UseNegativeCs
                {
                    get
                    {
                        return Functions.GetNBoolArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColUseNegativeCs.Name,
                            defaultValue: String.IsNullOrEmpty(value: TDFnEtlGetTargetVariableMetadataList.ColUseNegativeCs.DefaultValue) ?
                                (Nullable<bool>)null :
                                Boolean.Parse(value: TDFnEtlGetTargetVariableMetadataList.ColUseNegativeCs.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNBoolArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColUseNegativeCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Use negative value in English section of JSON file
                /// </summary>
                public Nullable<bool> UseNegativeEn
                {
                    get
                    {
                        return Functions.GetNBoolArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColUseNegativeEn.Name,
                            defaultValue: String.IsNullOrEmpty(value: TDFnEtlGetTargetVariableMetadataList.ColUseNegativeEn.DefaultValue) ?
                                (Nullable<bool>)null :
                                Boolean.Parse(value: TDFnEtlGetTargetVariableMetadataList.ColUseNegativeEn.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNBoolArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColUseNegativeEn.Name,
                            val: value);
                    }
                }

                #endregion UseNegative


                #region Version

                /// <summary>
                /// Version label in national language
                /// </summary>
                public string VersionLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColVersionLabelCs.Name,
                            defaultValue: TDFnEtlGetTargetVariableMetadataList.ColVersionLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColVersionLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Version description in national language
                /// </summary>
                public string VersionDescriptionCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColVersionDescriptionCs.Name,
                            defaultValue: TDFnEtlGetTargetVariableMetadataList.ColVersionDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColVersionDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Version label in English
                /// </summary>
                public string VersionLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColVersionLabelEn.Name,
                            defaultValue: TDFnEtlGetTargetVariableMetadataList.ColVersionLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColVersionLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Version description in English
                /// </summary>
                public string VersionDescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColVersionDescriptionEn.Name,
                            defaultValue: TDFnEtlGetTargetVariableMetadataList.ColVersionDescriptionEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColVersionDescriptionEn.Name,
                            val: value);
                    }
                }

                #endregion Version


                #region DefinitionVariant

                /// <summary>
                /// Definition variant label in national language
                /// </summary>
                public string DefinitionVariantLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColDefinitionVariantLabelCs.Name,
                            defaultValue: TDFnEtlGetTargetVariableMetadataList.ColDefinitionVariantLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColDefinitionVariantLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Definition variant description in national language
                /// </summary>
                public string DefinitionVariantDescriptionCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColDefinitionVariantDescriptionCs.Name,
                            defaultValue: TDFnEtlGetTargetVariableMetadataList.ColDefinitionVariantDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColDefinitionVariantDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Definition variant label in English
                /// </summary>
                public string DefinitionVariantLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColDefinitionVariantLabelEn.Name,
                            defaultValue: TDFnEtlGetTargetVariableMetadataList.ColDefinitionVariantLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColDefinitionVariantLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Definition variant description in English
                /// </summary>
                public string DefinitionVariantDescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColDefinitionVariantDescriptionEn.Name,
                            defaultValue: TDFnEtlGetTargetVariableMetadataList.ColDefinitionVariantDescriptionEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColDefinitionVariantDescriptionEn.Name,
                            val: value);
                    }
                }

                #endregion DefinitionVariant


                #region AreaDomain

                /// <summary>
                /// Area domain label in national language
                /// </summary>
                public string AreaDomainLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColAreaDomainLabelCs.Name,
                            defaultValue: TDFnEtlGetTargetVariableMetadataList.ColAreaDomainLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColAreaDomainLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Area domain description in national language
                /// </summary>
                public string AreaDomainDescriptionCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColAreaDomainDescriptionCs.Name,
                            defaultValue: TDFnEtlGetTargetVariableMetadataList.ColAreaDomainDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColAreaDomainDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Area domain label in English
                /// </summary>
                public string AreaDomainLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColAreaDomainLabelEn.Name,
                            defaultValue: TDFnEtlGetTargetVariableMetadataList.ColAreaDomainLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColAreaDomainLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Area domain description in English
                /// </summary>
                public string AreaDomainDescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColAreaDomainDescriptionEn.Name,
                            defaultValue: TDFnEtlGetTargetVariableMetadataList.ColAreaDomainDescriptionEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColAreaDomainDescriptionEn.Name,
                            val: value);
                    }
                }

                #endregion AreaDomain


                #region Population

                /// <summary>
                /// Population label in national language
                /// </summary>
                public string PopulationLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColPopulationLabelCs.Name,
                            defaultValue: TDFnEtlGetTargetVariableMetadataList.ColPopulationLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColPopulationLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Population description in national language
                /// </summary>
                public string PopulationDescriptionCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColPopulationDescriptionCs.Name,
                            defaultValue: TDFnEtlGetTargetVariableMetadataList.ColPopulationDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColPopulationDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Population label in English
                /// </summary>
                public string PopulationLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColPopulationLabelEn.Name,
                            defaultValue: TDFnEtlGetTargetVariableMetadataList.ColPopulationLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColPopulationLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Population description in English
                /// </summary>
                public string PopulationDescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColPopulationDescriptionEn.Name,
                            defaultValue: TDFnEtlGetTargetVariableMetadataList.ColPopulationDescriptionEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableMetadataList.ColPopulationDescriptionEn.Name,
                            val: value);
                    }
                }

                #endregion Population

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not TDFnEtlGetTargetVariableMetadata Type, return False
                    if (obj is not TDFnEtlGetTargetVariableMetadata)
                    {
                        return false;
                    }

                    return
                        Id == ((TDFnEtlGetTargetVariableMetadata)obj).Id;
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                #endregion Methods

            }


            /// <summary>
            /// List of target variable for ETL with metadata
            /// (in the TargetData schema)
            /// </summary>
            public class TDFnEtlGetTargetVariableMetadataList
                 : ALinqView, ITargetVariableMetadataList
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema = TDSchema.Name;

                /// <summary>
                /// Table name
                /// </summary>
                public const string Name = "fn_etl_get_target_variable_metadata";

                /// <summary>
                /// Table alias
                /// </summary>
                public const string Alias = "fn_etl_get_target_variable_metadata";

                /// <summary>
                /// Description in national language
                /// </summary>
                public const string CaptionCs = "Cílová proměnná pro ETL včetně metadat";

                /// <summary>
                /// Description in English
                /// </summary>
                public const string CaptionEn = "Target variable for ETL with metadata";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
                {
                    { Core.TargetVariableMetadataJson.ColId, new ColumnMetadata()
                    {
                        Name = Core.TargetVariableMetadataJson.ColId,
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = true,
                        NotNull = true,
                        DefaultValue = "0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = Core.TargetVariableMetadataJson.ColId.ToUpper(),
                        HeaderTextEn = Core.TargetVariableMetadataJson.ColId.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 0
                    }
                    },
                    { Core.TargetVariableMetadataJson.ColTargetVariableId, new ColumnMetadata()
                    {
                        Name = Core.TargetVariableMetadataJson.ColTargetVariableId,
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue = "0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = Core.TargetVariableMetadataJson.ColTargetVariableId.ToUpper(),
                        HeaderTextEn = Core.TargetVariableMetadataJson.ColTargetVariableId.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 1
                    }
                    },
                    { Core.TargetVariableMetadataJson.ColLocalDensityId, new ColumnMetadata()
                    {
                        Name = Core.TargetVariableMetadataJson.ColLocalDensityId,
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue = "0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = Core.TargetVariableMetadataJson.ColLocalDensityId.ToUpper(),
                        HeaderTextEn = Core.TargetVariableMetadataJson.ColLocalDensityId.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 2
                    }
                    },
                    { Core.TargetVariableMetadataJson.ColJsonValidity, new ColumnMetadata()
                    {
                        Name = Core.TargetVariableMetadataJson.ColJsonValidity,
                        DbName = null,
                        DataType = "System.Boolean",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = true,
                        DefaultValue = "false",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = Core.TargetVariableMetadataJson.ColJsonValidity.ToUpper(),
                        HeaderTextEn = Core.TargetVariableMetadataJson.ColJsonValidity.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 3
                    }
                    },

                    { "label", new ColumnMetadata()
                    {
                        Name = "label",
                        DbName = "label",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "LABEL",
                        HeaderTextEn = "LABEL",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 4
                    }
                    },
                    { "description", new ColumnMetadata()
                    {
                        Name = "description",
                        DbName = "description",
                        DataType = "System.String",
                        DbDataType = "text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "DESCRIPTION",
                        HeaderTextEn = "DESCRIPTION",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 5
                    }
                    },
                    { "label_en", new ColumnMetadata()
                    {
                        Name = "label_en",
                        DbName = "label_en",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "LABEL_EN",
                        HeaderTextEn = "LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 6
                    }
                    },
                    { "description_en", new ColumnMetadata()
                    {
                        Name = "description_en",
                        DbName = "description_en",
                        DataType = "System.String",
                        DbDataType = "text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "DESCRIPTION",
                        HeaderTextEn = "DESCRIPTION",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 7
                    }
                    },
                    { "id_etl_target_variable", new ColumnMetadata()
                    {
                        Name = "id_etl_target_variable",
                        DbName = "id_etl_target_variable",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ID_ETL_TARGET_VARIABLE",
                        HeaderTextEn = "ID_ETL_TARGET_VARIABLE",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 8
                    }
                    },
                    { "check_target_variable", new ColumnMetadata()
                    {
                        Name = "check_target_variable",
                        DbName = "check_target_variable",
                        DataType = "System.Boolean",
                        DbDataType = "bool",
                        NewDataType = "bool",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "CHECK_TARGET_VARIABLE",
                        HeaderTextEn = "CHECK_TARGET_VARIABLE",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 9
                    }
                    },
                    { "refyearset2panel_mapping", new ColumnMetadata()
                    {
                        Name = "refyearset2panel_mapping",
                        DbName = "refyearset2panel_mapping",
                        DataType = "System.String",
                        DbDataType = "_int4",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = "array_to_string({0}, ';', 'NULL')",
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "REF_YEAR_SET_TO_PANEL_MAPPING",
                        HeaderTextEn = "REF_YEAR_SET_TO_PANEL_MAPPING",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 10
                    }
                    },
                    { "check_atomic_area_domains", new ColumnMetadata()
                    {
                        Name = "check_atomic_area_domains",
                        DbName = "check_atomic_area_domains",
                        DataType = "System.Boolean",
                        DbDataType = "bool",
                        NewDataType = "bool",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "CHECK_ATOMIC_AREA_DOMAINS",
                        HeaderTextEn = "CHECK_ATOMIC_AREA_DOMAINS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 11
                    }
                    },
                    { "check_atomic_sub_populations", new ColumnMetadata()
                    {
                        Name = "check_atomic_sub_populations",
                        DbName = "check_atomic_sub_populations",
                        DataType = "System.Boolean",
                        DbDataType = "bool",
                        NewDataType = "bool",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "CHECK_ATOMIC_SUB_POPULATIONS",
                        HeaderTextEn = "CHECK_ATOMIC_SUB_POPULATIONS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 12
                    }
                    },

                    { Core.TargetVariableMetadataJson.ColIndicatorLabelCs, new ColumnMetadata()
                    {
                        Name = Core.TargetVariableMetadataJson.ColIndicatorLabelCs,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = Core.TargetVariableMetadataJson.ColIndicatorLabelCs.ToUpper(),
                        HeaderTextEn = Core.TargetVariableMetadataJson.ColIndicatorLabelCs.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 13
                    }
                    },
                    { Core.TargetVariableMetadataJson.ColIndicatorDescriptionCs, new ColumnMetadata()
                    {
                        Name = Core.TargetVariableMetadataJson.ColIndicatorDescriptionCs,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = Core.TargetVariableMetadataJson.ColIndicatorDescriptionCs.ToUpper(),
                        HeaderTextEn = Core.TargetVariableMetadataJson.ColIndicatorDescriptionCs.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 14
                    }
                    },
                    { Core.TargetVariableMetadataJson.ColIndicatorLabelEn, new ColumnMetadata()
                    {
                        Name = Core.TargetVariableMetadataJson.ColIndicatorLabelEn,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = Core.TargetVariableMetadataJson.ColIndicatorLabelEn.ToUpper(),
                        HeaderTextEn = Core.TargetVariableMetadataJson.ColIndicatorLabelEn.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 15
                    }
                    },
                    { Core.TargetVariableMetadataJson.ColIndicatorDescriptionEn, new ColumnMetadata()
                    {
                        Name = Core.TargetVariableMetadataJson.ColIndicatorDescriptionEn,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = Core.TargetVariableMetadataJson.ColIndicatorDescriptionEn.ToUpper(),
                        HeaderTextEn = Core.TargetVariableMetadataJson.ColIndicatorDescriptionEn.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 16
                    }
                    },

                    { Core.TargetVariableMetadataJson.ColStateOrChangeLabelCs, new ColumnMetadata()
                    {
                        Name = Core.TargetVariableMetadataJson.ColStateOrChangeLabelCs,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = Core.TargetVariableMetadataJson.ColStateOrChangeLabelCs.ToUpper(),
                        HeaderTextEn = Core.TargetVariableMetadataJson.ColStateOrChangeLabelCs.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 17
                    }
                    },
                    { Core.TargetVariableMetadataJson.ColStateOrChangeDescriptionCs, new ColumnMetadata()
                    {
                        Name = Core.TargetVariableMetadataJson.ColStateOrChangeDescriptionCs,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = Core.TargetVariableMetadataJson.ColStateOrChangeDescriptionCs.ToUpper(),
                        HeaderTextEn = Core.TargetVariableMetadataJson.ColStateOrChangeDescriptionCs.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 18
                    }
                    },
                    { Core.TargetVariableMetadataJson.ColStateOrChangeLabelEn, new ColumnMetadata()
                    {
                        Name = Core.TargetVariableMetadataJson.ColStateOrChangeLabelEn,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = Core.TargetVariableMetadataJson.ColStateOrChangeLabelEn.ToUpper(),
                        HeaderTextEn = Core.TargetVariableMetadataJson.ColStateOrChangeLabelEn.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 19
                    }
                    },
                    { Core.TargetVariableMetadataJson.ColStateOrChangeDescriptionEn, new ColumnMetadata()
                    {
                        Name = Core.TargetVariableMetadataJson.ColStateOrChangeDescriptionEn,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = Core.TargetVariableMetadataJson.ColStateOrChangeDescriptionEn.ToUpper(),
                        HeaderTextEn = Core.TargetVariableMetadataJson.ColStateOrChangeDescriptionEn.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 20
                    }
                    },

                    { Core.TargetVariableMetadataJson.ColUnitLabelCs, new ColumnMetadata()
                    {
                        Name = Core.TargetVariableMetadataJson.ColUnitLabelCs,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = Core.TargetVariableMetadataJson.ColUnitLabelCs.ToUpper(),
                        HeaderTextEn = Core.TargetVariableMetadataJson.ColUnitLabelCs.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 21
                    }
                    },
                    { Core.TargetVariableMetadataJson.ColUnitDescriptionCs, new ColumnMetadata()
                    {
                        Name = Core.TargetVariableMetadataJson.ColUnitDescriptionCs,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = Core.TargetVariableMetadataJson.ColUnitDescriptionCs.ToUpper(),
                        HeaderTextEn = Core.TargetVariableMetadataJson.ColUnitDescriptionCs.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 22
                    }
                    },
                    { Core.TargetVariableMetadataJson.ColUnitLabelEn, new ColumnMetadata()
                    {
                        Name = Core.TargetVariableMetadataJson.ColUnitLabelEn,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = Core.TargetVariableMetadataJson.ColUnitLabelEn.ToUpper(),
                        HeaderTextEn = Core.TargetVariableMetadataJson.ColUnitLabelEn.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 23
                    }
                    },
                    { Core.TargetVariableMetadataJson.ColUnitDescriptionEn, new ColumnMetadata()
                    {
                        Name = Core.TargetVariableMetadataJson.ColUnitDescriptionEn,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = Core.TargetVariableMetadataJson.ColUnitDescriptionEn.ToUpper(),
                        HeaderTextEn = Core.TargetVariableMetadataJson.ColUnitDescriptionEn.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 24
                    }
                    },

                    { Core.TargetVariableMetadataJson.ColLocalDensityObjectTypeLabelCs, new ColumnMetadata()
                    {
                        Name = Core.TargetVariableMetadataJson.ColLocalDensityObjectTypeLabelCs,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = Core.TargetVariableMetadataJson.ColLocalDensityObjectTypeLabelCs.ToUpper(),
                        HeaderTextEn = Core.TargetVariableMetadataJson.ColLocalDensityObjectTypeLabelCs.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 25
                    }
                    },
                    { Core.TargetVariableMetadataJson.ColLocalDensityObjectTypeDescriptionCs, new ColumnMetadata()
                    {
                        Name = Core.TargetVariableMetadataJson.ColLocalDensityObjectTypeDescriptionCs,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = Core.TargetVariableMetadataJson.ColLocalDensityObjectTypeDescriptionCs.ToUpper(),
                        HeaderTextEn = Core.TargetVariableMetadataJson.ColLocalDensityObjectTypeDescriptionCs.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 26
                    }
                    },
                    { Core.TargetVariableMetadataJson.ColLocalDensityObjectTypeLabelEn, new ColumnMetadata()
                    {
                        Name = Core.TargetVariableMetadataJson.ColLocalDensityObjectTypeLabelEn,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = Core.TargetVariableMetadataJson.ColLocalDensityObjectTypeLabelEn.ToUpper(),
                        HeaderTextEn = Core.TargetVariableMetadataJson.ColLocalDensityObjectTypeLabelEn.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 27
                    }
                    },
                    { Core.TargetVariableMetadataJson.ColLocalDensityObjectTypeDescriptionEn, new ColumnMetadata()
                    {
                        Name = Core.TargetVariableMetadataJson.ColLocalDensityObjectTypeDescriptionEn,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = Core.TargetVariableMetadataJson.ColLocalDensityObjectTypeDescriptionEn.ToUpper(),
                        HeaderTextEn = Core.TargetVariableMetadataJson.ColLocalDensityObjectTypeDescriptionEn.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 28
                    }
                    },

                    { Core.TargetVariableMetadataJson.ColLocalDensityObjectLabelCs, new ColumnMetadata()
                    {
                        Name = Core.TargetVariableMetadataJson.ColLocalDensityObjectLabelCs,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = Core.TargetVariableMetadataJson.ColLocalDensityObjectLabelCs.ToUpper(),
                        HeaderTextEn = Core.TargetVariableMetadataJson.ColLocalDensityObjectLabelCs.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 29
                    }
                    },
                    { Core.TargetVariableMetadataJson.ColLocalDensityObjectDescriptionCs, new ColumnMetadata()
                    {
                        Name = Core.TargetVariableMetadataJson.ColLocalDensityObjectDescriptionCs,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = Core.TargetVariableMetadataJson.ColLocalDensityObjectDescriptionCs.ToUpper(),
                        HeaderTextEn = Core.TargetVariableMetadataJson.ColLocalDensityObjectDescriptionCs.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 30
                    }
                    },
                    { Core.TargetVariableMetadataJson.ColLocalDensityObjectLabelEn, new ColumnMetadata()
                    {
                        Name = Core.TargetVariableMetadataJson.ColLocalDensityObjectLabelEn,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = Core.TargetVariableMetadataJson.ColLocalDensityObjectLabelEn.ToUpper(),
                        HeaderTextEn = Core.TargetVariableMetadataJson.ColLocalDensityObjectLabelEn.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 31
                    }
                    },
                    { Core.TargetVariableMetadataJson.ColLocalDensityObjectDescriptionEn, new ColumnMetadata()
                    {
                        Name = Core.TargetVariableMetadataJson.ColLocalDensityObjectDescriptionEn,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = Core.TargetVariableMetadataJson.ColLocalDensityObjectDescriptionEn.ToUpper(),
                        HeaderTextEn = Core.TargetVariableMetadataJson.ColLocalDensityObjectDescriptionEn.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 32
                    }
                    },

                    { Core.TargetVariableMetadataJson.ColLocalDensityLabelCs, new ColumnMetadata()
                    {
                        Name = Core.TargetVariableMetadataJson.ColLocalDensityLabelCs,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = Core.TargetVariableMetadataJson.ColLocalDensityLabelCs.ToUpper(),
                        HeaderTextEn = Core.TargetVariableMetadataJson.ColLocalDensityLabelCs.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 33
                    }
                    },
                    { Core.TargetVariableMetadataJson.ColLocalDensityDescriptionCs, new ColumnMetadata()
                    {
                        Name = Core.TargetVariableMetadataJson.ColLocalDensityDescriptionCs,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = Core.TargetVariableMetadataJson.ColLocalDensityDescriptionCs.ToUpper(),
                        HeaderTextEn = Core.TargetVariableMetadataJson.ColLocalDensityDescriptionCs.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 34
                    }
                    },
                    { Core.TargetVariableMetadataJson.ColLocalDensityLabelEn, new ColumnMetadata()
                    {
                        Name = Core.TargetVariableMetadataJson.ColLocalDensityLabelEn,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = Core.TargetVariableMetadataJson.ColLocalDensityLabelEn.ToUpper(),
                        HeaderTextEn = Core.TargetVariableMetadataJson.ColLocalDensityLabelEn.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 35
                    }
                    },
                    { Core.TargetVariableMetadataJson.ColLocalDensityDescriptionEn, new ColumnMetadata()
                    {
                        Name = Core.TargetVariableMetadataJson.ColLocalDensityDescriptionEn,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = Core.TargetVariableMetadataJson.ColLocalDensityDescriptionEn.ToUpper(),
                        HeaderTextEn = Core.TargetVariableMetadataJson.ColLocalDensityDescriptionEn.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 36
                    }
                    },

                    { Core.TargetVariableMetadataJson.ColUseNegativeCs, new ColumnMetadata()
                    {
                        Name = Core.TargetVariableMetadataJson.ColUseNegativeCs,
                        DbName = null,
                        DataType = "System.Boolean",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = Core.TargetVariableMetadataJson.ColUseNegativeCs.ToUpper(),
                        HeaderTextEn = Core.TargetVariableMetadataJson.ColUseNegativeCs.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 37
                    }
                    },
                    { Core.TargetVariableMetadataJson.ColUseNegativeEn, new ColumnMetadata()
                    {
                        Name = Core.TargetVariableMetadataJson.ColUseNegativeEn,
                        DbName = null,
                        DataType = "System.Boolean",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = Core.TargetVariableMetadataJson.ColUseNegativeEn.ToUpper(),
                        HeaderTextEn = Core.TargetVariableMetadataJson.ColUseNegativeEn.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 38
                    }
                    },

                    { Core.TargetVariableMetadataJson.ColVersionLabelCs, new ColumnMetadata()
                    {
                        Name = Core.TargetVariableMetadataJson.ColVersionLabelCs,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = Core.TargetVariableMetadataJson.ColVersionLabelCs.ToUpper(),
                        HeaderTextEn = Core.TargetVariableMetadataJson.ColVersionLabelCs.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 39
                    }
                    },
                    { Core.TargetVariableMetadataJson.ColVersionDescriptionCs, new ColumnMetadata()
                    {
                        Name = Core.TargetVariableMetadataJson.ColVersionDescriptionCs,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = Core.TargetVariableMetadataJson.ColVersionDescriptionCs.ToUpper(),
                        HeaderTextEn = Core.TargetVariableMetadataJson.ColVersionDescriptionCs.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 40
                    }
                    },
                    { Core.TargetVariableMetadataJson.ColVersionLabelEn, new ColumnMetadata()
                    {
                        Name = Core.TargetVariableMetadataJson.ColVersionLabelEn,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = Core.TargetVariableMetadataJson.ColVersionLabelEn.ToUpper(),
                        HeaderTextEn = Core.TargetVariableMetadataJson.ColVersionLabelEn.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 41
                    }
                    },
                    { Core.TargetVariableMetadataJson.ColVersionDescriptionEn, new ColumnMetadata()
                    {
                        Name = Core.TargetVariableMetadataJson.ColVersionDescriptionEn,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = Core.TargetVariableMetadataJson.ColVersionDescriptionEn.ToUpper(),
                        HeaderTextEn = Core.TargetVariableMetadataJson.ColVersionDescriptionEn.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 42
                    }
                    },

                    { Core.TargetVariableMetadataJson.ColDefinitionVariantLabelCs, new ColumnMetadata()
                    {
                        Name = Core.TargetVariableMetadataJson.ColDefinitionVariantLabelCs,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = Core.TargetVariableMetadataJson.ColDefinitionVariantLabelCs.ToUpper(),
                        HeaderTextEn = Core.TargetVariableMetadataJson.ColDefinitionVariantLabelCs.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 43
                    }
                    },
                    { Core.TargetVariableMetadataJson.ColDefinitionVariantDescriptionCs, new ColumnMetadata()
                    {
                        Name = Core.TargetVariableMetadataJson.ColDefinitionVariantDescriptionCs,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = Core.TargetVariableMetadataJson.ColDefinitionVariantDescriptionCs.ToUpper(),
                        HeaderTextEn = Core.TargetVariableMetadataJson.ColDefinitionVariantDescriptionCs.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 44
                    }
                    },
                    { Core.TargetVariableMetadataJson.ColDefinitionVariantLabelEn, new ColumnMetadata()
                    {
                        Name = Core.TargetVariableMetadataJson.ColDefinitionVariantLabelEn,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = Core.TargetVariableMetadataJson.ColDefinitionVariantLabelEn.ToUpper(),
                        HeaderTextEn = Core.TargetVariableMetadataJson.ColDefinitionVariantLabelEn.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 45
                    }
                    },
                    { Core.TargetVariableMetadataJson.ColDefinitionVariantDescriptionEn, new ColumnMetadata()
                    {
                        Name = Core.TargetVariableMetadataJson.ColDefinitionVariantDescriptionEn,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = Core.TargetVariableMetadataJson.ColDefinitionVariantDescriptionEn.ToUpper(),
                        HeaderTextEn = Core.TargetVariableMetadataJson.ColDefinitionVariantDescriptionEn.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 46
                    }
                    },

                    { Core.TargetVariableMetadataJson.ColAreaDomainLabelCs, new ColumnMetadata()
                    {
                        Name = Core.TargetVariableMetadataJson.ColAreaDomainLabelCs,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = Core.TargetVariableMetadataJson.ColAreaDomainLabelCs.ToUpper(),
                        HeaderTextEn = Core.TargetVariableMetadataJson.ColAreaDomainLabelCs.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 47
                    }
                    },
                    { Core.TargetVariableMetadataJson.ColAreaDomainDescriptionCs, new ColumnMetadata()
                    {
                        Name = Core.TargetVariableMetadataJson.ColAreaDomainDescriptionCs,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = Core.TargetVariableMetadataJson.ColAreaDomainDescriptionCs.ToUpper(),
                        HeaderTextEn = Core.TargetVariableMetadataJson.ColAreaDomainDescriptionCs.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 48
                    }
                    },
                    { Core.TargetVariableMetadataJson.ColAreaDomainLabelEn, new ColumnMetadata()
                    {
                        Name = Core.TargetVariableMetadataJson.ColAreaDomainLabelEn,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = Core.TargetVariableMetadataJson.ColAreaDomainLabelEn.ToUpper(),
                        HeaderTextEn = Core.TargetVariableMetadataJson.ColAreaDomainLabelEn.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 49
                    }
                    },
                    { Core.TargetVariableMetadataJson.ColAreaDomainDescriptionEn, new ColumnMetadata()
                    {
                        Name = Core.TargetVariableMetadataJson.ColAreaDomainDescriptionEn,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = Core.TargetVariableMetadataJson.ColAreaDomainDescriptionEn.ToUpper(),
                        HeaderTextEn = Core.TargetVariableMetadataJson.ColAreaDomainDescriptionEn.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 50
                    }
                    },

                    { Core.TargetVariableMetadataJson.ColPopulationLabelCs, new ColumnMetadata()
                    {
                        Name = Core.TargetVariableMetadataJson.ColPopulationLabelCs,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = Core.TargetVariableMetadataJson.ColPopulationLabelCs.ToUpper(),
                        HeaderTextEn = Core.TargetVariableMetadataJson.ColPopulationLabelCs.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 51
                    }
                    },
                    { Core.TargetVariableMetadataJson.ColPopulationDescriptionCs, new ColumnMetadata()
                    {
                        Name = Core.TargetVariableMetadataJson.ColPopulationDescriptionCs,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = Core.TargetVariableMetadataJson.ColPopulationDescriptionCs.ToUpper(),
                        HeaderTextEn = Core.TargetVariableMetadataJson.ColPopulationDescriptionCs.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 52
                    }
                    },
                    { Core.TargetVariableMetadataJson.ColPopulationLabelEn, new ColumnMetadata()
                    {
                        Name = Core.TargetVariableMetadataJson.ColPopulationLabelEn,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = Core.TargetVariableMetadataJson.ColPopulationLabelEn.ToUpper(),
                        HeaderTextEn = Core.TargetVariableMetadataJson.ColPopulationLabelEn.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 53
                    }
                    },
                    { Core.TargetVariableMetadataJson.ColPopulationDescriptionEn, new ColumnMetadata()
                    {
                        Name = Core.TargetVariableMetadataJson.ColPopulationDescriptionEn,
                        DbName = null,
                        DataType = "System.String",
                        DbDataType = null,
                        NewDataType = null,
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = Core.TargetVariableMetadataJson.ColPopulationDescriptionEn.ToUpper(),
                        HeaderTextEn = Core.TargetVariableMetadataJson.ColPopulationDescriptionEn.ToUpper(),
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 54
                    }
                    }
                };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols[Core.TargetVariableMetadataJson.ColId];

                /// <summary>
                /// Column target_variable_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColTargetVariableId = Cols[Core.TargetVariableMetadataJson.ColTargetVariableId];

                /// <summary>
                /// Column ldsity_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColLocalDensityId = Cols[Core.TargetVariableMetadataJson.ColLocalDensityId];

                /// <summary>
                /// Column json_validity metadata
                /// </summary>
                public static readonly ColumnMetadata ColJsonValidity = Cols[Core.TargetVariableMetadataJson.ColJsonValidity];


                /// <summary>
                /// Column label metadata
                /// </summary>
                public static readonly ColumnMetadata ColLabelCs = Cols["label"];

                /// <summary>
                /// Column description metadata
                /// </summary>
                public static readonly ColumnMetadata ColDescriptionCs = Cols["description"];

                /// <summary>
                /// Column label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColLabelEn = Cols["label_en"];

                /// <summary>
                /// Column description_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColDescriptionEn = Cols["description_en"];

                /// <summary>
                /// Column id_etl_target_variable metadata
                /// </summary>
                public static readonly ColumnMetadata ColIdEtlTargetVariableMetadata = Cols["id_etl_target_variable"];

                /// <summary>
                /// Column check_target_variable metadata
                /// </summary>
                public static readonly ColumnMetadata ColCheckTargetVariable = Cols["check_target_variable"];

                /// <summary>
                /// Column refyearset2panel_mapping metadata
                /// </summary>
                public static readonly ColumnMetadata ColRefYearSetToPanelMapping = Cols["refyearset2panel_mapping"];

                /// <summary>
                /// Column check_atomic_area_domains metadata
                /// </summary>
                public static readonly ColumnMetadata ColCheckAtomicAreaDomains = Cols["check_atomic_area_domains"];

                /// <summary>
                /// Column check_atomic_sub_populations metadata
                /// </summary>
                public static readonly ColumnMetadata ColCheckAtomicSubPopulations = Cols["check_atomic_sub_populations"];


                /// <summary>
                /// Column indicator_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColIndicatorLabelCs = Cols[Core.TargetVariableMetadataJson.ColIndicatorLabelCs];

                /// <summary>
                /// Column indicator_description_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColIndicatorDescriptionCs = Cols[Core.TargetVariableMetadataJson.ColIndicatorDescriptionCs];

                /// <summary>
                /// Column indicator_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColIndicatorLabelEn = Cols[Core.TargetVariableMetadataJson.ColIndicatorLabelEn];

                /// <summary>
                /// Column indicator_description_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColIndicatorDescriptionEn = Cols[Core.TargetVariableMetadataJson.ColIndicatorDescriptionEn];


                /// <summary>
                /// Column state_or_change_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColStateOrChangeLabelCs = Cols[Core.TargetVariableMetadataJson.ColStateOrChangeLabelCs];

                /// <summary>
                /// Column state_or_change_description_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColStateOrChangeDescriptionCs = Cols[Core.TargetVariableMetadataJson.ColStateOrChangeDescriptionCs];

                /// <summary>
                /// Column state_or_change_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColStateOrChangeLabelEn = Cols[Core.TargetVariableMetadataJson.ColStateOrChangeLabelEn];

                /// <summary>
                /// Column state_or_change_description_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColStateOrChangeDescriptionEn = Cols[Core.TargetVariableMetadataJson.ColStateOrChangeDescriptionEn];


                /// <summary>
                /// Column unit_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColUnitLabelCs = Cols[Core.TargetVariableMetadataJson.ColUnitLabelCs];

                /// <summary>
                /// Column unit_description_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColUnitDescriptionCs = Cols[Core.TargetVariableMetadataJson.ColUnitDescriptionCs];

                /// <summary>
                /// Column unit_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColUnitLabelEn = Cols[Core.TargetVariableMetadataJson.ColUnitLabelEn];

                /// <summary>
                /// Column unit_description_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColUnitDescriptionEn = Cols[Core.TargetVariableMetadataJson.ColUnitDescriptionEn];


                /// <summary>
                /// Column ldsity_object_type_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColLocalDensityObjectTypeLabelCs = Cols[Core.TargetVariableMetadataJson.ColLocalDensityObjectTypeLabelCs];

                /// <summary>
                /// Column ldsity_object_type_description_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColLocalDensityObjectTypeDescriptionCs = Cols[Core.TargetVariableMetadataJson.ColLocalDensityObjectTypeDescriptionCs];

                /// <summary>
                /// Column ldsity_object_type_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColLocalDensityObjectTypeLabelEn = Cols[Core.TargetVariableMetadataJson.ColLocalDensityObjectTypeLabelEn];

                /// <summary>
                /// Column ldsity_object_type_description_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColLocalDensityObjectTypeDescriptionEn = Cols[Core.TargetVariableMetadataJson.ColLocalDensityObjectTypeDescriptionEn];


                /// <summary>
                /// Column ldsity_object_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColLocalDensityObjectLabelCs = Cols[Core.TargetVariableMetadataJson.ColLocalDensityObjectLabelCs];

                /// <summary>
                /// Column ldsity_object_description_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColLocalDensityObjectDescriptionCs = Cols[Core.TargetVariableMetadataJson.ColLocalDensityObjectDescriptionCs];

                /// <summary>
                /// Column ldsity_object_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColLocalDensityObjectLabelEn = Cols[Core.TargetVariableMetadataJson.ColLocalDensityObjectLabelEn];

                /// <summary>
                /// Column ldsity_object_description_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColLocalDensityObjectDescriptionEn = Cols[Core.TargetVariableMetadataJson.ColLocalDensityObjectDescriptionEn];


                /// <summary>
                /// Column ldsity_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColLocalDensityLabelCs = Cols[Core.TargetVariableMetadataJson.ColLocalDensityLabelCs];

                /// <summary>
                /// Column ldsity_description_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColLocalDensityDescriptionCs = Cols[Core.TargetVariableMetadataJson.ColLocalDensityDescriptionCs];

                /// <summary>
                /// Column ldsity_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColLocalDensityLabelEn = Cols[Core.TargetVariableMetadataJson.ColLocalDensityLabelEn];

                /// <summary>
                /// Column ldsity_description_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColLocalDensityDescriptionEn = Cols[Core.TargetVariableMetadataJson.ColLocalDensityDescriptionEn];


                /// <summary>
                /// Column use_negative_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColUseNegativeCs = Cols[Core.TargetVariableMetadataJson.ColUseNegativeCs];

                /// <summary>
                /// Column use_negative_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColUseNegativeEn = Cols[Core.TargetVariableMetadataJson.ColUseNegativeEn];


                /// <summary>
                /// Column version_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColVersionLabelCs = Cols[Core.TargetVariableMetadataJson.ColVersionLabelCs];

                /// <summary>
                /// Column version_description_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColVersionDescriptionCs = Cols[Core.TargetVariableMetadataJson.ColVersionDescriptionCs];

                /// <summary>
                /// Column version_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColVersionLabelEn = Cols[Core.TargetVariableMetadataJson.ColVersionLabelEn];

                /// <summary>
                /// Column version_description_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColVersionDescriptionEn = Cols[Core.TargetVariableMetadataJson.ColVersionDescriptionEn];


                /// <summary>
                /// Column definition_variant_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColDefinitionVariantLabelCs = Cols[Core.TargetVariableMetadataJson.ColDefinitionVariantLabelCs];

                /// <summary>
                /// Column definition_variant_description_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColDefinitionVariantDescriptionCs = Cols[Core.TargetVariableMetadataJson.ColDefinitionVariantDescriptionCs];

                /// <summary>
                /// Column definition_variant_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColDefinitionVariantLabelEn = Cols[Core.TargetVariableMetadataJson.ColDefinitionVariantLabelEn];

                /// <summary>
                /// Column definition_variant_description_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColDefinitionVariantDescriptionEn = Cols[Core.TargetVariableMetadataJson.ColDefinitionVariantDescriptionEn];


                /// <summary>
                /// Column area_domain_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColAreaDomainLabelCs = Cols[Core.TargetVariableMetadataJson.ColAreaDomainLabelCs];

                /// <summary>
                /// Column area_domain_description_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColAreaDomainDescriptionCs = Cols[Core.TargetVariableMetadataJson.ColAreaDomainDescriptionCs];

                /// <summary>
                /// Column area_domain_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColAreaDomainLabelEn = Cols[Core.TargetVariableMetadataJson.ColAreaDomainLabelEn];

                /// <summary>
                /// Column area_domain_description_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColAreaDomainDescriptionEn = Cols[Core.TargetVariableMetadataJson.ColAreaDomainDescriptionEn];


                /// <summary>
                /// Column population_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColPopulationLabelCs = Cols[Core.TargetVariableMetadataJson.ColPopulationLabelCs];

                /// <summary>
                /// Column population_description_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColPopulationDescriptionCs = Cols[Core.TargetVariableMetadataJson.ColPopulationDescriptionCs];

                /// <summary>
                /// Column population_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColPopulationLabelEn = Cols[Core.TargetVariableMetadataJson.ColPopulationLabelEn];

                /// <summary>
                /// Column population_description_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColPopulationDescriptionEn = Cols[Core.TargetVariableMetadataJson.ColPopulationDescriptionEn];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TDFnEtlGetTargetVariableMetadataList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TDFnEtlGetTargetVariableMetadataList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// List of target variable for ETL with metadata
                /// (in the TargetData schema)
                /// (read-only)
                /// </summary>
                public List<ITargetVariableMetadata> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new TDFnEtlGetTargetVariableMetadata(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// Target variable for ETL with metadata from list by identifier (read-only)
                /// </summary>
                /// <param name="id">Target variable for ETL with metadata identifier</param>
                /// <returns>Target variable for ETL with metadata from list by identifier (null if not found)</returns>
                public ITargetVariableMetadata this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new TDFnEtlGetTargetVariableMetadata(composite: this, data: a))
                                .FirstOrDefault<ITargetVariableMetadata>();
                    }
                }

                #endregion Indexer


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public TDFnEtlGetTargetVariableMetadataList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new TDFnEtlGetTargetVariableMetadataList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new TDFnEtlGetTargetVariableMetadataList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                /// <summary>
                /// List of target variable for ETL with metadata
                /// (in the TargetData schema)
                /// for selected target variables
                /// </summary>
                /// <param name="targetVariableId">Selected target variables identifiers</param>
                /// <returns>
                /// List of target variable for ETL with metadata
                /// (in the TargetData schema)
                /// for selected target variables
                /// </returns>
                public TDFnEtlGetTargetVariableMetadataList Reduce(
                    List<int> targetVariableId = null)
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();

                    if ((targetVariableId != null) && targetVariableId.Count != 0)
                    {
                        rows = rows.Where(a => targetVariableId.Contains<int>(
                                value: (int)(a.Field<Nullable<int>>(columnName: ColTargetVariableId.Name) ?? 0)));
                    }

                    return
                        rows.Any() ?
                        new TDFnEtlGetTargetVariableMetadataList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new TDFnEtlGetTargetVariableMetadataList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                /// <summary>
                /// List of target variable for ETL with metadata
                /// (in the TargetData schema)
                /// for selected metadata element
                /// </summary>
                /// <param name="elementType">Selected metadata element type</param>
                /// <param name="element">Selected metadata element value</param>
                /// <returns>
                /// List of target variable for ETL with metadata
                /// (in the TargetData schema)
                /// for selected metadata element
                /// </returns>
                public ITargetVariableMetadataList Reduce(
                    MetadataElementType elementType,
                    MetadataElement element)
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();

                    if (element != null)
                    {
                        rows = Items
                                .Where(a => a.MetadataElements[elementType].Equals(obj: element))
                                .Select(a => a.Data);
                    }

                    return
                        rows.Any() ?
                        new TDFnEtlGetTargetVariableMetadataList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new TDFnEtlGetTargetVariableMetadataList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                /// <summary>
                /// Create new empty target variable metadata table
                /// </summary>
                /// <returns>New empty target variable metadata table</returns>
                public ITargetVariableMetadataList Empty()
                {
                    return new TDFnEtlGetTargetVariableMetadataList(database: ((NfiEstaDB)Database));
                }

                /// <summary>
                /// List of metadata elements for selected element type
                /// </summary>
                /// <param name="elementType">Selected metadata element type</param>
                /// <returns>List of metadata elements for selected element type</returns>
                public List<MetadataElement> GetMetadataElementsOfType(
                    MetadataElementType elementType)
                {
                    return LanguageVersion switch
                    {
                        LanguageVersion.International =>
                            [.. Items
                                .Select(a => a.MetadataElements[elementType])
                                .Distinct<MetadataElement>()
                                .OrderBy(a => (a.TargetVariableId == 0) ? 0 : 1)
                                .ThenBy(a => a.LabelEn)],
                        LanguageVersion.National =>
                            [.. Items
                                .Select(a => a.MetadataElements[elementType])
                                .Distinct<MetadataElement>()
                                .OrderBy(a => (a.TargetVariableId == 0) ? 0 : 1)
                                .ThenBy(a => a.LabelCs)],
                        _ =>
                            [.. Items
                                .Select(a => a.MetadataElements[elementType])
                                .Distinct<MetadataElement>()
                                .OrderBy(a => a.TargetVariableId)],
                    };
                }

                /// <summary>
                /// Loads object data
                /// (if it was not done before)
                /// </summary>
                /// <param name="fnEtlGetTargetVariable">Result of the function fn_etl_get_target_variable</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                public void Load(
                    TDFnEtlGetTargetVariableTypeList fnEtlGetTargetVariable,
                    string condition = null,
                    Nullable<int> limit = null)
                {
                    if (!Loaded)
                    {
                        ReLoad(
                            fnEtlGetTargetVariable: fnEtlGetTargetVariable,
                            condition: condition,
                            limit: limit);
                    }
                }

                /// <summary>
                /// Loads object data
                /// </summary>
                /// <param name="fnEtlGetTargetVariable">Result of the function fn_etl_get_target_variable</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                public void ReLoad(
                     TDFnEtlGetTargetVariableTypeList fnEtlGetTargetVariable,
                    string condition = null,
                    Nullable<int> limit = null)
                {
                    Data = EmptyDataTable(
                                 tableName: TableName,
                                 columns: Columns);
                    Condition = condition;
                    Limit = limit;

                    System.Diagnostics.Stopwatch stopWatch = new();
                    stopWatch.Start();

                    int i = 0;
                    foreach (ITargetVariable itv in fnEtlGetTargetVariable.Items)
                    {
                        TDFnEtlGetTargetVariableType item = (TDFnEtlGetTargetVariableType)itv;

                        Core.TargetVariableMetadataJson json = new();
                        json.LoadFromText(text: item.MetadataText);

                        List<DataRow> jsonDataRows =
                            [.. json.Data.AsEnumerable().OrderBy(a => a.Field<int>(columnName: Core.TargetVariableMetadataJson.ColId))];

                        foreach (DataRow row in jsonDataRows)
                        {
                            DataRow newRow = Data.NewRow();

                            Functions.SetIntArg(
                                row: newRow,
                                name: ColId.Name,
                                val: ++i);

                            Functions.SetIntArg(
                                row: newRow,
                                name: ColTargetVariableId.Name,
                                val: item.Id);

                            Functions.SetIntArg(
                                row: newRow,
                                name: ColLocalDensityId.Name,
                                val: Functions.GetIntArg(
                                    row: row,
                                    name: Core.TargetVariableMetadataJson.ColLocalDensityId,
                                    defaultValue: 0));

                            Functions.SetBoolArg(
                                row: newRow,
                                name: ColJsonValidity.Name,
                                val: Functions.GetBoolArg(
                                    row: row,
                                    name: Core.TargetVariableMetadataJson.ColJsonValidity,
                                    defaultValue: false));


                            Functions.SetStringArg(
                                row: newRow,
                                name: ColLabelCs.Name,
                                val: item.LabelCs);

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColDescriptionCs.Name,
                                val: item.DescriptionCs);

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColLabelEn.Name,
                                val: item.LabelEn);

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColDescriptionEn.Name,
                                val: item.DescriptionEn);

                            Functions.SetNIntArg(
                                row: newRow,
                                name: ColIdEtlTargetVariableMetadata.Name,
                                val: item.IdEtlTargetVariableMetadata);

                            Functions.SetNBoolArg(
                                row: newRow,
                                name: ColCheckTargetVariable.Name,
                                val: item.CheckTargetVariable);

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColRefYearSetToPanelMapping.Name,
                                val: item.RefYearSetToPanelMappingString);

                            Functions.SetNBoolArg(
                                row: newRow,
                                name: ColCheckAtomicAreaDomains.Name,
                                val: item.CheckAtomicAreaDomains);

                            Functions.SetNBoolArg(
                                row: newRow,
                                name: ColCheckAtomicSubPopulations.Name,
                                val: item.CheckAtomicSubPopulations);

                            #region Indicator

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColIndicatorLabelCs.Name,
                                val: Functions.GetStringArg(
                                    row: row,
                                    name: Core.TargetVariableMetadataJson.ColIndicatorLabelCs,
                                    defaultValue: String.Empty));

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColIndicatorDescriptionCs.Name,
                                val: Functions.GetStringArg(
                                    row: row,
                                    name: Core.TargetVariableMetadataJson.ColIndicatorDescriptionCs,
                                    defaultValue: String.Empty));

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColIndicatorLabelEn.Name,
                                val: Functions.GetStringArg(
                                    row: row,
                                    name: Core.TargetVariableMetadataJson.ColIndicatorLabelEn,
                                    defaultValue: String.Empty));

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColIndicatorDescriptionEn.Name,
                                val: Functions.GetStringArg(
                                    row: row,
                                    name: Core.TargetVariableMetadataJson.ColIndicatorDescriptionEn,
                                    defaultValue: String.Empty));

                            #endregion Indicator

                            #region StateOrChange

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColStateOrChangeLabelCs.Name,
                                val: Functions.GetStringArg(
                                    row: row,
                                    name: Core.TargetVariableMetadataJson.ColStateOrChangeLabelCs,
                                    defaultValue: String.Empty));

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColStateOrChangeDescriptionCs.Name,
                                val: Functions.GetStringArg(
                                    row: row,
                                    name: Core.TargetVariableMetadataJson.ColStateOrChangeDescriptionCs,
                                    defaultValue: String.Empty));

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColStateOrChangeLabelEn.Name,
                                val: Functions.GetStringArg(
                                    row: row,
                                    name: Core.TargetVariableMetadataJson.ColStateOrChangeLabelEn,
                                    defaultValue: String.Empty));

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColStateOrChangeDescriptionEn.Name,
                                val: Functions.GetStringArg(
                                    row: row,
                                    name: Core.TargetVariableMetadataJson.ColStateOrChangeDescriptionEn,
                                    defaultValue: String.Empty));

                            #endregion StateOrChange

                            #region Unit

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColUnitLabelCs.Name,
                                val: Functions.GetStringArg(
                                    row: row,
                                    name: Core.TargetVariableMetadataJson.ColUnitLabelCs,
                                    defaultValue: String.Empty));

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColUnitDescriptionCs.Name,
                                val: Functions.GetStringArg(
                                    row: row,
                                    name: Core.TargetVariableMetadataJson.ColUnitDescriptionCs,
                                    defaultValue: String.Empty));

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColUnitLabelEn.Name,
                                val: Functions.GetStringArg(
                                    row: row,
                                    name: Core.TargetVariableMetadataJson.ColUnitLabelEn,
                                    defaultValue: String.Empty));

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColUnitDescriptionEn.Name,
                                val: Functions.GetStringArg(
                                    row: row,
                                    name: Core.TargetVariableMetadataJson.ColUnitDescriptionEn,
                                    defaultValue: String.Empty));

                            #endregion Unit

                            #region LocalDensityObjectType

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColLocalDensityObjectTypeLabelCs.Name,
                                val: Functions.GetStringArg(
                                   row: row,
                                   name: Core.TargetVariableMetadataJson.ColLocalDensityObjectTypeLabelCs,
                                   defaultValue: String.Empty));

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColLocalDensityObjectTypeDescriptionCs.Name,
                                val: Functions.GetStringArg(
                                   row: row,
                                   name: Core.TargetVariableMetadataJson.ColLocalDensityObjectTypeDescriptionCs,
                                   defaultValue: String.Empty));

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColLocalDensityObjectTypeLabelEn.Name,
                                val: Functions.GetStringArg(
                                   row: row,
                                   name: Core.TargetVariableMetadataJson.ColLocalDensityObjectTypeLabelEn,
                                   defaultValue: String.Empty));

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColLocalDensityObjectTypeDescriptionEn.Name,
                                val: Functions.GetStringArg(
                                   row: row,
                                   name: Core.TargetVariableMetadataJson.ColLocalDensityObjectTypeDescriptionEn,
                                   defaultValue: String.Empty));

                            #endregion LocalDensityObjectType

                            #region LocalDensityObject

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColLocalDensityObjectLabelCs.Name,
                                val: Functions.GetStringArg(
                                    row: row,
                                    name: Core.TargetVariableMetadataJson.ColLocalDensityObjectLabelCs,
                                    defaultValue: String.Empty));

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColLocalDensityObjectDescriptionCs.Name,
                                val: Functions.GetStringArg(
                                   row: row,
                                   name: Core.TargetVariableMetadataJson.ColLocalDensityObjectDescriptionCs,
                                   defaultValue: String.Empty));

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColLocalDensityObjectLabelEn.Name,
                                val: Functions.GetStringArg(
                                   row: row,
                                   name: Core.TargetVariableMetadataJson.ColLocalDensityObjectLabelEn,
                                   defaultValue: String.Empty));

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColLocalDensityObjectDescriptionEn.Name,
                                val: Functions.GetStringArg(
                                   row: row,
                                   name: Core.TargetVariableMetadataJson.ColLocalDensityObjectDescriptionEn,
                                   defaultValue: String.Empty));

                            #endregion LocalDensityObject

                            #region LocalDensity

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColLocalDensityLabelCs.Name,
                                val: Functions.GetStringArg(
                                   row: row,
                                   name: Core.TargetVariableMetadataJson.ColLocalDensityLabelCs,
                                   defaultValue: String.Empty));

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColLocalDensityDescriptionCs.Name,
                                val: Functions.GetStringArg(
                                   row: row,
                                   name: Core.TargetVariableMetadataJson.ColLocalDensityDescriptionCs,
                                   defaultValue: String.Empty));

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColLocalDensityLabelEn.Name,
                                val: Functions.GetStringArg(
                                   row: row,
                                   name: Core.TargetVariableMetadataJson.ColLocalDensityLabelEn,
                                   defaultValue: String.Empty));

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColLocalDensityDescriptionEn.Name,
                                val: Functions.GetStringArg(
                                   row: row,
                                   name: Core.TargetVariableMetadataJson.ColLocalDensityDescriptionEn,
                                   defaultValue: String.Empty));

                            #endregion LocalDensity

                            #region UseNegative

                            Functions.SetNBoolArg(
                                row: newRow,
                                name: ColUseNegativeCs.Name,
                                val: Functions.GetNBoolArg(
                                    row: row,
                                    name: Core.TargetVariableMetadataJson.ColUseNegativeCs,
                                    defaultValue: null));

                            Functions.SetNBoolArg(
                                row: newRow,
                                name: ColUseNegativeEn.Name,
                                val: Functions.GetNBoolArg(
                                   row: row,
                                   name: Core.TargetVariableMetadataJson.ColUseNegativeEn,
                                   defaultValue: null));

                            #endregion UseNegative

                            #region Version

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColVersionLabelCs.Name,
                                val: Functions.GetStringArg(
                                  row: row,
                                  name: Core.TargetVariableMetadataJson.ColVersionLabelCs,
                                  defaultValue: String.Empty));

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColVersionDescriptionCs.Name,
                                val: Functions.GetStringArg(
                                   row: row,
                                   name: Core.TargetVariableMetadataJson.ColVersionDescriptionCs,
                                   defaultValue: String.Empty));

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColVersionLabelEn.Name,
                                val: Functions.GetStringArg(
                                   row: row,
                                   name: Core.TargetVariableMetadataJson.ColVersionLabelEn,
                                   defaultValue: String.Empty));

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColVersionDescriptionEn.Name,
                                val: Functions.GetStringArg(
                                   row: row,
                                   name: Core.TargetVariableMetadataJson.ColVersionDescriptionEn,
                                   defaultValue: String.Empty));

                            #endregion Version

                            #region DefinitionVariant

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColDefinitionVariantLabelCs.Name,
                                val: Functions.GetStringArg(
                                  row: row,
                                  name: Core.TargetVariableMetadataJson.ColDefinitionVariantLabelCs,
                                  defaultValue: String.Empty));

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColDefinitionVariantDescriptionCs.Name,
                                val: Functions.GetStringArg(
                                   row: row,
                                   name: Core.TargetVariableMetadataJson.ColDefinitionVariantDescriptionCs,
                                   defaultValue: String.Empty));

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColDefinitionVariantLabelEn.Name,
                                val: Functions.GetStringArg(
                                   row: row,
                                   name: Core.TargetVariableMetadataJson.ColDefinitionVariantLabelEn,
                                   defaultValue: String.Empty));

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColDefinitionVariantDescriptionEn.Name,
                                val: Functions.GetStringArg(
                                   row: row,
                                   name: Core.TargetVariableMetadataJson.ColDefinitionVariantDescriptionEn,
                                   defaultValue: String.Empty));

                            #endregion DefinitionVariant

                            #region AreaDomain

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColAreaDomainLabelCs.Name,
                                val: Functions.GetStringArg(
                                  row: row,
                                  name: Core.TargetVariableMetadataJson.ColAreaDomainLabelCs,
                                  defaultValue: String.Empty));

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColAreaDomainDescriptionCs.Name,
                                val: Functions.GetStringArg(
                                   row: row,
                                   name: Core.TargetVariableMetadataJson.ColAreaDomainDescriptionCs,
                                   defaultValue: String.Empty));

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColAreaDomainLabelEn.Name,
                                val: Functions.GetStringArg(
                                   row: row,
                                   name: Core.TargetVariableMetadataJson.ColAreaDomainLabelEn,
                                   defaultValue: String.Empty));

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColAreaDomainDescriptionEn.Name,
                                val: Functions.GetStringArg(
                                   row: row,
                                   name: Core.TargetVariableMetadataJson.ColAreaDomainDescriptionEn,
                                   defaultValue: String.Empty));

                            #endregion AreaDomain

                            #region Population

                            Functions.SetStringArg(
                               row: newRow,
                               name: ColPopulationLabelCs.Name,
                               val: Functions.GetStringArg(
                                  row: row,
                                  name: Core.TargetVariableMetadataJson.ColPopulationLabelCs,
                                  defaultValue: String.Empty));

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColPopulationDescriptionCs.Name,
                                val: Functions.GetStringArg(
                                   row: row,
                                   name: Core.TargetVariableMetadataJson.ColPopulationDescriptionCs,
                                   defaultValue: String.Empty));

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColPopulationLabelEn.Name,
                                val: Functions.GetStringArg(
                                   row: row,
                                   name: Core.TargetVariableMetadataJson.ColPopulationLabelEn,
                                   defaultValue: String.Empty));

                            Functions.SetStringArg(
                                row: newRow,
                                name: ColPopulationDescriptionEn.Name,
                                val: Functions.GetStringArg(
                                   row: row,
                                   name: Core.TargetVariableMetadataJson.ColPopulationDescriptionEn,
                                   defaultValue: String.Empty));

                            #endregion Population

                            Data.Rows.Add(row: newRow);
                        }
                    }

                    if (!String.IsNullOrEmpty(value: Condition))
                    {
                        DataView view = new(table: Data);
                        try
                        {
                            view.RowFilter = Condition;
                        }
                        catch
                        {
                            view.RowFilter = String.Empty;
                            Condition = String.Empty;
                        }
                        Data = view.ToTable();
                    }

                    stopWatch.Stop();
                    LoadingTime = 0.001 * stopWatch.ElapsedMilliseconds;
                    Data.TableName = TableName;
                }


                /// <summary>
                /// List of target variable for ETL with metadata aggregated by target variable
                /// </summary>
                /// <returns>List of target variable for ETL with metadata aggregated by target variable</returns>
                [SupportedOSPlatform("windows")]
                public TDFnEtlGetTargetVariableMetadataList Aggregated()
                {
                    const string strCore = "core";
                    const string strDivision = "division";

                    if (Items == null)
                    {
                        return new TDFnEtlGetTargetVariableMetadataList(
                            database: (NfiEstaDB)Database);
                    }

                    if (Items.Count == 0)
                    {
                        return new TDFnEtlGetTargetVariableMetadataList(
                            database: (NfiEstaDB)Database);
                    }

                    if (Items
                            .Where(a => (a.LocalDensityObjectTypeLabelEn != strCore) &&
                                         (a.LocalDensityObjectTypeLabelEn != strDivision))
                            .Any())
                    {
                        System.Windows.Forms.MessageBox.Show(
                           text: $"Only local density object types {strCore} or {strDivision} are allowed.",
                           caption: "Invalid json file format:",
                           buttons: System.Windows.Forms.MessageBoxButtons.OK,
                           icon: System.Windows.Forms.MessageBoxIcon.Information);

                        return new TDFnEtlGetTargetVariableMetadataList(
                           database: (NfiEstaDB)Database);
                    }

                    return new TDFnEtlGetTargetVariableMetadataList
                    (
                        database: (NfiEstaDB)Database,
                        rows:
                            Items
                                .GroupBy(a => a.TargetVariableId)
                                .Select(g => new TDFnEtlGetTargetVariableMetadata()
                                {
                                    Id = g.Key,
                                    TargetVariableId = g.Key,
                                    LocalDensityId = 0,
                                    JsonValidity = g.Select(a => a.JsonValidity).Aggregate((aa, ab) => aa && ab),

                                    LabelCs = g.Select(a => ((TDFnEtlGetTargetVariableMetadata)a).LabelCs).FirstOrDefault(),
                                    DescriptionCs = g.Select(a => ((TDFnEtlGetTargetVariableMetadata)a).DescriptionCs).FirstOrDefault(),
                                    LabelEn = g.Select(a => ((TDFnEtlGetTargetVariableMetadata)a).LabelEn).FirstOrDefault(),
                                    DescriptionEn = g.Select(a => ((TDFnEtlGetTargetVariableMetadata)a).DescriptionEn).FirstOrDefault(),
                                    IdEtlTargetVariableMetadata = g.Select(a => ((TDFnEtlGetTargetVariableMetadata)a).IdEtlTargetVariableMetadata).FirstOrDefault(),
                                    CheckTargetVariable = g.Select(a => ((TDFnEtlGetTargetVariableMetadata)a).CheckTargetVariable).FirstOrDefault(),
                                    RefYearSetToPanelMappingString = g.Select(a => ((TDFnEtlGetTargetVariableMetadata)a).RefYearSetToPanelMappingString).FirstOrDefault(),
                                    CheckAtomicAreaDomains = g.Select(a => ((TDFnEtlGetTargetVariableMetadata)a).CheckAtomicAreaDomains).FirstOrDefault(),
                                    CheckAtomicSubPopulations = g.Select(a => ((TDFnEtlGetTargetVariableMetadata)a).CheckAtomicSubPopulations).FirstOrDefault(),

                                    IndicatorLabelCs = g.Select(a => a.IndicatorLabelCs).FirstOrDefault(),
                                    IndicatorDescriptionCs = g.Select(a => a.IndicatorDescriptionCs).FirstOrDefault(),
                                    IndicatorLabelEn = g.Select(a => a.IndicatorLabelEn).FirstOrDefault(),
                                    IndicatorDescriptionEn = g.Select(a => a.IndicatorDescriptionEn).FirstOrDefault(),

                                    StateOrChangeLabelCs = g.Select(a => a.StateOrChangeLabelCs).FirstOrDefault(),
                                    StateOrChangeDescriptionCs = g.Select(a => a.StateOrChangeDescriptionCs).FirstOrDefault(),
                                    StateOrChangeLabelEn = g.Select(a => a.StateOrChangeLabelEn).FirstOrDefault(),
                                    StateOrChangeDescriptionEn = g.Select(a => a.StateOrChangeDescriptionEn).FirstOrDefault(),

                                    UnitLabelCs = g.Select(a => a.UnitLabelCs).FirstOrDefault(),
                                    UnitDescriptionCs = g.Select(a => a.UnitDescriptionCs).FirstOrDefault(),
                                    UnitLabelEn = g.Select(a => a.UnitLabelEn).FirstOrDefault(),
                                    UnitDescriptionEn = g.Select(a => a.UnitDescriptionEn).FirstOrDefault(),

                                    LocalDensityObjectTypeLabelCs =
                                        (g.Where(a => a.LocalDensityObjectTypeLabelEn == strCore).Any() &&
                                         g.Where(a => a.LocalDensityObjectTypeLabelEn == strDivision).Any()) ?
                                        String.Join(
                                            separator: " | ",
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strCore)
                                                .Select(a => a.LocalDensityObjectTypeLabelCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strDivision)
                                                .Select(a => a.LocalDensityObjectTypeLabelCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}")) :
                                            g
                                                .Select(a => a.LocalDensityObjectTypeLabelCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),

                                    LocalDensityObjectTypeDescriptionCs =
                                        (g.Where(a => a.LocalDensityObjectTypeLabelEn == strCore).Any() &&
                                         g.Where(a => a.LocalDensityObjectTypeLabelEn == strDivision).Any()) ?
                                        String.Join(
                                            separator: " | ",
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strCore)
                                                .Select(a => a.LocalDensityObjectTypeDescriptionCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strDivision)
                                                .Select(a => a.LocalDensityObjectTypeDescriptionCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}")) :
                                            g
                                                .Select(a => a.LocalDensityObjectTypeDescriptionCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),

                                    LocalDensityObjectTypeLabelEn =
                                        (g.Where(a => a.LocalDensityObjectTypeLabelEn == strCore).Any() &&
                                         g.Where(a => a.LocalDensityObjectTypeLabelEn == strDivision).Any()) ?
                                        String.Join(
                                            separator: " | ",
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strCore)
                                                .Select(a => a.LocalDensityObjectTypeLabelEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strDivision)
                                                .Select(a => a.LocalDensityObjectTypeLabelEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}")) :
                                            g
                                                .Select(a => a.LocalDensityObjectTypeLabelEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),

                                    LocalDensityObjectTypeDescriptionEn =
                                        (g.Where(a => a.LocalDensityObjectTypeLabelEn == strCore).Any() &&
                                         g.Where(a => a.LocalDensityObjectTypeLabelEn == strDivision).Any()) ?
                                        String.Join(
                                            separator: " | ",
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strCore)
                                                .Select(a => a.LocalDensityObjectTypeDescriptionEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strDivision)
                                                .Select(a => a.LocalDensityObjectTypeDescriptionEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}")) :
                                            g
                                                .Select(a => a.LocalDensityObjectTypeDescriptionEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),

                                    LocalDensityObjectLabelCs =
                                        (g.Where(a => a.LocalDensityObjectTypeLabelEn == strCore).Any() &&
                                         g.Where(a => a.LocalDensityObjectTypeLabelEn == strDivision).Any()) ?
                                        String.Join(
                                            separator: " | ",
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strCore)
                                                .Select(a => a.LocalDensityObjectLabelCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strDivision)
                                                .Select(a => a.LocalDensityObjectLabelCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}")) :
                                            g
                                                .Select(a => a.LocalDensityObjectLabelCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),

                                    LocalDensityObjectDescriptionCs =
                                        (g.Where(a => a.LocalDensityObjectTypeLabelEn == strCore).Any() &&
                                         g.Where(a => a.LocalDensityObjectTypeLabelEn == strDivision).Any()) ?
                                        String.Join(
                                            separator: " | ",
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strCore)
                                                .Select(a => a.LocalDensityObjectDescriptionCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strDivision)
                                                .Select(a => a.LocalDensityObjectDescriptionCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}")) :
                                            g
                                                .Select(a => a.LocalDensityObjectDescriptionCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),

                                    LocalDensityObjectLabelEn =
                                        (g.Where(a => a.LocalDensityObjectTypeLabelEn == strCore).Any() &&
                                         g.Where(a => a.LocalDensityObjectTypeLabelEn == strDivision).Any()) ?
                                        String.Join(
                                            separator: " | ",
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strCore)
                                                .Select(a => a.LocalDensityObjectLabelEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strDivision)
                                                .Select(a => a.LocalDensityObjectLabelEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}")) :
                                            g
                                                .Select(a => a.LocalDensityObjectLabelEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),

                                    LocalDensityObjectDescriptionEn =
                                        (g.Where(a => a.LocalDensityObjectTypeLabelEn == strCore).Any() &&
                                         g.Where(a => a.LocalDensityObjectTypeLabelEn == strDivision).Any()) ?
                                        String.Join(
                                            separator: " | ",
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strCore)
                                                .Select(a => a.LocalDensityObjectDescriptionEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strDivision)
                                                .Select(a => a.LocalDensityObjectDescriptionEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}")) :
                                            g
                                                .Select(a => a.LocalDensityObjectDescriptionEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),

                                    LocalDensityLabelCs =
                                        (g.Where(a => a.LocalDensityObjectTypeLabelEn == strCore).Any() &&
                                         g.Where(a => a.LocalDensityObjectTypeLabelEn == strDivision).Any()) ?
                                        String.Join(
                                            separator: " | ",
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strCore)
                                                .Select(a => a.LocalDensityLabelCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strDivision)
                                                .Select(a => a.LocalDensityLabelCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}")) :
                                            g
                                                .Select(a => a.LocalDensityLabelCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),

                                    LocalDensityDescriptionCs =
                                        (g.Where(a => a.LocalDensityObjectTypeLabelEn == strCore).Any() &&
                                         g.Where(a => a.LocalDensityObjectTypeLabelEn == strDivision).Any()) ?
                                        String.Join(
                                            separator: " | ",
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strCore)
                                                .Select(a => a.LocalDensityDescriptionCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strDivision)
                                                .Select(a => a.LocalDensityDescriptionCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}")) :
                                            g
                                                .Select(a => a.LocalDensityDescriptionCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),

                                    LocalDensityLabelEn =
                                        (g.Where(a => a.LocalDensityObjectTypeLabelEn == strCore).Any() &&
                                         g.Where(a => a.LocalDensityObjectTypeLabelEn == strDivision).Any()) ?
                                        String.Join(
                                            separator: " | ",
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strCore)
                                                .Select(a => a.LocalDensityLabelEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strDivision)
                                                .Select(a => a.LocalDensityLabelEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}")) :
                                            g
                                                .Select(a => a.LocalDensityLabelEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),

                                    LocalDensityDescriptionEn =
                                        (g.Where(a => a.LocalDensityObjectTypeLabelEn == strCore).Any() &&
                                         g.Where(a => a.LocalDensityObjectTypeLabelEn == strDivision).Any()) ?
                                        String.Join(
                                            separator: " | ",
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strCore)
                                                .Select(a => a.LocalDensityDescriptionEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strDivision)
                                                .Select(a => a.LocalDensityDescriptionEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}")) :
                                            g
                                                .Select(a => a.LocalDensityDescriptionEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),

                                    VersionLabelCs =
                                        (g.Where(a => a.LocalDensityObjectTypeLabelEn == strCore).Any() &&
                                         g.Where(a => a.LocalDensityObjectTypeLabelEn == strDivision).Any()) ?
                                        String.Join(
                                            separator: " | ",
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strCore)
                                                .Select(a => a.VersionLabelCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strDivision)
                                                .Select(a => a.VersionLabelCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}")) :
                                            g
                                                .Select(a => a.VersionLabelCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),

                                    VersionDescriptionCs =
                                        (g.Where(a => a.LocalDensityObjectTypeLabelEn == strCore).Any() &&
                                         g.Where(a => a.LocalDensityObjectTypeLabelEn == strDivision).Any()) ?
                                        String.Join(
                                            separator: " | ",
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strCore)
                                                .Select(a => a.VersionDescriptionCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strDivision)
                                                .Select(a => a.VersionDescriptionCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}")) :
                                            g
                                                .Select(a => a.VersionDescriptionCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),

                                    VersionLabelEn =
                                        (g.Where(a => a.LocalDensityObjectTypeLabelEn == strCore).Any() &&
                                         g.Where(a => a.LocalDensityObjectTypeLabelEn == strDivision).Any()) ?
                                        String.Join(
                                            separator: " | ",
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strCore)
                                                .Select(a => a.VersionLabelEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strDivision)
                                                .Select(a => a.VersionLabelEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}")) :
                                            g
                                                .Select(a => a.VersionLabelEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),

                                    VersionDescriptionEn =
                                        (g.Where(a => a.LocalDensityObjectTypeLabelEn == strCore).Any() &&
                                         g.Where(a => a.LocalDensityObjectTypeLabelEn == strDivision).Any()) ?
                                        String.Join(
                                            separator: " | ",
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strCore)
                                                .Select(a => a.VersionDescriptionEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strDivision)
                                                .Select(a => a.VersionDescriptionEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}")) :
                                            g
                                                .Select(a => a.VersionDescriptionEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),

                                    DefinitionVariantLabelCs =
                                        (g.Where(a => a.LocalDensityObjectTypeLabelEn == strCore).Any() &&
                                         g.Where(a => a.LocalDensityObjectTypeLabelEn == strDivision).Any()) ?
                                        String.Join(
                                            separator: " | ",
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strCore)
                                                .Select(a => a.DefinitionVariantLabelCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strDivision)
                                                .Select(a => a.DefinitionVariantLabelCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}")) :
                                            g
                                                .Select(a => a.DefinitionVariantLabelCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),

                                    DefinitionVariantDescriptionCs =
                                        (g.Where(a => a.LocalDensityObjectTypeLabelEn == strCore).Any() &&
                                         g.Where(a => a.LocalDensityObjectTypeLabelEn == strDivision).Any()) ?
                                        String.Join(
                                            separator: " | ",
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strCore)
                                                .Select(a => a.DefinitionVariantDescriptionCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strDivision)
                                                .Select(a => a.DefinitionVariantDescriptionCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}")) :
                                            g
                                                .Select(a => a.DefinitionVariantDescriptionCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),

                                    DefinitionVariantLabelEn =
                                        (g.Where(a => a.LocalDensityObjectTypeLabelEn == strCore).Any() &&
                                         g.Where(a => a.LocalDensityObjectTypeLabelEn == strDivision).Any()) ?
                                        String.Join(
                                            separator: " | ",
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strCore)
                                                .Select(a => a.DefinitionVariantLabelEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strDivision)
                                                .Select(a => a.DefinitionVariantLabelEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}")) :
                                            g
                                                .Select(a => a.DefinitionVariantLabelEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),

                                    DefinitionVariantDescriptionEn =
                                        (g.Where(a => a.LocalDensityObjectTypeLabelEn == strCore).Any() &&
                                         g.Where(a => a.LocalDensityObjectTypeLabelEn == strDivision).Any()) ?
                                        String.Join(
                                            separator: " | ",
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strCore)
                                                .Select(a => a.DefinitionVariantDescriptionEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strDivision)
                                                .Select(a => a.DefinitionVariantDescriptionEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}")) :
                                            g
                                                .Select(a => a.DefinitionVariantDescriptionEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),

                                    AreaDomainLabelCs =
                                        (g.Where(a => a.LocalDensityObjectTypeLabelEn == strCore).Any() &&
                                         g.Where(a => a.LocalDensityObjectTypeLabelEn == strDivision).Any()) ?
                                        String.Join(
                                            separator: " | ",
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strCore)
                                                .Select(a => a.AreaDomainLabelCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strDivision)
                                                .Select(a => a.AreaDomainLabelCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}")) :
                                            g
                                                .Select(a => a.AreaDomainLabelCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),

                                    AreaDomainDescriptionCs =
                                        (g.Where(a => a.LocalDensityObjectTypeLabelEn == strCore).Any() &&
                                         g.Where(a => a.LocalDensityObjectTypeLabelEn == strDivision).Any()) ?
                                        String.Join(
                                            separator: " | ",
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strCore)
                                                .Select(a => a.AreaDomainDescriptionCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strDivision)
                                                .Select(a => a.AreaDomainDescriptionCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}")) :
                                            g
                                                .Select(a => a.AreaDomainDescriptionCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),

                                    AreaDomainLabelEn =
                                        (g.Where(a => a.LocalDensityObjectTypeLabelEn == strCore).Any() &&
                                         g.Where(a => a.LocalDensityObjectTypeLabelEn == strDivision).Any()) ?
                                        String.Join(
                                            separator: " | ",
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strCore)
                                                .Select(a => a.AreaDomainLabelEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strDivision)
                                                .Select(a => a.AreaDomainLabelEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}")) :
                                            g
                                                .Select(a => a.AreaDomainLabelEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),

                                    AreaDomainDescriptionEn =
                                        (g.Where(a => a.LocalDensityObjectTypeLabelEn == strCore).Any() &&
                                         g.Where(a => a.LocalDensityObjectTypeLabelEn == strDivision).Any()) ?
                                        String.Join(
                                            separator: " | ",
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strCore)
                                                .Select(a => a.AreaDomainDescriptionEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strDivision)
                                                .Select(a => a.AreaDomainDescriptionEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}")) :
                                            g
                                                .Select(a => a.AreaDomainDescriptionEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),

                                    PopulationLabelCs =
                                        (g.Where(a => a.LocalDensityObjectTypeLabelEn == strCore).Any() &&
                                         g.Where(a => a.LocalDensityObjectTypeLabelEn == strDivision).Any()) ?
                                        String.Join(
                                            separator: " | ",
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strCore)
                                                .Select(a => a.PopulationLabelCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strDivision)
                                                .Select(a => a.PopulationLabelCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}")) :
                                            g
                                                .Select(a => a.PopulationLabelCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),

                                    PopulationDescriptionCs =
                                        (g.Where(a => a.LocalDensityObjectTypeLabelEn == strCore).Any() &&
                                         g.Where(a => a.LocalDensityObjectTypeLabelEn == strDivision).Any()) ?
                                        String.Join(
                                            separator: " | ",
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strCore)
                                                .Select(a => a.PopulationDescriptionCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strDivision)
                                                .Select(a => a.PopulationDescriptionCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}")) :
                                            g
                                                .Select(a => a.PopulationDescriptionCs)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),

                                    PopulationLabelEn =
                                        (g.Where(a => a.LocalDensityObjectTypeLabelEn == strCore).Any() &&
                                         g.Where(a => a.LocalDensityObjectTypeLabelEn == strDivision).Any()) ?
                                        String.Join(
                                            separator: " | ",
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strCore)
                                                .Select(a => a.PopulationLabelEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strDivision)
                                                .Select(a => a.PopulationLabelEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}")) :
                                            g
                                                .Select(a => a.PopulationLabelEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),

                                    PopulationDescriptionEn =
                                        (g.Where(a => a.LocalDensityObjectTypeLabelEn == strCore).Any() &&
                                         g.Where(a => a.LocalDensityObjectTypeLabelEn == strDivision).Any()) ?
                                        String.Join(
                                            separator: " | ",
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strCore)
                                                .Select(a => a.PopulationLabelEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}"),
                                            g
                                                .Where(a => a.LocalDensityObjectTypeLabelEn == strDivision)
                                                .Select(a => a.PopulationLabelEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}")) :
                                            g
                                                .Select(a => a.PopulationLabelEn)
                                                .Aggregate((aa, ab) => $"{aa} + {ab}")
                                })
                                .Select(a => a.Data)
                    );
                }

                #endregion Methods

            }

        }
    }
}