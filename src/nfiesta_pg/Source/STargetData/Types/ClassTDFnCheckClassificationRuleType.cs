﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {
            // fn_check_classification_rule

            /// <summary>
            /// Number of objects classified by the rule
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class TDFnCheckClassificationRuleType(
                TDFnCheckClassificationRuleTypeList composite = null,
                DataRow data = null)
                    : AParametrizedViewEntry<TDFnCheckClassificationRuleTypeList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Number of objects classified by the rule identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TDFnCheckClassificationRuleTypeList.ColId.Name,
                            defaultValue: Int32.Parse(s: TDFnCheckClassificationRuleTypeList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TDFnCheckClassificationRuleTypeList.ColId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Number of rules met
                /// </summary>
                public Nullable<bool> ComplianceStatus
                {
                    get
                    {
                        return Functions.GetNBoolArg(
                            row: Data,
                            name: TDFnCheckClassificationRuleTypeList.ColComplianceStatus.Name,
                            defaultValue:
                                String.IsNullOrEmpty(value: TDFnCheckClassificationRuleTypeList.ColComplianceStatus.DefaultValue) ?
                                (Nullable<bool>)null :
                                Boolean.Parse(value: TDFnCheckClassificationRuleTypeList.ColComplianceStatus.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNBoolArg(
                            row: Data,
                            name: TDFnCheckClassificationRuleTypeList.ColComplianceStatus.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Number of objects
                /// </summary>
                public Nullable<int> NumberOfObjects
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TDFnCheckClassificationRuleTypeList.ColNumberOfObjects.Name,
                            defaultValue:
                                String.IsNullOrEmpty(value: TDFnCheckClassificationRuleTypeList.ColNumberOfObjects.DefaultValue) ?
                                    (Nullable<int>)null :
                                    Int32.Parse(s: TDFnCheckClassificationRuleTypeList.ColNumberOfObjects.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TDFnCheckClassificationRuleTypeList.ColNumberOfObjects.Name,
                            val: value);
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not TDFnCheckClassificationRuleType, return False
                    if (obj is not TDFnCheckClassificationRuleType)
                    {
                        return false;
                    }

                    return
                        Id == ((TDFnCheckClassificationRuleType)obj).Id;
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                /// <summary>
                /// Text description of number of objects classified by the rule
                /// </summary>
                /// <returns>Text description of number of objects classified by the rule</returns>
                public override string ToString()
                {
                    return ((NfiEstaDB)Composite.Database).Postgres.Setting.LanguageVersion switch
                    {
                        LanguageVersion.National =>
                            String.Concat(
                                $"{"ID"} = {Functions.PrepIntArg(Id)}.{Environment.NewLine}"
                                ),
                        LanguageVersion.International =>
                            String.Concat(
                                $"{"ID"} = {Functions.PrepIntArg(Id)}.{Environment.NewLine}"
                                ),
                        _ =>
                            String.Concat(
                                $"{"ID"} = {Functions.PrepIntArg(Id)}.{Environment.NewLine}"
                                ),
                    };
                }

                #endregion Methods

            }


            /// <summary>
            /// List of number of objects classified by the rule
            /// </summary>
            public class TDFnCheckClassificationRuleTypeList
                : AParametrizedView
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema = TDSchema.Name;

                /// <summary>
                /// Stored procedure name
                /// </summary>
                public const string Name = "fn_check_classification_rule";

                /// <summary>
                /// Stored procedure alias
                /// </summary>
                public const string Alias = "fn_check_classification_rule";

                /// <summary>
                /// Description in national language
                /// </summary>
                public const string CaptionCs = "Počet objektů klasifikovaných pravidlem pro daný příspěvek lokální hustoty a hierarchii atributového členění.";

                /// <summary>
                /// Description in English
                /// </summary>
                public const string CaptionEn = "Number of objects classified by the rule for given ldsity and attribute_type hierarchy.";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
                {
                    { "id", new ColumnMetadata()
                    {
                        Name = "id",
                        DbName = "id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = true,
                        NotNull = true,
                        DefaultValue = "0",
                        FuncCall = "row_number() over()",
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ID",
                        HeaderTextEn = "ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 0
                    }
                    },
                    { "compliance_status", new ColumnMetadata()
                    {
                        Name = "compliance_status",
                        DbName = "compliance_status",
                        DataType = "System.Boolean",
                        DbDataType = "bool",
                        NewDataType = "bool",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "COMPLIANCE_STATUS",
                        HeaderTextEn = "COMPLIANCE_STATUS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 1
                    }
                    },
                    { "no_of_objects", new ColumnMetadata()
                    {
                        Name = "no_of_objects",
                        DbName = "no_of_objects",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "NO_OF_OBJECTS",
                        HeaderTextEn = "NO_OF_OBJECTS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 2
                    }
                    }
                };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column compliance_status metadata
                /// </summary>
                public static readonly ColumnMetadata ColComplianceStatus = Cols["compliance_status"];

                /// <summary>
                /// Column no_of_objects metadata
                /// </summary>
                public static readonly ColumnMetadata ColNumberOfObjects = Cols["no_of_objects"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Private Fields

                /// <summary>
                /// 1st parameter: Local density contribution identifier
                /// </summary>
                private Nullable<int> ldsityId;

                /// <summary>
                /// 2nd parameter: Local density object identifier
                /// </summary>
                private Nullable<int> ldsityObjectId;

                /// <summary>
                /// 3rd parameter: Classification rule text
                /// </summary>
                private string rule;

                /// <summary>
                /// 4th parameter: Pair of panel with corresponding reference year set identifier
                /// </summary>
                private Nullable<int> panelRefYearSetId;

                /// <summary>
                /// 5th parameter: Area domain categories
                /// </summary>
                private List<List<Nullable<int>>> adc;

                /// <summary>
                /// 6th parameter: Subpopulation categories
                /// </summary>
                private List<List<Nullable<int>>> spc;

                /// <summary>
                /// 7th parameter: Positive (false) or negative (true) classification rule
                /// </summary>
                public Nullable<bool> useNegative;

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TDFnCheckClassificationRuleTypeList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                {
                    LDsityId = null;
                    LDsityObjectId = null;
                    Rule = String.Empty;
                    PanelRefYearSetId = null;
                    Adc = [];
                    Spc = [];
                    UseNegative = null;
                }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TDFnCheckClassificationRuleTypeList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                {
                    LDsityId = null;
                    LDsityObjectId = null;
                    Rule = String.Empty;
                    PanelRefYearSetId = null;
                    Adc = [];
                    Spc = [];
                    UseNegative = null;
                }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// 1st parameter: Local density contribution identifier
                /// </summary>
                public Nullable<int> LDsityId
                {
                    get
                    {
                        return ldsityId;
                    }
                    set
                    {
                        ldsityId = value;
                    }
                }

                /// <summary>
                /// 2nd parameter: Local density object identifier
                /// </summary>
                public Nullable<int> LDsityObjectId
                {
                    get
                    {
                        return ldsityObjectId;
                    }
                    set
                    {
                        ldsityObjectId = value;
                    }
                }

                /// <summary>
                /// 3rd parameter: Classification rule text
                /// </summary>
                public string Rule
                {
                    get
                    {
                        return rule;
                    }
                    set
                    {
                        rule = value;
                    }
                }

                /// <summary>
                /// 4th parameter: Pair of panel with corresponding reference year set identifier
                /// </summary>
                public Nullable<int> PanelRefYearSetId
                {
                    get
                    {
                        return panelRefYearSetId;
                    }
                    set
                    {
                        panelRefYearSetId = value;
                    }
                }

                /// <summary>
                /// 5th parameter: Area domain categories
                /// </summary>
                public List<List<Nullable<int>>> Adc
                {
                    get
                    {
                        return adc;
                    }
                    set
                    {
                        adc = value;
                    }
                }

                /// <summary>
                /// 6th parameter: Subpopulation categories
                /// </summary>
                public List<List<Nullable<int>>> Spc
                {
                    get
                    {
                        return spc;
                    }
                    set
                    {
                        spc = value;
                    }
                }

                /// <summary>
                /// 7th parameter: Positive (false) or negative (true) classification rule
                /// </summary>
                public Nullable<bool> UseNegative
                {
                    get
                    {
                        return useNegative;
                    }
                    set
                    {
                        useNegative = value;
                    }
                }

                /// <summary>
                /// List of number of objects classified by the rule (read-only)
                /// </summary>
                public List<TDFnCheckClassificationRuleType> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new TDFnCheckClassificationRuleType(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexers

                /// <summary>
                /// Number of objects classified by the rule from list by identifier (read-only)
                /// </summary>
                /// <param name="id">Number of objects classified by the rule identifier</param>
                /// <returns>Number of objects classified by the rule from list by identifier (null if not found)</returns>
                public TDFnCheckClassificationRuleType this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new TDFnCheckClassificationRuleType(composite: this, data: a))
                                .FirstOrDefault<TDFnCheckClassificationRuleType>();
                    }
                }

                #endregion Indexers


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public TDFnCheckClassificationRuleTypeList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new TDFnCheckClassificationRuleTypeList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime)
                        {
                            LDsityId = LDsityId,
                            LDsityObjectId = LDsityObjectId,
                            Rule = Rule,
                            PanelRefYearSetId = PanelRefYearSetId,
                            Adc = [.. Adc],
                            Spc = [.. Spc],
                            UseNegative = UseNegative
                        } :
                        new TDFnCheckClassificationRuleTypeList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null)
                        {
                            LDsityId = LDsityId,
                            LDsityObjectId = LDsityObjectId,
                            Rule = Rule,
                            PanelRefYearSetId = PanelRefYearSetId,
                            Adc = [.. Adc],
                            Spc = [.. Spc],
                            UseNegative = UseNegative
                        };
                }

                #endregion Methods

            }

        }
    }
}
