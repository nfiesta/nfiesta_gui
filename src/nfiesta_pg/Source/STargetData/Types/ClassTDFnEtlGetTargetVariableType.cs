﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {
            // fn_etl_get_target_variable

            /// <summary>
            /// Target variable for ETL
            /// (in the TargetData schema)
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class TDFnEtlGetTargetVariableType(
                TDFnEtlGetTargetVariableTypeList composite = null,
                DataRow data = null)
                    : AParametrizedViewEntry<TDFnEtlGetTargetVariableTypeList>(composite: composite, data: data),
                      ITargetVariable<TDFnEtlGetTargetVariableTypeList>
            {

                #region Derived Properties

                /// <summary>
                /// Target variable identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableTypeList.ColId.Name,
                            defaultValue: Int32.Parse(s: TDFnEtlGetTargetVariableTypeList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableTypeList.ColId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Metadata of the target variable (json format as text)
                /// </summary>
                public string MetadataText
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableTypeList.ColMetadata.Name,
                            defaultValue: TDFnEtlGetTargetVariableTypeList.ColMetadata.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableTypeList.ColMetadata.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Metadata of the target variable (json format as object)
                /// </summary>
                public Core.TargetVariableMetadataJson MetadataObject
                {
                    get
                    {
                        Core.TargetVariableMetadataJson json = new();
                        json.LoadFromText(text: MetadataText);
                        return json;
                    }
                }


                /// <summary>
                /// Label in national language
                /// </summary>
                public string LabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableTypeList.ColLabelCs.Name,
                            defaultValue: TDFnEtlGetTargetVariableTypeList.ColLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableTypeList.ColLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Description in national language
                /// </summary>
                public string DescriptionCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableTypeList.ColDescriptionCs.Name,
                            defaultValue: TDFnEtlGetTargetVariableTypeList.ColDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableTypeList.ColDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Extended label in national language (read-only)
                /// </summary>
                public string ExtendedLabelCs
                {
                    get
                    {
                        return $"{Id} - {LabelCs}";
                    }
                }


                /// <summary>
                /// Label in international language
                /// </summary>
                public string LabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableTypeList.ColLabelEn.Name,
                            defaultValue: TDFnEtlGetTargetVariableTypeList.ColLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableTypeList.ColLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Description in international language
                /// </summary>
                public string DescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableTypeList.ColDescriptionEn.Name,
                            defaultValue: TDFnEtlGetTargetVariableTypeList.ColDescriptionEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableTypeList.ColDescriptionEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Extended label in international language (read-only)
                /// </summary>
                public string ExtendedLabelEn
                {
                    get
                    {
                        return $"{Id} - {LabelEn}";
                    }
                }


                /// <summary>
                /// IdEtlTargetVariableMetadata
                /// </summary>
                public Nullable<int> IdEtlTargetVariableMetadata
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableTypeList.ColIdEtlTargetVariableMetadata.Name,
                            defaultValue: String.IsNullOrEmpty(value: TDFnEtlGetTargetVariableTypeList.ColIdEtlTargetVariableMetadata.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TDFnEtlGetTargetVariableTypeList.ColIdEtlTargetVariableMetadata.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableTypeList.ColIdEtlTargetVariableMetadata.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// CheckTargetVariable
                /// </summary>
                public Nullable<bool> CheckTargetVariable
                {
                    get
                    {
                        return Functions.GetNBoolArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableTypeList.ColCheckTargetVariable.Name,
                            defaultValue: String.IsNullOrEmpty(value: TDFnEtlGetTargetVariableTypeList.ColCheckTargetVariable.DefaultValue) ?
                                (Nullable<bool>)null :
                                Boolean.Parse(value: TDFnEtlGetTargetVariableTypeList.ColCheckTargetVariable.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNBoolArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableTypeList.ColCheckTargetVariable.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// RefYearSetToPanelMapping
                /// </summary>
                public string RefYearSetToPanelMappingString
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableTypeList.ColRefYearSetToPanelMapping.Name,
                            defaultValue: TDFnEtlGetTargetVariableTypeList.ColRefYearSetToPanelMapping.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableTypeList.ColRefYearSetToPanelMapping.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// CheckAtomicAreaDomains
                /// </summary>
                public Nullable<bool> CheckAtomicAreaDomains
                {
                    get
                    {
                        return Functions.GetNBoolArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableTypeList.ColCheckAtomicAreaDomains.Name,
                            defaultValue: String.IsNullOrEmpty(value: TDFnEtlGetTargetVariableTypeList.ColCheckAtomicAreaDomains.DefaultValue) ?
                                (Nullable<bool>)null :
                                Boolean.Parse(value: TDFnEtlGetTargetVariableTypeList.ColCheckAtomicAreaDomains.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNBoolArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableTypeList.ColCheckAtomicAreaDomains.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// CheckAtomicSubPopulations
                /// </summary>
                public Nullable<bool> CheckAtomicSubPopulations
                {
                    get
                    {
                        return Functions.GetNBoolArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableTypeList.ColCheckAtomicSubPopulations.Name,
                            defaultValue: String.IsNullOrEmpty(value: TDFnEtlGetTargetVariableTypeList.ColCheckAtomicSubPopulations.DefaultValue) ?
                                (Nullable<bool>)null :
                                Boolean.Parse(value: TDFnEtlGetTargetVariableTypeList.ColCheckAtomicSubPopulations.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNBoolArg(
                            row: Data,
                            name: TDFnEtlGetTargetVariableTypeList.ColCheckAtomicSubPopulations.Name,
                            val: value);
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not TDFnEtlGetTargetVariableType Type, return False
                    if (obj is not TDFnEtlGetTargetVariableType)
                    {
                        return false;
                    }

                    return
                        Id == ((TDFnEtlGetTargetVariableType)obj).Id;
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                /// <summary>
                /// Text description of the object
                /// </summary>
                /// <returns>Text description of the objectt</returns>
                public override string ToString()
                {
                    return ((NfiEstaDB)Composite.Database).Postgres.Setting.LanguageVersion switch
                    {
                        LanguageVersion.National =>
                            String.Concat(
                                $"{"ID"} = {Functions.PrepIntArg(arg: Id)}.{Environment.NewLine}"
                                ),
                        LanguageVersion.International =>
                            String.Concat(
                                $"{"ID"} = {Functions.PrepIntArg(arg: Id)}.{Environment.NewLine}"
                                ),
                        _ =>
                            String.Concat(
                                $"{"ID"} = {Functions.PrepIntArg(arg: Id)}.{Environment.NewLine}"
                                ),
                    };
                }

                #endregion Methods

            }


            /// <summary>
            /// List of target variables for ETL
            /// (in the TargetData schema)
            /// </summary>
            public class TDFnEtlGetTargetVariableTypeList
                 : AParametrizedView, ITargetVariableList
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema = TDSchema.Name;

                /// <summary>
                /// Table name
                /// </summary>
                public static readonly string Name = TDFunctions.FnEtlGetTargetVariable.Name;

                /// <summary>
                /// Table alias
                /// </summary>
                public static readonly string Alias = TDFunctions.FnEtlGetTargetVariable.Name;

                /// <summary>
                /// Description in national language
                /// </summary>
                public const string CaptionCs = "Cílová proměnná pro ETL";

                /// <summary>
                /// Description in English
                /// </summary>
                public const string CaptionEn = "Target variable for ETL";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
                {
                    { "id", new ColumnMetadata()
                    {
                        Name = "id",
                        DbName = "id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = true,
                        NotNull = true,
                        DefaultValue = "0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ID",
                        HeaderTextEn = "ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 0
                    }
                    },
                    { "label", new ColumnMetadata()
                    {
                        Name = "label",
                        DbName = "label",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "LABEL",
                        HeaderTextEn = "LABEL",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 1
                    }
                    },
                    { "description", new ColumnMetadata()
                    {
                        Name = "description",
                        DbName = "description",
                        DataType = "System.String",
                        DbDataType = "text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "DESCRIPTION",
                        HeaderTextEn = "DESCRIPTION",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 2
                    }
                    },
                    { "label_en", new ColumnMetadata()
                    {
                        Name = "label_en",
                        DbName = "label_en",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "LABEL_EN",
                        HeaderTextEn = "LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 3
                    }
                    },
                    { "description_en", new ColumnMetadata()
                    {
                        Name = "description_en",
                        DbName = "description_en",
                        DataType = "System.String",
                        DbDataType = "text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "DESCRIPTION",
                        HeaderTextEn = "DESCRIPTION",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 4
                    }
                    },
                    { "id_etl_target_variable", new ColumnMetadata()
                    {
                        Name = "id_etl_target_variable",
                        DbName = "id_etl_target_variable",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ID_ETL_TARGET_VARIABLE",
                        HeaderTextEn = "ID_ETL_TARGET_VARIABLE",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 5
                    }
                    },
                    { "check_target_variable", new ColumnMetadata()
                    {
                        Name = "check_target_variable",
                        DbName = "check_target_variable",
                        DataType = "System.Boolean",
                        DbDataType = "bool",
                        NewDataType = "bool",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "CHECK_TARGET_VARIABLE",
                        HeaderTextEn = "CHECK_TARGET_VARIABLE",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 6
                    }
                    },
                    { "refyearset2panel_mapping", new ColumnMetadata()
                    {
                        Name = "refyearset2panel_mapping",
                        DbName = "refyearset2panel_mapping",
                        DataType = "System.String",
                        DbDataType = "_int4",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = "array_to_string({0}, ';', 'NULL')",
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "REF_YEAR_SET_TO_PANEL_MAPPING",
                        HeaderTextEn = "REF_YEAR_SET_TO_PANEL_MAPPING",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 7
                    }
                    },
                    { "check_atomic_area_domains", new ColumnMetadata()
                    {
                        Name = "check_atomic_area_domains",
                        DbName = "check_atomic_area_domains",
                        DataType = "System.Boolean",
                        DbDataType = "bool",
                        NewDataType = "bool",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "CHECK_ATOMIC_AREA_DOMAINS",
                        HeaderTextEn = "CHECK_ATOMIC_AREA_DOMAINS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 8
                    }
                    },
                    { "check_atomic_sub_populations", new ColumnMetadata()
                    {
                        Name = "check_atomic_sub_populations",
                        DbName = "check_atomic_sub_populations",
                        DataType = "System.Boolean",
                        DbDataType = "bool",
                        NewDataType = "bool",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "CHECK_ATOMIC_SUB_POPULATIONS",
                        HeaderTextEn = "CHECK_ATOMIC_SUB_POPULATIONS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 9
                    }
                    },
                    { "metadata", new ColumnMetadata()
                    {
                        Name = "metadata",
                        DbName = "metadata",
                        DataType = "System.String",
                        DbDataType = "json",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "METADATA",
                        HeaderTextEn = "METADATA",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 10
                    }
                    }
                };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column label metadata
                /// </summary>
                public static readonly ColumnMetadata ColLabelCs = Cols["label"];

                /// <summary>
                /// Column description metadata
                /// </summary>
                public static readonly ColumnMetadata ColDescriptionCs = Cols["description"];

                /// <summary>
                /// Column label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColLabelEn = Cols["label_en"];

                /// <summary>
                /// Column description_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColDescriptionEn = Cols["description_en"];

                /// <summary>
                /// Column id_etl_target_variable metadata
                /// </summary>
                public static readonly ColumnMetadata ColIdEtlTargetVariableMetadata = Cols["id_etl_target_variable"];

                /// <summary>
                /// Column check_target_variable metadata
                /// </summary>
                public static readonly ColumnMetadata ColCheckTargetVariable = Cols["check_target_variable"];

                /// <summary>
                /// Column refyearset2panel_mapping metadata
                /// </summary>
                public static readonly ColumnMetadata ColRefYearSetToPanelMapping = Cols["refyearset2panel_mapping"];

                /// <summary>
                /// Column check_atomic_area_domains metadata
                /// </summary>
                public static readonly ColumnMetadata ColCheckAtomicAreaDomains = Cols["check_atomic_area_domains"];

                /// <summary>
                /// Column check_atomic_sub_populations metadata
                /// </summary>
                public static readonly ColumnMetadata ColCheckAtomicSubPopulations = Cols["check_atomic_sub_populations"];

                /// <summary>
                /// Column metadata
                /// </summary>
                public static readonly ColumnMetadata ColMetadata = Cols["metadata"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Private Fields

                /// <summary>
                /// 1st parameter
                /// </summary>
                private Nullable<int> exportConnection;

                /// <summary>
                /// 2nd parameter
                /// </summary>
                private Language nationalLanguage;

                /// <summary>
                /// 3rd parameter
                /// </summary>
                private Nullable<bool> etl;

                /// <summary>
                /// 4th parameter
                /// </summary>
                private Nullable<int> targetVariable;

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TDFnEtlGetTargetVariableTypeList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                {
                    ExportConnection = null;
                    NationalLanguage = Language.CS;
                    Etl = null;
                    TargetVariable = null;
                }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TDFnEtlGetTargetVariableTypeList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                {
                    ExportConnection = null;
                    NationalLanguage = Language.CS;
                    Etl = null;
                    TargetVariable = null;
                }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// 1st parameter
                /// </summary>
                public Nullable<int> ExportConnection
                {
                    get
                    {
                        return exportConnection;
                    }
                    set
                    {
                        exportConnection = value;
                    }
                }

                /// <summary>
                /// 2nd parameter
                /// </summary>
                public Language NationalLanguage
                {
                    get
                    {
                        return nationalLanguage;
                    }
                    set
                    {
                        nationalLanguage = value;
                    }
                }

                /// <summary>
                /// 3rd parameter
                /// </summary>
                public Nullable<bool> Etl
                {
                    get
                    {
                        return etl;
                    }
                    set
                    {
                        etl = value;
                    }
                }

                /// <summary>
                /// 4th parameter
                /// </summary>
                public Nullable<int> TargetVariable
                {
                    get
                    {
                        return targetVariable;
                    }
                    set
                    {
                        targetVariable = value;
                    }
                }

                /// <summary>
                /// List of target variables for ETL
                /// (in the TargetData schema)
                /// (read-only)
                /// </summary>
                public List<ITargetVariable> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new TDFnEtlGetTargetVariableType(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// Target variable for ETL from list by identifier (read-only)
                /// </summary>
                /// <param name="id">Target variable for ETL identifier</param>
                /// <returns>Target variable for ETL from list by identifier (null if not found)</returns>
                public ITargetVariable this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new TDFnEtlGetTargetVariableType(composite: this, data: a))
                                .FirstOrDefault<ITargetVariable>();
                    }
                }

                #endregion Indexer


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public TDFnEtlGetTargetVariableTypeList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new TDFnEtlGetTargetVariableTypeList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime)
                        {
                            ExportConnection = ExportConnection,
                            NationalLanguage = NationalLanguage,
                            Etl = Etl,
                            TargetVariable = TargetVariable
                        } :
                        new TDFnEtlGetTargetVariableTypeList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null)
                        {
                            ExportConnection = ExportConnection,
                            NationalLanguage = NationalLanguage,
                            Etl = Etl,
                            TargetVariable = TargetVariable
                        };
                }

                /// <summary>
                /// Create new empty target variable table
                /// </summary>
                /// <returns>New empty target variable table</returns>
                public ITargetVariableList Empty()
                {
                    return new TDFnEtlGetTargetVariableTypeList(database: ((NfiEstaDB)Database));
                }

                #endregion Methods

            }

        }
    }
}