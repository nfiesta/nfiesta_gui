﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.Versioning;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {
            // fn_etl_get_panel_refyearset_combinations_false

            /// <summary>
            /// Combination of panels and refyearsets with a boolean, if it is useable for ETL
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class TDFnEtlGetPanelRefYearSetCombinationsFalseType(
                TDFnEtlGetPanelRefYearSetCombinationsFalseTypeList composite = null,
                DataRow data = null)
                    : AParametrizedViewEntry<TDFnEtlGetPanelRefYearSetCombinationsFalseTypeList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// RefYearSetToPanel
                /// </summary>
                public int RefYearSetToPanel
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TDFnEtlGetPanelRefYearSetCombinationsFalseTypeList.ColRefYearSetToPanel.Name,
                            defaultValue: Int32.Parse(s: TDFnEtlGetPanelRefYearSetCombinationsFalseTypeList.ColRefYearSetToPanel.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TDFnEtlGetPanelRefYearSetCombinationsFalseTypeList.ColRefYearSetToPanel.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Panel
                /// </summary>
                public string Panel
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnEtlGetPanelRefYearSetCombinationsFalseTypeList.ColPanel.Name,
                            defaultValue: TDFnEtlGetPanelRefYearSetCombinationsFalseTypeList.ColPanel.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnEtlGetPanelRefYearSetCombinationsFalseTypeList.ColPanel.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Reference year set
                /// </summary>
                public string ReferenceYearSet
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnEtlGetPanelRefYearSetCombinationsFalseTypeList.ColReferenceYearSet.Name,
                            defaultValue: TDFnEtlGetPanelRefYearSetCombinationsFalseTypeList.ColReferenceYearSet.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnEtlGetPanelRefYearSetCombinationsFalseTypeList.ColReferenceYearSet.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Useable for ETL
                /// </summary>
                public Nullable<bool> UseableForEtl
                {
                    get
                    {
                        return Functions.GetNBoolArg(
                            row: Data,
                            name: TDFnEtlGetPanelRefYearSetCombinationsFalseTypeList.ColUseableForEtl.Name,
                            defaultValue: String.IsNullOrEmpty(value: TDFnEtlGetPanelRefYearSetCombinationsFalseTypeList.ColUseableForEtl.DefaultValue) ?
                                (Nullable<bool>)null :
                                Boolean.Parse(value: TDFnEtlGetPanelRefYearSetCombinationsFalseTypeList.ColUseableForEtl.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNBoolArg(
                            row: Data,
                            name: TDFnEtlGetPanelRefYearSetCombinationsFalseTypeList.ColUseableForEtl.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Label (read-only)
                /// </summary>
                public string Label
                {
                    get
                    {
                        return
                           $"{Panel} / {ReferenceYearSet}";
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Specified object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not TDFnEtlGetPanelRefYearSetCombinationsFalseType, return False
                    if (obj is not TDFnEtlGetPanelRefYearSetCombinationsFalseType)
                    {
                        return false;
                    }

                    return
                        RefYearSetToPanel == ((TDFnEtlGetPanelRefYearSetCombinationsFalseType)obj).RefYearSetToPanel;
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        RefYearSetToPanel.GetHashCode();
                }

                /// <summary>
                /// Text description of object of panel and refyearset combination
                /// </summary>
                /// <returns>Text description of object of panel and refyearset combination</returns>
                public override string ToString()
                {
                    return Composite.Database.Postgres.Setting.LanguageVersion switch
                    {
                        LanguageVersion.National =>
                            String.Concat(
                                $"{"RefYearSetToPanel"} = {Functions.PrepIntArg(arg: RefYearSetToPanel)},{Environment.NewLine}",
                                $"{"Name"} = {Functions.PrepStringArg(arg: Label)}.{Environment.NewLine}"
                                ),
                        LanguageVersion.International =>
                            String.Concat(
                                $"{"RefYearSetToPanel"} = {Functions.PrepIntArg(arg: RefYearSetToPanel)},{Environment.NewLine}",
                                $"{"Name"} = {Functions.PrepStringArg(arg: Label)}.{Environment.NewLine}"
                                ),
                        _ =>
                            String.Concat(
                                $"{"RefYearSetToPanel"} = {Functions.PrepIntArg(arg: RefYearSetToPanel)},{Environment.NewLine}",
                                $"{"Name"} = {Functions.PrepStringArg(arg: Label)}.{Environment.NewLine}"
                                ),
                    };
                }

                #endregion Methods

            }


            /// <summary>
            /// List of panel and refyearset combinations
            /// </summary>
            public class TDFnEtlGetPanelRefYearSetCombinationsFalseTypeList
                : AParametrizedView
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema = TDSchema.Name;

                /// <summary>
                /// Table name
                /// </summary>
                public const string Name = "fn_etl_get_panel_refyearset_combinations_false";

                /// <summary>
                /// Table alias
                /// </summary>
                public const string Alias = "fn_etl_get_panel_refyearset_combinations_false";

                /// <summary>
                /// Description in national language
                /// </summary>
                public const string CaptionCs = "Kombinace panelů a refyearsetů s informací, zda je použitelná pro ETL";

                /// <summary>
                /// Description in English
                /// </summary>
                public const string CaptionEn = "Panel and refyearset combination together with the information if it is seable for ETL";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
                {
                    { "refyearset2panel", new ColumnMetadata()
                    {
                        Name = "refyearset2panel",
                        DbName = "refyearset2panel",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = "0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "REFYEARSETTOPANEL",
                        HeaderTextEn = "REFYEARSETTOPANEL",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 0
                    }
                    },
                    { "panel", new ColumnMetadata()
                    {
                        Name = "panel",
                        DbName = "panel",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "Panel",
                        HeaderTextEn = "Panel",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 1
                    }
                    },
                    { "reference_year_set", new ColumnMetadata()
                    {
                        Name = "reference_year_set",
                        DbName = "reference_year_set",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = true,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "Období",
                        HeaderTextEn = "Period",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 2
                    }
                    },
                    { "useable4etl", new ColumnMetadata()
                    {
                        Name = "useable4etl",
                        DbName = "useable4etl",
                        DataType = "System.Boolean",
                        DbDataType = "bool",
                        NewDataType = "bool",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "Použitelné pro ETL",
                        HeaderTextEn = "Useable for ETL",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 3
                    }
                    }
                };

                /// <summary>
                /// Column refyearset2panel metadata
                /// </summary>
                public static readonly ColumnMetadata ColRefYearSetToPanel = Cols["refyearset2panel"];

                /// <summary>
                /// Column panel metadata
                /// </summary>
                public static readonly ColumnMetadata ColPanel = Cols["panel"];

                /// <summary>
                /// Column refyearset metadata
                /// </summary>
                public static readonly ColumnMetadata ColReferenceYearSet = Cols["reference_year_set"];

                /// <summary>
                /// Column useable4etl metadata
                /// </summary>
                public static readonly ColumnMetadata ColUseableForEtl = Cols["useable4etl"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Private Fields

                /// <summary>
                /// 1st parameter: Variables json
                /// </summary>
                private string variables;

                /// <summary>
                /// 2nd parameter: Ids of the panel and refyearset combinations
                /// </summary>
                private List<Nullable<int>> refYearSetToPanelMapping;

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TDFnEtlGetPanelRefYearSetCombinationsFalseTypeList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                {
                    Variables = String.Empty;
                    RefYearSetToPanelMapping = [];
                }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TDFnEtlGetPanelRefYearSetCombinationsFalseTypeList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                {
                    Variables = String.Empty;
                    RefYearSetToPanelMapping = [];
                }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// 1st parameter: Variables json
                /// </summary>
                public string Variables
                {
                    get
                    {
                        return variables;
                    }
                    set
                    {
                        variables = value;
                    }
                }

                /// <summary>
                /// 2nd parameter: Ids of the panel and refyearset combinations
                /// </summary>
                public List<Nullable<int>> RefYearSetToPanelMapping
                {
                    get
                    {
                        return refYearSetToPanelMapping ?? [];
                    }
                    set
                    {
                        refYearSetToPanelMapping = value ?? [];
                    }
                }

                /// <summary>
                /// List of panel and refyearset combinations (read-only)
                /// </summary>
                public List<TDFnEtlGetPanelRefYearSetCombinationsFalseType> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new TDFnEtlGetPanelRefYearSetCombinationsFalseType(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexers

                /// <summary>
                /// Combination of panels and refyearsets from list by identifier (read-only)
                /// </summary>
                /// <param name="refYearSetToPanelId">Identifier of panel and refyearset combination</param>
                /// <returns>Combination of panels and refyearsets from list by identifier (null if not found)</returns>
                public TDFnEtlGetPanelRefYearSetCombinationsFalseType this[int refYearSetToPanelId]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColRefYearSetToPanel.Name) == refYearSetToPanelId)
                                .Select(a => new TDFnEtlGetPanelRefYearSetCombinationsFalseType(composite: this, data: a))
                                .FirstOrDefault<TDFnEtlGetPanelRefYearSetCombinationsFalseType>();
                    }
                }

                #endregion Indexers


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public TDFnEtlGetPanelRefYearSetCombinationsFalseTypeList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new TDFnEtlGetPanelRefYearSetCombinationsFalseTypeList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime)
                        {
                            Variables = Variables,
                            RefYearSetToPanelMapping = [.. RefYearSetToPanelMapping]
                        } :
                        new TDFnEtlGetPanelRefYearSetCombinationsFalseTypeList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null)
                        {
                            Variables = Variables,
                            RefYearSetToPanelMapping = [.. RefYearSetToPanelMapping]
                        };
                }

                /// <summary>
                /// Sets header, visibility, formats by data type and alignment in columns of given DataGridView
                /// </summary>
                /// <param name="dataGridView">Object DataGridView</param>
                [SupportedOSPlatform("windows")]
                public new void FormatDataGridView(System.Windows.Forms.DataGridView dataGridView)
                {
                    // General:
                    dataGridView.AllowUserToAddRows = false;
                    dataGridView.AllowUserToDeleteRows = false;
                    dataGridView.AllowUserToResizeRows = false;

                    dataGridView.AlternatingRowsDefaultCellStyle = new System.Windows.Forms.DataGridViewCellStyle()
                    {
                        BackColor = Color.LightCyan
                    };

                    dataGridView.AutoGenerateColumns = false;
                    dataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.None;
                    dataGridView.BackgroundColor = SystemColors.Window;
                    dataGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
                    dataGridView.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
                    dataGridView.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;

                    dataGridView.ColumnHeadersDefaultCellStyle = new System.Windows.Forms.DataGridViewCellStyle()
                    {
                        Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft,
                        BackColor = Color.DarkBlue,
                        Font = new Font("Microsoft Sans Serif", 9.75F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(238))),
                        ForeColor = Color.White,
                        SelectionBackColor = Color.DarkBlue,
                        SelectionForeColor = Color.White,
                        WrapMode = System.Windows.Forms.DataGridViewTriState.True
                    };

                    dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;

                    dataGridView.DefaultCellStyle = new System.Windows.Forms.DataGridViewCellStyle()
                    {
                        Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft,
                        BackColor = Color.White,
                        Font = new Font("Microsoft Sans Serif", 9.75F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(238))),
                        ForeColor = SystemColors.ControlText,
                        Format = "N3",
                        NullValue = "NULL",
                        SelectionBackColor = Color.Bisque,
                        SelectionForeColor = SystemColors.ControlText,
                        WrapMode = System.Windows.Forms.DataGridViewTriState.False
                    };

                    dataGridView.EnableHeadersVisualStyles = false;
                    dataGridView.MultiSelect = false;
                    dataGridView.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
                    dataGridView.RowHeadersVisible = false;
                    dataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;

                    // Columns:
                    foreach (ColumnMetadata column in Cols.Values.OrderBy(a => a.DisplayIndex))
                    {
                        if (dataGridView.Columns.Contains(column.Name))
                        {
                            dataGridView.Columns[column.Name].ReadOnly = column.ReadOnly;
                            dataGridView.Columns[column.Name].Visible = column.Visible;
                            dataGridView.Columns[column.Name].HeaderText = (Database.Postgres.Setting.LanguageVersion == LanguageVersion.National) ?
                                (String.IsNullOrEmpty(column.HeaderTextCs) ? column.Name : column.HeaderTextCs) :
                                (String.IsNullOrEmpty(column.HeaderTextEn) ? column.Name : column.HeaderTextEn);
                            dataGridView.Columns[column.Name].ToolTipText = (Database.Postgres.Setting.LanguageVersion == LanguageVersion.National) ?
                                (String.IsNullOrEmpty(column.ToolTipTextCs) ? dataGridView.Columns[column.Name].ToolTipText : column.ToolTipTextCs) :
                                (String.IsNullOrEmpty(column.ToolTipTextEn) ? dataGridView.Columns[column.Name].ToolTipText : column.ToolTipTextEn);
                            dataGridView.Columns[column.Name].DefaultCellStyle.Format =
                                (String.IsNullOrEmpty(column.NumericFormat) ? dataGridView.Columns[column.Name].DefaultCellStyle.Format : column.NumericFormat);
                            dataGridView.Columns[column.Name].DefaultCellStyle.Alignment = (System.Windows.Forms.DataGridViewContentAlignment)(int)column.Alignment;
                            dataGridView.Columns[column.Name].DisplayIndex = column.DisplayIndex;
                        }
                    }
                }

                #endregion Methods

            }
        }
    }
}


