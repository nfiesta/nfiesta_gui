﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {
            // v_check_classification_rules

            /// <summary>
            /// Classification rule check result with panel reference year set
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class TDVwClassificationRuleCheck(
                TDVwClassificationRuleCheckList composite = null,
                DataRow data = null)
                    : ALinqViewEntry<TDVwClassificationRuleCheckList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Record from sdesign.cm_refyearset2panel_mapping table identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TDVwClassificationRuleCheckList.ColId.Name,
                            defaultValue: Int32.Parse(s: TDVwClassificationRuleCheckList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TDVwClassificationRuleCheckList.ColId.Name,
                            val: value);
                    }
                }


                /// <summary>
                ///  Sampling panel identifier
                /// </summary>
                public int PanelId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TDVwClassificationRuleCheckList.ColPanelId.Name,
                            defaultValue: Int32.Parse(s: TDVwClassificationRuleCheckList.ColPanelId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TDVwClassificationRuleCheckList.ColPanelId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Sampling panel code
                /// </summary>
                public string PanelCode
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDVwClassificationRuleCheckList.ColPanelCode.Name,
                            defaultValue: TDVwClassificationRuleCheckList.ColPanelCode.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDVwClassificationRuleCheckList.ColPanelCode.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Sampling panel label
                /// </summary>
                public string PanelLabel
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDVwClassificationRuleCheckList.ColPanelLabel.Name,
                            defaultValue: TDVwClassificationRuleCheckList.ColPanelLabel.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDVwClassificationRuleCheckList.ColPanelLabel.Name,
                            val: value);
                    }
                }


                /// <summary>
                ///  Reference year or season set identifier
                /// </summary>
                public int RefYearSetId
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TDVwClassificationRuleCheckList.ColRefYearSetId.Name,
                            defaultValue: Int32.Parse(s: TDVwClassificationRuleCheckList.ColRefYearSetId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TDVwClassificationRuleCheckList.ColRefYearSetId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Reference year or season set code
                /// </summary>
                public string RefYearSetCode
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDVwClassificationRuleCheckList.ColRefYearSetCode.Name,
                            defaultValue: TDVwClassificationRuleCheckList.ColRefYearSetCode.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDVwClassificationRuleCheckList.ColRefYearSetCode.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Reference year or season set label
                /// </summary>
                public string RefYearSetLabel
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDVwClassificationRuleCheckList.ColRefYearSetLabel.Name,
                            defaultValue: TDVwClassificationRuleCheckList.ColRefYearSetLabel.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDVwClassificationRuleCheckList.ColRefYearSetLabel.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Classification rule check result identifier
                /// </summary>
                public Nullable<int> CheckId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TDVwClassificationRuleCheckList.ColCheckId.Name,
                            defaultValue:
                                String.IsNullOrEmpty(TDVwClassificationRuleCheckList.ColCheckId.DefaultValue) ?
                                    (Nullable<int>)null :
                                    Int32.Parse(s: TDVwClassificationRuleCheckList.ColCheckId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TDVwClassificationRuleCheckList.ColCheckId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Classification rule check result
                /// </summary>
                public Nullable<bool> Result
                {
                    get
                    {
                        return Functions.GetNBoolArg(
                            row: Data,
                            name: TDVwClassificationRuleCheckList.ColResult.Name,
                            defaultValue:
                                String.IsNullOrEmpty(value: TDVwClassificationRuleCheckList.ColResult.DefaultValue) ?
                                    (Nullable<bool>)null :
                                    Boolean.Parse(value: TDVwClassificationRuleCheckList.ColResult.DefaultValue)); ;
                    }
                    set
                    {
                        Functions.SetNBoolArg(
                            row: Data,
                            name: TDVwClassificationRuleCheckList.ColResult.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Message identifier
                /// </summary>
                public Nullable<int> MessageId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TDVwClassificationRuleCheckList.ColMessageId.Name,
                            defaultValue:
                                String.IsNullOrEmpty(value: TDVwClassificationRuleCheckList.ColMessageId.DefaultValue) ?
                                    (Nullable<int>)null :
                                    Int32.Parse(s: TDVwClassificationRuleCheckList.ColMessageId.DefaultValue)); ;
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TDVwClassificationRuleCheckList.ColMessageId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Message
                /// </summary>
                public string Message
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDVwClassificationRuleCheckList.ColMessage.Name,
                            defaultValue: TDVwClassificationRuleCheckList.ColMessage.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDVwClassificationRuleCheckList.ColMessage.Name,
                            val: value);
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not TDVwClassificationRuleCheck Type, return False
                    if (obj is not TDVwClassificationRuleCheck)
                    {
                        return false;
                    }

                    return
                        (Id == ((TDVwClassificationRuleCheck)obj).Id) &&
                        (CheckId == ((TDVwClassificationRuleCheck)obj).CheckId);
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode() ^
                        CheckId.GetHashCode();
                }

                /// <summary>
                /// Text description of classification rule check result with panel reference year set
                /// </summary>
                /// <returns>Text description of classification rule check result with panel reference year set</returns>
                public override string ToString()
                {
                    return ((NfiEstaDB)Composite.Database).Postgres.Setting.LanguageVersion switch
                    {
                        LanguageVersion.National =>
                            String.Concat(
                                $"{"ID"} = {Functions.PrepIntArg(arg: Id)}.{Environment.NewLine}"
                                ),
                        LanguageVersion.International =>
                            String.Concat(
                                $"{"ID"} = {Functions.PrepIntArg(arg: Id)}.{Environment.NewLine}"
                                ),
                        _ =>
                            String.Concat(
                                $"{"ID"} = {Functions.PrepIntArg(arg: Id)}.{Environment.NewLine}"
                                ),
                    };
                }

                #endregion Methods

            }


            /// <summary>
            /// List of classification rule check results with panel reference year set
            /// </summary>
            public class TDVwClassificationRuleCheckList
                : ALinqView
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema = TDSchema.Name;

                /// <summary>
                /// Stored procedure name
                /// </summary>
                public const string Name = "v_check_classification_rules";

                /// <summary>
                /// Stored procedure alias
                /// </summary>
                public const string Alias = "v_check_classification_rules";

                /// <summary>
                /// Description in national language
                /// </summary>
                public const string CaptionCs = "Seznam výsledků kontroly klasifikačních pravidel doplněný o názvy panelů a roků měření";

                /// <summary>
                /// Description in English
                /// </summary>
                public const string CaptionEn = "List of classification rule check results with panels and reference year sets";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
            {
                { "id", new ColumnMetadata()
                {
                    Name = "id",
                    DbName = "id",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = true,
                    NotNull = true,
                    DefaultValue = "0",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "ID",
                    HeaderTextEn = "ID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = false,
                    DisplayIndex = 0
                }
                },
                { "panel_id", new ColumnMetadata()
                {
                    Name = "panel_id",
                    DbName = "panel_id",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = "0",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "PANEL_ID",
                    HeaderTextEn = "PANEL_ID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = false,
                    DisplayIndex = 1
                }
                },
                { "panel_code", new ColumnMetadata()
                {
                    Name = "panel_code",
                    DbName = "panel",
                    DataType = "System.String",
                    DbDataType = "varchar",
                    NewDataType = "varchar",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = String.Empty,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "PANEL_CODE",
                    HeaderTextEn = "PANEL_CODE",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = false,
                    DisplayIndex = 2
                }
                },
                { "panel_label", new ColumnMetadata()
                {
                    Name = "panel_label",
                    DbName = "panel_label",
                    DataType = "System.String",
                    DbDataType = "varchar",
                    NewDataType = "varchar",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = String.Empty,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "PANEL_LABEL",
                    HeaderTextEn = "PANEL_LABEL",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = false,
                    DisplayIndex = 3
                }
                },
                { "refyearset_id", new ColumnMetadata()
                {
                    Name = "refyearset_id",
                    DbName = "refyearset_id",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = false,
                    NotNull = true,
                    DefaultValue = "0",
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "REFYEARSET_ID",
                    HeaderTextEn = "REFYEARSET_ID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = false,
                    DisplayIndex = 4
                }
                },
                { "refyearset_code", new ColumnMetadata()
                {
                    Name = "refyearset_code",
                    DbName = "reference_year_set",
                    DataType = "System.String",
                    DbDataType = "varchar",
                    NewDataType = "varchar",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = String.Empty,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "REFYEARSET_CODE",
                    HeaderTextEn = "REFYEARSET_CODE",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = false,
                    DisplayIndex = 5
                }
                },
                { "refyearset_label", new ColumnMetadata()
                {
                    Name = "refyearset_label",
                    DbName = "refyearset_label",
                    DataType = "System.String",
                    DbDataType = "varchar",
                    NewDataType = "varchar",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = String.Empty,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "REFYEARSET_LABEL",
                    HeaderTextEn = "REFYEARSET_LABEL",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = false,
                    DisplayIndex = 6
                }
                },
                { "check_id", new ColumnMetadata()
                {
                    Name = "check_id",
                    DbName = "check_id",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = null,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "CHECK_ID",
                    HeaderTextEn = "CHECK_ID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = false,
                    DisplayIndex = 7
                }
                },
                { "result", new ColumnMetadata()
                {
                    Name = "result",
                    DbName = "result",
                    DataType = "System.Boolean",
                    DbDataType = "bool",
                    NewDataType = "bool",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = null,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "RESULT",
                    HeaderTextEn = "RESULT",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = false,
                    DisplayIndex = 8
                }
                },
                { "message_id", new ColumnMetadata()
                {
                    Name = "message_id",
                    DbName = "message_id",
                    DataType = "System.Int32",
                    DbDataType = "int4",
                    NewDataType = "int4",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = null,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = "D",
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "MESSAGE_ID",
                    HeaderTextEn = "MESSAGE_ID",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = false,
                    DisplayIndex = 9
                }
                },
                { "message", new ColumnMetadata()
                {
                    Name = "message",
                    DbName = "message",
                    DataType = "System.String",
                    DbDataType = "text",
                    NewDataType = "text",
                    PKey = false,
                    NotNull = false,
                    DefaultValue = String.Empty,
                    FuncCall = null,
                    Visible = false,
                    ReadOnly = true,
                    NumericFormat = null,
                    Alignment = DataGridViewContentAlignment.MiddleLeft,
                    HeaderTextCs = "MESSAGE",
                    HeaderTextEn = "MESSAGE",
                    ToolTipTextCs = null,
                    ToolTipTextEn = null,
                    Width = 100,
                    Elemental = false,
                    DisplayIndex = 10
                }
                }
            };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column panel_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColPanelId = Cols["panel_id"];

                /// <summary>
                /// Column panel_code metadata
                /// </summary>
                public static readonly ColumnMetadata ColPanelCode = Cols["panel_code"];

                /// <summary>
                /// Column panel_label metadata
                /// </summary>
                public static readonly ColumnMetadata ColPanelLabel = Cols["panel_label"];

                /// <summary>
                /// Column refyearset_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColRefYearSetId = Cols["refyearset_id"];

                /// <summary>
                /// Column refyearset_code metadata
                /// </summary>
                public static readonly ColumnMetadata ColRefYearSetCode = Cols["refyearset_code"];

                /// <summary>
                /// Column refyearset_label metadata
                /// </summary>
                public static readonly ColumnMetadata ColRefYearSetLabel = Cols["refyearset_label"];

                /// <summary>
                /// Column check_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColCheckId = Cols["check_id"];

                /// <summary>
                /// Column result metadata
                /// </summary>
                public static readonly ColumnMetadata ColResult = Cols["result"];

                /// <summary>
                /// Column message_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColMessageId = Cols["message_id"];

                /// <summary>
                /// Column message metadata
                /// </summary>
                public static readonly ColumnMetadata ColMessage = Cols["message"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TDVwClassificationRuleCheckList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TDVwClassificationRuleCheckList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                { }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// List of classification rule check results with panel reference year set (read-only)
                /// </summary>
                public List<TDVwClassificationRuleCheck> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new TDVwClassificationRuleCheck(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexers

                /// <summary>
                /// Classification rule check result with panel reference year set from list by identifier (read-only)
                /// </summary>
                /// <param name="id">Classification rule check results with panel reference year set identifier</param>
                /// <returns>Classification rule check results with panel reference year set from list by identifier (null if not found)</returns>
                public TDVwClassificationRuleCheck this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new TDVwClassificationRuleCheck(composite: this, data: a))
                                .FirstOrDefault<TDVwClassificationRuleCheck>();
                    }
                }

                #endregion Indexers


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public TDVwClassificationRuleCheckList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new TDVwClassificationRuleCheckList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime) :
                        new TDVwClassificationRuleCheckList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null);
                }

                /// <summary>
                /// Loads data into list of classification rule check results with panel reference year set
                /// (if it was not done before)
                /// </summary>
                /// <param name="fnPanelRefYearSet">List of records from sdesign.cm_refyearset2panel_mapping table</param>
                /// <param name="fnClassificationRuleCheck">List of classification rule check results</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                public void Load(
                    TDFnGetPanelRefYearSetTypeList fnPanelRefYearSet,
                    TDFnCheckClassificationRulesTypeList fnClassificationRuleCheck,
                    string condition = null,
                    Nullable<int> limit = null)
                {
                    if (!Loaded)
                    {
                        ReLoad(
                            fnPanelRefYearSet: fnPanelRefYearSet,
                            fnClassificationRuleCheck: fnClassificationRuleCheck,
                            condition: condition,
                            limit: limit);
                    }
                }

                /// <summary>
                /// Loads data into list of classification rule check results with panel reference year set
                /// </summary>
                /// <param name="fnPanelRefYearSet">List of records from sdesign.cm_refyearset2panel_mapping table</param>
                /// <param name="fnClassificationRuleCheck">List of classification rule check results</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                public void ReLoad(
                    TDFnGetPanelRefYearSetTypeList fnPanelRefYearSet,
                    TDFnCheckClassificationRulesTypeList fnClassificationRuleCheck,
                    string condition = null,
                    Nullable<int> limit = null)
                {
                    Data = EmptyDataTable(
                                 tableName: TableName,
                                 columns: Columns);
                    Condition = condition;
                    Limit = limit;

                    System.Diagnostics.Stopwatch stopWatch = new();
                    stopWatch.Start();

                    // Seznam panelů a referenčních období
                    var ctePanelRefYearSet =
                        fnPanelRefYearSet.Data.AsEnumerable()
                        .Select(a => new
                        {
                            Id = a.Field<int>(columnName: TDFnGetPanelRefYearSetTypeList.ColId.Name),
                            PanelId = a.Field<int>(columnName: TDFnGetPanelRefYearSetTypeList.ColPanelId.Name),
                            PanelCode = a.Field<string>(columnName: TDFnGetPanelRefYearSetTypeList.ColPanelCode.Name),
                            PanelLabel = a.Field<string>(columnName: TDFnGetPanelRefYearSetTypeList.ColPanelLabel.Name),
                            RefYearSetId = a.Field<int>(columnName: TDFnGetPanelRefYearSetTypeList.ColRefYearSetId.Name),
                            RefYearSetCode = a.Field<string>(columnName: TDFnGetPanelRefYearSetTypeList.ColRefYearSetCode.Name),
                            RefYearSetLabel = a.Field<string>(columnName: TDFnGetPanelRefYearSetTypeList.ColRefYearSetLabel.Name)
                        });

                    // Kontrola klasifikačních pravidel
                    var cteClassificationRuleCheck =
                       fnClassificationRuleCheck.Data.AsEnumerable()
                       .Select(a => new
                       {
                           CheckId = a.Field<int>(columnName: TDFnCheckClassificationRulesTypeList.ColId.Name),
                           PanelRefYearSetId = a.Field<int>(columnName: TDFnCheckClassificationRulesTypeList.ColPanelRefYearSetId.Name),
                           Result = a.Field<Nullable<bool>>(columnName: TDFnCheckClassificationRulesTypeList.ColResult.Name),
                           MessageId = a.Field<Nullable<int>>(columnName: TDFnCheckClassificationRulesTypeList.ColMessageId.Name),
                           Message = a.Field<string>(columnName: TDFnCheckClassificationRulesTypeList.ColMessage.Name),
                       });

                    // LEFT JOIN fnPanelRefYearSet + fnClassificationRuleCheck
                    var cteResult =
                        from a in ctePanelRefYearSet
                        join b in cteClassificationRuleCheck
                        on a.Id equals b.PanelRefYearSetId into lj
                        from b in lj.DefaultIfEmpty()
                        select new
                        {
                            a.Id,
                            a.PanelId,
                            a.PanelCode,
                            a.PanelLabel,
                            a.RefYearSetId,
                            a.RefYearSetCode,
                            a.RefYearSetLabel,
                            b?.CheckId,
                            b?.Result,
                            b?.MessageId,
                            b?.Message
                        };

                    if (cteResult.Any())
                    {
                        Data = cteResult
                            .OrderBy(a => a.Id)
                            .Select(a => Data.LoadDataRow(
                                values:
                                [
                                    a.Id,
                                    a.PanelId,
                                    a.PanelCode,
                                    a.PanelLabel,
                                    a.RefYearSetId,
                                    a.RefYearSetCode,
                                    a.RefYearSetLabel,
                                    a.CheckId,
                                    a.Result,
                                    a.MessageId,
                                    a.Message
                                ],
                                fAcceptChanges: false))
                            .CopyToDataTable();
                        Data.TableName = Name;
                    }
                    else
                    {
                        Data = EmptyDataTable(
                            tableName: TableName,
                            columns: Columns);
                    }

                    // Omezení podmínkou
                    if (!String.IsNullOrEmpty(value: Condition))
                    {
                        DataView view = new(table: Data);
                        try
                        {
                            view.RowFilter = Condition;
                        }
                        catch
                        {
                            view.RowFilter = String.Empty;
                            Condition = String.Empty;
                        }
                        Data = view.ToTable();
                    }

                    // Omezeni počtem
                    if (Limit != null)
                    {
                        Data = Data.AsEnumerable()
                            .Take(count: (int)Limit)
                            .CopyToDataTable();
                    }

                    stopWatch.Stop();
                    LoadingTime = 0.001 * stopWatch.ElapsedMilliseconds;
                    Data.TableName = TableName;
                }

                #endregion Methods

            }

        }
    }
}
