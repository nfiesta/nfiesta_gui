﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {
            // fn_etl_get_variables_false

            /// <summary>
            /// Attribute variable for a selected combination of panel and reference-year set
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class TDFnEtlGetVariablesFalseType(
                TDFnEtlGetVariablesFalseTypeList composite = null,
                DataRow data = null)
                    : AParametrizedViewEntry<TDFnEtlGetVariablesFalseTypeList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Attribute variable identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TDFnEtlGetVariablesFalseTypeList.ColId.Name,
                            defaultValue: Int32.Parse(s: TDFnEtlGetVariablesFalseTypeList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TDFnEtlGetVariablesFalseTypeList.ColId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Subpopulation
                /// </summary>
                public string SubPopulation
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnEtlGetVariablesFalseTypeList.ColSubPopulation.Name,
                            defaultValue: TDFnEtlGetVariablesFalseTypeList.ColSubPopulation.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnEtlGetVariablesFalseTypeList.ColSubPopulation.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Subpopulation category
                /// </summary>
                public string SubPopulationCategory
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnEtlGetVariablesFalseTypeList.ColSubPopulationCategory.Name,
                            defaultValue: TDFnEtlGetVariablesFalseTypeList.ColSubPopulationCategory.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnEtlGetVariablesFalseTypeList.ColSubPopulationCategory.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Area domain
                /// </summary>
                public string AreaDomain
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnEtlGetVariablesFalseTypeList.ColAreaDomain.Name,
                            defaultValue: TDFnEtlGetVariablesFalseTypeList.ColAreaDomain.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnEtlGetVariablesFalseTypeList.ColAreaDomain.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Area domain category
                /// </summary>
                public string AreaDomainCategory
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnEtlGetVariablesFalseTypeList.ColAreaDomainCategory.Name,
                            defaultValue: TDFnEtlGetVariablesFalseTypeList.ColAreaDomainCategory.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnEtlGetVariablesFalseTypeList.ColAreaDomainCategory.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Subpopulation (English label)
                /// </summary>
                public string SubPopulationEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnEtlGetVariablesFalseTypeList.ColSubPopulationEn.Name,
                            defaultValue: TDFnEtlGetVariablesFalseTypeList.ColSubPopulationEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnEtlGetVariablesFalseTypeList.ColSubPopulationEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Subpopulation category (English label)
                /// </summary>
                public string SubPopulationCategoryEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnEtlGetVariablesFalseTypeList.ColSubPopulationCategoryEn.Name,
                            defaultValue: TDFnEtlGetVariablesFalseTypeList.ColSubPopulationCategoryEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnEtlGetVariablesFalseTypeList.ColSubPopulationCategoryEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Area domain (English label)
                /// </summary>
                public string AreaDomainEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnEtlGetVariablesFalseTypeList.ColAreaDomainEn.Name,
                            defaultValue: TDFnEtlGetVariablesFalseTypeList.ColAreaDomainEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnEtlGetVariablesFalseTypeList.ColAreaDomainEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Area domain category  (English label)
                /// </summary>
                public string AreaDomainCategoryEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnEtlGetVariablesFalseTypeList.ColAreaDomainCategoryEn.Name,
                            defaultValue: TDFnEtlGetVariablesFalseTypeList.ColAreaDomainCategoryEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnEtlGetVariablesFalseTypeList.ColAreaDomainCategory.Name,
                            val: value);
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not TDFnEtlGetVariablesFalseType, return False
                    if (obj is not TDFnEtlGetVariablesFalseType)
                    {
                        return false;
                    }

                    return
                        Id == ((TDFnEtlGetVariablesFalseType)obj).Id;
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                /// <summary>
                /// Text description of classification rule check result
                /// </summary>
                /// <returns>Text description of classification rule check result</returns>
                public override string ToString()
                {
                    return ((NfiEstaDB)Composite.Database).Postgres.Setting.LanguageVersion switch
                    {
                        LanguageVersion.National =>
                            String.Concat(
                                $"{"ID"} = {Functions.PrepIntArg(arg: Id)}.{Environment.NewLine}"
                                ),
                        LanguageVersion.International =>
                            String.Concat(
                                $"{"ID"} = {Functions.PrepIntArg(arg: Id)}.{Environment.NewLine}"
                                ),
                        _ =>
                            String.Concat(
                                $"{"ID"} = {Functions.PrepIntArg(arg: Id)}.{Environment.NewLine}"
                                ),
                    };
                }

                #endregion Methods

            }


            /// <summary>
            /// List of attribute variables
            /// </summary>
            public class TDFnEtlGetVariablesFalseTypeList
                : AParametrizedView
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema = TDSchema.Name;

                /// <summary>
                /// Table name
                /// </summary>
                public const string Name = "fn_etl_get_variables_false";

                /// <summary>
                /// Table alias
                /// </summary>
                public const string Alias = "fn_etl_get_variables_false";

                /// <summary>
                /// Description in national language
                /// </summary>
                public const string CaptionCs = "Atributová proměnná pro vybranou kombinaci panelů a období měření";

                /// <summary>
                /// Description in English
                /// </summary>
                public const string CaptionEn = "Attribute variable for a selected combination of panel and reference-year set";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
                {
                    { "id", new ColumnMetadata()
                    {
                        Name = "id",
                        DbName = "id",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = true,
                        NotNull = true,
                        DefaultValue = "0",
                        FuncCall = "row_number() over()",
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ID",
                        HeaderTextEn = "ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 0
                    }
                    },
                    { "sub_population", new ColumnMetadata()
                    {
                        Name = "sub_population",
                        DbName = "sub_population",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "Subpopulace",
                        HeaderTextEn = "Subpopulation",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 1
                    }
                    },
                    { "sub_population_category", new ColumnMetadata()
                    {
                        Name = "sub_population_category",
                        DbName = "sub_population_category",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "Kategorie subpopulace",
                        HeaderTextEn = "Subpopulation category",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 2
                    }
                    },
                    { "area_domain", new ColumnMetadata()
                    {
                        Name = "area_domain",
                        DbName = "area_domain",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "Plošná doména",
                        HeaderTextEn = "Area domain",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 3
                    }
                    },
                    { "area_domain_category", new ColumnMetadata()
                    {
                        Name = "area_domain_category",
                        DbName = "area_domain_category",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "Kategorie plošné domény",
                        HeaderTextEn = "Area domain category",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 4
                    }
                    },
                    { "sub_population_en", new ColumnMetadata()
                    {
                        Name = "sub_population_en",
                        DbName = "sub_population_en",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "Subpopulace",
                        HeaderTextEn = "Subpopulation",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 5
                    }
                    },
                    { "sub_population_category_en", new ColumnMetadata()
                    {
                        Name = "sub_population_category_en",
                        DbName = "sub_population_category_en",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "Kategorie subpopulace",
                        HeaderTextEn = "Subpopulation category",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 6
                    }
                    },
                    { "area_domain_en", new ColumnMetadata()
                    {
                        Name = "area_domain_en",
                        DbName = "area_domain_en",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "Plošná doména",
                        HeaderTextEn = "Area domain",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 7
                    }
                    },
                    { "area_domain_category_en", new ColumnMetadata()
                    {
                        Name = "area_domain_category_en",
                        DbName = "area_domain_category_en",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "Kategorie plošné domény",
                        HeaderTextEn = "Area domain category",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = true,
                        DisplayIndex = 8
                    }
                    }
                };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column sub_population metadata
                /// </summary>
                public static readonly ColumnMetadata ColSubPopulation = Cols["sub_population"];

                /// <summary>
                /// Column sub_population_category metadata
                /// </summary>
                public static readonly ColumnMetadata ColSubPopulationCategory = Cols["sub_population_category"];

                /// <summary>
                /// Column area_domain metadata
                /// </summary>
                public static readonly ColumnMetadata ColAreaDomain = Cols["area_domain"];

                /// <summary>
                /// Column area_domain_category metadata
                /// </summary>
                public static readonly ColumnMetadata ColAreaDomainCategory = Cols["area_domain_category"];

                /// <summary>
                /// Column sub_population_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColSubPopulationEn = Cols["sub_population_en"];

                /// <summary>
                /// Column subpopulation sub_population_category_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColSubPopulationCategoryEn = Cols["sub_population_category_en"];

                /// <summary>
                /// Column area_domain_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColAreaDomainEn = Cols["area_domain_en"];

                /// <summary>
                /// Column area_domain_category_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColAreaDomainCategoryEn = Cols["area_domain_category_en"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Private Fields

                /// <summary>
                /// 1st parameter: Variables json
                /// </summary>
                private string variables;

                /// <summary>
                /// 2nd parameter: Panel
                /// </summary>
                private string panel;

                /// <summary>
                /// 3rd parameter: Reference-year set
                /// </summary>
                private string referenceYearSet;

                /// <summary>
                /// 4th parameter: Language
                /// </summary>
                private Language nationalLanguage;

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TDFnEtlGetVariablesFalseTypeList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                {
                    Variables = String.Empty;
                    Panel = String.Empty;
                    ReferenceYearSet = String.Empty;
                    NationalLanguage = Language.CS;
                }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TDFnEtlGetVariablesFalseTypeList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                {
                    Variables = String.Empty;
                    Panel = String.Empty;
                    ReferenceYearSet = String.Empty;
                    NationalLanguage = Language.CS;
                }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// 1st parameter: Variables json
                /// </summary>
                public string Variables
                {
                    get
                    {
                        return variables;
                    }
                    set
                    {
                        variables = value;
                    }
                }

                /// <summary>
                /// 2nd parameter: Panel
                /// </summary>
                public string Panel
                {
                    get
                    {
                        return panel;
                    }
                    set
                    {
                        panel = value;
                    }
                }

                /// <summary>
                /// 3rd parameter: Reference-year set
                /// </summary>
                public string ReferenceYearSet
                {
                    get
                    {
                        return referenceYearSet;
                    }
                    set
                    {
                        referenceYearSet = value;
                    }
                }

                /// <summary>
                /// 4th parameter: Language
                /// </summary>
                public Language NationalLanguage
                {
                    get
                    {
                        return nationalLanguage;
                    }
                    set
                    {
                        nationalLanguage = value;
                    }
                }

                /// <summary>
                /// List of attribute variables (read-only)
                /// </summary>
                public List<TDFnEtlGetVariablesFalseType> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new TDFnEtlGetVariablesFalseType(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexers

                /// <summary>
                /// Attribute variable from list by identifier (read-only)
                /// </summary>
                /// <param name="id">Identifier of attribute variable</param>
                /// <returns>Attribute variables from list by identifier (null if not found)</returns>
                public TDFnEtlGetVariablesFalseType this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new TDFnEtlGetVariablesFalseType(composite: this, data: a))
                                .FirstOrDefault<TDFnEtlGetVariablesFalseType>();
                    }
                }

                #endregion Indexers


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public TDFnEtlGetVariablesFalseTypeList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new TDFnEtlGetVariablesFalseTypeList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime)
                        {
                            Variables = Variables,
                            Panel = Panel,
                            ReferenceYearSet = ReferenceYearSet,
                            NationalLanguage = NationalLanguage
                        } :
                        new TDFnEtlGetVariablesFalseTypeList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null)
                        {
                            Variables = Variables,
                            Panel = Panel,
                            ReferenceYearSet = ReferenceYearSet,
                            NationalLanguage = NationalLanguage
                        };
                }

                #endregion Methods

            }

        }
    }
}


