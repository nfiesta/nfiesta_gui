﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {
            // fn_get_eligible_panel_refyearset_combinations

            /// <summary>
            /// fn_get_eligible_panel_refyearset_combinations return type
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class TDFnGetEligiblePanelRefYearSetCombinationsType(
                TDFnGetEligiblePanelRefYearSetCombinationsTypeList composite = null,
                DataRow data = null)
                    : AParametrizedViewEntry<TDFnGetEligiblePanelRefYearSetCombinationsTypeList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TDFnGetEligiblePanelRefYearSetCombinationsTypeList.ColId.Name,
                            defaultValue: Int32.Parse(s: TDFnGetEligiblePanelRefYearSetCombinationsTypeList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TDFnGetEligiblePanelRefYearSetCombinationsTypeList.ColId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Panel label in national language
                /// </summary>
                public string PanelLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnGetEligiblePanelRefYearSetCombinationsTypeList.ColPanelLabelCs.Name,
                            defaultValue: TDFnGetEligiblePanelRefYearSetCombinationsTypeList.ColPanelLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnGetEligiblePanelRefYearSetCombinationsTypeList.ColPanelLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Panel description in national language
                /// </summary>
                public string PanelDescriptionCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnGetEligiblePanelRefYearSetCombinationsTypeList.ColPanelDescriptionCs.Name,
                            defaultValue: TDFnGetEligiblePanelRefYearSetCombinationsTypeList.ColPanelDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnGetEligiblePanelRefYearSetCombinationsTypeList.ColPanelDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Panel label in English
                /// </summary>
                public string PanelLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnGetEligiblePanelRefYearSetCombinationsTypeList.ColPanelLabelEn.Name,
                            defaultValue: TDFnGetEligiblePanelRefYearSetCombinationsTypeList.ColPanelLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnGetEligiblePanelRefYearSetCombinationsTypeList.ColPanelLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Panel description in English
                /// </summary>
                public string PanelDescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnGetEligiblePanelRefYearSetCombinationsTypeList.ColPanelDescriptionEn.Name,
                            defaultValue: TDFnGetEligiblePanelRefYearSetCombinationsTypeList.ColPanelDescriptionEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnGetEligiblePanelRefYearSetCombinationsTypeList.ColPanelDescriptionEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Reference year set label in national language
                /// </summary>
                public string ReferenceYearSetLabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnGetEligiblePanelRefYearSetCombinationsTypeList.ColRefYearSetLabelCs.Name,
                            defaultValue: TDFnGetEligiblePanelRefYearSetCombinationsTypeList.ColRefYearSetLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnGetEligiblePanelRefYearSetCombinationsTypeList.ColRefYearSetLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Reference year set description in national language
                /// </summary>
                public string ReferenceYearSetDescriptionCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnGetEligiblePanelRefYearSetCombinationsTypeList.ColRefYearSetDescriptionCs.Name,
                            defaultValue: TDFnGetEligiblePanelRefYearSetCombinationsTypeList.ColRefYearSetDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnGetEligiblePanelRefYearSetCombinationsTypeList.ColRefYearSetDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Reference year set label in English
                /// </summary>
                public string ReferenceYearSetLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnGetEligiblePanelRefYearSetCombinationsTypeList.ColRefYearSetLabelEn.Name,
                            defaultValue: TDFnGetEligiblePanelRefYearSetCombinationsTypeList.ColRefYearSetLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnGetEligiblePanelRefYearSetCombinationsTypeList.ColRefYearSetLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Reference year set description in English
                /// </summary>
                public string ReferenceYearSetDescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnGetEligiblePanelRefYearSetCombinationsTypeList.ColRefYearSetDescriptionEn.Name,
                            defaultValue: TDFnGetEligiblePanelRefYearSetCombinationsTypeList.ColRefYearSetDescriptionEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnGetEligiblePanelRefYearSetCombinationsTypeList.ColRefYearSetDescriptionEn.Name,
                            val: value);
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not TDFnGetEligiblePanelRefYearSetCombinationsType, return False
                    if (obj is not TDFnGetEligiblePanelRefYearSetCombinationsType)
                    {
                        return false;
                    }

                    return
                        Id == ((TDFnGetEligiblePanelRefYearSetCombinationsType)obj).Id;
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                /// <summary>
                /// Text description
                /// </summary>
                /// <returns>Text description</returns>
                public override string ToString()
                {
                    return ((NfiEstaDB)Composite.Database).Postgres.Setting.LanguageVersion switch
                    {
                        LanguageVersion.National =>
                            String.Concat(
                                $"{"ID"} = {Functions.PrepIntArg(arg: Id)},{Environment.NewLine}",
                                $"{"Panel label"} = {Functions.PrepStringArg(arg: PanelLabelCs)}.{Environment.NewLine}",
                                $"{"Reference year set label"} = {Functions.PrepStringArg(arg: ReferenceYearSetLabelCs)}.{Environment.NewLine}"
                                ),
                        LanguageVersion.International =>
                            String.Concat(
                                $"{"ID"} = {Functions.PrepIntArg(arg: Id)},{Environment.NewLine}",
                                $"{"Panel label"} = {Functions.PrepStringArg(arg: PanelLabelEn)}.{Environment.NewLine}",
                                $"{"Reference year set label"} = {Functions.PrepStringArg(arg: ReferenceYearSetLabelEn)}.{Environment.NewLine}"
                                ),
                        _ =>
                            String.Concat(
                                $"{"ID"} = {Functions.PrepIntArg(arg: Id)},{Environment.NewLine}",
                                $"{"Panel label"} = {Functions.PrepStringArg(arg: PanelLabelEn)}.{Environment.NewLine}",
                                $"{"Reference year set label"} = {Functions.PrepStringArg(arg: ReferenceYearSetLabelEn)}.{Environment.NewLine}"
                                ),
                    };
                }

                #endregion Methods

            }


            /// <summary>
            /// List of fn_get_eligible_panel_refyearset_combinations return type
            /// </summary>
            public class TDFnGetEligiblePanelRefYearSetCombinationsTypeList
                : AParametrizedView
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema = TDSchema.Name;

                /// <summary>
                /// Stored procedure name
                /// </summary>
                public const string Name = "fn_get_eligible_panel_refyearset_combinations";

                /// <summary>
                /// Stored procedure alias
                /// </summary>
                public const string Alias = "fn_get_eligible_panel_refyearset_combinations";


                /// <summary>
                /// Description in national language
                /// </summary>
                public const string CaptionCs =
                    "Uložená procedura, která vrací seznam párů panelů a roků měření, " +
                    "pro které mohou být spočítány lokální hustoty";

                /// <summary>
                /// Description in English
                /// </summary>
                public const string CaptionEn =
                    "Stored procedure that returns list of combinations of panel and reference-yearsets " +
                    "for which local densities can be calculated";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
                {
                    { "id", new ColumnMetadata()
                    {
                        Name = "id",
                        DbName = "refyearset2panel_mapping",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = true,
                        NotNull = true,
                        DefaultValue = "0",
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ID",
                        HeaderTextEn = "ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 0
                    }
                    },
                    { "panel_label_cs", new ColumnMetadata()
                    {
                        Name = "panel_label_cs",
                        DbName = "panel_label",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PANEL_LABEL_CS",
                        HeaderTextEn = "PANEL_LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 1
                    }
                    },
                    { "panel_description_cs", new ColumnMetadata()
                    {
                        Name = "panel_description_cs",
                        DbName = "panel_description",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PANEL_DESCRIPTION_CS",
                        HeaderTextEn = "PANEL_DESCRIPTION_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 2
                    }
                    },
                    { "panel_label_en", new ColumnMetadata()
                    {
                        Name = "panel_label_en",
                        DbName = "panel_label",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PANEL_LABEL_EN",
                        HeaderTextEn = "PANEL_LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 3
                    }
                    },
                    { "panel_description_en", new ColumnMetadata()
                    {
                        Name = "panel_description_en",
                        DbName = "panel_description",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "PANEL_DESCRIPTION_EN",
                        HeaderTextEn = "PANEL_DESCRIPTION_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 4
                    }
                    },
                    { "refyearset_label_cs", new ColumnMetadata()
                    {
                        Name = "refyearset_label_cs",
                        DbName = "refyearset_label",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "REFYEARSET_LABEL_CS",
                        HeaderTextEn = "REFYEARSET_LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 5
                    }
                    },
                    { "refyearset_description_cs", new ColumnMetadata()
                    {
                        Name = "refyearset_description_cs",
                        DbName = "refyearset_description",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "REFYEARSET_DESCRIPTION_CS",
                        HeaderTextEn = "REFYEARSET_DESCRIPTION_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 6
                    }
                    },
                    { "refyearset_label_en", new ColumnMetadata()
                    {
                        Name = "refyearset_label_en",
                        DbName = "refyearset_label",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "REFYEARSET_LABEL_EN",
                        HeaderTextEn = "REFYEARSET_LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 7
                    }
                    },
                    { "refyearset_description_en", new ColumnMetadata()
                    {
                        Name = "refyearset_description_en",
                        DbName = "refyearset_description",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "REFYEARSET_DESCRIPTION_EN",
                        HeaderTextEn = "REFYEARSET_DESCRIPTION_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 8
                    }
                    }
                };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column panel_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColPanelLabelCs = Cols["panel_label_cs"];

                /// <summary>
                /// Column panel_description_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColPanelDescriptionCs = Cols["panel_description_cs"];

                /// <summary>
                /// Column panel_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColPanelLabelEn = Cols["panel_label_en"];

                /// <summary>
                /// Column panel_description_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColPanelDescriptionEn = Cols["panel_description_en"];

                /// <summary>
                /// Column refyearset_label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColRefYearSetLabelCs = Cols["refyearset_label_cs"];

                /// <summary>
                /// Column refyearset_description_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColRefYearSetDescriptionCs = Cols["refyearset_description_cs"];

                /// <summary>
                /// Column refyearset_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColRefYearSetLabelEn = Cols["refyearset_label_en"];

                /// <summary>
                /// Column refyearset_description_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColRefYearSetDescriptionEn = Cols["refyearset_description_en"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Private Fields

                /// <summary>
                /// 1st parameter: Target variable identifier
                /// </summary>
                private Nullable<int> targetVariableId;

                /// <summary>
                /// 2nd parameter: List of categorization setup identifiers
                /// </summary>
                private List<Nullable<int>> categorizationSetupIds;

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TDFnGetEligiblePanelRefYearSetCombinationsTypeList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                {
                    TargetVariableId = null;
                    CategorizationSetupIds = [];
                }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TDFnGetEligiblePanelRefYearSetCombinationsTypeList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                {
                    TargetVariableId = null;
                    CategorizationSetupIds = [];
                }

                #endregion Constructor


                #region Properties


                /// <summary>
                /// 1st parameter: Target variable identifier
                /// </summary>
                public Nullable<int> TargetVariableId
                {
                    get
                    {
                        return targetVariableId;
                    }
                    set
                    {
                        targetVariableId = value;
                    }
                }

                /// <summary>
                /// 2nd parameter: List of categorization setup identifiers
                /// </summary>
                public List<Nullable<int>> CategorizationSetupIds
                {
                    get
                    {
                        return categorizationSetupIds;
                    }
                    set
                    {
                        categorizationSetupIds = value;
                    }
                }

                /// <summary>
                /// List of fn_get_eligible_panel_refyearset_combinations return type (read-only)
                /// </summary>
                public List<TDFnGetEligiblePanelRefYearSetCombinationsType> Items
                {
                    get
                    {
                        return
                            [..
                                Data.AsEnumerable()
                                .Select(a => new TDFnGetEligiblePanelRefYearSetCombinationsType(composite: this, data: a))
                            ];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// fn_get_eligible_panel_refyearset_combinations return type from list by identifier (read-only)
                /// </summary>
                /// <param name="id">fn_get_eligible_panel_refyearset_combinations return type identifier</param>
                /// <returns>fn_get_eligible_panel_refyearset_combinations return type from list by identifier (null if not found)</returns>
                public TDFnGetEligiblePanelRefYearSetCombinationsType this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new TDFnGetEligiblePanelRefYearSetCombinationsType(composite: this, data: a))
                                .FirstOrDefault<TDFnGetEligiblePanelRefYearSetCombinationsType>();
                    }
                }

                #endregion Indexer


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public TDFnGetEligiblePanelRefYearSetCombinationsTypeList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new TDFnGetEligiblePanelRefYearSetCombinationsTypeList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime)
                        {
                            TargetVariableId = TargetVariableId,
                            CategorizationSetupIds = [.. CategorizationSetupIds]
                        } :
                        new TDFnGetEligiblePanelRefYearSetCombinationsTypeList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null)
                        {
                            TargetVariableId = TargetVariableId,
                            CategorizationSetupIds = [.. CategorizationSetupIds]
                        };
                }

                #endregion Methods

            }

        }
    }
}


