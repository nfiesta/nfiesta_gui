﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {
        namespace TargetData
        {
            // fn_get_target_variable

            /// <summary>
            /// Target variable group
            /// </summary>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public class TDFnGetTargetVariableType(
                TDFnGetTargetVariableTypeList composite = null,
                DataRow data = null)
                    : AParametrizedViewEntry<TDFnGetTargetVariableTypeList>(composite: composite, data: data)
            {

                #region Derived Properties

                /// <summary>
                /// Entry identifier
                /// </summary>
                public override int Id
                {
                    get
                    {
                        return Functions.GetIntArg(
                            row: Data,
                            name: TDFnGetTargetVariableTypeList.ColId.Name,
                            defaultValue: Int32.Parse(s: TDFnGetTargetVariableTypeList.ColId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetIntArg(
                            row: Data,
                            name: TDFnGetTargetVariableTypeList.ColId.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Identifier of the core target variable
                /// </summary>
                public string Core
                {
                    get
                    {
                        string result = Functions.GetStringArg(
                            row: Data,
                            name: TDFnGetTargetVariableTypeList.ColCore.Name,
                            defaultValue: TDFnGetTargetVariableTypeList.ColCore.DefaultValue);

                        result = result
                            .Replace(oldValue: "t", newValue: Functions.StrTrue)
                            .Replace(oldValue: "f", newValue: Functions.StrFalse);

                        return result;
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnGetTargetVariableTypeList.ColCore.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// List of identifiers of the core target variable
                /// </summary>
                public List<Nullable<bool>> CoreList
                {
                    get
                    {
                        return Functions.StringToNBoolList(
                            text: Core,
                            separator: (char)59,
                            defaultValue: null);
                    }
                }


                /// <summary>
                /// Identifiers of the target variables in the group as text
                /// </summary>
                public string TargetVariableIds
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnGetTargetVariableTypeList.ColTargetVariableIds.Name,
                            defaultValue: TDFnGetTargetVariableTypeList.ColTargetVariableIds.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnGetTargetVariableTypeList.ColTargetVariableIds.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// List of identifiers of the target variables in the group
                /// </summary>
                public List<Nullable<int>> TargetVariableIdList
                {
                    get
                    {
                        return Functions.StringToNIntList(
                            text: TargetVariableIds,
                            separator: (char)59,
                            defaultValue: null);
                    }
                }


                /// <summary>
                /// Identifier of the local density object group
                /// </summary>
                public Nullable<int> LDsityObjectGroupId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TDFnGetTargetVariableTypeList.ColLDsityObjectGroupId.Name,
                            defaultValue:
                                String.IsNullOrEmpty(value: TDFnGetTargetVariableTypeList.ColLDsityObjectGroupId.DefaultValue) ?
                                    (Nullable<int>)null :
                                    Int32.Parse(s: TDFnGetTargetVariableTypeList.ColLDsityObjectGroupId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TDFnGetTargetVariableTypeList.ColLDsityObjectGroupId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Local density object group(read-only)
                /// </summary>
                public TDLDsityObjectGroup LDsityObjectGroup
                {
                    get
                    {
                        return (LDsityObjectGroupId != null) ?
                            ((NfiEstaDB)Composite.Database)
                            .STargetData.CLDsityObjectGroup[(int)LDsityObjectGroupId] : null;
                    }
                }


                /// <summary>
                /// Target variable group label in national language
                /// </summary>
                public string LabelCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnGetTargetVariableTypeList.ColLabelCs.Name,
                            defaultValue: TDFnGetTargetVariableTypeList.ColLabelCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnGetTargetVariableTypeList.ColLabelCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Target variable group description in national language
                /// </summary>
                public string DescriptionCs
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnGetTargetVariableTypeList.ColDescriptionCs.Name,
                            defaultValue: TDFnGetTargetVariableTypeList.ColDescriptionCs.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnGetTargetVariableTypeList.ColDescriptionCs.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Extended label of target variable group in national language (read-only)
                /// </summary>
                public string ExtendedLabelCs
                {
                    get
                    {
                        return $"{TargetVariableIds} - {LabelCs}";
                    }
                }


                /// <summary>
                /// Target variable group label in English
                /// </summary>
                public string LabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnGetTargetVariableTypeList.ColLabelEn.Name,
                            defaultValue: TDFnGetTargetVariableTypeList.ColLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnGetTargetVariableTypeList.ColLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Target variable group description in English
                /// </summary>
                public string DescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnGetTargetVariableTypeList.ColDescriptionEn.Name,
                            defaultValue: TDFnGetTargetVariableTypeList.ColDescriptionEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnGetTargetVariableTypeList.ColDescriptionEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Extended label of target variable group in English (read-only)
                /// </summary>
                public string ExtendedLabelEn
                {
                    get
                    {
                        return $"{TargetVariableIds} - {LabelEn}";
                    }
                }


                /// <summary>
                /// State or change identifier
                /// </summary>
                public Nullable<int> StateOrChangeId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TDFnGetTargetVariableTypeList.ColStateOrChangeId.Name,
                            defaultValue:
                                String.IsNullOrEmpty(value: TDFnGetTargetVariableTypeList.ColStateOrChangeId.DefaultValue) ?
                                    (Nullable<int>)null :
                                    Int32.Parse(s: TDFnGetTargetVariableTypeList.ColStateOrChangeId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TDFnGetTargetVariableTypeList.ColStateOrChangeId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// State or change value (read-only)
                /// </summary>
                public TDStateOrChangeEnum StateOrChangeValue
                {
                    get
                    {
                        return (TDStateOrChangeEnum)(StateOrChangeId ?? 0);
                    }
                }

                /// <summary>
                /// State or change object (read-only)
                /// </summary>
                public TDStateOrChange StateOrChange
                {
                    get
                    {
                        return (StateOrChangeId != null) ?
                            ((NfiEstaDB)Composite.Database)
                            .STargetData.CStateOrChange[(int)StateOrChangeId] : null;
                    }
                }

                /// <summary>
                /// State or change label in English
                /// </summary>
                public string StateOrChangeLabelEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnGetTargetVariableTypeList.ColSocLabelEn.Name,
                            defaultValue: TDFnGetTargetVariableTypeList.ColSocLabelEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnGetTargetVariableTypeList.ColSocLabelEn.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// State or change description in English
                /// </summary>
                public string StateOrChangeDescriptionEn
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: TDFnGetTargetVariableTypeList.ColSocDescriptionEn.Name,
                            defaultValue: TDFnGetTargetVariableTypeList.ColSocDescriptionEn.DefaultValue);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: TDFnGetTargetVariableTypeList.ColSocDescriptionEn.Name,
                            val: value);
                    }
                }


                /// <summary>
                /// Areal or population identifier
                /// </summary>
                public Nullable<int> ArealOrPopulationId
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: TDFnGetTargetVariableTypeList.ColArealOrPopulationId.Name,
                            defaultValue: String.IsNullOrEmpty(value: TDFnGetTargetVariableTypeList.ColArealOrPopulationId.DefaultValue) ?
                                (Nullable<int>)null :
                                Int32.Parse(s: TDFnGetTargetVariableTypeList.ColArealOrPopulationId.DefaultValue));
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: TDFnGetTargetVariableTypeList.ColArealOrPopulationId.Name,
                            val: value);
                    }
                }

                /// <summary>
                /// Areal or population value (read-only)
                /// </summary>
                public TDArealOrPopulationEnum ArealOrPopulationValue
                {
                    get
                    {
                        return (TDArealOrPopulationEnum)(ArealOrPopulationId ?? 0);
                    }
                }

                /// <summary>
                /// Areal or population object (read-only)
                /// </summary>
                public TDArealOrPopulation ArealOrPopulation
                {
                    get
                    {
                        return (ArealOrPopulationId != null) ?
                            ((NfiEstaDB)Composite.Database)
                            .STargetData.CArealOrPopulation[(int)ArealOrPopulationId] : null;
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not TDFnGetTargetVariableType, return False
                    if (obj is not TDFnGetTargetVariableType)
                    {
                        return false;
                    }

                    return
                        TargetVariableIds == ((TDFnGetTargetVariableType)obj).TargetVariableIds;
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        TargetVariableIds.GetHashCode();
                }

                /// <summary>
                /// Text description of target variable group object
                /// </summary>
                /// <returns> Text description of target variable group object</returns>
                public override string ToString()
                {
                    return ((NfiEstaDB)Composite.Database).Postgres.Setting.LanguageVersion switch
                    {
                        LanguageVersion.National =>
                            String.Concat(
                                $"{"TargetVariableIds"} = {Functions.PrepStringArg(TargetVariableIds)}.{Environment.NewLine}"
                                ),
                        LanguageVersion.International =>
                            String.Concat(
                                $"{"TargetVariableIds"} = {Functions.PrepStringArg(TargetVariableIds)}.{Environment.NewLine}"
                                ),
                        _ =>
                            String.Concat(
                                $"{"TargetVariableIds"} = {Functions.PrepStringArg(TargetVariableIds)}.{Environment.NewLine}"
                                ),
                    };
                }

                #endregion Methods

            }


            /// <summary>
            /// List of target variable groups
            /// </summary>
            public class TDFnGetTargetVariableTypeList
                : AParametrizedView
            {

                #region Static Fields

                /// <summary>
                /// Schema name
                /// </summary>
                public static readonly string Schema = TDSchema.Name;

                /// <summary>
                /// Stored procedure name
                /// </summary>
                public const string Name = "fn_get_target_variable";

                /// <summary>
                /// Stored procedure alias
                /// </summary>
                public const string Alias = "fn_get_target_variable";

                /// <summary>
                /// Description in national language
                /// </summary>
                public const string CaptionCs = "Uložená procedura, která vrací seznam skupin cílových proměnných";

                /// <summary>
                /// Description in English
                /// </summary>
                public const string CaptionEn = "Stored procedure that returns list of target variable groups";

                /// <summary>
                /// Columns metadata
                /// </summary>
                public static readonly Dictionary<string, ColumnMetadata> Cols = new()
                {
                    { "id", new ColumnMetadata()
                    {
                        Name = "id",
                        DbName = null,
                        DataType = "System.Int32",
                        DbDataType = null,
                        NewDataType = "int4",
                        PKey = true,
                        NotNull = true,
                        DefaultValue = "0",
                        FuncCall = "row_number() over()",
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "ID",
                        HeaderTextEn = "ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 0
                    }
                    },
                    { "core", new ColumnMetadata()
                    {
                        Name = "core",
                        DbName = "core",
                        DataType = "System.String",
                        DbDataType = "_bool",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = "array_to_string({0}, ';', 'NULL')",
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "CORE",
                        HeaderTextEn = "CORE",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 1
                    }
                    },
                    { "target_variable_ids", new ColumnMetadata()
                    {
                        Name = "target_variable_ids",
                        DbName = "id",
                        DataType = "System.String",
                        DbDataType = "_int4",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = "array_to_string({0}, ';', 'NULL')",
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "TARGET_VARIABLE_ID",
                        HeaderTextEn = "TARGET_VARIABLE_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 2
                    }
                    },
                    { "ldsity_object_group_id", new ColumnMetadata()
                    {
                        Name = "ldsity_object_group_id",
                        DbName = "ldsity_object_group",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "LDSITY_OBJECT_GROUP_ID",
                        HeaderTextEn = "LDSITY_OBJECT_GROUP_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 3
                    }
                    },
                    { "label_cs", new ColumnMetadata()
                    {
                        Name = "label_cs",
                        DbName = "label",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "LABEL_CS",
                        HeaderTextEn = "LABEL_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 4
                    }
                    },
                    { "description_cs", new ColumnMetadata()
                    {
                        Name = "description_cs",
                        DbName = "description",
                        DataType = "System.String",
                        DbDataType = "text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "DESCRIPTION_CS",
                        HeaderTextEn = "DESCRIPTION_CS",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 5
                    }
                    },
                    { "label_en", new ColumnMetadata()
                    {
                        Name = "label_en",
                        DbName = "label_en",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "LABEL_EN",
                        HeaderTextEn = "LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 6
                    }
                    },
                    { "description_en", new ColumnMetadata()
                    {
                        Name = "description_en",
                        DbName = "description_en",
                        DataType = "System.String",
                        DbDataType = "text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "DESCRIPTION_EN",
                        HeaderTextEn = "DESCRIPTION_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 7
                    }
                    },
                    { "state_or_change_id", new ColumnMetadata()
                    {
                        Name = "state_or_change_id",
                        DbName = "state_or_change",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "STATE_OR_CHANGE_ID",
                        HeaderTextEn = "STATE_OR_CHANGE_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 8
                    }
                    },
                    { "soc_label_en", new ColumnMetadata()
                    {
                        Name = "soc_label_en",
                        DbName = "soc_label",
                        DataType = "System.String",
                        DbDataType = "varchar",
                        NewDataType = "varchar",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "SOC_LABEL_EN",
                        HeaderTextEn = "SOC_LABEL_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 9
                    }
                    },
                    { "soc_description_en", new ColumnMetadata()
                    {
                        Name = "soc_description_en",
                        DbName = "soc_description",
                        DataType = "System.String",
                        DbDataType = "text",
                        NewDataType = "text",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = String.Empty,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = null,
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "SOC_DESCRIPTION_EN",
                        HeaderTextEn = "SOC_DESCRIPTION_EN",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 10
                    }
                    },
                    { "areal_or_population_id", new ColumnMetadata()
                    {
                        Name = "areal_or_population_id",
                        DbName = "areal_or_population",
                        DataType = "System.Int32",
                        DbDataType = "int4",
                        NewDataType = "int4",
                        PKey = false,
                        NotNull = false,
                        DefaultValue = null,
                        FuncCall = null,
                        Visible = false,
                        ReadOnly = true,
                        NumericFormat = "D",
                        Alignment = DataGridViewContentAlignment.MiddleLeft,
                        HeaderTextCs = "STATE_OR_CHANGE_ID",
                        HeaderTextEn = "STATE_OR_CHANGE_ID",
                        ToolTipTextCs = null,
                        ToolTipTextEn = null,
                        Width = 100,
                        Elemental = false,
                        DisplayIndex = 11
                    }
                    }
                };

                /// <summary>
                /// Column id metadata
                /// </summary>
                public static readonly ColumnMetadata ColId = Cols["id"];

                /// <summary>
                /// Column core metadata
                /// </summary>
                public static readonly ColumnMetadata ColCore = Cols["core"];

                /// <summary>
                /// Column target_variable_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColTargetVariableIds = Cols["target_variable_ids"];

                /// <summary>
                /// Column ldsity_object_group_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColLDsityObjectGroupId = Cols["ldsity_object_group_id"];

                /// <summary>
                /// Column label_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColLabelCs = Cols["label_cs"];

                /// <summary>
                /// Column description_cs metadata
                /// </summary>
                public static readonly ColumnMetadata ColDescriptionCs = Cols["description_cs"];

                /// <summary>
                /// Column label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColLabelEn = Cols["label_en"];

                /// <summary>
                /// Column description_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColDescriptionEn = Cols["description_en"];

                /// <summary>
                /// Column state_or_change_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColStateOrChangeId = Cols["state_or_change_id"];

                /// <summary>
                /// Column soc_label_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColSocLabelEn = Cols["soc_label_en"];

                /// <summary>
                /// Column soc_description_en metadata
                /// </summary>
                public static readonly ColumnMetadata ColSocDescriptionEn = Cols["soc_description_en"];

                /// <summary>
                /// Column areal_or_population_id metadata
                /// </summary>
                public static readonly ColumnMetadata ColArealOrPopulationId = Cols["areal_or_population_id"];

                #endregion Static Fields


                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable()
                {
                    return
                        ADatabaseTable.EmptyDataTable(
                            tableName: Name,
                            columns: Cols);
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    ADatabaseTable.SetColumnHeader(
                        columns: Cols,
                        columnName: columnName,
                        headerTextCs: headerTextCs,
                        headerTextEn: headerTextEn,
                        toolTipTextCs: toolTipTextCs,
                        toolTipTextEn: toolTipTextEn);
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    params string[] columnNames)
                {
                    ADatabaseTable.SetColumnsVisibility(
                        columns: Cols,
                        columnNames: columnNames);
                }

                #endregion Static Methods


                #region Private Fields

                /// <summary>
                /// 1st parameter: Group of local density objects identifier
                /// </summary>
                private Nullable<int> ldsityObjectGroupId;

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TDFnGetTargetVariableTypeList(
                    NfiEstaDB database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            data: data,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                {
                    LDsityObjectGroupId = null;
                }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public TDFnGetTargetVariableTypeList(
                    NfiEstaDB database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                            database: database,
                            rows: rows,
                            condition: condition,
                            limit: limit,
                            loadingTime: loadingTime,
                            storedProcedure: storedProcedure)
                {
                    LDsityObjectGroupId = null;
                }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// 1st parameter: Local density contribution identifier
                /// </summary>
                public Nullable<int> LDsityObjectGroupId
                {
                    get
                    {
                        return ldsityObjectGroupId;
                    }
                    set
                    {
                        ldsityObjectGroupId = value;
                    }
                }

                /// <summary>
                /// List of target variable groups (read-only)
                /// </summary>
                public List<TDFnGetTargetVariableType> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new TDFnGetTargetVariableType(composite: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// Target variable group from list by identifier (read-only)
                /// </summary>
                /// <param name="id">Target variable group identifier</param>
                /// <returns>Target variable group from list by identifier (null if not found)</returns>
                public TDFnGetTargetVariableType this[int id]
                {
                    get
                    {
                        return
                            Data.AsEnumerable()
                                .Where(a => a.Field<int>(ColId.Name) == id)
                                .Select(a => new TDFnGetTargetVariableType(composite: this, data: a))
                                .FirstOrDefault<TDFnGetTargetVariableType>();
                    }
                }

                #endregion Indexer


                #region Methods

                /// <summary>
                /// Copy of the object
                /// </summary>
                /// <returns>Copy of the object</returns>
                public TDFnGetTargetVariableTypeList Copy()
                {
                    IEnumerable<DataRow> rows = Data.AsEnumerable();
                    return
                        rows.Any() ?
                        new TDFnGetTargetVariableTypeList(
                            database: (NfiEstaDB)Database,
                            data: rows.CopyToDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: LoadingTime)
                        {
                            LDsityObjectGroupId = LDsityObjectGroupId
                        } :
                        new TDFnGetTargetVariableTypeList(
                            database: (NfiEstaDB)Database,
                            data: EmptyDataTable(),
                            condition: Condition,
                            limit: Limit,
                            loadingTime: null)
                        {
                            LDsityObjectGroupId = LDsityObjectGroupId
                        };
                }

                /// <summary>
                /// Converts return type of the function fn_get_target_variable
                /// into database table c_target_variable
                /// </summary>
                /// <returns></returns>
                public TDTargetVariableList ConvertToTargetVariableList()
                {
                    DataTable dt = TDTargetVariableList.EmptyDataTable();
                    List<DataRow> rows = [];

                    foreach (TDFnGetTargetVariableType group in Items)
                    {
                        if (group.TargetVariableIdList == null)
                        {
                            // Přeskočí chybný řádek: TargetVariableIdList nesmí být null
                            continue;
                        }
                        if (group.CoreList == null)
                        {
                            // Přeskočí chybný řádek: CoreList nesmí být null
                            continue;
                        }
                        if (group.TargetVariableIdList.Count != group.CoreList.Count)
                        {
                            // Přeskočí chybný řádek: TargetVariableIdList a CoreList
                            // musí mít stený počet prvků
                            continue;
                        }

                        for (int i = 0; i < group.TargetVariableIdList.Count; i++)
                        {
                            int id = group.TargetVariableIdList[i] ?? 0;
                            Nullable<bool> core = group.CoreList[i];

                            DataRow row = dt.NewRow();

                            Functions.SetIntArg(
                                row: row,
                                name: TDTargetVariableList.ColId.Name,
                                val: id);

                            Functions.SetStringArg(
                                row: row,
                                name: TDTargetVariableList.ColLabelCs.Name,
                                val: group.LabelCs);

                            Functions.SetStringArg(
                                row: row,
                                name: TDTargetVariableList.ColDescriptionCs.Name,
                                val: group.DescriptionCs);

                            Functions.SetIntArg(
                                row: row,
                                name: TDTargetVariableList.ColStateOrChangeId.Name,
                                val: group.StateOrChangeId ?? 0);

                            Functions.SetStringArg(
                                row: row,
                                name: TDTargetVariableList.ColLabelEn.Name,
                                val: group.LabelEn);

                            Functions.SetStringArg(
                                row: row,
                                name: TDTargetVariableList.ColDescriptionEn.Name,
                                val: group.DescriptionEn);

                            Functions.SetNIntArg(
                                row: row,
                                name: TDTargetVariableList.ColLDsityObjectGroupId.Name,
                                val: group.LDsityObjectGroupId);

                            Functions.SetNIntArg(
                                row: row,
                                name: TDTargetVariableList.ColArealOrPopulationId.Name,
                                val: group.ArealOrPopulationId);

                            Functions.SetNBoolArg(
                                row: row,
                                name: TDTargetVariableList.ColCore.Name,
                                val: core);

                            rows.Add(item: row);
                        }
                    }

                    return new TDTargetVariableList(
                        database: (NfiEstaDB)Database,
                        rows: rows);
                }

                #endregion Methods

            }

        }
    }
}