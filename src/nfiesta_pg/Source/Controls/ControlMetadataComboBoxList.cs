﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.PostgreSQL;

namespace ZaJi.NfiEstaPg.Controls
{

    /// <summary>
    /// <para lang="cs">Skupina ComboBox pro zobrazení metadatových prvků</para>
    /// <para lang="en">Group of ComboBoxes for displaying metadata elements</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    internal partial class ControlMetadataComboBoxList
        : UserControl, INfiEstaControl
    {

        #region Constants

        /// <summary>
        /// <para lang="cs">Výška ComboBox</para>
        /// <para lang="en">ComboBox height</para>
        /// </summary>
        private const int comboHeight = 25;

        /// <summary>
        /// <para lang="cs">Odsazení od okrajů</para>
        /// <para lang="en">Margin</para>
        /// </summary>
        private readonly static Padding margin = new()
        {
            Top = 10,
            Bottom = 10,
            Left = 10,
            Right = 30
        };

        #endregion Constants


        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Metadata pro zobrazení v ovládacím prvku</para>
        /// <para lang="en">Metadata for display in the control</para>
        /// </summary>
        private ITargetVariableMetadataList metadata;

        /// <summary>
        /// <para lang="cs">Metadata omezená výběrem v ovládacím prvku</para>
        /// <para lang="en">Metadata limited by selection in the control</para>
        /// </summary>
        private ITargetVariableMetadataList restrictedMetadata;

        #endregion Private Fields


        #region Events

        /// <summary>
        /// <para lang="cs">Událost "Změna vybrané cílové proměnné"</para>
        /// <para lang="en">"Selected target variable changed" event</para>
        /// </summary>
        public event EventHandler SelectedTargetVariableChanged;

        #endregion Events


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor ovládacího prvku</para>
        /// <para lang="en">Control constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        /// <param name="metadata">
        /// <para lang="cs">Metadata pro zobrazení v ovládacím prvku</para>
        /// <para lang="en">Metadata for display in the control</para>
        /// </param>
        public ControlMetadataComboBoxList(
            Control controlOwner,
            ITargetVariableMetadataList metadata)
        {
            InitializeComponent();
            Initialize(
                controlOwner: controlOwner,
                metadata: metadata);
        }

        #endregion Constructor


        #region Properties

        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku (not-null)</para>
        /// <para lang="en">Control owner (not-null)</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                if (controlOwner == null)
                {
                    throw new ArgumentNullException(
                        message: $"Argument {nameof(ControlOwner)} must not be null.",
                        paramName: nameof(ControlOwner));
                }

                if (controlOwner is not INfiEstaControl)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(INfiEstaControl)}.",
                        paramName: nameof(ControlOwner));
                }

                if (controlOwner is not ControlTargetVariableSelector)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(ControlTargetVariableSelector)}.",
                        paramName: nameof(ControlOwner));
                }

                return controlOwner;
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException(
                        message: $"Argument {nameof(ControlOwner)} must not be null.",
                        paramName: nameof(ControlOwner));
                }

                if (value is not INfiEstaControl)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(INfiEstaControl)}.",
                        paramName: nameof(ControlOwner));
                }

                if (value is not ControlTargetVariableSelector)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(ControlTargetVariableSelector)}.",
                        paramName: nameof(ControlOwner));
                }
                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables (read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        /// <summary>
        /// <para lang="cs">Vzhled ovládacího prvku "TargetVariableSelector" (read-only)</para>
        /// <para lang="en">Design of the user control "TargetVariableSelector" (read-only)</para>
        /// </summary>
        public TargetVariableSelectorDesign Design
        {
            get
            {
                return ((ControlTargetVariableSelector)ControlOwner).Design;
            }
        }

        /// <summary>
        /// <para lang="cs">Mezery mezi ComboBox ve skupině (read-only) (not-null)</para>
        /// <para lang="en">Gaps between ComboBoxes in the group (read-only) (not-null)</para>
        /// </summary>
        public List<int> Spacing
        {
            get
            {
                return ((ControlTargetVariableSelector)ControlOwner).Spacing;
            }
        }

        /// <summary>
        /// <para lang="cs">Přidané ovládací prvky (not-null)</para>
        /// <para lang="en">Additional controls (not-null)</para>
        /// </summary>
        public List<Control> AdditionalControls
        {
            get
            {
                return ((ControlTargetVariableSelector)ControlOwner).AdditionalControls;
            }
        }

        #endregion Common Properties

        #region Input Properties

        /// <summary>
        /// <para lang="cs">Metadata pro zobrazení v ovládacím prvku (not-null)</para>
        /// <para lang="en">Metadata for display in the control (not-null)</para>
        /// </summary>
        public ITargetVariableMetadataList Metadata
        {
            get
            {
                return metadata ??
                    throw new ArgumentNullException(
                        message: $"Argument {nameof(Metadata)} must not be null.",
                        paramName: nameof(Metadata));
            }
            set
            {
                metadata = value ??
                    throw new ArgumentNullException(
                        message: $"Argument {nameof(Metadata)} must not be null.",
                        paramName: nameof(Metadata));

                InitializeComboBoxList();
            }
        }

        #endregion Input Properties

        #region Output Properties

        /// <summary>
        /// <para lang="cs">Metadata omezená výběrem v ovládacím prvku (read-only) (not-null)</para>
        /// <para lang="en">Metadata limited by selection in the control (read-only) (not-null)</para>
        /// </summary>
        public ITargetVariableMetadataList RestrictedMetadata
        {
            get
            {
                return restrictedMetadata ??
                    Metadata.Empty();
            }
            private set
            {
                restrictedMetadata = value ??
                    Metadata.Empty();
                SelectedTargetVariableChanged?.Invoke(sender: this, e: null);
            }
        }

        #endregion Output Properties

        #endregion Properties


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku</para>
        /// <para lang="en">Initializing the control</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        /// <param name="metadata">
        /// <para lang="cs">Metadata pro zobrazení v ovládacím prvku</para>
        /// <para lang="en">Metadata for display in the control</para>
        /// </param>
        private void Initialize(
            Control controlOwner,
            ITargetVariableMetadataList metadata)
        {
            ControlOwner = controlOwner;

            Metadata = metadata;

            RestrictedMetadata = Metadata.Empty();

            InitializeLabels();

            Resize += new EventHandler((sender, e) => { ReDraw(); });
        }

        /// <summary>
        /// <para lang="cs">Inicializace seznamu ovládacích prvků MetadataComboBox</para>
        /// <para lang="en">Initializing the list of MetadataComboBox controls</para>
        /// </summary>
        private void InitializeComboBoxList()
        {
            Controls.Clear();

            List<MetadataElement> elements =
                (Design == TargetVariableSelectorDesign.Simple) ?
                    [.. MetadataElementList.Simple().Items
                            .Where(a => a.MetadataElementControl == MetadataElementControl.ComboBox)
                            .OrderBy(a => (int)a.MetadataElementType)] :
                    [.. MetadataElementList.Standard().Items
                            .Where(a => a.MetadataElementControl == MetadataElementControl.ComboBox)
                            .OrderBy(a => (int)a.MetadataElementType)];

            // Vytvoření MetadataComboBoxů
            int i = -1;
            foreach (MetadataElement element in elements)
            {
                i++;
                ControlMetadataComboBox metadataComboBox =
                    new(
                        controlOwner: this,
                        metadata: Metadata.Empty(),
                        elementType: element.MetadataElementType)
                    {
                        SerialNumber = i
                    };
                metadataComboBox.CreateControl();
                Controls.Add(value: metadataComboBox);
            }

            // Vytvoření panelu pro doplňující ovládací prvky
            Panel pnlAdditionalControls = new();
            foreach (Control control in AdditionalControls)
            {
                pnlAdditionalControls.Controls.Add(value: control);
            }
            pnlAdditionalControls.CreateControl();
            Controls.Add(value: pnlAdditionalControls);

            // Nastavení NextControl MetadataComboBoxů
            foreach (ControlMetadataComboBox metadataComboBox in
                Controls
                    .OfType<ControlMetadataComboBox>())
            {
                metadataComboBox.Next =
                    Controls
                        .OfType<ControlMetadataComboBox>()
                        .Where(a => a.SerialNumber == (metadataComboBox.SerialNumber + 1))
                        .FirstOrDefault();
            }

            // První MetadataComboBox
            ControlMetadataComboBox firstMetadataComboBox =
                Controls
                   .OfType<ControlMetadataComboBox>()
                   .Where(a => a.SerialNumber == 0)
                   .FirstOrDefault<ControlMetadataComboBox>();
            if (firstMetadataComboBox != null)
            {
                firstMetadataComboBox.Metadata = Metadata;
                firstMetadataComboBox.SelectedIndex = 0;
            }

            // Poslední MetadataComboBox
            ControlMetadataComboBox lastMetadataComboBox =
                Controls
                   .OfType<ControlMetadataComboBox>()
                   .Where(a => a.SerialNumber == i)
                   .FirstOrDefault<ControlMetadataComboBox>();
            if (lastMetadataComboBox != null)
            {
                lastMetadataComboBox.SelectedIndexChanged += new EventHandler(
                    (sender, e) => { RestrictedMetadata = lastMetadataComboBox.RestrictedMetadata; });
                RestrictedMetadata = lastMetadataComboBox.RestrictedMetadata;
            }

            ReDraw();
        }

        /// <summary>
        /// <para lang="cs">Nastaví rozměry MetadataComboBoxů v seznamu</para>
        /// <para lang="en">Reset size of MetadataComboBoxes in list</para>
        /// </summary>
        public void ReDraw()
        {
            int position = margin.Top;

            foreach (ControlMetadataComboBox metadataComboBox in
                Controls.OfType<ControlMetadataComboBox>())
            {
                int spacing =
                    (metadataComboBox.SerialNumber < Spacing.Count) ?
                        Spacing[metadataComboBox.SerialNumber] :
                        Spacing[^1];

                metadataComboBox.Anchor = AnchorStyles.None;

                metadataComboBox.Left = margin.Left;
                metadataComboBox.Top = position;

                metadataComboBox.Height = comboHeight;
                metadataComboBox.Width =
                    ((Width - (margin.Left + margin.Right)) >= 0) ?
                    (Width - (margin.Left + margin.Right)) : 0;

                metadataComboBox.ReDraw(width: metadataComboBox.Width);

                metadataComboBox.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;

                position += comboHeight + spacing;
            }

            foreach (Panel pnlAdditionalControls in
                Controls.OfType<Panel>())
            {
                pnlAdditionalControls.Left = margin.Left;
                pnlAdditionalControls.Top = position;
                pnlAdditionalControls.Height =
                    ((Height - position - margin.Bottom) >= 0) ?
                    (Height - position - margin.Bottom) : 0;
                pnlAdditionalControls.Width =
                    ((Width - (margin.Left + margin.Right)) >= 0) ?
                    (Width - (margin.Left + margin.Right)) : 0;
            }

            Refresh();
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků ovládacího prvku</para>
        /// <para lang="en">Initializing control labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            foreach (ControlMetadataComboBox metadataComboBox in
                Controls
                    .OfType<ControlMetadataComboBox>())
            {
                metadataComboBox.InitializeLabels();
            }
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent() { }

        #endregion Methods

    }

}
