﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.PostgreSQL;

namespace ZaJi.NfiEstaPg.Controls
{

    /// <summary>
    /// <para lang="cs">ComboBox pro zobrazení metadatového prvku</para>
    /// <para lang="en">ComboBox for metadata element display</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    internal partial class ControlMetadataComboBox
        : UserControl, INfiEstaControl
    {

        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Indikátor editace ovládacího prvku</para>
        /// <para lang="en">Control editing indicator</para>
        /// </summary>
        private bool onEdit;

        /// <summary>
        /// <para lang="cs">Metadata pro zobrazení v ovládacím prvku</para>
        /// <para lang="en">Metadata for display in the control</para>
        /// </summary>
        private ITargetVariableMetadataList metadata;

        /// <summary>
        /// <para lang="cs">Prvek metadat pro zobrazení</para>
        /// <para lang="en">Metadata element to display</para>
        /// </summary>
        private MetadataElementType elementType;

        /// <summary>
        /// <para lang="cs">Pořadové číslo metadatového prvku</para>
        /// <para lang="en">Serial number of the metadata element</para>
        /// </summary>
        private int serialNumber;

        /// <summary>
        /// <para lang="cs">ComboBox pro následující metadatový prvek</para>
        /// <para lang="en">ComboBox for the following metadata element</para>
        /// </summary>
        private ControlMetadataComboBox next;

        #endregion Private Fields


        #region Controls

        /// <summary>
        /// <para lang="cs">ComboBox pro zobrazení seznamu metadatových prvků</para>
        /// <para lang="en">ComboBox for displaying the list of metadata elements</para>
        /// </summary>
        private ComboBox cboMetadata;

        #endregion Controls


        #region Events

        /// <summary>
        /// <para lang="cs">Událost
        /// "Změna vybrané položky v seznamu"</para>
        /// <para lang="en">Event
        /// "Change a selected item in the list"</para>
        /// </summary>
        public event EventHandler SelectedIndexChanged;

        #endregion Events


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor ovládacího prvku</para>
        /// <para lang="en">Control constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        /// <param name="metadata">
        /// <para lang="cs">Metadata pro zobrazení v ovládacím prvku</para>
        /// <para lang="en">Metadata for display in the control</para>
        /// </param>
        /// <param name="elementType">
        /// <para lang="cs">Prvek metadat pro zobrazení</para>
        /// <para lang="en">Metadata element to display</para>
        /// </param>
        public ControlMetadataComboBox(
            Control controlOwner,
            ITargetVariableMetadataList metadata,
            MetadataElementType elementType)

        {
            InitializeComponent();
            Initialize(
                controlOwner: controlOwner,
                metadata: metadata,
                elementType: elementType);
        }

        #endregion Constructor


        #region Properties

        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku (not-null)</para>
        /// <para lang="en">Control owner (not-null)</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                if (controlOwner == null)
                {
                    throw new ArgumentNullException(
                        message: $"Argument {nameof(ControlOwner)} must not be null.",
                        paramName: nameof(ControlOwner));
                }

                if (controlOwner is not INfiEstaControl)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(INfiEstaControl)}.",
                        paramName: nameof(ControlOwner));
                }

                return controlOwner;
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException(
                        message: $"Argument {nameof(ControlOwner)} must not be null.",
                        paramName: nameof(ControlOwner));
                }

                if (value is not INfiEstaControl)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(INfiEstaControl)}.",
                        paramName: nameof(ControlOwner));
                }

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables (read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        #endregion Common Properties

        #region Input Properties

        /// <summary>
        /// <para lang="cs">Metadata pro zobrazení v ovládacím prvku (not-null)</para>
        /// <para lang="en">Metadata for display in the control (not-null)</para>
        /// </summary>
        public ITargetVariableMetadataList Metadata
        {
            get
            {
                return metadata ??
                    throw new ArgumentNullException(
                        message: $"Argument {nameof(Metadata)} must not be null.",
                        paramName: nameof(Metadata));
            }
            set
            {
                metadata = value ??
                    throw new ArgumentNullException(
                        message: $"Argument {metadata} must not be null.",
                        paramName: nameof(Metadata));

                InitializeComboBox();
            }
        }

        /// <summary>
        /// <para lang="cs">Prvek metadat pro zobrazení (read-only)</para>
        /// <para lang="en">Metadata element to display (read-only)</para>
        /// </summary>
        public MetadataElementType ElementType
        {
            get
            {
                return elementType;
            }
            private set
            {
                elementType = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Pořadové číslo metadatového prvku</para>
        /// <para lang="en">Serial number of the metadata element</para>
        /// </summary>
        public int SerialNumber
        {
            get
            {
                return serialNumber;
            }
            set
            {
                serialNumber = value;
            }
        }

        /// <summary>
        /// <para lang="cs">ComboBox pro následující metadatový prvek (nullable)</para>
        /// <para lang="en">ComboBox for the following metadata element (nullable)</para>
        /// </summary>
        public ControlMetadataComboBox Next
        {
            get
            {
                return next;
            }
            set
            {
                next = value;
            }
        }

        #endregion Input Properties

        #region Output Properties

        /// <summary>
        /// <para lang="cs">Vybraná položka metadat (read-only) (nullable)</para>
        /// <para lang="en">Selected metadata item (read-only) (nullable)</para>
        /// </summary>
        public MetadataElement SelectedItem
        {
            get
            {
                if (cboMetadata == null)
                {
                    return null;
                }

                MetadataElement selectedItem =
                   (cboMetadata.SelectedItem != null) ?
                   (MetadataElement)cboMetadata.SelectedItem : null;

                return selectedItem;
            }
        }

        /// <summary>
        /// <para lang="cs">Index vybrané položky v ComboBoxu</para>
        /// <para lang="en">Selected ComboBoxuItem index</para>
        /// </summary>
        public int SelectedIndex
        {
            get
            {
                if (cboMetadata == null)
                {
                    return -1;
                }

                return cboMetadata.SelectedIndex;
            }
            set
            {
                if (cboMetadata == null)
                {
                    return;
                }

                if (cboMetadata.Items == null)
                {
                    return;
                }

                if (value >= cboMetadata.Items.Count)
                {
                    return;
                }

                if (value < 0)
                {
                    return;
                }

                onEdit = true;
                cboMetadata.SelectedIndex = value;
                onEdit = false;

                RaiseSelectedIndexChanged(
                    sender: this,
                    e: new EventArgs());
            }
        }

        /// <summary>
        /// <para lang="cs">Metadata omezená výběrem v ovládacím prvku (read-only) (not-null)</para>
        /// <para lang="en">Metadata limited by selection in the control (read-only) (not-null)</para>
        /// </summary>
        public ITargetVariableMetadataList RestrictedMetadata
        {
            get
            {
                if (SelectedItem == null)
                {
                    return Metadata;
                }

                return
                    Metadata.Reduce(
                        elementType: ElementType,
                        element: SelectedItem);
            }
        }

        #endregion Output Properties

        #endregion Properties


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku</para>
        /// <para lang="en">Initializing the control</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        /// <param name="metadata">
        /// <para lang="cs">Metadata pro zobrazení v ovládacím prvku</para>
        /// <para lang="en">Metadata for display in the control</para>
        /// </param>
        /// <param name="elementType">
        /// <para lang="cs">Prvek metadat pro zobrazení</para>
        /// <para lang="en">Metadata element to display</para>
        /// </param>
        public void Initialize(
            Control controlOwner,
            ITargetVariableMetadataList metadata,
            MetadataElementType elementType)
        {
            ControlOwner = controlOwner;

            onEdit = false;

            ElementType = elementType;

            SerialNumber = 0;

            Next = null;

            cboMetadata = null;

            Metadata = metadata;

            InitializeLabels();
        }

        /// <summary>
        /// <para lang="cs">Inicializace seznamu metadatového prvku</para>
        /// <para lang="en">Initializing the metadata element list</para>
        /// </summary>
        public void InitializeComboBox()
        {
            pnlMetadata.Controls.Clear();
            List<MetadataElement> dataSource = LanguageVersion switch
            {
                LanguageVersion.National => [.. Metadata.GetMetadataElementsOfType(elementType: ElementType)
                        .OrderBy(a => (a.TargetVariableId == 0) ? 0 : 1)
                        .ThenBy(a => a.LabelCs)],
                LanguageVersion.International => [.. Metadata.GetMetadataElementsOfType(elementType: ElementType)
                        .OrderBy(a => (a.TargetVariableId == 0) ? 0 : 1)
                        .ThenBy(a => a.LabelEn)],
                _ => [.. Metadata.GetMetadataElementsOfType(elementType: ElementType).OrderBy(a => a.TargetVariableId)],
            };
            onEdit = true;
            if (dataSource.Count != 0)
            {
                cboMetadata = new ComboBox()
                {
                    BackColor = (dataSource.Count == 1) ?
                        System.Drawing.SystemColors.Control :
                        System.Drawing.SystemColors.Window,
                    DataSource = dataSource,
                    DisplayMember =
                         (LanguageVersion == LanguageVersion.National) ?
                            "LabelCs" :
                         (LanguageVersion == LanguageVersion.International) ?
                            "LabelEn" :
                            String.Empty,
                    DropDownStyle = ComboBoxStyle.DropDownList,
                    FlatStyle = FlatStyle.Flat,
                    Font = new System.Drawing.Font(
                        familyName: "Microsoft Sans Serif",
                        emSize: 9F,
                        style: System.Drawing.FontStyle.Regular,
                        unit: System.Drawing.GraphicsUnit.Point,
                        gdiCharSet: ((byte)(238))),
                    Margin = new Padding(all: 0),
                    SelectedIndex = -1
                };

                cboMetadata.SelectedIndexChanged += (sender, e) =>
                {
                    if (!onEdit)
                        RaiseSelectedIndexChanged(sender: this, e: e);
                };

                ToolTip toolTip = new()
                {
                    AutoPopDelay = 5000,
                    InitialDelay = 1000,
                    IsBalloon = false,
                    ReshowDelay = 0,
                    ShowAlways = true,
                    UseAnimation = true,
                    UseFading = true,
                    ToolTipTitle = MetadataElement.Caption(
                                                elementType: ElementType,
                                                languageVersion: LanguageVersion)
                };

                cboMetadata.MouseHover += new EventHandler(
                    (sender, e) =>
                    {
                        toolTip.SetToolTip(
                            control: cboMetadata,
                            caption: cboMetadata.Text);
                    });

                cboMetadata.CreateControl();

                pnlMetadata.Controls.Add(value: cboMetadata);

                if (cboMetadata.Items.Count > 0)
                {
                    cboMetadata.SelectedIndex = 0;
                }

                ReDraw(width: Width);
            }
            else
            {
                cboMetadata = null;
            }
            onEdit = false;
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků ovládacího prvku</para>
        /// <para lang="en">Initializing control labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            lblName.Text =
                MetadataElement.Caption(
                    elementType: ElementType,
                    languageVersion: LanguageVersion);

            if (cboMetadata != null)
            {
                onEdit = true;
                cboMetadata.DisplayMember =
                    (LanguageVersion == LanguageVersion.National) ?
                       "LabelCs" :
                    (LanguageVersion == LanguageVersion.International) ?
                       "LabelEn" :
                        String.Empty;
                onEdit = false;
            }
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent() { }

        /// <summary>
        /// <para lang="cs">Překreslení ComboBox</para>
        /// <para lang="en">Redraw ComboBox</para>
        /// </summary>
        public void ReDraw(int width)
        {
            foreach (Control control in pnlMetadata.Controls)
            {
                control.Anchor = AnchorStyles.None;
                control.Dock = DockStyle.None;

                control.Top = 0;
                control.Left = 0;
                control.Width = width - 177;

                control.Refresh();
            }
        }

        /// <summary>
        /// <para lang="cs">Vyvolání události
        /// "Změna vybrané položky v seznamu"</para>
        /// <para lang="en">Invoking the event
        /// "Change a selected item in the list"</para>
        /// </summary>
        /// <param name="sender">
        /// <para lang="cs">Objekt odesílající událost (ControlMetadataComboBox)</para>
        /// <para lang="en">Object that sends the event (ControlMetadataComboBox)</para>
        /// </param>
        /// <param name="e">
        /// <para lang="cs">Parametry události</para>
        /// <para lang="en">Parameters of the event</para>
        /// </param>
        private void RaiseSelectedIndexChanged(object sender, EventArgs e)
        {
            if (Next != null)
            {
                Next.Metadata = RestrictedMetadata;
                Next.SelectedIndex = 0;
                Next.ReDraw(width: Width);
            }

            SelectedIndexChanged?.Invoke(
                sender: sender,
                e: e);
        }

        #endregion Methods;

    }

}