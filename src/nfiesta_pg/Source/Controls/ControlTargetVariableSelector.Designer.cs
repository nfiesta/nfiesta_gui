﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi.NfiEstaPg.Controls
{

    partial class ControlTargetVariableSelector
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlMain = new System.Windows.Forms.Panel();
            this.splTargetVariable = new System.Windows.Forms.SplitContainer();
            this.grpTargetVariable = new System.Windows.Forms.GroupBox();
            this.pnlTargetVariable = new System.Windows.Forms.Panel();
            this.grpMetadata = new System.Windows.Forms.GroupBox();
            this.pnlMetadata = new System.Windows.Forms.Panel();
            this.pnlMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splTargetVariable)).BeginInit();
            this.splTargetVariable.Panel1.SuspendLayout();
            this.splTargetVariable.Panel2.SuspendLayout();
            this.splTargetVariable.SuspendLayout();
            this.grpTargetVariable.SuspendLayout();
            this.grpMetadata.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlMain
            // 
            this.pnlMain.BackColor = System.Drawing.SystemColors.HotTrack;
            this.pnlMain.Controls.Add(this.splTargetVariable);
            this.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMain.Location = new System.Drawing.Point(0, 0);
            this.pnlMain.Margin = new System.Windows.Forms.Padding(0);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Padding = new System.Windows.Forms.Padding(3);
            this.pnlMain.Size = new System.Drawing.Size(960, 540);
            this.pnlMain.TabIndex = 4;
            // 
            // splTargetVariable
            // 
            this.splTargetVariable.BackColor = System.Drawing.SystemColors.HotTrack;
            this.splTargetVariable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splTargetVariable.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splTargetVariable.Location = new System.Drawing.Point(3, 3);
            this.splTargetVariable.Margin = new System.Windows.Forms.Padding(0);
            this.splTargetVariable.Name = "splTargetVariable";
            // 
            // splTargetVariable.Panel1
            // 
            this.splTargetVariable.Panel1.BackColor = System.Drawing.SystemColors.Control;
            this.splTargetVariable.Panel1.Controls.Add(this.grpTargetVariable);
            this.splTargetVariable.Panel1.Padding = new System.Windows.Forms.Padding(5);
            this.splTargetVariable.Panel1MinSize = 0;
            // 
            // splTargetVariable.Panel2
            // 
            this.splTargetVariable.Panel2.BackColor = System.Drawing.SystemColors.Control;
            this.splTargetVariable.Panel2.Controls.Add(this.grpMetadata);
            this.splTargetVariable.Panel2.Padding = new System.Windows.Forms.Padding(5);
            this.splTargetVariable.Panel2MinSize = 0;
            this.splTargetVariable.Size = new System.Drawing.Size(954, 534);
            this.splTargetVariable.SplitterDistance = 250;
            this.splTargetVariable.SplitterWidth = 1;
            this.splTargetVariable.TabIndex = 1;
            // 
            // grpTargetVariable
            // 
            this.grpTargetVariable.Controls.Add(this.pnlTargetVariable);
            this.grpTargetVariable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpTargetVariable.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.grpTargetVariable.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.grpTargetVariable.Location = new System.Drawing.Point(5, 5);
            this.grpTargetVariable.Margin = new System.Windows.Forms.Padding(5);
            this.grpTargetVariable.Name = "grpTargetVariable";
            this.grpTargetVariable.Padding = new System.Windows.Forms.Padding(5);
            this.grpTargetVariable.Size = new System.Drawing.Size(240, 524);
            this.grpTargetVariable.TabIndex = 8;
            this.grpTargetVariable.TabStop = false;
            this.grpTargetVariable.Text = "grpTargetVariable";
            // 
            // pnlTargetVariable
            // 
            this.pnlTargetVariable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlTargetVariable.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.pnlTargetVariable.ForeColor = System.Drawing.SystemColors.WindowText;
            this.pnlTargetVariable.Location = new System.Drawing.Point(5, 20);
            this.pnlTargetVariable.Margin = new System.Windows.Forms.Padding(0);
            this.pnlTargetVariable.Name = "pnlTargetVariable";
            this.pnlTargetVariable.Size = new System.Drawing.Size(230, 499);
            this.pnlTargetVariable.TabIndex = 0;
            // 
            // grpMetadata
            // 
            this.grpMetadata.Controls.Add(this.pnlMetadata);
            this.grpMetadata.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpMetadata.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.grpMetadata.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.grpMetadata.Location = new System.Drawing.Point(5, 5);
            this.grpMetadata.Margin = new System.Windows.Forms.Padding(5);
            this.grpMetadata.Name = "grpMetadata";
            this.grpMetadata.Padding = new System.Windows.Forms.Padding(5);
            this.grpMetadata.Size = new System.Drawing.Size(693, 524);
            this.grpMetadata.TabIndex = 11;
            this.grpMetadata.TabStop = false;
            this.grpMetadata.Text = "grpMetadata";
            // 
            // pnlMetadata
            // 
            this.pnlMetadata.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMetadata.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.pnlMetadata.ForeColor = System.Drawing.SystemColors.WindowText;
            this.pnlMetadata.Location = new System.Drawing.Point(5, 20);
            this.pnlMetadata.Margin = new System.Windows.Forms.Padding(0);
            this.pnlMetadata.Name = "pnlMetadata";
            this.pnlMetadata.Size = new System.Drawing.Size(683, 499);
            this.pnlMetadata.TabIndex = 12;
            // 
            // ControlTargetVariableSelector
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnlMain);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "ControlTargetVariableSelector";
            this.Size = new System.Drawing.Size(960, 540);
            this.pnlMain.ResumeLayout(false);
            this.splTargetVariable.Panel1.ResumeLayout(false);
            this.splTargetVariable.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splTargetVariable)).EndInit();
            this.splTargetVariable.ResumeLayout(false);
            this.grpTargetVariable.ResumeLayout(false);
            this.grpMetadata.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.SplitContainer splTargetVariable;
        private System.Windows.Forms.GroupBox grpTargetVariable;
        private System.Windows.Forms.Panel pnlTargetVariable;
        private System.Windows.Forms.GroupBox grpMetadata;
        private System.Windows.Forms.Panel pnlMetadata;
    }

}
