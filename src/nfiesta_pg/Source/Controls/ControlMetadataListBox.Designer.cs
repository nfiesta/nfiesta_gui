﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi.NfiEstaPg.Controls
{

    partial class ControlMetadataListBox
    {

        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlMetadata = new System.Windows.Forms.Panel();
            this.listBox = new System.Windows.Forms.ListBox();
            this.pnlMetadata.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlMetadata
            // 
            this.pnlMetadata.Controls.Add(this.listBox);
            this.pnlMetadata.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMetadata.Location = new System.Drawing.Point(0, 0);
            this.pnlMetadata.Margin = new System.Windows.Forms.Padding(0);
            this.pnlMetadata.Name = "pnlMetadata";
            this.pnlMetadata.Size = new System.Drawing.Size(360, 360);
            this.pnlMetadata.TabIndex = 0;
            // 
            // listBox
            // 
            this.listBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBox.FormattingEnabled = true;
            this.listBox.Location = new System.Drawing.Point(0, 0);
            this.listBox.Name = "listBox";
            this.listBox.Size = new System.Drawing.Size(360, 360);
            this.listBox.TabIndex = 0;
            // 
            // ControlMetadataListBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnlMetadata);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "ControlMetadataListBox";
            this.Size = new System.Drawing.Size(360, 360);
            this.pnlMetadata.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlMetadata;
        private System.Windows.Forms.ListBox listBox;
    }

}
