﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.PostgreSQL;

namespace ZaJi.NfiEstaPg.Controls
{

    /// <summary>
    /// <para lang="cs">Ovládací prvek "Volba cílové proměnné z json souboru metadat"</para>
    /// <para lang="en">Control "Target variable selection from json file of metadata"</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    public partial class ControlTargetVariableSelector
            : UserControl, INfiEstaControl
    {

        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Databázové tabulky</para>
        /// <para lang="en">Database tables</para>
        /// </summary>
        private NfiEstaDB database;

        /// <summary>
        /// <para lang="cs">Indikátor editace ovládacího prvku</para>
        /// <para lang="en">Control editing indicator</para>
        /// </summary>
        private bool onEdit;

        /// <summary>
        /// <para lang="cs">Indikátor inicializace ovládacího prvku</para>
        /// <para lang="en">Control inicialization indicator</para>
        /// </summary>
        private bool onInit;

        /// <summary>
        /// <para lang="cs">
        /// Tématický okruh
        /// (pro který má komponenta zobrazit cílové proměnné)
        /// (pokud je null, pak se zobrazují všechny cílové proměnné)
        /// </para>
        /// <para lang="en">
        /// Topic
        /// (for which the component should display the target variables)
        /// (if null, then all target variables are displayed)</para>
        /// </summary>
        private Core.Topic topic;

        /// <summary>
        /// <para lang="cs">
        /// Jednotka
        /// (pro kterou má komponenta zobrazit cílové proměnné)
        /// (pokud je null, pak se zobrazují všechny cílové proměnné)
        /// </para>
        /// <para lang="en">
        /// Unit
        /// (for which the component should display the target variables)
        /// (if null, then all target variables are displayed)
        /// </para>
        /// </summary>
        private Unit unit;

        /// <summary>
        /// Design of the user control "TargetVariableSelector"
        /// </summary>
        private TargetVariableSelectorDesign design;

        /// <summary>
        /// <para lang="cs">Mezery mezi ComboBox ve skupině</para>
        /// <para lang="en">Gaps between ComboBoxes in the group</para>
        /// </summary>
        private List<int> spacing;

        /// <summary>
        /// <para lang="cs">
        /// Zobrazuje se kategorie nepřiřazeno?
        /// </para>
        /// <para lang="en">
        /// Is category "not assigned" displayed?"
        /// </para>
        /// </summary>
        private bool notAssignedCategory;

        /// <summary>
        /// <para lang="cs">Přidané ovládací prvky (not-null)</para>
        /// <para lang="en">Additional controls (not-null)</para>
        /// </summary>
        private List<Control> additionalControls;

        /// <summary>
        /// <para lang="cs">Vstupní data: Seznam cílových proměnných</para>
        /// <para lang="en">Input data: List of the target variables</para>
        /// </summary>
        private ITargetVariableList data;

        #endregion Private Fields


        #region Events

        /// <summary>
        /// <para lang="cs">Událost "Změna vybrané cílové proměnné"</para>
        /// <para lang="en">"Selected target variable changed" event</para>
        /// </summary>
        public event EventHandler SelectedTargetVariableChanged;

        /// <summary>
        /// <para lang="cs">Událost "Změna vybrané cílové proměnné"</para>
        /// <para lang="en">"Selected target variable changed" event</para>
        /// </summary>
        public event EventHandler SplitterMoved;

        #endregion Events


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor ovládacího prvku</para>
        /// <para lang="en">Control constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        public ControlTargetVariableSelector(Control controlOwner)
        {
            InitializeComponent();
            Initialize(controlOwner: controlOwner);
        }

        #endregion Constructor


        #region Properties

        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku (not-null)</para>
        /// <para lang="en">Control owner (not-null)</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                if (controlOwner == null)
                {
                    throw new ArgumentNullException(
                        message: $"Argument {nameof(ControlOwner)} must not be null.",
                        paramName: nameof(ControlOwner));
                }

                if (controlOwner is not INfiEstaControl)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(INfiEstaControl)}.",
                        paramName: nameof(ControlOwner));
                }

                return controlOwner;
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException(
                        message: $"Argument {nameof(ControlOwner)} must not be null.",
                        paramName: nameof(ControlOwner));
                }

                if (value is not INfiEstaControl)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(INfiEstaControl)}.",
                        paramName: nameof(ControlOwner));
                }

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky</para>
        /// <para lang="en">Database tables</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return database ??
                    ((INfiEstaControl)ControlOwner).Database;
            }
            set
            {
                database = value ??
                    ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        #endregion Common Properties


        #region Controls

        /// <summary>
        /// <para lang="cs">ListBox pro zobrazení seznamu cílových proměnných (read-only) (nullable)</para>
        /// <para lang="en">ListBox for displaying the list of target variables (read-only) (nullable)</para>
        /// </summary>
        private ControlMetadataListBox LstTargetVariable
        {
            get
            {
                if (Design != TargetVariableSelectorDesign.Standard)
                {
                    return null;
                }

                if (pnlTargetVariable.Controls.Count != 1)
                {
                    CreateNewMetadataListBox();
                }
                if (pnlTargetVariable.Controls[0] is not ControlMetadataListBox)
                {
                    CreateNewMetadataListBox();
                }
                return (ControlMetadataListBox)
                    pnlTargetVariable.Controls[0];
            }
        }

        /// <summary>
        /// <para lang="cs">Skupina ComboBox pro zobrazení metadatových prvků (read-only) (not-null)</para>
        /// <para lang="en">Group of ComboBoxes for displaying metadata elements (read-only) (not-null)</para>
        /// </summary>
        private ControlMetadataComboBoxList LstMetadata
        {
            get
            {
                if (pnlMetadata.Controls.Count != 1)
                {
                    CreateNewMetadataComboBoxList();
                }
                if (pnlMetadata.Controls[0] is not ControlMetadataComboBoxList)
                {
                    CreateNewMetadataComboBoxList();
                }
                return (ControlMetadataComboBoxList)
                    pnlMetadata.Controls[0];
            }
        }

        /// <summary>
        /// <para lang="cs">Přidané ovládací prvky (not-null)</para>
        /// <para lang="en">Additional controls (not-null)</para>
        /// </summary>
        public List<Control> AdditionalControls
        {
            get
            {
                return additionalControls ?? [];
            }
            set
            {
                additionalControls = value ?? [];
            }
        }

        #endregion Controls


        #region Design Properties

        /// <summary>
        /// <para lang="cs">Popisek pro GroupBox "Indikátor" (not-null)</para>
        /// <para lang="en">Label of GroupBox "Indicator" (not-null)</para>
        /// </summary>
        public string MetadataIndicatorText
        {
            get
            {
                return grpTargetVariable.Text;
            }
            set
            {
                grpTargetVariable.Text =
                    String.IsNullOrEmpty(value) ?
                        String.Empty : value;
            }
        }

        /// <summary>
        /// <para lang="cs">Popisek pro GroupBox "Skupina metadatových prvků" (not-null)</para>
        /// <para lang="en">Label of GroupBox "Group of metadata elements" (not-null)</para>
        /// </summary>
        public string MetadataElementsText
        {
            get
            {
                return grpMetadata.Text;
            }
            set
            {
                grpMetadata.Text =
                    String.IsNullOrEmpty(value) ?
                        String.Empty : value;
            }
        }

        /// <summary>
        /// <para lang="cs">Vzdálenost oddelovače mezi GroupBox "Indikátor" a GroupBox "Skupina metadatových prvků"</para>
        /// <para lang="en">Splitter distance between GroupBox "Indicator" and GroupBox "Group of metadata elements"</para>
        /// </summary>
        public int SplitterDistance
        {
            get
            {
                return splTargetVariable.SplitterDistance;
            }
            set
            {
                if (value > 0 && value < splTargetVariable.Width)
                {
                    onEdit = true;
                    splTargetVariable.SplitterDistance = value;
                    onEdit = false;
                }
            }
        }

        /// <summary>
        /// <para lang="cs">Varianta ovládacího prvku "TargetVariableSelector"</para>
        /// <para lang="en">Design of the user control "TargetVariableSelector"</para>
        /// </summary>
        public TargetVariableSelectorDesign Design
        {
            get
            {
                return design;
            }
            set
            {
                design = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Mezery mezi ComboBox ve skupině (not-null)</para>
        /// <para lang="en">Gaps between ComboBoxes in the group (not-null)</para>
        /// </summary>
        public List<int> Spacing
        {
            get
            {
                return spacing;
            }
            set
            {
                spacing =
                    (value == null) ? [-1] :
                    (value.Count == 0) ? [-1] :
                    value;

                for (int i = 0; i < spacing.Count; i++)
                {
                    spacing[i] = (spacing[i] < -1) ? -1 : spacing[i];
                }
            }
        }

        /// <summary>
        /// <para lang="cs">
        /// Zobrazuje se kategorie nepřiřazeno?
        /// </para>
        /// <para lang="en">
        /// Is category "not assigned" displayed?"
        /// </para>
        /// </summary>
        public bool NotAssignedCategory
        {
            get
            {
                return notAssignedCategory;
            }
            set
            {
                notAssignedCategory = value;
            }
        }

        #endregion Desing Properties


        #region Input Properties

        /// <summary>
        /// <para lang="cs">Vstupní data: Seznam cílových proměnných</para>
        /// <para lang="en">Input data: List of the target variables</para>
        /// </summary>
        public ITargetVariableList Data
        {
            get
            {
                if (data == null)
                {
                    throw new ArgumentNullException(
                        message: $"Argument {nameof(Data)} must not be null.",
                        paramName: nameof(Data));
                }

                return data;
            }
            set
            {
                data = value ??
                    throw new ArgumentNullException(
                        message: $"Argument {nameof(Data)} must not be null.",
                        paramName: nameof(Data));
            }
        }

        /// <summary>
        /// <para lang="cs">
        /// Tématický okruh
        /// (pro který má komponenta zobrazit cílové proměnné)
        /// (pokud je null, pak se zobrazují všechny cílové proměnné)
        /// </para>
        /// <para lang="en">
        /// Topic
        /// (for which the component should display the target variables)
        /// (if null, then all target variables are displayed)</para>
        /// </summary>
        public Core.Topic Topic
        {
            get
            {
                return topic;
            }
            set
            {
                topic = value;
            }
        }

        /// <summary>
        /// <para lang="cs">
        /// Jednotka
        /// (pro kterou má komponenta zobrazit cílové proměnné)
        /// (pokud je null, pak se zobrazují všechny cílové proměnné)
        /// </para>
        /// <para lang="en">
        /// Unit
        /// (for which the component should display the target variables)
        /// (if null, then all target variables are displayed)</para>
        /// </summary>
        public Unit Unit
        {
            get
            {
                return unit;
            }
            set
            {
                unit = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Seznam mapování cílových proměnných do témat (read-only)</para>
        /// <para lang="en">List of mappings between target variable and topic (read-only)</para>
        /// </summary>
        public Core.TargetVariableToTopicList CmTargetVariableToTopic
        {
            get
            {
                return Database.SNfiEsta.CmTargetVariableToTopic;
            }
        }

        #endregion Input Properties


        #region Output Properties

        /// <summary>
        /// <para lang="cs">Metadata cílových proměnných (bez agregace) (read-only) (not-null)</para>
        /// <para lang="en">Metadata of the target variables (without aggregation) (read-only) (not-null)</para>
        /// </summary>
        private ITargetVariableMetadataList MetadataOriginal
        {
            get
            {
                if (Data is Core.FnEtlGetTargetVariableTypeList fnEtlGetTargetVariableCore)
                {
                    Core.FnEtlGetTargetVariableMetadataList metadata =
                        new(database: Database);
                    metadata.ReLoad(fnEtlGetTargetVariable: fnEtlGetTargetVariableCore);
                    return metadata;
                }

                if (Data is TargetData.TDFnEtlGetTargetVariableTypeList fnTDEtlGetTargetVariable)
                {
                    TargetData.TDFnEtlGetTargetVariableMetadataList metadata =
                        new(database: Database);
                    metadata.ReLoad(fnEtlGetTargetVariable: fnTDEtlGetTargetVariable);
                    return metadata;
                }

                if (Data is Core.TargetVariableList cTargetVariable)
                {
                    Core.TargetVariableMetadataList metadata =
                        new(database: Database);
                    metadata.ReLoad(cTargetVariable: cTargetVariable);
                    return metadata;
                }

                return new Core.TargetVariableMetadataList(database: Database);
            }
        }

        /// <summary>
        /// <para lang="cs">Má JSON soubor obsahující metadata cílových proměnných validní formát? (ano: true, ne: false) (read-only)</para>
        /// <para lang="en">Is JSON file that contains target variables metadata in valid format? (yes: true, no: false) (read-only)</para>
        /// </summary>
        public bool JsonValidity
        {
            get
            {
                return !MetadataOriginal.Items.Where(a => !a.JsonValidity).Any();
            }
        }

        /// <summary>
        /// <para lang="cs">Metadata cílových proměnných (s agregací) (read-only) (not-null)</para>
        /// <para lang="en">Metadata of the target variables (with aggregation) (read-only) (not-null)</para>
        /// </summary>
        public ITargetVariableMetadataList Metadata
        {
            get
            {
                if (Data is Core.FnEtlGetTargetVariableTypeList fnEtlGetTargetVariableCore)
                {
                    Core.FnEtlGetTargetVariableMetadataList metadata =
                        new(database: Database);
                    metadata.ReLoad(fnEtlGetTargetVariable: fnEtlGetTargetVariableCore);
                    return metadata.Aggregated();
                }

                if (Data is TargetData.TDFnEtlGetTargetVariableTypeList fnTDEtlGetTargetVariable)
                {
                    TargetData.TDFnEtlGetTargetVariableMetadataList metadata =
                        new(database: Database);
                    metadata.ReLoad(fnEtlGetTargetVariable: fnTDEtlGetTargetVariable);
                    return metadata.Aggregated();
                }

                if (Data is Core.TargetVariableList cTargetVariable)
                {
                    Core.TargetVariableMetadataList metadata =
                        new(database: Database);
                    metadata.ReLoad(cTargetVariable: cTargetVariable);
                    return metadata.Aggregated();
                }

                return new Core.TargetVariableMetadataList(database: Database);
            }
        }

        /// <summary>
        /// <para lang="cs">
        /// Metadata cílových proměnných v tématickém okruhu a pro vybranou jednotku
        /// (read-only) (not-null)
        /// </para>
        /// <para lang="en">
        /// Metadata of the target variables for the topic and for the selected unit
        /// (read-only) (not-null)
        /// </para>
        /// </summary>
        public ITargetVariableMetadataList MetadataForTopicAndUnit
        {
            get
            {
                IEnumerable<DataRow> rows;

                if ((Topic == null) && (Unit == null))
                {
                    // Metadata cílových proměnných
                    rows = Metadata.Data.AsEnumerable();
                }

                else if ((Topic != null) && (Unit == null))
                {
                    // Metadata cílových proměnných v tématickém okruhu
                    rows =
                            from a in Metadata.Data.AsEnumerable()
                            join b in CmTargetVariableToTopic.Data.AsEnumerable()
                            on a.Field<int>(columnName: Core.TargetVariableMetadataList.ColTargetVariableId.Name)
                            equals b.Field<int>(columnName: Core.TargetVariableToTopicList.ColTargetVariableId.Name)
                            where
                            b.Field<int>(columnName: Core.TargetVariableToTopicList.ColTopicId.Name) == Topic.Id
                            select a;
                }

                else if ((Topic == null) && (Unit != null))
                {
                    // Metadata cílových proměnných pro vybranou jednotku
                    rows =
                         from a in Metadata.Data.AsEnumerable()
                         join b in CmTargetVariableToTopic.Data.AsEnumerable()
                         on a.Field<int>(columnName: Core.TargetVariableMetadataList.ColTargetVariableId.Name)
                            equals b.Field<int>(columnName: Core.TargetVariableToTopicList.ColTargetVariableId.Name)
                         where
                             ((a.Field<string>(columnName: Core.TargetVariableMetadataList.ColUnitLabelCs.Name) ?? String.Empty)
                             == (Unit.Numerator.LabelCs ?? String.Empty)) &&
                             ((a.Field<string>(columnName: Core.TargetVariableMetadataList.ColUnitDescriptionCs.Name) ?? String.Empty)
                             == (Unit.Numerator.DescriptionCs ?? String.Empty)) &&
                             ((a.Field<string>(columnName: Core.TargetVariableMetadataList.ColUnitLabelEn.Name) ?? String.Empty)
                             == (Unit.Numerator.LabelEn ?? String.Empty)) &&
                             ((a.Field<string>(columnName: Core.TargetVariableMetadataList.ColUnitDescriptionEn.Name) ?? String.Empty)
                             == (Unit.Numerator.DescriptionEn ?? String.Empty))
                         select a;
                }

                else
                {
                    // Metadata cílových proměnných v tématickém okruhu a pro vybranou jednotku
                    rows =
                        from a in Metadata.Data.AsEnumerable()
                        join b in CmTargetVariableToTopic.Data.AsEnumerable()
                               on a.Field<int>(columnName: Core.TargetVariableMetadataList.ColTargetVariableId.Name)
                                  equals b.Field<int>(columnName: Core.TargetVariableToTopicList.ColTargetVariableId.Name)
                        where
                                   (b.Field<int>(columnName: Core.TargetVariableToTopicList.ColTopicId.Name) == Topic.Id) &&
                                   ((a.Field<string>(columnName: Core.TargetVariableMetadataList.ColUnitLabelCs.Name) ?? String.Empty)
                                   == (Unit.Numerator.LabelCs ?? String.Empty)) &&
                                   ((a.Field<string>(columnName: Core.TargetVariableMetadataList.ColUnitDescriptionCs.Name) ?? String.Empty)
                                   == (Unit.Numerator.DescriptionCs ?? String.Empty)) &&
                                   ((a.Field<string>(columnName: Core.TargetVariableMetadataList.ColUnitLabelEn.Name) ?? String.Empty)
                                   == (Unit.Numerator.LabelEn ?? String.Empty)) &&
                                   ((a.Field<string>(columnName: Core.TargetVariableMetadataList.ColUnitDescriptionEn.Name) ?? String.Empty)
                                   == (Unit.Numerator.DescriptionEn ?? String.Empty))
                        select a;
                }

                if (Data is Core.FnEtlGetTargetVariableTypeList)
                {
                    return
                        new Core.FnEtlGetTargetVariableMetadataList(
                                database: Database,
                                rows: rows);
                }

                if (Data is TargetData.TDFnEtlGetTargetVariableTypeList)
                {
                    return
                        new TargetData.TDFnEtlGetTargetVariableMetadataList(
                                database: Database,
                                rows: rows);
                }

                return new Core.TargetVariableMetadataList(
                    database: Database,
                    rows: rows);
            }
        }

        /// <summary>
        /// <para lang="cs">
        /// Seznam identifikátorů vybraných cílových proměnných
        /// (read-only) (not-null)
        /// </para>
        /// <para lang="en">
        /// List of identifiers for selected target variables
        /// (read-only) (not-null)
        /// </para>
        /// </summary>
        public List<int> SelectedTargetVariableIdentifiers
        {
            get
            {
                if (LstMetadata.RestrictedMetadata == null)
                {
                    return [];
                }

                if (LstMetadata.RestrictedMetadata.Items == null)
                {
                    return [];
                }

                if (LstMetadata.RestrictedMetadata.Items.Count == 0)
                {
                    return [];
                }

                return
                    LstMetadata.RestrictedMetadata.Items
                        .Select(a => a.TargetVariableId)
                        .Distinct<int>()
                        .ToList<int>();
            }
        }

        /// <summary>
        /// <para lang="cs">
        /// Identifikátor vybrané cílové proměnné
        /// (read-only) (nullable)
        /// </para>
        /// <para lang="en">
        /// List of selected target variables
        /// (read-only) (nullable)
        /// </para>
        /// </summary>
        public Nullable<int> SelectedTargetVariableIdentifier
        {
            get
            {
                if ((SelectedTargetVariableIdentifiers == null) ||
                        (SelectedTargetVariableIdentifiers.Count == 0))
                {
                    return null;
                }

                if (SelectedTargetVariableIdentifiers.Count != 1)
                {
                    return null;
                }

                return
                    SelectedTargetVariableIdentifiers.FirstOrDefault<int>();
            }
        }

        /// <summary>
        /// <para lang="cs">
        /// Seznam vybraných cílových proměnných
        /// (read-only) (not-null)
        /// </para>
        /// <para lang="en">
        /// List of selected target variables
        /// (read-only) (not-null)
        /// </para>
        /// </summary>
        public ITargetVariableList SelectedTargetVariables
        {
            get
            {
                if ((SelectedTargetVariableIdentifiers == null) ||
                     (SelectedTargetVariableIdentifiers.Count == 0))
                {
                    return Data.Empty();
                }

                IEnumerable<DataRow> rows = Data.Items
                    .Where(a => SelectedTargetVariableIdentifiers.Contains(item: a.Id))
                    .Select(a => a.Data);

                if (Data is Core.FnEtlGetTargetVariableTypeList)
                {
                    return
                        new Core.FnEtlGetTargetVariableTypeList(
                                database: Database,
                                rows: rows);
                }

                if (Data is TargetData.TDFnEtlGetTargetVariableTypeList)
                {
                    return
                        new TargetData.TDFnEtlGetTargetVariableTypeList(
                                database: Database,
                                rows: rows);
                }

                return new Core.TargetVariableList(
                    database: Database,
                    rows: rows);
            }
        }

        /// <summary>
        /// <para lang="cs">
        /// Vybraná cílová proměnná (read-only) (nullable)
        /// </para>
        /// <para lang="en">
        /// Selected target variable (read-only) (nullable)</para>
        /// </summary>
        public ITargetVariable SelectedTargetVariable
        {
            get
            {
                if (SelectedTargetVariableIdentifier == null)
                {
                    return null;
                }

                return
                    Data.Items
                        .Where(a => a.Id == (int)SelectedTargetVariableIdentifier)
                        .FirstOrDefault<ITargetVariable>();
            }
        }

        /// <summary>
        /// <para lang="cs">Metadata vybrané cílové proměnné (read-only) (nullable)</para>
        /// <para lang="en">Selected target variable metadata (read-only) (nullable)</para>
        /// </summary>
        public ITargetVariableMetadata SelectedTargetVariableMetadata
        {
            get
            {
                if (SelectedTargetVariableIdentifier == null)
                {
                    return null;
                }

                return
                    Metadata.Items
                        .Where(a => a.TargetVariableId == (int)SelectedTargetVariableIdentifier)
                        .FirstOrDefault<ITargetVariableMetadata>();
            }
        }

        #endregion Output Properties

        #endregion Properties


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku</para>
        /// <para lang="en">Initializing the control</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        private void Initialize(Control controlOwner)
        {
            ControlOwner = controlOwner;
            Database = ((INfiEstaControl)ControlOwner).Database;
            onEdit = false;
            onInit = true;

            Topic = null;
            Unit = null;
            Design =
                TargetVariableSelectorDesign.Standard;
            Spacing = [-1];
            NotAssignedCategory = false;
            AdditionalControls = [];
            Data = new Core.TargetVariableList(database: Database);

            MetadataIndicatorText = String.Empty;
            MetadataElementsText = String.Empty;

            Visible = (Database != null) &&
               (Database.Postgres != null) &&
               Database.Postgres.Initialized &&
               MetadataForTopicAndUnit.Items.Count != 0;

            SetFrameColor();

            InitializeLabels();

            // Obsluhy událostí
            splTargetVariable.SplitterMoved += new SplitterEventHandler(
                (sender, e) =>
                {
                    if (!onEdit)
                    {
                        SplitterMoved?.Invoke(sender: sender, e: e);
                    }
                });

            Resize += new EventHandler((sender, e) => { ReDraw(); });

            onInit = false;
        }

        /// <summary>
        /// <para lang="cs">Inicializace seznamu cílových proměnných</para>
        /// <para lang="en">Initialize the list of target variables</para>
        /// </summary>
        private void CreateNewMetadataListBox()
        {
            onEdit = true;
            pnlTargetVariable.Controls.Clear();

            ControlMetadataListBox lstTargetVariable =
                new(
                    controlOwner: this,
                    metadata: Metadata.Empty(),
                    elementType: MetadataElementType.Indicator)
                {
                    Dock = DockStyle.Fill
                };

            lstTargetVariable.SelectedIndexChanged += (sender, e) =>
            {
                if (!onEdit)
                {
                    LstMetadata.Metadata = lstTargetVariable.RestrictedMetadata;
                }
            };

            lstTargetVariable.CreateControl();

            pnlTargetVariable.Controls.Add(
                value: lstTargetVariable);
            onEdit = false;
        }

        /// <summary>
        /// <para lang="cs">Inicializace skupiny ComboBox pro zobrazení metadatových prvků</para>
        /// <para lang="en">Initialize group of ComboBoxes for displaying metadata elements</para>
        /// </summary>
        private void CreateNewMetadataComboBoxList()
        {
            onEdit = true;
            pnlMetadata.Controls.Clear();

            ControlMetadataComboBoxList lstMetadata =
                new(
                    controlOwner: this,
                    metadata: Metadata.Empty())
                {
                    Dock = DockStyle.Fill
                };

            lstMetadata.SelectedTargetVariableChanged += (sender, e) =>
            {
                if (!onEdit)
                {
                    SelectedTargetVariableChanged?.Invoke(
                        sender: this,
                        e: new EventArgs());
                }
            };

            lstMetadata.CreateControl();

            pnlMetadata.Controls.Add(
                value: lstMetadata);
            onEdit = false;
        }

        /// <summary>
        /// <para lang="cs">Nastaví barvu orámování</para>
        /// <para lang="en">Sets frame color</para>
        /// </summary>
        private void SetFrameColor()
        {
            pnlMain.BackColor = SystemColors.Control;
            splTargetVariable.BackColor = SystemColors.Control;
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků ovládacího prvku</para>
        /// <para lang="en">Initialization of control labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            LstTargetVariable?.InitializeLabels();
            LstMetadata.InitializeLabels();

            if (!onInit)
            {
                LoadContent();
            }
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent()
        {
            Visible = false;

            CmTargetVariableToTopic.Load();

            if (Data[0] != null)
            {
                Data.Data.Rows.Remove(row: Data[0].Data);
            }

            if (NotAssignedCategory)
            {
                if (Data[0] == null)
                {
                    Data.Data.Rows.Add(values: [0, Core.TargetVariableMetadataJson.NotAssignedCategory]);
                }
            }

            switch (Design)
            {
                case TargetVariableSelectorDesign.Simple:

                    splTargetVariable.Panel1Collapsed = true;

                    CreateNewMetadataComboBoxList();
                    LstMetadata.Metadata = MetadataForTopicAndUnit;

                    Visible = (Database != null) &&
                       (Database.Postgres != null) &&
                       Database.Postgres.Initialized &&
                       MetadataForTopicAndUnit.Items.Count != 0;

                    SelectedTargetVariableChanged?.Invoke(
                        sender: this,
                        e: new EventArgs());

                    return;

                case TargetVariableSelectorDesign.Standard:

                    splTargetVariable.Panel1Collapsed = false;

                    CreateNewMetadataListBox();
                    LstTargetVariable.Metadata = MetadataForTopicAndUnit;

                    CreateNewMetadataComboBoxList();
                    LstMetadata.Metadata = LstTargetVariable.RestrictedMetadata;

                    Visible = (Database != null) &&
                       (Database.Postgres != null) &&
                       Database.Postgres.Initialized &&
                       MetadataForTopicAndUnit.Items.Count != 0;

                    SelectedTargetVariableChanged?.Invoke(
                        sender: this,
                        e: new EventArgs());

                    return;

                default:
                    return;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastaví rozměry MetadataComboBoxů v seznamu</para>
        /// <para lang="en">Reset size of MetadataComboBoxes in list</para>
        /// </summary>
        public void ReDraw()
        {
            foreach (ControlMetadataComboBoxList controlMetadataComboBoxList in
                pnlMetadata.Controls.OfType<ControlMetadataComboBoxList>())
            {
                controlMetadataComboBoxList.ReDraw();
            }
            Refresh();
        }

        #endregion Methods

    }

}