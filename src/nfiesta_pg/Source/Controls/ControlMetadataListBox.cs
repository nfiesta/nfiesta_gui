﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Versioning;
using System.Windows.Forms;
using ZaJi.PostgreSQL;

namespace ZaJi.NfiEstaPg.Controls
{

    /// <summary>
    /// <para lang="cs">ListBox pro zobrazení metadatového prvku</para>
    /// <para lang="en">ListBox for metadata element display</para>
    /// </summary>
    [SupportedOSPlatform("windows")]
    internal partial class ControlMetadataListBox
        : UserControl, INfiEstaControl
    {

        #region Private Fields

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </summary>
        private Control controlOwner;

        /// <summary>
        /// <para lang="cs">Indikátor editace ovládacího prvku</para>
        /// <para lang="en">Control editing indicator</para>
        /// </summary>
        private bool onEdit;

        /// <summary>
        /// <para lang="cs">Metadata pro zobrazení v ovládacím prvku</para>
        /// <para lang="en">Metadata for display in the control</para>
        /// </summary>
        private ITargetVariableMetadataList metadata;

        /// <summary>
        /// <para lang="cs">Prvek metadat pro zobrazení</para>
        /// <para lang="en">Metadata element to display</para>
        /// </summary>
        private MetadataElementType elementType;

        #endregion Private Fields


        #region Controls

        /// <summary>
        /// <para lang="cs">ListBox pro zobrazení seznamu metadatových prvků</para>
        /// <para lang="en">ListBox for displaying the list of metadata elements</para>
        /// </summary>
        private ListBox lstMetadata;

        #endregion Controls


        #region Events

        /// <summary>
        /// <para lang="cs">Událost
        /// "Změna vybrané položky v seznamu"</para>
        /// <para lang="en">Event
        /// "Change a selected item in the list"</para>
        /// </summary>
        public event EventHandler SelectedIndexChanged;

        #endregion Events


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor ovládacího prvku</para>
        /// <para lang="en">Control constructor</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        /// <param name="metadata">
        /// <para lang="cs">Metadata pro zobrazení v ovládacím prvku</para>
        /// <para lang="en">Metadata for display in the control</para>
        /// </param>
        /// <param name="elementType">
        /// <para lang="cs">Prvek metadat pro zobrazení</para>
        /// <para lang="en">Metadata element to display</para>
        /// </param>
        public ControlMetadataListBox(
            Control controlOwner,
            ITargetVariableMetadataList metadata,
            MetadataElementType elementType)

        {
            InitializeComponent();
            Initialize(
                controlOwner: controlOwner,
                metadata: metadata,
                elementType: elementType);
        }

        #endregion Constructor


        #region Properties

        #region Common Properties

        /// <summary>
        /// <para lang="cs">Vlastník ovládacího prvku (not-null)</para>
        /// <para lang="en">Control owner (not-null)</para>
        /// </summary>
        public Control ControlOwner
        {
            get
            {
                if (controlOwner == null)
                {
                    throw new ArgumentNullException(
                        message: $"Argument {nameof(ControlOwner)} must not be null.",
                        paramName: nameof(ControlOwner));
                }

                if (controlOwner is not INfiEstaControl)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(INfiEstaControl)}.",
                        paramName: nameof(ControlOwner));
                }

                return controlOwner;
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException(
                        message: $"Argument {nameof(ControlOwner)} must not be null.",
                        paramName: nameof(ControlOwner));
                }

                if (value is not INfiEstaControl)
                {
                    throw new ArgumentException(
                        message: $"Argument {nameof(ControlOwner)} must be type of {nameof(INfiEstaControl)}.",
                        paramName: nameof(ControlOwner));
                }

                controlOwner = value;
            }
        }

        /// <summary>
        /// <para lang="cs">Databázové tabulky (read-only)</para>
        /// <para lang="en">Database tables (read-only)</para>
        /// </summary>
        public NfiEstaDB Database
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).Database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jazyková verze (read-only)</para>
        /// <para lang="en">Language version (read-only)</para>
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return ((INfiEstaControl)ControlOwner).LanguageVersion;
            }
        }

        #endregion Common Properties

        #region Input Properties

        /// <summary>
        /// <para lang="cs">Metadata pro zobrazení v ovládacím prvku (not-null)</para>
        /// <para lang="en">Metadata for display in the control (not-null)</para>
        /// </summary>
        public ITargetVariableMetadataList Metadata
        {
            get
            {
                return metadata ??
                    throw new ArgumentNullException(
                        message: $"Argument {nameof(Metadata)} must not be null.",
                        paramName: nameof(Metadata));
            }
            set
            {
                metadata = value ??
                    throw new ArgumentNullException(
                        message: "Argument {nameof(Metadata)} must not be null.",
                        paramName: nameof(Metadata));

                InitializeListBox();
            }
        }

        /// <summary>
        /// <para lang="cs">Prvek metadat pro zobrazení (read-only)</para>
        /// <para lang="en">Metadata element to display (read-only)</para>
        /// </summary>
        public MetadataElementType ElementType
        {
            get
            {
                return elementType;
            }
            private set
            {
                elementType = value;
            }
        }

        #endregion Input Properties

        #region Output Properties

        /// <summary>
        /// <para lang="cs">Vybraná položka metadat (read-only) (nullable)</para>
        /// <para lang="en">Selected metadata item (read-only) (nullable)</para>
        /// </summary>
        public MetadataElement SelectedItem
        {
            get
            {
                if (lstMetadata == null)
                {
                    return null;
                }

                MetadataElement selectedItem =
                   (lstMetadata.SelectedItem != null) ?
                   (MetadataElement)lstMetadata.SelectedItem : null;

                return selectedItem;
            }
        }

        /// <summary>
        /// <para lang="cs">Index vybrané položky v ListBoxu</para>
        /// <para lang="en">Selected ListBoxItem index</para>
        /// </summary>
        public int SelectedIndex
        {
            get
            {
                if (lstMetadata == null)
                {
                    return -1;
                }

                return lstMetadata.SelectedIndex;
            }
            set
            {
                if (lstMetadata == null)
                {
                    return;
                }

                if (lstMetadata.Items == null)
                {
                    return;
                }

                if (value >= lstMetadata.Items.Count)
                {
                    return;
                }

                if (value < 0)
                {
                    return;
                }

                onEdit = true;
                lstMetadata.SelectedIndex = value;
                onEdit = false;

                SelectedIndexChanged?.Invoke(sender: this, e: new EventArgs());
            }
        }

        /// <summary>
        /// <para lang="cs">Metadata omezená výběrem v ovládacím prvku (read-only) (not-null)</para>
        /// <para lang="en">Metadata limited by selection in the control (read-only) (not-null)</para>
        /// </summary>
        public ITargetVariableMetadataList RestrictedMetadata
        {
            get
            {
                if (SelectedItem == null)
                {
                    return Metadata;
                }

                return Metadata.Reduce(
                    elementType: ElementType,
                    element: SelectedItem);
            }
        }

        #endregion Output Properties

        #endregion Properties


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace ovládacího prvku</para>
        /// <para lang="en">Initializing the control</para>
        /// </summary>
        /// <param name="controlOwner">
        /// <para lang="cs">Vlastník ovládacího prvku</para>
        /// <para lang="en">Control owner</para>
        /// </param>
        /// <param name="metadata">
        /// <para lang="cs">Metadata pro zobrazení v ovládacím prvku</para>
        /// <para lang="en">Metadata for display in the control</para>
        /// </param>
        /// <param name="elementType">
        /// <para lang="cs">Prvek metadat pro zobrazení</para>
        /// <para lang="en">Metadata element to display</para>
        /// </param>
        public void Initialize(
            Control controlOwner,
            ITargetVariableMetadataList metadata,
            MetadataElementType elementType)
        {
            ControlOwner = controlOwner;

            onEdit = false;

            ElementType = elementType;

            lstMetadata = null;

            Metadata = metadata;

            InitializeLabels();
        }

        /// <summary>
        /// <para lang="cs">Inicializace seznamu metadatového prvku</para>
        /// <para lang="en">Initializing the metadata element list</para>
        /// </summary>
        public void InitializeListBox()
        {
            pnlMetadata.Controls.Clear();
            List<MetadataElement> dataSource = LanguageVersion switch
            {
                LanguageVersion.National =>
                    [.. Metadata.GetMetadataElementsOfType(elementType: ElementType)
                        .OrderBy(a => (a.TargetVariableId == 0) ? 0 : 1)
                        .ThenBy(a => a.LabelCs)],
                LanguageVersion.International =>
                    [.. Metadata.GetMetadataElementsOfType(elementType: ElementType)
                        .OrderBy(a => (a.TargetVariableId == 0) ? 0 : 1)
                        .ThenBy(a => a.LabelEn)],
                _ =>
                    [.. Metadata.GetMetadataElementsOfType(elementType: ElementType)
                        .OrderBy(a => a.TargetVariableId)],
            };
            onEdit = true;
            if (dataSource.Count != 0)
            {
                lstMetadata = new ListBox()
                {
                    BackColor = System.Drawing.SystemColors.Window,
                    DataSource = dataSource,
                    DisplayMember =
                        (LanguageVersion == LanguageVersion.National) ?
                            "LabelCs" :
                         (LanguageVersion == LanguageVersion.International) ?
                            "LabelEn" :
                            String.Empty,
                    Dock = DockStyle.Fill,
                    Font = new System.Drawing.Font(
                        familyName: "Microsoft Sans Serif",
                        emSize: 9F,
                        style: System.Drawing.FontStyle.Regular,
                        unit: System.Drawing.GraphicsUnit.Point,
                        gdiCharSet: ((byte)(238))),
                    Margin = new Padding(all: 0),
                    SelectedIndex = -1,
                    SelectionMode = SelectionMode.One
                };
                lstMetadata.SelectedIndexChanged += (sender, e) =>
                {
                    if (!onEdit)
                    {
                        SelectedIndexChanged?.Invoke(sender: this, e: e);
                    }
                };
                lstMetadata.CreateControl();
                pnlMetadata.Controls.Add(value: lstMetadata);
                if (lstMetadata.Items.Count > 0)
                {
                    lstMetadata.SelectedIndex = 0;
                }
            }
            else
            {
                lstMetadata = null;
            }
            onEdit = false;
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků ovládacího prvku</para>
        /// <para lang="en">Initializing control labels</para>
        /// </summary>
        public void InitializeLabels()
        {
            if (lstMetadata != null)
            {
                onEdit = true;
                lstMetadata.DisplayMember =
                    (LanguageVersion == LanguageVersion.National) ?
                       "LabelCs" :
                    (LanguageVersion == LanguageVersion.International) ?
                       "LabelEn" :
                        String.Empty;
                onEdit = false;
            }
        }

        /// <summary>
        /// <para lang="cs">Nahrání dat databázových tabulek a jejich zobrazení v ovládacím prvku</para>
        /// <para lang="en">Loading database table data and displaying it in the control</para>
        /// </summary>
        public void LoadContent() { }

        #endregion Methods;

    }
}