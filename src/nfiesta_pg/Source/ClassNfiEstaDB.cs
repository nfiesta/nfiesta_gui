﻿//
// Copyright 2020, 2024 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZaJi.PostgreSQL;
using ZaJi.PostgreSQL.DataModel;

namespace ZaJi
{
    namespace NfiEstaPg
    {

        /// <summary>
        /// Database nfi_esta
        /// </summary>
        public class NfiEstaDB
            : IDatabase
        {

            #region Constants

            /// <summary>
            /// Database managers group role
            /// </summary>
            private const string managerGroupRoleName = "app_nfiesta_mng";

            #endregion Constants


            #region Private Fields

            /// <summary>
            /// PostgreSQL database connection
            /// </summary>
            private ZaJi.PostgreSQL.PostgreSQLWrapper postgres;

            /// <summary>
            /// Schema gisdata
            /// </summary>
            private AuxiliaryData.ADSchema sAuxiliaryData;

            /// <summary>
            /// Schema sdesign
            /// </summary>
            private SDesign.SDesignSchema sDesign;

            /// <summary>
            /// Schema nfiesta
            /// </summary>
            private Core.NfiEstaSchema sNfiEsta;

            /// <summary>
            /// Schema nfiesta_results
            /// </summary>
            private Results.ResultsSchema sResults;

            /// <summary>
            /// Schema target_data
            /// </summary>
            private TargetData.TDSchema sTargetData;

            #endregion Private Fields


            #region Constructor

            /// <summary>
            /// Constructor
            /// </summary>
            public NfiEstaDB()
            {
                Initialize();
            }

            #endregion Constructor


            #region Properties

            /// <summary>
            /// PostgreSQL database connection
            /// (not-null) (read-only)
            /// </summary>
            public ZaJi.PostgreSQL.PostgreSQLWrapper Postgres
            {
                get
                {
                    return postgres ??
                        new ZaJi.PostgreSQL.PostgreSQLWrapper();
                }
                private set
                {
                    postgres = value ??
                        new ZaJi.PostgreSQL.PostgreSQLWrapper();
                }
            }

            /// <summary>
            /// List of database schemas
            /// (not-null) (read-only)
            /// </summary>
            public List<IDatabaseSchema> Schemas
            {
                get
                {
                    return
                    [
                        SAuxiliaryData,
                        SDesign,
                        SNfiEsta,
                        SResults,
                        STargetData,
                    ];
                }
            }

            /// <summary>
            /// Schema gisdata
            /// (not-null) (read-only)
            /// </summary>
            public AuxiliaryData.ADSchema SAuxiliaryData
            {
                get
                {
                    return sAuxiliaryData ??
                        new AuxiliaryData.ADSchema(database: this);
                }
                private set
                {
                    sAuxiliaryData = value ??
                        new AuxiliaryData.ADSchema(database: this);
                }
            }

            /// <summary>
            /// Schema sdesign
            /// (not-null) (read-only)
            /// </summary>
            public SDesign.SDesignSchema SDesign
            {
                get
                {
                    return sDesign ??
                        new SDesign.SDesignSchema(database: this);
                }
                private set
                {
                    sDesign = value ??
                        new SDesign.SDesignSchema(database: this);
                }
            }

            /// <summary>
            /// Schema nfiesta
            /// (not-null) (read-only)
            /// </summary>
            public Core.NfiEstaSchema SNfiEsta
            {
                get
                {
                    return sNfiEsta ??
                        new Core.NfiEstaSchema(database: this);
                }
                private set
                {
                    sNfiEsta = value ??
                        new Core.NfiEstaSchema(database: this);
                }
            }

            /// <summary>
            /// Schema nfiesta_results
            /// (not-null) (read-only)
            /// </summary>
            public Results.ResultsSchema SResults
            {
                get
                {
                    return sResults ??
                        new Results.ResultsSchema(database: this);
                }
                private set
                {
                    sResults = value ??
                        new Results.ResultsSchema(database: this);
                }
            }

            /// <summary>
            /// Schema target_data
            /// (not-null) (read-only)
            /// </summary>
            public TargetData.TDSchema STargetData
            {
                get
                {
                    return sTargetData ??
                        new TargetData.TDSchema(database: this);
                }
                private set
                {
                    sTargetData = value ??
                        new TargetData.TDSchema(database: this);
                }
            }

            /// <summary>
            /// Database managers group role
            /// (not-null) (read-only)
            /// </summary>
            public string ManagerGroupRoleName
            {
                get
                {
                    return managerGroupRoleName;
                }
            }

            /// <summary>
            /// Is selected login role member of group role for database managers?
            /// (not-null) (read-only)
            /// </summary>
            public bool UserIsManager
            {
                get
                {
                    if (!Postgres.Initialized)
                    {
                        return false;
                    }

                    if (Postgres.Connection == null)
                    {
                        return false;
                    }

                    if (String.IsNullOrEmpty(value: Postgres.Connection.UserName))
                    {
                        return false;
                    }

                    if (Postgres.Connection.UserName == "postgres")
                    {
                        return true;
                    }

                    string sql = String.Concat(
                        $"SELECT{Environment.NewLine}",
                        $"    rolname{Environment.NewLine}",
                        $"FROM{Environment.NewLine}",
                        $"    pg_user AS a{Environment.NewLine}",
                        $"LEFT JOIN{Environment.NewLine}",
                        $"    pg_auth_members AS b {Environment.NewLine}",
                        $"    ON (b.member = a.usesysid){Environment.NewLine}",
                        $"LEFT JOIN{Environment.NewLine}",
                        $"    pg_roles AS c{Environment.NewLine}",
                        $"    ON (c.oid = b.roleid){Environment.NewLine}",
                        $"WHERE{Environment.NewLine}",
                        $"    a.usename = '{Postgres.Connection.UserName}';{Environment.NewLine}");

                    DataTable dtGroups = Postgres.ExecuteQuery(sqlCommand: sql);

                    if (dtGroups == null)
                    {
                        return false;
                    }

                    return
                        dtGroups.AsEnumerable()
                            .Select(a => new { GroupRoleName = a.Field<string>(columnName: "rolname") })
                            .Where(a => a.GroupRoleName == ManagerGroupRoleName)
                            .Any();
                }
            }

            #endregion Properties


            #region Methods

            /// <summary>
            /// Object initialization
            /// </summary>
            public void Initialize()
            {
                Postgres = new ZaJi.PostgreSQL.PostgreSQLWrapper();
                SAuxiliaryData = new AuxiliaryData.ADSchema(database: this);
                SDesign = new SDesign.SDesignSchema(database: this);
                SNfiEsta = new Core.NfiEstaSchema(database: this);
                SResults = new Results.ResultsSchema(database: this);
                STargetData = new TargetData.TDSchema(database: this);
            }

            /// <summary>
            /// Connect to database and load catalog data
            /// </summary>
            /// <param name="applicationName">Application name</param>
            public void Connect(string applicationName)
            {
                Postgres.SetConnectionDialog(
                    applicationName: applicationName);

                if (!Postgres.Initialized) { return; }

                Postgres.Catalog.Load();

                AuxiliaryData.ADSchema.Name =
                   (Postgres.Catalog.Extensions == null) ? AuxiliaryData.ADSchema.Name :
                   (Postgres.Catalog.Extensions[AuxiliaryData.ADSchema.ExtensionName] == null) ? AuxiliaryData.ADSchema.Name :
                   (Postgres.Catalog.Extensions[AuxiliaryData.ADSchema.ExtensionName].Schema == null) ? AuxiliaryData.ADSchema.Name :
                   (String.IsNullOrEmpty(value: Postgres.Catalog.Extensions[AuxiliaryData.ADSchema.ExtensionName].Schema.Name)) ? AuxiliaryData.ADSchema.Name :
                   (Postgres.Catalog.Extensions[AuxiliaryData.ADSchema.ExtensionName].Schema.Name == "public") ? AuxiliaryData.ADSchema.Name :
                   Postgres.Catalog.Extensions[AuxiliaryData.ADSchema.ExtensionName].Schema.Name;

                NfiEstaPg.SDesign.SDesignSchema.Name =
                    (Postgres.Catalog.Extensions == null) ? NfiEstaPg.SDesign.SDesignSchema.Name :
                    (Postgres.Catalog.Extensions[NfiEstaPg.SDesign.SDesignSchema.ExtensionName] == null) ? NfiEstaPg.SDesign.SDesignSchema.Name :
                    (Postgres.Catalog.Extensions[NfiEstaPg.SDesign.SDesignSchema.ExtensionName].Schema == null) ? NfiEstaPg.SDesign.SDesignSchema.Name :
                    (String.IsNullOrEmpty(value: Postgres.Catalog.Extensions[NfiEstaPg.SDesign.SDesignSchema.ExtensionName].Schema.Name)) ? NfiEstaPg.SDesign.SDesignSchema.Name :
                    (Postgres.Catalog.Extensions[NfiEstaPg.SDesign.SDesignSchema.ExtensionName].Schema.Name == "public") ? NfiEstaPg.SDesign.SDesignSchema.Name :
                    Postgres.Catalog.Extensions[NfiEstaPg.SDesign.SDesignSchema.ExtensionName].Schema.Name;

                Core.NfiEstaSchema.Name =
                    (Postgres.Catalog.Extensions == null) ? Core.NfiEstaSchema.Name :
                    (Postgres.Catalog.Extensions[Core.NfiEstaSchema.ExtensionName] == null) ? Core.NfiEstaSchema.Name :
                    (Postgres.Catalog.Extensions[Core.NfiEstaSchema.ExtensionName].Schema == null) ? Core.NfiEstaSchema.Name :
                    (String.IsNullOrEmpty(value: Postgres.Catalog.Extensions[Core.NfiEstaSchema.ExtensionName].Schema.Name)) ? Core.NfiEstaSchema.Name :
                    (Postgres.Catalog.Extensions[Core.NfiEstaSchema.ExtensionName].Schema.Name == "public") ? Core.NfiEstaSchema.Name :
                    Postgres.Catalog.Extensions[Core.NfiEstaSchema.ExtensionName].Schema.Name;

                Results.ResultsSchema.Name =
                    (Postgres.Catalog.Extensions == null) ? Results.ResultsSchema.Name :
                    (Postgres.Catalog.Extensions[Results.ResultsSchema.ExtensionName] == null) ? Results.ResultsSchema.Name :
                    (Postgres.Catalog.Extensions[Results.ResultsSchema.ExtensionName].Schema == null) ? Results.ResultsSchema.Name :
                    (String.IsNullOrEmpty(value: Postgres.Catalog.Extensions[Results.ResultsSchema.ExtensionName].Schema.Name)) ? Results.ResultsSchema.Name :
                    (Postgres.Catalog.Extensions[Results.ResultsSchema.ExtensionName].Schema.Name == "public") ? Results.ResultsSchema.Name :
                    Postgres.Catalog.Extensions[Results.ResultsSchema.ExtensionName].Schema.Name;

                TargetData.TDSchema.Name =
                   (Postgres.Catalog.Extensions == null) ? TargetData.TDSchema.Name :
                   (Postgres.Catalog.Extensions[TargetData.TDSchema.ExtensionName] == null) ? TargetData.TDSchema.Name :
                   (Postgres.Catalog.Extensions[TargetData.TDSchema.ExtensionName].Schema == null) ? TargetData.TDSchema.Name :
                   (String.IsNullOrEmpty(value: Postgres.Catalog.Extensions[TargetData.TDSchema.ExtensionName].Schema.Name)) ? TargetData.TDSchema.Name :
                   (Postgres.Catalog.Extensions[TargetData.TDSchema.ExtensionName].Schema.Name == "public") ? TargetData.TDSchema.Name :
                   Postgres.Catalog.Extensions[TargetData.TDSchema.ExtensionName].Schema.Name;


            }

            /// <summary>
            /// Returns the string that represents current object
            /// </summary>
            /// <returns>Returns the string that represents current object</returns>
            public override string ToString()
            {
                return Postgres.Setting.LanguageVersion switch
                {
                    LanguageVersion.National => String.Concat($"Databáze nfi_esta."),
                    LanguageVersion.International => String.Concat($"Database nfi_esta."),
                    _ => String.Concat($"Database nfi_esta."),
                };
            }

            #endregion Methods

        }

    }
}
